using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;

namespace Centrix.UM.Business.Entity
{
    [EntityIManager("Centrix.UM.Business.IManager.IRolePermissionManager,SagePOS.Server.Business.IManager", isLogicalDelete = true)]
    public class RolePermission : EntityBase<RolePermission, int>
    {
        private int rolePermissionId = -1;
        private int roleId = -1; 
        private int permissionid = -1;
        private int entityId = -1;
        private int createdBy = -1; 
        private int updatedBy = -1;

        private Role myRole;
        private Permission myPermission;
        

        #region Constrauctor
        public RolePermission()
        {
        }

        public RolePermission(int permissionId)
        {
            this.permissionid = permissionId;
        }

        public RolePermission(Permission permission)
        {
            myPermission = permission;
        } 
        #endregion

        public int RolePermissionId
        {
            get { return rolePermissionId; }
            set { rolePermissionId = value; }
        }
        public int RoleId
        {
            get { return roleId; }
            set { roleId = value; }
        }
        public int Permissionid
        {
            get { return permissionid; }
            set { permissionid = value; }
        }
        public int EntityId
        {
            get { return this.entityId; }
            set { this.entityId = value; }
        }

        public int CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }
        public int UpdatedBy
        {
            get { return updatedBy; }
            set { updatedBy = value; }
        }

        //[CompositeObject(CompositObjectTypeEnum.Single, "RoleId", LazyLoad = true, SaveBeforeParent = true, CascadeDelete = false)]
        //public Role MyRole
        //{
        //    get
        //    {
        //        if (myRole == null)
        //            loadCompositObject_Lazy("MyRole");
        //        return myRole;
        //    }
        //    set { myRole = value; myRole.MarkModified(); }
        //}

        [CompositeObject(CompositObjectTypeEnum.Single, "Permissionid", LazyLoad = true, SaveBeforeParent = true, CascadeDelete = false)]
        public Permission MyPermission
        {
            get
            {
                if (myPermission == null)
                    loadCompositObject_Lazy("MyPermission");
                return myPermission;
            }
            set { myPermission = value; myPermission.MarkModified(); }
        }

        public override int GetIdentity()
        {
            return rolePermissionId;
        }

        public override void SetIdentity(int value)
        {
            rolePermissionId = value;
        }

        public override void SetParentId(object value, Type ParentType)
        {
            if (ParentType == typeof(Role))
            {
                roleId = Convert.ToInt32(value);
            }
            if (ParentType == typeof(Permission))
            {
                permissionid = Convert.ToInt32(value);
            }
        }

    }
}
