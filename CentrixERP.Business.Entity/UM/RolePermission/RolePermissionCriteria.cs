using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;

namespace Centrix.UM.Business.Entity
{
    public class RolePermissionCriteria : CriteriaBase 
    {
        public int EntityId = -1;
        public bool hasPermission = false;
        public int ParentId = -1;
        public bool getAllPermissions = false;
        public int UserId = -1;
        public int RoleId = -1;
        public DateTime? EntityLastUpdatedDate;
        public DateTime? lastSyncDate;
        
    }
}
