using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Centrix.UM.Business.Entity
{
    public class RolePermissionLite
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public string RoleNameAr { get; set; }
        public int EntityId { get; set; }
        public string EntityName { get; set; }
        public string EntityNameAr { get; set; }
        public int EntityPermissionId { set; get; }
        public int PermissionId { get; set; }
        public string PermissionName { get; set; }
        public string PermissionNameAr { get; set; }
        public string PermissionDescription { get; set; }
        public bool DefaultPermission { get; set; }
        public int RolePermissionId { get; set; }
        public bool HasPermission { get; set; }
        public string CssClass { get; set; }
        public int ParentId { set; get; }
        public string ModuleName { get; set; }
        public string ModuleNameAR { get; set; }
        public int ModuleId { get; set; }
    }
}
