using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Manger.Common.Business.Entity;
using SagePOS.Server.Business.Factory ;


namespace Centrix.UM.Business.Entity
{
    [EntityIManager("Centrix.UM.Business.IManager.IEmailUserManager,SagePOS.Server.Business.IManager", isLogicalDelete = true)]
    public class EmailUser: EntityBase< EmailUser, int>//,IEmail
    {
        #region "Constructors"
        public EmailUser()
        {
            }
        public EmailUser(Email email)
        {
            emailobj = email;
        }
        
        public EmailUser(int email_user_id)
        {
            this.emailuserid=email_user_id;
            }
        #endregion
        
        #region "Private Property"
		private int emailuserid=-1;
		private int emailid=-1;
		private int userid=-1;
		private bool isdefault;
		private bool issystem;
		private int createdby=-1;
		private DateTime? createddate;
		private int updatedby=-1;
		private DateTime? updateddate;
        private bool flagdeleted;

        #endregion
        
        #region "Public Property"
		public int EmailUserId
		{
			get {return emailuserid;}
			set {emailuserid=value;}
		}

		public int EmailId
		{
			get {return emailid;}
			set {emailid=value;}
		}

		public int UserId
		{
			get {return userid;}
			set {userid=value;}
		}

        public bool Isdefault
		{
			get {return isdefault;}
			set {isdefault=value;}
		}

		public bool IsSystem
		{
			get {return issystem;}
			set {issystem=value;}
		}

		public int CreatedBy
		{
			get {return createdby;}
			set {createdby=value;}
		}

		public DateTime? CreatedDate
		{
			get {return createddate;}
			set {createddate=value;}
		}

		public int UpdatedBy
		{
			get {return updatedby;}
			set {updatedby=value;}
		}

		public DateTime? UpdatedDate
		{
			get {return updateddate;}
			set {updateddate=value;}
		}

        public bool FlagDeleted
        {
            get { return flagdeleted; }
            set { flagdeleted = value; }
        }

        #endregion
        
        #region "Composite Objects"

		private Email emailobj;
		[CompositeObject(CompositObjectTypeEnum.Single, "EmailId", LazyLoad = true, SaveBeforeParent = true, CascadeDelete = false)]
		public Email EmailObj
		{
			get{
				if (emailobj == null)
					loadCompositObject_Lazy("EmailObj");
				return emailobj; }
			set { emailobj = value; }
		}

		private User userobj;
		[CompositeObject(CompositObjectTypeEnum.Single, "UserId", LazyLoad = true, SaveBeforeParent = true, CascadeDelete = false)]
		public User UserObj
		{
			get{
				if (userobj == null)
					loadCompositObject_Lazy("UserObj");
				return userobj; }
			set { userobj = value; }
		}
        #endregion
        
        #region "Entity Base Methods"
         public override int GetIdentity()
        {
            return emailuserid;
        }

        public override void SetIdentity(int value)
        {
             emailuserid = value;
        }

        public override void SetParentId(object value, Type ParentType)
        {
            if (ParentType == typeof(User))
            {
                userid = Convert.ToInt32(value);
            }
            if (ParentType == typeof(Email))
            {
                EmailId = Convert.ToInt32(value);
            }
        }
        
        #endregion 
    
        #region IEmail Members

        public int Id
        {
            get
            {
                return this.emailuserid;
            }
            set
            {
                this.emailuserid = value;
            }
        }

        public int ModuleId
        {
            get
            {
                return this.userid;
            }
            set
            {
                this.userid = value;
            }
        }
        public int EmailType { get; set; }


        #endregion
    }
}


