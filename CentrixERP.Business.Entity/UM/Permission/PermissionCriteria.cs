using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;

namespace Centrix.UM.Business.Entity
{
    public class PermissionCriteria : CriteriaBase 
    {
        public int RoleToExclude;
        public string KeyWord;
        public DateTime? lastSyncDate;

    }
}
