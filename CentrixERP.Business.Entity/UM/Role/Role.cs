using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;



namespace Centrix.UM.Business.Entity
{
    [EntityIManager("Centrix.UM.Business.IManager.IRoleManager,SagePOS.Server.Business.IManager", isLogicalDelete = true)]
    public class Role : EntityBase<Role, int>
    {
        private int roleId = -1;
        private string nameArabic;
        private string nameEnglish;
        private string description;
        private int permissionLevel = -1;
        private int createdBy = -1;       
        private int updatedBy = -1;

        private List<RolePermission> myRolePermission;

        public int RoleId
        {
            get { return roleId; }
            set { roleId = value; }
        }
        public string NameArabic
        {
            get { return nameArabic; }
            set { nameArabic = value; }
        }
        public string NameEnglish
        {
            get { return nameEnglish; }
            set { nameEnglish = value; }
        }
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
        public int PermissionLevel
        {
            get { return permissionLevel; }
            set { permissionLevel = value; }
        }
        public int CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }
        public int UpdatedBy
        {
            get { return updatedBy; }
            set { updatedBy = value; }
        }



        [CompositeObject(CompositObjectTypeEnum.List, "RoleId", LazyLoad = false, SaveBeforeParent = false, CascadeDelete = true,Savable=false)]
        public List<RolePermission> MyRolePermission
        {
            get
            {
                if (myRolePermission == null)
                    loadCompositObject_Lazy("MyRolePermission");
                return myRolePermission;
            }
            set { myRolePermission = value; }
        }

        public override int GetIdentity()
        {
            return roleId;
        }

        public override void SetIdentity(int value)
        {
            roleId = value;
        }

        public override void SetParentId(object value, Type ParentType)
        {
            throw new NotImplementedException();
        }
    }
}
