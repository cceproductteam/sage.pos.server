using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;

namespace Centrix.UM.Business.Entity
{
    public class RoleCriteria:CriteriaBase 
    {
        public string Name;
        public string keyword;
        public int pageNumber = -1;
        public int resultCount = -1;
        public DateTime? lastSyncDate;
    }
}
