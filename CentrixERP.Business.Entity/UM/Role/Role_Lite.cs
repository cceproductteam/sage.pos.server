using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Centrix.UM.Business.Entity
{
    public class Role_Lite
    {
        public int Id { get; set; }
        public string NameArabic { get; set; }
        public string NameEnglish { get; set; }
        public string Description { get; set; }
        public int PermissionLevel { get; set; }
        public int CreatedById { get; set; }
        public int UpdatedById { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { set; get; }
        public bool LastRows { get; set; }
        public int TotalRecords { get; set; }
        public string label { set; get; }
        public int value { set; get; }
        public DateTime? PermissionsUpdatedDate { set; get; }
        public int UsersCount { set; get; }
    }
}
