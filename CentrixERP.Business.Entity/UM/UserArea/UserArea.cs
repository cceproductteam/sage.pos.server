using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;

namespace Centrix.UM.Business.Entity
{
    [EntityIManager("Centrix.UM.Business.IManager.IUserAreaManager,SagePOS.Server.Business.IManager", isLogicalDelete = true)]
    public class UserArea : EntityBase<UserArea, int>
    {
        private int userAreaId = -1;
        private int userId = -1;
        private int areaId = -1;
        private int createdBy = -1;
        private int updatedBy = -1;

        private User myUser;       
        private Area myArea;

        #region Constrauctor
        public UserArea()
        {
        }

        public UserArea(int AreaId)
        {
            areaId = AreaId;
        }

        public UserArea(Area area)
        {
            myArea = area;
        } 
        #endregion

        public int UserAreaId
        {
            get { return userAreaId; }
            set { userAreaId = value; }
        }
        public int UserId
        {
            get { return userId; }
            set { userId = value; }
        }
        public int AreaId
        {
            get { return areaId; }
            set { areaId = value; }
        }
        public int CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }
        public int UpdatedBy
        {
            get { return updatedBy; }
            set { updatedBy = value; }
        }

        //[CompositeObject(CompositObjectTypeEnum.Single, "UserId", LazyLoad = true, SaveBeforeParent = true, CascadeDelete = false)]
        //public User MyUser
        //{
        //    get
        //    {
        //        if (myUser == null)
        //            loadCompositObject_Lazy("MyUser");
        //        return myUser;
        //    }
        //    set { myUser = value; myUser.MarkModified(); }
        //}

        [CompositeObject(CompositObjectTypeEnum.Single, "AreaId", LazyLoad = true, SaveBeforeParent = true, CascadeDelete = false)]
        public Area MyArea
        {
            get {
                if (myArea == null)
                    loadCompositObject_Lazy("MyArea");
                return myArea; }
            set { myArea = value; myArea.MarkModified(); }
        }

        public override int GetIdentity()
        {
            return userAreaId;
        }

        public override void SetIdentity(int value)
        {
            userAreaId = value;
        }

        public override void SetParentId(object value, Type ParentType)
        {
            if (ParentType == typeof(User))
            {
                userId = Convert.ToInt32(value);
            }
            if (ParentType == typeof(Area))
            {
                areaId = Convert.ToInt32(value);
            }
        }
    }
}
