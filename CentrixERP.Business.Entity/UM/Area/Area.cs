using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;

namespace Centrix.UM.Business.Entity
{
    [EntityIManager("Centrix.UM.Business.IManager.IAreaManager,SagePOS.Server.Business.IManager", isLogicalDelete = false)]
    public class Area : EntityBase<Area, int>
    {
        private int areaId = -1;
        private string nameEnglish;
        private string nameArabic;
        private string note;
        private int createdBy = -1;
        private int updatedBy = -1;


        public int AreaId
        {
            get { return areaId; }
            set { areaId = value; }
        }
        public string NameEnglish
        {
            get { return nameEnglish; }
            set { nameEnglish = value; }
        }
        public string NameArabic
        {
            get { return nameArabic; }
            set { nameArabic = value; }
        }
        public string Note
        {
            get { return note; }
            set { note = value; }
        }
        public int CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }
        public int UpdatedBy
        {
            get { return updatedBy; }
            set { updatedBy = value; }
        }

        public override int GetIdentity()
        {
            return areaId;
        }

        public override void SetIdentity(int value)
        {
            areaId = value;
        }

        public override void SetParentId(object value, Type ParentType)
        {
            throw new NotImplementedException();
        }
    }
}
