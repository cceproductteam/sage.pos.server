using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;

namespace Centrix.UM.Business.Entity
{
    [EntityIManager("Centrix.UM.Business.IManager.IUserLogginManager,SagePOS.Server.Business.IManager", isLogicalDelete = true)]
    public class UserLoggin : EntityBase<UserLoggin, int>
    {
        #region Constructors

        public UserLoggin() { }

        public UserLoggin(int userLogginId)
        {
            this.userLogginId = userLogginId;
        }

        #endregion

        #region Private Properties

		private int userLogginId;

		private int userId = -1;

		private string userAgent;

		private string localIp;

		private string publicIp;

		private DateTime? lastRequest;

		private string randomString;


        #endregion

        #region Public Properties

		public int UserLogginId
		{
			get
			{
				return userLogginId;
			}
			set
			{
				userLogginId = value;
			}
		}
		public int UserId
		{
			get
			{
				return userId;
			}
			set
			{
				userId = value;
			}
		}
		public string UserAgent
		{
			get
			{
				return userAgent;
			}
			set
			{
				userAgent = value;
			}
		}
		public string LocalIp
		{
			get
			{
				return localIp;
			}
			set
			{
				localIp = value;
			}
		}
		public string PublicIp
		{
			get
			{
				return publicIp;
			}
			set
			{
				publicIp = value;
			}
		}
		public DateTime? LastRequest
		{
			get
			{
				return lastRequest;
			}
			set
			{
				lastRequest = value;
			}
		}
		public string RandomString
		{
			get
			{
				return randomString;
			}
			set
			{
				randomString = value;
			}
		}

        #endregion

        #region Composite Objects
		private User userObj;
		[CompositeObject(CompositObjectTypeEnum.Single, "UserId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable= false)]
		public User UserObj
		{
			get
			{
				if (userObj == null)
					loadCompositObject_Lazy("UserObj");
				return userObj;
			}
			set
			{
				userObj = value;;
			}
		}
		


        #endregion

        public override int GetIdentity()
        {
            return userLogginId;
        }
        public override void SetIdentity(int value)
        {
            userLogginId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
			if (ParentType == typeof(User))
				this.userId = (int)value;

        }
    }


    public class UserLogginLite
    {
        public int UserId { get; set; }
        public string RandomString { get; set; }
        public string LocalIp { get; set; }
        public string PublicIp { get; set; }
        public string UserAgent { get; set; }
    }
}