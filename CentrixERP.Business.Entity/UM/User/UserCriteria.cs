using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Factory ;

namespace Centrix.UM.Business.Entity  
{
    public class UserCriteria : CriteriaBase, IAdvancedSearchCriteria
    {
        public string Name;
        public string Title;
        public DateTime? DOBFrom;
        public DateTime? DOBTo;
        public int Role;
        public int Team;
        public int Area;
        public string FirstName;
        public string MiddleName;
        public string LastName;
        public string FirstNameAR;
        public string MiddleNameAR;
        public string LastNameAR;
        public string Keyword;
        public string Serach;
        public int schedualtaskid = -1;
        public int pageNumber = -1;
        public int resultCount = -1;
        public int ExceptUserId=-1;
        public bool isManager = false;
        public int? UserId = -1;
        public bool showSuperUser = true;

        public string SearchCriteria { get; set; }
        public bool SortType { get; set; }
        public int EntityId { get; set; }
        public string SortFields { get; set; }
        public string UserTypeFilter { get; set; }
        public string UserIds { get; set; }
        public DateTime? lastSyncDate;

        #region IAdvancedSearchCriteria Members


        int IAdvancedSearchCriteria.pageNumber
        {
            get
            {
                return pageNumber;
            }
            set
            {
                pageNumber = value;
            }
        }

        int IAdvancedSearchCriteria.resultCount
        {
            get
            {
                return resultCount;
            }
            set
            {
                resultCount = value;
            }
        }

        #endregion
    }
}
