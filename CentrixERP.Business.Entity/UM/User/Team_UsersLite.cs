using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Centrix.UM.Business.Entity
{
    public class TeamUsersLite
    {

        private DateTime? dob;
        public int Id { get; set; }
        public string IsDefault { get; set; }
        public bool IsDefaultValue { get; set; }

        public string IsManager { get; set; }
        public bool IsManagerValue { get; set; }

        public int ManagerId { set; get; }
        public string Manager { set; get; }

        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string FirstnameEnglish { get; set; }
        public string MiddleNameEnglish { get; set; }
        public string LastNameEnglish { get; set; }
        public string FirstNameArabic { get; set; }
        public string MiddleNameArabic { get; set; }
        public string LastNameArabic { get; set; }
        public DateTime? Dob
        {
            get
            {
                return dob;
            }
            set
            {
                if (value.HasValue)
                {
                    DobValue = value.Value.ToString(ConfigurationManager.AppSettings["DotNetDateTimeFormat"]);
                }
                dob = value;
            }
        }
        public string DobValue { get; set; }
        public string Title { get; set; }
        public string Notes { get; set; }
        public int CreatedById { get; set; }
        public int UpdatedById { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string UpdatedByName { get; set; }
        public string CreatedByName { get; set; }
        public int RoleNameId { get; set; }
        public int TeamNameId { get; set; }
        public bool ReceiveNoyificationsValue { get; set; }
        public string ReceiveNoyifications { get; set; }
        public string FullName { get; set; }
        public string RoleName { get; set; }
        public string TeamName { get; set; }
        public string UserEmail { get; set; }
        public bool IsSupperUser { set; get; }
        public bool IsPosUser { set; get; }
        public bool LastRows { get; set; }
        public int TotalRecords { get; set; }
        public string label { set; get; }
        public int value { set; get; }
        public bool IsManagement { get; set; }
        public string UserEmailTruncated { get; set; }
        public bool HasTeam { get; set; }
    }
}
