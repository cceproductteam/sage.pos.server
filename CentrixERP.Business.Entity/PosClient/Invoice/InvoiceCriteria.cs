using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
using SagePOS.Server.Business.Factory ;

namespace SagePOS.Server.Business.Entity
{
    public class InvoiceCriteria : CriteriaBase, IAdvancedSearchCriteria
    {
       

        [CrtiteriaField("@page_number", SqlDbType.Int)]
        public int? pageNumber { get; set; }
        [CrtiteriaField("@is_refund", SqlDbType.Int)]
        public bool? isRefund { get; set; }
        [CrtiteriaField("@result_count", SqlDbType.Int)]
        public int? resultCount { get; set; }

        [CrtiteriaField("@keyword", SqlDbType.NVarChar)]
        public string Keyword { get; set; }



        [CrtiteriaField("@search_criteria", SqlDbType.NVarChar)]
        public string SearchCriteria { get; set; }

        [CrtiteriaField("@sort_type", SqlDbType.Bit)]
        public bool? SortType { get; set; }

        [CrtiteriaField("@sort_fields", SqlDbType.NVarChar)]
        public string SortFields { get; set; }

        [CrtiteriaField("@invoice_id", SqlDbType.Int)]
        public int? InvoiceId { get; set; }

        [CrtiteriaField("@invoice_id", SqlDbType.Int)]
        public int? EntityId { get; set; }

        [CrtiteriaField("@status", SqlDbType.Int)]
        public int Status { get; set; }


        [CrtiteriaField("@cutomer_name", SqlDbType.NVarChar)]
        public string CustomerName { get; set; }

        [CrtiteriaField("@status", SqlDbType.NVarChar)]
        public string IntegraitonStatus { get; set; }

        [CrtiteriaField("@register_id", SqlDbType.Int)]
        public int? RegisterId { get; set; }

        [CrtiteriaField("@invoice_number", SqlDbType.NVarChar)]
        public string InvoiceNumber { get; set; }

        [CrtiteriaField("@store_id", SqlDbType.Int)]
        public int? StoreId { get; set; }

        [CrtiteriaField("@transaction_number", SqlDbType.NVarChar)]
        public string TransactionNumber { get; set; }

        [CrtiteriaField("@customer_id", SqlDbType.Int)]
        public int? CustomerId { get; set; }

        [CrtiteriaField("@date_from", SqlDbType.NVarChar)]
        public string DateFrom { get; set; }

        [CrtiteriaField("@date_to", SqlDbType.NVarChar)]
        public string DateTo { get; set; }

        [CrtiteriaField("@close_register_id", SqlDbType.Int)]
        public int CloseRegisterId { get; set; }

        //[CrtiteriaField("@dateFrom", SqlDbType.DateTime)]
        //public DateTime DateFromRe { get; set; }

        //[CrtiteriaField("@dateTo", SqlDbType.DateTime)]   
        //public DateTime DateTo { get; set; }


        [CrtiteriaField("@type_id", SqlDbType.Int)]
        public int? TypeId { get; set; }

        //[CrtiteriaField("@customer_type", SqlDbType.Int)]
        //public int? CustomerType { get; set; }

        [CrtiteriaField("@contact_name", SqlDbType.NVarChar)]
        public string ContactName { get; set; }

        [CrtiteriaField("@is_synced", SqlDbType.Bit)]
        public bool isSynced { get; set; }

        [CrtiteriaField("@is_posted_to_accpac", SqlDbType.Bit)]
        public bool isPostedToAccpac { get; set; }

        [CrtiteriaField("@invoice_Statuses", SqlDbType.NVarChar)]
        public string InvoiceStatuses { get; set; }

        //public bool IsConvertedToShipment { get; set; }


        //public bool IsConvertedToshipmentReturn { get; set; }

        public string InvoiceType { get; set; }





















        int IAdvancedSearchCriteria.EntityId
        {
            get;
            set;
        }

        bool IAdvancedSearchCriteria.SortType
        {
            get;
            set;
        }

        int IAdvancedSearchCriteria.pageNumber
        {
            get;
            set;
        }

        int IAdvancedSearchCriteria.resultCount
        {
            get;
            set;
        }
    }
}
