using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace SagePOS.Server.Business.Entity
{
    public class InvoiceLite : EntityBaseLite
    {

        #region Public Properties

        [LiteProperty("invoice_id", SqlDbType.Int)]
        public int InvoiceId
        {
            get;
            set;
        }

        [LiteProperty("invoice_date", SqlDbType.DateTime)]
        public DateTime? InvoiceDate
        {
            get;
            set;
        }

        [LiteProperty("contact_id", SqlDbType.Int)]
        public int Contactid
        {
            get;
            set;
        }
        [LiteProperty("contact_name", SqlDbType.NVarChar)]
        public string ContactName
        {
            get;
            set;
        }
        [LiteProperty("register_name", SqlDbType.NVarChar)]
        public string RegisterName
        {
            get;
            set;
        }




        [LiteProperty("total_price", SqlDbType.Money)]
        public Decimal TotalPrice
        {
            get;
            set;
        }

        [LiteProperty("grand_total", SqlDbType.Money)]
        public Decimal GrandTotal
        {
            get;
            set;
        }

        [LiteProperty("percentage_discount", SqlDbType.Float)]
        public Double PercentageDiscount
        {
            get;
            set;
        }

        [LiteProperty("tax_amount", SqlDbType.Decimal)]
        public Object TaxAmount
        {
            get;
            set;
        }

        [LiteProperty("note", SqlDbType.NVarChar)]
        public string Note
        {
            get;
            set;
        }

        [LiteProperty("amount_discount", SqlDbType.Float)]
        public Double AmountDiscount
        {
            get;
            set;
        }

        [LiteProperty("invoice_status", SqlDbType.Int)]
        public int InvoiceStatus
        {
            get;
            set;
        }



        [LiteProperty("store_id", SqlDbType.Int)]
        public int StoreId
        {
            get;
            set;
        }

        [LiteProperty("register_id", SqlDbType.Int)]
        public int RegisterId
        {
            get;
            set;
        }

        [LiteProperty("invoice_number", SqlDbType.NVarChar)]
        public string InvoiceNumber
        {
            get;
            set;
        }





        [LiteProperty("change_amount", SqlDbType.Float)]
        public Double ChangeAmount
        {
            get;
            set;
        }

        [LiteProperty("parent_invoice_id", SqlDbType.Int)]
        public int ParentInvoiceId
        {
            get;
            set;
        }

        [LiteProperty("sale_man_id", SqlDbType.Int)]
        public int SaleManId
        {
            get;
            set;
        }

        [LiteProperty("sales_man_name", SqlDbType.NVarChar)]
        public string SalesManName
        {
            get;
            set;
        }









        [LiteProperty("items_discount", SqlDbType.Float)]
        public Double ItemsDiscount
        {
            get;
            set;
        }

        [LiteProperty("is_synced", SqlDbType.Bit)]
        public bool? IsSynced
        {
            get;
            set;
        }

        [LiteProperty("invoice_unique_number", SqlDbType.NVarChar)]
        public string InvoiceUniqueNumber
        {
            get;
            set;
        }




        [LiteProperty("taxable_invoice", SqlDbType.Bit)]
        public bool? IsTaxableInvoice
        {
            get;
            set;
        }



        [LiteProperty("invoice_id", SqlDbType.Int)]
        public int value
        {
            get;
            set;
        }

        [LiteProperty("invoice_id", SqlDbType.Int)]
        public int label
        {
            get;
            set;
        }

        [LiteProperty("flag_deleted", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? FlagDeleted
        {
            get;
            set;
        }

        [LiteProperty("is_system", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? IsSystem
        {
            get;
            set;
        }

        [LiteProperty("created_date", SqlDbType.DateTime)]
        public DateTime? CreatedDate
        {
            get;
            set;
        }

        [LiteProperty("updated_date", SqlDbType.DateTime)]
        public DateTime? UpdatedDate
        {
            get;
            set;
        }

        [LiteProperty("created_by", SqlDbType.Int, FindByIdLiteParameter = false)]
        public int CreatedBy
        {
            get;
            set;
        }

        [LiteProperty("updated_by", SqlDbType.Int, FindByIdLiteParameter = false)]
        public int UpdatedBy
        {
            get;
            set;
        }


        [LiteProperty("created_by_name", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string CreatedByName { get; set; }

        [LiteProperty("updated_by_name", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string UpdatedByName { get; set; }

        [LiteProperty("invoice_id", SqlDbType.NVarChar)]
        public int Id
        {
            get { return this.InvoiceId; }
            set { this.InvoiceId = value; }
        }

        [LiteProperty("last_rows", SqlDbType.NVarChar)]
        public bool LastRows { get; set; }

        [LiteProperty("total_count", SqlDbType.NVarChar)]
        public int TotalRecords { get; set; }
        [LiteProperty("mobile_number", SqlDbType.NVarChar)]
        public string MobileNumber { get; set; }
        //[LiteProperty("sync_number", SqlDbType.NVarChar)]
        //public string SyncNumber { get; set; }


        //[LiteProperty("sync_status", SqlDbType.Int)]
        //public int SyncStatus { get; set; }
        [LiteProperty("sync_number", SqlDbType.NVarChar)]
        public string SyncNumber
        {
            get;
            set;
        }

        [LiteProperty("sync_number", SqlDbType.NVarChar)]
        public string SyncStatus
        {
            get;
            set;
        }

        [LiteProperty("accpac_invoice_no", SqlDbType.NVarChar)]
        public string AccpacInvoiceNo
        {
            get;
            set;
        }
        [LiteProperty("accpac_orderentry_no", SqlDbType.NVarChar)]
        public string AccpacOrderentryNo
        {
            get;
            set;
        }
        [LiteProperty("accpac_shipment_no", SqlDbType.NVarChar)]
        public string AccpacShipmentNo
        {
            get;
            set;
        }
        [LiteProperty("accpac_prepyament_no", SqlDbType.NVarChar)]
        public string AccpacPrepyamentNo
        {
            get;
            set;
        }
        [LiteProperty("accpac_refund_no", SqlDbType.NVarChar)]
        public string AccpacRefundNo
        {
            get;
            set;
        }

        [LiteProperty("accpac_cust_id", SqlDbType.NVarChar)]
        public string AccpacCustId
        {
            get; set;
        }




        #endregion


    }

    public class InvoiceIntegration
    {
        public int ParentInvoiceId { get; set; }
        public int InvoiceId
        {
            get;
            set;
        }

        public DateTime? InvoiceDate
        {
            get;
            set;
        }

        public string InvoiceUniqueNumber
        {
            get;
            set;
        }

        public double GrandTotal
        {
            get;
            set;
        }

        public double AmountDiscount
        {
            get;
            set;
        }
        public double PercentageDiscount
        {
            get;
            set;
        }

        public string InvoiceNumber
        {
            get;
            set;
        }

        public int ArCashCustomerId
        {
            get;
            set;
        }

        public int ArVisaCustomerId
        {
            get;
            set;
        }

        public int ArMixedCustomerId
        {
            get;
            set;
        }

        public int LocationId
        {
            get;
            set;
        }

        public string PersonName
        {
            get;
            set;
        }

        public double TotalTaxAmount
        {
            get;
            set;
        }
        public double TotalDiscountAmount
        {
            get;
            set;
        }
        public int TaxClassId
        {
            get;
            set;
        }

        public int CurrencyId
        {
            get;
            set;
        }
        public double TotalAmount
        {
            get;
            set;
        }
        public int CustomerId { get; set; }
        public int NonTaxableClassId { get; set; }

        public string CreditNoteAccountIds { get; set; }

        public double ChangeAmount { get; set; }

        public int InvoiceStatus { get; set; }

    }



    public class InvoiceInquiryLite
    {
        public int InvoiceId
        {
            get;
            set;
        }

        public string InvoiceNumber
        {
            get;
            set;
        }

        public string Type { get; set; }


        public DateTime? InvoiceDate
        {
            get;
            set;
        }

        public string ShipmentNumber { get; set; }


        public double Total
        {
            get;
            set;
        }

        public string Store
        {
            get;
            set;
        }


        public string Register
        {
            get;
            set;
        }

        public int ShipmentId
        {
            get;
            set;
        }

        public int RowNum
        {
            get;
            set;
        }
        public double CashAmount
        {
            get;
            set;
        }
        public double CreditCardAmount
        {
            get;
            set;
        }

        public double ChequeAmount
        {
            get;
            set;
        }
        public double OnAccountAmount
        {
            get;
            set;
        }
        public double TotalPrice
        {
            get;
            set;
        }
        public double Tax
        {
            get;
            set;
        }
        public double AmountDiscount
        {
            get;
            set;
        }
        public double ItemsDiscount
        {
            get;
            set;
        }

        public double TotalPayment
        {
            get;
            set;
        }

        public string Currency
        {
            get;
            set;
        }



    }

    public class POSQuantityInQuiryLite
    {
        public string LocationName
        {
            get;
            set;

        }
        public string ItemName
        {
            get;
            set;

        }
        public string ItemNumber
        {
            get;
            set;

        }

        public string StoreName
        {
            get;
            set;
        }

        public int ERPQuantity
        {
            get;
            set;

        }
        public int POSQuantity
        {
            get;
            set;

        }
        public int StagingQuantity
        {
            get;
            set;
        }

        public int PendingQTY
        {
            get;
            set;

        }
        public int SummationQTY
        {
            get;
            set;

        }

        public int StagingOutQuantity
        {
            get;
            set;
        }

        public int PendingOutQuantity
        {
            get;
            set;
        }

        public int RowNum
        {
            get;
            set;
        }
    }

    public class ICQuantityInQuiryLite
    {
        public string LocationName
        {
            get;
            set;

        }
        public string ItemName
        {
            get;
            set;

        }
        public string ItemNumber
        {
            get;
            set;

        }

        public string StoreName
        {
            get;
            set;
        }

        public int ERPQuantity
        {
            get;
            set;

        }
        public int POSQuantity
        {
            get;
            set;

        }
        public int StagingQuantity
        {
            get;
            set;
        }

        public int PendingQTY
        {
            get;
            set;

        }
        public int SummationQTY
        {
            get;
            set;

        }

        public int StagingOutQuantity
        {
            get;
            set;
        }

        public int PendingOutQuantity
        {
            get;
            set;
        }

        public int RowNum
        {
            get;
            set;
        }
        public Double AverageCost
        {
            get;
            set;
        }
    }

    public class POSDailyRecieptsReport
    {

        public string Date { get; set; }
        public string StoreName { get; set; }
        public string RegisterName { get; set; }
        public string PaymentTypeName { get; set; }
        public string Amount { get; set; }
        public string Total { get; set; }
        public string Currency { get; set; }
        public string StoreCurrency { get; set; }
        public string AmountStoreCurrency { get; set; }
        public string TransactionNumber { get; set; }

    }

    public class POSDailySalesReport
    {

        public string InvoiceNumber { get; set; }
        public string StoreName { get; set; }

        public string TransactionType { get; set; }

        public string ItemNumber { get; set; }

        public string ProductName { get; set; }

        public string Quantity { get; set; }

        public string OriginalPrice { get; set; }

        public string UnitPrice { get; set; }

        public string ItemDiscount { get; set; }
        public string InvoiceDiscountPerItem { get; set; }
        public string TaxAmount { get; set; }

        public string TotalPrice { get; set; }

        public string ContactName { get; set; }

        public string InvoiceDate { get; set; }
        public string RegisterName { get; set; }
        // public string SyncNumber { get; set; }
        //public int SyncStatus { get; set; }
        public string SyncNumber { get; set; }

        public string SyncStatus { get; set; }
        public string AccpacInvoiceNo { get; set; }
        public string AccpacOrderentryNo { get; set; }
        public string AccpacShipmentNo { get; set; }
        public string AccpacPrepyamentNo { get; set; }
        public string AccpacRefundNo { get; set; }
    }

    public class POSDailySalesSummary
    {
        public string InvoiceNumber { get; set; }
        public string StoreName { get; set; }

        public string TransactionType { get; set; }

        public string SalesManName { get; set; }

        public string TotalUnitPrice { get; set; }



        public string GrandTotal { get; set; }

        public string ItemDiscount { get; set; }
        public string InvoiceDiscountPerItem { get; set; }
        public string TaxAmount { get; set; }

        public string TotalPrice { get; set; }

        public string ContactName { get; set; }

        public string InvoiceDate { get; set; }

        public string Payments { get; set; }
    }


    public class POSDaileSalesSummation
    {
        public string StoreName { get; set; }
        public string RegisterName { get; set; }
        public string InvoiceDate { get; set; }
        public string TotalSales { get; set; }
        public string TotalRefund { get; set; }
        public string TotalExchange { get; set; }
        public string Total { get; set; }
    }


    public class POSPettyCash
    {
        public string StoreName { get; set; }
        public string RegisterName { get; set; }
        public string InvoiceNumber { get; set; }
        public string GrandTotal { get; set; }
        public string InvoiceDate { get; set; }
        public int SaleManId { get; set; }

        public string defaultcurrsymbol { get; set; }
        public string SaleManName { get; set; }
        public string Type { get; set; }
        public string SessionNumber { get; set; }
    }

    public class DashBoarBoxes
    {

        public string Stores { get; set; }
        public string Registers { get; set; }
        public string Users { get; set; }
        public string QuickKeys { get; set; }
    }

    public class RegistersSales
    {
        public string RegisterName { get; set; }
        public string Total { get; set; }
        public string Date { get; set; }
    }

    public class StoreSales
    {
        public string StoreName { get; set; }
        public string Total { get; set; }
    }

   
    



}