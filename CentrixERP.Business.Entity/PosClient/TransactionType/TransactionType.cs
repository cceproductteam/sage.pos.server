using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;


namespace SagePOS.Server.Business.Entity
{
    [EntityIManager("SagePOS.Server.Business.IManager.ITransactionTypeManager,SagePOS.Server.Business.IManager", isLogicalDelete = true)]
    public class TransactionType : EntityBase<TransactionType, int>
    {
        #region Constructors

        public TransactionType() { }

        public TransactionType(int transactionTypeId)
        {
            this.transactionTypeId = transactionTypeId;
        }

        #endregion

        #region Private Properties

		private int transactionTypeId;

		private string transactionTypeName;

		private int createdBy;

		private DateTime? createdDate;

		private int updatedBy;

		private DateTime? updatedDate;

		private bool? flagDeleted;

		private bool? isSystem;



        //private bool? flagDeleted;

        //private bool? isSystem;

        //private int createdBy;

        //private int updatedBy;

        //private DateTime? createdDate;

        //private DateTime? updatedDate;
        #endregion

        #region Public Properties

        [PrimaryKey("@transaction_type_id", SqlDbType.Int)]
[Property("transaction_type_id", SqlDbType.Int, AddParameter = false)]
		public int TransactionTypeId
		{
			get
			{
				return transactionTypeId;
			}
			set
			{
				transactionTypeId = value;
			}
		}

[Property("transaction_type_name", SqlDbType.NVarChar)]
[PropertyConstraint(false, "TransactionTypeName", MaxLength = 100, MinLength = 3)]
		public string TransactionTypeName
		{
			get
			{
				return transactionTypeName;
			}
			set
			{
				transactionTypeName = value;
			}
		}



        [Property("flag_deleted", SqlDbType.Bit, AddParameter = false, UpdateParameter = false,FindParameter=false)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [Property("is_system", SqlDbType.Bit, AddParameter = false, UpdateParameter = false,FindParameter=false)]
        public bool? IsSystem
        {
            get
            {
                return isSystem;
            }
            set
            {
                isSystem = value;
            }
        }

		[Property("created_date",SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
		public DateTime? CreatedDate
		{
			get
			{
				return createdDate;
			}
			set
			{
				createdDate = value;
			}
		}

        [Property("updated_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
		public DateTime? UpdatedDate
		{
			get
			{
				return updatedDate;
			}
			set
			{
				updatedDate = value;
			}
		}

		[Property("created_by",SqlDbType.Int, AddParameter = true, UpdateParameter = false)]
		public int CreatedBy
		{
			get
			{
				return createdBy;
			}
			set
			{
				createdBy = value;
			}
		}

		[Property("updated_by",SqlDbType.Int, AddParameter = false, UpdateParameter = true)]
		public int UpdatedBy
		{
			get
			{
				return updatedBy;
			}
			set
			{
				updatedBy = value;
			}
		}
        #endregion

             #region Composite Objects
              

                 #endregion
   

        public override int GetIdentity()
        {
            return transactionTypeId;
        }
        public override void SetIdentity(int value)
        {
            transactionTypeId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            			throw new NotImplementedException();
        }
    }
}