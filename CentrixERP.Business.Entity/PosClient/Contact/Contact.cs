using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;


namespace SagePOS.Server.Business.Entity
{
    [EntityIManager("SagePOS.Server.Business.IManager.IContactManager,SagePOS.Server.Business.IManager", isLogicalDelete = true)]
    public class Contact : EntityBase<Contact, int>
    {
        #region Constructors

        public Contact() { }

        public Contact(int contactId)
        {
            this.contactId = contactId;
        }

        #endregion

        #region Private Properties


        private int contactId;

        private string firstName;

        private string lastName;

        private string email;

        private int gender;

        private string phoneNumber;

        private string companyName;

        private int contactType;

        private int id;

        private string contactname;
        private bool? flagDeleted;

        private DateTime? createdDate;

        private DateTime? updatedDate;

        private bool? isSystem;

        private int createdBy;

        private int updatedBy;


        #endregion

        #region Public Properties




        [PrimaryKey("@contact_id", SqlDbType.Int)]
        [Property("contact_id", SqlDbType.Int, AddParameter = false)]
        public int ContactId
        {
            get
            {
                return contactId;
            }
            set
            {
                contactId = value;
            }
        }


        [Property("first_name", SqlDbType.NVarChar)]

        public string FirstName
        {
            get
            {
                return firstName;
            }
            set
            {
                firstName = value;
            }
        }

        [Property("last_name", SqlDbType.NVarChar)]
        [PropertyConstraint(true, "LastName", MaxLength = 50, MinLength = 3)]
        public string LastName
        {
            get
            {
                return lastName;
            }
            set
            {
                lastName = value;
            }
        }
                [Property("contact_name", SqlDbType.NVarChar)]

        public string ContactName
        {
            get
            {
                return contactname;
            }
            set
            {
                contactname = value;
            }
        }


        
        [Property("email", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "Email", MaxLength = 30, MinLength = 3)]
        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                email = value;
            }
        }

        [Property("gender", SqlDbType.Int)]
        [PropertyConstraint(false, "Gender")]
        public int Gender
        {
            get
            {
                return gender;
            }
            set
            {
                gender = value;
            }
        }

        [Property("phone_number", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "PhoneNumber", MaxLength = 30, MinLength = 1)]
        public string PhoneNumber
        {
            get
            {
                return phoneNumber;
            }
            set
            {
                phoneNumber = value;
            }
        }

        [Property("company_name", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "CompanyName", MaxLength = 50, MinLength = 3)]
        public string CompanyName
        {
            get
            {
                return companyName;
            }
            set
            {
                companyName = value;
            }
        }

        [Property("contact_type", SqlDbType.Int)]
        [PropertyConstraint(false, "ContactType")]
        public int ContactType
        {
            get
            {
                return contactType;
            }
            set
            {
                contactType = value;
            }
        }



       



        [Property("flag_deleted", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [Property("is_system", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool? IsSystem
        {
            get
            {
                return isSystem;
            }
            set
            {
                isSystem = value;
            }
        }

        [Property("created_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? CreatedDate
        {
            get
            {
                return createdDate;
            }
            set
            {
                createdDate = value;
            }
        }

        [Property("updated_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? UpdatedDate
        {
            get
            {
                return updatedDate;
            }
            set
            {
                updatedDate = value;
            }
        }

        [Property("created_by", SqlDbType.Int, AddParameter = true, UpdateParameter = false)]
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }
        public int Id
        {
            get
            {
                return contactId;
            }
            set
            {
                id = contactId;
            }
        }

        [Property("updated_by", SqlDbType.Int, AddParameter = false, UpdateParameter = true)]
        public int UpdatedBy
        {
            get
            {
                return updatedBy;
            }
            set
            {
                updatedBy = value;
            }
        }
        #endregion

        #region Composite Objects


        #endregion


        public override int GetIdentity()
        {
            return contactId;
        }
        public override void SetIdentity(int value)
        {
            contactId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            throw new NotImplementedException();
        }
    }
}