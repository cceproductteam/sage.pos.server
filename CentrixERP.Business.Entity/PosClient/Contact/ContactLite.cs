using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace SagePOS.Server.Business.Entity
{
     public class ContactLite : EntityBaseLite
    {      
        
        #region Public Properties


         [LiteProperty("contact_id", SqlDbType.Int)]
         public int ContactId
         {
             get;
             set;
         }

         		[LiteProperty("first_name",SqlDbType.NVarChar)]
		public string FirstName
		{
			get;
			set;
		}

		[LiteProperty("last_name",SqlDbType.NVarChar)]
		public string LastName
		{
			get;
			set;
		}

		[LiteProperty("email",SqlDbType.NVarChar)]
		public string Email
		{
			get;
			set;
		}

        [LiteProperty("gender", SqlDbType.Int)]
		public int GenderId
		{
			get;
			set;
		}

		[LiteProperty("phone_number",SqlDbType.NVarChar)]
		public string PhoneNumber
		{
			get;
			set;
		}

		[LiteProperty("company_name",SqlDbType.NVarChar)]
		public string CompanyName
		{
			get;
			set;
		}

		[LiteProperty("contact_type",SqlDbType.Int)]
		public int ContactTypeId
		{
			get;
			set;
		}



        public string ContactType
        {
            get {
                if (Configuration.Configuration.Lang.ToString() == "en")
                    return ContactType_EN;
                else
                    return ContactType_AR;
            }

            set{ }

        }

        public string Gender
        {

            get
            {
                if (Configuration.Configuration.Lang.ToString() == "en")
                    return GenderEN;
                else
                    return GenderAR;
            }
            set { } 
        }

        [LiteProperty("gender_EN", SqlDbType.NVarChar)]
        public string GenderEN
        {
            get;
            set;
        }
        [LiteProperty("gender_AR", SqlDbType.NVarChar)]
        public string GenderAR
        {
            get;
            set;
        }


        [LiteProperty("contacttypeen_EN", SqlDbType.NVarChar)]
        public string ContactType_EN
        {
            get;
            set;
        }
        [LiteProperty("contacttypeen_AR", SqlDbType.NVarChar)]
        public string ContactType_AR
        {
            get;
            set;
        }

        
   
         [LiteProperty("flag_deleted", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? FlagDeleted
        {
            get;
            set;
        }

        [LiteProperty("is_system",SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
		public bool? IsSystem
		{
			get;
			set;
		}

		[LiteProperty("created_date",SqlDbType.DateTime)]
		public DateTime? CreatedDate
		{
			get;
			set;
		}

		[LiteProperty("updated_date",SqlDbType.DateTime)]
		public DateTime? UpdatedDate
		{
			get;
			set;
		}

		[LiteProperty("created_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int CreatedBy
		{
			get;
			set;
		}

		[LiteProperty("updated_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int UpdatedBy
		{
			get;
			set;
		}

         [LiteProperty("created_by_name",SqlDbType.NVarChar, FindAllLiteParameter = false)]
         public string CreatedByName{get;set;}

         [LiteProperty("contact_name", SqlDbType.NVarChar, FindAllLiteParameter = false)]
         public string ContactName { get; set; }

         [LiteProperty("updated_by_name",SqlDbType.NVarChar,FindAllLiteParameter = false)]
         public string UpdatedByName{get;set;}

     [LiteProperty("contact_id", SqlDbType.Int)]
          public int Id
        {
            get{return this.ContactId; }
            set { this.ContactId = value;  }
        }

         [LiteProperty("last_rows",SqlDbType.Bit)]
         public bool LastRows { get; set; }

         [LiteProperty("total_count",SqlDbType.Int)]
         public int TotalRecords { get; set; }
        #endregion
       
          
     }
}