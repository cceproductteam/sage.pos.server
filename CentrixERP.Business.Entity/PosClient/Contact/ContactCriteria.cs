using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

using SagePOS.Server.Business.Factory ;

namespace SagePOS.Server.Business.Entity
{
    public class ContactCriteria : CriteriaBase, IAdvancedSearchCriteria
    {
        [CrtiteriaField("@page_number", SqlDbType.Int)]
        public int? pageNumber { get; set; }

        [CrtiteriaField("@result_count", SqlDbType.Int)]
        public int? resultCount { get; set; }

        [CrtiteriaField("@keyword", SqlDbType.NVarChar)]
        public string Keyword { get; set; }

        [CrtiteriaField("@search_criteria", SqlDbType.NVarChar)]
        public string SearchCriteria { get; set; }

        [CrtiteriaField("@sort_type", SqlDbType.Bit)]
        public bool? SortType { get; set; }

        [CrtiteriaField("@sort_fields", SqlDbType.NVarChar)]
        public string SortFields { get; set; }

        [CrtiteriaField("@contact_id", SqlDbType.Int, IsReferance = true)]
        public int? ContactId { get; set; }

        [CrtiteriaField("@contact_id", SqlDbType.Int, IsReferance = true)]
        public int? EntityId { get; set; }


        [CrtiteriaField("@email", SqlDbType.NVarChar)]
        public string Email { get; set; }
        [CrtiteriaField("@company_name", SqlDbType.NVarChar)]
        public string CompanyName { get; set; }

        [CrtiteriaField("@first_name", SqlDbType.NVarChar)]
        public string FirstName { get; set; }
        [CrtiteriaField("@last_name", SqlDbType.NVarChar)]
        public string LastName { get; set; }
        [CrtiteriaField("@phone_number", SqlDbType.NVarChar)]
        public string PhoneNumber { get; set; }

        [CrtiteriaField("@person_full_name", SqlDbType.NVarChar)]
        public string Name { get; set; }

        [CrtiteriaField("@contact_type", SqlDbType.Int)]
        public int ContactType { get; set; }

        int IAdvancedSearchCriteria.EntityId
        {
            get;
            set;
        }

        bool IAdvancedSearchCriteria.SortType
        {
            get;
            set;
        }

        int IAdvancedSearchCriteria.pageNumber
        {
            get;
            set;
        }

        int IAdvancedSearchCriteria.resultCount
        {
            get;
            set;
        }
    }
}
