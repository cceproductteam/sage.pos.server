using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
//using SagePOS.Server.Business.Entity;

namespace SagePOS.Server.Business.Entity
{
    [EntityIManager("SagePOS.Server.Business.IManager.ILocalConfigurationManager,SagePOS.Server.Business.IManager", isLogicalDelete = true)]
    public class LocalConfiguration : EntityBase<LocalConfiguration, int>
    {
        #region Constructors

        public LocalConfiguration() { }

        public LocalConfiguration(int configId)
        {
            this.configId = configId;
        }

        #endregion

        #region Private Properties

		private int configId;

		private int registerId;

		private string registerName;

		private int storeId;

		private string storeName;

		private int createdBy;

		private DateTime? createdDate;

		private int updatedBy;

		private DateTime? updatedDate;

		private bool? flagDeleted;

		private bool? isSystem;

		private string ipAddress;

		private string macAddress;

		private int syncMode;

		private string posManagerIpAddress;



        //private bool? flagDeleted;

        //private bool? isSystem;

        //private int createdBy;

        //private int updatedBy;

        //private DateTime? createdDate;

        //private DateTime? updatedDate;
        #endregion

        #region Public Properties

        [PrimaryKey("@config_id", SqlDbType.Int)]
[Property("config_id", SqlDbType.Int, AddParameter = false)]
		public int ConfigId
		{
			get
			{
				return configId;
			}
			set
			{
				configId = value;
			}
		}

[Property("register_id", SqlDbType.Int)]
[PropertyConstraint(false, "Register")]
		public int RegisterId
		{
			get
			{
				return registerId;
			}
			set
			{
				registerId = value;
			}
		}

[Property("register_name", SqlDbType.NVarChar)]
[PropertyConstraint(false, "RegisterName", MaxLength = 100, MinLength = 3)]
		public string RegisterName
		{
			get
			{
				return registerName;
			}
			set
			{
				registerName = value;
			}
		}

[Property("store_id", SqlDbType.Int)]
[PropertyConstraint(false, "Store")]
		public int StoreId
		{
			get
			{
				return storeId;
			}
			set
			{
				storeId = value;
			}
		}

[Property("store_name", SqlDbType.NVarChar)]
[PropertyConstraint(false, "StoreName", MaxLength = 100, MinLength = 3)]
		public string StoreName
		{
			get
			{
				return storeName;
			}
			set
			{
				storeName = value;
			}
		}

[Property("ip_address", SqlDbType.NVarChar)]
[PropertyConstraint(false, "IpAddress", MaxLength = 4000, MinLength = 3)]
		public string IpAddress
		{
			get
			{
				return ipAddress;
			}
			set
			{
				ipAddress = value;
			}
		}

[Property("mac_address", SqlDbType.NVarChar)]
[PropertyConstraint(false, "MacAddress", MaxLength = 4000, MinLength = 3)]
		public string MacAddress
		{
			get
			{
				return macAddress;
			}
			set
			{
				macAddress = value;
			}
		}

[Property("sync_mode", SqlDbType.Int)]
[PropertyConstraint(false, "SyncMode")]
		public int SyncMode
		{
			get
			{
				return syncMode;
			}
			set
			{
				syncMode = value;
			}
		}

[Property("pos_manager_ip_address", SqlDbType.NVarChar)]
[PropertyConstraint(false, "PosManagerIpAddress", MaxLength = 100, MinLength = 3)]
		public string PosManagerIpAddress
		{
			get
			{
				return posManagerIpAddress;
			}
			set
			{
				posManagerIpAddress = value;
			}
		}



        [Property("flag_deleted", SqlDbType.Bit, AddParameter = false, UpdateParameter = false,FindParameter=false)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [Property("is_system", SqlDbType.Bit, AddParameter = false, UpdateParameter = false,FindParameter=false)]
        public bool? IsSystem
        {
            get
            {
                return isSystem;
            }
            set
            {
                isSystem = value;
            }
        }

		[Property("created_date",SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
		public DateTime? CreatedDate
		{
			get
			{
				return createdDate;
			}
			set
			{
				createdDate = value;
			}
		}

        [Property("updated_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
		public DateTime? UpdatedDate
		{
			get
			{
				return updatedDate;
			}
			set
			{
				updatedDate = value;
			}
		}

		[Property("created_by",SqlDbType.Int, AddParameter = true, UpdateParameter = false)]
		public int CreatedBy
		{
			get
			{
				return createdBy;
			}
			set
			{
				createdBy = value;
			}
		}

		[Property("updated_by",SqlDbType.Int, AddParameter = false, UpdateParameter = true)]
		public int UpdatedBy
		{
			get
			{
				return updatedBy;
			}
			set
			{
				updatedBy = value;
			}
		}
        #endregion

             #region Composite Objects
              

                 #endregion
   

        public override int GetIdentity()
        {
            return configId;
        }
        public override void SetIdentity(int value)
        {
            configId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            			throw new NotImplementedException();
        }
    }
}