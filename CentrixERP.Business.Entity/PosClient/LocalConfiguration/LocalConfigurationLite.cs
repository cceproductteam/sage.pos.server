using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace SagePOS.Server.Business.Entity
{
     public class LocalConfigurationLite : EntityBaseLite
    {      
        
        #region Public Properties

         		[LiteProperty("config_id",SqlDbType.Int)]
		public int ConfigId
		{
			get;
			set;
		}

		[LiteProperty("register_id",SqlDbType.Int)]
		public int RegisterId
		{
			get;
			set;
		}

		[LiteProperty("register_name",SqlDbType.NVarChar)]
		public string RegisterName
		{
			get;
			set;
		}

		[LiteProperty("store_id",SqlDbType.Int)]
		public int StoreId
		{
			get;
			set;
		}

		[LiteProperty("store_name",SqlDbType.NVarChar)]
		public string StoreName
		{
			get;
			set;
		}

		[LiteProperty("ip_address",SqlDbType.NVarChar)]
		public string IpAddress
		{
			get;
			set;
		}

		[LiteProperty("mac_address",SqlDbType.NVarChar)]
		public string MacAddress
		{
			get;
			set;
		}

		[LiteProperty("sync_mode",SqlDbType.Int)]
		public int SyncMode
		{
			get;
			set;
		}

		[LiteProperty("pos_manager_ip_address",SqlDbType.NVarChar)]
		public string PosManagerIpAddress
		{
			get;
			set;
		}



        [LiteProperty("config_id", SqlDbType.Int)]
        public int value
        {
            get;
            set;
        }

          [LiteProperty("config_id", SqlDbType.Int)]
        public int label
        {
            get;
            set;
        }

         [LiteProperty("flag_deleted", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? FlagDeleted
        {
            get;
            set;
        }

        [LiteProperty("is_system",SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
		public bool? IsSystem
		{
			get;
			set;
		}

		[LiteProperty("created_date",SqlDbType.DateTime)]
		public DateTime? CreatedDate
		{
			get;
			set;
		}

		[LiteProperty("updated_date",SqlDbType.DateTime)]
		public DateTime? UpdatedDate
		{
			get;
			set;
		}

		[LiteProperty("created_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int CreatedBy
		{
			get;
			set;
		}

		[LiteProperty("updated_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int UpdatedBy
		{
			get;
			set;
		}

         [LiteProperty("created_by_name",SqlDbType.NVarChar, FindAllLiteParameter = false)]
         public string CreatedByName{get;set;}

         [LiteProperty("updated_by_name",SqlDbType.NVarChar,FindAllLiteParameter = false)]
         public string UpdatedByName{get;set;}

         [LiteProperty("config_id",SqlDbType.NVarChar)]
          public int Id
        {
            get{return this.ConfigId; }
            set{this.ConfigId=value;}
        }

         [LiteProperty("last_rows",SqlDbType.NVarChar)]
         public bool LastRows { get; set; }

         [LiteProperty("total_count",SqlDbType.NVarChar)]
         public int TotalRecords { get; set; }
        #endregion
       
          
     }
}