using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace SagePOS.Server.Business.Entity
{
    public class AccpacSyncLogLite : EntityBaseLite
    {

        #region Public Properties

        [LiteProperty("id", SqlDbType.Int)]
        public int SyncId
        {
            get;
            set;
        }

        [LiteProperty("sync_data", SqlDbType.NVarChar)]
        public string SyncData
        {
            get;
            set;
        }

        [LiteProperty("action", SqlDbType.NVarChar)]
        public string Action
        {
            get;
            set;
        }

        [LiteProperty("sync_data_id", SqlDbType.NVarChar)]
        public string SyncDataId
        {
            get;
            set;
        }

        [LiteProperty("sync_status", SqlDbType.Bit)]
        public bool? SyncStatus
        {
            get;
            set;
        }



        [LiteProperty("created_date", SqlDbType.NVarChar)]
        public string CreatedDate
        {
            get;
            set;
        }

        [LiteProperty("accpac_sync_unique_number", SqlDbType.NVarChar)]
        public string AccpacSyncUniqueNumber
        {
            get;
            set;
        }


        [LiteProperty("flag_deleted", SqlDbType.Bit)]
        public bool FlagDeleted
        {
            get;
            set;
        }

        [LiteProperty("is_system", SqlDbType.Bit)]
        public bool IsSystem
        {
            get;
            set;
        }


        public int value
        {
            get { return Id; }
        }


        public string label
        {
            get { return Id.ToString(); }
        }




        public int Id
        {
            get { return this.SyncId; }
        }

        [LiteProperty("last_rows", SqlDbType.Bit)]
        public bool LastRows { get; set; }

        [LiteProperty("total_count", SqlDbType.Int)]
        public int TotalRecords { get; set; }
        #endregion


    }
}