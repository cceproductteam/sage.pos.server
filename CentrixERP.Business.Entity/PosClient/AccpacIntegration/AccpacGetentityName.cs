﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
using SagePOS.Manger.Common.Business.Entity;
namespace SagePOS.Server.Business.Entity
{

    [EntityIManager("SagePOS.Server.Business.IManager.AccpacGetentityName,SagePOS.Server.Business.IManager", isLogicalDelete = true)]
    public class AccpacGetentityName : EntityBase<AccpacGetentityName, int>
    {
        #region Constructors

        public AccpacGetentityName() { }



        public bool GetTransactions
        {
            get;
            set;
        }

        public bool GetCloseRegister
        {
            get;
            set;
        }

        public bool GetCustomers
        {
            get;
            set;
        }

        public bool GetUserData
        {
            get;
            set;
        }

        public bool GetRoles
        {
            get;
            set;
        }


        public bool GetTeams
        {
            get;
            set;
        }

        public bool GetPermissions
        {
            get;
            set;
        }

        public bool GetShifts
        {
            get;
            set;
        }

        public bool GetUserShifts
        {
            get;
            set;
        }

        public bool GetPersonData
        {
            get;
            set;
        }

        public bool GetCompanyData
        {
            get;
            set;
        }

        public bool GetStores
        {
            get;
            set;
        }



        public bool GetRegisters
        {
            get;
            set;
        }

        public bool GetQuickKeys
        {
            get;
            set;
        }

        public bool GetPaymentData
        {
            get;
            set;
        }

        public bool GetItems
        {
            get;
            set;
        }

        public bool GetPriceList
        {
            get;
            set;
        }

        public bool GetItemPriceList
        {
            get;
            set;
        }

        public bool GetItemTax
        {
            get;
            set;
        }

        public bool GetCurrencyData
        {
            get;
            set;
        }

        public bool GetCurrencyRates
        {
            get;
            set;
        }

        public bool GetTaxMatrix
        {
            get;
            set;
        }


        public bool GetTaxAuth
        {
            get;
            set;
        }


        public bool GetAccpacCustomerData
        {
            get;
            set;
        }


        public bool GetConfigData
        {
            get;
            set;
        }


        public bool GetLovs
        {
            get;
            set;
        }


        public bool GetVouchers
        {
            get;
            set;
        }

        public bool GetStockData
        {
            get;
            set;
        }

        public bool GetLocationData
        {
            get;
            set;
        }

        public string DataBaseUserName
        {
            get;
            set;
        }
        public string DataBasePassword
        {
            get;
            set;
        }
        public string StoreName
        {
            get;
            set;
        }
        public string DataBaseInstanceName
        {
            get;
            set;
        }
        public string DataBaseName
        {
            get;
            set;
        }
        public string IpAddress
        {
            get;
            set;
        }
        public int SyncMode
        {
            get;
            set;
        }

        public bool GetItemSerial
        {
            get;
            set;
        }


        public bool BankAccount
        {
            get;
            set;
        }

        public bool Category
        {

            get;
            set;
        }
        public bool Currency
        {

            get;
            set;
        }
        public bool CurrencyRates
        {

            get;
            set;

        }

        public bool Customer
        {
            get;
            set;
        }

        public bool GLAccounts
        {
            get;
            set;
        }
        public bool Location
        {
            get;
            set;

        }


        public bool PriceList
        {
            get;
            set;
        }

        public bool TaxAuthorities
        {

            get;
            set;
 
        }
        public bool taxMatrix
        {
            get;
            set;
        }
        public bool ItemCard
        {

            get;
            set;

        }

        public bool ItemStock
        {


            get;
            set;
        }

        public bool ItemPriceList
        {
            get;
            set;
        }
        public bool ItemTax
        {
            get;
            set;
        }
        public bool TransferData
        {
            get;
            set;
        }

        public override int GetIdentity()
        {
            return -1;
        }
        public override void SetIdentity(int value)
        {
        }
        public override void SetParentId(object value, Type ParentType)
        {

        }

        #endregion
    }
}
