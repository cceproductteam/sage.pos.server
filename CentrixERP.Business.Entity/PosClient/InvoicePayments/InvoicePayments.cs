using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
using SagePOS.Manger.Common.Business.Entity;

namespace SagePOS.Server.Business.Entity
{
    [EntityIManager("SagePOS.Server.Business.IManager.IInvoicePaymentsManager,SagePOS.Server.Business.IManager", isLogicalDelete = true)]
    public class InvoicePayments : EntityBase<InvoicePayments, int>
    {
        #region Constructors

        public InvoicePayments() { }

        public InvoicePayments(int invoicePaymentsId)
        {
            this.invoicePaymentsId = invoicePaymentsId;
        }

        #endregion

        #region Private Properties

        private int invoicePaymentsId;

        private int invoiceId;

        private int paymentType;

        private int createdBy;

        private int updatedBy;

        private DateTime? createdDate;

        private DateTime? updatedDate;

        private bool? flagDeleted;

        private bool? isSystem;

        private Double amount;

        private string paymenttypeName;

        private string currency;

        private Double amountDefaultCurrency;

    

        private int registerId;

        private string creditCardNumber;

        private string creditCardType;

        private string creditCardExpirationdate;

        private string chequeNumber;

        private string bankName;

        private string chequeDate;

        private string pettyCashReason;

        private bool? isSynced;

        private string invoiceUniqueNumber;

        private string chequNote;
        private double availableAmount;
        private double currencyRate;


        //private bool? flagDeleted;

        //private bool? isSystem;

        //private int createdBy;

        //private int updatedBy;

        //private DateTime? createdDate;

        //private DateTime? updatedDate;
        #endregion

        #region Public Properties

        [PrimaryKey("@invoice_payments_id", SqlDbType.Int)]
        [Property("invoice_payments_id", SqlDbType.Int, AddParameter = false)]
        public int InvoicePaymentsId
        {
            get
            {
                return invoicePaymentsId;
            }
            set
            {
                invoicePaymentsId = value;
            }
        }

        [Property("invoice_id", SqlDbType.Int)]
        [PropertyConstraint(false, "Invoice")]
        public int InvoiceId
        {
            get
            {
                return invoiceId;
            }
            set
            {
                invoiceId = value;
            }
        }

        [Property("payment_type", SqlDbType.Int)]
        [PropertyConstraint(false, "PaymentType")]
        public int PaymentType
        {
            get
            {
                return paymentType;
            }
            set
            {
                paymentType = value;
            }
        }

        [Property("amount", SqlDbType.Money)]
        [PropertyConstraint(false, "Amount")]
        public Double Amount
        {
            get
            {
                return amount;
            }
            set
            {
                amount = value;
            }
        }

        [Property("paymentType_name", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "paymentType_name", MaxLength = 100, MinLength = 3)]
        public string PaymentTypeName
        {
            get
            {
                return paymenttypeName;
            }
            set
            {
                paymenttypeName = value;
            }
        }

        [Property("currency", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "Currency", MaxLength = 100, MinLength = 3)]
        public string Currency
        {
            get
            {
                return currency;
            }
            set
            {
                currency = value;
            }
        }

        [Property("amount_default_currency", SqlDbType.Money)]
        [PropertyConstraint(false, "AmountDefaultCurrency")]
        public Double AmountDefaultCurrency
        {
            get
            {
                return amountDefaultCurrency;
            }
            set
            {
                amountDefaultCurrency = value;
            }
        }



        [Property("available_amount", SqlDbType.Money)]
        [PropertyConstraint(false, "AvailableAmount")]
        public double AvailableAmount
        {
            get
            {
                return availableAmount;
            }
            set
            {
                availableAmount = value;
            }
        }

        [Property("currency_rate", SqlDbType.Money)]
        [PropertyConstraint(false, "CurrencyRate")]
        public double CurrencyRate
        {
            get
            {
                return currencyRate ;
            }
            set
            {
                currencyRate = value;
            }
        }


       

        [Property("register_id", SqlDbType.Int)]
        [PropertyConstraint(false, "Register")]
        public int RegisterId
        {
            get
            {
                return registerId;
            }
            set
            {
                registerId = value;
            }
        }

        [Property("credit_card_number", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "CreditCardNumber", MaxLength = 100, MinLength = 3)]
        public string CreditCardNumber
        {
            get
            {
                return creditCardNumber;
            }
            set
            {
                creditCardNumber = value;
            }
        }

        [Property("credit_card_type", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "CreditCardType", MaxLength = 100, MinLength = 3)]
        public string CreditCardType
        {
            get
            {
                return creditCardType;
            }
            set
            {
                creditCardType = value;
            }
        }

        [Property("credit_card_expirationDate", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "CreditCardExpirationDate", MaxLength = 50, MinLength = 3)]
        public string CreditCardExpirationDate
        {
            get
            {
                return creditCardExpirationdate;
            }
            set
            {
                creditCardExpirationdate = value;
            }
        }

        [Property("cheque_number", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "ChequeNumber", MaxLength = 100, MinLength = 3)]
        public string ChequeNumber
        {
            get
            {
                return chequeNumber;
            }
            set
            {
                chequeNumber = value;
            }
        }

        [Property("bank_name", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "BankName", MaxLength = 100, MinLength = 3)]
        public string BankName
        {
            get
            {
                return bankName;
            }
            set
            {
                bankName = value;
            }
        }

        [Property("cheque_date", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "ChequeDate", MaxLength = 50, MinLength = 3)]
        public string ChequeDate
        {
            get
            {
                return chequeDate;
            }
            set
            {
                chequeDate = value;
            }
        }

        [Property("petty_cash_reason", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "PettyCashReason", MaxLength = 500, MinLength = 3)]
        public string PettyCashReason
        {
            get
            {
                return pettyCashReason;
            }
            set
            {
                pettyCashReason = value;
            }
        }

        [Property("is_synced", SqlDbType.Bit,AddParameter=false,UpdateParameter=false)]
        [PropertyConstraint(false, "IsSynced")]
        public bool? IsSynced
        {
            get
            {
                return isSynced;
            }
            set
            {
                isSynced = value;
            }
        }

        [Property("invoice_unique_number", SqlDbType.NVarChar,AddParameter=false,UpdateParameter=false)]
        [PropertyConstraint(false, "InvoiceUniqueNumber", MaxLength = 100, MinLength = 3)]
        public string InvoiceUniqueNumber
        {
            get
            {
                return invoiceUniqueNumber;
            }
            set
            {
                invoiceUniqueNumber = value;
            }
        }



        [Property("flag_deleted", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [Property("is_system", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool? IsSystem
        {
            get
            {
                return isSystem;
            }
            set
            {
                isSystem = value;
            }
        }

        [Property("created_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? CreatedDate
        {
            get
            {
                return createdDate;
            }
            set
            {
                createdDate = value;
            }
        }

        [Property("updated_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? UpdatedDate
        {
            get
            {
                return updatedDate;
            }
            set
            {
                updatedDate = value;
            }
        }

        [Property("created_by", SqlDbType.Int, AddParameter = true, UpdateParameter = false)]
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        [Property("updated_by", SqlDbType.Int, AddParameter = false, UpdateParameter = true)]
        public int UpdatedBy
        {
            get
            {
                return updatedBy;
            }
            set
            {
                updatedBy = value;
            }
        }
        #endregion

        #region Composite Objects


        #endregion


        public override int GetIdentity()
        {
            return invoicePaymentsId;
        }
        public override void SetIdentity(int value)
        {
            invoicePaymentsId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            if (ParentType == typeof(Invoice))
                this.InvoiceId = (int)value;
        }
    }
}