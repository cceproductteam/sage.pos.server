using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace SagePOS.Server.Business.Entity
{
     public class InvoicePaymentsLite : EntityBaseLite
    {      
        
        #region Public Properties

         		[LiteProperty("invoice_payments_id",SqlDbType.Int)]
		public int InvoicePaymentsId
		{
			get;
			set;
		}

		[LiteProperty("invoice_id",SqlDbType.Int)]
		public int InvoiceId
		{
			get;
			set;
		}

		[LiteProperty("payment_type",SqlDbType.Int)]
		public int PaymentType
		{
			get;
			set;
		}

		[LiteProperty("amount",SqlDbType.Float)]
		public Double Amount
		{
			get;
			set;
		}

		[LiteProperty("paymentType_name",SqlDbType.NVarChar)]
		public string PaymenttypeName
		{
			get;
			set;
		}

		[LiteProperty("currency",SqlDbType.NVarChar)]
		public string Currency
		{
			get;
			set;
		}

		[LiteProperty("amount_default_currency",SqlDbType.Float)]
		public Double AmountDefaultCurrency
		{
			get;
			set;
		}

		[LiteProperty("gift_number",SqlDbType.Float)]
		public Double GiftNumber
		{
			get;
			set;
		}

		[LiteProperty("register_id",SqlDbType.Int)]
		public int RegisterId
		{
			get;
			set;
		}

		[LiteProperty("credit_card_number",SqlDbType.NVarChar)]
		public string CreditCardNumber
		{
			get;
			set;
		}

        [LiteProperty("available_amount", SqlDbType.Money)]
        public double AvailableAmount
        {
            get;
            set;
        }
        [LiteProperty("currency_rate", SqlDbType.Money)]
        public double CurrencyRate
        {
            get;
            set;
        }

        [LiteProperty("cheque_note", SqlDbType.NVarChar)]
        public string ChequeNote
        {
            get;
            set;
        }

		[LiteProperty("credit_card_type",SqlDbType.NVarChar)]
		public string CreditCardType
		{
			get;
			set;
		}

		[LiteProperty("credit_card_expirationDate",SqlDbType.NVarChar)]
		public string CreditCardExpirationdate
		{
			get;
			set;
		}

		[LiteProperty("cheque_number",SqlDbType.NVarChar)]
		public string ChequeNumber
		{
			get;
			set;
		}

		[LiteProperty("bank_name",SqlDbType.NVarChar)]
		public string BankName
		{
			get;
			set;
		}

		[LiteProperty("cheque_date",SqlDbType.NVarChar)]
		public string ChequeDate
		{
			get;
			set;
		}

		[LiteProperty("petty_cash_reason",SqlDbType.NVarChar)]
		public string PettyCashReason
		{
			get;
			set;
		}

		[LiteProperty("is_synced",SqlDbType.Bit)]
		public bool? IsSynced
		{
			get;
			set;
		}

		[LiteProperty("invoice_unique_number",SqlDbType.NVarChar)]
		public string InvoiceUniqueNumber
		{
			get;
			set;
		}



        [LiteProperty("invoice_payments_id", SqlDbType.Int)]
        public int value
        {
            get;
            set;
        }

          [LiteProperty("invoice_payments_id", SqlDbType.Int)]
        public int label
        {
            get;
            set;
        }

         [LiteProperty("flag_deleted", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? FlagDeleted
        {
            get;
            set;
        }

        [LiteProperty("is_system",SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
		public bool? IsSystem
		{
			get;
			set;
		}

		[LiteProperty("created_date",SqlDbType.DateTime)]
		public DateTime? CreatedDate
		{
			get;
			set;
		}

		[LiteProperty("updated_date",SqlDbType.DateTime)]
		public DateTime? UpdatedDate
		{
			get;
			set;
		}

		[LiteProperty("created_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int CreatedBy
		{
			get;
			set;
		}

		[LiteProperty("updated_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int UpdatedBy
		{
			get;
			set;
		}

         [LiteProperty("created_by_name",SqlDbType.NVarChar, FindAllLiteParameter = false)]
         public string CreatedByName{get;set;}

         [LiteProperty("updated_by_name",SqlDbType.NVarChar,FindAllLiteParameter = false)]
         public string UpdatedByName{get;set;}

         [LiteProperty("invoice_payments_id",SqlDbType.NVarChar)]
          public int Id
        {
            get{return this.InvoicePaymentsId; }
            set{this.InvoicePaymentsId=value;}
        }

         [LiteProperty("last_rows",SqlDbType.NVarChar)]
         public bool LastRows { get; set; }

         [LiteProperty("total_count",SqlDbType.NVarChar)]
         public int TotalRecords { get; set; }
        #endregion
       
          
     }

    public class InvoicePaymentIntegration
    {
        public string InvoiceUnqiueNumber { get; set; }
        public double AmountDefaultCurrency { get; set; }
        public double AvailableAmount { get; set; }
        public string Currency { get; set; }
        public double CurrencyRate { get; set; }
        public string CashBank { get; set; }
        public string VisaBank { get; set; }
        public string MasterBank { get; set; }
        public string AmexBank { get; set; }
        public string NationalBank { get; set; }
        public string ChequeBank { get; set; }
        public string CashCustomer { get; set; }
        public string AccpacOrderEntryNumber { get; set; }
        public string SyncNumber { get; set; }
        public DateTime? CreatedDate
        {
            get;
            set;
        }
        public int PaymentType { get; set; }
        public string CreditCardType { get; set; }

    }
}