using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
using SagePOS.Manger.Common.Business.Entity;

namespace SagePOS.Server.Business.Entity
{
    [EntityIManager("SagePOS.Server.Business.IManager.IInvoiceItemManager,SagePOS.Server.Business.IManager", isLogicalDelete = true)]
    public class InvoiceItem : EntityBase<InvoiceItem, int>
    {
        #region Constructors

        public InvoiceItem() { }

        public InvoiceItem(int invoiceItemId)
        {
            this.invoiceItemId = invoiceItemId;
        }

        #endregion

        #region Private Properties

        private int invoiceItemId;

        private int invoiceId;

        private string itemName;

        private Double orginalPrice;

        private Double unitPrice;

        private Double totalPrice;

        private Double percentageDiscount;

        private int createdBy;

        private int updatedBy;

        private DateTime? createdDate;

        private DateTime? updatedDate;

        private bool? flagDeleted;

        private bool? isSystem;

        private Double amountDiscount;

        private double quantity;

        private Double minPrice;

        private Double taxAmount;

        private string barCode;

        private Double taxpercentage;

        private bool isRefunded;

        private int parentItemId;

        private double refundedQuantity;

        private string unitId;

        private string itemNo;

        private string serialNumber;

        private bool isSerial;

        private bool isSynced;

        private string invoiceUniqueNumber;

        private double originalPricebeforChange;

        private double taxBeforChange;
        private double invoiceDiscountPerItem;
        private bool isExchanged;


        //private bool? flagDeleted;

        //private bool? isSystem;

        //private int createdBy;

        //private int updatedBy;

        //private DateTime? createdDate;

        //private DateTime? updatedDate;
        #endregion

        #region Public Properties

        [PrimaryKey("@invoice_item_id", SqlDbType.Int)]
        [Property("invoice_item_id", SqlDbType.Int, AddParameter = false)]
        public int InvoiceItemId
        {
            get
            {
                return invoiceItemId;
            }
            set
            {
                invoiceItemId = value;
            }
        }

        [Property("invoice_id", SqlDbType.Int)]
        [PropertyConstraint(false, "Invoice")]
        public int InvoiceId
        {
            get
            {
                return invoiceId;
            }
            set
            {
                invoiceId = value;
            }
        }

        [Property("item_name", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "ItemName", MaxLength = 100, MinLength = 3)]
        public string ItemName
        {
            get
            {
                return itemName;
            }
            set
            {
                itemName = value;
            }
        }

        [Property("orginal_price", SqlDbType.Money)]
        [PropertyConstraint(false, "OrginalPrice")]
        public Double OrginalPrice
        {
            get
            {
                return orginalPrice;
            }
            set
            {
                orginalPrice = value;
            }
        }

        [Property("unit_price", SqlDbType.Money)]
        [PropertyConstraint(false, "UnitPrice")]
        public Double UnitPrice
        {
            get
            {
                return unitPrice;
            }
            set
            {
                unitPrice = value;
            }
        }

        [Property("total_price", SqlDbType.Money)]
        [PropertyConstraint(false, "TotalPrice")]
        public Double TotalPrice
        {
            get
            {
                return totalPrice;
            }
            set
            {
                totalPrice = value;
            }
        }

        [Property("percentage_discount", SqlDbType.Money)]
        [PropertyConstraint(false, "PercentageDiscount")]
        public Double PercentageDiscount
        {
            get
            {
                return percentageDiscount;
            }
            set
            {
                percentageDiscount = value;
            }
        }

        [Property("amount_discount", SqlDbType.Money)]
        [PropertyConstraint(false, "AmountDiscount")]
        public Double AmountDiscount
        {
            get
            {
                return amountDiscount;
            }
            set
            {
                amountDiscount = value;
            }
        }

        [Property("quantity", SqlDbType.Real)]
        [PropertyConstraint(false, "Quantity")]
        public double Quantity
        {
            get
            {
                return quantity;
            }
            set
            {
                quantity = value;
            }
        }

      

        [Property("tax_amount", SqlDbType.Money)]
        [PropertyConstraint(false, "TaxAmount")]
        public Double TaxAmount
        {
            get
            {
                return taxAmount;
            }
            set
            {
                taxAmount = value;
            }
        }

        [Property("bar_code", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "BarCode", MaxLength = 100, MinLength = 3)]
        public string BarCode
        {
            get
            {
                return barCode;
            }
            set
            {
                barCode = value;
            }
        }

        [Property("tax_percentage", SqlDbType.Money)]
        [PropertyConstraint(false, "TaxPercentage")]
        public Double TaxPercentage
        {
            get
            {
                return taxpercentage;
            }
            set
            {
                taxpercentage = value;
            }
        }

        [Property("is_refunded", SqlDbType.Bit)]
        [PropertyConstraint(false, "IsRefunded")]
        public bool IsRefunded
        {
            get
            {
                return isRefunded;
            }
            set
            {
                isRefunded = value;
            }
        }

        [Property("parent_item_id", SqlDbType.Int)]
        [PropertyConstraint(false, "ParentItem")]
        public int ParentItemId
        {
            get
            {
                return parentItemId;
            }
            set
            {
                parentItemId = value;
            }
        }

        [Property("refunded_quantity", SqlDbType.Real)]
        [PropertyConstraint(false, "RefundedQuantity")]
        public double RefundedQuantity
        {
            get
            {
                return refundedQuantity;
            }
            set
            {
                refundedQuantity = value;
            }
        }

        [Property("unit_id", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "Unit", MaxLength = 100, MinLength = 3)]
        public string UnitId
        {
            get
            {
                return unitId;
            }
            set
            {
                unitId = value;
            }
        }

        [Property("item_no", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "ItemNo", MaxLength = 100, MinLength = 3)]
        public string ItemNumber
        {
            get
            {
                return itemNo;
            }
            set
            {
                itemNo = value;
            }
        }

        [Property("serial_number", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "SerialNumber", MaxLength = 4000, MinLength = 3)]
        public string SerialNumber
        {
            get
            {
                return serialNumber;
            }
            set
            {
                serialNumber = value;
            }
        }

        [Property("is_serial", SqlDbType.Bit)]
        [PropertyConstraint(false, "IsSerial")]
        public bool IsSerial
        {
            get
            {
                return isSerial;
            }
            set
            {
                isSerial = value;
            }
        }

        [Property("is_synced", SqlDbType.Bit,AddParameter=false,UpdateParameter=false)]
        [PropertyConstraint(false, "IsSynced")]
        public bool IsSynced
        {
            get
            {
                return isSynced;
            }
            set
            {
                isSynced = value;
            }
        }

        [Property("invoice_unique_number", SqlDbType.NVarChar, AddParameter = true, UpdateParameter = false)]
        [PropertyConstraint(false, "InvoiceUniqueNumber", MaxLength = 100, MinLength = 3)]
        public string InvoiceUniqueNumber
        {
            get
            {
                return invoiceUniqueNumber;
            }
            set
            {
                invoiceUniqueNumber = value;
            }
        }


        [Property("original_price_before_change", SqlDbType.Money)]
        [PropertyConstraint(false, "OriginalPriceBeforeChange")]
        public double OriginalPriceBeforeChange
        {
            get
            {
                return originalPricebeforChange;
            }
            set
            {
                originalPricebeforChange = value;
            }
        }

        [Property("tax_befor_change", SqlDbType.Money)]
        [PropertyConstraint(false, "TaxBeforeChange")]
        public double TaxBeforeChange
        {
            get
            {
                return taxBeforChange ;
            }
            set
            {
                taxBeforChange = value;
            }
        }

        [Property("invoice_discount_per_item", SqlDbType.Money)]
        [PropertyConstraint(false, "InvoiceDiscountPerItem")]
        public double InvoiceDiscountPerItem
        {
            get
            {
                return invoiceDiscountPerItem;
            }
            set
            {
                invoiceDiscountPerItem = value;
            }
        }


       


        [Property("flag_deleted", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [Property("is_system", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool? IsSystem
        {
            get
            {
                return isSystem;
            }
            set
            {
                isSystem = value;
            }
        }

        [Property("created_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? CreatedDate
        {
            get
            {
                return createdDate;
            }
            set
            {
                createdDate = value;
            }
        }

        [Property("updated_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? UpdatedDate
        {
            get
            {
                return updatedDate;
            }
            set
            {
                updatedDate = value;
            }
        }

        [Property("created_by", SqlDbType.Int, AddParameter = true, UpdateParameter = false)]
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        [Property("updated_by", SqlDbType.Int, AddParameter = false, UpdateParameter = true)]
        public int UpdatedBy
        {
            get
            {
                return updatedBy;
            }
            set
            {
                updatedBy = value;
            }
        }

        [Property("is_exchanged", SqlDbType.Bit)]
        public bool IsExchanged
        {
            get
            {
                return isExchanged;
            }
            set
            {
                isExchanged = value;
            }
        }


        public int InvoiceStatus
        {
            get;
            set;
        }

        public int StoreId
        {
            get;
            set;
        }
        #endregion

        #region Composite Objects


        #endregion


        public override int GetIdentity()
        {
            return invoiceItemId;
        }
        public override void SetIdentity(int value)
        {
            invoiceItemId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            if (ParentType == typeof(Invoice))
                this.InvoiceId = (int)value;
        }
    }
}