using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace SagePOS.Server.Business.Entity
{
    public class InvoiceItemLite : EntityBaseLite
    {

        #region Public Properties

        [LiteProperty("invoice_item_id", SqlDbType.Int)]
        public int InvoiceItemId
        {
            get;
            set;
        }

        [LiteProperty("invoice_id", SqlDbType.Int)]
        public int InvoiceId
        {
            get;
            set;
        }

        [LiteProperty("item_name", SqlDbType.NVarChar)]
        public string ItemName
        {
            get;
            set;
        }

        [LiteProperty("orginal_price", SqlDbType.Money)]
        public double OrginalPrice
        {
            get;
            set;
        }

        [LiteProperty("unit_price", SqlDbType.Money)]
        public double UnitPrice
        {
            get;
            set;
        }

        [LiteProperty("total_price", SqlDbType.Money)]
        public double TotalPrice
        {
            get;
            set;
        }

        [LiteProperty("percentage_discount", SqlDbType.Money)]
        public double PercentageDiscount
        {
            get;
            set;
        }

        [LiteProperty("amount_discount", SqlDbType.Money)]
        public double AmountDiscount
        {
            get;
            set;
        }

        [LiteProperty("quantity", SqlDbType.Real)]
        public Single? Quantity
        {
            get;
            set;
        }

        //[LiteProperty("min_price", SqlDbType.Money)]
        //public double MinPrice
        //{
        //    get;
        //    set;
        //}

        [LiteProperty("tax_amount", SqlDbType.Money)]
        public double TaxAmount
        {
            get;
            set;
        }

        [LiteProperty("bar_code", SqlDbType.NVarChar)]
        public string BarCode
        {
            get;
            set;
        }

        [LiteProperty("tax_percentage", SqlDbType.Money)]
        public double TaxPercentage
        {
            get;
            set;
        }

        [LiteProperty("is_refunded", SqlDbType.Bit)]
        public bool? IsRefunded
        {
            get;
            set;
        }

        [LiteProperty("parent_item_id", SqlDbType.Int)]
        public int ParentItemId
        {
            get;
            set;
        }

        [LiteProperty("refunded_quantity", SqlDbType.Real)]
        public Single? RefundedQuantity
        {
            get;
            set;
        }

        [LiteProperty("unit_id", SqlDbType.NVarChar)]
        public string UnitId
        {
            get;
            set;
        }

        [LiteProperty("item_no", SqlDbType.NVarChar)]
        public string ItemNo
        {
            get;
            set;
        }


        [LiteProperty("original_price_before_change", SqlDbType.Money)]
        public double OriginalPriceBeforeChange
        {
            get;
            set;
        }

        [LiteProperty("tax_befor_change", SqlDbType.Money)]
        public double TaxBeforeChange
        {
            get;
            set;
        }
        [LiteProperty("invoice_discount_per_item", SqlDbType.Money)]
        public double InvoiceDiscountPerItem
        {
            get;
            set;
        }  

        [LiteProperty("serial_number", SqlDbType.NVarChar)]
        public string SerialNumber
        {
            get;
            set;
        }

        [LiteProperty("is_serial", SqlDbType.Bit)]
        public bool? IsSerial
        {
            get;
            set;
        }

        [LiteProperty("is_synced", SqlDbType.Bit)]
        public bool? IsSynced
        {
            get;
            set;
        }

        [LiteProperty("invoice_unique_number", SqlDbType.NVarChar)]
        public string InvoiceUniqueNumber
        {
            get;
            set;
        }



        [LiteProperty("invoice_item_id", SqlDbType.Int)]
        public int value
        {
            get;
            set;
        }

        [LiteProperty("invoice_item_id", SqlDbType.Int)]
        public int label
        {
            get;
            set;
        }

        [LiteProperty("flag_deleted", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? FlagDeleted
        {
            get;
            set;
        }

        [LiteProperty("is_system", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? IsSystem
        {
            get;
            set;
        }

        [LiteProperty("created_date", SqlDbType.DateTime)]
        public DateTime? CreatedDate
        {
            get;
            set;
        }

        [LiteProperty("updated_date", SqlDbType.DateTime)]
        public DateTime? UpdatedDate
        {
            get;
            set;
        }

        [LiteProperty("created_by", SqlDbType.Int, FindByIdLiteParameter = false)]
        public int CreatedBy
        {
            get;
            set;
        }

        [LiteProperty("updated_by", SqlDbType.Int, FindByIdLiteParameter = false)]
        public int UpdatedBy
        {
            get;
            set;
        }



        [LiteProperty("is_exchanged", SqlDbType.Bit)]
        public bool IsExchanged
        {
            get;
            set;
        }

        [LiteProperty("created_by_name", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string CreatedByName { get; set; }

        [LiteProperty("updated_by_name", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string UpdatedByName { get; set; }

        [LiteProperty("invoice_item_id", SqlDbType.NVarChar)]
        public int Id
        {
            get { return this.InvoiceItemId; }
            set { this.InvoiceItemId = value; }
        }

        [LiteProperty("last_rows", SqlDbType.NVarChar)]
        public bool LastRows { get; set; }

        [LiteProperty("total_count", SqlDbType.NVarChar)]
        public int TotalRecords { get; set; }

        [LiteProperty("item_uom", SqlDbType.Char)]
        public string UnitofMeasure  { get; set; }

        
        #endregion


    }

    public class InvoiceItemIntegration
    {

        public int ItemId { get; set; }
        public int CategoryId { get; set; }
        public int UnitOfMeasureId { get; set; }
        public double Quantity { get; set; }
        public double UnitPrice { get; set; }
        public int TaxClassId { get; set; }
        public double LineDiscountPercent { get; set; }
        public double DiscountAmount { get; set; }
        public double LineTotalAfterDiscount { get; set; }
        public double TotalLineAmount { get; set; }
        public double TaxRate { get; set; }
        public double TaxAmount { get; set; }
        public int PriceListId { get; set; }
        public double UnitCost { get; set; }
        public bool IsSerial { get; set; }
        public string SerialNumber { get; set; }
        public double CashTotal { get; set; }

       


    }
}