using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
using SagePOS.Manger.Common.Business.Entity;

namespace SagePOS.Server.Business.Entity
{
    [EntityIManager("SagePOS.Server.Business.IManager.IPOSItemPriceListManager,SagePOS.Server.Business.IManager", isLogicalDelete = true)]
    public class POSItemPriceList : EntityBase<POSItemPriceList, int>
    {
        #region Constructors

        public POSItemPriceList() { }

        public POSItemPriceList(int posItemPriceListId)
        {
            this.posItemPriceListId = posItemPriceListId;
        }

        #endregion

        #region Private Properties

        private int posItemPriceListId;

        private int itemPriceListId;

        private int itemCardId;

        private string itemBarCode;

        private string itemNumber;

        private int priceListGroupId;

        private int priceListId;

        private string priceListCode;

        private string priceListDescription;

        private decimal? basePrice;

        private bool? flagDeleted;

        private bool? isSystem;

        private DateTime? createdDate;

        private DateTime? updatedDate;

        private int updatedBy;

        private int createdBy;

        private int priceListGroupItemId;


        //private bool? flagDeleted;

        //private bool? isSystem;

        //private int createdBy;

        //private int updatedBy;

        //private DateTime? createdDate;

        //private DateTime? updatedDate;
        #endregion

        #region Public Properties

        [PrimaryKey("@pos_item_price_list_id", SqlDbType.Int)]
        [Property("pos_item_price_list_id", SqlDbType.Int, AddParameter = false)]
        public int PosItemPriceListId
        {
            get
            {
                return posItemPriceListId;
            }
            set
            {
                posItemPriceListId = value;
            }
        }



        [Property("price_list_group_item_id", SqlDbType.Int)]
        [PropertyConstraint(true, "PriceListGroupItemId")]

        public int PriceListGroupItemId
        {
            get
            {
                return priceListGroupItemId;
            }
            set
            {
                priceListGroupItemId = value;
            }
        }


        [Property("item_price_list_id", SqlDbType.Int)]
        [PropertyConstraint(true, "ItemPriceList")]
        public int ItemPriceListId
        {
            get
            {
                return itemPriceListId;
            }
            set
            {
                itemPriceListId = value;
            }
        }

        [Property("item_card_id", SqlDbType.Int)]
        [PropertyConstraint(false, "ItemCard")]
        public int ItemCardId
        {
            get
            {
                return itemCardId;
            }
            set
            {
                itemCardId = value;
            }
        }

        [Property("item_bar_code", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "ItemBarCode", MaxLength = 100, MinLength = 3)]
        public string ItemBarCode
        {
            get
            {
                return itemBarCode;
            }
            set
            {
                itemBarCode = value;
            }
        }

        [Property("item_number", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "ItemNumber", MaxLength = 100, MinLength = 3)]
        public string ItemNumber
        {
            get
            {
                return itemNumber;
            }
            set
            {
                itemNumber = value;
            }
        }

        [Property("price_list_group_id", SqlDbType.Int)]
        [PropertyConstraint(false, "PriceListGroup")]
        public int PriceListGroupId
        {
            get
            {
                return priceListGroupId;
            }
            set
            {
                priceListGroupId = value;
            }
        }

        [Property("price_list_id", SqlDbType.Int)]
        [PropertyConstraint(false, "PriceList")]
        public int PriceListId
        {
            get
            {
                return priceListId;
            }
            set
            {
                priceListId = value;
            }
        }

        [Property("price_list_code", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "PriceListCode", MaxLength = 100, MinLength = 3)]
        public string PriceListCode
        {
            get
            {
                return priceListCode;
            }
            set
            {
                priceListCode = value;
            }
        }

        [Property("price_list_description", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "PriceListDescription", MaxLength = 100, MinLength = 3)]
        public string PriceListDescription
        {
            get
            {
                return priceListDescription;
            }
            set
            {
                priceListDescription = value;
            }
        }

        [Property("base_price", SqlDbType.Money)]
        [PropertyConstraint(false, "BasePrice")]
        public decimal? BasePrice
        {
            get
            {
                return basePrice;
            }
            set
            {
                basePrice = value;
            }
        }



        [Property("flag_deleted", SqlDbType.Bit, AddParameter = true, UpdateParameter = true, FindParameter = true)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [Property("is_system", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool? IsSystem
        {
            get
            {
                return isSystem;
            }
            set
            {
                isSystem = value;
            }
        }

        [Property("created_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? CreatedDate
        {
            get
            {
                return createdDate;
            }
            set
            {
                createdDate = value;
            }
        }

        [Property("updated_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? UpdatedDate
        {
            get
            {
                return updatedDate;
            }
            set
            {
                updatedDate = value;
            }
        }

        [Property("created_by", SqlDbType.Int, AddParameter = true, UpdateParameter = false)]
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        [Property("updated_by", SqlDbType.Int, AddParameter = false, UpdateParameter = true)]
        public int UpdatedBy
        {
            get
            {
                return updatedBy;
            }
            set
            {
                updatedBy = value;
            }
        }
        #endregion

        #region Composite Objects


        #endregion


        public override int GetIdentity()
        {
            return posItemPriceListId;
        }
        public override void SetIdentity(int value)
        {
            posItemPriceListId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            throw new NotImplementedException();
        }
    }
}