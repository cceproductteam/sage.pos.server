using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace SagePOS.Server.Business.Entity
{
     public class POSItemPriceListLite : EntityBaseLite
    {      
        
        #region Public Properties

         		[LiteProperty("pos_item_price_list_id",SqlDbType.Int)]
		public int PosItemPriceListId
		{
			get;
			set;
		}

                [LiteProperty("price_list_group_item_id", SqlDbType.Int)]
                public int PriceListGroupItemId
                {
                    get;
                    set;
                }

        
		[LiteProperty("item_price_list_id",SqlDbType.Int)]
		public int ItemPriceListId
		{
			get;
			set;
		}

		[LiteProperty("item_card_id",SqlDbType.Int)]
		public int ItemCardId
		{
			get;
			set;
		}

		[LiteProperty("item_bar_code",SqlDbType.NVarChar)]
		public string ItemBarCode
		{
			get;
			set;
		}

		[LiteProperty("item_number",SqlDbType.NVarChar)]
		public string ItemNumber
		{
			get;
			set;
		}

		[LiteProperty("price_list_group_id",SqlDbType.Int)]
		public int PriceListGroupId
		{
			get;
			set;
		}

		[LiteProperty("price_list_id",SqlDbType.Int)]
		public int PriceListId
		{
			get;
			set;
		}

		[LiteProperty("price_list_code",SqlDbType.NVarChar)]
		public string PriceListCode
		{
			get;
			set;
		}

		[LiteProperty("price_list_description",SqlDbType.NVarChar)]
		public string PriceListDescription
		{
			get;
			set;
		}

		[LiteProperty("base_price",SqlDbType.Money)]
		public decimal? BasePrice
		{
			get;
			set;
		}



        [LiteProperty("pos_item_price_list_id", SqlDbType.Int)]
        public int value
        {
            get;
            set;
        }

          [LiteProperty("pos_item_price_list_id", SqlDbType.Int)]
        public int label
        {
            get;
            set;
        }

         [LiteProperty("flag_deleted", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? FlagDeleted
        {
            get;
            set;
        }

        [LiteProperty("is_system",SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
		public bool? IsSystem
		{
			get;
			set;
		}

		[LiteProperty("created_date",SqlDbType.DateTime)]
		public DateTime? CreatedDate
		{
			get;
			set;
		}

		[LiteProperty("updated_date",SqlDbType.DateTime)]
		public DateTime? UpdatedDate
		{
			get;
			set;
		}

		[LiteProperty("created_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int CreatedBy
		{
			get;
			set;
		}

		[LiteProperty("updated_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int UpdatedBy
		{
			get;
			set;
		}

         [LiteProperty("created_by_name",SqlDbType.NVarChar, FindAllLiteParameter = false)]
         public string CreatedByName{get;set;}

         [LiteProperty("updated_by_name",SqlDbType.NVarChar,FindAllLiteParameter = false)]
         public string UpdatedByName{get;set;}

         [LiteProperty("pos_item_price_list_id",SqlDbType.Int)]
          public int Id
        {
            get{return this.PosItemPriceListId; }
            set{this.PosItemPriceListId=value;}
        }

         [LiteProperty("last_rows",SqlDbType.NVarChar)]
         public bool LastRows { get; set; }

         [LiteProperty("total_count",SqlDbType.NVarChar)]
         public int TotalRecords { get; set; }
        #endregion
       
          
     }
}