using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace SagePOS.Server.Business.Entity
{
    public class PersonLite : EntityBaseLite
    {

        #region Public Properties



        [LiteProperty("is_quick_add", SqlDbType.Bit)]
        public bool IsQuickAdd
        {
            get;
            set;

        }
        [LiteProperty("email", SqlDbType.NVarChar)]
        public string Email   
        {
            get;
            set;

        }

        [LiteProperty("mobile_number", SqlDbType.NVarChar)]
        public string MobileNumber
        {
            get;
            set;

        }




        [LiteProperty("person_id", SqlDbType.Int)]
        public int PersonId
        {
            get;
            set;
        }

        [LiteProperty("company_id", SqlDbType.Int)]
        public int CompanyId
        {
            get;
            set;
        }

        [LiteProperty("first_name_en", SqlDbType.NVarChar)]
        public string FirstNameEn
        {
            get;
            set;
        }

        [LiteProperty("middle_name_en", SqlDbType.NVarChar)]
        public string MiddleNameEn
        {
            get;
            set;
        }

        [LiteProperty("last_name_en", SqlDbType.NVarChar)]
        public string LastNameEn
        {
            get;
            set;
        }

        [LiteProperty("first_name_ar", SqlDbType.NVarChar)]
        public string FirstNameAr
        {
            get;
            set;
        }

        [LiteProperty("middle_name_ar", SqlDbType.NVarChar)]
        public string MiddleNameAr
        {
            get;
            set;
        }

        [LiteProperty("last_name_ar", SqlDbType.NVarChar)]
        public string LastNameAr
        {
            get;
            set;
        }

        [LiteProperty("full_name_en", SqlDbType.NVarChar)]
        public string FullNameEn
        {
            get;
            set;
        }

        [LiteProperty("full_name_ar", SqlDbType.NVarChar)]
        public string FullNameAr
        {
            get;
            set;
        }

         [LiteProperty("interested_in_id", SqlDbType.NVarChar)]
        public string InterestedInId
        {
            get;
            set;
        }

         [LiteProperty("interested_in_en", SqlDbType.NVarChar)]
         public string InterestedInEn
         {
             get;
             set;
         }
         [LiteProperty("interested_in_ar", SqlDbType.NVarChar)]
         public string InterestedInAr
         {
             get;
             set;
         }

     
         public string InterestedIn
         {
             get
             {
                 if (Configuration.Configuration.Lang.ToString() == "ar")
                     return InterestedInAr;
                 else
                     return InterestedInEn;
             }
             set { }
         }


         [LiteProperty("anniversary", SqlDbType.DateTime)]
         public DateTime?  Anniversary
         {
             get;
             set;
         }
         [LiteProperty("notify_for_anniversary", SqlDbType.Bit)]
         public bool NotifyForAnniversary
         {
             get;
             set;
         }
          [LiteProperty("notify_for_birthday", SqlDbType.Bit)]
         public bool NotifyForBirthday
         {
             get;
             set;
         }
          [LiteProperty("nationality_id", SqlDbType.Int)]
          public int  NationalityId
         {
             get;
             set;
         }

          [LiteProperty("nationality_en", SqlDbType.NVarChar)]
          public string NationalityEn
          {
              get;
              set;
          }

          [LiteProperty("nationality_ar", SqlDbType.NVarChar)]
          public string NationalityAr
          {
              get;
              set;
          }

      
          public string Nationality
          {
              get{
                  if (Configuration.Configuration.Lang.ToString() == "ar")
                      return NationalityAr;
                  else
                      return NationalityEn;
              }
              set { }
          }

         [LiteProperty("contact_type_id", SqlDbType.Int)]
         public int ContactTypeId
         {
             get;
             set;
         }

         [LiteProperty("contact_type_en", SqlDbType.NVarChar)]
         public string ContactTypeEn
         {
             get;
             set;
         }
            [LiteProperty("contact_type_ar", SqlDbType.NVarChar)]
         public string ContactTypeAr
         {
             get;
             set;
         }

      
             
         public string ContactType
         {
             get{
                 
                if (Configuration.Configuration.Lang.ToString() == "ar")
                    return ContactTypeAr;
                else
                     return ContactTypeEn;

             }

             set{}
         }




       

        [LiteProperty("primary_contact_employee_value", SqlDbType.Int)]
        public int PrimaryContactEmployeeId
        {
            get;
            set;
        }

        [LiteProperty("salutation_id", SqlDbType.Int)]
        public int SalutationId
        {
            get;
            set;
        }

        [LiteProperty("gender_id", SqlDbType.Int)]
        public int GenderId
        {
            get;
            set;
        }

        [LiteProperty("source_id", SqlDbType.Int)]
        public int SourceId
        {
            get;
            set;
        }

        [LiteProperty("date_of_birth", SqlDbType.Date)]
        public DateTime? DateOfBirth
        {
            get;
            set;
        }

        [LiteProperty("religion_id", SqlDbType.Int)]
        public int ReligionId
        {
            get;
            set;
        }

        [LiteProperty("marital_status_id", SqlDbType.Int)]
        public int MaritalStatusId
        {
            get;
            set;
        }

        [LiteProperty("department_id", SqlDbType.Int)]
        public int DepartmentId
        {
            get;
            set;
        }

        [LiteProperty("position_id", SqlDbType.Int)]
        public int PositionId
        {
            get;
            set;
        }

        [LiteProperty("contact_image_id", SqlDbType.Int)]
        public int ContactImageId
        {
            get;
            set;
        }


        [LiteProperty("flag_deleted", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? FlagDeleted
        {
            get;
            set;
        }

        [LiteProperty("is_system", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? IsSystem
        {
            get;
            set;
        }

        [LiteProperty("created_date", SqlDbType.DateTime)]
        public DateTime? CreatedDate
        {
            get;
            set;
        }

        [LiteProperty("updated_date", SqlDbType.DateTime)]
        public DateTime? UpdatedDate
        {
            get;
            set;
        }

        [LiteProperty("created_by", SqlDbType.Int, FindByIdLiteParameter = false)]
        public int CreatedBy
        {
            get;
            set;
        }

        [LiteProperty("updated_by", SqlDbType.Int, FindByIdLiteParameter = false)]
        public int? UpdatedBy
        {
            get;
            set;
        }

        [LiteProperty("created_by_name", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string CreatedByName { get; set; }

        [LiteProperty("updated_by_name", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string UpdatedByName { get; set; }

        [LiteProperty("person_id", SqlDbType.NVarChar)]
        public int Id
        {
            get { return this.PersonId; }
            set { this.PersonId = value; }
        }

        [LiteProperty("last_rows", SqlDbType.NVarChar)]
        public bool LastRows { get; set; }

        [LiteProperty("total_count", SqlDbType.NVarChar)]
        public int TotalRecords { get; set; }

       

        public string label
        {
            get
            {
                 
                    return FullNameEn;
            }
        }
        public int value
        {
            get { return PersonId; }

        }

      
         [LiteProperty("hear_about_us_id", SqlDbType.Int)]
        public int HearAboutUsId
        {
            get;
            set;
        }
        [LiteProperty("hear_about_us_en", SqlDbType.NVarChar)]
         public string HearAboutUsEn
        {
            get;
            set;
        }
        [LiteProperty("hear_about_us_ar", SqlDbType.NVarChar)]
        public string HearAboutUsAr
        {
            get;
            set;
        }
        public string HearAboutUs
        {
            get
            {
                if (Configuration.Configuration.Lang.ToString() == "ar")
                    return HearAboutUsAr;
                else
                    return HearAboutUsEn;
            }
            set { }
        }


        [LiteProperty("best_time_to_contact_id", SqlDbType.Int)]
        public int BestTimeToContactId
        {
            get;
            set;
        }
        [LiteProperty("best_time_to_contact_en", SqlDbType.NVarChar)]
        public string BestTimeToContactEn
        {
            get;
            set;
        }
        [LiteProperty("best_time_to_contact_ar", SqlDbType.NVarChar)]
        public string BestTimeToContactAr
        {
            get;
            set;
        }
        public string BestTimeToContact
        {
            get
            {
                if (Configuration.Configuration.Lang.ToString() == "ar")
                    return BestTimeToContactAr;
                else
                    return BestTimeToContactEn;
            }
            set { }
        }

        [LiteProperty("account_manager_id", SqlDbType.Int)]
        public int AccountManagerId
        {
            get;
            set;
        }
        [LiteProperty("account_manager_en", SqlDbType.NVarChar)]
        public string AccountManagerEn
        {
            get;
            set;
        }
        [LiteProperty("account_manager_ar", SqlDbType.NVarChar)]
        public string AccountManagerAr
        {
            get;
            set;
        }
        public string AccountManager
        {
            get
            {
                if (Configuration.Configuration.Lang.ToString() == "ar")
                    return AccountManagerAr;
                else
                    return AccountManagerEn;
            }
            set { }
        }

        //[LiteProperty("is_vip", SqlDbType.Bit)]
        //public bool? IsVip
        //{
        //    get;
        //    set;
        //}
        [LiteProperty("facebook_page", SqlDbType.Bit)]
        public string FacebookPage
        {
            get;
            set;
        }
        [LiteProperty("linkedIn_page", SqlDbType.Bit)]
        public string LinkedInPage
        {
            get;
            set;
        }
        [LiteProperty("twitter_account", SqlDbType.Bit)]
        public string TwitterAccount
        {
            get;
            set;
        }
        [LiteProperty("website", SqlDbType.Bit)]
        public string Website
        {
            get;
            set;
        }
      


        ///////////////////////////////////////////////////////////////////////////////////////////////////


        [LiteProperty("company_name_arabic", SqlDbType.NVarChar)]
        public string CompanyNameAr { get; set; }

        [LiteProperty("company_name_english", SqlDbType.NVarChar)]
        public string CompanyNameEN { get; set; }

        public string Company
        {
            get
            {
               
                    return CompanyNameEN;
            }
        }


        [LiteProperty("salutation_en", SqlDbType.NVarChar)]
        public string SalutationEN { get; set; }

        [LiteProperty("salutation_ar", SqlDbType.NVarChar)]
        public string SalutationAR { get; set; }


        public string Salutation
        {
            get
            {
                if (Configuration.Configuration.Lang.ToString() == "ar")
                    return SalutationAR;
                else
                    return SalutationEN;
            }
        }



        [LiteProperty("gender_en", SqlDbType.NVarChar)]
        public string GenderEN { get; set; }

        [LiteProperty("gender_ar", SqlDbType.NVarChar)]
        public string GenderAR { get; set; }

        public string Gender
        {
            get
            {
                if (Configuration.Configuration.Lang.ToString() == "ar")
                    return GenderAR;
                else
                    return GenderEN;
            }
        }



        [LiteProperty("source_en", SqlDbType.NVarChar)]
        public string SourceEN { get; set; }

        [LiteProperty("source_ar", SqlDbType.NVarChar)]
        public string SourceAR { get; set; }

        public string Source
        {
            get
            {
                if (Configuration.Configuration.Lang.ToString() == "ar")
                    return SourceAR;
                else
                    return SourceEN;
            }
        }

        [LiteProperty("religion_en", SqlDbType.NVarChar)]
        public string ReligionEN { get; set; }

        [LiteProperty("religion_ar", SqlDbType.NVarChar)]
        public string ReligionAR { get; set; }

        public string Religion
        {
            get
            {
                if (Configuration.Configuration.Lang.ToString() == "ar")
                    return ReligionAR;
                else
                    return ReligionEN;
            }
        }


        [LiteProperty("marital_status_en", SqlDbType.NVarChar)]
        public string MaritalStatusEN { get; set; }

        [LiteProperty("marital_status_ar", SqlDbType.NVarChar)]
        public string MaritalStatusAR { get; set; }

        public string MaritalStatus
        {
            get
            {
                if (Configuration.Configuration.Lang.ToString() == "ar")
                    return MaritalStatusAR;
                else
                    return MaritalStatusEN;
            }
        }


        [LiteProperty("position_en", SqlDbType.NVarChar)]
        public string PositionEN { get; set; }

        [LiteProperty("position_ar", SqlDbType.NVarChar)]
        public string PositionAR { get; set; }

        public string Position
        {
            get
            {
                if (Configuration.Configuration.Lang.ToString() == "ar")
                    return PositionAR;
                else
                    return PositionEN;
            }
        }



        private string vipClass;
        private string vipClassAr;
        [LiteProperty("vip_class_id", SqlDbType.NVarChar)]
        public int VipClassId
        {
            get;
            set;
        }
        [LiteProperty("vip_class", SqlDbType.NVarChar)]
        public string VipClass
        {
            get { return vipClass; }
            set { vipClass = value; }
        }
        [LiteProperty("vip_class_ar", SqlDbType.NVarChar)]
        public string VipClassAr
        {
            get { return vipClassAr; }
            set { vipClassAr = value; }
        }


        [LiteProperty("department_en", SqlDbType.NVarChar)]
        public string DepartmentEN { get; set; }

        [LiteProperty("department_ar", SqlDbType.NVarChar)]
        public string DepartmentAR { get; set; }

        public string Department
        {
            get
            {
                if (Configuration.Configuration.Lang.ToString() == "ar")
                    return DepartmentAR;
                else
                    return DepartmentEN;
            }
        }


        [LiteProperty("primary_contact_employee_en", SqlDbType.NVarChar)]
        public string PrimaryContactEmployeeEN { get; set; }


        [LiteProperty("primary_contact_employee_ar", SqlDbType.NVarChar)]
        public string PrimaryContactEmployeeAR { get; set; }


        public string PrimaryContactEmployee
        {
            get
            {
                if (Configuration.Configuration.Lang.ToString() == "ar")
                    return PrimaryContactEmployeeAR;
                else
                    return PrimaryContactEmployeeEN;
            }
        }

        [LiteProperty("business_email", SqlDbType.NVarChar)]
        public string BusinessEmail
        {
            get;
            set;
        }

        [LiteProperty("personal_email", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string PersonalEmail
        {
            get;
            set;
        }
      
        [LiteProperty("country_mobile_number", SqlDbType.NVarChar, FindAllLiteParameter = true)]
        public string CountryMobileNumber
        {
            get;
            set;
        }
        [LiteProperty("area_mobile_number", SqlDbType.NVarChar, FindAllLiteParameter = true)]
        public string AreaMobileNumber
        {
            get;
            set;
        }
        [LiteProperty("phone_number", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string HomeNumber
        {
            get;
            set;
        }
        [LiteProperty("country_phone_number", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string CountryHomeNumber
        {
            get;
            set;
        }
        [LiteProperty("area_phone_number", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string AreaHomeNumber
        {
            get;
            set;
        }
        [LiteProperty("fax_number", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string FaxNumber
        {
            get;
            set;
        }
        [LiteProperty("country_fax_number", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string CountryFaxNumber
        {
            get;
            set;
        }
        [LiteProperty("area_fax_number", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string AreaFaxNumber
        {
            get;
            set;
        }

        //[LiteProperty("mobile_number", SqlDbType.NVarChar)]
        //public string MobileNumber
        //{
        //    get;
        //    set;
        //}
        //[LiteProperty("phone_number", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        //public string HomeNumber
        //{
        //    get;
        //    set;
        //}
        //[LiteProperty("fax_number", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        //public string FaxNumber
        //{
        //    get;
        //    set;
        //}

        [LiteProperty("address_country", SqlDbType.Int, FindAllLiteParameter = false)]
        public int CountryId
        {
            get;
            set;
        }
        [LiteProperty("address_country_en", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string CountryEn { set; get; }


        [LiteProperty("address_country_ar", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string CountryAr { set; get; }
        public string Country
        {
            get
            {
                if (Configuration.Configuration.Lang.ToString() == "ar")
                    return CountryAr;
                else
                    return CountryEn;
            }
        }


        [LiteProperty("address_city_text", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string AddressCityText
        {
            get;
            set;
        }

        [LiteProperty("address_street_address", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string AddressStreetAddress
        {
            get;
            set;
        }

        [LiteProperty("address_nearby", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string NearBy
        {
            get;
            set;
        }

        [LiteProperty("address_place", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string Place
        {
            get;
            set;
        }


        [LiteProperty("address_building_number", SqlDbType.Int, FindAllLiteParameter = false)]
        public int? BuildingNumber
        {
            get;
            set;
        }

        [LiteProperty("address_pobox", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string POBox
        {
            get;
            set;
        }

        [LiteProperty("address_zip_code", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string ZipCode
        {
            get;
            set;
        }

        [LiteProperty("address_region", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string Region
        {
            get;
            set;
        }

        [LiteProperty("address_area", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string Area
        {
            get;
            set;
        }

        private List<SagePOS.Manger.Common.Business.Entity.DataTypeContentLite> intresetedInList = null;
        public List<SagePOS.Manger.Common.Business.Entity.DataTypeContentLite> IntresetedInList
        {
            get
            {
                return intresetedInList;
            }
            set
            {
                intresetedInList = value;
            }
        }
    
        
        
        #endregion


    }
}