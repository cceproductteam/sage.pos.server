using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
using SagePOS.Server.Business.Factory ;

namespace SagePOS.Server.Business.Entity
{
    public class PersonCriteria : CriteriaBase, IAdvancedSearchCriteria
    {
        [CrtiteriaField("@page_number", SqlDbType.Int)]
        public int pageNumber { get; set; }

        [CrtiteriaField("@result_count", SqlDbType.Int)]
        public int resultCount { get; set; }

        [CrtiteriaField("@keyword", SqlDbType.NVarChar)]
        public string Keyword { get; set; }

        [CrtiteriaField("@search_criteria", SqlDbType.NVarChar)]
        public string SearchCriteria { get; set; }

        [CrtiteriaField("@sort_type", SqlDbType.Bit)]
        public bool? SortType { get; set; }

        [CrtiteriaField("@sort_fields", SqlDbType.NVarChar)]
        public string SortFields { get; set; }

        [CrtiteriaField("@person_id", SqlDbType.Int, IsReferance = true)]
        public int? PersonId { get; set; }


        [CrtiteriaField("@company_id", SqlDbType.Int, IsReferance = true)]
        public int? CompanyId { get; set; }

        [CrtiteriaField("@person_id", SqlDbType.Int, IsReferance = true)]
        public int? EntityId { get; set; }

        [CrtiteriaField("@excipt_id", SqlDbType.Int)]
        public int? ExciptId { get; set; }

        public int NotificationOccasion { get; set; }

        [CrtiteriaField("@last_sync_date", SqlDbType.DateTime)]
        public DateTime? LastSyncDate { get; set; }

        [CrtiteriaField("@person_name", SqlDbType.NVarChar)]
        public string PersonName { get; set; }

        [CrtiteriaField("@mobile_number", SqlDbType.NVarChar)]
        public string MobileNumber { get; set; }

        [CrtiteriaField("@is_quick_add", SqlDbType.Bit)]
        public bool? IsQuickAdd { get; set; }

        [CrtiteriaField("@is_synced", SqlDbType.Bit)]
        public bool? IsSynced { get; set; }



        int IAdvancedSearchCriteria.EntityId
        {
            get;
            set;
        }

        bool IAdvancedSearchCriteria.SortType
        {
            get;
            set;
        }

        //int IAdvancedSearchCriteria.pageNumber
        //{
        //    get;
        //    set;
        //}

        //int IAdvancedSearchCriteria.resultCount
        //{
        //    get;
        //    set;
        //}
    }
}
