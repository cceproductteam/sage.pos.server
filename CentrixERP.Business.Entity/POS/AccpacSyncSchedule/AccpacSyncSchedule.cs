using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;




namespace SagePOS.Server.Business.Entity
{
    [EntityIManager("SagePOS.Server.Business.IManager.IAccpacSyncScheduleManager,Centrix.Business.IManager", isLogicalDelete = true)]
    public class AccpacSyncSchedule : EntityBase<AccpacSyncSchedule, int>
    {
        #region Constructors

        public AccpacSyncSchedule() { }

        public AccpacSyncSchedule(int tabId)
        {
            this.tabId = tabId;
        }

        #endregion

        #region Private Properties

		private int tabId;

		private bool? postInvoice;

		private bool? postRefund;

		private bool? postExchange;

		private bool? postPettyCash;

		private Int16? status;

		private string syncLog;
private  int updatedBy;
private  int createdBy;
private  DateTime? updatedDate;
private  DateTime? createdDate;
private  bool? isSystem;
private  bool? flagDeleted;
       


        #endregion

        #region Public Properties

        [PrimaryKey("@tab_id", SqlDbType.Int)]
[Property("tab_id", SqlDbType.Int, AddParameter = false)]
		public int TabId
		{
			get
			{
				return tabId;
			}
			set
			{
				tabId = value;
			}
		}

[Property("post_invoice", SqlDbType.Bit)]
[PropertyConstraint(false, "PostInvoice")]
		public bool? PostInvoice
		{
			get
			{
				return postInvoice;
			}
			set
			{
				postInvoice = value;
			}
		}

[Property("post_refund", SqlDbType.Bit)]
[PropertyConstraint(false, "PostRefund")]
		public bool? PostRefund
		{
			get
			{
				return postRefund;
			}
			set
			{
				postRefund = value;
			}
		}

[Property("post_exchange", SqlDbType.Bit)]
[PropertyConstraint(false, "PostExchange")]
		public bool? PostExchange
		{
			get
			{
				return postExchange;
			}
			set
			{
				postExchange = value;
			}
		}

[Property("post_petty_cash", SqlDbType.Bit)]
[PropertyConstraint(false, "PostPettyCash")]
		public bool? PostPettyCash
		{
			get
			{
				return postPettyCash;
			}
			set
			{
				postPettyCash = value;
			}
		}

[Property("status", SqlDbType.SmallInt)]
[PropertyConstraint(false, "Status")]
		public Int16? Status
		{
			get
			{
				return status;
			}
			set
			{
				status = value;
			}
		}

[Property("sync_log", SqlDbType.NVarChar)]
[PropertyConstraint(false, "SyncLog", MaxLength = 50, MinLength = 3)]
		public string SyncLog
		{
			get
			{
				return syncLog;
			}
			set
			{
				syncLog = value;
			}
		}



        [Property("flag_deleted", SqlDbType.Bit, AddParameter = false, UpdateParameter = false,FindParameter=false)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [Property("is_system", SqlDbType.Bit, AddParameter = false, UpdateParameter = false,FindParameter=false)]
        public bool? IsSystem
        {
            get
            {
                return isSystem;
            }
            set
            {
                isSystem = value;
            }
        }

		[Property("created_date",SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
		public DateTime? CreatedDate
		{
			get
			{
				return createdDate;
			}
			set
			{
				createdDate = value;
			}
		}

        [Property("updated_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
		public DateTime? UpdatedDate
		{
			get
			{
				return updatedDate;
			}
			set
			{
				updatedDate = value;
			}
		}

		[Property("created_by",SqlDbType.Int, AddParameter = true, UpdateParameter = false)]
		public int CreatedBy
		{
			get
			{
				return createdBy;
			}
			set
			{
				createdBy = value;
			}
		}

		[Property("updated_by",SqlDbType.Int, AddParameter = false, UpdateParameter = true)]
		public int UpdatedBy
		{
			get
			{
				return updatedBy;
			}
			set
			{
				updatedBy = value;
			}
		}
        #endregion

             #region Composite Objects
              

                 #endregion
   

        public override int GetIdentity()
        {
            return tabId;
        }
        public override void SetIdentity(int value)
        {
            tabId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            			throw new NotImplementedException();
        }
    
    }
}
