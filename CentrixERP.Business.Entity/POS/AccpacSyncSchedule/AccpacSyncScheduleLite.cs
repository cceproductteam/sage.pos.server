using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace SagePOS.Server.Business.Entity
{
     public class AccpacSyncScheduleLite : EntityBaseLite
    {      
        
        #region Public Properties

         		[LiteProperty("tab_id",SqlDbType.Int)]
		public int TabId
		{
			get;
			set;
		}

		[LiteProperty("post_invoice",SqlDbType.Bit)]
		public bool? PostInvoice
		{
			get;
			set;
		}

		[LiteProperty("post_refund",SqlDbType.Bit)]
		public bool? PostRefund
		{
			get;
			set;
		}

		[LiteProperty("post_exchange",SqlDbType.Bit)]
		public bool? PostExchange
		{
			get;
			set;
		}

		[LiteProperty("post_petty_cash",SqlDbType.Bit)]
		public bool? PostPettyCash
		{
			get;
			set;
		}

		[LiteProperty("status",SqlDbType.SmallInt)]
		public Int16? Status
		{
			get;
			set;
		}

		[LiteProperty("sync_log",SqlDbType.NVarChar)]
		public string SyncLog
		{
			get;
			set;
		}



       
        public int value
        {
            get{ return Id;}
        }

        
        public string label
        {
           get{ return Id.ToString();}
        }

         [LiteProperty("flag_deleted", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? FlagDeleted
        {
            get;
            set;
        }

        [LiteProperty("is_system",SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
		public bool? IsSystem
		{
			get;
			set;
		}

		[LiteProperty("created_date",SqlDbType.DateTime)]
		public DateTime? CreatedDate
		{
			get;
			set;
		}

		[LiteProperty("updated_date",SqlDbType.DateTime)]
		public DateTime? UpdatedDate
		{
			get;
			set;
		}

		[LiteProperty("created_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int CreatedBy
		{
			get;
			set;
		}

		[LiteProperty("updated_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int UpdatedBy
		{
			get;
			set;
		}

         [LiteProperty("created_by_name",SqlDbType.NVarChar, FindAllLiteParameter = false)]
         public string CreatedByName{get;set;}

         [LiteProperty("updated_by_name",SqlDbType.NVarChar,FindAllLiteParameter = false)]
         public string UpdatedByName{get;set;}

        
          public int Id
        {
            get{return this.TabId; }
        }

         [LiteProperty("last_rows",SqlDbType.Bit)]
         public bool LastRows { get; set; }

         [LiteProperty("total_count",SqlDbType.Int)]
         public int TotalRecords { get; set; }
        #endregion
       
          
     }
}