using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
using SagePOS.Manger.Common.Business.Entity;

namespace SagePOS.Server.Business.Entity
{
    [EntityIManager("SagePOS.Server.Business.IManager.IPOSItemTaxManager,SagePOS.Server.Business.IManager", isLogicalDelete = true)]
    public class POSItemTax : EntityBase<POSItemTax, int>
    {
        #region Constructors

        public POSItemTax() { }

        public POSItemTax(int posItemTaxId)
        {
            this.posItemTaxId = posItemTaxId;
        }

        #endregion

        #region Private Properties

		private int posItemTaxId;

		private int itemTaxId;

		private int itemCardId;

		private string itemBarCode;

		private int taxAuthorityId;

		private string taxAuthority;

		private int itemTaxClass;

		private double? rate;

		private int custTaxClass;

		private bool? flagDeleted;

		private bool? isSystem;

		private DateTime? createdDate;

		private DateTime? updatedDate;

		private int updatedBy;

		private int createdBy;



        //private bool? flagDeleted;

        //private bool? isSystem;

        //private int createdBy;

        //private int updatedBy;

        //private DateTime? createdDate;

        //private DateTime? updatedDate;
        #endregion

        #region Public Properties

        [PrimaryKey("@pos_item_tax_id", SqlDbType.Int)]
[Property("pos_item_tax_id", SqlDbType.Int, AddParameter = false)]
		public int PosItemTaxId
		{
			get
			{
				return posItemTaxId;
			}
			set
			{
				posItemTaxId = value;
			}
		}

[Property("item_tax_id", SqlDbType.Int)]
[PropertyConstraint(true, "ItemTax")]
		public int ItemTaxId
		{
			get
			{
				return itemTaxId;
			}
			set
			{
				itemTaxId = value;
			}
		}

[Property("item_card_id", SqlDbType.Int)]
[PropertyConstraint(false, "ItemCard")]
		public int ItemCardId
		{
			get
			{
				return itemCardId;
			}
			set
			{
				itemCardId = value;
			}
		}

[Property("item_bar_code", SqlDbType.NVarChar)]
[PropertyConstraint(false, "ItemBarCode", MaxLength = 100, MinLength = 3)]
		public string ItemBarCode
		{
			get
			{
				return itemBarCode;
			}
			set
			{
				itemBarCode = value;
			}
		}

[Property("tax_authority_id", SqlDbType.Int)]
[PropertyConstraint(false, "TaxAuthority")]
		public int TaxAuthorityId
		{
			get
			{
				return taxAuthorityId;
			}
			set
			{
				taxAuthorityId = value;
			}
		}

[Property("tax_authority", SqlDbType.NVarChar)]
[PropertyConstraint(false, "TaxAuthority", MaxLength = 100, MinLength = 3)]
		public string TaxAuthority
		{
			get
			{
				return taxAuthority;
			}
			set
			{
				taxAuthority = value;
			}
		}

[Property("item_tax_class", SqlDbType.Int)]
[PropertyConstraint(false, "ItemTaxClass")]
		public int ItemTaxClass
		{
			get
			{
				return itemTaxClass;
			}
			set
			{
				itemTaxClass = value;
			}
		}

[Property("rate", SqlDbType.Float)]
[PropertyConstraint(false, "Rate")]
		public double? Rate
		{
			get
			{
				return rate;
			}
			set
			{
				rate = value;
			}
		}

[Property("cust_tax_class", SqlDbType.Int)]
[PropertyConstraint(false, "CustTaxClass")]
		public int CustTaxClass
		{
			get
			{
				return custTaxClass;
			}
			set
			{
				custTaxClass = value;
			}
		}



        [Property("flag_deleted", SqlDbType.Bit, AddParameter = true, UpdateParameter = true,FindParameter=true)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [Property("is_system", SqlDbType.Bit, AddParameter = false, UpdateParameter = false,FindParameter=false)]
        public bool? IsSystem
        {
            get
            {
                return isSystem;
            }
            set
            {
                isSystem = value;
            }
        }

		[Property("created_date",SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
		public DateTime? CreatedDate
		{
			get
			{
				return createdDate;
			}
			set
			{
				createdDate = value;
			}
		}

        [Property("updated_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
		public DateTime? UpdatedDate
		{
			get
			{
				return updatedDate;
			}
			set
			{
				updatedDate = value;
			}
		}

		[Property("created_by",SqlDbType.Int, AddParameter = true, UpdateParameter = false)]
		public int CreatedBy
		{
			get
			{
				return createdBy;
			}
			set
			{
				createdBy = value;
			}
		}

		[Property("updated_by",SqlDbType.Int, AddParameter = false, UpdateParameter = true)]
		public int UpdatedBy
		{
			get
			{
				return updatedBy;
			}
			set
			{
				updatedBy = value;
			}
		}
        #endregion

             #region Composite Objects
              

                 #endregion
   

        public override int GetIdentity()
        {
            return posItemTaxId;
        }
        public override void SetIdentity(int value)
        {
            posItemTaxId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            			throw new NotImplementedException();
        }
    }
}