using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace SagePOS.Server.Business.Entity
{
     public class POSItemTaxLite : EntityBaseLite
    {      
        
        #region Public Properties

         		[LiteProperty("pos_item_tax_id",SqlDbType.Int)]
		public int PosItemTaxId
		{
			get;
			set;
		}

		[LiteProperty("item_tax_id",SqlDbType.Int)]
		public int ItemTaxId
		{
			get;
			set;
		}

		[LiteProperty("item_card_id",SqlDbType.Int)]
		public int ItemCardId
		{
			get;
			set;
		}

		[LiteProperty("item_bar_code",SqlDbType.NVarChar)]
		public string ItemBarCode
		{
			get;
			set;
		}

		[LiteProperty("tax_authority_id",SqlDbType.Int)]
		public int TaxAuthorityId
		{
			get;
			set;
		}

		[LiteProperty("tax_authority",SqlDbType.NVarChar)]
		public string TaxAuthority
		{
			get;
			set;
		}

		[LiteProperty("item_tax_class",SqlDbType.Int)]
		public int ItemTaxClass
		{
			get;
			set;
		}

		[LiteProperty("rate",SqlDbType.Float)]
		public double? Rate
		{
			get;
			set;
		}

		[LiteProperty("cust_tax_class",SqlDbType.Int)]
		public int CustTaxClass
		{
			get;
			set;
		}



        [LiteProperty("pos_item_tax_id", SqlDbType.Int)]
        public int value
        {
            get;
            set;
        }

          [LiteProperty("pos_item_tax_id", SqlDbType.Int)]
        public int label
        {
            get;
            set;
        }

         [LiteProperty("flag_deleted", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? FlagDeleted
        {
            get;
            set;
        }

        [LiteProperty("is_system",SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
		public bool? IsSystem
		{
			get;
			set;
		}

		[LiteProperty("created_date",SqlDbType.DateTime)]
		public DateTime? CreatedDate
		{
			get;
			set;
		}

		[LiteProperty("updated_date",SqlDbType.DateTime)]
		public DateTime? UpdatedDate
		{
			get;
			set;
		}

		[LiteProperty("created_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int CreatedBy
		{
			get;
			set;
		}

		[LiteProperty("updated_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int UpdatedBy
		{
			get;
			set;
		}

         [LiteProperty("created_by_name",SqlDbType.NVarChar, FindAllLiteParameter = false)]
         public string CreatedByName{get;set;}

         [LiteProperty("updated_by_name",SqlDbType.NVarChar,FindAllLiteParameter = false)]
         public string UpdatedByName{get;set;}

         [LiteProperty("pos_item_tax_id",SqlDbType.NVarChar)]
          public int Id
        {
            get{return this.PosItemTaxId; }
            set{this.PosItemTaxId=value;}
        }

         [LiteProperty("last_rows",SqlDbType.NVarChar)]
         public bool LastRows { get; set; }

         [LiteProperty("total_count",SqlDbType.NVarChar)]
         public int TotalRecords { get; set; }
        #endregion
       
          
     }
}