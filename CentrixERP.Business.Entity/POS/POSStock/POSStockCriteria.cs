using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
using SagePOS.Server.Business.Factory ;

namespace SagePOS.Server.Business.Entity
{
    public class POSStockCriteria : CriteriaBase, IAdvancedSearchCriteria
    {
        [CrtiteriaField("@page_number", SqlDbType.Int)]
        public int? pageNumber { get; set; }

        [CrtiteriaField("@result_count", SqlDbType.Int)]
        public int? resultCount { get; set; }

        [CrtiteriaField("@keyword", SqlDbType.NVarChar)]
        public string Keyword { get; set; }

        [CrtiteriaField("@search_criteria", SqlDbType.NVarChar)]
        public string SearchCriteria { get; set; }

        [CrtiteriaField("@sort_type", SqlDbType.Bit)]
        public bool? SortType { get; set; }

        [CrtiteriaField("@sort_fields", SqlDbType.NVarChar)]
        public string SortFields { get; set; }

        [CrtiteriaField("@pos_stock_id", SqlDbType.Int)]
        public int? PosStockId { get; set; }

        [CrtiteriaField("@pos_stock_id", SqlDbType.Int)]
        public int? EntityId { get; set; }

        [CrtiteriaField("@last_sync_date", SqlDbType.DateTime)]
        public DateTime? LastSyncDate { get; set; }

        int IAdvancedSearchCriteria.EntityId
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        bool IAdvancedSearchCriteria.SortType
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        int IAdvancedSearchCriteria.pageNumber
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        int IAdvancedSearchCriteria.resultCount
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }
}
