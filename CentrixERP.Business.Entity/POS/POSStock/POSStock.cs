using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
using SagePOS.Manger.Common.Business.Entity;

namespace SagePOS.Server.Business.Entity
{
    [EntityIManager("SagePOS.Server.Business.IManager.IPOSStockManager,SagePOS.Server.Business.IManager", isLogicalDelete = true)]
    public class POSStock : EntityBase<POSStock, int>
    {
        #region Constructors

        public POSStock() { }

        public POSStock(int posStockId)
        {
            this.posStockId = posStockId;
        }

        #endregion

        #region Private Properties

		private int posStockId;

		private int stockId;

		private int itemCardId;

		private string itemCard;

		private int locationId;

		private string locationName;

		private double? quantityOnHand;

		private int costUnitOfMeasure;

		private bool? flagDeleted;

		private bool? isSystem;

		private DateTime? createdDate;

		private DateTime? updatedDate;

		private int updatedBy;

		private int createdBy;



        //private bool? flagDeleted;

        //private bool? isSystem;

        //private int createdBy;

        //private int updatedBy;

        //private DateTime? createdDate;

        //private DateTime? updatedDate;
        #endregion

        #region Public Properties

        [PrimaryKey("@pos_stock_id", SqlDbType.Int)]
[Property("pos_stock_id", SqlDbType.Int, AddParameter = false)]
		public int PosStockId
		{
			get
			{
				return posStockId;
			}
			set
			{
				posStockId = value;
			}
		}

[Property("stock_id", SqlDbType.Int)]
[PropertyConstraint(true, "Stock")]
		public int StockId
		{
			get
			{
				return stockId;
			}
			set
			{
				stockId = value;
			}
		}

[Property("item_card_id", SqlDbType.Int)]
[PropertyConstraint(false, "ItemCard")]
		public int ItemCardId
		{
			get
			{
				return itemCardId;
			}
			set
			{
				itemCardId = value;
			}
		}

[Property("item_card", SqlDbType.NVarChar)]
[PropertyConstraint(false, "ItemCard", MaxLength = 100, MinLength = 3)]
		public string ItemCard
		{
			get
			{
				return itemCard;
			}
			set
			{
				itemCard = value;
			}
		}

[Property("location_id", SqlDbType.Int)]
[PropertyConstraint(false, "Location")]
		public int LocationId
		{
			get
			{
				return locationId;
			}
			set
			{
				locationId = value;
			}
		}

[Property("location_name", SqlDbType.NVarChar)]
[PropertyConstraint(false, "LocationName", MaxLength = 100, MinLength = 3)]
		public string LocationName
		{
			get
			{
				return locationName;
			}
			set
			{
				locationName = value;
			}
		}

[Property("quantity_on_hand", SqlDbType.Float)]
[PropertyConstraint(false, "QuantityOnHand")]
		public double? QuantityOnHand
		{
			get
			{
				return quantityOnHand;
			}
			set
			{
				quantityOnHand = value;
			}
		}

[Property("cost_unit_of_measure", SqlDbType.Int)]
[PropertyConstraint(false, "CostUnitOfMeasure")]
		public int CostUnitOfMeasure
		{
			get
			{
				return costUnitOfMeasure;
			}
			set
			{
				costUnitOfMeasure = value;
			}
		}



        [Property("flag_deleted", SqlDbType.Bit, AddParameter = true, UpdateParameter = true, FindParameter = true)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [Property("is_system", SqlDbType.Bit, AddParameter = false, UpdateParameter = false,FindParameter=false)]
        public bool? IsSystem
        {
            get
            {
                return isSystem;
            }
            set
            {
                isSystem = value;
            }
        }

		[Property("created_date",SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
		public DateTime? CreatedDate
		{
			get
			{
				return createdDate;
			}
			set
			{
				createdDate = value;
			}
		}

        [Property("updated_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
		public DateTime? UpdatedDate
		{
			get
			{
				return updatedDate;
			}
			set
			{
				updatedDate = value;
			}
		}

		[Property("created_by",SqlDbType.Int, AddParameter = true, UpdateParameter = false)]
		public int CreatedBy
		{
			get
			{
				return createdBy;
			}
			set
			{
				createdBy = value;
			}
		}

		[Property("updated_by",SqlDbType.Int, AddParameter = false, UpdateParameter = true)]
		public int UpdatedBy
		{
			get
			{
				return updatedBy;
			}
			set
			{
				updatedBy = value;
			}
		}
        #endregion

             #region Composite Objects
              

                 #endregion
   

        public override int GetIdentity()
        {
            return posStockId;
        }
        public override void SetIdentity(int value)
        {
            posStockId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            			throw new NotImplementedException();
        }
    }
}