using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
using SagePOS.Manger.Common.Business.Entity;

namespace SagePOS.Server.Business.Entity
{
    [EntityIManager("SagePOS.Server.Business.IManager.IItemCardManager,SagePOS.Server.Business.IManager", isLogicalDelete = true)]
    public class ItemCard : EntityBase<ItemCard, int>
    {
        #region Constructors

        public ItemCard() { }

        public ItemCard(int itemCardId)
        {
            this.itemCardId = itemCardId;
        }

        #endregion

        #region Private Properties

        private int itemCardId;

        private int structureCode;

        private string itemNumber;

        private string itemDescription;

        private int coastingMethod = -1;

        private int defaultPriceListId = -1;

        private string defaultPriceList;

        private string defaultPickingSequence;

        private string unitWeight;

        private int weightUnitOfMeasureId = -1;

        private int alternateItem;

        private string additionalItemInformation;

        private bool? isActive;

        private bool? stockItem;

        private bool? serialNumber;

        private bool? lotNumber;

        private bool? sellable;

        private bool? kittingItem;

        private int itemImageId = -1;

        private bool? validateQuantityOnBoq;

        private int createdBy;

        private DateTime? createdDate;

        private int updatedBy;

        private DateTime? updatedDate;

        private bool? flagDeleted;

        private bool? isSystem;

        private string itemBarCode;

        private string itemSku;

        private int categoryId = -1;

        private int accountSetCodeId = -1;

        private string manufacturingCompany;

        private double? capacity;



        //private bool? flagDeleted;

        //private bool? isSystem;

        //private int createdBy;

        //private int updatedBy;

        //private DateTime? createdDate;

        //private DateTime? updatedDate;
        #endregion

        #region Public Properties

        [PrimaryKey("@item_card_id", SqlDbType.Int)]
        [Property("item_card_id", SqlDbType.Int, AddParameter = false)]
        public int ItemCardId
        {
            get
            {
                return itemCardId;
            }
            set
            {
                itemCardId = value;
            }
        }

        [Property("structure_code", SqlDbType.Int)]
        [PropertyConstraint(false, "StructureCode")]
        public int StructureCode
        {
            get
            {
                return structureCode;
            }
            set
            {
                structureCode = value;
            }
        }

        [Property("item_number", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "ItemNumber", MaxLength = 100, MinLength = 3)]
        public string ItemNumber
        {
            get
            {
                return itemNumber;
            }
            set
            {
                itemNumber = value;
            }
        }

        [Property("item_description", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "ItemDescription", MaxLength = 100, MinLength = 3)]
        public string ItemDescription
        {
            get
            {
                return itemDescription;
            }
            set
            {
                itemDescription = value;
            }
        }

        [Property("coasting_method", SqlDbType.Int, ReferenceParameter = false)]
        [PropertyConstraint(false, "CoastingMethod", RefEntity = false)]
        public int CoastingMethod
        {
            get
            {
                return coastingMethod;
            }
            set
            {
                coastingMethod = value;
            }
        }

        [Property("default_price_list_id", SqlDbType.Int, ReferenceParameter = false)]
        [PropertyConstraint(false, "DefaultPriceList", RefEntity = false)]
        public int DefaultPriceListId
        {
            get
            {
                return defaultPriceListId;
            }
            set
            {
                defaultPriceListId = value;
            }
        }

        [Property("default_price_list", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "DefaultPriceList", MaxLength = 50, MinLength = 3)]
        public string DefaultPriceList
        {
            get
            {
                return defaultPriceList;
            }
            set
            {
                defaultPriceList = value;
            }
        }

        [Property("default_picking_sequence", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "DefaultPickingSequence", MaxLength = 50, MinLength = 3)]
        public string DefaultPickingSequence
        {
            get
            {
                return defaultPickingSequence;
            }
            set
            {
                defaultPickingSequence = value;
            }
        }

        [Property("unit_weight", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "UnitWeight", MaxLength = 50, MinLength = 3)]
        public string UnitWeight
        {
            get
            {
                return unitWeight;
            }
            set
            {
                unitWeight = value;
            }
        }

        [Property("weight_unit_of_measure_id", SqlDbType.Int, ReferenceParameter = false)]
        [PropertyConstraint(false, "WeightUnitOfMeasure", RefEntity = false)]
        public int WeightUnitOfMeasureId
        {
            get
            {
                return weightUnitOfMeasureId;
            }
            set
            {
                weightUnitOfMeasureId = value;
            }
        }

        [Property("alternate_item", SqlDbType.Int)]
        [PropertyConstraint(false, "AlternateItem")]
        public int AlternateItem
        {
            get
            {
                return alternateItem;
            }
            set
            {
                alternateItem = value;
            }
        }

        [Property("additional_item_information", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "AdditionalItemInformation", MaxLength = 500, MinLength = 3)]
        public string AdditionalItemInformation
        {
            get
            {
                return additionalItemInformation;
            }
            set
            {
                additionalItemInformation = value;
            }
        }

        [Property("is_active", SqlDbType.Bit)]
        [PropertyConstraint(false, "IsActive")]
        public bool? IsActive
        {
            get
            {
                return isActive;
            }
            set
            {
                isActive = value;
            }
        }

        [Property("stock_item", SqlDbType.Bit)]
        [PropertyConstraint(false, "StockItem")]
        public bool? StockItem
        {
            get
            {
                return stockItem;
            }
            set
            {
                stockItem = value;
            }
        }

        [Property("serial_number", SqlDbType.Bit)]
        [PropertyConstraint(false, "SerialNumber")]
        public bool? SerialNumber
        {
            get
            {
                return serialNumber;
            }
            set
            {
                serialNumber = value;
            }
        }

        [Property("lot_number", SqlDbType.Bit)]
        [PropertyConstraint(false, "LotNumber")]
        public bool? LotNumber
        {
            get
            {
                return lotNumber;
            }
            set
            {
                lotNumber = value;
            }
        }

        [Property("sellable", SqlDbType.Bit)]
        [PropertyConstraint(false, "Sellable")]
        public bool? Sellable
        {
            get
            {
                return sellable;
            }
            set
            {
                sellable = value;
            }
        }

        [Property("kitting_item", SqlDbType.Bit)]
        [PropertyConstraint(false, "KittingItem")]
        public bool? KittingItem
        {
            get
            {
                return kittingItem;
            }
            set
            {
                kittingItem = value;
            }
        }

        [Property("item_image_id", SqlDbType.Int, ReferenceParameter = false)]
        [PropertyConstraint(false, "ItemImage", RefEntity = false)]
        public int ItemImageId
        {
            get
            {
                return itemImageId;
            }
            set
            {
                itemImageId = value;
            }
        }

        [Property("validate_quantity_on_boq", SqlDbType.Bit)]
        [PropertyConstraint(false, "ValidateQuantityOnBoq")]
        public bool? ValidateQuantityOnBoq
        {
            get
            {
                return validateQuantityOnBoq;
            }
            set
            {
                validateQuantityOnBoq = value;
            }
        }

        [Property("item_bar_code", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "ItemBarCode", MaxLength = 100, MinLength = 3)]
        public string ItemBarCode
        {
            get
            {
                return itemBarCode;
            }
            set
            {
                itemBarCode = value;
            }
        }

        [Property("item_sku", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "ItemSku", MaxLength = 100, MinLength = 3)]
        public string ItemSku
        {
            get
            {
                return itemSku;
            }
            set
            {
                itemSku = value;
            }
        }

        [Property("category_id", SqlDbType.Int, ReferenceParameter = false)]
        [PropertyConstraint(false, "Category", RefEntity = false)]
        public int CategoryId
        {
            get
            {
                return categoryId;
            }
            set
            {
                categoryId = value;
            }
        }

        [Property("account_set_code_id", SqlDbType.Int, ReferenceParameter = false)]
        [PropertyConstraint(false, "AccountSetCode", RefEntity =false)]
        public int AccountSetCodeId
        {
            get
            {
                return accountSetCodeId;
            }
            set
            {
                accountSetCodeId = value;
            }
        }

        [Property("manufacturing_company", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "ManufacturingCompany", MaxLength = 100, MinLength = 3)]
        public string ManufacturingCompany
        {
            get
            {
                return manufacturingCompany;
            }
            set
            {
                manufacturingCompany = value;
            }
        }

        [Property("capacity", SqlDbType.Float)]
        [PropertyConstraint(false, "Capacity")]
        public double? Capacity
        {
            get
            {
                return capacity;
            }
            set
            {
                capacity = value;
            }
        }



        [Property("flag_deleted", SqlDbType.Bit, AddParameter = true, UpdateParameter = true, FindParameter = true)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [Property("is_system", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool? IsSystem
        {
            get
            {
                return isSystem;
            }
            set
            {
                isSystem = value;
            }
        }

        [Property("created_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? CreatedDate
        {
            get
            {
                return createdDate;
            }
            set
            {
                createdDate = value;
            }
        }

        [Property("updated_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? UpdatedDate
        {
            get
            {
                return updatedDate;
            }
            set
            {
                updatedDate = value;
            }
        }

        [Property("created_by", SqlDbType.Int, AddParameter = true, UpdateParameter = false)]
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        [Property("updated_by", SqlDbType.Int, AddParameter = false, UpdateParameter = true)]
        public int UpdatedBy
        {
            get
            {
                return updatedBy;
            }
            set
            {
                updatedBy = value;
            }
        }
        #endregion

        #region Composite Objects
        //private DataTypeContent coastingMethodObj;
        //[CompositeObject(CompositObjectTypeEnum.Single, "CoastingMethod", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable = false)]
        //public DataTypeContent CoastingMethodObj
        //{
        //    get
        //    {
        //        if (coastingMethodObj == null)
        //            loadCompositObject_Lazy("CoastingMethodObj");
        //        return coastingMethodObj;
        //    }
        //    set
        //    {
        //        coastingMethodObj = value; ;
        //    }
        //}

        //private IcPriceListGroup icPriceListGroupObj;
        //[CompositeObject(CompositObjectTypeEnum.Single, "DefaultPriceListId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable = false)]
        //public IcPriceListGroup IcPriceListGroupObj
        //{
        //    get
        //    {
        //        if (icPriceListGroupObj == null)
        //            loadCompositObject_Lazy("IcPriceListGroupObj");
        //        return icPriceListGroupObj;
        //    }
        //    set
        //    {
        //        icPriceListGroupObj = value; ;
        //    }
        //}

        //private IcWieghtUnitOfMeasures icWieghtUnitOfMeasuresObj;
        //[CompositeObject(CompositObjectTypeEnum.Single, "WeightUnitOfMeasureId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable = false)]
        //public IcWieghtUnitOfMeasures IcWieghtUnitOfMeasuresObj
        //{
        //    get
        //    {
        //        if (icWieghtUnitOfMeasuresObj == null)
        //            loadCompositObject_Lazy("IcWieghtUnitOfMeasuresObj");
        //        return icWieghtUnitOfMeasuresObj;
        //    }
        //    set
        //    {
        //        icWieghtUnitOfMeasuresObj = value; ;
        //    }
        //}

        //private Document documentObj;
        //[CompositeObject(CompositObjectTypeEnum.Single, "ItemImageId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable = false)]
        //public Document DocumentObj
        //{
        //    get
        //    {
        //        if (documentObj == null)
        //            loadCompositObject_Lazy("DocumentObj");
        //        return documentObj;
        //    }
        //    set
        //    {
        //        documentObj = value; ;
        //    }
        //}

        //private IcCategory icCategoryObj;
        //[CompositeObject(CompositObjectTypeEnum.Single, "CategoryId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable = false)]
        //public IcCategory IcCategoryObj
        //{
        //    get
        //    {
        //        if (icCategoryObj == null)
        //            loadCompositObject_Lazy("IcCategoryObj");
        //        return icCategoryObj;
        //    }
        //    set
        //    {
        //        icCategoryObj = value; ;
        //    }
        //}

        //private IcAccountSet icAccountSetObj;
        //[CompositeObject(CompositObjectTypeEnum.Single, "AccountSetCodeId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable = false)]
        //public IcAccountSet IcAccountSetObj
        //{
        //    get
        //    {
        //        if (icAccountSetObj == null)
        //            loadCompositObject_Lazy("IcAccountSetObj");
        //        return icAccountSetObj;
        //    }
        //    set
        //    {
        //        icAccountSetObj = value; ;
        //    }
        //}

        //private List<IcAdjustmentDetail> icAdjustmentDetailList;
        //[CompositeObject(CompositObjectTypeEnum.List, "ItemCardId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true)]
        //public List<IcAdjustmentDetail> IcAdjustmentDetailList
        //{
        //    get
        //    {
        //        if (icAdjustmentDetailList == null)
        //            loadCompositObject_Lazy("IcAdjustmentDetailList");
        //        return icAdjustmentDetailList;
        //    }
        //    set
        //    {
        //        icAdjustmentDetailList = value; ;
        //    }
        //}

        //private List<IcBucket> icBucketList;
        //[CompositeObject(CompositObjectTypeEnum.List, "ItemCardId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true)]
        //public List<IcBucket> IcBucketList
        //{
        //    get
        //    {
        //        if (icBucketList == null)
        //            loadCompositObject_Lazy("IcBucketList");
        //        return icBucketList;
        //    }
        //    set
        //    {
        //        icBucketList = value; ;
        //    }
        //}

        //private List<IcItemCardSegment> icItemCardSegmentList;
        //[CompositeObject(CompositObjectTypeEnum.List, "ItemCardId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true)]
        //public List<IcItemCardSegment> IcItemCardSegmentList
        //{
        //    get
        //    {
        //        if (icItemCardSegmentList == null)
        //            loadCompositObject_Lazy("IcItemCardSegmentList");
        //        return icItemCardSegmentList;
        //    }
        //    set
        //    {
        //        icItemCardSegmentList = value; ;
        //    }
        //}

        //private List<IcItemCardSerial> icItemCardSerialList;
        //[CompositeObject(CompositObjectTypeEnum.List, "ItemCardId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true)]
        //public List<IcItemCardSerial> IcItemCardSerialList
        //{
        //    get
        //    {
        //        if (icItemCardSerialList == null)
        //            loadCompositObject_Lazy("IcItemCardSerialList");
        //        return icItemCardSerialList;
        //    }
        //    set
        //    {
        //        icItemCardSerialList = value; ;
        //    }
        //}

        //private List<IcItemLocationCosting> icItemLocationCostingList;
        //[CompositeObject(CompositObjectTypeEnum.List, "ItemCardId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true)]
        //public List<IcItemLocationCosting> IcItemLocationCostingList
        //{
        //    get
        //    {
        //        if (icItemLocationCostingList == null)
        //            loadCompositObject_Lazy("IcItemLocationCostingList");
        //        return icItemLocationCostingList;
        //    }
        //    set
        //    {
        //        icItemLocationCostingList = value; ;
        //    }
        //}

        //private List<IcItemLocationPricingSummation> icItemLocationPricingSummationList;
        //[CompositeObject(CompositObjectTypeEnum.List, "ItemCardId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true)]
        //public List<IcItemLocationPricingSummation> IcItemLocationPricingSummationList
        //{
        //    get
        //    {
        //        if (icItemLocationPricingSummationList == null)
        //            loadCompositObject_Lazy("IcItemLocationPricingSummationList");
        //        return icItemLocationPricingSummationList;
        //    }
        //    set
        //    {
        //        icItemLocationPricingSummationList = value; ;
        //    }
        //}

        //private List<IcItemTax> icItemTaxList;
        //[CompositeObject(CompositObjectTypeEnum.List, "ItemCardId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true)]
        //public List<IcItemTax> IcItemTaxList
        //{
        //    get
        //    {
        //        if (icItemTaxList == null)
        //            loadCompositObject_Lazy("IcItemTaxList");
        //        return icItemTaxList;
        //    }
        //    set
        //    {
        //        icItemTaxList = value; ;
        //    }
        //}

        //private List<IcItemUnitOfMeasures> icItemUnitOfMeasuresList;
        //[CompositeObject(CompositObjectTypeEnum.List, "ItemCardId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true)]
        //public List<IcItemUnitOfMeasures> IcItemUnitOfMeasuresList
        //{
        //    get
        //    {
        //        if (icItemUnitOfMeasuresList == null)
        //            loadCompositObject_Lazy("IcItemUnitOfMeasuresList");
        //        return icItemUnitOfMeasuresList;
        //    }
        //    set
        //    {
        //        icItemUnitOfMeasuresList = value; ;
        //    }
        //}

        //private List<IcPriceListGroupItem> icPriceListGroupItemList;
        //[CompositeObject(CompositObjectTypeEnum.List, "ItemCardId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true)]
        //public List<IcPriceListGroupItem> IcPriceListGroupItemList
        //{
        //    get
        //    {
        //        if (icPriceListGroupItemList == null)
        //            loadCompositObject_Lazy("IcPriceListGroupItemList");
        //        return icPriceListGroupItemList;
        //    }
        //    set
        //    {
        //        icPriceListGroupItemList = value; ;
        //    }
        //}

        //private List<IcReceiptDetail> icReceiptDetailList;
        //[CompositeObject(CompositObjectTypeEnum.List, "ItemCardId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true)]
        //public List<IcReceiptDetail> IcReceiptDetailList
        //{
        //    get
        //    {
        //        if (icReceiptDetailList == null)
        //            loadCompositObject_Lazy("IcReceiptDetailList");
        //        return icReceiptDetailList;
        //    }
        //    set
        //    {
        //        icReceiptDetailList = value; ;
        //    }
        //}

        //private List<IcShipmentDetail> icShipmentDetailList;
        //[CompositeObject(CompositObjectTypeEnum.List, "ItemCardId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true)]
        //public List<IcShipmentDetail> IcShipmentDetailList
        //{
        //    get
        //    {
        //        if (icShipmentDetailList == null)
        //            loadCompositObject_Lazy("IcShipmentDetailList");
        //        return icShipmentDetailList;
        //    }
        //    set
        //    {
        //        icShipmentDetailList = value; ;
        //    }
        //}

        //private List<IcTransferDetail> icTransferDetailList;
        //[CompositeObject(CompositObjectTypeEnum.List, "ItemCardId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true)]
        //public List<IcTransferDetail> IcTransferDetailList
        //{
        //    get
        //    {
        //        if (icTransferDetailList == null)
        //            loadCompositObject_Lazy("IcTransferDetailList");
        //        return icTransferDetailList;
        //    }
        //    set
        //    {
        //        icTransferDetailList = value; ;
        //    }
        //}



        #endregion


        public override int GetIdentity()
        {
            return itemCardId;
        }
        public override void SetIdentity(int value)
        {
            itemCardId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {

        }
    }
}