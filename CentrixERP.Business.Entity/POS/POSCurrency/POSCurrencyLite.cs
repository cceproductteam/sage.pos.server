using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace SagePOS.Server.Business.Entity
{
     public class POSCurrencyLite : EntityBaseLite
    {      
        
        #region Public Properties

         		[LiteProperty("pos_currency_id",SqlDbType.Int)]
		public int PosCurrencyId
		{
			get;
			set;
		}

		[LiteProperty("currency_id",SqlDbType.Int)]
		public int CurrencyId
		{
			get;
			set;
		}

		[LiteProperty("code",SqlDbType.NVarChar)]
		public string Code
		{
			get;
			set;
		}

		[LiteProperty("description",SqlDbType.NVarChar)]
		public string Description
		{
			get;
			set;
		}

		[LiteProperty("symbol",SqlDbType.NVarChar)]
		public string Symbol
		{
			get;
			set;
		}



        [LiteProperty("pos_currency_id", SqlDbType.Int)]
        public int value
        {
            get;
            set;
        }

          [LiteProperty("symbol", SqlDbType.NVarChar)]
        public string label
        {
            get;
            set;
        }

         [LiteProperty("flag_deleted", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? FlagDeleted
        {
            get;
            set;
        }

        [LiteProperty("is_system",SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
		public bool? IsSystem
		{
			get;
			set;
		}

		[LiteProperty("created_date",SqlDbType.DateTime)]
		public DateTime? CreatedDate
		{
			get;
			set;
		}

		[LiteProperty("updated_date",SqlDbType.DateTime)]
		public DateTime? UpdatedDate
		{
			get;
			set;
		}

		[LiteProperty("created_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int CreatedBy
		{
			get;
			set;
		}

		[LiteProperty("updated_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int UpdatedBy
		{
			get;
			set;
		}

         [LiteProperty("created_by_name",SqlDbType.NVarChar, FindAllLiteParameter = false)]
         public string CreatedByName{get;set;}

         [LiteProperty("updated_by_name",SqlDbType.NVarChar,FindAllLiteParameter = false)]
         public string UpdatedByName{get;set;}

         [LiteProperty("pos_currency_id",SqlDbType.Int)]
          public int Id
        {
            get{return this.PosCurrencyId; }
            set{this.PosCurrencyId=value;}
        }

         [LiteProperty("last_rows",SqlDbType.NVarChar)]
         public bool LastRows { get; set; }

         [LiteProperty("total_count",SqlDbType.NVarChar)]
         public int TotalRecords { get; set; }
        #endregion
       
          
     }
}