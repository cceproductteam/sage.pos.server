using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
using CentrixERP.AdministrativeSettings.Business.Entity;
using SagePOS.Manger.Common.Business.Entity;
//using Centrix.Business.Entity;
//using Centrix.StandardEdition.CRM.Business.Entity;

namespace SagePOS.Server.Business.Entity
{
    [EntityIManager("SagePOS.Server.Business.IManager.IStoreManager,SagePOS.Server.Business.IManager", isLogicalDelete = true)]
    public class Store : EntityBase<Store, int>
    {
        #region Constructors

        public Store() { }

        public Store(int storeId)
        {
            this.storeId = storeId;
        }

        #endregion

        #region Private Properties

        private int storeId;

        private string storeName;

        private string storeNameAr;

        private string storeCode;

        private int defaultCurrencyId = -1;

        private string defaultCurrCode;

        private string defaultCurrSymbol;

        private int locationId = -1;

        private string locationCode;

        private string locationDesc;

        private string ipAddress;

        private string macAddress;

        private string dataBaseInstanceName;

        private string dataBaseName;

        private string dataBaseUserName;

        private string dataBasePassword;

        private int defaultTax;

        private int displayPricses;

        private bool? cashierDiscounts;

        private int legalEntityId;

        private int weightScaleFormulaId;

        private bool? isUsed;

        private int createdBy;

        private int updatedBy;

        private DateTime? createdDate;

        private DateTime? updatedDate;

        private bool? flagDeleted;

        private bool? isSystem;

        private int arCashCustomerId;
        private int arVisaCustomerId;
        private int arMixedCustomerId;

        private int glNationalId;

        private int glCheckId;
      



        //private bool? flagDeleted;

        //private bool? isSystem;

        //private int createdBy;

        //private int updatedBy;

        //private DateTime? createdDate;

        //private DateTime? updatedDate;
        #endregion

        #region Public Properties

        [PrimaryKey("@store_ID", SqlDbType.Int)]
        [Property("store_ID", SqlDbType.Int, AddParameter = false)]
        public int StoreId
        {
            get
            {
                return storeId;
            }
            set
            {
                storeId = value;
            }
        }

        [Property("store_name", SqlDbType.NVarChar)]
        [PropertyConstraint(true, "StoreName", MaxLength = 150, MinLength = 3)]
        public string StoreName
        {
            get
            {
                return storeName;
            }
            set
            {
                storeName = value;
            }
        }

        [Property("store_name_ar", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "StoreNameAr", MaxLength = 100, MinLength = 3)]
        public string StoreNameAr
        {
            get
            {
                return storeNameAr;
            }
            set
            {
                storeNameAr = value;
            }
        }

        [Property("store_code", SqlDbType.NVarChar)]
        [PropertyConstraint(true, "StoreCode", MaxLength = 4, MinLength = 1)]
        public string StoreCode
        {
            get
            {
                return storeCode;
            }
            set
            {
                storeCode = value;
            }
        }

        [Property("default_currency", SqlDbType.Int, ReferenceParameter = true)]
        [PropertyConstraint(false, "DefaultCurrency", RefEntity = true, RefPropertyEntity = "CurrencyObj")]
        public int DefaultCurrencyId
        {
            get
            {
                return defaultCurrencyId;
            }
            set
            {
                defaultCurrencyId = value;
            }
        }

        [Property("default_curr_code", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "DefaultCurrCode", MaxLength = 100, MinLength = 1)]
        public string DefaultCurrCode
        {
            get
            {
                return defaultCurrCode;
            }
            set
            {
                defaultCurrCode = value;
            }
        }

        [Property("default_curr_symbol", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "DefaultCurrSymbol", MaxLength = 100, MinLength = 1)]
        public string DefaultCurrSymbol
        {
            get
            {
                return defaultCurrSymbol;
            }
            set
            {
                defaultCurrSymbol = value;
            }
        }

        [Property("location_id", SqlDbType.Int, ReferenceParameter = true)]
        [PropertyConstraint(false, "Location", RefEntity = true, RefPropertyEntity = "CityObj")]
        public int LocationId
        {
            get
            {
                return locationId;
            }
            set
            {
                locationId = value;
            }
        }

        [Property("location_code", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "LocationCode", MaxLength = 100, MinLength = 3)]
        public string LocationCode
        {
            get
            {
                return locationCode;
            }
            set
            {
                locationCode = value;
            }
        }

        [Property("location_desc", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "LocationDesc", MaxLength = 100, MinLength = 3)]
        public string LocationDesc
        {
            get
            {
                return locationDesc;
            }
            set
            {
                locationDesc = value;
            }
        }

        [Property("ip_address", SqlDbType.NVarChar)]
        [PropertyConstraint(true, "IpAddress", MaxLength = 100, MinLength = 3)]
        public string IpAddress
        {
            get
            {
                return ipAddress;
            }
            set
            {
                ipAddress = value;
            }
        }

        [Property("mac_address", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "MacAddress", MaxLength = 100, MinLength = 3)]
        public string MacAddress
        {
            get
            {
                return macAddress;
            }
            set
            {
                macAddress = value;
            }
        }

        [Property("data_base_instance_name", SqlDbType.NVarChar)]
        [PropertyConstraint(true, "DataBaseInstanceName", MaxLength = 100, MinLength = 3)]
        public string DataBaseInstanceName
        {
            get
            {
                return dataBaseInstanceName;
            }
            set
            {
                dataBaseInstanceName = value;
            }
        }

        [Property("data_base_name", SqlDbType.NVarChar)]
        [PropertyConstraint(true, "DataBaseName", MaxLength = 100, MinLength = 3)]
        public string DataBaseName
        {
            get
            {
                return dataBaseName;
            }
            set
            {
                dataBaseName = value;
            }
        }

        [Property("data_base_user_name", SqlDbType.NVarChar)]
        [PropertyConstraint(true, "DataBaseUserName", MaxLength = 100, MinLength = 1)]
        public string DataBaseUserName
        {
            get
            {
                return dataBaseUserName;
            }
            set
            {
                dataBaseUserName = value;
            }
        }

        [Property("data_base_password", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "DataBasePassword", MaxLength = 100, MinLength = 3)]
        public string DataBasePassword
        {
            get
            {
                return dataBasePassword;
            }
            set
            {
                dataBasePassword = value;
            }
        }

        [Property("default_tax", SqlDbType.Int)]
        [PropertyConstraint(false, "DefaultTax")]
        public int DefaultTax
        {
            get
            {
                return defaultTax;
            }
            set
            {
                defaultTax = value;
            }
        }

        [Property("display_pricses", SqlDbType.Int)]
        [PropertyConstraint(false, "DisplayPricses")]
        public int DisplayPricses
        {
            get
            {
                return displayPricses;
            }
            set
            {
                displayPricses = value;
            }
        }

        [Property("cashier_discounts", SqlDbType.Bit)]
        [PropertyConstraint(false, "CashierDiscounts")]
        public bool? CashierDiscounts
        {
            get
            {
                return cashierDiscounts;
            }
            set
            {
                cashierDiscounts = value;
            }
        }

        [Property("legal_entity_id", SqlDbType.Int)]
        [PropertyConstraint(false, "LegalEntity")]
        public int LegalEntityId
        {
            get
            {
                return legalEntityId;
            }
            set
            {
                legalEntityId = value;
            }
        }

        [Property("weight_scale_formula_id", SqlDbType.Int)]
        [PropertyConstraint(false, "WeightScaleFormula")]
        public int WeightScaleFormulaId
        {
            get
            {
                return weightScaleFormulaId;
            }
            set
            {
                weightScaleFormulaId = value;
            }
        }

        [Property("is_used", SqlDbType.Bit)]
        [PropertyConstraint(false, "IsUsed")]
        public bool? IsUsed
        {
            get
            {
                return isUsed;
            }
            set
            {
                isUsed = value;
            }
        }

        [Property("ar_cash_customer_id", SqlDbType.Int)]
        [PropertyConstraint(false, "ArCashCustomerId")]
        public int ArCashCustomerId
        {
            get { return arCashCustomerId; }
            set { arCashCustomerId = value; }
        }

        [Property("ar_visa_customer_id", SqlDbType.Int)]
        [PropertyConstraint(false, "ArVisaCustomerId")]
        public int ArVisaCustomerId
        {
            get { return arVisaCustomerId; }
            set { arVisaCustomerId = value; }
        }

        [Property("ar_mixed_customer_id", SqlDbType.Int)]
        [PropertyConstraint(false, "ArMixedCustomerId")]
        public int ArMixedCustomerId
        {
            get { return arMixedCustomerId; }
            set { arMixedCustomerId = value; }
        }



        [Property("flag_deleted", SqlDbType.Bit, AddParameter = true, UpdateParameter = true, FindParameter = true)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [Property("is_system", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool? IsSystem
        {
            get
            {
                return isSystem;
            }
            set
            {
                isSystem = value;
            }
        }

        [Property("created_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? CreatedDate
        {
            get
            {
                return createdDate;
            }
            set
            {
                createdDate = value;
            }
        }

        [Property("updated_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? UpdatedDate
        {
            get
            {
                return updatedDate;
            }
            set
            {
                updatedDate = value;
            }
        }

        [Property("created_by", SqlDbType.Int, AddParameter = true, UpdateParameter = false)]
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        [Property("updated_by", SqlDbType.Int, AddParameter = false, UpdateParameter = true)]
        public int UpdatedBy
        {
            get
            {
                return updatedBy;
            }
            set
            {
                updatedBy = value;
            }
        }

        [Property("gl_account_id", SqlDbType.Int)]
        [PropertyConstraint(true, "GlAccountId")]
        public int GlAccountId
        {
            get;
            set;
        }

        //[Property("gl_account", SqlDbType.Int)]
        //[PropertyConstraint(true, "GlAccountId")]
        //public int GlAccount
        //{
        //    get;
        //    set;
        //}

        [Property("gl_national_id", SqlDbType.Int)]
       
        public int GlNationalId
        {
            get { return glNationalId;}
            set { glNationalId = value; }
        }

        [Property("gl_check_id", SqlDbType.Int)]
        public int GlCheckId
        {
            get { return glCheckId;}
            set { glCheckId = value; }
        }

        #endregion

        #region Composite Objects
        private Currency currencyObj;
        [CompositeObject(CompositObjectTypeEnum.Single, "DefaultCurrencyId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable = false)]
        public Currency CurrencyObj
        {
            get
            {
                if (currencyObj == null)
                    loadCompositObject_Lazy("CurrencyObj");
                return currencyObj;
            }
            set
            {
                currencyObj = value; ;
            }
        }

        private City cityObj;
        [CompositeObject(CompositObjectTypeEnum.Single, "LocationId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable = false)]
        public City CityObj
        {
            get
            {
                if (cityObj == null)
                    loadCompositObject_Lazy("CityObj");
                return cityObj;
            }
            set
            {
                cityObj = value; ;
            }
        }



        #endregion


        public override int GetIdentity()
        {
            return storeId;
        }
        public override void SetIdentity(int value)
        {
            storeId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            if (ParentType == typeof(Currency))
                this.defaultCurrencyId = (int)value;
            else if (ParentType == typeof(City))
                this.locationId = (int)value;

        }
    }
}