using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
using SagePOS.Manger.Common.Business.Entity;

namespace SagePOS.Server.Business.Entity
{
    [EntityIManager("SagePOS.Server.Business.IManager.IPOSPriceListManager,SagePOS.Server.Business.IManager", isLogicalDelete = true)]
    public class POSPriceList : EntityBase<POSPriceList, int>
    {
        #region Constructors

        public POSPriceList() { }

        public POSPriceList(int posPriceListId)
        {
            this.posPriceListId = posPriceListId;
        }

        #endregion

        #region Private Properties

        private int posPriceListId;

        private int priceListId;

        private string priceListCode;

        private string priceListDescription;

        private int currencyId;

        private string currency;

        private decimal? basePrice;

        private string unitOfMeasure;

        private bool? flagDeleted;

        private bool? isSystem;

        private DateTime? createdDate;

        private DateTime? updatedDate;

        private int updatedBy;

        private int createdBy;

        private int priceListGroupId;


        //private bool? flagDeleted;

        //private bool? isSystem;

        //private int createdBy;

        //private int updatedBy;

        //private DateTime? createdDate;

        //private DateTime? updatedDate;
        #endregion

        #region Public Properties

        [PrimaryKey("@pos_price_list_id", SqlDbType.Int)]
        [Property("pos_price_list_id", SqlDbType.Int, AddParameter = false)]
        public int PosPriceListId
        {
            get
            {
                return posPriceListId;
            }
            set
            {
                posPriceListId = value;
            }
        }

        [Property("price_list_group_id", SqlDbType.Int)]
        [PropertyConstraint(true, "PriceListGroupId")]
        public int PriceListGroupId
        {
            get
            {
                return priceListGroupId;
            }
            set
            {
                priceListGroupId = value;
            }
        }

        [Property("price_list_id", SqlDbType.Int)]
        [PropertyConstraint(true, "PriceList")]
        public int PriceListId
        {
            get
            {
                return priceListId;
            }
            set
            {
                priceListId = value;
            }
        }

        [Property("price_list_code", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "PriceListCode", MaxLength = 100, MinLength = 3)]
        public string PriceListCode
        {
            get
            {
                return priceListCode;
            }
            set
            {
                priceListCode = value;
            }
        }

        [Property("price_list_description", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "PriceListDescription", MaxLength = 100, MinLength = 3)]
        public string PriceListDescription
        {
            get
            {
                return priceListDescription;
            }
            set
            {
                priceListDescription = value;
            }
        }

        [Property("currency_id", SqlDbType.Int)]
        [PropertyConstraint(false, "Currency")]
        public int CurrencyId
        {
            get
            {
                return currencyId;
            }
            set
            {
                currencyId = value;
            }
        }

        [Property("currency", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "Currency", MaxLength = 50, MinLength = 3)]
        public string Currency
        {
            get
            {
                return currency;
            }
            set
            {
                currency = value;
            }
        }

        [Property("base_price", SqlDbType.Money)]
        [PropertyConstraint(false, "BasePrice")]
        public decimal? BasePrice
        {
            get
            {
                return basePrice;
            }
            set
            {
                basePrice = value;
            }
        }

        [Property("unit_of_measure", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "UnitOfMeasure", MaxLength = 100, MinLength = 3)]
        public string UnitOfMeasure
        {
            get
            {
                return unitOfMeasure;
            }
            set
            {
                unitOfMeasure = value;
            }
        }



        [Property("flag_deleted", SqlDbType.Bit, AddParameter = true, UpdateParameter = true, FindParameter = true)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [Property("is_system", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool? IsSystem
        {
            get
            {
                return isSystem;
            }
            set
            {
                isSystem = value;
            }
        }

        [Property("created_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? CreatedDate
        {
            get
            {
                return createdDate;
            }
            set
            {
                createdDate = value;
            }
        }

        [Property("updated_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? UpdatedDate
        {
            get
            {
                return updatedDate;
            }
            set
            {
                updatedDate = value;
            }
        }

        [Property("created_by", SqlDbType.Int, AddParameter = true, UpdateParameter = false)]
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        [Property("updated_by", SqlDbType.Int, AddParameter = false, UpdateParameter = true)]
        public int UpdatedBy
        {
            get
            {
                return updatedBy;
            }
            set
            {
                updatedBy = value;
            }
        }
        #endregion

        #region Composite Objects


        #endregion


        public override int GetIdentity()
        {
            return posPriceListId;
        }
        public override void SetIdentity(int value)
        {
            posPriceListId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            throw new NotImplementedException();
        }
    }
}