using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
using SagePOS.Manger.Common.Business.Entity;

namespace SagePOS.Server.Business.Entity
{
    [EntityIManager("SagePOS.Server.Business.IManager.IPOSCustomerManager,SagePOS.Server.Business.IManager", isLogicalDelete = true)]
    public class POSCustomer : EntityBase<POSCustomer, int>
    {
        #region Constructors

        public POSCustomer() { }

        public POSCustomer(int posCustomerId)
        {
            this.posCustomerId = posCustomerId;
        }

        #endregion

        #region Private Properties
        private bool taxable;
        private int posCustomerId;

        private int customerId;

        private string customerNumber;

        private string customerName;

        private int taxClassId;

        private double? creditLimitAmount;

        private string contactEmail;

        private string contactTelephone;

        private bool? flagDeleted;

        private bool? isSystem;

        private DateTime? createdDate;

        private DateTime? updatedDate;

        private int updatedBy;

        private int createdBy;

        private int currencyId;
        private int taxAuthorityId;


        //private bool? flagDeleted;

        //private bool? isSystem;

        //private int createdBy;

        //private int updatedBy;

        //private DateTime? createdDate;

        //private DateTime? updatedDate;
        #endregion

        #region Public Properties

        [PrimaryKey("@pos_customer_id", SqlDbType.Int)]
        [Property("pos_customer_id", SqlDbType.Int, AddParameter = false)]
        public int PosCustomerId
        {
            get
            {
                return posCustomerId;
            }
            set
            {
                posCustomerId = value;
            }
        }


        [Property("currency_id", SqlDbType.Int, AddParameter = true)]
        public int CurrencyId
        {
            get
            {
                return currencyId;
            }
            set
            {
                currencyId = value;
            }
        }


        [Property("tax_authority_id", SqlDbType.Int, AddParameter = true)]
        public int TaxAuthorityId
        {
            get
            {
                return taxAuthorityId;
            }
            set
            {
                taxAuthorityId = value;
            }
        }

        [Property("customer_id", SqlDbType.Int)]
        [PropertyConstraint(true, "Customer")]
        public int CustomerId
        {
            get
            {
                return customerId;
            }
            set
            {
                customerId = value;
            }
        }

        [Property("customer_number", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "CustomerNumber", MaxLength = 50, MinLength = 3)]
        public string CustomerNumber
        {
            get
            {
                return customerNumber;
            }
            set
            {
                customerNumber = value;
            }
        }

        [Property("customer_name", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "CustomerName", MaxLength = 60, MinLength = 3)]
        public string CustomerName
        {
            get
            {
                return customerName;
            }
            set
            {
                customerName = value;
            }
        }

        [Property("tax_class_id", SqlDbType.Int)]
        [PropertyConstraint(false, "TaxClass")]
        public int TaxClassId
        {
            get
            {
                return taxClassId;
            }
            set
            {
                taxClassId = value;
            }
        }

        [Property("credit_limit_amount", SqlDbType.Float)]
        [PropertyConstraint(false, "CreditLimitAmount")]
        public double? CreditLimitAmount
        {
            get
            {
                return creditLimitAmount;
            }
            set
            {
                creditLimitAmount = value;
            }
        }

        [Property("contact_email", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "ContactEmail", MaxLength = 24, MinLength = 3)]
        public string ContactEmail
        {
            get
            {
                return contactEmail;
            }
            set
            {
                contactEmail = value;
            }
        }

        [Property("contact_telephone", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "ContactTelephone", MaxLength = 24, MinLength = 3)]
        public string ContactTelephone
        {
            get
            {
                return contactTelephone;
            }
            set
            {
                contactTelephone = value;
            }
        }



        [Property("flag_deleted", SqlDbType.Bit, AddParameter = true, UpdateParameter = true, FindParameter = true)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [Property("is_system", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool? IsSystem
        {
            get
            {
                return isSystem;
            }
            set
            {
                isSystem = value;
            }
        }

        [Property("taxable", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool Taxable
        {
            get
            {
                return taxable;
            }
            set
            {
                taxable = value;
            }
        }

        [Property("created_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? CreatedDate
        {
            get
            {
                return createdDate;
            }
            set
            {
                createdDate = value;
            }
        }

        [Property("updated_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? UpdatedDate
        {
            get
            {
                return updatedDate;
            }
            set
            {
                updatedDate = value;
            }
        }

        [Property("created_by", SqlDbType.Int, AddParameter = true, UpdateParameter = false)]
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        [Property("updated_by", SqlDbType.Int, AddParameter = false, UpdateParameter = true)]
        public int UpdatedBy
        {
            get
            {
                return updatedBy;
            }
            set
            {
                updatedBy = value;
            }
        }
        #endregion

        #region Composite Objects


        #endregion


        public override int GetIdentity()
        {
            return posCustomerId;
        }
        public override void SetIdentity(int value)
        {
            posCustomerId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            throw new NotImplementedException();
        }
    }
}