using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace SagePOS.Server.Business.Entity
{
     public class POSCustomerLite : EntityBaseLite
    {      
        
        #region Public Properties

         		[LiteProperty("pos_customer_id",SqlDbType.Int)]
		public int PosCustomerId
		{
			get;
			set;
		}

		[LiteProperty("customer_id",SqlDbType.Int)]
		public int CustomerId
		{
			get;
			set;
		}

		[LiteProperty("customer_number",SqlDbType.NVarChar)]
		public string CustomerNumber
		{
			get;
			set;
		}

		[LiteProperty("customer_name",SqlDbType.NVarChar)]
		public string CustomerName
		{
			get;
			set;
		}

		[LiteProperty("tax_class_id",SqlDbType.Int)]
		public int TaxClassId
		{
			get;
			set;
		}

		[LiteProperty("credit_limit_amount",SqlDbType.Float)]
		public double? CreditLimitAmount
		{
			get;
			set;
		}

		[LiteProperty("contact_email",SqlDbType.NVarChar)]
		public string ContactEmail
		{
			get;
			set;
		}

		[LiteProperty("contact_telephone",SqlDbType.NVarChar)]
		public string ContactTelephone
		{
			get;
			set;
		}



        [LiteProperty("pos_customer_id", SqlDbType.Int)]
        public int value
        {
            get;
            set;
        }

          [LiteProperty("customer_name", SqlDbType.NVarChar)]
        public string label
        {
            get;
            set;
        }

         [LiteProperty("flag_deleted", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? FlagDeleted
        {
            get;
            set;
        }

         [LiteProperty("taxable", SqlDbType.Bit)]
         public bool Taxable
         {
             get;
             set;
         }

        [LiteProperty("is_system",SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
		public bool? IsSystem
		{
			get;
			set;
		}

		[LiteProperty("created_date",SqlDbType.DateTime)]
		public DateTime? CreatedDate
		{
			get;
			set;
		}

		[LiteProperty("updated_date",SqlDbType.DateTime)]
		public DateTime? UpdatedDate
		{
			get;
			set;
		}

		[LiteProperty("created_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int CreatedBy
		{
			get;
			set;
		}

		[LiteProperty("updated_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int UpdatedBy
		{
			get;
			set;
		}

         [LiteProperty("created_by_name",SqlDbType.NVarChar, FindAllLiteParameter = false)]
         public string CreatedByName{get;set;}

         [LiteProperty("updated_by_name",SqlDbType.NVarChar,FindAllLiteParameter = false)]
         public string UpdatedByName{get;set;}

         [LiteProperty("pos_customer_id",SqlDbType.Int)]
          public int Id
        {
            get{return this.PosCustomerId; }
            set{this.PosCustomerId=value;}
        }

         [LiteProperty("last_rows",SqlDbType.NVarChar)]
         public bool LastRows { get; set; }

         [LiteProperty("total_count",SqlDbType.NVarChar)]
         public int TotalRecords { get; set; }
        #endregion
       
          
     }
}