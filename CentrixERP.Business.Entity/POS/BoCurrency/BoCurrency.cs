using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;


namespace SagePOS.Server.Business.Entity
{
    [EntityIManager("SagePOS.Server.Business.IManager.IBoCurrencyManager,Centrix.Business.IManager", isLogicalDelete = true)]
    public class BoCurrency : EntityBase<BoCurrency, int>
    {
        #region Constructors

        public BoCurrency() { }

        public BoCurrency(int id)
        {
            this.currid = id;
        }

        #endregion
       
        #region Private Properties
        private int currid;
		private string currCode;

		private string currName;

		private string currSymbol;

		private bool? flagDeleted;

		private bool? isSystem;

		private int createdBy;

		private DateTime? createdDate;

		private int updatedBy;

		private DateTime? updatedDate;

	


        #endregion

        #region Public Properties


        [PrimaryKey("@curr_Id", SqlDbType.Int)]
        [Property("CurrId", SqlDbType.Int, AddParameter = false)]
        public int CurrId
        {
            get
            {
                return currid;
            }
            set
            {
                currid = value;
            }
        }

        [Property("curr_code", SqlDbType.NVarChar)]
[PropertyConstraint(true, "CurrCode", MaxLength = 3, MinLength = 3)]
		public string CurrCode
		{
			get
			{
				return currCode;
			}
			set
			{
				currCode = value;
			}
		}

[Property("curr_name", SqlDbType.NVarChar)]
[PropertyConstraint(false, "CurrName", MaxLength = 60, MinLength = 3)]
		public string CurrName
		{
			get
			{
				return currName;
			}
			set
			{
				currName = value;
			}
		}

[Property("curr_symbol", SqlDbType.NVarChar)]
[PropertyConstraint(false, "CurrSymbol", MaxLength = 4, MinLength = 3)]
		public string CurrSymbol
		{
			get
			{
				return currSymbol;
			}
			set
			{
				currSymbol = value;
			}
		}





        [Property("flag_deleted", SqlDbType.Bit, AddParameter = false, UpdateParameter = false,FindParameter=false)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [Property("is_system", SqlDbType.Bit, AddParameter = false, UpdateParameter = false,FindParameter=false)]
        public bool? IsSystem
        {
            get
            {
                return isSystem;
            }
            set
            {
                isSystem = value;
            }
        }

		[Property("created_date",SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
		public DateTime? CreatedDate
		{
			get
			{
				return createdDate;
			}
			set
			{
				createdDate = value;
			}
		}

        [Property("updated_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
		public DateTime? UpdatedDate
		{
			get
			{
				return updatedDate;
			}
			set
			{
				updatedDate = value;
			}
		}

		[Property("created_by",SqlDbType.Int, AddParameter = true, UpdateParameter = false)]
		public int CreatedBy
		{
			get
			{
				return createdBy;
			}
			set
			{
				createdBy = value;
			}
		}

		[Property("updated_by",SqlDbType.Int, AddParameter = false, UpdateParameter = true)]
		public int UpdatedBy
		{
			get
			{
				return updatedBy;
			}
			set
			{
				updatedBy = value;
			}
		}
        #endregion

             #region Composite Objects
              

                 #endregion
   

        public override int GetIdentity()
        {
            return currid;
        }
        public override void SetIdentity(int value)
        {
            currid = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            			throw new NotImplementedException();
        }
    }
}