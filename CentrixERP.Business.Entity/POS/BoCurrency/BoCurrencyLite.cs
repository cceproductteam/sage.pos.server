using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace SagePOS.Server.Business.Entity
{
     public class BoCurrencyLite : EntityBaseLite
    {      
        
        #region Public Properties


         [LiteProperty("curr_Id", SqlDbType.Int)]
         public int CurrId
         {
             get;
             set;
         }


         		[LiteProperty("curr_code",SqlDbType.NVarChar)]
		public string CurrCode
		{
			get;
			set;
		}

		[LiteProperty("curr_name",SqlDbType.NVarChar)]
		public string CurrName
		{
			get;
			set;
		}

		[LiteProperty("curr_symbol",SqlDbType.NVarChar)]
		public string CurrSymbol
		{
			get;
			set;
		}



       
        public int value
        {
            get{ return CurrId;}
        }

        
        public string label
        {
            get { return CurrCode; }
        }

         [LiteProperty("flag_deleted", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? FlagDeleted
        {
            get;
            set;
        }

        [LiteProperty("is_system",SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
		public bool? IsSystem
		{
			get;
			set;
		}

		[LiteProperty("created_date",SqlDbType.DateTime)]
		public DateTime? CreatedDate
		{
			get;
			set;
		}

		[LiteProperty("updated_date",SqlDbType.DateTime)]
		public DateTime? UpdatedDate
		{
			get;
			set;
		}

		[LiteProperty("created_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int CreatedBy
		{
			get;
			set;
		}

		[LiteProperty("updated_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int UpdatedBy
		{
			get;
			set;
		}

         [LiteProperty("created_by_name",SqlDbType.NVarChar, FindAllLiteParameter = false)]
         public string CreatedByName{get;set;}

         [LiteProperty("updated_by_name",SqlDbType.NVarChar,FindAllLiteParameter = false)]
         public string UpdatedByName{get;set;}

        
          public int Id
        {
            get{return this.CurrId; }
        }

        
        #endregion
       
          
     }
}