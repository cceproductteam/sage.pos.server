using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
namespace SagePOS.Server.Business.Entity
{
     public class BoLocationLite : EntityBaseLite
    {      
        
        #region Public Properties

         		[LiteProperty("location_code",SqlDbType.NVarChar)]
		public string LocationCode
		{
			get;
			set;
		}

		[LiteProperty("location_desc",SqlDbType.NVarChar)]
		public string LocationDesc
		{
			get;
			set;
		}

		[LiteProperty("id",SqlDbType.Int)]
		public int Id
		{
			get;
			set;
		}



        [LiteProperty("id", SqlDbType.Int)]
        public int value
        {
            get;
            set;
        }

          [LiteProperty("location_desc", SqlDbType.NVarChar)]
        public string label
        {
            get;
            set;
        }

         [LiteProperty("flag_deleted", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? FlagDeleted
        {
            get;
            set;
        }

        [LiteProperty("is_system",SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
		public bool? IsSystem
		{
			get;
			set;
		}

		[LiteProperty("created_date",SqlDbType.DateTime)]
		public DateTime? CreatedDate
		{
			get;
			set;
		}

		[LiteProperty("updated_date",SqlDbType.DateTime)]
		public DateTime? UpdatedDate
		{
			get;
			set;
		}

		[LiteProperty("created_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int CreatedBy
		{
			get;
			set;
		}

		[LiteProperty("updated_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int UpdatedBy
		{
			get;
			set;
		}

         [LiteProperty("created_by_name",SqlDbType.NVarChar, FindAllLiteParameter = false)]
         public string CreatedByName{get;set;}

         [LiteProperty("updated_by_name",SqlDbType.NVarChar,FindAllLiteParameter = false)]
         public string UpdatedByName{get;set;}

         [LiteProperty("id",SqlDbType.NVarChar)]
          public int locationId
        {
            get{return this.Id; }
            set{this.Id=value;}
        }

         [LiteProperty("last_rows",SqlDbType.NVarChar)]
         public bool LastRows { get; set; }

         [LiteProperty("total_count",SqlDbType.NVarChar)]
         public int TotalRecords { get; set; }
        #endregion
       
          
     }
}