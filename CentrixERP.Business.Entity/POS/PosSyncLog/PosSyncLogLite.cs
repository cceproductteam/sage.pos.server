using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace SagePOS.Server.Business.Entity
{
     public class PosSyncLogLite : EntityBaseLite
    {      
        
        #region Public Properties

         		[LiteProperty("sync_id",SqlDbType.Int)]
		public int SyncId
		{
			get;
			set;
		}

		[LiteProperty("store_id",SqlDbType.Int)]
		public int StoreId
		{
			get;
			set;
		}

		[LiteProperty("sync_date",SqlDbType.DateTime)]
		public DateTime? SyncDate
		{
			get;
			set;
		}

		[LiteProperty("status",SqlDbType.Bit)]
		public bool? Status
		{
			get;
			set;
		}

		[LiteProperty("sync_entity",SqlDbType.Int)]
		public int SyncEntity
		{
			get;
			set;
		}



        [LiteProperty("sync_id", SqlDbType.Int)]
        public int value
        {
            get;
            set;
        }

          [LiteProperty("sync_id", SqlDbType.Int)]
        public int label
        {
            get;
            set;
        }

         [LiteProperty("flag_deleted", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? FlagDeleted
        {
            get;
            set;
        }

        [LiteProperty("is_system",SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
		public bool? IsSystem
		{
			get;
			set;
		}

		[LiteProperty("created_date",SqlDbType.DateTime)]
		public DateTime? CreatedDate
		{
			get;
			set;
		}

		[LiteProperty("updated_date",SqlDbType.DateTime)]
		public DateTime? UpdatedDate
		{
			get;
			set;
		}

		[LiteProperty("created_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int CreatedBy
		{
			get;
			set;
		}

		[LiteProperty("updated_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int UpdatedBy
		{
			get;
			set;
		}

         [LiteProperty("created_by_name",SqlDbType.NVarChar, FindAllLiteParameter = false)]
         public string CreatedByName{get;set;}

         [LiteProperty("updated_by_name",SqlDbType.NVarChar,FindAllLiteParameter = false)]
         public string UpdatedByName{get;set;}

         [LiteProperty("sync_id",SqlDbType.NVarChar)]
          public int Id
        {
            get{return this.SyncId; }
            set{this.SyncId=value;}
        }

         [LiteProperty("last_rows",SqlDbType.NVarChar)]
         public bool LastRows { get; set; }

         [LiteProperty("total_count",SqlDbType.NVarChar)]
         public int TotalRecords { get; set; }

         [LiteProperty("entity_name", SqlDbType.NVarChar)]
         public string EntityName
         {
             get;
             set;
         }
         [LiteProperty("entity_name_ar", SqlDbType.NVarChar)]
         public string EntityNameAr
         {
             get;
             set;
         }
         [LiteProperty("message", SqlDbType.NVarChar)]
         public string Message
         {
             get;
             set;
         }
         [LiteProperty("store_name", SqlDbType.NVarChar)]
         public string StoreName
         {
             get;
             set;
         }
         [LiteProperty("store_name_ar", SqlDbType.NVarChar)]
         public string StoreNameAr
         {
             get;
             set;
         }
        #endregion
       
          
     }
}