using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
using SagePOS.Server.Business.Factory ;

namespace SagePOS.Server.Business.Entity
{
    public class PosSyncLogCriteria : CriteriaBase, IAdvancedSearchCriteria
    {
        [CrtiteriaField("@page_number", SqlDbType.Int)]
        public int? pageNumber { get; set; }

        [CrtiteriaField("@result_count", SqlDbType.Int)]
        public int? resultCount { get; set; }

        [CrtiteriaField("@keyword", SqlDbType.NVarChar)]
        public string Keyword { get; set; }

        [CrtiteriaField("@search_criteria", SqlDbType.NVarChar)]
        public string SearchCriteria { get; set; }

        [CrtiteriaField("@sort_type", SqlDbType.Bit)]
        public bool? SortType { get; set; }

        [CrtiteriaField("@sort_fields", SqlDbType.NVarChar)]
        public string SortFields { get; set; }

        [CrtiteriaField("@sync_id", SqlDbType.Int)]
        public int? SyncId { get; set; }

        [CrtiteriaField("@sync_id", SqlDbType.Int)]
        public int? EntityId { get; set; }

        [CrtiteriaField("@store_id", SqlDbType.Int)]
        public int? StoreId { get; set; }

        [CrtiteriaField("@entity_id", SqlDbType.Int)]
        public int? SyncEntityID { get; set; }

        [CrtiteriaField("@store_ids", SqlDbType.NVarChar)]
        public string StoreIds { get; set; }

        [CrtiteriaField("@entity_ids", SqlDbType.NVarChar)]
        public string SyncEntityIDs { get; set; }

        [CrtiteriaField("@date_from", SqlDbType.DateTime)]
        public string DateFrom { get; set; }

        [CrtiteriaField("@date_to", SqlDbType.DateTime)]
        public string DateTo { get; set; }

        [CrtiteriaField("@status", SqlDbType.Bit)]
        public bool? Status { get; set; }

        [CrtiteriaField("@message", SqlDbType.NVarChar)]
        public string Message { get; set; }

        int IAdvancedSearchCriteria.EntityId
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        bool IAdvancedSearchCriteria.SortType
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        int IAdvancedSearchCriteria.pageNumber
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        int IAdvancedSearchCriteria.resultCount
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

    }
}
