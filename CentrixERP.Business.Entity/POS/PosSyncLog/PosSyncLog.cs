using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
using SagePOS.Manger.Common.Business.Entity;

namespace SagePOS.Server.Business.Entity
{
    [EntityIManager("SagePOS.Server.Business.IManager.IPosSyncLogManager,SagePOS.Server.Business.IManager", isLogicalDelete = true)]
    public class PosSyncLog : EntityBase<PosSyncLog, int>
    {
        #region Constructors

        public PosSyncLog() { }

        public PosSyncLog(int syncId)
        {
            this.syncId = syncId;
        }

        #endregion

        #region Private Properties

        private int syncId;

        private int storeId;

        private DateTime? syncDate;

        private bool? status;

        private int syncEntity;

        private int createdBy;

        private DateTime? createdDate;

        private bool? flagDeleted;

        private bool? isSystem;

        private string message;

        private int updatedBy;



        private DateTime? updatedDate;
        #endregion

        #region Public Properties

        [PrimaryKey("@sync_id", SqlDbType.Int)]
        [Property("sync_id", SqlDbType.Int, AddParameter = false)]
        public int SyncId
        {
            get
            {
                return syncId;
            }
            set
            {
                syncId = value;
            }
        }

        [Property("store_id", SqlDbType.Int)]
        [PropertyConstraint(false, "Store")]
        public int StoreId
        {
            get
            {
                return storeId;
            }
            set
            {
                storeId = value;
            }
        }
        [Property("message", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "Message")]
        public string Message
        {
            get
            {
                return message;
            }
            set
            {
                message = value;
            }
        }

        [Property("sync_date", SqlDbType.DateTime)]
        [PropertyConstraint(false, "SyncDate")]
        public DateTime? SyncDate
        {
            get
            {
                return syncDate;
            }
            set
            {
                syncDate = value;
            }
        }

        [Property("status", SqlDbType.Bit)]
        [PropertyConstraint(false, "Status")]
        public bool? Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }

        [Property("sync_entity", SqlDbType.Int)]
        [PropertyConstraint(false, "SyncEntity")]
        public int SyncEntity
        {
            get
            {
                return syncEntity;
            }
            set
            {
                syncEntity = value;
            }
        }



        [Property("flag_deleted", SqlDbType.Bit, AddParameter = true, UpdateParameter = true, FindParameter = true)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [Property("is_system", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool? IsSystem
        {
            get
            {
                return isSystem;
            }
            set
            {
                isSystem = value;
            }
        }

        [Property("created_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? CreatedDate
        {
            get
            {
                return createdDate;
            }
            set
            {
                createdDate = value;
            }
        }

        [Property("updated_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? UpdatedDate
        {
            get
            {
                return updatedDate;
            }
            set
            {
                updatedDate = value;
            }
        }

        [Property("created_by", SqlDbType.Int, AddParameter = true, UpdateParameter = false)]
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        [Property("updated_by", SqlDbType.Int, AddParameter = false, UpdateParameter = true)]
        public int UpdatedBy
        {
            get
            {
                return updatedBy;
            }
            set
            {
                updatedBy = value;
            }
        }
        #endregion

        #region Composite Objects


        #endregion


        public override int GetIdentity()
        {
            return syncId;
        }
        public override void SetIdentity(int value)
        {
            syncId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            throw new NotImplementedException();
        }
    }
}