using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
using SagePOS.Manger.Common.Business.Entity;

namespace SagePOS.Server.Business.Entity
{
    [EntityIManager("SagePOS.Server.Business.IManager.IPosBankAccountManager,SagePOS.Server.Business.IManager", isLogicalDelete = true)]
    public class PosBankAccount : EntityBase<PosBankAccount, int>
    {
        #region Constructors

        public PosBankAccount() { }

        public PosBankAccount(int posBankAccountId)
        {
            this.posBankAccountId = posBankAccountId;
        }

        #endregion

        #region Private Properties

		private int posBankAccountId;

		private int bankId;

		private string bankCode;

		private string description;

		private DateTime? createdDate;

		private DateTime? updatedDate;

		private int createdBy;

		private int updatedBy;

		private bool? flagDeleted;

		private bool? isSystem;



        //private bool? flagDeleted;

        //private bool? isSystem;

        //private int createdBy;

        //private int updatedBy;

        //private DateTime? createdDate;

        //private DateTime? updatedDate;
        #endregion

        #region Public Properties

        [PrimaryKey("@pos_bank_account_id", SqlDbType.Int)]
[Property("bank_id", SqlDbType.Int, AddParameter = false)]
		public int PosBankAccountId
		{
			get
			{
				return posBankAccountId;
			}
			set
			{
				posBankAccountId = value;
			}
		}

[Property("bank_id", SqlDbType.Int)]
[PropertyConstraint(false, "Bank")]
		public int BankId
		{
			get
			{
				return bankId;
			}
			set
			{
				bankId = value;
			}
		}

[Property("bank_code", SqlDbType.NVarChar)]
[PropertyConstraint(false, "BankCode", MaxLength = 50, MinLength = 3)]
		public string BankCode
		{
			get
			{
				return bankCode;
			}
			set
			{
				bankCode = value;
			}
		}

[Property("description", SqlDbType.NVarChar)]
[PropertyConstraint(false, "Description", MaxLength = 100, MinLength = 3)]
		public string Description
		{
			get
			{
				return description;
			}
			set
			{
				description = value;
			}
		}



        [Property("flag_deleted", SqlDbType.Bit, AddParameter = true, UpdateParameter = true, FindParameter = true)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [Property("is_system", SqlDbType.Bit, AddParameter = false, UpdateParameter = false,FindParameter=false)]
        public bool? IsSystem
        {
            get
            {
                return isSystem;
            }
            set
            {
                isSystem = value;
            }
        }

		[Property("created_date",SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
		public DateTime? CreatedDate
		{
			get
			{
				return createdDate;
			}
			set
			{
				createdDate = value;
			}
		}

        [Property("updated_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
		public DateTime? UpdatedDate
		{
			get
			{
				return updatedDate;
			}
			set
			{
				updatedDate = value;
			}
		}

		[Property("created_by",SqlDbType.Int, AddParameter = true, UpdateParameter = false)]
		public int CreatedBy
		{
			get
			{
				return createdBy;
			}
			set
			{
				createdBy = value;
			}
		}

		[Property("updated_by",SqlDbType.Int, AddParameter = false, UpdateParameter = true)]
		public int UpdatedBy
		{
			get
			{
				return updatedBy;
			}
			set
			{
				updatedBy = value;
			}
		}
        #endregion

             #region Composite Objects
              

                 #endregion
   

        public override int GetIdentity()
        {
            return posBankAccountId;
        }
        public override void SetIdentity(int value)
        {
            posBankAccountId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            			throw new NotImplementedException();
        }
    }
}