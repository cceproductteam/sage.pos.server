using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;


namespace SagePOS.Server.Business.Entity
{
    [EntityIManager("SagePOS.Server.Business.IManager.IBoBankManager,SagePOS.Server.Business.IManager", isLogicalDelete = true)]
    public class BoBank : EntityBase<BoBank, int>
    {
        #region Constructors

        public BoBank() { }

        public BoBank(int id)
        {
            this.id = id;
        }

        #endregion

        #region Private Properties

		private string bankBankid;

		private string bankBankname;

		private int createdBy;

		private DateTime? createdDate;

		private int updatedBy;

		private DateTime? updatedDate;

		private bool? flagDeleted;

		private bool? isSystem;

		private int id;



        //private bool? flagDeleted;

        //private bool? isSystem;

        //private int createdBy;

        //private int updatedBy;

        //private DateTime? createdDate;

        //private DateTime? updatedDate;
        #endregion

        #region Public Properties

        [Property("bank_bankid", SqlDbType.Char)]
[PropertyConstraint(false, "BankBankid", MaxLength = 8, MinLength = 3)]
		public string BankBankid
		{
			get
			{
				return bankBankid;
			}
			set
			{
				bankBankid = value;
			}
		}

[Property("bank_bankname", SqlDbType.Char)]
[PropertyConstraint(false, "BankBankname", MaxLength = 60, MinLength = 3)]
		public string BankBankname
		{
			get
			{
				return bankBankname;
			}
			set
			{
				bankBankname = value;
			}
		}

[PrimaryKey("@id", SqlDbType.Int)]
[Property("id", SqlDbType.Int, AddParameter = false)]
		public int Id
		{
			get
			{
				return id;
			}
			set
			{
				id = value;
			}
		}



        [Property("flag_deleted", SqlDbType.Bit, AddParameter = false, UpdateParameter = false,FindParameter=false)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [Property("is_system", SqlDbType.Bit, AddParameter = false, UpdateParameter = false,FindParameter=false)]
        public bool? IsSystem
        {
            get
            {
                return isSystem;
            }
            set
            {
                isSystem = value;
            }
        }

		[Property("created_date",SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
		public DateTime? CreatedDate
		{
			get
			{
				return createdDate;
			}
			set
			{
				createdDate = value;
			}
		}

        [Property("updated_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
		public DateTime? UpdatedDate
		{
			get
			{
				return updatedDate;
			}
			set
			{
				updatedDate = value;
			}
		}

		[Property("created_by",SqlDbType.Int, AddParameter = true, UpdateParameter = false)]
		public int CreatedBy
		{
			get
			{
				return createdBy;
			}
			set
			{
				createdBy = value;
			}
		}

		[Property("updated_by",SqlDbType.Int, AddParameter = false, UpdateParameter = true)]
		public int UpdatedBy
		{
			get
			{
				return updatedBy;
			}
			set
			{
				updatedBy = value;
			}
		}
        #endregion

             #region Composite Objects
              

                 #endregion
   

        public override int GetIdentity()
        {
            return id;
        }
        public override void SetIdentity(int value)
        {
            id = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            			throw new NotImplementedException();
        }
    }
}