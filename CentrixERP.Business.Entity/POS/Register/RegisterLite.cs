using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace SagePOS.Server.Business.Entity
{
    public class RegisterLite : EntityBaseLite
    {

        #region Public Properties
        [LiteProperty("invoice_count", SqlDbType.Int)]
        public int InvoiceCount
        {
            get;
            set;
        }
        [LiteProperty("register_id", SqlDbType.Int)]
        public int RegisterId
        {
            get;
            set;
        }

        [LiteProperty("register_name", SqlDbType.NVarChar)]
        public string RegisterName
        {
            get;
            set;
        }

        [LiteProperty("register_name_ar", SqlDbType.NVarChar)]
        public string RegisterNameAr
        {
            get;
            set;
        }

        [LiteProperty("regiser_code", SqlDbType.NVarChar)]
        public string RegiserCode
        {
            get;
            set;
        }

        [LiteProperty("store_id", SqlDbType.Int)]
        public int StoreId
        {
            get;
            set;
        }

        [LiteProperty("store", SqlDbType.NVarChar)]
        public string Store
        {
            get;
            set;
        }

        [LiteProperty("cash_loan", SqlDbType.Money)]
        public decimal? CashLoan
        {
            get;
            set;
        }

        [LiteProperty("quick_keys_id", SqlDbType.Int)]
        public int QuickKeysId
        {
            get;
            set;
        }

        [LiteProperty("quick_keys", SqlDbType.NVarChar)]
        public string QuickKeys
        {
            get;
            set;
        }

        [LiteProperty("ip_address", SqlDbType.NVarChar)]
        public string IpAddress
        {
            get;
            set;
        }

        [LiteProperty("mac_address", SqlDbType.NVarChar)]
        public string MacAddress
        {
            get;
            set;
        }

        [LiteProperty("data_base_instance_name", SqlDbType.NVarChar)]
        public string DataBaseInstanceName
        {
            get;
            set;
        }

        [LiteProperty("data_base_name", SqlDbType.NVarChar)]
        public string DataBaseName
        {
            get;
            set;
        }

        [LiteProperty("data_base_user_name", SqlDbType.NVarChar)]
        public string DataBaseUserName
        {
            get;
            set;
        }

        [LiteProperty("data_base_password", SqlDbType.NVarChar)]
        public string DataBasePassword
        {
            get;
            set;
        }

        [LiteProperty("branch_id", SqlDbType.Int)]
        public int BranchId
        {
            get;
            set;
        }

        [LiteProperty("invoice_number_format", SqlDbType.NVarChar)]
        public string InvoiceNumberFormat
        {
            get;
            set;
        }

        [LiteProperty("layby_number_format", SqlDbType.NVarChar)]
        public string LaybyNumberFormat
        {
            get;
            set;
        }

        [LiteProperty("print_recipt", SqlDbType.Bit)]
        public bool? PrintRecipt
        {
            get;
            set;
        }

        [LiteProperty("email_recipt", SqlDbType.Bit)]
        public bool? EmailRecipt
        {
            get;
            set;
        }

        [LiteProperty("is_used", SqlDbType.Bit)]
        public bool? IsUsed
        {
            get;
            set;
        }

        //[LiteProperty("is_new", SqlDbType.Bit)]
        //public bool? IsNew
        //{
        //    get;
        //    set;
        //}



        [LiteProperty("register_id", SqlDbType.Int)]
        public int value
        {
            get;
            set;
        }

        [LiteProperty("register_name", SqlDbType.NVarChar)]
        public string label
        {
            get;
            set;
        }

        [LiteProperty("flag_deleted", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? FlagDeleted
        {
            get;
            set;
        }

        [LiteProperty("is_system", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? IsSystem
        {
            get;
            set;
        }

        [LiteProperty("created_date", SqlDbType.DateTime)]
        public DateTime? CreatedDate
        {
            get;
            set;
        }

        [LiteProperty("updated_date", SqlDbType.DateTime)]
        public DateTime? UpdatedDate
        {
            get;
            set;
        }

        [LiteProperty("created_by", SqlDbType.Int, FindByIdLiteParameter = false)]
        public int CreatedBy
        {
            get;
            set;
        }

        [LiteProperty("updated_by", SqlDbType.Int, FindByIdLiteParameter = false)]
        public int UpdatedBy
        {
            get;
            set;
        }

        [LiteProperty("created_by_name", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string CreatedByName { get; set; }

        [LiteProperty("updated_by_name", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string UpdatedByName { get; set; }

        [LiteProperty("register_id", SqlDbType.Int)]
        public int Id
        {
            get { return this.RegisterId; }
            set { this.RegisterId = value; }
        }

        [LiteProperty("last_rows", SqlDbType.NVarChar)]
        public bool LastRows { get; set; }

        [LiteProperty("total_count", SqlDbType.NVarChar)]
        public int TotalRecords { get; set; }
        #endregion


    }
}