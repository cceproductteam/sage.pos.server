using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace SagePOS.Server.Business.Entity
{
     public class PosSystemConfigrationLite : EntityBaseLite
    {      
        
        #region Public Properties

         		[LiteProperty("pos_config_id",SqlDbType.Int)]
		public int PosConfigId
		{
			get;
			set;
		}

		[LiteProperty("gift_voucher_over_amount_id",SqlDbType.Int)]
		public int GiftVoucherOverAmountId
		{
			get;
			set;
		}

         private string giftVoucherOverAmount;

		private string giftVoucherOverAmountAr;

		[LiteProperty("gift_voucher_over_amount",SqlDbType.NVarChar)]
		public string GiftVoucherOverAmount
		{
            get { return SagePOS.Server.Configuration.Configuration.Lang == SagePOS.Server.Configuration.Enums_S3.Configuration.Language.ar ? giftVoucherOverAmountAr : giftVoucherOverAmount; }
            
			set{ giftVoucherOverAmount= value; }
		}

		[LiteProperty("gift_voucher_over_amount_ar",SqlDbType.NVarChar)]
		public string GiftVoucherOverAmountAr
		{
			get{return giftVoucherOverAmountAr;}
            
			set{ giftVoucherOverAmountAr= value; }
		}

        //[LiteProperty("gift_voucher_over_amount", SqlDbType.NVarChar)]
        //public string GiftVoucherOverAmount
        //{
        //    get;
        //    set;
        //}
        //   [LiteProperty("gift_voucher_over_amount_ar", SqlDbType.NVarChar)]
        //public string GiftVoucherOverAmountAr
        //{
        //    get;
        //    set;
        //}

        [LiteProperty("discount_account_id", SqlDbType.Int)]
        public int DiscountAccountId
        {
            get;
            set;
        }

        [LiteProperty("is_post_invoice", SqlDbType.Bit  )]
        public bool IsInvoicesPosted
        {
            get;
            set;
        }

        [LiteProperty("is_post_refund", SqlDbType.Bit)]
        public bool IsRefundPosted
        {
            get;
            set;
        }

        [LiteProperty("is_post_exchange", SqlDbType.Bit)]
        public bool IsExchangePosted
        {
            get;
            set;
        }

        [LiteProperty("is_post_receipts", SqlDbType.Bit)]
        public bool IsReceiptsPosted
        {
            get;
            set;
        }





        private string discountAccountEn;

        private string discountAccountAr;

        [LiteProperty("discount_account_en", SqlDbType.NVarChar)]
        public string DiscountAccountEn
        {
            get { return discountAccountEn; }

            set { discountAccountEn = value; }
        }

        [LiteProperty("discount_account_ar", SqlDbType.NVarChar)]
        public string DiscountAccountAr
        {
            get { return discountAccountAr; }

            set { discountAccountAr = value; }
        }

        public string DiscountAccount
        {
            get { return SagePOS.Server.Configuration.Configuration.Lang == SagePOS.Server.Configuration.Enums_S3.Configuration.Language.ar ? discountAccountAr : discountAccountEn; }

          
        }



        [LiteProperty("exchange_account_id", SqlDbType.Int)]
        public int ExchangeAccountId
        {
            get;
            set;
        }




        private string exchangeAccountEn;

        private string exchangeAccountAr;

        [LiteProperty("exchange_account_en", SqlDbType.NVarChar)]
        public string ExchangeAccountEn
        {
            get { return exchangeAccountEn; }

            set { exchangeAccountEn = value; }
        }

        [LiteProperty("exchange_account_ar", SqlDbType.NVarChar)]
        public string ExchangeAccountAr
        {
            get { return exchangeAccountAr; }

            set { exchangeAccountAr = value; }
        }

        public string ExchangeAccount
        {
            get { return SagePOS.Server.Configuration.Configuration.Lang == SagePOS.Server.Configuration.Enums_S3.Configuration.Language.ar ? exchangeAccountAr : exchangeAccountEn; }


        }

        [LiteProperty("layby_min_percentage", SqlDbType.Money)]
        public decimal? LaybyMinPercentage
        {
            get;
            set;
        }

		[LiteProperty("login_method_id",SqlDbType.Int)]
		public int LoginMethodId
		{
			get;
			set;
		}

		private string loginMethod;

		private string loginMethodAr;

		[LiteProperty("login_method",SqlDbType.NVarChar)]
		public string LoginMethod
		{
			get{return loginMethod;}
            
			set{ loginMethod= value; }
		}

		[LiteProperty("login_method_ar",SqlDbType.NVarChar)]
		public string LoginMethodAr
		{
			get{return loginMethodAr;}
            
			set{ loginMethodAr= value; }
		}

		[LiteProperty("after_close_register_value_id",SqlDbType.Int)]
		public int AfterCloseRegisterValueId
		{
			get;
			set;
		}

		private string afterCloseRegisterValue;

		private string afterCloseRegisterValueAr;

		[LiteProperty("after_close_register_value",SqlDbType.NVarChar)]
		public string AfterCloseRegisterValue
		{
			get{return  afterCloseRegisterValue;}
            
			set{ afterCloseRegisterValue= value; }
		}

		[LiteProperty("after_close_register_value_ar",SqlDbType.NVarChar)]
		public string AfterCloseRegisterValueAr
		{
			get{return afterCloseRegisterValueAr;}
            
			set{ afterCloseRegisterValueAr= value; }
		}

		[LiteProperty("invoice_number_format",SqlDbType.NVarChar)]
		public string InvoiceNumberFormat
		{
			get;
			set;
		}

		[LiteProperty("layby_number_format",SqlDbType.NVarChar)]
		public string LaybyNumberFormat
		{
			get;
			set;
		}

		[LiteProperty("cash_customer_id",SqlDbType.NVarChar)]
		public string CashCustomerId
		{
			get;
			set;
		}

        [LiteProperty("cash_customer", SqlDbType.NVarChar)]
        public string CashCustomer
        {
            get;
            set;
        }
		[LiteProperty("accpac_staging_ip",SqlDbType.NVarChar)]
		public string AccpacStagingIp
		{
			get;
			set;
		}

		[LiteProperty("pos_server_ip",SqlDbType.NVarChar)]
		public string PosServerIp
		{
			get;
			set;
		}

		[LiteProperty("accpac_data_base_username",SqlDbType.NVarChar)]
		public string AccpacDataBaseUsername
		{
			get;
			set;
		}

		[LiteProperty("accpac_data_base_password",SqlDbType.NVarChar)]
		public string AccpacDataBasePassword
		{
			get;
			set;
		}


        [LiteProperty("accpac_database_instance", SqlDbType.NVarChar)]
        public string AccpacDataBaseInstance

        {

            get;
            set;
        }

        [LiteProperty("accpac_company_name", SqlDbType.NVarChar)]
        public string AccpacCompanyName
        {
            get;
            set;
        }
        [LiteProperty("accpac_username", SqlDbType.NVarChar)]
        public string AccpacUserName
        {
            get;
            set;
        }

		[LiteProperty("pos_server_data_base_username",SqlDbType.NVarChar)]
		public string PosServerDataBaseUsername
		{
			get;
			set;
		}

		[LiteProperty("pos_server_data_base_password",SqlDbType.NVarChar)]
		public string PosServerDataBasePassword
		{
			get;
			set;
		}

		[LiteProperty("pos_server_data_base_name",SqlDbType.NVarChar)]
		public string PosServerDataBaseName
		{
			get;
			set;
		}

		[LiteProperty("accpac_data_base_name",SqlDbType.NVarChar)]
		public string AccpacDataBaseName
		{
			get;
			set;
		}


        [LiteProperty("accpac_User_Password", SqlDbType.NVarChar)]
        public string AccpacUserPassword
        {
            get;
            set;
        }

        [LiteProperty("accpac_server_ip", SqlDbType.NVarChar)]
        public string AccpacServerIp
        {
            get;
            set;
        }

        [LiteProperty("pos_config_id", SqlDbType.Int)]
        public int value
        {
            get;
            set;
        }
         [LiteProperty("flag_deleted", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? FlagDeleted
        {
            get;
            set;
        }

        [LiteProperty("is_sysetm", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
		public bool? IsSystem
		{
			get;
			set;
		}

		[LiteProperty("created_date",SqlDbType.DateTime)]
		public DateTime? CreatedDate
		{
			get;
			set;
		}

		[LiteProperty("updated_date",SqlDbType.DateTime)]
		public DateTime? UpdatedDate
		{
			get;
			set;
		}

		[LiteProperty("created_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int CreatedBy
		{
			get;
			set;
		}

		[LiteProperty("updated_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int UpdatedBy
		{
			get;
			set;
		}

         [LiteProperty("created_by_name",SqlDbType.NVarChar, FindAllLiteParameter = true)]
         public string CreatedByName{get;set;}

         [LiteProperty("updated_by_name",SqlDbType.NVarChar,FindAllLiteParameter = true)]
         public string UpdatedByName{get;set;}

         [LiteProperty("pos_config_id",SqlDbType.Int)]
          public int Id
        {
            get{return this.PosConfigId; }
            set{this.PosConfigId=value;}
        }

         [LiteProperty("last_rows",SqlDbType.NVarChar)]
         public bool LastRows { get; set; }

         [LiteProperty("total_count",SqlDbType.NVarChar)]
         public int TotalRecords { get; set; }

         private string addingItemStructure;
         private string addingItemStructureAr;

         [LiteProperty("adding_item_structure_id", SqlDbType.Int)]
         public int AddingItemStructureId
         {
             get;

             set;
         }

         [LiteProperty("adding_item_structure", SqlDbType.NVarChar)]
         public string AddingItemStructure   
         {
             get { return addingItemStructure; }

             set { addingItemStructure = value; }
         }


         

         [LiteProperty("adding_item_structure_ar", SqlDbType.NVarChar)]
         public string AddingItemStructureAr
         {
             get { return addingItemStructureAr; }

             set { addingItemStructureAr = value; }
         }

         [LiteProperty("taxable_class_id", SqlDbType.Int)]

         public int TaxableClassNameId
         {
             get;
             set;
         }

         [LiteProperty("non_taxable_class_id", SqlDbType.Int)]

         public int NonTaxableClassNameId
         {
             get;
             set;
         }

         [LiteProperty("taxable_class_name", SqlDbType.NVarChar)]

         public string TaxableClassName
         {
             get;
             set;
         }

         [LiteProperty("non_taxable_class_name", SqlDbType.NVarChar)]

         public string NonTaxableClassName
         {
             get;
             set;
         }

         [LiteProperty("tax_Authority_id", SqlDbType.Int)]

         public int TaxAuthorityId
         {
             get;
             set;
         }

         [LiteProperty("tax_Authority", SqlDbType.NVarChar)]

         public string TaxAuthority
         {
             get;
             set;
         }
        
        #endregion
       
          
     }
}