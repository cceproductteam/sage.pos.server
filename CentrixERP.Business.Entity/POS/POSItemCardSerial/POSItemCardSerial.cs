using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
using SagePOS.Manger.Common.Business.Entity;

namespace SagePOS.Server.Business.Entity
{
    [EntityIManager("SagePOS.Server.Business.IManager.IIcItemCardSerialManager,SagePOS.Server.Business.IManager", isLogicalDelete = true)]
    public class POSItemCardSerial : EntityBase<POSItemCardSerial, int>
    {
        #region Constructors

        public POSItemCardSerial() { }

        public POSItemCardSerial(int itemCardSerialId)
        {
            this.itemCardSerialId = itemCardSerialId;
        }

        #endregion

        #region Private Properties

		private int itemCardSerialId;

		private string itemSerialNumber;

		private int itemCardId;

		private int locationId;

		private int status;

		private int receiptDetailId;

		private int transferDetailId;

		private int createdBy;

		private DateTime? createdDate;

		private int updatedBy;

		private DateTime? updatedDate;

		private bool? flagDeleted;

		private bool? isSystem;

		private DateTime? stockDate;


        #endregion

        #region Public Properties

        [PrimaryKey("@item_card_serial_id", SqlDbType.Int)]
[Property("item_card_serial_id", SqlDbType.Int, AddParameter = false)]
		public int ItemCardSerialId
		{
			get
			{
				return itemCardSerialId;
			}
			set
			{
				itemCardSerialId = value;
			}
		}

[Property("item_serial_number", SqlDbType.NVarChar)]
[PropertyConstraint(true, "ItemSerialNumber", MaxLength = 50, MinLength = 3)]
		public string ItemSerialNumber
		{
			get
			{
				return itemSerialNumber;
			}
			set
			{
				itemSerialNumber = value;
			}
		}

[Property("item_card_id", SqlDbType.Int)]
[PropertyConstraint(true, "ItemCard")]
		public int ItemCardId
		{
			get
			{
				return itemCardId;
			}
			set
			{
				itemCardId = value;
			}
		}

[Property("location_id", SqlDbType.Int)]
[PropertyConstraint(true, "Location")]
		public int LocationId
		{
			get
			{
				return locationId;
			}
			set
			{
				locationId = value;
			}
		}

[Property("status", SqlDbType.Int)]
[PropertyConstraint(true, "Status")]
		public int Status
		{
			get
			{
				return status;
			}
			set
			{
				status = value;
			}
		}

[Property("receipt_detail_id", SqlDbType.Int)]
[PropertyConstraint(false, "ReceiptDetail")]
		public int ReceiptDetailId
		{
			get
			{
				return receiptDetailId;
			}
			set
			{
				receiptDetailId = value;
			}
		}

[Property("transfer_detail_id", SqlDbType.Int)]
[PropertyConstraint(false, "TransferDetail")]
		public int TransferDetailId
		{
			get
			{
				return transferDetailId;
			}
			set
			{
				transferDetailId = value;
			}
		}

[Property("stock_date", SqlDbType.DateTime)]
[PropertyConstraint(false, "StockDate")]
		public DateTime? StockDate
		{
			get
			{
				return stockDate;
			}
			set
			{
				stockDate = value;
			}
		}



        [Property("flag_deleted", SqlDbType.Bit, AddParameter = true, UpdateParameter = true,FindParameter=true)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [Property("is_system", SqlDbType.Bit, AddParameter = true, UpdateParameter = true,FindParameter=true)]
        public bool? IsSystem
        {
            get
            {
                return isSystem;
            }
            set
            {
                isSystem = value;
            }
        }

		[Property("created_date",SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
		public DateTime? CreatedDate
		{
			get
			{
				return createdDate;
			}
			set
			{
				createdDate = value;
			}
		}

        [Property("updated_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
		public DateTime? UpdatedDate
		{
			get
			{
				return updatedDate;
			}
			set
			{
				updatedDate = value;
			}
		}

		[Property("created_by",SqlDbType.Int, AddParameter = true, UpdateParameter = false)]
		public int CreatedBy
		{
			get
			{
				return createdBy;
			}
			set
			{
				createdBy = value;
			}
		}

		[Property("updated_by",SqlDbType.Int, AddParameter = false, UpdateParameter = true)]
		public int UpdatedBy
		{
			get
			{
				return updatedBy;
			}
			set
			{
				updatedBy = value;
			}
		}
        #endregion

             #region Composite Objects
              

                 #endregion
   

        public override int GetIdentity()
        {
            return itemCardSerialId;
        }
        public override void SetIdentity(int value)
        {
            itemCardSerialId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            			throw new NotImplementedException();
        }
    }
}