using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
using SagePOS.Manger.Common.Business.Entity;

namespace SagePOS.Server.Business.Entity
{
    [EntityIManager("SagePOS.Server.Business.IManager.IIcSyncManager,SagePOS.Server.Business.IManager", isLogicalDelete = true)]
    public class IcSync : EntityBase<IcSync, int>
    {
        #region Constructors

        public IcSync() { }

        public IcSync(int syncId)
        {
            this.syncId = syncId;
        }

        #endregion

        #region Private Properties

		private int syncId;

		private int entityId;

		private int status;

		private DateTime? lastSyncDate;

		private string exciption;

		private bool? flagDeleted;

		private bool? isSystem;

		private DateTime? createdDate;

		private DateTime? updatedDate;

		private int updatedBy;

		private int createdBy;



        //private bool? flagDeleted;

        //private bool? isSystem;

        //private int createdBy;

        //private int updatedBy;

        //private DateTime? createdDate;

        //private DateTime? updatedDate;
        #endregion

        #region Public Properties

        [PrimaryKey("@sync_id", SqlDbType.Int)]
[Property("sync_id", SqlDbType.Int, AddParameter = false)]
		public int SyncId
		{
			get
			{
				return syncId;
			}
			set
			{
				syncId = value;
			}
		}

[Property("entity_id", SqlDbType.Int)]
[PropertyConstraint(false, "Entity")]
		public int EntityId
		{
			get
			{
				return entityId;
			}
			set
			{
				entityId = value;
			}
		}

[Property("status", SqlDbType.Int)]
[PropertyConstraint(false, "Status")]
		public int Status
		{
			get
			{
				return status;
			}
			set
			{
				status = value;
			}
		}

[Property("last_sync_date", SqlDbType.DateTime)]
[PropertyConstraint(false, "LastSyncDate")]
		public DateTime? LastSyncDate
		{
			get
			{
				return lastSyncDate;
			}
			set
			{
				lastSyncDate = value;
			}
		}

[Property("exciption", SqlDbType.NVarChar)]
[PropertyConstraint(false, "Exciption", MaxLength = 4000, MinLength = 3)]
		public string Exciption
		{
			get
			{
				return exciption;
			}
			set
			{
				exciption = value;
			}
		}



        [Property("flag_deleted", SqlDbType.Bit, AddParameter = true, UpdateParameter = true, FindParameter = true)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [Property("is_system", SqlDbType.Bit, AddParameter = false, UpdateParameter = false,FindParameter=false)]
        public bool? IsSystem
        {
            get
            {
                return isSystem;
            }
            set
            {
                isSystem = value;
            }
        }

		[Property("created_date",SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
		public DateTime? CreatedDate
		{
			get
			{
				return createdDate;
			}
			set
			{
				createdDate = value;
			}
		}

        [Property("updated_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
		public DateTime? UpdatedDate
		{
			get
			{
				return updatedDate;
			}
			set
			{
				updatedDate = value;
			}
		}

		[Property("created_by",SqlDbType.Int, AddParameter = true, UpdateParameter = false)]
		public int CreatedBy
		{
			get
			{
				return createdBy;
			}
			set
			{
				createdBy = value;
			}
		}

		[Property("updated_by",SqlDbType.Int, AddParameter = false, UpdateParameter = true)]
		public int UpdatedBy
		{
			get
			{
				return updatedBy;
			}
			set
			{
				updatedBy = value;
			}
		}
        #endregion

             #region Composite Objects
              

                 #endregion
   

        public override int GetIdentity()
        {
            return syncId;
        }
        public override void SetIdentity(int value)
        {
            syncId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            			throw new NotImplementedException();
        }
    }
}