using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
using SagePOS.Manger.Common.Business.Entity;

namespace SagePOS.Server.Business.Entity
{
    [EntityIManager("CentrixERP.POS.Business.IManager.IPosSyncConfigurationManager,SagePOS.Server.Business.IManager", isLogicalDelete = true)]
    public class PosSyncConfiguration : EntityBase<PosSyncConfiguration, int>
    {
        #region Constructors

        public PosSyncConfiguration() { }

        public PosSyncConfiguration(int posSyncConfigurationId)
        {
            this.posSyncConfigurationId = posSyncConfigurationId;
        }

        #endregion

        #region Private Properties

		private int posSyncConfigurationId;

		private int posSyncTime1Id = -1;

		private int posSyncTime2Id = -1;

		private int posSyncTime3Id = -1;

		private int posSyncTime4Id = -1;

		private int posSyncTime5Id = -1;

		private int posDataToSync;

		private bool? syncDataFromAccpac;

		private int accpacSyncTime1Id = -1;

		private int accpacSyncTime2Id = -1;

		private int accpacSyncTime3Id = -1;

		private int accpacSyncTime4Id = -1;

		private int accpacSyncTime5Id = -1;

		private string accpacDataToSyncIds;

		private string accpacDataToSync;

		private int status;

		private DateTime? createdDate;

		private DateTime? updatedDate;

		private int createdBy;

		private int updatedBy;

		private bool? flagDeleted;

		private bool? isSystem;



        //private bool? flagDeleted;

        //private bool? isSystem;

        //private int createdBy;

        //private int updatedBy;

        //private DateTime? createdDate;

        //private DateTime? updatedDate;
        #endregion

        #region Public Properties

        [PrimaryKey("@pos_sync_configuration_id", SqlDbType.Int)]
[Property("pos_sync_configuration_id", SqlDbType.Int, AddParameter = false)]
		public int PosSyncConfigurationId
		{
			get
			{
				return posSyncConfigurationId;
			}
			set
			{
				posSyncConfigurationId = value;
			}
		}

[Property("pos_sync_time1_id", SqlDbType.Int, ReferenceParameter = true)]
[PropertyConstraint(false, "PosSyncTime1")]
		public int PosSyncTime1Id
		{
			get
			{
				return posSyncTime1Id;
			}
			set
			{
				posSyncTime1Id = value;
			}
		}

[Property("pos_sync_time2_id", SqlDbType.Int, ReferenceParameter = true)]
[PropertyConstraint(false, "PosSyncTime2", RefEntity = true, RefPropertyEntity = "PersonObj")]
		public int PosSyncTime2Id
		{
			get
			{
				return posSyncTime2Id;
			}
			set
			{
				posSyncTime2Id = value;
			}
		}

[Property("pos_sync_time3_id", SqlDbType.Int, ReferenceParameter = true)]
[PropertyConstraint(false, "PosSyncTime3", RefEntity = true, RefPropertyEntity = "PersonObj")]
		public int PosSyncTime3Id
		{
			get
			{
				return posSyncTime3Id;
			}
			set
			{
				posSyncTime3Id = value;
			}
		}

[Property("pos_sync_time4_id", SqlDbType.Int, ReferenceParameter = true)]
[PropertyConstraint(false, "PosSyncTime4", RefEntity = true, RefPropertyEntity = "PersonObj")]
		public int PosSyncTime4Id
		{
			get
			{
				return posSyncTime4Id;
			}
			set
			{
				posSyncTime4Id = value;
			}
		}

[Property("pos_sync_time5_id", SqlDbType.Int, ReferenceParameter = true)]
[PropertyConstraint(false, "PosSyncTime5", RefEntity = true, RefPropertyEntity = "PersonObj")]
		public int PosSyncTime5Id
		{
			get
			{
				return posSyncTime5Id;
			}
			set
			{
				posSyncTime5Id = value;
			}
		}

[Property("pos_data_to_sync", SqlDbType.Int)]
[PropertyConstraint(false, "PosDataToSync")]
		public int PosDataToSync
		{
			get
			{
				return posDataToSync;
			}
			set
			{
				posDataToSync = value;
			}
		}

[Property("sync_data_from_accpac", SqlDbType.Bit)]
[PropertyConstraint(false, "SyncDataFromAccpac")]
		public bool? SyncDataFromAccpac
		{
			get
			{
				return syncDataFromAccpac;
			}
			set
			{
				syncDataFromAccpac = value;
			}
		}

[Property("accpac_sync_time1_id", SqlDbType.Int, ReferenceParameter = true)]
[PropertyConstraint(false, "AccpacSyncTime1", RefEntity = true, RefPropertyEntity = "PersonObj")]
		public int AccpacSyncTime1Id
		{
			get
			{
				return accpacSyncTime1Id;
			}
			set
			{
				accpacSyncTime1Id = value;
			}
		}

[Property("accpac_sync_time2_id", SqlDbType.Int, ReferenceParameter = true)]
[PropertyConstraint(false, "AccpacSyncTime2", RefEntity = true, RefPropertyEntity = "PersonObj")]
		public int AccpacSyncTime2Id
		{
			get
			{
				return accpacSyncTime2Id;
			}
			set
			{
				accpacSyncTime2Id = value;
			}
		}

[Property("accpac_sync_time3_id", SqlDbType.Int, ReferenceParameter = true)]
[PropertyConstraint(false, "AccpacSyncTime3", RefEntity = true, RefPropertyEntity = "PersonObj")]
		public int AccpacSyncTime3Id
		{
			get
			{
				return accpacSyncTime3Id;
			}
			set
			{
				accpacSyncTime3Id = value;
			}
		}

[Property("accpac_sync_time4_id", SqlDbType.Int, ReferenceParameter = true)]
[PropertyConstraint(false, "AccpacSyncTime4", RefEntity = true, RefPropertyEntity = "PersonObj")]
		public int AccpacSyncTime4Id
		{
			get
			{
				return accpacSyncTime4Id;
			}
			set
			{
				accpacSyncTime4Id = value;
			}
		}

[Property("accpac_sync_time5_id", SqlDbType.Int, ReferenceParameter = true)]
[PropertyConstraint(false, "AccpacSyncTime5", RefEntity = true, RefPropertyEntity = "PersonObj")]
		public int AccpacSyncTime5Id
		{
			get
			{
				return accpacSyncTime5Id;
			}
			set
			{
				accpacSyncTime5Id = value;
			}
		}

[Property("accpac_data_to_sync_ids", SqlDbType.NVarChar)]
[PropertyConstraint(false, "AccpacDataToSyncIds", MaxLength = 100, MinLength = 3)]
		public string AccpacDataToSyncIds
		{
			get
			{
				return accpacDataToSyncIds;
			}
			set
			{
				accpacDataToSyncIds = value;
			}
		}

[Property("last_Pos_sync_date", SqlDbType.NVarChar)]
[PropertyConstraint(false, "LastPosSyncDate", MaxLength = 500, MinLength = 0)]
public string LastPosSyncDate
{
    get;
    set;
}
[Property("last_Erp_sync_date", SqlDbType.NVarChar)]
[PropertyConstraint(false, "LastErpSyncDate", MaxLength = 500, MinLength = 0)]
public string LastErpSyncDate
{
    get;
    set;
}

[Property("accpac_data_to_sync", SqlDbType.NVarChar)]
[PropertyConstraint(false, "AccpacDataToSync", MaxLength = 500, MinLength = 3)]
		public string AccpacDataToSync
		{
			get
			{
				return accpacDataToSync;
			}
			set
			{
				accpacDataToSync = value;
			}
		}

[Property("status", SqlDbType.Int)]
[PropertyConstraint(false, "Status")]
		public int Status
		{
			get
			{
				return status;
			}
			set
			{
				status = value;
			}
		}



        [Property("flag_deleted", SqlDbType.Bit, AddParameter = true, UpdateParameter = true, FindParameter = true)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [Property("is_system", SqlDbType.Bit, AddParameter = false, UpdateParameter = false,FindParameter=false)]
        public bool? IsSystem
        {
            get
            {
                return isSystem;
            }
            set
            {
                isSystem = value;
            }
        }

		[Property("created_date",SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
		public DateTime? CreatedDate
		{
			get
			{
				return createdDate;
			}
			set
			{
				createdDate = value;
			}
		}

        [Property("updated_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
		public DateTime? UpdatedDate
		{
			get
			{
				return updatedDate;
			}
			set
			{
				updatedDate = value;
			}
		}

		[Property("created_by",SqlDbType.Int, AddParameter = true, UpdateParameter = false)]
		public int CreatedBy
		{
			get
			{
				return createdBy;
			}
			set
			{
				createdBy = value;
			}
		}

		[Property("updated_by",SqlDbType.Int, AddParameter = false, UpdateParameter = true)]
		public int UpdatedBy
		{
			get
			{
				return updatedBy;
			}
			set
			{
				updatedBy = value;
			}
		}
        #endregion

             
   

        public override int GetIdentity()
        {
            return posSyncConfigurationId;
        }
        public override void SetIdentity(int value)
        {
            posSyncConfigurationId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            //            if (ParentType == typeof(Person))
            //    this.posSyncTime1Id = (int)value;
            //else if (ParentType == typeof(Person))
            //    this.posSyncTime2Id = (int)value;
            //else if (ParentType == typeof(Person))
            //    this.posSyncTime3Id = (int)value;
            //else if (ParentType == typeof(Person))
            //    this.posSyncTime4Id = (int)value;
            //else if (ParentType == typeof(Person))
            //    this.posSyncTime5Id = (int)value;
            //else if (ParentType == typeof(Person))
            //    this.accpacSyncTime1Id = (int)value;
            //else if (ParentType == typeof(Person))
            //    this.accpacSyncTime2Id = (int)value;
            //else if (ParentType == typeof(Person))
            //    this.accpacSyncTime3Id = (int)value;
            //else if (ParentType == typeof(Person))
            //    this.accpacSyncTime4Id = (int)value;
            //else if (ParentType == typeof(Person))
            //    this.accpacSyncTime5Id = (int)value;

        }
    }
}