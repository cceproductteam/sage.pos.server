using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace SagePOS.Server.Business.Entity
{
    public class GiftVoucherLite : EntityBaseLite
    {

        #region Public Properties

        [LiteProperty("gift_voucher_id", SqlDbType.Int)]
        public int GiftVoucherId
        {
            get;
            set;
        }

        [LiteProperty("gift_number", SqlDbType.NVarChar)]
        public string GiftNumber
        {
            get;
            set;
        }

        [LiteProperty("gift_name", SqlDbType.NVarChar)]
        public string GiftName
        {
            get;
            set;
        }

        [LiteProperty("gift_amount", SqlDbType.Money)]
        public decimal? GiftAmount
        {
            get;
            set;
        }

        [LiteProperty("expire_date", SqlDbType.DateTime)]
        public DateTime? ExpireDate
        {
            get;
            set;
        }

        [LiteProperty("is_used", SqlDbType.Bit)]
        public bool? IsUsed
        {
            get;
            set;
        }



        [LiteProperty("gift_voucher_id", SqlDbType.Int)]
        public int value
        {
            get;
            set;
        }

        [LiteProperty("gift_name", SqlDbType.NVarChar)]
        public string label
        {
            get;
            set;
        }

        [LiteProperty("flag_deleted", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? FlagDeleted
        {
            get;
            set;
        }

        [LiteProperty("is_system", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? IsSystem
        {
            get;
            set;
        }

        [LiteProperty("created_date", SqlDbType.DateTime)]
        public DateTime? CreatedDate
        {
            get;
            set;
        }

        [LiteProperty("updated_date", SqlDbType.DateTime)]
        public DateTime? UpdatedDate
        {
            get;
            set;
        }

        [LiteProperty("created_by", SqlDbType.Int, FindByIdLiteParameter = false)]
        public int CreatedBy
        {
            get;
            set;
        }

        [LiteProperty("updated_by", SqlDbType.Int, FindByIdLiteParameter = false)]
        public int UpdatedBy
        {
            get;
            set;
        }

        [LiteProperty("created_by_name", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string CreatedByName { get; set; }

        [LiteProperty("updated_by_name", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string UpdatedByName { get; set; }

        [LiteProperty("gift_voucher_id", SqlDbType.Int)]
        public int Id
        {
            get { return this.GiftVoucherId; }
            set { this.GiftVoucherId = value; }
        }

        [LiteProperty("last_rows", SqlDbType.NVarChar)]
        public bool LastRows { get; set; }

        [LiteProperty("total_count", SqlDbType.NVarChar)]
        public int TotalRecords { get; set; }
        #endregion


    }
}