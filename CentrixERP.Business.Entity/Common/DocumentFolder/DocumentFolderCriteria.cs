using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;

namespace SagePOS.Manger.Common.Business.Entity
{
    public class DocumentFolderCriteria : CriteriaBase
    {
        public int parentFolder = -1;
        public int EntityID=-1;
        public int ParentEntityId = -1;
        public string KeyWord;
        public int CreatedBy;
        
    }
}
