using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;

namespace SagePOS.Manger.Common.Business.Entity
{
    [EntityIManager("CentrixERP.Common.Business.IManager.IDocumentFolderManager,SagePOS.Server.Business.IManager", isLogicalDelete = true)]
    public class DocumentFolder : EntityBase<DocumentFolder, int>
    {
        #region Constructors

        public DocumentFolder() { }

        public DocumentFolder(int folderId)
        {
            this.folderId = folderId;
        }

        #endregion

        #region Private Properties
		private int folderId;
		private string folderName;
		private int parentFolder;
		private int entityId;
        private int parentEntityId;
		private bool? flagDeleted;
		private int updatedBy;
		private DateTime? updatedDate;
		private bool? isSystem;
		private int createdBy;
		private DateTime? createdDate;

        #endregion

        #region Public Properties
		public int FolderId
		{
			get
			{
				return folderId;
			}
			set
			{
				folderId = value;
			}
		}
		public string FolderName
		{
			get
			{
				return folderName;
			}
			set
			{
				folderName = value;
			}
		}
		public int ParentFolder
		{
			get
			{
				return parentFolder;
			}
			set
			{
				parentFolder = value;
			}
		}
		public int EntityId
		{
			get
			{
				return entityId;
			}
			set
			{
				entityId = value;
			}
		}
		public bool? FlagDeleted
		{
			get
			{
				return flagDeleted;
			}
			set
			{
				flagDeleted = value;
			}
		}
		public int UpdatedBy
		{
			get
			{
				return updatedBy;
			}
			set
			{
				updatedBy = value;
			}
		}
		public DateTime? UpdatedDate
		{
			get
			{
				return updatedDate;
			}
			set
			{
				updatedDate = value;
			}
		}
		public bool? IsSystem
		{
			get
			{
				return isSystem;
			}
			set
			{
				isSystem = value;
			}
		}
		public int CreatedBy
		{
			get
			{
				return createdBy;
			}
			set
			{
				createdBy = value;
			}
		}
		public DateTime? CreatedDate
		{
			get
			{
				return createdDate;
			}
			set
			{
				createdDate = value;
			}
		}
        public int ParentEntityId
        {
            get { return parentEntityId; }
            set { parentEntityId = value; }
        }

        #endregion

        #region Composite Objects   
        private DocumentFolder documentParentFolder;
        [CompositeObject(CompositObjectTypeEnum.Single, "ParentFolder", LazyLoad = true, CascadeDelete = true, Savable = false)]
        public DocumentFolder DocumentParentFolder
        {
            get
            {
                if (documentParentFolder == null)
                    loadCompositObject_Lazy("DocumentParentFolder");
                return documentParentFolder;
            }
            set
            {
                documentParentFolder = value;
                documentParentFolder.MarkModified();
            }
        }

        private Module entity;
        [CompositeObject(CompositObjectTypeEnum.Single, "EntityId", LazyLoad = true, CascadeDelete = true, Savable = false)]
        public Module Entity
        {
            get
            {
                if (entity == null)
                    loadCompositObject_Lazy("Entity");
                return entity;
            }
            set
            {
                entity = value;
                entity.MarkModified();
            }
        }
        #endregion

        public override int GetIdentity()
        {
            return folderId;
        }
        public override void SetIdentity(int value)
        {
            folderId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
			throw new NotImplementedException();
        }
    }
}