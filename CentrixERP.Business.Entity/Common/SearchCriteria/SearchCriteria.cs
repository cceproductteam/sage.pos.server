using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;

namespace SagePOS.Manger.Common.Business.Entity
{
    [EntityIManager("CentrixERP.Common.Business.IManager.ISearchCriteriaManager,SagePOS.Server.Business.IManager", isLogicalDelete = true)]
    public class SearchCriteria : EntityBase<SearchCriteria, int>
    {
        private int savedSearchId;
        private int entityId;
        private int userId;
        private string searchName;
        private string searchCriteria;
        private DateTime createdDate;

        public int EntityId
        {
            get { return entityId; }
            set { entityId = value; }
        }
        public int UserId
        {
            get { return userId; }
            set { userId = value; }
        }
        public string SearchName
        {
            get { return searchName; }
            set { searchName = value; }
        }
        public string SerachCriteria
        {
            get { return searchCriteria; }
            set { searchCriteria = value; }
        }
        public int SavedSearchId
        {
            get { return savedSearchId; }
            set { savedSearchId = value; }
        }

        public DateTime CreatedDate
        {
            get { return createdDate; }
            set { createdDate = value; }
        }


        public override int GetIdentity()
        {
            return savedSearchId;
        }

        public override void SetIdentity(int value)
        {
            savedSearchId = value;
        }

        public override void SetParentId(object value, Type ParentType)
        {
            throw new NotImplementedException();
        }
    }
}
