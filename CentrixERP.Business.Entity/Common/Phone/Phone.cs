using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.Framework;
using SF.FrameworkEntity;
using SagePOS.Server.Configuration;

namespace SagePOS.Manger.Common.Business.Entity
{
    [EntityIManager("CentrixERP.Common.Business.IManager.IPhoneManager,SagePOS.Server.Business.IManager", isLogicalDelete = true)]
    public class Phone : EntityBase<Phone, int>
    {
        private int phoneID = -1;
        private string phoneNumber;
        private string areaCode;
        private string countryCode;
        private Enums_S3.Phone.PhoneType type;
        private int createdBy = -1;
        private int updatedBy = -1;
        public string Description { get; set; }
        public string UpdatedByName
        {
            get;
            set;
        }
        public string CreatedByName
        {
            get;
            set;
        }
       

        #region constructor
        public Phone() { }

        public Phone(int phoneID)
        {
            PhoneID = phoneID;
        }
        #endregion

       
        public int PhoneID
        {
            get { return phoneID; }
            set { phoneID = value; }
        }

        public string PhoneNumber
        {
            get { return phoneNumber; }
            set { phoneNumber = value; }
        }

        public string AreaCode
        {
            get { return areaCode; }
            set { areaCode = value; }
        }

        public string CountryCode
        {
            get { return countryCode; }
            set { countryCode = value; }
        }
        public DateTime? Crearted_Date
        {
            get;
            set;
        }
        public DateTime? Updated_Date
        {
            get;
            set;

        }

        public Enums_S3.Phone.PhoneType Type
        {
            get { return type; }
            set { type = value; }
        }

        public int CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }

        public int UpdatedBy
        {
            get { return updatedBy; }
            set { updatedBy = value; }
        }

        public override int GetIdentity()
        {
            return PhoneID;
        }

        public override void SetIdentity(int value)
        {
            PhoneID = value;
        }

        public override void SetParentId(object value, Type ParentType)
        {
            throw new NotImplementedException();
        }
    }
}
