using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Configuration;


namespace SagePOS.Manger.Common.Business.Entity
{
    [EntityIManager("CentrixERP.Common.Business.IManager.IEmailManager,SagePOS.Server.Business.IManager", isLogicalDelete = true)]
    public class Email : EntityBase<Email, int>
    {
        #region "Constructors"
        public Email()
        {
        }

        public Email(int email_id)
        {
            this.email_id = email_id;
        }
        #endregion

        #region "Private Property"
        private int email_id = -1;
        private string emailAddress;
        private Enums_S3.Email.EmailType type;
        private bool flag_deleted;
        private bool is_sysetm;
        private int createdBy = -1;
        private int updatedBy = -1;
        private DateTime? crearted_date;
        private DateTime? updated_date;
        #endregion

        #region "Public Property"
        public Enums_S3.Email.EmailType Type
        {
            get { return type; }
            set { type = value; }
        }
        public int Email_Id
        {
            get { return email_id; }
            set { email_id = value; }
        }

        public string EmailAddress
        {
            get { return emailAddress; }
            set { emailAddress = value; }
        }

        public string Description { get; set; }
        public int UpdatedBy
        {
            get { return updatedBy; }
            set { updatedBy = value; }
        }

        public int CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }
        public bool Flag_Deleted
        {
            get { return flag_deleted; }
            set { flag_deleted = value; }
        }

        public bool Is_Sysetm
        {
            get { return is_sysetm; }
            set { is_sysetm = value; }
        }
        public DateTime? Crearted_Date
        {
            get { return crearted_date; }
            set { crearted_date = value; }
        }

        public DateTime? Updated_Date
        {
            get { return updated_date; }
            set { updated_date = value; }
        }

        public string UpdatedByName
        {
            get;
            set;
        }
        public string CreatedByName
        {
            get;
            set;
        }



        #endregion

        #region "Entity Base Methods"
        public override int GetIdentity()
        {
            return email_id;
        }

        public override void SetIdentity(int value)
        {
            email_id = value;
        }

        public override void SetParentId(object value, Type ParentType)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}


