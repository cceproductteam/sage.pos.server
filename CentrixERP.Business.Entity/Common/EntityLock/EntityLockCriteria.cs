using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;

namespace SagePOS.Manger.Common.Business.Entity
{
    public class EntityLockCriteria : CriteriaBase
    {
        public int? UserId { get; set; }
        public int? EntityId { get; set; }
        public int? EntityValueId { get; set; }
        public int? LockedBy { get; set; }
        public bool LockWithoutCheck { get; set; }
    }
}
