using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SagePOS.Manger.Common.Business.Entity
{
    public class EntityLockLite
    {

        public int EntityLockId { get; set; }
        public int EntityId { get; set; }
        public int EntityValueId { get; set; }
        public string UserName { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public string EntityName { get; set; }
        public string EntityViewURL { get; set; }
        public string UIName { set; get; }

    }
}
