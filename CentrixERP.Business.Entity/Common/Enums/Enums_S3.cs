//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//namespace SagePOS.Manger.Common.Business.Entity
//{
//    public class Enums_S3
//    {
//        public class Reports
//        {
//            public enum schema
//            {
//                Company=1,
//                Lead=2,
//                Person =4,
//                Opportunity=5,
//                SchedualTask=6,
//                CompanyDetailesReport = 7,
//                LeadDetailsReport = 8,
//                PersonDetailsReport = 9,
//                OpportunityDetailsReport = 10,
//                SchedualTaskDetailesReport = 11
//            }
//        }

//        public class ReportQueue
//        {
//            public enum ReportStatus
//            {
//                Pending = 1,
//                InProgress = 2,
//                Succeed = 3,
//                Failed = 4
//            }
//        }

//        public enum Priority
//        {
//            High = 1,
//            Normal = 2,
//            Low = 3
//        }

//        public class Permissions
//        {
//            public enum SystemModules
//            {
//                None = 0,
//                ViewUser = 1,
//                AddUser = 2,
//                EditUser = 3,
//                DeleteUser = 4,
//                ViewRole = 5,
//                AddRole = 6,
//                EditRole = 7,
//                DeleteRole = 8,
//                ViewCompany = 9,
//                AddCompany = 10,
//                EditCompany = 11,
//                DeleteCompany = 12,
//                ViewPerson = 13,
//                AddPerson = 14,
//                EditPerson = 15,
//                DeletePerson = 16,
//                ViewPhone = 17,
//                AddPhone = 18,
//                EditPhone = 19,
//                DeletePhone = 20,
//                ViewAddress = 21,
//                AddAddress = 22,
//                EditAddress = 23,
//                DeleteAddress = 24,
//                ViewNote = 25,
//                AddNote = 26,
//                EditNote = 27,
//                DeleteNote = 28,
//                ViewPermissions = 29,
//                AddPermissions = 30,
//                EditPermissions = 31,
//                DeletePermissions = 32,
//                ViewDefaultPage = 33,
//                Logout = 34,
//                SchedualTask = 35,
//                UserCompanyPermissions = 36,
//                UserPersonPermissions = 37,
//                AssignTaskToUser = 38,
//                Configuration = 39,
//                DeleteTask = 40,
//                ViewLead = 41,
//                AddLead = 42,
//                EditLead = 43,
//                DeleteLead = 44,
//                ViewOpportunity = 45,
//                AddOpportunity = 46,
//                EditOpportunity = 47,
//                DeleteOpportunity = 48,
//                Reports = 49,
//                AddTeam = 50,
//                EditTeam = 51,
//                ViewTeam = 52,
//                DeleteTeam = 53,
//                AddDocument = 54,
//                EditDocument = 55,
//                ViewDocument = 56,
//                DeleteDocument = 57,
//                AddOpportunityDocument = 58,
//                EditOpportunityDocument = 59,
//                ViewOpportunityDocument = 60,
//                DeleteOpportunityDocument = 61,
//                ViewOpportunityTab = 67,
//                AddOpportunityTab = 68,
//                EditOpportunityTab = 69,
//                AddRealtedOpportunity = 70,
//                EditRealtedOpportunity = 71,
//                ViewRealtedOpportunity = 72,
//                ListRealtedOpportunity = 73,
//                ViewEmail = 74,
//                AddEmail = 75,
//                EditEmail = 76,
//                DeleteEmail = 77,
//                ViewOpportunityNote = 78,
//                AddOpportunityNote = 79,
//                EditOpportunityNote = 80,
//                DeleteOpportunityNote = 81,
//                CompanyCommunication = 82,
//                PersonCommunication = 83,
//                OpportunityCommunication = 84,
//                ViewProject = 85,
//                AddProject = 86,
//                EditProject = 87,
//                DeleteProject = 88,
//                ProjectCommunication = 89,
//                SalesEmail = 90,
//                AddForm = 91,
//                EditFormValue = 92,
//                AddFormValue = 93,
//                ViewStakeHolder = 94,
//                AddStakeHolder = 95,
//                EditStakeHolder = 96,
//                DeleteStakeHolder = 97,
//                ViewResource = 98,
//                AddResource = 99,
//                EditResource = 100,
//                DeleteResource = 101,
//                ViewMileStone = 102,
//                AddMileStone = 103,
//                EditMileStone = 104,
//                DeleteMileStone = 105,
//                AddEditTaxAuthority = 106,
//                ViewTaxAuthority = 107,
//                DeleteTaxAuthority = 108,
//                AddTaxClassHeader = 109,
//                ViewTaxClassHeader = 110,
//                ListTaxClassHeader = 111,
//                DeleteTaxClassHeader = 112,
//                AddEditTaxClassDetails = 113,
//                ViewTaxClassDetails = 114,
//                ListTaxClassDetails = 115,
//                DeleteTaxClassDetails = 116,
//                //AddEditCurrency = 117,
//                //ViewCurrency = 118,
//                //DeleteCurrency = 119,
//                ViewCurrencyRateHeader = 120,
//                AddEditCurrencyRateHeader = 121,
//                DeleteCurrencyRateHeader = 122,
//                ViewCurrencyRateDetails = 123,
//                AddEditCurrencyRateDetails = 124,
//                DeleteCurrencyRateDetails = 125,
//                AddEditTaxRate = 126,
//                ViewTaxRate = 127,
//                ListTaxRate = 128,
//                AddTaxGroupHeader = 129,
//                EditTaxGroupHeader = 130,
//                ViewTaxGroupHeader = 131,
//                ListTaxGroupHeader = 132,
//                AddEditTaxGroupDetails = 133,
//                ListTaxGroupDetails = 134,
//                ViewTaxGroupDetails = 135,
//                ListFiscalCalender = 136,
//                AddEditFiscalCalender = 137,
//                ViewFiscalCalender = 138,
//                DeleteFiscalCalender = 139,
//                LeadReport = 148,
//                CompanyReport = 149,
//                PersonReport = 150,
//                OpportunityReport = 151,
//                SystemReports = 152,
//                ViewPrizes = 153,
//                AddEditPrizes = 154,
//                DeletePrizes = 155,
//                ViewRedemptions = 156,
//                AddEditRedemptions = 157,
//                DeleteRedemptions = 158,
//                AddEditActivityLookup = 159,
//                ViewActivityLookup = 160,
//                ListDataTypeContent = 161,
//                AddDataTypeContent = 162,
//                EditDataTypeContent = 163,
//                ViewTemplate = 164,
//                AddTemplate = 165,
//                EditTemplate = 166,
//                ListTemplate = 167,
//                ViewTemplateType = 168,
//                AddTemplateType = 169,
//                EditTemplateType = 171,
//                ListTemplateType = 172,
//                SendNotifications = 173,
//                SentNotificationList = 174,
//                SentNotificationView = 175,
//                SchedualTaskReport = 176,
//                UserAccount = 177,
//                NewDesignCalenderDefault = 178,
//                AddCommunication = 179,
//                DuplicationRoleConfiguration = 180,
//                CompanyLead = 183,
//                PersonLead = 184,
//                ViewLeadTab = 185,
//                SavedSearches = 186,
//                AddEditDocumentFolder=187,
//                AddEditCategory	= 189,
//                ViewCategory = 190,
//                ListCategory =191,
//                DeleteCategory =192,
//                ViewHelp =193,
//                ViewCategoryContent	 = 194,
//                AddEditCategoryContent = 195,
//                ListCategoryContent = 196 ,
//                DeleteCategoryContent = 197,
//                ListDocumentFolder=197,
//                ViewDocumentFolder=198,
//                ViewCompanyRelationShip	 =199,
//                AddEditCompanyRelationShip	 = 200,
//                DeleteCompanyRelationShip= 202,
//                ViewCompanySits = 203,
//                viewCompanyContracts = 204,
//                AddEditCredit= 205,
//                ListCredit=206,
//                ViewCredit=207,
//                ListContactCredit=208,
//                ListCreditHistory=209,
//                ListClearanceLevel=210,
//                ListProduct=211,
//                ViewProduct=212,
//                AddEditProduct=213,
//                ListPriceList=214,
//                AddEditPriceList=215,
//                ViewPriceList=216,
//                ListCurrency=217,
//                AddEditCurrency=218,
//                ViewCurrency=219,
//                AddEditQuotation=220,
//                ViewQuotation=221,
//                ListQoutation=222,
              
//                AddEditGroup = 223,
//                ViewGroup = 224,
//                DeleteGroup = 225,
//                AddEditGroupSearchCriteria = 226,
//                ViewGroupSearchCriteria = 227,
//                DeleteGroupSearchCriteria = 228,
//                ViewEmailConfigration = 229,
//                AddEditEmailConfigration = 230,
//                DeleteEmailConfigration = 231,
//            }

//            public enum TeamPermission
//            {
//                User = 1,
//                Team = 2,
//                Role = 3
//            }
//        }

//        public class Phone
//        {
//            public enum PhoneType
//            {
//                LandLine = 1,
//                Skype= 2,
//                Mobile = 3
//            }
//        }

//        public class DataTypes
//        {
//            public enum DataType
//            {
//                Type = 1,
//                Segment = 2,
//                Source = 3,
//                Salutation = 4,
//                Gender = 5,
//                MaritalStatus = 6,
//                Religion = 7,
//                Department = 8,
//                Position = 9,
//                CompanyCategory = 10,
//                MainProductInterest = 11,
//                DecisionTimeframe = 12,
//                LeadStage = 13,
//                LeadStatus = 14,
//                Priority = 15,
//                Rating = 16,
//                AnnualRevenues = 17,
//                NoOfEmployee = 18,
//                Industry = 19,
//                OpportunityStage = 20,
//                OpportunityStatus = 21,
//                OpportunityType = 22,
//                Currency = 23,
//                RealtedOpportunityType = 24,
//                ProjectProduct = 28,
//                ProjectStatus = 29,
//                TransactionType = 30,
//                ClassType = 31,
//                DateMatched = 32,
//                RateOperation = 33,
//                PrizesTypes = 34,
//                Module=35,
//                FilterType=36,
//                PersonRelations=37,
//                scheduleTaskType=38,
//                CompanyOwner =39,
//                ClearanceLevel=40,
//                CommunicationMedium=41,
//                Frequency=42,
//                ProductStatus=43,
//                ProductItemType=44,
//                CurrencySymbol=45,
//                CloseReason = 46,
//                SuspendReason =47,
//                QuotationType=48,
//                QuotationCategory=49,
//                QuotationStatus = 50

//            }

//            public enum Stage
//            {
//                Assigned = 90
//            }

//            public enum Status
//            {
//                InProgress = 95
//            }

//            public enum DefualtOpportunityStage
//            {
//                Lead = 118

//            }

//            public enum DefualtOpportunityStatus
//            {
//                InProgress = 126
//            }

//            public enum DefualtRating
//            {
//                Rating = 101
//            }

//            public enum Gender
//            {
//                Male = 31,
//                Female = 32
//            }

//            public enum TransactionType
//            {
//                Purchase = 161,
//                Sale = 162
//            }

//            public enum ClassType
//            {
//                Vendor = 163,
//                Customer = 164,
//                Item = 165
//            }
        
//            public enum OpportunityStage
//            {
//                Lead = 118,
//                Suspended = 119,
//                Prospect = 120,
//                Quoted = 121,
//                QuoteSigned = 122,
//                OrderRecived = 123,
//                Closed = 125

//            }
//        }

//        public class Customers
//        {
//            public enum CustomerType
//            {
//                Company = 0,
//                Person = 1
//            }
//        }

//        public class Schedule
//        {
//            public enum TaskStatus
//            {
//                Active = 1,
//                Completed = 2,
//                Incomplete = 3,
//                Refused = 4
//            }
//        }

//        public class Configuration
//        {
//            public enum Language
//            {
//                ar = 1,
//                en = 2
//            }

//            public enum Mode
//            {
//                Add,
//                Edit,
//                Delete,
//                List,
//                View
//            }

//            public enum Module
//            {
//                Company = 1,
//                Person = 2,
//                User,
//                Lead,
//                Opportunity,
//                Team,
//                Role,
//                OpportunityTab,
//                RealtedOpportunity,
//                Project,
//                FormBuilder,
//                TaxAuthority,
//                TaxClassHeader,
//                TaxClassDetails,
//                Currency,
//                CurrencyRateHeader,
//                CurrencyRateDetails,
//                TaxRate,
//                TaxGroupHeader,
//                TaxGroupDetails,
//                FiscalCalender,
//                Report,
//                Prizes,
//                Redemptions,
//                Template,
//                TemplateType,
//                ActivityLookup,
//                DataTypeContent,
//                Address,
//                Phone,
//                Email,
//                Note,
//                Category,
//                CategoryContent,
//                DocumentFolder,
//                Credit,
//                Product,
//                PriceList,
//                Quotation,
//                Group
//            }

//            public enum Page
//            {
//                EditCompany,
//                AddCompany,
//                ViewCompany,
//                ListCompany,
//                EditPerson,
//                AddPerson,
//                ViewPerson,
//                ListPerson,
//                EditUser,
//                AddUser,
//                ViewUser,
//                ListUser,
//                EditTeam,
//                AddTeam,
//                ViewTeam,
//                ListTeam,
//                EditRole,
//                AddRole,
//                ViewRole,
//                ListRole,
//                EditLead,
//                AddLead,
//                ViewLead,
//                ListLead,
//                EditOpportunity,
//                AddOpportunity,
//                ViewOpportunity,
//                ListOpportunity,
//                Delete,
//                EditDocument,
//                AddDocument,
//                ViewDocument,
//                ListDocument,
//                Notes,
//                Phones,
//                Address,
//                Email,
//                CompanyPersons,
//                CompanyOpportunity,
//                LeadAddCompany,
//                LeadViewCompany,
//                LeadAddPerson,
//                LeadViewPerson,
//                ChangeLang,
//                Logoutaspx,
//                Login,
//                Configuration,
//                Calendar,
//                CompanyPermission,
//                PersonPermission,
//                EditDataTypeContent,
//                AddDataTypeContent,
//                EditNavigationMenu,
//                Search,
//                MainStyle,
//                IncStyle,
//                LoginStyle,
//                scheduleStyle,
//                SchedualTasksReport,
//                UserCompaniesReport,
//                UserPersonsReport,
//                AreaCompaniesReport,
//                ReportDefault,
//                AddNavigationMenu,
//                ViewPersonCompany,
//                CompanyCommunication,
//                PersonCommunication,
//                EditOpportunityTab,
//                AddOpportunityTab,
//                ViewOpportunityTab,
//                EditRealtedOpportunity,
//                AddRealtedOpportunity,
//                ViewRealtedOpportunity,
//                ListRealtedOpportunity,
//                OpportunityNotes,
//                DocumentOpportunityAdd,
//                DocumentOpportunityEdit,
//                DocumentOpportunityView,
//                DocumentOpportunityList,
//                OpportunityCommunication,
//                EditProject,
//                AddProject,
//                ViewProject,
//                ListProject,
//                ProjectCommunication,
//                SalesEmail,
//                AdminTool,
//                Default,
//                StakeHolders,
//                Resources,
//                Milestones,
//                AddEditTaxAuthority,
//                ViewTaxAuthority,
//                ListTaxAuthority,
//                AddTaxClassHeader,
//                ViewTaxClassHeader,
//                ListTaxClassHeader,
//                AddEditTaxClassDetails,
//                ViewTaxClassDetails,
//                ListTaxClassDetails,
//                //AddEditCurrency,
//                //ViewCurrency,
//                //ListCurrency,
//                AddEditCurrencyRateHeader,
//                ViewCurrencyRateHeader,
//                ListCurrencyRateHeader,
//                AddEditCurrencyRateDetails,
//                ViewCurrencyRateDetails,
//                ListCurrencyRateDetails,
//                TaxRate,
//                ViewTaxRate,
//                ListTaxRate,
//                AddTaxGroupHeader,
//                EditTaxGroupHeader,
//                ViewTaxGroupHeader,
//                ListTaxGroupHeader,
//                AddEditTaxGroupDetails,
//                ListTaxGroupDetails,
//                ViewTaxGroupDetails,
//                ListFiscalCalender,
//                AddEditFiscalCalender,
//                ViewFiscalCalender,
//                LeadReport,
//                Reports,
//                Style,
//                PurchasedItemsList,
//                ListPrizes,
//                ViewPrizes,
//                AddEditPrizes,
//                ListRedemptions,
//                ViewRedemptions,
//                AddEditRedemptions,
//                Jscrollpane,
//                TemplateList,
//                TemplateView,
//                TemplateAddEdit,
//                TemplateTypeList,
//                TemplateTypeView,
//                TemplateTypeAddEdit,
//                NotificationStyle,
//                EmailConfiguration,
//                ViewActivityLookup,
//                AddEditActivityLookup,
//                ListActivityLookup,
//                ListDTC,
//                AddDTC,
//                EditDTC,
//                TreeStyle,
//                SentNotificationList,
//                SentNotificationView,
//                NewDesignCalenderDefault,
//                AddCommunication,
//                CompanyLead,
//                PersonLead,
//                Communication,
//                ViewLeadTab,
//                SavedSearches ,
//                RelatedPerson,
//                AddEditCategory,
//                ListCategory,
//                ViewCategory,
//                UserHelp,
//                AddEditCategoryContent,
//                ListCategoryContent,
//                ViewCategoryContent,
//                AddEditDocumentFolder,
//                ListDocumentFolder,
//                ViewDocumentFolder,
//                CompanyRelationShips,
//                CompanyContracts ,
//                CompanySits,
//                AddEditCredit,
//                ListCredit,
//                ViewCredit ,
//                ListContactCredit ,
//                ViewContactCredit,
//                ListCreditHistory,
//                ListClearanceLevel ,
//                ListProduct ,
//                ViewProduct,
//                AddEditProduct ,
//                ListPriceList ,
//                AddEditPriceList ,
//                ViewPriceList,
//                ListCurrency,
//                AddEditCurrency,
//                ViewCurrency,
//                AddEditProductPriceList ,
//                ListProductPriceList,
//                ViewProductPriceList,
//                AddEditQuotation ,
//                ViewQuotation ,
//                ListQoutation ,
//                AddEditQuoteItem,
//                AddEditGroup,
//                ViewGroup,
//                ListGroup,
//                GroupSearchCriteriaList,
//                EmailConfigration
 
                
//            }
//        }

//        public class Statistics
//        {
//            public enum Type
//            {
//                Number = 1,
//                Value = 2
//            }
//        }

//        public class Geo
//        {
//            public enum Country
//            {
//                Jordan=1
//            }
//        }

//        public class ReturnedValue
//        {
//            public enum Status
//            {
//                Failed = 0,
//                Added = 1,
//                Edited = 2,
//                Deleted = 3
//            }
//        }

//        public enum PersonFormatKeyword
//        {
//            FirstName,
//            LastName,
//            MiddleName,
//            Salutation,
//            JobTitle,
//            PhoneNumber
//        }

//        public enum AddressFormatKeyword
//        {
//            Place,
//            NearBy,
//            BuildingNumber,
//            StreetAddress,
//            POBox,
//            Country,
//            City,
//            Area,
//            ZipCode,
//            Region
//        }

//        public class ModulesKeys
//        {
//            public enum ModuleKeys
//            {
//                NPers,
//                NComp,
//                NProj,
//                EPers,
//                EComp,
//                PPers,
//                PComp,
//                APers,
//                AComp,
//                DPers,
//                DComp,
//                DProj,
//                EUser,
//                NOpp,
//                DOpp,
//                NTask,
//            }
//        }

//        public enum SortDirection
//        {
//            asc=0,
//            desc=1,
 
//        }
//        public enum Filter
//        {
//            all=0,
//            recentlyViewed=1,
//        }

//        public enum ControlType
//        {
//            Text=1,
//            DatePicker=2,
//            DDL=3,
//            AC=4,
//            RadioButton=5,
//            Label=6,
 
//        }

//        public enum Module
//        {
//            Company=193,
//            Person=194,
        
//        }

//        public enum DocumentModule
//        {
//            Company=1,
//            Person=2,
//            Project=3,
//            Opportunity=4
//        }
//    }
//}