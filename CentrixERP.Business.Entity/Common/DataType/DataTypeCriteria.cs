using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;

namespace SagePOS.Manger.Common.Business.Entity
{
    public class DataTypeCriteria : CriteriaBase
    {
        public string Name;
        public bool UseSystemDataType;
        
        public DateTime? LastSyncDate { get; set; }


    }
}
