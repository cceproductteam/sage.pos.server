using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SagePOS.Server.Configuration;

namespace SagePOS.Manger.Common.Business.Entity
{
    [EntityIManager("CentrixERP.Common.Business.IManager.IDataTypeContentManager,SagePOS.Server.Business.IManager", isLogicalDelete = true)]
    public class DataTypeContent : EntityBase<DataTypeContent, int>
    {
        private int dataTypeContentID = -1;
        private int dataTypeContentValue = -1;

        private string dataTypeContentEN;
        private string dataTypeContentAR;
        private int dataTypeID = -1;
        private int dataTypeOrder = -1;
        private DataType dataTypes;
        private int useSystemDataType;
        private int createdBy;
        private string createdByName;
        private int updatedBy;
        private DateTime? createdDate;
        private DateTime? updateDate;
        private bool isSystem;
        private bool setAsDefault;

        #region constructor
        public DataTypeContent() { }
        #endregion

        [CompositeObject(CompositObjectTypeEnum.Single, "DataTypeID", LazyLoad = true, SaveBeforeParent = true, CascadeDelete = false)]
        public DataType DataTypes
        {
            get
            {
                if (dataTypes == null)
                    loadCompositObject_Lazy("DataTypes");
                return dataTypes;
            }
            set { dataTypes = value; }
        }

        public int DataTypeOrder
        {
            get { return dataTypeOrder; }
            set { dataTypeOrder = value; }
        }

        public int DataTypeID
        {
            get { return dataTypeID; }
            set { dataTypeID = value; }
        }

        public string DataTypeContentAR
        {
            get { return dataTypeContentAR; }
            set { dataTypeContentAR = value; }
        }

        public string DataTypeContentEN
        {
            get { return dataTypeContentEN; }
            set { dataTypeContentEN = value; }
        }

        public int DataTypeContentID
        {
            get { return dataTypeContentID; }
            set { dataTypeContentID = value; }
        }


        public int DataTypeContentValue
        {
            get { return dataTypeContentValue; }
            set { dataTypeContentValue = value; }
        }

        public int CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }

        public int UpdatedBy
        {
            get { return updatedBy; }
            set { updatedBy = value; }
        }

        public DateTime? CreatedDate
        {
            get { return createdDate; }
            set { createdDate = value; }
        }

        public string CreatedByName
        {
            get { return createdByName; }
            set { createdByName = value; }
        }

        public DateTime? UpdateDate
        {
            get { return updateDate; }
            set { updateDate = value; }
        }


        public bool IsSystem
        {
            get { return isSystem; }
            set { isSystem = value; }
        }


        public override int GetIdentity()
        {
            return DataTypeContentID;
        }

        public override void SetIdentity(int value)
        {
            DataTypeContentID = value;
        }

        public override void SetParentId(object value, Type ParentType)
        {
            if (ParentType == typeof(DataType))
                DataTypeID = (int)value;
        }

        public int UseSystemDataType
        {
            get { return useSystemDataType; }
            set { useSystemDataType = value; }
        }

        public bool SetAsDefault
        {
            get { return setAsDefault; }
            set { setAsDefault = value; }
        }
    }
    public class DataTypeContentLite
    {

        public int DataTypeContentId { get; set; }
        public string DataTypeContent { get; set; }
        public string DataTypeContentAr { get; set; }
        public int DataTypeId { get; set; }
        public int DataTypeContentOrder { get; set; }
        public bool FlagDeleted { get; set; }
        public bool IsSystem { get; set; }
        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.DateTime UpdatedDate { get; set; }
        public int DataTypeContentValue { get; set; }
        public bool? SetAsDefault { get; set; }
        public int value { get { return DataTypeContentId; } }
        public string label
        {
            get
            {
                if (Configuration.Lang.ToString() == "ar")
                    return DataTypeContentAr;
                else
                    return DataTypeContent;
            }
        }
    }
}