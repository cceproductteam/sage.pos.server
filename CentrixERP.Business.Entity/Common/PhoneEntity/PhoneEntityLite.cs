using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace SagePOS.Manger.Common.Business.Entity
{
     public class PhoneEntityLite : EntityBaseLite
    {      
        
        #region Public Properties

         		[LiteProperty("phone_entity_id",SqlDbType.Int)]
		public int PhoneEntityId
		{
			get;
			set;
		}

		[LiteProperty("phone_id",SqlDbType.Int)]
		public int PhoneId
		{
			get;
			set;
		}

		[LiteProperty("entity_id",SqlDbType.Int)]
		public int EntityId
		{
			get;
			set;
		}

		[LiteProperty("entity_value_id",SqlDbType.Int)]
		public int EntityValueId
		{
			get;
			set;
		}

		[LiteProperty("is_default",SqlDbType.Bit)]
		public bool? IsDefault
		{
			get;
			set;
		}

        //[LiteProperty("phone_type",SqlDbType.Int)]
        //public int PhoneType
        //{
        //    get;
        //    set;
        //}
        [LiteProperty("phone_type_id", SqlDbType.Int)]
        public int PhoneTypeId
        {
            get;
            set;
        }

        private string phoneType;

        private string phoneTypeAr;

        [LiteProperty("phone_type", SqlDbType.NVarChar)]
        public string PhoneType
        {
            get { return SagePOS.Server.Configuration.Configuration.Lang == SagePOS.Server.Configuration.Enums_S3.Configuration.Language.ar ? phoneTypeAr : phoneType; }

            set { phoneType = value; }
        }

        [LiteProperty("phone_type_ar", SqlDbType.NVarChar)]
        public string PhoneTypeAr
        {
            get { return phoneTypeAr; }

            set { phoneTypeAr = value; }
        }

        [LiteProperty("Description", SqlDbType.NVarChar, FindAllLiteParameter = true)]
        public string Description { get; set; }

        [LiteProperty("phone_area_code", SqlDbType.NVarChar, FindAllLiteParameter = true)]
        public string AreaCode { get; set; }

        [LiteProperty("phone_country_code", SqlDbType.NVarChar, FindAllLiteParameter = true)]
        public string CountryCode { get; set; }

        [LiteProperty("phone_number", SqlDbType.NVarChar, FindAllLiteParameter = true)]
        public string PhoneNumber { get; set; }


         [LiteProperty("flag_deleted", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? FlagDeleted
        {
            get;
            set;
        }

        [LiteProperty("is_system",SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
		public bool? IsSystem
		{
			get;
			set;
		}

		[LiteProperty("created_date",SqlDbType.DateTime)]
		public DateTime? CreatedDate
		{
			get;
			set;
		}

		[LiteProperty("updated_date",SqlDbType.DateTime)]
		public DateTime? UpdatedDate
		{
			get;
			set;
		}

		[LiteProperty("created_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int CreatedBy
		{
			get;
			set;
		}

		[LiteProperty("updated_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int UpdatedBy
		{
			get;
			set;
		}

         [LiteProperty("created_by_name",SqlDbType.NVarChar, FindAllLiteParameter = true)]
         public string CreatedByName{get;set;}

         [LiteProperty("updated_by_name", SqlDbType.NVarChar, FindAllLiteParameter = true)]
         public string UpdatedByName{get;set;}

         [LiteProperty("phone_entity_id",SqlDbType.NVarChar)]
          public int Id
        {
            get{return this.PhoneEntityId; }
            set{this.PhoneEntityId=value;}
        }

         [LiteProperty("last_rows",SqlDbType.NVarChar)]
         public bool LastRows { get; set; }

         [LiteProperty("total_count",SqlDbType.NVarChar)]
         public int TotalRecords { get; set; }
        #endregion
       
          
     }
}