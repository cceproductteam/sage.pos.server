using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace SagePOS.Manger.Common.Business.Entity
{
    [EntityIManager("Centrix.Common.Business.IManager.IPhoneEntityManager,Centrix.Common.Business.IManager", isLogicalDelete = true)]
    public class PhoneEntity : EntityBase<PhoneEntity, int>
    {
        #region Constructors

        public PhoneEntity() { }

        public PhoneEntity(int phoneEntityId)
        {
            this.phoneEntityId = phoneEntityId;
        }

        #endregion

        #region Private Properties

		private int phoneEntityId;

		private int phoneId = -1;

		private int entityId = -1;

		private int entityValueId;

		private bool? isDefault;

		private int phoneType;

		private int createdBy;

		private DateTime? createdDate;

		private int updatedBy;

		private DateTime? updatedDate;

		private bool? flagDeleted;

		private bool? isSystem;


        #endregion

        #region Public Properties

        		[PrimaryKey("@phone_entity_id", SqlDbType.Int)]
		[Property("phone_entity_id",SqlDbType.Int,AddParameter=false)]
		public int PhoneEntityId
		{
			get
			{
				return phoneEntityId;
			}
			set
			{
				phoneEntityId = value;
			}
		}

		
		[Property("phone_id",SqlDbType.Int)]
		[PropertyConstraint(true,"PhoneId")]
		public int PhoneId
		{
			get
			{
				return phoneId;
			}
			set
			{
				phoneId = value;
			}
		}

		
		[Property("entity_id",SqlDbType.Int)]
		[PropertyConstraint(true,"EntityId")]
		public int EntityId
		{
			get
			{
				return entityId;
			}
			set
			{
				entityId = value;
			}
		}

		
		[Property("entity_value_id",SqlDbType.Int)]
		[PropertyConstraint(true,"EntityValueId")]
		public int EntityValueId
		{
			get
			{
				return entityValueId;
			}
			set
			{
				entityValueId = value;
			}
		}

		
		[Property("is_default",SqlDbType.Bit)]
		[PropertyConstraint(false,"IsDefault")]
		public bool? IsDefault
		{
			get
			{
				return isDefault;
			}
			set
			{
				isDefault = value;
			}
		}

		
		[Property("phone_type",SqlDbType.Int)]
		[PropertyConstraint(false,"PhoneType")]
		public int PhoneType
		{
			get
			{
				return phoneType;
			}
			set
			{
				phoneType = value;
			}
		}



        [Property("flag_deleted", SqlDbType.Bit, AddParameter = false, UpdateParameter = false,FindParameter=false)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [Property("is_system", SqlDbType.Bit, AddParameter = false, UpdateParameter = false,FindParameter=false)]
        public bool? IsSystem
        {
            get
            {
                return isSystem;
            }
            set
            {
                isSystem = value;
            }
        }

		[Property("created_date",SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
		public DateTime? CreatedDate
		{
			get
			{
				return createdDate;
			}
			set
			{
				createdDate = value;
			}
		}

        [Property("updated_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
		public DateTime? UpdatedDate
		{
			get
			{
				return updatedDate;
			}
			set
			{
				updatedDate = value;
			}
		}

		[Property("created_by",SqlDbType.Int, AddParameter = true, UpdateParameter = false)]
		public int CreatedBy
		{
			get
			{
				return createdBy;
			}
			set
			{
				createdBy = value;
			}
		}

		[Property("updated_by",SqlDbType.Int, AddParameter = false, UpdateParameter = true)]
		public int UpdatedBy
		{
			get
			{
				return updatedBy;
			}
			set
			{
				updatedBy = value;
			}
		}
        #endregion

             #region Composite Objects
              		private Phone phoneObj;
        [CompositeObject(CompositObjectTypeEnum.Single, "PhoneId", Savable = true, CascadeDelete = true, SaveBeforeParent = true)]
		public Phone PhoneObj
		{
			get
			{
				if (phoneObj == null)
					loadCompositObject_Lazy("PhoneObj");
				return phoneObj;
			}
			set
			{
				phoneObj = value;;
			}
		}
		
	
		


                 #endregion
   

        public override int GetIdentity()
        {
            return phoneEntityId;
        }
        public override void SetIdentity(int value)
        {
            phoneEntityId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            			if (ParentType == typeof(Phone))
				this.phoneId = (int)value;
			else if (ParentType == typeof(SystemEntity))
				this.entityId = (int)value;

        }
    }
}