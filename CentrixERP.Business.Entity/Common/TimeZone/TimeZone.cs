using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;

namespace SagePOS.Manger.Common.Business.Entity
{
    [EntityIManager("CentrixERP.Common.Business.IManager.ITimeZoneManager,SagePOS.Server.Business.IManager", isLogicalDelete = true)]
    public class TimeZone : EntityBase<TimeZone, int>
    {
        #region Constructors

        public TimeZone() { }

        public TimeZone(int timeZoneId)
        {
            this.timeZoneId = timeZoneId;
        }

        #endregion

        #region Private Properties

		private int timeZoneId;

        private string timeZoneDescription;

		private int createdBy;

		private int updatedBy;

		private DateTime? createdDate;

		private DateTime? updatedDate;

		private bool? isSystem;

		private bool? flagDeleted;


        #endregion

        #region Public Properties

		public int TimeZoneId
		{
			get
			{
				return timeZoneId;
			}
			set
			{
				timeZoneId = value;
			}
		}
		public string TimeZoneDescription
		{
			get
			{
				return timeZoneDescription;
			}
			set
			{
                timeZoneDescription = value;
			}
		}
		public int CreatedBy
		{
			get
			{
				return createdBy;
			}
			set
			{
				createdBy = value;
			}
		}
		public int UpdatedBy
		{
			get
			{
				return updatedBy;
			}
			set
			{
				updatedBy = value;
			}
		}
		public DateTime? CreatedDate
		{
			get
			{
				return createdDate;
			}
			set
			{
				createdDate = value;
			}
		}
		public DateTime? UpdatedDate
		{
			get
			{
				return updatedDate;
			}
			set
			{
				updatedDate = value;
			}
		}
		public bool? IsSystem
		{
			get
			{
				return isSystem;
			}
			set
			{
				isSystem = value;
			}
		}
		public bool? FlagDeleted
		{
			get
			{
				return flagDeleted;
			}
			set
			{
				flagDeleted = value;
			}
		}

        #endregion

        #region Composite Objects


        #endregion

        public override int GetIdentity()
        {
            return timeZoneId;
        }
        public override void SetIdentity(int value)
        {
            timeZoneId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
			throw new NotImplementedException();
        }
    }
}