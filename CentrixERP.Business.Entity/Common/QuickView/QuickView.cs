using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SagePOS.Manger.Common.Business.Entity
{
   public class QuickView
    {
       private string title;
       private string titlevalue;
       private string url;

       public QuickView() { }
       public QuickView(string title,string value) 
       {
           this.title = title;
           titlevalue = value;
       }

       public string Title
       {
           set { title = value; }
           get { return title; }
       }


       public string TitleValue
       {
           set { titlevalue = value; }
           get { return titlevalue; }
       
       }

       public string Url
       {
           set { url = value; }
           get { return url; }
       }

    }
}
