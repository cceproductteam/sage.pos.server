using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;

namespace SagePOS.Manger.Common.Business.Entity
{
    [EntityIManager("CentrixERP.Common.Business.IManager.IAddressManager,SagePOS.Server.Business.IManager", isLogicalDelete = true)]
    public class Address : EntityBase<Address, int>
    {
        private int addressID = -1;
        private string place;
        private string nearBy;
        private string buildingNumber;
        private string streetAddress;
        private string pOBox;
        private int countryId = -1;
        private int cityId = -1;
        private string zipCode;
        private string region ;
        private int areaId = -1;
        private int createdBy = -1;
        private int updatedBy = -1;
        private Country country;
        private City city;
        private Centrix.UM.Business.Entity.Area area;
        private string officeNumber;
        private string floor;
        private string cityText;
        private string county;
        

        

        #region constructor
        
        public Address() { }
        public string Type { get; set; }
        public Address(int addressID)
        {
            this.addressID = addressID;
        }
        #endregion
      
        public int UpdatedBy
        {
            get { return updatedBy; }
            set { updatedBy = value; }
        }
        public string UpdatedByName
        {
            get;
            set;
        }
        public int CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }
        public string CreatedByName
        {
            get;
            set;
        }


        public int AreaId
        {
            get { return areaId; }
            set { areaId = value; }
        }

        public string Region
        {
            get { return region; }
            set { region = value; }
        }


        public string ZipCode
        {
            get { return zipCode; }
            set { zipCode = value; }
        }

        public int CityId
        {
            get { return cityId; }
            set { cityId = value; }
        }

        public int CountryId
        {
            get { return countryId; }
            set { countryId = value; }
        }

        public string POBox
        {
            get { return pOBox; }
            set { pOBox = value; }
        }

        public string StreetAddress
        {
            get { return streetAddress; }
            set { streetAddress = value; }
        }

        public string BuildingNumber
        {
            get { return buildingNumber; }
            set { buildingNumber = value; }
        }

        public string NearBy
        {
            get { return nearBy; }
            set { nearBy = value; }
        }

        public string Place
        {
            get { return place; }
            set { place = value; }
        }

        public int AddressID
        {
            get { return addressID; }
            set { addressID = value; }
        }

        public string OfficeNumber
        {
            get { return officeNumber; }
            set { officeNumber = value; }
        }

        public string Floor
        {
            get { return floor; }
            set { floor = value; }
        }


        public string CityText
        {
            get { return cityText; }
            set { cityText = value; }
        }


        public string County
        {
            get { return county; }
            set { county = value; }
        }
        public DateTime? Crearted_Date
        {
            get;
            set;
        }
        public DateTime? Updated_Date
        {
            get;
            set;

        }

        [CompositeObject(CompositObjectTypeEnum.Single, "CountryId", LazyLoad = true, CascadeDelete = false, Savable = false)]
        public Country Country
        {
            get
            {
                if (country == null)
                    loadCompositObject_Lazy("Country");
                return country;
            }
            set
            {
                country = value;
                country.MarkModified();
            }
        }


        [CompositeObject(CompositObjectTypeEnum.Single, "CityId", LazyLoad = true, CascadeDelete = false, Savable = false)]
        public City City
        {
            get
            {
                if (city == null)
                    loadCompositObject_Lazy("City");
                return city;
            }
            set
            {
                city = value;
                city.MarkModified();
            }
        }


        [CompositeObject(CompositObjectTypeEnum.Single, "AreaId", LazyLoad = true, CascadeDelete = false, Savable = false)]
        public Centrix.UM.Business.Entity.Area Area
        {
            get
            {
                if (area == null)
                    loadCompositObject_Lazy("Area");
                return area;
            }
            set
            {
                area = value;
                area.MarkModified();
            }
        }

        public override int GetIdentity()
        {
            return AddressID;
        }

        public override void SetIdentity(int value)
        {
            AddressID = value;
        }

        public override void SetParentId(object value, Type ParentType)
        {
            throw new NotImplementedException();
        }
    }
}
