using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;

namespace SagePOS.Manger.Common.Business.Entity
{
    [EntityIManager("CentrixERP.Common.Business.IManager.ISystemConfigurationWebKeyManager,SagePOS.Server.Business.IManager", isLogicalDelete = true)]
    public class SystemConfigurationWebKey : EntityBase<SystemConfigurationWebKey, int>
    {
        #region Constructors

        public SystemConfigurationWebKey() { }

        public SystemConfigurationWebKey(int systemConfigurationWebKeyId)
        {
            this.systemConfigurationWebKeyId = systemConfigurationWebKeyId;
        }

        #endregion

        #region Private Properties

		private int systemConfigurationWebKeyId;

		private string key;

		private string value;

		private int type;

		private int updatedBy;

		private int createdBy;

		private DateTime? updatedDate;

		private DateTime? createdDate;

		private bool? flagDeleted;

		private bool? isSystem;


        #endregion

        #region Public Properties

		public int SystemConfigurationWebKeyId
		{
			get
			{
				return systemConfigurationWebKeyId;
			}
			set
			{
				systemConfigurationWebKeyId = value;
			}
		}
		public string Key
		{
			get
			{
				return key;
			}
			set
			{
				key = value;
			}
		}
		public string Value
		{
			get
			{
				return value;
			}
			set
			{
				this.value = value;
			}
		}
		public int Type
		{
			get
			{
				return type;
			}
			set
			{
				type = value;
			}
		}
		public int UpdatedBy
		{
			get
			{
				return updatedBy;
			}
			set
			{
				updatedBy = value;
			}
		}
		public int CreatedBy
		{
			get
			{
				return createdBy;
			}
			set
			{
				createdBy = value;
			}
		}
		public DateTime? UpdatedDate
		{
			get
			{
				return updatedDate;
			}
			set
			{
				updatedDate = value;
			}
		}
		public DateTime? CreatedDate
		{
			get
			{
				return createdDate;
			}
			set
			{
				createdDate = value;
			}
		}
		public bool? FlagDeleted
		{
			get
			{
				return flagDeleted;
			}
			set
			{
				flagDeleted = value;
			}
		}
		public bool? IsSystem
		{
			get
			{
				return isSystem;
			}
			set
			{
				isSystem = value;
			}
		}

        #endregion

        #region Composite Objects


        #endregion

        public override int GetIdentity()
        {
            return systemConfigurationWebKeyId;
        }
        public override void SetIdentity(int value)
        {
            systemConfigurationWebKeyId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
			throw new NotImplementedException();
        }
    }
}