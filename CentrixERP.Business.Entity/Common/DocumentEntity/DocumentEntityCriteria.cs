﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace SagePOS.Manger.Common.Business.Entity
{
    public class DocumentEntityCriteria : CriteriaBase//, IAdvancedSearchCriteria
    {
        [CrtiteriaField("@page_number", SqlDbType.Int)]
        public int? pageNumber { get; set; }

        [CrtiteriaField("@result_count", SqlDbType.Int)]
        public int? resultCount { get; set; }

        [CrtiteriaField("@keyword", SqlDbType.NVarChar)]
        public string Keyword { get; set; }

        [CrtiteriaField("@search_criteria", SqlDbType.NVarChar)]
        public string SearchCriteria { get; set; }

        [CrtiteriaField("@sort_type", SqlDbType.NVarChar)]
        public bool? SortType { get; set; }

        [CrtiteriaField("@sort_fields", SqlDbType.NVarChar)]
        public string SortFields { get; set; }

        [CrtiteriaField("@document_entity_id", SqlDbType.Int)]
        public int? DocumentEntityId { get; set; }

        [CrtiteriaField("@entity_id", SqlDbType.Int)]
        public int? EntityId { get; set; }

        [CrtiteriaField("@entity_value_id", SqlDbType.Int)]
        public int? EntityValueId { get; set; }

        [CrtiteriaField("@parent_folder", SqlDbType.NVarChar)]
        public string ParentFolder { get; set; }

        [CrtiteriaField("@parent_folder_id", SqlDbType.Int)]
        public int ParentFolderId { get; set; }
    }
}
