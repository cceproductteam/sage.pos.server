using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;

namespace SagePOS.Manger.Common.Business.Entity
{
    [EntityIManager("CentrixERP.Common.Business.IManager.IEntityControlsManager,SagePOS.Server.Business.IManager", isLogicalDelete = true)]
    public class EntityControls : EntityBase<EntityControls, int>
    {
        #region Constructors

        public EntityControls() { }

        public EntityControls(int entityControlId)
        {
            this.entityControlId = entityControlId;
        }

        #endregion

        #region Private Properties

		private int entityControlId;

		private int entityId = -1;

		private string uiNameEn;

		private string uiNameAr;

		private string uiValue;

		private string dbColumnNameEn;

		private string dbColumnNameAr;

		private string serviceName;

		private string serviceMethod;

		private string methodArgs;

		private string methodArgsValues;

		private string childAc;

		private string childAcArgs;

		private int controlId = -1;

		private int propertyParentId;

		private string propertyNameEn;

		private string propertyNameAr;

		private int dataTypeId;

		private string dataTypeTableName;

		private int valueTypeId;

		private string valueType;

		private int createdBy;

		private int updatedBy;

		private DateTime? createdDate;

		private DateTime? updatedDate;

		private bool? isSystem;

		private bool? flagDeleted;

		private int entityReferanceId = -1;


        #endregion

        #region Public Properties

		public int EntityControlId
		{
			get
			{
				return entityControlId;
			}
			set
			{
				entityControlId = value;
			}
		}
		public int EntityId
		{
			get
			{
				return entityId;
			}
			set
			{
				entityId = value;
			}
		}
		public string UiNameEn
		{
			get
			{
				return uiNameEn;
			}
			set
			{
				uiNameEn = value;
			}
		}
		public string UiNameAr
		{
			get
			{
				return uiNameAr;
			}
			set
			{
				uiNameAr = value;
			}
		}
		public string UiValue
		{
			get
			{
				return uiValue;
			}
			set
			{
				uiValue = value;
			}
		}
		public string DbColumnNameEn
		{
			get
			{
				return dbColumnNameEn;
			}
			set
			{
				dbColumnNameEn = value;
			}
		}
		public string DbColumnNameAr
		{
			get
			{
				return dbColumnNameAr;
			}
			set
			{
				dbColumnNameAr = value;
			}
		}
		public string ServiceName
		{
			get
			{
				return serviceName;
			}
			set
			{
				serviceName = value;
			}
		}
		public string ServiceMethod
		{
			get
			{
				return serviceMethod;
			}
			set
			{
				serviceMethod = value;
			}
		}
		public string MethodArgs
		{
			get
			{
				return methodArgs;
			}
			set
			{
				methodArgs = value;
			}
		}
		public string MethodArgsValues
		{
			get
			{
				return methodArgsValues;
			}
			set
			{
				methodArgsValues = value;
			}
		}
		public string ChildAc
		{
			get
			{
				return childAc;
			}
			set
			{
				childAc = value;
			}
		}
		public string ChildAcArgs
		{
			get
			{
				return childAcArgs;
			}
			set
			{
				childAcArgs = value;
			}
		}
		public int ControlId
		{
			get
			{
				return controlId;
			}
			set
			{
				controlId = value;
			}
		}
		public int PropertyParentId
		{
			get
			{
				return propertyParentId;
			}
			set
			{
				propertyParentId = value;
			}
		}
		public string PropertyNameEn
		{
			get
			{
				return propertyNameEn;
			}
			set
			{
				propertyNameEn = value;
			}
		}
		public string PropertyNameAr
		{
			get
			{
				return propertyNameAr;
			}
			set
			{
				propertyNameAr = value;
			}
		}
		public int DataTypeId
		{
			get
			{
				return dataTypeId;
			}
			set
			{
				dataTypeId = value;
			}
		}
		public string DataTypeTableName
		{
			get
			{
				return dataTypeTableName;
			}
			set
			{
				dataTypeTableName = value;
			}
		}
		public int ValueTypeId
		{
			get
			{
				return valueTypeId;
			}
			set
			{
				valueTypeId = value;
			}
		}
		public string ValueType
		{
			get
			{
				return valueType;
			}
			set
			{
				valueType = value;
			}
		}
		public int CreatedBy
		{
			get
			{
				return createdBy;
			}
			set
			{
				createdBy = value;
			}
		}
		public int UpdatedBy
		{
			get
			{
				return updatedBy;
			}
			set
			{
				updatedBy = value;
			}
		}
		public DateTime? CreatedDate
		{
			get
			{
				return createdDate;
			}
			set
			{
				createdDate = value;
			}
		}
		public DateTime? UpdatedDate
		{
			get
			{
				return updatedDate;
			}
			set
			{
				updatedDate = value;
			}
		}
		public bool? IsSystem
		{
			get
			{
				return isSystem;
			}
			set
			{
				isSystem = value;
			}
		}
		public bool? FlagDeleted
		{
			get
			{
				return flagDeleted;
			}
			set
			{
				flagDeleted = value;
			}
		}
		public int EntityReferanceId
		{
			get
			{
				return entityReferanceId;
			}
			set
			{
				entityReferanceId = value;
			}
		}

        #endregion

        #region Composite Objects
		private SystemEntity entityObj;
		[CompositeObject(CompositObjectTypeEnum.Single, "EntityId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable= false)]
        public SystemEntity EntityObj
		{
			get
			{
				if (entityObj == null)
					loadCompositObject_Lazy("EntityObj");
				return entityObj;
			}
			set
			{
				entityObj = value;;
			}
		}

		private List<EntityControlConfig> entityControlConfigList;
		[CompositeObject(CompositObjectTypeEnum.List, "EntityControlId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true)]
		public List<EntityControlConfig> EntityControlConfigList
		{
			get
			{
				if (entityControlConfigList == null)
					loadCompositObject_Lazy("EntityControlConfigList");
				return entityControlConfigList;
			}
			set
			{
				entityControlConfigList = value;;
			}
		}
		


        #endregion

        public override int GetIdentity()
        {
            return entityControlId;
        }
        public override void SetIdentity(int value)
        {
            entityControlId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            if (ParentType == typeof(SystemEntity))
				this.entityId = (int)value;
        }
    }
}