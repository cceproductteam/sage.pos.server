using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace SagePOS.Manger.Common.Business.Entity
{
    public class EmailEntityLite : EntityBaseLite
    {

        #region Public Properties

        [LiteProperty("email_entity_id", SqlDbType.Int)]
        public int EmailEntityId
        {
            get;
            set;
        }

        [LiteProperty("email_id", SqlDbType.Int)]
        public int EmailId
        {
            get;
            set;
        }

        [LiteProperty("entity_id", SqlDbType.Int)]
        public int EntityId
        {
            get;
            set;
        }

        [LiteProperty("entity_value_id", SqlDbType.Int)]
        public int EntityValueId
        {
            get;
            set;
        }

        [LiteProperty("is_default", SqlDbType.Bit)]
        public bool? IsDefault
        {
            get;
            set;
        }

        //[LiteProperty("email_type", SqlDbType.Int)]
        //public int EmailType
        //{
        //    get;
        //    set;
        //}
        [LiteProperty("email_type_id", SqlDbType.Int)]
        public int EmailTypeId
        {
            get;
            set;
        }

        private string emailType;

        private string emailTypeAr;

        [LiteProperty("email_type", SqlDbType.NVarChar)]
        public string EmailType
        {
            get { return SagePOS.Server.Configuration.Configuration.Lang == SagePOS.Server.Configuration.Enums_S3.Configuration.Language.ar ? emailTypeAr : emailType; }

            set { emailType = value; }
        }

        [LiteProperty("email_type_ar", SqlDbType.NVarChar)]
        public string EmailTypeAr
        {
            get { return emailTypeAr; }

            set { emailTypeAr = value; }
        }

        [LiteProperty("email_address", SqlDbType.NVarChar, FindAllLiteParameter = true)]
        public string EmailAdress { get; set; }

        [LiteProperty("description", SqlDbType.NVarChar, FindAllLiteParameter = true)]
        public string Description { get; set; }



        [LiteProperty("flag_deleted", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? FlagDeleted
        {
            get;
            set;
        }

        [LiteProperty("is_system", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? IsSystem
        {
            get;
            set;
        }

        [LiteProperty("created_date", SqlDbType.DateTime)]
        public DateTime? CreatedDate
        {
            get;
            set;
        }

        [LiteProperty("updated_date", SqlDbType.DateTime)]
        public DateTime? UpdatedDate
        {
            get;
            set;
        }

        [LiteProperty("created_by", SqlDbType.Int, FindByIdLiteParameter = false)]
        public int CreatedBy
        {
            get;
            set;
        }

        [LiteProperty("updated_by", SqlDbType.Int, FindByIdLiteParameter = false)]
        public int UpdatedBy
        {
            get;
            set;
        }

        [LiteProperty("created_by_name", SqlDbType.NVarChar, FindAllLiteParameter = true)]
        public string CreatedByName { get; set; }

        [LiteProperty("updated_by_name", SqlDbType.NVarChar, FindAllLiteParameter = true)]
        public string UpdatedByName { get; set; }

        [LiteProperty("email_entity_id", SqlDbType.NVarChar)]
        public int Id
        {
            get { return this.EmailEntityId; }
            set { this.EmailEntityId = value; }
        }

        [LiteProperty("last_rows", SqlDbType.NVarChar)]
        public bool LastRows { get; set; }

        [LiteProperty("total_count", SqlDbType.NVarChar)]
        public int TotalRecords { get; set; }
        #endregion


    }
}