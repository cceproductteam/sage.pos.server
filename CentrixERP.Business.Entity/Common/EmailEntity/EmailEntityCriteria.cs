using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
//using SagePOS.Server.Business.Factory ;

namespace SagePOS.Manger.Common.Business.Entity
{
    public class EmailEntityCriteria : CriteriaBase//, IAdvancedSearchCriteria
    {
        [CrtiteriaField("@page_number", SqlDbType.Int)]
        public int? pageNumber { get; set; }

        [CrtiteriaField("@result_count", SqlDbType.Int)]
        public int? resultCount { get; set; }

        [CrtiteriaField("@keyword", SqlDbType.NVarChar)]
        public string Keyword { get; set; }

        [CrtiteriaField("@search_criteria", SqlDbType.NVarChar)]
        public string SearchCriteria { get; set; }

        [CrtiteriaField("@sort_type", SqlDbType.NVarChar)]
        public bool? SortType { get; set; }

        [CrtiteriaField("@sort_fields", SqlDbType.NVarChar)]
        public string SortFields { get; set; }

        [CrtiteriaField("@email_entity_id", SqlDbType.Int)]
        public int? EmailEntityId { get; set; }

        [CrtiteriaField("@entity_id", SqlDbType.Int)]
        public int? EntityId { get; set; }

        [CrtiteriaField("@entity_value_id", SqlDbType.Int)]
        public int? EntityValueId { get; set; }

        [CrtiteriaField("@type_id", SqlDbType.Int)]
        public int? typeId { get; set; }

        [CrtiteriaField("@email_address", SqlDbType.NVarChar)]
        public string  EmailAddress { get; set; }

        [CrtiteriaField("@is_default", SqlDbType.Bit)]
        public bool IsDefault { get; set; }
    }
}
