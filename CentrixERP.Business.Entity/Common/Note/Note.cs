using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.Framework;
using SF.FrameworkEntity;

namespace SagePOS.Manger.Common.Business.Entity
{
    [EntityIManager("CentrixERP.Common.Business.IManager.INoteManager,SagePOS.Server.Business.IManager", isLogicalDelete = true)]
    public class Note : EntityBase<Note, int>
    {
        private int noteID = -1;
        private int createdby = -1;
        private string noteContent;
        private int updatedBy = -1;

        #region
        public Note() { }
        public Note(int noteID)
        {
            NoteID = noteID;
        }
        #endregion

        public int UpdatedBy
        {
            get { return updatedBy; }
            set { updatedBy = value; }
        }

        public string NoteContent
        {
            get { return noteContent; }
            set { noteContent = value; }
        }

        public int Createdby
        {
            get { return createdby; }
            set { createdby = value; }
        }

        public int NoteID
        {
            get { return noteID; }
            set { noteID = value; }
        }
        public DateTime? Crearted_Date
        {
            get;
            set;
        }
        public DateTime? Updated_Date
        {
            get;
            set;

        }
        public string UpdatedByName
        {
            get;
            set;
        }
        public string CreatedByName
        {
            get;
            set;
        }

        public override int GetIdentity()
        {
            return NoteID;
        }

        public override void SetIdentity(int value)
        {
            NoteID = value;
        }

        public override void SetParentId(object value, Type ParentType)
        {
            throw new NotImplementedException();
        }
    }
}
