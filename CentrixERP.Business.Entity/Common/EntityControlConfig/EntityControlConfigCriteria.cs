using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;

namespace SagePOS.Manger.Common.Business.Entity
{
    public class EntityControlConfigCriteria : CriteriaBase
    {
        public int EntityId = -1;
        public bool AdvancedSearchType= false;
    }
}
