using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;

namespace SagePOS.Manger.Common.Business.Entity
{
    [EntityIManager("CentrixERP.Common.Business.IManager.IDuplicationRuleFilterTypeManager,SagePOS.Server.Business.IManager", isLogicalDelete = true)]
    public class DuplicationRuleFilterType : EntityBase<DuplicationRuleFilterType, int>
    {
        private int duplicationFilterTypeId = -1;
        private string duplicationFilterType;
        private string duplicationFilterTypeAr;

        public int DuplicationFilterTypeId
        {
            get { return duplicationFilterTypeId; }
            set { duplicationFilterTypeId = value; }

        }

        public string DuplicationFilterTypeAr
        {
            get { return duplicationFilterTypeAr; }
            set { duplicationFilterTypeAr = value; }
        }
        public string DuplicationFilterType
        {
            get { return duplicationFilterType; }
            set { duplicationFilterType = value; }
        }

        public override int GetIdentity()
        {
            return duplicationFilterTypeId;
        }

        public override void SetIdentity(int value)
        {
            duplicationFilterTypeId = value;
            
        }

        public override void SetParentId(object value, Type ParentType)
        {
            if (ParentType == typeof(DuplicationRule))
                duplicationFilterTypeId = Convert.ToInt32(value);
        }
    }
}
