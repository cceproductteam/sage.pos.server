﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SagePOS.Server.Business.Entity
{
    public class CustomerLite
    {

        public string label { get; set; }
        public int value { get; set; }

        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string CustomerNumber { get; set; }
        public string CustomerName { get; set; }
        public int CustomerGroupId { get; set; }
        public string CustomerGroup { get; set; }
        public bool OnHoldValue { get; set; }
        public string OnHold { get; set; }
        public string Status { get; set; }
        public bool StatusValue { get; set; }

        public string ShortName { get; set; }
        public DateTime StartDate { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string AddressLine4 { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string StateProv { get; set; }
        public string PostalCode { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        
        public string Contact { get; set; }
        public string ContactTelephone { get; set; }
        public string ContactFax { get; set; }
        public string ContactEmail { get; set; }
        
        public int AccountTypeId { get; set; }
        public string AccountType
        {
            get
            {
                if (SagePOS.Server.Configuration.Configuration.Lang == SagePOS.Server.Configuration.Enums_S3.Configuration.Language.ar)
                    return AccountTypeAr;
                else return AccountTypeEn;
            }
        }
        public string AccountTypeAr { get; set; }
        public string AccountTypeEn { get; set; }
        public string PrintStatements { get; set; }
        public bool PrintStatementsValue { get; set; }

        public int AccountSetId { get; set; }
        public string AccountSet { get; set; }
        public int PaymentTermCodeId { get; set; }
        public string PaymentTermCode { get; set; }
        public int PaymentCodeId { get; set; }
        public string PaymentCode { get; set; }
        public int DeliveryMethodId { get; set; }
        public string DeliveryMethod
        {
            get
            {
                if (SagePOS.Server.Configuration.Configuration.Lang == SagePOS.Server.Configuration.Enums_S3.Configuration.Language.ar)
                    return DeliveryMethodAr;
                else return DeliveryMethodEn;
            }
        }
        public string DeliveryMethodAr { get; set; }
        public string DeliveryMethodEn { get; set; }
        public string OutstandingBalanceExceedsLimit { get; set; }
        public bool OutstandingBalanceExceedsLimitValue { get; set; }

        public double CreditLimitAmount { get; set; }
        public string TransactionsOverdueNdaysExceedOverdueLimit { get; set; }
        public bool TransactionsOverdueNdaysExceedOverdueLimitValue { get; set; }

        public int NDays { get; set; }
        public double OverDueLimit { get; set; }
        
        public int CustomerTypeId { get; set; }
        public string CustomerType
        {
            get
            {
                if (SagePOS.Server.Configuration.Configuration.Lang == SagePOS.Server.Configuration.Enums_S3.Configuration.Language.ar)
                    return CustomerTypeAr;
                else return CustomerTypeEn;
            }
        }
        public string CustomerTypeAr { get; set; }
        public string CustomerTypeEn { get; set; }
        public int CheckDuplicatPosId { get; set; }
        public string CheckDuplicatPos
        {
            get
            {
                if (SagePOS.Server.Configuration.Configuration.Lang == SagePOS.Server.Configuration.Enums_S3.Configuration.Language.ar)
                    return CheckDuplicatPosAr;
                else return CheckDuplicatPosEn;
            }
        }
        public string CheckDuplicatPosAr { get; set; }
        public string CheckDuplicatPosEn { get; set; }
        public int TaxGroupId { get; set; }
        public string TaxGroup { get; set; }
        public int TaxAuthorityId { get; set; }
        public string TaxAuthority { get; set; }
        public int TaxClassId { get; set; }
        public string TaxClass { get; set; }
        public int SalesPersonId { get; set; }
        public string SalesPerson { get; set; }
        public double Percentage { get; set; }


        public bool LastRows { get; set; }
        public int TotalRecords { get; set; }

        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public int CreatedById { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int UpdatedById { get; set; }
        public DateTime? UpdatedDate { get; set; }

        public double CurrentExchangeRate { get; set; }
        public string CrCompanyName { get; set; }
        public int CrCompanyId { get; set; }

        public double TotalReceipts { get; set; }
        public double TotalInvoices { get; set; }
        public double CustomerBalance { get; set; }
        public int CustomerCurrencyId { get; set; }
        public string CustomerCurrency { get; set; }

        public string CustomerCurrencySymbol { get; set; }
    }

    public class POSIntegrationCustomer
    {
        public int CustomerId { get; set; }
        public string CustomerNumber { get; set; }
        public string CustomerName { get; set; }
        public int TaxClassId { get; set; }
        public double CreditLimitAmount { get; set; }
        public string ContactPhone { get; set; }
        public string ContactEmail { get; set; }
        public bool FlagDeleted
        {
            get;
            set;
        }

        public int TaxAuthorityId { get; set; }
        public int CurrencyId { get; set; }
        public bool taxable { get; set; }

    }
}