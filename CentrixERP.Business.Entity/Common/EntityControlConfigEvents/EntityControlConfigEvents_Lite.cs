using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SagePOS.Manger.Common.Business.Entity
{
  public class EntityControlConfigEvents_Lite
    {
        public int EntityControlConfigEventId { get; set; }
    
        public int EntityId { get; set; }
      
        public int EntityControlConfigId { get; set; }
       
        public int ControlEventId { get; set; }
       
        public string JsFunctionName { get; set; }
       
        public string JsFunctionBody{ get; set; }
       
        public int CreatedBy{ get; set; }
       
        public int UpdatedBy{ get; set; }
       
        public DateTime? CreatedDate{ get; set; }
       
        public DateTime? UpdatedDate{ get; set; }
       
        public bool? IsSystem{ get; set; }

        public bool? FlagDeleted { get; set; }

        public string EventName { get; set; }
        public int ControlId { get; set; }
        public string EntityName { get; set; }
        public string ControlPropertyName { set; get; }
    }
}
