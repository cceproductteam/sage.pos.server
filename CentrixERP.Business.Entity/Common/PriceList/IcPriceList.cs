using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
using SagePOS.Manger.Common.Business.Entity;

namespace SagePOS.Server.Business.Entity
{
    [EntityIManager("Centrix.Inventory.Business.IManager.IIcPriceListManager,SagePOS.Server.Business.IManager", isLogicalDelete = true)]
    public class IcPriceList : EntityBase<IcPriceList, int>
    {
        #region Constructors

        public IcPriceList() { }

        public IcPriceList(int priceListId)
        {
            this.priceListId = priceListId;
        }

        #endregion

        #region Private Properties

        private int priceListId;

        private string priceListCode;

        private string priceListDescription;

        private int priceBy = -1;

        private int priceDecimals = -1;

        private int roundingMethod = -1;

        private double? roundToMultipleOf;

        private int sellingPriceBasedOn = -1;

        private int discountOnPriceBy = -1;

        private int pricingDeterminedBy = -1;

        private int createdBy;

        private DateTime? createdDate;

        private int updatedBy;

        private DateTime? updatedDate;

        private bool? flagDeleted;

        private bool? isSystem;
        

        //private bool? flagDeleted;

        //private bool? isSystem;

        //private int createdBy;

        //private int updatedBy;

        //private DateTime? createdDate;

        //private DateTime? updatedDate;
        #endregion

        #region Public Properties

        [PrimaryKey("@price_list_id", SqlDbType.Int)]
        [Property("price_list_id", SqlDbType.Int, AddParameter = false)]
        public int PriceListId
        {
            get
            {
                return priceListId;
            }
            set
            {
                priceListId = value;
            }
        }

        [Property("price_list_code", SqlDbType.NVarChar)]
        [PropertyConstraint(true, "PriceListCode", MaxLength = 100, MinLength = 3)]
        public string PriceListCode
        {
            get
            {
                return priceListCode;
            }
            set
            {
                priceListCode = value;
            }
        }

        [Property("price_list_description", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "PriceListDescription", MaxLength = 100, MinLength = 3)]
        public string PriceListDescription
        {
            get
            {
                return priceListDescription;
            }
            set
            {
                priceListDescription = value;
            }
        }

        [Property("price_by", SqlDbType.Int, ReferenceParameter = true)]
        [PropertyConstraint(true, "PriceBy", RefEntity = true, RefPropertyEntity = "PriceByObj")]
        public int PriceBy
        {
            get
            {
                return priceBy;
            }
            set
            {
                priceBy = value;
            }
        }

        [Property("price_decimals", SqlDbType.Int, ReferenceParameter = true)]
        [PropertyConstraint(true, "PriceDecimals", RefEntity = true, RefPropertyEntity = "PriceDecimalsObj")]
        public int PriceDecimals
        {
            get
            {
                return priceDecimals;
            }
            set
            {
                priceDecimals = value;
            }
        }

        [Property("rounding_method", SqlDbType.Int, ReferenceParameter = true)]
        [PropertyConstraint(true, "RoundingMethod", RefEntity = true, RefPropertyEntity = "RoundingMethodObj")]
        public int RoundingMethod
        {
            get
            {
                return roundingMethod;
            }
            set
            {
                roundingMethod = value;
            }
        }
        
        [Property("round_to_multiple_of", SqlDbType.Float)]
        [PropertyConstraint(false, "RoundToMultipleOf")]
        public double? RoundToMultipleOf
        {
            get { return roundToMultipleOf; }
            set { roundToMultipleOf = value; }
        }
        
        [Property("selling_price_based_on", SqlDbType.Int, ReferenceParameter = true)]
        [PropertyConstraint(true, "SellingPriceBasedOn", RefEntity = true, RefPropertyEntity = "SellingPriceBasedOnObj")]
        public int SellingPriceBasedOn
        {
            get
            {
                return sellingPriceBasedOn;
            }
            set
            {
                sellingPriceBasedOn = value;
            }
        }

        [Property("discount_on_price_by", SqlDbType.Int, ReferenceParameter = true)]
        [PropertyConstraint(true, "DiscountOnPriceBy", RefEntity = true, RefPropertyEntity = "DiscountOnPriceByObj")]
        public int DiscountOnPriceBy
        {
            get
            {
                return discountOnPriceBy;
            }
            set
            {
                discountOnPriceBy = value;
            }
        }

        [Property("pricing_determined_by", SqlDbType.Int, ReferenceParameter = true)]
        [PropertyConstraint(true, "PricingDeterminedBy", RefEntity = true, RefPropertyEntity = "PricingDeterminedByObj")]
        public int PricingDeterminedBy
        {
            get
            {
                return pricingDeterminedBy;
            }
            set
            {
                pricingDeterminedBy = value;
            }
        }



        [Property("flag_deleted", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [Property("is_system", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool? IsSystem
        {
            get
            {
                return isSystem;
            }
            set
            {
                isSystem = value;
            }
        }

        [Property("created_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? CreatedDate
        {
            get
            {
                return createdDate;
            }
            set
            {
                createdDate = value;
            }
        }

        [Property("updated_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? UpdatedDate
        {
            get
            {
                return updatedDate;
            }
            set
            {
                updatedDate = value;
            }
        }

        [Property("created_by", SqlDbType.Int, AddParameter = true, UpdateParameter = false)]
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        [Property("updated_by", SqlDbType.Int, AddParameter = false, UpdateParameter = true)]
        public int UpdatedBy
        {
            get
            {
                return updatedBy;
            }
            set
            {
                updatedBy = value;
            }
        }              

        #endregion

        #region Composite Objects
        private DataTypeContent priceByObj;
        [CompositeObject(CompositObjectTypeEnum.Single, "PriceBy", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable = false)]
        public DataTypeContent PriceByObj
        {
            get
            {
                if (priceByObj == null)
                    loadCompositObject_Lazy("PriceByObj");
                return priceByObj;
            }
            set
            {
                priceByObj = value; ;
            }
        }

        private DataTypeContent priceDecimalsObj;
        [CompositeObject(CompositObjectTypeEnum.Single, "PriceDecimals", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable = false)]
        public DataTypeContent PriceDecimalsObj
        {
            get
            {
                if (priceDecimalsObj == null)
                    loadCompositObject_Lazy("PriceDecimalsObj");
                return priceDecimalsObj;
            }
            set
            {
                priceDecimalsObj = value; ;
            }
        }

        private DataTypeContent roundingMethodObj;
        [CompositeObject(CompositObjectTypeEnum.Single, "RoundingMethod", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable = false)]
        public DataTypeContent RoundingMethodObj
        {
            get
            {
                if (roundingMethodObj == null)
                    loadCompositObject_Lazy("RoundingMethodObj");
                return roundingMethodObj;
            }
            set
            {
                roundingMethodObj = value; ;
            }
        }

        private DataTypeContent sellingPriceBasedOnObj;
        [CompositeObject(CompositObjectTypeEnum.Single, "SellingPriceBasedOn", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable = false)]
        public DataTypeContent SellingPriceBasedOnObj
        {
            get
            {
                if (sellingPriceBasedOnObj == null)
                    loadCompositObject_Lazy("SellingPriceBasedOnObj");
                return sellingPriceBasedOnObj;
            }
            set
            {
                sellingPriceBasedOnObj = value; ;
            }
        }

        private DataTypeContent discountOnPriceByObj;
        [CompositeObject(CompositObjectTypeEnum.Single, "DiscountOnPriceBy", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable = false)]
        public DataTypeContent DiscountOnPriceByObj
        {
            get
            {
                if (discountOnPriceByObj == null)
                    loadCompositObject_Lazy("DiscountOnPriceByObj");
                return discountOnPriceByObj;
            }
            set
            {
                discountOnPriceByObj = value; ;
            }
        }

        private DataTypeContent pricingDeterminedByObj;
        [CompositeObject(CompositObjectTypeEnum.Single, "PricingDeterminedBy", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable = false)]
        public DataTypeContent PricingDeterminedByObj
        {
            get
            {
                if (pricingDeterminedByObj == null)
                    loadCompositObject_Lazy("PricingDeterminedByObj");
                return pricingDeterminedByObj;
            }
            set
            {
                pricingDeterminedByObj = value; ;
            }
        }

        private List<PriceListDiscount> priceListDiscountIdObj;
        [CompositeObject(CompositObjectTypeEnum.List, "PriceListId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable = true)]
        public List<PriceListDiscount> PriceListDiscounts
        {
            get
            {
                if (priceListDiscountIdObj == null)
                    loadCompositObject_Lazy("PriceListDiscounts");
                return priceListDiscountIdObj;
            }
            set
            {
                priceListDiscountIdObj = value; ;
            }
        }


        #endregion


        public override int GetIdentity()
        {
            return priceListId;
        }
        public override void SetIdentity(int value)
        {
            priceListId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            throw new NotImplementedException();
        }
    }
}