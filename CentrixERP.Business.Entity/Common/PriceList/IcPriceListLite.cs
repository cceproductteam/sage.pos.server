using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace SagePOS.Server.Business.Entity
{
    public class IcPriceListLite : EntityBaseLite
    {

        #region Public Properties

        [LiteProperty("price_list_id", SqlDbType.Int)]
        public int PriceListId
        {
            get;
            set;
        }

        [LiteProperty("price_list_code", SqlDbType.NVarChar)]
        public string PriceListCode
        {
            get;
            set;
        }

        [LiteProperty("price_list_description", SqlDbType.NVarChar)]
        public string PriceListDescription
        {
            get;
            set;
        }

        [LiteProperty("price_by_id", SqlDbType.Int)]
        public int PriceById
        {
            get;
            set;
        }

        private string priceBy;

        private string priceByAr;

        [LiteProperty("price_by", SqlDbType.NVarChar)]
        public string PriceBy
        {
            get { return SagePOS.Server.Configuration.Configuration.Lang == SagePOS.Server.Configuration.Enums_S3.Configuration.Language.ar ? priceByAr : priceBy; }

            set { priceBy = value; }
        }

        [LiteProperty("price_by_ar", SqlDbType.NVarChar)]
        public string PriceByAr
        {
            get { return priceByAr; }

            set { priceByAr = value; }
        }

        [LiteProperty("price_decimals_id", SqlDbType.Int)]
        public int PriceDecimalsId
        {
            get;
            set;
        }

        private string priceDecimals;

        private string priceDecimalsAr;

        [LiteProperty("price_decimals", SqlDbType.NVarChar)]
        public string PriceDecimals
        {
            get { return SagePOS.Server.Configuration.Configuration.Lang == SagePOS.Server.Configuration.Enums_S3.Configuration.Language.ar ? priceDecimalsAr : priceDecimals; }

            set { priceDecimals = value; }
        }

        [LiteProperty("price_decimals_ar", SqlDbType.NVarChar)]
        public string PriceDecimalsAr
        {
            get { return priceDecimalsAr; }

            set { priceDecimalsAr = value; }
        }

        [LiteProperty("rounding_method_id", SqlDbType.Int)]
        public int RoundingMethodId
        {
            get;
            set;
        }

        private string roundingMethod;

        private string roundingMethodAr;

        [LiteProperty("rounding_method", SqlDbType.NVarChar)]
        public string RoundingMethod
        {
            get { return SagePOS.Server.Configuration.Configuration.Lang == SagePOS.Server.Configuration.Enums_S3.Configuration.Language.ar ? roundingMethodAr : roundingMethod; }

            set { roundingMethod = value; }
        }

        [LiteProperty("round_to_multiple_of", SqlDbType.Float)]
        public double? RoundToMultipleOf
        {
            get;
            set;
        }

        [LiteProperty("rounding_method_ar", SqlDbType.NVarChar)]
        public string RoundingMethodAr
        {
            get { return roundingMethodAr; }

            set { roundingMethodAr = value; }
        }

        [LiteProperty("selling_price_based_on_id", SqlDbType.Int)]
        public int SellingPriceBasedOnId
        {
            get;
            set;
        }

        private string sellingPriceBasedOn;

        private string sellingPriceBasedOnAr;

        [LiteProperty("selling_price_based_on", SqlDbType.NVarChar)]
        public string SellingPriceBasedOn
        {
            get { return SagePOS.Server.Configuration.Configuration.Lang == SagePOS.Server.Configuration.Enums_S3.Configuration.Language.ar ? sellingPriceBasedOnAr : sellingPriceBasedOn; }

            set { sellingPriceBasedOn = value; }
        }

        [LiteProperty("selling_price_based_on_ar", SqlDbType.NVarChar)]
        public string SellingPriceBasedOnAr
        {
            get { return sellingPriceBasedOnAr; }

            set { sellingPriceBasedOnAr = value; }
        }

        [LiteProperty("discount_on_price_by_id", SqlDbType.Int)]
        public int DiscountOnPriceById
        {
            get;
            set;
        }

        private string discountOnPriceBy;

        private string discountOnPriceByAr;

        [LiteProperty("discount_on_price_by", SqlDbType.NVarChar)]
        public string DiscountOnPriceBy
        {
            get { return SagePOS.Server.Configuration.Configuration.Lang == SagePOS.Server.Configuration.Enums_S3.Configuration.Language.ar ? discountOnPriceByAr : discountOnPriceBy; }

            set { discountOnPriceBy = value; }
        }

        [LiteProperty("discount_on_price_by_ar", SqlDbType.NVarChar)]
        public string DiscountOnPriceByAr
        {
            get { return discountOnPriceByAr; }

            set { discountOnPriceByAr = value; }
        }

        [LiteProperty("pricing_determined_by_id", SqlDbType.Int)]
        public int PricingDeterminedById
        {
            get;
            set;
        }

        private string pricingDeterminedBy;

        private string pricingDeterminedByAr;

        [LiteProperty("pricing_determined_by", SqlDbType.NVarChar)]
        public string PricingDeterminedBy
        {
            get { return SagePOS.Server.Configuration.Configuration.Lang == SagePOS.Server.Configuration.Enums_S3.Configuration.Language.ar ? pricingDeterminedByAr : pricingDeterminedBy; }

            set { pricingDeterminedBy = value; }
        }

        [LiteProperty("pricing_determined_by_ar", SqlDbType.NVarChar)]
        public string PricingDeterminedByAr
        {
            get { return pricingDeterminedByAr; }

            set { pricingDeterminedByAr = value; }
        }    
        
        [LiteProperty("price_list_id", SqlDbType.Int)]
        public int value
        {
            get;
            set;
        }

        [LiteProperty("price_list_code", SqlDbType.NVarChar)]
        public string label
        {
            get;
            set;
        }

        [LiteProperty("flag_deleted", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? FlagDeleted
        {
            get;
            set;
        }

        [LiteProperty("is_system", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? IsSystem
        {
            get;
            set;
        }

        [LiteProperty("created_date", SqlDbType.DateTime)]
        public DateTime? CreatedDate
        {
            get;
            set;
        }

        [LiteProperty("updated_date", SqlDbType.DateTime)]
        public DateTime? UpdatedDate
        {
            get;
            set;
        }

        [LiteProperty("created_by", SqlDbType.Int, FindByIdLiteParameter = false)]
        public int CreatedBy
        {
            get;
            set;
        }

        [LiteProperty("updated_by", SqlDbType.Int, FindByIdLiteParameter = false)]
        public int? UpdatedBy
        {
            get;
            set;
        }

        [LiteProperty("created_by_name", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string CreatedByName { get; set; }

        [LiteProperty("updated_by_name", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string UpdatedByName { get; set; }

        [LiteProperty("price_list_id", SqlDbType.NVarChar)]
        public int Id
        {
            get;
            set;
            //get { return this.PriceListId; }
            //set { this.PriceListId = value; }
        }

        [LiteProperty("last_rows", SqlDbType.NVarChar)]
        public bool LastRows { get; set; }

        [LiteProperty("total_count", SqlDbType.NVarChar)]
        public int TotalRecords { get; set; }
        #endregion


    }



}