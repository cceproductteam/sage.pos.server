using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
using SagePOS.Server.Business.Factory ;

namespace SagePOS.Server.Business.Entity
{
    public class PriceListDiscountCriteria : CriteriaBase, IAdvancedSearchCriteria
    {
        [CrtiteriaField("@page_number", SqlDbType.Int)]
        public int pageNumber { get; set; }

        [CrtiteriaField("@result_count", SqlDbType.Int)]
        public int resultCount { get; set; }

        [CrtiteriaField("@keyword", SqlDbType.NVarChar)]
        public string Keyword { get; set; }

        [CrtiteriaField("@search_criteria", SqlDbType.NVarChar)]
        public string SearchCriteria { get; set; }

        [CrtiteriaField("@sort_type", SqlDbType.Bit)]
        public bool SortType { get; set; }

        [CrtiteriaField("@sort_fields", SqlDbType.NVarChar)]
        public string SortFields { get; set; }

        [CrtiteriaField("@price_list_discount_id", SqlDbType.Int, IsReferance = true)]
        public int? DiscountId { get; set; }

        [CrtiteriaField("@entity_id", SqlDbType.Int, IsReferance = true)]
        public int EntityId { get; set; }

        [CrtiteriaField("@price_list_id", SqlDbType.Int, IsReferance = true)]
        public int? PriceListId { get; set; }
    }
}
