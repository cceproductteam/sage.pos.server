using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
using SagePOS.Manger.Common.Business.Entity;

namespace SagePOS.Server.Business.Entity
{
    [EntityIManager("Centrix.Inventory.Business.IManager.IIcPriceListDiscountManager,SagePOS.Server.Business.IManager", isLogicalDelete = true)]
    public class PriceListDiscount : EntityBase<PriceListDiscount, int>
    {
        #region Constructors

        public PriceListDiscount() { }

        public PriceListDiscount(int discountId)
        {
            this.discountId = discountId;
        }

        #endregion

        #region Private Properties

        private int discountId;

        private int priceListId = -1;

        private int customerClass = -1;

        private double? discount;

        private double? quantity;

        private bool? flagDeleted;

        private bool? isSystem;

        private int createdBy;

        private int updatedBy;

        private DateTime? createdDate;

        private DateTime? updatedDate;

        #endregion

        #region Public Properties

        [PrimaryKey("@price_list_discount_id", SqlDbType.Int)]
        [Property("price_list_discount_id", SqlDbType.Int, AddParameter = false)]
        public int DiscountId
        {
            get
            {
                return discountId;
            }
            set
            {
                discountId = value;
            }
        }
        
        [Property("price_list_id", SqlDbType.Int, ReferenceParameter = true)]
        [PropertyConstraint(true, "PriceList", RefEntity = true, RefPropertyEntity = "IcPriceListObj")]
        public int PriceListId
        {
            get { return priceListId; }

            set { priceListId = value; }
        }

        [Property("customer_class", SqlDbType.Int, ReferenceParameter = true)]
        [PropertyConstraint(true, "CustomerClass", RefEntity = true, RefPropertyEntity = "CustomerClassObj")]
        public int CustomerClass
        {
            get
            {
                return customerClass;
            }
            set
            {
                customerClass = value;
            }
        }

        [Property("discount", SqlDbType.Float)]
        [PropertyConstraint(false, "Discount")]
        public double? Discount
        {
            get
            {
                return discount;
            }
            set
            {
                discount = value;
            }
        }

        [Property("quantity", SqlDbType.Float)]
        [PropertyConstraint(true, "Quantity")]
        public double? Quantity
        {
            get
            {
                return quantity;
            }
            set
            {
                quantity = value;
            }
        }
        
        [Property("flag_deleted", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [Property("is_system", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool? IsSystem
        {
            get
            {
                return isSystem;
            }
            set
            {
                isSystem = value;
            }
        }

        [Property("created_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? CreatedDate
        {
            get
            {
                return createdDate;
            }
            set
            {
                createdDate = value;
            }
        }

        [Property("updated_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? UpdatedDate
        {
            get
            {
                return updatedDate;
            }
            set
            {
                updatedDate = value;
            }
        }

        [Property("created_by", SqlDbType.Int, AddParameter = true, UpdateParameter = false)]
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        [Property("updated_by", SqlDbType.Int, AddParameter = false, UpdateParameter = true)]
        public int UpdatedBy
        {
            get
            {
                return updatedBy;
            }
            set
            {
                updatedBy = value;
            }
        }

        #endregion

        #region Composite Objects
        private DataTypeContent customerClassObj;
        [CompositeObject(CompositObjectTypeEnum.Single, "CustomerClass", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable = false)]
        public DataTypeContent CustomerClassObj
        {
            get
            {
                if (customerClassObj == null)
                    loadCompositObject_Lazy("CustomerClassObj");
                return customerClassObj;
            }
            set
            {
                customerClassObj = value;
            }
        }
        private IcPriceList icPriceListObj;
        [CompositeObject(CompositObjectTypeEnum.Single, "PriceListId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable = false)]
        public IcPriceList IcPriceListObj
        {
            get
            {
                if (icPriceListObj == null)
                    loadCompositObject_Lazy("IcPriceListObj");
                return icPriceListObj;
            }
            set
            {
                icPriceListObj = value;
            }
        }
        #endregion


        public override int GetIdentity()
        {
            return discountId;
        }
        public override void SetIdentity(int value)
        {
            discountId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            if (ParentType == typeof(IcPriceList))
                this.priceListId = (int)value;
        }
    }
}