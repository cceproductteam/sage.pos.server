using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace SagePOS.Server.Business.Entity
{
     public class PriceListDiscountLite : EntityBaseLite
    {      
        
        #region Public Properties

        [LiteProperty("price_list_discount_id",SqlDbType.Int)]
		public int DiscountId
		{
			get;
			set;
		}

        [LiteProperty("price_list_id", SqlDbType.Int)]
        public int PriceListId
        {
            get;
            set;
        }
        [LiteProperty("price_list", SqlDbType.NVarChar)]
        public string PriceList
        {
            get;
            set;
        }

        [LiteProperty("customer_class_id", SqlDbType.Int)]
        public int CustomerClassId
        {
            get;
            set;
        }
		private string customerClass;

		private string customerClassAr;

        [LiteProperty("customer_class", SqlDbType.Int)]
        public string CustomerClass
        {
            get { return SagePOS.Server.Configuration.Configuration.Lang == SagePOS.Server.Configuration.Enums_S3.Configuration.Language.ar ? customerClassAr : customerClass; }

            set { customerClass = value; }
        }

		[LiteProperty("customer_class_ar",SqlDbType.NVarChar)]
		public string CustomerClassAr
		{
			get{return customerClassAr;}
            
			set{ customerClassAr= value; }
		}

		[LiteProperty("discount",SqlDbType.Float)]
		public double? Discount
		{
			get;
			set;
		}

		[LiteProperty("quantity",SqlDbType.Float)]
		public double? Quantity
		{
			get;
			set;
		}
         
        [LiteProperty("price_list_discount_id", SqlDbType.Int)]
        public int value
        {
            get;
            set;
        }

          [LiteProperty("price_list_discount_id", SqlDbType.Int)]
        public int label
        {
            get;
            set;
        }

         [LiteProperty("flag_deleted", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? FlagDeleted
        {
            get;
            set;
        }

        [LiteProperty("is_system",SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
		public bool? IsSystem
		{
			get;
			set;
		}

		[LiteProperty("created_date",SqlDbType.DateTime)]
		public DateTime? CreatedDate
		{
			get;
			set;
		}

		[LiteProperty("updated_date",SqlDbType.DateTime)]
		public DateTime? UpdatedDate
		{
			get;
			set;
		}

		[LiteProperty("created_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int CreatedBy
		{
			get;
			set;
		}

		[LiteProperty("updated_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int UpdatedBy
		{
			get;
			set;
		}

         [LiteProperty("created_by_name",SqlDbType.NVarChar, FindAllLiteParameter = false)]
         public string CreatedByName{get;set;}

         [LiteProperty("updated_by_name",SqlDbType.NVarChar,FindAllLiteParameter = false)]
         public string UpdatedByName{get;set;}

         [LiteProperty("price_list_discount_id",SqlDbType.NVarChar)]
          public int Id
        {
            get{return this.DiscountId; }
            set{this.DiscountId=value;}
        }

         [LiteProperty("last_rows",SqlDbType.NVarChar)]
         public bool LastRows { get; set; }

         [LiteProperty("total_count",SqlDbType.NVarChar)]
         public int TotalRecords { get; set; }
        #endregion
       
          
     }
}