using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;

namespace SagePOS.Manger.Common.Business.Entity
{
    public class CountryCriteria : CriteriaBase
    {
        public string Name;
        public int pageNumber;
        public int resultCount;
        public int from;
        public int to;
    }
}
