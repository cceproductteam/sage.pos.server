using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;

namespace SagePOS.Manger.Common.Business.Entity
{
    [EntityIManager("CentrixERP.Common.Business.IManager.ICityManager,SagePOS.Server.Business.IManager", isLogicalDelete = false)]
    public class City : EntityBase<City, int>
    {
        private int cityId = -1;
        private int countryId = -1;
        private string cityNameEn;
        private string cityNameAr;
        //private List<Address> addressList;

        public int CityId
        {
            get { return cityId; }
            set { cityId = value; }
        }
        public int CountryId
        {
            get { return countryId; }
            set { countryId = value; }
        }
        public string CityNameEn
        {
            get { return cityNameEn; }
            set { cityNameEn = value; }
        }
        public string CityNameAr
        {
            get { return cityNameAr; }
            set { cityNameAr = value; }
        }

        public override int GetIdentity()
        {
            return cityId;
        }

        public override void SetIdentity(int value)
        {
            cityId = value;
        }


        //[CompositeObject(CompositObjectTypeEnum.List, "CityID", LazyLoad = true, CascadeDelete = true)]
        //public List<Address> AddressList
        //{
        //    get
        //    {
        //        if (AddressList == null)
        //            loadCompositObject_Lazy("AddressList");
        //        return AddressList;
        //    }
        //    set { AddressList = value; }
        //}




        public override void SetParentId(object value, Type ParentType)
        {
            if (ParentType == typeof(Country))
            {
                countryId = Convert.ToInt32(value);
            }
        }
    }
}

