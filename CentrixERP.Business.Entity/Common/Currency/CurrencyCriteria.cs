using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Factory ;

namespace CentrixERP.AdministrativeSettings.Business.Entity
{
   public class CurrencyCriteria: CriteriaBase,IAdvancedSearchCriteria
    {
       public string Code;
       public string Description;
       public int ExceptCurrencyId = -1;
       public int Page;
       public int resultCount= -1;
       public int CurrencyId;
       public bool? IsFunctional { get; set; }
       #region IAdvancedSearchCriteria Members

       public int EntityId { get; set; }

       public string SearchCriteria { get; set; }
       public string SortFields { get; set; }

       public bool SortType { get; set; }
       public int pageNumber { get; set; }

       int IAdvancedSearchCriteria.resultCount { get; set; }

       #endregion
    }

  
}


