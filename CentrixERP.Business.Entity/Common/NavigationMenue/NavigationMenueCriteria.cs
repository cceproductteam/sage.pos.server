using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;

namespace SagePOS.Manger.Common.Business.Entity
{
    public class NavigationMenueCriteria:CriteriaBase 
    {
        public string Name;
        public int UserId = -1;
        public int Category = -1;
        public int? RoleId { get; set; }
    }
}
