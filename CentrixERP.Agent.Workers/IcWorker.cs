﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Centrix.Inventory.Business.IManager;
using SF.Framework;
using Centrix.Inventory.Business.Entity;
using CentrixERP.Configuration;
using System.Configuration;

namespace CentrixERP.Agent.Workers
{
    public class IcWorker
    {
        IUploadItemsLogManager uploadLogManger = (IUploadItemsLogManager)IoC.Instance.Resolve(typeof(IUploadItemsLogManager));
        public void DoWork()
        {

            List<UploadItemsLogLite> uploadListAll = uploadLogManger.FindAllLite(new UploadItemsLogCriteria() { StatusId = 1 });

            List<UploadItemsLogLite> uploadList = null;
            if (uploadListAll != null && uploadListAll.Count() > 0) uploadList = (from item in uploadListAll orderby item.UploadItemsLogId ascending select item).ToList();
         
            if (uploadList != null && uploadList.Count() > 0)
            {
                List<UploadItemsLogLite> inprogressList = (from item in uploadList where item.UploadInProgress select item).ToList();

                if (inprogressList == null || inprogressList.Count() <= 0)
                {

                    UploadItemsLogLite item = uploadList.First();
                    UploadItemsLog obj = uploadLogManger.FindById(item.UploadItemsLogId, null);
                    try
                    {
                        obj.UploadInProgress = true;
                        obj.MarkOld();
                        obj.MarkModified();
                        uploadLogManger.Save(obj);

                        bool isAdjLog = false;

                        Dictionary<string, string> result = new Dictionary<string, string>();
                        if (obj != null)
                        {
                            switch (obj.TypeId)
                            {
                                case (int)Enums_S3.MigrationToolKeys.UploadTypes.Items:
                                    IcItemsMigration itemMigration = new IcItemsMigration();
                                   result = itemMigration.DoWork(item.FilePath, ConfigurationManager.AppSettings["UploadItemsErrorLog"].ToString());
                                  //  itemMigration.DoWork(item.FilePath, ConfigurationManager.AppSettings["UploadItemsErrorLog"].ToString());
                                    break;
                                case (int)Enums_S3.MigrationToolKeys.UploadTypes.Adjustment:
                                    IcAdjustmentMigration adjustmentMigration = new IcAdjustmentMigration();
                                  result = adjustmentMigration.DoWork(item.FilePath, ConfigurationManager.AppSettings["UploadItemsErrorLog"].ToString());
                                //    adjustmentMigration.DoWork(item.FilePath, ConfigurationManager.AppSettings["UploadItemsErrorLog"].ToString());
                                    break;

                            }

                            foreach (KeyValuePair<string, string> val in result)
                            {
                                if (val.Key == "result")
                                    obj.StatusId = (Convert.ToBoolean(val.Value)) ? (int)Enums_S3.MigrationToolKeys.UploadLogStatus.Completed : (int)Enums_S3.MigrationToolKeys.UploadLogStatus.Pending;
                                else if (val.Key == "LogPath" || val.Key == "AdjLogPath")
                                {
                                    if (val.Key == "AdjLogPath")
                                        isAdjLog = true;
                                    else
                                        isAdjLog = false;

                                    obj.LogFileName = val.Value;
                                }
                            }
                            if (!string.IsNullOrEmpty(obj.LogFileName))
                            {
                                if (isAdjLog)
                                    obj.StatusId = (int)Enums_S3.MigrationToolKeys.UploadLogStatus.Completed;
                                else
                                    obj.StatusId = (int)Enums_S3.MigrationToolKeys.UploadLogStatus.Failed;
                            }
                            obj.UploadInProgress = false;
                            obj.MarkOld();
                            obj.MarkModified();
                            uploadLogManger.Save(obj);
                        }
                    }
                    catch (Exception)
                    {
                        UploadItemsLogUpdate();
                    }
                }

            }
        }

        public void UploadItemsLogUpdate() {
            uploadLogManger.UploadItemsLogUpdate();
        }
    }
}