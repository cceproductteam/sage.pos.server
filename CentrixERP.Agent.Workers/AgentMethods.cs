using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Centrix.Notifications.Business.IManager;
using Centrix.Notifications.Business.Entity;
using SF.Framework;
using System.Text.RegularExpressions;
using System.Reflection;

namespace CentrixERP.Agent.Workers
{
    public class AgentMethods
    {
        INotificationQueueManager queueManager = null;
        //IUserManager userManager = null;
        int HightPriority = (int)CentrixERP.Configuration.Enums_S3.Priority.High;
        int pendingStatus = (int)CentrixERP.Configuration.Enums_S3.Notification.NotificationStatus.Pending;


        public void SendOnScreenNotification(int EntityId, int EntityValueId, string subject,string subject_ar, string text,string text_ar, int EscalationType)
        {
            IOnScreenNotificationManager iOnScreenNotificationManager = (IOnScreenNotificationManager)IoC.Instance.Resolve(typeof(IOnScreenNotificationManager));
            OnScreenNotification onScreenNotification = new OnScreenNotification();
            onScreenNotification.Subject = subject;
            //onScreenNotification.Subject = subject_ar;
            onScreenNotification.Test = text;
            //onScreenNotification.ContentAR = text_ar;
            onScreenNotification.NotificationDate = DateTime.Now;
            onScreenNotification.EntityId = EntityId;
            onScreenNotification.EntityValueId = EntityValueId;
            onScreenNotification.UpdatedDate = DateTime.Now;
            onScreenNotification.CreatedBy = 1;
            onScreenNotification.Priority = HightPriority;
            onScreenNotification.Status = pendingStatus;
            onScreenNotification.AllUsers = true;
            onScreenNotification.EscalationType = EscalationType; 
            iOnScreenNotificationManager.Save(onScreenNotification);

        }

        public Template GetTemplate(int template_number)
        {
            ITemplateManager templateManager = (ITemplateManager)IoC.Instance.Resolve(typeof(ITemplateManager));
            Template template = templateManager.FindByTemplateNumber(template_number, null);
            return template;
        }

        public string HandleTemplatedelimiter(string template, object Entity)
        {
            return Recursive(template, Entity);
        }

        public string Recursive(string templateCode, object Entity)
        {
            //string templateCode = template.TemplateCode;
            Regex regex = new Regex("%%(.*?)%%");
            var matchVariable = regex.Match(templateCode);
            string newTemplateCode = templateCode.Replace(matchVariable.ToString(), " " + GetPropertyValue(Entity, matchVariable.ToString().Replace("%", "")) + " ");
            //E.Template newTemplate = template;
            //newTemplate.TemplateCode = newTemplateCode;
            string s = matchVariable.Groups[1].ToString();

            var newMatchs = regex.Match(newTemplateCode);
            if (!string.IsNullOrEmpty(newMatchs.ToString()))
                return Recursive(newTemplateCode, Entity);
            else
                return newTemplateCode;
        }

        private string GetPropertyValue(object o, string propertyName)
        {
            Type type = o.GetType();
            PropertyInfo info = type.GetProperty(propertyName);
            // Type propType = info.PropertyType;
            object value = info.GetValue(o, null);

            if (value != null && (info.PropertyType == typeof(DateTime) || info.PropertyType == typeof(DateTime?)))
            {
                return ((DateTime)value).ToString("dd-MM-yyyy");
            }

            return value != null ? value.ToString() : "";
        }


    }
}
