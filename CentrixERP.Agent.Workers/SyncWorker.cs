using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using SF.Framework;
using System.Configuration;

using CentrixERP.Business.Entity;
using CentrixERP.Configuration;
using CentrixERP.Agent.Workers;
using CentrixERP.API;
using Centrix.POS.Server.API.POSSyncAPIs;
using Centrix.POS.Standalone.Client.Business.Entity;
using Centrix.Inventory.Business.Entity;
using Centrix.Inventory.Business.IManager;
using CX.Framework;
using CentrixERP.AccountsReceivable.Business.IManager;
using Centrix.POS.Standalone.Client.Business.IManager;
using CentrixERP.AccountsReceivable.Business.Entity;
using CentrixERP.AdministrativeSettings.Business.Entity;
using CentrixERP.AdministrativeSettings.Business.IManager;
using CentrixERP.Business.IManager;

namespace CentrixERP.Agent.Workers
{
    public class SyncWorker
    {

        ISyncScheduleManager syncMgr = null;
        IStoreManager iStoreManager = null;
        StringBuilder log = null;
        IIcShipmentManager iIcShipmentManager = null;
        IIcCommonQueriesManager iIcCommonQueries = null;
        IIcDocumentNumbersManager iIcDocumentNumbersManager = null;
        IBusinessManagerBase<IcItemCardSerial, int> iIcItemCardSerialManager = null;
        IBusinessManagerBase<IcItemCardSerialTransaction, int> iIcItemCardSerialTransactionManager = null;
        IIcLocationManager iIcLocationManager = null;
        IIcItemCardManager iIcItemCardManager = null;
        IIcAccountingIntegrationManager iIcAccountingIntegrationManager = null;
        IArInvoiceManager iArInvoiceManager = null;
       // IInvoiceManager iInvoiceManager = null;
        //IInvoiceItemManager iInvoiceItemManager = null;
        IPhysicalCalenderHeaderManager iPhysicalCalendarHeaderManager = null;
     
        public static bool IsBusy { get; set; }


        public SyncWorker()
        {
            syncMgr = (ISyncScheduleManager)SF.Framework.IoC.Instance.Resolve(typeof(ISyncScheduleManager));
            iStoreManager = (IStoreManager)SF.Framework.IoC.Instance.Resolve(typeof(IStoreManager));
            log = new StringBuilder();
            iIcShipmentManager = (IIcShipmentManager)CX.Framework.IoC.Instance.Resolve(typeof(IIcShipmentManager));
            iIcCommonQueries = (IIcCommonQueriesManager)CX.Framework.IoC.Instance.Resolve(typeof(IIcCommonQueriesManager));
            iIcDocumentNumbersManager = (IIcDocumentNumbersManager)SF.Framework.IoC.Instance.Resolve(typeof(IIcDocumentNumbersManager));
            iIcItemCardSerialManager = (IBusinessManagerBase<IcItemCardSerial, int>)CX.Framework.IoC.Instance.Resolve(typeof(IBusinessManagerBase<IcItemCardSerial, int>));
            iIcItemCardSerialTransactionManager = (IBusinessManagerBase<IcItemCardSerialTransaction, int>)CX.Framework.IoC.Instance.Resolve(typeof(IBusinessManagerBase<IcItemCardSerialTransaction, int>));
            iIcLocationManager = (IIcLocationManager)SF.Framework.IoC.Instance.Resolve(typeof(IIcLocationManager));
            iIcItemCardManager = (IIcItemCardManager)SF.Framework.IoC.Instance.Resolve(typeof(IIcItemCardManager));
            iIcAccountingIntegrationManager = (IIcAccountingIntegrationManager)CX.Framework.IoC.Instance.Resolve(typeof(IIcAccountingIntegrationManager));
            iArInvoiceManager = (IArInvoiceManager)SF.Framework.IoC.Instance.Resolve(typeof(IArInvoiceManager));
            //iInvoiceManager = (IInvoiceManager)SF.Framework.IoC.Instance.Resolve(typeof(IInvoiceManager));
            //iInvoiceItemManager = (IInvoiceItemManager)SF.Framework.IoC.Instance.Resolve(typeof(IInvoiceItemManager));
            iPhysicalCalendarHeaderManager = SF.Framework.IoC.Instance.Resolve<IPhysicalCalenderHeaderManager>();
         
        }

        public void DoWork()
        {
            SF.Framework.Logger.Instance.Info("Sync Worker, start do work");
            try
            {
                AddToSunc();
                BackEndSyncData();

            }
            catch (Exception ex)
            {

                SF.Framework.Logger.Instance.Info("sync work failed to AddToSunc");
                SF.Framework.Logger.Instance.Error("sync work failed to AddToSunc", ex);
                return;
            }
            List<SyncSchedule> syncLite = syncMgr.FindAllToSync((int)Enums_S3.BOSyncStatus.Pending);
            SF.Framework.Logger.Instance.Info("Sync Worker,  syncLite is null");
            if (syncLite != null)
            {
                foreach (var item in syncLite)
                {
                    if (item != null)
                    {
                        item.MarkOld();
                        item.MarkModified();
                        item.Status = (int)Enums_S3.BOSyncStatus.InProgress;
                        //bool saveStatus = false;
                        try
                        {
                            syncMgr.Save(item);
                            SF.Framework.Logger.Instance.Info("Sync Worker, save change status successfully");
                            //saveStatus = true;
                        }
                        catch (Exception ex)
                        {
                            SF.Framework.Logger.Instance.Info("Sync Worker, faild to save change status exception" + ex.Message);
                            //saveStatus = false;
                        }
                       // SyncItems(item);
                    }
                }

                Loggin.Log(log.ToString());
            }

            SF.Framework.Logger.Instance.Info("Sync Worker, finish do work");
        }

        //private void SyncItems(SyncSchedule SyncEntity)
        //{
        //    //
        //    SyncEntity.MarkOld();
        //    SyncEntity.MarkModified();
        //    bool successSync = false;

        //    try
        //    {
        //        //successSync = syncMgr.SynckEntities(SyncEntity);
        //        List<StoreLite> storeList = iStoreManager.FindAllLite(null);
        //        string[] Ids = SyncEntity.SyncEntitiesIds.Split(',');
        //        foreach (var item in Ids)
        //        {
        //            if (item == ((int)Enums_S3.SyncType.Invoice).ToString())
        //            {

        //                try
        //                {
        //                    //sync Invoices
        //                    SF.Framework.Logger.Instance.Info("start sync Invoices");
        //                    if (storeList == null || storeList.Count < 1)
        //                    {
        //                        SF.Framework.Logger.Instance.Info("there is no store, store list is null");
        //                        successSync = false;
        //                    }
        //                    else
        //                    {
        //                        foreach (var storeItem in storeList)
        //                        {
        //                            PostPOSInvoices(1);
        //                            PostPOSInvoices(7);
        //                            successSync = true;
        //                        }
        //                        SF.Framework.Logger.Instance.Info("end successfuly sync Invoices");
        //                    }
        //                }
        //                catch (Exception ex)
        //                {

        //                    successSync = false;
        //                    SF.Framework.Logger.Instance.Info("sync Invoices error :" + ex.Message);
        //                }

        //            }
        //            else if (item == ((int)Enums_S3.SyncType.Refund).ToString())
        //            {
        //                try
        //                {
        //                    //sync Refund
        //                    SF.Framework.Logger.Instance.Info("start sync Refund Invoices");
        //                    if (storeList == null || storeList.Count < 1)
        //                    {
        //                        SF.Framework.Logger.Instance.Info("there is no store, store list is null");
        //                        successSync = false;
        //                    }
        //                    else
        //                    {
        //                        foreach (var storeItem in storeList)
        //                        {
        //                            PostPOSInvoices(5);
        //                            successSync = true;
        //                        }
        //                        SF.Framework.Logger.Instance.Info("end successfuly sync Invoices");
        //                    }
        //                }
        //                catch (Exception ex)
        //                {
        //                    SF.Framework.Logger.Instance.Info("sync Refund Invoices error :" + ex.Message);
        //                    successSync = false;
        //                }
        //            }
        //            else if (item == ((int)Enums_S3.SyncType.PettyCash).ToString())
        //            {
        //                try
        //                {
        //                    //sync PettyCash
        //                    successSync = true;
        //                }
        //                catch (Exception)
        //                {

        //                    successSync = false;
        //                }
        //            }
        //        }

        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }

        //    if (successSync)
        //        SyncEntity.Status = (int)Enums_S3.BOSyncStatus.Succeed;
        //    else
        //        SyncEntity.Status = (int)Enums_S3.BOSyncStatus.Failed;

        //    try
        //    {
        //        syncMgr.Save(SyncEntity);
        //        log.AppendLine(string.Format("sync process success at {0}", DateTime.Now.ToString("dd-MM-yyyy hh:mm tt")));
        //    }
        //    catch (Exception ex)
        //    {

        //        log.AppendLine(string.Format("sync process faild with this inner exception : {0} at {1}", ex.ToString(), DateTime.Now.ToString("dd-MM-yyyy hh:mm tt")));
        //    }
        //}

        private bool mustTosync(string currentDate, string LastSyncDate)
        {
            try
            {
                if (Convert.ToDateTime(currentDate) > Convert.ToDateTime(LastSyncDate))
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {

                return false;
            }
        }
        private void AddToSunc()
        {
            IPosSyncConfigurationManager SyncConfigMGR = (IPosSyncConfigurationManager)SF.Framework.IoC.Instance.Resolve(typeof(IPosSyncConfigurationManager));
            List<PosSyncConfigurationLite> entityList = SyncConfigMGR.FindAllLite(null);
            SyncSchedule newItem = null;
            SF.Framework.Logger.Instance.Info("Sync Worker, start AddToSunc before loop");
            if (entityList == null)
                SF.Framework.Logger.Instance.Info("Sync Worker, start AddToSunc before loop entityList is null");
            else
            {
                foreach (var item in entityList)
                {
                    SF.Framework.Logger.Instance.Info("Sync Worker, start AddToSunc start loop");
                    string LastSyncDate = item.LastPosSyncDate;
                    string currentDate = DateTime.Now.Date.ToString("dd-MM-yyyy");
                    if (string.IsNullOrEmpty(item.LastPosSyncDate) || mustTosync(currentDate, LastSyncDate))
                    {
                        if ((DateTime.Now.Hour == (Convert.ToInt32(item.AccpacSyncTime1Id))) || (DateTime.Now.Hour == (Convert.ToInt32(item.AccpacSyncTime2Id))) || (DateTime.Now.Hour == (Convert.ToInt32(item.AccpacSyncTime3Id))) || (DateTime.Now.Hour == (Convert.ToInt32(item.AccpacSyncTime4Id))) || (DateTime.Now.Hour == (Convert.ToInt32(item.AccpacSyncTime5Id))))
                        {
                            newItem = new SyncSchedule();
                            newItem.Status =
                                (int)Enums_S3.BOSyncStatus.Pending;
                            newItem.SyncEntities = item.AccpacDataToSync;
                            newItem.SyncEntitiesIds = item.AccpacDataToSyncIds;
                            newItem.CreatedBy = 1;

                            try
                            {
                                syncMgr.Save(newItem);
                                SF.Framework.Logger.Instance.Info("Sync Worker, sync change status");
                            }
                            catch (Exception ex)
                            {

                                SF.Framework.Logger.Instance.Info("Sync Worker, sync Exception :" + ex.Message);
                            }


                            PosSyncConfiguration configEntity = SyncConfigMGR.FindById(item.PosSyncConfigurationId, null);
                            if (configEntity != null)
                            {
                                configEntity.MarkModified();
                                configEntity.LastPosSyncDate = currentDate;
                                SF.Framework.Logger.Instance.Info("Sync Worker, sync befor save");
                                SyncConfigMGR.Save(configEntity);
                                SF.Framework.Logger.Instance.Info("Sync Worker, sync after save");
                            }
                        }

                    }
                }
            }
        }

        private void BackEndSyncData()
        {
            IPosSyncConfigurationManager SyncConfigMGR = (IPosSyncConfigurationManager)SF.Framework.IoC.Instance.Resolve(typeof(IPosSyncConfigurationManager));
            List<PosSyncConfiguration> entityList = SyncConfigMGR.FindAll(null);
            SyncSchedule newItem = null;
            SF.Framework.Logger.Instance.Info("Sync Worker, start AddToSync before loop");
            if (entityList == null)
                SF.Framework.Logger.Instance.Info("Sync Worker, start AddToSunc before loop entityList is null");
            else
            {
                foreach (var item in entityList)
                {
                     SF.Framework.Logger.Instance.Info("Sync Worker, start AddToSunc start loop");
                     string LastErpSyncDate = item.LastErpSyncDate;
                    string currentDate = DateTime.Now.Date.ToString("dd-MM-yyyy");
                    if (string.IsNullOrEmpty(item.LastPosSyncDate) || mustTosync(currentDate, LastErpSyncDate))
                    {
                        if ((DateTime.Now.Hour == (item.PosSyncTime1Id)) || (DateTime.Now.Hour == (item.PosSyncTime2Id)) || (DateTime.Now.Hour == (item.PosSyncTime3Id)) || (DateTime.Now.Hour == (item.PosSyncTime4Id)) || (DateTime.Now.Hour == (item.PosSyncTime5Id)))
                        {
                            if (item.SyncDataFromAccpac.Value)
                            {
                                SyncBackEndData(item);
                                SyncPosData(item);
                            }
                            else
                            {
                                SyncPosData(item);
                            }

                        }
                        try
                        {
                            item.LastErpSyncDate = currentDate;
                            SyncConfigMGR.Save(item);
                        }
                        catch (Exception)
                        {

                            continue;
                        }
                    

                    }

                }
            }
        }

        private void SyncBackEndData(PosSyncConfiguration item)
        {
            throw new NotImplementedException();
        }

        
        private void SyncPosData( PosSyncConfiguration syncItem)
        {
            IStoreManager iStoreManager=(IStoreManager)SF.Framework.IoC.Instance.Resolve(typeof(IStoreManager));
            POSSyncAPIsWebService pOSSyncAPIsWebService = new POSSyncAPIsWebService();
            POSSync syncObject=null;
            List<StoreLite> storeList = iStoreManager.FindAllLite(null);
          
            foreach (var item in storeList)
            {
                if (pOSSyncAPIsWebService.CheckIPConnectivity(item.IpAddress))
                {
                    //syncObject.StoreId
                    if (syncItem.PosDataToSync == 1)//all
                    {
                        syncObject.GetTransactions = true;
                        syncObject.GetCloseRegister = true;
                        syncObject.GetCustomers = true;
                        syncObject.GetUserData = true;
                        syncObject.GetRoles = true;
                        syncObject.GetTeams = true;
                        syncObject.GetPermissions = true;
                        syncObject.GetShifts = true;
                        syncObject.GetUserShifts = true;
                        syncObject.GetPersonData = true;
                        syncObject.GetCompanyData = true;
                        syncObject.GetStores = true;
                        syncObject.GetRegisters = true;
                        syncObject.GetQuickKeys = true;
                        syncObject.GetPaymentData = true;
                        syncObject.GetVouchers = true;
                        syncObject.GetItems = true;
                        syncObject.GetPriceList = true;
                        syncObject.GetItemPriceList = true;
                        syncObject.GetItemTax = true;
                        syncObject.GetCurrencyData = true;
                        syncObject.GetCurrencyRates = true;
                        syncObject.GetTaxMatrix = true;
                        syncObject.GetTaxAuth = true;
                        syncObject.GetAccpacCustomerData = true;
                        syncObject.GetStockData = true;
                        syncObject.GetLocationData = true;
                        syncObject.GetConfigData = true;
                        syncObject.GetLovs = true;

                    }
                    else if (syncItem.PosDataToSync == 2)// Register Data 
                    {
                        syncObject.GetTransactions = true;
                        syncObject.GetCloseRegister = true;
                        syncObject.GetCustomers = true;
                        syncObject.GetUserData = false;
                        syncObject.GetRoles = false;
                        syncObject.GetTeams = false;
                        syncObject.GetPermissions = false;
                        syncObject.GetShifts = false;
                        syncObject.GetUserShifts = false;
                        syncObject.GetPersonData = false;
                        syncObject.GetCompanyData = false;
                        syncObject.GetStores = false;
                        syncObject.GetRegisters = false;
                        syncObject.GetQuickKeys = false;
                        syncObject.GetPaymentData = false;
                        syncObject.GetVouchers = false;
                        syncObject.GetItems = false;
                        syncObject.GetPriceList = false;
                        syncObject.GetItemPriceList = false;
                        syncObject.GetItemTax = false;
                        syncObject.GetCurrencyData = false;
                        syncObject.GetCurrencyRates = false;
                        syncObject.GetTaxMatrix = false;
                        syncObject.GetTaxAuth = false;
                        syncObject.GetAccpacCustomerData = false;
                        syncObject.GetStockData = false;
                        syncObject.GetLocationData = false;
                        syncObject.GetConfigData = false;
                        syncObject.GetLovs = false;
                    }
                    else if (syncItem.PosDataToSync == 3)// Admin Data
                    {
                        syncObject.GetTransactions = false;
                        syncObject.GetCloseRegister = false;
                        syncObject.GetCustomers = false;
                        syncObject.GetUserData = true;
                        syncObject.GetRoles = true;
                        syncObject.GetTeams = true;
                        syncObject.GetPermissions = true;
                        syncObject.GetShifts = true;
                        syncObject.GetUserShifts = true;
                        syncObject.GetPersonData = true;
                        syncObject.GetCompanyData = true;
                        syncObject.GetStores = true;
                        syncObject.GetRegisters = true;
                        syncObject.GetQuickKeys = true;
                        syncObject.GetPaymentData = true;
                        syncObject.GetVouchers = true;
                        syncObject.GetItems = true;
                        syncObject.GetPriceList = true;
                        syncObject.GetItemPriceList = true;
                        syncObject.GetItemTax = true;
                        syncObject.GetCurrencyData = true;
                        syncObject.GetCurrencyRates = true;
                        syncObject.GetTaxMatrix = true;
                        syncObject.GetTaxAuth = true;
                        syncObject.GetAccpacCustomerData = true;
                        syncObject.GetStockData = true;
                        syncObject.GetLocationData = true;
                        syncObject.GetConfigData = true;
                        syncObject.GetLovs = true;
                    }
                    pOSSyncAPIsWebService.SyncData(syncObject, item.StoreId.ToString());
                }
            }


          
        }

        //public bool PostPOSInvoices(int invType)
        //{
        //    List<InvoiceIntegration> invoiceList = iInvoiceManager.InvoiceIntegrationFindAll(new InvoiceCriteria() { Status = 1, InvoiceType = invType });

        //    if (invoiceList == null || invoiceList.Count() == 0) return true;
        //    List<InvoiceItemIntegration> invoiceItemList = new List<InvoiceItemIntegration>();
        //    List<ShipmentReturnObject> result = new List<ShipmentReturnObject>();
        //    if (invoiceList == null) invoiceList = new List<InvoiceIntegration>();
        //    ShipmentReturnObject obj = new ShipmentReturnObject();
        //    foreach (InvoiceIntegration item in invoiceList)
        //    {
        //        invoiceItemList = iInvoiceItemManager.InvoiceItemIntegrationFindAll(new InvoiceItemCriteria() { InvoiceUniqueNumber = item.InvoiceUniqueNumber, LocationId = item.LocationId });
        //        obj = AddShipmentSingle(item, invoiceItemList, invType);
        //        result.Add(obj);
        //        try
        //        {
        //            if (obj.ShipmentId > 0)
        //                iInvoiceManager.UpdateInvoiceTransNumber(item.InvoiceId, obj.ShipmentId, obj.TransReff);
        //        }
        //        catch (Exception)
        //        {
        //            return false;
        //        }

        //    }
        //    return true;
        //}
     
        private ShipmentReturnObject AddShipmentSingle(InvoiceIntegration invoiceObj, List<InvoiceItemIntegration> details, int Type)
        {
            var add = true;
            var postingAction = true;
         
            string IcShipmentInfo = "";
            IcShipment myIcShipment = null;

            if (Type == 1 || Type == 7)
                myIcShipment = mappShipmentData(invoiceObj, details, (int)Enums_S3.InventoryControl.ShipmentType.Shipment);
            else
                myIcShipment = mappShipmentData(invoiceObj, details, (int)Enums_S3.InventoryControl.ShipmentType.Return);

            //IcShipment myIcShipment =new Centrix.Inventory.Business.Entity.IcShipment();
            //map properties

            var automaticInvoiceCreated = false;
            var createAutomaticInvoiceEnabled = myIcShipment.CreateAutomaticARInvoice;
            var automaticInvoiceRegionEntered = false;
            var savedShipmenttId = 0;

            var result = this.CheckIfValidYearPeriodValue(myIcShipment.PostingDate.Value.Year, myIcShipment.PostingDate.Value.Month);
            if (result != CentrixERP.Configuration.DataTypeContectEnumerations.BatchPostingErrors.NoError)
            {
                return new ShipmentReturnObject { status = false, TransReff = result.ToString() };
            }
            else
            {
                UpdateShipmentYearPeriodValue(ref myIcShipment);
            }


            try
            {
                PrepareShipmentForSaving(add, 0, myIcShipment, postingAction);

                IcShipmentCriteria shipmentCriteria = new IcShipmentCriteria();
                shipmentCriteria.Keyword = myIcShipment.ShipmentNumber;
                shipmentCriteria.pageNumber = 1;
                shipmentCriteria.resultCount = 10;
                if (add)
                {
                    List<IcShipmentLite> duplicateShipments = iIcShipmentManager.FindAllLight<IcShipmentLite>(shipmentCriteria);//.ToList();
                    if (duplicateShipments != null)
                    {
                        if (duplicateShipments.Count > 0)
                        { throw new SF.Framework.Exceptions.RecordExsitsException("Duplicate Shipment Number"); }
                    }
                }

                myIcShipment.Status = false;
                // iIcShipmentManager.BeginTransaction();
                iIcShipmentManager.BeginTransaction();
                iIcShipmentManager.Save(myIcShipment);
                iIcShipmentManager.CommitTransaction();
                if (add)
                {

                    UpdateDocumentNextNumber((myIcShipment.ShipmentTypeId == (int)Enums_S3.InventoryControl.ShipmentType.Return) ? (int)Enums_S3.InventoryControl.DocumentNumbers.ShipmentReturnNumber : (int)Enums_S3.InventoryControl.DocumentNumbers.ShipmentNumber);
                }

                if (postingAction)
                {
                    CheckTransactionAccountsResultCriteria checkCriteria = new CheckTransactionAccountsResultCriteria();
                    checkCriteria.TransactionId = myIcShipment.ShipmentId;
                    if (myIcShipment.ShipmentTypeId == (int)Enums_S3.InventoryControl.ShipmentType.Shipment)
                    { checkCriteria.TransactionType = (int)Enums_S3.InventoryControl.TransactionType.Shipment; }
                    else if (myIcShipment.ShipmentTypeId == (int)Enums_S3.InventoryControl.ShipmentType.Return)
                    { checkCriteria.TransactionType = (int)Enums_S3.InventoryControl.TransactionType.ShipmentReturn; }

                    List<CheckTransactionAccountsResult> checkTransactionAccountsResults = iIcLocationManager.CheckTransactionAccounts(checkCriteria);

                    foreach (var resultLine in checkTransactionAccountsResults)
                    {
                        if (!resultLine.AccountIsValid)
                        {
                            iIcShipmentManager.RollbackTransaction();
                            return new ShipmentReturnObject { status = false, TransReff = Enums_S3.InventoryControl.InventoryErrors.GLAccountsDoesNotExist.ToString() };
                        }
                    }
                }

                iIcShipmentManager.CopySessionDetails((ISessionDetails)iIcItemCardSerialTransactionManager);
                HandleItemCardSerialTransactions(myIcShipment, postingAction);


                if (postingAction)
                {
                    PostShipment(myIcShipment, Type, invoiceObj.InvoiceId);
                }


                #region Optional Field
                ////Save Header Optional Field
                //if (add && myIcShipment.OptionalFieldEntityList != null)
                //{ OptionalFieldSave(myIcShipment.OptionalFieldEntityList, myIcShipment.ShipmentId); }
                ////Save Details Optional Feild
                //foreach (E.IcShipmentDetail detail in myIcShipment.ShipmentDetails)
                //{
                //    if (detail.OptionalFieldChanged)
                //        OptionalFieldSave(detail.OptionalFieldEntityList, detail.ShipmentDetailId, (int)Enums_S3.Entity.IcShipmentDetail, null);
                //}
                #endregion

                iIcShipmentManager.CopySessionDetails((ISessionDetails)iIcCommonQueries);


                //iIcShipmentManager.CommitTransaction();
                savedShipmenttId = myIcShipment.ShipmentId;

                bool DEPIsEnabled = iIcCommonQueries.GetIfGLPostingOnDayEndProcessingIsEnabled(null);

                //Create Automatic AR Invoice

                if (Type == 7)
                {
                    if (postingAction && !DEPIsEnabled && myIcShipment.CreateAutomaticARInvoice)
                    {
                        automaticInvoiceRegionEntered = true;
                        iIcShipmentManager.CopySessionDetails((ISessionDetails)iIcAccountingIntegrationManager);
                        Centrix.Inventory.Business.Entity.ArAutomaticInvoice arAutomaticInvoice = iIcAccountingIntegrationManager.GetAutomaticARInvoiceByShipmentId(myIcShipment.ShipmentId,1);

                        var createdInvoiceId = iIcAccountingIntegrationManager.CreateAutomaticARInvoice(arAutomaticInvoice);

                        ArInvoiceLite arInvoice = iArInvoiceManager.FindByIdLite(createdInvoiceId, null);
                        ArInvoice invoice = new ArInvoice();
                        invoice.ArInvoiceId = createdInvoiceId;
                        invoice.TotalInWords = "";
                        iArInvoiceManager.InvoicePartiallyUpdate(invoice);

                        IcShipmentCriteria crit = new IcShipmentCriteria();
                        crit.EntityId = myIcShipment.ShipmentId;
                        crit.ShipmentId = myIcShipment.ShipmentId;
                        crit.InvoiceId = createdInvoiceId;
                        iIcShipmentManager.ExecSP(IcShipment.UpdateAutomaticInvoiceIdSPName, crit);
                    }
                    automaticInvoiceCreated = true;
                }

                if (myIcShipment.ShipmentTypeId == (int)Enums_S3.InventoryControl.ShipmentType.Return)
                {
                    IcShipment parentShipment = iIcShipmentManager.FindById(myIcShipment.ParentShipmentId, null);

                    if (parentShipment != null)
                    {

                        foreach (IcShipmentDetail shipmentDetail in parentShipment.ShipmentDetails)
                        {
                            if (shipmentDetail == null) continue;
                            IcShipmentDetail newDetails = (from item in myIcShipment.ShipmentDetails where item.OldDetailsId == shipmentDetail.ShipmentDetailId select item).FirstOrDefault();
                            if (newDetails != null)
                            {
                                shipmentDetail.MarkModified();
                                if (shipmentDetail.QuantityReturned == null) shipmentDetail.QuantityReturned = 0;
                                shipmentDetail.QuantityReturned += newDetails.Quantity;
                            }

                        }
                        parentShipment.MarkModified();
                        iIcShipmentManager.Save(parentShipment);
                    }
                }

                // iIcShipmentManager.CommitTransaction();
     IcShipmentLite lite = iIcShipmentManager.FindByIdLight<IcShipmentLite>(myIcShipment.ShipmentId, null);



                return new ShipmentReturnObject { status = true, TransReff = lite.ShipmentNumber, ShipmentId = lite.ShipmentId };
                //return this.AttachStatusCode(lite, (int)result, null);




            }
            catch (Exception ex)
            {

                return new ShipmentReturnObject { status = false, TransReff = ex.Message };
            }

        }

        private void UpdateShipmentYearPeriodValue(ref IcShipment shipment)
        {
            IcCommonQueriesCriteria crit = new IcCommonQueriesCriteria();
            crit.Date = shipment.PostingDate;
            string yearPeriodValue = iIcCommonQueries.GetYearPeriodValue(crit);

            shipment.YearPeriod = yearPeriodValue;
            shipment.Year = int.Parse(yearPeriodValue.Split('-')[0]);
            shipment.Period = int.Parse(yearPeriodValue.Split('-')[1]);
        }

        private IcShipment PrepareShipmentForSaving(bool add, int id, IcShipment myIcShipment, bool postingAction)
        {
            if (!add)
            {
                myIcShipment.MarkOld();
                myIcShipment.MarkModified();
                myIcShipment.ShipmentId = id;
                myIcShipment.UpdatedBy = 1;
                myIcShipment.UpdatedDate = DateTime.Now;

                foreach (var shipmentDetail in myIcShipment.ShipmentDetails)
                {
                    shipmentDetail.Shipment = myIcShipment;
                    shipmentDetail.UpdatedBy = 1;
                    shipmentDetail.UpdatedDate = DateTime.Now;

                    if (shipmentDetail.LocationId < 0)
                        shipmentDetail.LocationId = null;

                    if (shipmentDetail.ShipmentDetailId == 0)
                    {
                        shipmentDetail.CreatedBy =1;
                        shipmentDetail.CreatedDate = DateTime.Now;
                    }
                }

                IcShipment oldShipment = iIcShipmentManager.FindById(id, null);
                var deletedDetails = oldShipment == null ? new List<IcShipmentDetail>() : oldShipment.ShipmentDetails.Where(detail => detail != null)
                                                .Where(deletedItem => !myIcShipment.ShipmentDetails.Select(old => old.ShipmentDetailId).Contains(deletedItem.ShipmentDetailId) & !deletedItem.FlagDeleted);
                foreach (var deletedDetail in deletedDetails)
                {
                    deletedDetail.FlagDeleted = true;
                    myIcShipment.ShipmentDetails.Add(deletedDetail);
                }

                myIcShipment.ShipmentNumber = oldShipment == null ? myIcShipment.ShipmentNumber : oldShipment.ShipmentNumber;
            }
            else
            {
                myIcShipment.MarkNew();
                myIcShipment.CreatedBy = 1;
                myIcShipment.CreatedDate = DateTime.Now;

                if (myIcShipment.ShipmentTypeId == (int)Enums_S3.InventoryControl.ShipmentType.Shipment)
                    myIcShipment.ShipmentNumber = GenerateShipmentNumber(false);
                else
                    myIcShipment.ShipmentNumber = GenerateShipmentNumber(true);

                foreach (var shipmentDetail in myIcShipment.ShipmentDetails)
                {
                    shipmentDetail.MarkNew();
                    shipmentDetail.Shipment = myIcShipment;
                    shipmentDetail.CreatedBy = 1;
                    shipmentDetail.CreatedDate = DateTime.Now;

                    if (shipmentDetail.LocationId < 0)
                        shipmentDetail.LocationId = null;
                }
            }
            myIcShipment.Status = false;


            //************Handle Serials*************/
            var currentDateDate = DateTime.Now;
            IcShipment oldMatchingShipment = iIcShipmentManager.FindById(id, null);
            IList<IcItemCardSerialTransaction> ItemCardSerialTransactions = new List<IcItemCardSerialTransaction>();

            foreach (var shipmentDetail in myIcShipment.ShipmentDetails)
            {
                //if (oldMatchingShipment != null)
                //{
                //    var matchingShipmentDetail = oldMatchingShipment.ShipmentDetails.Where(detail => detail != null
                //                                        && detail.ShipmentDetailId == shipmentDetail.ShipmentDetailId).SingleOrDefault();
                //    if (!add && !myIcShipment.Status.Value)
                //    {
                //        var deletedSerials = matchingShipmentDetail.RelatedSerials.Where(serial => serial != null)
                //                                        .Where(deletedSerial => !shipmentDetail.RelatedSerials.Select(old => old.ShipmentDetailSerialId).Contains(deletedSerial.ShipmentDetailSerialId) & !deletedSerial.FlagDeleted);
                //        foreach (var deletedSerial in deletedSerials)
                //        {
                //            deletedSerial.ItemCardSerial.FlagDeleted = true;
                //            deletedSerial.FlagDeleted = true;
                //            shipmentDetail.RelatedSerials.Add(deletedSerial);
                //        }
                //    }
                //}
                if (shipmentDetail.RelatedSerials != null)
                {
                    foreach (var serial in shipmentDetail.RelatedSerials.Where(ser => ser != null))
                    {
                        var matchingShipmentDetail = oldMatchingShipment == null ? null : oldMatchingShipment.ShipmentDetails.Where(detail => detail != null
                                                            && detail.ShipmentDetailId == shipmentDetail.ShipmentDetailId).SingleOrDefault();

                        var matchingShipmentDetailSerial = matchingShipmentDetail == null ? null : matchingShipmentDetail.RelatedSerials.Where(x => x != null && x.ItemCardSerial.ItemCardSerialId == serial.ItemCardSerial.ItemCardSerialId).FirstOrDefault();

                        if (matchingShipmentDetailSerial != null)
                        {
                            serial.ShipmentDetailSerialId = matchingShipmentDetailSerial.ShipmentDetailSerialId;
                            //if (postingAction && myIcShipment.PostType == (int)Enums_S3.InventoryControl.ReceiptPostType.Return)
                            //{
                            //    serial.ItemCardSerial.Status = (int)Enums_S3.InventoryControl.SerialItemStatus.NotAvailable;
                            //}
                            serial.ShipmentDetail = shipmentDetail;
                        }

                        serial.ShipmentDetail = shipmentDetail;
                        if (serial.ItemCardSerial == null)
                        { serial.ItemCardSerial = new IcItemCardSerial(); }
                        serial.ItemCardSerial.ItemCardId = shipmentDetail.ItemCardId;

                        //Only Update location if posting action of shipment return
                        if (postingAction)
                        {
                            serial.ItemCardSerial.LocationId = shipmentDetail.LocationId.Value;
                        }
                        else
                        {
                            IcItemCardSerial originalSerial = iIcItemCardSerialManager.FindById(serial.ItemCardSerial.ItemCardSerialId, null);
                            serial.ItemCardSerial.LocationId = originalSerial.LocationId;
                        }


                        //if (serial.ItemCardSerial.ItemCardSerialId == 0)
                        //{
                        //    serial.CreatedBy = LoggedInUser.UserId;
                        //    serial.CreatedDate = currentDateDate;
                        //    serial.ItemCardSerial.CreatedBy = LoggedInUser.UserId;
                        //    serial.ItemCardSerial.CreatedDate = currentDateDate;
                        //}
                        //else
                        //{
                        //    serial.CreatedDate = currentDateDate;
                        //}

                        if (serial.ShipmentDetailSerialId == 0)
                        {
                            serial.CreatedBy = 1;
                            serial.CreatedDate = currentDateDate;
                        }

                        serial.UpdatedBy = 1;
                        serial.UpdatedDate = currentDateDate;

                        if (myIcShipment.ShipmentTypeId == (int)Enums_S3.InventoryControl.ShipmentType.Shipment)
                        {
                            if (postingAction)
                            { serial.ItemCardSerial.Status = (int)Enums_S3.InventoryControl.SerialItemStatus.NotAvailable; }
                            else
                            { serial.ItemCardSerial.Status = (int)Enums_S3.InventoryControl.SerialItemStatus.Available; }
                        }
                        else if (myIcShipment.ShipmentTypeId == (int)Enums_S3.InventoryControl.ShipmentType.Return)
                        {
                            if (postingAction)
                            { serial.ItemCardSerial.Status = (int)Enums_S3.InventoryControl.SerialItemStatus.Available; }
                            else
                            { serial.ItemCardSerial.Status = (int)Enums_S3.InventoryControl.SerialItemStatus.NotAvailable; }
                        }
                    }
                }
            }

            return myIcShipment;
        }

        private string GenerateShipmentNumber(bool isReturnShipment)
        {
            IcDocumentNumbersCriteria criteria = new IcDocumentNumbersCriteria();
            if (!isReturnShipment)
            {
                criteria.EntityId = (int)Enums_S3.InventoryControl.DocumentNumbers.ShipmentNumber;
                criteria.DocumentNumbersId = (int)Enums_S3.InventoryControl.DocumentNumbers.ShipmentNumber;
            }
            else
            {
                criteria.EntityId = (int)Enums_S3.InventoryControl.DocumentNumbers.ShipmentReturnNumber;
                criteria.DocumentNumbersId = (int)Enums_S3.InventoryControl.DocumentNumbers.ShipmentReturnNumber;
            }
            IcDocumentNumbersLite DocNumber = iIcDocumentNumbersManager.FindAllLite(criteria).FirstOrDefault();

            var generatedNum = DocNumber.Prefix;
            generatedNum += DocNumber.NextNumber.ToString().PadLeft(DocNumber.Length - DocNumber.Prefix.Length, '0');

            return generatedNum;
        }


        private void UpdateDocumentNextNumber(int documentTypeId)
        {
            try
            {
                iIcDocumentNumbersManager.UpdateDocumentNextNumber(documentTypeId);
            }
            catch (Exception ex) { }
        }

        private void PostShipment(IcShipment Shipment, int invType, int invId)
        {
            IcShipmentCriteria crit = new IcShipmentCriteria();
            crit.ShipmentId = Shipment.ShipmentId;
            crit.EntityId = Shipment.ShipmentId;
            crit.CreatedBy =1;
            crit.PosInvId = invId;
            crit.PosInvType = invType;
            // iIcShipmentManager.Post(false, crit);
            iIcItemCardManager.PostIcShipment(Shipment.ShipmentId, 1, false, invType, invId);
        }

        private IcShipment mappShipmentData(InvoiceIntegration invoice, List<InvoiceItemIntegration> List, int Type)
        {
            IcShipment shipment = new Centrix.Inventory.Business.Entity.IcShipment();
            shipment.Reference = invoice.InvoiceNumber;
            shipment.Description = "POS-Shipment";
            //shipment.CurrencyId = invoice.CurrencyId;
            //shipment.CustomerId = invoice.CustomerId;
            shipment.PriceListId = -1;
            shipment.DiscountAmount = invoice.AmountDiscount;
            shipment.DiscountPercentage = invoice.PercentageDiscount;
            shipment.ShipmentDate = invoice.InvoiceDate;
            shipment.PostingDate = invoice.InvoiceDate;
            shipment.CreateAutomaticARInvoice = true;
            shipment.ShipmentTypeId = Type;
            //shipment.TaxClassId = invoice.TaxClassId;
            //shipment.TotalTaxAmount = invoice.TotalTaxAmount;
            shipment.TotalDetailsAmount = invoice.GrandTotal;

            shipment.ShipmentDetails = new List<IcShipmentDetail>();
            foreach (InvoiceItemIntegration item in List)
            {
                IcShipmentDetail shipmentItem = new Centrix.Inventory.Business.Entity.IcShipmentDetail();
                shipmentItem.ItemCardId = item.ItemId;
              //  shipmentItem.ItemPriceListId = item.PriceListId;
                shipmentItem.ItemUnitOfMeasureId = item.UnitOfMeasureId;
                shipmentItem.Quantity = item.Quantity;
                shipmentItem.LocationId = invoice.LocationId;
                shipmentItem.UnitPrice = item.UnitPrice;
                shipmentItem.ExtendedPrice = item.UnitPrice * item.Quantity;
                //shipmentItem.UnitCost = item.UnitCost;
                //shipmentItem.ExtendedCost = item.UnitCost * item.Quantity;
                shipmentItem.CategoryId = item.CategoryId;
                //shipmentItem.TaxClassId = item.TaxClassId;
                //shipmentItem.LineDiscountPercent = item.LineDiscountPercent;
                //shipmentItem.LineTotalAfterDiscount = item.LineTotalAfterDiscount;
                //shipmentItem.DiscountAmount = item.DiscountAmount;
                //shipmentItem.TaxRate = item.TaxRate;
                //shipmentItem.TaxAmount = item.TaxAmount;
                //shipmentItem.TotalLineAmount = item.TotalLineAmount;
                //if (item.IsSerial)
                //{
                //    shipmentItem.RelatedSerials = new List<Centrix.Inventory.Business.Entity.IcShipmentDetailSerial>();
                //    shipmentItem.RelatedSerials.Add(new Centrix.Inventory.Business.Entity.IcShipmentDetailSerial()
                //    {
                //        CreatedBy = 1,
                //        ItemCardSerial = new Centrix.Inventory.Business.Entity.IcItemCardSerial() { ItemCardId = item.ItemId, ItemSerialNumber = item.SerialNumber, LocationId = shipmentItem.LocationId.Value, Status = 2, StockDate = DateTime.Now, CreatedDate = DateTime.Now },
                //        ShipmentDetail = shipmentItem,
                //        CreatedDate = DateTime.Now
                //    });
                //}


                shipment.ShipmentDetails.Add(shipmentItem);
            }
            return shipment;
        }
     
        private void HandleItemCardSerialTransactions(IcShipment shipment, bool PostingAction)
        {
            int currentEntityId = (int)Enums_S3.Entity.IcShipmentDetail;
            int currentTransactionTypeId = 0;
            if (shipment.ShipmentTypeId == (int)Enums_S3.InventoryControl.ShipmentType.Shipment)
            { currentTransactionTypeId = (int)Enums_S3.InventoryControl.TransactionType.Shipment; }
            else if (shipment.ShipmentTypeId == (int)Enums_S3.InventoryControl.ShipmentType.Return)
            { currentTransactionTypeId = (int)Enums_S3.InventoryControl.TransactionType.ShipmentReturn; }

            IList<IcItemCardSerialTransaction> ItemCardSerialTransactions = new List<IcItemCardSerialTransaction>();
            IList<IcItemCardSerialTransaction> allDeletedSerials = new List<IcItemCardSerialTransaction>();
            var currentDate = DateTime.Now;
            foreach (var shipmentDetail in shipment.ShipmentDetails)
            {
                IcItemCardSerialTransactionCriteria itemCardSerialTransactionCriteria = new IcItemCardSerialTransactionCriteria();
                itemCardSerialTransactionCriteria.RelatedEntityId = currentEntityId;
                itemCardSerialTransactionCriteria.EntityValueId = shipmentDetail.ShipmentDetailId;

                IList<IcItemCardSerialTransactionLite> originalRelatedSerialTransactions = iIcItemCardSerialTransactionManager.FindAllLight<IcItemCardSerialTransactionLite>(itemCardSerialTransactionCriteria);
                var origionalreturnedSerialTransactions = originalRelatedSerialTransactions == null ? null : originalRelatedSerialTransactions.Where(x => x.TransactionTypeId == currentTransactionTypeId);
                if (shipmentDetail.RelatedSerials == null) shipmentDetail.RelatedSerials = new List<IcShipmentDetailSerial>();
                foreach (var shipmentDetailSerial in shipmentDetail.RelatedSerials)
                {

                    iIcItemCardManager.UpdateItemCardSerialStatus(shipmentDetailSerial.ItemCardSerial.ItemCardSerialId, currentTransactionTypeId);

                    var matchingItem = originalRelatedSerialTransactions == null ? null : originalRelatedSerialTransactions.Where(x => x.ItemCardSerialId == shipmentDetailSerial.ItemCardSerial.ItemCardSerialId).SingleOrDefault();

                    if (matchingItem == null) //New Item
                    {
                        IcItemCardSerialTransaction itemCardSerialTrans = new IcItemCardSerialTransaction();
                        itemCardSerialTrans.CreatedBy = 1;
                        itemCardSerialTrans.CreatedDate = currentDate;
                        itemCardSerialTrans.ItemCardSerialId = shipmentDetailSerial.ItemCardSerial.ItemCardSerialId;
                        itemCardSerialTrans.RelatedEntityId = currentEntityId;
                        itemCardSerialTrans.EntityValueId = shipmentDetail.ShipmentDetailId;
                        itemCardSerialTrans.TransactionTypeId = currentTransactionTypeId;
                        ItemCardSerialTransactions.Add(itemCardSerialTrans);
                    }

                }

                var deletedSerials = originalRelatedSerialTransactions == null ? new List<IcItemCardSerialTransactionLite>() : originalRelatedSerialTransactions.Where(relatedSerial => relatedSerial != null)
                                                .Where(deletedItem => !shipmentDetail.RelatedSerials.Select(old => old.ItemCardSerial.ItemCardSerialId).Contains(deletedItem.ItemCardSerialId) ||
                                                    shipmentDetail.RelatedSerials.Where(ser => ser.FlagDeleted == true).Select(old => old.ItemCardSerial.ItemCardSerialId).Contains(deletedItem.ItemCardSerialId));

                foreach (var deletedSerial in deletedSerials)
                {

                    IcItemCardSerialTransaction itemCardSerialTrans = new IcItemCardSerialTransaction();
                    itemCardSerialTrans.ItemCardSerialId = deletedSerial.ItemCardSerialId;
                    itemCardSerialTrans.RelatedEntityId = deletedSerial.RelatedEntityId;
                    itemCardSerialTrans.EntityValueId = deletedSerial.EntityValueId;
                    itemCardSerialTrans.TransactionTypeId = deletedSerial.TransactionTypeId;
                    itemCardSerialTrans.ItemCardSerialTransactionId = deletedSerial.ItemCardSerialTransactionId;
                    itemCardSerialTrans.FlagDeleted = true;
                    itemCardSerialTrans.MarkDeleted();
                    ItemCardSerialTransactions.Add(itemCardSerialTrans);
                }
                //}
            }


            foreach (var itemCardSerialTransaction in ItemCardSerialTransactions)
            {
                if (itemCardSerialTransaction.ItemCardSerialTransactionId == 0)
                    iIcItemCardSerialTransactionManager.Save(itemCardSerialTransaction);
                else
                    iIcItemCardSerialTransactionManager.Update(itemCardSerialTransaction);
            }
        }
        public DataTypeContectEnumerations.BatchPostingErrors CheckIfValidYearPeriodValue(int year, int month)
        {
            //ifiscalyearmanager.CalendarYearPeriodCheck
            YearPeriodCheckResult yearPeriodCheckResult = iPhysicalCalendarHeaderManager.CalendarYearPeriodCheck(year, month);

            if (yearPeriodCheckResult.IsLocked)
                return DataTypeContectEnumerations.BatchPostingErrors.EntryInLockedPeriod;
            if (!yearPeriodCheckResult.BeforOldestYear)
                return DataTypeContectEnumerations.BatchPostingErrors.EntryBeforOldesetYear;
            if (yearPeriodCheckResult.FutureYear)
                return DataTypeContectEnumerations.BatchPostingErrors.EntryInFutureYear;
            if (!yearPeriodCheckResult.InCurrentYear)
                return DataTypeContectEnumerations.BatchPostingErrors.AllowPostingToPreviousYear;
            if (yearPeriodCheckResult.HasNoPeriod)
                return DataTypeContectEnumerations.BatchPostingErrors.EntryHasNoPeriod;

            return DataTypeContectEnumerations.BatchPostingErrors.NoError;
        }
    }

    public class POSShipment
    {
        public string Refference;
        public int CurrencyId;
        public int ArcustomerId;
        public int PriceListId;
        public int locaitonId;
        public double AmountDiscout;
        public double PercentageDiscount;
        public string Note;
        public DateTime? ShipmentDate;
    }

    public class POSShipmentDetails
    {
        public int ItemId;
        public int CategoryId;
        public int UnitOfMeasureId;
        public double Quantity;
        public double UnitPrice;
    }


    public class ShipmentReturnObject
    {
        public string TransReff;
        public bool status;
        public int ShipmentId;
    }
}