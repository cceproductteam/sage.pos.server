﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace CentrixERP.Agent.Workers
{
    public class Loggin
    {
        public static string logPath = "";
        public static void Log(string text)
        {
            text = text + Environment.NewLine;
            try
            {
                if (!Directory.Exists(logPath))
                    Directory.CreateDirectory(logPath);

                string fileName = logPath + DateTime.Now.ToString("dd-MM-yyyy") + " agent-log.txt";
                FileStream log = File.Open(fileName, FileMode.Append);
                byte[] buffer = System.Text.ASCIIEncoding.ASCII.GetBytes(text);
                log.Write(buffer, 0, buffer.Length);
                log.Close();
            }
            catch (Exception ex)
            {
                //throw ex;
            }
        }
    }
}