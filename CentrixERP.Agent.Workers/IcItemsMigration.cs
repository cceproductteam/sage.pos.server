﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Interop.Excel;
using CentrixERP.Business.Entity;
using System.Reflection;
using SF.Framework;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using CX.Framework;
using System.IO;
using Centrix.Inventory.Business.Entity;
using Centrix.Inventory.Business.Manager;
using E=Centrix.Inventory.Business.Entity;
using M=Centrix.Inventory.Business.IManager;
using Centrix.Inventory.Business.IManager;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Data.OleDb;
using WindowsService1;
using AdS = CentrixERP.AdministrativeSettings.Business.Entity;
using AdsM = CentrixERP.AdministrativeSettings.Business.IManager;


namespace CentrixERP.Agent.Workers
{
    public class IcItemsMigration
    {
        public string ExcelFilePath { get; set; }
        public string ErrorLogFolderPath { get; set; }
        public string ErrorLogFilePath { get; set; }
        IcItemCardManager icItemCardManager = (IcItemCardManager)SF.Framework.IoC.Instance.Resolve(typeof(IcItemCardManager));
        IBusinessManagerBase<IcItemCardSegment, int> iIcItemCardSegmentManager = (IBusinessManagerBase<IcItemCardSegment, int>)CX.Framework.IoC.Instance.Resolve(typeof(IBusinessManagerBase<IcItemCardSegment, int>));
        IcOptionsManager icOptionManager = (IcOptionsManager)SF.Framework.IoC.Instance.Resolve(typeof(IcOptionsManager));
        M.IIcUnitOfMeasuresManager iIcUnitOfMeasuresManager = (M.IIcUnitOfMeasuresManager)SF.Framework.IoC.Instance.Resolve(typeof(M.IIcUnitOfMeasuresManager));
        IIcItemUnitOfMeasuresManager iIcItemUnitOfMeasuresManager = (IIcItemUnitOfMeasuresManager)SF.Framework.IoC.Instance.Resolve(typeof(IIcItemUnitOfMeasuresManager));
        IcItemUnitOfMeasuresCriteria ItemUOMCrit = new IcItemUnitOfMeasuresCriteria();
        IcPriceListGroupCriteria priceListGrpCrit = new IcPriceListGroupCriteria();
        List<AdS.TaxClassDetails> TaxClassDetailsList = new List<AdS.TaxClassDetails>();
        AdsM.ITaxClassDetailsManager iTaxClassDetailsMngr = (AdsM.ITaxClassDetailsManager)SF.Framework.IoC.Instance.Resolve(typeof(AdsM.ITaxClassDetailsManager));
        AdS.TaxClassDetailsCriteria taxClassDetailsCrit = new AdS.TaxClassDetailsCriteria();

        AdsM.ITaxRateManager iTaxRateMngr = (AdsM.ITaxRateManager)SF.Framework.IoC.Instance.Resolve(typeof(AdsM.ITaxRateManager));
        AdS.TaxRateCriteria taxRateCrit = new AdS.TaxRateCriteria();
        List<AdS.TaxRateLite> taxRatesListTemp = new List<AdS.TaxRateLite>();
        List<AdS.TaxRateLite> taxRatesList = new List<AdS.TaxRateLite>();
        TaxRate taxRateTemp;

        private StringBuilder Errors;
        private int BaseSegmentId;
        List<TempIcStructureSegment> segmentList;
        List<TempIcSegmentCodeList> SegmentCodeList;
        List<IcPriceListGroup> priceListGroupList;
        List<IcItemUnitOfMeasures> ItemUOMsList;

        List<taxTempResult> ItemTaxList;
        taxTempResult itemTaxObj;
        IIcItemTaxManager iIcItemTaxManager = (IIcItemTaxManager)SF.Framework.IoC.Instance.Resolve(typeof(IIcItemTaxManager));

        int oldItemStructureCode = -1;
        IIcPriceListGroupManager iIcPriceListGroupManager = (IIcPriceListGroupManager)SF.Framework.IoC.Instance.Resolve(typeof(IIcPriceListGroupManager));
        private string Excel07ConString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 8.0;HDR={1}'";
        SqlConnection cnn22 = new SqlConnection("Data Source=192.168.2.225;Initial Catalog=CentrixERPV3.3.27.Dev;Persist Security Info=True;User ID=sa;Password=Pocketaps!2;");
        SqlConnection cnn = new SqlConnection("Data Source=192.168.2.225;Initial Catalog=CentrixERPV3.3.27.Dev;Persist Security Info=True;User ID=sa;Password=Pocketaps!2;");


        public Dictionary<string, string> DoWork(string fileName, string logFilePath)
        {
            ExcelFilePath = fileName;
            ErrorLogFolderPath = logFilePath;
           // ImportData();
            return validateExcelSheetFile_Items();
        }

        



        public Dictionary<string, string> validateExcelSheetFile_Items()
        {
            Errors = new StringBuilder();
            Dictionary<string, string> result = new Dictionary<string, string>();

            bool taxresult = true;
            int itemPurchaseTax = -1;
            int itemSalesTax = -1; 

            try
            {

                Library.WriteErrorLog("Start Validating");

                string conStr;
                conStr = string.Format(Excel07ConString, ExcelFilePath, "Yes");

                List<IcOptionsLite> IcOptionsList = icOptionManager.FindAllLite(null);
                if (IcOptionsList != null && IcOptionsList.Count > 0)
                    BaseSegmentId = IcOptionsList.FirstOrDefault().BaseSegmentId;

                IcUnitOfMeasures UOMObj = null;
                int oldUOMId = -1;

                List<string> SheetName = new List<string>();

                System.Data.DataTable dt;
                List<TempItem> tempItemList;

                //   Get name of the Excel file.
                using (OleDbConnection con = new OleDbConnection(conStr))
                {
                    using (OleDbCommand cmd = new OleDbCommand())
                    {
                        con.Open();
                        cmd.Connection = con;

                        //Get name of Sheets in file
                        System.Data.DataTable dtsheet = new System.Data.DataTable();
                        dtsheet = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);   // Read Excel SubSheet


                        foreach (DataRow sheet in dtsheet.Rows)
                        {

                            string SubSheet = sheet["Table_Name"].ToString();

                            //Remove the $ from the end of sheets name
                            if (SubSheet.Length > 1 && SubSheet.EndsWith("$"))
                                SheetName.Add(SubSheet.Substring(0, SubSheet.Length - 1));

                        }

                    }
                    con.Close();
                }


                Object thisLock = new Object();
                OleDbConnection con2 = new OleDbConnection(conStr);


                for (int i = 0; i <= SheetName.Count - 1; i++)
                {

                    lock (thisLock)
                    {

                        try
                        {
                            if (SheetName[i] == "Items")
                            {

                                dt = new System.Data.DataTable();

                                OleDbCommand cmd2 = new OleDbCommand("SELECT * From [" + SheetName[i] + "$" + "]", con2); //Read Sheet Data from DB

                                if (con2.State == ConnectionState.Closed)
                                    con2.Open();

                                OleDbDataAdapter oda = new OleDbDataAdapter();
                                oda.SelectCommand = cmd2;
                                oda.Fill(dt);
                                con2.Close();


                                DataColumnCollection dtCol = dt.Columns;


                                if (dtCol != null)
                                {
                                    for (int ii = 0; ii <= dtCol.Count; ii++)
                                    {
                                        if (ii == 0 && !dtCol[ii].ColumnName.ToLower().Equals("structurecode"))
                                        {
                                            AddError("Check Excel Template : First Column should be structurecode", 1);
                                        }
                                        else if (ii == 1 && !dtCol[ii].ColumnName.ToLower().Equals("itemnumber"))
                                        {
                                            AddError("Check Excel Template : Second Column should be itemnumber", 1);
                                        }
                                        else if (ii == 2 && !dtCol[ii].ColumnName.ToLower().Equals("itemdescription"))
                                        {
                                            AddError("Check Excel Template : Third Column should be itemdescription", 1);
                                        }
                                        else if (ii == 3 && !dtCol[ii].ColumnName.ToLower().Equals("sku"))
                                        {
                                            AddError("Check Excel Template : Forth Column should be sku", 1);
                                        }
                                        else if (ii == 4 && !dtCol[ii].ColumnName.ToLower().Equals("barcode"))
                                        {
                                            AddError("Check Excel Template : Fifth Column should be barcode", 1);
                                        }
                                        else if (ii == 5 && !dtCol[ii].ColumnName.ToLower().Equals("costingmethod"))
                                        {
                                            AddError("Check Excel Template : Sixth Column should be costingmethod", 1);
                                        }
                                        else if (ii == 6 && !dtCol[ii].ColumnName.ToLower().Equals("category"))
                                        {
                                            AddError("Check Excel Template : Seventh Column should be category", 1);
                                        }
                                        else if (ii == 7 && !dtCol[ii].ColumnName.ToLower().Equals("accountsetcode"))
                                        {
                                            AddError("Check Excel Template : Eighth Column should be accountsetcode", 1);
                                        }
                                        else if (ii == 8 && !dtCol[ii].ColumnName.ToLower().Equals("defaultpricelistcode"))
                                        {
                                            AddError("Check Excel Template : Ninth Column should be defaultpricelistcode", 1);
                                        }
                                        else if (ii == 9 && !dtCol[ii].ColumnName.ToLower().Equals("currencycode"))
                                        {
                                            AddError("Check Excel Template : Tenth Column should be currencycode", 1);
                                        }
                                        else if (ii == 10 && !dtCol[ii].ColumnName.ToLower().Equals("baseprice"))
                                        {
                                            AddError("Check Excel Template : Eleventh Column should be baseprice", 1);
                                        }

                                        else if (ii == 11 && !dtCol[ii].ColumnName.ToLower().Equals("uom"))
                                        {
                                            AddError("Check Excel Template : Twelfth Column should be uom", 1);
                                        }
                                        else if (ii == 12 && !dtCol[ii].ColumnName.ToLower().Equals("tax"))
                                        {
                                            AddError("Check Excel Template : Thirteenth Column should be tax", 1);
                                        }

                                        //else if (ii == 12 && !dtCol[ii].ColumnName.ToLower().Equals("serialnumber"))
                                        //{
                                        //    AddError("Check Excel Template : Thirteenth Column should be serialnumber", 1);
                                        //}
                                        //else if (ii == 13 && !dtCol[ii].ColumnName.ToLower().Equals("lotnumber"))
                                        //{
                                        //    AddError("Check Excel Template : Forteenth Column should be lotnumber", 1);
                                        //}
                                    }



                                    SegmentCodeList = getStructureSegmentCodes();
                                    //Fill The Sheet In temp list
                                    tempItemList = new List<TempItem>();
                                    ItemTaxList = new List<taxTempResult>();
                                    int RowsCount = 0;

                                    for (int j = 0; j <= dt.Rows.Count - 1; j++) // check if the row is empty                                
                                    {
                                        if (dt.Rows[j]["structurecode"].ToString() != "" && dt.Rows[j]["itemnumber"].ToString() != "" && dt.Rows[j]["itemdescription"].ToString() != "" &&
                                            dt.Rows[j]["sku"].ToString() != "" && dt.Rows[j]["barcode"].ToString() != "" && dt.Rows[j]["costingmethod"].ToString() != "" &&
                                            dt.Rows[j]["category"].ToString() != "" && dt.Rows[j]["accountsetcode"].ToString() != "" && dt.Rows[j]["defaultpricelistcode"].ToString() != "" &&
                                            dt.Rows[j]["currencycode"].ToString() != "" && dt.Rows[j]["baseprice"].ToString() != "" && dt.Rows[j]["uom"].ToString() != "" &&
                                           dt.Rows[j]["tax"].ToString() != ""
                                            )
                                        {

                                            TempItem tempItem = new TempItem();

                                            tempItem.StructureCode = dt.Rows[j]["structurecode"].ToString();
                                            tempItem.ItemNumber = dt.Rows[j]["itemnumber"].ToString();
                                            tempItem.ItemDescription = dt.Rows[j]["itemdescription"].ToString();
                                            tempItem.SKU = dt.Rows[j]["sku"].ToString();
                                            tempItem.Barcode = dt.Rows[j]["barcode"].ToString();
                                            tempItem.CostingMethod = dt.Rows[j]["costingmethod"].ToString();
                                            tempItem.Category = dt.Rows[j]["category"].ToString();
                                            tempItem.AccountSetCode = dt.Rows[j]["accountsetcode"].ToString();
                                            tempItem.DefaultPriceListCode = dt.Rows[j]["defaultpricelistcode"].ToString();
                                            tempItem.CurrencyCode = dt.Rows[j]["currencycode"].ToString();
                                            tempItem.BasePrice = Convert.ToDouble(dt.Rows[j]["baseprice"].ToString());
                                            tempItem.UOM = dt.Rows[j]["uom"].ToString();
                                            tempItem.SerialNumber = "N"; //dt.Rows[j]["serialnumber"].ToString();
                                            tempItem.LotNumber = "N";//dt.Rows[j]["lotnumber"].ToString();
                                            tempItem.Tax = Convert.ToDouble(dt.Rows[j]["tax"].ToString());

                                            tempItem.ExcelSheetLineNumber = j;
                                            if (!string.IsNullOrEmpty(tempItem.StructureCode))
                                                tempItemList.Add(tempItem);

                                            RowsCount++;
                                        }
                                    }

                                    if (tempItemList.Count < RowsCount)
                                        AddError("Check Excel Template : Sheet has Empty Cells", 1);

                                    List<E.IcItemCard> IcItemCardList = new List<E.IcItemCard>();
                                    E.IcItemCard itemCard;
                                    List<string> ErrorArr;

                                    if (tempItemList != null && tempItemList.Count > 0)
                                    {

                                        foreach (TempItem item in tempItemList)
                                        {
                                            ErrorArr = new List<string>();
                                            int structureCodeId = HandleIcStructureCode(item.StructureCode);
                                            int coastingMethodId = HandleIcCoastingMethod(item.CostingMethod);
                                            int defaultPriceListId = HandleIcPriceList(item.DefaultPriceListCode);
                                            int currencyId = HandleIcCurrencyCode(item.CurrencyCode);
                                            int categoryId = HandleIcCategory(item.Category);
                                            int accountSetId = HandleIcAccountSet(item.AccountSetCode);
                                            int uomId = HandleIcUOM(item.UOM);
                                            bool isSerialNumber = (item.SerialNumber == "Y") ? true : false;
                                            bool isLotNumber = (item.LotNumber == "Y") ? true : false;

                                            double tax = item.Tax;
                                             taxresult = true;
                                            itemPurchaseTax = -1;
                                            itemSalesTax = -1;

                                            if (isSerialNumber && isLotNumber)
                                                isLotNumber = false;

                                            bool valid = true;

                                            if (structureCodeId <= 0)
                                            {
                                                ErrorArr.Add(string.Format("{0} as a Structure Code Does Not Exsit", item.StructureCode));
                                                valid = false;
                                            }
                                            if (coastingMethodId <= 0)
                                            {
                                                ErrorArr.Add(string.Format("{0} as a Costing Method Does Not Exsit", item.CostingMethod));
                                                valid = false;
                                            }
                                            if (defaultPriceListId <= 0)
                                            {
                                                ErrorArr.Add(string.Format("{0} as a Default Price List Code Does Not Exsit", item.DefaultPriceListCode));
                                                valid = false;
                                            }
                                            if (categoryId <= 0)
                                            {
                                                ErrorArr.Add(string.Format("{0} as a Category Does Not Exsit", item.Category));
                                                valid = false;
                                            }
                                            if (accountSetId <= 0)
                                            {
                                                ErrorArr.Add(string.Format("{0} as an Account Set Code Does Not Exsit", item.AccountSetCode));
                                                valid = false;
                                            }
                                            if (currencyId <= 0)
                                            {
                                                ErrorArr.Add(string.Format("{0} as a Currency Code Does Not Exsit", item.CurrencyCode));
                                                valid = false;
                                            }

                                            if (tax != null && tax > 0)
                                            {

                                                taxResult tx = HandleTaxRate(tax);

                                                if (tx != null)
                                                {
                                                    taxresult = tx.result;
                                                    itemPurchaseTax = tx.taxClassDetailsPurchase;
                                                    itemSalesTax = tx.taxClassDetailsSales;

                                                    if (!(tx.result))
                                                    {
                                                        if (itemPurchaseTax <= 0)
                                                        {
                                                            ErrorArr.Add(string.Format("{0} as Purchase tax Does Not Exsit", item.Tax));
                                                            valid = false;
                                                        }
                                                        if (itemSalesTax <= 0)
                                                        {
                                                            ErrorArr.Add(string.Format("{0} as Sales tax Does Not Exsit", item.Tax));
                                                            valid = false;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (itemPurchaseTax > 0 && itemSalesTax > 0)
                                                        {
                                                            itemTaxObj = new taxTempResult();
                                                            itemTaxObj.TaxAuthorityId = tx.taxAuthorityId;
                                                            itemTaxObj.SalesClassId = itemSalesTax;
                                                            itemTaxObj.PurchaseClassId = itemPurchaseTax;
                                                            itemTaxObj.ItemNumber = item.ItemNumber;

                                                            ItemTaxList.Add(itemTaxObj);


                                                        }
                                                    }

                                                }
                                              
                                            }


                                            itemCard = new E.IcItemCard()
                                            {
                                                AccountSetCodeId = accountSetId,
                                                StructureCode = structureCodeId,
                                                CoastingMethod = coastingMethodId,
                                                // DefaultPriceListId = defaultPriceListId,
                                                CategoryId = categoryId,
                                                ItemDescription = item.ItemDescription,
                                                ItemSku = item.SKU,
                                                ItemBarCode = item.Barcode,
                                                ItemNumber = item.ItemNumber,
                                                IsActive = true,
                                                StockItem = true,
                                                Sellable = true,
                                                SerialNumber = isSerialNumber,
                                                KittingItem = false,
                                                LotNumber = isLotNumber,
                                                ValidateQuantityOnBoQ = false,
                                                CreatedDate = new DateTime(),
                                                WeightUnitOfMeasureId = uomId
                                            };

                                            if (defaultPriceListId > 0 && currencyId > 0 && item.BasePrice > 0)
                                            {
                                                itemCard.PriceListGroup = new IcPriceListGroup()
                                                {
                                                    BasePriceTypeId = 214,
                                                    CurrencyId = currencyId,
                                                    PriceListId = defaultPriceListId,
                                                    BasePrice = Convert.ToDecimal(item.BasePrice),
                                                    CreatedBy = 1
                                                };
                                            }

                                            if (!BuildItemNumber(itemCard))
                                            {
                                                ErrorArr.Add(string.Format("Item Number: {0} Is not Valid", item.ItemNumber));
                                                valid = false;

                                            }

                                            if (CheckItemBarCodeExistance(itemCard.ItemNumber, item.Barcode))
                                            {
                                                ErrorArr.Add(string.Format("Item Number: {0} Barcode already Exists", item.ItemNumber));
                                                valid = false;
                                            }
                                          

                                            if (ErrorArr != null && ErrorArr.Count > 0)
                                            {
                                                foreach (string error in ErrorArr)
                                                {
                                                    AddError(error, item.ExcelSheetLineNumber);
                                                }
                                            }
                                            if (!valid)
                                            {
                                                LogErrors(Errors.ToString());
                                                continue;
                                            }

                                            IcItemCardList.Add(itemCard);

                                        }
                                    }
                                    else AddError("The Sheet Is Empty", 1);
                                    if (Errors.Length > 0)
                                    {
                                        LogErrors(Errors.ToString());
                                        result.Add("result", false.ToString());
                                        result.Add("LogPath", ErrorLogFilePath);
                                        return result;
                                    }




                                    if (IcItemCardList != null && IcItemCardList.Count > 0)
                                    {
                                        foreach (E.IcItemCard item in IcItemCardList)
                                        {
                                            try
                                            {
                                                if (CheckItemExistance(item.ItemNumber))   //If UPDATE
                                                {

                                                    int itemCrdId = CheckItemExistanceReturnItemId(item.ItemNumber);
                                                    item.ItemCardId = itemCrdId;
                                                    int uomId = item.WeightUnitOfMeasureId;
                                                    item.WeightUnitOfMeasureId = 0;


                                                    if (item.PriceListGroup != null)
                                                    {
                                                        priceListGrpCrit.ItemCardId = item.ItemCardId;
                                                        priceListGrpCrit.CurrencyId = item.PriceListGroup.CurrencyId;
                                                        priceListGroupList = iIcPriceListGroupManager.FindAll(priceListGrpCrit);
                                                        if (priceListGroupList != null && priceListGroupList.Count > 0)
                                                        {
                                                            foreach (IcPriceListGroup priceGrp in priceListGroupList)
                                                            {
                                                                priceGrp.MarkDeleted();
                                                                iIcPriceListGroupManager.Save(priceGrp);
                                                            }

                                                        }

                                                    }


                                                    if (uomId > 0)
                                                    {

                                                        UOMObj = iIcUnitOfMeasuresManager.FindById(uomId, null);

                                                        ItemUOMCrit.ItemCard = CheckItemExistanceReturnItemId(item.ItemNumber);
                                                        item.ItemCardId = (int)ItemUOMCrit.ItemCard;
                                                        ItemUOMsList = iIcItemUnitOfMeasuresManager.FindAll(ItemUOMCrit);
                                                        if (ItemUOMsList != null && ItemUOMsList.Count > 0)
                                                        {
                                                            foreach (IcItemUnitOfMeasures itemUOM in ItemUOMsList)
                                                            {
                                                                if (itemUOM.UnitOfMeasureId != UOMObj.UnitOfMeasureId)
                                                                {
                                                                    itemUOM.MarkOld();
                                                                    itemUOM.MarkModified();
                                                                    itemUOM.UpdatedBy = 1;
                                                                    itemUOM.StockingUnit = false;
                                                                    iIcItemUnitOfMeasuresManager.Save(itemUOM);
                                                                }
                                                                else
                                                                {
                                                                    //  itemUOM.ItemUnitOfMeasureId = uomId;
                                                                    itemUOM.MarkDeleted();
                                                                    iIcItemUnitOfMeasuresManager.Save(itemUOM);
                                                                }

                                                            }
                                                        }

                                                        if (UOMObj != null)
                                                        {
                                                            IcItemUnitOfMeasures itemUOM = new IcItemUnitOfMeasures();

                                                            itemUOM.ItemCardId = item.ItemCardId;
                                                            itemUOM.UnitOfMeasureId = uomId;
                                                            itemUOM.ConversionFactor = (double)UOMObj.DefaultConversionFactor;
                                                            itemUOM.CreatedBy = 1;
                                                            itemUOM.StockingUnit = true;
                                                            iIcItemUnitOfMeasuresManager.Save(itemUOM);

                                                        }
                                                    }

                                                    SaveItemCard(item, false, CheckItemExistanceReturnItemId(item.ItemNumber));







                                                }
                                                else   //If ADD
                                                {
                                                    int uomId = item.WeightUnitOfMeasureId;
                                                    item.WeightUnitOfMeasureId = 0;
                                                    SaveItemCard(item, true, -1);

                                                    if (uomId > 0)
                                                    {
                                                        if (uomId != oldUOMId)
                                                            UOMObj = iIcUnitOfMeasuresManager.FindById(uomId, null);
                                                        oldUOMId = uomId;
                                                        if (UOMObj != null)
                                                        {
                                                            IcItemUnitOfMeasures itemUOM = new IcItemUnitOfMeasures();

                                                            itemUOM.ItemCardId = item.ItemCardId;
                                                            itemUOM.UnitOfMeasureId = uomId;
                                                            itemUOM.ConversionFactor = (double)UOMObj.DefaultConversionFactor;
                                                            itemUOM.CreatedBy = 1;
                                                            itemUOM.StockingUnit = true;
                                                            iIcItemUnitOfMeasuresManager.Save(itemUOM);

                                                        }
                                                    }
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                AddError(string.Format("Exception Occured while Saving the item : {0} ", item.ItemNumber), 0);
                                            }
                                        }
                                    }

                                    if (Errors.Length > 0)
                                    {
                                        LogErrors(Errors.ToString());
                                        result.Add("result", false.ToString());
                                        result.Add("LogPath", ErrorLogFilePath);
                                        return result;
                                    }
                                    else
                                    {
                                        result.Add("result", true.ToString());
                                        result.Add("LogPath", ErrorLogFilePath);
                                        return result;
                                    }

                                }
                            }
                            else if (i < SheetName.Count - 1)
                                continue;
                            else
                                AddError("Items Excel sheet name should be Items", 0); // Check if The sheet name is Items



                            if (Errors != null && Errors.Length > 0)
                            {
                                LogErrors(Errors.ToString());
                                result.Add("result", false.ToString());
                                result.Add("LogPath", ErrorLogFilePath);
                                return result;
                            }


                        }
                        catch (Exception ex)
                        {
                            if (con2.State == ConnectionState.Open)
                                con2.Close();

                            AddError("Exception Occured while Saving the items", 0);
                            LogErrors(Errors.ToString());
                            result.Add("result", false.ToString());
                            result.Add("LogPath", ErrorLogFilePath);
                            return result;
                        }




                    }


                }
            }
            catch (Exception ex)
            {


                AddError("Exception Occured while Saving the items at last", 0);
                LogErrors(Errors.ToString());
                result.Add("result", false.ToString());
                result.Add("LogPath", ErrorLogFilePath);
                return result;
            }


            Library.WriteErrorLog("End Validating");
            return result;

        }
    

    

        private void AddError(string error, int lineNumber)
        {
          //  error = string.Format("Line #{0} : {1}", lineNumber.ToString(), error);
            error = string.Format("{0}", error);
            Errors.AppendLine(error);

        }
        public int HandleIcStructureCode(string code)
        {
            string SQL = "SELECT item_structure_id from dbo.tbl_ic_item_structure WHERE flag_deleted=0 AND item_structure_code=@code";

            Dictionary<string, object> paramsters = new Dictionary<string, object>();
            paramsters.Add("@code", code);
            System.Data.DataTable dt = Data.GetTableOf(SQL, paramsters);

            if (dt != null && dt.Rows.Count != 0)
                return Convert.ToInt32(dt.Rows[0]["item_structure_id"].ToString());
            else
                return -1;
        }

        public int HandleIcPriceList(string code)
        {
            //string SQL = "SELECT  price_list_group_id FROM dbo.tbl_ic_price_list_group INNER JOIN dbo.tbl_ic_price_list ON dbo.tbl_ic_price_list.price_list_id = dbo.tbl_ic_price_list_group.price_list_id AND dbo.tbl_ic_price_list.flag_deleted = 0 AND price_list_code=@code WHERE   tbl_ic_price_list_group.flag_deleted = 0";

            string SQL = "SELECT price_list_id FROM dbo.tbl_ic_price_list WHERE flag_deleted=0 AND price_list_code=@code";

            Dictionary<string, object> paramsters = new Dictionary<string, object>();
            paramsters.Add("@code", code);
            System.Data.DataTable dt = Data.GetTableOf(SQL, paramsters);


            if (dt != null && dt.Rows.Count != 0)
                return Convert.ToInt32(dt.Rows[0]["price_list_id"].ToString());
            else
                return -1;
        }

        public int HandleIcCategory(string code)
        {
            string SQL = "SELECT category_id from dbo.tbl_ic_category WHERE flag_deleted=0 AND category_code=@code";

            Dictionary<string, object> paramsters = new Dictionary<string, object>();
            paramsters.Add("@code", code);
            System.Data.DataTable dt = Data.GetTableOf(SQL, paramsters);
            if (dt != null && dt.Rows.Count != 0)
                return Convert.ToInt32(dt.Rows[0]["category_id"].ToString());
            else
                return -1;
        }

        public int HandleIcAccountSet(string code)
        {
            string SQL = "SELECT account_set_id from dbo.tbl_ic_account_set WHERE flag_deleted=0 AND account_set_code=@code";
            Dictionary<string, object> paramsters = new Dictionary<string, object>();
            paramsters.Add("@code", code);
            System.Data.DataTable dt = Data.GetTableOf(SQL, paramsters);
            if (dt != null && dt.Rows.Count != 0)
                return Convert.ToInt32(dt.Rows[0]["account_set_id"].ToString());
            else
                return -1;
        }

        public int HandleIcCoastingMethod(string code)
        {
            string SQL = "SELECT data_type_content_id from dbo.tbl_data_type_content WHERE flag_deleted=0 AND data_type_content=@code and data_type_id=@data_type_id";
            Dictionary<string, object> paramsters = new Dictionary<string, object>();
            paramsters.Add("@code", code);
            paramsters.Add("@data_type_id", DataTypes.CostingMethodId);
            System.Data.DataTable dt = Data.GetTableOf(SQL, paramsters);
            if (dt != null && dt.Rows.Count != 0)
                return Convert.ToInt32(dt.Rows[0]["data_type_content_id"].ToString());
            else
                return -1;
        }

        public int HandleIcItemSegmentCode(string code, int segmentId)
        {
            int segmentCodeId = (from item in SegmentCodeList where item.SegmentCode == code && item.SegmentId == segmentId select item.SegmentCodeId).ToList().FirstOrDefault();
            if (segmentCodeId > 0) return segmentCodeId;
            else return -1;

            //string SQL = "SELECT segment_code_id from dbo.tbl_ic_segment_code WHERE segment_id=@segmentId  AND flag_deleted=0 AND segment_code=@code";
            //Dictionary<string, object> paramsters = new Dictionary<string, object>();
            //paramsters.Add("@segmentId", segmentId);
            //paramsters.Add("@code", code);
            //System.Data.DataTable dt = Data.GetTableOf(SQL, paramsters);
            //if (dt != null && dt.Rows.Count != 0)
            //    return Convert.ToInt32(dt.Rows[0]["segment_code_id"].ToString());
            //else
            //    return -1;
        }

        public int HandleIcCurrencyCode(string code)
        {
            string SQL = "SELECT currency_id from dbo.tbl_Currency WHERE flag_deleted=0 AND code=@code";

            Dictionary<string, object> paramsters = new Dictionary<string, object>();
            paramsters.Add("@code", code);
            System.Data.DataTable dt = Data.GetTableOf(SQL, paramsters);


            if (dt != null && dt.Rows.Count != 0)
                return Convert.ToInt32(dt.Rows[0]["currency_id"].ToString());
            else
                return -1;
        }

        public int HandleIcUOM(string code)
        {
            string SQL = "SELECT unit_of_measure_id from dbo.tbl_ic_unit_of_measures WHERE flag_deleted=0 AND unit_of_measure=@code";
            Dictionary<string, object> paramsters = new Dictionary<string, object>();
            paramsters.Add("@code", code);
            System.Data.DataTable dt = Data.GetTableOf(SQL, paramsters);
            if (dt != null && dt.Rows.Count != 0)
                return Convert.ToInt32(dt.Rows[0]["unit_of_measure_id"].ToString());
            else
                return -1;
        }


        public taxResult HandleTaxRate(double taxRate)
        {
            taxResult taxRes = new taxResult();
            if (taxRatesList.Count > 0)
                taxRatesList.Clear();
            int DefaultTaxAuthorityId = -1;
            string SQL = "SELECT default_tax_authority_id FROM dbo.tbl_ic_options WHERE flag_deleted = 0";
             System.Data.DataTable dt = Data.GetTableOf(SQL, null);
             if (dt != null && dt.Rows.Count != 0)
                 DefaultTaxAuthorityId = Convert.ToInt32(dt.Rows[0]["default_tax_authority_id"].ToString());
             if (DefaultTaxAuthorityId > -1)
             {
                 taxClassDetailsCrit.TaxAuthorityId = DefaultTaxAuthorityId;
                 taxClassDetailsCrit.TransactionTypeId = 24;  //purchase
                 taxClassDetailsCrit.ClassType = 28;
                 TaxClassDetailsList = iTaxClassDetailsMngr.FindAll(taxClassDetailsCrit);

                 if (TaxClassDetailsList != null && TaxClassDetailsList.Count > 0)
                 {
                     foreach (AdS.TaxClassDetails taxDetail in TaxClassDetailsList)
                     {
                         taxRateCrit.TaxClassDetailId = taxDetail.TaxClassDetailsId;
                         taxRateCrit.TaxAuthorityID = DefaultTaxAuthorityId;
                         taxRateCrit.TransactionTypeId = 24;
                         taxRatesListTemp = iTaxRateMngr.FindAllLite(taxRateCrit);

                         taxRatesList.Add(taxRatesListTemp.First());

                     }
                 }

                 if (taxRatesList != null && taxRatesList.Count > 0)
                 {
                     foreach (AdS.TaxRateLite taxDetailRate in taxRatesList)
                     {
                         if (taxDetailRate.Rate == taxRate)
                         {
                             taxRes.result = true;
                             taxRes.taxClassDetailsPurchase = taxDetailRate.Details1Id;
                             break;
                         }
                     }
                 }
                 else
                 {
                     taxRes.result = false;  //When Purchase has no related Tax Class Rate as specified
                     taxRes.taxClassDetailsPurchase = -1;
                 }


                 taxRatesList.Clear();
                 taxClassDetailsCrit.TransactionTypeId = 25;   //Sales 
                 TaxClassDetailsList = iTaxClassDetailsMngr.FindAll(taxClassDetailsCrit);


                 if (TaxClassDetailsList != null && TaxClassDetailsList.Count > 0)
                 {
                     foreach (AdS.TaxClassDetails taxDetail in TaxClassDetailsList)
                     {
                         taxRateCrit.TaxClassDetailId = taxDetail.TaxClassDetailsId;
                         taxRateCrit.TaxAuthorityID = DefaultTaxAuthorityId;
                         taxRateCrit.TransactionTypeId = 25;
                         taxRatesListTemp = iTaxRateMngr.FindAllLite(taxRateCrit);

                         taxRatesList.Add(taxRatesListTemp.First());

                     }
                 }

                 if (taxRatesList != null && taxRatesList.Count > 0)
                 {
                     foreach (AdS.TaxRateLite taxDetailRate in taxRatesList)
                     {
                         if (taxDetailRate.Rate == taxRate)
                         {
                             if (taxRes.taxClassDetailsPurchase > 0)
                                 taxRes.result = true;

                             taxRes.taxClassDetailsSales = taxDetailRate.Details1Id;
                             break;
                         }
                     }
                 }
                 else
                 {
                     taxRes.result = false;  //When Sales has no related Tax Class Rate as specified
                     taxRes.taxClassDetailsSales = -1;
                     
                 }
                 taxRes.taxAuthorityId = DefaultTaxAuthorityId;

             }


             return taxRes;

        }





        public void SaveItemCard(E.IcItemCard itemCard, bool isAdd , int itemId)
        {

            if (isAdd)
            {
                icItemCardManager.Save(itemCard);

                foreach (IcItemCardSegment item in itemCard.ItemCardSegments)
                {
                    item.ItemCardId = itemCard.ItemCardId;
                    item.CreatedDate = DateTime.Now;
                    iIcItemCardSegmentManager.Save(item);
                }
                if (itemCard.PriceListGroup != null)
                {
                    itemCard.PriceListGroup.ItemCardId = itemCard.ItemCardId;
                    iIcPriceListGroupManager.Save(itemCard.PriceListGroup);
                }
            }
            else
            {
                itemCard.MarkOld();
                itemCard.MarkModified();
                itemCard.ItemCardId = itemId;
                itemCard.UpdatedBy = 1;


                icItemCardManager.Save(itemCard);


                if (itemCard.PriceListGroup != null)
                {
                    itemCard.PriceListGroup.ItemCardId = itemCard.ItemCardId;
                    iIcPriceListGroupManager.Save(itemCard.PriceListGroup);
                }


            }


            // Start Saving Items Taxes
            if (ItemTaxList != null && ItemTaxList.Count > 0)
            {
                int tempItemId = -1;
                int ItemTaxIdTemp = -1;
                IcItemTax ItemTaxToSave = new IcItemTax();
                foreach (taxTempResult itmTx in ItemTaxList)
                {
                    tempItemId = CheckItemExistanceReturnItemId(itmTx.ItemNumber);
                    if (tempItemId > 0)
                    {
                        ItemTaxToSave.ItemCardId = tempItemId;
                        ItemTaxToSave.TaxAuthorityId = itmTx.TaxAuthorityId;
                        ItemTaxToSave.SalesTaxClassId = itmTx.SalesClassId;
                        ItemTaxToSave.PurchaseTaxClassId = itmTx.PurchaseClassId;
                        

                        //Go Find This ItemTax and Update
                        ItemTaxIdTemp = GetItemTaxId(tempItemId, ItemTaxToSave.TaxAuthorityId);
                        if (ItemTaxIdTemp != null && ItemTaxIdTemp > 0)
                        {
                            ItemTaxToSave.MarkOld();
                            ItemTaxToSave.MarkModified();
                            ItemTaxToSave.ItemTaxId = ItemTaxIdTemp;
                            ItemTaxToSave.UpdatedBy = 1;
                            iIcItemTaxManager.Save(ItemTaxToSave);
                                 
                        }

                    }
                }
            }

        }


       


        public bool BuildItemNumber(E.IcItemCard itemCard)
        {
            if (oldItemStructureCode != itemCard.StructureCode)
            {
                segmentList = getStructureSegments(itemCard.StructureCode);//oldItemStructureId
                oldItemStructureCode = itemCard.StructureCode;
            }
            string ItemNumber = itemCard.ItemNumber;

            List<TempIcItemSegment> tempIcItemSegmentList = new List<TempIcItemSegment>();
            var valid = true;
            if (segmentList != null && segmentList.Count > 0)
            {

                string[] itemSegmentCodes = ItemNumber.Split('-');
                if (itemSegmentCodes.Length != segmentList.Count())
                {
                    valid = false;
                    return valid;
                }
                for (int i = 0; i < segmentList.Count(); i++)
                {
                    TempIcStructureSegment item = segmentList[i];
                    tempIcItemSegmentList.Add(new TempIcItemSegment() { SegmentId = item.SegmentId, SegmentLength = item.SegmentLength, Sperator = "", code = itemSegmentCodes[i] });

                }
                valid = valid && CheckItemNumberSegmentCodes(tempIcItemSegmentList, itemCard);


            }
            return valid;
            //  int totalSegmnetsLength = (from item in segmentList select item.SegmentLength).Sum();

            //    if (ItemNumber.Length != totalSegmnetsLength)
            //    {
            //        //Error Item Number Length
            //        //  itemCard.ItemNumber = "";
            //        valid = false;
            //        return valid;
            //    }
            //    int startIndex = 0;
            //    foreach (TempIcStructureSegment item in segmentList)
            //    {
            //        tempIcItemSegmentList.Add(new TempIcItemSegment() { SegmentId = item.SegmentId, SegmentLength = item.SegmentLength, Sperator = item.Sperator, code = ItemNumber.Substring(startIndex, item.SegmentLength) });
            //        startIndex += item.SegmentLength;
            //    }
            //}
            //valid = valid && CheckItemNumberSegmentCodes(tempIcItemSegmentList, itemCard);
            //string sperator = "-";
            //itemCard.ItemNumber = string.Join(sperator, (from item in tempIcItemSegmentList select item.code).ToArray());//string.Concat((from item in tempIcItemSegmentList select item.code).ToList(), sperator);
            //return valid;
        }

        public bool CheckItemNumberSegmentCodes(List<TempIcItemSegment> tempIcItemSegmentList, E.IcItemCard itemCard)
        {

            int segmentCodeId = -1;
            itemCard.ItemCardSegments = new List<IcItemCardSegment>();
            foreach (TempIcItemSegment item in tempIcItemSegmentList)
            {
                if (item.SegmentId == BaseSegmentId)
                    continue;

                segmentCodeId = HandleIcItemSegmentCode(item.code, item.SegmentId);
                if (segmentCodeId <= 0)
                    return false;
                itemCard.ItemCardSegments.Add(new IcItemCardSegment() { SegmentId = item.SegmentId, SegmentCodeId = segmentCodeId });
            }
            return true;
        }

        public bool CheckItemExistance(string ItemNumber)
        {
            bool exist = false;

            string SQL = "SELECT item_card_id from dbo.tbl_ic_item_card WHERE flag_deleted=0 AND  item_number='" + ItemNumber + "'";
            System.Data.DataTable dt = Data.GetTableOf(SQL, false);
            if (dt != null && dt.Rows.Count != 0)
            {
                if (Convert.ToInt32(dt.Rows[0]["item_card_id"].ToString()) > 0)
                    exist = true;
            }
            else exist = false;

            return exist;
        }


        public bool CheckItemBarCodeExistance(string ItemNumber , string barCode)
        {
            bool exist = false;

            string SQL = "SELECT item_card_id from dbo.tbl_ic_item_card WHERE flag_deleted=0 AND  item_number!='" + ItemNumber + "' AND item_bar_code='" + barCode + "'";
            System.Data.DataTable dt = Data.GetTableOf(SQL, false);
            if (dt != null && dt.Rows.Count != 0)
            {
                if (Convert.ToInt32(dt.Rows[0]["item_card_id"].ToString()) > 0)
                    exist = true;
            }
            else exist = false;

            return exist;
        }


        public int CheckItemExistanceReturnItemId(string ItemNumber)
        {
            bool exist = false;

            string SQL = "SELECT item_card_id from dbo.tbl_ic_item_card WHERE flag_deleted=0 AND  item_number='" + ItemNumber + "'";
            System.Data.DataTable dt = Data.GetTableOf(SQL, false);
            if (dt != null && dt.Rows.Count != 0)
            {
                if (Convert.ToInt32(dt.Rows[0]["item_card_id"].ToString()) > 0)
                    exist = true;
            }
            else exist = false;
            if (exist)
                return Convert.ToInt32(dt.Rows[0]["item_card_id"].ToString());
            else
                return -1;
        }


         public int GetItemTaxId(int itemCardId, int taxAuthorityId)
        {
            int ItemTaxId = -1;

            string SQL = "SELECT item_tax_id FROM dbo.tbl_ic_item_tax WHERE flag_deleted = 0 AND item_card_id = '" + itemCardId + "' AND tax_authority_id = '" + taxAuthorityId + "'";
            System.Data.DataTable dt = Data.GetTableOf(SQL, false);
            if (dt != null && dt.Rows.Count != 0)
            {
                if (Convert.ToInt32(dt.Rows[0]["item_tax_id"].ToString()) > 0)
                {
                    ItemTaxId = Convert.ToInt32(dt.Rows[0]["item_tax_id"].ToString());
                }
            }

            return ItemTaxId;

        }


        public List<TempIcStructureSegment> getStructureSegments(int structureId)
        {
            List<TempIcStructureSegment> segList = new List<TempIcStructureSegment>();

            string SQL = "SELECT  dbo.tbl_ic_segment.segment_id , segment_length ,'-' seperator FROM    dbo.tbl_ic_item_structure_segment INNER JOIN dbo.tbl_ic_segment ON dbo.tbl_ic_segment.segment_id = dbo.tbl_ic_item_structure_segment.segment_id AND dbo.tbl_ic_segment.flag_deleted = 0 WHERE dbo.tbl_ic_item_structure_segment.flag_deleted=0 and item_structure_id = '" + structureId + "'";
            System.Data.DataTable dt = Data.GetTableOf(SQL, false);
            if (dt != null && dt.Rows.Count != 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    segList.Add(new TempIcStructureSegment()
                    {
                        SegmentId = Convert.ToInt32(dt.Rows[i]["segment_id"].ToString()),
                        SegmentLength = Convert.ToInt32(dt.Rows[i]["segment_length"].ToString()),
                        Sperator = dt.Rows[i]["seperator"].ToString()
                    });
                }
                return segList;
            }
            else
                return segList;
        }

        public void LogErrors(string Errors)
        {
            string dir = ErrorLogFolderPath;
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);
            string fileName = "Errors-" + DateTime.Now.ToString("dd-MM-yy_HHmm") + ".txt";
            ErrorLogFilePath = fileName;
            FileStream file = System.IO.File.Create(dir + fileName);
            StreamWriter sw = new StreamWriter(file);
            sw.WriteLine(Errors);
            sw.AutoFlush = true;
            file.Close();
        }

        public List<TempIcSegmentCodeList> getStructureSegmentCodes()
        {
            List<TempIcSegmentCodeList> segCodeList = new List<TempIcSegmentCodeList>();

            string SQL = "SELECT  segment_id ,segment_code_id ,segment_code FROM    dbo.tbl_ic_segment_code WHERE   flag_deleted = 0";
            System.Data.DataTable dt = Data.GetTableOf(SQL, false);
            if (dt != null && dt.Rows.Count != 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    segCodeList.Add(new TempIcSegmentCodeList()
                    {
                        SegmentId = Convert.ToInt32(dt.Rows[i]["segment_id"].ToString()),
                        SegmentCode = Convert.ToString(dt.Rows[i]["segment_code"].ToString()),
                        SegmentCodeId = Convert.ToInt32(dt.Rows[i]["segment_code_id"].ToString())
                    });
                }
                return segCodeList;
            }
            else
                return segCodeList;
        }
        private bool GrantAccess(string fullPath)
        {
            //DirectoryInfo dInfo = new DirectoryInfo(fullPath);
            //DirectorySecurity dSecurity = dInfo.GetAccessControl();
            //dSecurity.AddAccessRule(new FileSystemAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null), FileSystemRights.FullControl, InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit, PropagationFlags.NoPropagateInherit, AccessControlType.Allow));
            //dInfo.SetAccessControl(dSecurity);
            return true;
        }
    }

    public class TempItem
    {
        public string StructureCode;
        public string ItemNumber;
        public string ItemDescription;
        public string SKU;
        public string Barcode;
        public string CostingMethod;
        public string DefaultPriceListCode;
        public string CurrencyCode;
        public double BasePrice;
        public string UOM;
        public string SerialNumber;
        public string LotNumber;
        public string Category;
        public string AccountSetCode;
        public int ExcelSheetLineNumber;

        public double Tax;
    }

    public class TempIcStructureSegment
    {
        public int SegmentId;
        public int SegmentLength;
        public string Sperator;
    }

    public class TempIcItemSegment
    {
        public int SegmentId;
        public int SegmentLength;
        public string Sperator;
        public string code;
    }

    public class TempIcSegmentCodeList
    {
        public int SegmentId;
        public string SegmentCode;
        public int SegmentCodeId;
    }
    public enum DataTypes
    {
        BucketTypeId = 64,
        AdjustmentTypeId = 63,
        CostingMethodId = 51
    }

    public class taxResult
    {
        public bool result;
        public int taxClassDetailsPurchase;
        public int taxClassDetailsSales;
        public int taxAuthorityId;
             
    }


    public class taxTempResult
    {
        public string ItemNumber;
        public int TaxAuthorityId;
        public int SalesClassId;
        public int PurchaseClassId;
    }

       
}  
