﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Centrix.Inventory.Business.Manager;
using Centrix.Inventory.Business.IManager;
using CX.Framework;
using System.Text;
using Centrix.Inventory.Business.Entity;
using System.IO;
using CentrixERP.Configuration;
using System.Data.SqlClient;
using WindowsService1;
using System.Data.OleDb;
using System.Data;
using CentrixERP.Common.Business.Entity;

namespace CentrixERP.Agent.Workers
{
    public class IcAdjustmentMigration
    {
        IcItemCardManager icItemCardManager = (IcItemCardManager)SF.Framework.IoC.Instance.Resolve(typeof(IcItemCardManager));
        IcAdjustmentManager icAdjustmentManager = (IcAdjustmentManager)SF.Framework.IoC.Instance.Resolve(typeof(IcAdjustmentManager));
        IIcDocumentNumbersManager iIcDocumentNumbersManager = (IIcDocumentNumbersManager)SF.Framework.IoC.Instance.Resolve(typeof(IIcDocumentNumbersManager));
        IIcCommonQueriesManager iIcCommonQueries = (IIcCommonQueriesManager)IoC.Instance.Resolve(typeof(IIcCommonQueriesManager));
        IIcItemCardManager iIcItemCardManager = (IIcItemCardManager)SF.Framework.IoC.Instance.Resolve(typeof(IIcItemCardManager));
        IIcItemCardLotSummationManager iIcItemCardLotSummationManager = (IIcItemCardLotSummationManager)SF.Framework.IoC.Instance.Resolve(typeof(IIcItemCardLotSummationManager));
        private string Excel07ConString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 8.0;HDR={1}'";
        SqlConnection cnn22 = new SqlConnection("Data Source=192.168.2.225;Initial Catalog=CentrixERPV3.3.27.Dev;Persist Security Info=True;User ID=sa;Password=Pocketaps!2;");
        SqlConnection cnn = new SqlConnection("Data Source=192.168.2.225;Initial Catalog=CentrixERPV3.3.27.Dev;Persist Security Info=True;User ID=sa;Password=Pocketaps!2;");
        IIcItemCardLotManager iIcItemCardLotManager = (IIcItemCardLotManager)SF.Framework.IoC.Instance.Resolve(typeof(IIcItemCardLotManager));



        public string ExcelFilePath;
        public string ErrorLogFolderPath;
        private StringBuilder Errors;
        private StringBuilder AdjustmentsNumbers;
        public string ErrorLogFilePath { get; set; }
        

        public Dictionary<string, string> DoWork(string fileName, string logFilePath)
        {
            ExcelFilePath = fileName;
            ErrorLogFolderPath = logFilePath;
            return validateExcelSheetFile_Adjustment();
        }

        public Dictionary<string, string> validateExcelSheetFile_Adjustment()
        {

            Errors = new StringBuilder();
            AdjustmentsNumbers = new StringBuilder();
           // bool AdjustmentsContainLots = false;
            List<TempAdjustment> tempAdjustmentList = null;
            bool firstTime = true;
            List<IcAdjustment> AdjustmentList = null;
            IcAdjustmentDetail adjDetails = null;
            IcAdjustment Adjustment = null;

           // List<TempSerialLot> tempLotsListOriginal = null;

          //  List<string> AdjustmentsLotsLinks = null;

            int AdjustmentSheetIndex = -1;   // LotsSheetIndex = -1;



            Dictionary<string, string> result = new Dictionary<string, string>();

            try
            {
                Library.WriteErrorLog("Start Validating");

                string conStr;
                conStr = string.Format(Excel07ConString, ExcelFilePath, "Yes");


                List<string> SheetName = new List<string>();

                System.Data.DataTable dt;
               // List<TempItem> tempItemList;

                //   Get name of the Excel file.
                using (OleDbConnection con = new OleDbConnection(conStr))
                {
                    using (OleDbCommand cmd = new OleDbCommand())
                    {
                        con.Open();
                        cmd.Connection = con;

                        //Get name of Sheets in file
                        System.Data.DataTable dtsheet = new System.Data.DataTable();
                        dtsheet = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);   // Read Excel SubSheet


                        foreach (DataRow sheet in dtsheet.Rows)
                        {

                            string SubSheet = sheet["Table_Name"].ToString();

                            //Remove the $ from the end of sheets name
                            if (SubSheet.Length > 1 && SubSheet.EndsWith("$"))
                                SheetName.Add(SubSheet.Substring(0, SubSheet.Length - 1));

                        }

                    }
                   // con.Close();
                }


                Object thisLock = new Object();
                OleDbConnection con2 = new OleDbConnection(conStr);

                AdjustmentSheetIndex = SheetName.FindIndex(x => x.StartsWith("Adjustments"));
                if (AdjustmentSheetIndex != null && AdjustmentSheetIndex >= 0 && SheetName[AdjustmentSheetIndex].ToString() != "Adjustments")
                    AddError("Items Excel sheet name should be Adjustments", 0, true);
                //LotsSheetIndex = SheetName.FindIndex(x => x.StartsWith("Lots"));
                //if (LotsSheetIndex != null && LotsSheetIndex >= 0 && SheetName[LotsSheetIndex].ToString() != "Lots")
                //    AddError("Items Excel sheet name should be Lots", 0, false);

              

                    lock (thisLock)
                    {
                        int i = AdjustmentSheetIndex;
                       // int l = LotsSheetIndex;

                        try
                        {
                            if (i >= 0 && SheetName[i] == "Adjustments")
                            {
                                dt = new System.Data.DataTable();

                                OleDbCommand cmd2 = new OleDbCommand("SELECT * From [" + SheetName[i] + "$" + "]", con2); //Read Sheet Data from DB

                                if (con2.State == ConnectionState.Closed)
                                    con2.Open();

                                OleDbDataAdapter oda = new OleDbDataAdapter();
                                oda.SelectCommand = cmd2;
                                oda.Fill(dt);
                                con2.Close();

                                AdjustmentList = new List<IcAdjustment>();
                                 adjDetails = new IcAdjustmentDetail();
                                 

                                DataColumnCollection dtCol = dt.Columns;


                                if (dtCol != null)
                                {
                                    for (int ii = 0; ii <= dtCol.Count; ii++)
                                    {
                                        if (ii == 0 && !dtCol[ii].ColumnName.ToLower().Equals("adjustmentdate"))
                                        {
                                            AddError("Check Excel Template : First Column should be adjustmentdate", 1, true);
                                        }
                                        else if (ii == 1 && !dtCol[ii].ColumnName.ToLower().Equals("reference"))
                                        {
                                            AddError("Check Excel Template : Second Column should be reference", 1, true);
                                        }
                                        else if (ii == 2 && !dtCol[ii].ColumnName.ToLower().Equals("description"))
                                        {
                                            AddError("Check Excel Template : Third Column should be description", 1, true);
                                        }
                                        else if (ii == 3 && !dtCol[ii].ColumnName.ToLower().Equals("itemnumber"))
                                        {
                                            AddError("Check Excel Template : Forth Column should be itemnumber", 1, true);
                                        }
                                        else if (ii == 4 && !dtCol[ii].ColumnName.ToLower().Equals("location"))
                                        {
                                            AddError("Check Excel Template : Fifth Column should be location", 1, true);
                                        }
                                        else if (ii == 5 && !dtCol[ii].ColumnName.ToLower().Equals("adjustmenttype"))
                                        {
                                            AddError("Check Excel Template : Sixth Column should be adjustmenttype", 1, true);
                                        }
                                        else if (ii == 6 && !dtCol[ii].ColumnName.ToLower().Equals("qty"))
                                        {
                                            AddError("Check Excel Template : Seventh Column should be qty", 1, true);
                                        }
                                        else if (ii == 7 && !dtCol[ii].ColumnName.ToLower().Equals("costadjutment"))
                                        {
                                            AddError("Check Excel Template : Eighth Column should be costadjutment", 1, true);
                                        }
                                        else if (ii == 8 && !dtCol[ii].ColumnName.ToLower().Equals("uom"))
                                        {
                                            AddError("Check Excel Template : Ninth Column should be uom", 1, true);
                                        }
                                        else if (ii == 9 && !dtCol[ii].ColumnName.ToLower().Equals("adjustmentaccount"))
                                        {
                                            AddError("Check Excel Template : Tenth Column should be adjustmentaccount", 1, true);
                                        }
                                        //else if (ii == 10 && !dtCol[ii].ColumnName.ToLower().Equals("lot"))
                                        //{
                                        //    AddError("Check Excel Template : Eleventh Column should be lot", 1, true);
                                        //}

                                    }
                                }


                                //Fill The Sheet In temp list
                            tempAdjustmentList = new List<TempAdjustment>();


                                for (int j = 0; j <= dt.Rows.Count - 1; j++) // check if the row is empty                                
                                {
                                    TempAdjustment tempAdj = new TempAdjustment();

                                    tempAdj.AdjustmentDate =dt.Rows[j]["adjustmentdate"].ToString();
                                    tempAdj.Reference = dt.Rows[j]["reference"].ToString();
                                    tempAdj.Description = dt.Rows[j]["description"].ToString();
                                    tempAdj.ItemNumber = dt.Rows[j]["itemnumber"].ToString();
                                    tempAdj.Location = dt.Rows[j]["location"].ToString();
                                    tempAdj.AdjustmentType = dt.Rows[j]["adjustmenttype"].ToString();
                                    tempAdj.QTY = dt.Rows[j]["qty"].ToString();
                                    tempAdj.CostAdjutment = dt.Rows[j]["costadjutment"].ToString();
                                    tempAdj.UOM = dt.Rows[j]["uom"].ToString();
                                    tempAdj.AdjustmentAccount = dt.Rows[j]["adjustmentaccount"].ToString();
                                    tempAdj.ExcelSheetLineNumber = i;
                                  //  tempAdj.serialLotAdjustmentIdentifier = (dt.Rows[j]["lot"].ToString());
                                    if (!string.IsNullOrEmpty(tempAdj.ItemNumber))
                                        tempAdjustmentList.Add(tempAdj);

                                    //if (tempAdj.serialLotAdjustmentIdentifier != null && tempAdj.serialLotAdjustmentIdentifier.Trim() != "" && AdjustmentsContainLots == false)
                                    //    AdjustmentsContainLots = true;
                                    //if(tempAdj.serialLotAdjustmentIdentifier != null && tempAdj.serialLotAdjustmentIdentifier.Trim() != "")
                                    //{
                                    //    if (AdjustmentsLotsLinks == null)
                                    //        AdjustmentsLotsLinks = new List<string>();
                                    //    AdjustmentsLotsLinks.Add(tempAdj.serialLotAdjustmentIdentifier.Trim());
                                    //}


                                }

                            

                            }


                            // if (l >= 0 && SheetName[l] == "Lots")
                            //{

                            //    dt = new System.Data.DataTable();
                            //    DataRow[] foundRows = null;

                            //    OleDbCommand cmd2 = new OleDbCommand("SELECT * From [" + SheetName[l] + "$" + "]", con2); //Read Sheet Data from DB

                            //    if (con2.State == ConnectionState.Closed)
                            //        con2.Open();

                            //    OleDbDataAdapter oda = new OleDbDataAdapter();
                            //    oda.SelectCommand = cmd2;
                            //    oda.Fill(dt);
                            //    con2.Close();


                            //    DataColumnCollection dtCol = dt.Columns;


                            //    if (dtCol != null)
                            //    {
                            //        for (int ii = 0; ii <= dtCol.Count; ii++)
                            //        {
                            //            if (ii == 0 && !dtCol[ii].ColumnName.ToLower().Equals("lot"))
                            //            {
                            //                AddError("Check Excel Template : First Column should be lot", 1, false);
                            //            }
                            //            else if (ii == 1 && !dtCol[ii].ColumnName.ToLower().Equals("lotnumber"))
                            //            {
                            //                AddError("Check Excel Template : Second Column should be lotnumber", 1, false);
                            //            }
                            //            else if (ii == 2 && !dtCol[ii].ColumnName.ToLower().Equals("quantity"))
                            //            {
                            //                AddError("Check Excel Template : Third Column should be quantity", 1, false);
                            //            }
                            //            else if (ii == 3 && !dtCol[ii].ColumnName.ToLower().Equals("expirydate"))
                            //            {
                            //                AddError("Check Excel Template : Forth Column should be expirydate", 1, false);
                            //            }

                            //        }
                            //    }


                            //    tempLotsListOriginal = new List<TempSerialLot>();
                            //    for (int k = 0; k <= dt.Rows.Count -1; k++) // check if the row is empty
                            //    {
                                   
                            //        TempSerialLot tempLot = new TempSerialLot();
                            //        tempLot.ExcelSheetLineNumber = k;
                            //        tempLot.AdjustmentIdentifier = dt.Rows[k]["lot"].ToString();
                            //        tempLot.ExpiryDate = dt.Rows[k]["expirydate"].ToString();
                            //        tempLot.SerialOrLotNumber = dt.Rows[k]["lotnumber"].ToString();
                            //        if (dt.Rows[k]["quantity"].ToString() == null || dt.Rows[k]["quantity"].ToString() == "")
                            //            dt.Rows[k]["quantity"] = "0";
                            //        tempLot.quantitySerialLot = Convert.ToInt32(dt.Rows[k]["quantity"].ToString());

                            //        if (!string.IsNullOrEmpty(tempLot.AdjustmentIdentifier) && !string.IsNullOrEmpty(tempLot.SerialOrLotNumber) && !string.IsNullOrEmpty(tempLot.ExpiryDate) && !string.IsNullOrEmpty(tempLot.quantitySerialLot.ToString()))
                            //        tempLotsListOriginal.Add(tempLot);
                            //    }


                            //    if (AdjustmentsContainLots)
                            //    {
                            //        // Validate if adjustment lots records existed in the Lots Sheet
                            //        foreach (string s in AdjustmentsLotsLinks)
                            //        {
                            //            foundRows = dt.Select("lot Like '" + s + "'");
                            //            if (foundRows == null || foundRows.Length <= 0)
                            //                AddError("Adjustment With Lot Item has no related record in the Lots Sheet", 1, true);

                            //        }

                            //    }
                            //}



                            if (Errors != null && Errors.Length > 0)
                            {
                                LogErrors(Errors.ToString());
                                result.Add("result", false.ToString());
                                result.Add("LogPath", ErrorLogFilePath);
                                return result;
                            }




                         //   List<TempSerialLot> tempLotsListEliminated = new List<TempSerialLot>();



                            List<string> ErrorArr;
                            if (tempAdjustmentList != null && tempAdjustmentList.Count > 0)
                            {
                                int AdjustmentHeadersCount = 0;
                                int AdjustmentDetailsCount = 0;
                                foreach (TempAdjustment item in tempAdjustmentList)
                                {
                                    ErrorArr = new List<string>();
                                    bool valid = true;
                                    if (firstTime || Adjustment == null || Adjustment.Reference != item.Reference)
                                    {
                                        if (!firstTime)
                                            AdjustmentList.Add(Adjustment);

                                        if (string.IsNullOrEmpty(item.AdjustmentDate))
                                            ErrorArr.Add("Adjustment Date is mandatory");

                                        if (string.IsNullOrEmpty(item.Reference))
                                            ErrorArr.Add("Reference is mandatory");

                                        Adjustment = new IcAdjustment()
                                        {
                                            AdjustmentDate = (!string.IsNullOrEmpty(item.AdjustmentDate) ? Convert.ToDateTime(item.AdjustmentDate) : (DateTime?)null),
                                            PostingDate = (!string.IsNullOrEmpty(item.AdjustmentDate) ? Convert.ToDateTime(item.AdjustmentDate) : (DateTime?)null),
                                            Reference = item.Reference,
                                            CreatedDate = DateTime.Now,
                                            AdjustmentNumber = GenerateAdjustmentNumber(((firstTime) ? 0 : AdjustmentList.Count())),
                                            Status = true,
                                            Description = item.Description,
                                            OptionalFieldEntityList = new List<OptionalFieldEntity>(),
                                            AdjustmentDetails = new List<IcAdjustmentDetail>()
                                        };
                                        firstTime = false;
                                        AdjustmentHeadersCount += 1;
                                        if (AdjustmentHeadersCount > 5000)
                                            ErrorArr.Add("Adjustments Exceed the Maximum Limit, which is 5000");
                                    }


                                    int itemCardId = HandleIcItemCard(item.ItemNumber);
                                    int isSerialOrLot = HandleIcItemCardType(itemCardId);  // 0 Normal , 1 Serial , 2 Lot
                                    int locationId = HandleIcLocation(item.Location);
                                    int AvailableQty = HandleIcQuantity(locationId, itemCardId);
                                    int adjTypeId = HandleIcAdjType(item.AdjustmentType);
                                    int bucketTypeId = (!string.IsNullOrEmpty(item.BucketType)) ? HandleIcBucketType(item.BucketType) : -1;
                                    int UOMId = HandleIcUOM(item.UOM, itemCardId);
                                    int adjustmentAccountId = HandleGlAccount(item.AdjustmentAccount);
                                    int docNumId = -1, lineId;
                                    if (!string.IsNullOrEmpty(item.DocumentNo) && !string.IsNullOrEmpty(item.DocumentNo.Trim()))
                                    {
                                        docNumId = HandleIcDocument(item.DocumentNo.Trim());
                                        //get lineId
                                    }

                                    // Check other validations



                                    if (itemCardId <= 0)
                                    {
                                        ErrorArr.Add(string.Format("Item Number: {0} Does Not Exsit", item.ItemNumber));
                                        valid = false;
                                    }

                                    if (locationId <= 0)
                                    {
                                        ErrorArr.Add(string.Format("{0} as a Location Does Not Exsit", item.Location));
                                        valid = false;
                                    }

                                    if (adjTypeId <= 0)
                                    {
                                        ErrorArr.Add(string.Format("{0} as an Adjustment Type Does Not Exsit", item.AdjustmentType));
                                        valid = false;
                                    }

                                    if (UOMId <= 0)
                                    {
                                        ErrorArr.Add(string.Format("UOM {0} Does Not Exsit For Item Number: {1}", item.UOM, item.ItemNumber));
                                        valid = false;
                                    }



                                    if (!string.IsNullOrEmpty(item.DocumentNo))
                                    {
                                        if (docNumId <= 0)
                                        {
                                            ErrorArr.Add(string.Format("{0} as a Document Number Does Not Exsit", item.DocumentNo));
                                            valid = false;
                                        }
                                        if (bucketTypeId <= 0)
                                        {
                                            ErrorArr.Add(string.Format("{0} as a Bucket Type Does Not Exsit", item.BucketType));
                                            valid = false;
                                        }
                                    }

                                    bool checkQty = false;
                                    bool checkCost = false;

                                    switch (adjTypeId)
                                    {
                                        
                                        case (int)AdjustmentType.QuantityDecrease:
                                            checkQty = true;
                                            break;
                                     
                                        case (int)AdjustmentType.CostDecrease:
                                            checkCost = true;
                                            break;
                                        case (int)AdjustmentType.BothDecrease:
                                     
                                            checkQty = true;
                                            checkCost = true;
                                            break;
                                    }

                                    if (checkQty)
                                    {
                                        if (string.IsNullOrEmpty(item.QTY) || Convert.ToDouble(item.QTY) <= 0)
                                        {
                                            ErrorArr.Add("QTY field should be filled");
                                            valid = false;
                                        }
                                        if (Convert.ToDouble(item.QTY) > AvailableQty)
                                        {
                                            ErrorArr.Add("exceed the available quantity for the item :" + item.ItemNumber);
                                            valid = false;
                                        }

                                        
                                    }

                                    if (checkCost)
                                    {
                                        if (string.IsNullOrEmpty(item.CostAdjutment))
                                        {
                                            ErrorArr.Add("Cost adjutment field should be filled");
                                            valid = false;
                                        }
                                    }


                                    //if (isSerialOrLot == 2 && adjTypeId != (int)AdjustmentType.CostIncrease && adjTypeId != (int)AdjustmentType.CostDecrease)
                                    //    FillTheEliminatedLots(itemCardId, locationId, adjTypeId, item.serialLotAdjustmentIdentifier, Convert.ToInt32(item.QTY), tempLotsListEliminated, tempLotsListOriginal);



                                    if (ErrorArr != null && ErrorArr.Count > 0)
                                    {
                                        foreach (string error in ErrorArr)
                                        {
                                            AddError(error, item.ExcelSheetLineNumber, true);
                                        }
                                    }
                                    if (!valid || ErrorArr.Count > 0)
                                    {
                                        LogErrors(Errors.ToString());
                                        continue;
                                    }





                                    adjDetails = new IcAdjustmentDetail()
                                    {
                                        AdjustmentAccountId = (adjustmentAccountId > 0) ? adjustmentAccountId : (int?)null,
                                        AdjustmentTypeId = adjTypeId,
                                        BucketTypeId = (bucketTypeId > 0) ? bucketTypeId : -1,
                                        CostAdjustment = (!string.IsNullOrEmpty(item.CostAdjutment)) ? Convert.ToDouble(item.CostAdjutment) : 0,
                                        CostDate = (!string.IsNullOrEmpty(item.CostDate)) ? Convert.ToDateTime(item.CostDate) : (DateTime?)null,
                                        Comments = "",
                                        CostUomId = 0,
                                        DocumentNoId = (docNumId > 0) ? docNumId : (int?)null,
                                        ItemCardId = itemCardId,
                                        ItemUnitOfMeasureId = UOMId,
                                        LocationId = locationId,
                                        Quantity = (!string.IsNullOrEmpty(item.QTY)) ? Convert.ToDouble(item.QTY) : 0,
                                        CreatedDate = DateTime.Now,
                                        CreatedBy = 1,
                                        RelatedLots =new List<IcItemCardLot>(), //GetLots(item.serialLotAdjustmentIdentifier, tempLotsListEliminated),
                                        RelatedSerials = new List<IcAdjustmentDetailSerial>(),
                                        OptionalFieldEntityList = new List<OptionalFieldEntity>(),
                                        Adjustment = Adjustment

                                    };
                                    Adjustment.AdjustmentDetails.Add(adjDetails);
                                    AdjustmentDetailsCount += 1;
                                    if (AdjustmentDetailsCount > 100)
                                        ErrorArr.Add("Details in each adjustment must not exceed 100 lines");

                                }

                                //if (tempLotsListOriginal.Count > 0 && tempLotsListEliminated.Count > 0)
                                //    ValidateSerialLotSheetByAdjustmentId(tempLotsListEliminated, tempLotsListOriginal);



                            }
                            else AddError("The Sheet Is Empty", 1, true);

                            if (Errors.Length > 0)
                            {
                                LogErrors(Errors.ToString());
                                result.Add("result", false.ToString());
                                result.Add("LogPath", ErrorLogFilePath);
                                return result;
                            }
                          

                            AdjustmentList.Add(Adjustment);

                            if (AdjustmentList != null && AdjustmentList.Count() > 0)
                            {
                                try
                                {
                                    IcAdjustment adjustmentItem;
                                   // IcItemCardLotSummation icItemCardLotSummation;

                                    foreach (IcAdjustment item in AdjustmentList)
                                    {
                                        adjustmentItem = item;
                                        adjustmentItem.MarkNew();
                                       
                                        UpdateAdjustmentYearPeriodValue(ref adjustmentItem);

                                       
                                        try
                                        {
                                            SaveAdjustment(adjustmentItem);
                                        }
                                        catch (Exception ex )
                                        {
                                            
                                            throw ex ;
                                        }
                                       
                                       // HandleItemCardLotTransactions(adjustmentItem);

                                        UpdateDocumentNumbersNextNumber(adjustmentItem.AdjustmentId);
                                        PostAdjustment(adjustmentItem);

                                        //Save Lot Summation Items

                                        //foreach (IcAdjustmentDetail detail in adjustmentItem.AdjustmentDetails)
                                        //{
                                        //    if (detail.RelatedLots != null && detail.RelatedLots.Count > 0 && (detail.AdjustmentTypeId == (int)AdjustmentType.QuantityIncrease || detail.AdjustmentTypeId == (int)AdjustmentType.BothIncrease))
                                        //        foreach (IcItemCardLot lt in detail.RelatedLots)
                                        //        {

                                        //            //Check If existed 

                                        //            string SQL = "SELECT item_card_lot_summation_id FROM dbo.tbl_ic_item_card_lot_summation WHERE   lot_number = @item_lot_number AND item_card_id = @item_card_id AND location_id = @location_id AND flag_deleted = 0  AND is_system = 0";
                                        //            Dictionary<string, object> parameters = new Dictionary<string, object>();
                                        //            parameters.Add("@item_lot_number", lt.ItemLotNumber);
                                        //            parameters.Add("@item_card_id", lt.ItemCardId);
                                        //            parameters.Add("@location_id", lt.LocationId);
                                        //            System.Data.DataTable dtt = Data.GetTableOf(SQL, parameters);

                                        //            if (dtt != null && dtt.Rows.Count != 0 && dtt.Rows[0]["item_card_lot_summation_id"].ToString() != null && dtt.Rows[0]["item_card_lot_summation_id"].ToString().Length > 0)
                                        //                iIcItemCardLotSummationManager.UpdateLotSummationByLotNumber(lt.ItemLotNumber, lt.Quantity, lt.ItemCardId, lt.LocationId, 0);

                                        //            else
                                        //            {
                                        //                icItemCardLotSummation = new IcItemCardLotSummation();
                                        //                icItemCardLotSummation.AvailableQnty = lt.AvailableQnty;
                                        //                icItemCardLotSummation.CreatedBy = 1;
                                        //                icItemCardLotSummation.CreatedDate = DateTime.Now;
                                        //                icItemCardLotSummation.ExpiryDate = (!string.IsNullOrEmpty(lt.ExpiryDate.ToString()) ? Convert.ToDateTime(lt.ExpiryDate) : (DateTime?)null); // lt.ExpiryDate;
                                        //                icItemCardLotSummation.ItemCardId = lt.ItemCardId;
                                        //                icItemCardLotSummation.LocationId = lt.LocationId;
                                        //                icItemCardLotSummation.LotNumber = lt.ItemLotNumber;
                                        //                icItemCardLotSummation.OriginalQnty = lt.AvailableQnty;
                                        //                icItemCardLotSummation.MarkNew();
                                        //                iIcItemCardLotSummationManager.Save(icItemCardLotSummation);
                                        //            }




                                        //        }
                                        //    else if (detail.RelatedLots != null && detail.RelatedLots.Count > 0 && (detail.AdjustmentTypeId == (int)AdjustmentType.QuantityDecrease || detail.AdjustmentTypeId == (int)AdjustmentType.BothDecrease))
                                        //    {
                                        //        foreach (IcItemCardLot lt in detail.RelatedLots)
                                        //        {
                                        //            //FindThatLotInSummationTable , Decrease Available Qty
                                        //            iIcItemCardLotSummationManager.UpdateLotSummationByLotNumber(lt.ItemLotNumber, lt.Quantity, lt.ItemCardId, lt.LocationId, 1);
                                        //        }
                                        //    }
                                        //}
                                    }


                                    if (AdjustmentsNumbers != null && AdjustmentsNumbers.Length > 0 && ( Errors == null  || Errors.Length <= 0 ))
                                    {
                                        LogAdjustments(AdjustmentsNumbers.ToString());
                                        result.Add("result", true.ToString());
                                        result.Add("AdjLogPath", ErrorLogFilePath);
                                        return result;
                                    }



                                }
                                catch (Exception ex)
                                {
                                    AddError("Exception Occured while Saving", 0, true);
                                }
                            }

                            if (Errors.Length > 0)
                            {
                                LogErrors(Errors.ToString());
                                result.Add("result", false.ToString());
                                result.Add("LogPath", ErrorLogFilePath);
                                return result;
                            }
                            else
                            {
                                result.Add("result", true.ToString());
                                result.Add("LogPath", ErrorLogFilePath);
                                return result;
                            }
                        }

                        catch (Exception ex)
                        {
                            if (con2.State == ConnectionState.Open)
                                con2.Close();

                            AddError("Exception Occured while Saving the items", 0, true);
                            LogErrors(Errors.ToString());
                            result.Add("result", false.ToString());
                            result.Add("LogPath", ErrorLogFilePath);
                            return result;
                        }
                    }
                //}

          

             
            }
            catch (Exception ex)
            {

               
                AddError("Exception Occured while Saving the items", 0,true);
                LogErrors(Errors.ToString());
                result.Add("result", false.ToString());
                result.Add("LogPath", ErrorLogFilePath);
                return result;

            }
         
            result.Add("result", true.ToString());
            result.Add("LogPath", ErrorLogFilePath);
            return result;

        }



       


        //public List<IcItemCardLot> GetLots(string adjustmentId , List<TempSerialLot> EliminatedList)
        //{
        //    if (EliminatedList != null && EliminatedList.Count > 0)
        //    {

        //        List<IcItemCardLot> lotsList = new List<IcItemCardLot>();
        //        IcItemCardLot icItemCardLot = new IcItemCardLot();

        //        foreach (TempSerialLot tmpLot in EliminatedList)
        //        {
        //            if (tmpLot.AdjustmentIdentifier == adjustmentId)
        //            {
        //                icItemCardLot = new IcItemCardLot();
        //                icItemCardLot.AvailableQnty = tmpLot.quantitySerialLot;
        //                icItemCardLot.ItemCardId = tmpLot.ItemCardId;
        //                icItemCardLot.LocationId = tmpLot.locationId;
        //                icItemCardLot.ExpiryDate = (!string.IsNullOrEmpty(tmpLot.ExpiryDate) ? Convert.ToDateTime(tmpLot.ExpiryDate) : (DateTime?)null);  // tmpLot.ExpiryDate;
        //                icItemCardLot.StockDate = DateTime.Now;
        //                icItemCardLot.FromLocationId = 0;
        //                icItemCardLot.ItemLotNumber = tmpLot.SerialOrLotNumber;
        //                icItemCardLot.Quantity = tmpLot.quantitySerialLot;
        //                icItemCardLot.Status = 1;
        //                icItemCardLot.TransactionTypeId = 15;
        //                icItemCardLot.CreatedBy = 1;
        //                icItemCardLot.CreatedDate = DateTime.Now;
        //                icItemCardLot.EntityId = 1131;
        //                icItemCardLot.EntityValueId = -1;

        //                lotsList.Add(icItemCardLot);
        //            }
        //        }

        //        return lotsList;
        //    }
        //    else
        //        return new List<IcItemCardLot>();
        //}

       

        //public void FillTheEliminatedLots(int itemCardId, int locationId, int adjustmentTypeId, string AdjustmentIdentifier,int adjustmentQty , List<TempSerialLot> tempLotsListEliminated, List<TempSerialLot> tempLotsListOriginal)
        //{
        //    List<TempSerialLot> list = (from item in tempLotsListOriginal where item.AdjustmentIdentifier == AdjustmentIdentifier select item).ToList();


        //    foreach (TempSerialLot tmpLt in list)
        //    {
        //        tmpLt.ItemCardId = itemCardId;
        //        tmpLt.locationId = locationId;
        //        tmpLt.AdjustmentTypeId = adjustmentTypeId;
        //        tmpLt.AdjustmentQty = adjustmentQty;
        //        tempLotsListEliminated.Add(tmpLt);
        //    }
        //}



        //public Dictionary<string, string> ValidateSerialLotSheetByAdjustmentId(List<TempSerialLot> tempLotsListEliminated, List<TempSerialLot> tempLotsListOriginal)
        //{
        //    List<string> ErrorArr;

           
        //    Dictionary<string, string> result = new Dictionary<string, string>();

        //    try
        //    {
               

        //            foreach (TempSerialLot lot in tempLotsListEliminated)
        //            {
        //                ErrorArr = new List<string>();
        //            //    List<TempSerialLot> currentAdjustmentLotsList = new List<TempSerialLot>();


        //                if (lot.AdjustmentTypeId == (int)AdjustmentType.QuantityDecrease || lot.AdjustmentTypeId == (int)AdjustmentType.BothDecrease)
        //                {
        //                    string errorStr = CheckLotExistanceAndAvailabilityDecrease(lot.SerialOrLotNumber, lot.quantitySerialLot, lot.ItemCardId, lot.locationId , lot);

        //                    if (errorStr != null && errorStr != "" && errorStr.Length > 0)
        //                    {
        //                        ErrorArr.Add(errorStr);
        //                        AddError(errorStr, lot.ExcelSheetLineNumber, false);
        //                    }
        //                    // case when adding multiple rows decreasing the same lot in the same location more than once 
        //                   int sumQty = lot.quantitySerialLot;
        //                   int accumulatedQty = lot.quantitySerialLot;

        //                   foreach (TempSerialLot lt in tempLotsListEliminated)
        //                   {
        //                       if (lot.ExcelSheetLineNumber != lt.ExcelSheetLineNumber && lot.ItemCardId == lt.ItemCardId && lot.locationId == lt.locationId && lot.SerialOrLotNumber == lt.SerialOrLotNumber && lt.AdjustmentTypeId == (int)AdjustmentType.QuantityDecrease &&  lt.AdjustmentTypeId == (int)AdjustmentType.BothDecrease)
        //                       {
        //                           sumQty += lt.quantitySerialLot  ;

        //                           if (lot.AvailableQuantityLotSummation < sumQty)
        //                           {
        //                               errorStr = "Total Quantity Decrease exceeds available quantity for the corresponding lot";
        //                               ErrorArr.Add(errorStr);
        //                               AddError(errorStr, lot.ExcelSheetLineNumber, false);
        //                           }
        //                       }

        //                       if (lot.ExcelSheetLineNumber != lt.ExcelSheetLineNumber && lot.AdjustmentIdentifier == lt.AdjustmentIdentifier && lt.AdjustmentTypeId == (int)AdjustmentType.QuantityDecrease && lt.AdjustmentTypeId == (int)AdjustmentType.BothDecrease)
        //                           accumulatedQty += lt.quantitySerialLot;                              
        //                   }

        //                   if (lot.AdjustmentQty < accumulatedQty)
        //                   {
        //                       errorStr = "Related Adjustment Quantity is less than total lots quantity";
        //                       ErrorArr.Add(errorStr);
        //                       AddError(errorStr, lot.ExcelSheetLineNumber, false);
        //                   }
        //                   if (lot.AdjustmentQty > accumulatedQty)
        //                   {
        //                       errorStr = "Related Adjustment Quantity doesn/'t match total lots quantity decreased";
        //                       ErrorArr.Add(errorStr);
        //                       AddError(errorStr, lot.ExcelSheetLineNumber, false);
        //                   }
                       
        //                }
        //                else if (lot.AdjustmentTypeId == (int)AdjustmentType.QuantityIncrease || lot.AdjustmentTypeId == (int)AdjustmentType.BothIncrease)
        //                {
        //                    int accumulatedQty = lot.quantitySerialLot;
        //                    string errorStr = CheckLotExistanceAndAvailabilityIncrease(lot.SerialOrLotNumber, lot.quantitySerialLot, lot.ItemCardId, lot.locationId, Convert.ToDateTime(lot.ExpiryDate), lot.AdjustmentQty);

        //                    if (errorStr != null && errorStr != "" && errorStr.Length > 0)
        //                    {
        //                        ErrorArr.Add(errorStr);
        //                        AddError(errorStr, lot.ExcelSheetLineNumber, false);
        //                    }

        //                    foreach (TempSerialLot lt in tempLotsListEliminated)
        //                    {
        //                        if (lot.ExcelSheetLineNumber != lt.ExcelSheetLineNumber && lot.ItemCardId == lt.ItemCardId && lot.locationId == lt.locationId && lot.SerialOrLotNumber == lt.SerialOrLotNumber && lt.AdjustmentTypeId == (int)AdjustmentType.QuantityIncrease || lt.AdjustmentTypeId == (int)AdjustmentType.BothIncrease)
        //                        {
        //                            errorStr = "Same Lot is being added more than once into the same location";
        //                            ErrorArr.Add(errorStr);
        //                            AddError(errorStr, lot.ExcelSheetLineNumber, false);
        //                        }
        //                        // If Qty in first sheet doesn't equal sum of Quantities in second sheet with the same adjustmentIdentifier  >> Error 
        //                        // Else >> return ""
        //                        if (lot.ExcelSheetLineNumber != lt.ExcelSheetLineNumber && lot.AdjustmentIdentifier == lt.AdjustmentIdentifier)
        //                            accumulatedQty += lt.quantitySerialLot;
                                   
        //                    }

        //                    if (lot.AdjustmentQty < accumulatedQty)
        //                    {
        //                        errorStr = "Related Adjustment Quantity is less than total lots quantity";
        //                        ErrorArr.Add(errorStr);
        //                        AddError(errorStr, lot.ExcelSheetLineNumber, false);
        //                    }
        //                    if (lot.AdjustmentQty > accumulatedQty)
        //                    {
        //                        errorStr = "Related Adjustment Quantity doesn/'t match total lots quantity decreased";
        //                        ErrorArr.Add(errorStr);
        //                        AddError(errorStr, lot.ExcelSheetLineNumber, false);
        //                    }

        //                }



                      
        //                if (ErrorArr.Count > 0)
        //                {
        //                    LogErrors(Errors.ToString());
        //                    continue;
        //                }

        //            }


                
        //    }

        //    catch(Exception ex)
        //    {

        //    }

        //    return result;
        //}


       

        //public string CheckLotExistanceAndAvailabilityIncrease(string lotNumber, int quantityAffected, int itemId, int locationId, DateTime expiryDate , int adjustmentQty)
        //{
        //    string SQL = "SELECT item_card_lot_summation_id,available_qnty FROM dbo.tbl_ic_item_card_lot_summation WHERE flag_deleted = 0 AND lot_number = @lotNumber AND item_card_id = @itemId AND location_id = @locationId";
        //    Dictionary<string, object> parameters = new Dictionary<string, object>();
        //    parameters.Add("@lotNumber", lotNumber);
        //    parameters.Add("@itemId", itemId);
        //    parameters.Add("@locationId", locationId);
        //    System.Data.DataTable dt = Data.GetTableOf(SQL, parameters);

        //    if (dt != null && dt.Rows.Count != 0 && dt.Rows[0]["item_card_lot_summation_id"].ToString() != null && dt.Rows[0]["item_card_lot_summation_id"].ToString().Length > 0)
        //    {
        //        if (dt.Rows[0]["available_qnty"].ToString() != null && Convert.ToInt32(dt.Rows[0]["available_qnty"]) > 0)
        //            return "Lot Already Exists";
        //        else return "";
        //    }
        //    else
        //        return "";

        //}

        //public string CheckLotExistanceAndAvailabilityDecrease(string lotNumber, int quantityAffected, int itemId, int locationId , TempSerialLot lot)
        //{
        //    string SQL = "SELECT item_card_lot_summation_id,available_qnty FROM dbo.tbl_ic_item_card_lot_summation WHERE flag_deleted = 0 AND lot_number = @lotNumber AND item_card_id = @itemId AND location_id = @locationId";
        //    Dictionary<string, object> parameters = new Dictionary<string, object>();
        //    parameters.Add("@lotNumber", lotNumber);
        //    parameters.Add("@itemId", itemId);
        //    parameters.Add("@locationId", locationId);
        //    System.Data.DataTable dt = Data.GetTableOf(SQL, parameters);

        //    if (dt != null && dt.Rows.Count != 0)
        //    {
        //        if (dt.Rows[0]["item_card_lot_summation_id"].ToString() != null && dt.Rows[0]["item_card_lot_summation_id"].ToString().Length > 0)
        //        {
        //            lot.AvailableQuantityLotSummation = Convert.ToInt32(dt.Rows[0]["available_qnty"].ToString());

        //            if (dt.Rows[0]["available_qnty"].ToString() == null || (dt.Rows[0]["available_qnty"].ToString() != null && Convert.ToInt32(dt.Rows[0]["available_qnty"]) <= 0))
        //                return "Lot is not available";
        //            else if (dt.Rows[0]["available_qnty"].ToString() != null && (Convert.ToInt32(dt.Rows[0]["available_qnty"]) > 0 && quantityAffected > Convert.ToInt32(dt.Rows[0]["available_qnty"])))
        //                return "Lot Has no enough quantity to be decreased";
        //            else
        //                return "";
        //        }
        //        else
        //            return "Lot was not found for it/'s corresponding item";
        //    }
        //    else return "Item has no related lots";

        //}


        public int HandleIcItemCard(string itemNumber)
        {
            string SQL = "SELECT item_card_id from dbo.tbl_ic_item_card WHERE flag_deleted=0 AND item_number=@itemNumber";

            Dictionary<string, object> paramsters = new Dictionary<string, object>();
            paramsters.Add("@itemNumber", itemNumber);
            System.Data.DataTable dt = Data.GetTableOf(SQL, paramsters);

            if (dt != null && dt.Rows.Count != 0)
                return Convert.ToInt32(dt.Rows[0]["item_card_id"].ToString());
            else
                return -1;
        }

        public int HandleIcItemCardType(int itemCardId)
        {
            string SQL = "SELECT ( CASE serial_number WHEN 1 THEN 1 ELSE CASE lot_number WHEN 1 THEN 2 ELSE 0 END END) AS item_type FROM dbo.tbl_ic_item_card WHERE flag_deleted = 0 AND item_card_id = @itemCardId";

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("@itemCardId", itemCardId);
            System.Data.DataTable dt = Data.GetTableOf(SQL, parameters);

            if (dt != null && dt.Rows.Count != 0)
                return Convert.ToInt32(dt.Rows[0]["item_type"].ToString());
            else
                return -1;
        }

        public int HandleIcLocation(string code)
        {
            string SQL = "SELECT location_id from dbo.tbl_ic_location WHERE flag_deleted=0 AND location_code=@code";

            Dictionary<string, object> paramsters = new Dictionary<string, object>();
            paramsters.Add("@code", code);
            System.Data.DataTable dt = Data.GetTableOf(SQL, paramsters);

            if (dt != null && dt.Rows.Count != 0)
                return Convert.ToInt32(dt.Rows[0]["location_id"].ToString());
            else
                return -1;
        }


        public int HandleIcQuantity(int locationId,int itemCard)
        {
            string SQL = "SELECT quantity_on_hand FROM dbo.tbl_ic_item_location_costing WHERE item_card_id=@itemCard AND location_id=@locationId";

            Dictionary<string, object> paramsters = new Dictionary<string, object>();
            paramsters.Add("@locationId", locationId);
            paramsters.Add("@itemCard", itemCard);
            System.Data.DataTable dt = Data.GetTableOf(SQL, paramsters);

            if (dt != null && dt.Rows.Count != 0)
                return Convert.ToInt32(dt.Rows[0]["quantity_on_hand"].ToString());
            else
                return -1;
        }



        public int HandleIcAdjType(string code)
        {
            string SQL = "SELECT data_type_content_id from dbo.tbl_data_type_content WHERE flag_deleted=0 AND LOWER(data_type_content)=LOWER(@code) and data_type_id=@data_type_id";

            Dictionary<string, object> paramsters = new Dictionary<string, object>();
            paramsters.Add("@code", code);
            paramsters.Add("@data_type_id", (int)DataTypes.AdjustmentTypeId);
            System.Data.DataTable dt = Data.GetTableOf(SQL, paramsters);

            if (dt != null && dt.Rows.Count != 0)
                return Convert.ToInt32(dt.Rows[0]["data_type_content_id"].ToString());
            else
                return -1;
        }

        public int HandleIcBucketType(string code)
        {
            string SQL = "SELECT data_type_content_id from dbo.tbl_data_type_content WHERE flag_deleted=0 AND LOWER(data_type_content)=LOWER(@code) and data_type_id=@data_type_id";

            Dictionary<string, object> paramsters = new Dictionary<string, object>();
            paramsters.Add("@code", code);
            paramsters.Add("@data_type_id", (int)DataTypes.BucketTypeId);
            System.Data.DataTable dt = Data.GetTableOf(SQL, paramsters);

            if (dt != null && dt.Rows.Count != 0)
                return Convert.ToInt32(dt.Rows[0]["data_type_content_id"].ToString());
            else
                return -1;
        }

        public int HandleIcDocument(string docNumber)
        {
            string SQL = "SELECT receipt_id from dbo.tbl_ic_receipt WHERE flag_deleted=0 AND  receipt_number=@docNumber";

            Dictionary<string, object> paramsters = new Dictionary<string, object>();
            paramsters.Add("@docNumber", docNumber);
            System.Data.DataTable dt = Data.GetTableOf(SQL, paramsters);

            if (dt != null && dt.Rows.Count != 0)
                return Convert.ToInt32(dt.Rows[0]["receipt_id"].ToString());
            else
                return -1;
        }

        public int HandleGlAccount(string number)
        {
            string SQL = "SELECT account_id FROM dbo.tbl_account WHERE flag_deleted=0 and account_number=@number";

            Dictionary<string, object> paramsters = new Dictionary<string, object>();
            paramsters.Add("@number", number);
            System.Data.DataTable dt = Data.GetTableOf(SQL, paramsters);

            if (dt != null && dt.Rows.Count != 0)
                return Convert.ToInt32(dt.Rows[0]["account_id"].ToString());
            else
                return -1;
        }

        public int HandleIcUOM(string uom, int itemCardId)
        {
            string SQL = "SELECT TOP 1 tbl_ic_item_unit_of_measures.item_unit_of_measure_id FROM    dbo.tbl_ic_item_unit_of_measures INNER JOIN dbo.tbl_ic_unit_of_measures ON dbo.tbl_ic_unit_of_measures.unit_of_measure_id = dbo.tbl_ic_item_unit_of_measures.unit_of_measure_id AND dbo.tbl_ic_unit_of_measures.flag_deleted = 0 AND unit_of_measure =@uom WHERE   dbo.tbl_ic_item_unit_of_measures.flag_deleted = 0 AND item_card_id =@itemCardId";
            Dictionary<string, object> paramsters = new Dictionary<string, object>();
            paramsters.Add("@uom", uom);
            paramsters.Add("@itemCardId", itemCardId);
            System.Data.DataTable dt = Data.GetTableOf(SQL, paramsters);

            if (dt != null && dt.Rows.Count != 0)
                return Convert.ToInt32(dt.Rows[0]["item_unit_of_measure_id"].ToString());
            else
                return -1;
        }

        public void SaveAdjustment(IcAdjustment adjustment)
        {
            try
            {
                adjustment.AdjustmentId = iIcItemCardManager.AddIcAdjustment(adjustment.AdjustmentNumber, adjustment.Description, adjustment.Reference, adjustment.AdjustmentDate, adjustment.PostingDate, adjustment.YearPeriod, (int)adjustment.Year, (int)adjustment.Period);
                int tmp = -1;
                if (adjustment.AdjustmentId != null && adjustment.AdjustmentId > 0 && adjustment.AdjustmentDetails != null && adjustment.AdjustmentDetails.Count > 0)
                {
                    foreach (IcAdjustmentDetail adjDtl in adjustment.AdjustmentDetails)
                    {
                        tmp = iIcItemCardManager.AddIcAdjustmentDetails(adjustment.AdjustmentId, adjDtl.ItemCardId, adjDtl.LocationId, adjDtl.AdjustmentTypeId, (float)adjDtl.Quantity, (float?)adjDtl.CostAdjustment, adjDtl.ItemUnitOfMeasureId, (int?)adjDtl.CostUomId, adjDtl.AdjustmentAccountId);
                    }

                    AddAdjustmentNumber(adjustment.AdjustmentNumber);
                }
            }
            catch (Exception ex)
            {
                AddError("Exception Occured while Saving", 0, true);
            }
            //try
            //{
            //  //  icAdjustmentManager.UseTransaction = false;
            //  //  icAdjustmentManager.BeginTransaction();
            //    icAdjustmentManager.Save(adjustment);

            // //   icAdjustmentManager.CommitTransaction();
            //}
            //catch (Exception ex)
            //{
                
            //    AddError(string.Format("Exception Occured while Saving Exception is : {0}", ex.Message), 0, true);
            //}
        }

        private void AddError(string error, int lineNumber , bool fromAdjustmentSheet)
        {
            if (fromAdjustmentSheet)
                error = string.Format("Adjustment Sheet: {0}", error);
            else
                error = string.Format("Serial-Lot Sheet : {0}", error);

            Errors.AppendLine(error);

        }

        public void LogErrors(string Errors)
        {
            string dir = ErrorLogFolderPath;
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);
            string fileName = "Errors-" + DateTime.Now.ToString("dd-MM-yy_HHmm") + ".txt";
            ErrorLogFilePath = fileName;
            FileStream file = System.IO.File.Create(dir + fileName);
            StreamWriter sw = new StreamWriter(file);
            sw.WriteLine(Errors);
            sw.AutoFlush = true;
            file.Close();


        }


        public void LogAdjustments(string AdjustmentsNumbers)
        {
            string dir = ErrorLogFolderPath;
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);
            string fileName = "Adjustments Numbers Log-" + DateTime.Now.ToString("dd-MM-yy_HHmm") + ".txt";
            ErrorLogFilePath = fileName;
            FileStream file = System.IO.File.Create(dir + fileName);
            StreamWriter sw = new StreamWriter(file);
            sw.WriteLine(AdjustmentsNumbers);
            sw.AutoFlush = true;
            file.Close();
        }



        private void AddAdjustmentNumber(string adjustmentNum)
        {
            adjustmentNum = string.Format("Adjustment Sheet , Adjustment Number : {0} has been added successfully", adjustmentNum);


            AdjustmentsNumbers.AppendLine(adjustmentNum);

        }

        private string GenerateAdjustmentNumber(int adjustmentCount)
        {
            IcDocumentNumbersCriteria criteria = new IcDocumentNumbersCriteria();

            criteria.EntityId = (int)Enums_S3.InventoryControl.DocumentNumbers.AdjustmentNumber;
            criteria.DocumentNumbersId = (int)Enums_S3.InventoryControl.DocumentNumbers.AdjustmentNumber;

            IcDocumentNumbersLite DocNumber = iIcDocumentNumbersManager.FindAllLite(criteria).FirstOrDefault();

            var generatedNum = DocNumber.Prefix;
            int nextAdjusNum = DocNumber.NextNumber + adjustmentCount;
            generatedNum += nextAdjusNum.ToString().PadLeft(DocNumber.Length - DocNumber.Prefix.Length, '0');

            return generatedNum;
        }

        private void UpdateAdjustmentYearPeriodValue(ref IcAdjustment adjustment)
        {
            adjustment.PostingDate = DateTime.Now;

            string yearPeriodValue = iIcItemCardManager.GetYearPeriodValueFromUpload(Convert.ToDateTime(adjustment.PostingDate));
            //IcCommonQueriesCriteria crit = new IcCommonQueriesCriteria();
            //crit.Date = adjustment.PostingDate;
            //string yearPeriodValue = iIcCommonQueries.GetYearPeriodValue(crit);
           
            adjustment.YearPeriod = yearPeriodValue;
            adjustment.Year = int.Parse(yearPeriodValue.Split('-')[0]);
            adjustment.Period = int.Parse(yearPeriodValue.Split('-')[1]);
        }

        /// <summary>
        /// This Function inceremnts "Next Number" Value for Adjustment Document
        /// </summary>
        /// <param name="documentNumber">Stands for Adjustment Id</param>
        private void UpdateDocumentNumbersNextNumber(int documentNumber)
        {

            iIcItemCardManager.UpdateIcTransactionNextNumber(null, null, null, documentNumber);
            //IcCommonQueriesCriteria crit = new IcCommonQueriesCriteria();
            //crit.AdjustmentId = documentNumber;

            //iIcCommonQueries.DocumentNumbers_UpdateNextNumber(crit);
        }
        private void PostAdjustment(IcAdjustment Adjustment)
        {
            IcAdjustmentCriteria crit = new IcAdjustmentCriteria();
            crit.AdjustmentId = Adjustment.AdjustmentId;
            crit.EntityId = Adjustment.AdjustmentId;
            crit.CreatedBy = 1;
            try
            {
                iIcItemCardManager.PostIcAdjustment(Adjustment.AdjustmentId, 1, false);
            }
            catch (Exception ex)
            {
                throw;
            }
        }



        //private void HandleItemCardLotTransactions(IcAdjustment adjustment)
        //{
        //    if (adjustment.AdjustmentId > 0)
        //    {

        //        int currentEntityId = (int)Enums_S3.Entity.IcAdjustmentDetail;
        //        int currentTransactionTypeId = 0;
        //        currentTransactionTypeId = (int)Enums_S3.InventoryControl.TransactionType.Adjustment;

        //        IList<IcItemCardLot> ItemCardLot = new List<IcItemCardLot>();
        //        List<IcItemCardLot> modifiedLots = new List<IcItemCardLot>();
        //        var currentDate = DateTime.Now;
        //        foreach (var adjustmentDetail in adjustment.AdjustmentDetails)
        //        {
        //            //GET  adjustment line lots
        //            IcItemCardLotCriteria itemCardLotCriteria = new IcItemCardLotCriteria();
        //            itemCardLotCriteria.EntityId = currentEntityId;
        //            itemCardLotCriteria.EntityValueId = adjustmentDetail.AdjustmentDetailId;

        //            List<IcItemCardLot> originalRelatedLot = iIcItemCardLotManager.FindAll(itemCardLotCriteria);


        //            if (adjustmentDetail.RelatedLots == null) adjustmentDetail.RelatedLots = new List<IcItemCardLot>();

        //            List<int> oldIds = (originalRelatedLot != null && originalRelatedLot.Count() > 0) ? (from item in originalRelatedLot select item.ItemCardLotId).ToList() : new List<int>();
        //            List<int> newIds = (adjustmentDetail.RelatedLots != null && adjustmentDetail.RelatedLots.Count() > 0) ? (from item in adjustmentDetail.RelatedLots where item.ItemCardLotId > 0 select item.ItemCardLotId).ToList() : new List<int>();


        //            IEnumerable<int> ToUpdate = oldIds.Intersect(newIds);
        //            IEnumerable<int> ToDelete = oldIds.Except(newIds);


        //            foreach (var adjustmentDetailLot in adjustmentDetail.RelatedLots)
        //            {
        //                if (adjustmentDetailLot.ItemCardLotId <= 0)
        //                {
        //                    IcItemCardLot itemCardLot = new IcItemCardLot();
        //                    itemCardLot.CreatedBy = 1;
        //                    itemCardLot.CreatedDate = currentDate;
        //                    itemCardLot.ItemCardLotId = adjustmentDetailLot.ItemCardLotId;
        //                    itemCardLot.ExpiryDate = adjustmentDetailLot.ExpiryDate;
        //                    itemCardLot.ItemLotNumber = adjustmentDetailLot.ItemLotNumber;
        //                    itemCardLot.ItemCardId = adjustmentDetailLot.ItemCardId;
        //                    itemCardLot.LocationId = adjustmentDetailLot.LocationId;
        //                    itemCardLot.StockDate = adjustmentDetailLot.StockDate;
        //                    itemCardLot.ExpiryDate = adjustmentDetailLot.ExpiryDate;
        //                    itemCardLot.Quantity = adjustmentDetailLot.Quantity;
        //                    itemCardLot.EntityId = currentEntityId;
        //                    itemCardLot.TransactionTypeId = currentTransactionTypeId;
        //                    itemCardLot.EntityValueId = adjustmentDetail.AdjustmentDetailId;
        //                    itemCardLot.LocationId = adjustmentDetailLot.LocationId;
        //                    itemCardLot.Status = (int)Enums_S3.InventoryControl.LotItemStatus.Available;
        //                    itemCardLot.OldItemCardLotId = adjustmentDetailLot.OldItemCardLotId;
        //                    itemCardLot.MarkNew();
        //                    ItemCardLot.Add(itemCardLot);

        //                    //if (itemCardLot.OldItemCardLotId > 0)
        //                    //{
        //                    //    IcItemCardLot oldItemCardLot = (from item in originalRelatedLot where item.ItemCardLotId == itemCardLot.OldItemCardLotId select item).FirstOrDefault();
        //                    //    oldItemCardLot.MarkOld();
        //                    //    oldItemCardLot.MarkModified();
        //                    //    oldItemCardLot.AvailableQnty = oldItemCardLot.AvailableQnty - itemCardLot.Quantity;
        //                    //    modifiedLots.Add(oldItemCardLot);
        //                    //}
        //                }
        //            }

        //            if (ToDelete != null)
        //            {
        //                foreach (int id in ToDelete)
        //                {
        //                    if (id > 0)
        //                    {
        //                        IcItemCardLot toDelete = (from item in originalRelatedLot where item.ItemCardLotId == id select item).ToList().FirstOrDefault();
        //                        toDelete.ItemCardLotId = id;
        //                        toDelete.MarkDeleted();
        //                        ItemCardLot.Add(toDelete);
        //                    }
        //                }

        //            }
        //        }


        //        foreach (var item in ItemCardLot)
        //            iIcItemCardLotManager.Save(item);
        //    }
        //}

    }

    public class TempAdjustment
    {
        public string AdjustmentDate;
        public string Reference;
        public string Description;
        public string ItemNumber;
        public string Location;
        public string AdjustmentType;
        public string BucketType;
        public string DocumentNo;
        public string DocumentLine;
        public string CostDate;
        public string QTY;
        public string CostAdjutment;
        public string UOM;
        public string CostUOM;
        public string AdjustmentAccount;
        public int ExcelSheetLineNumber;
      //  public string serialLotAdjustmentIdentifier;
        public int AdjustmentDetailId;
    }

    //public class TempSerialLot
    //{
    //    public string AdjustmentIdentifier;
    //    public int ItemCardId;
    //    public int locationId;
    //    public int AdjustmentTypeId;
    //    public int ItemType;
    //    public string SerialOrLotNumber;
    //    public int quantitySerialLot;
    //    public string ExpiryDate;
    //    public int AvailableQuantityLotSummation;
    //    public int ExcelSheetLineNumber;
    //    public int AdjustmentQty;
    //}

    enum AdjustmentType
    {
        QuantityIncrease = 200,
        QuantityDecrease = 201,
        CostIncrease = 202,
        CostDecrease = 203,
        BothIncrease = 204,
        BothDecrease = 205
    }
}