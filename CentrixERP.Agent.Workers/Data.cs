﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace CentrixERP.Agent.Workers
{
    public static class Data
    {
        private static string ConnStr = ConfigurationManager.ConnectionStrings["DBConn"].ToString();
        private static SqlConnection sqlConn;

        static Data()
        {
        }

        public static DataTable GetTableOf(string sql, bool replaceQuotes)
        {
            DataTable dt = new DataTable();
            try
            {
                if (replaceQuotes)
                    sql = sql.Replace("'", "''");
                InitialConn();
                sqlConn.Open();
                SqlCommand cmd = new SqlCommand(sql, sqlConn);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                sqlConn.Close();
            }
            catch (Exception ex) { }
            return dt;

        }

        public static DataTable GetTableOf(string sql, Dictionary<string, object> Parameters)
        {
            DataTable dt = new DataTable();
            try
            {
                InitialConn();
                sqlConn.Open();
                SqlCommand cmd = new SqlCommand(sql, sqlConn);

                if (Parameters != null)
                {
                    foreach (var item in Parameters)
                    {
                        cmd.Parameters.AddWithValue(item.Key, item.Value);
                    }
                }


                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                sqlConn.Close();
            }
            catch (Exception ex) { }
            return dt;

        }


        private static void InitialConn()
        {
            try
            {
                sqlConn = new SqlConnection(ConnStr);
            }
            catch (Exception ex)
            {
                // System.Windows.Forms.MessageBox.Show("your connection information are invalid");
            }
        }
        enum DataTypes
        {
            CostingMethodId = 160,
            BucketTypeId = 64,
            AdjustmentTypeId = 63
        }

    }
}