using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SF.Framework;
using SagePOS.Common.Web;
using SagePOS.Server.Configuration;

namespace Centrix.UM.Web.NewUserManagement.Users
{
    public partial class ViewUser : BasePageLoggedIn
    {

        public ViewUser()
            : base(Enums_S3.Permissions.SystemModules.None) { }

        public int UserId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            UserId = Request["uid"].ToNumber();
        }
    }
}