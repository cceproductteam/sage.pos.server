using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SagePOS.Common.Web;
using SagePOS.Server.Configuration;

namespace Centrix.UM.Web.NewUserManagement.Users
{
    public partial class UsersList : BasePageLoggedIn
    {
        public UsersList()
            : base(Enums_S3.Permissions.SystemModules.ViewUser) { }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}