using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SagePOS.Common.Web;
using SagePOS.Server.Configuration;

namespace Centrix.UM.Web.NewUserManagement.Roles
{
    public partial class RolesList :  BasePageLoggedIn
    {
        public RolesList()
            : base(Enums_S3.Permissions.SystemModules.ViewRole) { }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}