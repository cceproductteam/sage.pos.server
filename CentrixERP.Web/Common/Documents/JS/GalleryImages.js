function EditDocument(fileEl, id) {
    $('.white_content').hide();
    showLoadingMsg();
    $.ajax({
        type: "post",
        contentType: "application/json; charset=utf-8",
        url: ajaxEditDocPageURL,
        data: '{ id: "' + id + '",moduleKey: "' + moduleKey + '" }',
        dataType: "json",
        success: function (e) {
            if (e.d != null) {
                var obj = eval('(' + e.d + ')').result;
                
                var titleEn = obj.Title != null ? obj.Title : '';
                var titleAr = obj.Description != null ? obj.Description : '';
                showConfirmMsg(getResource("Edit"), '<table class="Data-Table" cellspacing="0" cellpadding="0" border="0"><tr><th><span class="label-title">' + getResource("CaptionEn") + '</span></th><td><input class="cx-control DocTitle  txt string type="text" value="' + titleEn + '"  /></td></tr><tr><th><span class="label-title">' + getResource("CaptionAr") + '</span></th><td><input class="cx-control DocDescription  txt string type="text" value="' + titleAr + '"  /></td></tr><tr><th></th><td><span class="label-title">' + getResource("DocContinueSave") + '</span></td></tr></table>', function () {
                    ContinueSaveDoc(id, $('.DocTitle').val(), $('.DocDescription').val(), DocName);
                });
            }
            hideLoadingMsg();
        },
        error: function (e) {
            hideLoadingMsg();

        }
    });
}