using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SF.Framework;
using System.Configuration;
using CommonMgr = CentrixERP.Common.Business.IManager;
using SagePOS.Common.Web;
using SagePOS.Manger.Common.Business.Entity;
using SagePOS.Server.Business.Factory;
using SF.FrameworkEntity;
using CentrixERP.Common.Business.IManager;
using SagePOS.Server.Configuration;
using SagePOS.Common.Web.NewCommon.Common;

namespace SagePOS.Common.Web.Documents
{
    public partial class DocumentHtml5Ajax : BasePageLoggedIn
    {
        public DocumentHtml5Ajax()
            : base(Enums_S3.Permissions.SystemModules.None, false)
        {

        }

        public string moduleKey;
        string moduleName;
        public int parentId;
        public int DocumentID;
        public string subFolder;
        protected string DeleteFunctionName = "";
        public int folderId;


        protected void Page_Load(object sender, EventArgs e)
        {

            subFolder = Request["subFolder"];
            moduleKey = Request["key"];
            parentId = Request["id"].ToNumber();
            folderId = Request["folderId"].ToNumber();
            moduleName = Request["modulename"];

          

            DocumentEntity documentEntity = new DocumentEntity();
            CommonMgr.IDocumentEntityManager IDocumentEntityManager = (IDocumentEntityManager)IoC.Instance.Resolve(typeof(IDocumentEntityManager));
            DocumentEntityCriteria criteria = new DocumentEntityCriteria
            {
                ParentFolderId = folderId,
                EntityId = moduleKey.ToNumber(),
                EntityValueId = parentId,
            };


            List<DocumentEntity> list = IDocumentEntityManager.FindByEntityId(criteria);
            if (list == null)
                list = new List<DocumentEntity>();

           

            string url2 = SagePOS.Server.Configuration.Configuration.GetConfigKeyValue("NewDocumentsURL");
             var documents = from DocumentEntity doc in list
                             select new
                             {
                                 DocName = doc.DocumentObj.OrginalFileName + doc.DocumentObj.FileExtension,
                                 DocTitle = doc.DocumentObj.OrginalFileName + doc.DocumentObj.FileExtension + "\n" + doc.DocumentObj.Title + "\n" + doc.DocumentObj.Description,                                 // DocURL = SagePOS.Server.Configuration.Configuration.GetConfigKeyURL("DocumentsURL") + moduleName + "/" + doc.EntityValueId.ToString()
                                 //+ "/" + doc.DocumentObj.FileName + doc.DocumentObj.FileExtension,
                                 DocURL = SagePOS.Server.Configuration.Configuration.GetConfigKeyURL("DownloadPagePath") + "?Id=" + doc.DocumentEntityId + "&moduleName=" + moduleName + "&parentId=" + parentId,
                                 //+ "?path=" + url2 + moduleName + "/" + parentId + "/" + doc.DocumentObj.FileName + doc.DocumentObj.FileExtension + "&&downloadName=" +HttpUtility.UrlEncode( doc.DocumentObj.OrginalFileName )+ doc.DocumentObj.FileExtension,
                                 Id = doc.DocumentEntityId,
                                 DId = doc.DocumentId,
                                 DocExt = doc.DocumentObj.FileExtension.TrimStart('.')
                             };
                        
            rptDocuments.DataSource = documents.ToArray();
            rptDocuments.DataBind();

        }


        protected void rpt_DataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //IDocument temp = (IDocument)e.Item.DataItem;
                //IDocumentManager iManager = IoC.Instance.Resolve<IDocumentManager>();

                //if (temp.DocumentObj.ParentFolder != temp.DocumentObj.OldParentFolder)
                //    subFolder = temp.DocumentObj.OldParentFolder;
                // else 
                // subFolder = temp.DocumentObj.ParentFolder;

                //Document.Text = (temp.DocumentObj.OrginalFileName + temp.DocumentObj.FileExtension).Truncate(60);
                //var documentURL = temp.DocumentUrl+parentId +"/";//+subFolder
                //lblFileValuelnk.NavigateUrl = documentURL + temp.DocumentObj.FileName + temp.DocumentObj.FileExtension;
                //ltrDocumentCssClass.Text = iManager.GetIcon(temp.DocumentObj.FileExtension).CssClass;


            }
        }
    }
}
