using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using SF.CustomScriptControls;
using CentrixERP.Common.Business.IManager;
using SF.Framework;
using E = SagePOS.Manger.Common.Business.Entity;
using System.Web.Script.Serialization;
using SagePOS.Common.Web.Documents;
using SagePOS.Manger.Common.Business.Entity;
using System.Configuration;
using System.IO;
using SagePOS.Server.Business.Factory;

namespace SagePOS.Common.Web.DocumentFolder
{
    /// <summary>
    /// Summary description for DocumentFolderWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class DocumentFolderWebService : System.Web.Services.WebService
    {

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Search(string keyword, string args)
        {
            string[] argsArr = args.TrimEnd(',').Split(',');
            List<AutoCompleteItem> returnedList = new List<AutoCompleteItem>();
            IEnumerable<AutoCompleteItem> temp;

            IDocumentFolderManager myManager = (IDocumentFolderManager)IoC.Instance.Resolve(typeof(IDocumentFolderManager));
            E.DocumentFolderCriteria criteria = new E.DocumentFolderCriteria();



            if (argsArr.Length > 0)
            {
                if (argsArr.Length == 1)
                    criteria.EntityID = argsArr[0].ToNumber();
                if (argsArr.Length > 1)
                    criteria.parentFolder = argsArr[1].ToNumber();
            }

            List<E.DocumentFolder> List = myManager.FindAll(criteria);
            JavaScriptSerializer oSerializer = new JavaScriptSerializer();

            if (List != null)
            {
                temp = from E.DocumentFolder item in List orderby item.FolderName select new AutoCompleteItem() { label = item.FolderName, value = item.FolderId, categoryId = 0 };
                returnedList.AddRange(temp.ToList());
            }

            if (returnedList != null && returnedList.Count != 0)
            {
                string sJSON = oSerializer.Serialize(returnedList);
                return sJSON;
            }
            else
            {
                return null;
            }

        }

        [WebMethod]
        public void Delete(int id)
        {
            IDocumentFolderManager manager = (IDocumentFolderManager)IoC.Instance.Resolve(typeof(IDocumentFolderManager));
            E.DocumentFolder docFolder = manager.FindById(id, null);
            docFolder.FolderId = id;
            try
            {
                docFolder.MarkDeleted();
                manager.Save(docFolder);
            }
            catch (Exception ex)
            { }

        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string getSubFolders(int folderId, int entityId, int parentEntityId)//int entityId, int parentFolderId
        {
            IDocumentFolderManager docFolderManager = (IDocumentFolderManager)IoC.Instance.Resolve(typeof(IDocumentFolderManager));
            List<SagePOS.Manger.Common.Business.Entity.DocumentFolder> documentFolderList = new List<SagePOS.Manger.Common.Business.Entity.DocumentFolder>();

            DocumentFolderCriteria criteria = new DocumentFolderCriteria();
            criteria.EntityID = entityId;
            criteria.parentFolder = folderId; // (folderId !=0)?folderId:1;
            criteria.ParentEntityId = parentEntityId;
            documentFolderList = docFolderManager.FindAll(criteria);

            if (documentFolderList != null)
            {


                var allFolders = from E.DocumentFolder folder in documentFolderList
                                 select new Folders()
                                 {
                                     name = folder.FolderName,
                                     folderId = folder.FolderId
                                 };

                JavaScriptSerializer OSerializer = new JavaScriptSerializer();
                return OSerializer.Serialize(allFolders);



                //foreach (SagePOS.Manger.Common.Business.Entity.DocumentFolder item in documentFolderList)
                //{
                //    Folders myfs = new Folders()
                //    {
                //        name = item.FolderName,
                //        path = path,
                //        folderId = item.FolderId
                //    };
                //    listFolders.Add(myfs);
                //}
            }
            return null;
            // JavaScriptSerializer OSerializer = new JavaScriptSerializer();
            //string Json=null;
            //if (listFolders != null)
            //    Json = OSerializer.Serialize(listFolders);
            //return Json;

            //List<Folders> listFolders = new List<Folders>();
            //listFolders.Add(new Folders() { name = "1111", path = "" });
            //listFolders.Add(new Folders() { name = "2222", path = "" });
            //listFolders.Add(new Folders() { name = "3333", path = "" });
            //JavaScriptSerializer OSerializer = new JavaScriptSerializer();
            //string FolderArr = OSerializer.Serialize(listFolders);
            //return FolderArr;
        }

        [WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string AddSubFolder(string name, int entityId, int parentEntityId, int createdBy, string moduleName, int parentFolderId)
        {
            IDocumentFolderManager manager = (IDocumentFolderManager)IoC.Instance.Resolve(typeof(IDocumentFolderManager));
            E.DocumentFolder documentFolder = new E.DocumentFolder();
            documentFolder.FolderName = Server.UrlDecode(name);
            documentFolder.EntityId = entityId;
            documentFolder.CreatedBy = createdBy;
            documentFolder.ParentFolder = parentFolderId;
            documentFolder.ParentEntityId = parentEntityId;
            try
            {
                manager.Save(documentFolder);
            }
            catch (Exception ex)
            {
            }

            return refreshFolderList(moduleName, parentFolderId, parentEntityId, entityId, createdBy);
        }

        string path = ConfigurationManager.AppSettings["DocumentsFolderPath"].ToString();

        [WebMethod]
        public string refreshFolderList(string moduleName, int parentId, int parentEntityId, int entityId, int createdBy)
        {
            IDocumentFolderManager docFolderManager = (IDocumentFolderManager)IoC.Instance.Resolve(typeof(IDocumentFolderManager));
            //var root = path + moduleName + @"\" + parentId;
            //string FolderArr = "";
            List<SagePOS.Manger.Common.Business.Entity.DocumentFolder> documentFolderList = new List<SagePOS.Manger.Common.Business.Entity.DocumentFolder>();
            DocumentFolderCriteria criteria = new DocumentFolderCriteria();
            criteria.EntityID = entityId;
            criteria.parentFolder = parentId;
            criteria.CreatedBy = createdBy;
            criteria.ParentEntityId = parentEntityId;
            documentFolderList = docFolderManager.FindAll(criteria);


            if (documentFolderList != null)
            {


                var allFolders = from E.DocumentFolder folder in documentFolderList
                                 select new Folders()
                                 {
                                     name = folder.FolderName,
                                     folderId = folder.FolderId
                                 };

                JavaScriptSerializer OSerializer = new JavaScriptSerializer();
                return OSerializer.Serialize(allFolders);



                //listFolders.Add(new Folders() { name = "Root", path = root, folders = getDirectories(documentFolderList, path, moduleName, parentId), folderId = 1 });
            }
            //else
            //{
            //    Directory.CreateDirectory(root);
            //    listFolders.Add(new Folders() { name = "Root", path = parentId.ToString(), folders = getDirectories(documentFolderList, path, moduleName, parentId), folderId = 1 });
            //}

            //JavaScriptSerializer OSerializer = new JavaScriptSerializer();
            //FolderArr = OSerializer.Serialize(listFolders);
            return null;
        }


        private List<Folders> getDirectories(List<SagePOS.Manger.Common.Business.Entity.DocumentFolder> documentFolderList, string parentFolder, string moduleName, int parentId)
        {
            List<Folders> listFolders = new List<Folders>();
            List<Folders> temp = new List<Folders>();
            temp.Add(new Folders() { name = "ff", path = "C:\\" });

            if (documentFolderList != null)
            {
                foreach (SagePOS.Manger.Common.Business.Entity.DocumentFolder item in documentFolderList)
                {
                    Folders myfs = new Folders()
                        {
                            name = item.FolderName,
                            path = path + moduleName + @"\" + parentId,
                            folderId = item.FolderId,
                            folders = temp
                        };
                    listFolders.Add(myfs);
                }
            }
            return listFolders;
        }


        [WebMethod]
        public string DeleteFolder(int parentId, int folderId,string  moduleName,int entityId,int createdBy)
        {
            IDocumentFolderManager manager = (IDocumentFolderManager)IoC.Instance.Resolve(typeof(IDocumentFolderManager));
            E.DocumentFolder docFolder = manager.FindById(folderId, null);
            docFolder.FolderId = folderId;
            try
            {
                docFolder.MarkDeleted();
                manager.Save(docFolder);
            }
            catch (Exception ex)
            { }
            return null;
            //return refreshFolderList(moduleName, parentId, entityId, createdBy);         
        }

        [WebMethod]
        public string RenameFolder(int FolderId, string FolderName)
        {
            IDocumentFolderManager iDocumentManager = (IDocumentFolderManager)IoC.Instance.Resolve(typeof(IDocumentFolderManager));
            E.DocumentFolder folder = iDocumentManager.FindById(FolderId, null);
            folder.FolderName = FolderName;
            folder.MarkModified();
            try
            {
                iDocumentManager.Save(folder);
                return "1";
            }
            catch (Exception)
            {
                return "0";
            }
        }

    }
}
