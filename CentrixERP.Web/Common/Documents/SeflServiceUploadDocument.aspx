<%--<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SeflServiceSeflServiceUploadDocument.aspx.cs"
    Inherits="SagePOS.Common.Web.Documents.SeflServiceUploadDocument" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="<%=ResolveUrl("~/Common/css/site_" + Lang + ".css") + "?v=" + Centrix_Version %>" />
    <link rel="stylesheet" type="text/css" href="<%=ResolveUrl("~/Common/css/slickupload_" + Lang + ".css") + "?v=" + Centrix_Version %>" />
    <link rel="stylesheet" type="text/css" href="<%=ResolveUrl("~/Common/css/uploaddragdrop_" + Lang + ".css") + "?v=" + Centrix_Version %>" />
        <link rel="stylesheet" type="text/css" href="<%=ResolveUrl("~/Common/css/main_" + Lang + ".css") + "?v=" + Centrix_Version %>" />
    <script src="../DataDictionary/DataDictionary-ar.js" type="text/javascript"></script>
    <script src="../DataDictionary/DataDictionary-en.js" type="text/javascript"></script>
    <script src="../JScript/jquery-1.8.0.js?v=<%=Centrix_Version %>" type="text/javascript"></script>

    <script type="text/javascript" src="../../SelfService/JS/masterjs.js?v=<%=Centrix_Version %>"></script>    
    <script type="text/javascript" src="../../SelfService/JS/main.js?v=<%=Centrix_Version %>"></script>
    <script type="text/javascript" src="../JScript/facescroll.js?v=<%=Centrix_Version %>"></script>

    <script type="text/javascript">
        var Centrix_Version = '<%=Centrix_Version %>';
        var entityId = '<%=EntityId %>';
        var DocumentId = '<%=DocumentId %>';
        var URLName = '<%=URLName %>';
        var DocId = '<%=DocId %>';
        var VarDocId = '<%=VarDocId %>';
        var removeLnk;
        var UploadFileSize = '<%=ConfigurationManager.AppSettings["FileUploadSize"].ToString() %>';
        var UploadFileType = '<%=ConfigurationManager.AppSettings["FileUploadType"].ToString() %>';
        var valid = '<%=valid %>';
        var fs = '<%=FS %>';
        var extnsn = '<%=ext %>';
        var whichImage = "";




        $(document).ready(function () {



            if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
                $('input.file').addClass('ff-width');
            }



            if (VarDocId != "") {
                if (VarDocId == "PersonalImageId") {
                    whichImage = getResource("PersonalImagee");
                }
                else if (VarDocId == "IdentificationImageId") {
                    whichImage = getResource("IdentificationImagee");
                }
            }

            if (valid == 'False') {
                alert(whichImage);
            }

            addLnk = $('#<%=UploadDoc.ClientID %>');

            var userLang = navigator.userLanguage || navigator.language;
            if (userLang.toLowerCase() == 'ar') {
                addLnk.css('width', '65px');
            }


            parent.changeDocId(Number(DocId), '<%=VarDocId %>', "hasFile" + '<%=VarDocId %>', Number(DocId) > 0);
            $('.label-data').hide();

            $('.iframe-submit').click(function (e) {

                if (!addLnk.val()) {
                    if (parent.DocumentIframSubmited) {
                        parent.DocumentIframSubmited(-1, '');
                    }
                    return false;
                }
            });

            parent.setDocumentId(Number('<%=DocumentId %>'));

            removeLnk = $('<a>').html('&nbsp&nbsp&nbsp&nbsp&nbsp').click(function () {
                $('.label-data').hide();
                addLnk.val('');

                parent.setDocumentId(-1);
                parent.changeDocId(-1, '<%=VarDocId %>', "hasFile" + '<%=VarDocId %>', false);
            });


            if (Number(DocId) > 0)
                $('.label-data10').html(URLName.replace(/C:\\fakepath\\/i, '').substring(0, 18)).append(removeLnk).attr('title', URLName.replace(/C:\\fakepath\\/i, '')).show();

            addLnk.change(function () {

                var FileSize = this.files ? ((this.files[0].size)) : 0;
                var extension = this.files ? (this.files[0].name.replace(/^.*\./, '').toLowerCase()) : '';
                var DocumentId = DocId;
                var documentVariable = VarDocId;
                var URLDocumentName = URLName;
                var valdType = false;
                var UploadFileTypes = UploadFileType.split(',');

                if (extension) {
                    $.each(UploadFileTypes, function (index, item) {
                        if (item == extension)
                            valdType = true;
                    });
                }
                else {
                    valdType = true;
                }

                if (!valdType) {
                    parent.alert(parent.getResource("ImageFormatIsNotAllowed") + " [" + UploadFileType + "]");

                    $(this).val('');
                }
                else

                    if (FileSize > UploadFileSize) {
                        $('.label-data').hide();

                        parent.alert(parent.getResource("MaximumImageSizee"));
                        $(this).val('');
                    } else {


                        parent.changeDocId(Number(DocId), '<%=VarDocId %>', "hasFile" + '<%=VarDocId %>', true);

                        var fileName = $(this).val();

                        removeLnk = $('<a>').html('&nbsp&nbsp&nbsp&nbsp&nbsp').click(function () {
                            $('.label-data').hide();
                            addLnk.val('');

                            parent.setDocumentId(-1);
                            parent.changeDocId(-1, '<%=VarDocId %>', "hasFile" + '<%=VarDocId %>', false);
                        });


                        $('.label-data10').html(fileName.replace(/C:\\fakepath\\/i, '').substring(0, 18)).append(removeLnk).attr('title', fileName.replace(/C:\\fakepath\\/i, '')).show();

                    }
            });

            if (DocumentId > 0) {
                var removeLnk = $('<a>').html('&nbsp&nbsp&nbsp&nbsp&nbspX').click(function () {
                    $('.label-data').hide();
                    addLnk.val('');
                    addLnk.replaceWith(addLnk.clone(true));

                    parent.setDocumentId(-1);
                    parent.changeDocId(-1, '<%=VarDocId %>', "hasFile" + '<%=VarDocId %>', false);
                });

                var fileName = '<%=Filename %>';
                $('.label-data').html(fileName.replace(/C:\\fakepath\\/i, '').substring(0, 18)).append(removeLnk).attr('title', fileName.replace(/C:\\fakepath\\/i, '')).show();

                if (parent.DocumentIframSubmited) {
                    parent.DocumentIframSubmited(DocumentId, '<%=Filename %>');
                }
            }

            $('form').unbind();
        });

        function DeleteDocument(id) {

            parent.showConfirmMsg('Delete Document', 'Are you sure?', function () {
                $('.label-data').hide();
                $.ajax({
                    type: "post",
                    contentType: "application/json; charset=utf-8",
                    url: 'DocumentWebService.asmx/DeleteDocument',
                    data: '{ id: "' + id + '"}',
                    dataType: "json",
                    success: function (e) {
                    },
                    error: function (e) {

                    }
                });
            });
        }

        function loadConfigurationValues() {
        }

        function getSystemOnScreenNotification()
        { }

    </script>


    <style type="text/css">
        body
        {
            background-image:none !important;
            background-color:White !important;
            }
        .file2
        {
            border: 1px solid #DDDDDD;
            padding: 0.3em 1px;
            width: 178px;            
        }
        .file
        {
            [width:85px;width:85px;]            
            }
            
            .file
            {
                width:85px;
                }
                
                .ff-width
                {
                    width:78px;
                }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div class="white_content">
            <div class="confermitaion-msg-box">
                <div class="cmb-title">
                    <span>Delete record</span></div>
                <div class="cmb-desc">
                    <span>Are you sure you want to delete this record ?!</span></div>
                <div class="cmb-controls">
                    <input type="button" value="Confirm" class="button2 confirm-button" />&nbsp;<input
                        type="button" value="Cancel" class="button-cancel" /></div>
            </div>
        </div>
        <asp:FileUpload ID="UploadDoc" runat="server" CssClass="file"  />
        <asp:Button runat="server" ID="btnUpload" OnClick="btnUpload_Clikc" Style="display: none;"  CssClass="iframe-submit"/>
        <asp:Panel runat="server" ID="pnlDoc" Visible="true">
        <span class="label-data" style="cursor: pointer; display:none;"><a></a></span>
            <ul class="ac-selected-items cx-ac-mult-ul" style="width: 155px;">
               
                <li style="cursor: pointer;display:none;" class="label-data ac-multi-li-text label-data10"><div></div><a></a></li>
            </ul>
        </asp:Panel>
    </div>
    </form>
</body>
</html>
--%>