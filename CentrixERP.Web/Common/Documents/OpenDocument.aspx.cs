using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.IO;
using SagePOS.Server.Configuration;
using SagePOS.Manger.Common.Business.Entity;
using CommonMgr = CentrixERP.Common.Business.IManager;
using SF.Framework;

namespace SagePOS.Common.Web.NewCommon.Common.Documents
{
    public partial class OpenDocument : System.Web.UI.Page
    {
        static Dictionary<string, string> mimeTypesMapper = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            //String FileName = HttpUtility.UrlDecode(Request.QueryString["downloadName"]);
            //String FilePath = HttpUtility.UrlDecode(Request.QueryString["Path"]);
            string FilePath = "";
            string FileName = "";
            string url2 = SagePOS.Server.Configuration.Configuration.GetConfigKeyValue("NewDocumentsURL");          

            int DocumentEntityId = HttpUtility.UrlDecode(Request["Id"]).ToNumber();
            int ParentId = HttpUtility.UrlDecode(Request["parentId"]).ToNumber();  
            string moduleName = HttpUtility.UrlDecode(Request.QueryString["moduleName"]);

            CommonMgr.IDocumentEntityManager IDocumentEntityManager = (CommonMgr.IDocumentEntityManager)IoC.Instance.Resolve(typeof(CommonMgr.IDocumentEntityManager));
            DocumentEntity document = IDocumentEntityManager.FindById(DocumentEntityId, null);
            if (document != null) {
                FilePath = url2 + moduleName + "/" + ParentId + "/" + document.DocumentObj.FileName + document.DocumentObj.FileExtension;
                FileName = HttpUtility.UrlEncode(document.DocumentObj.OrginalFileName)+ document.DocumentObj.FileExtension;
            }

            string ext = System.IO.Path.GetExtension(FilePath);
            string mimeType = getMimeType(ext.ToLower());

            System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
            response.ClearContent();
            response.Clear();
            response.ContentType = mimeType;
            //response.ContentType = "text/plain";
            //response.AddHeader("Content-Disposition", "attachment; filename=" + FileName + ";");
            if (!IsImageExtension(ext.ToLower()))
                response.AddHeader("Content-Disposition", "attachment; filename=" + "\"" + FileName + "\"" + ";");
            //Server.UrlPathEncode(FileName);
            response.TransmitFile(FilePath);
            response.Flush();
            response.End();



            //FileStream fileStream = new FileStream("D:\\2.pdf", FileMode.Open, FileAccess.Read);

            //Response.ContentType = "application/octet-stream";
            //FileStream fileStream = new FileStream(FilePath, FileMode.Open, FileAccess.Read);

            //Response.ContentType = "application/octet-stream";
            //byte[] buffer = new byte[4096];
            //int count = 0;
            //Response.AppendHeader("content-Disposition", string.Format("inline;filename=" + FileName));
            //while ((count = fileStream.Read(buffer, 0, buffer.Length)) > 0)
            //{
            //    Response.OutputStream.Write(buffer, 0, count);
            //    Response.Flush();
            //}
        }

        string getMimeType(string ext)
        {
            LoadMimeTypes();

            return mimeTypesMapper[ext];
        }

        static void LoadMimeTypes()
        {
            if (mimeTypesMapper == null || mimeTypesMapper.Count == 0)
            {
                mimeTypesMapper = new Dictionary<string, string>();

                string MimeTypesMapperPath = SagePOS.Server.Configuration.Configuration.GetConfigKeyPath("MimeTypesMapper");
                XDocument doc = XDocument.Load(MimeTypesMapperPath);

                foreach (XElement element in doc.Elements("MimeTypes").Elements("MimeType"))
                {
                    string mimeExt = element.Element("Ext").Value.ToLower(); ;
                    string mimeType = element.Element("Mime").Value;
                    if (!mimeTypesMapper.ContainsKey(mimeExt))
                        mimeTypesMapper.Add(mimeExt, mimeType);
                }

            }
        }

        static bool IsImageExtension(string ext)
        {
            return (ext == ".png" || ext == ".gif" || ext == ".bmp" || ext == ".dib" || ext == ".jpg" || ext == ".tif" || ext == ".tiff" || ext == ".txt" || ext == ".pdf");
        }
    }
}