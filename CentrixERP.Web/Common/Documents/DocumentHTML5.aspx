<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocumentHTML5.aspx.cs"
    Inherits="SagePOS.Common.Web.Documents.DocumentHTML5" EnableViewState="false"
    Title="Slick" %>

<%@ Import Namespace="Krystalware.SlickUpload" %>
<%@ Register TagPrefix="kw" Assembly="Krystalware.SlickUpload" Namespace="Krystalware.SlickUpload.Web.Controls" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="stylesheet" type="text/css" href="<%=ResolveUrl("~/Common/css/site_" + Lang + ".css") + "?v=" + Centrix_Version %>" />
    <link rel="stylesheet" type="text/css" href="<%=ResolveUrl("~/Common/css/slickupload_" + Lang + ".css") + "?v=" + Centrix_Version %>" />
    <link rel="stylesheet" type="text/css" href="<%=ResolveUrl("~/Common/css/uploaddragdrop_" + Lang + ".css") + "?v=" + Centrix_Version %>" />
   
    <script src="../DataDictionary/DataDictionary-ar.js" type="text/javascript"></script>
    <script src="../DataDictionary/DataDictionary-en.js" type="text/javascript"></script>
    <script type="text/javascript" src="../JScript/jquery-1.8.0.js?v=<%=Centrix_Version %>"></script>
    <script type="text/javascript" src="../JScript/jquery.min.js?v=<%=Centrix_Version %>"></script>
    <script type="text/javascript" src="../JScript/jquery-ui.min.js?v=<%=Centrix_Version %>"></script>
    <script type="text/javascript" src="../JScript/facescroll.js?v=<%=Centrix_Version %>"></script>
    <script type="text/javascript" src="../JScript/jquery.ui.touch-punch.min.js?v=<%=Centrix_Version %>"></script>
    <script src="../JScript/jquery.ui.widget.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <script src="../JScript/jquery.ui.button.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
        <script src="../JScript/ERPJS/systemconfiguraation.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <script type="text/javascript">
        var Centrix_Version = '<%=Centrix_Version %>';
        var ApplicationURL_Common = '<%=ApplicationURL_Common %>';
        var slickUpload = "<%=slickUpload.ClientID %>";
        var fileList = "<%=fileList.ClientID %>";
        var moduleKey = "<%=moduleKey%>";
        var moduleName = "<%=moduleName%>";
        var parentId = "<%=parentId %>";
        var folder = "<%=parentId %>";
        var userId = "<%=LoggedInUser.UserId %>";
        var selectedFolder = '';
        var foldersJSON = '<%=FolderArr %>';
        var flag = false;
        var ApplicationURL = "<%=ApplicationURL_Common %>";
        var lang = "<%=Lang %>";
        var confirmMsgText = '<%=GetLocalResourceObject("confirmMsgText").ToString()%>';
        var deleteDocumentConfirmMsgText = '<%=GetLocalResourceObject("deleteDocumentConfirmMsg.Text").ToString()%>';
        var rowToDelete = '#divDocument';
        var SubFolder = '<%= hdnSubFolder.ClientID %>';
        var Path = '';
        var FolderPath = "<%=parentId %>";
        var DisabledAttachment = '<%=DisableAttachment %>';
        var FolderName = '';
        var firstTime = true;
        var EntityID = '<%= entityId %>';
        var parentEntityId = '<%= ParentEntityId %>';
        var parentDocumentFolderId = 1;
        var maxDocumentSizeKB = '<%=ConfigurationManager.AppSettings["maxDocumentSizeKB"] %>';
        var PermissionList = '<%= PermissionsList %>';
        var AjaxAddFolderLogically = "../WebServices/DocumentFolderWebService.asmx/AddSubFolder"
        var ajaxDeleteFolderPageURL = "../WebServices/DocumentWebService.asmx/DeleteFolder";
        var ajaxDeletePageURL = "../WebServices/DocumentWebService.asmx/DeleteDocument";
        var ajaxAddFolderPageURL = "../WebServices/DocumentWebService.asmx/AddFolder";
        var DocumentPath = ""; //  '<%= DocumentPath %>';
        var DocumentCount = '<%=DocumentCount %>';
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.Folders-side').alternateScroll();
            $('.Folders-side').height($(window).height() - 63);

            $(this).find('.edit-pnl').hide();
            if (DisabledAttachment == 1) {
                $('.Action-div').remove();
            }
        });
    </script>
    <kw:KrystalwareScriptRenderer ID="KrystalwareScriptRenderer1" ScriptInclude="true"
        runat="server" />
    <script type="text/javascript" src="../JScript/uploaddragdrop.js?v=<%=Centrix_Version %>"></script>
    <script type="text/javascript" src="../JScript/tree.js?v=<%=Centrix_Version %>"></script>
    <script type="text/javascript" src="../JScript/main.js?v=<%=Centrix_Version %>"></script>
    <script type="text/javascript" src="../JScript/ERPJS/masterjs.js?v=<%=Centrix_Version %>"></script>
    <%-- to make button add style--%>
    <%-- to make button add style--%>
</head>
<body>
    <form id="uploadForm" runat="server" autocomplete="off">
    <%--<div class="DragDropBackground">
        <span class="background-text-img"></span><span class="background-text">Drop files here</span>
    </div>--%>
    <input type="hidden" runat="server" id="hdnSubFolder" />
    <input type="hidden" runat="server" id="hdnPermissionList" />
    <div class="white_content">
        <div class="confermitaion-msg-box">
            <div class="cmb-title">
                <span>Delete record</span></div>
            <div class="cmb-desc">
                <span>Are you sure you want to delete this record ?!</span></div>
            <div class="cmb-controls">
                <input type="button" value="Confirm" class="button2 confirm-button" />&nbsp;<input
                    type="button" value="Cancel" class="button-cancel" /></div>
        </div>
    </div>
    <div class="folderscontainer">
        <div class="Details-div ">
            <div class="Action-div" style="display:none">
                <ul>
                    <li class="edit-pnl viewedit-display-block doc-Rename-icons"><span class="folder-img rename-folder floatLeft"></span><a class="ActionEdit-link"><span resourcekey="DocumentRename"></span> </a></li>
                    <li class="edit-pnl viewedit-display-block doc-delete-icons"><span class="deleteFolder"></span><a class="ActionEdit-link"><span resourcekey="DocumentDelete"></span> </a></li>
                    <li class="edit-pnl viewedit-display-block doc-AddFolder-icons">
                       <button class='btnAdd folder-img add-folder floatLeft' id="btnAdd_level_1" title="+ Add new folder">
                      </button> <a class="ActionEdit-link"><span resourcekey="Addfolder"></span> </a></li>
                </ul>
            </div>
        </div>
        <div class="add-text-box">
            <div id="divAdd_1" style="padding-right: 5px">
                <%-- <button class='btnAdd folder-img add-folder' id="btnAdd_level_1" title="+ Add new folder"></button>--%>
                <%--<button class='btnAdd folder-img add-folder' id="btnAdd_level_1" onclick="ShowTextBox();return false;">
    <%= AddFolder%> </button>--%>
            </div>
            <div id="adding">
                <asp:TextBox ID="txtAddSubFolder" MaxLength="30" runat="server" onfocus="this.value==this.defaultValue?this.value='':null"
                    CssClass="new-folder-textbox"></asp:TextBox>
                <button id="btnSave" class="btnsave" style="display: none;" visible="false" onclick="SaveFolders();return false;">
                    <%= SaveString %></button>
            </div>
        </div>
        <div class="SubFoldersList" style="display: none;">
            <div class="Folders-list" id="FoldersListScroll">
                <ul class="folder-tree">
                    <li><span class="folder-img closed-folder"></span><span class="folder-lvl1">Root</span>
                        <ul>
                            <li><span class="folder-img empty-folder"></span><span class="folder-lvl2">Year2010</span></li>
                            <li><span class="folder-img closed-folder"></span><span class="folder-lvl2">Year2011</span>
                                <ul>
                                    <li><span class="folder-img empty-folder"></span><span class="folder-lvl2">Year2010</span></li>
                                    <li><span class="folder-img closed-folder"></span><span class="folder-lvl2">Year2011</span></li>
                                    <li><span class="folder-img empty-folder"></span><span class="folder-lvl2">Year2012</span></li>
                                </ul>
                            </li>
                            <li><span class="folder-img empty-folder"></span><span class="folder-lvl2">Year2012</span></li>
                        </ul>
                    </li>
                    <li>
                        <div>
                            <span class="folder-img empty-folder"></span><span class="folder-lvl1">Old Files</span>
                        </div>
                    </li>
                    <li>
                        <div>
                            <span class="folder-img empty-folder"></span><span class="folder-lvl1">Images</span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="tree-view Folders-side">
            <ul class="sub-folder">
            </ul>
        </div>
    </div>
    <div id="uploadContainer" class="initial" style="height: 700px;">
        <div class="uploadContainer">
            <%--<div id="dropInstructions" class="dropInstruction">
             <%=GetGlobalResourceObject("Globalvariables","lblDropFilesText").ToString() %>
        </div>--%>
            <div class="fileselecter">
                <kw:FileSelector ID="fileSelector" UploadConnectorId="slickUpload" DropZoneId="uploadContainer"
                    runat="server">
                    <Template>
                        <a href="javascript:;" class="lnkBrowsFiles"><span id="fileSelectorText"><span resourcekey="filesfromyourcomputer"></span> 
                            <%-- <%=GetGlobalResourceObject("Globalvariables","lblBrowseFilesText").ToString() %>--%>
                        </span></a>
                    </Template>
                    <%--  <FolderTemplate>
                    <a href="javascript:;"><div id="folderSelectorText" class="lnkBrowsFiles">
                      <%=GetGlobalResourceObject("Globalvariables","lblBrowseFolderText").ToString() %>
                    </div></a>
                </FolderTemplate>--%>
                </kw:FileSelector>
            <div class="removePluginIcon" style="position: absolute;width:200px;height:100px;border:solid 0px #000;top: 38px;z-index: 1000001;right: 0;background-color: white;"></div>
            </div>
        </div>
        <div class="DragDropBackground">
            <span class="background-text-img"></span><span class="background-text" resourcekey="Dropfileshere"></span>
        </div>
        <div id="Filesorder" style="height: 335px">
            <kw:FileList ID="fileList" FileSelectorId="fileSelector" runat="server">
                <ItemTemplate>
                    <div id="Filesorder" class="Files-order">
                        <div class="inline-divs">

                            <div class="whiteBG">
                                <a href="javascript:void(0);" class="delete-file"></a><span class="dd-filename su-ext-default"></span>
                                <a class="File-img-title" target="_blank">
                                    <kw:FileListElement ID="FileListElement1" Element="FileName" runat="server" />
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="FileListElementContainer">
                        <%--<kw:FileListElement ID="FileListElement2" Element="FileSize" runat="server">
                    (calculating)</kw:FileListElement>--%>
                        <%-- <kw:FileListRemoveCommand ID="FileListRemoveCommand1" runat="server">
                    [x]</kw:FileListRemoveCommand>--%>
                        <kw:UploadProgressDisplay ID="uploadProgressDisplay" UploadConnectorId="slickUpload"
                            runat="server" Style="display: block">
                            <Template>
                                <div class="UploadProgressBarContainer">
                                    <kw:UploadProgressBar ID="UploadProgressBar1" runat="server" />
                                </div>
                                <kw:UploadProgressElement ID="UploadProgressElement1" Element="PercentCompleteText"
                                    runat="server">
                                    (calculating)</kw:UploadProgressElement>
                            </Template>
                        </kw:UploadProgressDisplay>
                    </div>
                </ItemTemplate>
            </kw:FileList>
            <div class="existfiles">
            </div>
        </div>
    </div>
    <kw:UploadConnector ID="slickUpload" AutoCompleteAfterLastFile="false" AutoUploadOnSubmit="false"
        UploadProfile="slick" OnClientUploadFileStarted="onFileStart" runat="server"
        OnClientUploadFileEnded="onFileEnd" OnClientBeforeSessionStart="onSessionStarted" />
    </form>
</body>
</html>


