using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using SF.Framework;
using System.Web.Script.Serialization;
using CentrixERP.Common.Business.IManager;
using SagePOS.Manger.Common.Business.Entity;
using SagePOS.Server.Business.Factory;
using System.Configuration;
using System.IO;

namespace SagePOS.Common.Web.Documents
{
    /// <summary>
    /// Summary description for DocumentWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class DocumentWebService : System.Web.Services.WebService
    {

        [WebMethod]
        public void DeleteDocument(int id, string moduleKey)
        {
            IDocumentEntityManager iDocumentEntityManager = (IDocumentEntityManager)IoC.Instance.Resolve<IDocumentEntityManager>();
            DocumentEntity doc = iDocumentEntityManager.FindById(id, null);
            doc.MarkDeleted();
            iDocumentEntityManager.Save(doc);
            
            //DocumentFactory factory = new DocumentFactory(moduleKey);
            //IDocument iDoc = factory.FindById(id, null);
            //iDoc.MarkDeleted();
            //factory.Save(iDoc);
        }

        [WebMethod]
        public string GetDocumentLigth(int id)
        {
            IDocumentManager manager = (IDocumentManager)IoC.Instance.Resolve(typeof(IDocumentManager));
            Document doc = manager.FindById(id, null);
            DocumentLight light = new DocumentLight();
            light = light.GetLightDocument(doc);

            JavaScriptSerializer OSerializer = new JavaScriptSerializer();
            return OSerializer.Serialize(light);
        }

        [WebMethod]
        public string AddFolder(string moduleName, string FolderPath, string FolderName)
        {

            string Path = SagePOS.Server.Configuration.Configuration.GetConfigKeyPath("DocumentsFolderPath");
            string DirectoryPath = Path + moduleName + "\\" + FolderPath + "\\" + FolderName;
            Directory.CreateDirectory(DirectoryPath);

            return RefreshFolderList(moduleName, FolderPath);

        }

        [WebMethod]
        public string DeleteFolder(int parentId, string Module, string moduleKey, string FolderPath)
        {
            string Path = SagePOS.Server.Configuration.Configuration.GetConfigKeyPath("DocumentsFolderPath");
            string toPath =  SagePOS.Server.Configuration.Configuration.GetConfigKeyValue("DeletedDocumentsFolderPath");
            string Folder = (FolderPath.Contains('/')) ? FolderPath.Replace('/', '\\') : FolderPath;
            string name = null;
            if (Folder.Contains("\\"))
                name = Folder.Substring(Folder.LastIndexOf("\\") + 1);
            else
                name = Folder;

            string newFolderName = string.Format("{0}_{1}_{2}", parentId, name, DateTime.Now.ToString("MM-dd-yyyy-hh-mm-ss"));
            string newFolderPath = toPath + "\\" + Module;


            if (!Directory.Exists(newFolderPath))
                Directory.CreateDirectory(newFolderPath);

            if (Directory.Exists(Path + Module + "\\" + Folder))
                Directory.Move(Path + Module + "\\" + Folder, newFolderPath + "\\" + newFolderName);


            IDocumentManager mgr = (IDocumentManager)IoC.Instance.Resolve(typeof(IDocumentManager));
            DocumentFactory factory = new DocumentFactory(moduleKey);
            IDocument iDoc = factory.IEntity;

            return RefreshFolderList(Module, FolderPath);
            //mgr.DeleteModuleDocumentPath(hdnSubFolder.Value.Replace('\\', '/'), iDoc.Module);
        }

        public string RefreshFolderList(string moduleName, string FolderPath)
        {
            string Path = SagePOS.Server.Configuration.Configuration.GetConfigKeyPath("DocumentsFolderPath");
            string objID = string.Empty;
            if (FolderPath.Contains('/'))
                objID = FolderPath.Substring(0, FolderPath.IndexOf('/'));
            else
                objID = FolderPath;


            DirectoryInfo dirInfo = new DirectoryInfo(Path + moduleName + "\\" + objID);
            List<Folders> listFolders = new List<Folders>();
            if (dirInfo.Exists)
                listFolders.Add(new Folders() { name = "Root", path = FolderPath, folders = getDirectories(dirInfo, dirInfo.Name) });
            else
            {
                Directory.CreateDirectory(Path + moduleName + "\\" + objID);
                listFolders.Add(new Folders() { name = "Root", path = FolderPath, folders = getDirectories(dirInfo, dirInfo.Name) });
            }
            JavaScriptSerializer OSerializer = new JavaScriptSerializer();
            return OSerializer.Serialize(listFolders);

        }

        private List<Folders> getDirectories(DirectoryInfo dirInfo, string parentFolder)
        {
            List<Folders> listFolders = new List<Folders>();


            if (dirInfo.GetDirectories() != null)
            {
                foreach (DirectoryInfo dir in dirInfo.GetDirectories())
                {

                    Folders myfs = new Folders()
                    {
                        name = dir.Name,
                        path = parentFolder + "/" + dir.Name,
                        folders = getDirectories(dir, parentFolder + "/" + dir.Name)
                    };
                    listFolders.Add(myfs);
                }

            }
            return listFolders;
        }

        [WebMethod]
        public void UpdateDocument(int id)
        {
            // Log("In Update Document API");
            IDocumentEntityManager iDocumentEntityManager = (IDocumentEntityManager)IoC.Instance.Resolve(typeof(IDocumentEntityManager));
            DocumentEntity documentEntity = iDocumentEntityManager.FindById(id, null);
            documentEntity.MarkModified();
            iDocumentEntityManager.Save(documentEntity);
          
        }

        public void SynchronizationEngineUpdatedCach(int EntityId, int CachingTypeId, int UserId)
        {



        }

        [WebMethod]
        public void UpdateDoc(int id, string title, string description)
        {
            // Log("In Update Document API");
            IDocumentManager iDocumentManager = (IDocumentManager)IoC.Instance.Resolve(typeof(IDocumentManager));
            Document document = iDocumentManager.FindById(id, null);
            document.MarkModified();
            document.Description = description;
            document.Title = title;
            iDocumentManager.Save(document);
        }



    }

    public class DocumentLight
    {
        public DocumentLight()
        { }

        public string Title { get; set; }
        public string Description { get; set; }
        public string FileName { get; set; }

        public DocumentLight GetLightDocument(SagePOS.Manger.Common.Business.Entity.Document document)
        {
            DocumentLight light = new DocumentLight()
            {
                Title = document.Title,
                Description = document.Description,
                FileName = document.FileName
            };
            return light;
        }
    }

    public class Folders
    {
        public string name { get; set; }
        public string path { get; set; }
        public List<Folders> folders { get; set; }
        public int folderId { get; set; }
        public bool? isSystem { get; set; }
    }
}
