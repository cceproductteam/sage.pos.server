using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SagePOS.Server.Business.Factory;
using SF.Framework;
using SF.FrameworkEntity;
using System.IO;
using System.Configuration;
using CentrixERP.Common.Business.IManager;
using SagePOS.Manger.Common.Business.Entity;
using SagePOS.Server.Configuration;

namespace SagePOS.Common.Web.Documents
{
    public partial class UploadDocument : BasePageLoggedIn
    {

        public UploadDocument()
            : base(Enums_S3.Permissions.SystemModules.None) { }

        public int EntityId = 0;
        public int EntityValueId = 0;
        //public string ModuleKey = "";
        //public int UserId = -1;
        public int DocumentId = -1;
        public string URLName = "";
        public string Filename = "";
        public int DocId = -1;
        public string VarDocId = "";
        public string UploadTypes = "";


        protected void Page_Load(object sender, EventArgs e)
        {
            EntityId = Request["entityId"].ToNumber();
            URLName = Request["URLName"];
            //    DocumentId = Request["DocId"].ToNumber();
            DocId = Request["DocId"].ToNumber();
            VarDocId = Request["VarDocId"];
            //ModuleName = Request["moduleName"];
            EntityValueId = Request["entityValueId"].ToNumber();
            //UserId = Request["UserId"].ToNumber();
            UploadTypes = Request["UploadTypes"];
        }


        protected void btnUpload_Clikc(Object sender, EventArgs e)
        {
            if (UploadDoc.HasFile)
            {

                FileInfo info = new FileInfo(UploadDoc.FileName);
                Filename = info.Name;
                //DocumentFactory factory = new DocumentFactory(ModuleKey);
                //factory.IEntity.CreatedBy = UserId;
                //factory.IEntity.ModuleId = ParentId;
                //factory.IEntity.DocumentObj = new Business.Entity.Document();
                //factory.IEntity.DocumentObj.CreatedBy = UserId;
                //factory.IEntity.DocumentObj.OrginalFileName = Path.GetFileNameWithoutExtension(UploadDoc.FileName);
                //factory.IEntity.DocumentObj.FileExtension = Path.GetExtension(UploadDoc.FileName);
                //factory.IEntity.DocumentObj.documentBytes = UploadDoc.FileBytes;
                //factory.IEntity.DocumentObj.PreventPhysicalSave = false;
                //factory.IEntity.DocumentObj.DocumentPath = factory.IEntity.DocumentPath;
                //factory.IEntity.DocumentObj.FolderId = 0;
                //factory.IEntity.DocumentObj.ParentFolder = "1";
                //factory.IEntity.DocumentObj.OldParentFolder = "1";
                //factory.IEntity.DocumentObj.ParentEntityId = ParentId;
                //factory.Save(factory.IEntity);
                //DocumentId = factory.IEntity.Id;
                //pnlDoc.Visible = true;
                IDocumentEntityManager iDocumentEntityManager = (IDocumentEntityManager)IoC.Instance.Resolve(typeof(IDocumentEntityManager));
                DocumentEntity documentEntity = new DocumentEntity();
                documentEntity.CreatedBy = LoggedInUser.UserId;
                documentEntity.EntityId = EntityId;
                documentEntity.EntityValueId = EntityValueId;
                documentEntity.DocumentObj = new SagePOS.Manger.Common.Business.Entity.Document();
                documentEntity.DocumentObj.CreatedBy = LoggedInUser.UserId;
                documentEntity.DocumentObj.OrginalFileName = Path.GetFileNameWithoutExtension(UploadDoc.FileName);
                documentEntity.DocumentObj.FileExtension = Path.GetExtension(UploadDoc.FileName);
                documentEntity.DocumentObj.documentBytes = UploadDoc.FileBytes;
                documentEntity.DocumentObj.PreventPhysicalSave = false;
                //documentEntity.DocumentObj.DocumentPath = documentEntity.DocumentPath;
                documentEntity.DocumentObj.DocumentPath = SagePOS.Server.Configuration.Configuration.GetConfigKeyPath("DocumentsPath") + "CX-Entity-" + EntityId + "\\";
                documentEntity.DocumentObj.FolderId = 0;
                documentEntity.DocumentObj.ParentFolder = "1";
                documentEntity.DocumentObj.OldParentFolder = "1";
                documentEntity.DocumentObj.ParentEntityId = EntityValueId;
                iDocumentEntityManager.Save(documentEntity);
                DocumentId = documentEntity.DocumentId;
                pnlDoc.Visible = true;

            }
        }

        //public int ParentId = 0;
        //public string ModuleName = "";
        //public string ModuleKey = "";
        //public int UserId = -1;
        //public int DocumentId = -1;
        //public string Filename = "";
        //public string CompanyProfileDocumntPath;



        //protected void Page_Load(object sender, EventArgs e)
        //{
        //    ParentId = Request["parentId"].ToNumber();
        //    //ModuleName = Request["moduleName"];
        //    ModuleKey = Request["moduleKey"];
        //    UserId = Request["UserId"].ToNumber();
        //    CompanyProfileDocumntPath = System.Configuration.ConfigurationManager.AppSettings["CompanyProfileDocumentsFolderPath"].ToString();
        //}


        //protected void btnUpload_Clikc(Object sender, EventArgs e)
        //{
        //    if (UploadDoc.HasFile)
        //    {

        //        FileInfo info = new FileInfo(UploadDoc.FileName);
        //        Filename = info.Name;

        //        if (ParentId > 0)
        //        {
        //            DocumentFactory factory = new DocumentFactory(ModuleKey);
        //            factory.IEntity.CreatedBy = UserId;
        //            factory.IEntity.ModuleId = ParentId;

        //            factory.IEntity.DocumentObj = new Business.Entity.Document();
        //            factory.IEntity.DocumentObj.CreatedBy = UserId;

        //            factory.IEntity.DocumentObj.OrginalFileName = Path.GetFileNameWithoutExtension(UploadDoc.FileName);
        //            factory.IEntity.DocumentObj.FileExtension = Path.GetExtension(UploadDoc.FileName);
        //            factory.IEntity.DocumentObj.documentBytes = UploadDoc.FileBytes;
        //            factory.IEntity.DocumentObj.PreventPhysicalSave = false;
        //            factory.IEntity.DocumentObj.DocumentPath = factory.IEntity.DocumentPath;
        //            factory.IEntity.DocumentObj.FolderId = 0;
        //            factory.IEntity.DocumentObj.ParentFolder = "1";
        //            factory.IEntity.DocumentObj.OldParentFolder = "1";
        //            factory.IEntity.DocumentObj.ParentEntityId = ParentId;

        //            factory.Save(factory.IEntity);

        //            DocumentId = factory.IEntity.Id;
        //        }
        //        else
        //        {
        //            IDocumentManager documentManager = (IDocumentManager)IoC.Instance.Resolve(typeof(IDocumentManager));
        //            Document documentObject = new Document();
        //            documentObject.OrginalFileName = Path.GetFileNameWithoutExtension(UploadDoc.FileName);
        //            documentObject.FileExtension = Path.GetExtension(UploadDoc.FileName);
        //            documentObject.documentBytes = UploadDoc.FileBytes;
        //            documentObject.PreventPhysicalSave = false;
        //            documentObject.DocumentPath = CompanyProfileDocumntPath;
        //            documentObject.FolderId = 0;
        //            documentObject.ParentFolder = "1";
        //            documentObject.OldParentFolder = "1";
        //            documentObject.ParentEntityId = ParentId;
        //            documentObject.PreventPhysicalSave = false;
        //            documentManager.Save(documentObject);
        //            DocumentId = documentObject.DocumentID;
        //        }
        //        pnlDoc.Visible = true;
        //    }
        //}

    }
}