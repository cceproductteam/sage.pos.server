<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GroupList.aspx.cs" Inherits="SagePOS.Common.Web.NewCommon.Common.Groups.GroupList"
    MasterPageFile="../MasterPage/Site.Master" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="PageHeader">
    <script src="JS/Groups.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="PageBody">
    <div id="demo2" class="result-container">
        <ul class="Result-list">
        </ul>
        <div class="show-more-results-div">
            <span>Show more</span>
        </div>
        <div class="loading-list" style="width: 100%; height: 50px; text-align: center">
            <span>loading...</span></div>
    </div>
    <div id='content'>
        <div class="Result-info-container">
            <div class="result-name">
                <ul>
                    <li class="nameLI"><span class="CompanyNameTrim"></span></li>
                </ul>
            </div>
            <div style="float: right;" id='page_navigation'>
            </div>
            <div class="ClearDiv">
            </div>
            <div class="test">
                <div class="QuickSearchDiv">
                </div>
                <div id="TabbedPanels1" class="TabbedPanels">
                    <ul class="TabbedPanelsTabGroup">
                        <li class="TabbedPanelsTab" tabindex="1">
                            <div class="TabbedPanelsTabSelected-arrow" style="display: block;">
                            </div>
                            <span class="TabbedPanelsTab-img Summery-tab"></span><span>Summary</span> </li>
                    </ul>
                    <div class="TabbedPanelsContentGroup">
                    </div>
                    <!--More-->
                    <div class="More-holder">
                        <div class="moreTabsList">
                            <ul class="moreTabs-ul">
                            </ul>
                        </div>
                    </div>
                    <!--End More-->
                </div>
            </div>
        </div>
    </div>
</asp:Content>
