using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SF.Framework;
using SagePOS.Common.Web;
using SagePOS.Server.Configuration;
using Centrix.UM.Business.Entity;

namespace SagePOS.Common.Web.Settings
{
    public partial class ERPSetup : BasePageLoggedIn
    {
        public ERPSetup()
            : base(Enums_S3.Permissions.SystemModules.None) { }

        List<RolePermission> Permissionslist = new List<RolePermission>();
        public bool GLSetups = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            GLSetups = Convert.ToBoolean(Request["glsetup"]);
            //int? Id = -1;
            //int roleID = LoggedInUser.RoleId;
            //IRolePermissionManager mgr = (IRolePermissionManager)IoC.Instance.Resolve(typeof(IRolePermissionManager));
            //Permissionslist = mgr.FindByParentId(roleID, typeof(Role), null);

            //lnkUsers.NavigateUrl = SagePOS.Server.Configuration.URLs.GetURL(SagePOS.Server.Configuration.URLEnums.Entity.User, SagePOS.Server.Configuration.URLEnums.Action.Default.List, ref Id);
            //lnkUsers.Enabled = checkPermission(Id.Value, lnkUsers);


            //lnkRoles.NavigateUrl = SagePOS.Server.Configuration.URLs.GetURL(SagePOS.Server.Configuration.URLEnums.Entity.Role, SagePOS.Server.Configuration.URLEnums.Action.Default.List, ref Id);
            //lnkRoles.Enabled = checkPermission(Id.Value, lnkRoles);

            //lnkTeams.NavigateUrl = SagePOS.Server.Configuration.URLs.GetURL(SagePOS.Server.Configuration.URLEnums.Entity.Team, SagePOS.Server.Configuration.URLEnums.Action.Default.List, ref Id);
            //lnkTeams.Enabled = checkPermission(Id.Value, lnkTeams);

            //lnkLovs.NavigateUrl = SagePOS.Server.Configuration.URLs.GetURL(SagePOS.Server.Configuration.URLEnums.Entity.DataTypeContent, SagePOS.Server.Configuration.URLEnums.Action.Default.List, ref Id);
            //lnkLovs.Enabled = checkPermission(Id.Value, lnkLovs);

            //lnkActivityLookups.NavigateUrl = SagePOS.Server.Configuration.URLs.GetURL(SagePOS.Server.Configuration.URLEnums.Entity.ActivityLookup, SagePOS.Server.Configuration.URLEnums.Action.Default.List, ref Id);
            //lnkActivityLookups.Enabled = checkPermission(Id.Value, lnkActivityLookups);

            //lnkConfigrations.NavigateUrl = SagePOS.Server.Configuration.URLs.GetURL(SagePOS.Server.Configuration.URLEnums.Entity.Common, SagePOS.Server.Configuration.URLEnums.Action.Extended.Configration, ref Id);
            //lnkConfigrations.Enabled = checkPermission(Id.Value, lnkConfigrations);

            //lnkTemplates.NavigateUrl = SagePOS.Server.Configuration.URLs.GetURL(SagePOS.Server.Configuration.URLEnums.Entity.Template, SagePOS.Server.Configuration.URLEnums.Action.Default.List, ref Id);
            //lnkTemplates.Enabled = checkPermission(Id.Value, lnkTemplates);

            //lnkTemplatesTypes.NavigateUrl = SagePOS.Server.Configuration.URLs.GetURL(SagePOS.Server.Configuration.URLEnums.Entity.TemplateType, SagePOS.Server.Configuration.URLEnums.Action.Default.List, ref Id);
            //lnkTemplatesTypes.Enabled = checkPermission(Id.Value, lnkTemplatesTypes);

            //lnkHistory.NavigateUrl = SagePOS.Server.Configuration.URLs.GetURL(SagePOS.Server.Configuration.URLEnums.Entity.SentNotifications, SagePOS.Server.Configuration.URLEnums.Action.Default.List, ref Id);
            //lnkHistory.Enabled = checkPermission(Id.Value, lnkHistory);

            //lnkSendNotification.NavigateUrl = SagePOS.Server.Configuration.URLs.GetURL(SagePOS.Server.Configuration.URLEnums.Entity.SendNotifications, SagePOS.Server.Configuration.URLEnums.Action.Extended.SendNotifications, ref Id);
            //lnkSendNotification.Enabled = checkPermission(Id.Value, lnkSendNotification);

            //lnkEmailConfigration.NavigateUrl = SagePOS.Server.Configuration.URLs.GetURL(SagePOS.Server.Configuration.URLEnums.Entity.EmailConfigration, SagePOS.Server.Configuration.URLEnums.Action.Default.Edit, ref Id);
            //lnkEmailConfigration.Enabled = checkPermission(Id.Value, lnkEmailConfigration);

            //lnkDuplicationRule.NavigateUrl = SagePOS.Server.Configuration.URLs.GetURL(SagePOS.Server.Configuration.URLEnums.Entity.DuplicationRule, SagePOS.Server.Configuration.URLEnums.Action.Default.Edit, ref Id);
            //lnkDuplicationRule.Enabled = checkPermission(Id.Value, lnkDuplicationRule);

            //lnkAccountInformations.NavigateUrl = SagePOS.Server.Configuration.URLs.GetURL(SagePOS.Server.Configuration.URLEnums.Entity.UserAccount, SagePOS.Server.Configuration.URLEnums.Action.Default.Edit, ref Id);
            //lnkAccountInformations.Enabled = checkPermission(Id.Value, lnkAccountInformations);

            //lnkCredit.NavigateUrl = SagePOS.Server.Configuration.URLs.GetURL(SagePOS.Server.Configuration.URLEnums.Entity.Credits, SagePOS.Server.Configuration.URLEnums.Action.Default.List, ref Id);
            //lnkCredit.Enabled = checkPermission(Id.Value, lnkCredit);

            //lnkCreditHistory.NavigateUrl = SagePOS.Server.Configuration.URLs.GetURL(SagePOS.Server.Configuration.URLEnums.Entity.CreditsHistory, SagePOS.Server.Configuration.URLEnums.Action.Default.List, ref Id);
            //lnkCreditHistory.Enabled = checkPermission(Id.Value, lnkCreditHistory);

            //lnkPriceList.NavigateUrl = SagePOS.Server.Configuration.URLs.GetURL(SagePOS.Server.Configuration.URLEnums.Entity.PriceList, SagePOS.Server.Configuration.URLEnums.Action.Default.List, ref Id);
            //lnkPriceList.Enabled = checkPermission(Id.Value, lnkPriceList);
            //lnkCurrency.NavigateUrl = SagePOS.Server.Configuration.URLs.GetURL(SagePOS.Server.Configuration.URLEnums.Entity.Currency, SagePOS.Server.Configuration.URLEnums.Action.Default.List, ref Id);
            //lnkCurrency.Enabled = checkPermission(Id.Value, lnkCurrency);

            //lnkSecurityGroups.NavigateUrl = SagePOS.Server.Configuration.URLs.GetURL(SagePOS.Server.Configuration.URLEnums.Entity.SelfServiceSecurityGroup, SagePOS.Server.Configuration.URLEnums.Action.Default.List, ref Id);
            //lnkSecurityGroups.Enabled = checkPermission(Id.Value, lnkSecurityGroups);
        }


        //protected bool checkPermission(int Id, HyperLink lnk)
        //{
        //    bool hasPermission = LoggedInUser.IsAdmin ? true : false;
        //    if (!hasPermission && Permissionslist != null)
        //    {
        //        IEnumerable<RolePermission> permission = from RolePermission per in Permissionslist
        //                                                 where per.Permissionid == Id
        //                                                 select per;
        //        hasPermission = permission != null && permission.Count() != 0;
        //    }

        //    lnk.CssClass = (!hasPermission) ? "settingslnkdisabled" : "settingslnkenabled";
        //    return (hasPermission);
        //}
    }
}