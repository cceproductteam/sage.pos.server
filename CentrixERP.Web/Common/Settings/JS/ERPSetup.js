function InnerPage_Load() {


    $('.Adminstration-main-div').alternateScroll();
    if ($('.MoreListMenu-ul').find('li').length <= 0) {
        $('.MoreList').hide();
    }


}

$(document).ready(function () {
    $('.Top-Mid-section').remove();
    if (getQSKey('glsetup') == "true") {
        $('.general-ledger-div').show();
        $('.administrative-settings-div').hide();
        setSelectedMenuItem('.general-ledger');
       // setPageTitle(getResource('General Ledger'));

      //  checkGLPermissions(SystemEntities.GlReport);
    }
    else {
        if (getQSKey('cmsetup') == "true") {
            $('.general-ledger-div').hide();
            $('.administrative-settings-div').hide();
            $('.cash-managment').show();
            setSelectedMenuItem('.cash-managment');
            //setPageTitle(getResource('Cash Management'));
        }
        else {
            $('.general-ledger-div').hide();
            $('.administrative-settings-div').show();
            setSelectedMenuItem('.administrative-settings');
            //setPageTitle(getResource('CommonServices'));
        }
    }
});


function checkGLPermissions(EntityId) {

    var data = formatString('{EntityId:{0},ParentId:{1}}', EntityId, -1);
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: PemissionWebServiceURL + "checkUnAvailablePermissions",
        data: data,
        datatype: "json",
        success: function (data) {
            debugger;
            var returnedData = eval("(" + data.d + ")");
            if (returnedData.statusCode.Code == 1) {
                if (returnedData.result != null) {
                    UnAvailablePermissionsList = returnedData.result;

                }

            }
            else {
                //if 0: then there is no permissions at all
                // alert('no access');
            }

        },
        error: function (e) { }
    });
}