﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SagePOS.Server.Configuration;
using SagePOS.Common.Web;

namespace SagePOS.Common.Web.NewCommon.Common.Settings
{
    public partial class OldSetup :BasePageLoggedIn
    {
        public OldSetup()
            : base(Enums_S3.Permissions.SystemModules.None) { }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}