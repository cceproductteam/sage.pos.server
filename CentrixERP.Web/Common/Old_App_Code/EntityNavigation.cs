using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SF.FrameworkEntity;
using System.Web.UI.WebControls;
using SF.Framework;
using System.Text;
using System.Web.Script.Serialization;
using SagePOS.Manger.Common.Business.Entity;
using System.Reflection;
using SagePOS.Server.Configuration;

namespace SagePOS.Common.Web
{
    public class EntityNavigation<TEntity, TValue, TCriteria, ITManager>
       where TEntity : EntityBase<TEntity, TValue>
    {
        public string SortField { get; set; }
        public string PageName { get; set; }
        public string SearchCriteriaObj { get; set; }
        public string CurrentIdList { get; set; }
        public string PreviousIdList { get; set; }
        public int LoggedInUserID { get; set; }
        public int SortDirection { get; set; }
        public int Flag { get; set; }
        public int Filter { get; set; }
        public int EntityId { get; set; }
        public int ModuleID { get; set; }
        public HyperLink LnkPrevious { get; set; }
        public HyperLink LnkNext { get; set; }

        public void SetNavigationLink()
        {
            string DecryptedIds = CurrentIdList;
            List<string> PreviousIdsList = (PreviousIdList != "" && PreviousIdList != null) ? PreviousIdList.TrimEnd(',').Split(',').ToList() : null;
            List<string> NextIdsList = new List<string>();
            string PreviousIds = "";
            List<TEntity> List;

            if (DecryptedIds == null)
            {
                List = GetList();
                //get all  ids after findAll 
                List<string> companyListIds = (from TEntity item in List select item.GetIdentity().ToString()).ToList();
                Flag = ((companyListIds.IndexOf(EntityId.ToString())) / Paging.PageSize) + 1;

                NextIdsList = (from TEntity item in List.Skip((Flag - 1) * 10).Take(Paging.PageSize)
                               select item.GetIdentity().ToString()).ToList();
                
                PreviousIdsList = (Flag != 0) ? (from TEntity item in List.Skip((Flag - 2) * 10).Take(Paging.PageSize)
                                                 select item.GetIdentity().ToString()).ToList() : null;

                foreach (string item in NextIdsList)
                {
                    DecryptedIds += item.ToString() + ",";
                }

                if (PreviousIdsList != null)
                    foreach (string item in PreviousIdsList)
                    {
                        PreviousIds += item.ToString() + ",";
                    }
            }
            else
            {
                NextIdsList = DecryptedIds.TrimEnd(',').Split(',').ToList(); 
            }

            int currentIndex = NextIdsList.IndexOf(EntityId.ToString());
            int NextIndex = currentIndex + 1;
            int PreviousIndex = currentIndex - 1;

            if (currentIndex > 0 && currentIndex < NextIdsList.Count - 1)
            {
                //LnkNext.NavigateUrl = string.Format("{0}{1}&ids={2}&previousIds={3}&SearchCriteria={4}&flag={5}", PageName, NextIdsList[NextIndex], DecryptedIds, PreviousIds,SearchCriteriaObj,Flag);
                //LnkPrevious.NavigateUrl = string.Format("{0}{1}&ids={2}&previousIds={3}&SearchCriteria={4}&flag={5}", PageName, NextIdsList[PreviousIndex], DecryptedIds, PreviousIds, SearchCriteriaObj,Flag);

                LnkNext.NavigateUrl = string.Format("{0}{1}&ids={2}&flag={4}&field={5}&sortDirection={6}&filter={7}&previousIds={8}&SearchCriteria={3}", PageName, NextIdsList[NextIndex], DecryptedIds, SearchCriteriaObj, Flag, SortField, SortDirection, Filter, PreviousIds);
                LnkPrevious.NavigateUrl = string.Format("{0}{1}&ids={2}&flag={4}&field={5}&sortDirection={6}&filter={7}&previousIds={8}&SearchCriteria={3}", PageName, NextIdsList[PreviousIndex], DecryptedIds, SearchCriteriaObj, Flag, SortField, SortDirection, Filter, PreviousIds);
            }

            //// The first element in list 
            if (currentIndex == 0)
            {
                // Calling Previous ids list(previous 10 from current index)
                if ((Flag - 1) > 0)
                {
                    // Get all companies with specific search
                    List = GetList();
                    PreviousIdsList = (from TEntity item in List.Skip((Flag - 2) * 10).Take(Paging.PageSize) select item.GetIdentity().ToString()).ToList();
                    StringBuilder Previous = new StringBuilder();
                    foreach (var item in PreviousIdsList)
                    {
                        Previous.Append(item); Previous.Append(",");
                    }
                    PreviousIds = Previous.ToString();
                    PreviousIndex = Paging.PageSize - 1;
                }

                // one element in list 
                if (NextIndex < NextIdsList.Count)
                    //LnkNext.NavigateUrl = string.Format("{0}{1}&ids={2}&previousIds={3}&SearchCriteria={4}&flag={5}", PageName, NextIdsList[NextIndex], DecryptedIds,PreviousIds, SearchCriteriaObj, Flag);
                    LnkNext.NavigateUrl = string.Format("{0}{1}&ids={2}&flag={4}&field={5}&sortDirection={6}&filter={7}&previousIds={8}&SearchCriteria={3}", PageName, NextIdsList[NextIndex], DecryptedIds, SearchCriteriaObj, Flag, SortField, SortDirection, Filter, PreviousIds);
       
                // the list empty and no previous pages 
                if (PreviousIdsList != null && (Flag - 1) != 0)
                    //LnkPrevious.NavigateUrl = string.Format("{0}{1}&ids={2}&previousIds={3}&SearchCriteria={4}&flag={5}", PageName, PreviousIdsList[Paging.PageSize - 1], PreviousIds, PreviousIds, SearchCriteriaObj, (Flag - 1));
                    LnkPrevious.NavigateUrl = string.Format("{0}{1}&ids={2}&flag={4}&field={5}&sortDirection={6}&filter={7}&previousIds={8}&SearchCriteria={3}", PageName, PreviousIdsList[Paging.PageSize - 1], PreviousIds, SearchCriteriaObj, (Flag - 1), SortField, SortDirection, Filter, PreviousIds);
                    
            }
            // The last element in list
            else if (currentIndex == NextIdsList.Count - 1)
            {
                // Calling Next ids list(Next 10)

                List = GetList();
                NextIdsList = (from TEntity item in List.Skip(Flag * 10).Take(Paging.PageSize) select item.GetIdentity().ToString()).ToList();
                PreviousIds = DecryptedIds;
                StringBuilder nextIds = new StringBuilder();
                foreach (var item in NextIdsList)
                {
                    nextIds.Append(item); nextIds.Append(",");
                }
                PreviousIdsList = PreviousIds.TrimEnd(',').Split(',').ToList();
                // The list empty
                if (NextIdsList.Count != 0)
                    LnkNext.NavigateUrl = string.Format("{0}{1}&ids={2}&flag={4}&field={5}&sortDirection={6}&filter={7}&previousIds={8}&SearchCriteria={3}", PageName, NextIdsList[0], nextIds, SearchCriteriaObj, (Flag + 1), SortField, SortDirection, Filter, PreviousIds);
                LnkPrevious.NavigateUrl = string.Format("{0}{1}&ids={2}&flag={4}&field={5}&sortDirection={6}&filter={7}&previousIds={8}&SearchCriteria={3}", PageName, PreviousIdsList[PreviousIndex], DecryptedIds, SearchCriteriaObj, Flag, SortField, SortDirection, Filter, PreviousIds);
                   // LnkNext.NavigateUrl = string.Format("{0}{1}&ids={2}&previousIds={3}&SearchCriteria={4}&flag={5}", PageName, NextIdsList[0], nextIds, PreviousIds, SearchCriteriaObj, (Flag + 1));
                   //LnkPrevious.NavigateUrl = string.Format("{0}{1}&ids={2}&previousIds={3}&SearchCriteria={4}&flag={5}", PageName, PreviousIdsList[PreviousIndex], DecryptedIds, PreviousIds, SearchCriteriaObj, Flag);

            }
        }

        private List<TEntity> GetList()
        {
            Object criteria = null;
            if (string.IsNullOrEmpty(SearchCriteriaObj))
                SearchCriteriaObj = "{IsQualified:true}";

            criteria = (new JavaScriptSerializer()).Deserialize<TCriteria>(SearchCriteriaObj);

            ITManager manager = (ITManager)IoC.Instance.Resolve(typeof(ITManager));

            if (Filter == (int)Enums_S3.Filter.recentlyViewed)
            {
                MethodInfo method = manager.GetType().GetMethod("FindAll_RecentlyViewed");
                return (List<TEntity>)method.Invoke(manager, new Object[] { LoggedInUserID, ModuleID, criteria });

            }
            else
            {
                MethodInfo method = manager.GetType().GetMethod("FindAll");
                List<TEntity> temp = (List<TEntity>)method.Invoke(manager, new Object[] { criteria });
                if (SortField != "")
                    return SagePOS.Server.Configuration.URLs.SortEntity<TEntity>(temp, SortField, (Enums_S3.SortDirection)SortDirection);
                else
                    return temp;

            }
        }

        //fill previous next in ajax page
        public static void FillNavigationLists<TEntity>(int flag, ref string NextIds, ref string PreviousIds, List<TEntity> list) where TEntity : EntityBase<TEntity, int>
        {
            List<string> NextIdList = new List<string>();
            List<string> PreviousIdList = new List<string>();
            if (flag == 1)
            { NextIdList = (from TEntity item in list.Take(Paging.PageSize) select item.GetIdentity().ToString()).ToList(); }
            else
            {
                NextIdList = (from TEntity item in list.Skip((flag - 1) * 10).Take(Paging.PageSize) select item.GetIdentity().ToString()).ToList();
                PreviousIdList = (from TEntity item in list.Skip((flag - 2) * 10).Take(Paging.PageSize) select item.GetIdentity().ToString()).ToList();
            }
            StringBuilder nextIds = new StringBuilder();
            foreach (var item in NextIdList)
            {
                nextIds.Append(item);
                nextIds.Append(",");
            }
            StringBuilder previousids = new StringBuilder();
            foreach (var item in PreviousIdList)
            {
                previousids.Append(item);
                previousids.Append(",");
            }
            NextIds = nextIds.ToString();
            PreviousIds = previousids.ToString();

        }


       
    }


    public class NavigationCriteria
    {
        public string Field { get; set; }
        public Enums_S3.Filter Filter { get; set; }
        public Enums_S3.SortDirection SortDirection { get; set; }
        public string View { get; set; }
        public string Pn { get; set; }
        public string Flag { get; set; }

        //public Enums_S3.Configuration.Page Page { get; set; }
        public string ModuleKey { get; set; }
        public string PreviousIDs { get; set; }
        public string NextIDs { get; set; }
        public string SearchCriteria { get; set; }

      
    }

   
}