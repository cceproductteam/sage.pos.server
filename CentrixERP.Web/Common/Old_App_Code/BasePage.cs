using System;
using System.Threading;
using System.Globalization;
using Microsoft.Win32;
using CentrixERP.Common.Business.IManager;
using SagePOS.Manger.Common.Business.Entity;
using SF.Framework;
using SagePOS.Server.Configuration;
using System.Configuration;

namespace SagePOS.Common.Web
{
    public class BasePage : System.Web.UI.Page
    {
        public Enums_S3.Configuration.Language Lang { get; set; }
        public Enums_S3.Configuration.Language OppositeLang { get; set; }
        //public SSS.Business.Configuration config { get; set; }
        public static Configration config { get; set; }

        public string CompanyDelimeter { get { return GetGlobalResourceObject("Globalvariables", "CompanyText").ToString().ToDelimiter(); } set { CompanyDelimeter = value; } }
        public string PersonDelimeter { get { return GetGlobalResourceObject("Globalvariables", "PersonText").ToString().ToDelimiter(); } set { PersonDelimeter = value; } }

        public int LangNumber { get; set; }
        public int OppositeLangNumber { get; set; }
        public bool Direction { get; set; }
        public int IsClient = ConfigurationManager.AppSettings["isClient"].ToNumber();

        public static string ApplicationURL
        {
            get { return SagePOS.Common.Web.Configuration.Domain + SagePOS.Common.Web.Configuration.RootDirectory; }
            set { ApplicationURL = value; }
        }
        public BasePage()
        {
            SagePOS.Server.Configuration.Configuration.setLanguage(false);
        }

        protected override void InitializeCulture()
        {
            //IConfigrationManager myConfigMgr = (IConfigrationManager)IoC.Instance.Resolve(typeof(IConfigrationManager));
            //config = myConfigMgr.FindById(-1,null);
           // config = new SSS.Business.Configuration();
           // config.Get();
            if (string.IsNullOrEmpty(Cookies.Get("Lang")))
            {
                setEnglishLanguage();
                //switch (config.DefaultLanguage)
                //{
                //    case Enums_S3.Configuration.Language.en:
                //        setEnglishLanguage();
                //        break;
                //    case Enums_S3.Configuration.Language.ar:
                //        setArabicLanguage();
                //        break;
                //    default:
                //        setArabicLanguage();
                //        break;
                //}
            }
            else if (Cookies.Get("lang") == Enums_S3.Configuration.Language.en.ToString())
            {
                setEnglishLanguage();
            }
            else
            {
                setArabicLanguage();
            }
        }

        protected override void OnInit(EventArgs e)
        {

            //RegistryKey regKeyAppRoot = Registry.CurrentUser.OpenSubKey("Software").OpenSubKey("Microsoft").OpenSubKey("ASP.NET").OpenSubKey("S3");
            ////regKeyAppRoot.SetValue("lid", "cccc");
            //bool licensed;
            //if (regKeyAppRoot == null || regKeyAppRoot.GetValue("lid") == null) licensed = false;
            //else licensed = checkRegistryKeyValue(regKeyAppRoot.GetValue("lid").ToString());
            ////else int.TryParse(regKeyAppRoot.GetValue("lid").ToString(), out regValue);

            //if (!licensed)
            //{
            //    Response.Write("not licensed");
            //    Response.End();
            //}
        }

        private bool checkRegistryKeyValue(string value)
        {
            return true;
        }

        private void setEnglishLanguage()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");
            Lang = Enums_S3.Configuration.Language.en;
            LangNumber = (int)Enums_S3.Configuration.Language.en;
            OppositeLangNumber = (int)Enums_S3.Configuration.Language.ar;
            OppositeLang = Enums_S3.Configuration.Language.ar;
            Direction = false;
        }

        private void setArabicLanguage()
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("ar-SA");
            Lang = Enums_S3.Configuration.Language.ar;
            LangNumber = (int)Enums_S3.Configuration.Language.ar;
            OppositeLangNumber = (int)Enums_S3.Configuration.Language.en;
            OppositeLang = Enums_S3.Configuration.Language.en;
            Direction = true;
        }


    }
}