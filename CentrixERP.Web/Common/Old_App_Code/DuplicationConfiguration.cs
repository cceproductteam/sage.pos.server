using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SagePOS.Manger.Common.Business.Entity;
using SF.Framework;
using CentrixERP.Common.Business.IManager;
using System.Web.UI.WebControls;
using System.Web.UI;
using SFLib.CustomControls;
using SF.CustomScriptControls;
using System.Web.Script.Serialization;
using System.Web.UI.HtmlControls;
using SagePOS.Server.Configuration;

namespace SagePOS.Common.Web
{
    public class DuplicationConfiguration<P>
        where P : BasePageLoggedIn
    {
        private int ModuleId;
        private Repeater RepeaterName;
        private Enums_S3.Configuration.Language Lang;
        private P Page;
        int repeaterItemsIndex = 0;

        private List<DuplicationRule> duplicationRuleList;
        public List<DuplicationRuleLite> DuplicationRuleLiteList = new List<DuplicationRuleLite>();

        public DuplicationConfiguration(int moduleId, Repeater repeaterName, Enums_S3.Configuration.Language lang, P page)
        {
            ModuleId = moduleId; RepeaterName = repeaterName; Lang = lang; Page = page;
        }

        public void GetDuplicationRule()
        {
            int moduleId = ModuleId;
            Repeater repeaterName = RepeaterName;
            DuplicationRuleCriteria myCriteria = new DuplicationRuleCriteria();
            myCriteria.ModuleID = moduleId;
            IDuplicationRuleManager duplicationRuleManager = (IDuplicationRuleManager)IoC.Instance.Resolve(typeof(IDuplicationRuleManager));
            duplicationRuleList = duplicationRuleManager.FindAll(myCriteria);

            if (duplicationRuleList != null)
            {
                duplicationRuleList = (from DuplicationRule D in duplicationRuleList
                                       where D.FilterTypeId != 4
                                       select D).ToList();
                if (duplicationRuleList != null && duplicationRuleList.Count != 0)
                    repeaterName.DataSource = duplicationRuleList;
                else
                    repeaterName.DataSource = new List<DuplicationRule>();
            }
            else
            {
                repeaterName.DataSource = new List<DuplicationRule>();
            }
            repeaterName.DataBind();
        }

        private Control AddControl(int type, string id, string text, int parentId, string serviceName, string serviceMethod, ref string clientId)
        {
            switch (type)
            {
                case (int)Enums_S3.ControlType.Text:
                    return GetTextBox(id, ref clientId);
                case (int)Enums_S3.ControlType.DDL:
                    return GetDDL(id, parentId, ref clientId);
                case (int)Enums_S3.ControlType.AC:
                    return GetAutoComplete(id, serviceName, serviceMethod, ref clientId);
                case (int)Enums_S3.ControlType.Label:
                    return GetLabel(id, text);
                case (int)Enums_S3.ControlType.DatePicker:
                    return GetDatePicker(id, ref clientId);
                case (int)Enums_S3.ControlType.RadioButton:
                    //RadioButtonList radioButtonList = new RadioButtonList();
                    //radioButtonList.Controls.Add(GetRadioButton(id, text));
                    return GetRadioButton(id,text);
                default:
                    return new Control();
            }
        }

        private TextBox GetDatePicker(string id, ref string clientId)
        {
            TextBox textBox = new TextBox();
            textBox.ID = "txt_" + id;
            textBox.CssClass = "txt";
            textBox.Style.Add("height", "20px");
            textBox.Style.Add("width", "84%");
            return textBox;
        }
        private Label GetLabel(string id, string text)
        {
            Label label = new Label();
            label.ID = "lbl_" + id;
            label.Text = text;
            return label;
        }

        private CustomTextBox GetTextBox(string id, ref string clientId)
        {
            CustomTextBox textBox = new CustomTextBox();
            textBox.ID = "txt_" + id;
            clientId = textBox.ClientID;
            return textBox;
        }

        private AutoComplete GetAutoComplete(string id, string serviceName, string serviceMethod, ref string clientId)
        {
            AutoComplete autoComplete = new AutoComplete();
            autoComplete.ID = "AC_" + id;
            autoComplete.ResultTemplate = "<div>{label}</div>";
            autoComplete.Width = 170;
            autoComplete.Height = 20;
            autoComplete.ServiceName = serviceName;
            autoComplete.ServiceMethod = serviceMethod;
            if (Lang == Enums_S3.Configuration.Language.en)
                autoComplete.Args = "en";
            else
                autoComplete.Args = "ar";
            //clientId = autoComplete.ClientID;
            return autoComplete;
        }

        private AutoComplete GetDDL(string id, int parentId, ref string clientId)
        {
            AutoComplete DDLAC = new AutoComplete();
            DDLAC.ID = "DDLAC_" + id;
            DDLAC.ResultTemplate = "<div>{label}</div>";
            DDLAC.Width = 170;
            DDLAC.Height = 20;
            DDLAC.EmptyText = "No data found.";
            // clientId = DDLAC.ClientID;
            IDataTypeContentManager datatypeContentmgr = (IDataTypeContentManager)IoC.Instance.Resolve(typeof(IDataTypeContentManager));
            List<DataTypeContent> DDLList = datatypeContentmgr.FindByParentId<int>(parentId, typeof(DataType), null);
            DDLAC.IsRequired = false;
            DDLAC.IsRequiredMsg = "";
            SF.CustomScriptControls.AutoCompleteItem item = new SF.CustomScriptControls.AutoCompleteItem();
            DDLAC.SuggestedItems = (from DataTypeContent data in DDLList select new AutoCompleteItem { label = (Lang == Enums_S3.Configuration.Language.en) ? data.DataTypeContentEN : data.DataTypeContentAR, value = data.DataTypeContentID }).ToList();
            return DDLAC;
        }

        private RadioButton GetRadioButton(string id,string label)
        {
            RadioButton radioButton = new RadioButton();
            radioButton.ID = "chk_" + id;
            radioButton.Text = label;
            return radioButton;
 
        }

        public void rptCheckDuplicationRule_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
           if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (duplicationRuleList.Count > repeaterItemsIndex)
                {
                    DuplicationRule duplicationRule1 = duplicationRuleList[repeaterItemsIndex];
                    DuplicationRuleLite duplicationRuleLite1 = new DuplicationRuleLite();
                    if (duplicationRule1.FilterTypeId != 6)
                    {
                        Label lblField_1 = (Label)e.Item.FindControl("lblField_1");
                        Panel pnlField_1 = (Panel)e.Item.FindControl("pnlField_1");
                        lblField_1.Text =(Lang==Enums_S3.Configuration.Language.en)?duplicationRule1.UiNameEn:duplicationRule1.UiNameAr;
                        string ctr1ClientID = null;
                        Control control = AddControl(duplicationRule1.ControlType, duplicationRule1.Id.ToString(), duplicationRule1.UiNameEn, (int)duplicationRule1.PropertyParentId, duplicationRule1.ServiceName, duplicationRule1.ServiceMethod, ref ctr1ClientID);
                        pnlField_1.Controls.Add(control);
                        if (string.IsNullOrEmpty(ctr1ClientID))
                            ctr1ClientID = control.ClientID;

                        duplicationRuleLite1.ControlID = ctr1ClientID;
                        duplicationRuleLite1.ControlType = duplicationRule1.ControlType;
                        duplicationRuleLite1.DuplicationID = duplicationRule1.Id;
                        duplicationRuleLite1.MapToContolID = this.Page.GetControlClientID(duplicationRule1.MapToControlId);
                        duplicationRuleLite1.DDLParentID = duplicationRule1.PropertyParentId;
                        DuplicationRuleLiteList.Add(duplicationRuleLite1);
                    }

                    repeaterItemsIndex++;

                    if (duplicationRuleList.Count > repeaterItemsIndex)
                    {
                        DuplicationRule duplicationRule2 = duplicationRuleList[repeaterItemsIndex];
                        DuplicationRuleLite duplicationRuleLite2 = new DuplicationRuleLite();
                        if (duplicationRule2.FilterTypeId != 6)
                        {
                            Label lblField_2 = (Label)e.Item.FindControl("lblField_2");
                            Panel pnlField_2 = (Panel)e.Item.FindControl("pnlField_2");
                            lblField_2.Text = (Lang == Enums_S3.Configuration.Language.en) ? duplicationRule2.UiNameEn : duplicationRule2.UiNameAr;
                            string ctr2ClientID = null;
                            Control control = AddControl(duplicationRule2.ControlType, duplicationRule2.Id.ToString(), duplicationRule2.UiNameEn, (int)duplicationRule2.PropertyParentId, duplicationRule2.ServiceName, duplicationRule2.ServiceMethod, ref ctr2ClientID);
                            pnlField_2.Controls.Add(control);
                            if (string.IsNullOrEmpty(ctr2ClientID))
                                ctr2ClientID = control.ClientID;

                            duplicationRuleLite2.ControlID = ctr2ClientID;
                            duplicationRuleLite2.ControlType = duplicationRule2.ControlType;
                            duplicationRuleLite2.DuplicationID = duplicationRule2.Id;
                            duplicationRuleLite2.MapToContolID = this.Page.GetControlClientID(duplicationRule2.MapToControlId);
                            duplicationRuleLite2.DDLParentID = duplicationRule2.PropertyParentId;
                            DuplicationRuleLiteList.Add(duplicationRuleLite2);
                        }
                        repeaterItemsIndex++;

                    }
                }
                else
                    return;
            }
        }

        public string Serialize()
        {
            if (DuplicationRuleLiteList == null || DuplicationRuleLiteList.Count == 0)
                return null;
            else
            {
                JavaScriptSerializer ser = new JavaScriptSerializer();
                return ser.Serialize(DuplicationRuleLiteList);
            }
        }
    }
}
