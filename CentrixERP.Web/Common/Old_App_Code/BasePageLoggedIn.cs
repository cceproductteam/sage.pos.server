using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Threading;
using System.Globalization;
//using SSS.Business;
//using SFLib;
using System.Collections.Generic;
using Centrix.UM.Business.Entity;
using Centrix.UM.Business.IManager;

using SagePOS.Server.Business.Entity;
using SF.Framework;
using SagePOS.Server.Configuration;


namespace SagePOS.Common.Web
{
    public class BasePageLoggedIn : BasePage
    {
        //public Users.User LoggedInUser = new Users.User();
        public User LoggedInUser = null;

        private static bool ForceCreateNewSession = false;

        private Enums_S3.Permissions.SystemModules PagePermission;

        public static string ApplicationURL
        {
            get { return SagePOS.Common.Web.Configuration.Domain + SagePOS.Common.Web.Configuration.RootDirectory + System.Configuration.ConfigurationManager.AppSettings["Application"].ToString(); }
            set { ApplicationURL = value; }
        }

        public static string ApplicationURL_Common
        {
            get { return SagePOS.Common.Web.Configuration.Domain + SagePOS.Common.Web.Configuration.RootDirectory; }
            set { ApplicationURL_Common = value; }
        }

        public static string Centrix_Version
        {
            get
            {
                return ConfigurationManager.AppSettings["CentrixVersion"].ToString();
            }
        }
        public static string ERPApplicationURL {
            get { return SagePOS.Common.Web.Configuration.Domain + System.Configuration.ConfigurationManager.AppSettings["ERPApplicationURL"].ToString(); }
            set { ERPApplicationURL = value; }
        }
      

        

        private static bool preventPhysicalSave ;

        public static bool PreventPhysicalSave
        {
            get { return preventPhysicalSave; }
        }

        protected BasePageLoggedIn(Enums_S3.Permissions.SystemModules pagePermission)
        {
            ForceCreateNewSession = true;
            //IUserManager myUserManager = (IUserManager)IoC.Instance.Resolve(typeof(IUserManager));
            //if (!myUserManager.IsLoggedIn())
            //{
            //    myUserManager.Logout();
            //    HttpContext.Current.Response.Redirect("~/Login.aspx");
            //}
            //else
            //{
            //    LoggedInUser = myUserManager.FindLoggedInUser(Cookies.Get("uid").ToNumber());
            //    if (LoggedInUser == null)
            //    {
            //        myUserManager.Logout();
            //        HttpContext.Current.Response.Redirect("~/Login.aspx");
            //    }
            //}

            preventPhysicalSave = Convert.ToBoolean(ConfigurationManager.AppSettings["PreventPhysicalSave"]);
            PagePermission = pagePermission;
        }

        protected BasePageLoggedIn(Enums_S3.Permissions.SystemModules pagePermission, bool forceCreateNewSession)
        {
            ForceCreateNewSession = forceCreateNewSession;
            preventPhysicalSave = Convert.ToBoolean(ConfigurationManager.AppSettings["PreventPhysicalSave"]);
            PagePermission = pagePermission;
        }

        protected override void OnLoad(EventArgs e)
        {
            LoggedInUser = getLoggedInUser();
            if (LoggedInUser == null)
            {
                int sessionexpired = Request.QueryString["sexp"].ToNumber();
                if (sessionexpired == 1)
                    Response.Redirect(URLs.GetURL(Enums_S3.Configuration.Module.Common, URLEnums.Action.Extended.Login, null));
                else
                    Response.Redirect(URLs.GetURL(Enums_S3.Configuration.Module.Common, URLEnums.Action.Extended.Login, null));
                    //Configuration.PageURL.Login());
            }
            //CheckPermissons(PagePermission);
            base.OnLoad(e);
        }

        public static User getLoggedInUser()
        {
            IUserLogginManager userLogginManager = IoC.Instance.Resolve<IUserLogginManager>();
            userLogginManager.ForceCreateNewSession = ForceCreateNewSession;
            UserLogginCriteria criteria = new UserLogginCriteria()
            {
                UserAgent = HttpContext.Current.Request.UserAgent,
                PublicIP = HttpContext.Current.Request.UserHostName,
                LocalIP = HttpContext.Current.Request.UserHostAddress
            };
            if (userLogginManager.ValidateLogginSession(criteria) != SagePOS.Server.Configuration.Enums_S3.UserLoggin.UserLogginStatus.LoggedIn)

            {
            
                //SF.Framework.Cookies.Abandon("cxuser");
                //userLogginManager.KillUserSession(criteria);
                return null;
            }
            else
            {
                return userLogginManager.CurrentLoggedUser;
            }

            //IUserManager myUserManager = (IUserManager)IoC.Instance.Resolve(typeof(IUserManager));
            //User myUser = null;
            //if (!myUserManager.IsLoggedIn())
            //{
            //    myUserManager.Logout();
            //}
            //else
            //{
            //    myUser = myUserManager.FindLoggedInUser(Cookies.Get("uid").ToNumber());
            //    if (myUser == null)
            //    {
            //        myUserManager.Logout();
            //    }
            //}
            //return myUser;
        }

        public void CheckPermissons(Enums_S3.Permissions.SystemModules PagePermission)
        {
            return;
            //bool permitted = SSS.Business.Permissions.SystemModules.HasPermission(PagePermission, LoggedInUser);
            IEnumerable<bool> permitted = null;

            if (!LoggedInUser.IsAdmin && PagePermission != Enums_S3.Permissions.SystemModules.None)
            {
                if (LoggedInUser.MyRole != null && LoggedInUser.MyRole.MyRolePermission != null && LoggedInUser.MyRole.MyRolePermission.Count > 0)
                {
                    permitted = from RolePermission rolePer in LoggedInUser.MyRole.MyRolePermission where rolePer.Permissionid == Convert.ToInt32(PagePermission) select true;
                }

                if (permitted == null || (permitted != null && permitted.Count() == 0))
                {
                    HttpContext.Current.Response.Write("Access denied!");
                    HttpContext.Current.Response.End();
                }
            }
        }

        public void masterVariables()
        {
            //IUserManager myUserManager = (IUserManager)IoC.Instance.Resolve(typeof(IUserManager));
            //Label mpLabel = (Label)Master.FindControl("lblUserName");
            //mpLabel.Text = (myUserManager.IsLoggedIn()) ? LoggedInUser.UserName : "Guest";
        }


        public virtual string GetControlClientID(string id)
        {
            if (this.Master.FindControl("PageBody").Controls[1] != null && this.Master.FindControl("PageBody").Controls[1].FindControl(id)!=null)
            {
                Control control = this.Master.FindControl("PageBody").Controls[1].FindControl(id);
                if (control.GetType() == typeof(SFLib.CustomControls.CustomTextBox))
                {
                    SFLib.CustomControls.CustomTextBox txtBox = (SFLib.CustomControls.CustomTextBox)control;
                    return txtBox.ClientID;
                }
                else
                {
                    return this.Master.FindControl("PageBody").Controls[1].FindControl(id).ClientID;
                }
            }
            else
                return null;
        }

    }
}