jQuery.create = function() {

    if (arguments.length == 0) return [];
    var args = arguments[0] || {}, elem = null, elements = null;
    var siblings = null;

    // In case someone passes in a null object,
    // assume that they want an empty string.
    if (args == null) args = "";
    if (args.constructor == String) {
        if (arguments.length > 1) {
            var attributes = arguments[1];
            if (attributes.constructor == String) {
                elem = document.createTextNode(args);
                elements = [];
                elements.push(elem);
                siblings =
        jQuery.create.apply(null, Array.prototype.slice.call(arguments, 1));
                elements = elements.concat(siblings);
                return elements;

            } else {
                elem = document.createElement(args);
                // Set element attributes.
                var attributes = arguments[1];
                for (var attr in attributes)
                    jQuery(elem).attr(attr, attributes[attr]);

                // Add children of this element.
                var children = arguments[2];
                if (children != undefined) {
                    children = jQuery.create.apply(null, children);
                }
                jQuery(elem).append(children);

                // If there are more siblings, render those too.
                if (arguments.length > 3) {
                    siblings =
        jQuery.create.apply(null, Array.prototype.slice.call(arguments, 3));
                    return [elem].concat(siblings);
                }
                return elem;
            }
        } else return document.createTextNode(args);
    } else {
        elements = [];
        elements.push(args);
        siblings =
        jQuery.create.apply(null, (Array.prototype.slice.call(arguments, 1)));
        elements = elements.concat(siblings);
        return elements;
    }
};


/**
* jQuery sound plugin (no flash)
*
* port of script.aculo.us' sound.js (http://script.aculo.us), based on code by Jules Gravinese (http://www.webveteran.com/)
*
* Copyright (c) 2007 J�rn Zaefferer (http://bassistance.de)
*
* Licensed under the MIT license:
* http://www.opensource.org/licenses/mit-license.php
*
* $Id: jquery.sound.js 5854 2008-10-04 10:22:25Z joern.zaefferer $
*/
/**
* API Documentation
*
* // play a sound from the url
* $.sound.play(url)
*
* // play a sound from the url, on a track, stopping any sound already running on that track
* $.sound.play(url, {
* track: "track1"
* });
*
* // increase the timeout to four seconds before removing the sound object from the dom for longer sounds
* $.sound.play(url, {
* timeout: 4000
* });
*
* // stop a sound by removing the element returned by play
* var sound = $.sound.play(url);
* sound.remove();
*
* // disable playing sounds
* $.sound.enabled = false;
*
* // enable playing sounds
* $.sound.enabled = true
*/
(function($) {
    $.sound = {
        tracks: {},
        enabled: true,
        template: function(src) {
            // todo: move bgsound element and browser sniffing in here
            // todo: test wmv on windows: Builder.node('embed', {type:'application/x-mplayer2', pluginspage:'http://microsoft.com/windows/mediaplayer/en/download/', id:'mediaPlayer', name:'mediaPlayer', displaysize:'4', autosize:'-1', bgcolor:'darkblue', showcontrols:'false', showtracker:'-1', showdisplay:'0', showstatusbar:'-1', videoborder3d:'-1', width:'0', height:'0', src:audioFile, autostart:'true', designtimesp:'5311', loop:'false'});
            // is_win = (agt.indexOf("windows") != -1);
            return '<embed style="height:0" loop="false" src="' + src + '" autostart="true" hidden="true"/>';
        },
        play: function(url, options) {
            if (!this.enabled)
                return;
            options = $.extend({
                url: url,
                timeout: 2000
            }, options);
            if (options.track) {
                if (this.tracks[options.track]) {
                    var current = this.tracks[options.track];
                    // TODO check when Stop is avaiable, certainly not on a jQuery object
                    current[0].Stop && current[0].Stop();
                    current.remove();
                }
            }
            var element = $.browser.msie
? $('<bgsound/>').attr({
    src: options.url,
    loop: 1,
    autostart: true
})
: $(this.template(options.url));
            element.appendTo("body");
            if (options.track) {
                this.tracks[options.track] = element;
            }
            setTimeout(function() {
                element.remove();
            }, options.timeout);
            return element;
        }
    };
})(jQuery);




/*!!
* Title Alert 0.7
*
* Copyright (c) 2009 ESN | http://esn.me
* Jonatan Heyman | http://heyman.info
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*/

/*
* @name jQuery.titleAlert
* @projectDescription Show alert message in the browser title bar
* @author Jonatan Heyman | http://heyman.info
* @version 0.7.0
* @license MIT License
*
* @id jQuery.titleAlert
* @param {String} text The text that should be flashed in the browser title
* @param {Object} settings Optional set of settings.
* @option {Number} interval The flashing interval in milliseconds (default: 500).
* @option {Number} originalTitleInterval Time in milliseconds that the original title is diplayed for. If null the time is the same as interval (default: null).
* @option {Number} duration The total lenght of the flashing before it is automatically stopped. Zero means infinite (default: 0).
* @option {Boolean} stopOnFocus If true, the flashing will stop when the window gets focus (default: true).
* @option {Boolean} stopOnMouseMove If true, the flashing will stop when the browser window recieves a mousemove event. (default:false).
* @option {Boolean} requireBlur Experimental. If true, the call will be ignored unless the window is out of focus (default: false).
* Known issues: Firefox doesn't recognize tab switching as blur, and there are some minor IE problems as well.
*
* @example $.titleAlert("Hello World!", {requireBlur:true, stopOnFocus:true, duration:10000, interval:500});
* @desc Flash title bar with text "Hello World!", if the window doesn't have focus, for 10 seconds or until window gets focused, with an interval of 500ms
*/
(function($) {
    $.titleAlert = function(text, settings) {
        // check if it currently flashing something, if so reset it
        if ($.titleAlert._running)
            $.titleAlert.stop();

        // override default settings with specified settings
        $.titleAlert._settings = settings = $.extend({}, $.titleAlert.defaults, settings);

        // if it's required that the window doesn't have focus, and it has, just return
        if (settings.requireBlur && $.titleAlert.hasFocus)
            return;

        // originalTitleInterval defaults to interval if not set
        settings.originalTitleInterval = settings.originalTitleInterval || settings.interval;

        $.titleAlert._running = true;
        $.titleAlert._initialText = document.title;
        document.title = text;
        var showingAlertTitle = true;
        var switchTitle = function() {
            // WTF! Sometimes Internet Explorer 6 calls the interval function an extra time!
            if (!$.titleAlert._running)
                return;

            showingAlertTitle = !showingAlertTitle;
            document.title = (showingAlertTitle ? text : $.titleAlert._initialText);
            $.titleAlert._intervalToken = setTimeout(switchTitle, (showingAlertTitle ? settings.interval : settings.originalTitleInterval));
        };
        $.titleAlert._intervalToken = setTimeout(switchTitle, settings.interval);

        if (settings.stopOnMouseMove) {
            $(document).mousemove(function(event) {
                $(this).unbind(event);
                $.titleAlert.stop();
            });
        }

        // check if a duration is specified
        if (settings.duration > 0) {
            $.titleAlert._timeoutToken = setTimeout(function() {
                $.titleAlert.stop();
            }, settings.duration);
        }
    };

    // default settings
    $.titleAlert.defaults = {
        interval: 500,
        originalTitleInterval: null,
        duration: 0,
        stopOnFocus: true,
        requireBlur: false,
        stopOnMouseMove: false
    };

    // stop current title flash
    $.titleAlert.stop = function() {
        clearTimeout($.titleAlert._intervalToken);
        clearTimeout($.titleAlert._timeoutToken);
        document.title = $.titleAlert._initialText;

        $.titleAlert._timeoutToken = null;
        $.titleAlert._intervalToken = null;
        $.titleAlert._initialText = null;
        $.titleAlert._running = false;
        $.titleAlert._settings = null;
    };

    $.titleAlert.hasFocus = true;
    $.titleAlert._running = false;
    $.titleAlert._intervalToken = null;
    $.titleAlert._timeoutToken = null;
    $.titleAlert._initialText = null;
    $.titleAlert._settings = null;


    $.titleAlert._focus = function() {
        $.titleAlert.hasFocus = true;

        if ($.titleAlert._running && $.titleAlert._settings.stopOnFocus) {
            var initialText = $.titleAlert._initialText;
            $.titleAlert.stop();

            // ugly hack because of a bug in Chrome which causes a change of document.title immediately after tab switch
            // to have no effect on the browser title
            setTimeout(function() {
                if ($.titleAlert._running)
                    return;
                document.title = ".";
                document.title = initialText;
            }, 1000);
        }
    };
    $.titleAlert._blur = function() {
        $.titleAlert.hasFocus = false;
    };

    // bind focus and blur event handlers
    $(window).bind("focus", $.titleAlert._focus);
    $(window).bind("blur", $.titleAlert._blur);
})(jQuery);



/*
VERSION: Drop Shadow jQuery Plugin 1.6  12-13-2007

REQUIRES: jquery.js (verified for 1.3.2)

SYNTAX: $(selector).dropShadow(options);  // Creates new drop shadows
$(selector).redrawShadow();       // Redraws shadows on elements
$(selector).removeShadow();       // Removes shadows from elements
$(selector).shadowId();           // Returns an existing shadow's ID

OPTIONS:

left    : integer (default = 4)
top     : integer (default = 4)
blur    : integer (default = 2)
opacity : decimal (default = 0.5)
color   : string (default = "black")
swap    : boolean (default = false)

The left and top parameters specify the distance and direction, in  pixels, to
offset the shadow. Zero values position the shadow directly behind the element.
Positive values shift the shadow to the right and down, while negative values 
shift the shadow to the left and up.
  
The blur parameter specifies the spread, or dispersion, of the shadow. Zero 
produces a sharp shadow, one or two produces a normal shadow, and three or four
produces a softer shadow. Higher values increase the processing load.
  
The opacity parameter should be a decimal value, usually less than one. You can
use a value higher than one in special situations, e.g. with extreme blurring. 
  
Color is specified in the usual manner, with a color name or hex value. The
color parameter does not apply with transparent images.
  
The swap parameter reverses the stacking order of the original and the shadow.
This can be used for special effects, like an embossed or engraved look.

EXPLANATION:
  
This jQuery plug-in adds soft drop shadows behind page elements. It is only
intended for adding a few drop shadows to mostly stationary objects, like a
page heading, a photo, or content containers.

The shadows it creates are not bound to the original elements, so they won't
move or change size automatically if the original elements change. A window
resize event listener is assigned, which should re-align the shadows in many
cases, but if the elements otherwise move or resize you will have to handle
those events manually. Shadows can be redrawn with the redrawShadow() method
or removed with the removeShadow() method. The redrawShadow() method uses the
same options used to create the original shadow. If you want to change the
options, you should remove the shadow first and then create a new shadow.
  
The dropShadow method returns a jQuery collection of the new shadow(s). If
further manipulation is required, you can store it in a variable like this:

var myShadow = $("#myElement").dropShadow();

You can also read the ID of the shadow from the original element at a later
time. To get a shadow's ID, either read the shadowId attribute of the
original element or call the shadowId() method. For example:

var myShadowId = $("#myElement").attr("shadowId");  or
var myShadowId = $("#myElement").shadowId();

If the original element does not already have an ID assigned, a random ID will
be generated for the shadow. However, if the original does have an ID, the 
shadow's ID will be the original ID and "_dropShadow". For example, if the
element's ID is "myElement", the shadow's ID would be "myElement_dropShadow".

If you have a long piece of text and the user resizes the window so that the
text wraps or unwraps, the shape of the text changes and the words are no
longer in the same positions. In that case, you can either preset the height
and width, so that it becomes a fixed box, or you can shadow each word
separately, like this:

<h1><span>Your</span> <span>Page</span> <span>Title</span></h1>

$("h1 span").dropShadow();

The dropShadow method attempts to determine whether the selected elements have
transparent backgrounds. If you want to shadow the content inside an element,
like text or a transparent image, it must not have a background-color or
background-image style. If the element has a solid background it will create a
rectangular shadow around the outside box.

The shadow elements are positioned absolutely one layer below the original 
element, which is positioned relatively (unless it's already absolute).

*** All shadows have the "dropShadow" class, for selecting with CSS or jQuery.

ISSUES:
  
1)  Limited styling of shadowed elements by ID. Because IDs must be unique,
and the shadows have their own ID, styles applied by ID won't transfer
to the shadows. Instead, style elements by class or use inline styles.
2)  Sometimes shadows don't align properly. Elements may need to be wrapped
in container elements, margins or floats changed, etc. or you may just 
have to tweak the left and top offsets to get them to align. For example,
with draggable objects, you have to wrap them inside two divs. Make the 
outer div draggable and set the inner div's position to relative. Then 
you can create a shadow on the element inside the inner div.
3)  If the user changes font sizes it will throw the shadows off. Browsers 
do not expose an event for font size changes. The only known way to 
detect a user font size change is to embed an invisible text element and
then continuously poll for changes in size.
4)  Safari support is shaky, and may require even more tweaks/wrappers, etc.
    
The bottom line is that this is a gimick effect, not PFM, and if you push it
too hard or expect it to work in every possible situation on every browser,
you will be disappointed. Use it sparingly, and don't use it for anything 
critical. Otherwise, have fun with it!
        
AUTHOR: Larry Stevens.  This work is in the public domain,
and is not supported in any way. Use it at your own risk.
*/


(function($) {

    var dropShadowZindex = 1;  //z-index counter

    $.fn.dropShadow = function(options) {
        // Default options
        var opt = $.extend({
            left: 4,
            top: 4,
            blur: 2,
            opacity: .5,
            color: "black",
            swap: false
        }, options);
        var jShadows = $([]);  //empty jQuery collection

        // Loop through original elements
        this.not(".dropShadow").each(function() {
            var jthis = $(this);
            var shadows = [];
            var blur = (opt.blur <= 0) ? 0 : opt.blur;
            var opacity = (blur == 0) ? opt.opacity : opt.opacity / (blur * 8);
            var zOriginal = (opt.swap) ? dropShadowZindex : dropShadowZindex + 1;
            var zShadow = (opt.swap) ? dropShadowZindex + 1 : dropShadowZindex;

            // Create ID for shadow
            var shadowId;
            if (this.id) {
                shadowId = this.id + "_dropShadow";
            }
            else {
                shadowId = "ds" + (1 + Math.floor(9999 * Math.random()));
            }

            // Modify original element
            $.data(this, "shadowId", shadowId); //store id in expando
            $.data(this, "shadowOptions", options); //store options in expando
            jthis
        .attr("shadowId", shadowId)
        .css("zIndex", zOriginal);
            if (jthis.css("position") != "absolute") {
                jthis.css({
                    position: "relative",
                    zoom: 1 //for IE layout
                });
            }

            // Create first shadow layer
            bgColor = jthis.css("backgroundColor");
            if (bgColor == "rgba(0, 0, 0, 0)") bgColor = "transparent";  //Safari
            if (bgColor != "transparent" || jthis.css("backgroundImage") != "none"
          || this.nodeName == "SELECT"
          || this.nodeName == "INPUT"
          || this.nodeName == "TEXTAREA") {
                shadows[0] = $("<div></div>")
          .css("background", opt.color);
            }
            else {
                shadows[0] = jthis
          .clone()
          .removeAttr("id")
          .removeAttr("name")
          .removeAttr("shadowId")
          .css("color", opt.color);
            }
            shadows[0]
        .addClass("dropShadow")
        .css({
            height: jthis.outerHeight(),
            left: blur,
            opacity: opacity,
            position: "absolute",
            top: blur,
            width: jthis.outerWidth(),
            zIndex: zShadow
        });

            // Create other shadow layers
            var layers = (8 * blur) + 1;
            for (i = 1; i < layers; i++) {
                shadows[i] = shadows[0].clone();
            }

            // Position layers
            var i = 1;
            var j = blur;
            while (j > 0) {
                shadows[i].css({ left: j * 2, top: 0 });           //top
                shadows[i + 1].css({ left: j * 4, top: j * 2 });   //right
                shadows[i + 2].css({ left: j * 2, top: j * 4 });   //bottom
                shadows[i + 3].css({ left: 0, top: j * 2 });       //left
                shadows[i + 4].css({ left: j * 3, top: j });       //top-right
                shadows[i + 5].css({ left: j * 3, top: j * 3 });   //bottom-right
                shadows[i + 6].css({ left: j, top: j * 3 });       //bottom-left
                shadows[i + 7].css({ left: j, top: j });           //top-left
                i += 8;
                j--;
            }

            // Create container
            var divShadow = $("<div></div>")
        .attr("id", shadowId)
        .addClass("dropShadow")
        .css({
            left: jthis.position().left + opt.left - blur,
            marginTop: jthis.css("marginTop"),
            marginRight: jthis.css("marginRight"),
            marginBottom: jthis.css("marginBottom"),
            marginLeft: jthis.css("marginLeft"),
            position: "absolute",
            top: jthis.position().top + opt.top - blur,
            zIndex: zShadow
        });

            // Add layers to container  
            for (i = 0; i < layers; i++) {
                divShadow.append(shadows[i]);
            }

            // Add container to DOM
            jthis.after(divShadow);

            // Add shadow to return set
            jShadows = jShadows.add(divShadow);

            // Re-align shadow on window resize
            $(window).resize(function() {
                try {
                    divShadow.css({
                        left: jthis.position().left + opt.left - blur,
                        top: jthis.position().top + opt.top - blur
                    });
                }
                catch (e) { }
            });

            // Increment z-index counter
            dropShadowZindex += 2;

        });  //end each

        return this.pushStack(jShadows);
    };


    $.fn.redrawShadow = function() {
        // Remove existing shadows
        this.removeShadow();

        // Draw new shadows
        return this.each(function() {
            var shadowOptions = $.data(this, "shadowOptions");
            $(this).dropShadow(shadowOptions);
        });
    };


    $.fn.removeShadow = function() {
        return this.each(function() {
            var shadowId = $(this).shadowId();
            $("div#" + shadowId).remove();
        });
    };


    $.fn.shadowId = function() {
        return $.data(this[0], "shadowId");
    };


    $(function() {
        // Suppress printing of shadows
        var noPrint = "<style type='text/css' media='print'>";
        noPrint += ".dropShadow{visibility:hidden;}</style>";
        $("head").append(noPrint);
    });

})(jQuery);

(function($) {
    $.fn.setValue = function(value1) {
        $(this).data('selectedValue', value1);
    };
})(jQuery);

/**
* Cookie plugin
*
* Copyright (c) 2006 Klaus Hartl (stilbuero.de)
* Dual licensed under the MIT and GPL licenses:
* http://www.opensource.org/licenses/mit-license.php
* http://www.gnu.org/licenses/gpl.html
*
*/

/**
* Create a cookie with the given name and value and other optional parameters.
*
* @example $.cookie('the_cookie', 'the_value');
* @desc Set the value of a cookie.
* @example $.cookie('the_cookie', 'the_value', { expires: 7, path: '/', domain: 'jquery.com', secure: true });
* @desc Create a cookie with all available options.
* @example $.cookie('the_cookie', 'the_value');
* @desc Create a session cookie.
* @example $.cookie('the_cookie', null);
* @desc Delete a cookie by passing null as value. Keep in mind that you have to use the same path and domain
*       used when the cookie was set.
*
* @param String name The name of the cookie.
* @param String value The value of the cookie.
* @param Object options An object literal containing key/value pairs to provide optional cookie attributes.
* @option Number|Date expires Either an integer specifying the expiration date from now on in days or a Date object.
*                             If a negative value is specified (e.g. a date in the past), the cookie will be deleted.
*                             If set to null or omitted, the cookie will be a session cookie and will not be retained
*                             when the the browser exits.
* @option String path The value of the path atribute of the cookie (default: path of page that created the cookie).
* @option String domain The value of the domain attribute of the cookie (default: domain of page that created the cookie).
* @option Boolean secure If true, the secure attribute of the cookie will be set and the cookie transmission will
*                        require a secure protocol (like HTTPS).
* @type undefined
*
* @name $.cookie
* @cat Plugins/Cookie
* @author Klaus Hartl/klaus.hartl@stilbuero.de
*/

/**
* Get the value of a cookie with the given name.
*
* @example $.cookie('the_cookie');
* @desc Get the value of a cookie.
*
* @param String name The name of the cookie.
* @return The value of the cookie.
* @type String
*
* @name $.cookie
* @cat Plugins/Cookie
* @author Klaus Hartl/klaus.hartl@stilbuero.de
*/
jQuery.cookie = function(name, value, options) {
    if (typeof value != 'undefined') { // name and value given, set cookie
        options = options || {};
        if (value === null) {
            value = '';
            options.expires = -1;
        }
        var expires = '';
        if (options.expires && (typeof options.expires == 'number' || options.expires.toUTCString)) {
            var date;
            if (typeof options.expires == 'number') {
                date = new Date();
                date.setTime(date.getTime() + (options.expires * 24 * 60 * 60 * 1000));
            } else {
                date = options.expires;
            }
            expires = '; expires=' + date.toUTCString(); // use expires attribute, max-age is not supported by IE
        }
        // CAUTION: Needed to parenthesize options.path and options.domain
        // in the following expressions, otherwise they evaluate to undefined
        // in the packed version for some reason...
        var path = options.path ? '; path=' + (options.path) : '';
        var domain = options.domain ? '; domain=' + (options.domain) : '';
        var secure = options.secure ? '; secure' : '';
        document.cookie = [name, '=', encodeURIComponent(value), expires, path, domain, secure].join('');
    } else { // only name given, get cookie
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
};




(function($) {
    $.fn.truncate = function(len) {
        var orginalText = $(this).text();
        if (len == null) len = 80;
        (orginalText.length > len) ? $(this).text(orginalText.substring(0, len) + " .....") : orginalText;
    };
})(jQuery);