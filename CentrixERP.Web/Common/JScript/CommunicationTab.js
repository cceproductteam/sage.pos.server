
var event;
var ParentId, EntityId, EntityValueId, EntityLabelName;
var currentTemplate = null;

function getCommunication(parentId, entityId, entityValueId, entityLabelName) {

    $('.main-task').html('');
    $('.main-task').find('#event-info').remove();
    $('.event-info,.main-task,.sub-task').hide();

    ParentId = parentId;
    EntityId = entityId;
    EntityValueId = entityValueId;
    EntityLabelName = entityLabelName;

    getEntityCommunication();


    $('.add-new-Communication').unbind().click(function () {
        $('.main-task').html('');
        $('.main-task').find('#event-info').remove();
        $('.event-info,.main-task').show();
        $('.sub-task').hide();
        //$('.main-task').find('.event-info').remove();
        loadCalendarFromTab(function () {
            dayClick(new Date());
            $.each(taskConfigControl.taskEntity, function () {

                if (this.entityId == entityId) {
                    this.control.set_Item({ label: entityLabelName, value: parentId });
                    this.control.Disabled(true);
                    if (typeof window[this.controlName + '_OnChange'] === "function") {
                        eval(this.controlName + '_OnChange(' + parentId + ')');
                    }
                    return false;
                }
            });
            return false;
        });
        return false;
    });
    return false;
}

function addCommunicationTab(item, ParentId) {


    var id = (item != null) ? item.TaskId : 0;
    var template = $(CommunicationTabTemplate).clone();


    if (item != null) {

        //template.find('.historyUpdated').append(HistoryUpdatedListViewTemplate);
        template = mapEntityInfoToControls(item, template);
        template.find('.TruncateDescription').text(item.Description != null && item.Description != '' ? '/' + Truncate(item.Description, 40) : '');
        //done by mieassar
        if (item.SubTask)
            template.find('.ViewTask').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.CalendarURL + "?s=" + item.TaskAssignmentId);
        else
            template.find('.ViewTask').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.CalendarURL + "?m=" + item.TaskId);

    } else {
        //template.addClass('addnew');
        template.hide();
    }




    template.find('.ActionFullView-link').click(function () {

        currentTemplate = template;
        $('.main-task').html('');
        $('.sub-task').html('');



        showLoadingWithoutBG($('#demo1'));
        taskEvent = { id: item.TaskId, TaskAssignmentId: item.TaskAssignmentId, SubTask: item.SubTask }
        $('.event-info,.main-task').show();
        loadEventInfo(taskEvent);
        if (item.SubTask) {
            $('.main-task').hide();
            $('.sub-task').show();
        }
        else {
            $('.main-task').show();
            $('.sub-task').hide();
        }
        return false;
    });

    $('.Communication-tabs').append(template);
}



function removeCommunication() {
    currentTemplate.remove();
    ShowHideEventInfo(false);
    //    $.ajax({
    //        type: "POST",
    //        url: systemConfig.ApplicationURL_Common + systemConfig.CalendarWebServiceURL + "DeleteTask",
    //        contentType: "application/json; charset=utf-8",
    //        data: formatString('{taskId:{0}}', id),
    //        dataType: "json",
    //        success: function (data) {
    //            showStatusMsg('Deleted successfully');
    //        },
    //        error: function (e) {
    //        }
    //    });
}


function getEntityCommunication() {

    showLoading($('.TabbedPanels'));
    $('.main-task').children().remove();
    var data = formatString('{taskCriteria:"{EntityId:{0},EntityValueId:{1}}"}', EntityId, ParentId);

    $.ajax({
        type: "POST",
        url: systemConfig.ApplicationURL_Common + systemConfig.CalendarWebServiceURL + "FindAllByEntityLite",
        contentType: "application/json; charset=utf-8",
        data: data,
        dataType: "json",
        success: function (data) {

            $('.Communication-tabs').empty();
            var CommunicationObj = eval("(" + data.d + ")");
            addCommunicationTab(null, ParentId);
            if (CommunicationObj.result != null) {
                $('.Communication-tabs').parent().find('.DragDropBackground').remove();
                $.each(CommunicationObj.result, function (index, item) {
                    addCommunicationTab(item, ParentId);
                });


            }
            hideLoading();
            attachTabEvents();

            loadCalendarFromTab(function () {
                //dayClick(new Date());
                hideLoading();
            });
        },
        error: function (e) {
            hideLoading();
        }
    });



    //getCommunication(ParentId, EntityId, EntityValueId, EntityLabelName);
}
