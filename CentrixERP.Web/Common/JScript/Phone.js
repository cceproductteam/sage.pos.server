
function getPhones(ParentId, mKey) {
    $('.phone-tab').children().remove();
    var data = formatString('{ParentId:{0}, mKey:"{1}"}', ParentId, mKey);
    $.ajax({
        type: "POST",
        url: systemConfig.PhoneServiceURL + "FindAll",
        contentType: "application/json; charset=utf-8",
        data: data,
        dataType: "json",
        success: function (data) {
            var phoneObj = eval("(" + data.d + ")");
            addPhoneTab(null, ParentId, mKey);

            if (phoneObj.result != null) {
                $('.phone-tab').parent().find('.DragDropBackground').remove();
                $.each(phoneObj.result, function (index, item) {
                    addPhoneTab(item, ParentId, mKey);
                });
            }
            else
                $('.TabbedPanelsContentGroup').css('min-height', $('.Result-info-container').height() - 31);
            attachTabEvents();
        },
        error: function (e) {
        }
    });
}

function addPhoneTab(item, ParentId, mKey) {
    var id = (item != null) ? item.Id : 0;
    var isDefault = (item != null) ? item.Isdefault : false;
    var template = $(PhoneTemplate);

    if (item != null) {
        template = mapEntityInfoToControls(item, template);
    } else {
        template.hide();
        $('.add-new-phone').click(function () {

            template.find('.itemheader').click();
            template.find('.viewedit-display-none').show();
            template.find('.viewedit-display-block').hide();

            template.find('input').val('');
            template.find("input:radio").attr("checked", false);

            template.find('.edit-tab-lnk').remove();
            template.find('.save-tab-lnk').show();
            template.find('.cancel-tab-lnk').show();
            template.find('.delete-tab-lnk').hide();
            template.find('.validation').remove();
            if (template.is(':visible')) {
                template.hide();
            }
            else {
                template.show();
            }
            return false;
        });
    }

    if (isDefault)
        template.find('.default-icon').addClass('default-iconVisited')

    template.find('.save-tab-lnk').on('click', function () {
        savePhone(id, ParentId, mKey, template, isDefault);
        template.find('.edit-tab-lnk').show();
        return false;
    });


    template.find('.delete-tab-lnk').click(function () {
        deletePhone(id, mKey, ParentId);
        return false;
    })


    template.find('.edit-tab-lnk').click(function () {
        template.find('.edit-tab-lnk').hide();
        template.find('.save-tab-lnk').show();
        template.find('.cancel-tab-lnk').show();
        template.find('.viewedit-display-none').show();
        template.find('.viewedit-display-block').hide();
        return false;
    })

    template.find('a.default-icon').click(function () {
        if (!$(this).hasClass('default-iconVisited')) {
            SetDefault(id, ParentId, mKey, PhoneServiceURL + "SetPhoneDefualt", true, UserId);
            $('.default-icon').removeClass('default-iconVisited');
            $(this).addClass('default-iconVisited');
        }
        return false;

    });



    template.find('.cancel-tab-lnk').on('click', function () {
        template.find('.itemheader').click();
        getPhones(ParentId, mKey);
        template.find('.edit-tab-lnk').show();
        template.find('.save-tab-lnk').hide();
        return false;
    });


    $('.phone-tab').append(template);
}





function savePhone(id, parentId, mKey, template, isDefault) {
    var countryCode = template.find('input:.CountryCode').val();
    var areaCode = template.find('input:.AreaCode').val();
    var phoneNumber = template.find('input:.PhoneNumber').val();
    var phoneType;
    if ($('input:.LandLine').is(':checked'))
        phoneType = 1;
    else if ($('input:.Skype').is(':checked'))
        phoneType = 2;
    else if ($('input:.Mobile').is(':checked'))
        phoneType = 3;



    var data = '{id:{0}, moduleId:{1}, mKey:"{2}",CountryCode:"{3}",AreaCode:"{4}",PhoneNumber:"{5}",Type:"{6}",IsDefault:"{7}",userId:"{8}"}';
    data = formatString(data, id, parentId, mKey, countryCode, areaCode, phoneNumber, phoneType, isDefault, UserId);

    $.ajax({
        type: "POST",
        url: systemConfig.PhoneServiceURL + "AddEditPhone",
        contentType: "application/json; charset=utf-8",
        data: data,
        dataType: "json",
        success: function (data) {
            getPhones(parentId, mKey);
        },
        error: function (e) {
        }
    });
}





function deletePhone(id, mKey, parentId) {
    $.ajax({
        type: "POST",
        url: systemConfig.PhoneServiceURL + "DeletePhone",
        contentType: "application/json; charset=utf-8",
        data: formatString('{id:{0}, mKey:"{1}"}', id, mKey),
        dataType: "json",
        success: function (data) {
            getPhones(parentId, mKey);
        },
        error: function (e) {
        }
    });
}
