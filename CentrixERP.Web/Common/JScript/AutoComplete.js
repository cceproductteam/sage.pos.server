
//TODO : 1- check the double change callback function calling for the first click on any item
//       2- call the search function if the items are static (no web service call) 

(function ($) {

    jQuery.fn.AutoComplete = function (options) {
        var autoComplete = this;
        this.DefaultArgs = '{keyword:"{0}", page:{1}, resultCount:{2}, argsCriteria: "{3}"}';
        this.defaultArgs = '{keyword:"{0}", page:{1}, resultCount:{2}, argsCriteria: "{3}"}';

        this.selectedItem = null;
        this.AutoComplete = null;
        this.settings = null;
        this.oldItem = null;
        this.fetched = false;
        this.Items = null;
        this.input = null;
        this.onChange = null;
        this.selectlnk = null;
        this.selectedItemsList = null;
        this.selectedItems = new Array();
        this.selectedItemsIds = ',';
        this.defaultItemLoaded = false;
        this.defaultItem = null;
        this.setFirstItemAsDefault = false;

        var defaults = $.extend({
            serviceURL: '',
            selectedItem: null,
            lazyLoad: true,
            args: '',
            fetchOnce: false,
            required: false,
            width: '150px',
            disabled: false,
            autoLoad: true,
            staticParams: true,
            staticItems: false,
            multiSelect: false,
            defaultResultCount: 50,
            defaultPageNumber: 1,
            hasDefaultItem: false,
            customParams: false,
            noneItem: [{ label: 'no data found', value: -1, option: null}]
        }, options);

        autoComplete.init = function () {
            this.defaults = $.extend({}, defaults, options);
            this.initComboBox();
            //$(selectlnk).click();
            if (!defaults.lazyLoad) {
                autoComplete.AutoComplete.getData(autoComplete, settings, '', false);
            }
        }

        autoComplete.set_Item = function (item) {
            this.selectedItem = item;
            $(this.inputHiddenValue).val(item.value);
            $(this.input).val(item.label);
        }

        autoComplete.set_First_AsDefault = function (item) {
            loadSource(autoComplete, '', null, autoComplete.wrapper);
            //$(wrapper).find('.ui-ac-loader').hide();
            //            if (self.Items && self.Items.length > 0) {
            //                autoComplete.set_Item(self.Items[0]);
            //                if (typeof self.onChange == 'function') {
            //                    self.onChange(self.selectedItem, autoComplete);
            //                }
            //            }
        }

        autoComplete.set_Width = function (width) {
            this.input.attr('style', 'width: ' + width + ' !important');
        }
        autoComplete.get_Item = function () {
            return this.selectedItem;
        }

        autoComplete.get_ItemValue = function () {
            return this.selectedItem ? this.selectedItem.value : -1;
        }

        autoComplete.set_Args = function (args) {
            if (this.defaults.staticParams) {
                this.defaultArgs = this.DefaultArgs.replace('{3}', args ? args : '');
            }
            else {
                this.defaults.args = args;
            }

            this.fetched = false;
        }

        autoComplete.set_CustomArgs = function (args) {
            this.defaults.args = args;
            this.defaults.staticParams = false;
            this.fetched = false;
        }

        autoComplete.set_DefaultArgs = function () {
            this.defaults.args = formatString(this.DefaultArgs, '{0}', '1', '50', '{}');
            this.defaults.staticParams = false;
            this.fetched = false;
        }

        autoComplete.set_ServiceRL = function (url) {
            this.defaults.serviceURL = url;
            this.fetched = false;
        }

        autoComplete.get_ServiceRL = function () {
            return this.defaults.serviceURL
        }

        //added by Haneen
        autoComplete.set_keyword_DefaultArgs = function (keyword) {
            this.defaults.args = formatString(this.DefaultArgs, keyword, '1', '50', '{}');
            this.defaults.staticParams = false;
            this.fetched = false;
        }

        autoComplete.clear = function () {
            this.selectedItem = null;
            this.inputHiddenValue.val('-1');
            this.input.val('');

            if (this.defaults.multiSelect) {
                this.selectedItemsIds = ',';
                this.selectedItems = new Array();
                if (this.selectedItemsList) {
                    this.selectedItemsList.children().remove();
                }
            }

            if (this.defaults.hasDefaultItem && this.defaultItem) {
                this.set_Item(this.defaultItem);
            }

            if (this.defaults.setFirstItemAsDefault && this.defaultItem) {
                this.set_Item(this.defaultItem);
            }
        }

        autoComplete.onChange = function (callback) {
            //var item = autoComplete.get_Item();
            //this.onChange = callback(item);
            this.onChange = callback;
        }

        autoComplete.Disabled = function (disabled) {
            this.defaults.disabled = disabled;
            if (disabled) {
                $(this.input).attr('disabled', 'disabled')
                .addClass('ui-autocomplete-disabled');
                $(this.selectlnk).addClass('ui-autocomplete-disabled');
            }
            else {
                $(this.input).removeAttr('disabled')
                .removeClass('  ui-autocomplete-disabled');
                $(this.selectlnk).removeClass('ui-autocomplete-disabled');
            }
        }

        autoComplete.set_Required = function (required) {
            if (required) {
                $(this.input).addClass('required');
            }
            else
                $(this.input).removeClass('required');
        }

        autoComplete.get_POSTArgs = function (term) {
            if (this.defaults.customParams) {
                return formatString(this.defaults.args, term);
            }
            else {
                var ajaxData = this.defaults.staticParams ?
                                formatString(this.defaultArgs, term,
                                 this.defaults.defaultPageNumber,
                                  this.defaults.defaultResultCount,
                                   this.defaults.args)
                                : this.defaults.args;
                return ajaxData;
            }
        }


        autoComplete.set_Items = function (items) {
            if (items) {
                autoComplete.Items = items;
                if (this.defaults.multiSelect) {
                    $.each(items, function (index, item) {
                        addMultiSelectItem(item);
                    })
                }
            }
        }

        autoComplete.get_ItemsIds = function () {
            var ids = this.selectedItemsIds.substring(1, this.selectedItemsIds.length);
            return ids.substring(0, ids.length - 1);
        }

        autoComplete.get_Items = function () {
            return this.selectedItems;
        }

        autoComplete.append_Item = function (item) {
            if (this.defaults.multiSelect && item)
                addMultiSelectItem(item);
        }

        autoComplete.initComboBox = function () {
            $(this).hide();
            var self = this,
            wrapper = this.wrapper = $("<span>").addClass("ui-combobox").insertAfter($(this));
            //            wrapper = this.wrapper = $("<span>").addClass("ui-combobox"),
            //            demoDiv = $('<div>').attr('id', 'demo5').html(wrapper).insertAfter($(this));

            this.input = $("<input>").blur(function () {
                if ($(this).val().trim() == '') {
                    self.selectedItem = null;
                    self.inputHiddenValue.val(-1);
                }
                if (self.oldItem != self.selectedItem) {
                    {
                        self.oldItem = self.selectedItem;
                        if (self.selectedItem)
                            self.inputHiddenValue.val(self.selectedItem.value);
                        else
                            self.inputHiddenValue.val(-1);
                        //$(this.inputHiddenValue).val(self.selectedItem.value);
                    }
                    if (typeof self.onChange == 'function') {
                        self.onChange(self.selectedItem, autoComplete);
                    }
                }
                else if (!self.oldItem) {
                    $(this).val('');
                    if (typeof self.onChange == 'function') {
                        self.onChange(self.selectedItem, autoComplete);
                    }
                }
            })
					.appendTo(wrapper)
					.addClass("cx-control ui-state-default ui-combobox-input")
            //Added By Ruba Al-Sa'di 8/10/2014 to enable inserting new row if last element of grid is combobox
					.prop("type", "text")
                    .css({ width: this.defaults.width })
					.autocomplete({
					    delay: 0,
					    minLength: 0,
					    appendTo: wrapper,
					    source: function (request, response) {
					        loadSource(self, request, response, self.wrapper);
					    },
					    select: function (event, ui) {
					        self.selectedItem = ui.item;
					        if (!ui.item.option)
					            ui.item.option = {};
					        ui.item.option.selected = true;
					        if (self.oldItem != self.selectedItem) {
					            {
					                self.oldItem = self.selectedItem;
					                $(self.inputHiddenValue).val(self.selectedItem.value);
					            }
					            if (typeof self.onChange == 'function') {
					                self.onChange(self.selectedItem, autoComplete);
					            }
					        }

					        if (self.defaults.multiSelect) {
					            addMultiSelectItem(ui.item);
					        }
					        //self.input.autocomplete("close");
					    },
					    change: function (event, ui) {
					        if (!ui.item) {
					            var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex($(this).val()) + "$", "i"),
									valid = false;
					            self.children("option").each(function () {
					                if ($(this).text().match(matcher)) {
					                    this.selected = valid = true;
					                    self.input.blur();
					                    return false;
					                }
					            });
					            if (!valid) {
					                // remove invalid value, as it didn't match anything
					                $(self.input).val("");
					                //self.val("");
					                self.input.data("autocomplete").term = "";
					                self.selectedItem = null;
					            }
					            if (self.oldItem != self.selectedItem) {
					                self.oldItem = self.selectedItem;
					                if (typeof self.onChange == 'function') {
					                    self.onChange(self.selectedItem, autoComplete);
					                }
					            }

					            return false;
					        }

					        if (self.defaults.multiSelect) {
					            //    autoComplete.clear();
					        }
					    }
					})
					.addClass("ui-widget ui-widget-content ui-corner-left")
					.keydown(function (e) {
					    var term = $(this).val();
					    //autoComplete.AutoComplete.getData(self, this.defaults, term, false);
					    if (e.which == 13) {
					        return false;
					    }
					});

            self.input.data("autocomplete")._renderItem = function (ul, item) {

                if (item.value != -1) {
                    return $("<li></li>")
						.data("item.autocomplete", item)
						.append("<a>" + item.label + "</a>")
						.appendTo(ul);
                }
                else {
                    return $("<li></li>")
						.data("item.autocomplete", item)
						.html(item.label)
						.appendTo(ul);
                }
            };

            this.selectlnk = $("<a class='ui-ac-loader'></a>")
                .appendTo(wrapper);




            this.selectlnk = $("<a>")
					.attr("tabIndex", -1)
					.attr("title", "Show All Items")
					.appendTo(wrapper)
					.button({
					    icons: {
					        primary: "ui-icon-triangle-1-s"
					    },
					    text: false
					})
					.removeClass("ui-corner-all")
					.addClass("ui-corner-right ui-combobox-toggle")
					.click(function () {
					    if (autoComplete.defaults.disabled)
					        return false;
					    if (self.input.autocomplete("widget").is(":visible")) {
					        self.input.autocomplete("close");
					        return;
					    }
					    self.input.autocomplete("search", '');
					    self.input.focus();
					});

            if (autoComplete.defaults.multiSelect) {
                var width = wrapper.width() + this.selectlnk.width();
                width = width < 120 ? 183 : width;
                this.selectedItemsList = $('<ul>').addClass('ac-selected-items').addClass('cx-ac-mult-ul').css('width', width);
                wrapper.parent().append(this.selectedItemsList);
            }

            this.inputHiddenValue = $("<input type='hidden'>").addClass(this.selector.replace('select:.', 'hdn').replace('select.', 'hdn')).val('-1').appendTo(wrapper);

            if (this.defaults.required) {
                $(this.input).addClass('required');
            }
            if (this.defaults.disabled) {
                $(this.input).attr('disabled', 'disabled');
            }
        }

        function removeMultiSelectItem(id) {
            autoComplete.selectedItemsIds = autoComplete.selectedItemsIds.replace(',' + id + ',', ',');
            $.each(autoComplete.selectedItems, function (index, item) {
                if (item && item.value === id) {
                    autoComplete.selectedItems.splice(index, 1);
                }
            })
        }

        function addMultiSelectItem(item) {

            if (autoComplete.selectedItemsIds.indexOf(',' + item.value + ',') < 0) {
                autoComplete.selectedItemsIds += item.value + ',';
                autoComplete.selectedItems.push(item);

                var div = $('<div>').html(item.label).addClass('');
                var li = $('<li>').css('cursor', 'pointer').addClass('label-data ac-multi-li-text').append(div);

                var deleteLnk = $('<a>').click(function () {
                    removeMultiSelectItem(item.value);
                    li.remove();
                });

                li.append(deleteLnk);
                // if (autoComplete.selectedItemsList == null)
                //      autoComplete.selectedItemsList = $('<ul>').addClass('ac-selected-items').addClass('cx-ac-mult-ul').css('width', 100);
                autoComplete.selectedItemsList.append(li);
            }
            //autoComplete.clear();
            //autoComplete.input.val('');
            //autoComplete.selectedItem = null;
            //autoComplete.wrapper.find('ul').hide();
        }


        function loadSource(self, request, response, wrapper) {
            var term = request ? request.term : '';
            $(wrapper).find('.ui-ac-loader').show();

            if (!self.defaults.autoLoad && self.fetched || self.defaults.staticItems) {
                var matcher = new RegExp($.ui.autocomplete.escapeRegex(term), "i");
                response($.map(self.Items, function (item) {
                    var text = item.label;
                    var Item = item
                    if (Item != null && text && (!request.term || matcher.test(text))) {
                        Item.option = this;
                        return Item;
                    }

                }));
                request.term = '';
                $(wrapper).find('.ui-ac-loader').hide();
            }
            else if (!self.defaults.staticItems) {

                ////////////////////////
                // createRequest(self, request.term, response, wrapper);

                $.ajax({
                    type: "POST",
                    url: self.defaults.serviceURL,
                    contentType: "application/json; charset=utf-8",
                    data: autoComplete.get_POSTArgs(term),
                    dataType: "json",
                    headers: { 'cx-autocomplete': 'y' },
                    success: function (data) {
                        self.fetched = true;
                        self.Items = null;
                        var result = eval("(" + data.d + ")");
                        var noDataFound = false;
                        if (result.result == null) {
                            noDataFound = true;
                            result.result = new Array();
                            result.result.push(self.defaults.noneItem[0]);
                        }
                        self.Items = result.result;
                        if (self.defaults.hasDefaultItem && !self.defaultItemLoaded) {
                            self.defaultItemLoaded = true;
                            $.each(self.Items, function () {
                                if (this.setAsDefault && self.get_ItemValue() <= 0) {
                                    autoComplete.defaultItem = this;
                                    self.set_Item(this);
                                    $(wrapper).find('.ui-ac-loader').hide();
                                    return false;
                                }
                            })
                        }
                        else if (self.defaults.setFirstItemAsDefault && self.Items[0].value > 0 && self.get_ItemValue() <= 0) {
                            if (self.Items.length > 0) {
                                autoComplete.defaultItem = self.Items[0];
                                self.set_Item(self.Items[0]);
                                if (typeof self.onChange == 'function') {
                                    self.onChange(self.selectedItem, autoComplete);
                                }
                                $(wrapper).find('.ui-ac-loader').hide();
                                return false;
                            }
                        }
                        else {
                            // if (result.result && !noDataFound) {
                            $(wrapper).find('.ui-ac-loader').hide();
                            response($.map(result.result, function (item) {
                                var Item = item;
                                Item.option = this;
                                return Item;
                            }));
                            // }
                        }
                        $(wrapper).find('.ui-ac-loader').hide();
                    },
                    error: function (e) {
                    }
                });
            }
        }

        autoComplete.fillDefaultValue = function () {
            if (defaults.hasDefaultItem) {
                loadSource(autoComplete, '', null, autoComplete.wrapper);
            }
        }

        autoComplete.init();
        return autoComplete;
    };

})(jQuery);