﻿var POSInvoiceServiceURL;

var InvoiceInquiryHeaderTemplate;
var InvoiceInquiryResultsGridDetailsTemplate;
var itemsGrid = null;


function InnerPage_Load() {
    setPageTitle(getResource("InvoiceInquiry"));
    setSelectedMenuItem('.pos-icon');
    POSInvoiceServiceURL = systemConfig.ApplicationURL + systemConfig.POSPostInvoiceToShipmentServiceURL;

  
    GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.POSInvoiceListViewItemTemplate, function () {
        $('.white-container-data').append(InvoiceInquiryHeaderTemplate);
    }, 'InvoiceInquiryHeaderTemplate');

    GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.POSInvoiceResultGridTemplate, function () {
    }, 'InvoiceInquiryResultsGridDetailsTemplate');

    

    initControls();

    $('.btnSearch').attr('value', getResource('Search'));
    $('.lnkReset').attr('value', getResource('Reset'));
    filldataSourceSelect();
}

function initControls() {

    LoadDatePicker('input:.date');

    StoreAC = $('select:.Store').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.StoreServiceURL + 'FindAllLite', required: false, multiSelect: true });


    var InvoiceDateFrom;
    var InvoiceDateTo;

    $('input:.btnSearch').click(function () {
        InvoiceDateFrom = ($('input:.InvoiceDateFrom').val() != "") ? getDate('mm-dd-yy', getDotNetDateFormat($('input:.InvoiceDateFrom').val())) : getDate('mm-dd-yy', formatDate($('input:.InvoiceDateFrom').val()));
        InvoiceDateTo = ($('input:.InvoiceDateTo').val() != "") ? getDate('mm-dd-yy', getDotNetDateFormat($('input:.InvoiceDateTo').val())) : getDate('mm-dd-yy', formatDate($('input:.InvoiceDateTo').val()));

        var url = POSInvoiceServiceURL + "InvoiceInquiryResults";
        var data = formatString('{storeIds:"{0}",InvoiceDateFrom :"{1}" , InvoiceDateTo:"{2}"}', StoreAC.get_ItemsIds(), InvoiceDateFrom, InvoiceDateTo);

        post(url, data, FindResultsSuccess, function () { }, null);
    });

    $('input:.lnkReset').click(function () {
        StoreAC.clear();

        $('.InvoiceDateFrom').val('');
        $('.InvoiceDateTo').val('');
    });
}

function loadItemsGrid(Template, datasource, rowTemplate, headerTemplate) {

    var control = [
  {
      controlId: 'InvoiceNumber',
      type: 'text'

  },
   {
       controlId: 'Type',
       type: 'text'

   },
    {
        controlId: 'InvoiceDate',
        type: 'text'

    },
     {
         controlId: 'ShipmentNumber',
         type: 'text'

     },
      {
          controlId: 'Store',
          type: 'text'

      },
      {
          controlId: 'Register',
          type: 'text'

      },

      {
          controlId: 'Total',
          type: 'text'

      }

 ];
    //after create array of controls, we need init the grid

      itemsGrid = Template.find('#items-grid').Grid({
          headerTeamplte: Template.find('#h-template').html(),
          rowTemplate: InvoiceInquiryResultsGridDetailsTemplate,
          rowControls: control,
          dataSource: datasource,
          selector: '.',
          appendBefore: '.footer-row',
          rowDeleted: function (grid) {

              var Count = 0;
              $.each(grid.table.find('tr.r-data'), function (index, item) {
                  Count = Count + 1;
              });
              if (grid.rows.length == 1) {
                  grid.defaults.preventDelete = true;
              } else grid.defaults.preventDelete = false;
          },
          rowAdded: function (row, controls, dataRow, grid) {

              if (!dataRow)
                  row.find('.viewedit-display-none').show();

              var Count = 0;
              $.each(grid.table.find('tr.r-data'), function (index, item) {

                  row.find('.ShipmentNumber').unbind().click(function () {
                      window.open(systemConfig.ApplicationURL + systemConfig.pageURLs.IcShipmentList + "?key=" + item.ShipmentId);

                  });

                  Count = Count + 1;
              });





              if (grid.rows.length == 1) {
                  grid.defaults.preventDelete = true;
              } else grid.defaults.preventDelete = false;

          }, preventAdd: false
      });

}


function FindResultsSuccess(returnedValue) {   //Here Create the details grid , Fill it with corresponding data
    $('#items-grid').empty();
    var result = JSON.stringify(returnedValue.result);
    $('.result').DataTable({
        "bFilter": false,
        "data": returnedValue.result,
        "columns": [
            { "data": "Store" },
            { "data": "InvoiceNumber" },
            { "data": "Type" },
            { "data": "InvoiceDate" },
            { "data": "ShipmentNumber" },
            { "data": "Register" },
            { "data": "Total" }
        ],
        dom: 'Bfrtip',
        buttons: [
          
            {
                extend: 'collection',
                text: 'Export',
                buttons: [
                    'copy',
                    'excel',
                    'csv',
                    'pdf',
                    'print'
                ]
            }
        ]
    });
     //$('.buttons-html5').hide();
    return;
   
    if (result && result.length > 0) {
        $('.NoDataFound').hide();
        loadItemsGrid($('.gridContainerDiv'), result);


        $($('table')[2]).wrap("<div class='grid-height'></div>");  //To Control the grid height


        $($('.ShipmentNumber')[0]).click(function () {
            window.open(systemConfig.ApplicationURL + systemConfig.pageURLs.IcShipmentList + "?key=" + $($('.ShipmentId')[0]).text());

        });

        $.each($('.InvoiceDate'), function (ix, itm) {
            $($('.InvoiceDate')[ix]).text(getDate("dd-mm-yy", formatDate($($('.InvoiceDate')[ix]).text())));
        });


    }
    else {
        $('.NoDataFound').show();
        $('.NoDataFound').attr('style', 'display: inline-block !important;');
    }


   


}



function getdate(date) {
    var arr = date.split('-');
    return arr[1] + '-' + arr[0] + '-' + arr[2];
}


function filldataSourceSelect() {
    var ExcellOption = $('<option class=dt-button buttons-excel buttons-html5>').html('Excel');
    var PDFOptions = $('<option class=dt-button buttons-pdf buttons-html5>').html('PDF');
    var CSVOption = $('<option>').html('CSV');
    $('.exportOptions').append(ExcellOption, PDFOptions, CSVOption);
}

