var systemConfig;
var DeleteTitleMessage;
var DeleteMessage;
var LoadingDataError = '';
var LoadingDataErrorTitle = 'Error';
var UserId = -1, LoggedInUser;
var Lang = 'en';
var queryStringObj = {}, qsLoaded = false;

var allowTabsScroll = true
var totalItemsToScroll = 3;
var ramaining = 0;
var totalItemsToScrollHeight;
var heightOfLi;
var numOfLi;
var heightOfUL;
var HistoryUpdatedListViewTemplate;
var QuickSearch = false;
var systemEntities = null;
var permissions = null;
var PemissionWebServiceURL = null;
var preventSendHeader = true;
var AddMode = false;
var icModule;
var optionalFieldListViewTemplate;
var optionalFieldRowTemplate;
var addOptionalFieldTemplate;
var OptionalFieldPopUpTemplate;
var NoteTemplate, PhoneTemplate, EmailTemplate, CommunicationTabTemplate, AddressTemplate

function SystemEntities() {
    this.ArOptions = 1;
    this.ARDocumentNumber = 2;
    this.PhysicalCalenderHeader = 3;
    this.TaxAuthority = 4;
    this.TaxClassHeader = 5;
    this.TaxClassDetails = 6;
    this.UserEntityId = 7;
    this.RoleEntityId = 8;
    this.PhysicalCalenderDetails = 9;
    this.TaxRate = 10;
    this.TaxGroupHeader = 11;
    this.TeamEntityId = 12;
    this.TaxGroupDetails = 13;
    this.Currency = 14;
    this.CurrencyRateHeader = 15;
    this.CurrencyRateDetails = 16;
    this.CompanyProfile = 17;
    this.AccountSet = 18;
    this.PaymentCode = 19;
    this.PaymentTerm = 20;
    this.CutsomerGroup = 21;
    this.Customer = 22;
    this.SalesPerson = 23;
    this.SalesPersonCommission = 24;
    this.InvoiceBatchList = 25;
    this.ArInvoice = 26;
    this.ArInvoiceDetails = 27;
    this.ReceiptBatchList = 28;
    this.ArReceipt = 29;
    this.CmOptions = 30;
    this.CreditCardType = 31;
    this.CMDocuments = 32;
    this.GLIntegration = 33;
    this.Bank = 34;
    this.CheckStocks = 35;
    this.BankCurrency = 36;
    this.TransferBatchList = 37;
    this.TransferEntryHeader = 38;
    this.TransferEntryDetails = 39;
    this.ArReceiptDetails = 40;
    this.BankEntryBatchList = 41;
    this.BankEntryHeader = 42;
    this.BankEntryDetails = 43;
    this.RefundBatchList = 44;
    this.AdjustmentBatchList = 45;
    this.ArRefund = 46;
    this.ArAdjustment = 47;
    this.ArAdjustmentDetails = 48;
    this.ArRefundDetails = 49;
    this.ReverseTransaction = 50;
    this.BankReconciliation = 51;
    this.CheckReconcile = 52;
    this.Option = 53;
    this.SegmentCode = 54;
    this.ClassCode = 55;
    this.RolePermissions = 56;
    this.PeriodicProcessing = 57;
    this.Segment = 58;
    this.AccountClass = 59;
    this.AccountStructure = 60;
    this.AccountGroup = 61;
    this.SourceCode = 62;
    this.Account = 63;
    this.ProhibitedStatusTracking = 64;
    this.BatchList = 65;
    this.AllocatedAccount = 66;
    this.JournalEntryHeader = 67;
    this.JournalEntryDetails = 68;
    this.CreateNewYear = 69;
    this.GlReport = 70;
    this.CmReport = 71;
    this.ArReport = 72;
    this.LockEntity = 73;
    this.CustomerGroupSalesPerson = 74;
    this.NoteEntityId = 75;
    this.ApOptions = 76;
    this.APDocumentNumber = 77;
    this.ApPaymentCode = 78;
    this.ApPaymentTerm = 79;
    this.ApAccountSet = 80;
    this.VendorGroup = 81;
    this.Vendor = 82;
    this.ApInvoiceBatchList = 83;
    this.ApInvoice = 84;
    this.ApInvoiceDetails = 85;
    this.PaymentBatchList = 86;
    this.ApPayment = 87;
    this.ApPaymentDetails = 88;
    this.APPeriodicProcessing = 89;
    this.OptionalFieldEntity = 1134;
    this.EmailEntityId = 1135;
    this.PhoneEntityId = 2185;


    //*******IC*********
    this.IcOptionsEntityId = 1094;
    this.IcUnitOfMeasuresEntityId = 1095;
    this.IcWieghtUnitOfMeasuresEntityId = 1096;
    this.IcLocationEntityId = 1100;
    this.IcWarrantyInfoEntityId = 1101;
    this.IcSegment = 1105;
    this.IcSegmentCode = 1106;
    this.ItemStructureEntityId = 1107;
    this.IcPriceListEntityId = 1109;
    this.IcAccountSetEntityId = 1112;
    this.IcCategoryEntityId = 1113;
    this.IcDocumentNumbersEntityId = 1114;
    this.IcItemCardEntityId = 1116;
    this.IcItemLocationCostingEntityId = 1119;
    this.IcPriceListGroupEntityId = 1120;
    this.IcPriceListGroupDiscountEntityId = 1121;
    this.IcPriceListGroupTaxEntityId = 1123;
    this.IcReceiptEntityId = 1124;
    this.IcShipmentEntityId = 1126;
    this.IcTransferEntityId = 1128;
    this.IcAdjustmentEntityId = 1130;
    this.IcReceiptDetailEntityId = 1125;
    this.IcShipmentDetailEntityId = 1127;
    this.IcTransferDetailEntityId = 1129;
    this.IcAdjustmentDetailEntityId = 1131;
    this.IcPriceListGroupItemEntityId = 1137;
    this.IcAssetEntityId = 2141;
    this.IcUploadItemsGrid = 2142;
    this.IcAdditionalCostTypeEntityId = 2143;
     
    this.RegisterstatusEntityId = 2174;

    this.ShiftsEntityId = 2177;
    this.UserStoresShiftsEntityId = 2179;

    this.PurchaseOrderHeaderEntityId = 3190;
    this.PoInvoiceEntityId = 3191;
    this.PurchaseOrderDetailsEntityId = 3192;
    this.PoReceiptEntityId = 3193;
    this.PoReturnReceiptEntityId = 4192;
    this.PoUploadLogEntityId = 4193;
    this.OrderEntryEntityId = 5193;
    this.OeInvoiceEntityId = 6194;
    this.ShipmentEntityId = 6193;
    this.ShipmentReturnEntityId = 6195;
    this.OeUploadLogEntityId = 6196;
}

function Permissions() {
    this.ViewId = 1;
    this.ListId = 2;
    this.AddId = 3;
    this.EditId = 4;
    this.DeleteId = 5;
    this.PrintBatchList = 6;
    this.PostBatchList = 7;
    this.YearEndProcessing = 10;
    this.ReverseBatch = 11;
    this.CloneBatch = 12;
    ProvisionalPosting = 13;
    this.RenameFolderId = 21;
    this.ShowCost = 22;
}


$(document).ready(function () {

    if (readCookie("Lang") == "")
        setCooki("Lang", "en", "", "/");
    var lang = readCookie("Lang");
    if (lang == 'ar')
        Lang = 'ar';
    else
        Lang = 'en';

    $('.language').bind('click', function () {
        if (Lang == 'en')
            Lang = "ar";
        else
            Lang = "en";

        setCooki("Lang", Lang, "", "/");
        window.location.reload(true);
    });

    DeleteTitleMessage = getResource("Delete");
    DeleteMessage = getResource("Areyousure");
    systemEntities = new SystemEntities();
    permissions = new Permissions();

    try {
        numOfLi = $("ul.TabbedPanelsTabGroup li").size();
        heightOfUL = $("ul.TabbedPanelsTabGroup").height();
        heightOfLi = parseFloat($("ul.TabbedPanelsTabGroup li").height()) +
                             parseFloat($("ul.TabbedPanelsTabGroup li").css('margin-bottom').replace('px', '')) +
                             parseFloat($("ul.TabbedPanelsTabGroup li").css('padding-bottom').replace('px', '')) +
                             parseFloat($("ul.TabbedPanelsTabGroup li").css('padding-top').replace('px', '')) + 2;

        totalItemsToScrollHeight = heightOfLi * totalItemsToScroll;
        UPtotalItemsToScrollHeight = 0;
        ramaining = numOfLi - 5;
        UPramaining = 0;
    }
    catch (err) { }
    $("ul.TabbedPanelsTabGroup").css("position", "relative");


    //$('#demo1').alternateScroll();
    //$('#demo2').alternateScroll();
    //$('#demo3').alternateScroll();

    SetResourceKey($(document));


    $('.TabbedPanelsTab').bind('click', function () {
        $(this).children('.TabbedPanelsTab-img').css('position', 'relative');
        $(this).children('.TabbedPanelsTab-img').animate({ bottom: 5 }, 300);
        $(this).children('.TabbedPanelsTab-img').animate({ bottom: 0 }, 300);
    });


    $('textarea[maxlength]').bind('keyup blur', function () {

        // Store the maxlength and value of the field.
        var maxlength = $(this).attr('maxlength');
        var val = $(this).val();

        // Trim the field if it has content over the maxlength.
        if (val.length > maxlength) {
            $(this).val(val.slice(0, maxlength));
        }
    });

    $('form').submit(function () { return false; });

    $(document).keyup(function (e) {
        if (e.which == 27) {
            hideNotifications();
        }  // esc   (does not work)
    });

    getSystemConfiguration();
    $('.searchTextbox').focus();

    if ($('.MoreListMenu-ul').find('li').length <= 0) {
        $('.MoreList').hide();
    }

});

function getSystemConfiguration() {
    //UserId = readCookie('uid');
    ajaxSetup();
    $.ajax({
        type: "POST",
        url: "../../Common/WebServices/UsersWebService.asmx/GetLoggedInUser",
        contentType: "application/json; charset=utf-8",
        data: '{}',
        dataType: "json",
        success: function (data) {
            LoggedInUser = eval("(" + data.d + ")").result;
            UserId = LoggedInUser.Id;
            getSystemConfigurationValues();
            //getSystemOnScreenNotification(UserId);
        },
        error: function (e) {
        }
    });
}

function getSystemConfigurationValues() {
    GetTemaplte('../../Common/Templates/NoteListViewItem.html', null, 'NoteTemplate');
    GetTemaplte('../../Common/Templates/HistoryUpdatedListViewItem.html', null, 'HistoryUpdatedListViewTemplate');
    GetTemaplte('../../Common/Templates/EmailListViewItem.html', null, 'EmailTemplate');
    GetTemaplte('../../Common/Templates/OptionalFieldEntity/OptionalFieldRowItem.html', null, 'optionalFieldRowTemplate');
    GetTemaplte('../../Common/Templates/OptionalFieldEntity/OptionalFieldListViewItem.html', null, 'optionalFieldListViewTemplate');
    GetTemaplte('../../Common/Templates/OptionalFieldEntity/AddOptionalField.html', null, 'addOptionalFieldTemplate');
    GetTemaplte('../../Common/Templates/OptionalFieldEntity/OptionalFieldPopUp.html', null, 'OptionalFieldPopUpTemplate');
    GetTemaplte('../../Common/Templates/PhoneListViewItem.html', null, 'PhoneTemplate');
  


    loadConfigurationValues();

    if (typeof InnerPage_Load == 'function') {
        InnerPage_Load();
    }

}

function setAddLinkURL(url, label) {
    $('.addicon').show();
    $('.add-entity-button .addlabel').html(label);
    $('.add-entity-button').attr('href', url);
}

//Added by ruba 14-10-2014
function showBlackOverlay(element, cssClass) {
    //Changed by Haneen 18-11-2014
    if (element) {
        $(".black_overlay").css('z-index', Number($(element).css('z-index') - 2)).show();
    }
    if (cssClass) $(".black_overlay").addClass(cssClass).show(); //black_overlay_on_add_popup

    $(".black_overlay").show();
}


//Added by Haneen 18-11-2014
function hideBlackOverlay(cssClass, isVisibleOverLay) {
    if (cssClass)
        $(".black_overlay").removeClass(cssClass);
    if (!isVisibleOverLay)
        $(".black_overlay").hide();
}



function setAddLinkAction(label ,itemClass) {
    $('.addicon').show();
    $('.add-entity-button .addlabel').html(label);
    $('.add-entity-button').click(function () {
        //Added by ruba 14-10-2014
        showBlackOverlay();
        addingRecord = true;
        AddMode = true;
        $('.add-new-item').html('');
        var template = $(AddItemTemplate).clone();
        $('.add-new-item').append(template);
        $('.add-new-item').show();
        
        initAddControls(template);
        $(template).find('.ActionSave-link').unbind().click(function () { SaveEntity(true, template); return false; });
        $(template).find('.ActionCancel-link').unbind().click(function () {
            addingRecord = false;
            //Edited by Ruba Al-Sa'di 4/11/2014
            var EditingOrAddingInProcess = CheckIfAddingOrEditingIsInProcess();
            if (EditingOrAddingInProcess) {
                if (AddMode)
                    showConfirmMsg2(getResource("Warning"), getResource('AreYouSureYouWantToCancel'), function () {
                        AddMode = false;
                        //Added By Haneen 25-11-2014
                        $('.optional-field-pop-up').empty().hide();
                        $('.black_overlay').removeClass('black_overlay_on_add_popup');

                        $('.add-new-item').hide();
                        $('.add-new-item').html('');
                        //Added by ruba 14-10-2014
                        $(".black_overlay").hide();
                        if (selectedEntityId > 0)
                            getEntityData(selectedEntityId);
                        return false;
                    }, function () {
                        return false;
                    });
            }
            else {
                AddMode = false;
                //Added By Haneen 25-11-2014
                $('.optional-field-pop-up').empty().hide();
                $('.black_overlay').removeClass('black_overlay_on_add_popup');

                $('.add-new-item').hide();
                $('.add-new-item').html('');
                //Added by ruba 14-10-2014
                $(".black_overlay").hide();
                if (selectedEntityId > 0)
                    getEntityData(selectedEntityId);
                return false;
            }

            //            AddMode = false; $('.add-new-item').hide(); $('.add-new-item').html('');
            //            //Added by ruba 14-10-2014
            //            $(".black_overlay").hide();
            //            return false;
            // 
            return false;
        });

        if ($(template).find('input:first').hasClass('date')) $(template).find('input:first').next().focus();
        else $(template).find('input:first').focus();
        if (itemClass) {
            $('.add-new-item').addClass(itemClass);
        }
        return false;
    });

}

function logOut(args) {
    window.location = systemConfig.ApplicationURL_Common + "Login.aspx?" + args;
}


function showStatusMsg(message) {
    $(".Messaging-Box").show();
    $('.Messaging-Box span').html(message);
    $(".Messaging-Box").animate({
        opacity: 1,
        top: 0
    }, 500, function () {


        window.setTimeout(function () {
            $(".Messaging-Box").animate({
                opacity: 1,
                top: -40
            }, 500);
        }, 2000)
    });

}


function showLoading(element) {

    var loadingBG = $('<div class="loadingbg">');
    $('.Main-Div').append(loadingBG);
    $(loadingBG).css('z-index', '10000000');
    $(loadingBG).css('position', 'absolute');
    $(loadingBG).css('opacity', '0.4');
    $(loadingBG).css('background-color', '#fff');
    $(loadingBG).css('width', $(element).width());
    $(loadingBG).css('height', $(element).height() + 20);
    $(loadingBG).css('top', $(element).offset().top);
    $(loadingBG).css('left', $(element).offset().left);



    $('.TabbedPanelsContentGroup').css('opacity', '0.4');
    var top = ($(element).offset().top + $(element).height()) / 2;
    var left = $(element).offset().left + ($(element).width()) / 2;
    $('.loading').css({ top: top, left: left, display: 'block' });
}

function showLoadingWithoutBG(element) {
    $('.TabbedPanelsContentGroup').css('opacity', '0.9');
    var top = ($(element).offset().top + $(element).height()) / 2;
    var left = $(element).offset().left + ($(element).width()) / 2;
    $('.loading').css({ top: top, left: left, display: 'block' });
}



function hideLoading() {
    $('.loadingbg').remove();
    $('.loading').hide();
    $('.TabbedPanelsContentGroup').css('opacity', '1');
}

function showConfirmMsg(title, message, confirmCallback, cancelCallback) {
    

    var pnl = $(".white_content:first").clone();
    pnl.find('.cmb-title span').html(title);
    pnl.find('.cmb-desc span').html(message);

    pnl.find('.button-cancel').show();
    pnl.find('.confirm-button').show();
    pnl.find('.button-cancel').attr('value', getResource("Cancel"));
    pnl.find('.confirm-button').attr('value', getResource("Confirm"));
   
    pnl.find('.confirm-button').unbind('click').click(function () {
        if (typeof confirmCallback == 'function') {
            confirmCallback();
        }
        if (!AddMode) { $(".black_overlay").hide(); }
        //Addeb by Ruba Al-Sa'di 8/12/2014
        $('.black_overlay').css('z-index', '');
        $(this).unbind('click');
        pnl.remove();
    })

    pnl.find('.button-cancel').click(function () {
        if (typeof cancelCallback == 'function') {
            cancelCallback();
        }

        if (!AddMode) { $(".black_overlay").hide(); }
        //Addeb by Ruba Al-Sa'di 8/12/2014
        $('.black_overlay').css('z-index', '');
        pnl.remove();
    });
    //debugger;
    $(".black_overlay").show();
    //Addeb by Ruba Al-Sa'di 8/12/2014
     $('.black_overlay').css('z-index', Number($('.confermitaion-msg-box:last').css('z-index'))+1);
    //$('.black_overlay').css({ 'z-index': '1006' });
    $(".white_content").after(pnl);
    pnl.show();

    pnl.find('.confirm-button').focus();
}


function showOkayMsg(title, message) {
    
    var pnl = $(".white_content:first").clone();
    pnl.find('.cmb-title span').html(title);
    pnl.find('.cmb-desc span').html(message);
    pnl.find('.button-cancel').hide();
    pnl.find('.button-cancel').attr('value', getResource('Cancel'));
    pnl.find('.confirm-button').attr('value', getResource('Confirm'));

    pnl.find('.confirm-button').attr('value', getResource('Ok')).click(function () {
        if (!AddMode) { $(".black_overlay").hide(); }
        //Addeb by Ruba Al-Sa'di 8/12/2014
        $('.black_overlay').css('z-index', '');
        $(this).unbind('click');
        pnl.remove();
    });

    pnl.find('.button-cancel').hide();

    $(".black_overlay").show();
    //Addeb by Ruba Al-Sa'di 8/12/2014
    $('.black_overlay').css('z-index', Number($('.confermitaion-msg-box:last').css('z-index'))+1);
    $(".white_content").after(pnl);
    pnl.show();
    pnl.find('.confirm-button').focus();
}

function showOkayMsgWithAction(title, message,CallBackFunction) {

    var pnl = $(".white_content:first").clone();
    pnl.find('.cmb-title span').html(title);
    pnl.find('.cmb-desc span').html(message);
    pnl.find('.button-cancel').hide();
    pnl.find('.button-cancel').attr('value', getResource('Cancel'));
    pnl.find('.confirm-button').attr('value', getResource('Confirm'));

    pnl.find('.confirm-button').attr('value', getResource('Ok')).click(function () {
        if (!AddMode) { $(".black_overlay").hide(); }
        //Addeb by Ruba Al-Sa'di 8/12/2014
        $('.black_overlay').css('z-index', '');
        $(this).unbind('click');
        pnl.remove();
        CallBackFunction();
    });

    pnl.find('.button-cancel').hide();

    $(".black_overlay").show();
    //Addeb by Ruba Al-Sa'di 8/12/2014
    $('.black_overlay').css('z-index', Number($('.confermitaion-msg-box:last').css('z-index')) + 1);
    $(".white_content").after(pnl);
    pnl.show();
    pnl.find('.confirm-button').focus();
}

function setPageTitle(title) {
    $('.page-title').html(title);
}

function getQSKey(key) {
    // ***this goes on the global scope
    // get querystring as an array split on "&"
    if (!qsLoaded) {
        var querystring = location.search.replace('?', '').split('&');
        // declare object

        // loop through each name-value pair and populate object
        for (var i = 0; i < querystring.length; i++) {
            // get name and value
            var name = querystring[i].split('=')[0];
            var value = querystring[i].split('=')[1];
            // populate object
            queryStringObj[name] = value;
        }
        qsLoaded = true;
    }
    // ***now you can use queryObj["<name>"] to get the value of a url
    // ***variable
    return queryStringObj[key]
}

function ajaxSetup() {
    $.ajaxSetup({
        beforeSend: function (ajax, data) {
            //showLoading('');
            ajax.setRequestHeader('uid', UserId);
            if (!preventSendHeader)
                ajax.setRequestHeader('request-data', data.data);
        },
        complete: function (ajax, statusMsg) {

            if (statusMsg == 'error') {
                var response = eval('(' + ajax.responseText + ')');
                var responseMsg = eval('(' + response.Message + ')');

                if (responseMsg && responseMsg.code == '501') {
                }
                else if (responseMsg && responseMsg.code == '502') {
                }
                else if (responseMsg && responseMsg.code == '402') {
                    var lokedUser = eval('(' + responseMsg.message + ')');
                    var date = getDate('dd-mm-yy hh:mm tt', formatDate(lokedUser.LockedDate));
                    showStatusMsg(getResource('ThisRecordIsLockedby') + lokedUser.UserName + getResource('at') + date);
                }
                else if (responseMsg && responseMsg.code == '401') {
                debugger
                    showStatusMsg(getResource('vldDontHavePermission'));
                }
                else if (responseMsg && responseMsg.code == '403') {
                    showStatusMsg(getResource('vldLoginSessionExpired'));
                    setTimeout(function () {
                        window.location = systemConfig.ApplicationURL + 'Logout.aspx';
                        hideLoading();
                    }, 2000);
                }
                else if (responseMsg && responseMsg.code == '503') {
                    var errorMessage = getResource("vldDeleteLinkedItem");
                    $.each(responseMsg.result, function (index, item) {
                        if (item.RelatedEntityViewUrl.length > 0)
                            var error = formatString("{0}{1}{2}{3}{4}{5}", (formatString("{0}{1}{2}", "<a href='", systemConfig.ApplicationURL + item.RelatedEntityViewUrl + item.RelatedPrimaryKeyId, "'>")), "<span class='label-data'>", (index + 1), "- ", item.RelatedEntityNameEn, "</span></a><br>");
                        else
                            var error = formatString("{0}{1}{2}{3}{4}", "<span class='label-data'>", (index + 1), "- ", item.RelatedEntityNameEn, "</span></a><br>");

                        errorMessage += error;
                    });
                    showOkayMsg(getResource('Error'), errorMessage);
                }
                else if (responseMsg && responseMsg.code == '505') {
                    var errorMessage = getResource("vldDeactivateLinkedItem");
                    $.each(responseMsg.result, function (index, item) {
                        var error = formatString("{0}{1}{2}{3}{4}{5}", (formatString("{0}{1}{2}", "<a href='", systemConfig.ApplicationURL + item.RelatedEntityViewUrl + item.RelatedPrimaryKeyId, "'>")), "<span class='label-data lnkLabel'>", (index + 1), "- ", item.RelatedEntityNameEn, "</span></a><br>");
                        errorMessage += error;
                    });
                    showOkayMsg(getResource('Error'), errorMessage);
                }
                else {
                    showStatusMsg(getResource('systemError'));
                }

                hideLoading();
            }

        },
        error: function (xx, ee, rr) {

        }
    });
}



////On Screen Notification////////////////

function hideNotifications() {
    $('.task-img,.notif-num').removeClass('task-img-clicked');
    $('.notification-list-div').hide();
    $('.notif-num').show();
}

function setOnScreenNotificationsEvents(userId) {

    $(document).not('.notification-list-div,.task-img,.notif-num').click(function () {

        hideNotifications();
    });

    $('.notification-list-div,.task-img,.notif-num,.NotificationIcon').bind('click', function (e) {
        e.stopPropagation();
    });

    $('.task-img,.notif-num').click(function () {
        //$('.notification-list-div').show();
        if ($(this).hasClass('task-img-clicked')) {
            $(this).removeClass('task-img-clicked');
            $('.notification-list-div').hide();
            $('.notif-num').show();
        }
        else {
            $(this).addClass('task-img-clicked');
            $('.notification-list-div').show();
            $('.notif-num').hide();
        }


    });

    $('.snooze-all-option').hide();
    $('.noti-dismiss-all').click(function () {
        saveOnScrrenNotification(-1, 1, 0, userId);


        clearNotifications();
        return false;
    });

    $('.noti-snooze-all').click(function () {
        $('.snooze-all-option').show();
        $(this).hide();
        return false;
    })

    $('.noti-all-save').click(function () {


        var timeType = $('select:.snooze-select-all').find('option:selected').attr('Ttype');
        var value = $('select:.snooze-select-all').find('option:selected').attr('value');

        var minutes = value;
        if (timeType == 'h')
            minutes = value * 60;
        else if (timeType == 'd')
            minutes = value * 60 * 24;
        else if (timeType == 'w')
            minutes = value * 60 * 24 * 7;

        saveOnScrrenNotification(-1, 2, minutes, userId);
        $('.snooze-all-option').hide();
        $('.noti-snooze-all').show();

        clearNotifications();

        return false;
    });

    $('.noti-all-cancel').click(function () {
        $('.snooze-all-option').hide();
        $('.noti-snooze-all').show();
        return false;
    });

}

function clearNotifications() {
    $('.notif-num').text(0);
    $('.noti-total').text(getResource("OnScreenNotTotalOf") + 0);
    $('.noti-all-option').remove();
    $('.noti-list ul').children().remove();
    $('.notification-list-div').hide();
    $('.notif-num').show();
}

function setNotificationsCount(count) {
    $('.notif-num').text(count);
    $('.noti-total').text(getResource("OnScreenNotTotalOf") + count);
}

var totalNotificationsCount = 0;
function getSystemOnScreenNotification(userId) {

    var args = formatString('{userId:{0}}', userId);
    $.ajax({
        type: 'POST',
        data: args,
        url: '../../Common/WebServices/OnScreenNotificationWebService.asmx/FindAll',
        dataType: 'json',
        contentType: 'application/json; charset=utf8',
        success: function (data) {
            var template = $('li:.notification-template').removeClass('display-none').clone();
            $('li:.notification-template').remove();
            if (data.d) {
                var notifications = eval('(' + data.d + ')').result;

                if (notifications) {

                    totalNotificationsCount = notifications.length;
                    $('li:.notification-template').remove();

                    setNotificationsCount(totalNotificationsCount);

                    $.each(notifications, function (index, item) {
                        addOnScreenNotification(template.clone(), item);


                    });

                    $('.task-img,.notif-num').show();
                }
                else {
                    $('.notif-num').remove();
                    $('li:.notification-template').remove();
                }
            }
            $('.task-img,.notif-num').show();

        },
        error: function () {
            $('.task-img,.notif-num').remove();
            $('li:.notification-template').remove();
        }
    });

    setOnScreenNotificationsEvents(userId);
}

function addOnScreenNotification(template, item) {
    if (item.Url) {
        template.find('.noti-subject').html(item.Subject).attr('href', systemConfig.ApplicationURL_Common + item.Url);
    }
    else {
        template.find('.noti-subject').html(item.Subject).attr('href', 'javascript:void(0);');
    }

    template.find('.noti-desc').html(item.Text);
    template.find('.snooze-option').hide();
    template.find('.noti-snooze').click(function () {
        template.find('.noti-cancel,.noti-snooze').hide();
        template.find('.snooze-option').show();
    });

    template.find('.noti-cancel').click(function () {
        template.find('.snooze-option').hide();
        template.find('.noti-snooze').show();
    });

    template.find('.noti-save').click(function () {

        //$(this).find('option:selected').index();
        var timeType = template.find('select:.snooze-select').find('option:selected').attr('Ttype');
        var value = template.find('select:.snooze-select').find('option:selected').attr('value');

        var minutes = value;
        if (timeType == 'h')
            minutes = value * 60;
        else if (timeType == 'd')
            minutes = value * 60 * 24;
        else if (timeType == 'w')
            minutes = value * 60 * 24 * 7;

        saveOnScrrenNotification(item.OnScreenNotificationId, 2, minutes, UserId);
        totalNotificationsCount = totalNotificationsCount - 1;
        setNotificationsCount(totalNotificationsCount);
        template.remove();
    });

    template.find('.noti-dismiss').click(function () {
        saveOnScrrenNotification(item.OnScreenNotificationId, 1, 0, UserId);
        totalNotificationsCount = totalNotificationsCount - 1;
        setNotificationsCount(totalNotificationsCount);
        template.remove();
    });

    //var noti = template.clone();
    $('.noti-list ul').append(template);
}

//id     : on screen notification id, if -1 then all notifications
//option : 1 => dismiss , 2 => snooze
function saveOnScrrenNotification(id, option, minutes, userId, callback) {
    var url = systemConfig.ApplicationURL + systemConfig.OnScreenNotificationWebService;
    var args = '';

    if (id == -1 && option == 1) {
        url += 'DeActivateAll';
        args = formatString('{userId:{0}}', userId);
    }
    else if (id == -1 && option == 2) {
        url += 'SnoozeAll';
        args = formatString('{userId:{0},minutes:{1}}', userId, minutes);
    }
    else if (id > 0 && option == 1) {
        url += 'DeActivate';
        args = formatString('{id:{0},status:2}', id);
    }
    else if (id > 0 && option == 2) {
        url += 'Snooze';
        args = formatString('{id:{0},minutes:{1}}', id, minutes);
    }

    $.ajax({
        type: 'POST',
        data: args,
        url: url,
        dataType: 'json',
        contentType: 'application/json; charset=utf8',
        success: function (data) {
            if (typeof callback == 'function')
                callback();
        },
        error: function () { }
    });


}
////On Screen Notification////////////////


function SetResourceKey(data) {
    var template = $(data);
    $.each($(template).find('[ResourceKey]'), function (index, value) {

        if (Lang == 'ar')
            $(this).text(DataDictionaryAR[$(this).attr('ResourceKey')]);
        else
            $(this).text(DataDictionaryEN[$(this).attr('ResourceKey')]);
    });

    $.each($(template).find('[title]'), function (index, value) {
        if (Lang == 'ar')
            $(this).attr("title", DataDictionaryAR[$(this).attr("TResourcekey")]);
        else
            $(this).attr("title", DataDictionaryEN[$(this).attr('TResourcekey')]);
    });

    return template;

}

function getResource(resource) {

    if (Lang == 'ar') {
        var value = DataDictionaryAR[resource];
        return value ? value : "";
    }
    else {
        var value = DataDictionaryEN[resource];
        return value ? value : "";
    }
}



function setCooki(sKey, sValue, vEnd, sPath, sDomain, bSecure) {
    if (!sKey || /^(?:expires|max\-age|path|domain|secure)$/.test(sKey)) { return; }
    var sExpires = "";
    if (vEnd) {
        switch (typeof vEnd) {
            case "number": sExpires = "; max-age=" + vEnd; break;
            case "string": sExpires = "; expires=" + vEnd; break;
            case "object": if (vEnd.hasOwnProperty("toGMTString")) { sExpires = "; expires=" + vEnd.toGMTString(); } break;
        }
    }
    document.cookie = escape(sKey) + "=" + escape(sValue) + sExpires + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "") + (bSecure ? "; secure" : "");
}




//Added By Ruba Al-Sa'di
function CheckIfAddingOrEditingIsInProcess() {
    return AddMode //Adding in process
            || (!AddMode && $('.save-cancel-pnl').is(":visible")) //editing (main tab)
            || (!AddMode && TabbedPanels1 && $('.' + TabbedPanels1.tabSelectedClass).attr('tabindex') != 1 && $('.save-tab-lnk').is(":visible")); // editing (other tabs)
}



function showConfirmMsg2(title, message, confirmCallback, cancelCallback) {
    
 
    var pnl = $(".white_content:first").clone();
    pnl.find('.cmb-title span').html(title);
    pnl.find('.cmb-desc span').html(message);

    pnl.find('.button-cancel').show();
    pnl.find('.confirm-button').show();
    pnl.find('.confirm-button').attr('value', getResource('Confirm'));
    pnl.find('.button-cancel').attr('value', getResource('Cancel'));
    pnl.find('.confirm-button').unbind('click').click(function () {
        if (typeof confirmCallback == 'function') {
            confirmCallback();
        }
        //$(".black_overlay").hide();
        //Addeb by Ruba Al-Sa'di 8/12/2014
        $('.black_overlay').css('z-index', '');
        $(this).unbind('click');
        pnl.remove();
    })

    pnl.find('.button-cancel').click(function () {
        if (typeof cancelCallback == 'function') {
            cancelCallback();
        }
        //$(".black_overlay").hide();
        //Addeb by Ruba Al-Sa'di 8/12/2014
        $('.black_overlay').css('z-index', '');
        pnl.remove();
    });
    //
    $(".black_overlay").show();
    //Addeb by Ruba Al-Sa'di 8/12/2014
    $('.black_overlay').css('z-index', Number($('.confermitaion-msg-box:last').css('z-index'))+1);
    $(".white_content").after(pnl);
    pnl.show();

    pnl.find('.confirm-button').focus();
}
function showOkayMsg2(title, message) {
    
    var pnl = $(".white_content:first").clone();
    pnl.find('.cmb-title span').html(title);
    pnl.find('.cmb-desc span').html(message);
    pnl.find('.button-cancel').hide();

    pnl.find('.confirm-button').attr('value', 'Ok').click(function () {
        //$(".black_overlay").hide();
        //Addeb by Ruba Al-Sa'di 8/12/2014
        $('.black_overlay').css('z-index', '');
        $(this).unbind('click');
        pnl.remove();
    });

    pnl.find('.button-cancel').hide();

    $(".black_overlay").show();
    //Addeb by Ruba Al-Sa'di 8/12/2014
    $('.black_overlay').css('z-index', Number($('.confermitaion-msg-box:last').css('z-index')) + 1);
    $(".white_content").after(pnl);
    pnl.show();
}

function showOkayMsgWithouOverlay(title, message) {

    var pnl = $(".white_content:first").clone();
    pnl.find('.cmb-title span').html(title);
    pnl.find('.cmb-desc span').html(message);
    pnl.find('.button-cancel').hide();
    pnl.find('.confirm-button').attr('value', 'Ok').click(function () {
        $('.black_overlay').css('z-index', '');
        $(this).unbind('click');
        pnl.remove();
    });
    pnl.find('.button-cancel').hide();
    $(".white_content").after(pnl);
    pnl.show();
}