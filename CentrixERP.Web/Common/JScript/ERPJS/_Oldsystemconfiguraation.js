
function loadConfigurationValues() {

    systemConfig = new Object();
    systemConfig.dataTypes = new Object();
    systemConfig.WebKeys = new Object();
    systemConfig.pageURLs = new Object();
    systemConfig.HTMLTemplateURLs = new Object();
    systemConfig.ReportSchema = new Object();
    systemConfig.BatchPostingErrors = new Object();
    systemConfig.BatchTransactionErrors = new Object();
    systemConfig.CommonErrors = new Object();
    systemConfig.InventoryErrors = new Object();


    systemConfig.TemplateType = new Object();


    // ************************************** Web keys ***********************************************************   
    systemConfig.WebKeys.MultiblyTypeId = '35';
    systemConfig.WebKeys.DevideTypeId = '36';
    systemConfig.WebKeys.DebitNormalBalanceId = '40';
    systemConfig.WebKeys.RetainedErningID = '44';
    systemConfig.WebKeys.AllowPostToAccount = '45';
    systemConfig.WebKeys.DeletedStatusId = '49';
    systemConfig.WebKeys.PostedStatusId = '50';
    systemConfig.WebKeys.OpenStatusID = '51';
    systemConfig.WebKeys.GeneralLedgerId = '48';
    systemConfig.WebKeys.InvoiceDocumentType = 79;
    systemConfig.WebKeys.InvoiceDebitNote = '295';
    systemConfig.WebKeys.InvoiceCreditNote = 81;
    systemConfig.WebKeys.TaxBaseTypeID = '7';
    systemConfig.WebKeys.SortDepositDetailsBy = '28';
    systemConfig.WebKeys.CMDocumentType = '29';
    systemConfig.WebKeys.DepositTransaction = 100;
    systemConfig.WebKeys.WithdrawalTransaction = 101;
    systemConfig.WebKeys.ClearedDiffrence = 106;
    systemConfig.WebKeys.OutstandingStatusId = 105;
    systemConfig.WebKeys.ARModuleId = 103;
    systemConfig.WebKeys.CMModuleId = 104;
    systemConfig.WebKeys.Prepayment = 82;
    systemConfig.WebKeys.Receipt = 83;
    systemConfig.WebKeys.ApplyDocument = 84;
    systemConfig.WebKeys.MiscellaneousReceipt = 85;
    systemConfig.WebKeys.ApApplyDocument = 114;
    systemConfig.WebKeys.PurchaseTypeId = 24;
    systemConfig.WebKeys.SalesTypeId = 25;
    systemConfig.WebKeys.VendorClassType = 26;
    systemConfig.WebKeys.CustomerClassType = 27;
    systemConfig.WebKeys.CheckPaymentTypeId = 55;
    systemConfig.WebKeys.APModuleId = 126;
    systemConfig.WebKeys.DayOfNextMonthTerm = 212;
    systemConfig.WebKeys.DayOfNextWeekTerm = 213;
    systemConfig.WebKeys.IncomeAccountTypeId = 43;
    systemConfig.WebKeys.BalanceSheetAccountTypeId = 42;
    systemConfig.WebKeys.NoOfEmptyRows = 5;
    systemConfig.WebKeys.IcAdjustmentReceipt = 188;
    
    // ************************************** End Web keys ***********************************************************   



    // ************************************** Page URLs **************************************************************
    // ************************************** Account Receivable Page URLs ********************************************
    systemConfig.pageURLs.AddAccountSetPage = 'AccountsReceivable/AccountSet/AccountSet.aspx';
    systemConfig.pageURLs.AccountSetListPage = 'AccountsReceivable/AccountSet/AccountSetList.aspx';
    systemConfig.pageURLs.AddArInvoicePage = 'AccountsReceivable/ArInvoice/ArInvoice.aspx';
    systemConfig.pageURLs.ArInvoiceListPage = 'AccountsReceivable/ArInvoice/ArInvoiceList.aspx';
    systemConfig.pageURLs.AddArReceiptePage = 'AccountsReceivable/ArReceipt/ArReceipt.aspx';
    systemConfig.pageURLs.ArReceiptListPage = 'AccountsReceivable/ArReceipt/ArReceiptList.aspx';
    systemConfig.pageURLs.AddCustomerPage = 'AccountsReceivable/Customer/Customer.aspx';
    systemConfig.pageURLs.CustomerListPage = 'AccountsReceivable/Customer/CustomerList.aspx';
    systemConfig.pageURLs.AddCustomerGroupPage = 'AccountsReceivable/CustomerGroup/CustomerGroup.aspx';
    systemConfig.pageURLs.CustomerGroupListPage = 'AccountsReceivable/CustomerGroup/CustomerGroupList.aspx';
    systemConfig.pageURLs.AROptionsPage = 'AccountsReceivable/Options/AccountsReceivableOptions.aspx';
    systemConfig.pageURLs.AddPaymentCodePage = 'AccountsReceivable/PaymentCode/PaymentCode.aspx';
    systemConfig.pageURLs.PaymentCodeListPage = 'AccountsReceivable/PaymentCode/PaymentCodeList.aspx';
    systemConfig.pageURLs.AddPaymentTermPage = 'AccountsReceivable/PaymentTerm/PaymentTerm.aspx';
    systemConfig.pageURLs.PaymentTermListPage = 'AccountsReceivable/PaymentTerm/PaymentTermList.aspx';
    systemConfig.pageURLs.AddSalesPersonPage = 'AccountsReceivable/SalesPerson/SalesPerson.aspx';
    systemConfig.pageURLs.SalesPersonListPage = 'AccountsReceivable/SalesPerson/SalesPersonList.aspx';
    systemConfig.pageURLs.ArAdjustmentListPage = 'AccountsReceivable/ArAdjustment/ArAdjustmentList.aspx';
    systemConfig.pageURLs.ArAdjustmentPage = 'AccountsReceivable/ArAdjustment/ArAdjustment.aspx';
    systemConfig.pageURLs.ArRefundListPage = 'AccountsReceivable/ArRefund/ArRefundList.aspx';
    systemConfig.pageURLs.ArRefundPage = 'AccountsReceivable/ArRefund/ArRefund.aspx';
    systemConfig.pageURLs.InvoiceBatchList = 'AccountsReceivable/InvoiceBatchList/InvoiceBatchList.aspx';
    systemConfig.pageURLs.ReceiptBatchList = 'AccountsReceivable/ReceiptBatchList/ReceiptBatchList.aspx';
    systemConfig.pageURLs.AdjustmentBatchList = 'AccountsReceivable/AdjustmentBatchList/AdjustmentBatchList.aspx';
    systemConfig.pageURLs.RefundBatchList = 'AccountsReceivable/RefundBatchList/RefundBatchList.aspx';
    systemConfig.pageURLs.ReportExportPageURL = '/NewReports/ExportReports.aspx?';
    // ************************************** Administrative Settings Page URLs ****************************************
    systemConfig.pageURLs.AddCurrency = 'NewAdministrativeSettings/Currency/AddCurrency.aspx';
    systemConfig.pageURLs.CurrencyList = 'NewAdministrativeSettings/Currency/CurrencyList.aspx';
    systemConfig.pageURLs.AddCurrencyRateHeader = 'NewAdministrativeSettings/CurrencyRate/AddCurrencyRateHeader.aspx';
    systemConfig.pageURLs.CurrencyRateHeaderList = 'NewAdministrativeSettings/CurrencyRate/CurrencyRateHeaderList.aspx';
    systemConfig.pageURLs.AddFiscalCalendar = 'NewAdministrativeSettings/FiscalCalendar/AddFiscalCalendar.aspx';
    systemConfig.pageURLs.FiscalCalendarList = 'NewAdministrativeSettings/FiscalCalendar/FiscalCalendarList.aspx';
    systemConfig.pageURLs.AddTaxAuthority = 'NewAdministrativeSettings/TaxAuthority/AddTaxAuthority.aspx';
    systemConfig.pageURLs.ListTaxAuthority = 'NewAdministrativeSettings/TaxAuthority/TaxAuthorityList.aspx';
    systemConfig.pageURLs.AddTaxClassHeader = 'NewAdministrativeSettings/TaxClass/AddTaxClassHeader.aspx';
    systemConfig.pageURLs.TaxClassHeaderList = 'NewAdministrativeSettings/TaxClass/TaxClassHeaderList.aspx';
    systemConfig.pageURLs.AddTaxGroupHeader = 'NewAdministrativeSettings/TaxGroup/AddTaxGroupHeader.aspx';
    systemConfig.pageURLs.TaxGroupHeaderList = 'NewAdministrativeSettings/TaxGroup/TaxGroupHeaderList.aspx';
    systemConfig.pageURLs.AddTaxRate = 'NewAdministrativeSettings/TaxRate/AddTaxRate.aspx';
    systemConfig.pageURLs.TaxRateList = 'NewAdministrativeSettings/TaxRate/TaxRaeList.aspx';

    // ************************************** General Ledger Page URLs ************************************************
    systemConfig.pageURLs.AccountList = 'NewGeneralLedger/Account/AccountList.aspx';
    systemConfig.pageURLs.AddAccount = 'NewGeneralLedger/Account/AddAccount.aspx';
    systemConfig.pageURLs.AccountStructureList = 'NewGeneralLedger/AccountStructure/AccountStructureList.aspx';
    systemConfig.pageURLs.AddAccountStructure = 'NewGeneralLedger/AccountStructure/AddAccountStructure.aspx';
    systemConfig.pageURLs.AddBatchlist = 'NewGeneralLedger/BatchList/AddBatchList.aspx';
    systemConfig.pageURLs.BatchlistList = 'NewGeneralLedger/BatchList/BatchListList.aspx';
    systemConfig.pageURLs.AddClassCode = 'NewGeneralLedger/ClassCode/AddClassCode.aspx';
    systemConfig.pageURLs.ClassCodeList = 'NewGeneralLedger/ClassCode/ClassCodeList.aspx';
    systemConfig.pageURLs.AddGroupCode = 'NewGeneralLedger/GroupCode/AddGroupCode.aspx';
    systemConfig.pageURLs.GroupCodeList = 'NewGeneralLedger/GroupCode/GroupCodeList.aspx';
    systemConfig.pageURLs.AddJournalEntry = 'NewGeneralLedger/JournalEntryHeader/AddJournalEntry.aspx';
    systemConfig.pageURLs.JournalEntryList = 'NewGeneralLedger/JournalEntryHeader/JournalEntryList.aspx';
    systemConfig.pageURLs.GLOption = 'NewGeneralLedger/Option/GLOptions.aspx';
    systemConfig.pageURLs.AddSegmentCode = 'NewGeneralLedger/SegmentCode/AddSegmentCode.aspx';
    systemConfig.pageURLs.SegmentCodeList = 'NewGeneralLedger/SegmentCode/SegmentCodeList.aspx';
    systemConfig.pageURLs.AddSourceCode = 'NewGeneralLedger/SourceCode/AddSourceCode.aspx';
    systemConfig.pageURLs.SourceCodeList = 'NewGeneralLedger/SourceCode/SourceCodeList.aspx';

    // ************************************** Cash Managment Page URLs ************************************************ 
    systemConfig.pageURLs.AddCreditCardType = 'CashManagment/CreditCardType/AddCreditCardType.aspx';
    systemConfig.pageURLs.CreditCardTypeList = 'CashManagment/CreditCardType/CreditCardTypeList.aspx';
    systemConfig.pageURLs.CMOptionList = 'CashManagment/Option/CMOption.aspx';
    systemConfig.pageURLs.BankList = 'CashManagment/Bank/ListBank.aspx';
    systemConfig.pageURLs.AddBank = 'CashManagment/Bank/AddBank.aspx';
    systemConfig.pageURLs.CMTransferBatchList = 'CashManagment/TransferBatchList/ListTransferBatchList.aspx';
    systemConfig.pageURLs.TransferEntryHeaderList = 'CashManagment/TransferEntryHeader/ListTransferEntryHeader.aspx';
    systemConfig.pageURLs.AddTransferEntryHeader = 'CashManagment/TransferEntryHeader/addTransferEntryHeader.aspx';
    systemConfig.pageURLs.AddBankEntryHeader = 'CashManagment/BankEntryHeader/AddBankEntryHeader.aspx';
    systemConfig.pageURLs.BankEntryHeaderList = 'CashManagment/BankEntryHeader/ListBankEntryHeader.aspx';
    systemConfig.pageURLs.BankReconciliationList = 'CashManagment/BankReconciliation/ListBankReconciliation.aspx';
    systemConfig.pageURLs.AddBankReconciliation = 'CashManagment/BankReconciliation/BankReconciliation.aspx';
    systemConfig.pageURLs.CMBankEntryBatchList = 'CashManagment/BankEntryBatchList/ListBankEntryBatchList.aspx';

    // ************************************** Account Payable Page URLs ********************************************
    systemConfig.pageURLs.AddAPAccountSetPage = 'AccountsPayable/AccountSet/AccountSet.aspx';
    systemConfig.pageURLs.APAccountSetListPage = 'AccountsPayable/AccountSet/AccountSetList.aspx';
    systemConfig.pageURLs.AddAPInvoicePage = 'AccountsPayable/ApInvoice/ApInvoice.aspx';
    systemConfig.pageURLs.APInvoiceListPage = 'AccountsPayable/ApInvoice/ApInvoiceList.aspx';
    systemConfig.pageURLs.AddAPPaymentPage = 'AccountsPayable/ApPayment/ApPayment.aspx';
    systemConfig.pageURLs.APPaymentListPage = 'AccountsPayable/ApPayment/ApPaymentList.aspx';
    systemConfig.pageURLs.AddAPCustomerPage = 'AccountsPayable/Customer/Customer.aspx';
    systemConfig.pageURLs.VendorGroupList = 'AccountsPayable/VendorGroup/VendorGroupList.aspx';
    systemConfig.pageURLs.APOptionsPage = 'AccountsPayable/Options/AccountsPayableOptions.aspx';
    systemConfig.pageURLs.AddAPPaymentCodePage = 'AccountsPayable/PaymentCode/PaymentCode.aspx';
    systemConfig.pageURLs.APPaymentCodeListPage = 'AccountsPayable/PaymentCode/PaymentCodeList.aspx';
    systemConfig.pageURLs.AddAPPaymentTermPage = 'AccountsPayable/PaymentTerm/PaymentTerm.aspx';
    systemConfig.pageURLs.APPaymentTermListPage = 'AccountsPayable/PaymentTerm/PaymentTermList.aspx';
    systemConfig.pageURLs.APAdjustmentListPage = 'AccountsPayable/ArAdjustment/ArAdjustmentList.aspx';
    systemConfig.pageURLs.APAdjustmentPage = 'AccountsPayable/ArAdjustment/ArAdjustment.aspx';
    systemConfig.pageURLs.APRefundListPage = 'AccountsPayable/ArRefund/ArRefundList.aspx';
    systemConfig.pageURLs.APRefundPage = 'AccountsPayable/ArRefund/ArRefund.aspx';
    systemConfig.pageURLs.APInvoiceBatchList = 'AccountsPayable/InvoiceBatchList/InvoiceBatchList.aspx';
    systemConfig.pageURLs.APReceiptBatchList = 'AccountsPayable/ReceiptBatchList/ReceiptBatchList.aspx';
    systemConfig.pageURLs.APAdjustmentBatchList = 'AccountsPayable/AdjustmentBatchList/AdjustmentBatchList.aspx';
    systemConfig.pageURLs.APRefundBatchList = 'AccountsPayable/RefundBatchList/RefundBatchList.aspx';
    systemConfig.pageURLs.VendorList = 'AccountsPayable/Vendor/VendorList.aspx';
    systemConfig.pageURLs.AddVendor = 'AccountsPayable/Vendor/AddVendor.aspx';
    systemConfig.pageURLs.APPaymentBatchList = 'AccountsPayable/PaymentBatchList/PaymentBatchList.aspx';

    // ************************************** Common Page URLs *********************************************************  
    systemConfig.pageURLs.EntityLockList = 'Common/Settings/EntityLockList.aspx';
    systemConfig.pageURLs.CompanyList = "NewCRM/Company/CompanyList.aspx";
    systemConfig.pageURLs.ReportExportPage = '/NewReports/ExportReports.aspx';
    systemConfig.pageURLs.CRMRolePermission = "newUserManagement/Roles/RolePermission.aspx";

    // ************************************** End Page URLs ******************************************************************************************************************* 




    // ************************************** HTML Templates URLs ************************************************  
    //*************************************** Common *************************************************************
    systemConfig.HTMLTemplateURLs.StoreListViewItemTemplate = "POS/Template/Store/StoreListViewItem.html";
    systemConfig.HTMLTemplateURLs.StoreListItemTemplate = "POS/Template/Store/StoreListItem.html";
    systemConfig.HTMLTemplateURLs.AddStoreItemTemplate = "POS/Template/Store/AddStore.html";

    systemConfig.HTMLTemplateURLs.PersonListViewItemTemplate = "POS/Template/Person/PersonListViewItem.html";
    systemConfig.HTMLTemplateURLs.PersonListItemTemplate = "POS/Template/Person/PersonListItem.html";
    systemConfig.HTMLTemplateURLs.AddPersonTemplate = "POS/Template/Person/AddPersonTemplate.html";

    systemConfig.HTMLTemplateURLs.CompanyListViewItemTemplate = "POS/Template/Company/CompanyListViewItem.html";
    systemConfig.HTMLTemplateURLs.CompanyListItemTemplate = "POS/Template/Company/CompanyListItem.html";
    systemConfig.HTMLTemplateURLs.AddCompanyTemplate = "POS/Template/Company/AddCompanyTemplate.html";

    systemConfig.HTMLTemplateURLs.RegisterTabTemplate = "POS/Template/Store/RegisterTabTemplate.html";
    systemConfig.HTMLTemplateURLs.RegisterListViewItemTemplate = "POS/Template/Register/RegisterListViewItem.html";
    systemConfig.HTMLTemplateURLs.RegisterListItemTemplate = "POS/Template/Register/RegisterListItem.html";
    systemConfig.HTMLTemplateURLs.AddRegisterItemTemplate = "POS/Template/Register/AddRegister.html";
    systemConfig.HTMLTemplateURLs.GiftVoucherListViewItemTemplate = "POS/Template/GiftVoucher/GiftVoucherListViewItem.html";
    systemConfig.HTMLTemplateURLs.GiftVoucherListItemTemplate = "POS/Template/GiftVoucher/GiftVoucherListItem.html";
    systemConfig.HTMLTemplateURLs.AddGiftVoucherItemTemplate = "POS/Template/GiftVoucher/AddGiftVoucher.html";
    systemConfig.HTMLTemplateURLs.BarCodeTemplate = "POS/Template/GenerateBarcode/BarCodeTemplate.html";
    systemConfig.HTMLTemplateURLs.QuickKeysListItemTemplate = "POS/Template/QuickKeys/QuickKeysListItem.html";
    systemConfig.HTMLTemplateURLs.AddQuickKeysItemTemplate = "POS/Template/QuickKeys/AddQuickKeysItem.html";
    systemConfig.HTMLTemplateURLs.QuickKeysListViewItemTemplate = "POS/Template/QuickKeys/QuickKeysListViewItem.html";

    systemConfig.HTMLTemplateURLs.OptionalFieldListViewItem = 'Common/Templates/OptionalFieldEntity/OptionalFieldListViewItem.html';
    systemConfig.HTMLTemplateURLs.OptionalFieldRowItem = 'Common/Templates/OptionalFieldEntity/OptionalFieldRowItem.html';

    systemConfig.HTMLTemplateURLs.SyncScheduleListViewItemTemplate = "POS/Template/SyncSchedule/SyncScheduleListViewItem.html";
    systemConfig.HTMLTemplateURLs.SyncScheduleListItemTemplate = "POS/Template/SyncSchedule/SyncScheduleListItem.html";
    systemConfig.HTMLTemplateURLs.AddSyncScheduleItemTemplate = "POS/Template/SyncSchedule/AddSyncScheduleItem.html";


    // ************************************** Account Receivable Templates URLs ********************************************
    systemConfig.HTMLTemplateURLs.AccountSetListItemTemplate = 'AccountsReceivable/Template/AccountSet/AccountSetListItem.html';
    systemConfig.HTMLTemplateURLs.AccountSetListViewItemTemplate = 'AccountsReceivable/Template/AccountSet/AccountSetListViewItem.html';
    systemConfig.HTMLTemplateURLs.ArInvoiceDetailsListViewItem = 'AccountsReceivable/Template/ArInvoice/ArInvoiceDetailsListViewItem.html';
    systemConfig.HTMLTemplateURLs.ArInvoiceListItemTemplate = 'AccountsReceivable/Template/ArInvoice/ArInvoiceListItemTemplate.html';
    systemConfig.HTMLTemplateURLs.ArInvoiceListViewItem = 'AccountsReceivable/Template/ArInvoice/ArInvoiceListViewItem.html';
    systemConfig.HTMLTemplateURLs.ArReceiptListItemTemplate = 'AccountsReceivable/Template/ArReceipt/ArReceiptListItemTemplate.html';
    systemConfig.HTMLTemplateURLs.ArReceiptListViewItem = 'AccountsReceivable/Template/ArReceipt/ArReceiptListViewItem.html';
    systemConfig.HTMLTemplateURLs.ArReceiptDetailsTemplate = 'AccountsReceivable/Template/ArReceipt/ArReceiptDeatilsListViewItem.html';
    systemConfig.HTMLTemplateURLs.ArReceiptApplyDocumentTemplate = 'AccountsReceivable/Template/ArReceipt/ArReceiptApplyDocumentListViewItem.html';
    systemConfig.HTMLTemplateURLs.ArReceiptMiscTemplate = 'AccountsReceivable/Template/ArReceipt/ArReceiptMiscListViewItem.html';
    systemConfig.HTMLTemplateURLs.CustomerTransactionListViewItem = 'AccountsReceivable/Template/Customer/CustomerTransactionListViewItem.html';
    systemConfig.HTMLTemplateURLs.ArRefundListItemTemplate = 'AccountsReceivable/Template/ArRefund/ArRefundListItemTemplate.html';
    systemConfig.HTMLTemplateURLs.ArRefundListViewItem = 'AccountsReceivable/Template/ArRefund/ArRefundListViewItem.html';
    systemConfig.HTMLTemplateURLs.ArRefundDetailsTemplate = 'AccountsReceivable/Template/ArRefund/ArRefundDeatilsListViewItem.html';
    systemConfig.HTMLTemplateURLs.ArAdjustmentListItemTemplate = 'AccountsReceivable/Template/ArAdjustment/ArAdjustmentListItemTemplate.html';
    systemConfig.HTMLTemplateURLs.ArAdjustmentListViewItem = 'AccountsReceivable/Template/ArAdjustment/ArAdjustmentListViewItem.html';
    systemConfig.HTMLTemplateURLs.ArAdjustmentDetailsTemplate = 'AccountsReceivable/Template/ArAdjustment/ArAdjustmentDeatilsListViewItem.html';
    systemConfig.HTMLTemplateURLs.BatchListViewItem = 'AccountsReceivable/Template/BatchList/BatchListViewItem.html';
    systemConfig.HTMLTemplateURLs.CustomerAddressListViewItem = 'AccountsReceivable/Template/Customer/CustomerAddressListViewItem.html';
    systemConfig.HTMLTemplateURLs.CustomerInvoicingListViewItem = 'AccountsReceivable/Template/Customer/CustomerInvoicingListViewItem.html';
    systemConfig.HTMLTemplateURLs.CustomerListItemTemplate = 'AccountsReceivable/Template/Customer/CustomerListItem.html';
    systemConfig.HTMLTemplateURLs.CustomerListViewItemTemplate = 'AccountsReceivable/Template/Customer/CustomerListViewItem.html';
    systemConfig.HTMLTemplateURLs.CustomerProcessingListViewItem = 'AccountsReceivable/Template/Customer/CustomerProcessingListViewItem.html';
    systemConfig.HTMLTemplateURLs.CustomerGroupCurrencyViewItemTemplate = 'AccountsReceivable/Template/CustomerGroup/CustomerGroupCurrencyListViewItem.html';
    systemConfig.HTMLTemplateURLs.CustomerGroupListItemTemplate = 'AccountsReceivable/Template/CustomerGroup/CustomerGroupListItem.html';
    systemConfig.HTMLTemplateURLs.CustomerGroupListViewItemTemplate = 'AccountsReceivable/Template/CustomerGroup/CustomerGroupListViewItem.html';
    systemConfig.HTMLTemplateURLs.CustomerGroupSalesPersonViewItemTemplate = 'AccountsReceivable/Template/CustomerGroup/CustomerGroupSalesPersonListViewItem.html';
    systemConfig.HTMLTemplateURLs.CustomerGroupTaxAuthorityViewItemTemplate = 'AccountsReceivable/Template/CustomerGroup/CustomerGroupTaxAuthorityListViewItem.html';
    systemConfig.HTMLTemplateURLs.AROptionsViewTemplate = 'AccountsReceivable/Template/Options/AccountsReceivableOptionsListViewItem.html';
    systemConfig.HTMLTemplateURLs.ARDocumentNumberListViewItem = 'AccountsReceivable/Template/Options/ARDocumentNumberListViewItem.html';
    systemConfig.HTMLTemplateURLs.PaymentCodeListItemTemplate = 'AccountsReceivable/Template/PaymentCode/PaymentCodeListItem.html';
    systemConfig.HTMLTemplateURLs.PaymentCodeListViewItemTemplate = 'AccountsReceivable/Template/PaymentCode/PaymentCodeListViewItem.html';
    systemConfig.HTMLTemplateURLs.PaymentTermListItemTemplate = 'AccountsReceivable/Template/PaymentTerm/PaymentTermListItem.html';
    systemConfig.HTMLTemplateURLs.PaymentTermListViewItemTemplate = 'AccountsReceivable/Template/PaymentTerm/PaymentTermListViewItem.html';
    systemConfig.HTMLTemplateURLs.SalesPersonCommissionTemplate = 'AccountsReceivable/Template/SalesPerson/SalesPersonCommissionListViewItem.html';
    systemConfig.HTMLTemplateURLs.SalesPersonListItemTemplate = 'AccountsReceivable/Template/SalesPerson/SalesPersonListItem.html';
    systemConfig.HTMLTemplateURLs.SalesPersonListViewItemTemplate = 'AccountsReceivable/Template/SalesPerson/SalesPersonListViewItem.html';
    systemConfig.HTMLTemplateURLs.AddArInvoiceTemplate = 'AccountsReceivable/Template/ArInvoice/addArInvoiceItem.html';
    systemConfig.HTMLTemplateURLs.AddArReceiptTemplate = 'AccountsReceivable/Template/ArReceipt/addArReceiptItem.html';
    systemConfig.HTMLTemplateURLs.AddArAdjustmentTemplate = 'AccountsReceivable/Template/ArAdjustment/addArAdjustmentItem.html';
    systemConfig.HTMLTemplateURLs.AddArRefundTemplate = 'AccountsReceivable/Template/ArRefund/addArRefundItem.html';
    systemConfig.HTMLTemplateURLs.AddPaymentCodeTemplate = 'AccountsReceivable/Template/PaymentCode/addPaymentCodeItem.html';
    systemConfig.HTMLTemplateURLs.AddPaymentTermTemplate = 'AccountsReceivable/Template/PaymentTerm/addPaymentTermItem.html';
    systemConfig.HTMLTemplateURLs.AddAccountSetTemplate = 'AccountsReceivable/Template/AccountSet/addAccountSetItem.html';
    systemConfig.HTMLTemplateURLs.AddSalesPersonTemplate = 'AccountsReceivable/Template/SalesPerson/addSalesPersonItem.html';
    systemConfig.HTMLTemplateURLs.AddCustomerGroupTemplate = 'AccountsReceivable/Template/CustomerGroup/addCustomerGroupItem.html';
    systemConfig.HTMLTemplateURLs.AddCustomerTemplate = 'AccountsReceivable/Template/Customer/addCustomerItem.html';

    // ************************************** General Ledger Templates URLs ************************************************
    systemConfig.HTMLTemplateURLs.AccountTransactionListViewItem = "NewGeneralLedger/Template/Account/AccountTransactionListViewItem.html";
    systemConfig.HTMLTemplateURLs.AccountListTemplate = 'NewGeneralLedger/Template/Account/AccountListItem.html';
    systemConfig.HTMLTemplateURLs.AccountListReport = 'NewGeneralLedger/Template/Account/AccountListReport.html';
    systemConfig.HTMLTemplateURLs.AccountListViewTemplate = 'NewGeneralLedger/Template/Account/AccountListViewItem.html';
    systemConfig.HTMLTemplateURLs.AccountReport = 'NewGeneralLedger/Template/Account/AccountReport.html';
    systemConfig.HTMLTemplateURLs.AccountStructureListTemplate = 'NewGeneralLedger/Template/AccountStructure/AccountStructureListItem.html';
    systemConfig.HTMLTemplateURLs.AccountStructureListViewTemplate = 'NewGeneralLedger/Template/AccountStructure/AccountStructureListViewItem.html';
    systemConfig.HTMLTemplateURLs.BatchlistListTemplate = 'NewGeneralLedger/Template/BatchList/BatchlistListItem.html';
    systemConfig.HTMLTemplateURLs.BatchListReport = 'NewGeneralLedger/Template/BatchList/BatchListReport.html';
    systemConfig.HTMLTemplateURLs.BatchlistListViewTemplate = 'NewGeneralLedger/Template/BatchList/BatchListViewListItem.html';
    systemConfig.HTMLTemplateURLs.ClassListViewTamplate = 'NewGeneralLedger/Template/Class/ClassListViewItem.html';
    systemConfig.HTMLTemplateURLs.ClassListViewTemplate = 'NewGeneralLedger/Template/Class/ClassListViewItem.html';
    systemConfig.HTMLTemplateURLs.ClassCodeListTemplate = 'NewGeneralLedger/Template/ClassCode/ClassCodeListItem.html';
    systemConfig.HTMLTemplateURLs.ClassCodeRowTemplate = 'NewGeneralLedger/Template/ClassCode/ClassCodeRowTemplate.html';
    systemConfig.HTMLTemplateURLs.ClassCodeListViewTemplate = 'NewGeneralLedger/Template/ClassCode/ClassCodeViewListItem.html';
    systemConfig.HTMLTemplateURLs.GroupCodeListTemplate = 'NewGeneralLedger/Template/GroupCode/GroupCodeListItem.html';
    systemConfig.HTMLTemplateURLs.GroupCodeListViewTemplate = 'NewGeneralLedger/Template/GroupCode/GroupCodeListViewItem.html';
    systemConfig.HTMLTemplateURLs.journalEntryDetailsRowTemplate = 'NewGeneralLedger/Template/JournalEntryDetails/journalEntryDetailsRowTemplate.html';
    systemConfig.HTMLTemplateURLs.JournalEntryDetailsListViewTemplate = 'NewGeneralLedger/Template/JournalEntryDetails/journalEntryDetailsViewList.html';
    systemConfig.HTMLTemplateURLs.JournalEntryListViewTemplate = 'NewGeneralLedger/Template/JournalEntryHeader/JournalEntryHeaderListViewItem.html';
    systemConfig.HTMLTemplateURLs.JournalEntryListTemplate = 'NewGeneralLedger/Template/JournalEntryHeader/JournalHeaderListItem.html';
    systemConfig.HTMLTemplateURLs.TransactionReport = 'NewGeneralLedger/Template/JournalEntryHeader/TransactionReport.html';
    systemConfig.HTMLTemplateURLs.OptionsListViewTemplate = 'NewGeneralLedger/Template/Options/OptionsListViewItem.html';
    systemConfig.HTMLTemplateURLs.SegmentListViewTamplate = 'NewGeneralLedger/Template/Segment/SegmentListViewItem.html';
    systemConfig.HTMLTemplateURLs.SegmentCodeListTemplate = 'NewGeneralLedger/Template/SegmentCode/SegmentCodeListItem.html';
    systemConfig.HTMLTemplateURLs.SegmentCodeRowTemplate = 'NewGeneralLedger/Template/SegmentCode/SegmentCodeRowTemplate.html';
    systemConfig.HTMLTemplateURLs.SegmentCodeListViewTemplate = 'NewGeneralLedger/Template/SegmentCode/SegmentCodeViewListItem.html';
    systemConfig.HTMLTemplateURLs.SourceCodeListTemplate = 'NewGeneralLedger/Template/SourceCode/SourceCodeListItem.html';
    systemConfig.HTMLTemplateURLs.SourceCodeListViewTemplate = 'NewGeneralLedger/Template/SourceCode/SourceCodeViewListItem.html';
    systemConfig.HTMLTemplateURLs.AccountTransactionListViewItem = 'NewGeneralLedger/Template/Account/AccountTransactionListViewItem.html';
    systemConfig.HTMLTemplateURLs.SegmentListViewRowTamplate = '/NewGeneralLedger/Template/Segment/SegmentListViewItem.html';
    systemConfig.HTMLTemplateURLs.ClassListViewRowTamplate = '/NewGeneralLedger/Template/Class/ClassListViewItem.html';
    systemConfig.HTMLTemplateURLs.AddJournalHeaderTemplate = '/NewGeneralLedger/Template/JournalEntryHeader/addJournalHeaderItem.html';
    systemConfig.HTMLTemplateURLs.AddAccountStructureTemplate = 'NewGeneralLedger/Template/AccountStructure/addAccountStructureItem.html';
    systemConfig.HTMLTemplateURLs.AddAccountGroupTemplate = 'NewGeneralLedger/Template/GroupCode/addAccountGroupItem.html';
    systemConfig.HTMLTemplateURLs.AddSourceCodeTemplate = 'NewGeneralLedger/Template/SourceCode/addSourceCodeItem.html';
    systemConfig.HTMLTemplateURLs.AddAccountTemplate = 'NewGeneralLedger/Template/Account/addAccountItem.html';
    systemConfig.HTMLTemplateURLs.SegmentListViewRowTamplate = '/NewGeneralLedger/Template/Segment/SegmentListViewItem.html';
    systemConfig.HTMLTemplateURLs.SegmentListViewRowTamplate = 'NewGeneralLedger/Template/Segment/SegmentListViewItem.html';
    systemConfig.HTMLTemplateURLs.ClassListViewRowTamplate = 'NewGeneralLedger/Template/Class/ClassListViewItem.html';


    // ************************************** Cash Managment Templates URLs ************************************************  
    systemConfig.HTMLTemplateURLs.CreditCardTypeListTemplate = 'CashManagment/Template/CreditCardType/CreditCardTypeListItem.html';
    systemConfig.HTMLTemplateURLs.CreditCardTypeListViewTemplate = 'CashManagment/Template/CreditCardType/CreditCardTypeListViewItem.html';
    systemConfig.HTMLTemplateURLs.DocumentListViewItemTemplate = 'CashManagment/Template/Document/DocumentListViewItem.html';
    systemConfig.HTMLTemplateURLs.CMOptionListViewTemplate = 'CashManagment/Template/Option/OptionsListViewItem.html';
    systemConfig.HTMLTemplateURLs.BankListViewItemTemplate = 'CashManagment/Template/Bank/BankListViewItem.html';
    systemConfig.HTMLTemplateURLs.BankListItemTemplate = 'CashManagment/Template/Bank/BankListItem.html';
    systemConfig.HTMLTemplateURLs.BankCheckStockTemplate = 'CashManagment/Template/BankCheckStock/BankCheckStockListViewItem.html';
    systemConfig.HTMLTemplateURLs.BankCurrencyTemplate = 'CashManagment/Template/BankCurrency/BankCurrencyListViewItem.html';
    systemConfig.HTMLTemplateURLs.BatchListRowItem = 'CashManagment/Template/BatchList/BatchListRowItem.html';
    systemConfig.HTMLTemplateURLs.TransferEntryHeaderListViewItemTemplate = 'CashManagment/Template/TransferEntryHeader/EntryHeaderListViewItem.html';
    systemConfig.HTMLTemplateURLs.TransferEntryHeaderListItemTemplate = 'CashManagment/Template/TransferEntryHeader/EntryHeaderListItem.html';
    systemConfig.HTMLTemplateURLs.BankEntryHeaderListViewItemTemplate = 'CashManagment/Template/BankEntryHeader/EntryHeaderListViewItem.html';
    systemConfig.HTMLTemplateURLs.BankEntryHeaderListItemTemplate = 'CashManagment/Template/BankEntryHeader/EntryHeaderListItem.html';
    systemConfig.HTMLTemplateURLs.BankEntryDetailsListViewTemplate = 'CashManagment/Template/BankEntryDetails/EntryDetailsListViewItem.html';
    systemConfig.HTMLTemplateURLs.ReverseTransactionListViewTemplate = 'CashManagment/Template/ReverseTransaction/ReverseTransacionListViewItem.html';
    systemConfig.HTMLTemplateURLs.BankTransactionListViewTemplate = 'CashManagment/Template/BankReconciliation/checkListViewItem.html';
    systemConfig.HTMLTemplateURLs.BankReconciliaionListViewTemplate = 'CashManagment/Template/BankReconciliation/BankReconciliationListViewItem.html';
    systemConfig.HTMLTemplateURLs.BankReconciliaionListTemplate = 'CashManagment/Template/BankReconciliation/BankReconciliationListItem.html';
    systemConfig.HTMLTemplateURLs.AddBankHeaderTemplate = 'CashManagment/Template/BankEntryHeader/addBankEntryHeaderItem.html';
    systemConfig.HTMLTemplateURLs.AddTransferHeaderTemplate = 'CashManagment/Template/TransferEntryHeader/addEntryHeaderItem.html';
    systemConfig.HTMLTemplateURLs.AddCreditCardTypeTemplate = 'CashManagment/Template/CreditCardType/addCreditCardTypeItem.html';
    systemConfig.HTMLTemplateURLs.AddBankTemplate = 'CashManagment/Template/Bank/addBankItem.html';


    // ************************************** Administrative Settings Templates URLs ****************************************
    systemConfig.HTMLTemplateURLs.CompanyProfileListViewItemTemplate = 'NewAdministrativeSettings/Template/CompanyProfile/companyProfileListViewItem.html';
    systemConfig.HTMLTemplateURLs.CurrencyListTemplate = 'NewAdministrativeSettings/Template/Currency/CurrencyListItem.html';
    systemConfig.HTMLTemplateURLs.CurrencyListViewTemplate = 'NewAdministrativeSettings/Template/Currency/CurrencyListViewItem.html';
    systemConfig.HTMLTemplateURLs.CurrencyRateDetailsViewListTemplate = 'NewAdministrativeSettings/Template/CurrencyRate/CurrencyRateDetailsListViewItem.html';
    systemConfig.HTMLTemplateURLs.CurrencyRateHeaderListTemplate = 'NewAdministrativeSettings/Template/CurrencyRate/CurrencyRateHeaderListItem.html';
    systemConfig.HTMLTemplateURLs.CurrencyRateHeaderViewListTemplate = 'NewAdministrativeSettings/Template/CurrencyRate/CurrencyRateHeaderListViewItem.html';
    systemConfig.HTMLTemplateURLs.FiscalCalendarListTemplate = 'NewAdministrativeSettings/Template/FiscalCalendar/FiscalCalendarHeaderListItem.html';
    systemConfig.HTMLTemplateURLs.FiscalCalendarListViewTemplate = 'NewAdministrativeSettings/Template/FiscalCalendar/FiscalCalendarHeaderListViewItem.html';
    systemConfig.HTMLTemplateURLs.TaxAuthorityListTemplate = 'NewAdministrativeSettings/Template/TaxAuthority/TaxAuthorityListItem.html';
    systemConfig.HTMLTemplateURLs.TaxAuthorityListViewTemplate = 'NewAdministrativeSettings/Template/TaxAuthority/TaxAuthorityListViewItem.html';
    systemConfig.HTMLTemplateURLs.TaxClassDetailsListTemplate = 'NewAdministrativeSettings/Template/TaxClass/TaxClassDetailsListViewItem.html';
    systemConfig.HTMLTemplateURLs.TaxClassHeaderListItemTemplate = 'NewAdministrativeSettings/Template/TaxClass/TaxClassHeaderListItem.html';
    systemConfig.HTMLTemplateURLs.TaxClassListViewTemplate = 'NewAdministrativeSettings/Template/TaxClass/TaxClassHeaderListViewItem.html';
    systemConfig.HTMLTemplateURLs.TaxGroupDetailsListViewTemplate = 'NewAdministrativeSettings/Template/TaxGroup/TaxGroupDetailsListViewItem.html';
    systemConfig.HTMLTemplateURLs.TaxGroupHeaderListItemTemplate = 'NewAdministrativeSettings/Template/TaxGroup/TaxGroupHeaderListItem.html';
    systemConfig.HTMLTemplateURLs.TaxGroupHeaderListViewTemplate = 'NewAdministrativeSettings/Template/TaxGroup/TaxGroupHeaderListViewItem.html';
    systemConfig.HTMLTemplateURLs.TaxRateListTemplate = 'NewAdministrativeSettings/Template/TaxRate/TaxRateListItem.html';
    systemConfig.HTMLTemplateURLs.TaxRateListViewTemplate = 'NewAdministrativeSettings/Template/TaxRate/TaxRateListViewItem.html';
    systemConfig.HTMLTemplateURLs.AddTaxAuthorityTemplate = 'NewAdministrativeSettings/Template/TaxAuthority/addTaxAuthorityItem.html';
    systemConfig.HTMLTemplateURLs.AddTaxClassTemplate = 'NewAdministrativeSettings/Template/TaxClass/addTaxClassItem.html';
    systemConfig.HTMLTemplateURLs.AddTaxGroupTemplate = 'NewAdministrativeSettings/Template/TaxGroup/addTaxGroupItem.html';
    systemConfig.HTMLTemplateURLs.AddCurrencyTemplate = 'NewAdministrativeSettings/Template/Currency/addCurrencyItem.html';
    systemConfig.HTMLTemplateURLs.AddCurrencyRateTemplate = 'NewAdministrativeSettings/Template/CurrencyRate/addCurrencyRateItem.html';


    // ************************************** Account Payable Templates URLs ********************************************
    systemConfig.HTMLTemplateURLs.APAccountSetListItemTemplate = 'AccountsPayable/Template/AccountSet/AccountSetListItem.html';
    systemConfig.HTMLTemplateURLs.APAccountSetListViewItemTemplate = 'AccountsPayable/Template/AccountSet/AccountSetListViewItem.html';
    systemConfig.HTMLTemplateURLs.APInvoiceDetailsListViewItem = 'AccountsPayable/Template/ApInvoice/ApInvoiceDetailsListViewItem.html';
    systemConfig.HTMLTemplateURLs.APInvoiceListItemTemplate = 'AccountsPayable/Template/ApInvoice/ApInvoiceListItemTemplate.html';
    systemConfig.HTMLTemplateURLs.APInvoiceListViewItem = 'AccountsPayable/Template/ApInvoice/ApInvoiceListViewItem.html';
    systemConfig.HTMLTemplateURLs.APReceiptListItemTemplate = 'AccountsPayable/Template/ArReceipt/ArReceiptListItemTemplate.html';
    systemConfig.HTMLTemplateURLs.APReceiptListViewItem = 'AccountsPayable/Template/ArReceipt/ArReceiptListViewItem.html';
    systemConfig.HTMLTemplateURLs.APReceiptDetailsTemplate = 'AccountsPayable/Template/ArReceipt/ArReceiptDeatilsListViewItem.html';
    systemConfig.HTMLTemplateURLs.APReceiptApplyDocumentTemplate = 'AccountsPayable/Template/ArReceipt/ArReceiptApplyDocumentListViewItem.html';
    systemConfig.HTMLTemplateURLs.APReceiptMiscTemplate = 'AccountsPayable/Template/ArReceipt/ArReceiptMiscListViewItem.html';
    systemConfig.HTMLTemplateURLs.APRefundListItemTemplate = 'AccountsPayable/Template/ArRefund/ArRefundListItemTemplate.html';
    systemConfig.HTMLTemplateURLs.APRefundListViewItem = 'AccountsPayable/Template/ArRefund/ArRefundListViewItem.html';
    systemConfig.HTMLTemplateURLs.APRefundDetailsTemplate = 'AccountsPayable/Template/ArRefund/ArRefundDeatilsListViewItem.html';
    systemConfig.HTMLTemplateURLs.APAdjustmentListItemTemplate = 'AccountsPayable/Template/ArAdjustment/ArAdjustmentListItemTemplate.html';
    systemConfig.HTMLTemplateURLs.APAdjustmentListViewItem = 'AccountsPayable/Template/ArAdjustment/ArAdjustmentListViewItem.html';
    systemConfig.HTMLTemplateURLs.APAdjustmentDetailsTemplate = 'AccountsPayable/Template/ArAdjustment/ArAdjustmentDeatilsListViewItem.html';
    systemConfig.HTMLTemplateURLs.APBatchListViewItem = 'AccountsPayable/Template/BatchList/BatchListViewItem.html';
    systemConfig.HTMLTemplateURLs.APOptionsViewTemplate = 'AccountsPayable/Template/Options/AccountsPayableOptionsListViewItem.html';
    systemConfig.HTMLTemplateURLs.APDocumentNumberListViewItem = 'AccountsPayable/Template/Options/APDocumentNumberListViewItem.html';
    systemConfig.HTMLTemplateURLs.APPaymentCodeListItemTemplate = 'AccountsPayable/Template/PaymentCode/PaymentCodeListItem.html';
    systemConfig.HTMLTemplateURLs.APPaymentCodeListViewItemTemplate = 'AccountsPayable/Template/PaymentCode/PaymentCodeListViewItem.html';
    systemConfig.HTMLTemplateURLs.APPaymentTermListItemTemplate = 'AccountsPayable/Template/PaymentTerm/PaymentTermListItem.html';
    systemConfig.HTMLTemplateURLs.APPaymentTermListViewItemTemplate = 'AccountsPayable/Template/PaymentTerm/PaymentTermListViewItem.html';
    systemConfig.HTMLTemplateURLs.AddAPInvoiceTemplate = 'AccountsPayable/Template/ApInvoice/addApInvoiceItem.html';
    systemConfig.HTMLTemplateURLs.AddAPReceiptTemplate = 'AccountsPayable/Template/ArReceipt/addArReceiptItem.html';
    systemConfig.HTMLTemplateURLs.AddAPAdjustmentTemplate = 'AccountsPayable/Template/ArAdjustment/addArAdjustmentItem.html';
    systemConfig.HTMLTemplateURLs.AddAPRefundTemplate = 'AccountsPayable/Template/ArRefund/addArRefundItem.html';
    systemConfig.HTMLTemplateURLs.AddAPPaymentCodeTemplate = 'AccountsPayable/Template/PaymentCode/addPaymentCodeItem.html';
    systemConfig.HTMLTemplateURLs.AddAPPaymentTermTemplate = 'AccountsPayable/Template/PaymentTerm/addPaymentTermItem.html';
    systemConfig.HTMLTemplateURLs.AddAPAccountSetTemplate = 'AccountsPayable/Template/AccountSet/addAccountSetItem.html';
    systemConfig.HTMLTemplateURLs.AddVendorGroupTemplate = 'AccountsPayable/Template/VendorGroup/addVendorGroupItem.html';
    systemConfig.HTMLTemplateURLs.VendorGroupListItem = 'AccountsPayable/Template/VendorGroup/VendorGroupListItem.html';
    systemConfig.HTMLTemplateURLs.VendorGroupListViewItem = 'AccountsPayable/Template/VendorGroup/VendorGroupListViewItem.html';
    systemConfig.HTMLTemplateURLs.VendorListViewItemTemplate = "AccountsPayable/Template/Vendor/VendorListViewItem.html";
    systemConfig.HTMLTemplateURLs.VendorListItemTemplate = "AccountsPayable/Template/Vendor/VendorListItem.html";
    systemConfig.HTMLTemplateURLs.AddVendorTemplate = "AccountsPayable/Template/Vendor/addVendorItem.html";
    systemConfig.HTMLTemplateURLs.VendorContactListViewItemTemplate = "AccountsPayable/Template/Vendor/VendorContactListViewItem.html";
    systemConfig.HTMLTemplateURLs.VendorInvoicingListViewItemTemplate = "AccountsPayable/Template/Vendor/VendorInvoicingListViewItem.html";
    systemConfig.HTMLTemplateURLs.VendorProcessingListViewItemTemplate = "AccountsPayable/Template/Vendor/VendorProcessingListViewItem.html";
    systemConfig.HTMLTemplateURLs.AddAPPaymentTemplate = 'AccountsPayable/Template/ApPayment/addApPaymentItem.html';
    systemConfig.HTMLTemplateURLs.APPaymentListItemTemplate = 'AccountsPayable/Template/ApPayment/ApPaymentListItemTemplate.html';
    systemConfig.HTMLTemplateURLs.APPaymentDetailsListViewItem = 'AccountsPayable/Template/ApPayment/ApPaymentDetailsListViewItem.html';
    systemConfig.HTMLTemplateURLs.APPaymentListViewItem = 'AccountsPayable/Template/ApPayment/ApPaymentListViewItem.html';

    // ************************************** Common Templates URLs ****************************************
    systemConfig.HTMLTemplateURLs.AdvancedSearchControlsTemplate = 'Common/Templates/AdvancedSearch/AdvancedSearchControlsTemplate.html';
    systemConfig.HTMLTemplateURLs.HistoryUpdatedListViewItemTemplate = 'Common/Templates/HistoryUpdatedListViewTemplate.html';
    systemConfig.HTMLTemplateURLs.QuickSearchTemplate = 'Common/Templates/AdvancedSearch/QuickSearchTemplate.html';
    systemConfig.HTMLTemplateURLs.EntityLockListViewItemTemplate = '/Common/Templates/EntityLock/EntityLockListViewItem.html';
    systemConfig.HTMLTemplateURLs.EntityLockRowItem = '/Common/Templates/EntityLock/EntityLockRow.html';
    systemConfig.HTMLTemplateURLs.RolePermissionTemplate = 'NewUserManagement/Template/Roles/RolePermissionTemplate.htm';

    systemConfig.HTMLTemplateURLs.PosSyncConfigurationListViewItemTemplate = "POS/Template/PosSyncConfiguration/PosSyncConfigurationListViewItem.html";
    systemConfig.HTMLTemplateURLs.PosSyncConfigurationListItemTemplate = "POS/Template/PosSyncConfiguration/PosSyncConfigurationListItem.html";
    systemConfig.HTMLTemplateURLs.AddPosSyncConfigurationItemTemplate = "POS/Template/PosSyncConfiguration/AddPosSyncConfigurationItem.html";

    // ************************************** End Templates URLs *************************************************************************************************************** 


    // ************************************** API URLs **************************************************************
    systemConfig.GroupCodeServiceURL = 'Common/WebServices/ERP/AccountGroupWebService.asmx/';
    systemConfig.AccountSetWebService = 'Common/WebServices/ERP/AccountSetWebService.asmx/';
    systemConfig.AccountStructureServiceURL = 'Common/WebServices/ERP/AccountStructureWebService.asmx/';
    systemConfig.accountServiceURL = 'Common/WebServices/ERP/AccountWebService.asmx/';
    systemConfig.ARInvoiceBatchListWebService = 'Common/WebServices/ERP/ArInvoiceBatchListWebService.asmx/';
    systemConfig.ArDocument = 'Common/WebServices/ERP/ArDocumentWebService.asmx/';
    systemConfig.ArInvoiceServiceURl = 'Common/WebServices/ERP/ARInvoiceWebService.asmx/';
    systemConfig.ArInvoiceDetailsServiceURl = 'Common/WebServices/ERP/ArInvoiceDetailsWebService.asmx/';
    systemConfig.ARReceiptBatchListWebService = 'Common/WebServices/ERP/ArReceiptBatchListWebService.asmx/';
    systemConfig.ArReceiptServiceURl = 'Common/WebServices/ERP/ArReceiptWebService.asmx/';
    systemConfig.ArReceiptDetailsServiceURl = 'Common/WebServices/ERP/ARReceiptDetailsWebService.asmx/';
    systemConfig.ARRefundBatchListWebService = 'Common/WebServices/ERP/ArRefundBatchListWebService.asmx/';
    systemConfig.ARAdjustmentBatchListWebService = 'Common/WebServices/ERP/ArAdjustmentBatchListWebService.asmx/';
    systemConfig.ArAdjustmentServiceURl = 'Common/WebServices/ERP/ArAdjustmentWebService.asmx/';
    systemConfig.ArAdjustmentDetailsServiceURl = 'Common/WebServices/ERP/ARAdjustmentDetailsWebService.asmx/';
    systemConfig.ArRefundServiceURl = 'Common/WebServices/ERP/ArRefundWebService.asmx/';
    systemConfig.ArRefundDetailsServiceURl = 'Common/WebServices/ERP/ARRefundDetailsWebService.asmx/';
    systemConfig.ARPeriodicProcessingWebService = 'Common/WebServices/ERP/ARPeriodicProcessingWebService.asmx/';
    systemConfig.AROptionsWebService = 'Common/WebServices/ERP/AROptionsWebService.asmx/';
    systemConfig.DocumentNumberService = 'Common/WebServices/ERP/ArDocumentNumberWebService.asmx/';
    systemConfig.AutoAllocationServiceURL = 'Common/WebServices/ERP/AutoAllocatedWebService.asmx/';
    systemConfig.BatchListServiceURL = 'Common/WebServices/ERP/BatchListWebService.asmx/';
    systemConfig.ClassCodeServiceURL = 'Common/WebServices/ERP/ClassCodeWebService.asmx/';
    systemConfig.ClassServiceURL = 'Common/WebServices/ERP/ClassWebService.asmx/';
    systemConfig.CMDocumentServiceURL = 'Common/WebServices/ERP/CMDocumentWebService.asmx/';
    systemConfig.CMOptionServiceURL = 'Common/WebServices/ERP/CMOptionWebService.asmx/';
    systemConfig.CompanyProfileWebServiceURL = 'Common/WebServices/ERP/CompanyProfileWebService.asmx/';
    systemConfig.CurrencyRateDetailsURL = 'Common/WebServices/ERP/CurrencyRateDetailsWebService.asmx/';
    systemConfig.CurrencyRateHeaderURL = 'Common/WebServices/ERP/CurrencyRateHeaderWebService.asmx/';
    systemConfig.CurrencyWebService = 'Common/WebServices/ERP/CurrencyWebService.asmx/';
    systemConfig.CustomerGroupWebService = 'Common/WebServices/ERP/CustomerGroupWebService.asmx/';
    systemConfig.CustomerWebService = 'Common/WebServices/ERP/CustomerWebService.asmx/';
    systemConfig.dataTypeContentServiceURL = 'Common/WebServices/ERP/DataTypeContentWebService.asmx/';
    systemConfig.FiscalCalenderWebServiceURL = 'Common/WebServices/ERP/FiscalCalenderWebService.asmx/';
    systemConfig.JournalEntryDetailsServiceURL = 'Common/WebServices/ERP/JournalEntryDetailsWebService.asmx/';
    systemConfig.JournalEntryHeaderWebServiceURL = 'Common/WebServices/ERP/JournalEntryHeaderWebService.asmx/';
    systemConfig.optionServiceURL = 'Common/WebServices/ERP/OptionWebService.asmx/';
    systemConfig.PaymentCodeWebService = 'Common/WebServices/ERP/PaymentCodeWebService.asmx/';
    systemConfig.PaymentTermWebService = 'Common/WebServices/ERP/PaymentTermWebService.asmx/';
    systemConfig.PostBatchRangeServiceURL = 'Common/WebServices/ERP/PostBatchRangeWebService.asmx/';
    systemConfig.ProhibtedStatusServiceURL = 'Common/WebServices/ERP/ProhibtedStatusWebService.asmx/';
    systemConfig.SalesPersonWebService = 'Common/WebServices/ERP/SalesPersonWebService.asmx/';
    systemConfig.SegmentCodeServiceURL = 'Common/WebServices/ERP/SegmentCodeWebService.asmx/';
    systemConfig.segmentServiceURL = 'Common/WebServices/ERP/SegmentWebService.asmx/';
    systemConfig.SourceCodeServiceURL = 'Common/WebServices/ERP/SourceCodeWebService.asmx/';
    systemConfig.TaxAuthorityWebService = 'Common/WebServices/ERP/TaxAuthorityWebService.asmx/';
    systemConfig.TaxClassDetails = 'Common/WebServices/ERP/TaxClassDetailsWebService.asmx/';
    systemConfig.TaxClassHeaderWebService = 'Common/WebServices/ERP/TaxClassHeaderService.asmx/';
    systemConfig.TaxGroupDetailsWebService = 'Common/WebServices/ERP/TaxGroupDetailsWebService.asmx/';
    systemConfig.TaxGroupHeaderWebService = 'Common/WebServices/ERP/TaxGroupHeaderWebService.asmx/';
    systemConfig.TaxRateWebServiceURL = 'Common/WebServices/ERP/TaxRateWebService.asmx/';
    systemConfig.YearEndServiceURL = 'Common/WebServices/ERP/YearEndWebService.asmx/';
    systemConfig.CreditCardTypeServiceURL = "Common/WebServices/ERP/CreditCardTypeWebService.asmx/";
    systemConfig.ConfigurationWebServiceURL = 'Common/WebServices/ERP/ConfigurationWebService.asmx/';
    systemConfig.BankWebServiceURL = 'Common/WebServices/ERP/BankWebService.asmx/';
    systemConfig.BankCheckStockServiceURL = 'Common/WebServices/ERP/BankCheckStockWebService.asmx/';
    systemConfig.BankCurrencyServiceURL = 'Common/WebServices/ERP/BankCurrencyWebService.asmx/';
    systemConfig.AdvancedSearchWebServiceURL = 'Common/WebServices/ERP/AdvancedSearchWebService.asmx/';
    systemConfig.CMTransferBatchListWebService = 'Common/WebServices/ERP/TransferBatchListWebService.asmx/';
    systemConfig.CMBankEntriesBatchListWebService = 'Common/WebServices/ERP/BankEntryBatchListWebService.asmx/';
    systemConfig.TransferEntryHeaderServiceURL = 'Common/WebServices/ERP/TransferEntryHeaderWebService.asmx/';
    systemConfig.BankEntryHeaderServiceURL = 'Common/WebServices/ERP/BankEntryHeaderWebService.asmx/';
    systemConfig.BankServiceURL = 'Common/WebServices/ERP/BankWebService.asmx/';
    systemConfig.BankEntryDetailsServiceURL = 'Common/WebServices/ERP/BankEntryDetailsWebService.asmx/';
    systemConfig.ReverseTransactionServiceURL = 'Common/WebServices/ERP/ReverseTrasactionWebService.asmx/';
    systemConfig.BankReconciliationServiceURL = 'Common/WebServices/ERP/BankReconciliationWebService.asmx/';
    systemConfig.CheckReconcileServiceURL = 'Common/WebServices/ERP/CheckReconcileWebService.asmx/';
    systemConfig.GlReportServiceURL = 'Common/WebServices/ERP/ReportWebService.asmx/';
    systemConfig.CompanyServiceURL = 'Common/WebServices/CompanyWebService.asmx/';
    systemConfig.LockEntityWebServiceURL = 'Common/WebServices/ERP/LockEntityWebService.asmx/';
    systemConfig.UsersServiceURL = 'Common/WebServices/UsersWebService.asmx/';
    systemConfig.OnScreenNotificationWebService = 'Common/WebServices/OnScreenNotificationWebService.asmx/';
    systemConfig.NoteServiceURL = 'Common/WebServices/ERP/NoteWebService.asmx/';
    systemConfig.RoleServiceURL = 'Common/WebServices/RoleWebService.asmx/';
    systemConfig.RoleWebServiceURL = 'Common/WebServices/RoleWebService.asmx/';
    systemConfig.OptionalFieldWebServiceURL = 'Common/WebServices/ERP/OptionalFieldEntityWebService.asmx/';
    systemConfig.UploadItemsLogServiceURL = "Common/WebServices/IC/UploadItemsLogWebService.asmx/";

    systemConfig.PhoneServiceURL = "Common/WebServices/POS/PhoneWebService.asmx/"
    systemConfig.StoreServiceURL = "Common/WebServices/POS/StoreWebService.asmx/";
    systemConfig.SyncScheduleServiceURL = "Common/WebServices/POS/SyncScheduleWebService.asmx/";
    //systemConfig.QuickKeysServiceURL = 'Common/WebServices/POS/QuickKeyWebService.asmx/';
    systemConfig.RegisterServiceURL = "Common/WebServices/POS/RegisterWebService.asmx/";
    systemConfig.PersonServiceURL = "Common/WebServices/POS/PersonWebService.asmx/";
    systemConfig.GiftVoucherServiceURL = "Common/WebServices/POS/GiftVoucherWebService.asmx/";
    systemConfig.RegisterStatusServiceURL = "Common/WebServices/POS/RegisterStatusWebService.asmx/";
    systemConfig.SystemConfigrationServiceURL = "Common/WebServices/POS/PosSystemConfigrationWebService.asmx/";
    systemConfig.QuickKeysServiceURL = "Common/WebServices/POS/QuickKeysWebService.asmx/";
    systemConfig.QuickKeysProductServiceURL = "Common/WebServices/POS/QuickKeysProductWebService.asmx/";
    systemConfig.GLAccountWebServiceURL = "Common/WebServices/ERP/AccountWebService.asmx/";
    systemConfig.BankAccountWebServiceURL = "Common/WebServices/ERP/BankWebService.asmx/";
    //    systemConfig.SystemConfigrationServiceURL = "Common/WebServices/POS/PosSystemConfigrationWebService.asmx/";
    //    
    //    systemConfig.SystemConfigrationServiceURL = 'Common/WebServices/POS/PosSystemConfigrationWebService.asmx/'
    systemConfig.PossystemConfigurationServiceURL = "Common/WebServices/POS/PosSystemConfigrationWebService.asmx/";
    systemConfig.PosSyncConfigurationServiceURL = "Common/WebServices/POS/PosSyncConfigurationWebService.asmx/";

    systemConfig.LocationWebServiceURL = "Common/WebServices/IC/IcLocationWebService.asmx/";
    systemConfig.DefaultCurrencWebServiceURL = "Common/WebServices/ERP/CurrencyWebService.asmx/";
    systemConfig.CustomerWebServiceURL = "Common/WebServices/ERP/CustomerWebService.asmx/";
    //systemConfig.GLAccountWebServiceURL = "Common/WebServices/POS/PosGlAccountWebService.asmx/";
    //systemConfig.BankAccountWebServiceURL = "Common/WebServices/POS/PosBankAccountWebService.asmx/";
    systemConfig.POSSyncServiceURL = "Common/WebServices/POS/POSSyncAPIsWebService.asmx/";
    systemConfig.POSPostInvoiceToShipmentServiceURL = "Common/WebServices/POS/WritePOSTransactionsWebService.asmx/";

    // ******************* AP APIs ****************
    systemConfig.APOptionsWebService = 'Common/WebServices/ERP/APOptionsWebService.asmx/';
    systemConfig.ApDocument = 'Common/WebServices/ERP/ApDocumentWebService.asmx/';
    systemConfig.ApDocumentNumberService = 'Common/WebServices/ERP/ApDocumentNumberWebService.asmx/';
    systemConfig.ApPaymentCodeWebService = 'Common/WebServices/ERP/ApPaymentCodeWebService.asmx/';
    systemConfig.ApPaymentTermWebService = 'Common/WebServices/ERP/ApPaymentTermWebService.asmx/';
    systemConfig.ApAccountSetWebService = 'Common/WebServices/ERP/ApAccountSetWebService.asmx/';
    systemConfig.VendorGroupWebService = 'Common/WebServices/ERP/VendorGroupWebService.asmx/';
    systemConfig.VendorServiceURL = "Common/WebServices/ERP/VendorWebService.asmx/";
    systemConfig.APInvoiceBatchListWebService = 'Common/WebServices/ERP/ApInvoiceBatchListWebService.asmx/';
    systemConfig.ApInvoiceServiceURl = 'Common/WebServices/ERP/ApInvoiceWebService.asmx/';
    systemConfig.APInvoiceDetailsServiceURl = 'Common/WebServices/ERP/ApInvoiceDetailsWebService.asmx/';
    systemConfig.APPaymenteBatchListWebService = 'Common/WebServices/ERP/ApPaymentBatchListWebService.asmx/';
    systemConfig.ApPaymentServiceURl = 'Common/WebServices/ERP/ApPaymentWebService.asmx/';
    systemConfig.APPaymentBatchListWebService = 'Common/WebServices/ERP/ApPaymentBatchListWebService.asmx/';
    systemConfig.APPeriodicProcessingWebService = 'Common/WebServices/ERP/APPeriodicProcessingWebService.asmx/';

    // ************************************** End API URLs **************************************************************


    // ************************************** DataTypes Ids **************************************************************
    systemConfig.dataTypes.DefaultPostingDate = 1;
    systemConfig.dataTypes.DefaultTransactionType = 2;
    systemConfig.dataTypes.DefaultOrderofOpenDocuments = 3;
    systemConfig.dataTypes.IncludePendingTransactionsType = 4;
    systemConfig.dataTypes.SortChecksBy = 5;
    systemConfig.dataTypes.CheckForDuplicatedChecks = 6;
    systemConfig.dataTypes.TransactionType = 8;
    systemConfig.dataTypes.DateMatched = 10;
    systemConfig.dataTypes.RateOperation = 11;
    systemConfig.dataTypes.SegmentDelimeter = 12;
    systemConfig.dataTypes.NormalBalance = 13;
    systemConfig.dataTypes.AccountType = 14;
    systemConfig.dataTypes.ProhibitedStatus = 15;
    systemConfig.dataTypes.PaymentType = 18;
    systemConfig.dataTypes.CalculateBaseforDiscountWithTax = 19;
    systemConfig.dataTypes.DueDateType = 20
    systemConfig.dataTypes.DisccountType = 21
    systemConfig.dataTypes.ARAccountType = 22;
    systemConfig.dataTypes.DeliverMethod = 23;
    systemConfig.dataTypes.CustomerType = 24;
    systemConfig.dataTypes.CheckforDuplicatePOs = 25;
    systemConfig.dataTypes.InvoiceDocumentType = 26;
    systemConfig.dataTypes.ReceiptType = 27;
    systemConfig.dataTypes.CMDocumentType = 29;
    systemConfig.dataTypes.GLTransaction = 30;
    systemConfig.dataTypes.GlTransactionBy = 31;
    systemConfig.dataTypes.BankTransactionType = 32;
    systemConfig.dataTypes.BankEntryTransactionType = 33;
    systemConfig.dataTypes.ModuleType = 34;
    systemConfig.dataTypes.ReconciliationStatus = 35;
    systemConfig.dataTypes.ReconciliationDiffrenceReason = 36;
    systemConfig.dataTypes.ArPaymentType = 37;
    systemConfig.dataTypes.ApPaymentType = 38;
    systemConfig.dataTypes.AgeUnApplied = 39;
    systemConfig.dataTypes.IncludePendingTransactionsType = 40;
    systemConfig.dataTypes.ApSortChecksBy = 41;
    systemConfig.dataTypes.EmailType = 65;
    systemConfig.dataTypes.PhysicalLocation = 127;
    // ************************************** End DataTypes Ids **************************************************************



    // ************************************** Report Schema Ids **************************************************************
    systemConfig.ReportSchema.CustomerStatementReport = 12;
    systemConfig.ReportSchema.CustomerTransactionReport = 13;
    systemConfig.ReportSchema.CustomerAgingReport = 14;
    systemConfig.ReportSchema.CMTransactionListing = 15;
    systemConfig.ReportSchema.BankReconciliation = 16;
    systemConfig.ReportSchema.InvoiceBatchList = 17;
    systemConfig.ReportSchema.ReceiptbatchList = 18;
    systemConfig.ReportSchema.AdjustmentBatchList = 19;
    systemConfig.ReportSchema.RefundBatchList = 20;
    systemConfig.ReportSchema.RevenueRecognitionReport = 30;
    systemConfig.ReportSchema.CurrencyExchangeRateReport = 31;
    systemConfig.ReportSchema.InvoiceDetailsReport = 21;
    systemConfig.ReportSchema.ApInvoiceBatchReport = 32;
    systemConfig.ReportSchema.ApInvoiceDetailsReport = 33;
    systemConfig.ReportSchema.VendorTransactionReport = 34;
    systemConfig.ReportSchema.VendorStatmentReport = 35;
    systemConfig.ReportSchema.ApPaymentBatchReport = 36;
    systemConfig.ReportSchema.ApPaymentDetailsReport = 37;
    systemConfig.ReportSchema.VendorAgingReport = 38;
    systemConfig.ReportSchema.IcQuantityOnHandReport = 40;
    systemConfig.ReportSchema.IcSalesReport = 41;
    systemConfig.ReportSchema.IcItemDeliveryReport = 42;
    systemConfig.ReportSchema.IcSellingPriceListReport = 43;
    systemConfig.ReportSchema.GLIncomeStatementsReport = 44;
    systemConfig.ReportSchema.GLAccountTransactionsReport = 45;
    systemConfig.ReportSchema.GLCashFlowStatementsReport = 46;
    systemConfig.ReportSchema.IcItemSellingPriceReport = 47;
    systemConfig.ReportSchema.ICReceiptReport = 48;
    systemConfig.ReportSchema.ICShipmentReport = 49;
    systemConfig.ReportSchema.PDCReportSchema = 51;
    systemConfig.ReportSchema.ICAdjustmentReport = 52;
    systemConfig.ReportSchema.ICATransferReport = 53;
    systemConfig.ReportSchema.InvoiceReport = 54;
    systemConfig.ReportSchema.TotalDailySales = 55;
    systemConfig.ReportSchema.DiscountReport = 56;
    systemConfig.ReportSchema.DailySalesPerItem = 57;
    systemConfig.ReportSchema.Dailysales = 58;
    systemConfig.ReportSchema.ChequeDetails = 59;

    systemConfig.ReportSchema.GLBatchListReport = 60;
    systemConfig.ReportSchema.GenerateBarcode = 61;
    // ************************************** End Report Schema Ids **************************************************************



    // ************************************** Batch Posting Errors Ids ***********************************************************  
    systemConfig.BatchPostingErrors.EmptyBatch = 10;
    systemConfig.BatchPostingErrors.NotBalanceBatch = 11;
    systemConfig.BatchPostingErrors.PostedBatch = 12;
    systemConfig.BatchPostingErrors.ReadyToPostBatch = 13;
    systemConfig.BatchPostingErrors.DeletedBatch = 14;
    systemConfig.BatchPostingErrors.EntryInLockedPeriod = 15;
    systemConfig.BatchPostingErrors.EntryBeforOldesetYear = 16;
    systemConfig.BatchPostingErrors.EntryAccountInActive = 17;
    systemConfig.BatchPostingErrors.AllowPostingToPreviousYear = 18;
    //Added By Ruba Al-Sa'di 21-10-2014
    systemConfig.BatchPostingErrors.EntryInFutureYear = 19;
    systemConfig.BatchPostingErrors.EntryHasNoPeriod = 21;
    systemConfig.BatchPostingErrors.AdjustmentPeriodLocked = 22;
    // ************************************** End Batch Posting Errors Ids *******************************************************  


    // ************************************** Batch Transaction Errors Ids ***********************************************************  
    //Added By Lama Barqawi
    systemConfig.BatchTransactionErrors.EmptyBatch = 910;
    systemConfig.BatchTransactionErrors.NotBalanceBatch = 911;
    systemConfig.BatchTransactionErrors.PostedBatch = 912;
    systemConfig.BatchTransactionErrors.ReadyToPostBatch = 913;
    systemConfig.BatchTransactionErrors.DeletedBatch = 914;
    systemConfig.BatchTransactionErrors.EntryInLockedPeriod = 915;
    systemConfig.BatchTransactionErrors.EntryBeforOldesetYear = 916;
    systemConfig.BatchTransactionErrors.EntryAccountInActive = 917;
    systemConfig.BatchTransactionErrors.AllowPostingToPreviousYear = 918;
    systemConfig.BatchTransactionErrors.EntryInFutureYear = 919;
    systemConfig.BatchTransactionErrors.EntryHasNoPeriod = 921;
    systemConfig.BatchTransactionErrors.AdjustmentPeriodLocked = 922;

    // ************************************** End Batch Transaction Errors Ids *******************************************************  




    systemConfig.ApplicationURL_Common = ApplicationURL_Common;
    systemConfig.ApplicationURL = ApplicationURL_Common;
    //  systemConfig.Jabbakam_ApplicationURL = Jabbakam_ApplicationURL;
    PemissionWebServiceURL = systemConfig.ApplicationURL_Common + systemConfig.ConfigurationWebServiceURL;

    // ************************************* Common Errors ***********************************************************************
    systemConfig.CommonErrors.FailedToDelete = 503;
    systemConfig.CommonErrors.FailedToDeleteActiveItem = 504;
    systemConfig.CommonErrors.FailedToInactiveItem = 505;
    systemConfig.CommonErrors.OptioanlFieldDetailsError = 510;




    //************************************** UM URLS *****************************************************************************
    systemConfig.HTMLTemplateURLs.UserListItemTemplate = 'NewUserManagement/Template/Users/UsersListItem.html';
    systemConfig.HTMLTemplateURLs.UserListViewItemTemplate = 'NewUserManagement/Template/Users/UsersListViewItem.html';
    systemConfig.HTMLTemplateURLs.AddUserTemplate = "NewUserManagement/Template/Users/addUserTemplate.html";
    systemConfig.HTMLTemplateURLs.TeamItemTemplate = 'NewUserManagement/Template/Teams/TeamItem.html';
    systemConfig.TeamServiceURL = 'Common/WebServices/TeamWebService.asmx/';
    systemConfig.TeamWebServiceURL = 'Common/WebServices/TeamWebService.asmx/';













    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////INVENTORY CONTROL//////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    systemConfig.dataTypes.Type = 1;
    systemConfig.dataTypes.CompanyType = 1;
    //systemConfig.dataTypes.CustomerType = 85;
    systemConfig.dataTypes.YearlyRevenue = 17;
    systemConfig.dataTypes.EmployeeNumber = 18;
    systemConfig.dataTypes.Status = 90;
    systemConfig.dataTypes.PaymentTerms = 56;
    //systemConfig.dataTypes.PaymentType = 55;
    systemConfig.dataTypes.MainProductInterest = 11;
    systemConfig.dataTypes.DecisionTimeframe = 12;
    systemConfig.dataTypes.OpportunityStage = 20;
    systemConfig.dataTypes.OpportunityStatus = 21;
    systemConfig.dataTypes.Unit = 107;
    systemConfig.dataTypes.PriceListId = 108;
    systemConfig.dataTypes.CurrencyId = 23;
    //systemConfig.dataTypes.TransactionType = 109;
    systemConfig.dataTypes.MoveFrom = 110;
    systemConfig.dataTypes.MoveTo = 110;
    systemConfig.dataTypes.Stock = 110;
    systemConfig.dataTypes.ReturnLocation = 111;

    systemConfig.UsersWebService = 'Common/WebServices/UsersWebService.asmx/';
    systemConfig.CountryServiceURL = 'Common/WebServices/CountryWebService.asmx/';
    systemConfig.PersonsServiceURL = 'Common/WebServices/POS/PersonWebService.asmx/';
    systemConfig.OpportunityServiceURL = "Common/WebServices/OpportunityWebService.asmx/";
    systemConfig.InventoryPriceListServiceURL = "Common/WebServices/InventoryPriceListWebService.asmx/";
    systemConfig.QuotationServiceURL = "Common/WebServices/QuotationWebService.asmx/";
    systemConfig.KitServiceURL = "Common/WebServices/KitWebService.asmx/";
    systemConfig.InventoryKitServiceURL = "Common/WebServices/InventoryKitWebService.asmx/";
    systemConfig.KitPriceListServiceURL = "Common/WebServices/KitPriceListWebService.asmx/";
    systemConfig.InventoryStockServiceURL = "Common/WebServices/InventoryStockWebService.asmx/";
    systemConfig.SalesOrderServiceURL = "Common/WebServices/SalesOrderWebService.asmx/";
    systemConfig.ReturnedItemDetailsServiceURL = "Common/WebServices/ReturnedItemDetailsWebService.asmx/";
    systemConfig.SalesOrderItemServiceURL = "Common/WebServices/SalesOrderItemWebService.asmx/";
    systemConfig.OrderDeliveredItemsServiceURL = "Common/WebServices/OrderDeliveredItemsWebService.asmx/";
    systemConfig.FixedAssetsServiceURL = "Common/WebServices/FixedAssetsWebService.asmx/";


    //systemConfig.HTMLTemplateURLs.CompanyListViewItemTemplate = "NewCRM/Template/Company/CompanyListViewItem.html";
    //systemConfig.HTMLTemplateURLs.CompanyListItemTemplate = "NewCRM/Template/Company/CompanyListItem.html";
    //systemConfig.HTMLTemplateURLs.CompanyPersonsListViewItemTemplate = "NewCRM/Template/Company/CompanyPersonsListViewItem.html";
    //systemConfig.HTMLTemplateURLs.PersonListViewItemTemplate = "NewCRM/Template/Person/PersonListViewItem.html";
    //systemConfig.HTMLTemplateURLs.PersonListItemTemplate = "NewCRM/Template/Person/PersonListItem.html";
    systemConfig.HTMLTemplateURLs.OpportunityListViewItemTemplate = "Sales/Template/Opportunity/OpportunityListViewItem.html";
    systemConfig.HTMLTemplateURLs.OpportunityListItemTemplate = "Sales/Template/Opportunity/OpportunityListItem.html";
    systemConfig.HTMLTemplateURLs.ProgressListTemplate = "Sales/Template/Opportunity/OpportunityProgress.html";
    systemConfig.HTMLTemplateURLs.InventoryPriceListListViewItemTemplate = "Inventory/Template/InventoryPriceList/InventoryPriceListListViewItem.html";
    systemConfig.HTMLTemplateURLs.QuotationListViewItemTemplate = "Sales/Template/Quotation/QuotationListViewItem.html";
    systemConfig.HTMLTemplateURLs.QuotationListItemTemplate = "Sales/Template/Quotation/QuotationListItem.html";
    systemConfig.HTMLTemplateURLs.KitListViewItemTemplate = "Inventory/Template/Kit/KitListViewItem.html";
    systemConfig.HTMLTemplateURLs.KitListItemTemplate = "Inventory/Template/Kit/KitListItem.html";
    systemConfig.HTMLTemplateURLs.InventoryKitListViewItemTemplate = "Inventory/Template/InventoryKit/InventoryKitListViewItem.html";
    systemConfig.HTMLTemplateURLs.KitPriceListListViewItemTemplate = "Inventory/Template/KitPriceList/KitPriceListListViewItem.html";
    systemConfig.HTMLTemplateURLs.InventoryStockListViewItemTemplate = "Inventory/Template/InventoryStock/InventoryStockListViewItem.html";
    systemConfig.HTMLTemplateURLs.InventoryStockListItemTemplate = "Inventory/Template/InventoryStock/InventoryStockListItem.html";
    systemConfig.HTMLTemplateURLs.SalesOrderListViewItemTemplate = "Sales/Template/SalesOrder/SalesOrderListViewItem.html";
    systemConfig.HTMLTemplateURLs.SalesOrderListItemTemplate = "Sales/Template/SalesOrder/SalesOrderListItem.html";
    systemConfig.HTMLTemplateURLs.ReturnedItemDetailsListViewItemTemplate = "Sales/Template/ReturnedItemDetails/ReturnedItemDetailsListViewItem.html";
    systemConfig.HTMLTemplateURLs.QuoteTabTemplate = "Sales/Template/Quotation/QuoteTab.html";
    systemConfig.HTMLTemplateURLs.OrderTabTemplate = "Sales/Template/SalesOrder/SalesOrderTab.html";
    systemConfig.HTMLTemplateURLs.QuoteItemRowTemplate = "Sales/Template/SalesOrder/ItemTemplate.html";
    systemConfig.HTMLTemplateURLs.OrderItemRowTemplate = "Sales/Template/SalesOrder/ItemTemplate.html";
    systemConfig.HTMLTemplateURLs.VariationOrderTemplate = "Sales/Template/ReturnedItem/ReturnedItemTab.html";
    systemConfig.HTMLTemplateURLs.TaskGridRowTemplate = "NewCRM/Template/Task/TaskListGridViewItem.html";
    systemConfig.HTMLTemplateURLs.OrderDeliveredItemTemplate = "Sales/Template/OrderDeliveredItems/ItemTemplate.html";
    systemConfig.HTMLTemplateURLs.TaskListViewItemTemplate = "NewCRM/Template/Task/TasksListViewItems.html";
    systemConfig.HTMLTemplateURLs.TaskListItemTemplate = "NewCRM/Template/Task/TaskListItem.html";

    systemConfig.pageURLs.AddCompany = 'NewCRM/Company/AddCompany.aspx';
    systemConfig.pageURLs.PersonList = 'NewCRM/Person/PersonList.aspx';
    systemConfig.pageURLs.AddPerson = 'NewCRM/Person/AddPerson.aspx';
    systemConfig.pageURLs.OpportunityList = 'Sales/Opportunity/OpportunityList.aspx';
    systemConfig.pageURLs.AddOpportunity = 'Sales/Opportunity/AddOpportunity.aspx';
    systemConfig.pageURLs.QuotationList = 'Sales/Quotation/QuotationList.aspx';
    systemConfig.pageURLs.AddQuotation = 'Sales/Quotation/AddQuotation.aspx';
    systemConfig.pageURLs.KitList = 'Inventory/Kit/KitList.aspx';
    systemConfig.pageURLs.AddKit = 'Inventory/Kit/AddKit.aspx';
    systemConfig.pageURLs.InventoryKitList = 'NewInventory/InventoryKit/InventoryKitList.aspx';
    systemConfig.pageURLs.InventoryStockList = 'Inventory/InventoryStock/InventoryStockList.aspx';
    systemConfig.pageURLs.AddInventoryStock = 'Inventory/InventoryStock/AddInventoryStock.aspx';
    systemConfig.pageURLs.SalesOrderList = 'Sales/SalesOrder/SalesOrdersList.aspx';
    systemConfig.pageURLs.AddSalesOrder = 'Sales/SalesOrder/AddSalesOrder.aspx';
    systemConfig.pageURLs.AddOrderDeliveredItems = 'Sales/OrderDeliveredItems/AddOrderDeliveredItems.aspx';
    systemConfig.pageURLs.QuotationReport = 'Reports/SalesQuotationReport/QuotationReport.aspx';
    systemConfig.pageURLs.FixedAssetsLists = 'NewCRM/FixedAssets/FixedAssetsList.aspx';
    systemConfig.pageURLs.ReportExportPage = 'NewReports/ExportReports.aspx?';
    systemConfig.pageURLs.GLIncomeStatementsReport = 'NewReports/GLAccount/GLIncomeStatements.aspx';

    systemConfig.WebKeys.Employee = 21;
    systemConfig.WebKeys.CustomerReferral = 22;
    systemConfig.WebKeys.Scouting = 69;
    systemConfig.WebKeys.StageLead = 118;
    systemConfig.WebKeys.SaleQuoted = 119;
    systemConfig.WebKeys.SaleLost = 120;
    systemConfig.WebKeys.SaleApproved = 121;
    systemConfig.WebKeys.SaleReserved = 122;
    systemConfig.WebKeys.SaleDelivered = 123;
    systemConfig.WebKeys.SaleDesign = 124;
    systemConfig.WebKeys.SaleTechnicalSubmitted = 125;
    systemConfig.WebKeys.SaleCompleted = 571;
    systemConfig.WebKeys.Ordered = 669;
    systemConfig.WebKeys.SaleInProgress = 126;
    systemConfig.WebKeys.SaleClosed = 127;
    systemConfig.WebKeys.AddStock = 681;
    systemConfig.WebKeys.MoveStock = 682;
    systemConfig.WebKeys.RemoveStock = 683;
    systemConfig.WebKeys.KhaldaStockId = 684;
    systemConfig.WebKeys.BondedStockId = 685;
    //Ruba Al-Sa'di 10/4/2014 - Inventory Control - PriceList
    systemConfig.WebKeys.VolumeDiscounts = 153;
    systemConfig.WebKeys.CustomerType = 152;
    systemConfig.WebKeys.NoRounding = 141;
    systemConfig.dataTypes.CustomerClass = 50;
    systemConfig.WebKeys.PriceByQuantity = 132;
    systemConfig.WebKeys.SellingPriceBasedOnDiscount = 144;
    systemConfig.WebKeys.DiscountOnPriceByPercentage = 150;
    systemConfig.WebKeys.ItemClassType = 28;

    systemConfig.dataTypes.CompanyStatus = 76;
    systemConfig.dataTypes.Segment = 77;
    systemConfig.dataTypes.InterestedIn = 78;
    systemConfig.dataTypes.EmployeeNumber = 79;
    systemConfig.dataTypes.YearlyRevenue = 80;
    systemConfig.dataTypes.Source = 81;

    systemConfig.dataTypes.MaritalStatus = 82;
    systemConfig.dataTypes.ContactType = 83;
    systemConfig.dataTypes.Position = 84;
    systemConfig.dataTypes.Department = 85;
    systemConfig.dataTypes.Salutation = 86;
    systemConfig.dataTypes.Gender = 87;
    systemConfig.dataTypes.Religion = 88;
    systemConfig.dataTypes.VipClass = 89;

    ///////////////////////////////////used in Snax/////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////Used in Therapedic//////////////////////////////////////////////////////////////////////////////////////

    systemConfig.dataTypes.BestTimeToContact = 98;
    systemConfig.dataTypes.ItemGroup = 91;
    systemConfig.dataTypes.CaseType = 92;
    systemConfig.dataTypes.CaseStatus = 93;
    systemConfig.dataTypes.TaskAction = 94;
    systemConfig.dataTypes.VisitType = 104;

    systemConfig.WebKeys.En = 'en';
    systemConfig.WebKeys.Ar = 'ar';
    systemConfig.WebKeys.CustomerId = '3';
    systemConfig.WebKeys.EndCustomer = '519';
    systemConfig.WebKeys.CompanySupplierTypeId = '2';

    systemConfig.WebKeys.MeetingTask = 450;
    systemConfig.WebKeys.Rejected = 543;
    systemConfig.WebKeys.Pending = 540;
    systemConfig.WebKeys.AutoCompleteControl = '1';
    systemConfig.WebKeys.TextBoxControl = '2';
    systemConfig.WebKeys.TextAreaControl = '3';
    systemConfig.WebKeys.RadioButton = '4';
    systemConfig.WebKeys.CheckBox = '5';
    systemConfig.WebKeys.CompanyEntityId = '5';
    systemConfig.WebKeys.PersonEntityId = '6';
    systemConfig.WebKeys.ClientCareEntityId = '4';
    systemConfig.WebKeys.Logged = '438';
    systemConfig.WebKeys.Closed = '439';
    systemConfig.WebKeys.Confirmed = '440';
    systemConfig.WebKeys.InProgress = '442';
    systemConfig.WebKeys.Completed = '444';
    systemConfig.WebKeys.AdministrationStatusKey = '80';
    systemConfig.WebKeys.OpportunityDocumentsURL = 'http://localhost/Centrix.InventoryModule.Sales.Web/SavedDocument/Opportunity/';
    systemConfig.WebKeys.MobileType = '3';
    systemConfig.WebKeys.LandLineType = '1';
    systemConfig.WebKeys.FasxType = '2';
    systemConfig.WebKeys.Repair = '539';
    systemConfig.WebKeys.Complaint = '538';
    systemConfig.WebKeys.ReportPermission = '262';
    systemConfig.WebKeys.BusinessEmail = 1;
    systemConfig.WebKeys.PersonalEmail = 2;



    //    systemConfig.dataTypeContentServiceURL = 'Common/WebServices/DataTypeContentWebService.asmx/';
    //    systemConfig.ReportWebServiceURL = "Common/WebServices/ReportsWebService.asmx/";
    //    systemConfig.AddressServiceURL = 'Common/WebServices/AddressWebService.asmx/';
    //    systemConfig.PhoneServiceURL = 'Common/WebServices/PhoneWebService.asmx/';
    //    systemConfig.EmailServiceURL = 'Common/WebServices/EmailWebService.asmx/';
    //    systemConfig.NoteServiceURL = 'Common/WebServices/NoteWebService.asmx/';
    //    systemConfig.CommunicationServiceURL = 'Common/WebServices/CommunicationWebService.asmx/';
    //    systemConfig.RoleServiceURL = 'Common/WebServices/RoleWebService.asmx/';
    //    systemConfig.TeamServiceURL = 'Common/WebServices/TeamWebService.asmx/';
    //    systemConfig.DataTypeWebServiceURL = 'Common/WebServices/DataTypeWebService.asmx/';
    //systemConfig.TeamWebServiceURL = 'Common/WebServices/TeamWebService.asmx/';
    //systemConfig.RoleWebServiceURL = 'Common/WebServices/RoleWebService.asmx/';
    //systemConfig.AdvancedSearchWebServiceURL = 'Common/WebServices/AdvancedSearchWebService.asmx/';
    //systemConfig.CalendarWebServiceURL = 'Common/WebServices/CalendarWebService.asmx/';
    //systemConfig.TaskStatusWebService = 'Common/WebServices/TaskStatusWebService.asmx/';
    //systemConfig.TaskConfigWebService = 'Common/WebServices/TaskConfigWebService.asmx/';
    //systemConfig.WebServiceBaseClassURL = 'Common/WebServices/WebServiceBaseClass.cs/';
    //systemConfig.ConfigurationWebServiceURL = 'Common/WebServices/ConfigurationWebService.asmx/';
    //systemConfig.ActivityLookupWebService = 'Common/WebServices/ActivityLookupWebService.asmx/';
    //systemConfig.OnScreenNotificationWebService = 'Common/WebServices/OnScreenNotificationWebService.asmx/';
    //systemConfig.DashBoardWebServiceURL = 'Common/WebServices/DashBoardWebService.asmx/';
    //systemConfig.LockEntityWebServiceURL = 'Common/WebServices/LockEntityWebService.asmx/';
    systemConfig.EmailServiceURL = 'Common/WebServices/EmailWebService.asmx/';
    systemConfig.GroupWebService = 'Common/WebServices/GroupWebService.asmx/';
    systemConfig.EmailConfigWebService = 'Common/WebServices/EmailConfigrationWebService.asmx/';
    systemConfig.NotificationTemplateWebService = 'Common/WebServices/TemplatesWebService.asmx/';
    systemConfig.SendNotificationWebService = 'Common/WebServices/NotificationWebService.asmx/';
    systemConfig.EntityWebService = 'Common/WebServices/EntityWebService.asmx/';
    systemConfig.EmailConfigWebService = 'Common/WebServices/EmailConfigrationWebService.asmx/';
    systemConfig.SMSConfigurationWebService = 'Common/WebServices/SMSConfigurationWebService.asmx/';
    systemConfig.InventoryServiceURL = "Common/WebServices/InventoryWebService.asmx/";
    systemConfig.CategoryServiceURL = "Common/WebServices/CategoryWebService.asmx/";
    systemConfig.SubCategoryServiceURL = "Common/WebServices/SubCategoryWebService.asmx/";
    systemConfig.ReturnedItemServiceURL = "Common/WebServices/ReturnedItemWebService.asmx/";
    systemConfig.ItemServiceURL = "Common/WebServices/IC/IcItemCardWebService.asmx/";



    systemConfig.ReportSchema.CompanyListReport = 1;
    systemConfig.ReportSchema.CompanyDetailsReport = 2;
    systemConfig.ReportSchema.PersonListReport = 3;
    systemConfig.ReportSchema.PersonDetailsReport = 4;
    systemConfig.ReportSchema.SalesListReport = 7;
    systemConfig.ReportSchema.SalesDetailsReport = 8;
    systemConfig.ReportSchema.StockUpdateReport = 9;
    systemConfig.ReportSchema.StockUpdateDetailsReport = 10;
    systemConfig.ReportSchema.ReturnedItemsReport = 11;
    systemConfig.ReportSchema.ReturnedItemsDetailsReport = 12;
    systemConfig.ReportSchema.GiveAwaysListReport = 13;
    systemConfig.ReportSchema.GiveAwaysDetailsReport = 14;
    systemConfig.ReportSchema.WalkInListReport = 15;
    systemConfig.ReportSchema.WalkInDetailsReport = 16;
    systemConfig.ReportSchema.SoldPiecesListReport = 17;
    systemConfig.ReportSchema.SoldPiecesDetailsReport = 18;
    systemConfig.ReportSchema.CalenderListReport = 19;
    systemConfig.ReportSchema.CommunicationHistoryReport = 20;
    systemConfig.ReportSchema.NoCommunicationCompanyList = 21;
    systemConfig.ReportSchema.NoCommunicationPersonList = 22;
    systemConfig.ReportSchema.ProjectsTotal = 23;
    systemConfig.ReportSchema.DeliveryNote = 24;
    systemConfig.ReportSchema.ProjectPerSalesMan = 25;
    systemConfig.ReportSchema.ProjectTrackingReport = 26;
    systemConfig.ReportSchema.CustomerReservedItem = 29;
    systemConfig.ReportSchema.SchedualTask = 5;
    systemConfig.ReportSchema.SchedualTaskDetailesReport = 6;
    systemConfig.ReportSchema.FixedAssetsReport = 30;
    systemConfig.ReportSchema.FixedAssetsDetailsSchemaId = 31;
    systemConfig.ReportSchema.ExpiredFixedAssetsReport = 32;

    systemConfig.pageURLs.AddUser = 'NewUserManagement/Users/AddUsers.aspx';
    systemConfig.pageURLs.UserList = 'NewUserManagement/Users/UsersList.aspx';
    systemConfig.pageURLs.AddUsers = 'NewUserManagement/Users/AddUser.aspx';
    systemConfig.pageURLs.ViewUser = 'NewUserManagement/Users/ViewUser.aspx';
    systemConfig.pageURLs.LOVs = 'Common/LOVs/LOVs.aspx';
    systemConfig.pageURLs.TeamsList = 'NewUserManagement/Teams/TeamsList.aspx';
    systemConfig.pageURLs.RolesList = 'NewUserManagement/Roles/RolesList.aspx';
    systemConfig.pageURLs.RolePermissions = '/NewUserManagement/Roles/RolePermission.aspx';
    systemConfig.pageURLs.CalendarURL = '/NewCRM/Task/Calendar.aspx';
    systemConfig.pageURLs.AddGroup = '/Common/Groups/AddEditGroup.aspx';
    systemConfig.pageURLs.ListGroup = '/Common/Groups/GroupList.aspx';
    systemConfig.pageURLs.GroupContacts = '/Common/Groups/GroupContacts.aspx';
    systemConfig.pageURLs.EmailConfigList = '/NewNotifications/EmailConfiguration/EmailList.aspx';
    systemConfig.pageURLs.AddEmailConfig = '/NewNotifications/EmailConfiguration/AddEmail.aspx';
    systemConfig.pageURLs.SMSConfigurationListPage = 'NewNotifications/SMSConfiguration/SMSConfigurationList.aspx';
    systemConfig.pageURLs.AddSMSConfigurationPage = 'NewNotifications/SMSConfiguration/AddSMSConfiguration.aspx';
    systemConfig.pageURLs.TemplateList = '/NewNotifications/NotificationsTemplate/TemplateList.aspx';
    systemConfig.pageURLs.AddEditTemplate = '/NewNotifications/NotificationsTemplate/AddEditTemplate.aspx';
    systemConfig.pageURLs.InventoryList = 'Inventory/Inventory/InventoryList.aspx';
    systemConfig.pageURLs.AddInventory = 'Inventory/Inventory/AddInventory.aspx';
    systemConfig.pageURLs.CategoryList = 'Common/Category/CategoryList.aspx';
    systemConfig.pageURLs.AddCategory = 'Common/Category/AddCategory.aspx';
    systemConfig.pageURLs.SubCategoryList = 'Common/SubCategory/SubCategoryList.aspx';
    systemConfig.pageURLs.AddSubCategory = 'Common/SubCategory/AddSubCategory.aspx';
    systemConfig.pageURLs.ReturnedItemList = 'Sales/ReturnedItem/ReturnedItemList.aspx';
    systemConfig.pageURLs.AddReturnedItem = 'Sales/ReturnedItem/AddReturnedItem.aspx';
    systemConfig.pageURLs.FixedAssetsList = 'NewCRM/FixedAssets/FixedAssetsList.aspx';
    systemConfig.pageURLs.AddFixedAssets = 'NewCRM/FixedAssets/AddFixedAssets.aspx';
    systemConfig.pageURLs.TaskList = 'NewCRM/Task/TasksList.aspx';

    systemConfig.HTMLTemplateURLs.ContactsGroupListItem = '/Common/Templates/Groups/GroupListItem.html';
    systemConfig.HTMLTemplateURLs.GroupListViewItem = '/Common/Templates/Groups/GroupListViewItem.html';
    systemConfig.HTMLTemplateURLs.NotificationsTemplateListItem = 'NewNotifications/Template/NotificationsTemplate/TemplateListItem.html';
    systemConfig.HTMLTemplateURLs.NotificationsTemplateListViewItem = 'NewNotifications/Template/NotificationsTemplate/TemplateListViewItem.html';
    systemConfig.HTMLTemplateURLs.SMSConfigurationListItemTemplate = 'NewNotifications/Template/SMSConfiguration/SMSConfigurationListItem.html';
    systemConfig.HTMLTemplateURLs.SMSConfigurationListViewItemTemplate = 'NewNotifications/Template/SMSConfiguration/SMSConfigurationListViewItem.html';

    systemConfig.HTMLTemplateURLs.UserListItemTemplate = 'NewUserManagement/Template/Users/UsersListItem.html';
    systemConfig.HTMLTemplateURLs.UserListViewItemTemplate = 'NewUserManagement/Template/Users/UsersListViewItem.html';
    systemConfig.HTMLTemplateURLs.DataTypeContentItemTemplate = 'Common/Templates/LOVs/DataTypeContentItem.html';
    systemConfig.HTMLTemplateURLs.DataTypeContentListItemsTemplate = 'Common/Templates/LOVs/DataTypeContentListItems.html';
    systemConfig.HTMLTemplateURLs.TeamItemTemplate = 'NewUserManagement/Template/Teams/TeamItem.html';
    systemConfig.HTMLTemplateURLs.RoleItemTemplate = 'NewUserManagement/Template/Roles/RoleItem.html';
    systemConfig.HTMLTemplateURLs.AdvancedSearchControlsTemplate = 'Common/Templates/AdvancedSearch/AdvancedSearchControlsTemplate.html';
    systemConfig.HTMLTemplateURLs.HistoryUpdatedListViewItemTemplate = 'Common/Templates/HistoryUpdatedListViewTemplate.html';
    systemConfig.HTMLTemplateURLs.QuickSearchTemplate = 'Common/Templates/AdvancedSearch/QuickSearchTemplate.html';
    systemConfig.HTMLTemplateURLs.RolePermissionTemplate = 'NewUserManagement/Template/Roles/RolePermissionTemplate.htm';
    systemConfig.HTMLTemplateURLs.TaskListViewItem = 'Common/Templates/Task/TaskListViewItem.html';
    systemConfig.HTMLTemplateURLs.SubTaskListViewItem = 'Common/Templates/Task/SubTaskListViewItem.html';
    systemConfig.HTMLTemplateURLs.TaskDiscussionViewItem = 'Common/Templates/Task/TaskDiscussionViewItem.html';
    systemConfig.HTMLTemplateURLs.CommunicationTabTemplate = 'Common/Templates/Task/CommunicationListItem.html';
    systemConfig.HTMLTemplateURLs.EntityLockListViewItemTemplate = '/Common/Templates/EntityLock/EntityLockListViewItem.html';
    systemConfig.HTMLTemplateURLs.EntityLockRowItem = '/Common/Templates/EntityLock/EntityLockRow.html';
    systemConfig.HTMLTemplateURLs.EmailConfigListItem = '/NewNotifications/Template/EmailConfig/EmailConfigListItem.html';
    systemConfig.HTMLTemplateURLs.EmailConfigpListViewItem = '/NewNotifications/Template/EmailConfig/EmailConfigListViewItem.html';
    systemConfig.HTMLTemplateURLs.InventoryListViewItemTemplate = "Inventory/Template/Inventory/InventoryListViewItem.html";
    systemConfig.HTMLTemplateURLs.InventoryListItemTemplate = "Inventory/Template/Inventory/InventoryListItem.html";
    systemConfig.HTMLTemplateURLs.CategoryListViewItemTemplate = "Common/Templates/Category/CategoryListViewItem.html";
    systemConfig.HTMLTemplateURLs.CategoryListItemTemplate = "Common/Templates/Category/CategoryListItem.html";
    systemConfig.HTMLTemplateURLs.SubCategoryListViewItemTemplate = "Common/Templates/SubCategory/SubCategoryListViewItem.html";
    systemConfig.HTMLTemplateURLs.ReturnedItemListViewItemTemplate = "Sales/Template/ReturnedItem/ReturnedItemListViewItem.html";
    systemConfig.HTMLTemplateURLs.ReturnedItemListItemTemplate = "Sales/Template/ReturnedItem/ReturnedItemListItem.html";
    systemConfig.HTMLTemplateURLs.RelatedOpportunities = "NewCRM/Template/CommonRelatedTab/RelatedOpportunities.html";
    systemConfig.HTMLTemplateURLs.FixedAssetsListItem = "NewCRM/Template/FixedAssets/FixedAssetsListItem.html";
    systemConfig.HTMLTemplateURLs.FixedAssetsListViewItem = "NewCRM/Template/FixedAssets/FixedAssetsListViewItem.html";

    /*systemConfig.dataTypes.Segment = 2;
    systemConfig.dataTypes.Source = 3;
    systemConfig.dataTypes.Salutation = 4;
    systemConfig.dataTypes.Gender = 5;
    systemConfig.dataTypes.MaritalStatus = 6;
    systemConfig.dataTypes.Religion = 7;
    systemConfig.dataTypes.Department = 8;
    systemConfig.dataTypes.Position = 9;
    systemConfig.dataTypes.CompanyCategory = 10;

    systemConfig.dataTypes.LeadStage = 13;
    systemConfig.dataTypes.LeadStatus = 14;
    systemConfig.dataTypes.Priority = 15;
    systemConfig.dataTypes.Rating = 16;
    systemConfig.dataTypes.AnnualRevenues = 17;
    systemConfig.dataTypes.NoOfEmployee = 18;
    systemConfig.dataTypes.Industry = 19;
    systemConfig.dataTypes.OpportunityType = 22;
    systemConfig.dataTypes.Currency = 23;
    systemConfig.dataTypes.RealtedOpportunityType = 24;
    systemConfig.dataTypes.ProjectProduct = 28;
    systemConfig.dataTypes.ProjectStatus = 29;
    systemConfig.dataTypes.ClassType = 31;
    systemConfig.dataTypes.DateMatched = 32;
    systemConfig.dataTypes.RateOperation = 33;
    systemConfig.dataTypes.PrizesTypes = 34;
    systemConfig.dataTypes.Module = 35;
    systemConfig.dataTypes.FilterType = 36;
    systemConfig.dataTypes.PersonRelations = 37;
    systemConfig.dataTypes.scheduleTaskType = 38;
    systemConfig.dataTypes.CompanyOwner = 39;
    systemConfig.dataTypes.ClearanceLevel = 40;
    systemConfig.dataTypes.CommunicationMedium = 41;
    systemConfig.dataTypes.Frequency = 42;
    systemConfig.dataTypes.ProductStatus = 43;
    systemConfig.dataTypes.ProductItemType = 44;
    systemConfig.dataTypes.CurrencySymbol = 45;
    systemConfig.dataTypes.CloseReason = 46;
    systemConfig.dataTypes.SuspendReason = 47;
    systemConfig.dataTypes.QuotationType = 48;
    systemConfig.dataTypes.QuotationCategory = 49;
    systemConfig.dataTypes.QuotationStatus = 50;
    systemConfig.dataTypes.siteInternetConnectivity = 51;
    systemConfig.dataTypes.customerGroup = 52;
    systemConfig.dataTypes.vendorGroup = 53;
    systemConfig.dataTypes.supplierCategory = 54;
    systemConfig.dataTypes.paymentCode = 55;
    systemConfig.dataTypes.paymentTerm = 56;
    systemConfig.dataTypes.CaseStage = 59;
    systemConfig.dataTypes.CaseSolutionType = 60;
    systemConfig.dataTypes.CaseAccessDevice = 61;
    systemConfig.dataTypes.CaseInternetConnection = 62;
    systemConfig.dataTypes.CaseMobileAccessType = 63;
    systemConfig.dataTypes.CaseAccessDeviceType = 64;
    systemConfig.dataTypes.SecurityGroupType = 65;
    systemConfig.dataTypes.SelfServiceUserType = 66;
    systemConfig.dataTypes.PaymentTypes = 67;
    systemConfig.dataTypes.PaymentStatus = 68;
    systemConfig.dataTypes.RequestedResponseMethod = 69;
    systemConfig.dataTypes.Browser = 70;
    systemConfig.dataTypes.TimeZone = 71;
    systemConfig.dataTypes.spicWorkOrderType = 72;
    systemConfig.dataTypes.spicWorkOrderSurveyType = 73;
    systemConfig.dataTypes.spicWorkOrderStage = 74;
    systemConfig.dataTypes.TaskPriority = 75;
    systemConfig.dataTypes.TaskType = 76
    systemConfig.dataTypes.vatRate = 84;
    systemConfig.dataTypes.Branch = 86;
    systemConfig.dataTypes.SalesRepresentative = 87;
    systemConfig.dataTypes.GiveAway = 88;
    systemConfig.dataTypes.InventoryStatus = 90;*/


    systemConfig.TemplateType.EmailTemplateTypeId = 2;
    systemConfig.TemplateType.SMSTemplateTypeId = 3;
    systemConfig.TemplateType.TermsAndConditionTemplateTypeId = 1;

    systemConfig.ApplicationURL_Common = ApplicationURL_Common;
    systemConfig.ApplicationURL = ApplicationURL_Common;
    PemissionWebServiceURL = systemConfig.ApplicationURL_Common + systemConfig.ConfigurationWebServiceURL;



    systemConfig.dataTypes.InterestedIn = 112;
    systemConfig.dataTypes.ContactType = 113;

    systemConfig.WebKeys.BusinessEmail = 1;
    systemConfig.WebKeys.PersonalEmail = 2;

    systemConfig.WebKeys.MobieType = 3;

    systemConfig.IcUnitOfMeasuresServiceURL = "Common/WebServices/IC/IcUnitOfMeasuresWebService.asmx/";
    systemConfig.HTMLTemplateURLs.IcUnitOfMeasuresListViewItemTemplate = "Inventory/Template/IcUnitOfMeasures/IcUnitOfMeasuresListViewItem.html";
    systemConfig.HTMLTemplateURLs.IcUnitOfMeasuresListItemTemplate = "Inventory/Template/IcUnitOfMeasures/IcUnitOfMeasuresListItem.html";
    systemConfig.HTMLTemplateURLs.IcUnitOfMeasuresAddTemplate = "Inventory/Template/IcUnitOfMeasures/IcUnitOfMeasereAddTemplate.html";
    systemConfig.pageURLs.IcUnitOfMeasuresList = 'Inventory/UnitOfMeasures/IcUnitOfMeasuresList.aspx';

    systemConfig.IcWieghtUnitOfMeasuresServiceURL = "Common/WebServices/IC/IcWieghtUnitOfMeasuresWebService.asmx/";
    systemConfig.HTMLTemplateURLs.IcWieghtUnitOfMeasuresListViewItemTemplate = "Inventory/Template/IcWieghtUnitOfMeasures/IcWieghtUnitOfMeasuresListViewItem.html";
    systemConfig.HTMLTemplateURLs.IcWieghtUnitOfMeasuresAddTemplate = "Inventory/Template/IcWieghtUnitOfMeasures/IcWieghtUnitOfMeasureAddTemplate.html";
    systemConfig.HTMLTemplateURLs.IcWieghtUnitOfMeasuresListItemTemplate = "Inventory/Template/IcWieghtUnitOfMeasures/IcWieghtUnitOfMeasuresListItem.html";
    systemConfig.pageURLs.IcWieghtUnitOfMeasuresList = 'Inventory/WieghtUnitOfMeasures/IcWieghtUnitOfMeasuresList.aspx';
    systemConfig.IcLocationServiceURL = "Common/WebServices/IC/IcLocationWebService.asmx/";
    systemConfig.HTMLTemplateURLs.IcLocationListViewItemTemplate = "Inventory/Template/IcLocation/IcLocationListViewItem.html";
    systemConfig.HTMLTemplateURLs.IcLocationListItemTemplate = "Inventory/Template/IcLocation/IcLocationListItem.html";
    systemConfig.HTMLTemplateURLs.IcLocationAddItemTemplate = "Inventory/Template/IcLocation/IcLocationAddtem.html";
    systemConfig.pageURLs.IcLocationList = 'Inventory/Location/IcLocationList.aspx';
    systemConfig.dataTypes.LocationType = 42;
    systemConfig.IcWarrantyInfoServiceURL = "Common/WebServices/IC/IcWarrantyInfoWebService.asmx/";
    systemConfig.HTMLTemplateURLs.IcWarrantyInfoListViewItemTemplate = "Inventory/Template/IcWarrantyInfo/IcWarrantyInfoListViewItem.html";
    systemConfig.HTMLTemplateURLs.IcWarrantyInfoListItemTemplate = "Inventory/Template/IcWarrantyInfo/IcWarrantyInfoListItem.html";
    systemConfig.HTMLTemplateURLs.IcWarrantyInfoAddtemTemplate = "Inventory/Template/IcWarrantyInfo/IcWarrantyInfoAddtem.html";
    systemConfig.pageURLs.IcWarrantyInfoList = 'Inventory/IcWarrantyInfo/IcWarrantyInfoList.aspx';
    systemConfig.HTMLTemplateURLs.IcSegmentListViewRowTamplate = "Inventory/Template/Segment/SegmentListViewItem.html";
    systemConfig.IcSegmentServiceURL = "Common/WebServices/IC/SegmentWebService.asmx/";
    systemConfig.HTMLTemplateURLs.IcSegmentCodeRowTemplate = "Inventory/Template/SegmentCode/SegmentCodeRowTemplate.html";
    systemConfig.IcSegmentCodeServiceURL = "Common/WebServices/IC/SegmentCodeWebService.asmx/";
    systemConfig.ItemStructureDelimiterWebService = '/'
    systemConfig.ItemStructureServiceURL = "Common/WebServices/IC/ItemStructureWebService.asmx/";
    systemConfig.HTMLTemplateURLs.ItemStructureListViewItemTemplate = "Inventory/Template/ItemStructure/ItemStructureListViewItem.html";
    systemConfig.HTMLTemplateURLs.ItemStructureListItemTemplate = "Inventory/Template/ItemStructure/ItemStructureListItem.html";
    systemConfig.HTMLTemplateURLs.ItemStructureAddItemTemplate = "Inventory/Template/ItemStructure/ItemStructureAddItem.html"
    systemConfig.pageURLs.ItemStructureList = 'Inventory/ItemStructure/ItemStructureList.aspx';
    systemConfig.dataTypes.ItemStructureDelimiter = 43;
    systemConfig.ItemStructureSegmentServiceURL = "Common/WebServices/IC/ItemStructureSegmentWebService.asmx/";
    ///////////////////////////////////// IcItemCard Configuration//////////////////////////////////////////////////////////
    systemConfig.CoastingMethodWebService = '/'
    systemConfig.WeightUnitOfMeasureIdWebService = 'Common/WebServices/IcWieghtUnitOfMeasuresWebService.asmx//'
    systemConfig.IcItemCardServiceURL = "Common/WebServices/IC/IcItemCardWebService.asmx/";
    systemConfig.HTMLTemplateURLs.IcItemCardListViewItemTemplate = "Inventory/Template/IcItemCard/IcItemCardListViewItem.html";
    systemConfig.HTMLTemplateURLs.IcItemCardListItemTemplate = "Inventory/Template/IcItemCard/IcItemCardListItem.html";
    systemConfig.HTMLTemplateURLs.IcItemCardAddItemTemplate = "Inventory/Template/IcItemCard/IcItemCardAddItem.html";
    systemConfig.pageURLs.IcItemCardList = 'Inventory/ItemCard/IcItemCardList.aspx';
    systemConfig.pageURLs.AddIcItemCard = 'Inventory/IcItemCard/AddIcItemCard.aspx';
    systemConfig.dataTypes.CoastingMethod = 58;
    systemConfig.HTMLTemplateURLs.ItemUnitOfMeasures = 'Inventory/Template/ItemUnitOfMeasures/ItemUnitOfMeasure.html';
    systemConfig.ItemUnitOfMeasuresServiceURL = "Common/WebServices/IC/IcItemUnitOfMeasuresWebService.asmx/";
    systemConfig.HTMLTemplateURLs.ItemTaxTemplate = 'Inventory/Template/ItemTax/ItemTax.html';
    systemConfig.IcItemTaxServiceURL = "Common/WebServices/IC/IcItemTaxWebService.asmx/";
    systemConfig.dataTypes.PurchasingTaxClass = 24;
    systemConfig.dataTypes.SalesTaxClass = 25;
    systemConfig.HTMLTemplateURLs.ItemLocationCostingTemplate = 'Inventory/Template/ItemLocationCosting/ItemLocationCosting.html';
    systemConfig.LocationCostingWebService = 'Common/WebServices/IC/IcItemLocationCostingWebService.asmx/'
    //24-4-2014
    systemConfig.HTMLTemplateURLs.ItemLocationCostingListViewTemplate = 'Inventory/Template/ItemLocationCosting/ItemLocationCostingListViewItem.html';
    systemConfig.HTMLTemplateURLs.IcItemLocationCostingAddItemTemplate = 'Inventory/Template/ItemLocationCosting/IcItemLocationCostingAddItem.html';
    systemConfig.HTMLTemplateURLs.IcItemLocationCostingListItemTemplate = 'Inventory/Template/ItemLocationCosting/IcItemLocationCostingListItem.html';


    ///////////////////////////////////// IcPriceList Configuration//////////////////////////////////////////////////////////    
    systemConfig.IcPriceListServiceURL = "Common/WebServices/IC/IcPriceListWebService.asmx/";
    systemConfig.HTMLTemplateURLs.IcPriceListListViewItemTemplate = "Inventory/Template/IcPriceList/IcPriceListListViewItem.html";
    systemConfig.HTMLTemplateURLs.IcPriceListListItemTemplate = "Inventory/Template/IcPriceList/IcPriceListListItem.html";
    systemConfig.HTMLTemplateURLs.IcPriceListAddItemTemplate = "Inventory/Template/IcPriceList/IcPriceListAddItem.html";
    systemConfig.dataTypes.PriceBy = 44;
    systemConfig.dataTypes.PriceDecimals = 45;
    systemConfig.dataTypes.RoundingMethod = 46;
    systemConfig.dataTypes.SellingPriceBasedOn = 47;
    systemConfig.dataTypes.DiscountOnPriceBasedOn = 48;
    systemConfig.dataTypes.PricingDeterminedBy = 49;
    systemConfig.WebKeys.PricingDeterminedByCustomerType = 152;
    systemConfig.HTMLTemplateURLs.PriceListTaxTemplate = "Inventory/Template/PriceListTax/PriceListTax.html";
    systemConfig.IcPriceListTaxServiceURL = "Common/WebServices/IC/IcPriceListTaxWebService.asmx/";

    //Ruba Al-Sa'di 14/4/2014 - IC
    systemConfig.pageURLs.PriceListList = 'Inventory/PriceList/IcPriceListList.aspx';
    systemConfig.pageURLs.ItemCardList = 'Inventory/ItemCard/IcItemCardList.aspx';
    systemConfig.HTMLTemplateURLs.PriceListDiscountTemplate = "Inventory/Template/IcPriceList/IcPriceListDiscountListItem.html";
    systemConfig.IcPriceListDiscountsServiceURL = "Common/WebServices/IC/IcPriceListDiscountWebService.asmx/";

    systemConfig.IcOptionsServiceURL = "Common/WebServices/IC/IcOptionsWebService.asmx/";
    systemConfig.HTMLTemplateURLs.IcOptionsViewTemplate = "Inventory/Template/IcOptions/IcOptionsListItem.html";
    systemConfig.dataTypes.RateType = 52;
    systemConfig.dataTypes.AdditionalCostAction = 53;
    systemConfig.dataTypes.PostingDate = 54;
    systemConfig.dataTypes.AccumulateBy = 55;
    systemConfig.dataTypes.PeriodType = 56;
    systemConfig.dataTypes.Duration = 57;

    systemConfig.IcDocumentNumbersServiceURL = "Common/WebServices/IC/IcDocumentNumbersWebService.asmx/";
    systemConfig.HTMLTemplateURLs.IcDocumentNumbersViewTemplate = "Inventory/Template/IcOptions/IcDocumentNumberListViewItem.html";
    systemConfig.HTMLTemplateURLs.IcDocumentNumbersRowTemplate = "Inventory/Template/IcOptions/IcDocumentNumbersRow.html";

    systemConfig.IcPriceListGroupServiceURL = "Common/WebServices/IC/IcPriceListGroupWebService.asmx/";
    systemConfig.HTMLTemplateURLs.IcPriceListGroupListViewItemTemplate = "Inventory/Template/IcPriceListGroup/IcPriceListGroupListViewItem.html";
    systemConfig.HTMLTemplateURLs.IcPriceListGroupListItemTemplate = "Inventory/Template/IcPriceListGroup/IcPriceListGroupListItem.html";
    systemConfig.pageURLs.IcPriceListGroupList = 'Inventory/PriceListGroup/IcPriceListGroupList.aspx';
    systemConfig.HTMLTemplateURLs.AddIcPriceListGroup = "Inventory/Template/IcPriceListGroup/IcPriceListGroupAddItem.html";
    systemConfig.dataTypes.BasePriceType = 66;
    systemConfig.dataTypes.CostBaseType = 67;
    systemConfig.WebKeys.BasePriceType_FixedPrice = 214;
    systemConfig.WebKeys.BasePriceType_CalculatedUsingCost = 215;

    systemConfig.dataTypes.SalePriceType = 159;

    systemConfig.dataTypes.CostingMethod = 51;

    systemConfig.IcAccountSetServiceURL = "Common/WebServices/IC/IcAccountSetWebService.asmx/";
    systemConfig.HTMLTemplateURLs.IcAccountSetListViewItemTemplate = "Inventory/Template/IcAccountSet/IcAccountSetListViewItem.html";
    systemConfig.HTMLTemplateURLs.IcAccountSetListItemTemplate = "Inventory/Template/IcAccountSet/IcAccountSetListItem.html";
    systemConfig.HTMLTemplateURLs.AddIcAccountSet = "Inventory/Template/IcAccountSet/IcAccountSetAddItem.html";
    systemConfig.pageURLs.IcAccountSetList = 'Inventory/AccountSet/IcAccountSetList.aspx';


    systemConfig.IcPriceListGroupTaxServiceURL = "Common/WebServices/IC/IcPriceListGroupTaxWebService.asmx/";
    systemConfig.HTMLTemplateURLs.PriceListGroupTaxTemplate = "Inventory/Template/PriceListGroupTax/PriceListGroupTax.html";

    systemConfig.IcPriceListGroupDiscountServiceURL = "Common/WebServices/IC/IcPriceListGroupDiscountWebService.asmx/";
    systemConfig.HTMLTemplateURLs.PriceListGroupDiscountTemplate = "Inventory/Template/PriceListGroupDiscount/PriceListGroupDiscount.html";
    systemConfig.IcPriceListGroupDiscountValueServiceURL = "Common/WebServices/IC/IcPriceListGroupDiscountValueWebService.asmx/";
    systemConfig.HTMLTemplateURLs.PriceListGroupDiscountValueTemplate = "Inventory/Template/PriceListGroupDiscount/PriceListGroupDiscountValueRow.html";

    systemConfig.IcCategoryServiceURL = "Common/WebServices/IC/IcCategoryWebService.asmx/";
    systemConfig.pageURLs.IcCategoryList = 'Inventory/Category/IcCategoryList.aspx';
    systemConfig.HTMLTemplateURLs.IcCategoryListViewItemTemplate = "Inventory/Template/IcCategory/IcCategoryListViewItem.html";
    systemConfig.HTMLTemplateURLs.IcCategoryListItemTemplate = "Inventory/Template/IcCategory/IcCategoryListItem.html";
    systemConfig.HTMLTemplateURLs.AddIcCategory = "Inventory/Template/IcCategory/IcCategoryAddItem.html";


    systemConfig.IcPriceListGroupItemServiceURL = "Common/WebServices/IC/IcPriceListGroupItemWebService.asmx/";
    systemConfig.HTMLTemplateURLs.PriceListGroupItemTemplate = "Inventory/Template/PriceListGroupItem/PriceListGroupItem.html";

    ////////////////////////////Inventory Control Transactions////////////////////////////////////
    systemConfig.InventoryErrors.GLAccountsDoesNotExist = 600;
    systemConfig.InventoryErrors.RecordExists = 501;
    systemConfig.InventoryErrors.AutomaticInvoiceNotCreated = 601;
    systemConfig.InventoryErrors.LotPostingErrors = 602;

    ////////////////////////////////////////// Receipt //////////////////////////////////////////
    systemConfig.IcReceiptServiceURL = "Common/WebServices/IC/IcReceiptWebService.asmx/";
    systemConfig.pageURLs.IcReceiptList = 'Inventory/Receipt/IcReceiptList.aspx';
    systemConfig.HTMLTemplateURLs.IcReceiptListViewItemTemplate = "Inventory/Template/IcReceipt/IcReceiptListViewItem.html";
    systemConfig.HTMLTemplateURLs.IcReceiptListItemTemplate = "Inventory/Template/IcReceipt/IcReceiptListItem.html";
    systemConfig.HTMLTemplateURLs.AddIcReceipt = "Inventory/Template/IcReceipt/IcReceiptAddItem.html";
    systemConfig.dataTypes.IcReceiptType = 59;
    systemConfig.HTMLTemplateURLs.ReceiptDetailTemplate = "Inventory/Template/IcReceipt/IcReceiptDetailListItem.html";
    systemConfig.dataTypes.ReturnPostType = 187; // 1051;
    systemConfig.dataTypes.AdjustmentPostType = 188; // 1052;
    systemConfig.dataTypes.CompletePostType = 189; // 1053;

    ////////////////////////////////////////// Shipment //////////////////////////////////////////
    systemConfig.HTMLTemplateURLs.AddIcShipment = "Inventory/Template/Shipment/IcShipmentAddItem.html";
    systemConfig.pageURLs.IcShipmentList = 'Inventory/Shipment/IcShipmentList.aspx';
    systemConfig.dataTypes.IcShipmentType = 60;
    systemConfig.HTMLTemplateURLs.ShipmentDetailsTemplate = 'Inventory/Template/Shipment/IcShipmentDetailListItem.html';
    systemConfig.IcShipmentServiceURL = 'Common/WebServices/IC/IcShipmentWebService.asmx/';
    systemConfig.HTMLTemplateURLs.IcShipmentListViewItemTemplate = 'Inventory/Template/Shipment/IcShipmentListViewItem.html';
    systemConfig.HTMLTemplateURLs.IcShipmentListItemTemplate = 'Inventory/Template/Shipment/IcShipmentListItem.html';
    systemConfig.dataTypes.ShipmmentType = 190; // 1054;
    systemConfig.dataTypes.ReturnShipmentType = 191; // 1055;

    ////////////////////////////////////////// Adjustment //////////////////////////////////////////
    systemConfig.HTMLTemplateURLs.AddIcAdjustment = "Inventory/Template/IcAdjustment/IcAdjustmentAddItem.html";
    systemConfig.pageURLs.IcAdjustmentList = 'Inventory/Adjustment/IcAdjustmentList.aspx';
    systemConfig.HTMLTemplateURLs.AdjustmentDetailsTemplate = 'Inventory/Template/IcAdjustment/IcAdjustmentDetailListItem.html';
    systemConfig.IcAdjustmentServiceURL = 'Common/WebServices/IC/IcAdjustmentWebService.asmx/';
    systemConfig.HTMLTemplateURLs.IcAdjustmentListViewItemTemplate = 'Inventory/Template/IcAdjustment/IcAdjustmentListViewItem.html';
    systemConfig.HTMLTemplateURLs.IcAdjustmentListItemTemplate = 'Inventory/Template/IcAdjustment/IcAdjustmentListItem.html';
    systemConfig.dataTypes.AdjustmentType = 63;
    systemConfig.dataTypes.QuantityIncreaseAdjustmentType = 200;
    systemConfig.dataTypes.QuantityDecreaseAdjustmentType = 201;
    systemConfig.dataTypes.CostIncreaseAdjustmentType = 202;
    systemConfig.dataTypes.CostDecreaseAdjustmentType = 203;
    systemConfig.dataTypes.BothIncreaseAdjustmentType = 204;
    systemConfig.dataTypes.BothDecreaseAdjustmentType = 205;

    systemConfig.dataTypes.FIFOCostingMethod = 160;
    systemConfig.dataTypes.LIFOCostingMethod = 161;
    systemConfig.dataTypes.MovingAverageCostingMethod = 162;
    systemConfig.dataTypes.UserSpecifiedCostingMethod = 163;

    systemConfig.dataTypes.BucketType = 64;
    systemConfig.dataTypes.SpecificBucketType = 207;

    systemConfig.WebKeys.IcReceiptDocumentType = 8;
    systemConfig.WebKeys.IcTransferDocumentType = 3;
    systemConfig.WebKeys.IcTransferReceiptDocumentType = 4;

    ////////////////////////////////////////// Transfer //////////////////////////////////////////
    systemConfig.HTMLTemplateURLs.AddIcTransfer = "Inventory/Template/IcTransfer/IcTransferAddItem.html";
    systemConfig.pageURLs.IcTransferList = 'Inventory/Transfer/IcTransferList.aspx';
    systemConfig.HTMLTemplateURLs.TransferDetailsTemplate = 'Inventory/Template/IcTransfer/IcTransferDetailListItem.html';
    systemConfig.IcTransferServiceURL = 'Common/WebServices/IC/IcTransferWebService.asmx/';
    systemConfig.HTMLTemplateURLs.IcTransferListViewItemTemplate = 'Inventory/Template/IcTransfer/IcTransferListViewItem.html';
    systemConfig.HTMLTemplateURLs.IcTransferListItemTemplate = 'Inventory/Template/IcTransfer/IcTransferListItem.html';
    systemConfig.dataTypes.IcTransferType = 61;
    systemConfig.dataTypes.IcTransferProrationMethod = 62;
    systemConfig.dataTypes.IcTransferTransferType = 192;
    systemConfig.dataTypes.IcTransitTransferTransferType = 193;
    systemConfig.dataTypes.IcTransitReceiptTransferType = 194;
    systemConfig.dataTypes.IcTransferManualProrationMethod = 199;
    systemConfig.dataTypes.IcTransferEquallyProrationMethod = 198;
    systemConfig.dataTypes.UsernamePassword = 236;


    ///////////////////////////////////////////////////////////////////////
    systemConfig.dataTypes.DuringPosting = 184;
    systemConfig.dataTypes.DuringDayEndProcessing = 185;
    systemConfig.IcDayEndProcessingServiceURL = 'Common/WebServices/IC/IcDayEndProcessing.asmx/';


    ///////////////////////////////////////////////////////////////////
    systemConfig.pageURLs.IcSegmentList = 'Inventory/Segment/SegmentList.aspx';
    systemConfig.IcCommonQueriesServiceURL = "Common/WebServices/IC/IcCommonQueries.asmx/";

    /////////////////////////////// IcItemCardSerials ////////////////////////////////////
    systemConfig.HTMLTemplateURLs.IcItemCardSerialsAddTemplate = 'Inventory/Template/IcItemCardSerials/IcItemCardSerialsAdd.html';
    systemConfig.HTMLTemplateURLs.IcItemCardSerialsSelectTemplate = 'Inventory/Template/IcItemCardSerials/IcItemCardSerialsSelect.html';
    systemConfig.HTMLTemplateURLs.IcItemCardSerialsDetailsTemplate = 'Inventory/Template/IcItemCardSerials/IcItemCardSerialsDetailsTemplate.html';
    systemConfig.IcItemCardSerialsServiceURL = "Common/WebServices/IC/IcItemCardSerialWebService.asmx/";
    systemConfig.IcItemCardLotsServiceURL = "Common/WebServices/IC/IcItemCardLotWebService.asmx/";
    systemConfig.WebKeys.AvailableItemSerialStatus = 1;
    systemConfig.WebKeys.NotAvailableItemSerialStatus = 2;
    systemConfig.WebKeys.ReservedForReceiptReturnItemSerialStatus = 3;
    systemConfig.WebKeys.ReservedForShipmentItemSerialStatus = 4;
    systemConfig.WebKeys.ReservedForTransferItemSerialStatus = 5;


    systemConfig.HTMLTemplateURLs.IcItemCardLotsAddTemplate = 'Inventory/Template/IcItemCardLot/TransLotAdd.html';
    systemConfig.HTMLTemplateURLs.IcItemCardLotsSelectTemplate = 'Inventory/Template/IcItemCardLot/IcItemCardLotsSelect.html';
    systemConfig.HTMLTemplateURLs.IcItemCardLotsDetailsTemplate = 'Inventory/Template/IcItemCardLot/IcItemCardLotsDetailsTemplate.html';


    systemConfig.AssetServiceURL = "Common/WebServices/AssetWebService.asmx/";
    systemConfig.HTMLTemplateURLs.AssetListViewItemTemplate = "NewInventory/Template/Asset/AssetListViewItem.html";
    systemConfig.HTMLTemplateURLs.AssetListItemTemplate = "NewInventory/Template/Asset/AssetListItem.html";
    systemConfig.HTMLTemplateURLs.AddAssetItemTemplate = "NewInventory/Template/Asset/AddAssetItem.html";
    systemConfig.pageURLs.AssetList = 'NewInventory/Asset/AssetList.aspx';
    systemConfig.pageURLs.AddAsset = 'NewInventory/Asset/AddAsset.aspx';



    systemConfig.WebKeys.TransactionType_Receipt = 1;
    systemConfig.WebKeys.TransactionType_ReceiptReturn = 2;
    systemConfig.WebKeys.TransactionType_ReceiptAdjustment = 3;
    systemConfig.WebKeys.TransactionType_Shipment = 4;
    systemConfig.WebKeys.TransactionType_ShipmentReturn = 5;
    systemConfig.WebKeys.TransactionType_Transfer = 6;
    systemConfig.WebKeys.TransactionType_TransitTransfer = 7;
    systemConfig.WebKeys.TransactionType_TransitReceipt = 8;
    systemConfig.WebKeys.TransactionType_AdjustmentQuantityIncrease = 9;
    systemConfig.WebKeys.TransactionType_AdjustmentQuantityDecrease = 10;
    systemConfig.WebKeys.TransactionType_AdjustmentCostIncrease = 11;
    systemConfig.WebKeys.TransactionType_AdjustmentCostDecrease = 12;
    systemConfig.WebKeys.TransactionType_AdjustmentBothIncrease = 13;
    systemConfig.WebKeys.TransactionType_AdjustmentBothDecrease = 14;

    /////////////////////////////// IcItemCardSerials ////////////////////////////////////
    systemConfig.HTMLTemplateURLs.ConstructNumberFromSegmentsTemplate = "Common/Templates/ConstructNumberFromSegments/ConstructNumberFromSegments.html";
    systemConfig.HTMLTemplateURLs.ConstructNumberFromSegmentsDetailsTemplate = "Common/Templates/ConstructNumberFromSegments/ConstructNumberFromSegmentsDetails.html";


    systemConfig.HTMLTemplateURLs.IcLocationIntegrationTemplate = 'Inventory/Template/IcLocation/IcLocationIntegration.html';

    systemConfig.IcLocationIntegrationServiceURL = "Common/WebServices/IC/IcLocationIntegrationWebService.asmx/";

    systemEntities.DocumentEntityId = 2139;
    systemEntities.SystemConfigrationEntityId = 2155;

    systemEntities.DocumentEntityId = 2139;
    systemConfig.AssetServiceURL = "Common/WebServices/IC/AssetWebService.asmx/";
    systemConfig.HTMLTemplateURLs.AssetListViewItemTemplate = "Inventory/Template/Asset/AssetListViewItem.html";
    systemConfig.HTMLTemplateURLs.AssetListItemTemplate = "Inventory/Template/Asset/AssetListItem.html";
    systemConfig.HTMLTemplateURLs.AddAssetItemTemplate = "Inventory/Template/Asset/AddAssetItem.html";
    systemConfig.pageURLs.AssetList = 'NewInventory/Asset/AssetList.aspx';
    systemConfig.pageURLs.AddAsset = 'NewInventory/Asset/AddAsset.aspx';
    systemConfig.dataTypes.IcAssetCategory = 68;
    systemConfig.dataTypes.IcAssetSource = 70;
    systemConfig.dataTypes.IcAssetLocation = 69;
    systemConfig.dataTypes.GiftVoucherOverAmount = 72;
    systemConfig.dataTypes.LoginMethod = 74;
    systemConfig.dataTypes.AfterCloseRegisterValue = 73;
    systemConfig.dataTypes.ItemStructurer = 75;
    systemConfig.HTMLTemplateURLs.AssetDetailsItemTemplate = "Inventory/Template/Asset/AssetDetailsItem.html";

    systemConfig.HTMLTemplateURLs.UploadItemsLogListViewItemTemplate = "Inventory/Template/UploadItemsLog/UploadItemsLogListViewItem.html";
    systemConfig.HTMLTemplateURLs.UploadItemsLogListItemTemplate = "Inventory/Template/UploadItemsLog/UploadItemsLogListItem.html";
    systemConfig.HTMLTemplateURLs.AddUploadItemsLogItemTemplate = "Inventory/Template/UploadItemsLog/AddUploadItemsLogItem.html";
    systemConfig.pageURLs.UploadItemsLogList = 'Inventory/UploadItemsLog/UploadItems.aspx';
    systemConfig.HTMLTemplateURLs.UploadItemsLogDetailsTemplate = 'Inventory/Template/UploadItemsLog/uploadItemDetailsItems.html';

    systemConfig.IcAdditionalCostTypeServiceURL = "Common/WebServices/IC/IcAdditionalCostTypeWebService.asmx/";
    systemConfig.pageURLs.IcAdditionalCostTypeList = 'Inventory/AdditionalCostType/IcAdditionalCostTypeList.aspx';
    systemConfig.HTMLTemplateURLs.IcAdditionalCostTypeDetailsItem = "Inventory/Template/AdditionalCostType/IcAdditionalCostTypeDetailsItem.html";
    systemConfig.HTMLTemplateURLs.IcAdditionalCostReceiptDetailsItem = "Inventory/Template/AdditionalCostTypeReceipt/detailsRowItem.html";
    systemConfig.IcAdditionalCostReceiptServiceURL = "Common/WebServices/IC/IcAdditionalCostTypeReceiptWebService.asmx/";

    systemConfig.pageURLs.IcReceiptReturnList = 'Inventory/ReceiptReturn/IcReceiptReturnList.aspx';
    systemConfig.HTMLTemplateURLs.IcReceiptReturnListViewItemTemplate = "Inventory/Template/IcReceiptReturn/IcReceiptReturnListViewItem.html";
    systemConfig.HTMLTemplateURLs.IcReceiptReturnListItemTemplate = "Inventory/Template/IcReceiptReturn/IcReceiptReturnListItem.html";
    systemConfig.HTMLTemplateURLs.AddIcReceiptReturn = "Inventory/Template/IcReceiptReturn/IcReceiptReturnAddItem.html";
    systemConfig.HTMLTemplateURLs.ReceiptReturnDetailTemplate = "Inventory/Template/IcReceiptReturn/IcReceiptReturnDetailListItem.html";


    systemConfig.HTMLTemplateURLs.ItemPriceListRowTemplate = 'Inventory/Template/ItemPriceList/itemPriceListRowItem.html';

    ///////////////////////////////////Serials//////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    systemConfig.pageURLs.IcItemCardSerialsManagement = 'Inventory/ItemCardSerials/ItemCardSerialsManagement.aspx';
    systemConfig.pageURLs.IcItemCardSerialsManagementResult = 'Inventory/ItemCardSerials/IcItemCardSerialsManagementResult.aspx';
    systemConfig.HTMLTemplateURLs.IcItemCardSerialsManagementResultsGridTemplate = 'Inventory/Template/IcItemCardSerials/IcItemCardSerialsManagementResultsGrid.html';
    systemConfig.HTMLTemplateURLs.SerialTransactionsTemplate = 'Inventory/Template/IcItemCardSerials/SerialTransactions.html';
    systemConfig.HTMLTemplateURLs.IcItemCardSerialsListViewItemTemplate = 'Inventory/Template/IcItemCardSerials/IcItemCardSerialsListViewItem.html';
    systemConfig.HTMLTemplateURLs.SerialTransactionsItemRowTemplate = 'Inventory/Template/IcItemCardSerials/SerialTransactionsItemRowTemplate.html';



    ///////////////////////////////////Lots//////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    systemConfig.pageURLs.IcItemCardLotManagement = 'Inventory/ItemCardLot/ItemCardLotManagement.aspx';
    systemConfig.pageURLs.IcItemCardLotManagementResult = 'Inventory/ItemCardLot/IcItemCardLotManagementResult.aspx';
    systemConfig.HTMLTemplateURLs.IcItemCardLotManagementResultsGridTemplate = 'Inventory/Template/IcItemCardLot/IcItemCardLotManagementResultsGrid.html';
    systemConfig.HTMLTemplateURLs.LotTransactionsTemplate = 'Inventory/Template/IcItemCardLot/LotTransactions.html';
    systemConfig.HTMLTemplateURLs.IcItemCardLotListViewItemTemplate = 'Inventory/Template/IcItemCardLot/IcItemCardLotListViewItem.html';
    systemConfig.HTMLTemplateURLs.LotTransactionsItemRowTemplate = 'Inventory/Template/IcItemCardLot/LotTransactionsItemRowTemplate.html';
    systemConfig.IcItemCardLotServiceURL = "Common/WebServices/IC/IcItemCardLotWebService.asmx/";
    systemConfig.pageURLs.IcOption = "Inventory/Options/IcOptions.aspx";

    /////////////////////////////////Shifts////////////////////////////////
    systemConfig.ShiftsServiceURL = "Common/WebServices/ERP/ShiftsWebService.asmx/";
    systemConfig.UserStoreShifsServiceURL = "Common/WebServices/ERP/UserStoreShifsWebService.asmx/";
    systemConfig.HTMLTemplateURLs.ShiftsListViewItemTemplate = "NewUserManagement/Template/Shifts/ShiftsListViewItem.html";
    systemConfig.HTMLTemplateURLs.ShiftsListItemTemplate = "NewUserManagement/Template/Shifts/ShiftsListItem.html";
    systemConfig.HTMLTemplateURLs.AddShiftsItemTemplate = "NewUserManagement/Template/Shifts/AddShifts.html";
    systemConfig.HTMLTemplateURLs.UserStoresShiftsTabTemplate = "NewUserManagement/Template/Shifts/UserStoresShifts.html";
    systemConfig.pageURLs.ShiftsList = 'NewUserManagement/Shifts/ShiftsList.aspx';

    //systemConfig.POSSyncServiceURL = "Common/WebServices/POS/POSSyncAPIsWebService.asmx/";
    systemConfig.IntegrationWebServiceURL = "Common/WebServices/ERP/CustomerWebService.asmx/";
    systemConfig.CompanyServiceURL = "Common/WebServices/POS/CompanyWebService.asmx/";
    systemConfig.pageURLs.QuickKeysList = 'POS/QuickKeys/QuickKeysList.aspx';
    systemConfig.pageURLs.AddQuickKeys = 'POS/QuickKeys/AddQuickKeys.aspx';
    systemConfig.HTMLTemplateURLs.BarCodeTemplate = "POS/Template/GenerateBarcode/BarCodeTemplate.html";
    systemConfig.HTMLTemplateURLs.QuickKeysListItemTemplate = "POS/Template/QuickKeys/QuickKeysListItem.html";
    systemConfig.HTMLTemplateURLs.AddQuickKeysItemTemplate = "POS/Template/QuickKeys/AddQuickKeysItem.html";
    systemConfig.HTMLTemplateURLs.QuickKeysListViewItemTemplate = "POS/Template/QuickKeys/QuickKeysListViewItem.html";
    systemConfig.PosSyncLogWebService = "Common/WebServices/POS/PosSyncLogWebService.asmx/";



    ////POS Entity//////
    systemEntities.Store = 2171;
    systemEntities.RegisterEntityId = 2174;
    systemEntities.GiftVoucherEntityId = 2176;
    systemEntities.PosSyncConfigurationEntityId = 2166;
    systemEntities.GenerateBarcodeEntityId = 2178;
    systemEntities.QuickKeysEntityId = 2172;
    systemEntities.POSSyncEntityId = 2153;
    systemEntities.SyncScheduleEntityId = 2171;
    systemEntities.CompanyEntityId = 2183;
    systemEntities.PersonEntityId = 2184;

    systemEntities.WalkInId = 2165;

    systemConfig.dataTypes.SyncAll = 1;
    systemConfig.dataTypes.SyncRegisterData = 2;
    systemConfig.dataTypes.SyncAdminData = 3;

    systemConfig.dataTypes.SyncInvoice = 1;
    systemConfig.dataTypes.SyncRefund = 2;
    systemConfig.dataTypes.SyncPettyCash = 3;


    systemConfig.HTMLTemplateURLs.POSInvoiceListViewItemTemplate = "POS/Template/ERPIntegration/POSInvoiceListViewItemTemplate.html";
    systemConfig.HTMLTemplateURLs.POSInvoiceResultGridTemplate = "POS/Template/ERPIntegration/POSInvoiceResultGrid.html";

    systemConfig.HTMLTemplateURLs.POSCloseRegisterListViewItemTemplate = "POS/Template/ERPIntegration/CloseRegisterInquiryListViewItem.html";
    systemConfig.HTMLTemplateURLs.POSCloseRegisterResultGridTemplate = "POS/Template/ERPIntegration/POSCloseRegisterResultGrid.html";


    systemConfig.pageURLs.StoreList = 'POS/Store/StoreList.aspx';
    systemConfig.pageURLs.RegisterList = 'POS/Register/RegisterList.aspx';



    //////////////////////////////////////////////////////////////////////////////////////////////
    // ------------------------------------ PO Configuratuion ----------------------------------

    systemConfig.dataTypes.POStatus = 90;
    systemConfig.dataTypes.OEOrderEntryStatus = 1092;
    systemConfig.dataTypes.OEShipmentStatus = 2092;
    
    systemConfig.WebKeys.POStatusNew = 452;//saved
    systemConfig.WebKeys.POStatusInProgress = 453;//partially recived
    systemConfig.WebKeys.POStatusCompleted = 454;//fully recived
    systemConfig.WebKeys.POReadyToRecived = 1476;
    systemConfig.WebKeys.POStatusSaved = 457;
    systemConfig.WebKeys.POStatusPosted = 458;

    systemConfig.dataTypes.POReciptStatus = 1090;
    systemConfig.dataTypes.Saved = 455;
    systemConfig.dataTypes.Posted = 456;
    systemConfig.dataTypes.PoReturnPosted = 1478;
    systemConfig.dataTypes.PoReturnSaved = 1477;
    systemConfig.dataTypes.POReturnReciptStatus = 2096;

    systemConfig.WebKeys.OeOrderStatusNew = 466;
    systemConfig.WebKeys.OeShipmentStatusNew = 1463;

    systemConfig.PoInvoiceServiceURL = "Common/WebServices/PoInvoiceWebService.asmx/";
    systemConfig.PoInvoiceDetailsServiceURL = "Common/WebServices/PoInvoiceDetailsWebService.asmx/";
    systemConfig.PoInvoiceAdditionalCostServiceURL = "Common/WebServices/PoAdditionalCostTypeInvoiceWebService.asmx/";
    systemConfig.HTMLTemplateURLs.PoInvoiceListViewItemTemplate = "PO/Template/Invoice/PoInvoiceListViewItem.html";
    systemConfig.HTMLTemplateURLs.PoInvoiceListItemTemplate = "PO/Template/Invoice/PoInvoiceListItem.html";
    systemConfig.HTMLTemplateURLs.AddPoInvoiceItemTemplate = "PO/Template/Invoice/AddPoInvoice.html";
    systemConfig.HTMLTemplateURLs.PoReceiptDetailsTemplate = "PO/Template/Invoice/ReceiptDetails.html";
    systemConfig.HTMLTemplateURLs.PoAdditionalCostTemplate = "PO/Template/Invoice/AdditionalCostDetails.html";
    systemConfig.pageURLs.PoInvoiceList = 'PO/Invoice/POInvoiceList.aspx';

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    systemConfig.PurchaseOrderHeaderServiceURL = "Common/WebServices/PurchaseOrderHeaderWebService.asmx/";
    systemConfig.PurchaseOrderDetailsServiceURL = "Common/WebServices/PurchaseOrderDetailsWebService.asmx/";
    systemConfig.HTMLTemplateURLs.PurchaseOrderHeaderListViewItemTemplate = "PO/Template/PurchaseOrderHeader/PurchaseOrderHeaderListViewItem.html";
    systemConfig.HTMLTemplateURLs.PurchaseOrderHeaderListItemTemplate = "PO/Template/PurchaseOrderHeader/PurchaseOrderHeaderListItem.html";
    systemConfig.HTMLTemplateURLs.AddPurchaseOrderHeaderTemplate = "PO/Template/PurchaseOrderHeader/AddPurchaseOrderHeader.html";

    systemConfig.pageURLs.PurchaseOrderHeaderList = 'PO/PurchaseOrderHeader/PurchaseOrderHeaderList.aspx';
    systemConfig.pageURLs.POInqueryPage = 'PO/PurchaseOrderHeader/POInqueryPage.aspx';
    systemConfig.HTMLTemplateURLs.PurchaseOrderDetailsItem = "PO/Template/PurchaseOrderHeader/PurchaseOrderDetailsItem.html";
    systemConfig.HTMLTemplateURLs.POInqueryResultsItem = "PO/Template/PurchaseOrderHeader/POInqueryResultsItem.html";

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    systemConfig.VendorIdWebService = 'Common/WebServices/VendorWebService.asmx//';

    systemConfig.HTMLTemplateURLs.PoAdditionalCostReceiptDetailsItem = "PO/Template/AdditionalCostTypeReceipt/detailsRowItem.html";
    systemConfig.PoReceiptServiceURL = "Common/WebServices/PoReceiptWebService.asmx/";
    systemConfig.HTMLTemplateURLs.PoReceiptListViewItemTemplate = "PO/Template/PoReceipt/PoReceiptListViewItem.html";
    systemConfig.HTMLTemplateURLs.PoReceiptListItemTemplate = "PO/Template/PoReceipt/PoReceiptListItem.html";
    systemConfig.HTMLTemplateURLs.AddPoReceiptItemTemplate = "PO/Template/PoReceipt/AddPoReceipt.html";
    systemConfig.pageURLs.PoReceiptList = 'PO/PoReceipt/PoReceiptList.aspx';

    systemConfig.PoAdditionalCostReceiptServiceURL = "Common/WebServices/PoAdditionalCostTypeReceiptWebService.asmx/";
    systemConfig.HTMLTemplateURLs.PoReceiptDetailTemplate = "PO/Template/PoReceipt/PoReceiptDetailListItem.html";

    /////////////////////////////////////PoReturnReceipt Configuration //////////////////////////////////////////////////////////////////////////////////////////////////
    systemConfig.ReceiptIdWebService = 'Common/WebServices/PoReceiptWebService.asmx//'
    systemConfig.VendorIdWebService = 'Common/WebServices/VendorWebService.asmx//'


    systemConfig.PoReturnReceiptServiceURL = "Common/WebServices/PoReturnReceiptWebService.asmx/";
    systemConfig.HTMLTemplateURLs.PoReturnReceiptListViewItemTemplate = "PO/Template/PoReturnReceipt/PoReturnReceiptListViewItem.html";
    systemConfig.HTMLTemplateURLs.PoReturnReceiptListItemTemplate = "PO/Template/PoReturnReceipt/PoReturnReceiptListItem.html";
    systemConfig.HTMLTemplateURLs.AddPoReturnReceiptTemplate = "PO/Template/PoReturnReceipt/AddPoReturnReceipt.html";
    systemConfig.pageURLs.PoReturnReceiptList = 'PO/PoReturnReceipt/PoReturnReceiptList.aspx';
    systemConfig.pageURLs.AddPoReturnReceipt = 'PO/PoReturnReceipt/AddPoReturnReceipt.aspx';
    systemConfig.HTMLTemplateURLs.PoReturnReceiptDetailListItem = "PO/Template/PoReturnReceipt/PoReturnReceiptDetailListItem.html";
    systemConfig.HTMLTemplateURLs.PoAdditionalCostReceiptDetailsItem = "PO/Template/AdditionalCostTypeReceipt/detailsRowItem.html";
    systemConfig.HTMLTemplateURLs.PoAdditionalCostReturnReceiptDetailsItem = "PO/Template/PoReturnReceipt/AdditionalCostDetailsRowItem.html";
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    systemConfig.HTMLTemplateURLs.PoUploadLogListViewItemTemplate = "PO/Template/PoUploadLog/PoUploadLogListViewItem.html";
    systemConfig.HTMLTemplateURLs.PoUploadLogListItemTemplate = "PO/Template/PoUploadLog/PoUploadLogListItem.html";
    systemConfig.HTMLTemplateURLs.AddPoUploadLogItemTemplate = "PO/Template/PoUploadLog/AddPoUploadLogItem.html";
    systemConfig.pageURLs.PoUploadLogList = 'Po/PoUploadLog/PoUploadLog.aspx';
    systemConfig.HTMLTemplateURLs.PoUploadLogDetailsTemplate = 'PO/Template/PoUploadLog/PoUploadDetailsItems.html';
    systemConfig.PoUploadLogServiceURL = "Common/WebServices/PoUploadLogWebService.asmx/";


    ////////////////////////////////////////Order Entry////////////////////////////////////////////////////////////////////////////

    systemConfig.OrderEntryServiceURL = "Common/WebServices/OeOrderEntryWebService.asmx/";
    systemConfig.OrderEntryDetailsServiceURL = "Common/WebServices/OrderEntryDetailsWebService.asmx/";
    systemConfig.HTMLTemplateURLs.OrderEntryListViewItemTemplate = "OE/Template/OrderEntry/OrderEntryListViewItem.html";
    systemConfig.HTMLTemplateURLs.OrderEntryListItemTemplate = "OE/Template/OrderEntry/OrderEntryListItem.html";
    systemConfig.HTMLTemplateURLs.AddOrderEntryHeaderTemplate = "OE/Template/OrderEntry/AddOrderEntry.html";
    systemConfig.HTMLTemplateURLs.OEInqueryResultsItem = "OE/Template/OrderEntry/OEInqueryResultsItem.html";
    systemConfig.HTMLTemplateURLs.OEDeliveryInqueryResultsItem = "OE/Template/OrderEntry/OEDeliveryInqueryResultsItem.html";
    systemConfig.HTMLTemplateURLs.OEDeliveryDetailsInqueryResultsItem = "OE/Template/OrderEntry/OEDeliveryDetailsInqueryResultsItem.html";

    systemConfig.pageURLs.OrderEntryList = 'OE/OrderEntry/OrderEntryList.aspx';
    systemConfig.pageURLs.OeShipmentList = 'OE/Shipment/ShipmentList.aspx';
    systemConfig.ShipmentServiceURL = "Common/WebServices/ERP/OeShipmentWebService.asmx/";
    systemConfig.HTMLTemplateURLs.ShipmentListViewItemTemplate = "OE/Template/Shipment/ShipmentListViewItem.html";
    systemConfig.HTMLTemplateURLs.ShipmentListItemTemplate = "OE/Template/Shipment/ShipmentListItem.html";
    systemConfig.HTMLTemplateURLs.AddShipmentHeaderTemplate = "OE/Template/Shipment/AddShipment.html";
    systemConfig.HTMLTemplateURLs.ShipmentDetailsItem = "OE/Template/Shipment/ShipmentDetailsItem.html";

//    systemConfig.pageURLs.PurchaseOrderHeaderList = 'OE/PurchaseOrderHeader/OrderEntryList.aspx';

//    systemConfig.pageURLs.POInqueryPage = 'PO/PurchaseOrderHeader/POInqueryPage.aspx';
    systemConfig.HTMLTemplateURLs.OrderEntryDetailsItem = "OE/Template/OrderEntry/OrderEntryDetailsItem.html";
//    systemConfig.HTMLTemplateURLs.POInqueryResultsItem = "PO/Template/PurchaseOrderHeader/POInqueryResultsItem.html";

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   

    // ------------------------------------ OE Configuratuion ----------------------------------
    /////////////////////////OE Invoice////////////////////////////////////////////////////////////////////////////////////////////////////
    systemConfig.WebKeys.OEInvoiceStatusSaved = 1466;
    systemConfig.WebKeys.OEInvoiceStatusPosted = 1467;

    systemConfig.WebKeys.OEStatusCompleted = 111111;
    systemConfig.OeInvoiceServiceURL = "Common/WebServices/OeInvoiceWebService.asmx/";
    systemConfig.OeInvoiceDetailsServiceURL = "Common/WebServices/OeInvoiceDetailsWebService.asmx/";
    systemConfig.OeInvoiceAdditionalCostServiceURL = "Common/WebServices/PoAdditionalCostTypeInvoiceWebService.asmx/";
    systemConfig.HTMLTemplateURLs.OeInvoiceListViewItemTemplate = "OE/Template/Invoice/OeInvoiceListViewItem.html";
    systemConfig.HTMLTemplateURLs.OeInvoiceListItemTemplate = "OE/Template/Invoice/OeInvoiceListItem.html";
    systemConfig.HTMLTemplateURLs.AddOeInvoiceItemTemplate = "OE/Template/Invoice/AddOeInvoice.html";
    systemConfig.HTMLTemplateURLs.OeInvoiceDetailsTemplate = "OE/Template/Invoice/OeInvoiceDetailListItem.html";
    systemConfig.HTMLTemplateURLs.OeAdditionalCostTemplate = "OE/Template/Invoice/AdditionalCostDetails.html";
    systemConfig.pageURLs.OeInvoiceList = 'OE/Invoice/OeInvoiceList.aspx';
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////OE ShipmentReturn////////////////////////////////////////////////////////////////////////////////////////////////////
    systemConfig.WebKeys.OEShipmentReturnStatusSaved = 1469;
    systemConfig.WebKeys.OEShipmentReturnStatusPosted = 1470;

    systemConfig.OeShipmentReturnServiceURL = "Common/WebServices/ShipmentReturnWebService.asmx/";
    systemConfig.HTMLTemplateURLs.ShipmentReturnListViewItemTemplate = "OE/Template/ShipmentReturn/ShipmentReturnListViewItem.html";
    systemConfig.HTMLTemplateURLs.ShipmentReturnListItemTemplate = "OE/Template/ShipmentReturn/ShipmentReturnListItem.html";
    systemConfig.HTMLTemplateURLs.AddShipmentReturnItemTemplate = "OE/Template/ShipmentReturn/AddShipmentReturn.html";
    systemConfig.HTMLTemplateURLs.ShipmentReturnDetailsTemplate = "OE/Template/ShipmentReturn/ShipmentReturnDetailListItem.html";

    systemConfig.pageURLs.ShipmentReturnList = 'OE/ShipmentReturn/ShipmentReturnList.aspx';
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    systemConfig.HTMLTemplateURLs.OeUploadLogDetailsTemplate = 'Oe/Template/OeUploadLog/OeUploadDetailsItems.html';
    systemConfig.pageURLs.OeUploadLogList = 'Oe/OeUploadLog/OeUploadLog.aspx';
    systemConfig.OeUploadLogServiceURL = "Common/WebServices/OeUploadLogWebService.asmx/";

    systemConfig.dataTypes.PoInvoiceType = 2097;
    systemConfig.WebKeys.ReciptInvoiceType = 1479;
    systemConfig.WebKeys.AdditionalCostInvoiceType = 1480;
    systemConfig.HTMLTemplateURLs.PoAdditionalCostInvoiceDetailsItem = "PO/Template/Invoice/detailsRowItem.html";
    systemConfig.WebKeys.PoInvoiceStatusSaved = 457;
    systemConfig.WebKeys.PoInvoiceStatusPosted = 458;
    systemConfig.HTMLTemplateURLs.footerInvTemp = "PO/Template/Invoice/footerInvTemp.html";
}

