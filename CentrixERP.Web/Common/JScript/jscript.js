if (parent.length == 0) {
    if (EnabledChat == "True")
        window.location = defualtPage + "?pageurl=" + document.location.href;
}

$(document).ajaxStop(function () {
    if ($('.DataGridPanel').length != 0) {
        if ($('.DataGridPanel').children('.arrow').length == 0) {
            $('.DataGridPanel').prepend('<div class="arrow"></div>');
            $('.DataGridPanel').prepend('<div class="hide-custom-grid-row">' + hideCustomGridRowText + '</div>');
        }
    }

});

$(document).ready(function () {
    $('body').click(function () {
        $('.searchboxDDL').hide();
    });

    $('#table-content').css('min-height', ($(window).height() - 200));
    $('#drop-down-menu').height($(window).height() - 105);

    $(".custom-grid-options").live('mouseover', function () {
        $('.custom-grid-options').children('div').hide();
        $(this).children('div').show();
    });

    $(".custom-grid-row").live('mouseover', function () {
        $('.custom-grid-row').css({ 'background-color': 'Transparent', 'border': 'dotted 0px #999' });
        $('.alternate-row').css('background-color', '#ECECEC');
        $(this).css({ 'background-color': '#a7cdf0', 'border': 'dotted 1px #999' });

    });


    $(".custom-grid-options").live('mouseout', function () {
        $(this).children('.custom-grid-options div').hide();
    });

    $(".custom-grid-row").live('mouseout', function () {
        $('.custom-grid-row').css({ 'background-color': 'Transparent', 'border': 'dotted 0px #999' });
        $('.alternate-row').css('background-color', '#ECECEC');

    });

    $(document).ready(function () {
        $('.hide-custom-grid-row').live('click', function () {
            HideDiv();
            // oldDevId = 0;
        });
    });

});

function OnFailed() {
    //alert('Failed');
}


function SetDatePickersValues() {
    $(hdnDatePickers).val("");
    var str;
    var DatePicker = $('input[id*="DatePicker"]');
    for (var x = 0; x < DatePicker.length; x++) {
        str += DatePicker[x].id + "|" + DatePicker[x].value + ",";
    }
    $(hdnDatePickers).val(str);
}

function deleteFunction(id) {
    if (confirm(confirmMsgText)) {
        var Originalcolor = $(rowToDelete + id).css('background-color');
        var OriginalData = $(rowToDelete + id).html();
        $(rowToDelete + id).css('background-color', '#cc0000');
        $.ajax({
            type: "post",
            contentType: "application/json; charset=utf-8",
            url: ajaxDeletePageURL,
            data: '{ id: "' + id + '" }',
            dataType: "json",
            success: function (e) {
                $(rowToDelete + id).remove();
                window.location = RedirectPageURL;
            },
            error: function (e) {
                OnFailed();
                $(rowToDelete + id).css('background-color', Originalcolor);
                $(rowToDelete + id).html(OriginalData);
            }
        });
    }
}

function centerThis(div) {
    var winH = $(window).height();
    var winW = $(window).width();
    var centerDiv = $('#' + div);
    centerDiv.css('top', winH / 2 - centerDiv.height() / 2);
    centerDiv.css('left', winW / 2 - centerDiv.width() / 2);
}

function getDate(format, date) {
    if (date != null) {
        try {
            date = date.replace(/-/g, "/")
        }
        catch (e) {
        }
    }
    var d = new Date(date);
    var curr_date = (d.getDate() < 10) ? "0" + d.getDate() : d.getDate();
    var curr_month = (d.getMonth() + 1 < 10) ? "0" + (d.getMonth() + 1) : d.getMonth() + 1;
    var curr_year = d.getFullYear();
    var curr_hour = (d.getHours() < 10) ? "0" + (d.getHours()) : d.getHours();
    var curr_minute = (d.getMinutes() + 1 < 10) ? "0" + (d.getMinutes()) : d.getMinutes();
    var time;
    var formattedDated;
    if (curr_hour > 12) {
        curr_hour -= 12;
        time = "PM";
    }
    else {
        time = "AM"
    }
    switch (format) {
        case "yy-mm-dd":
            formattedDated = curr_year + "-" + curr_month + "-" + curr_date;
            break;
        case "mm-dd-yy":
            formattedDated = curr_month + "/" + curr_date + "/" + curr_year;
            break;
        case "yy-mm-dd hh:mm tt":
            if (curr_hour == "00" && curr_minute == "00")
                formattedDated = curr_year + "-" + curr_month + "-" + curr_date;
            else
                formattedDated = curr_year + "-" + curr_month + "-" + curr_date + '  ' + curr_hour + ":" + curr_minute + '  ' + time;
            break;
        default:
            formattedDated = curr_year + "-" + curr_month + "-" + curr_date;
            break;
    }
    return formattedDated;
}

function getTime(format, date) {

    var h = date.getHours();
    var m = date.getMinutes();
    var s = date.getSeconds();
    var formattedTime;
    switch (format) {
        case "hh:mm":
            h = (h < 10) ? "0" + h : h;
            m = (m < 10) ? "0" + m : m;
            formattedTime = h + ":" + m;
            break;
        default:
            h = (h < 10) ? "0" + h : h;
            m = (m < 10) ? "0" + m : m;
            formattedTime = h + ":" + m;
            break;
    }
    return formattedTime;
}

function getDateName(date) {
    date = new Date(date);
    var days = [sundayText, mondayText, tuesdayText, wednesdayText, thursdayText, fridayText, saturdayText];
    var day = days[date.getDay()];
    return day;
}

function showPassword() {

    if (show) {
        $(lblPassword).hide();
        $(txtPassword).show();
        $(lnkChangePassword).text(cancelText);
        show = 0;

    }
    else {
        $(lblPassword).show();
        $(txtPassword).hide();
        $(lnkChangePassword).text(showText);
        show = 1;

    }
}

function searchCompanies(keyword, view, page, field, sortDirection, filter) {
    if (keyword == null || keyword == "")
        $('#txtSelectedCompany').val(0);

    $('#pnlCompanies').show();
    $('#loading').show();
    $.ajax({
        type: "POST",
        url: ApplicationURL + "Companies/CompanyAjax.aspx",
        data: "keyword=" + keyword + "&view=" + view + "&pn=" + page + "&lang=" + lang + "&field=" + field + "&sortDirection=" + sortDirection + "&filter=" + filter,
        success: function (data) {
            $('#loading').hide();
            $('#pnlCompanies').html(data);
            $("#arrowImg").attr("src", "../../Images/" + sortDirection + ".gif");
        },
        error: function (e) {
            OnFailed();
        }
    });
}

function searchPersons(keyword, company, view, page, field, sortDirection, filter) {
    if (keyword == null || keyword == "")
        $('#txtSelectedPerson').val(0);

    $('#pnlPersons').show();
    $('#loading').show();

    $.ajax({
        type: "POST",
        url: ApplicationURL + "Persons/Person_ajax.aspx",
        data: "search=person&keyword=" + keyword + "&cid=" + company + "&view=" + view + "&pn=" + page + "&lang=" + lang + "&field=" + field + "&sortDirection=" + sortDirection + "&filter=" + filter,
        success: function (data) {
            $('#loading').hide();
            $('#pnlPersons').html(data);
            $("#arrowImg").attr("src", "../../Images/" + sortDirection + ".gif");
        },
        error: function (e) {
            OnFailed();
        }
    });
}

function searchProjects(keyword, view, page, field, sortDirection, filter) {
    if (keyword == null || keyword == "")

        $('#pnlProjects').show();
    $('#loading').show();
    $.ajax({
        type: "POST",
        url: ApplicationURL + "Projects/Project_ajax.aspx",
        data: "keyword=" + keyword + "&view=" + view + "&pn=" + page + "&lang=" + lang + "&field=" + field + "&sortDirection=" + sortDirection + "&filter=" + filter,
        success: function (data) {
            $('#loading').hide();
            $('#pnlProjects').html(data);
            $("#arrowImg").attr("src", "../../Images/" + sortDirection + ".gif");
        },
        error: function (e) {
            OnFailed();
        }
    });
}



function searchCompaniesRB(keyword, view, page) {
    $('#pnlCompanies').show();
    $('#loading').show();
    $.ajax({
        type: "POST",
        url: ApplicationURL + "Companies/CompanyAjaxRB.aspx",
        data: "keyword=" + keyword + "&view=" + view + "&pn=" + page + "&lang=" + lang,
        success: function (data) {
            $('#loading').hide();
            $('#pnlCompanies').html(data);
        },
        error: function (e) {
            OnFailed();
        }
    });
}





function searchLeadCompanies(keyword, view, page, leadID) {
    $('#pnlCompanies').show();
    $('#loading').show();
    $.ajax({
        type: "POST",
        url: "http://" + ApplicationURL + "modules/sales/ajax/Search.aspx",
        data: "search=company&keyword=" + keyword + "&view=" + view + "&pn=" + page + "&lang=" + lang + "&lid=" + leadID,
        success: function (data) {
            $('#loading').hide();
            $('#pnlCompanies').html(data);
        },
        error: function () {
            OnFailed();
        }
    });
}




function searchPersonsRB(keyword, company, view, page) {
    $('#pnlPersons').show();
    $('#loading').show();

    $.ajax({
        type: "POST",
        url: ApplicationURL + "Persons/PersonAjaxRB.aspx",
        data: "search=person&keyword=" + keyword + "&cid=" + company + "&view=" + view + "&pn=" + page + "&lang=" + lang,
        success: function (data) {
            $('#loading').hide();
            $('#pnlPersons').html(data);
        },
        error: function (e) {
            OnFailed();
        }
    });
}

function searchCompanyPersons(keyword, company, view, page) {
    $('#pnlPersons').show();
    $('#loading').show();
    $.ajax({
        type: "POST",
        url: ApplicationURL + "Companies/CompanyPersonsAjax.aspx",
        data: "keyword=" + keyword + "&cid=" + company + "&view=" + view + "&pn=" + page + "&lang=" + lang,
        success: function (data) {
            $('#loading').hide();
            $('#pnlPersons').html(data);
        },
        error: function () {
            OnFailed();
        }
    });
}

function searchLeadPersons(keyword, company, view, page, leadID) {
    $('#pnlPersons').show();
    $('#loading').show();
    $.ajax({
        type: "POST",
        url: "http://" + ApplicationURL + "modules/sales/ajax/Search.aspx",
        data: "search=person&keyword=" + keyword + "&cid=" + company + "&view=" + view + "&pn=" + page + "&lang=" + lang + "&lid=" + leadID,
        success: function (data) {
            $('#loading').hide();
            $('#pnlPersons').html(data);
        },
        error: function () {
            OnFailed();
        }
    });
}

function searchUsers(keyword, view, page, field, sortDirection, filter) {

    $('#pnlUsers').show();
    $('#loading').show();
    $.ajax({
        type: "POST",
        url: ApplicationURL + "Users/UsersAjax.aspx",
        data: "search=user&keyword=" + keyword + "&view=" + view + "&pn=" + page + "&lang=" + lang + "&field=" + field + "&sortDirection=" + sortDirection + "&filter=" + filter,
        success: function (data) {
            $('#loading').hide();
            $('#pnlUsers').html(data);
            $("#arrowImg").attr("src", "../../Images/" + sortDirection + ".gif");
        },
        error: function (e) {
            OnFailed();
        }
    });
}


function searchRoles(keyword, view, page, field, sortDirection, filter) {
    args.keyword = keyword;
    args.view = view;
    args.field = field;
    args.filter = filter;
    args.sortDirection = sortDirection;
    args.pn = page;
    args.lang = lang;
    args.flag = page;
    args.Name = keyword;

    var SearchCriteria = '{';
    for (var key in args) {
        if (args.hasOwnProperty(key))
            if (args[key] != "") {
                SearchCriteria = SearchCriteria + ('"' + key + '":' + '"' + args[key] + '",');
            }
    }
    SearchCriteria = SearchCriteria.slice(0, -1);
    SearchCriteria = SearchCriteria + '}';


    $('#pnlRoles').show();
    $('#loading').show();
    $.ajax({
        type: "POST",
        url: ApplicationURL + "Roles/RolesAjax.aspx",
        data: "SearchCriteria=" + SearchCriteria,
        success: function (data) {
            $('#loading').hide();
            $('#pnlRoles').html(data);
            $("#arrowImg").attr("src", "../../Images/" + sortDirection + ".gif");
        },
        error: function (e) {
            OnFailed();
        }
    });
}

function searchTeams(keyword, view, page, field, sortDirection, filter) {
    args.keyword = keyword;
    args.view = view;
    args.field = field;
    args.filter = filter;
    args.sortDirection = sortDirection;
    args.pn = page;
    args.lang = lang;
    args.search = 'team';
    args.flag = page;
    args.Name = keyword;

    var SearchCriteria = '{';
    for (var key in args) {
        if (args.hasOwnProperty(key))
            if (args[key] != "") {
                SearchCriteria = SearchCriteria + ('"' + key + '":' + '"' + args[key] + '",');
            }
    }
    SearchCriteria = SearchCriteria.slice(0, -1);
    SearchCriteria = SearchCriteria + '}';


    $('#pnlTeams').show();
    $('#loading').show();
    $.ajax({
        type: "POST",
        url: ApplicationURL + "Teams/TeamsAjax.aspx",
        data: "SearchCriteria=" + SearchCriteria,
        success: function (data) {
            $('#loading').hide();
            $('#pnlTeams').html(data);
            $("#arrowImg").attr("src", "../../Images/" + sortDirection + ".gif");
        },
        error: function () {
            OnFailed();
        }
    });
}


//function searchDocuments(keyword, view, page, companyID, personID, projectID) {

//    $('#pnlDocuments').show();
//    $('#loading').show();
//    $.ajax({
//        type: "POST",
//        url: ApplicationURL + "Documents/Document.aspx",
//        data: "keyword=" + keyword + "&view=" + view + "&pn=" + page + "&lang=" + lang + "&cid=" + companyID + "&pid=" + personID + "&prid=" + projectID,
//        success: function(data) {
//            $('#loading').hide();
//            $('#pnlDocuments').html(data);
//        },
//        error: function() {
//            OnFailed();
//        }
//    });
//}

function searchDocumentsOpportunity(keyword, view, page, opportunityID) {
    $('#pnlDocuments').show();
    $('#loading').show();
    $.ajax({
        type: "POST",
        url: ApplicationURL + "Document/DocumentAjax.aspx",
        data: "keyword=" + keyword + "&view=" + view + "&pn=" + page + "&lang=" + lang + "&oid=" + opportunityID,
        success: function (data) {
            $('#loading').hide();
            $('#pnlDocuments').html(data);
        },
        error: function () {
            OnFailed();
        }
    });
}

function searchAreas(keyword, view, page) {
    $('#pnlAreas').show();
    $('#loading').show();
    $.ajax({
        type: "POST",
        url: ApplicationURL + "Areas/AreaAjax.aspx",
        data: "search=area&keyword=" + keyword + "&view=" + view + "&pn=" + page + "&lang=" + lang,
        success: function (data) {
            $('#loading').hide();
            $('#pnlAreas').html(data);
        },
        error: function () {
            OnFailed();
        }
    });
}

function searchLeads(keyword, view, page, field, sortDirection, filter) {
    $('#pnlLeads').show();
    $('#loading').show();
    $.ajax({
        type: "POST",
        url: ApplicationURL + "Lead/Ajax.aspx",
        data: "search=lead&keyword=" + keyword + "&view=" + view + "&pn=" + page + "&lang=" + lang + "&field=" + field + "&sortDirection=" + sortDirection + "&filter=" + filter,
        success: function (data) {
            $('#loading').hide();
            $('#pnlLeads').html(data);
            $("#arrowImg").attr("src", "../../Images/" + sortDirection + ".gif");
        },
        error: function (e) {
            OnFailed();
        }
    });
}

function searchLeadProgresses(keyword, leadid, page) {
    $('#LeadProgress').show();
    $('#loading').show();
    $.ajax({
        type: "POST",
        url: ApplicationURL + "Lead/LeadProgresses.aspx",
        data: "keyword=" + keyword + "&leadid=" + leadid + "&pn=" + page,
        success: function (data) {
            $('#loading').hide();
            $('#LeadProgress').html(data);
        },
        error: function (e) {
            OnFailed();
        }
    });
}

function searchOpportunities(keyword, Stageid, view, page, compnayID, personID, FromTab, field, sortDirection, filter) {
    $('#pnlOpportunities').show();
    $('#loading').show();
    $("#txtSearchPerson").removeAttr("disabled");
    $("#txtSearchCompany").removeAttr("disabled");

    $.ajax({
        type: "POST",
        url: ApplicationURL + "Opportunity/AjaxOpportunity.aspx",
        data: "search=opportunity&keyword=" + keyword + "&Stageid=" + Stageid + "&view=" + view + "&pn=" + page + "&lang=" + lang + "&cid=" + compnayID + "&pid=" + personID + "&tab=" + FromTab + "&field=" + field + "&sortDirection=" + sortDirection + "&filter=" + filter,
        success: function (data) {
            $('#loading').hide();
            $('#pnlOpportunities').html(data);
            $("#arrowImg").attr("src", "../../Images/" + sortDirection + ".gif");
        },
        error: function (e) {
            OnFailed();
        }
    });
}

function selectOpportunity(opportunityID, PersonId, PersonName, CompanyId, CompanyName, Description) {
    $(txtSelectedOpportunity).val(opportunityID);
    $(txtSearchOpportunity).val(Description);
    $('#pnlOpportunities').each(function () {
        $(this).removeClass('selectedRow');
    });
    $('#divOpportunity' + opportunityID).addClass('selectedRow');

    $("#txtSearchCompany").val(CompanyName);
    $("#txtSelectedCompany").val(CompanyId);
    $("#txtSearchPerson").val(PersonName);
    $("#txtSelectedPerson").val(PersonId);
    $("#lblCompany").text(CompanyName);
    $('#pnlOpportunities').hide();
}

function searchDataTypeContent(DTId, view, page) {

    $('#pnlDataTypeContent').show();
    $('#loading').show();
    $.ajax({
        type: "POST",
        url: ApplicationURL_Common + "LOVs/Search.aspx",
        data: "DTId=" + DTId + "&view=" + view + "&pn=" + page + "&lang=" + lang,
        success: function (data) {
            $('#loading').hide();
            $('#pnlDataTypeContent').html(data);
        },
        error: function () {
            OnFailed();
        }
    });
}

function showIframe(id, src) {
    for (var i = 1; i <= 10; i++) {
        $('#iframe' + i).hide();
        $('#tab' + i).removeClass('selectedtab');
    }
    $('#extendedProfileContainer').show();
    $('#iframe' + id).show();
    $('#iframe' + id).attr('src', src);
    $('#tab' + id).addClass('selectedtab');
}


//this will move selected items from source list to destination list     
function move_list_items(sourceid, destinationid) {
    $(sourceid + "  option:selected").appendTo(destinationid);
    $(destinationList).each(function () {
        $(destinationList + " option").attr("selected", "selected");
    });
    if ($(destinationid).val() != null) $(selectedValuesObject).val($(destinationList).val());
}

//this will move all selected items from source list to destination list
function move_list_items_all(sourceid, destinationid) {
    $(sourceid + " option").appendTo(destinationid);
    $(destinationList).each(function () {
        $(destinationList + " option").attr("selected", "selected");
    });
    $(selectedValuesObject).val($(destinationList).val());
}

function setCompanyTextValues(ID, CompanyName) {
    $(txtCompanyID).val(ID);
    $(txtSelectedCompanyName).val(CompanyName);
}
function setPerosnTextValues(ID, FirstName, LastName) {
    $(txtPersonID).val(ID);
    $(txtSelectedPersonFirstName).val(FirstName);
    $(txtSelectedPersonLastName).val(LastName);
}

function clearbtn_click(txtSearch, txtID) {
    txtSearch.val('');
    txtID.val('');
}

function clearbtn_clickAC(AC_name) {

    var ctr = $find(AC_name);
    ctr.clear();

}

function selectPerson(PersonID) {
    $(txtPersonID).val(PersonID);
    $('#pnlPersons').each(function () {
        $(this).removeClass('selectedRow');
    });
    $('#divPerson' + PersonID).addClass('selectedRow');
    $(txtSearchPerson).val($.trim($('#divPerson' + PersonID).text()));
    $('#pnlPersons').hide();
}

function selectUser(userID) {
    $(txtUserID).val(userID);
    $('#pnlUsers').each(function () {
        $(this).removeClass('selectedRow');
    });
    $('#divUser' + userID).addClass('selectedRow');
    $(txtSearchUser).val($.trim($('#divUser' + userID).text()));
    $('#pnlUsers').hide();
}

function selectCompany(companyID) {
    $(txtCompanyID).val(companyID);
    $('#pnlCompanies').each(function () {
        $(this).removeClass('selectedRow');
    });
    $('#divCompany' + companyID).addClass('selectedRow');
    $(txtSearchCompany).val($.trim($('#divCompany' + companyID).text()));
    $('#pnlCompanies').hide();
}

function selectArea(areaID) {
    $(txtAreaID).val(areaID);
    $('#pnlAreas').each(function () {
        $(this).removeClass('selectedRow');
    });
    $('#divArea' + areaID).addClass('selectedRow');
    $(txtSearchArea).val($.trim($('#divArea' + areaID).text()));
    $('#pnlAreas').hide();
}

/**/
function setCountryCodes() {

    var countryIndex = parseInt($(lstCountry).attr("selectedIndex"));
    var countryCodesArr = $(countryCodes).val().split(",");
    var countryCode = countryCodesArr[countryIndex - 1];
    $(txtPhoneCountryCode).val(countryCode);
    $(txtFaxCountryCode).val(countryCode);
    $(txtMobileCountryCode).val(countryCode);
}

function listCountryCites() {
    $(document).ready(function () {
        $('#loading').show();

        $.ajax({
            type: "POST",
            url: ApplicationURL_Common + "UserControls/listcity.aspx",
            data: "CountryID=" + $(lstCountry).val(),
            success: function (data) {
                if (data != "") var citiesArr = data.split(',');
                $('#loading').hide();
                $(lstCity).empty();
                $(lstCity).append("<option value='" + 0 + "'>" + CityListTitle + "</option>")
                //$(lstCity).append(new Option(CityListTitle, 0, true, true));
                if (citiesArr != '') {
                    for (var i = 0; i < citiesArr.length; i++) {
                        var cityArr = citiesArr[i].split(stringDelimiter);
                        var cityValue = cityArr[0];
                        var cityText = cityArr[1];
                        $(lstCity).append("<option value='" + cityValue + "'>" + cityText + "</option>")
                        //$(lstCity).append(new Option(cityText, cityValue, true, true));
                    }
                }
                $(lstCity).val($(hdnCityID).val());
            },
            error: function () {
                OnFailed();
            }
        });
    });
}

function setCityValue() {
    $(hdnCityID).val($(lstCity).val());
}


function loadDatePicker(input, format) {
    if (format == undefined) {
        format = 'yy-mm-dd';
    }
    $(function () {
        $(input).datepicker({
            showOn: 'button',
            buttonImage: ApplicationURL_Common + "Images/icon_calendar.jpg",
            buttonImageOnly: true,
            yearRange: 'c-100:c',
            changeMonth: true,
            changeYear: true,
            dateFormat: format,
            appendText: (lang == "en") ? 'clear' : 'مسح',
            readOnly: true,
        });
    });
}

function getOrder() {
    var dataTypeContentArr = $(setSelector).sortable("toArray");
    $(dataTypeContentIDs).val(dataTypeContentArr);
}

function AddOrSelectCompany(leadID) {
    $("#ifrmdCustomerLightBx").attr('src', "addcompany.aspx?lid=" + leadID);
    $("#divCustomerLightBx").show();
}

function AddOrSelectPerson(leadID) {
    $("#ifrmdCustomerLightBx").attr('src', "addperson.aspx?lid=" + leadID);
    $("#divCustomerLightBx").show();
}

function lstSearchDataTypeContentChange() {
    $(lstSearchDataTypeContent).change(function () {
        searchDataTypeContent($(this).val(), 'grid', 1);
    });
}

function setCountryValue() {
    $(hdnCountryID).val($(lstCountry).val());
}

function setSelectorFunction() {
    $(function () {
        $(setSelector).sortable({
            axis: "y",
            cursor: "move",
            update: function () { getOrder(); }
        });
    });
}
function setDivLocation() {
    $("#divCustomerLightBx").css("left", (screen.width / 2 - $("#divCustomerLightBx").width() / 2) + 'px');
    $("#divCustomerLightBx").css("top", (screen.height / 2 - $("#divCustomerLightBx").height() / 2) + 'px');
}

function searchRealtedOpportunities(keyword, view, page, ParentOpportunityID) {
    $('#loading').show();
    $.ajax({
        type: "POST",
        url: ApplicationURL + "RelatedOpportunities/RealtedOpportunityAjax.aspx",
        data: "search=opportunity&keyword=" + keyword + "&view=" + view + "&pn=" + page + "&lang=" + lang + "&oppid=" + ParentOpportunityID,
        success: function (data) {
            $('#loading').hide();
            $('#pnlOpportunities').html(data);
        },
        error: function (e) {
            OnFailed();
        }
    });
}

function searchOpportunitiesRealted(keyword, view) {
    $('#pnlOpportunities').show();
    $('#loading').show();
    $.ajax({
        type: "POST",
        url: ApplicationURL + "Opportunity/AjaxOpportunity.aspx",
        data: "search=opportunity&keyword=" + keyword + "&view=" + view + "&isrealted=true",
        success: function (data) {
            $('#loading').hide();
            $('#pnlOpportunities').html(data);
        },
        error: function (e) {
            OnFailed();
        }
    });
}

function SelectRealtedOpportunity(opportunityID, Description) {
    $(txtSelectedOpportunity).val(opportunityID);
    $(txtSearchOpportunity).val(Description);
    $('#ParentError').css({ 'visibility': 'hidden' });
    $('#OpportunityError').css({ 'visibility': 'hidden' });
    $('#TypeError').css({ 'visibility': 'hidden' });
}

function CheckRealtedOpportunity() {
    if ($(txtSelectedOpportunity).val() == 0) {
        $('#OpportunityError').css({ 'visibility': 'visible' });
        return false;
    }
    if ($(lstType).val() == 0) {
        $('#TypeError').css({ 'visibility': 'visible' });
        return false;
    }
}

function getCustomForms() {
    if ($('#pnlForms').css('display') == 'none') {
        $('#pnlForms').html('Loading...');
        $('#pnlForms').show();
        $.ajax({
            type: "POST",
            url: ApplicationURL_Common + "formbuilder/forms.aspx",
            success: function (data) {
                $('#pnlForms').html(data);
            },
            error: function (e) {
                $('#pnlForms').hide();
                OnFailed();
            }
        });
    }
    else {
        $('#pnlForms').hide();
    }
}

function deleteFormValue(id) {
    if (confirm(confirmMsgText)) {
        var Originalcolor = $("tr[id*=DivValue" + id + "]").css('background-color');
        var OriginalData = $("tr[id*=DivValue" + id + "]").html()
        $("tr[id*=DivValue" + id + "]").css('background-color', '#cc0000');
        $.ajax({
            type: "post",
            contentType: "application/json; charset=utf-8",
            url: ajaxDeletePageURL,
            data: '{ id: "' + id + '" }',
            dataType: "json",
            success: function (e) {
                $("tr[id*=DivValue" + id + "]").remove();
            },
            error: function (e) {
                OnFailed();
                $("tr[id*=DivValue" + id + "]").css('background-color', Originalcolor);
                $("tr[id*=DivValue" + id + "]").html(OriginalData);
            }
        });
    }
}

function selectMileStone(MileStoneId) {

    $(txtMileStoneID).val(MileStoneId);
    $('#pnlMileStone').each(function () {
        $(this).removeClass('selectedRow');
    });
    $('#divMileStone' + MileStoneId).addClass('selectedRow');
    $(txtSearchMileStone).val($.trim($('#divMileStone' + MileStoneId).text()));
    $('#pnlMileStone').hide();
}

function searchMileStones(keyword, ProjectID, view, page) {

    if (keyword == null || keyword == "")
        $('#txtSelectedMileStone').val(0);

    $('#pnlMileStone').show();
    $('#loading').show();
    $.ajax({
        type: "POST",
        url: ApplicationURL + "Milestones/SearchMileStones.aspx",
        data: "keyword=" + keyword + "&view=" + view + "&pn=" + page + "&prid=" + ProjectID,
        success: function (data) {

            $('#loading').hide();
            $('#pnlMileStone').html(data);
        },
        error: function (e) {
            OnFailed();
        }
    });
}

function setIframeSrc(src, id) {
    if ($('#' + id).attr('src') == '') {
        $('#' + id).attr('src', src);
    }
}

function searchTaxAuthority(keyword, view, page) {
    if (keyword == null || keyword == "")
        $('#txtSearchTaxAuthority').val(0);

    $('#pnlTaxAuthority').show();
    $('#loading').show();
    $.ajax({
        type: "POST",
        url: ApplicationURL + "TaxAuthority/TaxAuthorityAjax.aspx",
        data: "keyword=" + keyword + "&view=" + view + "&pn=" + page,
        success: function (data) {

            $('#loading').hide();
            $('#pnlTaxAuthority').html(data);
        },
        error: function (e) {
            OnFailed();
        }
    });
}

function searchTaxClassHeader(keyword, view, page) {
    $("#pnlTaxHeader").show();
    $("#loading").show();

    $.ajax({
        type: "POST",
        url: ApplicationURL + "TaxClassHeader/TaxClassHeaderAjax.aspx",
        data: "keyword=" + keyword + "&view=" + view + "&pn=" + page,
        success: function (data) {
            $("#loading").hide();
            $("#pnlTaxHeader").html(data);
        },
        error: function (e) {
            $("#loading").hide();
            OnFailed();
        }
    });
}

function searchTaxGroupHeader(keyword, view, page) {
    $("#pnlTaxGroupHeader").show();
    $("#loading").show();

    $.ajax({
        type: "POST",
        url: ApplicationURL + "TaxGroupHeader/TaxGroupHeaderAjax.aspx",
        data: "keyword=" + keyword + "&view=" + view + "&pn=" + page,
        success: function (data) {
            $("#loading").hide();
            $("#pnlTaxGroupHeader").html(data);
        },
        error: function (e) {
            $("#loading").hide();
            OnFailed();
        }
    });
}

function searchCurrency(keyword, view, page) {
    if (keyword == null || keyword == "")
        $('#txtSearchCurrency').val(0);

    $('#pnlCurrency').show();
    $('#loading').show();
    $.ajax({
        type: "POST",
        url: ApplicationURL + "Currency/CurrencyAjax.aspx",
        data: "keyword=" + keyword + "&view=" + view + "&pn=" + page,
        success: function (data) {

            $('#loading').hide();
            $('#pnlCurrency').html(data);
        },
        error: function (e) {
            OnFailed();
        }
    });
}

function searchTaxClassDetails(keyword, tchid, view, page) {
    $("#pnlTaxClassDetails").show();
    $("#loading").show();

    $.ajax({
        type: "POST",
        url: ApplicationURL + "TaxClassHeader/TaxClassDetailsList.aspx",
        data: "keyword=" + keyword + "&tchid=" + tchid + "&view=" + view + "&pn=" + page,
        success: function (data) {
            $("#pnlTaxClassDetails").html("");
            $("#loading").hide();
            $("#pnlTaxClassDetails").html(data);
        },
        error: function (e) {
            $("#loading").hide();
            OnFailed();
        }
    });
}



function searchCurrencyRateHeader(keyword, view, page) {
    if (keyword == null || keyword == "")
        $('#txtSearchCurrencyRateHeader').val(0);

    $('#pnlCurrencyRateHeader').show();
    $('#loading').show();
    $.ajax({
        type: "POST",
        url: ApplicationURL + "CurrencyRateHeader/CurrencyRateHeaderAjax.aspx",
        data: "keyword=" + keyword + "&view=" + view + "&pn=" + page,
        success: function (data) {

            $('#loading').hide();
            $('#pnlCurrencyRateHeader').html(data);
        },
        error: function (e) {
            OnFailed();
        }
    });
}



function searchCurrencyRateDetails(keyword, CRHId, view, page) {
    $("#pnlCRDetails").show();
    $("#loading").show();

    $.ajax({
        type: "POST",
        url: ApplicationURL + "CurrencyRateDetails/CurrencyRateDetailsList.aspx",
        data: "keyword=" + keyword + "&tchid=" + tchid + "&view=" + view + "&pn=" + page,
        success: function (data) {
            $("#pnlCRDetails").html("");
            $("#loading").hide();
            $("#pnlCRDetails").html(data);
        },
        error: function (e) {
            $("#loading").hide();
            OnFailed();
        }
    });
}

function searchTaxRate(keyword, view, page) {
    $('#pnlTaxRate').show();
    $('#loading').show();
    $.ajax({
        type: "POST",
        url: ApplicationURL + "TaxRate/TaxRateAjax.aspx",
        data: "keyword=" + keyword + "&view=" + view + "&pn=" + page,
        success: function (data) {
            $('#loading').hide();
            $('#pnlTaxRate').html(data);
        },
        error: function (e) {
            OnFailed();
        }
    });
}


function searchFiscalCalender(keyword, view, page) {
    $('#pnlFCalender').show();
    $('#loading').show();
    $.ajax({
        type: "POST",
        url: ApplicationURL + "FiscalCalender/FiscalCalenderAjax.aspx",
        data: "keyword=" + keyword + "&view=" + view + "&pn=" + page,
        success: function (data) {
            $('#loading').hide();
            $('#pnlFCalender').html(data);
        },
        error: function (e) {
            OnFailed();
        }
    });
}


function searchItems(keyword, view, page) {
    if (keyword == null || keyword == "")

        $('#pnlItems').show();
    $('#loading').show();
    $.ajax({
        type: "POST",
        url: ApplicationURL + "PurchasedItems/PurchasedItems_ajax.aspx",
        data: "keyword=" + keyword + "&view=" + view + "&pn=" + page + "&lang=" + lang,
        success: function (data) {
            $('#loading').hide();
            $('#pnlItems').html(data);
        },
        error: function (e) {
            OnFailed();
        }
    });
}

function searchPrizes(keyword, view, page) {
    if (keyword == null || keyword == "")

        $('#pnlPrizes').show();
    $('#loading').show();
    $.ajax({
        type: "POST",
        url: ApplicationURL + "Prizes/PrizesList_ajax.aspx",
        data: "keyword=" + keyword + "&view=" + view + "&pn=" + page + "&lang=" + lang,
        success: function (data) {
            $('#loading').hide();
            $('#pnlPrizes').html(data);
        },
        error: function (e) {
            OnFailed();
        }
    });
}


function searchRedemptions(keyword, view, page) {
    if (keyword == null || keyword == "")

        $('#pnlRedemptions').show();
    $('#loading').show();
    $.ajax({
        type: "POST",
        url: ApplicationURL + "Redemption/RedemptionsList_ajax.aspx",
        data: "keyword=" + keyword + "&view=" + view + "&pn=" + page + "&lang=" + lang,
        success: function (data) {
            $('#loading').hide();
            $('#pnlRedemptions').html(data);
        },
        error: function (e) {
            OnFailed();
        }
    });
}


function searchUMEmails(keyword, UserId, view, page) {
    $("#pnlEmails").show();
    $("#loading").show();

    $.ajax({
        type: "POST",
        url: ApplicationURL + "EmailTab/EmailList.aspx",
        data: "keyword=" + keyword + "&Uid=" + UserId + "&view=" + view + "&pn=" + page,
        success: function (data) {
            $("#pnlEmails").html("");
            $("#loading").hide();
            $("#pnlEmails").html(data);
        },
        error: function (e) {
            $("#loading").hide();
            OnFailed();
        }
    });
}


function searchCRMEmails(keyword, page, parentId, moduleKey) {
    $("#pnlEmails").show();
    $("#loading").show();

    $.ajax({
        type: "POST",
        url: ApplicationURL + "Email/Email.aspx",
        data: "keyword=" + keyword + "&pn=" + page + "&key=" + moduleKey + "&id=" + parentId,
        success: function (data) {
            $("#pnlEmails").html("");
            $("#loading").hide();
            $("#pnlEmails").html(data);
        },
        error: function (e) {
            $("#loading").hide();
            OnFailed();
        }
    });
}


function searchNotes(keyword, page, parentId, moduleKey) {
    $("#pnlNotes").show();
    $("#loading").show();

    $.ajax({
        type: "POST",
        url: ApplicationURL + "Note/Notes.aspx",
        data: "keyword=" + keyword + "&key=" + moduleKey + "&id=" + parentId + "&pn=" + page,
        success: function (data) {
            $("#pnlNotes").html("");
            $("#loading").hide();
            $("#pnlNotes").html(data);
        },
        error: function (e) {
            $("#loading").hide();
            OnFailed();
        }
    });
}


function searchPhones(keyword, page, parentId, moduleKey) {

    $("#pnlPhones").show();
    $("#loading").show();

    $.ajax({
        type: "POST",
        url: ApplicationURL + "Phone/Phones.aspx",
        data: "keyword=" + keyword + "&pn=" + page + "&key=" + moduleKey + "&id=" + parentId,
        success: function (data) {
            $("#pnlPhones").html("");
            $("#loading").hide();
            $("#pnlPhones").html(data);
        },
        error: function (e) {
            $("#loading").hide();
            OnFailed();
        }
    });
}

function searchAddress(keyword, page, parentId, moduleKey) {

    $("#pnlAddresses").show();
    $("#loading").show();

    $.ajax({
        type: "POST",
        url: ApplicationURL + "Address/Address.aspx",
        data: "keyword=" + keyword + "&pn=" + page + "&key=" + moduleKey + "&id=" + parentId,
        success: function (data) {
            $("#pnlAddresses").html("");
            $("#loading").hide();
            $("#pnlAddresses").html(data);
        },
        error: function (e) {
            $("#loading").hide();
            OnFailed();
        }
    });
}



function searchOpportunityTab(compnayID, personID, view, page) {
    $('#pnlOpportunities').show();
    $('#loading').show();

    $.ajax({
        type: "POST",
        url: ApplicationURL + "Opportunity/Opportunity.aspx",
        data: "&cid=" + compnayID + "&pid=" + personID + "&view=" + view + "&pn=" + page,
        success: function (data) {
            $('#loading').hide();
            $('#pnlOpportunities').html(data);
        },
        error: function (e) {
            OnFailed();
        }
    });
}


function searchLeadTab(compnayID, personID, view, page) {
    $('#pnlLeads').show();
    $('#loading').show();

    $.ajax({
        type: "POST",
        url: ApplicationURL + "Lead/LeadAjax.aspx",
        data: "&cid=" + compnayID + "&pid=" + personID + "&view=" + view + "&pn=" + page,
        success: function (data) {
            $('#loading').hide();
            $('#pnlLeads').html(data);
        },
        error: function (e) {
            OnFailed();
        }
    });
}




function searchResource(PrId, Rid, view, page) {

    $("#pnlResources").show();
    $("#loading").show();

    $.ajax({
        type: "POST",
        url: ApplicationURL + "Resources/Resources.aspx",
        data: "prid=" + PrId + "&Rid=" + Rid + "&view=" + view + "&pn=" + page,
        success: function (data) {
            $("#pnlResources").html("");
            $("#loading").hide();
            $("#pnlResources").html(data);
        },
        error: function (e) {
            $("#loading").hide();
            OnFailed();
        }
    });
}



function searchStakeHolder(PrId, Sid, view, page) {

    $("#pnlStakeHolders").show();
    $("#loading").show();

    $.ajax({
        type: "POST",
        url: ApplicationURL + "StakeHolders/StakeHolders.aspx",
        data: "prid=" + PrId + "&Sid=" + Sid + "&view=" + view + "&pn=" + page,
        success: function (data) {
            $("#pnlStakeHolders").html("");
            $("#loading").hide();
            $("#pnlStakeHolders").html(data);
        },
        error: function (e) {
            $("#loading").hide();
            OnFailed();
        }
    });
}


function searchActivityLookup(keyword, page) {
    $('#loading').show();
    $.ajax({
        type: "POST",
        url: ApplicationURL + "ActivityLookup/ActivityLookupAjax.aspx",
        data: "keyword=" + keyword + "&pn=" + page,
        success: function (data) {
            $('#loading').hide();
            $('#pnlActivityLookups').html(data);
        },
        error: function (e) {
            OnFailed();
        }
    });
}

function searchDocuments(keyword, view, page, parentId, moduleKey, subFolder) {
    $('#pnlDocumentsList').show();
    $('#pnlDocumentsList').html('loading . . . ');
    $.ajax({
        type: "POST",
        url: ApplicationURL + "Documents/DocumentHtml5Ajax.aspx",
        data: "keyword=" + keyword + "&view=" + view + "&pn=" + page + "&lang=" + lang + "&id=" + parentId + "&key=" + moduleKey + "&subFolder=" + subFolder,
        success: function (data) {
            $('#pnlDocumentsList').html(data);
        },
        error: function () {
            OnFailed();
        }
    });
}

function searchPersons(page) {
    args.view = 'grid';
    args.pn = page;
    args.field = field;
    args.filter = filter;
    args.sortDirection = sortDirection;
    args.search = "person";
    args.lang = lang;
    args.flag = page; //next previous flag
    args.IsQualified = true;

    if (advanceSearch) {
        var companyId = -1;
        var companyObj = company.get_Item();
        if (companyObj != null) {
            companyId = companyObj.value;
            args.companyName = companyObj.label;
        }
        args.keyword = '';
        args.AreaCode = $(phoneAreaCode).val();
        args.CountryCode = $(phoneCountryCode).val();
        args.phone = $(phoneNum).val();
        args.company_id = companyId;
        args.firstName = $(firstName).val();
        args.middleName = $(middleName).val();
        args.lastName = $(lastName).val();


    }
    else {
        args.keyword = $(Keyword).val();
        args.AreaCode = '';
        args.CountryCode = '';
        args.phone = '';
        args.company_id = 0;
        args.firstName = '';
        args.middleName = '';
        args.lastName = '';

    }


    var SearchCriteria = '{';
    for (var key in args) {
        if (args.hasOwnProperty(key))
            if (args[key] != "") {
                SearchCriteria = SearchCriteria + ('"' + key + '":' + '"' + args[key] + '",');
            }
    }
    SearchCriteria = SearchCriteria.slice(0, -1);
    SearchCriteria = SearchCriteria + '}';

    if (keyword == null || keyword == "")
        $('#txtSelectedPerson').val(0);

    $('#pnlPersons').show();
    $('#loading').show();

    $.ajax({
        type: "POST",
        url: ApplicationURL + "Persons/Person_ajax.aspx",
        data: "SearchCriteria=" + SearchCriteria,
        success: function (data) {
            $('#loading').hide();
            $('#pnlPersons').html(data);
            $("#arrowImg").attr("src", "../../Images/" + sortDirection + ".gif");
        },
        error: function (e) {
            OnFailed();
        }
    });
}
function GoToReportPage() {
    window.open(ApplicationURL_Common + schedualTaskReportPageUrl, 'Report', 'width=1200,hight=400');
}

function searchPersons_duplicationRule(page) {
    $('#pnlPersonGrid').show();
    $('#loading').show();

    $.ajax({
        type: "POST",
        url: ApplicationURL + "Persons/DuplicationRulePersonAjax.aspx",
        data: "pn=" + page,
        success: function (data) {
            $('#loading').hide();
            $('#pnlPersonGrid').html(data);
        },
        error: function (e) {
            OnFailed();
        }
    });
}

function searchCompanies_duplicationRule(page) {
    $('#pnlCompanyGrid').show();
    $('#loading').show();

    $.ajax({
        type: "POST",
        url: ApplicationURL + "Companies/DuplicationRuleCompanyAjax.aspx",
        data: "pn=" + page,
        success: function (data) {
            $('#loading').hide();
            $('#pnlCompanyGrid').html(data);
        },
        error: function (e) {
            OnFailed();
        }
    });
}



function searchLeads(page) {
    args.view = 'grid';
    args.field = field;
    args.filter = filter;
    args.sortDirection = sortDirection;
    args.pn = page;
    args.search = 'lead';
    args.lang = lang;
    args.flag = page;

    if (advanceSearch) {
        var companyId = -1;
        var companyObj = company.get_Item();
        if (companyObj != null)
            companyId = companyObj.value;

        var personId = -1;
        var personObj = person.get_Item();
        if (personObj != null) {
            personId = personObj.value;
        }


        var priorityId = '';
        var priorityObj = priority.get_Item();
        if (priorityObj != null)
            priorityId = priorityObj.value;

        var stageId = '';
        var stageObj = stage.get_Item();
        if (stageObj != null)
            stageId = stageObj.value;

        var ratingId = '';
        var ratingObj = rating.get_Item();
        if (ratingObj != null)
            ratingId = ratingObj.value;

        var industryId = '';
        var industryObj = industry.get_Item();
        if (industryObj != null)
            industryId = industryObj.value;

        var sourceId = '';
        var sourceObj = source.get_Item();
        if (sourceObj != null)
            sourceId = sourceObj.value;


        var decisionTimeFrameId = '';
        var decisionTimeFrameObj = decisionTimeFrame.get_Item();
        if (decisionTimeFrameObj != null)
            decisionTimeFrameId = decisionTimeFrameObj.value;

        var mainProductId = '';
        var mainProductObj = mainProductIntereset.get_Item();
        if (mainProductObj != null)
            mainProductId = mainProductObj.value;


        args.keyword = '';
        args.description = $(description).val();
        args.details = $(details).val();
        args.OpenedFrom = $(openedDatefrom).val();
        args.OpenedTo = $(openedDateTo).val();
        args.personId = personId;
        args.CompanyID = companyId;
        args.rating = ratingId;
        args.priority = priorityId;
        args.stage = stageId;
        args.industry = industryId;
        args.MainProduceIntereset = mainProductId;
        args.decisionTimeFrame = decisionTimeFrameId;
        args.source = sourceId;


    }
    else {
        args.keyword = $('#txtSearchLead').val();
        args.description = '';
        args.details = '';
        args.OpenedFrom = '';
        args.OpenedTo = '';
        args.personId = '';
        args.CompanyID = '';
        args.rating = '';
        args.priority = '';
        args.stage = '';
        args.industry = '';
        args.MainProduceIntereset = '';
        args.decisionTimeFrame = '';
        args.source = '';
    }


    var SearchCriteria = '{';
    for (var key in args) {
        if (args.hasOwnProperty(key))
            if (args[key] != "") {
                SearchCriteria = SearchCriteria + ('"' + key + '":' + '"' + args[key] + '",');
            }
    }


    SearchCriteria = SearchCriteria.slice(0, -1);
    SearchCriteria = SearchCriteria + '}';
    $('#pnlLeads').show();
    $('#loading').show();
    $.ajax({
        type: "POST",
        url: ApplicationURL + "Lead/Ajax.aspx",
        data: "SearchCriteria=" + SearchCriteria,
        success: function (data) {
            $('#loading').hide();
            $('#pnlLeads').html(data);
            $("#arrowImg").attr("src", "../../Images/" + sortDirection + ".gif");
        },
        error: function (e) {
            OnFailed();
        }
    });
}
function searchOpportunities(page, Stageid, FromTab, compnayID, personID) {
    args.view = 'grid';
    args.field = field;
    args.filter = filter;
    args.sortDirection = sortDirection;
    args.pn = page;
    args.lang = lang;
    args.Stageid = Stageid;
    args.pid = personID;
    args.tab = FromTab;
    args.cid = companyId;
    args.search = 'opportunity';
    args.flag = page;

    if (advanceSearch) {
        var companyId = -1;
        var companyObj = company.get_Item();
        if (companyObj != null)
            companyId = companyObj.value;

        var personId = -1;
        var personObj = person.get_Item();
        if (personObj != null) {
            personId = personObj.value;
        }


        var priorityId = '';
        var priorityObj = priority.get_Item();
        if (priorityObj != null)
            priorityId = priorityObj.value;

        var stageId = '';
        var stageObj = stage.get_Item();
        if (stageObj != null)
            stageId = stageObj.value;

        var sourceId = '';
        var sourceObj = source.get_Item();
        if (sourceObj != null)
            sourceId = sourceObj.value;


        var decisionTimeFrameId = '';
        var decisionTimeFrameObj = decisionTimeFrame.get_Item();
        if (decisionTimeFrameObj != null)
            decisionTimeFrameId = decisionTimeFrameObj.value;

        var mainProductId = '';
        var mainProductObj = mainProductIntereset.get_Item();
        if (mainProductObj != null)
            mainProductId = mainProductObj.value;

        var assignedToId = -1;
        var assignedToObj = assignedTo.get_Item();
        if (assignedToObj != null) {
            assignedToId = assignedToObj.value;
        }
        var typeId = '';
        var typeObj = type.get_Item();
        if (typeObj != null) {
            typeId = typeObj.value;
        }

        var statusId = '';
        var statusObj = statusList.get_Item();
        if (statusObj != null) {
            statusId = statusObj.value;
        }



        args.description = $(description).val();
        args.details = $(details).val();
        args.openedDateFrom = $(openedDatefrom).val();
        args.openedDateTo = $(openedDateTo).val();
        args.EstimatedClosedDateFrom = $(estimatedClosedDateFrom).val();
        args.EstimatedClosedDateTo = $(estimatedClosedDateTo).val();
        args.person = personId;
        args.company = companyId;
        args.priorityId = priorityId;
        args.StageId = stageId;
        args.MainProductInterestId = mainProductId;
        args.DecisionTimeframeId = decisionTimeFrameId;
        args.SourceId = sourceId;
        args.statusId = statusId;
        args.userId = assignedToId;
        args.typeId = typeId;

    }
    else {
        args.keyword = $("#txtSearchOpportunity").val();
        args.description = $("#txtSearchOpportunity").val();
        args.details = '';
        args.openedDateFrom = '';
        args.openedDateTo = '';
        args.EstimatedClosedDateFrom = '';
        args.EstimatedClosedDateTo = '';
        args.person = '';
        args.company = '';
        args.priorityId = '';
        args.StageId = '';
        args.MainProductInterestId = '';
        args.DecisionTimeframeId = '';
        args.SourceId = '';
        args.statusId = '';
        args.userId = '';
        args.typeId = '';
    }

    var SearchCriteria = '{';
    for (var key in args) {
        if (args.hasOwnProperty(key))
            if (args[key] != "") {
                SearchCriteria = SearchCriteria + ('"' + key + '":' + '"' + args[key] + '",');

            }
    }

    SearchCriteria = SearchCriteria.slice(0, -1);
    SearchCriteria = SearchCriteria + '}';

    $('#pnlOpportunities').show();
    $('#loading').show();

    $("#txtSearchPerson").removeAttr("disabled");
    $("#txtSearchCompany").removeAttr("disabled");

    $.ajax({
        type: "POST",
        url: ApplicationURL + "Opportunity/AjaxOpportunity.aspx",
        data: "SearchCriteria=" + SearchCriteria,
        success: function (data) {
            $('#loading').hide();
            $('#pnlOpportunities').html(data);
            $("#arrowImg").attr("src", "../../Images/" + sortDirection + ".gif");
        },
        error: function (e) {
            OnFailed();
        }
    });
}

function searchUsers(page) {
    args.view = 'grid';
    args.field = field;
    args.filter = filter;
    args.sortDirection = sortDirection;
    args.pn = page;
    args.lang = lang;
    args.search = 'user';
    args.flag = page;

    if (advanceSearch) {
        var RoleId = -1;
        var RoleObj = role.get_Item();
        if (RoleObj != null)
            RoleId = RoleObj.value;

        var teamId = -1;
        var teamObj = team.get_Item();
        if (teamObj != null)
            teamId = teamObj.value;

        var areaId = -1;
        var areaObj = area.get_Item();
        if (areaObj != null)
            areaId = areaObj.value;

        args.keyword = '';
        args.role = RoleId;
        args.team = teamId;
        args.title = $(title).val();
        args.DOBFrom = $(DOBFrom).val();
        args.DOBTo = $(DOBTo).val();
        args.area = areaId;
        args.firstName = $(firstName).val();
        args.middleName = $(middleName).val();
        args.lastName = $(lastName).val();
        args.firstNameAr = $(firstNameAr).val();
        args.middleNameAr = $(middleNameAr).val();
        args.lastNameAr = $(lastNameAr).val();

    }
    else {
        args.keyword = $("#txtSearchUser").val();
        args.role = '';
        args.team = '';
        args.title = '';
        args.DOBFrom = '';
        args.DOBTo = '';
        args.area = '';
        args.firstName = '';
        args.middleName = '';
        args.lastName = '';
        args.firstNameAr = '';
        args.middleNameAr = '';
        args.lastNameAr = '';
    }

    //var parameters = '';
    var SearchCriteria = '{';
    for (var key in args) {
        if (args.hasOwnProperty(key))
            if (args[key] != "") {
                //parameters = parameters + ("&" + key + "=" + args[key]);
                SearchCriteria = SearchCriteria + ('"' + key + '":' + '"' + args[key] + '",');

            }
    }
    SearchCriteria = SearchCriteria.slice(0, -1);
    SearchCriteria = SearchCriteria + '}';

    $('#pnlUsers').show();
    $('#loading').show();
    $.ajax({
        type: "POST",
        url: ApplicationURL + "Users/UsersAjax.aspx",
        data: "SearchCriteria=" + SearchCriteria,
        success: function (data) {
            $('#loading').hide();
            $('#pnlUsers').html(data);
            $("#arrowImg").attr("src", "../../Images/" + sortDirection + ".gif");
        },
        error: function (e) {
            OnFailed();
        }
    });
}
function searchProjects(page) {
    args.view = 'grid';
    args.pn = page;
    args.field = field;
    args.filter = filter;
    args.sortDirection = sortDirection;
    args.lang = lang;
    args.flag = page;

    if (advanceSearch) {
        var companyId = -1;
        var companyObj = company.get_Item();
        if (companyObj != null)
            companyId = companyObj.value;

        var opportunityId = -1;
        var opportunityObj = opportunity.get_Item();
        if (opportunityObj != null)
            opportunityId = opportunityObj.value;

        var projectManagerId = -1;
        var projectManagerObj = projectManager.get_Item();
        if (projectManagerObj != null)
            projectManagerId = projectManagerObj.value;

        var customerProjManagerId = -1;
        var customerProjManagerObj = customerProjManager.get_Item();
        if (customerProjManagerObj != null)
            customerProjManagerId = customerProjManagerObj.value;

        var statusId = '';
        var statusObj = projectStatus.get_Item();
        if (statusObj != null)
            statusId = statusObj.value;

        var productId = '';
        var productObj = product.get_Item();
        if (productObj != null)
            productId = productObj.value;


        args.keyword = '';
        args.CompanyId = companyId;
        args.OpportunityId = opportunityId;
        args.projectManagerId = projectManagerId;
        args.CustomerProjectManagerId = customerProjManagerId;
        args.statusId = statusId;
        args.productId = productId;
        args.refNumber = $(refNum).val();
        args.ProjectScope = $(projScope).val();
        args.details = $(details).val();
        args.ProjectName = $(name).val();
        args.startDate = $(txtSD).val();
        args.endDate = $(txtED).val();
        args.estimatedDate = $(txtEsD).val();
        args.actualStartDate = $(txtASD).val();
        args.actualEndDate = $(txtAED).val();
        args.contractDate = $(txtCD).val();

    }
    else {
        args.keyword = $("#" + txtbox).val();
        args.CompanyId = '';
        args.OpportunityId = '';
        args.projectManagerId = '';
        args.CustomerProjectManagerId = '';
        args.statusId = '';
        args.productId = '';
        args.refNumber = '';
        args.ProjectScope = '';
        args.details = '';
        args.ProjectName = '';
        args.startDate = '';
        args.endDate = '';
        args.estimatedDate = '';
        args.actualStartDate = '';
        args.actualEndDate = '';
        args.contractDate = '';
    }


    var SearchCriteria = '{';
    for (var key in args) {
        if (args.hasOwnProperty(key))
            if (args[key] != "") {
                SearchCriteria = SearchCriteria + ('"' + key + '":' + '"' + args[key] + '",');
            }
    }
    SearchCriteria = SearchCriteria.slice(0, -1);
    SearchCriteria = SearchCriteria + '}';

    $('#pnlProjects').show();
    $('#loading').show();
    $.ajax({
        type: "POST",
        url: ApplicationURL + "Projects/Project_ajax.aspx",
        data: "SearchCriteria=" + SearchCriteria,
        success: function (data) {
            $('#loading').hide();
            $('#pnlProjects').html(data);
            $("#arrowImg").attr("src", "../../Images/" + sortDirection + ".gif");
        },
        error: function (e) {
            OnFailed();
        }
    });
}




function searchCompanies(page) {
    args.view = 'grid';
    args.field = field;
    args.filter = filter;
    args.sortDirection = sortDirection;
    args.pn = page;
    args.flag = page;
    args.IsQualified = true;
    var companyStatus = '';

    if (advanceSearch) {
        //        var EmpNumId = '';
        //        var EmpNumObj = EmpNum.get_Item();
        //        if (EmpNumObj != null) {
        //            EmpNumId = EmpNumObj.value;

        //        }

        //        var YearlyRevenueId = '';
        //        var YearlyRevenueObj = YearlyRevenue.get_Item();
        //        if (YearlyRevenueObj != null)
        //            YearlyRevenueId = YearlyRevenueObj.value;

        var SourceId = '';
        var SourceObj = Source.get_Item();
        if (SourceObj != null)
            SourceId = SourceObj.value;

        var SegmentId = '';
        var SegmentObj = Segment.get_Item();
        if (SegmentObj != null)
            SegmentId = SegmentObj.value;

        var SalesManId = -1;
        var SalesManObj = SalesMan.get_Item();
        if (SalesManObj != null) {
            SalesManId = SalesManObj.value;
            args.SalesManName = SalesManObj.label;
        }

        var TypeId = '';
        var TypeObj = Type.get_Item();
        if (TypeObj != null)
            TypeId = TypeObj.value;

        //        var DSId = -1;
        //        var DSObj = DS.get_Item();
        //        if (DSObj != null) {
        //            DSId = DSObj.value;
        //            args.DSName = DSObj.label;
        //        }

        //        var CountryId = -1;
        //        var CountryObj = Country.get_Item();
        //        if (CountryObj != null) {
        //            CountryId = CountryObj.value;
        //            args.countryName = CountryObj.label;
        //        }

        //        var CityId = -1;
        //        var CityObj = City.get_Item();
        //        if (CityObj != null) {
        //            CityId = CityObj.value;
        //            args.cityName = CityObj.label;
        //        }

        //        var AreaId = -1;
        //        var AreaObj = Area.get_Item();
        //        if (AreaObj != null) {
        //            AreaId = AreaObj.value;
        //            args.areaName = AreaObj.label;
        //        }

        if ($(statusActive).attr('checked') == true && $(statusInActive).attr('checked') == false)
            companyStatus = true;
        else if ($(statusActive).attr('checked') == false && $(statusInActive).attr('checked') == true)
            companyStatus = false;


        args.website = $(websit).val();
        args.email = '';
        args.abbreviation = $(abbreviation).val();
        args.RegistrationNumber = '';
        args.RegistrationDate = '';
        args.TypeId = TypeId;
        args.salesManId = SalesManId;
        args.countryId = 0;
        args.cityId = 0;
        args.areaId = 0;
        args.segmentId = SegmentId;
        args.sourceId = SourceId;
        args.yearlyRevenueId = 0;
        args.EmployeesNumberId = 0;
        args.AreaCode = '';
        args.CountryCode = '';
        args.Phone = '';
        args.DecisionMakerId = 0;
        args.companyName = $(companyName).val();
        args.companyNameAr = '';
        args.keyword = '';

    }
    else {
        args.keyword = $('#' + txtbox).val();
        args.website = '';
        args.email = '';
        args.abbreviation = '';
        args.RegistrationNumber = '';
        args.RegistrationDate = '';
        args.TypeId = 0;
        args.salesManId = 0;
        args.countryId = 0;
        args.cityId = 0;
        args.areaId = 0;
        args.segmentId = 0;
        args.sourceId = 0;
        args.yearlyRevenueId = 0;
        args.EmployeesNumberId = 0;
        args.AreaCode = '';
        args.CountryCode = '';
        args.Phone = '';
        args.DecisionMakerId = '';
        args.companyName = '';
        args.companyNameAr = '';
    }

    var SearchCriteria = '{';
    for (var key in args) {
        if (args.hasOwnProperty(key)) {

            if (args[key] != "") {
                SearchCriteria = SearchCriteria + ('"' + key + '":' + '"' + args[key] + '",');
            }
        }
    }
    SearchCriteria = SearchCriteria + ('"status":' + '"' + companyStatus + '",');
    SearchCriteria = SearchCriteria.slice(0, -1);
    SearchCriteria = SearchCriteria + '}';

    if (keyword == null || keyword == "")
        $('#txtSelectedCompany').val(0);
    $('#pnlCompanies').show();
    $('#loading').show();
    $.ajax({
        type: "POST",
        url: ApplicationURL + "Companies/CompanyAjax.aspx",
        data: "SearchCriteria=" + SearchCriteria,
        success: function (data) {
            $('#loading').hide();
            $('#pnlCompanies').html(data);
            $("#arrowImg").attr("src", "../../Images/" + sortDirection + ".gif");
        },
        error: function (e) {
            OnFailed();
        }
    });
}

function SetSearchMethod(option) { advanceSearch = option; }
function searchRelatedPerson(page, personID) {
    $('#pnlRelatedPersons').show();
    $('#loading').show();

    $.ajax({
        type: "POST",
        url: "../Persons/RelatedPersonAjax.aspx",
        data: "pn=" + page + "&pid=" + personID,
        success: function (data) {
            $('#loading').hide();
            $('#pnlRelatedPersons').html(data);
        },
        error: function (e) {
            OnFailed();
        }
    });
}

function SearchDocumentFolder(keyword, page) {
    $('#loading').show();
    $.ajax({
        type: "POST",
        url: ApplicationURL + "DocumentFolder/DocumentFolderAjax.aspx",
        data: "keyword=" + keyword + "&pn=" + page,
        success: function (data) {
            $('#pnl-DocumentFolder').empty();
            $('#pnl-DocumentFolder').html(data);
            $('#loading').hide();
        },
        error: function (e) {
            OnFailed();
        }
    });
}