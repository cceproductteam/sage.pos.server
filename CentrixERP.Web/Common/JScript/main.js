$(document).ready(function () {
    $('.Page-body').height($(window).height() - 51);

    /// For Quotation and Contract Full View
    $('.white-container-NewMaster').css('min-height', $(window).height() - 70);
    $('.Quotation-left').css('min-height', $('.white-container-NewMaster').height() - 70);

    $('.white-container').height($('.Page-body').height() - 32);
    $('.white-shadow-container').height($('.white-container').height());
    $('.white-container-data').height($('.white-container').height());
    $('.Result-info-container').height($('.white-container-data').height() - 9);
    $('.result-container').height($('.white-container').height());
    $('.test').height($('.Result-info-container').height() - 30);
    $('.TabbedPanels').height($('.Result-info-container').height() - 59);

    $('.AdvancedSearch').height($('.white-container-data').height() - 13);
    $('.AdvancedSearch-form-fields').height($('.AdvancedSearch').height() - 46);
    $('.saved-search-list').height($('.AdvancedSearch').height() - 5);
    $('.saved-search-list-links').height($('.saved-search-list').height() - 107);
    $('.GridTableDiv-body').height($('.Result-info-container').height() - 80);
    $('.Adminstration-main-div').height($('.white-container-data').height());

    $('.TabbedPanelsTab').bind('click', function () {
        $('.TabbedPanelsTab').children('.TabbedPanelsTabSelected-arrow').css("display", "none");
        $(this).children('.TabbedPanelsTabSelected-arrow').css("display", "block");

        /// START - Hot Fix For Tab Container Height and Scroll Top
        var TabContainerHeight = $('.Result-info-container').height() > $('.TabbedPanelsTabGroup').height() ? $('.Result-info-container').height() - 61 : $('.TabbedPanelsTabGroup').height() - 35;
        $('.TabbedPanelsContentGroup').css('min-height', TabContainerHeight);
        $('.TabbedPanelsTabGroup').css('min-height', TabContainerHeight);
        $('#demo3').find('.alt-scroll-content,.alt-scroll-vertical-bar').css({ top: 0 });
        //$('.TabbedPanelsContentFieldset').alternateScroll();
        /// END - Hot Fix For Tab Container Height and Scroll Top
    });

    $('.ActionFlag-link').bind('click', function () {

        var entity = $(this).attr('entity');
        var entityID = $(this).attr('id');
        if ($(this).hasClass('selectedflag')) {
            SetCookie("flag_" + entity + "_" + entityID, 1, -1);
            $('.Result-list').find('.result-blockVisited').find('.flag').removeClass('selectedflag');
        }
        else {
            SetCookie("flag_" + entity + "_" + entityID, 1, 100);
            $('.Result-list').find('.result-blockVisited').find('.flag').addClass('selectedflag');
        }

        $(this).toggleClass('selectedflag');

    });

    $('.details-grid TR').bind('mouseover', function () {
        $(this).children('td.td-r1-data-table, td.td-r2-data-table').addClass("td-data-hover");
    });

    $('.details-grid TR').bind('mouseout', function () {
        $(this).children('td.td-r1-data-table, td.td-r2-data-table').removeClass("td-data-hover");
    });


    /// Start More Tab Actions
    $('body').click(function (e) {
        if (!$(e.target).is('.More-tabs, .More-tab, .moreTabsList, .more-title')) {
            $('.moreTabsList').hide().css('opacity', '0');
            $('.More-tabs-arrow').hide().css('opacity', '0');
            $('.More-tabs').css('opacity', '0.5');
        }
    });


    var moreLi = $('.more-tabs');
    $('.more-tabs').remove();
    var numberOfTabs = $('.TabbedPanelsTabGroup li').length;
    var heightOfLi = $('.TabbedPanelsTabGroup li').outerHeight();
    var sum = 0;
    $('.TabbedPanelsTabGroup li').each(function () {
        sum += $(this).outerHeight();
        if (sum < ($('.TabbedPanels').height() - heightOfLi)) {
            $(this).show();
            $('.TabbedPanelsTabGroup').append($(this));
        }
        else {
            $(this).css('display', 'block');
            $('.moreTabs-ul').append($(this).removeClass('TabbedPanelsTab finalTab').addClass('moreTabsList-divs'));
        }
    });

    $('.TabbedPanelsTabGroup').append($(moreLi));
    if (sum > ($('.TabbedPanels').height() - heightOfLi)) {
        $('.more-tabs').show();
    }



    $('.More-tabs, .More-tab, .more-title, .more-tabs').click(function () {
        $(this).css('opacity', '1');
        $('.moreTabsList').show().animate({ opacity: 1 }, 600);
        $('.More-tabs-arrow').show().animate({ opacity: 1 }, 600);
    });

    $('.moreTabsList-divs').bind('click', function () {

        var lastLi = $('.TabbedPanelsTabGroup').children('li').last().prev();
        $('.more-tabs').before($(this).removeClass('moreTabsList-divs').addClass('TabbedPanelsTab finalTab TabbedPanelsTabSelected'));

        var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");

        $(lastLi).remove();
        $('.moreTabs-ul').prepend($(lastLi).addClass('moreTabsList-divs').removeClass('TabbedPanelsTab finalTab TabbedPanelsTabSelected'));

        $(this).children('.TabbedPanelsTab-img').css('position', 'relative');
        $(this).children('.TabbedPanelsTab-img').animate({ bottom: 5 }, 300);
        $(this).children('.TabbedPanelsTab-img').animate({ bottom: 0 }, 300);

        $('.TabbedPanelsTab').removeClass('TabbedPanelsTabSelected');
        $(this).addClass('TabbedPanelsTabSelected');


        $('.TabbedPanelsContent').hide();
        $('.TabbedPanelsContentGroup').find("[tabindex='" + $(this).attr('tabindex') + "']").show();

    });

    /// End More Tab Actions
});

function SetCookie(cookieName, cookieValue, nDays) {
    var today = new Date();
    var expire = new Date();
    if (nDays == null || nDays == 0) nDays = 1;
    expire.setTime(today.getTime() + 3600000 * 24 * nDays);
    document.cookie = cookieName + "=" + escape(cookieValue)
                 + ";expires=" + expire.toGMTString();
}

function readCookie(cookieName) {
    var re = new RegExp('[; ]' + cookieName + '=([^\\s;]*)');
    var sMatch = (' ' + document.cookie).match(re);
    if (cookieName && sMatch) return unescape(sMatch[1]);
    return '';
}

function GetTemaplte(template, callback, variable) {
    $.ajax({
        type: 'GET',
        url: template + '?v=' + Centrix_Version,
        success: function (data) {
            var result = SetResourceKey(data);
            if (variable != null)
                eval(variable + "=result;");
            if (callback != null && callback != "")
                callback(result);
        },
        error: function (e) {
        }
    });
}

//just for add
function attachControlsValidation() {
    $('.cx-control').each(function () {
        $(this).blur(function () {
            validateForm(this);
        });
    });
}

function formatString() {
    var s = arguments[0];
    for (var i = 0; i < arguments.length - 1; i++) {
        var arg = arguments[i + 1];
        if (Object.prototype.toString.call(arg) === '[object Array]') {
            var tempIndex = i;
            $.each(arg, function (ix, item) {
                var reg = new RegExp("\\{" + tempIndex + "\\}", "gm");
                s = s.replace(reg, item);
                tempIndex++;
            });
        }
        else {
            var reg = new RegExp("\\{" + i + "\\}", "gm");
            s = s.replace(reg, arguments[i + 1]);
        }
    }
    return s;
}


function SetDefault(id, ParentId, mKey, serviceURL, isDefault, UserId, callback) {
    var data = formatString('{Id:{0},moduleId:{1}, mKey:"{2}",IsDefault:{3},userId:{4}}', id, ParentId, mKey, isDefault, UserId);
    $.ajax({
        type: "POST",
        url: serviceURL,
        contentType: "application/json; charset=utf-8",
        data: data,
        dataType: "json",
        success: function (data) {
            if (typeof callback == 'function')
                callback();
        },
        error: function (e) { }
    });
}



function attachTabEvents(newTab) {


    $('li.Tablist').mouseover(function () {
        if ($(this).hasClass('Tab-listSelected')) {
            $(this).css('background-color', '#e7e9e9');
            $(this).find(".Action-div").show();
        }
        else {
            $(this).css('background-color', '#c7daa8');
            $(this).find(".Action-div").show();
        }
    });

    $('li.Tablist').mouseout(function () {
        if (!$(this).hasClass('Tab-listSelected')) {
            $(this).css('background-color', '#fff');
            $(this).find(".Action-div").hide();
        }

    });


    $(".addnew1").click(function (e) {
        var template = $(this);
        template.find('.itemheader').click();
        template.find('.viewedit-display-none').show();
        template.find('.viewedit-display-block').hide();
        template.find('.edit-tab-lnk').remove();
        template.find('.save-tab-lnk').show();
        template.find('.cancel-tab-lnk').show();
        template.find('.delete-tab-lnk').hide();
        if (template.is(':visible')) {
            template.hide();
        }
        else {
            template.show();
        }
        return false;
    });

    $(".itemheader").click(function (e) {


        if (!$(e.target).hasClass('action-anchor')) {
            var parent = $(this).parent();
            if ($(parent).hasClass('Tab-listSelected')) {
                if (!$(this).parent().hasClass('addnew')) {
                    $(parent).find('.Tab-basics-info').show();
                    $(parent).removeClass("Tab-listSelected");
                    $(parent).children('.Tab-full-info').slideUp();
                    $(parent).animate({ minHeight: 42 }, 1000, function () {
                        $(parent).css('background-color', '#FFF');

                        $(parent).find('.viewedit-display-none').hide();
                        $(parent).find('.viewedit-display-block').show();
                    });
                    $(parent).children('.Tab-name').css('color', '#95b660');

                    $(this).children(".Action-div").find('.save-tab-lnk, .cancel-tab-lnk').hide();
                    $(this).children(".Action-div").find('.edit-tab-lnk').show();
                }

            }
            else {
                $(parent).find('.Tab-basics-info').hide();
                $(parent).addClass("Tab-listSelected");
                $(parent).children(".Tab-divHolder").css('color', '#4f4f4f');
                $(parent).animate({ maxHeight: 665 }, 1000, function () {

                });

                $(this).children(".action-list").show();
                // $(this).children(".action-list").animate({ "left": "+=290px" }, 400);
                $(parent).children('.Tab-name').css('color', '#4f4f4f');
                $(parent).children('.Tab-full-info').slideDown(350);
                $(parent).css('background-color', '#e7e9e9');

                $(parent).find('.viewedit-display-none').hide();
                $(parent).find('.viewedit-display-block').show();
            }
        }
        //  return false;
    });



}

function fillDataTypeContentList(contentAC, dataTypeId, required, multiSelect, hasDefaultItem) {
    contentAC = $(contentAC).AutoComplete({
        serviceURL: systemConfig.ApplicationURL_Common + systemConfig.dataTypeContentServiceURL + "FindAll",
        autoLoad: false, required: required, staticParams: false, multiSelect: multiSelect, hasDefaultItem: hasDefaultItem
    });
    contentAC.set_Args(formatString('{parentId:{0}}', dataTypeId));
    contentAC.fillDefaultValue();
    return contentAC;
}

function preventInputChars(element) {
    $(element).keydown(function (event) {
        if ((event.keyCode < 48 || event.which > 57) && (event.which < 96 || event.which > 105) && (event.which != 8) && (event.which != 9) || event.shiftKey) {
            event.preventDefault();
        }
    });
}


function LoadDatePicker(input, format) {
       /// Changed By Omar Bazzari - Remove Icon & enable clear using backspace
    $(input).datepicker({
        yearRange: 'c-100:c+50',
        changeMonth: true,
        changeYear: true,
        appendText: '',
        dateFormat: (format === undefined || format == null || format == '') ? 'dd-mm-yy' : format
    }).click(function () {
        $(this).select();
    }).keydown(function (e) {
        if (e.keyCode == 8)
            $(this).val('');
        if (!(e.keyCode == 9 || e.keyCode == 46)) {
            e.preventDefault();
        }
    });


}

function getDate(format, date) {
    if (date != null && date != "") {
        try {
            date = date.replace(/-/g, "/")
        }
        catch (e) {
        }
    }


    if (date != null && date != "") {
        var d = new Date(date);
        if (!isNaN(d.getTime())) {
            var curr_date = (d.getDate() < 10) ? "0" + d.getDate() : d.getDate();
            var curr_month = (d.getMonth() + 1 < 10) ? "0" + (d.getMonth() + 1) : d.getMonth() + 1;
            var curr_year = d.getFullYear();
            var curr_hour = (d.getHours() < 10) ? "0" + (d.getHours()) : d.getHours();
            var curr_minute = (d.getMinutes() + 1 < 10) ? "0" + (d.getMinutes()) : d.getMinutes();
            var time;
            var formattedDated;
            if (curr_hour > 12) {
                curr_hour -= 12;
                time = "PM";
            }
            else {
                time = "AM"
            }
            switch (format) {
                case "yy-mm-dd":
                    formattedDated = curr_year + "-" + curr_month + "-" + curr_date;
                    break;
                case "mm-dd-yy":
                    formattedDated = curr_month + "-" + curr_date + "-" + curr_year;
                    break;
                case "dd-mm-yy":
                    formattedDated = curr_date + "-" + curr_month + "-" + curr_year;
                    break;
                case "mm-yy":
                    formattedDated = curr_month + "-" + curr_year;
                    break;

                case "yy-mm-dd hh:mm tt":
                    if (curr_hour == "00" && curr_minute == "00")
                        formattedDated = curr_year + "-" + curr_month + "-" + curr_date;
                    else
                        formattedDated = curr_year + "-" + curr_month + "-" + curr_date + ' ' + curr_hour + ":" + curr_minute + ' ' + time;
                    break;
                case "dd-mm-yy hh:mm tt":
                    if (curr_hour == "00" && curr_minute == "00")
                        formattedDated = curr_date + "-" + curr_month + "-" + curr_year;
                    else
                        formattedDated = curr_date + "-" + curr_month + "-" + curr_year + ' ' + curr_hour + ":" + curr_minute + ' ' + time;
                    break;
                default:
                    formattedDated = curr_year + "-" + curr_month + "-" + curr_date;
                    break;
            }

            return formattedDated;
        }
        else
            return date;
    }


    else
        return "";
}

if (typeof String.prototype.trim !== 'function') {
    String.prototype.trim = function () {
        return this.replace(/^\s+|\s+$/g, '');
    }
}

(function ($) {
    $.fn.Show = function () {
        this.each(function () {
            $(this).removeClass('display-none').css('display', 'inline-block');
        });
    };
})(jQuery);


//Start Translation To Arabic Functions
function getResource(resource) {

    if (Lang == 'ar') {
        var value = DataDictionaryAR[resource];
        return value ? value : "";
    }
    else {
        var value = DataDictionaryEN[resource];
        return value ? value : "";
    }
}


function SetResourceKey(data) {
    var template = $(data);
    $.each($(template).find('[ResourceKey]'), function (index, value) {
        if (Lang == 'ar')
            $(this).text(DataDictionaryAR[$(this).attr('ResourceKey')]);
        else
            $(this).text(DataDictionaryEN[$(this).attr('ResourceKey')]);
    });

    $.each($(template).find('[title]'), function (index, value) {
        if (Lang == 'ar')
            $(this).attr("title", DataDictionaryAR[$(this).attr("TResourcekey")]);
        else
            $(this).attr("title", DataDictionaryEN[$(this).attr('TResourcekey')]);
    });

    return template;

}


function RemoveFloatNumberFormatting(floatString) {
    return floatString.toString().replace(/\,/g, '');
}