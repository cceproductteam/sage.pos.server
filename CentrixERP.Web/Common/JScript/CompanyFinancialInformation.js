var PaymentCodeAC;
var PaymentTermAC;
var StatementCurrencyAC;
var CustomerGroupAC;
var VendorGroupAC;
var SupplierCategoryAC;


function getcompanyFinancialInformation(ParentId) {
    $('.companyFinancialInformation-tab').children().remove();
    var data = formatString('{ParentId:{0}"}', ParentId);
    $.ajax({
        type: "POST",
        url: systemConfig.CompanyFinancialInformationServiceURL + "FindAll",
        contentType: "application/json; charset=utf-8",
        data: data,
        dataType: "json",
        success: function (data) {
            var CompanyFinancialInformationObj = eval("(" + data.d + ")");
            if (CompanyFinancialInformationObj.result != null) {
                $('.companyFinancialInformation-tab').parent().find('.DragDropBackground').remove();
                addFinancialInformationTab(null, ParentId);
                $.each(CompanyFinancialInformationObj.result, function (index, item) {
                    addFinancialInformationTab(item, ParentId);
                });
                attachTabEvents();

            }
        },
        error: function (e) {
        }
    });

}




function addFinancialInformationTab(item, ParentId) {
    var id = (item != null) ? item.Id : 0;
    var template = $(CompanyFinancialInformationTemplate);
    PaymentCodeAC = fillDataTypeContentList('select:.PaymentCode', systemConfig.dataTypes.paymentCode);
    PaymentTermAC = fillDataTypeContentList('select:.PaymentTerm', systemConfig.dataTypes.paymentTerm);
    StatementCurrencyAC = fillDataTypeContentList('select:.StatementCurrency', systemConfig.dataTypes.Currency);
    CustomerGroupAC = fillDataTypeContentList('select:.CustomerGroup', systemConfig.dataTypes.customerGroup);
    VendorGroupAC = fillDataTypeContentList('select:.VendorGroup', systemConfig.dataTypes.vendorGroup);
    SupplierCategoryAC = fillDataTypeContentList('select:.SupplierCategory', systemConfig.dataTypes.supplierCategory);



    if (item != null) {
        template = mapEntityInfoToControls(item, template, [{ controlName: 'PaymentCodeAC', control: PaymentCodeAC }, { controlName: 'PaymentTermAC', control: PaymentTermAC }, { controlName: 'StatementCurrencyAC', control: StatementCurrencyAC }, { controlName: 'CustomerGroupAC', control: CustomerGroupAC }, { controlName: 'VendorGroupAC', control: VendorGroupAC }, { controlName: 'SupplierCategoryAC', control: SupplierCategoryAC}]);
    } else {
        template.hide();
        $('.add-new-companyFinancialInformation').click(function () {
            template.find('.itemheader').click();
            template.find('.viewedit-display-none').show();
            template.find('.viewedit-display-block').hide();

            template.find('input').val('');
            PaymentCodeAC.clear();
            PaymentTermAC.clear();
            StatementCurrencyAC.clear();
            CustomerGroupAC.clear();
            VendorGroupAC.clear();
            SupplierCategoryAC.clear();

            template.find('.edit-tab-lnk').remove();
            template.find('.save-tab-lnk').show();
            template.find('.cancel-tab-lnk').show();
            template.find('.delete-tab-lnk').hide();
            template.find('.validation').remove();
            if (template.is(':visible')) {
                template.hide();
            }
            else {
                template.show();
            }
            return false;
        });
    }

    template.find('.save-tab-lnk').on('click', function () {
        saveCompanyFinancialInformation(id, ParentId, template, PaymentCodeAC, PaymentTermAC, StatementCurrencyAC, CustomerGroupAC, VendorGroupAC, SupplierCategoryAC);
        template.find('.edit-tab-lnk').show();
        return false;
    });


    template.find('.delete-tab-lnk').click(function () {
        deleteCompnayFinancialInformation(id, ParentId);
        return false;
    })


    template.find('.edit-tab-lnk').click(function () {
        template.find('.edit-tab-lnk').hide();
        template.find('.save-tab-lnk').show();
        template.find('.cancel-tab-lnk').show();
        template.find('.viewedit-display-none').show();
        template.find('.viewedit-display-block').hide();
        return false;
    })


    template.find('.cancel-tab-lnk').on('click', function () {
        template.find('.itemheader').click();
        getCompnayFinancialInformation(ParentId);
        template.find('.edit-tab-lnk').show();
        template.find('.save-tab-lnk').hide();
        return false;
    });

    $('.companyFinancialInformation-tab').append(template);

}




function saveCompanyFinancialInformation(id, parentId, template, paymentCodeAC, paymentTermAC, statementCurrencyAC, customerGroupAC, vendorGroupAC, supplierCategoryAC) {

    var paymentCodeId = paymentCodeAC.get_Item() != null ? paymentCodeAC.get_Item().value : -1;
    var paymentTermId = paymentTermAC.get_Item() != null ? paymentTermAC.get_Item().value : -1;
    var statementCurrencyId = statementCurrencyAC.get_Item() != null ? statementCurrencyAC.get_Item().value : -1;
    var customerGroupId = customerGroupAC.get_Item() != null ? customerGroupAC.get_Item().value : -1;
    var vendorGroupId = vendorGroupAC.get_Item() != null ? vendorGroupAC.get_Item().value : -1;
    var supplierCategoryId = supplierCategoryAC.get_Item() != null ? supplierCategoryAC.get_Item().value : -1;

    var bankName = template.find('input:.BankName').val();
    var bankAccountNo = template.find('input:.BankAccountNo').val();
    var sortCode = template.find('input:.SortCode').val();
    var iban = template.find('input:.Iban').val();
    var swiftCode = template.find('input:.SwiftCode').val();
    var vat = template.find('input:.Vat').val();
    var CreditController = template.find('input:.CreditController').val();
    var NextCreditLimitDate = template.find('input:.NextCreditLimitDate').val();
    var CreditLimit = template.find('input:.CreditLimit').val();


    var data = '{CompanyId:{0}, FInformationId:{1}, BankName:"{2}",BankAccountNo:"{3}",SortCode:"{4}",Iban:"{5}",SwiftCode:"{6}",Vat:"{7}",CreditController:"{8}",NextCreditLimitDate:"{9}",CreditLimit:"{10}",CustomerGroupId:"{11}",PaymentCodeId:"{12}",PaymentTermId:"{13}",StatementCurrencyId:"{14}",VendorGroupId:"{15}",SupplierCategoryId:"{16}",userId:"{5}"}';
    data = formatString(data, parentId, id, bankName, bankAccountNo, sortCode, iban, swiftCode, vat, CreditController, NextCreditLimitDate, CreditLimit, customerGroupId, paymentCodeId, paymentTermId, statementCurrencyId, vendorGroupId, supplierCategoryId, UserId);

    $.ajax({
        type: "POST",
        url: systemConfig.CompanyFinancialInformationServiceURL + "AddEditCompanyFinancialInformation",
        contentType: "application/json; charset=utf-8",
        data: data,
        dataType: "json",
        success: function (data) {
            getcompanyFinancialInformation(parentId);
        },
        error: function (e) {
        }
    });
}

function deleteCompnayFinancialInformation(id, parentId) {
    $.ajax({
        type: "POST",
        url: systemConfig.CompanyFinancialInformationServiceURL + "DeleteCompanyFinancialInformation",
        contentType: "application/json; charset=utf-8",
        data: formatString('{id:{0}}', id),
        dataType: "json",
        success: function (data) {
            getcompanyFinancialInformation(parentId);
        },
        error: function (e) {
        }
    });
}
