<%@ Page Title="" Language="C#" MasterPageFile="../MasterPage/ERPSite.Master" AutoEventWireup="true"
    CodeBehind="ERPEntityLockList.aspx.cs" Inherits="SagePOS.Common.Web.Common.Settings.ERPEntityLockList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHeader" runat="server">
    <script src="JS/ERPEntityLock.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <link href="CSS/EntityLockCSS.css?v=<%=Centrix_Version %>" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageBody" runat="server">
    <div class="LockPage">
        <div class="LockPage-action-div display-none">
            <span class="lbl-title">Select user </span>
            <select class="lstUser">
            </select>
             <span class="ActionUnlock-link unlock-all-user"></span>
            <a class="link unlock-all-user">Unlock for all users</a>
             <%-- <a class="crm-lock-link lnkLabel"><span class="lbl-title ">CRM Entity lock</span></a>--%>
        </div>
        <div class="entity-lock-div">
        </div>
        <div class="DragDropBackground  display-none">
            <span class="background-text">No Data To Be Found</span>
        </div>
    </div>
</asp:Content>
