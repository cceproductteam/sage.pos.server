﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Setup.aspx.cs" Inherits="SagePOS.Common.Web.NewCommon.Common.Settings.Setup"  MasterPageFile="../MasterPage/ERPSite.Master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="PageHeader" runat="server">
    <script src="JS/Setup.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
<%--    <link href="CSS/setup.css?v=<%=Centrix_Version %>" rel="stylesheet" type="text/css" />--%>
    <link href="css/new_style.css" rel="stylesheet" type="text/css">
    <link href="css/featherlight.css" rel="stylesheet" type="text/css">
    <script type="text/javascript">
        $(document).ready(function () {
            $('.Top-Mid-section').remove();
        });
        function InnerPage_Load() {
        }

        !function (d, s, id) { var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https'; if (!d.getElementById(id)) { js = d.createElement(s); js.id = id; js.src = p + '://platform.twitter.com/widgets.js'; fjs.parentNode.insertBefore(js, fjs); } } (document, 'script', 'twitter-wjs');

        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
			m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//stats.g.doubleclick.net/dc.js', 'ga');

        ga('create', 'UA-5342062-6', 'noelboss.github.io');
        ga('send', 'pageview');
    </script>
    <script src="js/featherlight.js" type="text/javascript" charset="utf-8"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageBody" runat="server">
   <div id="pageContainerInner">
        <div class="ModulesBlock">
            <div class="iconBlock">
                <img src="images/transactionalSetup.png" alt="" width="84" height="84"></div>
            <h2>
                <span resourcekey="CentrixSetup"></span></h2>
            <div class="BtnsBox">
                <div class="BtnContainer">
                    <a href="../../NewUserManagement/Users/UsersList.aspx" class="mod"  resourcekey="SPUsers"></a> 
                    <a href="#" class="help btn btn-default" ></a>
                </div>
                 <div class="BtnContainer">
                    <a href="../../NewUserManagement/Roles/RolesList.aspx" class="mod"  resourcekey="SPRols"></a> 
                    <a href="#" class="help btn btn-default" ></a>
                </div>
                 <div class="BtnContainer">
                    <a href="../../NewUserManagement/Teams/TeamsList.aspx" class="mod"  resourcekey="SPTeams"></a> 
                    <a href="#" class="help btn btn-default" ></a>
                </div>
                 <div class="BtnContainer">
                    <a href="../../newUserManagement/Roles/RolePermission.aspx" class="mod"  resourcekey="SPPermissions"></a> 
                    <a href="#" class="help btn btn-default" ></a>
                </div>
                <div class="BtnContainer">
                    <a href="../../newUserManagement/Shifts/ShiftsList.aspx" class="mod"  resourcekey="Shifts"></a> 
                    <a href="#" class="help btn btn-default" ></a>
                </div>
              
            </div>
        </div>
        <div class="ModulesBlock">
            <div class="iconBlock">
                <img src="images/dataDefinition.png" alt="" width="84" height="84"></div>
            <h2>
                <span resourceKey="EntityLockController"></span></h2>
            <div class="BtnsBox">
                <div class="BtnContainer">
                    <a href="ERPEntityLockList.aspx" class="mod"  resourcekey="SPEntitylock"></a> <a href="#" class="help btn btn-default"
                       ></a>
                </div>
                
            </div>
        </div>
       
    </div>

   <iframe class="lightbox" src="index.html" width="600" height="400" id="fl3" style="border: none;"
        webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
</asp:Content>