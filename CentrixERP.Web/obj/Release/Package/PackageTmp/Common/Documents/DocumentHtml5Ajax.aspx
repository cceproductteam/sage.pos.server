<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocumentHtml5Ajax.aspx.cs"
    Inherits="SagePOS.Common.Web.Documents.DocumentHtml5Ajax" %>
    
<%@ Register TagPrefix="SF" Namespace="SFLib.CustomControls" Assembly="SFCustomControls" %>
<asp:panel id="pnlDocumentGrid" runat="server" class="pnldocumentgrid">

<script type="text/javascript">
    $(document).ready(function () {

        $('.File-img-title').each(function () {
            var txtTrim = $.trim($(this).text());
            if (txtTrim.length > 16) {
                var smallfileImgName = txtTrim.substring(0, 13);
                smallfileImgName = smallfileImgName + "...";
                $(this).text(smallfileImgName);
            }
            else { }
        });

    });

    function openDocument(url) {
        window.location = url;
    }
</script>
    

    
    <div id="1Filesorder" class="Files-order">
           <asp:Repeater ID="rptDocuments" runat="server" >
       <ItemTemplate>
       <div class="inline-divs">
                <div class="whiteBG" title="<%# DataBinder.Eval(Container.DataItem,"DocName")%>" folderId="<%#folderId %>" docId="<%# DataBinder.Eval(Container.DataItem,"Id")%>">
                    <a onclick="DeleteDocument(this, <%# DataBinder.Eval(Container.DataItem,"Id")%>)" href="javascript:void(0);" class="delete-file"></a>
                    <a class="su-filename su-ext-unk su-ext-<%# DataBinder.Eval(Container.DataItem,"DocExt") %> "
                    href="<%# DataBinder.Eval(Container.DataItem,"DocURL")%>" target="_blank"></a>
                    <a class="File-img-title" target="_blank" href="<%# DataBinder.Eval(Container.DataItem,"DocURL")%>">
                        <%# DataBinder.Eval(Container.DataItem, "DocName")%>
                    </a>
                </div>
            </div>             
       </ItemTemplate>
    </asp:Repeater>


    </div>
</asp:panel>
