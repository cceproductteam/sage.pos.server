var groupId = -1;
var template = null;

function InnerPage_Load() {
    setPageTitle('Group Contacts');
    setSelectedMenuItem('.setup');

    setAddLinkURL(systemConfig.ApplicationURL + systemConfig.pageURLs.AddGroup, 'Add Contacts Group');
    groupId = getQSKey('gid') || -1;

    template = $('.ContentRow').clone();
    $('.ContentRow').remove();

    showLoading($('#demo1'));
    if (groupId > 0)
        getGroupContact(groupId);
    else {
        showStatusMsg('no data found');
        hideLoading();
    }

    $('.ActionCancel-link').attr('href', systemConfig.ApplicationURL_Common + systemConfig.pageURLs.ListGroup);
}

function initControls() {
   
}



function getGroupContact(groupId) {
    $.ajax({
        type: 'post',
        url: systemConfig.ApplicationURL_Common + systemConfig.GroupWebService + "FindGroupContact",
        data: '{groupId:' + groupId + '}',
        dataType: 'json',
        contentType: 'application/json; charset=utf8',
        success: function (data) {

            var result = eval('(' + data.d + ')');
            if (result.statusCode.Code) {
                if (result.result)
                    loadContacts(result.result);
                else {
                    showStatusMsg('no data found');
                }
            }
            else {
                showStatusMsg('system error');
            }

            hideLoading();
        },
        error: function (ex) {
            showStatusMsg('system error');
            hideLoading();
        }
    });
}

function loadContacts(group) {
    $('.GroupName').text(group.GroupName);
    $('.Entity').text(group.EntityName);
    $('.Description').text(group.Description);

    if (group.Contacts) {
        $.each(group.Contacts, function (index, item) {
            var row = template.clone();

            row.find('.view-url').attr('href', systemConfig.ApplicationURL_Common + item.URL);
            row.find('.ContactName').text(item.Name);
            row.find('.Mobile').text(item.MobileNumber);
            row.find('.Email').text(item.Email);
            row.find('.RowNo').text(index + 1);

            $('.search-criteria-info').append(row);
        });
    }
}