var DataTypeContentItemTemplate;
var DataTypeContentListItemsTemplate;
var DataTypeId = -1;
var ContentTypeAC;
var ItemIdentity = 1;
var ContentItems = new Array();
var validated = true;
var edit_mood = false;
var LastUpdatedDate = null;
var IsNotEmptyLovs = false;


function ContenItem() {
    this.itemIdentity = ++ItemIdentity;
    this.DataTypeContentID = -1;
    this.DataTypeContentAR = '';
    this.DataTypeContentEN = '';
    this.CreatedBy = '';
    this.CreatedDate = '';
    this.UpdatedBy = ''
}



function InnerPage_Load() {
    setSelectedMenuItem('.setup');
    checkPermissions(this, systemEntities.LOVs, -1, $('.Action-div'), function (template) { });
    setPageTitle('LOV');
    initControls();
    GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.DataTypeContentItemTemplate, null, 'DataTypeContentItemTemplate');
    $('.add-new-link').click(function () { InsertNewRow(); });

    $('.ActionEdit-link').live('click', function () {
        if (IsNotEmptyLovs) {
            lockEntity(systemEntities.LOVs, -1, function () {
                LoadDefaultOptions(); edit_mood = true;
            }, LastUpdatedDate, function () { ListDataTypeContent(ContentTypeAC.get_Item().value); });

        }
        else { LoadDefaultOptions(); $('.LovsList').show(); }

    });
    $('.ActionSave-link').live('click', function () { SaveEntity(); });
    $('.delete-lnk').live('click', function () { Delete(); });

    $('.ActionCancel-link').live('click', function () {
        ListDataTypeContent(DataTypeId);
        LoadViewDefaultOption();
        edit_mood = false;
        return false;
    });


    $('.LOV-info').find('.cx-control').live('click', function () {
        var TR = this;
        $('.ContentRow').removeClass('selected');
        $(TR).addClass('selected');
    });

    $('.LOV-info').find('.cx-control').live('keyup', function (e) {
        var TR = this;
        if (e.keyCode == 45 || e.which == 45) {
            InsertNewRow();
        }
        if (e.keyCode == 46 || e.which == 46) {
            var TRtoDelete = TR;
            if (eval($(TRtoDelete).parent().parent().attr('isSystem'))) {
                showOkayMsg('Delete Sataus', 'System Contents can not be deleted!');
            }
            else {
                Delete();
            }
        }
    });

}

function RenumberRows() {
    ItemIdentity = 1;
    $.each($('.LOV-info tr'), function (index, item) {
        if ($(this).hasClass('ContentRow')) {
            $(this).find('.ItemIdentity').text(ItemIdentity);
            $(this).removeClass();
            $(this).addClass('ContentRow');
            $(this).addClass('Item' + ItemIdentity);
            ItemIdentity = ItemIdentity + 1;
        }
    });
    //$('.LOV-info tr:last').addClass('last-row');
    $('.LOV-info tr:last').addClass('last-row selected');
    $('.delete-lnk').hide();
}

function initControls() {
    ContentTypeAC = $('select:.ContentType').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.DataTypeWebServiceURL + "Search", required: true });
    ContentTypeAC.set_Args(formatString("{ keyword: '{0}', from:0, to:0,args:'en' }", ''));

    ContentTypeAC.onChange(function () {
        LoadViewDefaultOption();
        if (ContentTypeAC.get_Item() != null) {
            $('.add-new-link').show();
            DataTypeId = ContentTypeAC.get_Item().value;

        }
        else
            DataTypeId = -1;
        ListDataTypeContent(DataTypeId);
    });

}

function ListDataTypeContent(ParentId) {
    $('.LOV-info').find("tr:.ContentRow").remove();
    ItemIdentity = 1;
    // showLoading($('.TabbedPanelsContent'));
    showLoading($('#demo1'));
    var data = formatString('{ParentId:{0}}', ParentId);

    $.ajax({
        type: "POST",
        url: systemConfig.ApplicationURL_Common + systemConfig.DataTypeWebServiceURL + "ListDataTypeContent",
        contentType: "application/json; charset=utf-8",
        data: data,
        dataType: "json",
        success: function (data) {
            var DataTypeContentObj = eval("(" + data.d + ")");
            var template = $(DataTypeContentItemTemplate);
            if (DataTypeContentObj.result != null) {
                IsNotEmptyLovs = true;
                LastUpdatedDate = DataTypeContentObj.result[0].UpdatedDate;
                $.each(DataTypeContentObj.result, function (index, item) {
                    FillContentData(item);
                    ItemIdentity = ItemIdentity + 1;

                });
                ItemIdentity = ItemIdentity - 1;
            }
            else { IsNotEmptyLovs = false; }

            $('.LOV-info tr:last').addClass('last-row');
            $('.LOV-info tr:last').addClass('selected');
            hideLoading();
            edit_mood = false;
        },
        error: function (e) {

        }
    });
}

function FillContentData(Item) {
    if (Item != null) {
        var template = $(DataTypeContentItemTemplate).click(function () {
            $('.selected').removeClass('selected');
            $(this).addClass('selected');

            if (edit_mood && Item.CreatedBy != 1)
                $('.delete-lnk').show();
            else
                $('.delete-lnk').hide();

        });

        template = mapEntityInfoToControls(Item, template);
        template.attr('id', Item.Id);
        template.addClass('Item' + ItemIdentity);
        template.attr('validate', true);
        template.attr('isSystem', Item.IsSystem);
        template.find('.ItemIdentity').text(ItemIdentity);
        template.find('input:.EnglishValue').addClass('Item' + ItemIdentity);
        template.find('input:.ArabicValue').addClass('Item' + ItemIdentity);

        template.find('.CreatedBy').text(Item.CreatedByName);
        template.find('.CreatedDate').text(getDate('dd-mm-yy', formatDate(Item.CreatedDate)));
        template.find('.cx-control').keyup(function () { ValidateItem(template, Item.Id); });
        $('.LOV-info').append(template);

    }

}


function InsertNewRow() {

    if (!$('.LOV-info').find('.last-row').find('input:.EnglishValue').val() == "" || !IsNotEmptyLovs) {
        //&& $('.LOV-info').find('.last-row').find('.ArabicValue').val() == "")
        var template = $(DataTypeContentItemTemplate);
        $('.LOV-info').find('tr').removeClass('last-row');
        template.addClass('last-row');
        template.attr('id', -1);
        template.attr('validate', false);
        template.attr('isSystem', false);
        template.find('.cx-control').keyup(function () { ValidateItem(template, -1); });
        template.find('.ItemIdentity').text($('.LOV-info tr').length - 1);
        $('.ContentRow').removeClass('selected');
        template.addClass('selected');
        template.find('input:.EnglishValue').select();
        template.find('.viewedit-display-none').Show();
        template.find('.viewedit-display-block').hide();
        $('.LOV-info').append(template);
    }
    else {
        $('.ContentRow').removeClass('selected');
        $('.LOV-info').find('.last-row').addClass('selected');
        $('.LOV-info').find('.last-row').find('input:.EnglishValue').select();
    }
}

function ValidateItem(template, Id) {
    template.find('.validation').remove();
    if (template.find('input:.EnglishValue').val() == "") {
        addPopupMessage(template.find('input:.EnglishValue'), 'required');

        //&& template.find('input:.ArabicValue').val() == ""
        // addPopupMessage(template.find('input:.ArabicValue'),'required');
    }
    else {
        template.attr('validate', true);
        template.find('.validation').remove();
        template.find('input:.EnglishValue').removeClass('cx-control-red-border');
    }

}



function getItems() {
    validated = true;
    ContentItems = new Array();
    if ($('.LOV-info').find('.last-row').find('input:.EnglishValue').val() == "") {
        //&& $('.LOV-info').find('.last-row').find('input:.ArabicValue').val() == ""
        $('.last-row').remove();
    }

    $.each($('.LOV-info tr'), function (index, item) {
        if ($(this).attr('id') != "-2") {
            if (eval($(this).attr('validate'))) {
                var newItem = new ContenItem();
                newItem.DataTypeContentID = $(this).attr('Id');
                newItem.DataTypeContentAR = $(this).find('input:.ArabicValue').val();
                newItem.DataTypeContentEN = $(this).find('input:.EnglishValue').val();
                if ($(this).attr('id') == -1)
                    newItem.CreatedBy = UserId;
                else
                    newItem.UpdatedBy = UserId;
                ContentItems.push(newItem);
                validated = true;



            }
            else { validated = false; }
        }
    });


    if (!validated)
        showStatusMsg('Error');
    else validated = true;
}


function SaveEntity() {
    $('.loader').show();
    getItems();
    if (!validated)
        return false;

    if (!saveForm($('.LOVsList'))) {
        return false;
    }
    var lastUpdatedDate = getEntityLockDateFormat(LastUpdatedDate);
    var data = '{DataTypeId:{0},UserId:{1}, ContentItems:"{2}"}';
    data = formatString(data, DataTypeId, UserId, escape(JSON.stringify(ContentItems)));

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: systemConfig.ApplicationURL + systemConfig.DataTypeWebServiceURL + "AddContent",
        headers: getEntitylockAjaxHeaders(lastUpdatedDate),
        data: data,
        datatype: "json",
        success: function (data) {
            $('.loader').remove();
            DataTypeId = eval('(' + data.d + ')').result;
            showStatusMsg('Saved successfully');
            ListDataTypeContent(DataTypeId);
            $('.viewedit-display-none').hide();
            $('.viewedit-display-block').show();
        },
        error: function (ex) {
            handleLockEntityError(ex, null, function () {
                ListDataTypeContent(ContentTypeAC.get_Item().value);
                $('.ActionSave-link').hide();
                $('.add-new-link').hide();
                $('.ActionCancel-link').hide();
                $('.ActionDelete-link').hide();
                $('.delete-lnk').hide();
                $('.ActionEdit-link').show();
                $('.viewedit-display-block').show();
            });
        }
    });

}


function LoadDefaultOptions() {
    $('.viewedit-display-none').show();
    $('.viewedit-display-block').hide();
    $('.delete-lnk').hide();
}
function LoadViewDefaultOption() {
    $('.Action-div').show();
    $('.viewedit-display-none').hide();
    $('.viewedit-display-block').show();
}

function Delete() {
    showConfirmMsg('Delete', 'Are you Sure you want to delete this Content?', function () {
        $('.selected').remove();
        RenumberRows();
    });
}