
<%@ Page Title="" Language="C#" MasterPageFile="~/Common/MasterPage/Site.master"
    AutoEventWireup="true" CodeBehind="LOVs.aspx.cs" Inherits="SagePOS.Common.Web.NewCommon.Common.LOVs.LOVs" %>

<asp:Content runat="server" ContentPlaceHolderID="PageHeader" ID="PageHeader">
    <script src="JS/LOV.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="PageBody" ID="PageBody">
    <div id='content' class="View-info">
        <div class="TabbedPanelsContent">
            <div class="TabbedPanelsContent-info">
                <div class="Add-new-form" style="min-height: 450px;">
                   <div class="LOVsList">
                        <div class="Details-div">
                            <span class="details-div-title">LOVs</span>
                            <div class="Action-div display-none">
                                <ul>
                                 <li><span class="ActionSave-link save-tab-lnk viewedit-display-none"></span><a class="ActionSave-link save-tab-lnk viewedit-display-none"> Save </a></li>
                                 <li><span class="ActionCancel-link cancel-tab-lnk viewedit-display-none"></span><a class="ActionCancel-link cancel-tab-lnk viewedit-display-none">Cancel</a> </li>
                                 <li class="delete-LOV viewedit-display-none"><span class="ActionDelete-link delete-lnk"></span><a class="delete-lnk"> Delete </a></li>
                                 <li class="add-LOV viewedit-display-none"><span class="add-new-img add-new-link"></span><a class="add-new-link">Add new LOV</a></li>
                                 <li class="edit-LOV viewedit-display-block"><span class="ActionEdit-link edit-tab-lnk viewedit-display-block"></span>
                                 <a class="edit-tab-lnk ActionEdit-link viewedit-display-block">Edit</a></li>
                                </ul>
                            </div>
                        </div>
                        <table border="0" cellpadding="0" cellspacing="0" class="Data-Table LOV-List" style="width: 50%;">
                            <tr>
                                <th>
                                    <span class="label-title">Content Type <span class="smblreqyuired viewedit-display-block">
                                        *</span></span>
                                </th>
                                <td>
                                    <span class="label-data ContentType viewedit-display-none"></span>
                                    <div class="">
                                        <select class="ContentType lst">
                                        </select>
                                    </div>
                                </td>
                                <th>
                                </th>
                                <td>
                                </td>
                            </tr>
                        </table>
                        <table width="100%" border="0" cellpadding="0" cellspacing="1" class="LOV-info">
                            <tr id="-2">
                                <th class="th-data-table th-item-num">
                                    #
                                </th>
                                <th class="th-data-table">
                                    Value
                                </th>
                                <th class="th-data-table display-none">
                                    ValueAR
                                </th>
                                <th class="th-data-table">
                                    Created By
                                </th>
                                <th class="th-data-table">
                                    Created Date
                                </th>
                            </tr>
                            <tr id="-2">
                                <td class="td-data-border" colspan="6">
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="five-px">
                </div>
            </div>
        </div>
    </div>
</asp:Content>
