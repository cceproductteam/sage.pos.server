var resultCount = 18, pageNumber = 1;
var selectedEntityId = -1, selectedEntityItemList, entityItemListTemplate, entityItemViewTemplate;
var loadEntityServiceURL, findAllEntityServiceURL, deleteEntityServiceURL;
var selectEntityCallBack = null;
var initControlsStatus = false;
var showMore = false;
var filterEntityId = null, entityKey = 'key';
var lockSave = false;
var UnAvailablePermissionsList = null;
var preventCtrlFind = false;
var currentEntity = -1;
var currentEntityLastUpdatedDate = null;
var deletedEntityCallBack, updatedEntityCallBack;
var hideLoadingAfterLock = true;
var hideLoadingAfterUnLock = true;
var searcholdWebserviceURL = '';
var totalEntityCount = 0, totalCurrenrEntityCount = 0;
var IsIntegratedEntity = false;
var IntegratedDeleteMsg = "";
var unSynchronizedMsg = '';
var SynchronizedMsg = '';
var enablePermissionAdd = true, enablePermissionDelete = true; //added by Haneen to disable insert/delete on grids
var lastRowResultGrid = false; //added by Haneen to handle quick search in grids
var max_min = false;
var listEntityCallBack = null, loadMoreAction = false;
var RowStyle = 'td-r1-data-table';
var afterEditCallBack;
var pendingAjaxRequest = false;
var fromBatchList = false;
var editingRecord = false;
var addingRecord = false;
var AddMode = false;
var RoundingDigits = 3;
var havePer = 1;


function getEntityData(selectedEntityId) {
    editingRecord = false;
    addingRecord = false;
    loadEntityData(loadEntityServiceURL, entityItemViewTemplate, selectedEntityId, function (dataObject) {
        currentEntityLastUpdatedDate = dataObject.UpdatedDate || dataObject.Updated_Date;
        $('.TabbedPanelsTab:first').click();
        if (selectEntityCallBack) selectEntityCallBack(dataObject);
    });
}


function loadDefaultEntityOptions() {
    pendingAjaxRequest = false;
    resultCount = 30;
    $('.show-more-results-div').hide();
    $('.result-block').live("click", function () {
        $('.result-block').removeClass('SelectedTr');
        $('.add-new-item').hide();
        $('.add-new-item').html('');
        //Added by ruba 14-10-2014
        $(".black_overlay").hide();
        $(this).addClass('SelectedTr');
        var timmer = 0;
        try {
            var timmer = ($('.Filter-search').position().top == 0) ? 400 : 0;
            $('.Filter-search').animate({
                top: filterSearchTop
            }, 500);
        }
        catch (err) {

        }

        $('.TabbedPanelsTab').removeAttr('pChecked');
        selectedEntityId = $(this).attr('id');
        selectedEntityItemList = $(this);
        window.setTimeout(function () {
            getEntityData(selectedEntityId);
        }, timmer);
    });

    $('.MoreSearchOptions').click(function () {
        if ($('.MoreSearchOptions').text() == getResource("HideOptions")) {
            QuickSearch = false;
            $('.Quick-Search-Div').hide();
            $(this).html("<a class='more-options'>" + getResource('MoreOptions') + "</a>");
            $('.GridTableDiv-body').height($('.GridTableDiv-body').height() + 154);
        }
        else {

            $('.Quick-Search-Div').show();
            $(this).html("<a class='more-options'>" + getResource('HideOptions') + "</a>");
            $('.GridTableDiv-body').height($('.GridTableDiv-body').height() - 154);
        }

    });

    $('#demo2').alternateScroll('remove');

    //  if (Lang != "ar")
    $("#demo2").resizable({
        handles: 'e',
        resize: function (event, ui) { }

    });

    $('.GridTableDiv-header table').find('th').each(function ($index) {
        $(".GridTableDiv-body table tr td:eq(" + $index + ")").width($(this).width());
    });



    $('#demo2').on("resize", function (event, ui) {
        var demo2width = $('#demo2').width();
        var demo2parentWidth = $('#demo2').offsetParent().width();
        var demo2percent = 100 * demo2width / demo2parentWidth;
        var widthinpercentage = 98 - demo2percent;
        $('.Result-info-container').css("width", widthinpercentage + "%");
        $('.GridTableDiv-header table').find('th').each(function ($index) {
            $(".GridTableDiv-body table tr td:eq(" + $index + ")").width($(this).width());
        });
        if ($('.lbl-invoice-number').length > 0) { $('.lbl-invoice-number').css('width', $('.th-entry-number').width()); }
        if ($('.lbl-invoice-number2').length > 0) { $('.lbl-invoice-number2').css('width', $('.th-invoice-number').width()); }
        if ($('.result-container').width() <= 530) { $('.SD-ul').children('li').addClass('FullWidth100'); }
        else { $('.SD-ul').children('li').removeClass('FullWidth100'); }

    });



    $('.ui-resizable-e').dblclick(function () {
        if (max_min) {
            $('#demo2').width(parseInt($('#demo2').css('max-width')) + '%');
            max_min = false;
        }
        else {
            $('#demo2').width(parseInt($('#demo2').css('min-width')) + '%');
            max_min = true;
        }
        window.setTimeout(function () {
            var demo2width = $('#demo2').width();
            var demo2parentWidth = $('#demo2').offsetParent().width();
            var demo2percent = 100 * demo2width / demo2parentWidth;
            var widthinpercentage = 98 - demo2percent;
            $('.Result-info-container').css("width", widthinpercentage + "%");
            $('.GridTableDiv-header table').find('th').each(function ($index) {
                $(".GridTableDiv-body table tr td:eq(" + $index + ")").width($(this).width());
            });
        }, 100);

    });

    loadKeyDownActions();
    $('.ActionEdit-link').live("click", function () {
        editingRecord = false;

        showLoading($('.TabbedPanels'));
        $('.add-new-item').hide();
        $('.add-new-item').html('');
        lockEntity(currentEntity, selectedEntityId, function () {
            editingRecord = true;
            $('.viewedit-display-block').hide();
            $('.viewedit-display-none').show();
            $('.TabbedPanelsContent').find('.cx-control:visible').first().focus();
            if (afterEditCallBack) afterEditCallBack();
        }, currentEntityLastUpdatedDate, function () {
            loadEntityData(loadEntityServiceURL, entityItemViewTemplate, selectedEntityId, function (dataObject) {
                currentEntityLastUpdatedDate = dataObject.UpdatedDate || dataObject.Updated_Date;
                if (selectEntityCallBack) selectEntityCallBack(dataObject); $('.TabbedPanelsTab:first').click();
            });
        }, deletedEntityCallBack);
    });


    $('.ActionDelete-link').live('click', function () {
        if (IsIntegratedEntity) {
            showStatusMsg(IntegratedDeleteMsg);
            return false;
        }
        else {
            showConfirmMsg(DeleteTitleMessage, DeleteMessage, function () {
                showLoading($('.TabbedPanels'));
                lockForDelete(currentEntity, selectedEntityId, function () {
                    deleteEntity(selectedEntityId);
                }, deletedEntityCallBack);
            });
            return false;
        }
    });

    $('.ActionSave-link').live('click', function () {
       // editingRecord = false;
        SaveEntity();
        $('.TabbedPanelsTab').removeAttr('pChecked');
        return false;
    });

    $('.Action-div').live('click', function (e) {

    });

    //Edited By Ruba Al-Sa'di to prevent executing this event on clicking on Cancel Button in Add Form
    //$('.ActionCancel-link').live('click', function () {
    $('.Result-info-container').find('.ActionCancel-link').live('click', function () {
        //Cancel button of default tab (Summary tab)
        
        //Edited by Ruba Al-Sa'di 4/11/2014
        var EditingOrAddingInProcess = CheckIfAddingOrEditingIsInProcess();

        if (EditingOrAddingInProcess && !AddMode) {
            showConfirmMsg(getResource("Warning"), getResource('AreYouSureYouWantToCancel'), function () {
                loadEntityData(loadEntityServiceURL, entityItemViewTemplate, selectedEntityId, selectEntityCallBack);
                UnlockEntity(currentEntity, selectedEntityId, function () {
                    editingRecord = false;
                    loadEntityData(loadEntityServiceURL, entityItemViewTemplate, selectedEntityId, selectEntityCallBack);
                });
                return false;
            }, function () {
                return false;
            });
        }
        else {
            if (!AddMode) {
                loadEntityData(loadEntityServiceURL, entityItemViewTemplate, selectedEntityId, selectEntityCallBack);
                UnlockEntity(currentEntity, selectedEntityId, function () {
                    loadEntityData(loadEntityServiceURL, entityItemViewTemplate, selectedEntityId, selectEntityCallBack);
                });
            }
            return false;
        }

        //        loadEntityData(loadEntityServiceURL, entityItemViewTemplate, selectedEntityId, selectEntityCallBack);
        //        UnlockEntity(currentEntity, selectedEntityId, function () {
        //            editingRecord = false;
        //            loadEntityData(loadEntityServiceURL, entityItemViewTemplate, selectedEntityId, selectEntityCallBack);
        //        });
    });

    $('.GridTableDiv-body').scroll(function () {
        loadMoreAction = true;
        if (($('.GridTableDiv-body').outerHeight() == Math.round(($('.GridTableDiv-body').get(0).scrollHeight - $('.GridTableDiv-body').scrollTop()) + .1, 2)) && !lastRowResultGrid && $('.myCustomLoader').length != 1) {
            pageNumber++;
            showMore = true;
            if (!QuickSearch)
                ListEntityItems($('.searchTextbox').val().trim());
            else
                ListEntityItems('');
        }
    });
    ListEntityItems('');
    SetSearchTextBoxEvent();
}

function loadKeyDownActions() {
    document.onkeydown = function (e) {
        if (e.ctrlKey && e.keyCode === 83) {
            e.preventDefault();

            //Added By Ruba Al-Sa'di 24/1/2015
            //To Disable Ctrl+S btn when another pop-up opened above add form
            if ($('.black_overlay').is(':visible') && $('.black_overlay').css('z-index') <= $('.add-new-item').css('z-index')) {
                if ($('.add-new-item').find('span:.ActionSave-link').length > 0) {
                    $('.add-new-item').find('span:.ActionSave-link').click();
                    $('.searchTextbox').focus();
                }
                else if ($('span:.save-optional-field').is(":visible"))
                { $('span:.save-optional-field').click(); }
                else if ($('span:.ActionSave-link').is(":visible")) {
                    $('span:.ActionSave-link').click();
                    $('.searchTextbox').focus();
                }
            }
            return false;
        }
        if (e.keyCode == 27 || e.which == 27) {
            //Added By Ruba Al-Sa'di 5/1/2015
            //To Disable ESC btn when another pop-up opened above add form
            if ($('.black_overlay').is(':visible') && $('.black_overlay').css('z-index') <= $('.add-new-item').css('z-index')) {
                //Updated By Ruba Al-Sa'di 4/11/2014
                $('.add-new-item').find('span:.ActionCancel-link').click();
                //            $('.add-new-item').html('');
                //            $('.add-new-item').hide();
                return false;
            }
        }
        if (e.keyCode == 13 || e.which == 13) {
            $(':focus').click();
            return false;
        }
        addNew(e);

    };

}
function LoadAddEntityDefaultOptions(listPage) {
    initControls();
    $('.ActionSave-link').live('click', function () {
        // $('.ActionSave-link').hide();
        SaveEntity($('#content'));

        window.setTimeout(function () { $('.ActionSave-link').show(); }, 2000);
        return false;
    });
    $('.ActionCancel-link').live('click', function () {
        window.location.href = listPage;
    });
}


/*Set SearchTextBox*/
var noResultKeyword;
var previousKeyWord;

function SetSearchTextBoxEvent() {
    $('.searchTextbox').attr('placeholder', getResource('Search...'));
    $('.search-box').show();
    $('.searchTextbox').keydown(function (e) {
        if (e.keyCode == 13 && !e.ctrlKey) {
            var currentText = $('.searchTextbox').val().trim().toLowerCase();
            if ((currentText == previousKeyWord) || (noResultKeyword && currentText && currentText.indexOf(noResultKeyword) >= 0)) {
                return false;
            }
            var keyword;
            pageNumber = 1;
            //changes related to quicksearch //19/3 mieassar
            $('.result-name').hide();
            $('.TabbedPanelsContentGroup').hide();
            $('.QuickSearchDiv').hide();
            lastRowResultGrid = false;
            QuickSearch = false;
            findAllEntityServiceURL = (searcholdWebserviceURL == "") ? findAllEntityServiceURL : searcholdWebserviceURL;
            //changes related to quicksearch
            //  
            // showLoadingWithoutBG1($('.GridTableDiv-body'));
            // $('.loading-list').show();
            $('.result-list').empty();
            $('.show-more-results-div').hide();
            $('.total-result-div').find('span').html(formatString("{0} ? {1} ?", getResource("Total"), getResource("of")));
            try { window.clearTimeout(timeoutID); } catch (e) { }
            timeoutID = window.setTimeout(function () {
                keyword = $('.searchTextbox').val();
                previousKeyWord = keyword.trim().toLowerCase();
                loadMoreAction = false;
                ListEntityItems(keyword.trim());
            }, 500); //delay
        }
        else {
            preventCtrlFind = false;
        }
    });
}

function ListEntityItems(keyword) {
    if (filterEntityId == null) {
        filterEntityId = getQSKey(entityKey) ? getQSKey(entityKey) : -1;
    }
    else
        filterEntityId = -1;

    showLoadingWithoutBG1($('.GridTableDiv-body'));
    keyword = (keyword) ? keyword : '';

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        data: setListItemServiceData(keyword, pageNumber, resultCount),
        url: findAllEntityServiceURL,
        dataType: "json",
        success: function (data) {
            //   window.setTimeout(function () {
            var LastRows;
            var Total;
            //  showLoadingWithoutBG('.GridTableDiv-body');
            // $('.loading-list').hide();
            var EntityObj = eval("(" + data.d + ")");
            if (EntityObj.result != null) {
                $('.empty-list-item').remove();
                $('.result-name').show();
                $('.TabbedPanelsContentGroup').show();
                if (QuickSearch) {
                    LastRows = (pageNumber * resultCount >= EntityObj.result[0].TotalRecords);
                    Total = EntityObj.result[0].TotalRecords;
                    // QuickSearch = false;
                    // findAllEntityServiceURL = searcholdWebserviceURL;
                    if (!showMore)
                        $('.result-list').html('');
                }
                else {
                    LastRows = EntityObj.result[0].LastRows;
                    Total = EntityObj.result[0].TotalRecords;
                    totalEntityCount = Total;
                }

                if (!LastRows) {
                    $('.show-more-results-div').show();
                    $('.total-result-div').find('span').html(getResource("Total") + parseInt(pageNumber * resultCount) + getResource("of") + Total);
                    totalCurrenrEntityCount = parseInt(pageNumber * resultCount);
                    lastRowResultGrid = false;
                }
                else {
                    $('.show-more-results-div').hide();
                    $('.total-result-div').find('span').html(getResource("Total") + Total + getResource("of") + Total);
                    totalCurrenrEntityCount = Total;
                    lastRowResultGrid = true;
                }
                var rowNumber = Number($('.result-list').find('tr').length + 1);
                $.each(EntityObj.result, function (index, item) {
                    var template = entityItemListTemplate;
                    template = setListItemTemplateValue($(template).clone(), item);
                    $(template).attr('id', item.Id);
                    $(template).find('span:.row-number').textFormat(rowNumber++);
                    if (template) {
                        template.find('td').addClass(RowStyle);
                        RowStyle = (RowStyle == 'td-r1-data-table' ? 'td-r2-data-table' : 'td-r1-data-table');
                    }
                    $('.result-list').append(template);
                });
                if (!loadMoreAction) {
                    $('.result-list').find('.result-block:first').click();
                    tableIsFocus = true;
                    loadArrowsActions($('.TBodyGridTable'));
                }
                loadMoreAction = false;
                noResultKeyword = '';
            }
            else {
                //
                noResultKeyword = keyword.toLowerCase();
                $('.show-more-results-div').hide();
                $('.total-result-div').find('span').html("Total 0 of 0");
                $('.result-list').html('');
                if ($('.empty-list-item').length <= 0)
                    $('.result-list').parent().parent().append('<div class="empty-list-item"><span class="addlabel">' + getResource("NoDataFound") + '</span></label></div>');
                loadMoreAction = false;

            }

            $('.myCustomLoader').remove();
            if (listEntityCallBack != undefined && listEntityCallBack != null) {
                listEntityCallBack();
            }
            $("#demo2").resize();
            if (Lang == "ar")
                $("#demo2").unbind();
            if ($('.result-list').children().length == 0)
                $('.TabbedPanelsContent').empty();

        },
        error: function (e) {
            $('.loading-list').hide();
        }
    });
}


function setAddLinkEvent(template) {
    template.find('input').val('');
    template.find('.itemheader').click();
    template.find('.viewedit-display-none').show();
    template.find('.viewedit-display-block').hide();
    template.find('.edit-tab-lnk').remove();
    template.find('.save-tab-lnk').show();
    template.find('.cancel-tab-lnk').show();
    template.find('.delete-tab-lnk').hide();
    template.find('.validation').remove();
    //template.show();
    if (template.is(':visible')) {
        template.hide();
    }
    else {
        template.show();
    }
}

function deleteEntity(id) {
    preventSendHeader = false;
    $.ajax({
        type: "POST",
        url: deleteEntityServiceURL,
        contentType: "application/json; charset=utf-8",
        data: formatString('{ id: "{0}" }', id),
        dataType: "json",
        success: function (data) {
            preventSendHeader = true;
            var returnedResult = eval('(' + data.d + ')');
            if (returnedResult) {
                var code = returnedResult.statusCode.Code;
                if (code == 504) showStatusMsg(getResource("vldDeleteActiveFailed"));
                else {
                    showStatusMsg(getResource("deletesuccessfully"));
                    removeDeletedEntity();
                }
            }
            else {
                showStatusMsg(getResource("deletesuccessfully"));
                removeDeletedEntity();
            }

        },
        error: function (ex) {


        }
    });

}

function removeDeletedEntity() {
    var PreselectedItemList = selectedEntityItemList;
    if ($('.Result-list').find('.SelectedTr').next().length > 0)
        $('.Result-list').find('.SelectedTr').next().click();
    else {
        $('.TabbedPanelsContentGroup').empty();
        $('.Result-list').find('.SelectedTr').prev().click();
    }
    totalEntityCount--;
    totalCurrenrEntityCount--;
    $('.total-result-div').find('span').html(getResource("Total") + totalCurrenrEntityCount + getResource("of") + totalEntityCount);
    $(PreselectedItemList).remove();
}

function escapeString(input) {
    if ($(input).length > 0)
        return encodeURIComponent($(input).val().trim());
    else
        return encodeURIComponent(input.trim());
}

function unescapeString(string) {
    return unescape(string);
}

function setSelectedMenuItem(rightMenuItem, topMenuItem) {
    if (rightMenuItem) {
        $(rightMenuItem).parents('li').find('.MenuNav-a').addClass('SelectedNav');
    }

    if (topMenuItem) {
        $(topMenuItem).parents('.TabLinks-square').addClass('TabLinks-square-hover');
    }
}




function loadEntityData(serviceURL, itemTemplate, id, callback) {
    AddMode = false;
    addingRecord = false;
    editingRecord = false;
    $('.add-entity-button').hide();
    try {
        showLoading($('.TabbedPanels'));
        $('.result-block').children(".openDiv").hide();
        $('.result-block').children(".gray-square").hide();
        $('.result-block').removeClass("result-blockVisited");
        $('#' + id).children("div").toggleClass("result-blockVisited");
        $('#' + id).children("div").children(".openDiv").show();
        $('#' + id).children("div").children(".gray-square").show();

        $('.TabbedPanelsContentGroup').css('opacity', '0.4');
    } catch (e) {

    }
    pendingAjaxRequest = true;
    $.ajax({
        type: "POST",
        url: serviceURL,
        contentType: "application/json; charset=utf-8",
        data: '{ id: ' + id + ' }',
        dataType: "json",
        success: function (data) {
            pendingAjaxRequest = false;
            $('.add-entity-button').show();
            $('.add-new-item').hide();
            $('.add-new-item').html('');

            $('.TabbedPanelsContentGroup').css('opacity', '1');
            if (data.d != null) {

                dataObject = eval("(" + data.d + ")").result;
                $('.TabbedPanelsContentGroup').html($(itemTemplate).clone());

                if (!initControlsStatus)
                    initControls();

                $('#demo5').alternateScroll();
                $('.viewedit-display-none').hide();
                // mapEntityInfoToControls(dataObject, $('.TabbedPanelsContentGroup'));


                $('.ActionFlag-link').attr('id', id);
                var entity = $('.ActionFlag-link').attr('entity');
                var cookieName = "flag_" + entity + "_" + id;

                if (readCookie(cookieName) == 1) {
                    $('.ActionFlag-link').addClass('selectedflag');
                }

                readCookie(cookieName)

                if (dataObject != null) {
                    currentEntityLastUpdatedDate = dataObject.UpdatedDate || dataObject.Updated_Date;
                    for (var prop in dataObject) {
                        var className = '.' + prop;
                        var lblValue = dataObject[prop];
                        var lblValueId = prop + 'Id';

                        if ($(className).hasClass('lst')) {
                            var valudId = dataObject[lblValueId];
                            if (valudId > 0) {
                                var item = { label: lblValue, value: valudId };
                                eval(prop + 'AC.set_Item(' + JSON.stringify(item) + ')');
                            }
                        }
                        else if ($(className).hasClass('txt') || $(className).hasClass('integer')) {
                            if ($(className).hasClass('integerMask') || $(className).hasClass('numeric')) {
                                $('input:.' + prop).autoNumericSet(lblValue);
                                $('span:.' + prop).val(lblValue);
                            }
                            else {
                                $('.' + prop).val(lblValue);
                            }
                        }
                        else if ($(className).hasClass('date-time')) {
                            if (lblValue)
                                lblValue = getDate('dd-mm-yy hh:mm tt', formatDate(lblValue));
                            $('.' + prop).val(lblValue);
                        }
                        else if ($(className).hasClass('date')) {
                            if (lblValue)
                                lblValue = getDate('dd-mm-yy', formatDate(lblValue));
                            $('.' + prop).val(lblValue);
                        }
                        else if ($(className).hasClass('chk')) {
                            if (eval(dataObject[prop + "Value"])) {
                                $('.' + prop).attr('checked', 'checked');
                            }
                        }
                        else if ($(className).hasClass('radio')) {
                            lblValue = dataObject[prop];
                            if (eval(dataObject[prop + "Value"])) {
                                $('.' + prop).attr('checked', true);
                            }
                        }
                        //                        else if ($(className).hasClass('radio')) {
                        //                            $(className).attr('name', 'entity-group' + entity.Id);
                        //                            template.find('.' + lblValue).attr({ checked: true });
                        //                        }

                        // Fill In Labels
                        $('.' + prop).textFormat(lblValue);

                    }
                }
                else {
                    if (LoadingDataError != "")
                        showOkayMsg(LoadingDataErrorTitle, LoadingDataError);
                    return false;
                }
                if (callback != undefined && callback != null) {
                    callback(dataObject);
                }
                if (setListItemTemplateValue != undefined && setListItemTemplateValue != null)
                    setListItemTemplateValue($('#' + id), dataObject);
                //$('.TabbedPanelsContentFieldset').alternateScroll();
            }
            else {
                showOkayMsg(LoadingDataErrorTitle, LoadingDataError);
            }
            hideLoading();
        },
        error: function (e) {
            pendingAjaxRequest = false;
            $('.add-entity-button').show();
            hideLoading();
        }
    });
}

function loadViewEntityData(serviceURL, itemTemplate, id, callback) {
    $.ajax({
        type: "POST",
        url: serviceURL,
        contentType: "application/json; charset=utf-8",
        data: '{ id: ' + id + ' }',
        dataType: "json",
        success: function (data) {
            if (data.d != null) {
                dataObject = eval("(" + data.d + ")").result;
                if (dataObject != null) {
                    for (var prop in dataObject) {
                        var className = '.' + prop;
                        var lblValue = dataObject[prop];
                        var lblValueId = prop + 'Id';

                        if (element.hasClass('date-time')) {
                            if (lblValue)
                                lblValue = getDate('dd-mm-yy hh:mm tt', formatDate(lblValue));
                            element.val(lblValue);
                        }
                        if (element.hasClass('date-time')) {
                            if (lblValue)
                                lblValue = getDate('dd-mm-yy hh:mm tt', formatDate(lblValue));
                            element.val(lblValue);
                        }
                        else if ($(className).hasClass('date')) {
                            if (lblValue)
                                lblValue = getDate('dd-mm-yy', formatDate(lblValue));
                            $('.' + prop).val(lblValue);
                        }
                        itemTemplate.find('.' + prop).textFormat(lblValue);
                    }
                }
            }
            if (callback) {
                callback(dataObject);
            }
        },
        error: function (e) {
        }
    });
}



function saveForm(form) {
debugger
    var validForm = true;
    form.find('.validation').remove();
    form.find('.cx-control-red-border').removeClass('cx-control-red-border');

    form.find('.cx-control')
    //form.find('.cx-control:.required, .cx-control:.numeric, , .cx-control:.integer, , .cx-control:.email')
    .focus(function () {
        form.find('.validation').remove();
        form.find('.cx-control-red-border').removeClass('cx-control-red-border');
    });


    if (arguments[1] != undefined) {
    debugger
        $.each(arguments, function (index, IItem) {
            if (index > 0) {
                if (arguments[1].get_Item() == null) {
                    addPopupMessage($(arguments[1]), getResource('required'))
                    validForm = false;
                }
            }
        });
    }
    form.find('.cx-control').each(function () {
    
        validForm = validateForm($(this)) && validForm;
    });
   
    return validForm;
}

function validateForm(element) {
    //element.hasClass('ui-combobox-input')

    if (element.hasClass('required') && (element.val().trim() == '') && element.hasClass('ui-combobox-input')) {
        addPopupMessage(element, getResource('required'), true);
        return false;
    }
    if (element.hasClass('required') && (element.val().trim() == '')) {
        addPopupMessage(element, getResource('required'));
        return false;
    }

    if ((element.val().trim() != '' && element.hasClass('numeric') && !Number(getNumericValue(element))) && (parseInt(element.val().trim()) != 0)) {
        addPopupMessage(element, getResource('numeric'))
        return false;
    }

    if (element.val().trim() != '' && element.hasClass('integer') && isNaN(element.val().trim()) && !Number(getNumericValue(element))) {
        addPopupMessage(element, getResource('integer'))
        return false;
    }

    if (element.attr('length')) {
        var maxLength = element.attr('length');
        var inputValue = element.val().trim();
        if (inputValue.length < maxLength) {
            addPopupMessage(element, getResource('lengthMustBe') + maxLength);
            return false;
        }
    }

    //add new by mieassar
    //    if (element.attr('maxlength')) {
    //        var maxLength = element.attr('maxlength');
    //        var inputValue = element.val().trim();
    //        if (inputValue.length > maxLength) {
    //            addPopupMessage(element, 'maximum length must be ' + maxLength);
    //            return false;
    //        }
    //    }

    //    //add new by mieassar
    //    if (element.attr('minlength')) {
    //        var minLength = element.attr('minlength');
    //        var inputValue = element.val().trim();
    //        if (inputValue.length < minLength) {
    //            addPopupMessage(element, 'minimum length must be ' + minLength);
    //            return false;
    //        }
    //    }

    if (element.val().trim() != '' && element.hasClass('email')) {
        //var emailReg = /[\w-]+(?:\.[\w-]+)*@(?:[\w-]+\.)+[a-zA-Z]{2,7}/i;
        //var emailReg = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
        var emailReg = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var string = element.val().trim();
        if (!emailReg.test(string)) {
            addPopupMessage(element, getResource('invalidEmail'));
            return false;
        }
    }

    return true;
}


function addPopupMessage(element, message, isList) {
    if (isList) {
        var selectWidth = Number(element.css('width').replace('px', '')); //- 100;
        element.addClass('cx-control-red-border');
        element = element.parent().parent();
        var validation = $('<div>').attr({ 'class': 'validation' })//.css('width', (selectWidth / 2) - 20 + 'px')
    .append($('<span>').html(message), $('<span>'));
        if (validation.width() >= (selectWidth / 2))
            validation.css('width', (selectWidth / 3) - 5);
        validation.insertAfter(element);

    }
    else {
        var validation = $('<div>').attr({ 'class': 'validation' })//.css('width', ((Number(element.css('width').replace('px', '')) / 2) - 10) + 'px')
    .append($('<span>').html(message), $('<span>'));

        validation.insertAfter(element);
        if ($(validation).width() >= ($(element).width() / 2))
            $(validation).css('width', ($(element).width() / 3) + 10);
        element.addClass('cx-control-red-border')
    }
      //Added By Haneen in order to scroll down if there is hidden validation in the add div
    if (addingRecord && $('.validation').length > 0) {
        if ($('.FormScroll-Div').offset() != null) //Added By Ruba Al-Sa'di 17-12-2014
            $('.FormScroll-Div').animate({
                scrollTop: ($('.validation').first().offset().top - $('.FormScroll-Div').offset().top) - 10
            });
    }
}



function mapEntityInfoToControls(entity, template) {

    for (var prop in entity) {
        var className = '.' + prop;
        var lblValue = entity[prop];
        var lblValueId = prop + 'Id';
        var element = $(template).find(className);

        if (element.hasClass('lst')) {
            var valudId = entity[lblValueId];
            var ACItem = { label: lblValue, value: valudId };
            if (valudId > 0 && arguments[2]) {
                var controlFound = false;
                $.each(arguments[2], function (index, item) {
                    if (item.controlName == prop + 'AC') {
                        controlFound = true;
                        item.control.set_Item(ACItem);
                        return false;
                    }
                });
                if (!controlFound)
                    eval(prop + 'AC.set_Item(' + JSON.stringify(ACItem) + ')');
            }
        }
        else if (element.hasClass('txt')) {
            if (element.hasClass('numeric')) {
                if (!isNaN($(template).find('input:' + className).attr('anLength'))) {
                    element.autoNumeric({ vMax: $(template).find('input:' + className).attr('anLength'), vMin: '0.0' });
                }
                else {
                    element.autoNumeric();
                }

                element.autoNumericSet(lblValue);
            }
            else if (element.hasClass('integer')) {
                element.val(lblValue);
                //element.autoNumeric({aPad : false});//new
                // element.autoNumericSet(lblValue);//new
            }
            else element.val(lblValue);
        }
        else if (element.hasClass('date-time')) {
            if (lblValue) {
                lblValue = getDate('dd-mm-yy hh:mm tt', formatDate(lblValue));
                element.val(lblValue);
            }
        }
        else if (element.hasClass('date')) {

            if (lblValue) {
                lblValue = getDate('dd-mm-yy', formatDate(lblValue));
                element.val(lblValue);
            }
        }
        else if ($(className).hasClass('radio')) {
            lblValue = dataObject[prop];
            if (eval(dataObject[prop + "Value"])) {
                $('.' + prop).attr('checked', true);
            }
        }
        //        else if (element.hasClass('radio')) {
        //            template.find(className).attr('name', 'entity-group' + entity.Id);
        //            template.find('.' + lblValue).attr({ checked: true });
        //        }
        else if (element.hasClass('chk')) {
            lblValue = entity[prop + "Value"];
            if (eval(lblValue)) {//.toLowerCase() change for ERP
                element.attr('checked', 'checked');
            }
            lblValue = entity[prop];
        }
        if ($(element).hasClass('money') && lblValue == null) //added by haneen
            lblValue = 0; //added by haneen
        // Fill In Labels
        if (lblValue == 0)
            $(element).text(lblValue);
        else
            $(element).textFormat(lblValue);
    }
    return template;
}

function SetDefultCountry(CountryAC) {
    CountryAC.set_Item(
    { label: systemConfig.WebKeys.DefualtCountryName, value: systemConfig.WebKeys.DefualtCountryId });
}

function Truncate(text, length) {
    if (text) {
        var truncatedText = '';
        length = !length ? 50 : length;
        truncatedText = text.length > length ? text.substring(0, length) + " ..." : text;
        return truncatedText;
    }
    else
        return "";
}

//Updated by Ruba Al-Sa'di to enable initializing multiSelect AC - 10-12-2014
function fillACList(list, serviceURL, required, staticParams, args, multiSelect) {
    var ac = $(list).AutoComplete({ serviceURL: serviceURL, required: required, staticParams: staticParams, multiSelect: multiSelect == undefined ? false : multiSelect });
    if (args) {
        ac.set_Args(args);
    }
    return ac;
}


function getDotNetDateFormat(date) {
    return new Date(date.replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
}

function formatDate(date) {
    if (date) //new
        return new Date(Number(date.match(/\d+/)[0]));
    else return ""; //new
}

function setNowDate(input) {
    $(input).val(getDate('dd-mm-yy', new Date()));
}

function getNumericValue(input, isLabel) {
    if (isLabel)
        return $(input).text().replace(/,/g, '');
    else
        return $(input).val().replace(/,/g, '');
}

function setMask(input, mask) {
    if (mask == 'integer') {
        preventInputChars($(input));
    }
    else
        $(input).autoNumeric();
}

(function ($) {
    jQuery.fn.textFormat = function (val) {
       if(!val || val=="") val=" ";
        if ($(this).hasClass('number') && val != null) {
            $(this).text(val.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
        }
        else if ($(this).hasClass('money') && val != null) {
        if(val == " ") val=0;
            $(this).text(val.toFixed(RoundingDigits).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
        }
        else if ($(this).hasClass('phone') && val != null && val!=0 &&  val.trim() != '') {
            var phoneArray = val.split(" ");
            var cc = "+(" + phoneArray[0] + ")";
            var ac = " " + phoneArray[1] + "-";
            var pn = phoneArray[2];
            $(this).text(cc + ac + pn);
        }
        else {
            $(this).text(val);
        }
    };
})(jQuery);



//function checkAvailablePermissions() {
//    var data = formatString('{EntityId:{0}}', EntityId);
//    $.ajax({
//        type: "POST",
//        contentType: "application/json; charset=utf-8",
//        url: PemissionWebServiceURL + "checkUnAvailablePermissions",
//        data: data,
//        datatype: "json",
//        success: function (data) {
//            var returnedData = eval("(" + data.d + ")");
//            if (returnedData.statusCode.Code == 1) {
//                if (returnedData.result != null) UnAvailablePermissionsList = returnedData.result;
//            }
//            else {
//                //if 0: then there is no permissions at all
//            }
//        },
//        error: function (e) { }
//    });
//}


//function ApplyUserPermissions(template) {
//    var Template = $(template);
//    if (UnAvailablePermissionsList != null) {
//        $.each(UnAvailablePermissionsList, function (index, item) {
//            Template.find('.' + item.CssClass).remove();
//            $('.add-entity-button').hide();

//        });
//    }
//    return Template;
//}

function compareDates(date1, date2) {
    var d1, d2;
    if (typeof date1 == 'string')
        d1 = getDotNetDateFormat(date1).getTime();
    else
        d1 = date1.getTime();

    if (typeof date2 == 'string')
        d2 = getDotNetDateFormat(date2).getTime();
    else
        d2 = date2.getTime();

    if (d1 == d2)
        return 0;
    else if (d1 > d2)
        return 1;
    else
        return -1;
}


function checkPermissions(Sender, EntityId, ParentId, Template, Callback) {
    debugger
    //   if ($(Sender).attr('pChecked') != 'checked') {
    //   $(Sender).attr('pChecked', 'checked');
    var data = formatString('{EntityId:{0},ParentId:{1}}', EntityId, ParentId);
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: PemissionWebServiceURL + "checkUnAvailablePermissions",
        data: data,
        datatype: "json",
        success: function (data) {
        
            var returnedData = eval("(" + data.d + ")");
            if (returnedData.statusCode.Code == 1) {
                if (returnedData.result != null) {
                    UnAvailablePermissionsList = returnedData.result;
                    if (returnedData.statusCode.message != "all permission are available")
                        showStatusMsg(getResource('vldDontHavePermission'));
                    Template = ApplyUserPermissions(Template);
                }
                else {

                    $('.add-entity-button').show();
                    $('.TabbedPanelsContent:visible').find('.Tab-action-header').show();

                }
            }
            else {

                showStatusMsg(getResource('vldDontHavePermission'));
                
            }
       
            if (Callback) {
                Callback(Template);

                //Added By Ruba Al-Sa'di 4/11/2014 to handle Leaving/Refreshing the page when add form is opened OR in any tab editing is in process
                window.onbeforeunload = function (e) {
                    var msg = 'You Are attempting to leave this page without saving your changes!';

                    return CheckIfAddingOrEditingIsInProcess()
                            ? msg : null;  //If Add Mode OR edit button is clicked
                }
            }
        },
        error: function (e) { }
    });
    //    }
    //    else {
    //        if (Callback)
    //            Callback(Template);

    //    }
}



///new Check permission  NOTE :::: this for reports Page 

function NewcheckPermissions(Sender, EntityId, ParentId) {
    //   if ($(Sender).attr('pChecked') != 'checked') {
    //   $(Sender).attr('pChecked', 'checked');
    var data = formatString('{EntityId:{0},ParentId:{1}}', EntityId, ParentId);
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: PemissionWebServiceURL + "checkUnAvailablePermissions",
        data: data,
        datatype: "json",
        async: false,
        cache: false,
        
        success: function (data) {
            var returnedData = eval("(" + data.d + ")");
            if (returnedData.statusCode.Code == 1) {
                if (returnedData.result != null) {
                    UnAvailablePermissionsList = returnedData.result;
                    if (returnedData.statusCode.message != "all permission are available")
                        havePer = 0;
                    else
                        havePer = 1;
                }
                else {

                    $('.add-entity-button').show();
                    $('.TabbedPanelsContent:visible').find('.Tab-action-header').show();

                }
            }
            else {
                //if 0: then there is no permissions at all
                // alert('no access');
            }

        },
        error: function (e) { }
    });
    //    }
    //    else {
    //        if (Callback)
    //            Callback(Template);

    //    }
}

///end 
function ApplyUserPermissions(template) {

    var Template = $(template);
    if (UnAvailablePermissionsList != null) {
        $.each(UnAvailablePermissionsList, function (index, item) {
            Template.find(item.CssClass).remove();

            if (item.PermissionId == permissions.ListId) {
                if ($('.TabbedPanelsTabSelected').index() == -1) {
                    //summary page
                    $(item.CssClass).hide();
                }
            }
            if (item.PermissionId == permissions.ViewId) {
                if ($('.TabbedPanelsTabSelected').index() == -1) {
                    //summary page
                    $(item.CssClass).hide();
                }
            }

            if (item.PermissionId == permissions.AddId) {
                enablePermissionAdd = false;
                if ($('.TabbedPanelsTabSelected').index() == -1) {
                    //summary page
                    $(item.CssClass).remove();
                }
                else {
                    //tab
                    $('.TabbedPanelsContent:visible').find(item.CssClass).remove();
                }
            }

            if (item.PermissionId == permissions.DeleteId)
                enablePermissionDelete = false;
            if (item.PermissionId == permissions.UnLock) {
                $(item.CssClass).remove();
            }

        });
    }

    $('.TabbedPanelsContent:visible').find('.Tab-action-header').show();
    $('.add-entity-button').show();
    return Template;
}


function lockEntity(entityId, entityValueId, callback, lastUpdatedDate, modifiedCallback, deletedCallBack) {
    debugger
    var DateTime = getEntityLockDateFormat(lastUpdatedDate);

    var url = systemConfig.ApplicationURL_Common + systemConfig.ConfigurationWebServiceURL + 'Lock';
    var args = formatString('{entityId:{0}, entityValueId:{1},timeStamp:"{2}"}', entityId, entityValueId ? entityValueId : null, DateTime);

    $.ajax({
        type: 'POST',
        url: url,
        data: args,
        contentType: 'application/json; charset=utf8;',
        dataType: 'json',
        success: function (data) {

            var result = eval('(' + data.d + ')');
            if (result.statusCode.Code == 0) {
                var date = getDate('dd-mm-yy hh:mm tt', formatDate(result.result.LockedDate));
                showStatusMsg(getResource('ThisRecordIsLockedby') + result.result.UserName + getResource('at') + date);
            }
            else {
                if (typeof callback == 'function')
                    callback();
            }
            if (hideLoadingAfterLock)
                hideLoading();
        },
        error: function (ex) {
            var response = eval('(' + ex.responseText + ')');
            var responseMsg = eval('(' + response.Message + ')');

            if (responseMsg && responseMsg.code == '501') {
                showConfirmMsg(getResource("RecordDeleted"), getResource("RecordHasBeenDeleted"), function () {
                    if (typeof deletedCallBack == 'function')
                        deletedCallBack();

                });              
            }
            else if (responseMsg && responseMsg.code == '502') {


                showConfirmMsg(getResource("RecordChanged"), getResource("RecordHasBeenChanged"), function () {
                    if (typeof modifiedCallback == 'function')
                        modifiedCallback();
                });
            }

            hideLoading();
        }
    });
}


function lockForDelete(entityId, entityValueId, callback, deletedCallBack) {

    var url = systemConfig.ApplicationURL_Common + systemConfig.ConfigurationWebServiceURL + 'LockForDelete';
    var args = formatString('{entityId:{0}, entityValueId:{1}}', entityId, entityValueId ? entityValueId : null);

    $.ajax({
        type: 'POST',
        url: url,
        data: args,
        contentType: 'application/json; charset=utf8;',
        dataType: 'json',
        success: function (data) {

            var result = eval('(' + data.d + ')');
            if (result.statusCode.Code == 0) {
                var date = getDate('dd-mm-yy hh:mm tt', formatDate(result.result.LockedDate));
                showStatusMsg(getResource("ThisRecordIsLockedby") + result.result.UserName + getResource('at') + date);
            }
            else {
                if (typeof callback == 'function')
                    callback();
            }
            hideLoading();
        },
        error: function (ex) {
            var response = eval('(' + ex.responseText + ')');
            var responseMsg = eval('(' + response.Message + ')');

            if (responseMsg && responseMsg.code == '501') {

                showConfirmMsg(getResource("RecordDeleted"), getResource("RecordHasBeenDeleted"), function () {
                    if (typeof deletedCallBack == 'function')
                        deletedCallBack();

                });
        
            }
            else if (responseMsg && responseMsg.code == '502') {
                showConfirmMsg(getResource('RecordChanged'), getResource('RecordHasBeenChanged'), function () {
                    if (typeof modifiedCallback == 'function')
                        modifiedCallback();
                });
            }

            hideLoading();
        }
    });
}


function UnlockEntity(entityId, entityValueId, callback) {

    var args = formatString('{entityId:{0}, entityValueId:{1}}', entityId, entityValueId ? entityValueId : null);

    $.ajax({
        type: 'POST',
        url: systemConfig.ApplicationURL_Common + systemConfig.ConfigurationWebServiceURL + 'UnLock',
        data: args,
        contentType: 'application/json; charset=utf8;',
        dataType: 'json',
        success: function (data) {

            var result = eval('(' + data.d + ')');
            if (result.statusCode.Code == 0) {
                showStatusMsg(getResource("systemError"));
            }
            else {
                if (typeof callback == 'function')
                    callback();
            }

            if (hideLoadingAfterUnLock)
                hideLoading();
        },
        error: function (ex) {

            hideLoading();
        }
    })
}


function getEntityLockDateFormat(lastUpdatedDate) {
    if (lastUpdatedDate) {
        return Number(lastUpdatedDate.match(/\d+/)[0]);
    }
    else
        return '';    
}

function handleLockEntityError(ex, DeletedEntityCallBack, UpdatedEntityCallBack) {
    var response = eval('(' + ex.responseText + ')');
    var responseMsg = eval('(' + response.Message + ')');

    if (responseMsg && responseMsg.code == '501') {


        showConfirmMsg(getResource('RecordDeleted'), getResource("RecordHasBeenDeleted"), function () {
            if (typeof DeletedEntityCallBack == 'function')
                DeletedEntityCallBack();

        });
    }
    else if (responseMsg && responseMsg.code == '502') {

        showConfirmMsg(getResource('RecordChanged'), getResource('RecordHasBeenChanged'), function () {
            if (typeof UpdatedEntityCallBack == 'function')
                UpdatedEntityCallBack();
        });
    }
}

function getEntitylockAjaxHeaders(lastupdatedDate) {
    return { entityLastUpdatedDate: lastupdatedDate };
}

function SetCookie(cookieName, cookieValue, nDays) {
    var today = new Date();
    var expire = new Date();
    if (nDays == null || nDays == 0) nDays = 1;
    expire.setTime(today.getTime() + 3600000 * 24 * nDays);
    document.cookie = cookieName + "=" + escape(cookieValue)
                 + ";expires=" + expire.toGMTString();
}

function readCookie(cookieName) {
    var re = new RegExp('[; ]' + cookieName + '=([^\\s;]*)');
    var sMatch = (' ' + document.cookie).match(re);
    if (cookieName && sMatch) return unescape(sMatch[1]);
    return '';
}

//GridTableDiv-body
function showLoadingWithoutBG1(element) {
    var myTemplate = $('<div>').attr('class', "myCustomLoader");
    $(myTemplate).append($('<img>').attr('src', "../../Common/Images/loading.gif"));
    element.append($(myTemplate));
}

function hideLoadingWithoutBG() {
    $('.myCustomLoader').remove();
}


function appendNewListRow(item) {
    debugger
    var newRow = setListItemTemplateValue($(ItemListTemplate).clone(), item);
    newRow.attr('id', item.Id);
    var rowNumber = Number($('.result-list').find('tr').length + 1);
    newRow.find('span:.row-number').textFormat(0); //rowNumber++
    if ($('.result-list > tr:first').length > 0) {
        $(newRow).insertBefore($('.result-list > tr:first'));
    }
    else {
        $('.empty-list-item').remove();
        $('.result-list').append($(newRow));
    }
    $('.result-list').find('.result-block:first').click();
    $('.total-result-div').find('span').html(getResource("Total") + (++totalCurrenrEntityCount) + getResource("of") + (++totalEntityCount));
}



function post(url, data, success, error, headers) {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: url,
        data: data,
        datatype: "json",
        headers: headers ? headers : {},
        success: function (data) {
            var result = eval('(' + data.d + ')');
            if (success)
                success(result);
        },
        error: function (e) {
            if (error)
                error(e);
        }
    });
}

function postAccpacTransaction(url, data, success, error, headers) {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: url,
        data: data,
        datatype: "json",
        headers: headers ? headers : {},
        success: function (data) {
            var result = data.d;
            if (success)
                success(result);
        },
        error: function (e) {
            if (error)
                error(e);
        }
    });
}

var tableIsFocus = true;
function loadArrowsActions(table) {
    $(document).mousedown(function () {
        tableIsFocus = false;
    });
    table.mousedown(function () {
        tableIsFocus = true;
        return false;
    });
    table.children().mousedown(function () {
        tableIsFocus = true;
        return false;
    });
    $(document).keydown(function (e) {
        if (!tableIsFocus)
            return;
        if (e.which == 40) {
            if (pendingAjaxRequest)
                return;
            if (table.find('tr.SelectedTr').length > 0) {
                stopArrowScroll(e, table.find('tr.SelectedTr'), table);
                table.find('tr.SelectedTr').next().click();
            }
            else if (table.find('tr.selected-row').length > 0) {
                stopArrowScroll(e, table.find('tr.selected-row'), table, true);
                table.find('tr.selected-row').next().click();
            }
            else {
                stopArrowScroll(e, table.find('tr.selected-batch-row'), table, true);
                table.find('tr.selected-batch-row').next().click();
            }
        }
        if (e.which == 38) {
            if (pendingAjaxRequest)
                return;
            if (table.find('tr.SelectedTr').length > 0) {
                stopArrowScroll(e, table.find('tr.SelectedTr'), table);
                table.find('tr.SelectedTr').prev().click();
            }
            else if (table.find('tr.selected-row').length > 0) {
                stopArrowScroll(e, table.find('tr.selected-row'), table, true);
                table.find('tr.selected-row').prev().click();
            }
            else {
                stopArrowScroll(e, table.find('tr.selected-batch-row'), table, true);
                table.find('tr.selected-batch-row').prev().click();
            }

        }
    });
}

function stopArrowScroll(e, current, table, isBatchList) {
    var position = 0, tableScrollHeight, parentDiv;
    if (e.which == 40) {
        current.next("tr").prevAll("tr").each(function () {
            position += $(this).height();
        });
        position += 30;

        if (isBatchList)
            parentDiv = table.parent();
        else parentDiv = table.parent().parent();

        tableScrollHeight = parentDiv.outerHeight();
        if (!(current.next("tr").size() > 0 && position > (parentDiv.scrollTop() + tableScrollHeight - current.next("tr").height()))) {
            e.preventDefault();
        }
        else {
            parentDiv.scrollTop(parentDiv.scrollTop() + current.next("tr").height() + 30);
        }
    }
    else if (e.which == 38) {
        e.preventDefault();
        var scrolls = 0;
        if (current.prev("tr").size() > 0) {
            current.prev("tr").prevAll("tr").each(function () {
                position += $(this).height();
            });
            scrolls = position;
            scrolls -= current.prev("tr").height();
            if (isBatchList)
                table.parent().scrollTop(scrolls - current.height());
            else table.parent().parent().scrollTop(scrolls - current.height());
        }
        else {
            scrolls = 0;
            table.parent().scrollTop(0);
        }
    }
}

function addNew(e) {
    if (e.altKey && e.keyCode === 78) {
        if ($('.add-entity-button').is(":visible"))
            $('.add-entity-button').first().click();
    }
}


function SetTodayDate(input) {
    $(input).val(getDate("dd-mm-yy", new Date()));
}

function RoundNumber(number) {
    if (number) return Number(number).toFixed(RoundingDigits);
    else return number;
}