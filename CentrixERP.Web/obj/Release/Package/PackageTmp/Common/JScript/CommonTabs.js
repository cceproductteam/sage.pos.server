
var AddressTemplate = null, AddressServiceURL;
var NoteTemplate = null, NoteServiceURL;
var EmailTemplate = null, EmailServiceURL;
var PhoneTemplate = null, PhoneServiceURL;
var phonesCount = 0;
var emailCount = 0;
var isNullEmailPersonal = true, isNullEmailBusseness = true;


/*Start Address*/
function getAddresses(ParentId, mKey) {
    AddressServiceURL = systemConfig.ApplicationURL_Common + systemConfig.AddressServiceURL;
    $('.address-tab').children().remove();
    showLoading($('.TabbedPanels'));
    var data = formatString('{parentId:{0}, mKey:"{1}"}', ParentId, mKey);
    $.ajax({
        type: "POST",
        url: AddressServiceURL + "FindAll",
        contentType: "application/json; charset=utf-8",
        data: data,
        dataType: "json",
        success: function (data) {
            var addressObj = eval("(" + data.d + ")");
            addAddressTab(null, ParentId, mKey, null);
            if (addressObj.result != null) {
                $('.address-tab').parent().find('.DragDropBackground').remove();
                $.each(addressObj.result, function (index, item) {
                    addAddressTab(item, ParentId, mKey);
                });
            }

            attachTabEvents();
            hideLoading();
        },
        error: function (e) {
        }
    });
}

function addAddressTab(item, ParentId, mKey) {

    var id = (item != null) ? item.Id : 0;
    var isDefault = (item != null) ? item.Isdefault : false;
    var template = $(AddressTemplate).clone();
    var CountryNameAC = template.find('select:.CountryName').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.CountryServiceURL + "FindAll" });

    if (item != null) {
        template.find('.historyUpdated').append(HistoryUpdatedListViewTemplate);
        template = mapEntityInfoToControls(item, template, [{ controlName: 'CountryNameAC', control: CountryNameAC}]);
        template.find('.TruncateStreetAddress').text(Truncate(item.StreetAddress, 50));
        template.find('.TruncateType').text(Truncate(item.Type, 50));
        template.find('.TruncateCity').text("/" + Truncate(item.City, 50));
    }
    else {
        template.find('.historyUpdated').remove();
        template.find('.Tab-basics-info,.default-add').remove();
        template.addClass('addnew');
        template.hide();
        SetDefultCountry(CountryNameAC);
        $('.add-new-address').click(function () {
            CountryNameAC.clear();
            setAddLinkEvent(template);
            SetDefultCountry(CountryNameAC);
            return false;
        });
    }

    if (isDefault)
        template.find('.default-icon').addClass('default-iconVisited')

    template.find('.save-tab-lnk').on('click', function () {
        var updatedDate = item ? item.Updated_Date : null;
        saveAddress(id, ParentId, mKey, CountryNameAC, template, isDefault, updatedDate);
        template.find('.edit-tab-lnk').show();
        return false;
    });

    template.find('.delete-tab-lnk').click(function () {
        showConfirmMsg(DeleteTitleMessage, DeleteMessage, function () {
            showLoading($('.TabbedPanels'));
            lockForDelete(systemEntities.AddressEntityId, item.AddressID, function () {
                deleteAddress(id, mKey, ParentId)
                template.remove();

            }, function () {
                template.remove();
            });
        });
        return false;
    });

    template.find('.edit-tab-lnk').click(function () {
        showLoading($('.TabbedPanels'));
        lockEntity(systemEntities.AddressEntityId, item.AddressID, function () {
            //var template = $(this).parents('li.Tablist:first');
            template.find('.edit-tab-lnk').hide();
            template.find('.save-tab-lnk').show();
            template.find('.cancel-tab-lnk').show();
            template.find('.delete-tab-lnk').hide();

            if (!template.hasClass('Tab-listSelected'))
                template.find('.itemheader').click();

            template.find('.viewedit-display-none').show();
            template.find('.viewedit-display-block').hide();

            template.find('.cx-control:visible').first().focus();
        }, item.Updated_Date, function () {
            getAddresses(ParentId, mKey);
        }, function () {
            template.remove();
        });
        return false;
    });


    template.find('.cancel-tab-lnk').click(function () {
        if (item == null) {
            template.find('.delete-tab-lnk').show();
            template.find('.itemheader').click();

            $('.addnew').hide();
        }
        else {
            showLoading($('.TabbedPanels'));
            UnlockEntity(systemEntities.AddressEntityId, item.AddressID, function () {
                //var template = $(this).parents('li.Tablist:first')
                template.find('.delete-tab-lnk').show();
                template.find('.itemheader').click();

                $('.addnew').hide();
            });
        }
        return false;
    });

    template.find('a.default-icon').click(function () {

        var This = $(this);
        if (!template.find('a.default-icon').hasClass('default-iconVisited')) {
            showLoading($('.TabbedPanels'));
            lockEntity(systemEntities.AddressEntityId, item.AddressID, function () {
                // SetDefault(id, ParentId, mKey, systemConfig.ApplicationURL_Common + AddressServiceURL + "SetAddressDefualt", true, UserId);
                SetDefault(id, ParentId, mKey, AddressServiceURL + "SetAddressDefualt", true, UserId);
                $('.default-icon').removeClass('default-iconVisited');
                This.addClass('default-iconVisited');
            });
        }

        return false;
    });

    $('.address-tab').append(template);
}


function saveAddress(id, parentId, mKey, countryNameAC, template, isDefault, lastUpdatedDate) {

    if (!saveForm(template, countryNameAC)) {
        return false;
    }

    var countryId = countryNameAC.get_Item() != null ? countryNameAC.get_Item().value : -1;
    var buildingNo = escape(template.find('input:.BuildingNumber').val().trim());
    var streetAddress = escape(template.find('input:.StreetAddress').val().trim());
    var city = escape(template.find('input:.City').val().trim());
    var floor = escape(template.find('input:.Floor').val().trim());
    var zipCode = escape(template.find('input:.ZipCode').val().trim());
    var officeNo = escape(template.find('input:.OfficeNumber').val().trim());
    var county = escape(template.find('input:.County').val().trim());
    var Type = escape(template.find('textarea:.Type').val().trim());

    showLoading($('.TabbedPanels'));
    var data = '{Id:{0}, moduleId:{1}, mKey:"{2}", CountryId:{3}, City:"{4}", StreetAddress:"{5}", ZipCode:"{6}", BuildingNumber:"{7}", floor:"{8}", officeNum:"{9}", county:"{10}", IsDefault:{11},Type:"{12}",userId:{13}}';
    data = formatString(data, id, parentId, mKey, countryId, city, streetAddress, zipCode, buildingNo, floor, officeNo, county, isDefault, Type, UserId);

    $.ajax({
        type: "POST",
        url: AddressServiceURL + "AddEditAddress",
        contentType: "application/json; charset=utf-8",
        data: data,
        dataType: "json",
        headers: getEntitylockAjaxHeaders(getEntityLockDateFormat(lastUpdatedDate)),
        success: function (data) {
            showStatusMsg('saved successfully');
            getAddresses(parentId, mKey);
        },
        error: function (ex) {
            handleLockEntityError(ex, function () { template.remove(); }, function () { getAddresses(parentId, mKey); });
        }
    });
}

function deleteAddress(id, mKey, parentId) {
    $.ajax({
        type: "POST",
        url: AddressServiceURL + "DeleteAddress",
        contentType: "application/json; charset=utf-8",
        data: formatString('{id:{0}, mKey:"{1}"}', id, mKey),
        dataType: "json",
        success: function (data) {
            showStatusMsg('Deleted successfully');
            //getAddresses(parentId, mKey);
        },
        error: function (ex) {
            handleLockEntityError(ex, function () { template.remove(); }, function () { getAddresses(parentId, mKey); });
        }
    });
}

/*End Address*/


/*Start Note*/
function getNotes(entityId, entityValueId) {
    NoteServiceURL = systemConfig.ApplicationURL_Common + systemConfig.NoteServiceURL;
    $('.note-tab').children().remove();

    showLoading($('.TabbedPanels'));
    var data = formatString('{entityId:{0}, entityValueId:{1}}', entityId, entityValueId);
    $.ajax({
        type: "POST",
        url: NoteServiceURL + "FindAllLite",
        contentType: "application/json; charset=utf-8",
        data: data,
        dataType: "json",
        success: function (data) {
            var noteObj = eval("(" + data.d + ")");
            addNoteTab(null, entityId, entityValueId);
            if (noteObj.result != null) {
                $('.note-tab').parent().find('.DragDropBackground').remove();
                $.each(noteObj.result, function (index, item) {
                    addNoteTab(item, entityId, entityValueId);
                });
            }

            attachTabEvents();
            hideLoading();
        },
        error: function (e) {
        }
    });
}

function addNoteTab(item, entityId, entityValueId) {
    var id = (item != null) ? item.Id : 0;

    var template = $(NoteTemplate).clone();
    //template.find('.NoteContent').css('width', $('.Tab-list').width() - 40);
    if (item != null) {
        template.find('.historyUpdated').append(HistoryUpdatedListViewTemplate.clone());
        item.NoteContent = unescape(item.NoteContent);
        template = mapEntityInfoToControls(item, template);
        template.find('.truncatedNoteContent').text(Truncate(item.NoteContent, 50));
        /// Truncate Note Header
        var noteContent = template.find('.itemheader').find('.NoteContent').text();
        template.find('.UpdatedByName').text(item.UpdatedBy);
    }
    else {
        template.find('.historyUpdated').remove();
        template.addClass('addnew');
        template.hide();
        $('.add-new-note').click(function () {
            template.find('.cx-control-red-border').removeClass('cx-control-red-border');
            setAddLinkEvent(template);
            return false;
        });
    }

    template.find('.save-tab-lnk').on('click', function () {
        var updatedDate = item ? item.Updated_Date : null;
        saveNote(id, entityId, entityValueId, template, updatedDate);
        template.find('.edit-tab-lnk').show();
        return false;
    });


    template.find('.delete-tab-lnk').click(function () {
        showConfirmMsg(DeleteTitleMessage, DeleteMessage, function () {
            showLoading($('.TabbedPanels'));
            lockForDelete(systemEntities.NoteEntityId, item.NoteID, function () {
                deleteNote(id);
                template.remove();

            }, function () {
                template.remove();
            });
        });
        return false;
    });


    template.find('.edit-tab-lnk').click(function () {

        showLoading($('.TabbedPanels'));
        lockEntity(systemEntities.NoteEntityId, item.NoteID, function () {
            template.find('.edit-tab-lnk').hide();
            template.find('.save-tab-lnk').show();
            template.find('.cancel-tab-lnk').show();
            template.find('.delete-tab-lnk').hide();

            if (!template.hasClass('Tab-listSelected'))
                template.find('.itemheader').click();

            template.find('.viewedit-display-none').show();
            template.find('.viewedit-display-block').hide();

            template.find('.cx-control:visible').first().focus();
        }, item.Updated_Date, function () {
            getNotes(entityId, entityValueId);
        }, function () {
            template.remove();
        });
        return false;
    });


    template.find('.cancel-tab-lnk').click(function () {
        if (item == null) {
            //var template = $(this).parents('li.Tablist:first')
            template.find('.delete-tab-lnk').show();
            template.find('.itemheader').click();

            $('.addnew').hide();
        }
        else {
            showLoading($('.TabbedPanels'));
            UnlockEntity(systemEntities.NoteEntityId, item.NoteID, function () {
                template.find('.delete-tab-lnk').show();
                template.find('.itemheader').click();
                $('.addnew').hide();
            });
        }
        return false;
    });



    $('.note-tab').append(template);
}

function saveNote(id, entityId, entityValueId, template, lastUpdatedDate) {

    if (!saveForm(template)) {
        return false;
    }
    showLoading($('.TabbedPanels'));
    var noteContent = escape(template.find('textArea:.NoteContent').val().trim());
    var data = '{id:{0}, entityId:{1}, entityValueId:{2},content:"{3}"}';
    data = formatString(data, id, entityId, entityValueId, noteContent);

    $.ajax({
        type: "POST",
        url: NoteServiceURL + ((id > 0) ? "Edit" : "Add"),
        contentType: "application/json; charset=utf-8",
        data: data,
        dataType: "json",
        headers: getEntitylockAjaxHeaders(getEntityLockDateFormat(lastUpdatedDate)),
        success: function (data) {
            showStatusMsg(getResource("savedsuccessfully"));
            getNotes(entityId, entityValueId);
        },
        error: function (ex) {

            handleLockEntityError(ex, function () { template.remove(); }, function () { getNotes(entityId, entityValueId); });
        }
    });
}

function deleteNote(id) {
    $.ajax({
        type: "POST",
        url: NoteServiceURL + "Delete",
        contentType: "application/json; charset=utf-8",
        data: formatString('{id:{0}}', id),
        dataType: "json",
        success: function (data) {
            showStatusMsg('Deleted successfully');
            //getNotes(parentId, mKey);
        },
        error: function (ex) {
            handleLockEntityError(ex, function () { template.remove(); }, function () { getNotes(entityId, entityValueId); });
        }
    });
}
/*End Note*/


/*Start Phone*/
function getPhones(ParentId, mKey) {
    PhoneServiceURL = systemConfig.ApplicationURL_Common + systemConfig.PhoneServiceURL;
    $('.phone-tab').children().remove();
    showLoading($('.TabbedPanels'));
    var data = formatString('{entityId:{0}, entityValueId:{1}}', ParentId, mKey);
    $.ajax({
        type: "POST",
        url: PhoneServiceURL + "FindAllLite",
        contentType: "application/json; charset=utf-8",
        data: data,
        dataType: "json",
        success: function (data) {
            var phoneObj = eval("(" + data.d + ")");
            addPhoneTab(null, ParentId, mKey);
            if (phoneObj.result != null) {
                $('.phone-tab').parent().find('.DragDropBackground').remove();
                $.each(phoneObj.result, function (index, item) {
                    addPhoneTab(item, ParentId, mKey);
                });

                phonesCount = phoneObj.result.length;
            }

            attachTabEvents();
            hideLoading();
        },
        error: function (e) {
        }
    });
}

function addPhoneTab(item, ParentId, mKey) {
    var id = (item != null) ? item.Id : 0;
    var isDefault = (item != null) ? item.Isdefault : false;
    var template = $(PhoneTemplate).clone();

    if (item != null) {
        template.find('.historyUpdated').append(HistoryUpdatedListViewTemplate);
        template = mapEntityInfoToControls(item, template);
        template.find('.phoneDescription').text(Truncate(item.Description, 30));
    } else {
        template.find('.historyUpdated').remove();
        template.find('.Tab-basics-info,.default-add').remove();
        template.addClass('addnew');
        template.hide();
        $('.add-new-phone').click(function () {
            setAddLinkEvent(template);
            return false;
        });
    }

    if (isDefault)
        template.find('.default-icon').addClass('default-iconVisited')

    template.find('.save-tab-lnk').on('click', function () {
        var updatedDate = item ? item.Updated_Date : null;
        savePhone(id, ParentId, mKey, template, isDefault, updatedDate);
        template.find('.edit-tab-lnk').show();
        return false;
    });

    template.find('.delete-tab-lnk').click(function () {
        showConfirmMsg(DeleteTitleMessage, DeleteMessage, function () {
            showLoading($('.TabbedPanels'));
            lockForDelete(systemEntities.PhoneEntityId, item.PhoneID, function () {
                deletePhone(id, mKey, ParentId);
                template.remove();

            }, function () {
                template.remove();
            });
        });
        return false;
    })

    //    template.find('.edit-tab-lnk').click(function () {
    //        $(".PhoneCodesTable").show();
    //    });
    //    template.find('.cancel-tab-lnk').click(function () {
    //        $(".PhoneCodesTable").hide();
    //    });

    template.find('.edit-tab-lnk').click(function () {
        showLoading($('.TabbedPanels'));
        lockEntity(systemEntities.PhoneEntityId, item.PhoneID, function () {
            $(".PhoneCodesTable").show();
            //var template = $(this).parents('li.Tablist:first');
            template.find('.edit-tab-lnk').hide();
            template.find('.save-tab-lnk').show();
            template.find('.cancel-tab-lnk').show();
            template.find('.delete-tab-lnk').hide();

            if (!template.hasClass('Tab-listSelected'))
                template.find('.itemheader').click();

            template.find('.viewedit-display-none').show();
            template.find('.viewedit-display-block').hide();

            template.find('.cx-control:visible').first().focus();
        }, item.Updated_Date, function () {
            getPhones(ParentId, mKey);
        }, function () {
            template.remove();
        });
        return false;
    });


    template.find('.cancel-tab-lnk').click(function () {
        if (item == null) {
            //var template = $(this).parents('li.Tablist:first')
            $(".PhoneCodesTable").hide();
            template.find('.delete-tab-lnk').show();
            template.find('.itemheader').click();

            $('.addnew').hide();
        }
        else {
            showLoading($('.TabbedPanels'));
            UnlockEntity(systemEntities.PhoneEntityId, item.PhoneID, function () {
                //var template = $(this).parents('li.Tablist:first')
                $(".PhoneCodesTable").hide();
                template.find('.delete-tab-lnk').show();
                template.find('.itemheader').click();

                $('.addnew').hide();
            });
        }
        return false;
    });

    template.find('a.default-icon').addClass('default-icon-' + (item ? item.PhoneType : '')).click(function () {

        var This = $(this);
        if (!template.find('a.default-icon').hasClass('default-iconVisited')) {
            showLoading($('.TabbedPanels'));
            lockEntity(systemEntities.PhoneEntityId, item.PhoneID, function () {
                SetDefault(id, ParentId, mKey, systemConfig.ApplicationURL_Common + systemConfig.PhoneServiceURL + "SetPhoneDefualt", true, UserId, function () {
                    var countryCode = template.find('input:.CountryCode').val();
                    var areaCode = template.find('input:.AreaCode').val();
                    var phoneNumber = template.find('input:.PhoneNumber').val();
                    var FullPhoneNumber = "+(" + countryCode + ")" + areaCode + "-" + phoneNumber;
                    selectedEntityItemList.find('.result-data4 .phone').text(FullPhoneNumber);
                });
                $('.default-icon-' + item.PhoneType).removeClass('default-iconVisited');
                This.addClass('default-iconVisited');
            });
        }

        return false;
    });

    preventInputChars(template.find('input[type=text]'));
    $('.phone-tab').append(template);
}

function savePhone(id, parentId, mKey, template, isDefault, lastUpdatedDate) {

    if (!saveForm(template)) {
        return false;
    }

    var countryCode = template.find('input:.CountryCode').val();
    var areaCode = template.find('input:.AreaCode').val();
    var phoneNumber = template.find('input:.PhoneNumber').val();
    var Description = escape(template.find('textarea:.Description').val().trim());
    var phoneType;
    if (template.find('input:.LandLine').is(':checked'))
        phoneType = 1;
    else if (template.find('input:.Skype').is(':checked'))
        phoneType = 2;
    else if (template.find('input:.Mobile').is(':checked'))
        phoneType = 3;

    showLoading($('.TabbedPanels'));
    var data = '{id:{0}, entityId:{1}, entityValueId:"{2}",CountryCode:"{3}",AreaCode:"{4}",PhoneNumber:"{5}",Type:"{6}",IsDefault:"{7}",Description:"{8}"}';
    data = formatString(data, id, parentId, mKey, countryCode, areaCode, phoneNumber, phoneType, isDefault, Description);


    $.ajax({
        type: "POST",
        url: PhoneServiceURL + ((id > 0) ? "Edit" : "Add"),
        //url: PhoneServiceURL + "AddEditPhone",
        contentType: "application/json; charset=utf-8",
        data: data,
        dataType: "json",
        headers: getEntitylockAjaxHeaders(getEntityLockDateFormat(lastUpdatedDate)),
        success: function (data) {
            //var phone = eval("(" + data.d + ")").result; 
            if ((phoneType == 3 && template.find('.default-icon').hasClass('default-iconVisited')) || (phoneType == 3 && phonesCount == 0)) {
                var FullPhoneNumber = "+(" + countryCode + ")" + areaCode + "-" + phoneNumber;
                selectedEntityItemList.find('.result-data4 .phone').text(FullPhoneNumber);
            }
            showStatusMsg('saved successfully');
            getPhones(parentId, mKey);
        },
        error: function (ex) {
            handleLockEntityError(ex, function () { template.remove(); }, function () { getPhones(parentId, mKey); });
        }
    });
}

function deletePhone(id, mKey, parentId) {
    $.ajax({
        type: "POST",
        url: PhoneServiceURL + "DeletePhone",
        contentType: "application/json; charset=utf-8",
        data: formatString('{id:{0}, mKey:"{1}"}', id, mKey),
        dataType: "json",
        success: function (data) {
            selectedEntityItemList.find('.result-data4 .phone').text('');
            showStatusMsg('Deleted successfully');
            //getPhones(parentId, mKey);
        },
        error: function (ex) {
            handleLockEntityError(ex, function () { template.remove(); }, function () { getPhones(parentId, mKey); });
        }
    });
}

/*End Phone*/

/*Start Email*/


function getEmails(entityId, entityValueId) {
    EmailServiceURL = systemConfig.ApplicationURL_Common + systemConfig.EmailServiceURL;
    $('.email-tab').children().remove();
    showLoading($('.TabbedPanels'));
    var data = formatString('{entityId:{0}, entityValueId:{1}}', entityId, entityValueId);
    $.ajax({
        type: "POST",
        url: EmailServiceURL + "FindAllLite",
        contentType: "application/json; charset=utf-8",
        data: data,
        dataType: "json",
        success: function (data) {
            var emailObj = eval("(" + data.d + ")");
            addEmailTab(null, entityId, entityValueId);
            if (emailObj.result != null) {
                $('.email-tab').parent().find('.DragDropBackground').remove();
                $.each(emailObj.result, function (index, item) {
                    addEmailTab(item, entityId, entityValueId);
                });
                emailCount = emailObj.result.length;

            }
            else emailCount = 0;

            attachTabEvents();
            hideLoading();
        },
        error: function (e) {
        }
    });
}


function addEmailTab(item, entityId, entityValueId) {
    var id = (item != null) ? item.Id : 0;
    var isDefault = (item != null) ? item.Isdefault : false;

    var template = $(EmailTemplate).clone();
    var EmailTypeAC = fillDataTypeContentList(template.find('select:.EmailType'), systemConfig.dataTypes.EmailType, true, false);

    if (item != null) {
        template.find('.email-to').attr('href', 'mailto:' + item.EmailAddress);
        template.find('.historyUpdated').append(HistoryUpdatedListViewTemplate.clone());
        template = mapEntityInfoToControls(item, template, [{ controlName: 'EmailTypeAC', control: EmailTypeAC}]);
        //template = mapEntityInfoToControls(item, template);
        template.find('.UpdatedByName').text(item.UpdatedBy);
        template.find('.EmailDescription').text(Truncate(item.Description, 50));
        if (isDefault) {
            template.find('input:.IsDefult').removeClass('viewedit-display-none').hide(); //attr('checked', 'checked');
            template.find('span:.IsDefult').text(getResource("Yes")).removeClass('viewedit-display-block');
        }
        else {
            template.find('span:.IsDefult').text(getResource("No"));
            template.find('.default-icon').remove();
        }



        if (item.EmailType == systemConfig.dataTypes.BussenessEmail)
            isNullEmailBusseness = false;
        else if (item.EmailType == systemConfig.dataTypes.PersonalEmail)
            isNullEmailPersonal = false;



    } else {
        template.find('.historyUpdated').remove();
        template.find('.Tab-basics-info,.default-add').remove();
        template.addClass('addnew');
        template.hide();
        $('.add-new-email').click(function () {
            template.find('.cx-control-red-border').removeClass('cx-control-red-border');
            EmailTypeAC.clear();
            setAddLinkEvent(template);
            return false;
        });
    }

    if (isDefault) {

        template.find('.default-icon').addClass('default-iconVisited')
        if (item.EmailTypeId == systemConfig.dataTypes.BussenessEmail) {
            $('.BusinessEmail').text(item.EmailAddress);
            $('.HeaderBusinessEmail').text(item.EmailAddress);
            selectedEntityItemList.find('.result-data3 .Email').text(' ' + item.EmailAddress);
        }
        else if (item.EmailTypeId == systemConfig.dataTypes.PersonalEmail)
            $('.PersonalEmail').text(item.EmailAddress);
    }


    template.find('.save-tab-lnk').on('click', function () {

        isDefault = (isDefault == null) ? false : isDefault;
        var updatedDate = item ? item.Updated_Date : null;
        saveEmail(id, entityId, entityValueId, template, isDefault, updatedDate, EmailTypeAC);

        return false;
    });


    template.find('.delete-tab-lnk').click(function () {
        showConfirmMsg(DeleteTitleMessage, DeleteMessage, function () {
            showLoading($('.TabbedPanels'));
            lockForDelete(systemEntities.EmailEntityId, item.EmailId, function () {
                var removeFromList = false;
                if (isDefault && item.EmailType)
                    removeFromList = true;
                deleteEmail(id, removeFromList, entityId, entityValueId);
                template.remove();

            }, function () {
                template.remove();
            });
        });
        return false;
    });

    template.find('.edit-tab-lnk').click(function () {
        showLoading($('.TabbedPanels'));
        lockEntity(systemEntities.EmailEntityId, item.EmailId, function () {
            //var template = $(this).parents('li.Tablist:first');
            template.find('.edit-tab-lnk').hide();
            template.find('.save-tab-lnk').show();
            template.find('.cancel-tab-lnk').show();
            template.find('.delete-tab-lnk').hide();

            if (!template.hasClass('Tab-listSelected'))
                template.find('.itemheader').click();

            template.find('.viewedit-display-none').show();
            template.find('.viewedit-display-block').hide();

            template.find('.cx-control:visible').first().focus();
        }, item.Updated_Date, function () {
            getEmails(entityId, entityValueId);
        }, function () {
            template.remove();
        });
        return false;
    });


    template.find('.cancel-tab-lnk').click(function () {
        if (item == null) {
            //var template = $(this).parents('li.Tablist:first')
            template.find('.delete-tab-lnk').show();
            template.find('.itemheader').click();

            $('.addnew').hide();
        }
        else {
            showLoading($('.TabbedPanels'));
            UnlockEntity(systemEntities.EmailEntityId, item.EmailId, function () {
                //var template = $(this).parents('li.Tablist:first')
                template.find('.delete-tab-lnk').show();
                template.find('.itemheader').click();

                $('.addnew').hide();
            });
        }
        return false;
    });
    template.find('a.default-icon').attr('title', getResource("default"));

    $('.email-tab').append(template);
}

function saveEmail(id, entityId, entityValueId, template, isDefault, lastUpdatedDate, EmailTypeAC) {

    if (!saveForm(template)) {
        return false;
    }

    if (template.find('input:.EmailAddress').val() == '') {
        addPopupMessage(template.find('input:.EmaillAddress'), 'Email is required');
    }

    var EditDefultRecord = isDefault;
    showLoading($('.TabbedPanels'));
    var EmailAddress = template.find('input:.EmailAddress').val().trim();
    var EmailType = EmailTypeAC.get_ItemValue();

    var IsDefultValue = !EditDefultRecord == true ? template.find('input:.IsDefult').is(':checked') : true;
    showLoading($('.TabbedPanels'));
    var description = "";
    var data = '{id:{0}, entityId:{1}, entityValueId:{2},EmailAddress:"{3}",EmailTypeId:{4},isDefault:{5},description:"{6}"}';
    //data = formatString(data, id, entityId, entityValueId, EmailAddress, EmailType, isDefault, description);
    data = formatString(data, id, entityId, entityValueId, EmailAddress, EmailType, IsDefultValue, description);

    $.ajax({
        type: "POST",
        url: EmailServiceURL + ((id > 0) ? "Edit" : "Add"),
        contentType: "application/json; charset=utf-8",
        data: data,
        dataType: "json",
        headers: getEntitylockAjaxHeaders(getEntityLockDateFormat(lastUpdatedDate)),
        success: function (data) {
            var returnedValue = eval("(" + data.d + ")");

            if (returnedValue.statusCode.Code == 501) {
                hideLoading();
                addPopupMessage(template.find('input:.EmailAddress'), 'This Email Already exists');
                return false;
            }
            else if (returnedValue.statusCode.Code == 502) {
                hideLoading();
                addPopupMessage(template.find('input:.EmailAddress'), 'This Email Already exists');
                return false;
            }
            //var email = eval("(" + data.d + ")").result;
            showStatusMsg(getResource("savedsuccessfully"));
            getEmails(entityId, entityValueId);

            if (template.find('.default-icon').hasClass('default-iconVisited') || emailCount == 0) {

                if (EmailType == systemConfig.dataTypes.BussenessEmail) {
                    selectedEntityItemList.find('.result-data3 .Email').text(' ' + EmailAddress);
                    $('.HeaderPersonEmail').text(EmailAddress);
                    $('.BusinessEmail').text(EmailAddress);
                    $('.HeaderBusinessEmail').text(EmailAddress);
                }
                else if (EmailType == systemConfig.dataTypes.PersonalEmail) {
                    $('.PersonalEmail').text(EmailAddress);
                }

                if (isNullEmailBusseness) {
                    $('.BusinessEmail').text('');
                    $('.HeaderBusinessEmail').text('');
                    selectedEntityItemList.find('.result-data3 .Email').text(' ');
                }
                if (isNullEmailPersonal)
                    $('.PersonalEmail').text('');


            }
            template.find('.edit-tab-lnk').show();
        },
        error: function (ex) {
            handleLockEntityError(ex, function () { template.remove(); }, function () { getEmails(entityId, entityValueId); });
        }
    });
}

function deleteEmail(id, RemoveFromList, entityId, entityValueId) {
    $.ajax({
        type: "POST",
        url: EmailServiceURL + "Delete",
        contentType: "application/json; charset=utf-8",
        data: formatString('{id:{0}}', id),
        dataType: "json",
        success: function (data) {
            showStatusMsg(getResource("deletesuccessfully"));

            $('.PersonalEmail').text('');
            $('.BusinessEmail').text('');
            $('.HeaderBusinessEmail').text('');
            selectedEntityItemList.find('.result-data3 .Email').text(' ');
            getEmails(entityId, entityValueId);
        },
        error: function (ex) {
            handleLockEntityError(ex, function () { template.remove(); }, function () { getEmails(entityId, entityValueId); });
        }
    });
}

/*End Email*/



/*Start Attachments*/

function getAttachments(entityId, entityValueId, moduleName, parentEntityId) {
    showLoading($('.TabbedPanels'));
    var url = formatString("{0}Common/Documents/DocumentHTML5.aspx?key={1}&modulename={2}&id={3}&eid={4}&parententityid={5}", systemConfig.ApplicationURL_Common, entityId, moduleName, entityValueId, entityValueId, parentEntityId);
    $('.document-iframe').attr('src', url);
}


/*End Attachments*/

