var TaskListViewItemTemplate, SubTaskListViewItemTemplate, TaskDiscussionViewItem;
var fullCalendarObj;
var taskServiceURL, taskConfigServiceURL, taskStatusServiceURL, taskActivityLookupServiceURL;

var loadedCalendar = false;
var CurrentStatusId = -1;
var opendFromTab = false;

var TaskPriorityAC, TaskTypeAC, StatsuAC, SubTaskStatus, ActivityLookupAC;
var taskConfigControl;
//, currentTaskConfigValues;

var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

var currentEvent = null, saveMode = false;

var deleteTaskError = "faild to delete this task", deleteTaskSuccess = "deleted successfuly", deleteTaskTitle = "Delete Task";
var errorLoadingCalendar = "error while loading calendar";
var areYouSure = "Are you sure ?";


var CalenderPermissionsList = '';
var taskViewPermission = false;
var taskAddPermission = false;
var taskDeletePermission = false;
var taskEditPermission = false;
var discussionAddPermission = false;
var discussionViewPermission = false;
var AttachmentsViewPermission = false;

var viewMaintaskId = -1;
var viewSubTaskId = -1;
var AddNewEvent = 0;

var CompanyTaskItem = null;
var PersonTaskItem = null;
var OpportunityTaskItem = null;
var QuotationTaskItem = null;
var ContractTaskItem = null;
var WorkOrderTaskItem = null;
var ClientCareTaskItem = null;


function Load_Calendar() {
    $('.search-box').hide();
    getCalendarPermissions(function () {
        showLoadingWithoutBG($('.Page-container'))
        setSelectedMenuItem($('.Calendar-icon'));
        setPageTitle('Calendar');
        setServicesURLs();
        initCalendarControls();
        //    taskServiceURL = systemConfig.ApplicationURL_Common + systemConfig.CalendarWebServiceURL;
        //    taskConfigServiceURL = systemConfig.ApplicationURL_Common + systemConfig.TaskConfigWebService + "FindAllLite";
        //    taskStatusServiceURL = systemConfig.ApplicationURL_Common + systemConfig.TaskStatusWebService + "FindAllLite";
        //    taskActivityLookupServiceURL = systemConfig.ApplicationURL_Common + systemConfig.ActivityLookupWebService + "FindAllLite";
    });

}

function loadCalendarFromTab(callback) {
    getCalendarPermissions(function () {
        $('.event-info:first').addClass('event-info-tab');
        opendFromTab = true;
        setServicesURLs();
        //showLoadingWithoutBG($('.Page-container'));
        loadTaskConfig(true, callback);
        init_CommonEvents();
    });
}

function setServicesURLs() {
    taskServiceURL = systemConfig.ApplicationURL_Common + systemConfig.CalendarWebServiceURL;
    taskConfigServiceURL = systemConfig.ApplicationURL_Common + systemConfig.TaskConfigWebService + "FindAllLite";
    taskStatusServiceURL = systemConfig.ApplicationURL_Common + systemConfig.TaskStatusWebService + "FindAllLite";
    taskActivityLookupServiceURL = systemConfig.ApplicationURL_Common + systemConfig.ActivityLookupWebService + "FindAllLite";
}

function initCalendarControls() {
    $('.all-tasks').click(function () {
        $('.seletedStatus').removeClass('seletedStatus');
        CurrentStatusId = -1;
        $(this).addClass('seletedStatus')
        ShowHideEventInfo(false);
        searchEvents(-1);
    });

    setAddEvent();

    //    $('.btn-add-task').click(function () {
    //        ShowHideEventInfo(false, null);
    //        ShowHideEventInfo(true, true);
    //        dayClick(new Date());
    //        //dayClick(formatDate(event.task.StartDate));
    //        return false;
    //    });

    var args = '{keyword:"",page:1,resultCount:50,argsCriteria:""}';

    createPostRequest(taskStatusServiceURL, args, function (data) {
        if (data.result) {
            $.each(data.result, function (index, item) {
                var li = $('<li>').append($('<span>').addClass('taskStatus  ' + item.CssClass), item.label)
                .click(function () {
                    $('.seletedStatus').removeClass('seletedStatus');
                    CurrentStatusId = item.value;
                    $(this).addClass('seletedStatus')
                    searchEvents(item.value);
                })
                if (item.setAsDefault) {
                    CurrentStatusId = item.value;
                    $('.seletedStatus').removeClass('seletedStatus');
                    li.addClass('seletedStatus');
                }
                $('.all-tasks').after(li);

            });
            loadTaskConfig(false)
            init_CommonEvents();

        }
    });

    $('.btn-task-search').click(function () {
        ShowHideEventInfo(false);
        searchEvents(CurrentStatusId);
    });
}

function initCalendar(events) {

    //    var date = new Date();
    //    var d = date.getDate();
    //    var m = date.getMonth();
    //    var y = date.getFullYear();

    if (!loadedCalendar) {
        loadedCalendar = true;
        fullCalendarObj = $('.calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            defaultView: 'month',
            // theme:true,
            allDayDefault: false,
            // events: events,
            eventClick: eventClick,
            dayClick: dayClick,
            eventResize: eventResize,
            eventDrop: eventDrop,
            viewDisplay: viewDisplay,
            disableResizing: !taskEditPermission,
            disableDragging: !taskEditPermission
        });
    }

    fullCalendarObj.fullCalendar('removeEventSource');
    fullCalendarObj.fullCalendar('removeEvents');
    if (events) {
        fullCalendarObj.fullCalendar('addEventSource', events);
        fullCalendarObj.fullCalendar('refetchEvents');
    }

    //    else {
    //        fullCalendarObj.fullCalendar('removeEventSource');
    //        fullCalendarObj.fullCalendar('removeEvents');
    //        if (events) {
    //            fullCalendarObj.fullCalendar('addEventSource', events);
    //            fullCalendarObj.fullCalendar('refetchEvents');
    //        }
    //    }
    hideLoading();
}


function initMainTaskTemplateControl(taskConfig, callback) {
    //debugger;
    TaskListViewItemTemplate = $(TaskListViewItemTemplate);

    taskConfigControl = {
        taskAssignment: new Array(),
        taskEntity: new Array()
    };


    fillDateList(TaskListViewItemTemplate.find('select:.lstStartDate,select:.lstEndDate'));
    TaskListViewItemTemplate.find('select:.lstStartDate').change(function () {
        var selectedOptionIndex = $(this).find('option:selected').index();
        var next = TaskListViewItemTemplate.find('select:.lstEndDate option').eq(selectedOptionIndex + 1);
        TaskListViewItemTemplate.find('select:.lstEndDate').val(next.html());
        //var next = selectedOption.attr('next');
        //TaskListViewItemTemplate.find('select:.lstEndDate').val(next);
    });


    TaskListViewItemTemplate.find('input:.StartDate').change(function () {
        TaskListViewItemTemplate.find('input:.EndDate').val($(this).val());
    });

    if (taskConfig) {

        var assignedToTable = $('<table border="0" cellpadding="0" cellspacing="0" class="Data-Table noBorder">');
        var assignedForTable = $('<table border="0" cellpadding="0" cellspacing="0" class="Data-Table noBorder">');
        //var tr = $('<tr>');
        var currentTR, currentEntityConfig, currentIndex;
        $.each(taskConfig, function (index, item) {
            currentIndex = index;
            currentEntityConfig = item.ConfigType;

            if (index % 2 == 0) {
                //assignedToTable.append($('<tr>'));
                currentTR = $('<tr>'); // assignedToTable.find('tr:last');
            }


            var list = $('<ul>').addClass('task-assignentity-list  viewedit-display-block lst-' + item.EntityName);

            var th = $('<th class="label-title">').html(item.EntityUIName);

            var controlContainer = $('<td>').addClass('task-assignentity-control')
            .append($('<div>').addClass('viewedit-display-none').append($('<select>')), list);


            var control = controlContainer.find('select').AutoComplete({
                serviceURL: systemConfig.ApplicationURL_Common + item.ServiceUrl + item.ServiceFindallMethod
            , required: false, staticParams: true, multiSelect: (item.ConfigType == 1)
            });

            //            if (item.OnChangeFunction) {

            //                var func = new Function("parentLst", item.OnChangeFunction);
            //                control.onChange(function () {
            //                    func(control);
            //                    //$.each(taskConfigControl.taskEntity, function () { if (this.controlName == 'lstCompany') { this.control.clear(); if (parentLst.get_Item() == null); this.control.set_Item({label: parentLst.get_Item().companyName , value: parentLst.get_Item().companyId }); return false; } });
            //                });
            //            }

            currentTR.append(th, controlContainer);

            if (item.ConfigType == 1) {
                if (index % 2 > 0) {
                    assignedToTable.append(currentTR);
                }
                //TaskListViewItemTemplate.find('.task-assignment').append(controlContainer);

                taskConfigControl.taskAssignment.push({ control: control, entityId: item.EntityId, controlName: 'lst' + item.EntityName });
            }
            else {
                if (index % 2 > 0) {
                    assignedForTable.append(currentTR);
                }

                taskConfigControl.taskEntity.push({ control: control, entityId: item.EntityId, controlName: 'lst' + item.EntityName });
            }
        });

        if (currentEntityConfig == 1) {
            if (currentIndex % 2 == 0) {
                assignedToTable.append(currentTR);
            }
        }
        else if (currentIndex % 2 == 0) {
            assignedForTable.append(currentTR);
        }



        TaskListViewItemTemplate.find('.task-entity').append(assignedForTable);
        TaskListViewItemTemplate.find('.task-assignment').append(assignedToTable);
    }

    TaskTypeAC = fillDataTypeContentList(TaskListViewItemTemplate.find('select:.TaskType'), systemConfig.dataTypes.TaskType, true, false, true);
    TaskPriorityAC = fillDataTypeContentList(TaskListViewItemTemplate.find('select:.TaskPriority'), systemConfig.dataTypes.TaskPriority, true, false, true);
    StatsuAC = TaskListViewItemTemplate.find('select:.Status').AutoComplete({
        serviceURL: taskStatusServiceURL,
        autoLoad: false, required: true,
        hasDefaultItem: true
    });
    StatsuAC.fillDefaultValue();

    ActivityLookupAC = TaskListViewItemTemplate.find('select:.ActivityLookup').AutoComplete({ serviceURL: taskActivityLookupServiceURL, multiSelect: true });

    TaskListViewItemTemplate.find('.btn-save-discussion').click(function () {
        var details = TaskListViewItemTemplate.find('.txt-discussion').val().trim();
        if (details) {
            addTaskDicussion(TaskListViewItemTemplate, currentEvent.task.id, details);
            TaskListViewItemTemplate.find('.txt-discussion').val('');
        }
    });

    LoadDatePicker(TaskListViewItemTemplate.find('input:.date'));

    if (typeof callback == 'function')
        callback();
}


function initSubTaskTemplateControl(taskConfig) {
    SubTaskListViewItemTemplate = $(SubTaskListViewItemTemplate);

    if (taskConfig) {

        var assignedToTable = $('<table border="0" cellpadding="0" cellspacing="0" class="Data-Table noBorder">');
        var assignedForTable = $('<table border="0" cellpadding="0" cellspacing="0" class="Data-Table noBorder">');
        //var tr = $('<tr>');
        var currentTR, currentEntityConfig, currentIndex;
        $.each(taskConfig, function (index, item) {

            currentIndex = index;
            currentEntityConfig = item.ConfigType;

            if (index % 2 == 0) {
                //assignedToTable.append($('<tr>'));
                currentTR = $('<tr>'); // assignedToTable.find('tr:last');
            }


            var list = $('<ul>').addClass('task-assignentity-list lst-' + item.EntityName);

            var th = $('<th class="label-title">').html(item.EntityUIName);

            var controlContainer = $('<td>').addClass('task-assignentity-control')
            .append($('<div>'), list);


            currentTR.append(th, controlContainer);

            if (item.ConfigType == 1) {
                if (index % 2 > 0) {
                    assignedToTable.append(currentTR);
                }

                //taskConfigControl.taskAssignment.push({ control: control, entityId: item.EntityId, controlName: 'lst' + item.EntityName });
            }
            else {
                if (index % 2 > 0) {
                    assignedForTable.append(currentTR);
                }

                //taskConfigControl.taskEntity.push({ control: control, entityId: item.EntityId, controlName: 'lst' + item.EntityName });
            }
        });

        if (currentEntityConfig == 1) {
            if (currentIndex % 2 == 0) {
                assignedToTable.append(currentTR);
            }
        }
        else if (currentIndex % 2 == 0) {
            assignedForTable.append(currentTR);
        }

        SubTaskListViewItemTemplate.find('.task-entity').append(assignedForTable);
        SubTaskListViewItemTemplate.find('.task-assignment').append(assignedToTable);
    }

    SubTaskStatus = SubTaskListViewItemTemplate.find('select:.Status').AutoComplete({
        serviceURL: taskStatusServiceURL,
        autoLoad: false, required: true
    });

    SubTaskListViewItemTemplate.find('.btn-save-discussion').click(function () {
        var details = SubTaskListViewItemTemplate.find('.txt-discussion').val().trim();
        SubTaskListViewItemTemplate.find('.txt-discussion').val('');
        if (details)
            addTaskDicussion(SubTaskListViewItemTemplate, currentEvent.task.TaskId, details);
    });

    SubTaskListViewItemTemplate.find('.ActionEdit-link').click(function () {
        SubTaskListViewItemTemplate.find('.ActionEdit-link').hide();
        SubTaskListViewItemTemplate.find('.ActionSave-link, .ActionCancel-link').hide();
        return false;
    })
}


/////////////////////////////////////////////
////////////Event Action ADD-EDIT-DELETE////

/// moved to task.js

//////////////Event Action ADD-EDIT-DELETE//////
///////////////////////////////////////////////


///////////////////////////////////////////////
//////////////LOAD DATA///////////////////////


function loadTaskConfig(fromTab, callback) {

    GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.TaskDiscussionViewItem, null, 'TaskDiscussionViewItem');

    createPostRequest(taskConfigServiceURL, '{keyword:"",page:1,resultCount:10,argsCriteria:"{}"}',
    function (result) {

        GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.TaskListViewItem,
        function () {
            initMainTaskTemplateControl(result.result, callback);
            //done by mieassar
            if (!fromTab) {
                HandleCalenderQueryString(true);
                LoadClaenderOnChangeAcs();
            }
        }
    , 'TaskListViewItemTemplate');


        GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.SubTaskListViewItem,
        function () {
            initSubTaskTemplateControl(result.result);
            //done by mieassar
            if (!fromTab) {
                HandleCalenderQueryString(false);
            }
        }
        , 'SubTaskListViewItemTemplate');

        if (!fromTab) {
            searchEvents(CurrentStatusId);
        }

        //loadEvents(UserId);
    },
    function (ex) {
        DisplayStatusMsg(errorLoadingCalendar, 'error');
    });
}

function loadEvents(userId, month, status, assignedByUser, assignedToUser) {
    //var month
    var criteria = '{UserId:{0}, Month:{1}, StatusId:{2}, AssignedByUser:{3}, AssignedToUser:{4}}';
    criteria = formatString(criteria, userId || -1, month || -1, status || -1, assignedByUser ? assignedByUser : null, assignedToUser ? assignedToUser : null);


    createPostRequest(taskServiceURL + 'FindAllLite', '{taskCriteria:"' + criteria + '"}',
    function (result) {

        if (result.statusCode.Code) {
            initCalendar(result.result);
        }
        else {
            DisplayStatusMsg(errorLoadingCalendar, 'error');
            hideLoading();
        }

    }, function (ex) {
        showStatusMsg('error while loading events');
        hideLoading();
    });
}

function loadEventInfo(event) {
    var args = formatString('{TaskId:{0}, AssignmentTaskId:{1}, subTask:{2}}', event.id, event.TaskAssignmentId, event.SubTask);
    createPostRequest(taskServiceURL + 'FindTaskByIdLite', args,
    function (result) {
        if (result.result.task)
            showEvent(result.result, false);
        else {
            showStatusMsg('this task has been deleted by the creator');
            fullCalendarObj.fullCalendar('removeEvents', event.id);
        }
    },
    function (ex) {
        showStatusMsg('error while loading event');
        //debugger;
    });
}





function showEvent(eventData, addEvent) {
    showLoadingWithoutBG($('.calendar'));

    clearEventInfo();
    currentEvent = eventData;


    var template = (!eventData.task.SubTask || addEvent) ? TaskListViewItemTemplate : SubTaskListViewItemTemplate;
    template.find('.document-iframe').attr('src', '');
    CheckTemplatePermission(template);


    if (eventData.task.SubTask) {
        if (!addEvent && LoggedInUser.UserId != eventData.task.EntityValueId) {
            template.find('.task-user-action').hide();
            eventData.task.EnabledTask = false;
        }
        else
            template.find('.task-user-action').show();
    }
    else {
        if (!addEvent && LoggedInUser.UserId != eventData.task.CreatedById) {
            template.find('.task-user-action').hide();
            eventData.task.EnabledTask = false;
        }
        else
            template.find('.task-user-action').show();
    }

    if (addEvent) {
        template.find('.viewedit-display-none').show();
        template.find('.viewedit-display-block').hide();
    }
    else {
        template.find('.viewedit-display-none').hide();
        template.find('.viewedit-display-block').show();

    }

    ShowHideEventInfo(true, !eventData.task.SubTask);


    template = mapEntityInfoToControls(eventData.task, template);
    if (eventData.task.UpdatedDate) {
        template.find('span:.UpdatedDate').text(getDate('dd-mm-yy hh:mm tt', formatDate(eventData.task.UpdatedDate)));
    }
    template.find('span:.CreatedDate').text(getDate('dd-mm-yy hh:mm tt', formatDate(eventData.task.CreatedDate)));
    template.find('span:.ActivityLookup').text('');


    if (eventData.task.allDay) {
        template.find('select:.lstStartDate,select:.lstStartDate').val('--:--');
    }
    else {
        var startTime = formatAMPM(formatDate(eventData.task.StartDate));
        template.find('select:.lstStartDate').val(eventData.task.StartHour);
        var endTime = formatAMPM(formatDate(eventData.task.EndDate));
        template.find('select:.lstEndDate').val(eventData.task.EndHour);
    }



    if (!addEvent) {
        if (!eventData.task.SubTask) {
            if (eventData.task.TaskTypeId > 0)
                TaskTypeAC.set_Item({ label: eventData.task.TaskType, value: eventData.task.TaskTypeId });
            if (eventData.task.TaskPriorityId > 0)
                TaskPriorityAC.set_Item({ label: eventData.task.TaskPriority, value: eventData.task.TaskPriorityId });
            if (eventData.task.StatusId > 0) {
                StatsuAC.set_Item({ label: eventData.task.Status, value: eventData.task.StatusId, statusColor: eventData.task.TaskColor });
            }

            //template.find('select:.lstStartDate').val(eventData.task.StartHour);
            //template.find('select:.lstEndDate').val(eventData.task.EndHour);
        }
        else {
            if (eventData.task.StatusId > 0)
                SubTaskStatus.set_Item({ label: eventData.task.Status, value: eventData.task.StatusId });
        }

        if (eventData.activityList) {
            ActivityLookupAC.set_Items(eventData.activityList);
            var activityText = '';
            $.each(eventData.activityList, function () {
                activityText += this.label + ',';
            })
            template.find('span:.ActivityLookup').text(activityText);
        }

        if (eventData.assignmentList) {
            $.each(eventData.assignmentList, function (index, item) {

                $.each(taskConfigControl.taskAssignment, function (index, assign) {
                    if (assign.entityId == item.EntityId) {
                        assign.control.append_Item(item);

                        var ul = eventData.task.SubTask ?
                                            template.find('.task-assignment ul:.lst-' + item.EntityName) :
                                            assign.control.parents('.task-assignentity-control:first').find('.task-assignentity-list');

                        ul.append($('<li>').addClass('label-data').html(item.label + ' - ' + item.Status));

                        return false;
                    }
                });

            });
        }

        if (eventData.entityList) {
            $.each(eventData.entityList, function (index, item) {

                $.each(taskConfigControl.taskEntity, function (index, entity) {
                    if (entity.entityId == item.EntityId) {
                        entity.control.set_Item(item);
                        var ul = eventData.task.SubTask ?
                                            template.find('.task-entity ul:.lst-' + item.EntityName) :
                                            entity.control.parents('.task-assignentity-control:first').find('.task-assignentity-list');

                        ul.append($('<li>').addClass('label-data').html(item.label));
                        if (item.EntityId == systemEntities.CompanyEntityId) {
                            CompanyTaskItem = { label: item.label, value: item.value }
                        } else if (item.EntityId == systemEntities.PersonEntityId) {
                            PersonTaskItem = item;
                        } else if (item.EntityId == systemEntities.OpportunityId) {
                            OpportunityTaskItem = item;
                        } else if (item.EntityId == systemEntities.QuotationId) {
                            QuotationTaskItem = item;
                        } else if (item.EntityId == systemEntities.ContractId) {
                            ContractTaskItem = item;
                        } else if (item.EntityId == systemEntities.SPICWorkOrderId) {
                            WorkOrderTaskItem = item;
                        } else if (item.EntityId == systemEntities.ClientCareCaseId) {
                            ClientCareTaskItem = item;
                        }
                        return false;
                    }
                });

            });
            $.each(taskConfigControl.taskEntity, function (index, control) {
                if (control.entityId == systemEntities.PersonEntityId && PersonTaskItem != null) {
                    var person = { EntityId: PersonTaskItem.EntityId, EntityName: PersonTaskItem.EntityName, TaskEntityId: PersonTaskItem.TaskEntityId, label: PersonTaskItem.label, value: PersonTaskItem.value, companyName: CompanyTaskItem.label, companyId: CompanyTaskItem.value }
                    this.control.set_Args(formatString('{company_id:{0}}', CompanyTaskItem.value));
                    this.control.set_Item(person);
                } else if (control.entityId == systemEntities.OpportunityId && OpportunityTaskItem != null) {
                    var Opportunity = { EntityId: OpportunityTaskItem.EntityId, EntityName: OpportunityTaskItem.EntityName, TaskEntityId: OpportunityTaskItem.TaskEntityId, label: OpportunityTaskItem.label, value: OpportunityTaskItem.value, CompanyName: CompanyTaskItem.label, CompanyNameId: CompanyTaskItem.value }
                    this.control.set_Args(formatString('{CompanyId:{0}}', CompanyTaskItem.value));
                    this.control.set_Item(Opportunity);
                } else if (control.entityId == systemEntities.QuotationId && QuotationTaskItem != null) {
                    var Quotation = { EntityId: QuotationTaskItem.EntityId, EntityName: QuotationTaskItem.EntityName, TaskEntityId: QuotationTaskItem.TaskEntityId, label: QuotationTaskItem.label, value: QuotationTaskItem.value, CompanyName: CompanyTaskItem.label, CompanyID: CompanyTaskItem.value }
                    this.control.set_Args(formatString('{CompanyId:{0}}', CompanyTaskItem.value));
                    this.control.set_Item(Quotation);
                } else if (control.entityId == systemEntities.ContractId && ContractTaskItem != null) {

                    var Contract = { ContractId: ContractTaskItem.value, EntityId: ContractTaskItem.EntityId, EntityName: ContractTaskItem.EntityName, TaskEntityId: ContractTaskItem.TaskEntityId, label: ContractTaskItem.label, value: ContractTaskItem.value, CompanyName: CompanyTaskItem.label, CompanyID: CompanyTaskItem.value }
                    this.control.set_Args(formatString('{CompanyId:{0}}', CompanyTaskItem.value));
                    this.control.set_Item(Contract);
                } else if (control.entityId == systemEntities.SPICWorkOrderId && WorkOrderTaskItem != null) {
                    var WorkOrder = { EntityId: WorkOrderTaskItem.EntityId, EntityName: WorkOrderTaskItem.EntityName, TaskEntityId: WorkOrderTaskItem.TaskEntityId, label: WorkOrderTaskItem.label, value: WorkOrderTaskItem.value, Company: CompanyTaskItem.label, CompanyID: CompanyTaskItem.value }
                    this.control.set_Args(formatString('{ContractId:{0}}', ContractTaskItem.value));
                    this.control.set_Item(WorkOrder);
                } else if (control.entityId == systemEntities.ClientCareCaseId && ClientCareTaskItem != null) {
                    var ClientCare = { EntityId: ClientCareTaskItem.EntityId, EntityName: ClientCareTaskItem.EntityName, TaskEntityId: ClientCareTaskItem.TaskEntityId, label: ClientCareTaskItem.label, value: ClientCareTaskItem.value, Company: CompanyTaskItem.label, CompanyId: CompanyTaskItem.value }
                    this.control.set_Args(formatString('{CompanyId:{0}}', CompanyTaskItem.value));
                    this.control.set_Item(ClientCare);
                }

            });
        }

        if (eventData.discussion) {
            $.each(eventData.discussion, function (index, discussion) {
                //debugger;
                appendNewDiscussion(template, discussion, null, discussion.CreatedDate);
            });
        }

        template.find('.viewedit-display-none').hide();
        template.find('.viewedit-display-block').show();
        template.find('li:#event-attachment').show();
        template.find('li:#event-discussion').show();
        template.find('textarea:.Description').attr('readonly', 'readonly');
    }
    else {
        template.find('li:#event-attachment, li:#event-discussion').hide();
        template.find('.viewedit-display-none').show();
        template.find('.viewedit-display-block').hide();
        template.find('textarea:.Description').removeAttr('readonly');
        StatsuAC.Disabled(true)
    }



    if (eventData.task.SubTask)
        $('.sub-task').append(template);
    else {
        $('.main-task').append(template);
    }

    setEventActionControlsEvents(template, eventData, eventData, addEvent);

    //debugger;
    if (eventData.task.EnabledTask) {
        template.find('.action-edit').show();
        enableDiscussion(template);
    }
    else {
        template.find('.action-edit').hide();
        disableDiscussion(template);
    }

    if (!eventData.task.EnabledTask && !addEvent)
        template.find('.task-closed-info').show();
    else
        template.find('.task-closed-info').hide();

    if (!AttachmentsViewPermission)
        template.find('li:#event-attachment').hide();

    if (!discussionViewPermission)
        template.find('li:#event-discussion').hide();


    ShowHideEventInfo(true, !eventData.task.SubTask);
    hideLoading();

    if (addEvent) {
        //template.find('input:.StartDate').click();
        clearACArgs();
    }

    return false;
}

function appendNewDiscussion(template, discussion, allowDelete, date) {

    var createdDate = discussion.CreatedDate ? formatDate(discussion.CreatedDate) : new Date();
    var createdBy = discussion.CreatedBy + ' on ' + days[createdDate.getDay()] + ', '
     + months[createdDate.getMonth() + 1] + ' ' + createdDate.getFullYear() + ' '
      + createdDate.getDate() + ' at ' + formatAMPM(createdDate);
    //, January 6, 2013 at 3:30pm


    var li = $(TaskDiscussionViewItem);
    li.find('.discussion-detail').text(discussion.Details);
    li.find('.dis-user-name').text(createdBy);

    template.find('.save-li').before(li);
}

function setEventActionControlsEvents(template, event, eventData, addEvent) {


    template.find('#event-summery').click(function () {
        template.find('.task-assignements-divs').hide();
        template.find('.task-summery,.task-history-info,.event-actions').show();
        $('.calendar-bg-img').show();
    });

    template.find('#event-assigned-to').click(function () {
        template.find('.task-assignements-divs,.task-history-info').hide();
        template.find('.task-assignment').show();
        $('.calendar-bg-img').show();
    });

    template.find('#event-assigned-for').click(function () {
        template.find('.task-assignements-divs,.task-history-info').hide();
        template.find('.task-entity').show();
        $('.calendar-bg-img').show();
    });

    template.find('#event-discussion').click(function () {
        showLoadingWithoutBG($('#demo1'));
        template.find('.task-assignements-divs,.task-history-info,.event-actions').hide();
        template.find('.task-discussion').show();
        hideLoading();
    });

    template.find('#event-attachment').click(function () {
        template.find('.task-assignements-divs,.task-history-info,.event-actions').hide();
        template.find('.task-attachment').show();
        $('.calendar-bg-img').hide();
        if (!template.find('.document-iframe').attr('src')) {
            getTaskAttachments(currentEvent.task.TaskId, 'dtask', 'Task', systemEntities.TaskId, currentEvent.task.EnabledTask);
        }
    });


    template.find('.tabs-title').click(function () {
        template.find('.tabs-title').removeClass('tabs-title-selected');
        template.find(this).addClass('tabs-title-selected');
    });


    template.find('.ActionEdit-link').unbind('click').click(function () {

        saveMode = true;
        template.find('.viewedit-display-none').show();
        template.find('.viewedit-display-block').hide();
        template.find('.ActionCancel-link,.ActionSave-link').show();
        template.find('.ActionEdit-link,.ActionDelete-link').hide();
        template.find('textarea:.Description').removeAttr('readonly');
        return false;

    });
    template.find('.ActionCancel-link').unbind('click').click(function () {
        saveMode = false;
        template.find('.viewedit-display-none').hide();
        template.find('.viewedit-display-block').show();
        template.find('.ActionCancel-link,.ActionSave-link').hide();
        template.find('.ActionEdit-link,.ActionDelete-link').show();
        //ShowHideEventInfo(false);
        return false;

    }).hide();
    template.find('.ActionDelete-link').unbind('click').click(function () {

        saveMode = false;
        showConfirmMsg(deleteTaskTitle, areYouSure, function () { deleteTask(event.task) });

        return false;
    });

    template.find('.ActionClosed-link').unbind('click').click(function () {

        saveMode = false;
        ShowHideEventInfo(false, true);

        return false;
    });

    template.find('.ActionSave-link').unbind('click').click(function () {

        if (!event.task.SubTask) {
            validateMainTask(event);
            //   saveMainTask(event.task, TaskListViewItemTemplate, taskConfigControl);
        }
        else {
            validateSubTask(event);
            //updateSubTaskStatus(event.task.TaskAssignmentId, SubTaskStatus.get_ItemValue());
        }

        saveMode = false;
        return false;
    }).hide();


    if (addEvent) {
        template.find('.task-assign-info,.task-history-info,.task-closed-info').hide();
        template.find('.ActionSave-link').show();
        template.find('.ActionCancel-link').hide();
        //template.find('.ActionAdd-link').hide();
        template.find('.ActionEdit-link').hide();
        template.find('.ActionDelete-link').hide();
    }
    else {
        template.find('.task-assign-info,.task-history-info').show();
        template.find('.ActionSave-link').hide();
        //template.find('.ActionCancel-link').show();
        //template.find('.ActionAdd-link').show();
        template.find('.ActionEdit-link').show();
        template.find('.ActionDelete-link').show().css('display', 'inline');
    }

    if (!eventData.task.EnabledTask) {
        template.find('.ActionDelete-link,.ActionEdit-link').hide();
        disableDiscussion(template);
    }
    else {
        enableDiscussion(template);
    }
    //template.find('.ActionCancel-link').show();
}


function searchEvents(statuId) {
    //var statuId = -1;
    showLoadingWithoutBG($('.Page-container'));
    var toMe = $('.chk-to-me').is(':checked');
    var byMe = $('.chk-by-me').is(':checked');
    var date = fullCalendarObj ? fullCalendarObj.fullCalendar('getDate') : new Date();
    var month = date.getMonth() + 1;

    loadEvents(UserId, month, statuId, byMe, toMe);
}


//////////////LOAD DATA///////////////////////
///////////////////////////////////////////////



///////////////////////////////////////////////
//////////////EVENT VALIDATION/////////////////



function validateMainTask(event) {

    if (!saveForm($('.main-task').find('div:.task-summery'))) {
        return false;
    }

    showLoadingWithoutBG($('.main-task'));
    saveMainTask(event.task, TaskListViewItemTemplate, taskConfigControl);

}

function validateSubTask(event) {
    if (!saveForm($('.sub-task').find('div:.task-summery'))) {
        return false;
    }
    showLoadingWithoutBG($('.sub-task'));
    updateSubTaskStatus(event.task.TaskAssignmentId, event.task.TaskId, SubTaskStatus.get_ItemValue());
}


//////////////EVENT VALIDATION//////////////////
///////////////////////////////////////////////





///////////////////////////////////////////////
//////////////EVENT RENDER AND CLICK///////////////////////

///event : the event itself.
///jsEvent : get click coordinates and more..
///view : calendar view mode
function eventClick(event, jsEvent, view) {
    showLoadingWithoutBG($('#demo1'));
    loadEventInfo(event);
}

function dayClick(date, allDay, jsEvent, view) {
    showLoadingWithoutBG($('#demo1'));
    var event = {
        task: {
            StartDate: date.getTime().toString(),
            EndDate: date.getTime().toString(),
            id: -1,
            SubTask: false
        }
    };

    if (taskAddPermission)
        showEvent(event, true);
    else {
        hideLoading();
        showStatusMsg('You Dont Have Permissions for adding task');
    }

}

function eventResize(event) {
    showLoadingWithoutBG($('.Page-container'))
    updateTaskDate(event, false);
}


function eventDrop(event, x, c, allDay) {
    showLoadingWithoutBG($('.Page-container'))
    updateTaskDate(event, allDay);
}

function viewDisplay(view) {
    showLoadingWithoutBG($('.Page-container'))
    if (view.name == 'month') {
        //searchEvents();
        $('.fc-view-month').height($('.white-container-data').height() - 100);
        //return;
    }
    else if (view.name == 'agendaWeek') {
        $('.fc-view-agendaWeek').height($('.fc-view-month').height());
        $('.fc-agendaWeek-scroll').height($('.fc-view-agendaWeek').height() - 74);
    }
    else if (view.name == 'agendaDay') {
        $('.fc-view-agendaDay').height($('.fc-view-month').height());
        $('.fc-agendaWeek-scroll').height($('.fc-view-agendaDay').height() - 74);
    }
    hideLoading();
}




//////////////EVENT RENDER AND CLICK////////////
///////////////////////////////////////////////



function createPostRequest(url, args, successCallback, errorCallBack) {

    $.ajax({
        type: 'post',
        url: url,
        data: args,
        contentType: 'application/json; charset=utf-8;',
        dataType: 'json',
        success: function (data) {
            if (typeof successCallback == 'function') {
                var result = eval('(' + data.d + ')');
                successCallback(result);
            }
            //hideLoading();
        },
        error: function (ex) {
            if (typeof errorCallBack == 'function') {
                errorCallBack(ex);
            }
            hideLoading();
        }
    });
}

function createGetRequest(url, successCallback, errorCallBack) {
    $.ajax({
        type: 'get',
        url: url,
        success: function (data) {
            if (typeof successCallback == 'function') {
                successCallback(data);
            }
        },
        error: function (ex) {
            if (typeof errorCallBack == 'function') {
                errorCallBack(ex);
            }
        }
    });
}


function init_CommonEvents() {
    $('body').keydown(function (key) {
        if (key.keyCode == 27 || key.which == 27)
            ShowHideEventInfo(false);
    })
}


function ShowHideEventInfo(show, mainTaskDiv) {

    if (show) {
        if (mainTaskDiv)
            $('.main-task').show();
        else
            $('.sub-task').show();
    }
    else {
        clearEventInfo();
        $('.event-info').hide();
    }
    //hideLoading();
}


function DisplayStatusMsg(msg, type) {
    showStatusMsg(msg);
}

function clearEventInfo() {
    saveMode = false;
    currentEvent = null;
    TaskPriorityAC.clear();
    TaskTypeAC.clear();
    StatsuAC.clear();
    ActivityLookupAC.clear();
    if (SubTaskStatus)
        SubTaskStatus.clear();
    StatsuAC.Disabled(false);
    $('.validation,.TimeValidation').remove();
    $('.cx-control-red-border').removeClass('cx-control-red-border');

    $('.task-assignment ul, .task-entity ul').children().remove();

    $('.lstEndDate, .lstStartDate').val('--:--').attr('selected');

    $('.event-info').find('.tabs-title-selected').removeClass('tabs-title-selected');
    $('.event-info').find('#event-summery').click().find('span').addClass('tabs-title-selected')

    $('.event-info').find('input:[type="text"]').val('');
    $('.event-info').find('textarea').val('');
    $('.event-info').find('.li-discussion-item').remove();
    $('.event-info').find('span:.label-value').text('');
    $('.event-info').find('.task-assignentity-list').children().remove();
    $('.event-info').find('.lbl-history').html('');

    $.each(taskConfigControl.taskAssignment, function (index, item) {
        item.control.clear();
    });

    $.each(taskConfigControl.taskEntity, function (index, item) {
        item.control.clear();
    });
}



function fillDateList(list) {

    var liAllDay = $('<option>').html('--:--');
    list.append(liAllDay);

    for (var hour = 1; hour < 12; hour++) {
        var label = (hour >= 10) ? hour : '0' + hour;

        var liHour = $('<option>').html(label + ':00 am').attr('value', label + ':00 am');
        var liHalf = $('<option>').html(label + ':30 am').attr('value', label + ':30 am'); //05:00 am
        //        if (hour == 11) {
        //            liHalf = $('<option>').html(hour + ':30 am').attr('next', '12:00 pm');
        //        }
        list.append(liHour, liHalf);
    }

    var liHour = $('<option>').html('12:00 pm').attr('value', '12:00 pm');
    var liHalf = $('<option>').html('12:30 pm').attr('value', '12:30 pm');
    list.append(liHour, liHalf);

    for (var hour = 1; hour < 12; hour++) {
        var label = (hour >= 10) ? hour : '0' + hour;
        var liHour = $('<option>').html(label + ':00 pm').attr('value', label + ':00 pm');
        var liHalf = $('<option>').html(label + ':30 pm').attr('value', label + ':30 pm');

        //        if (hour == 12) {
        //            liHalf = $('<option>').html(hour + ':30 pm').attr('next', '1:00 am');
        //        }
        list.append(liHour, liHalf);
    }

    list.val('--:--').attr('selected');

    //    list.change(function (x,u) {
    //        debugger;
    //        if ($(this).attr('next-time'))
    //            $(this).val($(this).attr('next-time'));
    //    })

}

function getTaskAttachments(entityId, moduleKey, moduleName, parentEntityId, enable) {
    showLoadingWithoutBG($('.calendar'));
    var url = formatString("{0}Common/Documents/DocumentHTML5.aspx?key={1}&modulename={2}&id={3}&eid={4}&parententityid={5}&fromCalendar=1", systemConfig.ApplicationURL_Common, moduleKey, moduleName, entityId, entityId, parentEntityId);
    if (!enable)
        url += '&disabled=1';
    $('.document-iframe').attr('src', url);
}


function disableDiscussion(template) {
    template.find('.div-text-discussion').hide();
    //template.find('.txt-discussion').attr('readonly', 'readonly');
    //template.find('.btn-save-discussion').attr('disabled', 'disabled');
}

function enableDiscussion(template) {
    template.find('.div-text-discussion').show();
    //template.find('.txt-discussion').attr('readonly', 'readonly');
    //template.find('.btn-save-discussion').attr('disabled', 'disabled');
}

function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}

function setAddEvent() {
    if (taskAddPermission) {
        $('.addicon').show();
        $('.add-entity-button .addlabel').html('Add Task');
        $('.add-entity-button')
        .click(function () {
            ShowHideEventInfo(false, null);
            ShowHideEventInfo(true, true);
            dayClick(new Date());
        }).show();
    }

}

function DateDiff(date1, date2) {
    var Startdate = new Date(date1.replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
    var EndDate = new Date(date2.replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
    return EndDate.getTime() - Startdate.getTime();
}

//1 hour delay bug fixing
function getTaskDateFormat(strDate) {
    var values = strDate.split('-');
    return values[1] + '-' + values[0] + '-' + values[2];
}


function getCalendarPermissions(callBack) {
    var url = '../../Common/WebServices/PermissionsWebService.asmx/FindEntityPermissions';
    createPostRequest(url, formatString("{id:{0}}", systemEntities.TaskId), function (permissionsList) {
        CalenderPermissionsList = permissionsList.result;
        if (permissionsList != null) {
            $.each(permissionsList.result, function (index, item) {
                if (item.PermissionId == permissions.ViewId && item.HasPermission) taskViewPermission = true;
                if (item.PermissionId == permissions.AddId && item.HasPermission) taskAddPermission = true;
                if (item.PermissionId == permissions.DeleteId && item.HasPermission) taskDeletePermission = true;
                if (item.PermissionId == permissions.EditId && item.HasPermission) taskEditPermission = true;
                if (item.PermissionId == permissions.AddDiscussionId && item.HasPermission) discussionAddPermission = true;
                if (item.PermissionId == permissions.ViewDiscussionId && item.HasPermission) discussionViewPermission = true;
                if (item.PermissionId == permissions.ViewTaskAttachments && item.HasPermission) AttachmentsViewPermission = true;
                if (!item.HasPermission) $(item.CssClass).parent().remove();
            });
        }

        if (typeof callBack == 'function') callBack();
    });
}

function CheckTemplatePermission(template) {
    if (CalenderPermissionsList != null) {
        $.each(CalenderPermissionsList, function (index, item) {
            if (!item.HasPermission)
                $(template).find(item.CssClass).parent().remove();
        });
    }
}


function HandleCalenderQueryString(showMainTask) {
    //done by mieassar
    viewMaintaskId = getQSKey('m') ? getQSKey('m') : -1;
    viewSubTaskId = getQSKey('s') ? getQSKey('s') : -1;
    AddNewEvent = getQSKey('add') ? getQSKey('add') : 0;

    if (viewMaintaskId > 0 & showMainTask) {
        var event = {
            id: viewMaintaskId,
            TaskAssignmentId: -1,
            SubTask: false
        };
        loadEventInfo(event);
    }
    else if (viewSubTaskId > 0 & !showMainTask) {
        var event = {
            id: -1,
            TaskAssignmentId: viewSubTaskId,
            SubTask: true
        };
        loadEventInfo(event);
    }
    else if (AddNewEvent == 1) {
        ShowHideEventInfo(false, null);
        ShowHideEventInfo(true, true);
        dayClick(new Date());
    }
}



//-------------------------------------------------------------- Start AuotoComplete On Change Behavior------------------------------------------------------------------------
//-------------------------------------------------------------- Start AuotoComplete On Change Behavior------------------------------------------------------------------------

function lstOpportunity_OnChange(OpportunityId) {
    var data = formatString('{id:{0}}', Number(OpportunityId));
    var url = systemConfig.ApplicationURL_Common + systemConfig.OpportunityServiceURL + "FindLite"

    createPostRequest(url, data, function (resultObj) {
        if (resultObj != null) {
            resultObj = resultObj.result;
            var person = { label: resultObj.PersonName, value: resultObj.PersonNameId };
            var company = { label: resultObj.CompanyName, value: resultObj.CompanyNameId };
            var CompanyId = resultObj.CompanyNameId;
            $.each(taskConfigControl.taskEntity, function (index, control) {
                if (control.entityId == systemEntities.PersonEntityId) {
                    this.control.set_Args(formatString('{company_id:{0}}', resultObj.CompanyNameId));
                    resultObj.PersonNameId > 0 ? this.control.set_Item(person) : null;
                    this.control.Disabled(true);
                }
                else if (control.entityId == systemEntities.CompanyEntityId) {
                    this.control.set_Item(company);
                    this.control.Disabled(true);
                }
                else if (control.entityId == systemEntities.QuotationId) {
                    this.control.set_Args(formatString('{OpportunityId:{0}}', resultObj.OpportunityID));
                    if (EntityId != systemEntities.PersonEntityId) {
                        this.control.onChange(function () {
                            if (this.get_Item() != null)
                                getContractInfoAc("QuotationId", this.get_Item().value);
                            else ClearOpportunityAcChild(OpportunityId);
                        });
                    }
                }
                else if (control.entityId == systemEntities.ContractId) {
                    this.control.set_Args(formatString('{OpportunityId:{0}}', resultObj.OpportunityID));
                    if (EntityId != systemEntities.PersonEntityId) {
                        this.control.onChange(function () {
                            if (this.get_Item() != null)
                                getContractInfoAc("ContractId", this.get_Item().value);
                            else ClearOpportunityAcChild(OpportunityId);
                        });
                    }
                }
                else if (control.entityId == systemEntities.SPICWorkOrderId) {
                    this.control.set_Args(formatString('{OpportunityId:{0}}', resultObj.OpportunityID));
                    if (EntityId != systemEntities.PersonEntityId) {
                        this.control.onChange(function () {
                            if (this.get_Item() != null)
                                getContractInfoAc("ContractId", this.get_Item().ContractId);
                            else ClearOpportunityAcChild(OpportunityId);
                        });
                    }
                }
                else if (control.entityId == systemEntities.ClientCareCaseId) {
                    this.control.set_Args(formatString('{CompanyId:{0}}', CompanyId));
                }
            });
        }
        hideLoading();
    }, function (ex) {

    });
}

function lstQuotation_OnChange(QuotationId) {
    var data = formatString('{id:{0}}', Number(QuotationId));
    var url = systemConfig.ApplicationURL_Common + systemConfig.QuotationWebServiceURL + "FindByIdLite";
    createPostRequest(url, data, function (resultObj) {
        if (resultObj != null) {
            resultObj = resultObj.result;
            var opportunity = { label: resultObj.Opportunity, value: resultObj.OpportunityId };
            var company = { label: resultObj.CompanyName, value: resultObj.CompanyID };
            var contract = { label: resultObj.ContractReferanceNumber, value: resultObj.ContractId };

            $.each(taskConfigControl.taskEntity, function (index, control) {
                if (control.entityId == systemEntities.OpportunityId) {
                    this.control.set_Args(formatString('{OpportunityId:{0}}', resultObj.OpportunityId));
                    this.control.set_Item(opportunity);
                    this.control.Disabled(true);
                }
                else if (control.entityId == systemEntities.CompanyEntityId) {
                    this.control.set_Item(company);
                    this.control.Disabled(true);
                }
                else if (control.entityId == systemEntities.PersonEntityId) {
                    this.control.set_Args(formatString('{company_id:{0}}', resultObj.CompanyID));
                }
                else if (control.entityId == systemEntities.ContractId) {
                    this.control.set_Args(formatString('{QuotationId:{0}}', QuotationId));
                    this.control.set_Item(contract);
                    this.control.Disabled(true);
                }
                else if (control.entityId == systemEntities.SPICWorkOrderId) {
                    this.control.set_Args(formatString('{QuotationId:{0}}', resultObj.QuotationId));
                }
                else if (control.entityId == systemEntities.ClientCareCaseId) {
                    this.control.set_Args(formatString('{CompanyId:{0}}', resultObj.CompanyID));
                }
            });

        }
        hideLoading();
    }, function (ex) { });

}

function lstPerson_OnChange(personId) {
    var data = formatString('{id:{0}}', Number(personId));
    var url = systemConfig.ApplicationURL_Common + systemConfig.PersonsServiceURL + "FindPersonLite";
    createPostRequest(url, data, function (resultObj) {
        if (resultObj != null) {

            resultObj = resultObj.result;
            var company = { label: resultObj.CompanyName, value: resultObj.CompanyNameId };
            var CompanyId = resultObj.CompanyNameId;
            var CompanyName = resultObj.CompanyName;
            var PersonID = resultObj.PersonID;
            $.each(taskConfigControl.taskEntity, function (index, control) {
                if (control.entityId == systemEntities.CompanyEntityId) {
                    this.control.set_Item(company);
                    this.control.Disabled(true);
                }
                else if (control.entityId == systemEntities.OpportunityId) {


                    this.control.set_Args(formatString('{CompanyId:{0}}', CompanyId));
                    this.control.onChange(function () {
                        if (this.get_Item() != null)
                            getOpportunityInfoAcs(this.get_Item(), CompanyId, CompanyName);
                        else getOpportunityInfoAcs(null, CompanyId);
                    });

                    //                    this.control.set_Args(formatString('{CompanyId:{0},PersonID:{1}}', CompanyId, PersonID));
                    //                    this.control.onChange(function () {
                    //                        if (this.get_Item() != null)
                    //                            lstOpportunity_OnChange(this.get_Item().value);
                    //                    });
                }
                else if (control.entityId == systemEntities.SPICWorkOrderId) {
                    this.control.set_Args(formatString('{CompanyId:{0}}', CompanyId));
                    this.control.onChange(function () {
                        if (this.get_Item() != null)
                            getContractInfoAc("ContractId", this.get_Item().ContractId);
                        else ClearQuotationAcChild(CompanyId);
                    });
                }
                else if (control.entityId == systemEntities.QuotationId) {
                    this.control.set_Args(formatString('{CompanyId:{0}}', CompanyId));
                    this.control.onChange(function () {
                        if (this.get_Item() != null)
                            getContractInfoAc("QuotationId", this.get_Item().value);
                        else ClearQuotationAcChild(CompanyId);
                    });
                }
                else if (control.entityId == systemEntities.ContractId) {
                    this.control.set_Args(formatString('{CompanyId:{0}}', CompanyId));
                    this.control.onChange(function () {
                        if (this.get_Item() != null)
                            getContractInfoAc("ContractId", this.get_Item().ContractId);
                        else ClearQuotationAcChild(CompanyId);
                    });
                }
                else if (control.entityId == systemEntities.ClientCareCaseId) {
                    this.control.set_Args(formatString('{CompanyId:{0}}', CompanyId));
                }
            });
        }
        hideLoading();
    }, function (ex) { });
}


function lstContract_OnChange(ContractId) {
    var data = formatString('{id:{0}}', Number(ContractId));
    var url = systemConfig.ApplicationURL_Common + systemConfig.ContractsWebServiceURL + "FindByIdLite"
    createPostRequest(url, data, function (resultObj) {
        if (resultObj != null) {

            resultObj = resultObj.result;
            var company = { label: resultObj.CompanyName, value: resultObj.CompanyID };
            var opportunity = { label: resultObj.Opportunity, value: resultObj.OpportunityId };
            var quotation = { label: resultObj.QuotationReferenceNumber, value: resultObj.QuotationId };
            var companyId = resultObj.CompanyID;

            $.each(taskConfigControl.taskEntity, function (index, control) {
                if (control.entityId == systemEntities.OpportunityId) {
                    this.control.set_Item(opportunity);
                    this.control.Disabled(true);
                }
                else if (control.entityId == systemEntities.CompanyEntityId) {
                    this.control.set_Item(company);
                    this.control.Disabled(true);
                }
                else if (control.entityId == systemEntities.QuotationId) {
                    this.control.set_Item(quotation);
                    this.control.Disabled(true);
                }
                else if (control.entityId == systemEntities.PersonEntityId) {
                    this.control.set_Args(formatString('{company_id:{0}}', companyId));
                }
                else if (control.entityId == systemEntities.SPICWorkOrderId) {
                    this.control.set_Args(formatString('{ContractId:{0}}', ContractId));
                }
                else if (control.entityId == systemEntities.ClientCareCaseId) {
                    this.control.set_Args(formatString('{CompanyId:{0}}', companyId));
                }
            });
        }
        hideLoading();
    }, function (ex) {
    });
}

function lstWorkOrders_OnChange(WorkOrderId) {
    var data = formatString('{id:{0}}', Number(WorkOrderId));
    var url = systemConfig.ApplicationURL_Common + systemConfig.SPICWorkOrderWebServiceURL + "FindByIdLite"

    createPostRequest(url, data, function (resultObj) {
        if (resultObj != null) {
            resultObj = resultObj.result;
            var company = { label: resultObj.Company, value: resultObj.CompanyID };
            var opportunity = { label: resultObj.OpportunityDescription, value: resultObj.OpportunityId };
            var contract = { label: resultObj.Contract, value: resultObj.ContractId };
            var Quotation = { label: resultObj.QuotationRefrenceNumber, value: resultObj.QuotationId };
            var companyId = resultObj.CompanyID;
            $.each(taskConfigControl.taskEntity, function (index, control) {
                if (control.entityId == systemEntities.OpportunityId) {
                    this.control.set_Item(opportunity);
                    this.control.Disabled(true);
                }
                else if (control.entityId == systemEntities.CompanyEntityId) {
                    this.control.set_Item(company);
                    this.control.Disabled(true);
                }
                else if (control.entityId == systemEntities.ContractId) {
                    this.control.set_Item(contract);
                    this.control.Disabled(true);
                }
                else if (control.entityId == systemEntities.PersonEntityId) {
                    this.control.set_Args(formatString('{company_id:{0}}', companyId));
                }
                else if (control.entityId == systemEntities.QuotationId) {
                    this.control.set_Item(Quotation);
                    this.control.Disabled(true);
                }
                else if (control.entityId == systemEntities.ClientCareCaseId) {
                    this.control.set_Args(formatString('{CompanyId:{0}}', companyId));
                }
            });
        }
        hideLoading();
    }, function (ex) {
    });
}

function lstClientCareCases_OnChange(ClientCareCase) {
    var data = formatString('{id:{0}}', Number(ClientCareCase));
    var url = systemConfig.ApplicationURL_Common + systemConfig.ClientCareCaseWebServiceURL + "FindByIdLite"
    createPostRequest(url, data, function (resultObj) {
        if (resultObj != null) {
            resultObj = resultObj.result;
            var person = { label: resultObj.Person, value: resultObj.PersonId };
            var company = { label: resultObj.Company, value: resultObj.CompanyId };
            var CompanyId = resultObj.CompanyId;
            $.each(taskConfigControl.taskEntity, function (index, control) {
                if (control.entityId == systemEntities.PersonEntityId) {
                    this.control.set_Item(person);
                    this.control.Disabled(true);
                }
                else if (control.entityId == systemEntities.CompanyEntityId) {
                    this.control.set_Item(company);
                    this.control.Disabled(true);
                }
                else if (control.entityId == systemEntities.OpportunityId) {
                    this.control.set_Args(formatString('{CompanyId:{0}}', CompanyId));
                }
                else if (control.entityId == systemEntities.SPICWorkOrderId) {
                    this.control.set_Args(formatString('{CompanyId:{0}}', CompanyId));
                    this.control.onChange(function () {
                        if (this.get_Item() != null)
                            getContractInfoAc("ContractId", this.get_Item().ContractId);
                        else ClearQuotationAcChild(CompanyId);
                    });
                }
                else if (control.entityId == systemEntities.QuotationId) {
                    this.control.set_Args(formatString('{CompanyId:{0}}', CompanyId));
                    this.control.onChange(function () {
                        if (this.get_Item() != null)
                            getContractInfoAc("QuotationId", this.get_Item().value);
                        else ClearQuotationAcChild(CompanyId);
                    });
                }
                else if (control.entityId == systemEntities.ContractId) {
                    this.control.set_Args(formatString('{CompanyId:{0}}', CompanyId));
                    this.control.onChange(function () {
                        if (this.get_Item() != null) {
                            getContractInfoAc("ContractId", this.get_Item().value);
                        } else {
                            ClearQuotationAcChild(CompanyId);
                        }
                    });
                }
            });
        }
        hideLoading();
    }, function (ex) {
    });
}

function lstCompany_OnChange(CompanyId) {
    var data = formatString('{id:{0}}', Number(CompanyId));
    var url = systemConfig.ApplicationURL_Common + systemConfig.CompanyServiceURL + "FindCompayLite";
    createPostRequest(url, data, function (resultObj) {
        if (resultObj != null) {
            resultObj = resultObj.result;
            var CompanyId = resultObj.CompanyId;
            var CompanyName = resultObj.CompanyName;
            $.each(taskConfigControl.taskEntity, function (index, control) {
                if (control.entityId == systemEntities.PersonEntityId) {
                    //if (this.get_Item() != null)
                    this.control.set_Args(formatString('{company_id:{0}}', CompanyId));
                    this.control.onChange(function () {

                        if (this.get_Item() != null && this.get_Item().companyId)
                            CompanyOnChangeSetArgs(this.get_Item().companyId, this.get_Item().companyName, this.get_Item().value, null);
                        else
                            CompanyOnChangeSetArgs(CompanyId, CompanyName, null, null);
                    });
                }
                else if (control.entityId == systemEntities.OpportunityId) {
                    this.control.set_Args(formatString('{CompanyId:{0}}', CompanyId));
                    this.control.onChange(function () {
                        if (this.get_Item() != null)//zzzz
                            getOpportunityInfoAcs(this.get_Item(), CompanyId, CompanyName);
                        else getOpportunityInfoAcs(null, CompanyId);
                    });
                }
                else if (control.entityId == systemEntities.SPICWorkOrderId) {
                    this.control.set_Args(formatString('{CompanyId:{0}}', CompanyId));
                    this.control.onChange(function () {
                        if (this.get_Item() != null)
                            getContractInfoAc("ContractId", this.get_Item().ContractId);
                        else ClearQuotationAcChild(CompanyId);
                    });
                }
                else if (control.entityId == systemEntities.QuotationId) {
                    this.control.set_Args(formatString('{CompanyId:{0}}', CompanyId));
                    this.control.onChange(function () {
                        if (this.get_Item() != null)
                            getContractInfoAc("QuotationId", this.get_Item().value);
                        else ClearQuotationAcChild(CompanyId);
                    });
                }
                else if (control.entityId == systemEntities.ContractId) {
                    this.control.set_Args(formatString('{CompanyId:{0}}', CompanyId));
                    this.control.onChange(function () {
                        if (this.get_Item() != null)
                            getContractInfoAc("ContractId", this.get_Item().ContractId);
                        else ClearQuotationAcChild(CompanyId);
                    });
                }
                else if (control.entityId == systemEntities.ClientCareCaseId) {
                    this.control.set_Args(formatString('{CompanyId:{0}}', CompanyId));
                }
            });
        }
        hideLoading();
    }, function (ex) { });
}

function LoadClaenderOnChangeAcs() {
    ///xxxx
    $.each(taskConfigControl.taskEntity, function (index, control) {
        if (control.entityId == systemEntities.PersonEntityId) {
            this.control.onChange(function () {

                if (this.get_Item() != null)
                    CompanyOnChangeSetArgs(this.get_Item().companyId, this.get_Item().companyName, this.get_Item().value);
                else
                    CompanyOnChangeSetArgs(this.Items[0].companyId, this.Items[0].companyName, null, null);
            });
        }
        else if (control.entityId == systemEntities.CompanyEntityId) {
            this.control.onChange(function () {
                if (this.get_Item() != null)
                    CompanyOnChangeSetArgs(this.get_Item().value, this.get_Item().label);
                else
                    CompanyOnChangeSetArgs(-1, '');
            });
        }
        else if (control.entityId == systemEntities.OpportunityId) {
            this.control.onChange(function () {
                if (this.get_Item() != null) {

                    //getOpportunityInfoAcs(this.get_Item(), -1);
                    //  getContractInfoAc("OpportunityId", this.get_Item().value);
                    CompanyOnChangeSetArgs(this.get_Item().CompanyNameId, this.get_Item().CompanyName, null, this.get_Item().value);
                }
                else {
                    getOpportunityInfoAcs(null, -1);
                }
            });
        } else if (control.entityId == systemEntities.SPICWorkOrderId) {
            this.control.onChange(function () {
                if (this.get_Item() != null) {
                    getContractInfoAc("ContractId", this.get_Item().ContractId);
                    CompanyOnChangeSetArgs(this.get_Item().CompanyID, this.get_Item().Company);
                }
                else {
                    ClearQuotationAcChild(-1);
                }
            });
        }
        else if (control.entityId == systemEntities.QuotationId) {
            this.control.onChange(function () {
                // debugger;
                if (this.get_Item() != null) {
                    //zzzzz
                    getContractInfoAc("QuotationId", this.get_Item().value);
                    CompanyOnChangeSetArgs(this.get_Item().CompanyID, this.get_Item().CompanyName, -1, this.get_Item().OpportunityId ? this.get_Item().OpportunityId : -1);
                }
                else {

                    ClearQuotationAcChild(-1);
                }
            });
        }
        else if (control.entityId == systemEntities.ContractId) {
            this.control.onChange(function () {
                if (this.get_Item() != null) {

                    getContractInfoAc("ContractId", this.get_Item().ContractId);
                    CompanyOnChangeSetArgs(this.get_Item().CompanyID, this.get_Item().CompanyName, -1, this.get_Item().OpportunityId ? this.get_Item().OpportunityId : -1);
                }
                else {
                    ClearQuotationAcChild(-1);
                }
            });
        }
        else if (control.entityId == systemEntities.ClientCareCaseId) {
            this.control.onChange(function () {
                if (this.get_Item() != null)
                    CompanyOnChangeSetArgs(this.get_Item().CompanyId, this.get_Item().Company);
            });
        }
    });
}

function getContractInfoAc(Critria, CritriaId) {

    if (Critria != "" && CritriaId > 0) {
        var data = formatString('{keyword:"",page:1,resultCount:10,argsCriteria:"{' + Critria + ':{0}}"}', CritriaId);
        var url = systemConfig.ApplicationURL_Common + systemConfig.ContractsWebServiceURL + "FindAllLite"
        createPostRequest(url, data, function (resultObj) {
            if (resultObj != null && resultObj.result != null) {
                resultObj = resultObj.result[0];
                //xxxxx
                var contract = { label: resultObj.label, value: resultObj.ContractId, ContractId: resultObj.ContractId, CompanyID: resultObj.CompanyID, CompanyName: resultObj.CompanyName, OpportunityId: resultObj.OpportunityId, Opportunity: resultObj.Opportunity };
                var Quotation = { label: resultObj.Quotation, value: resultObj.QuotationId, CompanyID: resultObj.CompanyID, CompanyName: resultObj.CompanyName, OpportunityId: resultObj.OpportunityId, Opportunity: resultObj.Opportunity };
                var Opportunity = { label: resultObj.Opportunity, value: resultObj.OpportunityId, CompanyNameId: resultObj.CompanyID, CompanyName: resultObj.CompanyName, OpportunityId: resultObj.OpportunityId, Opportunity: resultObj.Opportunity };
                $.each(taskConfigControl.taskEntity, function (index, control) {
                    if (control.entityId == systemEntities.SPICWorkOrderId) {
                        this.control.set_Args(formatString('{ContractId:{0}}', resultObj.ContractId));
                    } else if (control.entityId == systemEntities.ContractId) {
                        this.control.set_Item(contract);
                    } else if (control.entityId == systemEntities.QuotationId) {
                        this.control.set_Item(Quotation);
                        this.control.set_Args(formatString('{OpportunityId:{0}}', resultObj.OpportunityId));
                    } else if (control.entityId == systemEntities.OpportunityId) {
                        this.control.set_Item(Opportunity);
                        this.control.set_Args(formatString('{OpportunityId:{0}}', resultObj.OpportunityId));
                    }
                });
            }
        });
    }
    else {
        $.each(taskConfigControl.taskEntity, function (index, control) {
            if (control.entityId == systemEntities.SPICWorkOrderId) {
                // this.control.set_Args(formatString('{ContractId:{0}}', resultObj.ContractId));
            } else if (control.entityId == systemEntities.ContractId) {
                this.control.set_Item(null);
            } else if (control.entityId == systemEntities.QuotationId) {
                this.control.set_Item(null);
            }
        });
    }
}


function getOpportunityInfoAcs(Opportunity, CompanyId, CompanyName) {

    if (Opportunity != null) {

        var Person = { label: Opportunity.PersonName, value: Opportunity.PersonNameId };
        var Company = { label: CompanyName, value: CompanyId };
        $.each(taskConfigControl.taskEntity, function (index, control) {
            if (control.entityId == systemEntities.SPICWorkOrderId) {
                this.control.set_Args(formatString('{OpportunityId:{0}}', Opportunity.OpportunityId));
            } else if (control.entityId == systemEntities.ContractId) {
                this.control.set_Args(formatString('{OpportunityId:{0}}', Opportunity.OpportunityId));
            } else if (control.entityId == systemEntities.QuotationId) {

                if (this.control.get_Item.OpportunityId != 1)
                    varx = 2;
                this.control.set_Args(formatString('{OpportunityId:{0}}', Opportunity.OpportunityId));
            } else if (control.entityId == systemEntities.PersonEntityId) {
                Opportunity.PersonNameId > 0 ? this.control.set_Item(Person) : null;
            } else if (control.entityId == systemEntities.CompanyEntityId) {
                this.control.set_Item(Company);
            }
        });
    }
    else {
        $.each(taskConfigControl.taskEntity, function (index, control) {
            if (control.entityId == systemEntities.SPICWorkOrderId) {
                this.control.set_Args(formatString('{CompanyId:{0}}', CompanyId));
            } else if (control.entityId == systemEntities.ContractId) {
                this.control.clear();
                this.control.set_Args(formatString('{CompanyId:{0}}', CompanyId));
            } else if (control.entityId == systemEntities.QuotationId) {
                this.control.clear();
                this.control.set_Args(formatString('{CompanyId:{0}}', CompanyId));
            } else if (control.entityId == systemEntities.PersonEntityId) {
                this.control.set_Args(formatString('{CompanyId:{0}}', CompanyId));
            }
        });
    }
}
function CompanyOnChangeSetArgs(CompanyId, CompanyName, personId, OpportunityId) {

    var opportunityId = OpportunityId > 0 ? OpportunityId : -1;
    var Company = { label: CompanyName, value: CompanyId };
    $.each(taskConfigControl.taskEntity, function (index, control) {
        if (control.entityId == systemEntities.OpportunityId) {

            if (this.control.get_Item() != null && CompanyId != this.control.get_Item().CompanyNameId)
                this.control.clear();
            if (personId)
                this.control.set_Args(formatString('{CompanyId:{0},PersonID:{1}}', CompanyId, personId));
            else
                this.control.set_Args(formatString('{CompanyId:{0}}', CompanyId));
        }
        else if (control.entityId == systemEntities.ContractId) {

            if (this.control.get_Item() != null && CompanyId != this.control.get_Item().CompanyID)
                this.control.clear();

            if (this.control.get_Item() != null && this.control.get_Item().OpportunityId && OpportunityId != this.control.get_Item().OpportunityId) {
                this.control.clear();
                this.control.set_Args(formatString('{CompanyId:{0},OpportunityId:{1}}', CompanyId, OpportunityId));
            }
            this.control.set_Args(formatString('{CompanyId:{0},OpportunityId:{1}}', CompanyId, opportunityId));
        }
        else if (control.entityId == systemEntities.PersonEntityId) {
            //if (this.control.get_Item() != null && CompanyId != this.control.get_Item().companyId)
            //this.control.clear();
            this.control.set_Args(formatString('{company_id:{0}}', CompanyId));
        }
        else if (control.entityId == systemEntities.ClientCareCaseId) {
            if (this.control.get_Item() != null && CompanyId != this.control.get_Item().CompanyId)
                this.control.clear();
            this.control.set_Args(formatString('{CompanyId:{0}}', CompanyId));
        }
        else if (control.entityId == systemEntities.QuotationId) {

            if (this.control.get_Item() != null && this.control.get_Item().CompanyID && CompanyId != this.control.get_Item().CompanyID)
                this.control.clear();

            if (this.control.get_Item() != null && OpportunityId != this.control.get_Item().OpportunityId) {
                this.control.clear();
                this.control.set_Args(formatString('{CompanyId:{0},OpportunityId:{1}}', CompanyId, OpportunityId));
            }

            if (OpportunityId)
                this.control.set_Args(formatString('{CompanyId:{0},OpportunityId:{1}}', CompanyId, OpportunityId));
            else
                this.control.set_Args(formatString('{CompanyId:{0}}', CompanyId));
        }
        else if (control.entityId == systemEntities.SPICWorkOrderId) {
            if (this.control.get_Item() != null && CompanyId != this.control.get_Item().CompanyID)
                this.control.clear();
            this.control.set_Args(formatString('{CompanyId:{0},OpportunityId:{1}}', CompanyId, opportunityId));
        }
        else if (control.entityId == systemEntities.CompanyEntityId) {

            if (CompanyName != '' && CompanyName != null)
                this.control.set_Item(Company);
            else
                this.control.clear();

        }

    });
}



//-------------Clear ACs

function ClearQuotationAcChild(CompanyId) {
    $.each(taskConfigControl.taskEntity, function (index, control) {
        if (control.entityId == systemEntities.OpportunityId) {
            this.control.clear();
            this.control.set_Args(formatString('{CompanyId:{0}}', CompanyId));
        }
        else if (control.entityId == systemEntities.ContractId) {
            this.control.clear();
            this.control.set_Args(formatString('{CompanyId:{0}}', CompanyId));
        }
        else if (control.entityId == systemEntities.PersonEntityId) {
            //this.control.clear();
            this.control.set_Args(formatString('{company_id:{0}}', CompanyId));
        }
        else if (control.entityId == systemEntities.SPICWorkOrderId) {
            this.control.clear();
            this.control.set_Args(formatString('{CompanyId:{0}}', CompanyId));
        }
        else if (control.entityId == systemEntities.QuotationId) {
            this.control.clear();
        }

    });
    //lstCompany_OnChange(CompanyId);
}

function ClearOpportunityAcChild(OpportunityId) {
    $.each(taskConfigControl.taskEntity, function (index, control) {
        if (control.entityId == systemEntities.ContractId) {
            this.control.clear();
            this.control.set_Args(formatString('{OpportunityId:{0}}', OpportunityId));
        }
        else if (control.entityId == systemEntities.SPICWorkOrderId) {
            this.control.clear();
            this.control.set_Args(formatString('{OpportunityId:{0}}', OpportunityId));
        }
        else if (control.entityId == systemEntities.QuotationId) {
            this.control.clear();
            this.control.set_Args(formatString('{OpportunityId:{0}}', OpportunityId));
        }

    });
}

//-------------------------------------------------------------- End AuotoComplete On Change Behavior------------------------------------------------------------------------
//-------------------------------------------------------------- End AuotoComplete On Change Behavior------------------------------------------------------------------------


function SendAttachmentNotifications(documentId) {

    var data = formatString('{taskId:{0},documentId:{1},UserId:{2}}', currentEvent.task.TaskId, documentId, 1);
    var url = systemConfig.ApplicationURL_Common + systemConfig.CalendarWebServiceURL + "SendAttachmentNotification"
    createPostRequest(url, data, function (data) { });
}

function clearACArgs() {
    $.each(taskConfigControl.taskEntity, function (index, control) {
        if (control.entityId == systemEntities.OpportunityId) {
            this.control.set_Args(null);
        }
        else if (control.entityId == systemEntities.ContractId) {
            this.control.set_Args(null);
        }
        else if (control.entityId == systemEntities.PersonEntityId) {
            this.control.set_Args(null);
        }
        else if (control.entityId == systemEntities.ClientCareCaseId) {
            this.control.set_Args(null);
        }
        else if (control.entityId == systemEntities.QuotationId) {
            this.control.set_Args(null);
        }
        else if (control.entityId == systemEntities.SPICWorkOrderId) {
            this.control.set_Args(null);
        }
        else if (control.entityId == systemEntities.CompanyEntityId) {
            this.control.set_Args(null);
        }

    });

}
