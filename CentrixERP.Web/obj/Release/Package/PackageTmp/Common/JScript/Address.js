
function getAddresses(ParentId, mKey) {
    $('.address-tab').children().remove();
    var data = formatString('{parentId:{0}, mKey:"{1}"}', ParentId, mKey);
    $.ajax({
        type: "POST",
        url: systemConfig.AddressServiceURL + "FindAll",
        contentType: "application/json; charset=utf-8",
        data: data,
        dataType: "json",
        success: function (data) {
            var addressObj = eval("(" + data.d + ")");
            addAddressTab(null, ParentId, mKey, null);
            if (addressObj.result != null) {

                $('.address-tab').parent().find('.DragDropBackground').remove();

                $.each(addressObj.result, function (index, item) {
                    addAddressTab(item, ParentId, mKey);
                });

            }
            else
                $('.TabbedPanelsContentGroup').css('min-height', $('.Result-info-container').height() - 31);
            attachTabEvents();
        },
        error: function (e) {
        }
    });
}

function addAddressTab(item, ParentId, mKey) {

    var id = (item != null) ? item.Id : 0;
    var isDefault = (item != null) ? item.Isdefault : false;
    var template = $(AddressTemplate);
    var CountryNameAC = template.find('select:.CountryName').AutoComplete({ serviceURL: systemConfig.CountryServiceURL + "FindAll" });

    if (item != null)
        template = mapEntityInfoToControls(item, template, [{ controlName: 'CountryNameAC', control: CountryNameAC}]);
    else {
        template.hide();
        $('.add-new-address').click(function () {

            template.find('.itemheader').click();
            template.find('.viewedit-display-none').show();
            template.find('.viewedit-display-block').hide();
            template.find('input').val('');
            CountryNameAC.clear();
            template.find('.edit-tab-lnk').remove();
            template.find('.save-tab-lnk').show();
            template.find('.cancel-tab-lnk').show();
            template.find('.delete-tab-lnk').hide();
            template.find('.validation').remove();
            //template.show();
            if (template.is(':visible')) {
                template.hide();
            }
            else {
                template.show();
            }
            return false;
        });
    }

    if (isDefault)
        template.find('.default-icon').addClass('default-iconVisited')

    template.find('.save-tab-lnk').on('click', function () {
        saveAddress(id, ParentId, mKey, CountryNameAC, template, isDefault);
        template.find('.edit-tab-lnk').show();
        return false;
    });


    template.find('.delete-tab-lnk').click(function () {
        deleteAddress(id, mKey, ParentId)
        return false;
    })
    template.find('.edit-tab-lnk').click(function () {
        template.find('.edit-tab-lnk').hide();
        template.find('.save-tab-lnk').show();
        template.find('.cancel-tab-lnk').show();

        if (!template.hasClass('Tab-listSelected'))
            template.find('.itemheader').click();

        template.find('.viewedit-display-none').show();
        template.find('.viewedit-display-block').hide();
        return false;
    })

    template.find('a.default-icon').click(function () {
        if (!$(this).hasClass('default-iconVisited')) {
            SetDefault(id, ParentId, mKey, systemConfig.AddressServiceURL + "SetAddressDefualt", true, UserId);
            $('.default-icon').removeClass('default-iconVisited');
            $(this).addClass('default-iconVisited');
        }
        return false;
    });

    template.find('.cancel-tab-lnk').on('click', function () {
        template.find('.itemheader').click();
        return false;
    });


    $('.address-tab').append(template);

}

function saveAddress(id, parentId, mKey, countryNameAC, template, isDefault) {

    if (!saveForm(template, countryNameAC)) {
        return false;
    }


    var countryId = countryNameAC.get_Item() != null ? countryNameAC.get_Item().value : -1;
    var buildingNo = template.find('input:.BuildingNumber').val();
    var streetAddress = template.find('input:.StreetAddress').val();
    var city = template.find('input:.City').val();
    var floor = template.find('input:.Floor').val();
    var zipCode = template.find('input:.ZipCode').val();
    var officeNo = template.find('input:.OfficeNumber').val();
    var county = template.find('input:.County').val();


    var data = '{Id:{0}, moduleId:{1}, mKey:"{2}", CountryId:{3}, City:"{4}", StreetAddress:"{5}", ZipCode:"{6}", BuildingNumber:"{7}", floor:"{8}", officeNum:"{9}", county:"{10}", IsDefault:{11}, userId:{12}}';
    data = formatString(data, id, parentId, mKey, countryId, city, streetAddress, zipCode, buildingNo, floor, officeNo, county, isDefault, UserId);

    $.ajax({
        type: "POST",
        url: systemConfig.AddressServiceURL + "AddEditAddress",
        contentType: "application/json; charset=utf-8",
        data: data,
        dataType: "json",
        success: function (data) {
            getAddresses(parentId, mKey);
        },
        error: function (e) {
        }
    });
}

function deleteAddress(id, mKey, parentId) {
    $.ajax({
        type: "POST",
        url: systemConfig.AddressServiceURL + "DeleteAddress",
        contentType: "application/json; charset=utf-8",
        data: formatString('{id:{0}, mKey:"{1}"}', id, mKey),
        dataType: "json",
        success: function (data) {
            getAddresses(parentId, mKey);
        },
        error: function (e) {
        }
    });
}