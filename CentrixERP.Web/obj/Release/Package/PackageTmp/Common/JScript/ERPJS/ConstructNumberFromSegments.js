﻿var ConstructNumberFromSegmentsTemplate;
var ConstructNumberFromSegmentsDetailsTemplate;
var serviceUrl = '';
var exactLength = false;
var delimiterValue = '';
var structureCodeId = null;

function LoadConstructNumberFromSegmentsTemplates() {
    GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.ConstructNumberFromSegmentsTemplate, null, 'ConstructNumberFromSegmentsTemplate');
    GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.ConstructNumberFromSegmentsDetailsTemplate, null, 'ConstructNumberFromSegmentsDetailsTemplate');
}

function InitializeConstructNumberFromSegmentsForm(segmentsList, segmentCodesServiceUrl, isExactLength, delimiter, structureCodeObj, saveCallBack, cancelCallBack) {

    serviceUrl = segmentCodesServiceUrl;
    exactLength = isExactLength;
    delimiterValue = delimiter == null ? '' : delimiter;
    if (structureCodeObj)
    { structureCodeId = structureCodeObj.Id; }

    $('.black_overlay').addClass('segments-blackoverlay');
    $('.construct-number').html('');
    var template = ConstructNumberFromSegmentsTemplate.clone();

    $('.construct-number').append(template);
    $('.construct-number').show();

    var noOfVarianceSegments = structureCodeObj ? (structureCodeObj.HasVarianceSegments ? structureCodeObj.NoOfVarianceSegments : null) : null;
    if (noOfVarianceSegments) {
        for (i = 0; i < noOfVarianceSegments; i++) {
            segmentsList.push({});
        }
    }

    loadConstructNumberFromSegmentsGrid(segmentsList, template);

    UpdateConstructedNumberValue();

    $(template).find('.ActionSave-link').die('click').unbind().click(function () {

        if (!saveForm($('.construct-number'))) {
            return false;
        }
        if (!ValidateSegments())
        { return false; }

        var returnedData = GetSegmentsValues();
        var segmentAndSegmentCodesPairs = GetSegmentAndSegmentCodeValues();
        if (saveCallBack) {
            var result = saveCallBack(returnedData, segmentAndSegmentCodesPairs);
        }
        if (result != false) {
            ClearFormAndResetVariables();
        }
        return false;
    });
    $(template).find('.ActionCancel-link').die('click').unbind().click(function () {
        if (cancelCallBack) {
            cancelCallBack();
        }
        ClearFormAndResetVariables();
        return false;
    });
}
function GetSegmentsValues() {
    var segmentsValues = [];
    $.each(SegmentedNumberGrid.get(), function (idx, item) {

        if (item.template.find('.BaseSegment').is(':visible')) {
            segmentsValues.push(item.BaseSegmentCode);
        }
        else {
            segmentsValues.push(item.SegmentCodeItem.SegmentCode);
        }
    });
    return segmentsValues;
}
function GetSegmentAndSegmentCodeValues() {
    var segmentAndSegmentCodeValues = [];
    $.each(SegmentedNumberGrid.get(), function (idx, item) {

        var segmentId = item.template.find('.SegmentId').text();
        var segmentCodeId = item.SegmentCodeId;

        if (item.template.find('.BaseSegment').is(':visible')) {
            //segmentAndSegmentCodeValues.push(segmentId + '-' + item.BaseSegmentCode);
        }
        else {
            segmentAndSegmentCodeValues.push(segmentId + '-' + segmentCodeId);
        }
    });
    return segmentAndSegmentCodeValues;
}
function loadConstructNumberFromSegmentsGrid(datasource, passedTemplate) {
    var control = [
     {
         controlId: 'Segment',
         required: false,
         type: 'label'
     },
     {
         controlId: 'SegmentList',
         required: false,
         type: 'ac',
         service: systemConfig.ApplicationURL + systemConfig.IcSegmentServiceURL + "FindAllLite",
         onChange: function (item, row, controls, rowIndex) {
             if (item) {
                 row.find('.Description').text(item.Description);
                 var refSegmentCodeId = null;
                 var matchingItems = $.grep(SegmentedNumberGrid.get(), function (e) { return (e.template.find('.SegmentId').text() == item.ReferenceSegmentId); });
                 if (matchingItems && matchingItems.length > 0) {
                     refSegmentCodeId = matchingItems[0].SegmentCodeId;
                 }
                 if (refSegmentCodeId != null) {
                     if (refSegmentCodeId <= 0)
                     { refSegmentCodeId = null; }
                 }
                 controls[3].control.set_Args(formatString('{SegmentId:{0},ReferenceSegmentId:{1},ReferenceSegmentCodeId:{2}}', item.SegmentId, item.ReferenceSegmentId > 0 ? item.ReferenceSegmentId : null, refSegmentCodeId));
                 controls[3].control.clear();
                 row.find('.SegmentId').text(item.SegmentId);
                 row.find('.SegmentLength').text(item.SegmentLength);
                 row.find('.ReferenceSegmentId').text(item.ReferenceSegmentId);
             }
             else {
                 row.find('.Description').text('');
                 controls[3].control.set_Args();
                 row.find('.SegmentId').text('');
                 row.find('.SegmentLength').text('');
                 row.find('.ReferenceSegmentId').text('');
             }
             controls[3].control.clear();
             controls[3].control.onChange();
         }
     },
     {
         controlId: 'SegmentDescription',
         required: false,
         type: 'label'
     },
     {
         controlId: 'SegmentCode',
         required: false,
         type: 'ac',
         service: serviceUrl, //systemConfig.ApplicationURL + systemConfig.IcSegmentCodeServiceURL + "FindAllLite",
         onChange: function (item, row, controls, rowIndex) {
             if (item) {
                 row.find('.SegmentCodeDescription').text(item.SegmentCodeDescription);

                 $.each(SegmentedNumberGrid.get(), function (idx, gridItem) {
                     var referenceSegId = gridItem.template.find('.ReferenceSegmentId').text();
                     if (referenceSegId && referenceSegId == item.SegmentId) {
                         gridItem.controls[3].control.set_Args(formatString('{SegmentId:{0},ReferenceSegmentId:{1},ReferenceSegmentCodeId:{2}}', gridItem.template.find('.SegmentId').text(), referenceSegId, item.SegmentCodeId));
                     }
                 });
             }
             else {
                 row.find('.SegmentCodeDescription').text('');

                 $.each(SegmentedNumberGrid.get(), function (idx, gridItem) {
                     var referenceSegId = gridItem.template.find('.ReferenceSegmentId').text();
                     if (referenceSegId && referenceSegId == row.find('.SegmentId').text()) {
                         gridItem.controls[3].control.set_Args(formatString('{SegmentId:{0}}', gridItem.template.find('.SegmentId').text()));
                     }
                 });
             }
             UpdateConstructedNumberValue();
         }
     },
     {
         controlId: 'BaseSegmentCode',
         required: false,
         type: 'text'
     },
     {
         controlId: 'SegmentCodeDescription',
         required: false,
         type: 'label'
     }
];
    var rowTemplate = ConstructNumberFromSegmentsDetailsTemplate.clone();

    SegmentedNumberGrid = $('#segments-details-items-grid').Grid({
        headerTeamplte: $('#segments-details-h-template').html(),
        rowTemplate: rowTemplate, //
        rowControls: control,
        dataSource: datasource.length > 0 ? datasource : null,
        selector: '.',
        appendBefore: 'tr:last',
        autoInsertEmptyRow: false,
        preventAdd: true,
        preventDelete: true,
        rowAdded: function (row, controls, dataRow, grid) {
            if (dataRow) {
                if (dataRow.IsBaseSegment) {
                    row.find('.BaseSegment').show();
                    row.find('.NonBaseSegment').hide();
                    controls[4].control.attr('maxlength', dataRow.SegmentLength);
                    controls[4].control.addClass('required');
                    row.find('.SegmentCodeDescription').text(getResource('N/A'));
                    row.find('.BaseSegmentCode').blur(function () { UpdateConstructedNumberValue(); });
                }
                else {
                    row.find('.BaseSegment').hide();
                    row.find('.NonBaseSegment').show();
                    controls[3].control.set_Required(true);
                }
                if (dataRow.SegmentId) {

                    row.find('.SegmentId').text(dataRow.SegmentId);
                    row.find('.SegmentLength').text(dataRow.SegmentLength);
                    row.find('.ReferenceSegmentId').text(dataRow.ReferenceSegmentId);

                    controls[3].control.set_Args(formatString('{SegmentId:{0}}', dataRow.SegmentId));
                    row.find('.varianceSegment').hide();
                    controls[1].control.set_Required(false);
                }
                else {
                    row.find('.SegmentName').hide();
                    controls[1].control.set_Required(true);
                }
            }
            controls[1].control.set_Args(formatString('{ConstructionItemStructureId:{0}}', structureCodeId));
        }
    });
}

function ClearFormAndResetVariables() {
    $('.construct-number').hide();
    $('.construct-number').html('');
    $('.black_overlay').removeClass('segments-blackoverlay');
    serviceUrl = '';
    delimiterValue = '';
    exactLength = false;
    structureCodeId = null;

    if (!AddMode) {
        $(".black_overlay").hide();
    }
}


function ValidateSegments() {
    var validSegments = true;
    var formTemplate = $('.construct-number');

    $.each(formTemplate.find('.BaseSegment:visible'), function (idx, item) {
        if ($(this).find('input.BaseSegmentCode').val()) {
            if (exactLength) {
                if ($(this).find('input.BaseSegmentCode').val().length != $(this).find('input.BaseSegmentCode').attr('maxLength')) {
                    validSegments = false;
                    addPopupMessage($(this).find('input.BaseSegmentCode'), getResource("LengthMustBe") + $(this).find('input.BaseSegmentCode').attr('maxLength'));
                }
            } else {
                if ($(this).find('input.BaseSegmentCode').val().length < 1) {
                    validSegments = false;
                }
            }
        }
    });

    $.each(SegmentedNumberGrid.get(), function (idx, item) {
        if (item.template.find('.varianceSegment').is(':visible')) {
            $.each(SegmentedNumberGrid.get(), function (idx2, item2) {
                if (idx != idx2) {
                    if (item.template.find('.varianceSegment').is(':visible')) {
                        if (item.SegmentListId == item2.SegmentListId) {
                            validSegments = false;
                            addPopupMessage(item.template.find('select:.SegmentList'), getResource('AlreadyExists'));
                        }
                    }
                }
            });
        }
    });

    return validSegments;
}

function UpdateConstructedNumberValue() {
    var result = '';
    var segmentCode = '';
    var segmentLength;
    $.each(SegmentedNumberGrid.get(), function (idx, item) {
        segmentCode = '';
        segmentLength = Number(item.template.find('.SegmentLength').text());
        if (result != '') { result += delimiterValue; }

        if (item.BaseSegmentCode == '' && item.SegmentCodeId == -1) {
            result += padRight(segmentCode, segmentLength, "X");
        }
        else if (item.BaseSegmentCode != '') {
            result += item.BaseSegmentCode;
        }
        else if (item.SegmentCodeId != -1) {
            result += item.SegmentCodeItem.SegmentCode;
        }
    });

    $('.construct-number').find('.ConstructedNumber').text(result);
}

function padRight(str, max, char) {
    str = str.toString();
    return str.length < max ? padRight(str + char, max, char) : str;
}