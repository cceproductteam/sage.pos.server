var DataDictionaryAR = new Array();

////////////////////////////////////////Linkes//////////////////////////////////////////////////////////////////////////
DataDictionaryAR['IsDefult'] = 'Is Default';
DataDictionaryAR['Numbering'] = 'ترقيم';
DataDictionaryAR["LinkLang"] = 'EN';
DataDictionaryAR["Edit"] = 'تعديل';
DataDictionaryAR["Save"] = 'حفظ';
DataDictionaryAR["Cancel"] = 'الغاء';
DataDictionaryAR["Delete"] = 'حذف';
DataDictionaryAR["Clear"] = 'مسح';
DataDictionaryAR["Reassign"] = 'اعادة تعيين';
DataDictionaryAR["Close"] = 'اغلاق';
DataDictionaryAR["Add"] = 'اضافه';
DataDictionaryAR["FaildToSave"] = 'فشل في الحفظ';
DataDictionaryAR["SMNoDataFound"] = 'لا يوجد بيانات';
DataDictionaryAR["RoleNotFound"] = 'لا يوجد دور';
DataDictionaryAR["savedsuccessfully"] = 'تم الحفظ بنجاح';
DataDictionaryAR["deletesuccessfully"] = 'تم الحذف بنجاح';
DataDictionaryAR["Areyousure"] = 'هل انت متأكد ؟';
DataDictionaryAR["ConfirmMsgCancle"] = 'الغاء';
DataDictionaryAR["ConfirmMsgConfirm"] = 'تأكيد';
DataDictionaryAR["ThisRecordIsLockedby"] = 'هذا السجل مستخدم من قبل ';
DataDictionaryAR["at"] = ',  في ';
DataDictionaryAR["systemError"] = 'خطأ في النظام';
DataDictionaryAR["RecordDeleted"] = 'سجل محذوف';
DataDictionaryAR["RecordHasBeenDeleted"] = 'تم حذف هذا السجل، سيتم تحديث النافذة بعد النقر فوق موافق.';
DataDictionaryAR["RecordChanged"] = 'سجل معدل';
DataDictionaryAR["RecordHasBeenChanged"] = 'تم تعديل هذا السجل، سيتم تحديث النافذة بعد النقر فوق موافق.';
DataDictionaryAR["DeleteSuperUserMsg"] = 'لا يمكن حذف المستخدم المميز';

DataDictionaryAR["required"] = 'مطلوب';
DataDictionaryAR["numeric"] = 'رقم';
DataDictionaryAR["integer"] = 'عدد صحيح';
DataDictionaryAR["lengthMustBe"] = 'الطول يجب ان يكون';
DataDictionaryAR["invalidEmail"] = 'البريد الالكتروني غير صحيح';
DataDictionaryAR["HistoryUpdated"] = 'قائمة التحديثات';
DataDictionaryAR["LastUpdatedDate"] = 'تاريخ اخر تعديل';
DataDictionaryAR["LastUpdatedBy"] = 'اخر تعديل من';
DataDictionaryAR["True"] = 'نعم';
DataDictionaryAR["False"] = 'لا';
DataDictionaryAR["MinLength"] = 'الحد الادنى هو 3';
DataDictionaryAR["MaxLength"] = 'الحد الاعلى هو 100';
DataDictionaryAR["Ok"] = 'موافق';
DataDictionaryAR['MoreOptions'] = '▼خيارات اخرى';
DataDictionaryAR['HideOptions'] = 'اخفاء الخيارات ▲';
DataDictionaryAR['Search...'] = 'بحث...';
DataDictionaryAR["Language"] = 'English';
DataDictionaryAR["vldDeleteActiveFailed"] = 'فشل في حذف العنصر الفعال';
DataDictionaryAR["vldDontHavePermission"] = 'ليس لك صلاحية لهذه الصفحة';
DataDictionaryAR["vldLoginSessionExpired"] = 'انتهت جلسة الدخول';
DataDictionaryAR["vldDeleteLinkedItem"] = 'خطأ في الحذف , العنصر مرتبط بعناصر اخرى : <br>';
DataDictionaryAR["vldDeactivateLinkedItem"] = 'خطأ في الالغاء , العنصر مرتبط بعناصر اخرى : <br>';
DataDictionaryAR["OptionalFieldEdit"] = "اختيار";
DataDictionaryAR["CashFlowStatements"] = "بيانات النقد";
DataDictionaryAR["FromPeriod"] = "من الفترة";
DataDictionaryAR["ToPeriod"] = "الى الفترة";
DataDictionaryAR["NoDetails"] = "لايوجد تفاصيل مدخلة";
DataDictionaryAR["vldChangeHeaderTaxClass"] = "لقد قمت بتغيير فئة الضريبة <br>. الرجاء التأكد من فئة الضريبة على كل سطر في تفاصيل الفاتورة.";
DataDictionaryAR["ContactPerson"] = 'الشخص الذي يمكن الاتصال به';
DataDictionaryAR["ContactPersonMob"] = 'رقم موبايل الشخص الذي يمكن الاتصال به';
DataDictionaryAR["UploadItemsLog"] = 'UploadItemsLog';
DataDictionaryAR["AddUploadItemsLog"] = 'اضافة UploadItemsLog';
DataDictionaryAR['UploadItemsLogDetails'] = ' تفاصيل UploadItemsLog';
DataDictionaryAR['UploadItemsLog'] = 'Upload Items Log';
DataDictionaryAR['Type'] = 'Type';
DataDictionaryAR['Note'] = 'Note';
DataDictionaryAR['Status'] = 'Status';
DataDictionaryAR['Errors'] = 'Errors';
DataDictionaryAR['LogFileName'] = 'Log File Name';
DataDictionaryAR["AlreadyExist"] = ' موجود مسبقا ';
DataDictionaryAR["File"] = "الملف";
DataDictionaryAR["UploadItems"] = "تحميل العناصر";
DataDictionaryAR['vldInvalidFile'] = 'خطأ في الملف ,, الرجاء تحميل ملف صحيح';
DataDictionaryAR["Failed"] = "فشلت";
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Setup Page section//////////////////////////////////////////////////////////////
DataDictionaryAR["SPSetup"] = 'الاعدادات';
DataDictionaryAR["SPUserManagement"] = 'إدارة المستخدم';
DataDictionaryAR["SPUsers"] = 'المستخدمون';
DataDictionaryAR["SPRols"] = 'الأدوار';
DataDictionaryAR["SPTeams"] = 'الفرق';
DataDictionaryAR["SPPermissions"] = 'الصلاحيات';
DataDictionaryAR["SPDataAdminstration"] = 'إدارة البيانات';
DataDictionaryAR["SPLovs"] = 'قائمة القيم';
DataDictionaryAR["SPEntitylock"] = 'قفل الكيان';
DataDictionaryAR["SPInventory"] = 'المخزون';
DataDictionaryAR["SPSearchInventory"] = 'البحث في المخزون';
DataDictionaryAR["SPStockUpdate"] = 'تحديثات المخزون';
DataDictionaryAR["SPInventory"] = 'المخزون';
DataDictionaryAR["SReturnedItems"] = 'المواد المرجعه';

DataDictionaryAR["SPEmailConfiguration"] = 'اعدادات البريد الالكتروني';
DataDictionaryAR["SPSMSConfiguration"] = 'اعدادات الرسائل';
DataDictionaryAR["SPTemplates"] = 'النماذج';
DataDictionaryAR["SPSendMassEmails"] = 'إرسال رسائل البريد الإلكتروني ';
DataDictionaryAR["SPGroups"] = 'المجموعات';
DataDictionaryAR["Groups&Marketing"] = 'المجموعات والتسويق';
DataDictionaryAR["SPKit"] = 'ادوات';
DataDictionaryAR["SPCategory"] = 'الفئة';

DataDictionaryAR['FullName'] = 'الإسم كامل';
DataDictionaryAR['Users'] = 'المستخدمين';
DataDictionaryAR['ShowAllUser'] = 'المستخدمين';
DataDictionaryAR["AssetRegister"] = 'سجل الاصول';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Page Titels///////////////////////////////////////////////////////////////////////
DataDictionaryAR["QuickSearch"] = 'بحث سريع';
DataDictionaryAR["CompanyPageTitle"] = 'شركه';
DataDictionaryAR["AddCompany"] = 'اضافة شركه';
DataDictionaryAR["Total"] = 'مجموع   ';
DataDictionaryAR["of"] = '  من  ';
DataDictionaryAR["Showmore"] = 'عرض المزيد';
DataDictionaryAR["loading"] = 'جاري التحميل...';
DataDictionaryAR["SlideDown"] = 'عرض المزيد';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Tabs Add Link//////////////////////////////////////////////////////////////////
DataDictionaryAR["AddPerson"] = 'اضافة شخص';
DataDictionaryAR["NoDataFound"] = 'لايوجد بيانات';
DataDictionaryAR["AddnewCommunication"] = 'اضافة تواصل ';
DataDictionaryAR["AddSales"] = 'اضافة مبيعات';
DataDictionaryAR["Addnewaddress"] = 'اضافة عنوان';
DataDictionaryAR["newphone"] = 'اضافة هاتف';
DataDictionaryAR["Addnewnote"] = 'اضافة ملاحظه';
DataDictionaryAR["Addnewemail"] = 'اضافة بريد الكتروني';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Tabs Names//////////////////////////////////////////////////////////////////////
DataDictionaryAR["Summary"] = 'ملخص';
DataDictionaryAR["Persons"] = 'الاشخاص';
DataDictionaryAR["Communications"] = 'التواصل';
DataDictionaryAR["Sales"] = 'المبيعات';
DataDictionaryAR["TradingTerms"] = 'شروط التداول';
DataDictionaryAR["Addresses"] = 'العنوان';
DataDictionaryAR["Phones"] = 'الهاتف';
DataDictionaryAR["Emails"] = 'البريد الإلكتروني';
DataDictionaryAR["Attachments"] = 'المرفقات';
DataDictionaryAR["Notes"] = 'ملاحظات';
DataDictionaryAR["More"] = 'المزيد';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Email Tab///////////////////////////////////////////////////////////////////////
DataDictionaryAR["EmailAddress"] = 'عنوان البريد الالكتروني';
DataDictionaryAR["Type"] = 'النوع';
DataDictionaryAR["Business"] = 'عمل';
DataDictionaryAR["Personal"] = 'شخصي';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Adress Tab///////////////////////////////////////////////////////////////////////
DataDictionaryAR["Country"] = 'البلد';
DataDictionaryAR["City"] = 'المدينه';
DataDictionaryAR["StreetAddress"] = 'عنوان الشارع';
DataDictionaryAR["NearBy"] = 'بالقرب من';
DataDictionaryAR["Place"] = 'المكان';
DataDictionaryAR["BuildingNumber"] = 'رقم العمارة';
DataDictionaryAR["POBox"] = 'صندوق البريد';
DataDictionaryAR["ZipCode"] = ' العنوان البريدي';
DataDictionaryAR["Region"] = 'المنطقة';
DataDictionaryAR["Area"] = 'الحي';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Note Tab///////////////////////////////////////////////////////////////////////
DataDictionaryAR["CreatedDate"] = 'تاريخ الإنشاء';
DataDictionaryAR["CreatedBy"] = 'أنشأت بواسطة';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Attachment Tab///////////////////////////////////////////////////////////////////////
DataDictionaryAR["filesfromyourcomputer"] = 'اضف ملفات من جهاز الكمبيوتر';
DataDictionaryAR["Dropfileshere"] = 'إسحب الملفات هنا';
DataDictionaryAR["Addfolder"] = 'اضافة مجلد';
DataDictionaryAR["DocumentRename"] = 'اعادة تسميه';
DataDictionaryAR["DocumentDelete"] = 'حذف';
DataDictionaryAR["DeleteFolder"] = 'حذف مجلد';
DataDictionaryAR["DeleteDocument"] = 'حذف ملف';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Phone Tab///////////////////////////////////////////////////////////////////////
DataDictionaryAR["Number"] = 'الرقم';
DataDictionaryAR["Mobile"] = 'موبايل';
DataDictionaryAR["LandLine"] = 'ارضي';
DataDictionaryAR["Fax"] = 'فاكس';
DataDictionaryAR["PhCountry"] = 'البلد';
DataDictionaryAR["PhArea"] = 'المنطقه';
DataDictionaryAR["PhPhone"] = 'الهاتف';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Company View list and View Item//////////////////////////////////////////////////
DataDictionaryAR["Company"] = 'الشركات';
DataDictionaryAR["AddCompany"] = 'اضافة الشركات';
DataDictionaryAR['CompanyDetails'] = ' تفاصيل الشركة';
DataDictionaryAR['CompanyNameEnglish'] = 'اسم الشركة انجليزي';
DataDictionaryAR['CompanyNameArabic'] = 'اسم الشركة  عربي';
DataDictionaryAR['Website'] = 'الموقع الالكتروني';
DataDictionaryAR['CompanyType'] = 'نوع الشركة';
DataDictionaryAR['DecisionMaker'] = 'صانع القرار';
DataDictionaryAR['SalesManAccountManager'] = 'البائع';
DataDictionaryAR['Abbreviation'] = 'الاختصار';
DataDictionaryAR['RegistrationNumber'] = 'رقم التسجيل';
DataDictionaryAR['RegistrationDate'] = 'تاريخ التسجيل';
DataDictionaryAR['Segment'] = 'المجال';
DataDictionaryAR['Source'] = 'المصدر';
DataDictionaryAR['YearlyRevenue'] = 'الإيرادات السنوية';
DataDictionaryAR['EmployeeNumber'] = 'عدد الموظفين';
DataDictionaryAR['Status'] = 'الحالة';
DataDictionaryAR['IsBranch'] = 'فرعي؟';
DataDictionaryAR['ParentCompany'] = 'الشركة الام';
DataDictionaryAR['PaymentTerms'] = 'شروط الدفع';
DataDictionaryAR["PaymentTerm"] = "شرط الدفع";
DataDictionaryAR['PaymentType'] = 'طريقة الدفع';
DataDictionaryAR["AlreadyExist"] = ' موجود مسبقا ';
DataDictionaryAR["HeaderRegistrationNumber"] = 'رقم التسجيل: ';
DataDictionaryAR["HeaderRegistrationDate"] = 'تاريخ التسجيل: ';
DataDictionaryAR["DublicatatedName"] = 'اسم الشركة موجود مسبقا';
DataDictionaryAR["HeaderCompanyName"] = 'اسم الشركة :';
DataDictionaryAR["HeaderWebsite"] = 'الموقع الالكتروني : ';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Company Person Tab//////////////////////////////////////////////////////////////
DataDictionaryAR["Company"] = 'الشركه';
DataDictionaryAR["Email"] = 'البريد الالكتروني';
DataDictionaryAR["Mobile"] = 'الموبايل';
DataDictionaryAR["LandLine"] = 'هاتف المنزل';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// Communication Tab//////////////////////////////////////////////////////////////
DataDictionaryAR["StartDate"] = 'تاريخ البدء';
DataDictionaryAR["EndDate"] = 'تاريخ الانتهاء';
DataDictionaryAR["Type"] = 'النوع';
DataDictionaryAR["Priority"] = 'الاولويه';
DataDictionaryAR["Status"] = 'الحاله';
DataDictionaryAR["Action"] = 'الإجراء';
DataDictionaryAR["View"] = 'عرض';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Company Trading terms Tab///////////////////////////////////////////////////////
DataDictionaryAR["PaymentType"] = 'طريقة الدفع';
DataDictionaryAR["PaymentTerm"] = 'شرط الدفع';
DataDictionaryAR["CreditLimit"] = 'الحد الائتماني';
DataDictionaryAR["CompanyNameInCommercialRegister"] = 'اسم الشركه في السجل التجاري';
DataDictionaryAR["TradeName"] = 'الإسم التجاري';
DataDictionaryAR["CompanyNationalNumber"] = 'الرقم الوطني للشركه';
DataDictionaryAR["OwnerName"] = 'اسم المالك';
DataDictionaryAR["TradingTerms"] = 'شروط التداول';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Company Sales Tab///////////////////////////////////////////////////////////////
DataDictionaryAR["Company"] = 'الشركه';
DataDictionaryAR["InvoiceNumber"] = 'رقم الفاتورة';
DataDictionaryAR["SalesRepresentative"] = 'مندوب المبيعات';
DataDictionaryAR["Branch"] = 'الفرع';
DataDictionaryAR["InvoiceDate"] = 'تاريخ الفاتورة';
/////////////////////////Common tabs///////////////////////////////////////////////////////////////////////////////
DataDictionaryAR['email'] = 'البريد الالكتروني';
DataDictionaryAR['AddNewemail'] = 'اضافةبريد الكتروني';
DataDictionaryAR['note'] = 'الملحظات';
DataDictionaryAR['AddNewnote'] = 'اضافةملاحظة';
DataDictionaryAR['address'] = 'العناوين';
DataDictionaryAR['AddNewaddress'] = 'اضافةعنوان';
DataDictionaryAR['phone'] = 'الهواتف';
DataDictionaryAR['AddNewphone'] = 'اضافةهاتف';
DataDictionaryAR['Attach'] = 'المرفقات';
DataDictionaryAR['AddNewAttach'] = 'اضافةمرفق';
DataDictionaryAR['Communication'] = 'الاتصالات';
DataDictionaryAR['AddNewCommunication'] = 'اضافةاتصال';

DataDictionaryAR["PersonalEmail"] = 'البريد الالكتروني الشخصي';
DataDictionaryAR["BusinessEmail"] = 'البريد الالكتروني للعمل';
DataDictionaryAR["Email"] = 'البريد الالكتروني';
DataDictionaryAR["Address"] = 'العنوان';
DataDictionaryAR["Country"] = ' البلد';
DataDictionaryAR["City"] = 'المدينه';
DataDictionaryAR["StreetAddress"] = 'عنوان الشارع';
DataDictionaryAR["Phone"] = 'الهاتف';
DataDictionaryAR["Home"] = 'البيت';
DataDictionaryAR["Fax"] = 'فاكس';
DataDictionaryAR["Mobile"] = 'موبايل';


DataDictionaryAR['Persons'] = 'الاشخاص';
DataDictionaryAR['AddNewPersons'] = 'اضافةشخص';
DataDictionaryAR['ProjectsOpporunities'] = 'فرص المشاريع';
DataDictionaryAR['AddNewProjectsOpporunities'] = 'اضافة فرصة مشروع';
DataDictionaryAR['Cases'] = 'خدمة العملاء';
DataDictionaryAR['AddNewCase'] = 'اضافة خدمة عميل';
DataDictionaryAR['Appliances'] = 'الشكاوي';
DataDictionaryAR['AddNewAppliances'] = 'اضافةشكوى';
DataDictionaryAR['AccountTransactions'] = 'الحركات على الحسابات';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Person Section//////////////////////////////////////////////////////////////////
////////////////////////////////////////Person View list and View Item//////////////////////////////////////////////////

DataDictionaryAR["HeaderGender"] = 'الجنس : ';
DataDictionaryAR["HeaderBirthDate"] = 'تاريخ الميلاد : ';
DataDictionaryAR["DateOfBirthError"] = 'لايمكن ان يكون في المستقبل';
DataDictionaryAR["Person"] = 'Person';
DataDictionaryAR["AddPerson"] = 'اضافة شخص';
DataDictionaryAR['PersonDetails'] = ' تفاصيل الشخص';
DataDictionaryAR['Company'] = 'الشركة';
DataDictionaryAR['FirstNameEn'] = 'الاسم الاول';
DataDictionaryAR['MiddleNameEn'] = 'الاسم الثاني انجليزي';
DataDictionaryAR['LastNameEn'] = 'اسم العائلة ';
DataDictionaryAR['FirstNameAr'] = 'الاسم الاول عربي';
DataDictionaryAR['MiddleNameAr'] = 'الاسم الثاني عربي';
DataDictionaryAR['LastNameAr'] = 'الاسم الاخير عربي';
DataDictionaryAR['FullName'] = 'الاسم كامل';
DataDictionaryAR['FullNameEn'] = 'الاسم كامل بالانجليزي';
DataDictionaryAR['FullNameAr'] = 'الاسم كامل بالعربي';
DataDictionaryAR['NickName'] = 'اللقب';
DataDictionaryAR['SalesMan'] = 'البائع';
DataDictionaryAR['PersonType'] = '';
DataDictionaryAR['PrimaryContactEmployee'] = 'موظف الاتصال المسؤول';
DataDictionaryAR['Salutation'] = 'التحية';
DataDictionaryAR['Gender'] = 'الجنس';
DataDictionaryAR['Source'] = 'المصدر';
DataDictionaryAR['DateOfBirth'] = 'تاريخ الميلاد';
DataDictionaryAR['Religion'] = 'الديانة';
DataDictionaryAR['MaritalStatus'] = 'الحالة الاجتماعية';
DataDictionaryAR['Department'] = 'القسم';
DataDictionaryAR['Position'] = 'المنصب';
DataDictionaryAR['ContactImage'] = 'الصورة الشخصية';
DataDictionaryAR['CustomerReferralCompany'] = 'إحالة العملاء- الشركة';
DataDictionaryAR['CustomerReferralPerson'] = 'إحالة العملاء- الشخص';
DataDictionaryAR['EmployeeUser'] = 'الموظف';
DataDictionaryAR['ScoutingComments'] = 'تعليقات الاستطلاع';
DataDictionaryAR['DublicatatedFullName'] = 'الاسم الكامل للشخص موجود مسبقا';
DataDictionaryAR['HeaderPersonName'] = 'الاسم الكامل: ';
DataDictionaryAR['HeaderPersonEmail'] = 'البريد الالكتروني: ';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////Opportunity Section///////////////////////////////////////////////////////////////////
////////////////////////////////////////Opportunity View list and View Item///////////////////////////////////////////////////
DataDictionaryAR["Opportunity"] = 'فرصة بيع';
DataDictionaryAR["AddOpportunity"] = 'اضافة فرصة بيع';
DataDictionaryAR['OpportunityDetails'] = ' تفاصيل فرص البيع';
DataDictionaryAR['ApprovedContractorPerson'] = 'المقاول المسؤول- الشخص';
DataDictionaryAR['ApprovedContractorCompany'] = 'المقاول المسؤول- الشركة';
DataDictionaryAR['ProjectName'] = 'اسم المشروع';
DataDictionaryAR['ProjectScope'] = 'اطار المشروع';
DataDictionaryAR['ProjectDetails'] = 'تفاصيل المشروع';
DataDictionaryAR['OpenedDate'] = 'تاريخ البدء';
DataDictionaryAR['Certainty'] = ' % الدقة';
DataDictionaryAR['Details'] = 'التفاصيل';
DataDictionaryAR['MainProductInterestValues'] = 'المنتجات المهتم بها';
DataDictionaryAR['Source'] = 'المصدر';
DataDictionaryAR['DecisionTimeframe'] = 'الإطار الزمني المقرر';
DataDictionaryAR['Priority'] = 'الاهمية';
DataDictionaryAR['MainCompetitor'] = 'المنافس الرئيس';
DataDictionaryAR['EstimatedClosedDate'] = 'تاريخ الاغلاق المقدَر';
DataDictionaryAR['EstimatedEndDate'] = 'تاريخ الانتهاء المقدَر';
DataDictionaryAR['EstimatedStartDate'] = 'تاريخ البدء المقدَر';
DataDictionaryAR['Quotes'] = 'عروض الاسعار';
DataDictionaryAR['AddNewQuotes'] = 'اضافةعروض الاسعار';
DataDictionaryAR['Orders'] = 'الطلبيات';
DataDictionaryAR['AddNewOrders'] = 'اضافةالطلبيات';
DataDictionaryAR['Reports'] = 'التقارير';
DataDictionaryAR['AddNewReports'] = 'اضافةالتقارير';
DataDictionaryAR['Resources'] = 'الموارد';
DataDictionaryAR['AddNewResources'] = 'اضافةالموارد';
DataDictionaryAR["CompanyOrPersonIsRequired"] = 'الشركه او الشخص متطلب';
DataDictionaryAR['ConsultantCompany'] = 'الشركات المسؤولة';
DataDictionaryAR['ConsultantPerson'] = 'الاشخاص المسؤولين';
DataDictionaryAR['HeaderStatus'] = 'الحالة: ';
DataDictionaryAR['HeaderStage'] = 'المرحلة: ';
DataDictionaryAR['HeaderProjectName'] = 'اسم المشروع :';
DataDictionaryAR['PercentageValidation'] = 'اكبر قيمة مسموحة 100';
DataDictionaryAR['ExpiredFixedAssetsReport'] = 'الممتلكات المنتهية';
///////////////////////////////////////////////Opportunity Progress Links/////////////////////////////////////////////////////
DataDictionaryAR["SaleApproved"] = 'اعتماد البيعة';
DataDictionaryAR["SaleLost"] = 'خساره';
DataDictionaryAR["ReserveItems"] = 'محجوز';
DataDictionaryAR["Quoted"] = 'اعتماد عرض السعر';
DataDictionaryAR["Delivered"] = 'تسليم';
DataDictionaryAR["Design"] = 'تصميم';
DataDictionaryAR["Completed"] = 'اكتملت';
DataDictionaryAR["TechnicalSubmitted"] = 'ارسال التقنية';
DataDictionaryAR["Ordered"] = 'اعتماد الطلبية';

DataDictionaryAR["SaleWorkflow"] = 'تعديل مرحلة البيع';
DataDictionaryAR["AddSalesOrderToContinue"] = ' الرجاء اضافة طلب البيع حتى تتمكن من اتمام التعديل ';
DataDictionaryAR["OpportunityProgress"] = 'مراحل فرص البيع';
DataDictionaryAR["Stage"] = 'المرحله';
DataDictionaryAR["Status"] = 'الحاله';
DataDictionaryAR["DeliveryDate"] = 'تاريخ التسليم ';
DataDictionaryAR["LostReason"] = 'سبب الخسارة';
DataDictionaryAR["TrackingNote"] = 'ملاحظه';
DataDictionaryAR["lblStageDefault"] = 'جار';
DataDictionaryAR["lblStatusDefault"] = 'قيد التنفيذ';

DataDictionaryAR['AssignedTo'] = 'تعين الى';
DataDictionaryAR['ContractDate'] = 'تاريخ العقد';
DataDictionaryAR['ActualStartDate'] = 'تاريخ البدء الفعلي';
DataDictionaryAR['ActualEndDate'] = 'تاريخ الانتهاء الفعلي';
DataDictionaryAR['CompletionPercentage'] = ' % نسبة الانجاز';
DataDictionaryAR['DeliveryComments'] = 'ملاحظات التسليم';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////Opportunity Tabs/////////////////////////////////////////////////////
DataDictionaryAR["Progress"] = 'مراحل البيع';
DataDictionaryAR["Orders"] = 'طلبات البيع';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Quotation Section///////////////////////////////////////////////////////////////////
////////////////////////////////////////Quotation List /////////////////////////////////////////////////////////////////////
DataDictionaryAR["Quotation"] = 'عروض الاسعار';
DataDictionaryAR["AddQuotation"] = 'اضافة عرض السعر';
DataDictionaryAR['QuotationDetails'] = 'تفاصيل عرض السعر';
DataDictionaryAR['Opportunity'] = 'فرصة بيع';
DataDictionaryAR['ReferenceNumber'] = 'الرقم المرجعي';
DataDictionaryAR['ContractorPerson'] = 'المقاول المسؤول- الشخص';
DataDictionaryAR['ContractorCompany'] = 'المقاول المسؤول- الشركة';
DataDictionaryAR['Currency'] = 'العملة';
DataDictionaryAR['PriceList'] = 'السعر';
DataDictionaryAR['TermsAndConditions'] = 'الشروط والأحكام';
DataDictionaryAR['Attnto'] = 'Attn To';
DataDictionaryAR['EstimatedDeliveryDate'] = 'تاريخ التسليم المقرر';
DataDictionaryAR['DurationOfScheduledVisitsInMonths'] = 'مدة الزيارات المقررة خلال الأشهر';
DataDictionaryAR['NumberOfScheduledVisits'] = 'عدد الزيارات المجدولة';
DataDictionaryAR['TotalQuotePrice'] = 'السعر الكلي';
DataDictionaryAR['NetPrice'] = 'السعر الصافي';
DataDictionaryAR['DiscountAmount'] = 'قيمة الخصم';
DataDictionaryAR['DiscountPercentage'] = 'نسبة الخصم %';
DataDictionaryAR['AddNewOrders'] = 'اضافة طلب شراء';

DataDictionaryAR['HReferenceNo'] = 'الرقم المرجعي :';
DataDictionaryAR['HOpportunity'] = 'عرض السعر :';
DataDictionaryAR['QuotationItems'] = 'سلع عرض البيع';
DataDictionaryAR['Generated'] = 'Generated';
DataDictionaryAR['Generate'] = 'اعتماد عرض البيع';
DataDictionaryAR['ActionConvertToOrder'] = 'اعتماد طلب بيع';
DataDictionaryAR['Item'] = 'Item';
DataDictionaryAR['ReferenceNumber'] = 'الرقم المرجعي';
DataDictionaryAR['Location'] = 'الموقع';
DataDictionaryAR['Quantity'] = 'الكمية';
DataDictionaryAR['UnitPrice'] = 'سلع القطعة';
DataDictionaryAR['PercentageDiscount'] = 'نسبة الخصم';
DataDictionaryAR['ItemTotalprice'] = 'سعر الفطعة الكلي';
DataDictionaryAR['TotalQuotePrice'] = 'السعر الكلي لعرض البيع';
DataDictionaryAR['NetPrice'] = 'السعر النهائي';
DataDictionaryAR['Generatedsuccessfully'] = 'تم الاعتماد بنجاح';
DataDictionaryAR['QuotationOptionalItems'] = 'السلع الاختيارية';
DataDictionaryAR['OptionalItemsDiscountPercentage'] = 'نسبة الخصم على السلع الاختيارية';
DataDictionaryAR['OptionalItemsDiscountAmount'] = 'قيمة الخصم على السلع الاختيارية';
DataDictionaryAR['OptionalItemsTotalQuotePrice'] = 'السعر الكلي لعرض البيع على السلع الاختيارية';
DataDictionaryAR['OptionalItemsNetPrice'] = 'السعر النهائي على السلع الاختيارية';
DataDictionaryAR['TermsAndConditionsText'] = 'نص الشروط والأحكام';
DataDictionaryAR['Clone'] = 'Clone';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////SalesOrder Section///////////////////////////////////////////////////////////////////
DataDictionaryAR["SalesOrder"] = 'طلب البيع';
DataDictionaryAR["AddSalesOrder"] = 'اضافة طلب بيع';
DataDictionaryAR['SalesOrderDetails'] = 'تفاصيل طلب البيع';
DataDictionaryAR['Qoutation'] = 'عرض البيع';
DataDictionaryAR['Opportunity'] = 'فرصة بيع';
DataDictionaryAR['ReferenceNumber'] = 'الرقم المرجعي';
DataDictionaryAR['Currency'] = 'العملة';
DataDictionaryAR['PriceList'] = 'السعر';
DataDictionaryAR['TermsAndConditions'] = 'الشروط والأحكام';
DataDictionaryAR['Attnto'] = 'Attn To';
DataDictionaryAR['EstimatedDeliveryDate'] = 'تاريخ التسليم المقرر';
DataDictionaryAR['DurationOfScheduledVisitsInMonths'] = 'مدة الزيارات المقررة خلال الأشهر';
DataDictionaryAR['AgreedNumberofServiceVisits'] = 'عدد الزيارات الموافق عليها';
DataDictionaryAR['TotalPrice'] = 'السعر الكلي';
DataDictionaryAR['NetPrice'] = 'السعر النهائي';
DataDictionaryAR['DiscountAmount'] = 'قيمة الخصم';
DataDictionaryAR['DiscountPercentage'] = 'نسبة الخصم %';
DataDictionaryAR['IsFinal'] = 'نهائي';
DataDictionaryAR['QuotationOptionalItems'] = 'السلع الاختيارية';
DataDictionaryAR['OptionalItemsDiscountPercentage'] = 'نسبة الخصم على السلع الاختيارية';
DataDictionaryAR['OptionalItemsDiscountAmount'] = 'قيمة الخصم على السلع الاختيارية';
DataDictionaryAR['OptionalItemsTotalPrice'] = 'السعر الكلي لعرض البيع على السلع الاختيارية';
DataDictionaryAR['OptionalItemsNetPrice'] = 'السعر النهائي على السلع الاختيارية';
DataDictionaryAR['VariationOrders'] = 'Variation Orders';
DataDictionaryAR['AddNewVariationOrders'] = 'Add New VariationOrders';
DataDictionaryAR['PercentageDiscount'] = 'نسبة الخصم';
DataDictionaryAR['ItemTotalprice'] = 'سعر الفطعة الكلي';
DataDictionaryAR['HQuotation'] = 'Quotation: ';
DataDictionaryAR['HFinal'] = 'Final Order';
DataDictionaryAR['MarkFinal'] = 'Mark Final';
DataDictionaryAR['MarkedFinalsuccessfully'] = 'Marked Final successfully';

DataDictionaryAR['OrderAlredayExist'] = 'The current quotation already has sales order';
DataDictionaryAR['CanNotDeleteGeneratedQuotation'] = 'Can not delete generated quotation';
DataDictionaryAR['CanNotEditGeneratedQuotation'] = 'Can not edit generated quotation';
DataDictionaryAR['quotationAlreadyGenerated'] = 'quotation already generated';
DataDictionaryAR['CanNoDeleteFinalOrder'] = 'Can not delete final Order';
DataDictionaryAR['CanNotEditFinalOrder'] = 'Can not edit final order';
DataDictionaryAR['OrderAlreadyMarkedAsFinal'] = 'Order already marked as final';
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////OrderDeliveredItems eArabic Dictionary/////////////////////////////////////////////////////
DataDictionaryAR["OrderDeliveredItems"] = 'OrderDeliveredItems';
DataDictionaryAR["AddOrderDeliveredItems"] = 'اضافة OrderDeliveredItems';
DataDictionaryAR['OrderDeliveredItemsDetails'] = ' تفاصيل OrderDeliveredItems';
DataDictionaryAR['DeliveredItem'] = 'Delivered Item';
DataDictionaryAR['Opportunity'] = 'Opportunity';
DataDictionaryAR['Stage'] = 'Stage';
DataDictionaryAR['Quotation'] = 'Quotation';
DataDictionaryAR['SalesOrder'] = 'Sales Order';
DataDictionaryAR['Item'] = 'Item';
DataDictionaryAR['Quantity'] = 'Quantity';
DataDictionaryAR['SerialNo'] = 'Serial No';
DataDictionaryAR['DeliveryDate'] = 'Delivery Date';
DataDictionaryAR['IsKit'] = 'Is Kit';
DataDictionaryAR['QuantityToDeliver'] = 'Quantity To Deliver';
DataDictionaryAR['OrderedQuantity'] = 'Ordered Quantity';
DataDictionaryAR['AvailableToDeliver'] = 'Available To Deliver';
DataDictionaryAR['ExceedsNeededQuantity'] = 'Exceeds needed quantity';
DataDictionaryAR['GreaterThanZero'] = 'should be greater than 0';
DataDictionaryAR['ItemsAlreadyAdded'] = 'Items already added';
DataDictionaryAR['SelectItems'] = 'Select Items to be delivered';
DataDictionaryAR['checkItemsQuantities'] = 'Check Items quantities';
DataDictionaryAR['ClickToAddItems'] = 'Click To Add Items';
DataDictionaryAR['NotAllItemsDelivered'] = 'Not all items delivered';
DataDictionaryAR['addItems'] = 'Delivered Items';
DataDictionaryAR['ThereIsNoAnyDeliveredItems'] = 'There is no any delivered items';
DataDictionaryAR['QtyValidation'] = 'Quantity musn\'t be more than availabe quantity ';
DataDictionaryAR['Report'] = 'Report';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Stock Section///////////////////////////////////////////////////////////////////
////////////////////////////////////////Stock List /////////////////////////////////////////////////////////////////////
DataDictionaryAR["StockUpdate"] = 'تحديثات المخزون';
DataDictionaryAR["StockUpdateDetails"] = 'تفاصيل تحديثات المخزون';
DataDictionaryAR["SItemGroup"] = 'البند';
DataDictionaryAR["SItem"] = 'وصف السلعة';
DataDictionaryAR["TransactionType"] = 'نوع الاجراء';
DataDictionaryAR["NewStockAction"] = 'New Stock Action';
DataDictionaryAR["MoveFrom"] = 'من';
DataDictionaryAR["MoveTo"] = 'الى';
DataDictionaryAR["QuantityAdded"] = 'الكمية المضافة';
DataDictionaryAR["TransactionDate"] = 'التاريخ';
DataDictionaryAR["Details"] = 'التفاصيل';
DataDictionaryAR["AddStock"] = 'اضافه للمخزون';
DataDictionaryAR["NewItems"] = 'سلعه جديده';
DataDictionaryAR["MoveFromAndToShouldBeDifferant"] = 'الانتقال من والانتقال إلى يجب ان تكون مختلفه';
DataDictionaryAR["WarehouseQuantityNotAvailable"] = 'هذه الكميه غير متاحه في المخزن';
DataDictionaryAR["ShowRoomQuantityNotAvailable"] = 'هذه الكميه غير متاحه في المعرض';
DataDictionaryAR["ShouldBePositiveValueToBeMoved"] = 'يجب أن تكون القيمة إيجابية حتى تتمكن من نقلها';
DataDictionaryAR["ShouldBePositiveValueToBeAdded"] = 'يجب أن تكون القيمة إيجابية حتى تتمكن من الاضافه';
DataDictionaryAR["ShouldBeNegativeValueToBeRemoved"] = 'يجب أن تكون القيمة سالبه حتى تتمكن من حذفها';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Stock Links /////////////////////////////////////////////////////////////////////
DataDictionaryAR["Reset"] = 'إعادة';
DataDictionaryAR["Search"] = 'بحث';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Walkin section//////////////////////////////////////////////////////////////////
DataDictionaryAR["WalkIn"] = "الزوار";
DataDictionaryAR["Clear"] = 'مسح';
DataDictionaryAR["WalkInList"] = "قائمة الزوار";
DataDictionaryAR["NumberOfWalkIn"] = 'عدد من يدخلون المعرض يوميا';
DataDictionaryAR["WalkInDateFrom"] = 'من';
DataDictionaryAR["WalkInDateTo"] = 'الى';
DataDictionaryAR["WalkInTimeFrom"] = 'من';
DataDictionaryAR["WalkInTimeTo"] = 'الى';
DataDictionaryAR["Filter"] = 'بحث';
DataDictionaryAR["WalkInDate"] = 'تاريخ';
DataDictionaryAR["Comments"] = 'تعليق';
DataDictionaryAR["WalkInTime"] = 'الوقت';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////User section//////////////////////////////////////////////////////////////////
DataDictionaryAR["User"] = 'مستخدم';
DataDictionaryAR["AddUser"] = 'اضافة مستخدم';
DataDictionaryAR["UserDetails"] = 'تفاصيل المستخدم';
DataDictionaryAR["UUserName"] = 'اسم المستخدم';
DataDictionaryAR["UFirstName"] = 'الاسم الاول';
DataDictionaryAR["UMiddleName"] = 'الاسم الثاني';
DataDictionaryAR["ULastName"] = 'اسم العائله';
DataDictionaryAR["UPassword"] = 'كلمة المرور';
DataDictionaryAR["UConfirmPassword"] = 'تأكيد كلمة المرور';
DataDictionaryAR["UTitle"] = 'المسمى الوظيفي';
DataDictionaryAR["UDateOfBirth"] = 'تاريخ الميلاد';
DataDictionaryAR["UEmail"] = 'البريد الالكتروني';
DataDictionaryAR["URole"] = 'الدور';
DataDictionaryAR["UTeam"] = 'الفريق';
DataDictionaryAR["UManager"] = 'المدير';
DataDictionaryAR["UIsDefault"] = 'Is Default';
DataDictionaryAR["UIsManager"] = 'المدير';
DataDictionaryAR["UNote"] = 'ملاحظات';
DataDictionaryAR["UHeaderUserName"] = 'اسم المستخدم : ';
DataDictionaryAR["UHeaderName"] = 'الاسم : ';
DataDictionaryAR["UHeaderEMail"] = 'البريد الالكتروني : ';
DataDictionaryAR["USpecialCharError"] = 'يجب ان لا تحتوي على فراغات او حروف خاصة ';
DataDictionaryAR["USPasswordConfirmation"] = 'كلمة السر غير مطابقة';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////Team section//////////////////////////////////////////////////////////////////
DataDictionaryAR["TManager"] = 'المدير';
DataDictionaryAR["TDescription"] = 'الوصف';
DataDictionaryAR["TeamsList"] = 'قائمة الفرق';
DataDictionaryAR["TArabicName"] = 'الاسم بالعربي';
DataDictionaryAR["TName"] = 'الاسم';
DataDictionaryAR["TAddnewTeam"] = 'اضافة فريق جديد';
DataDictionaryAR["TTeams"] = 'الفرق';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////Role section//////////////////////////////////////////////////////////////////
DataDictionaryAR["Roles"] = 'الأدوار';
DataDictionaryAR["RDescription"] = 'الوصف';
DataDictionaryAR["RolesList"] = 'قائمة الأدوار';
DataDictionaryAR["RArabicName"] = 'الاسم بالعربي';
DataDictionaryAR["RName"] = 'الاسم';
DataDictionaryAR["RAddnewRole"] = 'اضافة دور جديد';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////Lovs section//////////////////////////////////////////////////////////////////
DataDictionaryAR["LOVs"] = 'قائمة القيم';
DataDictionaryAR["LOVsValueAR"] = 'القيمة بالعربي';
DataDictionaryAR["LOVsValue"] = 'القيمه';
DataDictionaryAR["LOVsContentType"] = 'نوع المحتوى';
DataDictionaryAR["AddnewLOV"] = 'اضافة قائمة قيم جديده';
DataDictionaryAR["SystemContentsCannotBeDeleted"] = 'لا يمكن حذف هذه المحتويات  !';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////Lock Entity section///////////////////////////////////////////////////////////
DataDictionaryAR["UnlockForAllUsers"] = 'فك القفل لكافة المستخدمين';
DataDictionaryAR["SelectUser"] = 'اختر مستخدم';
DataDictionaryAR["Unlock"] = 'فك القفل';
DataDictionaryAR["Date"] = 'التاريخ';
DataDictionaryAR["LockedBy"] = 'مقفله من قبل';
DataDictionaryAR["UnlockAll"] = ' فك قفل الكل ';
DataDictionaryAR["UnlockedSuccessfully"] = 'تم فك القفل بنجاح';
DataDictionaryAR["FailedToUnlock"] = 'فشل في فك القفل';
DataDictionaryAR["EntityLock"] = 'Entity lock';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////Dash Board section///////////////////////////////////////////////////////////
DataDictionaryAR["ToDoList"] = 'قائمة المهام';
DataDictionaryAR["DBReferenceNo"] = 'الرقم المرجعي';
DataDictionaryAR["DBDetails"] = 'التفاصيل';
DataDictionaryAR["DBVisitType"] = 'نوع الزيارة';
DataDictionaryAR["DBStatus"] = 'الحاله';
DataDictionaryAR["DBDate"] = 'التاريخ';
DataDictionaryAR["DBShowAll"] = 'مشاهدة الكل';
DataDictionaryAR["MyProfile"] = 'صفحتي';
DataDictionaryAR["ChangePassword"] = 'تغيير كلمة المرور';
DataDictionaryAR["DBUserName"] = 'اسم المستخدم';
DataDictionaryAR["DBFirstName"] = 'الاسم الأول';
DataDictionaryAR["DBMiddleName"] = 'الاسم الثاني';
DataDictionaryAR["DBLastName"] = 'اسم العائله';
DataDictionaryAR["DBOldPassword"] = 'كلمة المرور القديمة';
DataDictionaryAR["DBNewPassword"] = 'كلمة المرور الجديدة';
DataDictionaryAR["DBConfirm"] = 'تأكيد';
DataDictionaryAR["DBTitle"] = 'المسمى الوظيفي';
DataDictionaryAR["DBManager"] = 'المدير';
DataDictionaryAR["DBDateOfBirth"] = ' تاريخ الميلاد ';
DataDictionaryAR["DBEmail"] = ' البريد الالكتروني ';
DataDictionaryAR["DBRole"] = 'الدور';
DataDictionaryAR["DBTeam"] = 'الفريق';
DataDictionaryAR["DBManager"] = 'المدير';
DataDictionaryAR["DBIsManager"] = 'مدير';
DataDictionaryAR["DBSales"] = 'المبيعات';
DataDictionaryAR["DBInvoiceNo"] = 'رقم الفاتوره';
DataDictionaryAR["DBCompany"] = 'الشركه';
DataDictionaryAR["DBPerson"] = 'الشخص';
DataDictionaryAR["DBBranch"] = 'الفرع';
DataDictionaryAR["DBDescription"] = 'الوصف';
DataDictionaryAR["SalesGraph"] = 'الرسم البياني للمبيعات';
DataDictionaryAR["DBClientCareCasesGraph"] = 'الرسم البياني لخدمة العملاء';
DataDictionaryAR["DBClientCareCase"] = 'خدمة العملاء';
DataDictionaryAR["DBType"] = 'النوع';
DataDictionaryAR["ChequeDate"] = 'تاريخ الشيك';
DataDictionaryAR["TransactionDate"] = 'تاريخ الحركة';

DataDictionaryAR["ChequeDateFrom"] = 'تاريخ الشيك من';
DataDictionaryAR["TransactionDateFrom"] = 'تاريخ الحركة من';
DataDictionaryAR["ChequeDateTo"] = 'تاريخ الشيك الى';
DataDictionaryAR["TransactionDateTo"] = 'تاريخ الحركة الى';
DataDictionaryAR["PDCReport"] = "تقرير شيكات برسم التحصيل";
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////Permission  section///////////////////////////////////////////////////////////
DataDictionaryAR["PRole"] = 'الدور';
DataDictionaryAR["CollapseAll"] = 'اغلاق الكل';
DataDictionaryAR["SelectAll"] = 'تحديد الكل';
DataDictionaryAR["UnSelectAll"] = 'الغاء تحديد الكل';
DataDictionaryAR["Permissions"] = 'الصلاحيات';
DataDictionaryAR["UnCollapseAll"] = 'فتح الكل';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////ReturnedItems  section///////////////////////////////////////////////////////////
DataDictionaryAR["HOrderNumber"] = 'رقم الطلبية : ';
DataDictionaryAR["HSalesCreatedBy"] = 'إنشاء بواسطة : ';
DataDictionaryAR["OrderNumber"] = 'رقم الطلبية';
DataDictionaryAR["SalesCreatedDate"] = 'تاريخ الانشاء';
DataDictionaryAR["ReturnedItemsDetails"] = 'تفاصيل العناصر المرجعه';
DataDictionaryAR["CreatedBy"] = 'انشأت بواسطة';
DataDictionaryAR["CreatedDate"] = 'تاريخ الانشاء';
DataDictionaryAR["ReturnReson"] = 'سبب الارجاع';
DataDictionaryAR["Notes"] = 'ملاحظات';
DataDictionaryAR["ReturnLocation"] = 'ارجعت الى';
DataDictionaryAR["ReturnQuantity"] = 'الكمية المرجعة';
DataDictionaryAR["Quantity"] = 'العدد';
DataDictionaryAR["Total"] = 'المجموع  ';
DataDictionaryAR["ActualDimensions(Width)"] = 'القياس الفعلي (العرض)';
DataDictionaryAR["ActualDimensions(Length)"] = 'القياس الفعلي (طول)';
DataDictionaryAR["Dimensions(Width)"] = 'القياس (العرض)';
DataDictionaryAR["Dimensions(Length)"] = 'القياس (طول)';
DataDictionaryAR["Item"] = 'وصف السلعة';
DataDictionaryAR["ItemGroup"] = 'البند ';
DataDictionaryAR["AddReturnItems"] = 'اضافة سلعه مرجعه';
DataDictionaryAR["ReturnItems"] = 'السلع المرجعه';
DataDictionaryAR["AddReturnItems"] = 'اضافة سلعه مرجعه';
DataDictionaryAR["can'tAddReturnItem"] = 'يجب أن يتم تسليم أمر البيع لتتمكن من ارجاع سلع';
DataDictionaryAR["mustBeLessOrEqualQuantity"] = 'يجب ان تكون اقل او تساوي العدد';
DataDictionaryAR["youMustToAddReturnQuantityToSaveReturnItem"] = 'يجب اضافة كميه مرجعه للاستمرار';
DataDictionaryAR["SalesOrderItemsListIsEmpty"] = 'امر البيع لا يحتوي على أية سلع';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////SalesOrder  section///////////////////////////////////////////////////////////
DataDictionaryAR["SalesOrderDetails"] = 'تفاصيل طلب البيع';
DataDictionaryAR["SOOrderNumber"] = 'رقم الطلبية';
DataDictionaryAR["SOSales"] = 'المبيعات';
DataDictionaryAR["SOIsTaxExempt"] = 'فاتورة غير ضريبية ؟ ';
DataDictionaryAR["SOTotalInvoiceValue"] = ' القيمة الإجمالية للفاتورة';
DataDictionaryAR["SOComments"] = 'تعليقات و ملاحظات أخرى';
DataDictionaryAR["SOSalesOrderItem"] = 'عناصر طلب البيع';
DataDictionaryAR["SOItemGroup"] = 'البند ';
DataDictionaryAR["SOItem"] = 'وصف السلعة';
DataDictionaryAR["SODimensionsLength"] = 'القياس (طول)';
DataDictionaryAR["SODimensionsWidth"] = 'القياس (العرض)';
DataDictionaryAR["SOListPrice"] = 'السعر الاصلي';
DataDictionaryAR["SOPriceWithoutTax"] = 'السعر بدون ضريبة';
DataDictionaryAR["SOActualDimensionsLength"] = 'القياس الفعلي (طول)';
DataDictionaryAR["SOActualDimensionsWidth"] = 'القياس الفعلي (العرض)';
DataDictionaryAR["SOQuantity"] = 'العدد';
DataDictionaryAR["SOIsGiveaway"] = 'هدية ؟';
DataDictionaryAR["SODiscountPercentage"] = 'نسبة الخصم %';
DataDictionaryAR["SOMax"] = 'Max. % =';
DataDictionaryAR["SODiscountValue"] = 'قيمة الخصم';
DataDictionaryAR["SONetPrice"] = 'السعر الصافي';
DataDictionaryAR["SOInventoryType"] = 'نوع المخزن';
DataDictionaryAR["SONotes"] = 'ملاحظات';
DataDictionaryAR["SOListPricewithtax"] = 'السعر مع ضريبة';
DataDictionaryAR["SOListPricewithouttax"] = 'السعر بدون ضريبة';
DataDictionaryAR["SOTotalPrice"] = 'السعر الإجمالي';
DataDictionaryAR["SOSaleStage"] = 'مرحلة البيع';
DataDictionaryAR["couldNotSaveWithoutAddItemMsg"] = 'لا يمكن اضافة طلب البيع دون إضافة سلعة';
DataDictionaryAR["addItemMsg"] = 'اضافة سلعة';
DataDictionaryAR["AddSalesOrder"] = 'اضافة طلب بيع';
DataDictionaryAR["SalesOrder"] = 'طلب بيع';
DataDictionaryAR["deleteNonLeadSaleSalesOrder"] = 'لا يمكن اتمام عملية الحذف على طلب البيع الحالي';
DataDictionaryAR["InstallationOrder"] = 'تثبيت طلبيه';
DataDictionaryAR["ProductionOrder"] = 'تصنيع فرشات';
DataDictionaryAR["FrunitureInstallationOrder"] = 'تثبيت طلبية اثاث';
DataDictionaryAR["PostponeRequest"] = 'طلب تأجيل';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////Calender  section///////////////////////////////////////////////////////////
DataDictionaryAR["Rejected"] = 'مرفوض';
DataDictionaryAR["Closed"] = 'مغلق';
DataDictionaryAR["Inactive"] = 'غير فعال';
DataDictionaryAR["Active"] = 'فعال';

DataDictionaryAR["Calendar"] = 'التقويم';
DataDictionaryAR["AddTask"] = 'اضافة مهمه';
DataDictionaryAR["AllDay"] = 'كل-اليوم';
DataDictionaryAR["am"] = 'م';
DataDictionaryAR["pm"] = 'ص';
DataDictionaryAR["AM"] = 'م';
DataDictionaryAR["PM"] = 'ص';
DataDictionaryAR["Today"] = 'اليوم';
DataDictionaryAR["Month"] = 'شهر';
DataDictionaryAR["Week"] = 'اسبوع';
DataDictionaryAR["Day"] = 'يوم';
////////DaysShort
DataDictionaryAR["Sun"] = 'الاحد';
DataDictionaryAR["Mon"] = 'الاثنين';
DataDictionaryAR["Tue"] = 'الثلاثاء';
DataDictionaryAR["Wed"] = 'الاربعاء';
DataDictionaryAR["Thu"] = 'الخميس';
DataDictionaryAR["Fri"] = 'الجمعه';
DataDictionaryAR["Sat"] = 'السبت';

////////Days
DataDictionaryAR["Sunday"] = 'الاحد';
DataDictionaryAR["Monday"] = 'الاثنين';
DataDictionaryAR["Tuesday"] = 'الثلاثاء';
DataDictionaryAR["Wednesday"] = 'الاربعاء';
DataDictionaryAR["Thursday"] = 'الخميس';
DataDictionaryAR["Friday"] = 'الجمعه';
DataDictionaryAR["Saturday"] = 'السبت';

////////monthNames
DataDictionaryAR["January"] = 'كانون الثاني';
DataDictionaryAR["February"] = 'شباط';
DataDictionaryAR["March"] = 'اذار';
DataDictionaryAR["April"] = 'نيسان';
DataDictionaryAR["May"] = 'أيار';
DataDictionaryAR["June"] = 'حزيران';
DataDictionaryAR["July"] = 'تموز';
DataDictionaryAR["August"] = 'اب';
DataDictionaryAR["September"] = 'ايلول';
DataDictionaryAR["October"] = 'تشرين الاول';
DataDictionaryAR["November"] = 'تشرين الثاني';
DataDictionaryAR["December"] = 'كانون الاول';


////////monthNamesShort
DataDictionaryAR["Jan"] = 'كانون الثاني';
DataDictionaryAR["Feb"] = 'شباط';
DataDictionaryAR["Mar"] = 'اذار';
DataDictionaryAR["Apr"] = 'نيسان';
DataDictionaryAR["May"] = 'أيار';
DataDictionaryAR["Jun"] = 'حزيران';
DataDictionaryAR["Jul"] = 'تموز';
DataDictionaryAR["Aug"] = 'اب';
DataDictionaryAR["Sep"] = 'ايلول';
DataDictionaryAR["Oct"] = 'تشرين الاول';
DataDictionaryAR["Nov"] = 'تشرين الثاني';
DataDictionaryAR["Dec"] = 'كانون الاول';

////////Dive Add Task
DataDictionaryAR["CalStartDate"] = 'تاريخ البدء';
DataDictionaryAR["CalEndDate"] = 'تاريخ الانتهاء';
DataDictionaryAR["CalType"] = 'النوع';
DataDictionaryAR["CalPriority"] = 'الاولويه';
DataDictionaryAR["CalStatus"] = 'الحاله';
DataDictionaryAR["CalAction"] = 'مبادرات';
DataDictionaryAR["CalVisitType"] = 'نوع الزياره';
DataDictionaryAR["CalArea"] = 'المنطقه';
DataDictionaryAR["CalDescription"] = 'الوصف';
DataDictionaryAR["CalMeetingResults"] = 'نتائج الاجتماع';
DataDictionaryAR["CalHistoryUpdated"] = 'تاريخ التحديث';
DataDictionaryAR["CalAssignedBy"] = 'معينة من قبل';
DataDictionaryAR["CalCreatedDate"] = 'تاريخ الإنشاء';
DataDictionaryAR["CalLastUpdatedBy"] = 'اخر تحديث بواسطة';
DataDictionaryAR["CalLastUpdatedDate"] = 'تاريخ اخر تحديث ';
DataDictionaryAR["CalCreatedBy"] = 'أنشأت بواسطة';
DataDictionaryAR["CalRef-NO"] = 'الرقم المرجعي';
DataDictionaryAR["CalClosedBy"] = 'اغلقت بواسطة';
DataDictionaryAR["CalClosedDate"] = 'تاريخ الاغلاق';
DataDictionaryAR["Discussion"] = 'محادثه';
DataDictionaryAR["DeleteTask"] = 'حذف المهمه';
DataDictionaryAR["FaildDeleteTask"] = 'فشل في حذف هذه المهمه';
DataDictionaryAR["errorLoadingCalender"] = 'خطأ أثناء تحميل التقويم';
DataDictionaryAR["CalSearch"] = 'بحث';
DataDictionaryAR["CalAllTask"] = 'كل المهام';
DataDictionaryAR["CalByMe"] = 'بواسطتي';
DataDictionaryAR["CalToMe"] = 'الي';

DataDictionaryAR["ClAssignedTo"] = 'تعيين الى';
DataDictionaryAR["AssignedBy"] = 'تعيين من';
DataDictionaryAR["Type"] = 'النوع';
DataDictionaryAR["Priority"] = 'الاولوية';
DataDictionaryAR["DateFrom"] = 'التاريخ من';
DataDictionaryAR["DateTo"] = 'التاريخ الى';
DataDictionaryAR["TimeFrom"] = 'الوقت من';
DataDictionaryAR["TimeTo"] = 'الوقت الى';
DataDictionaryAR["ShowHideFilter"] = 'بحث متقدم';
DataDictionaryAR["Assignment"] = 'تعيين المهمة';
DataDictionaryAR["TaskDetails"] = 'قائمة المهام';
DataDictionaryAR["CalStartDateTime"] = 'تاريخ البدء و الوقت';
DataDictionaryAR["CalEndDateTime"] = 'تاريخ الانتهاء و الوقت';
DataDictionaryAR["CalendarListView"] = 'عرض القائمة';
DataDictionaryAR["CalendarView"] = 'عرض الرزنامة';
DataDictionaryAR["EndDateErrorMsg"] = 'تاريخ الانتهاء يجب ان يكون بعد تاريخ البدء';
DataDictionaryAR["Print"] = 'طباعة';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////Advanced Search  section/////////////////////////////////////////////////////
DataDictionaryAR["AdvFilterYourResults"] = 'تصفية النتائج';
DataDictionaryAR["AdvSortingField"] = 'الترتيب';
DataDictionaryAR["AdvAscending"] = 'تصاعدي';
DataDictionaryAR["AdvFrom"] = 'من';
DataDictionaryAR["AdvTo"] = 'الى';
DataDictionaryAR["LnkReset"] = 'مسح';
DataDictionaryAR["LnkSearch"] = 'بحث';
DataDictionaryAR["AdvFromTo"] = ' من-الى ';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////OnScreen Notification section//////////////////////////////////////////////////
DataDictionaryAR["OnScreenNotTotalOf"] = ' المجموع ';
DataDictionaryAR["SnoozeAll"] = 'غفوة للكل ';
DataDictionaryAR["Snooze"] = 'غفوة';
DataDictionaryAR["Dismiss"] = 'رفض';
DataDictionaryAR["DismissAll"] = 'رفض الكل';
DataDictionaryAR["Nmin"] = 'دقيقه';
DataDictionaryAR["Nhour"] = 'ساعه';
DataDictionaryAR["Nday"] = 'يوم';
DataDictionaryAR["Nweek"] = 'اسبوع';
DataDictionaryAR["Totalof0"] = 'مجموع من 0';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryAR["ChromeRecommend"] = 'للحصول على أداء أفضل، نحن نوصي باستخدام متصفح جوجل كروم';
/////////////////////////////////////////Report  section////////////////////////////////////////////////////////////////
DataDictionaryAR["DailySalesSummary"] = 'ملخص المبيعات اليومية';
DataDictionaryAR["OrderDateFrom"] = 'تاريخ التوصية من';
DataDictionaryAR["OrderDateTo"] = 'تاريخ التوصية الى';
DataDictionaryAR["OpportunityNumber"] = 'رقم البيع';
DataDictionaryAR["TotalAmount"] = 'القيمه الاجماليه';
DataDictionaryAR["FilterYourResults"] = 'تصفية النتائج';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////Client Care section////////////////////////////////////////////////////////////////
DataDictionaryAR["lblCaseType"] = 'نوع الشكوى';
DataDictionaryAR["lblReferenceNumber"] = 'رقم الشكوى';
DataDictionaryAR["lblCasesDetails"] = 'تفاصيل الخدمة';
DataDictionaryAR["lblCustomerMood"] = 'مزاج العملاء عند الشكوى';
DataDictionaryAR["lblSale"] = 'رقم الطلبية';
DataDictionaryAR["lblDateOfSale"] = 'تاريخ البيع';
DataDictionaryAR["lblDateReceived"] = 'تاريخ ووقت استلام الشكوى';
DataDictionaryAR["lblRequiredDeliveryDate"] = 'تاريخ الارجاع المطلوب';
DataDictionaryAR["lblActualDeliveryDate"] = 'تاريخ الارجاع الفعلي';
DataDictionaryAR["lblDateOfWithdrawalOfGoods"] = 'تاريخ سحب البضاعة';
DataDictionaryAR["lblEffectiveDateOfTheWithdrawalOfGoods"] = 'تاريخ سحب البضاعة الفعلي';
DataDictionaryAR["lblProposedSolution"] = 'الحل المقترح';
DataDictionaryAR["lblReason"] = 'السبب';
DataDictionaryAR["lblShowroomOffiecerComments"] = 'ملاحظات مسؤول المعرض';
DataDictionaryAR["lblWarehouseManagerComments"] = 'ملاحظات مدير المستودعات';
DataDictionaryAR["lblQAMangerComments"] = 'ملاحظات مدير تأكيد الجودة';
DataDictionaryAR["lblReasonRejected"] = 'رفض السبب';
DataDictionaryAR["HeaderCaseType"] = 'نوع الشكوى : ';
DataDictionaryAR["HeaderStatus"] = 'الحالة : ';
DataDictionaryAR["AddCase"] = 'اضافة شكوى';
DataDictionaryAR["Cases"] = 'الشكاوي';
DataDictionaryAR["StatusPending"] = 'في وضع الانتظار ';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////Reports Section////////////////////////////////////////////////////////////////
DataDictionaryAR["btnSearch"] = 'بحث';
DataDictionaryAR["lnkReset"] = 'مسح';
DataDictionaryAR["CreateDateFrom"] = 'تاريخ الانشاء من';
DataDictionaryAR["CreateDateTo"] = 'تاريخ الانشاء الى';
DataDictionaryAR["BusinessEmail"] = ' بريد العمل الالكتروني';
DataDictionaryAR["PersonalEmail"] = 'البريد الالكتروني الشخصي';

/////////////////////////////////////////Company Report
DataDictionaryAR["CompanyName"] = 'اسم الشركة';
DataDictionaryAR["Type"] = 'النوع';
DataDictionaryAR["Status"] = 'الحالة';
DataDictionaryAR["Active"] = 'فعال';
DataDictionaryAR["Inactive"] = 'غير فعال';
DataDictionaryAR["WebSite"] = 'الموقع الالكتروني';
DataDictionaryAR["LandLineNumber"] = 'رقم الهاتف الارضي';
DataDictionaryAR["CompaniesReport"] = 'تقرير العملاء (الشركات )';
/////////////////////////////////////////Person Report
DataDictionaryAR["FirstName"] = 'الاسم الاول';
DataDictionaryAR["LastName"] = 'الاسم الاخير';
DataDictionaryAR["MobileNumber"] = 'رقم الموبايل';
DataDictionaryAR["Gender"] = 'الجنس';
DataDictionaryAR["MaritalStatus"] = 'الحالة الاجتماعية';
DataDictionaryAR["Nationality"] = 'الجنسية';
DataDictionaryAR["Country"] = 'البلد';
DataDictionaryAR["City"] = 'المدينة';
DataDictionaryAR["PersonsReport"] = 'تقري العملاء (الاشخاص )';
/////////////////////////////////////////WalkIn Report
DataDictionaryAR["OrderDateFrom"] = 'تاريخ الطابية من';
DataDictionaryAR["OrderDateTo"] = 'تاريخ الطلبية الى';
DataDictionaryAR["WalkInTime"] = 'وقت المرور';
DataDictionaryAR["Item"] = 'السلعة';
DataDictionaryAR["ItemGroup"] = 'نوع السلعة';
DataDictionaryAR["SalesRepresentative"] = 'البائع السؤول';
DataDictionaryAR["WalkInReportTitle"] = 'عدد المبيعات بالنسبة لعدد زوار المعرض';
/////////////////////////////////////////StockUpdate Report
DataDictionaryAR["TransactionDateFrom"] = 'تاريخ الحركة من';
DataDictionaryAR["TransactionDateTo"] = 'تاريخ الحركة الى';
DataDictionaryAR["TransactionType"] = 'نوع الحركة';
DataDictionaryAR["NewStockAction"] = 'New Stock Action';
DataDictionaryAR["MoveFrom"] = 'نقل من';
DataDictionaryAR["MoveTo"] = 'نقل الى';
DataDictionaryAR["QuantityAdded"] = 'الكمية المضافة';
DataDictionaryAR["Details"] = 'التفاصيل';
DataDictionaryAR["StockUpdateReportTitle"] = 'تقرير حركات المستودع';
/////////////////////////////////////////ReturnedItems Report
DataDictionaryAR["ReturnDateFrom"] = 'تاريخ الارجاع من';
DataDictionaryAR["ReturnDateTo"] = 'تاريخ الارجاع الى';
DataDictionaryAR["ReturnedItemsReportTitle"] = 'تقرير السلع المرجعة';
/////////////////////////////////////////GiveAways Report
DataDictionaryAR["GiveAwaysReportTitle"] = 'مجموع الهدايا الشهري';
/////////////////////////////////////////SoldPieces Report
DataDictionaryAR["SoldPiecesReportTitle"] = 'تقرير مبيعات السلع الشهري';
/////////////////////////////////////////Calender Report
DataDictionaryAR["CalenderReportTitle"] = 'تقارير الزيارات و الأجراءات اليومية';
DataDictionaryAR["StartDateFrom"] = 'تاريخ البدء من';
DataDictionaryAR["StartDateTo"] = 'تاريخ البدء الى';
DataDictionaryAR["ReferenceNo"] = 'الرقم المرجعي';
DataDictionaryAR["StartTimeFrom"] = 'وقت البدء كم';
DataDictionaryAR["StartTimeTo"] = 'وقت البدء الى';
DataDictionaryAR["Area"] = 'المنطقة';
DataDictionaryAR["EndDateFrom"] = 'تاريخ الانتهاء من';
DataDictionaryAR["EndDateTo"] = 'تاريخ الانتهاء الى';
DataDictionaryAR["EndTimeFrom"] = 'وقت الانتهاء من';
DataDictionaryAR["EndTimeTo"] = 'وقت الانتهاء الى';
DataDictionaryAR["VisitType"] = 'نوع الزيارة';
DataDictionaryAR["Actions"] = 'الاجراءات';
DataDictionaryAR["AssignedTo"] = 'الموظف المسؤول';
DataDictionaryAR["TaskType"] = 'نوع التقرير';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////SMS Configuration  section////////////////////////////////////////////////////////////////
DataDictionaryAR["SMSConfigurationDetails"] = 'تفاصيل اعدادات الرسائل';
DataDictionaryAR["AddParameter"] = 'اضافة علامه';
DataDictionaryAR["Name"] = 'الاسم';
DataDictionaryAR["SMSUrl"] = 'رابط الرسائل النصيه';
DataDictionaryAR["Key"] = 'المفتاح';
DataDictionaryAR["Value"] = 'القيمه';
DataDictionaryAR["Option"] = 'خيارات';
DataDictionaryAR["HName"] = 'الاسم :';
DataDictionaryAR["HDescription"] = 'الوصف :';
DataDictionaryAR["AddSMSConfiguration"] = 'اضافة لاعدادات الرسائل';
DataDictionaryAR["SMSConfiguration"] = 'اعدادات الرسائل';
DataDictionaryAR["Remove"] = 'ازاله';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////Template Details  section////////////////////////////////////////////////////////////////
DataDictionaryAR["TemplateDetails"] = 'تفاصيل النماذج';
DataDictionaryAR["TemplateName"] = 'اسم النموذج';
DataDictionaryAR["Type"] = 'النوع';
DataDictionaryAR["Email"] = 'البريد الالكتروني';
DataDictionaryAR["SMS"] = 'الرسائل النصيه';
DataDictionaryAR["TermsAndCondition"] = 'Terms and conditions';
DataDictionaryAR["Description"] = 'الوصف';
DataDictionaryAR["Template"] = 'النموذج';
DataDictionaryAR["SMSTemplate"] = 'نموذج الرساله النصيه';
DataDictionaryAR["maximumLengthForArabic"] = '** The maximum length for arabic characters is 110';
DataDictionaryAR["maximumLengthForEnglish"] = '** The maximum length for english characters is 300';
DataDictionaryAR["HTemplateName"] = 'اسم النموذج : ';
DataDictionaryAR["HType"] = 'النوع : ';
DataDictionaryAR["NotificationTemplates"] = 'نماذج الاشعارات';
DataDictionaryAR["AddTemplate"] = 'اضافة نموذج';
DataDictionaryAR["TemplateErrorMsg"] = "الرجاءادخال نص";
DataDictionaryAR["Error"] = "خطأ";
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////Send Notification section////////////////////////////////////////////////////////
DataDictionaryAR["Details"] = 'التفاصيل';
DataDictionaryAR["Subject"] = 'الموضوع';
DataDictionaryAR["Description"] = 'الوصف';
DataDictionaryAR["Company"] = 'الشركه';
DataDictionaryAR["Person"] = 'الشخص';
DataDictionaryAR["Group"] = 'المجموعه';
DataDictionaryAR["All"] = 'الكل';
DataDictionaryAR["Email&Template"] = 'البريد الالكتروني النماذج';
DataDictionaryAR["Type"] = 'النوع';
DataDictionaryAR["Email"] = 'البريد الالكتروني';
DataDictionaryAR["EmailType"] = 'نوع البريد الالكتروني';
DataDictionaryAR["SMS"] = 'رسائل نصيه';
DataDictionaryAR["BusinessEmail"] = 'البريد الالكتروني للعمل';
DataDictionaryAR["PersonalEmail"] = 'البريد الالكتروني الشخصي';
DataDictionaryAR["Both"] = 'كلاهما';
DataDictionaryAR["Template"] = 'النموذج';
DataDictionaryAR["SendSMSAndEmails"] = 'ارسال رسائل نصيه وبريد الالكتروني';
DataDictionaryAR["Your"] = '';
DataDictionaryAR["email"] = 'البريد الالكتروني';
DataDictionaryAR["sms"] = 'الرساله النصيه';
DataDictionaryAR["hasBeenSentSuccessfully"] = ' تم ارساله بنجاح';
DataDictionaryAR["pleaseSelectCompanyOrPersonOrGroup"] = 'الرجاء اختيار شركه او شخص او مجموعه';
DataDictionaryAR["Required"] = 'متطلب';
DataDictionaryAR["Contacts"] = 'جهات الاتصال';
DataDictionaryAR["Send"] = 'ارسال';
DataDictionaryAR["Reset"] = 'اعاده';
DataDictionaryAR["GroupDescriptionName"] = 'الوصف';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////Email Configuration section////////////////////////////////////////////////////////
DataDictionaryAR["EmailConfigurationDetails"] = 'تفاصيل اعداد البريد الالكتروني';
DataDictionaryAR["EmailConfiguration"] = 'اعداد البريد الالكتروني';
DataDictionaryAR["AddEmailConfiguration"] = 'اضافة اعداد للبريد الالكتروني';
DataDictionaryAR["Email"] = 'البريد الالكتروني';
DataDictionaryAR["DisplayName"] = 'من';
DataDictionaryAR["UserName"] = 'اسم المستخدم';
DataDictionaryAR["Password"] = 'الرقم السري';
DataDictionaryAR["Host"] = 'الخادم';
DataDictionaryAR["Port"] = 'المنفذ';
DataDictionaryAR["EnableSSL"] = 'تفعيل SSL';
DataDictionaryAR["Timeout"] = 'المده';
DataDictionaryAR["SendInternalNotifications"] = 'ارسال الاشعارات الداخليه';
DataDictionaryAR["Description"] = 'الوصف';
DataDictionaryAR["HEmail"] = 'البريد الالكتروني : ';
DataDictionaryAR["HDisplayName"] = 'من : ';
DataDictionaryAR["Description"] = 'الوصف';
DataDictionaryAR["AlreadyExists"] = 'موجود مسبقا';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////Group section////////////////////////////////////////////////////////
DataDictionaryAR["GroupDetails"] = 'تفاصيل المجموعات';
DataDictionaryAR["FullView"] = 'عرض كل التفاصيل';
DataDictionaryAR["GroupName"] = 'اسم المجموعه';
DataDictionaryAR["Entity"] = 'الكيان';
DataDictionaryAR["Description"] = 'الوصف';
DataDictionaryAR["HEntity"] = 'الكيان : ';
DataDictionaryAR["HGroupName"] = 'اسم المجموعه : ';
DataDictionaryAR["ContactsGroups"] = 'مجموعات الاتصال';
DataDictionaryAR["AddContactsGroup"] = 'اضافة مجموعة';
DataDictionaryAR["ShowResult"] = 'رؤية النتائج';
DataDictionaryAR["SearchCriteria"] = 'معايير البحث';
DataDictionaryAR["Field"] = 'الحقل';
DataDictionaryAR["Value1"] = 'القيمه 1';
DataDictionaryAR["Value2"] = 'القيمه 2';
DataDictionaryAR["Yes"] = 'نعم';
DataDictionaryAR["No"] = 'لا';
DataDictionaryAR["All"] = 'الكل';
DataDictionaryAR["Filter"] = 'تصفيه';
DataDictionaryAR["Operation"] = 'العمليه';
DataDictionaryAR["Group"] = 'مجموعه';
DataDictionaryAR["Options"] = 'خيارات';
DataDictionaryAR["AND"] = ' و ';
DataDictionaryAR["OR"] = ' او ';
DataDictionaryAR["Start"] = 'يبدأ';
DataDictionaryAR["End"] = 'ينتهي';
DataDictionaryAR["Fields"] = 'الحقول';
DataDictionaryAR["Add"] = 'اضافه';
DataDictionaryAR["Clear"] = 'مسح';
DataDictionaryAR["True"] = 'صح';
DataDictionaryAR["False"] = 'خطأ';
DataDictionaryAR["GroupContacts"] = 'مجموعات الاتصال';
DataDictionaryAR["Mobile"] = 'موبايل';
DataDictionaryAR["BusinessEmail"] = 'البريد الالكتروني للعمل';
DataDictionaryAR["ContactName"] = 'اسم جهة الاتصال';
DataDictionaryAR["Contacts"] = 'جهات الاتصال';

DataDictionaryAR["BracketsCountError"] = 'Brackets Count Error';
DataDictionaryAR["BracketsCountErrorMsg"] = 'No. of opened bracket doesn\'t match no. of closed brackets';
DataDictionaryAR["BracketsError"] = 'Brackets Error';
DataDictionaryAR["BracketsCombinationError"] = 'Brackets combination error';
DataDictionaryAR["Equal"] = 'يساوي';
DataDictionaryAR["Contains"] = 'يحتوي';
DataDictionaryAR["StartWith"] = 'يبدأ ب';
DataDictionaryAR["EndWith"] = 'ينتهي ب';
DataDictionaryAR["Between"] = 'بين';
DataDictionaryAR["Before"] = 'قبل';
DataDictionaryAR["After"] = 'بعد';
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////ReturnedItem/////////////////////////////////////////////////////
DataDictionaryAR["ReturnedItem"] = 'العناصر المرجعة';
DataDictionaryAR["AddReturnedItem"] = 'اضافة سلعه مرجعه';
DataDictionaryAR['ReturnedItemDetails'] = ' تفاصيل العناصر المرجعة';
DataDictionaryAR['ReturnedItem'] = 'العناصر المرجعة';
DataDictionaryAR['SalesOrder'] = 'طلبية بيع';
DataDictionaryAR['OrderNumber'] = 'رقم الطلب';
DataDictionaryAR['ReturnReason'] = 'سبب الارجاع';
DataDictionaryAR["AlreadyExist"] = ' موجود مسبقا ';
DataDictionaryAR["TotalReturnedItems"] = 'مجموع العناصر المرجعة';
DataDictionaryAR["ModelNumber"] = 'رقم النموذج';
DataDictionaryAR["ProductName"] = 'اسم المنتج';
DataDictionaryAR["Quantity"] = 'الكمية';
DataDictionaryAR["ReturnQuantity"] = 'الكمية المرجعة';
DataDictionaryAR["ReturnLocation"] = 'موقع الارجاع';
DataDictionaryAR["ErrorAddReturndItem"] = 'لايمكن ارجاع هذا العنصر';
DataDictionaryAR['Details'] = 'تفاصيل';
DataDictionaryAR['AddNewDetails'] = 'اضافة تفاصيل';
DataDictionaryAR['TotalReturendItems'] = 'المجموع';
DataDictionaryAR['DeliveredQuantity'] = 'الكمية المستلمة';
DataDictionaryAR['QuantityError'] = 'لايمكن تجاوز الكمية المستلمة';


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////Inventory//////////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryAR["Inventory"] = 'المنتج';
DataDictionaryAR["AddInventory"] = 'اضافة منتج';
DataDictionaryAR['InventoryDetails'] = ' تفاصيل منتج';
DataDictionaryAR['Inventory'] = 'منتج';
DataDictionaryAR['ProductName'] = 'اسم المنتج';
DataDictionaryAR['ProductNameAr'] = 'اسم المنتج عربي';
DataDictionaryAR['Description'] = 'الوصف';
DataDictionaryAR['Image'] = 'صورة';
DataDictionaryAR['Cost'] = 'السعر';
DataDictionaryAR['Unit'] = 'وحدة';
DataDictionaryAR['DefaultWarrantyInMonths'] = 'الكفالة بالاشهر';
DataDictionaryAR['ModelNumber'] = 'رقم النموذج';
DataDictionaryAR['AvailableQuantity'] = 'الكمية المتوفرة';
DataDictionaryAR['AvailableQuantityBonded'] = 'الكمية المتوفرة في بوندد';
DataDictionaryAR['AvailableQuantityKhalda'] = 'الكمية المتوفرة خلدا';
DataDictionaryAR['QuantityOnSalesOrder'] = 'الكمية في ترتيب المبيعات';
DataDictionaryAR['QuantityOrderedOnWay'] = 'الكمية المطلوبة في الطريق';
DataDictionaryAR['SuspendedItem'] = 'منتج موقوف';
DataDictionaryAR['Sku'] = 'SKU';
DataDictionaryAR['RequireSerialNumberAtDelivery'] = 'تتطلب الرقم التسلسلي عند التسليم';
DataDictionaryAR['StockableItem'] = 'قابل للتخزين';
DataDictionaryAR['ArabicDescription'] = 'الوصف بالعربي';
DataDictionaryAR['ProductCategory'] = 'فئة المنتج';
DataDictionaryAR['ProductSubCategory'] = 'فئة المنتج الفرعية';
DataDictionaryAR["AlreadyExist"] = ' موجود مسبقا ';
DataDictionaryAR['PriceList'] = 'لائحة الاسعار';
DataDictionaryAR['AddNewPricelist'] = 'اضافةلائحة اسعار';
DataDictionaryAR['ParentProduct'] = 'المنتج الرئيسي';
DataDictionaryAR["ImageFormatIsNotAllowed"] = ' نوع الصورة غير مسموح ';
DataDictionaryAR['ImageSizeError'] = 'Image size error';
DataDictionaryAR['MaximumImageSize'] = 'Maximum size is';
DataDictionaryAR['kb'] = ' kb';
// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// /////////////////////////////////////InventoryKit/////////////////////////////////////////////////////
DataDictionaryAR['ModelNumber'] = 'رقم المنتج';
DataDictionaryAR['Inventory'] = 'المنتج';

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////InventoryPriceList//////////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryAR["InventoryPriceList"] = 'لائحة اسعار المنتجات';
DataDictionaryAR["AddInventoryPriceList"] = 'اضافة لائحة اسعار';
DataDictionaryAR['InventoryPriceListDetails'] = ' تفاصيل لائحة الاسعار';
DataDictionaryAR['InventoryPriceList'] = 'لائحة اسعار المنتج';
DataDictionaryAR['PriceList'] = 'لائحة الاسعار';
DataDictionaryAR['Currency'] = 'العملة';
DataDictionaryAR['ListedPrice'] = 'السعر المسجل';
DataDictionaryAR['MinimumPrice'] = 'الحد الادنى للسعر';
DataDictionaryAR['MaximumPrice'] = 'الحد الاعلى للسعر';
DataDictionaryAR['Inventory'] = 'المخزن';
DataDictionaryAR['ValidatePrice'] = ' يجب أن يكون أقل من السعر الأقصى';
DataDictionaryAR["AlreadyExist"] = ' موجود مسبقا ';




///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////Asset eArabic Dictionary/////////////////////////////////////////////////////
DataDictionaryAR["Asset"] = 'Asset';
DataDictionaryAR["AddAsset"] = 'اضافة Asset';
DataDictionaryAR['AssetDetails'] = ' تفاصيل Asset';
DataDictionaryAR['Asset'] = 'الثوابت';
DataDictionaryAR['#'] = '#';
DataDictionaryAR['Description'] = 'الوصف';
DataDictionaryAR['Categroy'] = 'الفئة';
DataDictionaryAR['Location'] = 'الموقع';
DataDictionaryAR['AcquisitionDate'] = 'Acquisition Date';
DataDictionaryAR['OriginalValue'] = 'القيمة الاصلية';
DataDictionaryAR['AddOn'] = 'اضافة على';
DataDictionaryAR['Total'] = 'المجموع';
DataDictionaryAR['DepreciationRate'] = 'Depreciation Rate';
DataDictionaryAR['MonthlyDepreciation'] = 'Monthly Depreciation';
DataDictionaryAR['AccumulatedDepreciation'] = 'Accumulated Depreciation';
DataDictionaryAR['NetValue'] = 'Net Value';
DataDictionaryAR['Source'] = 'Source';

DataDictionaryAR["AlreadyExist"] = ' موجود مسبقا ';
DataDictionaryAR["LineNumber"] = 'رقم السطر';

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////InventoryStock/////////////////////////////////////////////////////
DataDictionaryAR["InventoryStock"] = 'جرد المخزون';
DataDictionaryAR["AddInventoryStock"] = 'اضافة جرد المخزون';
DataDictionaryAR['InventoryStockDetails'] = ' تفاصيل جرد المخزون';
DataDictionaryAR['InventoryStock'] = 'جرد المخزون';
DataDictionaryAR['Inventory'] = 'المنتج / رقم النموذج ';
DataDictionaryAR['TransactionType'] = 'نوع العملية';
DataDictionaryAR['MoveFrom'] = 'نقل من';
DataDictionaryAR['MoveTo'] = 'نقل الى';
DataDictionaryAR['Quantity'] = 'الكمية';
DataDictionaryAR['Details'] = 'تفاصيل';
DataDictionaryAR['Stock'] = 'المخزن';
DataDictionaryAR["AlreadyExist"] = ' موجود مسبقا ';

DataDictionaryAR['QtyValidation'] = 'Quantity musn\'t be more than availabe quantity ';
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////Kit/////////////////////////////////////////////////////
DataDictionaryAR["Kit"] = 'معدات';
DataDictionaryAR["AddKit"] = 'اضافة معدات';
DataDictionaryAR['KitDetails'] = ' تفاصيل معدات';
DataDictionaryAR['Kit'] = 'معدات';
DataDictionaryAR['KitName'] = 'اسم المعدة';
DataDictionaryAR['KitDescription'] = 'الوصف';
DataDictionaryAR['KitModelNumber'] = 'رقم المعدة';
DataDictionaryAR["AlreadyExist"] = ' موجود مسبقا ';
DataDictionaryAR['Inventory'] = 'المخزن';
DataDictionaryAR['AddNewInventory'] = 'اضافة مخزن';
DataDictionaryAR['Pricelist'] = 'لائحة الاسعار';
DataDictionaryAR['AddNewPricelist'] = 'اضافة لائحة اسعار';
DataDictionaryAR['Product'] = 'المنتج /رقم نموذج ';


// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////KitPriceList//////////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryAR["InventoryPriceList"] = 'لائحة اسعار المعدات';
DataDictionaryAR["AddInventoryPriceList"] = 'اضافة لائحة اسعار المعدات';
DataDictionaryAR['InventoryPriceListDetails'] = ' تفاصيل لائحة الاسعار';
DataDictionaryAR['InventoryPriceList'] = 'لائحة اسعار المعدات';
DataDictionaryAR['PriceList'] = 'لائحة الاسعار';
DataDictionaryAR['Currency'] = 'العملة';
DataDictionaryAR['ListedPrice'] = 'السعر المسجل';
DataDictionaryAR['MinimumPrice'] = 'الحد الادنى للسعر';
DataDictionaryAR['MaximumPrice'] = 'الحد الاقصى للسعر';
DataDictionaryAR['Inventory'] = 'المنتج';
DataDictionaryAR['ValidatePrice'] = ' يجب أن يكون أقل من السعر الأقصى';
DataDictionaryAR["AlreadyExist"] = ' موجود مسبقا ';


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ////////////////////////////////////////Category///////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryAR["Category"] = 'الفئة';
DataDictionaryAR["AddCategory"] = 'اضافة فئة';
DataDictionaryAR['CategoryDetails'] = ' تفاصيل الفئة';
DataDictionaryAR['Category'] = 'الفئة';
DataDictionaryAR['CategoryNameEn'] = 'اسم الفئة';
DataDictionaryAR['CategoryNameAr'] = 'اسم الفئة بالعربي';
DataDictionaryAR['DescriptionAr'] = 'الوصف بالعربي';
DataDictionaryAR['DescriptionEn'] = 'الوصف';
DataDictionaryAR['Entity'] = 'الكيان';
DataDictionaryAR["AlreadyExist"] = ' موجود مسبقا ';
DataDictionaryAR['CategoryNameEn'] = 'اسم الفئة';
DataDictionaryAR['CategoryNameAr'] = 'اسم الفئة بالعربي';

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////SubCategory//////////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryAR["SubCategory"] = 'الفئة الفرعية';
DataDictionaryAR["AddSubCategory"] = 'اضافة فئة فرعية';
DataDictionaryAR['SubCategoryDetails'] = 'تفاصيل الفئة الفرعية';
DataDictionaryAR['SubCategory'] = 'الفئة الفرعية';
DataDictionaryAR['SubCategoryValueAr'] = 'الفيمة بالعربي';
DataDictionaryAR['SubCategoryValueEn'] = 'القيمة';
DataDictionaryAR['DescriptionAr'] = 'الوصف بالعربي';
DataDictionaryAR['DescriptionEn'] = 'الوصف';
DataDictionaryAR['Parent'] = 'الاب';
DataDictionaryAR['SubCategoryAr'] = 'الفئة الفرعية';
DataDictionaryAR['AddNewSubCategory'] = 'اضافة فئة فرعية';
DataDictionaryAR["AlreadyExist"] = ' موجود مسبقا ';
DataDictionaryAR["ParentSubCategory"] = 'الفئة';
DataDictionaryAR['SubCategoryValueAr'] = 'الفيمة بالعربي';
DataDictionaryAR['SubCategoryValueEn'] = 'الفيمة';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// /////////////////////////////////////////Fixed Assets///////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryAR["AddFixedAssets"] = 'اضافه ممتلكات';
DataDictionaryAR["TypeName"] = 'نوع الممتلكات';
DataDictionaryAR["Warranty"] = 'الكفاله';
DataDictionaryAR["Model"] = 'الموديل';
DataDictionaryAR["Location"] = 'الموقع';
DataDictionaryAR["SerialNumber"] = 'الرقم التسلسلي';
DataDictionaryAR["License"] = 'الرخصة';
DataDictionaryAR["ProcurementDate"] = 'تاريخ التحصيل';
DataDictionaryAR["LicenseExpirationDate"] = 'تاريخ انتهاء الرخصة';
DataDictionaryAR["Notes"] = 'الملاحظات';
DataDictionaryAR["FixedAssetsDetails"] = 'تفاصيل الممتلكات';
DataDictionaryAR["FixedAssets"] = 'الممتلكات';
DataDictionaryAR["TypeNameHeader"] = " 'نوع الممتلكات : ";
DataDictionaryAR["SerialNumberHeader"] = " الرقم التسلسلي : ";


////////////////////////////////////////////////////////////////////////////////
///////////////////////////////Reports/////////////////////////////////////////
DataDictionaryAR["CompanyListReport"] = "تقرير لائحه الشركات"
DataDictionaryAR["PersonName"] = "اسم الشخص بالانجليزيه"
DataDictionaryAR["PersonNameAr"] = "اسم الشخص بالعربيه"
DataDictionaryAR["PersonListReport"] = "تقرير لائحه الاشخاص";
DataDictionaryAR["CompanyListReport"] = "تقرير لائحه الشركات"
DataDictionaryAR["PersonName"] = "اسم الشخص بالانجليزيه"
DataDictionaryAR["PersonNameAr"] = "اسم الشخص بالعربيه"
DataDictionaryAR["PersonListReport"] = "تقرير لائحه الاشخاص";
DataDictionaryAR["TaskListReport"] = "تقرير المهمة المجدولة";


DataDictionaryAR['CompanyNameEnglish'] = 'اسم الشركه'
DataDictionaryAR["InterestedIn"] = "مهتم بي";
DataDictionaryAR["ContactType"] = "نوع الاتصال";
DataDictionaryAR["Anniversary"] = "ذكرى الزفاف";
DataDictionaryAR["Nationality"] = "الجنسيه";
DataDictionaryAR["NotifyforBirthday"] = "تذكير عيد الميلاد";
DataDictionaryAR["NotifyForAnniversary"] = "تذكير ذكرى الزفاف"
DataDictionaryAR["FixedAssetsReport"] = 'تقرير لائحه الممتلكات';
DataDictionaryAR["CreatedDateFrom"] = "تاريخ الانشاء من";
DataDictionaryAR["CreatedDateTo"] = "تاريخ الانشاء الى";



DataDictionaryAR["Customer"] = 'العميل';

DataDictionaryAR["false"] = 'لا';
DataDictionaryAR["true"] = 'نعم';
DataDictionaryAR["Http"] = 'http://'




//RUBA
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// IcUnitOfMeasures English Dictionary////////////////////////////////////////////////
DataDictionaryAR["IcUnitOfMeasures"] = 'وحدة القياس';
DataDictionaryAR["AddIcUnitOfMeasures"] = 'اضافة وحدة القياس';
DataDictionaryAR['IcUnitOfMeasuresDetails'] = 'تفاصيل وحدة القياس';
DataDictionaryAR['UnitOfMeasure'] = 'وحدة القياس';
DataDictionaryAR['DefaultConversionFactor'] = 'معامل التحويل';
DataDictionaryAR["AlreadyExist"] = 'موجود';
DataDictionaryAR["HUnitOfMeasure"] = "وحدة القياس: ";
DataDictionaryAR["HConversionFactor"] = "معامل التحويل: ";

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// IcLocation English Dictionary////////////////////////////////////////////////
DataDictionaryAR["IcLocation"] = 'مستودع';
DataDictionaryAR["AddIcLocation"] = 'اضافة مستودع';
DataDictionaryAR['IcLocationDetails'] = 'تفاصيل المستودع';
DataDictionaryAR['Location'] = 'مستودع';
DataDictionaryAR['LocationCode'] = 'رمز المستودع';
DataDictionaryAR['LocationName'] = 'اسم المستودع';
DataDictionaryAR['LocationAddress'] = 'العنوان';
DataDictionaryAR['LocationCountry'] = 'الدولة';
DataDictionaryAR['LocationCity'] = 'المدينة';
DataDictionaryAR['LocationPhone'] = 'الهاتف';
DataDictionaryAR['LocationFax'] = 'رقم الفاكس';
DataDictionaryAR['LocationEmail'] = 'البريد الالكتروني';
DataDictionaryAR['LocationType'] = 'النوع';
DataDictionaryAR['LocationContactName'] = 'اسم الشخص';
DataDictionaryAR['LocationContactPhone'] = 'هاتف الشخص';
DataDictionaryAR['LocationContactFax'] = 'رقم الفاكس الخاص بالشخص';
DataDictionaryAR['LocationContactEmail'] = 'البريد الالكتروني الخاص بالشخص';
DataDictionaryAR['LocationStatus'] = 'الحالة';
DataDictionaryAR["AlreadyExist"] = ' موجود بالفعل ';
DataDictionaryAR["Contact"] = ' الشخص ';
DataDictionaryAR["HLocationCode"] = 'رمز المستودع :   ';
DataDictionaryAR['HLocationName'] = 'اسم المستودع : ';
DataDictionaryAR['HLocationType'] = 'النوع : ';
DataDictionaryAR['Integration'] = 'تزامن';
DataDictionaryAR['LocationIntegration'] = 'Override GL Segments';
DataDictionaryAR['OverrideGLSegments'] = 'Override GL Segments';



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// IcWarrantyInfo English Dictionary////////////////////////////////////////////////
DataDictionaryAR["IcWarrantyInfo"] = ' الكفالة';
DataDictionaryAR["AddIcWarrantyInfo"] = 'اضافة كفالة';
DataDictionaryAR['IcWarrantyInfoDetails'] = 'تفاصيل الكفالة';
DataDictionaryAR['WarrantyInfo'] = 'الكفالة ';
DataDictionaryAR['WarrantyCode'] = 'رمز الكفالة';
DataDictionaryAR['WarrantyDescription'] = 'شرح الكفالة';
DataDictionaryAR["AlreadyExist"] = ' موجود بالفعل ';
DataDictionaryAR['IsLifeTime'] = 'كفالة مدى الحياة ';
DataDictionaryAR['WarrantyDays'] = 'أيام الكفالة';
DataDictionaryAR['EffictiveDays'] = 'أيام تفعيل الكفالة';
DataDictionaryAR['HWarrantyCode'] = 'رمز الكفالة: ';
DataDictionaryAR['HWarrantyDays'] = 'أيام الكفالة : ';
DataDictionaryAR['HIsLifeTime'] = 'كفالة مدى الحياة : ';


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// IcSegment English Dictionary////////////////////////////////////////////////
DataDictionaryAR["Segmant"] = "قطاع";
DataDictionaryAR["SegmentNumber"] = "قطاع #";
DataDictionaryAR["SegmentName"] = "اسم القطاع";
DataDictionaryAR["SegmentLength"] = "طول القطاع";
DataDictionaryAR["SegmentCode"] = "رمز القطاع";
DataDictionaryAR['IcMaximumNumberOfSegmentsAllowed'] = 'الحد الأقصى لعدد القطاعات هو '
DataDictionaryAR['IcSegmentEditingDisabled'] = 'Editing disabled, All Segments are linked to other module';
DataDictionaryAR['IcSegmentNameMustBeUnique'] = 'يجب ان يكون اسم القطاع فريد من نوعه';
DataDictionaryAR['IcSegmentLengthMustBe'] = 'يجب ان يكون الطول  ';
DataDictionaryAR['#'] = '#';
DataDictionaryAR['HasReference'] = 'يوجد مرجع';
DataDictionaryAR['SegmentNameH'] = 'اسم القطاع: ';
DataDictionaryAR['SegmentCodeH'] = 'رمز القطاع: ';
DataDictionaryAR['IcSegmentReferenceSegmentVldMsg'] = 'لا يمكن انشاء مرجع الى نفس القطاع';
DataDictionaryAR['ReferenceSegment'] = 'مرجع القطاع';



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// IcSegmentCode English Dictionary////////////////////////////////////////////////
DataDictionaryAR["SegmentCodeDescription"] = "شرح رمز القطاع";
DataDictionaryAR["SegmentCode"] = "رمز القطاع";
DataDictionaryAR['SegmentName'] = 'اسم القطاع';
DataDictionaryAR['SegmentCodeDetails'] = 'تفاصيل رمز القطاع';
DataDictionaryAR['AddNewSegmentCode'] = 'اضافة رمز قطاع جديد';
DataDictionaryAR['ReferenceSegment'] = 'مرجع القطاع';
DataDictionaryAR['ReferenceSegmentCode'] = 'رمز مرجع القطاع';
DataDictionaryAR['ReferenceSegmentCodeDescription'] = 'شرح رمز مرجع القطاع';
DataDictionaryAR['IcSegmentCode'] = 'رمز القطاع : ';
DataDictionaryAR['IcSegmentCodeDescription'] = 'شرح رمز القطاع : ';



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// ItemStructure English Dictionary////////////////////////////////////////////////
DataDictionaryAR["ItemStructure"] = 'تركيب رقم المادة';
DataDictionaryAR["AddItemStructure"] = 'اضافة تركيب رقم المادة';
DataDictionaryAR['ItemStructureDetails'] = 'تفاصيل تركيب رقم المادة';
DataDictionaryAR['ItemStucture'] = 'تركيب رقم المادة';
DataDictionaryAR['ItemStructureCode'] = 'رمز تركيب رقم المادة';
DataDictionaryAR['ItemStructureDescription'] = 'شرح';
DataDictionaryAR['ItemStructureDelimiter'] = 'الفاصل';

DataDictionaryAR["AlreadyExist"] = ' موجود بالفعل ';
//////List Header English Dictionary JS
DataDictionaryAR['ItemStructureCode'] = 'رمز تركيب رقم المادة';
DataDictionaryAR["HItemStructureCode"] = 'رمز تركيب رقم المادة : ';
DataDictionaryAR["HItemStructureDescription"] = 'شرح : ';
DataDictionaryAR["HItemStructureDelimiter"] = 'الفاصل : ';
DataDictionaryAR["ItemManagement"] = "ادارة المادة ";
DataDictionaryAR['IcItemStructureLengthExceeds'] = 'طول رمز تركيب رقم المادة يتعدى  ';



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// IcPriceList English Dictionary////////////////////////////////////////////////
DataDictionaryAR["IcPriceList"] = 'قائمة الاسعار';
DataDictionaryAR["AddIcPriceList"] = 'اضافة قائمة اسعار';
DataDictionaryAR['IcPriceListDetails'] = 'تفاصيل قائمة الاسعار ';
DataDictionaryAR['PriceList'] = 'قائمة الاسعار';
DataDictionaryAR['PriceListCode'] = 'رمز قائمه الاسعار';
DataDictionaryAR['PriceListDescription'] = 'شرح قائمة الاسعار';
DataDictionaryAR['PriceBy'] = 'السعر بناءا على';
DataDictionaryAR['PriceDecimals'] = 'الخانات العشريه الخاصة للسعر';
DataDictionaryAR['RoundingMethod'] = 'طريقة الترقيم';
DataDictionaryAR['SellingPriceBasedOn'] = 'سعر البيع بناءا على';
DataDictionaryAR['DiscountOnPriceBy'] = 'الخصم على السعر بناءا على';
DataDictionaryAR['PricingDeterminedBy'] = 'تحديد السعر باستخدام';
DataDictionaryAR["AlreadyExist"] = ' موجود بالفعل ';
//////List Header English Dictionary JS
DataDictionaryAR['PriceListCode'] = 'رمز قائمة الاسعار';

////Tab English Dictionary JS
DataDictionaryAR["HPriceListCode"] = "رمز قائمة الاسعار : ";
DataDictionaryAR["HPriceListDescription"] = "شرح قائمة الاسعار : ";
DataDictionaryAR["HPriceBy"] = "السعر يناءا على : ";
DataDictionaryAR["Taxes"] = "الضرائب";
DataDictionaryAR["CustomerTaxClass"] = "فئة الضريبه الخاصة بالعميل";



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// IcAccountSet English Dictionary////////////////////////////////////////////////
DataDictionaryAR["IcAccountSet"] = 'مجموعة الحسابات';
DataDictionaryAR["AddIcAccountSet"] = 'اضافة مجموعة حسابات';
DataDictionaryAR['IcAccountSetDetails'] = 'تفاصيل مجموعة الحسابات';
DataDictionaryAR['HAccountSetCode'] = 'رمز مجموعة الحسابات: ';
DataDictionaryAR['HDescription'] = 'الشرح: ';
DataDictionaryAR['AccountSet'] = 'مجموعة حسابات';
DataDictionaryAR['AccountSetCode'] = 'رمز مجموعة الحسابات';
DataDictionaryAR['Description'] = 'الشرح';
DataDictionaryAR['CostingMethod'] = 'طريقة التكليف';
DataDictionaryAR['InventoryControlAccount'] = 'حساب المخزون';
DataDictionaryAR['PayableClearingAccount'] = 'حساب الذمم الدائنه الوسيط';
DataDictionaryAR['AdjustmentWriteOffAccount'] = 'حساب التسويات';
DataDictionaryAR['NonStockClearingAccount'] = 'الحساب الوسيط الخاص بالمواد الغير مخزنة';
DataDictionaryAR['TransferClearingAccount'] = 'حساب التحويلات الوسيط';
DataDictionaryAR['ShipmentClearingAccount'] = 'حساب الاخراج الوسيط';



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// IcCategory English Dictionary////////////////////////////////////////////////
DataDictionaryAR["IcCategory"] = 'المجموعة';
DataDictionaryAR["CategoryCode"] = 'رمز المجموعة';
DataDictionaryAR['IcCategoryDetails'] = "تفاصيل المجموعة";
DataDictionaryAR["HCategoryCode"] = 'رمز المجموعة: ';
DataDictionaryAR["AddIcCategory"] = "اضافة مجموعة";
DataDictionaryAR["SalesAccount"] = "المبيعات";
DataDictionaryAR["ReturnsAccount"] = "المرتجعات";
DataDictionaryAR["CostOfGoodsSoldAccount"] = "تكلفة البضاعة المباعة ";
DataDictionaryAR["CostVarianceAccount"] = "فروقات التكلفة";
DataDictionaryAR["DamagedGoodsAccount"] = "البضاعة التالفة";
DataDictionaryAR["InternalUsageAccount"] = "الاستخدام الداخلي";
DataDictionaryAR["UOM"] = "وحدة القياس";
DataDictionaryAR["Discounts"] = "الخصومات";



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// /////////////////////////////////////////InventoryOptions///////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryAR["IcOptions"] = 'خيارات';
DataDictionaryAR["Processing"] = 'تنفيذ';
DataDictionaryAR["DocumentNumbers"] = 'ارقام المستندات';
DataDictionaryAR['MaxNoOfIcSegments'] = 'الحد الاعلى للقطاعات';
DataDictionaryAR['MaxNoOfIcSegmentCodes'] = 'الحد الاعلى لرموز القطاع لكل قطاع';
DataDictionaryAR['ICOptions'] = "خيارات التحكم بالمخزون";
DataDictionaryAR['Processing'] = "تنفيذ";
DataDictionaryAR['DefaultRateType'] = "Default Rate Type";
DataDictionaryAR['MultiCurrency'] = " متعدد عملا";
DataDictionaryAR['ValidateLotExpiryDate'] = "Validate Lot Expiry Date";

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// IcAdditionalCostType English Dictionary////////////////////////////////////////////////
DataDictionaryAR["IcAdditionalCostType"] = 'نوع التكلفة الاضافية';
DataDictionaryAR["AddIcAdditionalCostType"] = 'اضافة نوع تكلفة اضافية';
DataDictionaryAR['IcAdditionalCostTypeDetails'] = 'تفاصيل نوع التكلفة الاضافية';
DataDictionaryAR['AdditionalCostType'] = 'نوع التكلفة الاضافية';
DataDictionaryAR['AdditionalCost'] = 'التكلفة الاضافية';
DataDictionaryAR['Account'] = 'الحساب';
DataDictionaryAR['TaxClass'] = 'فئة الضريبة';
DataDictionaryAR["AlreadyExist"] = ' موجود حاليا ';
DataDictionaryAR["IncludeExculdeProration"] = "تشمل التناسب";
DataDictionaryAR["DocumentReference"] = "مرجع المستند";
DataDictionaryAR["ValidateLength"] = "التحقق من صحة الطول";

DataDictionaryAR["IcReceiptReturnDetails"] = "سندات الارجاع";
DataDictionaryAR["AddIcReceiptReturn"] = "اضافة ارجاع ";
DataDictionaryAR["IcReceiptReturn"] = "سندات الارجاع ";
DataDictionaryAR["ReturnReference"] = "مرجع ";
DataDictionaryAR["ParentReceipt"] = "ParentReceipt";
DataDictionaryAR["ReturnAllQuantity"] = "ارجاع جميع الكميات";
DataDictionaryAR["Shipment"] = "Shipment";
DataDictionaryAR["ReturnedPrice"] = "Returned Price";
DataDictionaryAR["Return"] = "ارجاع";



//********************************************************************************************************************************
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////ERP Section///////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////Common Services////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryAR["CommonServices"] = "اعدادات عامة";
DataDictionaryAR["TAXSERVICES"] = "خدمات الضريبة";
DataDictionaryAR["TaxAuthority"] = "هيئة الضريبة";
DataDictionaryAR["TaxAuthorityCode"] = "رمز هيئة الضريبة";
DataDictionaryAR["Description"] = "الوصف";
DataDictionaryAR["ReportingCurrency"] = "عملة التقارير";
DataDictionaryAR["MaximumTaxAllowable"] = "الحد الاقصى من الضرائب المسموحة";
DataDictionaryAR["MinimumTaxAllowable"] = "الحد الادنى من الضرائب المسموحة";
DataDictionaryAR["AllowTaxInPrice"] = "السماح للضرائب في الاسعار";
DataDictionaryAR["TaxBase"] = "قاعدة الضرائب";
DataDictionaryAR["LiabilityAccount"] = "حساب ضريبة الدخل";
DataDictionaryAR["TaxRecoverable"] = "الضريبة مستردة";
DataDictionaryAR["RecoverableAccount"] = "حساب الضريبة المستردة";
DataDictionaryAR["RecoverablePercentage"] = "نسبة الاسترداد";
DataDictionaryAR["ExpenseSeparately"] = "فصل الضريبة على المصاريف";
DataDictionaryAR["ExpenseAccount"] = "حساب مصروف الضريبة";
DataDictionaryAR["TaxClasses"] = "فئة الضريبة";
DataDictionaryAR["TaxAuthority"] = "هيئة الضريبة";
DataDictionaryAR["TransactionType"] = "نوع العملية";
DataDictionaryAR["ClassType"] = "نوع الفئة";
DataDictionaryAR["TaxRates"] = "نسبة الضريبة";
DataDictionaryAR["TaxGroups"] = "مجموعات الضرائب";
DataDictionaryAR["FiscalCalendar"] = "السنة المالية";
DataDictionaryAR["Currency"] = "العملة";
DataDictionaryAR["CurrencyRate"] = "أسعار العملات";
DataDictionaryAR["DateMatched"] = "موعد المطابقة";
DataDictionaryAR["RateOperation"] = "العملية";
DataDictionaryAR["CompanyProfile"] = "نبذة عن الشركة";
DataDictionaryAR["FunctionalCurrency"] = "العملة الاساسية";
DataDictionaryAR["CompanyName"] = "اسم الشركة";
DataDictionaryAR["Address"] = "العنوان";
DataDictionaryAR["City"] = "المدينة";
DataDictionaryAR["Country"] = "الدولة";
DataDictionaryAR["Phone"] = "رقم الهاتف";
DataDictionaryAR["FaxNumber"] = "رقم الفاكس";
DataDictionaryAR["RegistrationNumber"] = "رقم التسجيل";
DataDictionaryAR["CompanyLogo"] = "شعار الشركة";
DataDictionaryAR["VATNumber"] = "الرقم الضريبي";
DataDictionaryAR["CurrencyExchangeRates"] = "اسعار صرف العملات";
DataDictionaryAR['FISCALCALENDAR'] = 'السنة المالية';
DataDictionaryAR['CURRENCIES'] = 'العملات';
DataDictionaryAR['COMPANYPROFILE'] = 'نبذة عن الشركة';
DataDictionaryAR['REPORTS'] = 'التقارير';
DataDictionaryAR['CurrencyExchangeRates'] = '';
DataDictionaryAR['TRANSACTIONALSETUP'] = 'اعدادات المعاملات';
DataDictionaryAR['AddTaxAuthority'] = 'اضافة هيئة الضريبة';
DataDictionaryAR['TaxAuthorityDefinition'] = 'تعريف هيئة الضريبة';
DataDictionaryAR['Clone'] = 'نسخ';
DataDictionaryAR['TaxAuthorityCode'] = 'رمز هيئة الضريبة';
DataDictionaryAR['Accounts'] = 'الحسابات';
DataDictionaryAR['TaxProfile'] = 'الضرائب';
DataDictionaryAR['Header'] = 'معلومات';
DataDictionaryAR['Details'] = 'تفاصيل';
DataDictionaryAR['AddTaxClassHeader'] = 'اضافة فئة ضريبة';
DataDictionaryAR['Classnumber'] = 'رقم الفئة';
DataDictionaryAR['Exempt'] = 'مشمولة';
DataDictionaryAR['Addnewdetails'] = 'اضافة تفاصيل جديدة';
DataDictionaryAR['TaxAuthorityDescription'] = 'وصف هيئة الضريبة';
DataDictionaryAR['vldMinimumTaxAllowable'] = 'يجب أن يكون الحد الأدنى المسموح به  أقل من الحد الأقصى المسموح به من الضرائب';
DataDictionaryAR['CurrencyExchangeRates'] = 'اسعار صرف العملات';
DataDictionaryAR['AddTaxClass'] = 'اضافة فئة ضريبة';
DataDictionaryAR['vldNoTaxRate'] = 'لايوجد نسبة الضريبة';
DataDictionaryAR['AddTaxRate'] = 'اضافة نسبة ضريبة';
DataDictionaryAR['vldNorowFound'] = 'لايوجد سطور';
DataDictionaryAR['vldOneRowFound'] = 'يوجد سطر واحد';
DataDictionaryAR['vldNoDetalilsFound'] = 'لا يوجد تفاصيل';
DataDictionaryAR['TaxGroupCode'] = 'رمز المجموعة';
DataDictionaryAR['AddTaxGroup'] = 'اضافة مجموعة ضرائب';
DataDictionaryAR['Authority'] = 'هيئة الضريبة';
DataDictionaryAR['IsTaxable'] = 'غير خاضع للضريبة';
DataDictionaryAR['AuthorityDescription'] = 'وصف هيئة الضريبة';
DataDictionaryAR['StartDate'] = 'تاريخ البدء';
DataDictionaryAR['EndDate'] = 'تاريخ الانتهاء';
DataDictionaryAR['Year'] = 'السنة';
DataDictionaryAR['Period'] = 'الفترة';
DataDictionaryAR['Locked'] = 'مغلقة';
DataDictionaryAR['Period1'] = 'الفترة1';
DataDictionaryAR['Period2'] = 'الفترة2';
DataDictionaryAR['Period3'] = 'الفترة3';
DataDictionaryAR['Period4'] = 'الفترة4';
DataDictionaryAR['Period5'] = 'الفترة5';
DataDictionaryAR['Period6'] = 'الفترة6';
DataDictionaryAR['Period7'] = 'الفترة7';
DataDictionaryAR['Period8'] = 'الفترة8';
DataDictionaryAR['Period9'] = 'الفترة9';
DataDictionaryAR['Period10'] = 'الفترة10';
DataDictionaryAR['Period11'] = 'الفترة11';
DataDictionaryAR['Period12'] = 'الفترة12';
DataDictionaryAR['AddFiscalCalendar'] = 'اضافة سنة محاسبية';
DataDictionaryAR['Symbol'] = 'رمز العملة';
DataDictionaryAR['DecimalPlaces'] = 'أماكن عشرية';
DataDictionaryAR['AddCurrency'] = 'اضافة عملة';
DataDictionaryAR['AddCurrencyRate'] = 'اضافة سعر صرف العملة';
DataDictionaryAR['LockAdjustmentPeriod'] = 'اقفال فترة التعديلات';

//////////////////////////////////////////////General Ledger////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryAR["GeneralLedger"] = "دفتر الأستاذ العام";
DataDictionaryAR["TRANSACTIONALSETUP"] = "اعدادات المعاملات";
DataDictionaryAR["MASTERDATASETUP"] = "اعدادات البيانات";
DataDictionaryAR["MASTERDATADEFINITION"] = "تعريف البيانات";
DataDictionaryAR["TRANSACTIONSPROCESSING"] = "معالجة المعاملات";
DataDictionaryAR["PERIODICPROCESSING"] = "المعالجة الدورية";
DataDictionaryAR["Options"] = "الخيارات";
DataDictionaryAR["AccountSegment"] = "قطاع الحساب";
DataDictionaryAR["AccountStructure"] = "هيكلة الحساب";
DataDictionaryAR["SegmentDelimiter"] = "محدد القطاع";
DataDictionaryAR["IsMultiCurrency"] = "متعدد العملات";
DataDictionaryAR["UseAccountClass"] = "استخدام فئة الحسابات";
DataDictionaryAR["DefaultClosingAccount"] = "الحساب الختامي";
DataDictionaryAR["ContractDebitAccount"] = "حساب العقد المدين";
DataDictionaryAR["DeferredIncomeAccount"] = "حساب الإيرادات المؤجلة";
DataDictionaryAR["EarnedRevenueAccount"] = "حساب الإيرادات المكتسبة";
DataDictionaryAR["Un-EarnedRevenueAccount"] = "حساب الإيرادات غير المكتسبة";
DataDictionaryAR["Posting"] = "الترحيل";
DataDictionaryAR["AllowPostingToPreviousYear"] = "السماح بالترحيل للسنوات السابقة";
DataDictionaryAR["AllowProvisionalPosting"] = "السماح بالترحيل المؤقت";
DataDictionaryAR["YearsToKeepHistory"] = "سنوات حفظ السجلات";
DataDictionaryAR["ForceListingOfBatches"] = "اجبر المستخدم على طباعة الحوافظ";
DataDictionaryAR["CurrentYear"] = "السنة الحالية";
DataDictionaryAR["OldestYearOfFiscalSet"] = "اقدم سنة مالية ";
DataDictionaryAR["LastBatchNumber"] = "اخر رقم حافظة";
DataDictionaryAR["PostingSequence"] = "تسلسل الترحيل";
DataDictionaryAR["Segment"] = "قطاع";
DataDictionaryAR["SegmentName"] = "اسم القطاع";
DataDictionaryAR["SegmentLength"] = "طول القطاع";
DataDictionaryAR["Description"] = "الوصف";
DataDictionaryAR["SegmentNumber"] = "رقم القطاع";
DataDictionaryAR["Class"] = "فئة";
DataDictionaryAR["ClassName"] = "اسم الفئة";
DataDictionaryAR["ClassLength"] = "طول الفئة";
DataDictionaryAR["Description"] = "الوصف";
DataDictionaryAR["ClassNumber"] = "رقم الفئة";
DataDictionaryAR["SegmentCode"] = "رموز القطاع";
DataDictionaryAR["Code"] = "الرمز";
DataDictionaryAR["ClassCodes"] = "رموز الفئة";
DataDictionaryAR["AccountStructure"] = "هيكلة الحساب";
DataDictionaryAR["StructureCode"] = "رمز الهيكل";
DataDictionaryAR["SegmentName"] = "اسم القطاع";
DataDictionaryAR["AccountGroups"] = "مجموعات الحساب";
DataDictionaryAR["GroupCode"] = "رمز المجموعة";
DataDictionaryAR["SourceCode"] = "رموز المصادر";
DataDictionaryAR["Account"] = "الحسابات";
DataDictionaryAR["NormalBalance"] = "الرصيد العادي";
DataDictionaryAR["AccountNumber"] = "رقم الحساب";
DataDictionaryAR["AccountType"] = "نوع الحساب";
DataDictionaryAR["Status"] = "الحالة";
DataDictionaryAR["MultiCurrency"] = "متعدد العملات";
DataDictionaryAR["RollUp"] = "حساب تجميعي";
DataDictionaryAR["AccountGroupDescription"] = "وصف مجموعة الحساب";
DataDictionaryAR["AccountDescription"] = "وصف الحساب";
DataDictionaryAR["HasAccountClass"] = "فئة الحساب";
DataDictionaryAR["AutoAllocation"] = "توزيع تلقائي";
DataDictionaryAR["PostIn"] = "ترحيل ب";
DataDictionaryAR["AccountClass"] = "فئة الحساب";
DataDictionaryAR["AllocatedAccounts"] = "الحسابات المخصصة";
DataDictionaryAR["Percent "] = "النسبة";
DataDictionaryAR["AllocationPercent"] = "نسبة التوزيع";
DataDictionaryAR["Add"] = "اضافة";
DataDictionaryAR["BatchListing"] = "قائمة الحافظات";
DataDictionaryAR["BatchNumber"] = "رقم الحافظة";
DataDictionaryAR["SourceLedger"] = "المصدر";
DataDictionaryAR["No.OfEntries"] = "عدد الحركات";
DataDictionaryAR["TotalDebits"] = "اجمالي المدين";
DataDictionaryAR["TotalCredits"] = "اجمالي الدائن";
DataDictionaryAR["ReadyToPost"] = "جاهزة للترحيل";
DataDictionaryAR["Status"] = "الحالة";
DataDictionaryAR["Printed"] = "مطبوع";
DataDictionaryAR["PostingSequence"] = "تسلسل الترحيل";
DataDictionaryAR["LastEdit"] = "اخر تعديل";
DataDictionaryAR["JournalEntry"] = "دفتر اليومية";
DataDictionaryAR["EntryNumber"] = "رقم الحركة";
DataDictionaryAR["Date"] = "التاريخ";
DataDictionaryAR["SourceCode"] = "رمز المصدر";
DataDictionaryAR["EntryDescription"] = "وصف الحركة";
DataDictionaryAR["EntryTotal"] = "اجمالي الحركة";
DataDictionaryAR["SourceCurrency"] = "العملة";
DataDictionaryAR["SourceDebit"] = "مدين";
DataDictionaryAR["sourcecredit"] = "دائن";
DataDictionaryAR["Reference"] = "المرجع";
DataDictionaryAR["RateDate"] = "تاريخ نسبة التحويل";
DataDictionaryAR["Rate"] = "نسبة التحويل";
DataDictionaryAR["Funct.Debit"] = "مدين بالعملة الاساسية";
DataDictionaryAR["Funct.Credit"] = "دائن بالعملة الاساسية";
DataDictionaryAR["optionalfields"] = "الحقول الاختيارية";
DataDictionaryAR["PostBatchRange"] = "ترحيل مجموعة من الحافظات";
DataDictionaryAR["FromBatch"] = "من الحافظة";
DataDictionaryAR["ToBatch"] = "الى الحافظة";
DataDictionaryAR["ExcludeBatches"] = "استبعاد الحافظات";
DataDictionaryAR["CreateNewYear"] = "انشاء سنة جديدة";
DataDictionaryAR["TrialBalance"] = "ميزان المراجعة";
DataDictionaryAR["TransactionListing"] = "قائمة الحركات";
DataDictionaryAR['Options'] = 'الخيارات';
DataDictionaryAR['SegmentsCodes'] = 'رموز القطاع';
DataDictionaryAR['PostRangesofBatches'] = 'ترحيل مجموعة حافظات';
DataDictionaryAR['CRMIntegration'] = 'CRM Integration';
DataDictionaryAR['ForceListingOfBatches'] = 'اجبر المستخدم على طباعة الحوافظ';
DataDictionaryAR['vldAddFiscal'] = 'الرجاء اضافة سنة محاسبية';
DataDictionaryAR['vldSegmentLength'] = 'القيمة يجب ان تكون بين 1 و 15';
DataDictionaryAR['vldSegmentNumberExceeded'] = 'الحد الاقصى من القطع هو 5';
DataDictionaryAR['vldClassNumberExceeded'] = 'الحد الاقصى من الفئات هو 20';
DataDictionaryAR['Addnewcode'] = 'اضافة  رمز قطاع جديد';
DataDictionaryAR['vldAccountStructureDefaultSegment'] = 'القطاع {0} هيكلة الحساب يجب ان تحوي';
DataDictionaryAR["AddAccountStructure"] = 'اضافة هيكلة الحساب';
DataDictionaryAR["AddAccountGroup"] = 'اضافة مجموعة حساب';
DataDictionaryAR["AddSourceCode"] = 'اضافة رمز مصدر';
DataDictionaryAR["AccountGroup"] = "مجموعة الحساب";
DataDictionaryAR['RollUp'] = 'حساب تجميعي';
DataDictionaryAR['ChildAccounts'] = 'الحسابات التابعة';
DataDictionaryAR['Percent'] = 'النسبة';
DataDictionaryAR['Transactions'] = 'الحركات';
DataDictionaryAR['FromDate'] = 'من تاريخ';
DataDictionaryAR['ToDate'] = 'الى تاريخ';
DataDictionaryAR['Posted'] = 'تم الترحيل';
DataDictionaryAR['NotPosted'] = 'غير مرحلة';
DataDictionaryAR['Debits'] = 'مدين';
DataDictionaryAR['Credits'] = 'دائن';
DataDictionaryAR['Balance'] = 'الرصيد';
DataDictionaryAR['AllCurrency'] = 'كل العملات';
DataDictionaryAR['SpecifiedCurrency'] = 'عملات محددة';
DataDictionaryAR['TotalPercent'] = 'مجموع النسبة';
DataDictionaryAR['AddAccount'] = 'اضافة حساب';
DataDictionaryAR['vldAllocationPercentError'] = 'يجب ان يكون مجموع نسبة التوزيع 100 ';
DataDictionaryAR['vldAccountNumberError'] = '( {0} ) بنية رقم الحساب';
DataDictionaryAR['Open'] = 'فتح';
DataDictionaryAR['Post'] = 'ترحيل';
DataDictionaryAR['Prov.Post'] = 'ترحيل مبدئي';
DataDictionaryAR['Clone'] = 'نسخ';
DataDictionaryAR['Reverse'] = 'عكس';
DataDictionaryAR['NewBatch'] = 'حافظة جديدة';
DataDictionaryAR['BatchList'] = 'قائمة الحافظات';
DataDictionaryAR['vldConfirmPostBatch'] = 'هل انت متأكد انك تريد ترحيل الحافظة؟';
DataDictionaryAR['vldPostNotReadyBatch'] = 'الحافظة غير جاهزة للترحيل, هل بالتأكيد تريد ترحيلها؟';
DataDictionaryAR['vldPostNotPrintedBatch'] = 'يجب طباعة الحافظة اولا';
DataDictionaryAR['Deleted'] = 'محذوف';
DataDictionaryAR['PrintedSuccessfully'] = 'تمت الطباعة بنجاح';
DataDictionaryAR['Posted'] = 'تم الترحيل';
DataDictionaryAR['PostedSuccessfully'] = 'تم الترحيل بنجاح';
DataDictionaryAR['vldReadyToPostEmptyBatch'] = 'الحافظة فارغة';
DataDictionaryAR['clonedSuccessfully'] = 'تم النسخ بنجاح';
DataDictionaryAR['AddJournalEntry'] = 'اضافة دفتر يومية';
DataDictionaryAR['ReturntoBatchList'] = 'الرجوع الى قائمة الحافظات';
DataDictionaryAR['BatchListNumber'] = 'رقم الحافظة';
DataDictionaryAR['BatchListDescription'] = 'وصف الحافظة';
DataDictionaryAR['SourceCredit'] = 'دائن';
DataDictionaryAR['Refrence'] = 'المرجع';
DataDictionaryAR['Funct.Currency'] = 'العملة الاساسية';
DataDictionaryAR['OptionalFields'] = 'حقول اختيارية';
DataDictionaryAR['BatchListInfo'] = 'معلومات الحافظة';
DataDictionaryAR['AccountClassCodes'] = 'فئة الحساب';
DataDictionaryAR['PostBatchRange'] = 'ترحيل مجموعة حافظات';
DataDictionaryAR["vldFromBatchError"] = 'من الحافظة يجب ان تكون اقل من الى الحافظة';
DataDictionaryAR["vldPostBatchRangeError"] = 'خطأ، النطاق المحدد لا يحتوي على حافظات للترحيل';
DataDictionaryAR["YearEndCondition1"] = 'الرجاء ادخال سنة محاسبية جديدة قبل المتابعة';
DataDictionaryAR["YearEndCondition2"] = 'هذه العملية سوف تقوم بتحويل الحسابات من حسابات الدخل لحساب الارباح المحتجزة';
DataDictionaryAR["YearEndConfirmation"] = 'هل انت متأكد من انشاء سنة جديدة ؟';
DataDictionaryAR["PasswordConfirmation"] = "تأكيد كلمة المرور";
DataDictionaryAR["EnterPasswordMsg"] = 'يرجى إدخال كلمة المرور : ';
DataDictionaryAR["vldPasswordInvalid"] = 'خطأ في كلمة المرور';
DataDictionaryAR["completedSuccessfully"] = 'تمت بنجاح';
DataDictionaryAR["vldCreateFiscal"] = 'يجب ان تنشأ سنة محاسبية لسنة {0} قبل المتابعة';
DataDictionaryAR["Process"] = 'اتمام';
DataDictionaryAR['FromAccount'] = 'من حساب';
DataDictionaryAR['ToAccount'] = 'الى حساب';
DataDictionaryAR['BatchDescription'] = 'وصف الحافظة';
DataDictionaryAR['JournalEntryDate'] = 'تاريخ الحركة';
DataDictionaryAR['AutoAllocationBatch'] = 'توزيع الحسابات';
DataDictionaryAR["vldNoAccountAllocated"] = 'لا يوجد حسابات للتوزيع في المدى المختار';
DataDictionaryAR["vldAllocatedCompleted"] = 'التوزيع تم بنجاح';
DataDictionaryAR["AccountReport"] = 'ميزان المراجعة';
DataDictionaryAR["BatchListReport"] = "تقرير الحافظة";
DataDictionaryAR["AddJournalEntryHeader"] = "اضافة دفتر يومية";
DataDictionaryAR["IsRestricted"] = "Is Restricted";
DataDictionaryAR["AllowedUser"] = "المستخدمين المسموحين";
DataDictionaryAR["AccountDescriptionAr"] = "وصف الحساب بالعربي";
DataDictionaryAR["IncomeAccountReport"] = "بيانات الدخل";
DataDictionaryAR["BalanceSheetAccountReport"] = "الميزانية العمومية";
DataDictionaryAR["AccDescription"] = "وصف الحساب";
//////////////////////////////////////////////Cash Management////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryAR["CashManagement"] = "ادارة النقد";
DataDictionaryAR["DefaultBankCode"] = "رمز البنك الافتراضي";
DataDictionaryAR["SortDepositDetailsby"] = "ترتيب تفاصيل الايداع ب";
DataDictionaryAR["DefaultGLAccount"] = "الحساب الافتراضي";
DataDictionaryAR["TransferAdjustmentGLaccount"] = "حساب تعديل التحويل الافتراضي";
DataDictionaryAR["NextPostingSequence"] = "رقم تسلسل الترحيل القادم";
DataDictionaryAR["DocumentType"] = "نوع الوثيقة";
DataDictionaryAR["CreditCardType"] = "نوع بطاقة الائتمان";
DataDictionaryAR["CreditCard"] = "بطاقة الائتمان";
DataDictionaryAR["Bank"] = "البنك";
DataDictionaryAR["BankCode"] = "رمز البنك";
DataDictionaryAR["TransitNumber"] = "رقم النقل";
DataDictionaryAR["InActive"] = "غير فعال";
DataDictionaryAR["BankAccountNumber"] = "رقم الحساب المصرفي";
DataDictionaryAR["NextDepositNumber"] = "رقم الايداع القادم";
DataDictionaryAR["Error\Write-offSpread"] = "نشر خطأ/شطب";
DataDictionaryAR["Multi-currency"] = "متعدد العملات";
DataDictionaryAR["StatementCurrency"] = "العملة";
DataDictionaryAR["BankAccount"] = "الحساب المصرفي";
DataDictionaryAR["Write-offAccount"] = "حساب الحذف";
DataDictionaryAR["CreditCardChargesAccount"] = "حساب رسوم بطاقات الائتمان";
DataDictionaryAR["City\Country"] = "المدينة/الدولة";
DataDictionaryAR["State\Province"] = "المقاطعة";
DataDictionaryAR["Contact"] = "الاتصال";
DataDictionaryAR["PhoneNumber"] = "رقم الهاتف";
DataDictionaryAR["Address"] = "العنوان";
DataDictionaryAR["LastClosingStatementBalance"] = "رصيد اخر اغلاق";
DataDictionaryAR["Deposits"] = "الودائع";
DataDictionaryAR["Withdrawals"] = "السحب";
DataDictionaryAR["CurrentBalance"] = "الرصيد الحالي";
DataDictionaryAR["ChequesStocks"] = "اسهم الشيكات";
DataDictionaryAR["ChequeNumber"] = "رقم الشيك";
DataDictionaryAR["ChequeStockCode"] = "رمز سهم الشيك";
DataDictionaryAR["ChequeFrom"] = "شيك من";
DataDictionaryAR["ExchangeGain"] = "ارباح الصرف";
DataDictionaryAR["ExchangeLoss"] = "خسائر الصرف";
DataDictionaryAR["RoundingAccount"] = "حساب التقريب";
DataDictionaryAR["BankEntryBatch"] = "حافظة حركات البنك";
DataDictionaryAR["BankEntry"] = "حركات البنك";
DataDictionaryAR["BatchNumber"] = "رقم الحافظة";
DataDictionaryAR["TransferBatch"] = "حافظة التحويل";
DataDictionaryAR["FromBank"] = "من البنك ";
DataDictionaryAR["ToBank"] = "الى البنك";
DataDictionaryAR["TransferAmount"] = "مبلغ التحويل";
DataDictionaryAR["DepositAmount"] = "مبلغ الايداع";
DataDictionaryAR["FunctionalTransferAmount"] = "مبلغ التحويل بالعملة الاساسية";
DataDictionaryAR["FunctionalDepositAmount"] = "مبلغ الايداع بالعملة الاساسية";
DataDictionaryAR["ReverseTransactions "] = "المعاملات العكسية";
DataDictionaryAR["BankReconciliation"] = "تسوية البنوك";
DataDictionaryAR["StatementBalance"] = "بيان الرصيد";
DataDictionaryAR["BookBalance"] = "الرصيد المحجوز";
DataDictionaryAR["DepositOutstanding"] = "الايداعات القائمة";
DataDictionaryAR["BankEntriesNotPosted"] = "حركات البنك غير مرحلة";
DataDictionaryAR["WithdrawalsOutstanding"] = "السحوبات المعلقة";
DataDictionaryAR["AdjustedBookBalance"] = "الرصيد المعدل";
DataDictionaryAR["AdjustedStatementBalance"] = "الرصيد المعدل";
DataDictionaryAR["OutOfBalanceBy"] = "الفرق";
DataDictionaryAR["UnclearItems"] = "حركات غير واضحة";
DataDictionaryAR['ReverseTransactions'] = 'عكس الحركات';
DataDictionaryAR['Reconciliation'] = 'تسوية البنوك';
 DataDictionaryAR['Processing'] = 'المعالجة';
DataDictionaryAR['Documents'] = 'المستندات';
DataDictionaryAR['Addnewdocument'] = 'اضافة مستند جديد';
DataDictionaryAR['DocumentType'] = 'نوع المستند';
DataDictionaryAR['Length'] = 'الطول';
DataDictionaryAR['Prefix'] = 'الرمز البدائي';
DataDictionaryAR['NextNumber'] = 'الرقم التالي';
DataDictionaryAR['AddCreditCardType'] = 'اضافة نوع بطاقة الائتمان';
DataDictionaryAR['ErrorWriteOff'] = 'نشر خطأ/شطب';
DataDictionaryAR['Multicurrency'] = 'متعدد العملة';
DataDictionaryAR['StatementCurrency'] = 'العملة';
DataDictionaryAR['Write-offAccount'] = 'حساب الحذف';
DataDictionaryAR['LastClosingStatmentBalance'] = 'رصيد اخر اغلاق';
DataDictionaryAR['+Deposits'] = 'ايداع+';
DataDictionaryAR['-Withdrawals'] = 'سحب-';
DataDictionaryAR['AddBank'] = 'اضافة بنك';
DataDictionaryAR['AddChequeStock'] = 'اضافة سهم شيك';
DataDictionaryAR['BankDetails'] = 'تفاصيل البنك';
DataDictionaryAR['CityCountry'] = 'المدينة/البلد';
DataDictionaryAR['StateProvince'] = 'المقاطعة';
DataDictionaryAR['vldDeactivateConnectedItem'] = "لا يمكن تغير الحالة الى غير فعال لانه مستعمل";
DataDictionaryAR['vldAddCurrencyToBank'] = 'البنك غير متعدد العملة , لا تستطيع اضافة عملة جديدة';
DataDictionaryAR['vldFailedCreateNewBatch'] = 'خطأ في انشاء حافظة جديدة';
DataDictionaryAR['vldFailedToLoadBatchList'] = 'خطأ في تحميل الحافظات';
DataDictionaryAR['BankEntryBatchList'] = 'حافظات حركات البنوك';
DataDictionaryAR['BankTransferBatchList'] = 'حافظات حركات التحويل على البنوك';
DataDictionaryAR['EntryDate'] = 'تاريخ الحركة';
DataDictionaryAR['TransactionNumber'] = 'رقم المعاملة';
DataDictionaryAR['Line'] = 'الرقم';
DataDictionaryAR['AccountName'] = 'اسم الحساب';
DataDictionaryAR['AddBankEntryHeader'] = 'اضافة حركة بنك';
DataDictionaryAR['AddBankEntry'] = 'اضافة حركة بنك';
DataDictionaryAR['ThisIsa'] = 'هذا ';
DataDictionaryAR['AddTransferEntry'] = 'اضافة حركة تحويل';
DataDictionaryAR['TransferEntry'] = 'حركة تحويل';
DataDictionaryAR['vldToAmountError'] = 'القيمة يجب ان لا تكون اكبر من قيمة الايداع';
DataDictionaryAR['vldFromAmountError'] = 'القيمة يجب ان لا تكون اكبر من قيمة التحويل';
DataDictionaryAR['vldTransferAmount'] = 'القيمة المحولة يجب ان تساوي القيمة المودعة';
DataDictionaryAR['AddTransferEntry'] = 'اضافة حركة تحويل';
DataDictionaryAR['TransferEntry'] = 'حركة تحويل';
DataDictionaryAR['NoTransSelected'] = 'لا يوجد حركات مختارة';
DataDictionaryAR["vldSuccessfullyReversed"] = 'عكس العملية تمت بنجاح';
DataDictionaryAR["vldFailedToReverse"] = 'خطأ في العكس,, بعض الحركات معكوسة سابقا';
DataDictionaryAR['Module'] = 'Module';
DataDictionaryAR['BankDescription'] = 'وصف البنك';
DataDictionaryAR['CheckNumber'] = 'رقم الشيك';
DataDictionaryAR['CheckAmount'] = 'قيمة الشيك';
DataDictionaryAR['StartReversing'] = 'ابدأ عكس الحركات';
DataDictionaryAR['FromCheckNumber'] = 'من الشيك رقم';
DataDictionaryAR['ToCheckNumber'] = 'الى الشيك رقم';
DataDictionaryAR['FromPaymentDate'] = 'من تاريخ الدفعة';
DataDictionaryAR['ToPaymentDate'] = 'الى تاريخ الدفعة';
DataDictionaryAR['FromVendor'] = 'من المورد ';
DataDictionaryAR['ToVendor'] = 'الى المورد';
DataDictionaryAR['FromCustomer'] = 'من العميل';
DataDictionaryAR['ToCustomer'] = 'الى العميل';
DataDictionaryAR['FromAmount'] = 'من القيمة ';
DataDictionaryAR['ToAmount'] = 'الى القيمة';
DataDictionaryAR['ReceiptEntry'] = 'ايصال';
DataDictionaryAR['TransferEntry'] = 'تحويل';
DataDictionaryAR['BankEntry'] = 'مدخلات بنوك';
DataDictionaryAR['Searchcriteria'] = 'البحث';
DataDictionaryAR['UnclearItems'] = 'عناصر غير واضحة';
DataDictionaryAR['CheckNumber'] = 'شيك#';
DataDictionaryAR['Mark'] = 'تعليم';
DataDictionaryAR['ToCheck'] = 'الى الشيك';
DataDictionaryAR['FromCheck'] = 'من الشيك';
DataDictionaryAR['QuickClearing'] = 'Quick Clearing';
DataDictionaryAR['AdjustedStatementBalance'] = 'الرصيد المعدل';
DataDictionaryAR['AdjustedBookBalance'] = 'الرصيد المعدل';
DataDictionaryAR['WithdrawalsOutstanding'] = '-السحوبات المعلقة';
DataDictionaryAR['BankEntriesNotPosted'] = 'حركات البنك غير مرحلة';
DataDictionaryAR['ReconciliationDate'] = 'تاريخ التسوية';
DataDictionaryAR['StatementDate'] = 'Statement Date';
DataDictionaryAR['DepositOutstanding'] = '+الايداعات القائمة';
DataDictionaryAR['ReconciliationDescription'] = 'وصف التسوية';
DataDictionaryAR['Withdrawal'] = 'السحب';
DataDictionaryAR['Deposit'] = 'الايداع';
DataDictionaryAR['Reconciled'] = 'Reconciled';
DataDictionaryAR['ClearedAmount'] = 'مبلغ موضح';
DataDictionaryAR['Difference'] = 'الفرق';
DataDictionaryAR['DifferenceReason'] = 'سبب الفرق';
DataDictionaryAR["AddBankReconciliation"] = "اضافة تسوية بنك";
DataDictionaryAR["BankStatement"] = "بيان رصيد بنك";
DataDictionaryAR["Statement"] = "بيان";
DataDictionaryAR["Continue"] = "اكمال";
DataDictionaryAR["GetChecks"] = "عرض الشيكات";
DataDictionaryAR['As'] = 'ك';
DataDictionaryAR['vldOutOfBalanceError'] = 'Out off balance must be 0 to post';
DataDictionaryAR['alreadyPosted'] = "تم ترحيله مسبقا";
DataDictionaryAR['BankRequired'] = "الرجاء اختيار البنك";
DataDictionaryAR['GLAccount'] = 'الحساب';
DataDictionaryAR['Amount'] = 'القيمة';
DataDictionaryAR['TransferEntryDetails'] = 'تفاصيل الحركة';
DataDictionaryAR['vldInsertEntryNumber'] = 'الرجاء ادخال رقم الحركة';
DataDictionaryAR['vldSelectEntryDate'] = 'الرجاء ادخال تاريخ الحركة ';
DataDictionaryAR['vldBankRequired'] = 'الرجاء اختيار البنك';
DataDictionaryAR['vldClearedAmount'] = 'Value must be less than or equal diffrence';
DataDictionaryAR['vldNoChecksFound'] = 'لايوجد حركات';


//////////////////////////////////////////////Account Receivable/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

DataDictionaryAR["AccountReceivable"] = "المقبوضات";
DataDictionaryAR["AllowEditofImportedBatches"] = "تحرير الحافظات المستوردة";
DataDictionaryAR["AllowEditofExternalBatches"] = "تحرير الحافظات الخارجية";
DataDictionaryAR["DefaultPostingDate"] = "تاريخ الترحيل الافتراضي";
DataDictionaryAR["CalculateTaxAmountsAutomatically"] = "حساب الضريبة تلقائيا";
DataDictionaryAR["AllowPrintingofInvoices"] = "السماح لطباعة الفواتير";
DataDictionaryAR["AllowEditAfterInvoicePrinted"] = "السماح بتعديل الفاتورة بعد الطباعة";
DataDictionaryAR["DefaultTransactionType"] = "نوع العملية الافتراضي";
DataDictionaryAR["DefaultOrderofOpenDocuments"] = "الترتيب الافتراضي للحركات المفتوحة";
DataDictionaryAR["DefaultPaymentCode"] = "رمز الدفع الافتراضي";
DataDictionaryAR["DefaultBankCode"] = "رمز البنك الافتراضي";
DataDictionaryAR["DefaultPostingDate"] = "تاريخ الترحيل الافتراضي";
DataDictionaryAR["AllowPrintingofReceipts"] = "السماح بطباعة الايصالات";
DataDictionaryAR["AllowEditAfterReceiptPrinted"] = "السماح بالتعديل بعد طباعة الايصال";
DataDictionaryAR["CheckForDuplicatedChecks"] = "التحقق من وجود الشيكات المكررة";
DataDictionaryAR["SortChecksBy"] = "ترتيب الشيكات بواسطة";
DataDictionaryAR["PaymentCode"] = "طريقة الدفع";
DataDictionaryAR["PaymentType"] = "نوع الدفع";
DataDictionaryAR["PaymentTerms"] = "شروط الدفع";
DataDictionaryAR["DueDateType"] = "نوع تاريخ الاستحقاق";
DataDictionaryAR["No.ofDaysForInvoiceDueDate"] = "عدد الايام لتاريخ استحقاق الفاتورة";
DataDictionaryAR["DiscountType"] = "نوع الخصم";
DataDictionaryAR["CalculateBaseforDiscountWithTax"] = "حساب الاساس للخصم مع الضريبة";
DataDictionaryAR["No.OfDaysForDiscountDueDate"] = "عدد الأيام للخصم";
DataDictionaryAR["DiscountPercentage"] = "نسبة الخصم ";
DataDictionaryAR["AccountsSets"] = "مجموعة الحسابات";
DataDictionaryAR["ReceivablesControlAccount"] = "حساب الذمم";
DataDictionaryAR["InvoiceDiscountAccount"] = "حساب خصم الفاتورة";
DataDictionaryAR["PrepaymentAccount"] = "حساب الدفع المسبق";
DataDictionaryAR["ReceiptsDiscountAccount"] = "حساب خصم المقبوضات";
DataDictionaryAR["CurrencyCode"] = "رمز العملة";
DataDictionaryAR["Un-RealizedExchangeGain"] = "ارباح غير محققة";
DataDictionaryAR["Un-RealizedExchangeLoss"] = "خسائر غير محققة";
DataDictionaryAR["ExchangeGain"] = "ارباح الصرف";
DataDictionaryAR["ExchangeLoss"] = "خسائر الصرف";
DataDictionaryAR["ExchangeRounding"] = "تقريب الصرف";
DataDictionaryAR["Write-OffAccount"] = "حساب الحذف";
DataDictionaryAR["SalesPerson"] = "مندوب مبيعات";
DataDictionaryAR["EmployeeName"] = "اسم الموظف";
DataDictionaryAR["EmployeeNumber"] = "رقم الموظف";
DataDictionaryAR["AnnualSalesTarget"] = "هدف المبيعات السنوي";
DataDictionaryAR["CustomerGroups"] = "مجموعات العملاء";
DataDictionaryAR["AccountType"] = "نوع الحساب";
DataDictionaryAR["AccountSet"] = "مجموعة الحسابات";
DataDictionaryAR["TermCode"] = "شروط الدفع";
DataDictionaryAR["TaxGroup"] = "مجموعة الضرائب";
DataDictionaryAR["PrintStatements"] = "طباعة كشوفات الحساب";
DataDictionaryAR["CreditLimit"] = "حد الائتمان";
DataDictionaryAR["OverdueLimit"] = "حد تأخير الدفع";
DataDictionaryAR["Customer "] = "العملاء";
DataDictionaryAR["CustomerNumber"] = "رقم العميل";
DataDictionaryAR["CustomerName"] = "اسم العميل";
DataDictionaryAR["ShortName"] = "الاسم المختصر";
DataDictionaryAR["Group"] = "المجموعة";
DataDictionaryAR["OnHold"] = "في الانتظار";
DataDictionaryAR["StartDate"] = "بدء التسجيل";
DataDictionaryAR["Website"] = "الموقع الالكتروني";
DataDictionaryAR["Email"] = "البريد الالكتروني";
DataDictionaryAR["AccountType"] = "نوع الحساب";
DataDictionaryAR["DeliveryMethod"] = "طريقة التسليم";
DataDictionaryAR["Invoice"] = "فاتورة";
DataDictionaryAR["Receipt"] = "الايصالات";
DataDictionaryAR["Document"] = "الحركة";
DataDictionaryAR["DocumentType"] = "نوع الحركة ";
DataDictionaryAR["AppliedAmount"] = "المبلغ المستعمل";
DataDictionaryAR["ReceiptDescription"] = "وصف الايصال";
DataDictionaryAR["DefaultBank"] = "البنك";
DataDictionaryAR["ReceiptType"] = "نوع الايصال";
DataDictionaryAR["ExchangeRate"] = "سعر الصرف";
DataDictionaryAR["ReceiptDate"] = "تاريخ الايصال";
DataDictionaryAR["PostingDate"] = "تاريخ الترحيل";
DataDictionaryAR["PaymentType"] = "نوع الدفع";
DataDictionaryAR["ReceiptAmount"] = "قيمة الايصال";
DataDictionaryAR["ReferenceNumber"] = "الرقم المرجعي";
DataDictionaryAR["Apply"] = "تطبيق";
DataDictionaryAR["Note"] = "الملاحظات";
DataDictionaryAR["AdjustmentBachlist"] = "قائمة حافظة التعديلات";
DataDictionaryAR["RefundBatchList"] = " حافظة المردودات";
DataDictionaryAR["AllowRefundWithoutApply"] = "استرداد المبالغ دون تطبيق";
DataDictionaryAR["RefundAmount"] = "المبلغ المسترد";
DataDictionaryAR["PostingDate"] = "تاريخ الترحيل";
DataDictionaryAR["Type"] = "النوع";
DataDictionaryAR["OriginalAmount"] = "المبلغ الاصلي";
DataDictionaryAR["CurrentBalance"] = "المبلغ الحالي";
DataDictionaryAR["PaymentType"] = "نوع الدفع";
DataDictionaryAR["PaymentAmount"] = "قيمة الدفعة";
DataDictionaryAR["NetBalance"] = "صافي الرصيد";
DataDictionaryAR["YearEnd"] = "نهاية العام";
DataDictionaryAR["Revaluation"] = "اعادة تقييم";
DataDictionaryAR["CustomerStatement"] = "بيان العملاء";
DataDictionaryAR[" CustomerTransactions"] = "معاملات العملاء";
DataDictionaryAR["CustomerAgeing"] = "اعمار الديون - عملاء";
DataDictionaryAR['CustomerGroup'] = 'مجموعة العملاء';
DataDictionaryAR['InvoiceBatchList'] = 'حافظة الفواتير';
DataDictionaryAR['ReceiptBatchList'] = 'حافظة الايصالات';
DataDictionaryAR['AdjustmentBatchList'] = 'حافظة التعديلات';
DataDictionaryAR['CustomerTransactions'] = 'معاملات العملاء';
DataDictionaryAR['Numbering'] = 'الترقيم';
DataDictionaryAR['ReceiptOptions'] = 'خيارات الايصالات';
DataDictionaryAR['InvoiceOptions'] = 'خيارات الفواتير';
DataDictionaryAR['DebitNote'] = 'Debit Note';
DataDictionaryAR['CreditNote'] = 'Credit Note';
DataDictionaryAR['Receipts'] = 'الايصالات';
DataDictionaryAR['Prepayment'] = 'دفعة مسبقة';
DataDictionaryAR['Miscellaneous'] = 'متفرقات';
DataDictionaryAR['UnappliedCash'] = 'النقد الغير مطبق';
DataDictionaryAR['Adjustment'] = 'التعديلات';
DataDictionaryAR['Refund'] = 'المردودات';
DataDictionaryAR['AddPaymentCode'] = 'اضافة طريقة دفع';
DataDictionaryAR['AddPaymentTerm'] = 'اضافة شروط دفع';
DataDictionaryAR['AddAccountSet'] = 'اضافة مجموعة حسابات';
DataDictionaryAR['AddSalesPerson'] = 'اضافة مندوب مبيعات';
DataDictionaryAR['Commission'] = 'عمولة';
DataDictionaryAR['Employee#'] = 'رقم الموظف';
DataDictionaryAR['NumberOfRates'] = 'عدد النسب 1-5';
DataDictionaryAR['PaidCommission'] = 'العمولة المدفوعة';
DataDictionaryAR['OnSalesOf'] = 'على البيع';
DataDictionaryAR['To'] = 'الى';
DataDictionaryAR['Percentage'] = 'النسبة';
DataDictionaryAR['TaxClass'] = 'فئة الضريبة';
DataDictionaryAR['AddCustomerGroup'] = 'اضافة مجموعة عملاء';
DataDictionaryAR['vldTaxGroupCurrencyDiffrent'] = 'عملة مجموعة الضريبة {0} تختلف عن العملة المستخدمة {1} هل تريد اتمام العملية؟';
DataDictionaryAR['NotFound'] = 'غير موجود';
DataDictionaryAR['Invalid'] = 'غير صحيح';
DataDictionaryAR['vldBatchNotFound'] = 'الحافظة غير موجودة';
DataDictionaryAR['vldBatchIs'] = 'الحافطة {0}';
DataDictionaryAR['vldBatchReadyToPost'] = 'الحافظة جاهزة للترحيل';
DataDictionaryAR['vldDocumentType'] = 'خطأ في النوع ';
DataDictionaryAR['vldDocumentCustomer'] = 'هذه الحركة غير تابعة للعميل ';
DataDictionaryAR['vldDocumentNotPosted'] = 'هذه الحركة غير مرحلة';
DataDictionaryAR['vldDateWithinLockedPeriod'] = 'التاريخ ضمن فترة مغلقة';
DataDictionaryAR['Contact/Address'] = 'الاتصال / العنوان';
DataDictionaryAR['Invoicing'] = 'الفواتير';
DataDictionaryAR['AddCustomer'] = 'اضافة عميل';
DataDictionaryAR['Zip/PostalCode'] = 'الرمز البريدي';
DataDictionaryAR['Telephone'] = 'الهاتف';
DataDictionaryAR['State/Prov'] = 'الدولة /الولاية';
DataDictionaryAR['TermsCode'] = 'شروط الدفع';
DataDictionaryAR['TotalOutstandingBalanceExceedsTheCreditLimit'] = 'إجمالي الرصيد القائم يتجاوز حد الائتمان';
DataDictionaryAR['ARTransactionsOverdueByN'] = 'المعاملات المتأخرة بواسطة';
DataDictionaryAR['DaysOrMoreAndExceedTheOverdueLimitOfNAmount'] = 'أيام أو أكثر، ويتجاوز الحد المتأخرة ';
DataDictionaryAR['TotalInvoices'] = 'مجموع الفواتير';
DataDictionaryAR['TotalReceipts'] = 'مجموع الايصالات';
DataDictionaryAR['OutstandingBalanceExceedsLimit'] = 'الرصيد المستحق يتجاوز الحد';
DataDictionaryAR['CustomerType'] = 'نوع العميل';
DataDictionaryAR['CheckforDuplicatePOs'] = 'التحقق من وجود تكرار أوامر الشراء';
DataDictionaryAR['DocumentDate'] = 'تاريخ الحركة';
DataDictionaryAR['AllocatedAmount'] = 'المبلغ المحجوز';
DataDictionaryAR['orMoreandExceedTheoverduelimitofnamount'] = 'أو أكثر وتتجاوز الحد بالقيمة';
DataDictionaryAR['Address-Contact'] = 'العنوان-الاتصال';
DataDictionaryAR['vldEditPrintedBatchError'] = 'لا يمكن تعديل حافظة مطبوعة';
DataDictionaryAR['TotalWithTax'] = 'المجموع مع ضريبة';
DataDictionaryAR['Batch'] = 'الحافظة';
DataDictionaryAR['vldExceedError'] = '{0} يجب ان لا يتعدى';
DataDictionaryAR['vldCustomerOnHoldError'] = 'العميل {0} في الانتظار ,هل تريد الاكمال؟';
DataDictionaryAR['vldExceedCreditLimit'] = 'تجاوز حد الائتمان , هل تريد اتمام العملية؟';
DataDictionaryAR['ApplyToDocument'] = 'تطبيق على الحركة';
DataDictionaryAR['SourceValue'] = 'المبلغ';
DataDictionaryAR['AddInvoice'] = 'اضافة فاتورة';
DataDictionaryAR['BatchDate'] = 'تاريخ الحافظة';
DataDictionaryAR['vldInvoiceRequired'] = 'الرجاء اختيار فاتورة';
DataDictionaryAR['NotExist'] = 'غير موجود';
DataDictionaryAR['vldAppliedAmountError'] = 'المبلغ المطبق يجب ان يكون اكبر من 0';
DataDictionaryAR['vldAppliedAmountGreaterThanAmount'] = ' مجموع المبلغ المطبق {0} للفاتورة رقم {1} اكثر من المبلغ المسموح {2}';
DataDictionaryAR['AddReceipt'] = 'اضافة ايصال';
DataDictionaryAR['PrepaymentAmount'] = 'قيمة الدفعة الاولية';
DataDictionaryAR['DocumentNumber'] = 'رقم الحركة';
DataDictionaryAR['GLAccountDescription'] = 'وصف الحساب';
DataDictionaryAR['RefundNumber'] = 'رقم حركة المردود';
DataDictionaryAR['CashAmount'] = 'قيمة النقد';
DataDictionaryAR['AddRefund'] = 'اضافة مردود';
DataDictionaryAR['AddAdjustmentHeader'] = 'اضافة معلومات التعديل';
DataDictionaryAR['AddAdjustment'] = 'اضافة حركة تعديل';
DataDictionaryAR['Adjust'] = 'تعديل';
DataDictionaryAR['AdjustmentNumber'] = 'رقم التعديل';
DataDictionaryAR['vldSelectAdjustment'] = 'الرجاء اختيار حركة تعديل';
DataDictionaryAR['vldAdjustmentNotFound'] = 'حركة التعديل غير موجودة';
DataDictionaryAR['vldInvoiceLineNotBelongToEntry'] = 'الفاتورة لا تنتمي لهذه الحركة';
DataDictionaryAR['vldRefundRequiredError'] = 'الرجاء اختيار حركة مردودات';
DataDictionaryAR['vldRefundNotFound'] = 'حركة المردودات غير موجودة';
DataDictionaryAR['vldReceiptRequiredError'] = 'الرجاء اختيار ايصال';
DataDictionaryAR['vldReceiptNotPostedError'] = 'سند القبض غير مرحل';
DataDictionaryAR['vldRefundAmountRequired'] = 'القيمة يجب ان تكون اكبر من 0';
DataDictionaryAR['YearEnd'] = 'نهاية العام';
DataDictionaryAR['YearEndNote'] = 'هذه الحركة سوف تقوم بأعادة ارقام الحافظات للوضع الاساسي';
DataDictionaryAR['YearEndConfirm'] = 'هذه الحركة سوف تقوم بأعادة ارقام الحافظات للوضع الاساسي, هل انت متأكد؟';
DataDictionaryAR['resetBatchSuccessfully'] = 'ارقام الحافظات عادة للوضع الاساسي بنجاح';
DataDictionaryAR['vldYearEndOpendInvoices'] = 'خطأ .. حافظات الفواتير غير مغلقة';
DataDictionaryAR['vldYearEndOpendReceipts'] = 'خطأ .. حافظات سندات القبض غير مغلقة';
DataDictionaryAR['vldYearEndOpendAdjustment'] = 'خطأ .. حافظات التعديلات غير مغلقة';
DataDictionaryAR['vldYearEndOpendRefund'] = 'خطأ .. حافظات المردودات غير مغلقة';
DataDictionaryAR['vldReEvalFailed'] = 'فشل في اعادة التقييم';
DataDictionaryAR['vldReEvalSuccessfullly'] = 'تمت اعادة التقييم بنجاح';
DataDictionaryAR['vldNoTransNeedEval'] = 'لايوجد حركات تحتاج اعادة تقييم';
DataDictionaryAR['vldCurrencyIsFunc'] = 'العملة هي العملة الاساسية';
DataDictionaryAR['CreditNote'] = 'اوراق مدينة';
DataDictionaryAR['DebitNote'] = 'اوراق دائنة';
DataDictionaryAR['DepositSlip'] = 'سند الايداع';

DataDictionaryAR['vldReceiptBankNotExist'] = 'الايصال {0} : البنك غير موجود';
DataDictionaryAR['vldReceiptBankInactive'] = 'الايصال {0} : البنك غير فعال';
DataDictionaryAR['vldReceiptBankAccNotExist'] = 'الايصال {0} : حساب البنك غير موجود';
DataDictionaryAR['vldReceiptBankAccInactive'] = 'الايصال {0} : حساب البنك غير فعال';
DataDictionaryAR['vldReceiptCustomerNotExist'] = 'الايصال {0} : العميل غير موجود';
DataDictionaryAR['vldReceiptCustomerInactive'] = 'الايصال {0} : العميل غير فعال';
DataDictionaryAR['vldReceiptDateLocked'] = 'الايصال {0} : تاريخ الترحيل ضمن فترة مغلقة';
DataDictionaryAR['vldInvoiceDateBeforOldest'] = 'الايصال {0} : تاريخ الترحيل قبل اقدم سنة';
DataDictionaryAR['vldReceiptDateNotInCurrent'] = 'الايصال {0} : تاريخ الترحيل ليست ضمن السنة الحالية';
DataDictionaryAR['vldReceiptNoDetails'] = 'الايصال {0} : الحركة لا تحتوي على تفاصيل';
DataDictionaryAR['vldAccSetNoDeclaredReceivableAcc'] = 'الايصال {0} : مجموعة الحسابات لهذا العميل لاتحتوي على تعريف لحساب الذمم';
DataDictionaryAR['vldAccSetReceivableAccInactive'] = 'الايصال {0} : حساب الذمم لهذا العميل غير فعال';
DataDictionaryAR['vldNoDeclaredLibilityAccount'] = 'الايصال {0} : هيئة الضريبة لهذا العميل لا تحتوي على تعريف لحساب ضريبة الدخل';
DataDictionaryAR['vldReceiptAccNotExist'] = 'الايصال {0} : الحساب غير موجود';
DataDictionaryAR['vldReceiptAccInactive'] = 'الايصال {0} : الحساب {1} غير فعال';


DataDictionaryAR['vldRefundBankNotExist'] = 'المردود {0} : البنك غير موجود';
DataDictionaryAR['vldRefundBankInactive'] = 'المردود {0} : البنك غير فعال';
DataDictionaryAR['vldRefundBankAccNotExist'] = 'المردود {0} : حساب البنك غير موجود';
DataDictionaryAR['vldRefundBankAccInactive'] = 'المردود {0} : البنك غير فعال';
DataDictionaryAR['vldRefundCustomerNotExist'] = 'المردود {0} : العميل غير موجود';
DataDictionaryAR['vldRefundCustomerInactive'] = 'المردود {0} : العميل غير فعال';
DataDictionaryAR['vldRefundDateLocked'] = 'المردود {0} : تاريخ الترحيل ضمن فترة مغلقة';
DataDictionaryAR['vldRefundDateBeforOldest'] = 'المردود {0} : تاريخ الترحيل قبل اقدم سنة';
DataDictionaryAR['vldRefundDateNotInCurrent'] = 'المردود {0} : تاريخ الترحيل ليست ضمن السنة الحالية';
DataDictionaryAR['vldRefundNoDetails'] = 'المردود {0} : الحركة لاتحتوي على اي تفاصيل';
DataDictionaryAR['vldRefAccSetNoDeclaredReceivableAcc'] = 'المردود {0} : حساب الذمم غير معرف لمجموعة الحسابات';
DataDictionaryAR['vldRefundAccNotExist'] = 'المردود {0} : الحساب غير موجود';
DataDictionaryAR['vldRefundAccInactive'] = 'المردود {0} : الحساب غير فعال';

DataDictionaryAR['vldInvoiceBankNotExist'] = 'الفاتورة {0} : البنك غير موجود';
DataDictionaryAR['vldInvoiceBankInactive'] = 'الفاتورة {0} : البنك غير فعال';
DataDictionaryAR['vldInvoiceBankAccNotExist'] = 'الفاتورة {0} :حساب البنك غير موجود';
DataDictionaryAR['vldInvoiceBankAccInactive'] = 'الفاتورة {0} : حساب البنك غير فعال';
DataDictionaryAR['vldInvoiceCustomerNotExist'] = 'الفاتورة {0} : العميل غير موجود';
DataDictionaryAR['vldInvoiceCustomerInactive'] = 'الفاتورة {0} : العميل غير فعال';
DataDictionaryAR['vldInvoiceDateLocked'] = 'الفاتورة {0} : تاريخ الترحيل ضمن فترة مغلقة';
DataDictionaryAR['vldInvoiceDateBeforOldest'] = 'الفاتورة {0} : تاريخ الترحيل قبل اقدم سنة';
DataDictionaryAR['vldInvoiceDateNotInCurrent'] = 'الفاتورة {0} : تاريخ الترحيل ليست ضمن السنة الحالية';
DataDictionaryAR['vldInvoiceNoDetails'] = 'الفاتورة {0} : الحركة لا تحتوي على تفاصيل';
DataDictionaryAR['vldInvAccSetNoDeclaredReceivableAcc'] = 'الفاتورة {0} : مجموعة الحساب لاتحتوي على تعريف لحساب الذمم';
DataDictionaryAR['vldInvoiceAccNotExist'] = 'الفاتورة {0} : الحساب غير موجود';
DataDictionaryAR['vldInvoiceAccInactive'] = 'الفاتورة {0} : الحساب غير فعال';
DataDictionaryAR['vldInvoiceAccCurrency'] = 'الفاتورة {0} : مجموعة الحساب لا تحتوي على تعريف للعملة';
DataDictionaryAR['vldInvoiceTaxClassNoRate'] = 'الفاتورة {0} : فئة الحساب {1} لا تحتوي على تعريف لنسبة الضريبة';
DataDictionaryAR["AutoDistributeReceipt"] = "توزيع قيمة الايصال";
DataDictionaryAR["NoDetailsSelected"] = 'لايوجد تفاصيل لتوزيع قيمة الايصال';
DataDictionaryAR["vldAppliedAmountMoreThanReceipt"] = "مجموع المبلغ المطبق يجب ان لا  يتجاوز قيمة الايصال";
DataDictionaryAR["vldReceiptAmountFullyApplied"] = "مبلغ الايصال {0} يجب ان يطبق بالكامل على الفواتير";
DataDictionaryAR["DocumentBalance"] = "اجمالي قيمة الحركة";
DataDictionaryAR["vldReceiptAmountFullyDistributed"] = "مبلغ الايصال {0} يجب ان يوزع بالكامل ";
DataDictionaryAR["DayOfMonth"] = "اليوم من الشهر";
DataDictionaryAR["DayOfWeek"] = "اليوم من الاسبوع";
DataDictionaryAR["vldTermMonthDays"] = "الرجاء اختيار يوم شهر صحيح بين 1-30";
DataDictionaryAR["vldTermWeekDays"] = "الرجاء اختيار يوم اسبوع صحيح بين 1-7";
DataDictionaryAR["DueDate"] = "تاريخ الاستحقاق";
//////////////////////////////////////////////Account Payable////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryAR["AccountPayable"] = "المقبوضات";
DataDictionaryAR["VendorGroup"] = "مجموعة الموردين";
DataDictionaryAR["Vendor"] = "مورد";
DataDictionaryAR["VendorNumber "] = "رقم المورد";
DataDictionaryAR["VendorName"] = "اسم المورد";
DataDictionaryAR["LegalName"] = "الاسم القانوني";
DataDictionaryAR["DuplicateVendorAndAmountCheck"] = "التحقق من تكرار المورد مع القيمة";
DataDictionaryAR["DuplicateVendorAndDateCheck"] = "التحقق من تكرار المورد مع التاريخ";
DataDictionaryAR["AccountPayableInvoice"] = "فواتير المقبوضات";
DataDictionaryAR["UndistributedAmount"] = "المبلغ الغير موزع";
DataDictionaryAR["Comments"] = "تعليقات";
DataDictionaryAR["DocumentTotal"] = "اجمالي الحركة";
DataDictionaryAR["SourceAmountWithTax"] = "المبلغ مع ضريبة";
DataDictionaryAR["PaymentBatchList"] = " حافظة الدفعات";
DataDictionaryAR["miscellaneous"] = "متفرقات";
DataDictionaryAR["Payeename"] = "اسم المستفيد";
DataDictionaryAR["PayeeName"] = "اسم المستفيد";
DataDictionaryAR["PaymentType"] = "نوع الدفعة";
DataDictionaryAR["ApplyDocument"] = "تطبيق على حركة";
DataDictionaryAR["PendingAmount"] = "المبلغ المعلق";
DataDictionaryAR["VendorStatement"] = "كشف حساب مورد";
DataDictionaryAR["VendorTransactions"] = "حركات مورد";
DataDictionaryAR["VendorAgeing"] = "اعمار الديون - موردين";
DataDictionaryAR['IncludePendingTransactions'] = 'اشمال المعاملات المعلقة';
DataDictionaryAR['AgeUnappliedCreditDebitNote'] = 'Age Unapplied Credit Debit Note';
DataDictionaryAR['DefaultDocumentType'] = 'نوع الحركة الافتراضي';
DataDictionaryAR['DefaultPaymentTerm'] = ' شرط الدفع الافتراضي';
DataDictionaryAR['PaymentOptions'] = 'خيارات الدفعة';
DataDictionaryAR['AllowAdjustmentsinPaymentBatches'] = 'سماح التعديل في قائمة حافظات الدفعات';
DataDictionaryAR['ApplyDoc'] = 'تطبيق الحركة';
DataDictionaryAR['Misc.'] = 'متفرقات';
DataDictionaryAR['Payment'] = 'دفعة';
DataDictionaryAR['PayablesControlAccount'] = 'حساب المدفوعات';
DataDictionaryAR['PurchaseDiscountAccount'] = 'حساب خصم المشتريات';
DataDictionaryAR['AddVendorGroup'] = 'اضافة مجموعة موردين';
DataDictionaryAR['VendorGroups'] = 'مجموعات الموردين';
DataDictionaryAR['VendorNumber'] = 'رقم المورد';
DataDictionaryAR['AddressLine1'] = 'العنوان1';
DataDictionaryAR['AddressLine2'] = 'العنوان2';
DataDictionaryAR['AddressLine3'] = 'العنوان3';
DataDictionaryAR['AddressLine4'] = 'العنوان4';
DataDictionaryAR['State/Prov.'] = 'الولاية';
DataDictionaryAR['PostalCode'] = 'الرمز البريدي';
DataDictionaryAR['TotalPayments'] = 'مجموع المدفوعات';
DataDictionaryAR['VendorBalance'] = 'حساب المورد';
DataDictionaryAR['PaymentTermCode'] = 'شرط الدفع';
DataDictionaryAR['CreditLimitAmount'] = 'قيمة الحد الائتماني';
DataDictionaryAR['AddVendor'] = 'اضافة مورد';
DataDictionaryAR['vldVendorOnHoldError'] = 'المورد {0} في الانتظار ,هل تريد الاكمال؟';
DataDictionaryAR['Warning'] = 'تحذير';
DataDictionaryAR['vldDisAmountNotEqualToDocumentTot'] = 'القيمة الموزعة لاتساوي قيمة الدفعة هل تريد الاستمرار؟';
DataDictionaryAR['vldVendorExceedCreditLimit'] = 'المورد تجاوز حد الائتمان، هل تريد الاستمرار؟';
DataDictionaryAR['vldAddDetailsLine'] = 'يجب اضافة تفاصيل';
DataDictionaryAR['TaxAmount'] = 'قيمة الضريبة';
DataDictionaryAR['Exchangerate'] = 'سعر الصرف';
DataDictionaryAR['SourceAmount'] = 'المبلغ الاصل';
DataDictionaryAR['VendorAmount'] = 'مبلغ المورد';
DataDictionaryAR['AddPayment'] = 'اضافة دفعة';
DataDictionaryAR['DocumentNo.'] = 'رقم الحركة';
DataDictionaryAR['vldAppliedPrepaymentError'] = ' المبلغ المطبق يجب ان لا يكون اكبر من قيمة الباقية من الدفعة المسبقة';
DataDictionaryAR['PostingDate'] = 'تاريخ الترحيل';
DataDictionaryAR['vldYearEndOpendPayments'] = 'يوجد قوائم حافظات دفعات مفتوحة.';

DataDictionaryAR['vldPostEmptyBatch'] = 'لا يمكن ترحيل الحافظة , الحافظة فارغة';
DataDictionaryAR['vldPostPostedBatch'] = 'الحافظة مرحلة مسبقا';
DataDictionaryAR['vldPostDeletedBatch'] = 'الحافظة محذوفة مسبقا';
DataDictionaryAR['vldEditReadyToPostBatch'] = 'لا يمكن التعديل على الحافظة , الحافظة جاهزة للترحيل ';
DataDictionaryAR['vldPostLockedEntries'] = 'لا يمكن ترحيل الحافظة , بعض  الحركات ضمن فترة مغلقة على السنة المالية';
DataDictionaryAR['vldPostBeforOldesetEntries'] = 'لا يمكن ترحيل الحافظة , بعض الحركات قبل اقدم سنة مالية مسجلة';
DataDictionaryAR['vldPostEntriesWithInactiveAcc'] = 'لا يمكن ترحيل الحافظة , بعض الحركات تؤثر على حسابات غير فعالة';
DataDictionaryAR['vldPostBeforCurrentYear'] = 'لا يمكن ترحيل الحافظة , بعض الحركات قبل السنة الحالية';
DataDictionaryAR['vldPostUnbalancedBatch'] = 'لا يمكن ترحيل الحافظة , الحافظة غير متوازنة';
DataDictionaryAR['vldAdjustmentPeriodLocked'] = 'لا يمكن ترحيل الحافظة , فترة التعديل مقفلة';


DataDictionaryAR['vldApInvoiceBankNotExist'] = 'الفاتورة {0} : البنك غير موجود';
DataDictionaryAR['vldApInvoiceBankInactive'] = 'الفاتورة {0} : البنك غير فعال';
DataDictionaryAR['vldApInvoiceBankAccNotExist'] = 'الفاتورة {0} : حساب البنك غير موجود';
DataDictionaryAR['vldApInvoiceBankAccInactive'] = 'الفاتورة {0} : حساب البنك غير فعال';
DataDictionaryAR['vldApInvoiceVendorNotExist'] = 'الفاتورة {0} : المورد غير موجود';
DataDictionaryAR['vldApInvoiceVendorInactive'] = 'الفاتورة {0} : المورد غير فعال';
DataDictionaryAR['vldApInvoiceDateLocked'] = 'الفاتورة {0} : تاريخ الترحيل ضمن فترة مغلقة';
DataDictionaryAR['vldApInvoiceDateBeforOldest'] = 'الفاتورة {0} : تاريخ الترحيل قبل اقدم سنة';
DataDictionaryAR['vldApInvoiceDateNotInCurrent'] = 'الفاتورة {0} : تاريخ الترحيل ليس ضمن الفترة الحالية';
DataDictionaryAR['vldApInvoiceNoDetails'] = 'الفاتورة {0} : الحركة لا تحتوي على تفاصيل';
DataDictionaryAR['vldApInvAccSetNoDeclaredReceivableAcc'] = 'الفاتورة {0} : مجموعة الحسابات لا تحتوي  على تعريف لحساب المدفوعات';
DataDictionaryAR['vldApInvoiceAccNotExist'] = 'الفاتورة {0} : الحساب غير موجود';
DataDictionaryAR['vldApInvoiceAccInactive'] = 'الفاتورة {0} : الحساب {1} غير فعال';
DataDictionaryAR['vldApInvoiceAccCurrency'] = 'الفاتورة {0} : مجموعة الحساب غير معرفة العملة';
DataDictionaryAR['vldApInvoiceTaxClassNoRate'] = 'الفاتورة {0} : فئة الحساب {1} غير معرفة النسبة';
DataDictionaryAR['vldApInvoiceDisAmount'] = 'الفاتورة {0} : المبلغ الموزع لايساوي اجمالي الحركة';



DataDictionaryAR['vldPaymentBankNotExist'] = 'الدفعة {0} : البنك غير موجود';
DataDictionaryAR['vldPaymentBankInactive'] = 'الدفعة {0} : البنك غير فعال';
DataDictionaryAR['vldPaymentBankAccNotExist'] = 'الدفعة {0} : حساب البنك غير موجود';
DataDictionaryAR['vldPaymentBankAccInactive'] = 'الدفعة {0} : حساب البنك غير فعال';
DataDictionaryAR['vldPaymentvendorNotExist'] = 'الدفعة {0} : المورد غير موجود';
DataDictionaryAR['vldPaymentvendorInactive'] = 'الدفعة {0} : المورد غير فعال';
DataDictionaryAR['vldPaymentDateLocked'] = 'الدفعة {0} : تاريخ الترحيل ضمن فترة مغلقة';
DataDictionaryAR['vldPaymentDateBeforOldest'] = 'الدفعة {0} : تاريخ الترحيل قبل اقدم سنة';
DataDictionaryAR['vldPaymentDateNotInCurrent'] = 'الدفعة {0} : تاريخ الترحيل ليست ضمن السنة الحالية';
DataDictionaryAR['vldPaymentNoDetails'] = 'الدفعة {0} : الحركة لا تحتوي على تفاصيل';
DataDictionaryAR['vldApInvAccSetNoDeclaredPayablesAcc'] = 'الدفعة {0} : مجموعة الحساب لا تحتوي على تعريف لحساب المدفوعات';
DataDictionaryAR['vldApInvAccSetReceivableAccInactive'] = ' الدفعة {0} : حساب المدفوعات غير فعال';
DataDictionaryAR['vldApInvNoDeclaredLibilityAccount'] = 'الدفعة {0} : هيئة الضريبة لا تحتوي على تعريف حساب ضريبة الدخل';
DataDictionaryAR['vldPaymentAccNotExist'] = 'الدفعة {0} : الحساب غير موجود';
DataDictionaryAR['vldPaymentAccInactive'] = 'الدفعة {0} : الحساب {1} غير فعال';

///////////////////////////////////////////// Reports ///////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryAR['FromCurrency'] = 'من العملة';
DataDictionaryAR['ToCurrency'] = 'الى العملة';
DataDictionaryAR['RateDateFrom'] = 'تاريخ الصرف من';
DataDictionaryAR['RateDateTo'] = 'تاريخ الصرف الى';
DataDictionaryAR['CurrencyExchangeRates'] = 'اسعار صرف العملات';
DataDictionaryAR['Cashmanagmentreports'] = 'تقارير ادارة النقد';
DataDictionaryAR['Transactionlisting'] = 'قائمة الحركات';
DataDictionaryAR['BankReconciliation'] = 'تسوية البنك';
DataDictionaryAR['Accountreceivablereports'] = 'تقارير المقبوضات';
DataDictionaryAR['Customeraging'] = ' اعمار الديون - عملاء';
DataDictionaryAR['CustomerStatement'] = 'بيان العملاء ';
DataDictionaryAR['CustomerTransactions'] = ' معاملات العملاء';
DataDictionaryAR['Statmentdatefrom'] = 'تاريخ البيان من';
DataDictionaryAR['Statmentdateto'] = 'تاريخ البيان الى';
DataDictionaryAR['Batch'] = 'الحافظة';
DataDictionaryAR['Fromentrydate'] = 'تاريخ الحركة من';
DataDictionaryAR['Toentrydate'] = 'تاريخ الحركة الى';
DataDictionaryAR['AgeingColumns'] = 'اعمدة اعمار الديون :';
DataDictionaryAR['Current'] = 'الحالي';
DataDictionaryAR['days'] = 'ايام';
DataDictionaryAR['Customers'] = 'العملاء';
DataDictionaryAR['Asofage'] = 'اعتبارا من عمر';
DataDictionaryAR['CutoffDate'] = 'Cut off Date';
DataDictionaryAR['Includeallinvoices'] = 'اشمال كل الفواتير';
DataDictionaryAR['VendorCurrency'] = 'عملة المورد';
DataDictionaryAR['PrintAmountsIn'] = 'طباعة القيم ب';
DataDictionaryAR['Vendors'] = 'الموردين';
DataDictionaryAR['FromDocumentDate'] = 'تاريخ الحركة من';
DataDictionaryAR['CustomerTransactionsReport'] = 'تقرير معاملات العملاء ';
DataDictionaryAR['CustomerStatementReport'] = 'تقرير بيان العملاء';
DataDictionaryAR['CustomerAgingReport'] = 'تقرير اعمار الديون - عملاء';
DataDictionaryAR['Transactionlistingreport'] = 'تقرير الحركات';
DataDictionaryAR['VendorTransactionsReport'] = 'تقرير معاملات الموردين';
DataDictionaryAR['VendorStatmentReport'] = 'تقرير بيان الموردين';
DataDictionaryAR['VendorAgingReport'] = 'تقرير اعمار الديون - موردين';
DataDictionaryAR['vldGreaterPeriod'] = 'يجب ان تكون اكبر من {0} فترة';
DataDictionaryAR['Period+1'] = 'الفترة+1';
DataDictionaryAR['Period+2'] = 'الفترة+2';
DataDictionaryAR['Period+3'] = 'الفترة+3';
DataDictionaryAR['Over'] = 'اكبر';
DataDictionaryAR['BankReconciliationreport'] = 'تقرير تسوية البنك';
DataDictionaryAR['From'] = 'من';


///////////////////////////////////////////// Common ///////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryAR['OptionalField'] = 'الحقول الاختيارية';
DataDictionaryAR['OptionalFieldDescription'] = 'وصف الحقل الاختياري';
DataDictionaryAR['OptionalFieldDetails'] = 'القيمة';
DataDictionaryAR['OptionalFieldDetailsDescription'] = 'وصف القيمة';
DataDictionaryAR["SessionDate"] = 'Session Date';
DataDictionaryAR["TotalDiscountAmount"] = 'الفيمة الكلية للخصم';
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////End ERP Section///////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************************************************************************************************************


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////IcAdditionalCostType eArabic Dictionary/////////////////////////////////////////////////////



////////////////////////////////////////////////ItemSerials/////////////////////////////////////////////
DataDictionaryAR["ItemSerialsManagement"] = 'الارقام المتسلسلة للمادة';
DataDictionaryAR["HSerial"] = 'الرقم المتسلسل: ';
DataDictionaryAR["GetTransactions"] = 'الحركات';
DataDictionaryAR["ItemSerialsManagementResults"] = 'النتائج';
DataDictionaryAR["HAvgCost"] = 'متوسط التلكلفة';
DataDictionaryAR["TransactionId"] = 'رقم الحركة';
DataDictionaryAR["SerialTransactions"] = 'حركات الرقم المتسلسل';


////////////////////////////////////////////////ItemLots/////////////////////////////////////////////
DataDictionaryAR["ItemLotManagement"] = 'ارقام الصلاحية للمادة';
DataDictionaryAR["HLot"] = 'رقم الصلاحية: ';
DataDictionaryAR["ItemLotManagementResults"] = 'النتائج';
DataDictionaryAR["LotTransactions"] = 'الحركات';

DataDictionaryAR["LotNo"] = 'الرقم';
DataDictionaryAR["ExpiryDate"] = 'تاريخ الانتهاء';
DataDictionaryAR["NextLotNumber"] = "رقم القادم";
DataDictionaryAR["IcItemCardLotsAdd"] = "اضافة";

DataDictionaryAR['IcLotNumberAlreadyExists'] = 'الرقم موجود مسبقا';
DataDictionaryAR['IcMaximumNumberOfLotsVldMsg'] = 'Maximum number of Lot is ';
DataDictionaryAR['IcLotNumberAlreadyInserted'] = 'الرقم مضاف بمسبقا.'
DataDictionaryAR['IcLotNumberNotAvailable'] = 'Lot Number doesn\'t exist/Not available.';
DataDictionaryAR['IcLotQuantityMismatchError'] = 'Quantity doesn\'t match the selected lots in the following Receipt line(s) :<br/>';
DataDictionaryAR["ItemLots"] = "Item Lots";

DataDictionaryAR["SystemAlert"] = 'System Alert';
DataDictionaryAR["QuantitySpecifiedExceedsAvailableQuantity"] = 'Quantity Specified Exceeds Available Serials, Please adjust the quantity then try again';
DataDictionaryAR["NoRelatedSerialsFound"] = 'No Related Serials Found for this Transaction';
DataDictionaryAR['InsertLotsManually'] = 'Insert Lots Manually';
DataDictionaryAR["vldLotAvailableQnty"] = 'Quantity more than lot available quantity';


DataDictionaryAR["ExpiryDateFrom"] = 'تاريخ الصلاحية من: ';
DataDictionaryAR["ExpiryDateTo"] = 'تاريخ الصلاحية إلى: ';

DataDictionaryAR["NoRelatedLotsFound"] = 'No Related Lots Found';
DataDictionaryAR["QuantitySpecifiedExceedsAvailableQuantityLots"] = 'Quantity Specified Exceeds Available Lots, Please adjust the quantity then try again. Available Quantity: ';

DataDictionaryAR["ReturnedQuantityIsGreaterThanQuantityError"] = 'Returned Quantity Cannot be greater than quantity';

DataDictionaryAR["AnItemCardCannotbeSerializedandLotted"] = 'An Item Card Cannot be Serialized and Lotted';
DataDictionaryAR["vldLotAvaliabiltyErrors"] = "The listed lots are not avaliable :";

DataDictionaryAR["MovingAverage"] = 'Moving Average';
DataDictionaryAR["vldUndefinedClosingAcc"] = "Insert Default Closing Account First";





//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// IcReceipt English Dictionary////////////////////////////////////////////////
DataDictionaryAR['AddIcReceipt'] = 'اضافة ادخال';
DataDictionaryAR['Reference'] = 'مرجع';
DataDictionaryAR['PostingDate'] = 'تاريخ الترحيل';
DataDictionaryAR['ReceiptDate'] = 'تاريخ الادخال';
DataDictionaryAR['YearPeriod'] = 'السنة\الفترة';
DataDictionaryAR['AdditionalCost'] = 'التكلفة الاضافية';
DataDictionaryAR['TotalCost'] = 'مجموع التكلفة';
DataDictionaryAR['TotalExtendedCost'] = 'مجموع التكلفة التفصيلية';
DataDictionaryAR['IcReceiptDetails'] = 'تفاصيل سند الادخال';
DataDictionaryAR['ReceiptNumber'] = 'رقم  سند الادخال';
DataDictionaryAR['HReceiptNumber'] = 'رقم سند الادخال : ';
DataDictionaryAR['HReceiptDate'] = 'تاريخ  سند الادخال : ';
DataDictionaryAR['Posted'] = 'مرحلة';
DataDictionaryAR['NotPosted'] = 'معلقة';
DataDictionaryAR['AdditionalCostAction'] = 'Additional Cost Action';
DataDictionaryAR['TotalReturnCost'] = 'مجموع تكلفة الارجاع';
DataDictionaryAR['QuantityReceived'] = 'الكمية المستلمة';
DataDictionaryAR['UnitCost'] = 'تكلفة الوحدة';
DataDictionaryAR['ExtendedCost'] = 'التكلفة التفصيلية';
DataDictionaryAR['Comments'] = 'التعليقات';
DataDictionaryAR['QuantityReturned'] = 'الكمية المرتجعة';
DataDictionaryAR['ReturnCost'] = 'التكلفة المرتجعة';
DataDictionaryAR['ItemCardDescription'] = 'وصف المادة';
DataDictionaryAR['AdjustedCost'] = 'التكلفة المعدلة';
DataDictionaryAR['TotalAdjusted'] = 'مجموع التسوية';
DataDictionaryAR['PONumber'] = 'PO Number';
DataDictionaryAR['AutomaticProration'] = 'Automatic Additional Cost Proration';
DataDictionaryAR['ManualProration'] = 'Manual Proration';
DataDictionaryAR['IcAccountsDoesNotExist'] = 'Following GL Account(s) does not exist: ';
DataDictionaryAR['CreateAutomaticAPInvoice'] = 'Create Automatic AP Invoice';
DataDictionaryAR['ICSetup'] = "اعدادات المخزون";



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// IcItemCard English Dictionary////////////////////////////////////////////////
DataDictionaryAR["IcItemCard"] = 'كرت المادة'; //'Item Card';
DataDictionaryAR["AddIcItemCard"] = 'اضافة كرت مادة'; //'Add Item Card';
DataDictionaryAR['IcItemCardDetails'] = 'تفاصيل كرت المادة'; //'Item Card Details';
DataDictionaryAR["ItemCard"] = "كرت المادة"; //"Item Card";
DataDictionaryAR['StructureCode'] = 'رمز تركيب رقم المادة :';
DataDictionaryAR['ItemNumber'] = 'رقم المادة';
DataDictionaryAR['ItemDescription'] = 'وصف المادة';
DataDictionaryAR['CoastingMethod'] = 'طريقة التكليف';
DataDictionaryAR['DefaultPriceList'] = 'قائمة الاسعار الافتراضية';
DataDictionaryAR['DefaultPickingSequence'] = 'رقم الرف الافتراضي';
DataDictionaryAR['UnitWeight'] = 'وزن الوحدة';
DataDictionaryAR['WeightUnitOfMeasure'] = 'وحدة قياس الوزن';
DataDictionaryAR['AlternateItem'] = 'رقم المادة البديل';
DataDictionaryAR['AdditionalItemInformation'] = 'معلومات اضافية للمادة';
DataDictionaryAR['IsActive'] = 'فعال';
DataDictionaryAR['StockItem'] = 'مادة قابلة للتخزين';
DataDictionaryAR['SerialNumber'] = 'مادة ذات رقم تسلسلي';
DataDictionaryAR['LotNumber'] = 'مادة ذات مدة صلاحية';
DataDictionaryAR['Sellable'] = 'قابل للبيع';
DataDictionaryAR['KittingItem'] = 'مادة طقم';
DataDictionaryAR['ItemCardTax'] = 'فئة ضريبة المادة'; //'Item Card Tax';
DataDictionaryAR['ItemCardLocation'] = 'مستودع كرت المادة'; //'Item Card Location';

DataDictionaryAR["AlreadyExist"] = ' موجود مسبقا ';
//////List Header English Dictionary JS
DataDictionaryAR['HItemNumber'] = 'رقم كرت المادة : ';
DataDictionaryAR['HItemDescription'] = 'وصف كرت المادة : ';
DataDictionaryAR['HCoastingMethod'] = 'طريقة التكليف : ';
DataDictionaryAR['HDefaultPriceList'] = 'قائمة الاسعار الافتراضية : ';
DataDictionaryAR['StockingUnitOFMeasure'] = 'وحدة القياس الخاصة بالتخزين';
DataDictionaryAR["ItemUnitOFMeasure"] = "وحدة القياس الخاصة بكرت المادة";
DataDictionaryAR["ItemTax"] = "ضرائب المادة";
DataDictionaryAR["TaxAuthority"] = "مجموعة الضرائب";
DataDictionaryAR["TaxAuthorityDesc"] = "وصف مجموعة الضرائب";
DataDictionaryAR["SalesTaxClass"] = "قائمة ضريبة المبيعات";
DataDictionaryAR["PurchaseTaxClass"] = "قائمة ضريبة المشتريات";
DataDictionaryAR["locationCosting"] = "المستودع والكلفة";
DataDictionaryAR["Allowed"] = "مسموح";
DataDictionaryAR["PickingSequence"] = "رقم الرف";
DataDictionaryAR["CostUnitOfMeasure"] = "وحدة قياس التكلفة";
DataDictionaryAR["StandardCost"] = "التكلفة المعيارية";
DataDictionaryAR["MostRecentCost"] = "احدث تكلفة";
DataDictionaryAR["LastUnitCost"] = "اخر تكلفة";
DataDictionaryAR["AllLocations"] = "اضفها الى جميع المستودعات ";
DataDictionaryAR["btnCreate"] = "انشاء";
DataDictionaryAR["ItemBarCode"] = "باركود";
DataDictionaryAR["ItemSku"] = "Sku";
DataDictionaryAR["TaxAuthorityDescription"] = "وصف مجموعة الضرائب";
DataDictionaryAR["ItemImage"] = "صورة كرت المادة";
DataDictionaryAR["ValidateQuantityOnBoQ"] = "Validate Quantity on BoQ";
DataDictionaryAR["ConstructItemNumber"] = "Create Number";
DataDictionaryAR['LengthMustBe'] = 'Length must be ';
DataDictionaryAR['HNumber'] = 'Number: ';
DataDictionaryAR['IcItemNumberMaxLengthAllowedIs'] = 'Max item number length allowed is ';

DataDictionaryAR['HasVarianceSegments'] = 'Has Variance Segments';
DataDictionaryAR['NoOfVarianceSegments'] = 'No. of Variance Segments';

DataDictionaryAR['ItemNumberFormat'] = 'Item Number Format ';
DataDictionaryAR['IcUOMConversionFactorVld'] = 'Conversion Factor for the stocking UOM has to be 1.';
DataDictionaryAR['IcUOMRequiredVld'] = 'You have to select a stocking UOM (with conversion factor=1).';
DataDictionaryAR['IcItemSerialsCancelMsg'] = 'Serials entered will be lost! Are you sure you want to Cancel?';

DataDictionaryAR['ManufacturingCompany'] = 'الشركة المصنعه';
DataDictionaryAR['Capacity'] = 'السعة';
DataDictionaryAR['AreYouSureYouWantToCancel'] = 'هل انت متأكد من الالغاء ؟';
DataDictionaryAR["ConfirmMsgCancle"] = 'اغلاق';
DataDictionaryAR["ConfirmMsgConfirm"] = 'تاكيد';



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// IcItemLocationCosting English Dictionary////////////////////////////////////////////////
DataDictionaryAR["IcItemLocationCostingDetails"] = 'تكلفة وموقع المادة';
DataDictionaryAR["HItemLocationCosting"] = 'Location & Costing Id: ';
DataDictionaryAR["HLocation"] = 'المستودع: ';
DataDictionaryAR["HItemCard"] = 'المادة: '; //'Item Card: ';
DataDictionaryAR["AddIcItemLocationCostingInfo"] = 'Add Item Location And Costing';
DataDictionaryAR["ItemLocationCostingId"] = "Id";
DataDictionaryAR["ItemLocationCosting"] = "تكلفة وموقع المادة";
DataDictionaryAR['InUse'] = 'قيد الاستعمال';
DataDictionaryAR['AverageCost'] = 'متوسط التكلفة';
DataDictionaryAR['QuantityOnHand'] = 'الكمية الموجودة حاليا';
DataDictionaryAR['QuantityOnPurchaseOrder'] = 'الكمية المسجلة على طلبات الشراء';
DataDictionaryAR['QuantityOnSellOrder'] = 'المية المسجلة على طلبات البيع';
DataDictionaryAR['QuantityAvailableToShip'] = 'الكمية المتوفرة للاخراج';
DataDictionaryAR['QuantityCommitted'] = 'الكمية المحجوزة';
DataDictionaryAR['IcItemLocationCosting'] = 'تكلفة وموقع المادة ';
DataDictionaryAR['QtyOnHand'] = 'الكمية الموجودة حاليا';

DataDictionaryAR["Reset"] = 'اعادة';
DataDictionaryAR["Search"] = 'بحث';


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// IcTransaction English Dictionary////////////////////////////////////////////////
DataDictionaryAR['ICTransactions'] = "الحركات";
DataDictionaryAR['IcReceipt'] = "سندات الادخال";
DataDictionaryAR['Post'] = 'الترحيل';
DataDictionaryAR['SaveAndPost'] = 'حفظ وترحيل';
DataDictionaryAR['postedsuccessfully'] = 'تم الترحيل بنجاح';
DataDictionaryAR['FaildToPost'] = 'خطأ في الترحيل';
DataDictionaryAR['IcShipment'] = 'سندات الاخراج';
DataDictionaryAR['IcAdjustment'] = 'التسوية';
DataDictionaryAR['IcTransfer'] = 'التحويل';



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// IcShipment English Dictionary////////////////////////////////////////////////
DataDictionaryAR['ShipmentNumber'] = 'رقم  سندالاخراج';
DataDictionaryAR['ShipmentDate'] = 'تاريخ  سند الاخراج';
DataDictionaryAR['HShipmentNumber'] = 'رقم سندالاخراج: ';
DataDictionaryAR['HShipmentDate'] = 'تاريخ سند الاخراج: ';
DataDictionaryAR['AddIcShipment'] = 'اضافة  سند اخراج';
DataDictionaryAR['ShipmentType'] = 'نوع القيد';
DataDictionaryAR['ExtendedPrice'] = 'السعر التفصيلي';
DataDictionaryAR['IcShipmentDetails'] = 'تفاصيل  سندالاخراج';
DataDictionaryAR['CreateAutomaticARInvoice'] = 'انشاء تلقائي لفاتورة مقبوضات ';
DataDictionaryAR['DiscountPercent'] = 'نسبة الخصم';



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// IcShipment English Dictionary////////////////////////////////////////////////
DataDictionaryAR['AdjustmentNumber'] = 'رقم التسوية';
DataDictionaryAR['AdjustmentDate'] = 'تاريخ التسوية';
DataDictionaryAR['HAdjustmentNumber'] = 'رقم التسوية: ';
DataDictionaryAR['HAdjustmentDate'] = 'تاريخ التسوية: ';
DataDictionaryAR['AddIcAdjustment'] = 'اضافة تسوية';
DataDictionaryAR['IcAdjustmentDetails'] = 'تفاصيل التسوية ';
DataDictionaryAR['AdjustmentType'] = 'نوع التسوية';
DataDictionaryAR['BucketType'] = 'نوع المحتوى';
DataDictionaryAR['DocumentNo'] = 'رقم الوثيقة.';
DataDictionaryAR['DocumentLine'] = 'سطر الوثيقة';
DataDictionaryAR['CostDate'] = 'تاريخ التكلفة ';
DataDictionaryAR['CostAdjutment'] = 'تعديل التكلفة';
DataDictionaryAR['CostUOM'] = 'وحدة قياس التكلفة ';
DataDictionaryAR['AdjustmentAccount'] = 'حساب التسوية';
DataDictionaryAR['AdjustmentAccountDescription'] = 'وصف الحساب';

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// IcTransfer English Dictionary////////////////////////////////////////////////
DataDictionaryAR['TransferNumber'] = 'رقم التحويل';
DataDictionaryAR['TransferDate'] = 'تاريخ التحويل';
DataDictionaryAR['HTransferNumber'] = 'رقم التحويل: ';
DataDictionaryAR['HTransferDate'] = 'تاريخ التحويل: ';
DataDictionaryAR['AddIcTransfer'] = 'اضافة تحويل';
DataDictionaryAR['IcTransferDetails'] = 'تفاصيل التحويل';
DataDictionaryAR['FromLocation'] = 'من مستودع';
DataDictionaryAR['GitLocation'] = 'مستودع وسيط';
DataDictionaryAR['ToLocation'] = 'الى مستودع';
DataDictionaryAR['RequestedQuantity'] = 'الكمية المطلوبة';
DataDictionaryAR['OutstandingQuantity'] = 'الكمية المتبقية';
DataDictionaryAR['TransferQuantity'] = 'الكمية المحولة ';
DataDictionaryAR['TransferUnitOfMeasure'] = 'وحدة قياس التحويل';
DataDictionaryAR['TransferredToDate'] = 'محول الى هذا التاريخ';
DataDictionaryAR['ManualProration'] = 'توزيع يدوي';
DataDictionaryAR['ReceivedToDate'] = 'استلام الى هذه الوحدة';
DataDictionaryAR['UnitWeight'] = 'وزن الوحدة';
DataDictionaryAR['ExtendedWeight'] = 'الوزن الاجمالي';
DataDictionaryAR['WeightUnitOfMeasure'] = 'وحدة قياس الوزن';
DataDictionaryAR['WeightConversionFactor'] = 'معامل تحويل الوزن';
DataDictionaryAR['TransferType'] = 'نوع التحويل';
DataDictionaryAR['SelectedTransfer'] = 'تحويل';
DataDictionaryAR['ExpectedArrivalDate'] = 'التاريخ المتوقع للوصول';
DataDictionaryAR['ProrationMethod'] = 'طريقة التوزيع';

DataDictionaryAR["IcReports"] = "التقارير";

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// Item Number Construction English Dictionary////////////////////////////////////////////////
DataDictionaryAR['FromItem'] = 'من مادة';
DataDictionaryAR['ToItem'] = 'الى مادة';
DataDictionaryAR['IcQuantityOnHandReport'] = 'تقرير الكمية الحالية';
DataDictionaryAR['IcSalesReport'] = 'تقرير المبيعات';
DataDictionaryAR['FromQuantity'] = 'من كمية ';
DataDictionaryAR['ToQuantity'] = 'الي كمية';
DataDictionaryAR['FromValue'] = 'من قيمة ';
DataDictionaryAR['ToValue'] = 'الى قيمة';
DataDictionaryAR['IcItemDeliveryReport'] = 'تقرير التوصيل';
DataDictionaryAR['IcSellingPriceListReport'] = 'تقرير بيع قائمة الاسعار ';
DataDictionaryAR['FromPriceList'] = 'من قائمة اسعار';
DataDictionaryAR['ToPriceList'] = 'الي قائمة اسعار';
DataDictionaryAR['IcItemSellingPriceReport'] = 'تقرير بيع اسعار المواد';



/////////////////////////////////////////////////////
/////////// Inventory control options

DataDictionaryAR["ICOptions"] = "خيارات التحكم بالمخزون";
DataDictionaryAR["processing"] = "المعالجة";

DataDictionaryAR["DefaultRateType"] = "نوع المعدل الافتراضي";
DataDictionaryAR["MultiCurrency"] = "متعدد عملات";
DataDictionaryAR["ValidateLotExpiryDate"] = "التحقق من الصلاحية";
DataDictionaryAR["AllowFractionalQuantities"] = "السماح بكسور الكميات";
DataDictionaryAR["AlternateAmount1Name"] = "اسم قيمة المبلغ البديل الاول";
DataDictionaryAR["AllowItemsatAllLocations"] = "سماح المواد في جميع المستودعات";
DataDictionaryAR["AlternateAmount2Name"] = "اسم قيمه المبلغ البديل الثاني";
DataDictionaryAR["AllowNegativeInventoryLevels"] = "السماح بمستويات المخزون السلبية";
DataDictionaryAR["BaseSegment"] = "القاعدة الاساسية ";
DataDictionaryAR["AllowReceiptofNonStockItems"] = "السماح باستلام المواد الغير قابلة للتخزين";
DataDictionaryAR["ActionAdditionalCostForItemsonReceiptReturns"] = "عمل التكلفة الافتراضيه للمواد في الارجاع";
DataDictionaryAR["KeepTransactionHistory"] = "ابقاء تاريخ الحركات";
DataDictionaryAR["DefaultGoodsinTransitLocation"] = "السلع في المستودع الوسيط الافتراضية";
DataDictionaryAR["PromptForDeleteDuringPosting"] = "اخطار عند الحذف خلال الترحيل";
DataDictionaryAR["DefaultPostingDate"] = "تاريخ الترحيل الافتراضي";
DataDictionaryAR["OnlyUseDefinedUOM"] = "فقط استخدام وحدة قياس معرفة";
DataDictionaryAR["BaseUOM"] = "وحدة القياس الاساسية";
DataDictionaryAR["DefaultWeightUOM"] = "وحدة قياس الوزن الافتراضية";
DataDictionaryAR["ItemStatistics"] = "احصائيات المواد";
DataDictionaryAR["KeepItemStatistics"] = "الحفاظ على احصائيات المواد";
DataDictionaryAR["AllowEditofStatistics"] = "السماح باتعديل على احصائيات المواد";
DataDictionaryAR["AccumulateBy"] = "تتراكم بواسطة ";
DataDictionaryAR["PeriodType"] = "نوع الفترة";
DataDictionaryAR["Costing"] = "التكليف";
DataDictionaryAR["CostItemsDuring"] = "تكلفه المادة خلال";
DataDictionaryAR["CreateSubledgerTransactionsandAuditInfo.During"] = "إنشاء معاملات دفتر الأستاذ الفرعي ومعلومات التدقيق. أثناء";
DataDictionaryAR["ItemTaxOptions"] = "خيارات ضريبة المواد";
DataDictionaryAR["DefaultTaxAuthority"] = "مجموعة الضرائب الافتراضية";
DataDictionaryAR["DefaultPurchaseTaxClass"] = "فئة ضريبة المشتريات الافتراضية";
DataDictionaryAR["DefaultSalesTaxClass"] = "فئة ضريبة المبيعات الافتراضية";
DataDictionaryAR["Confirm"] = "تأكيد";
DataDictionaryAR["Cancel"] = "الغاء";
DataDictionaryAR['RoundToMultipleOf'] = 'Round To a Multiple Of';
DataDictionaryAR["DiscountPercentage"] = "نسبة الخصم";
DataDictionaryAR['IcLotsQuantityItemLocationRequiredValidationMsg'] = 'To Pick Lots you must set Item, Location, Quantity and UOM first.';
DataDictionaryAR['IcSerialsQuantityRequiredValidationMsg'] = 'To Pick Serials you must set Quantity first.';
DataDictionaryAR['IcSerialsQuantityItemLocationRequiredValidationMsg'] = 'To Pick Serials you must set Item, Location, Quantity and UOM first.';
DataDictionaryAR["ExcludeZeroBalances"] = "Exclude Ending Zero Balances";


DataDictionaryAR["ReturnedQtyError"] = 'Returned Quantity for all selected items must be greater than 0';

DataDictionaryAR["APDocumentNumber"] = "المستند #";

DataDictionaryAR["VldReceiptRowError"] = "Please enter all the mandatory fields, and related serials or lots in order to add new line";

DataDictionaryAR["ChequeReverse"] = "Cheque Reverse";

DataDictionaryAR["POS"] = 'Point Of Sale';

DataDictionaryAR["SyncConfiguration"] = 'Sync Configuration';
DataDictionaryAR["Store"] = 'Store';
DataDictionaryAR["Register"] = 'Register';
DataDictionaryAR["GiftVoucher"] = 'Gift Voucher';
DataDictionaryAR["GenerateBarcode"] = 'Generate Barcode';
DataDictionaryAR["RegisterStatus"] = 'Register Status';
DataDictionaryAR["QuickKeys"] = 'Quick Key';
DataDictionaryAR["BackEndSync"] = 'Back End Sync';
DataDictionaryAR["POSSync"] = 'POS Sync';
DataDictionaryAR["SyncSchedule"] = 'Sync Schedule';

DataDictionaryAR["POSSYNCHRONIZATION"] = 'POS SYNCHRONIZATION';

DataDictionaryAR["IsPosUser"] = 'Is POS User';

///////////////////////////////////////// Shifts arabic Dictionary////////////////////////////////////////////////
DataDictionaryAR["Shifts"] = 'فترات الدوام';
DataDictionaryAR["AddShifts"] = 'اضافه فترة دوام';
DataDictionaryAR['ShiftsDetails'] = 'تفاصيل فترة الدوام';
DataDictionaryAR['Shift'] = 'فترة دوام';
DataDictionaryAR['ShiftName'] = 'اسم فترة الدوام';
DataDictionaryAR['ShiftDescription'] = 'وصف فترة الدوام';
DataDictionaryAR['StartTime'] = 'وقت البدء';
DataDictionaryAR['EndTime'] = 'وقت الانتهاء';
DataDictionaryAR['ValidateShift'] = 'ربط بفترة دوام';
DataDictionaryAR['HShiftName'] = 'اسم فترة الدوام :';
DataDictionaryAR['UserStoresShifts'] = 'فروع و فترات دوام المستخدم';
DataDictionaryAR['DublicatedShiftName'] = 'اسم فترة الدوام موجود مسبقا';
DataDictionaryAR['AddStoreAndShift'] = 'اضافة فرع و فترة دوام';
DataDictionaryAR['Dublicated'] = 'فترة الدوام موجودة مسبقا';

DataDictionaryAR['Pending'] = 'Pending';

DataDictionaryAR["ERPIntegration"] = "ERP Integration";
DataDictionaryAR["POSShipmentPostResult"] = "Created Transactions Numbers : <br>";


DataDictionaryAR["NoInvoiceToPost"] = "No Invoice To Post";

DataDictionaryAR["PostInvoice"] = ' Post Invoice ';
DataDictionaryAR["PostRefund"] = ' Post Refund ';
DataDictionaryAR["PostOnAccount"] = ' Post On Account ';

DataDictionaryAR["InvoiceInquiry"] = 'Invoice Inquiry';
DataDictionaryAR["InvoiceDateFrom"] = 'Invoice Date From';
DataDictionaryAR["InvoiceDateTo"] = 'Invoice Date To';


DataDictionaryAR["syncCompany"] = 'sync Company';

DataDictionaryAR["POSInquiries"] = 'POS Inquiries';
DataDictionaryAR["CloseRegisterInquiry"] = 'Close Register Inquiry';


DataDictionaryAR["IsIdentical"] = 'Is Identical';
DataDictionaryAR["OpenedCashLoan"] = 'Opened Cash Loan';
DataDictionaryAR["DifferenceAmount"] = 'Difference Amount';

DataDictionaryAR["OpenedDateFrom"] = 'Opened Date From';
DataDictionaryAR["OpenedDateTo"] = 'Opened Date To';
DataDictionaryAR["ClosedDateFrom"] = 'Closed Date From';
DataDictionaryAR["ClosedDateTo"] = 'Closed Date To';

DataDictionaryAR["DiscountAccount"] = 'Discount Account';

DataDictionaryAR["PleaseCheckAdditionalCostVendors"] = 'Please Check Additional Cost Vendors, Vendors Must have same Currency as it\'s Receipt';
DataDictionaryAR["CurrencyMismatch"] = 'Currency Mismatch';


DataDictionaryAR["Totalresultscounter"] = 'Total results counter: ';

DataDictionaryAR["PhysicalLocation"] = 'Physical Location';

DataDictionaryAR["UsernameandPassword"] = 'User name and Password';
DataDictionaryAR["pending"] = "Pending";
DataDictionaryAR["Inprogress"] = "Inprogress";
DataDictionaryAR["failed"] = "Failed";
DataDictionaryAR["success"] = "Success";
DataDictionaryAR["SyncEntities"] = "Sync Entities";
DataDictionaryAR["SyncBy"] = "Sync By";
DataDictionaryAR["SyncDate"] = "Sync Date";


DataDictionaryAR["ParentInvoiceNotPostedYet"] = 'Parent invoice of one or more of the refund invoices is/are not posted yet, please post them then post refund again.';

DataDictionaryAR["ShipmentDiscountExceedsTotalAmount"] = 'Shipment Discount Exceeds Total Amount';



DataDictionaryAR["DetailsCurrenciesAreUsedBySystem"] = 'Failed to delete; Details Currencies Are Used By ERP system data';

DataDictionaryAR["IcReceiptDiscountExceedsReceiptAmount"] = 'Receipt Discount Exceeds it\'s Amount, resulting in a negative total cost!';

DataDictionaryAR["CurrencyAlreadyExists"] = 'Currency Cannot be duplicated for the same header currency';

DataDictionaryAR["QuantityInLocationsIsNotEnoughtoFullyReturnReceipt"] = 'Quantities In Locations are Not Enough to Fully Return Receipt, Negative Quantities are not allowed !';

DataDictionaryAR["PostExchange"] = 'Post Exchange';

DataDictionaryAR["ExchangeAccount"] = 'Exchange Clearing Account';


DataDictionaryAR["TotalAfterDiscount"] = 'Total After Discount';
DataDictionaryAR["TotalAfterTax"] = 'Total After Tax and Discount';

DataDictionaryAR["TotalExtendedPrice"] = 'Total Extended Price';


DataDictionaryAR["ItemFrom"] = 'Item from';
DataDictionaryAR["ItemTo"] = 'Item to';

DataDictionaryAR["InvoiceReport"] = 'Invoice report';

DataDictionaryAR["TotalDailySales"] = 'Total daily sales report';
DataDictionaryAR["DiscountReport"] = 'Discount report';
DataDictionaryAR["DailySalesPerItem"] = 'Daily sales per item report';
DataDictionaryAR["Dailysales"] = 'Daily sales report';
DataDictionaryAR["ChequeDetails"] = 'Cheque details report';


DataDictionaryAR["LastValueInserted"] = 'Last Value Inserted';
DataDictionaryAR["MustBeGreaterThan0"] = 'Quantity Must Be Greater Than 0';

DataDictionaryAR["ItemsType"] = 'Items Type';
DataDictionaryAR["ItemTransactionsReport"] = 'Item Transactions Report';


DataDictionaryAR["Skype"] = 'Skype';

DataDictionaryAR["ItemIsNotAvailableInTheSourceLocation"] = 'Item Is Not Available In The Source Location';

DataDictionaryAR["SummarySalesPerItem"] = 'Summary Sales Per Item';

DataDictionaryAR["AccessCode"] = 'Access Code';

DataDictionaryAR["FromBrand"] = 'From Brand';
DataDictionaryAR["ToBrand"] = 'To Brand';
DataDictionaryAR["FromCategory"] = 'From Category';
DataDictionaryAR["ToCategory"] = 'To Category';
DataDictionaryAR["FromColor"] = 'From Color';
DataDictionaryAR["ToColor"] = 'To Color';
DataDictionaryAR["FromSize"] = 'From Size';
DataDictionaryAR["ToSize"] = 'To Size';

DataDictionaryAR["AthletiqueSalesReport"] = 'Details Sales Report';

DataDictionaryAR["PostAll"] = 'Post All';



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////Start Purchase Order Module ////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////PO Invoice English Dictionary JS///////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryAR["PoInvoice"] = 'الفواتير';
DataDictionaryAR["AddPoInvoice"] = 'اضافة فاتورة جديدة';
DataDictionaryAR['PoInvoiceDetails'] = 'تفاصيل الفاتورة';
DataDictionaryAR['Invoice'] = 'الفاتورة';
DataDictionaryAR['InvoiceReference'] = 'مرجع الفاتورة';
DataDictionaryAR['InvoiceTotalAmount'] = 'مجموع الفاتورة';
DataDictionaryAR['Vendor'] = 'البائع';
DataDictionaryAR['ReceiptIds'] = 'ارقام سندات الادخال';
DataDictionaryAR['InvoiceDate'] = 'تاريخ الفاتورة';
DataDictionaryAR['InvoicingDate'] = 'تاريخ الفوترة ';
DataDictionaryAR['RelatedPO'] = 'طلبات الشراء المرتبطة';
DataDictionaryAR['ReceiptItemLine'] = 'Receipt Item Line';
DataDictionaryAR['InvoiceTotalAmount'] = 'مجموع الفاتورة';
DataDictionaryAR['PostingDate'] = 'تاريخ النشر';
DataDictionaryAR['Description'] = 'الوصف';
DataDictionaryAR['DocumentReferenceNumber'] = 'رقم الفاتورة المرجعي';
DataDictionaryAR['InvoiceNumber'] = 'رقم الفاتورة';
DataDictionaryAR['TotalAmountWithTax'] = 'مجموع الفاتورة مع الضريبة';
DataDictionaryAR['TotalAmountAfterDiscount'] = 'مجموع الفاتورة بعد الخصم';
DataDictionaryAR['TotalAmountBeforDiscount'] = 'مجموع الفاتورة قبل الخصم';
DataDictionaryAR['DiscountAmount'] = 'قيمة الخصم';
DataDictionaryAR['DiscountPercentage'] = 'نسبة الخصم';
//////List Header English Dictionary JS
DataDictionaryAR['InvoiceReference'] = 'مرجع الفاتورة';
DataDictionaryAR['InvoiceTotalAmount'] = 'مجموع الفاتورة';
////Tab English Dictionary JS
DataDictionaryAR['AdditionalCost'] = 'التكاليف الاضافية';
DataDictionaryAR['AddNewAdditionalCost'] = 'اضافة تكاليف اضافية جديدة';

///////////////////////////////////////////////////////////////////PO entity/////////////////////////////////////////////////////////////////////////////////////////////

DataDictionaryAR["PurchaseOrderHeader"] = 'طلب الشراء';
DataDictionaryAR["AddPurchaseOrderHeader"] = 'اضافة طلب شراء';
DataDictionaryAR['PurchaseOrderHeaderDetails'] = 'تفاصيل طلب الشراء';
DataDictionaryAR['ReferenceNumber'] = 'الرقم المرجعي';
DataDictionaryAR['PurchaseDate'] = 'تاريخ طلب الشراء';
DataDictionaryAR['Currency'] = 'العملة';
DataDictionaryAR['Status'] = 'الحالة';
DataDictionaryAR['AutoGenerateReceipt'] = 'انشاء سند ادخال تلقائي';
DataDictionaryAR['AutoGenerateInvoice'] = 'انشاء فاتورة تلقائي';
DataDictionaryAR['Vendor'] = 'البائع';
DataDictionaryAR['TaxClass'] = 'فئة الضريبة';
DataDictionaryAR['ExchangeRate'] = 'سعر الصرف';
DataDictionaryAR['Description'] = 'الوصف';
DataDictionaryAR['HeaderDiscountAmount'] = 'قيمة الخصم';
DataDictionaryAR['HeaderDiscountPercentage'] = 'نسبة الخصم';
DataDictionaryAR['NumberOfLines'] = 'عدد الاسطر';
DataDictionaryAR['TotalCost'] = 'مجموع التكلفة';
DataDictionaryAR['TotalQuantity'] = 'مجموع الكمية';
DataDictionaryAR['TotalAmountWithTax'] = 'مجموع الفاتورة مع الضريبة';
DataDictionaryAR['TotalAmountAfterDiscount'] = 'مجموع الفاتورة بعد الخصم';
DataDictionaryAR['TotalAmountBeforeDiscount'] = 'مجموع الفاتورة قبل الخصم';
DataDictionaryAR['IsPosted'] = 'تم نشره';
DataDictionaryAR['PostingDate'] = 'تاريخ النشر';
DataDictionaryAR['ItemCard'] = 'بطاقة القطعة';
DataDictionaryAR['ItemDescription'] = 'وصف القطعة';
DataDictionaryAR['Location'] = 'الموقع';
DataDictionaryAR['Quantity'] = 'الكمية';
DataDictionaryAR['UnitOfMeasure'] = 'وحدة القياس';
DataDictionaryAR['UnitCost'] = 'كلفة القطعة';
DataDictionaryAR['TaxClass'] = 'فئة الضريبة';
DataDictionaryAR['UnitDiscountAmount'] = 'قيمة الخصم';
DataDictionaryAR['UnitDiscountPercentage'] = 'نسبة الخصم';
DataDictionaryAR['ReceivedQuantity'] = 'تلقي الكمية';
DataDictionaryAR['OutstandingQuantity'] = 'الكمية المعلقة';
DataDictionaryAR['UnitTotalAmountBeforeDiscount'] = 'مجموع الفاتورة قبل الخصم';
DataDictionaryAR['UnitTotalAmountAfterDiscount'] = 'مجموع الفاتورة بعد الخصم';
DataDictionaryAR['UnitTotalAmountWithTax'] = 'مجموع الفاتورة مع الضريبة';
DataDictionaryAR['Comments'] = 'تعليقات';
DataDictionaryAR['IsLastOne'] = 'هو اخر واحد';
DataDictionaryAR['POStatusNew'] = 'جديد';
DataDictionaryAR['POStatusInProgress'] = 'في التقدم';
DataDictionaryAR['POStatusCompleted'] = 'انتهى بنجاح';

DataDictionaryAR['HeaderReferenceNumber'] = "رقم طلب الشراء : ";
DataDictionaryAR['HeaderPODate'] = "تاريخ طلب الشراء : ";
DataDictionaryAR['HeaderStatus'] = "الحالة : ";

DataDictionaryAR['PODetails'] = "تفاصيل طلب الشراء";
/////////////////////////////////////////English Dictionary JS///////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryAR["PoReceipt"] = 'سند ادخال';
DataDictionaryAR["Receipt"] = 'سند ادخال';
DataDictionaryAR["ReceiptSummary"] = 'ملخص سند الادخال';
DataDictionaryAR["AddPoReceipt"] = 'اضافة سند ادخال جديد';
DataDictionaryAR["ReceiptDetails"] = 'تفاصيل سند الادخال';
DataDictionaryAR["ReceiptLineDetails"] = 'تفاصيل سند الادخال';
DataDictionaryAR['PoReceiptDetails'] = 'تفاصيل سند الادخال';
DataDictionaryAR['SummaryPoReceiptDetails'] = 'PO Summary Receipt Details';
DataDictionaryAR['Receipt'] = 'سند ادخال';
DataDictionaryAR['ReceiptStatus'] = 'حالة سند الادخال';
DataDictionaryAR['Reference'] = 'الرقم المرجعي';
DataDictionaryAR['ReceiptDate'] = 'تاريخ سند الادخال';
DataDictionaryAR['PurchaseOrder'] = 'طلب الشراء';
DataDictionaryAR['PurchaseOrderReference'] = 'مرجع طلب الشراء';
DataDictionaryAR['CreateAutomaticInvoice'] = 'انشاء فاتورة تلقائي';
DataDictionaryAR['AutomaticAdditionalCostProration'] = 'Automatic Additional Cost Proration';
DataDictionaryAR['Vendor'] = 'البائع';
DataDictionaryAR['Description'] = 'الوصف';
DataDictionaryAR['HeaderDiscountAmount'] = 'قيمة الخصم';
DataDictionaryAR['HeaderDiscountPercentage'] = 'نسبة الخصم';
DataDictionaryAR['NumberOfLines'] = 'عدد الاسطر';
DataDictionaryAR['TotalCost'] = 'مجموع التلفة';
DataDictionaryAR['TotalRQuantity'] = 'مجموع الكمية المتلقية';
DataDictionaryAR['TotalAmountWithTax'] = 'مجموع الفاتورة مع الضريبة';
DataDictionaryAR['TotalAmountAfterDiscount'] = 'مجموع الفاتورة بعد الخصم';
DataDictionaryAR['TotalAmountBeforeDiscount'] = 'مجموع الفاتورة دون الخصم';
//////List Header English Dictionary JS
DataDictionaryAR['HReference'] = 'الرقم المرجعي : ';
DataDictionaryAR['HReceiptDate'] = 'تاريخ سند الادخال : ';
DataDictionaryAR['HTotalCost'] = 'مجموع التكلفة : ';
////Tab English Dictionary JS
DataDictionaryAR['AdditionalCostAmount'] = 'قيمة التلفة الاضافية';
DataDictionaryAR['AddNewAdditionalCost'] = 'اضافة تكلفة اضافية جديدة';
DataDictionaryAR['LoadReceipts'] = 'تحميل سندات الادخال';
DataDictionaryAR['PurchaseOrderQuantity'] = 'كمية طلب الشراء';
DataDictionaryAR['ReceivedQuantity'] = 'كمية سندات الادخال';
DataDictionaryAR['OutstandingQuantity'] = 'الكمية المعلقة';
DataDictionaryAR['UOM'] = 'وحدة القياس';
DataDictionaryAR['PleasLoadReceipts'] = 'الرجاء تحميل سندات الادخال';
DataDictionaryAR['InvoiceDocumentNumber'] = 'الرقم الموثق للفاتورة';

DataDictionaryAR['PostingPO'] = 'حفظ ونشر طلب الشراء';
DataDictionaryAR['AreYouSureToPostPO'] = 'هل انت متاكد من حفظ ونشر طلب الشراء ؟';
DataDictionaryAR['LockedPeriod'] = 'تاريخ النشر لا يمكن أن يكون في غضون فترة مقفلة';
DataDictionaryAR['OriginalQuantity'] = 'الكمية الاصلية';
DataDictionaryAR['PurchaseOrderQuantity'] = 'كمية طلب الشراء';
DataDictionaryAR['OutstandingQuantity'] = 'الكمية المعلقة';
DataDictionaryAR['EditQuantity'] = 'تعديل الكمية';
DataDictionaryAR['POInquery'] = 'حالة طلب الشسراء';


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////// PO Receipt Return //////////////////////////////////////
DataDictionaryAR["ReceiptReturn"] = 'سندات الادخال العائدة';
DataDictionaryAR["PoReceiptReturn"] = 'PO Receipt Return';
DataDictionaryAR["AddPoReceiptReturn"] = 'Add PO Receipt Return';
DataDictionaryAR['PoReceiptReturnDetails'] = 'PO Receipt Return Details';
DataDictionaryAR['PoReceiptReturn'] = 'PO Receipt Return';

DataDictionaryAR['ReturnReference'] = 'Return Reference';
DataDictionaryAR['ReturnDate'] = 'Return Date';
DataDictionaryAR['ReturnDescription'] = 'Return Description';
DataDictionaryAR['ReturnAllQuantity'] = 'Return All Quantity';
DataDictionaryAR['Vendor'] = 'Vendor';
DataDictionaryAR['TaxClass'] = 'Tax Class';
DataDictionaryAR['Currency'] = 'Currency';
DataDictionaryAR['ExchangeRate'] = 'Exchange Rate';
DataDictionaryAR['ReceiptTotalAmount'] = 'Receipt Total Amount';
DataDictionaryAR['ReturnedCost'] = 'Returned Cost';
DataDictionaryAR['ReturnedQuantity'] = 'Returned Quantity';
DataDictionaryAR['RelatedInvoice'] = 'Related Invoice';
DataDictionaryAR['RelatedInvoicesIds'] = 'Related Invoices Ids';
DataDictionaryAR['PoReceiptAdditionalCost'] = 'PO Receipt Additional Cost';
DataDictionaryAR['AdditionalCostAction'] = 'Additional Cost Action';
DataDictionaryAR['IsPosted'] = 'Is Posted';
DataDictionaryAR['PostingDate'] = 'Posting Date';

//////////////////////////////////////////////InqueryPage //////////////////////////////////////////
DataDictionaryAR['PurchaseOrderStatus'] = 'Purchase Order Status';
DataDictionaryAR['PurchaseOrderReference'] = 'Purchase Order Reference';
DataDictionaryAR['ReceiptReference'] = 'Receipt Reference';
DataDictionaryAR['InvoiceReference'] = 'Invoice Reference';
DataDictionaryAR['Results'] = 'Results';
DataDictionaryAR['Transaction'] = 'Transaction';
DataDictionaryAR['TransactionDate'] = 'Transaction Date';
DataDictionaryAR['TypeCreatedDate'] = 'Type Create Date';
DataDictionaryAR['TypeTransactionDate'] = 'Type Transaction Date';
DataDictionaryAR['TypeTotalAmount'] = 'Type Total Amount';
DataDictionaryAR['ExceedsUnitTotal'] = 'Exceeds Ext. cost';
DataDictionaryAR['ExceedsPOTotalAmount'] = 'Exceeds PO Total Amount';
DataDictionaryAR['ExceedsReciptTotalAmount'] = 'Exceeds Total Amount';

//////////////////////////////////////// End Purchase Order ///////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



DataDictionaryAR["ParentInvoiceNotPostedYet"] = 'Parent invoice of one or more of the refund invoices is/are not posted yet, please post them then post refund again.';

DataDictionaryAR["ShipmentDiscountExceedsTotalAmount"] = 'Shipment Discount Exceeds Total Amount';



DataDictionaryAR["DetailsCurrenciesAreUsedBySystem"] = 'Failed to delete; Details Currencies Are Used By ERP system data';


DataDictionaryAR["IcReceiptDiscountExceedsReceiptAmount"] = 'Receipt Discount Exceeds it\'s Amount, resulting in a negative total cost!';


DataDictionaryAR["CurrencyAlreadyExists"] = 'Currency Cannot be duplicated for the same header currency';

DataDictionaryAR["QuantityInLocationsIsNotEnoughtoFullyReturnReceipt"] = 'Quantities In Locations are Not Enough to Fully Return Receipt, Negative Quantities are not allowed !';

DataDictionaryAR["POSummary"] = "Purchase Order Summary";

/////////////////////////////////////////English Dictionary JS///////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryAR["PoReturnReceipt"] = 'Receipt Return ';
DataDictionaryAR["AddPoReturnReceipt"] = 'Add New Return Receipt ';
DataDictionaryAR['PoReturnReceiptDetails'] = 'Receipt Return Details';
DataDictionaryAR['PoReturnReceipt'] = 'Receipt Return ';
DataDictionaryAR['ReturnReferenceNumber'] = 'Return Reference Number';
DataDictionaryAR['Receipt'] = 'Receipt';
DataDictionaryAR['ReturnDescription'] = 'Return Description';
DataDictionaryAR['ReturnDate'] = 'Return Date';
DataDictionaryAR['ReturnAllQuantity'] = 'Return All Quantity';
DataDictionaryAR['Vendor'] = 'Vendor';
DataDictionaryAR['TaxClass'] = 'Tax Class';
DataDictionaryAR['Currency'] = 'Currency';
DataDictionaryAR['ExchangeRate'] = 'Exchange Rate';
DataDictionaryAR['ReceiptTotalQuantity'] = 'Receipt Total Quantity';
DataDictionaryAR['ReturnTotalQuantity'] = 'Return Total Quantity';
DataDictionaryAR['ReceiptTotalAmountWithTax'] = 'Receipt Total Amount With Tax';
DataDictionaryAR['ReceiptTotalAmountAfterDiscount'] = 'Receipt Total Amount After Discount';
DataDictionaryAR['ReceiptTotalAmountBeforeDiscount'] = 'Receipt Total Amount Before Discount';
DataDictionaryAR['ReturnTotalAmountWithTax'] = 'Return Total Amount With Tax';
DataDictionaryAR['ReturnTotalAmountAfterDiscount'] = 'Return Total Amount After Discount';
DataDictionaryAR['ReturnTotalAmountBeforeDiscount'] = 'Return Total Amount Before Discount';
DataDictionaryAR['RelatedInvoice'] = 'Related Invoice';
DataDictionaryAR['InvoiceReference'] = 'Invoice Reference';
DataDictionaryAR['InvoiceAmount'] = 'Invoice Amount';
DataDictionaryAR['NumberOfLines'] = 'Number Of Lines';

DataDictionaryAR["AlreadyExist"] = ' Already Exist ';
//////List Header English Dictionary JS
DataDictionaryAR['HReferenceNumber'] = 'Reference Number : ';
DataDictionaryAR['HReturnDate'] = 'Return Date : ';
DataDictionaryAR['HReturnTotalAmountWithTax'] = 'Return Total Amount With Tax : ';
DataDictionaryAR['ReceiptTotalAmount'] = 'Receipt Total Amount';
DataDictionaryAR['ReturnQuantity'] = 'Return Quantity';
DataDictionaryAR['ReturnTotalCost'] = 'Return Total Cost';
DataDictionaryAR['HReturnTotalCost'] = 'Return Total Cost : ';
DataDictionaryAR["ReceiptReturnLineDetails"] = 'Receipt Return Line Details';

DataDictionaryAR["ErrorPostDate"] = 'Posting date must be greater than or equal transaction date';

////Tab English Dictionary JS

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryAR['PoUploadLog'] = 'PO Upload Log';
DataDictionaryAR['CompletedWithError'] = 'Completed With Error';
DataDictionaryAR['ExportTemplate'] = 'Export Template';
DataDictionaryAR['AdditionalCostClearingAccount'] = 'Additional Cost Clearing Account';

DataDictionaryAR['NonProratedAdditionalCostTotal'] = 'Non Prorated Additional Cost Total';
DataDictionaryAR['LineTotalCost'] = 'Line Total Cost';
DataDictionaryAR['TotalCostInfo'] = 'this total includes the Receipt total after the discount including the prorated additional Cost and the Tax.';
DataDictionaryAR['ErrorNumberDetailsLine'] = 'Details must less than 100 line';
DataDictionaryAR['ErrorQtyZero'] = 'po cannot be "saved" or "saved asd posted" having no quantity determined';

DataDictionaryAR["PostingPODateMissing"] = 'Posting Date Missing';
DataDictionaryAR["EditThenPost"] = 'To proceed with posting, please edit to add posting date then Save and Post';

DataDictionaryAR["RollupAccountsReport"] = 'Rollup Accounts';
DataDictionaryAR["RollupAccountsReportt"] = 'Rollup Accounts Report';

DataDictionaryAR["VendorBalanceReport"] = 'Vendor Balance';
DataDictionaryAR["VendorBalanceReportt"] = 'Vendor Balance Report';
DataDictionaryAR["CustomerBalanceReport"] = 'Customer Balance';
DataDictionaryAR["CustomerBalanceReportt"] = 'Customer Balance Report';

DataDictionaryAR["ItemLocation0Qty"] = 'Item has 0 quantity on this location. Cannot Decrease';
DataDictionaryAR["ItemLocationQtyLessThanAvailable"] = 'Exceeds Available quantity';

DataDictionaryAR["ItemLocation0Cost"] = 'Item has 0 Cost on this location. Cannot Decrease';
DataDictionaryAR["ItemLocationCostLessThanAvailable"] = 'Average Cost for this item location is less than this';


DataDictionaryAR["AutoGenerated"] = 'Auto Generated';
DataDictionaryAR["POHeaderDiscountPercentage"] = 'Header Discount Percentage';
DataDictionaryAR["POHeaderDiscountAmount"] = 'Header Discount Amount';
DataDictionaryAR["AreYouSureYouWantToContinueWithPosting"] = 'Are You Sure You Want to Continue With Posting ! ';

DataDictionaryAR["Adjustment"] = "تعديل";
DataDictionaryAR["Receipt"] = "فاتورة";
DataDictionaryAR["CustomField"] = "جمرك";
DataDictionaryAR["CustomFieldTo"] = "الى جمرك";

DataDictionaryAR["OriginalBank"] = "البنك الرئيسي";

DataDictionaryAR["CustomerFrom"] = "من زبون";
DataDictionaryAR["CustomerTo"] = "الى زبون";



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////Start Purchase Order/////////////////////////////////////////////////////////////////////

/////////////////////////////////////PO Invoice Arabic Dictionary JS//////////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryAR["PoInvoice"] = 'Po Invoice';
DataDictionaryAR["AddPoInvoice"] = 'اضافة Po Invoice';
DataDictionaryAR['PoInvoiceDetails'] = ' تفاصيل Po Invoice';
DataDictionaryAR['Invoice'] = 'Invoice';
DataDictionaryAR['InvoiceReference'] = 'Invoice Reference';
DataDictionaryAR['InvoiceTotalAmount'] = 'Invoice Total Amount';
DataDictionaryAR['Vendor'] = 'Vendor';
DataDictionaryAR['ReceiptIds'] = 'Receipt Ids';
DataDictionaryAR['InvoiceDate'] = 'Invoice Date';
DataDictionaryAR['RelatedPO'] = 'Related PO';
DataDictionaryAR['ReceiptItemLine'] = 'Receipt Item Line';
DataDictionaryAR['InvoiceTotalAmount'] = 'Invoice Total Amount';
DataDictionaryAR['InvoicingDate'] = 'Invoicing Date';
DataDictionaryAR['PostingDate'] = 'Posting Date';
DataDictionaryAR['Description'] = 'Description';
DataDictionaryAR['DocumentReferenceNumber'] = 'Invoice Reference Number';
DataDictionaryAR['InvoiceNumber'] = 'Invoice Number';
DataDictionaryAR['TotalAmountWithTax'] = 'Total Amount With Tax';
DataDictionaryAR['TotalAmountAfterDiscount'] = 'Total Amount After Discount';
DataDictionaryAR['TotalAmountBeforDiscount'] = 'Total Amount Befor Discount';
DataDictionaryAR['DiscountAmount'] = 'Discount Amount';
DataDictionaryAR['DiscountPercentage'] = 'Discount Percentage';
////Tab Arabic Dictionary JS
DataDictionaryAR['AddNewAdditionalCost'] = 'اضافةAdditionalCost';
///List Header Arabic Dictionary JS
DataDictionaryAR['InvoiceReference'] = 'Invoice Reference';
DataDictionaryAR['InvoiceTotalAmount'] = 'Invoice Total Amount';

//////////////////////////////////////////////////////////PO Entity //////////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryAR["PurchaseOrderHeader"] = 'أمر الشراء';
DataDictionaryAR["AddPurchaseOrderHeader"] = 'اضافة أمر شراء';
DataDictionaryAR['PurchaseOrderHeaderDetails'] = ' تفاصيل أمر الشراء';
DataDictionaryAR['ReferenceNumber'] = 'الرقم المرجعي';
DataDictionaryAR['PurchaseDate'] = 'تاريخ أمر الشراء';
DataDictionaryAR['Currency'] = 'العملة';
DataDictionaryAR['Status'] = 'الحالة';
DataDictionaryAR['AutoGenerateReceipt'] = 'إنشاء استيلام تلقائياً';
DataDictionaryAR['AutoGenerateInvoice'] = 'إنشاء فاتورة تلقائياً';
DataDictionaryAR['Vendor'] = 'البائع';
DataDictionaryAR['TaxClass'] = 'فئة الضريبة';
DataDictionaryAR['ExchangeRate'] = 'سعر الصرف';
DataDictionaryAR['Description'] = 'وصف';
DataDictionaryAR['HeaderDiscountAmount'] = 'قيمة الخصم ';
DataDictionaryAR['HeaderDiscountPercentage'] = 'نسبة الخصم';
DataDictionaryAR['NumberOfLines'] = 'عدد محتويات أمر الشراء';
DataDictionaryAR['TotalCost'] = 'التكلفة الاجمالية';
DataDictionaryAR['TotalQuantity'] = 'العدد الكلي';
DataDictionaryAR['TotalAmountWithTax'] = 'المجموع الكلي لأمر الشراء';
DataDictionaryAR['TotalAmountAfterDiscount'] = 'المجموع الكلي لأمر الشراء بعد الخصومات';
DataDictionaryAR['TotalAmountBeforeDiscount'] = 'المجموع الكلي لأمر الشراء قبل الخصومات';
DataDictionaryAR['IsPosted'] = 'ترحيل';
DataDictionaryAR['PostingDate'] = 'تاريخ الترحيل';
DataDictionaryAR['ItemCard'] = 'كرت المادة';
DataDictionaryAR['ItemDescription'] = 'وصف المادة';
DataDictionaryAR['Location'] = 'الموقع';
DataDictionaryAR['Quantity'] = 'الكمية';
DataDictionaryAR['UnitOfMeasure'] = 'وحدة القياس';
DataDictionaryAR['UnitCost'] = 'تكلفة الوحدة';
DataDictionaryAR['TaxClass'] = 'فئة الضريبة';
DataDictionaryAR['UnitDiscountAmount'] = 'قيمة الخصم';
DataDictionaryAR['UnitDiscountPercentage'] = 'نسبة الخصم';
DataDictionaryAR['ReceivedQuantity'] = 'الكمية المستلمه';
DataDictionaryAR['OutstandingQuantity'] = 'الكمية المتبقية';
DataDictionaryAR['UnitTotalAmountBeforeDiscount'] = 'المجموع الكلي لأمر الشراء قبل الخصومات';
DataDictionaryAR['UnitTotalAmountAfterDiscount'] = 'المجموع الكلي لأمر الشراء بعد الخصومات';
DataDictionaryAR['UnitTotalAmountWithTax'] = 'المجموع الكلي لأمر الشراء';
DataDictionaryAR['Comments'] = 'الملاحظات';
DataDictionaryAR['IsLastOne'] = 'Is Last One';
DataDictionaryAR['POStatusNew'] = 'جديد';
DataDictionaryAR['POStatusInProgress'] = 'قيد التنفيذ';
DataDictionaryAR['POStatusCompleted'] = 'مكتمل';
DataDictionaryAR['HeaderReferenceNumber'] = "الرقم المرجعي :";
DataDictionaryAR['HeaderPODate'] = "تاريخ أمر الشراء :";
DataDictionaryAR['HeaderStatus'] = "الحالة :";

/////////////////////////////////////Arabic Dictionary JS//////////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryAR["PoReceipt"] = 'PoReceipt';
DataDictionaryAR["AddPoReceipt"] = 'اضافة PoReceipt';
DataDictionaryAR['PoReceiptDetails'] = ' تفاصيل PoReceipt';
DataDictionaryAR['SummaryPoReceiptDetails'] = 'Po Summary Receipt Details';
DataDictionaryAR['Receipt'] = 'Receipt';
DataDictionaryAR['Reference'] = 'Reference';
DataDictionaryAR['ReceiptDate'] = 'Receipt Date';
DataDictionaryAR['PurchaseOrder'] = 'Purchase Order Reference';
DataDictionaryAR['CreateAutomaticInvoice'] = 'Create Automatic Invoice';
DataDictionaryAR['AutomaticAdditionalCostProration'] = 'Automatic Additional Cost Proration';
DataDictionaryAR['Vendor'] = 'Vendor';
DataDictionaryAR['Description'] = 'Description';
DataDictionaryAR['HeaderDiscountAmount'] = 'Discount Amount';
DataDictionaryAR['HeaderDiscountPercentage'] = 'Discount Percentage';
DataDictionaryAR['NumberOfLines'] = 'Number Of Lines';
DataDictionaryAR['TotalCost'] = 'Total Cost';
DataDictionaryAR['TotalQuantity'] = 'Total Quantity';
DataDictionaryAR['TotalAmountWithTax'] = 'Total Amount With Tax';
DataDictionaryAR['TotalAmountAfterDiscount'] = 'Total Amount After Discount';
DataDictionaryAR['TotalAmountBeforeDiscount'] = 'Total Amount Before Discount';
///List Header Arabic Dictionary JS
DataDictionaryAR['HReference'] = 'Reference : ';
DataDictionaryAR['HTotalCost'] = 'Total Cost : ';
////Tab English Dictionary JS
DataDictionaryAR['LoadReceipts'] = 'Load Receipts';
DataDictionaryAR['PurchaseOrderQuantity'] = 'PO Qty';
DataDictionaryAR['PleasLoadReceipts'] = 'Please Load Receipts';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryAR['InvoiceDocumentNumber'] = 'رقم الفاتوره';
DataDictionaryAR['PostingPO'] = 'Posting Purchase Order';
DataDictionaryAR['AreYouSureToPostPO'] = 'Are you sure you want to post purchase order?';

DataDictionaryAR['PostingPO'] = 'Save and posting Purchase Order';
DataDictionaryAR['AreYouSureToPostPO'] = 'Are you sure you want to save and post purchase order?';
DataDictionaryAR['LockedPeriod'] = 'Posting date can not be in locked period';
DataDictionaryAR['EditQuantity'] = 'Edit Quantity';
DataDictionaryAR['OriginalQuantity'] = 'Original Quantity';
DataDictionaryAR['OutstandingQuantity'] = 'Outstanding Qty';
DataDictionaryAR['POInquery'] = 'Purchase Order Status';


///////////////////////////////////////////// PO Receipt Return //////////////////////////////////////

DataDictionaryAR["PoReceiptReturn"] = 'PO Receipt Return';
DataDictionaryAR["AddPoReceiptReturn"] = 'Add PO Receipt Return';
DataDictionaryAR['PoReceiptReturnDetails'] = 'PO Receipt Return Details';
DataDictionaryAR['PoReceiptReturn'] = 'PO Receipt Return';
DataDictionaryAR['PoReceipt'] = 'PO Receipt';
DataDictionaryAR['ReturnReference'] = 'Return Reference';
DataDictionaryAR['ReturnDate'] = 'Return Date';
DataDictionaryAR['ReturnDescription'] = 'Return Description';
DataDictionaryAR['ReturnAllQuantity'] = 'Return All Quantity';
DataDictionaryAR['Vendor'] = 'Vendor';
DataDictionaryAR['TaxClass'] = 'Tax Class';
DataDictionaryAR['Currency'] = 'Currency';
DataDictionaryAR['ExchangeRate'] = 'Exchange Rate';
DataDictionaryAR['ReceiptTotalAmount'] = 'Receipt Total Amount';
DataDictionaryAR['ReturnedCost'] = 'Returned Cost';
DataDictionaryAR['ReturnedQuantity'] = 'Returned Quantity';
DataDictionaryAR['RelatedInvoice'] = 'Related Invoice';
DataDictionaryAR['RelatedInvoicesIds'] = 'Related Invoices Ids';
DataDictionaryAR['PoReceiptAdditionalCost'] = 'PO Receipt Additional Cost';
DataDictionaryAR['AdditionalCostAction'] = 'Additional Cost Action';
DataDictionaryAR['IsPosted'] = 'Is Posted';
DataDictionaryAR['PostingDate'] = 'Posting Date';


//////////////////////////////////////////////InqueryPage //////////////////////////////////////////
DataDictionaryAR['PurchaseOrderStatus'] = 'Purchase Order Status';
DataDictionaryAR['PurchaseOrderReference'] = 'Purchase Order Reference';
DataDictionaryAR['ReceiptReference'] = 'Receipt Reference';
DataDictionaryAR['InvoiceReference'] = 'Invoice Reference';
DataDictionaryAR['Results'] = 'Results';
DataDictionaryAR['Transaction'] = 'Transaction';
DataDictionaryAR['TransactionDate'] = 'Transaction Date';
DataDictionaryAR['TypeCreatedDate'] = 'Type Create Date';
DataDictionaryAR['TypeTransactionDate'] = 'Type Transaction Date';
DataDictionaryAR['TypeTotalAmount'] = 'Type Total Amount';
DataDictionaryAR['ExceedsUnitTotal'] = 'Exceeds Ext. cost';
DataDictionaryAR['ExceedsPOTotalAmount'] = 'Exceeds PO Total Amount';
DataDictionaryAR['ExceedsReciptTotalAmount'] = 'Exceeds Total Amount';
//////////////////////////////////////// End Purchase Order ///////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


DataDictionaryAR["ParentInvoiceNotPostedYet"] = 'Parent invoice of one or more of the refund invoices is/are not posted yet, please post them then post refund again.';

DataDictionaryAR["ShipmentDiscountExceedsTotalAmount"] = 'Shipment Discount Exceeds Total Amount';



DataDictionaryAR["DetailsCurrenciesAreUsedBySystem"] = 'Failed to delete; Details Currencies Are Used By ERP system data';

DataDictionaryAR["IcReceiptDiscountExceedsReceiptAmount"] = 'Receipt Discount Exceeds it\'s Amount, resulting in a negative total cost!';

DataDictionaryAR["CurrencyAlreadyExists"] = 'Currency Cannot be duplicated for the same header currency';

DataDictionaryAR["QuantityInLocationsIsNotEnoughtoFullyReturnReceipt"] = 'Quantities In Locations are Not Enough to Fully Return Receipt, Negative Quantities are not allowed !';


/////////////////////////////////////Arabic Dictionary JS//////////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryAR["PoReturnReceipt"] = 'PoReturnReceipt';
DataDictionaryAR["AddPoReturnReceipt"] = 'اضافة PoReturnReceipt';
DataDictionaryAR['PoReturnReceiptDetails'] = ' تفاصيل PoReturnReceipt';
DataDictionaryAR['PoReturnReceipt'] = 'Po Return Receipt';
DataDictionaryAR['ReturnReferenceNumber'] = 'Return Reference Number';
DataDictionaryAR['Receipt'] = 'Receipt';
DataDictionaryAR['ReturnDescription'] = 'Return Description';
DataDictionaryAR['ReturnDate'] = 'Return Date';
DataDictionaryAR['ReturnAllQuantity'] = 'Return All Quantity';
DataDictionaryAR['Vendor'] = 'Vendor';
DataDictionaryAR['TaxClass'] = 'Tax Class';
DataDictionaryAR['Currency'] = 'Currency';
DataDictionaryAR['ExchangeRate'] = 'Exchange Rate';
DataDictionaryAR['ReceiptTotalQuantity'] = 'Receipt Total Quantity';
DataDictionaryAR['ReturnTotalQuantity'] = 'Return Total Quantity';
DataDictionaryAR['ReceiptTotalAmountWithTax'] = 'Receipt Total Amount With Tax';
DataDictionaryAR['ReceiptTotalAmountAfterDiscount'] = 'Receipt Total Amount After Discount';
DataDictionaryAR['ReceiptTotalAmountBeforeDiscount'] = 'Receipt Total Amount Before Discount';
DataDictionaryAR['ReturnTotalAmountWithTax'] = 'Return Total Amount With Tax';
DataDictionaryAR['ReturnTotalAmountAfterDiscount'] = 'Return Total Amount After Discount';
DataDictionaryAR['ReturnTotalAmountBeforeDiscount'] = 'Return Total Amount Before Discount';
DataDictionaryAR['RelatedInvoice'] = 'Related Invoice';
DataDictionaryAR['InvoiceReference'] = 'Invoice Reference';
DataDictionaryAR['InvoiceAmount'] = 'Invoice Amount';
DataDictionaryAR['NumberOfLines'] = 'Number Of Lines';

DataDictionaryAR["AlreadyExist"] = ' موجود مسبقا ';
////Tab Arabic Dictionary JS

///List Header Arabic Dictionary JS
DataDictionaryAR['ReferenceNumber'] = 'Reference Number';
DataDictionaryAR['ReturnDate'] = 'Return Date';
DataDictionaryAR['ReturnTotalAmountWithTax'] = 'Return Total Amount With Tax';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryAR['CompletedWithError'] = 'Completed With Error';
DataDictionaryAR['ExportTemplate'] = 'Export Template';
DataDictionaryAR['AdditionalCostClearingAccount'] = 'Additional Cost Clearing Account';
DataDictionaryAR['NonProratedAdditionalCostTotal'] = 'Non Prorated Additional Cost Total';
DataDictionaryAR['LineTotalCost'] = 'Line Total Cost';
DataDictionaryAR['TotalCostInfo'] = 'this total includes the Receipt total after the discount including the prorated additional Cost and the Tax.';


/////////////////////////////////////Arabic Dictionary JS//////////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryAR["OeOrderEntry"] = 'OeOrderEntry';
DataDictionaryAR["AddOeOrderEntry"] = 'اضافة OeOrderEntry';
DataDictionaryAR['OeOrderEntryDetails'] = ' تفاصيل OeOrderEntry';
DataDictionaryAR['OeOrderEntry'] = 'Oe Order Entry';
DataDictionaryAR['OeReference'] = 'Oe Reference';
DataDictionaryAR['OeDate'] = 'Oe Date';
DataDictionaryAR['PostingDate'] = 'Posting Date';
DataDictionaryAR['DeliveryDate'] = 'Delivery Date';
DataDictionaryAR['OeStatus'] = 'Oe Status';
DataDictionaryAR['Customer'] = 'Customer';
DataDictionaryAR['TaxClass'] = 'Tax Class';
DataDictionaryAR['Currency'] = 'Currency';
DataDictionaryAR['ExchangeRate'] = 'Exchange Rate';
DataDictionaryAR['Description'] = 'Description';
DataDictionaryAR['DiscountAmount'] = 'Discount Amount';
DataDictionaryAR['DiscountPercentage'] = 'Discount Percentage';
DataDictionaryAR['TotalAmountBeforeDiscount'] = 'Total Amount Before Discount';
DataDictionaryAR['TotalAmountAfterDiscount'] = 'Total Amount After Discount';
DataDictionaryAR['TotalAmountWithTax'] = 'Total Amount With Tax';
DataDictionaryAR['TotalQuantity'] = 'Total Quantity';
DataDictionaryAR['TotalOrderProfitability'] = 'Total Order Profitability';

DataDictionaryAR["AlreadyExist"] = ' موجود مسبقا ';
////Tab Arabic Dictionary JS

///List Header Arabic Dictionary JS

///////////////////////////////////////////////////////////////////////////////////////

DataDictionaryAR["APBatchList"] = "AP Payment Batch List";

