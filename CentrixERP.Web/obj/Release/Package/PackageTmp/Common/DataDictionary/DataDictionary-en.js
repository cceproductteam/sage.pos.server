var DataDictionaryEN = new Array();

////////////////////////////////////////Linkes//////////////////////////////////////////////////////////////////////////
DataDictionaryEN['IsDefult'] = 'Is Default';
DataDictionaryEN['Numbering'] = 'Numbering';
DataDictionaryEN["LinkLang"] = 'AR';
DataDictionaryEN["Edit"] = 'Edit';
DataDictionaryEN["Save"] = 'Save';
DataDictionaryEN["Cancel"] = 'Cancel';
DataDictionaryEN["Close"] = 'Close';
DataDictionaryEN["Delete"] = 'Delete';
DataDictionaryEN["Clear"] = 'Clear';
DataDictionaryEN["Post"] = 'Post';
DataDictionaryEN["Reassign"] = 'Reassign';
DataDictionaryEN["Add"] = 'Add';
DataDictionaryEN["FaildToSave"] = 'failed to save';
DataDictionaryEN["SMNoDataFound"] = 'no data found';
DataDictionaryEN["RoleNotFound"] = 'role not found';
DataDictionaryEN["savedsuccessfully"] = 'saved successfully';
DataDictionaryEN["deletesuccessfully"] = 'delete successfully';
DataDictionaryEN["Areyousure"] = 'Are you sure ?';
DataDictionaryEN["ConfirmMsgCancle"] = 'Cancel';
DataDictionaryEN["ConfirmMsgConfirm"] = 'Confirm';
DataDictionaryEN["ThisRecordIsLockedby"] = 'This record is locked by ';
DataDictionaryEN["at"] = ', at ';
DataDictionaryEN["systemError"] = 'system error';
DataDictionaryEN["RecordDeleted"] = 'Record Deleted';
DataDictionaryEN["RecordHasBeenDeleted"] = 'This record has been deleted, the window will be refreshed after you click ok.';
DataDictionaryEN["RecordChanged"] = 'Record Changed';
DataDictionaryEN["RecordHasBeenChanged"] = 'This record has been changed, the window will be refreshed after you click ok.';
DataDictionaryEN["DeleteSuperUserMsg"] = 'Supper user can not be deleted.';
DataDictionaryEN["required"] = 'required';
DataDictionaryEN["numeric"] = 'numeric';
DataDictionaryEN["integer"] = 'integer';
DataDictionaryEN["lengthMustBe"] = 'length must be';
DataDictionaryEN["invalidEmail"] = 'invalid email';
DataDictionaryEN["HistoryUpdated"] = 'History Updated';
DataDictionaryEN["LastUpdatedDate"] = 'Last Updated Date';
DataDictionaryEN["LastUpdatedBy"] = 'Last Updated By';
DataDictionaryEN["True"] = 'True';
DataDictionaryEN["False"] = 'False';

DataDictionaryEN["AccpacSyncLog"] = 'Accpac Sync Log';
DataDictionaryEN["Posted"] = 'Posted';
DataDictionaryEN["NotPosted"] = 'Not Posted';
DataDictionaryEN["SyncLogDateFrom"] = 'Date From';
DataDictionaryEN["SyncStatus"] = 'Sync Status';
DataDictionaryEN["SyncLogDateTo"] = 'Date To';
DataDictionaryEN["AccpacSyncUniqueNumber"] = 'Accpac Sync Unique Number';
DataDictionaryEN["MinLength"] = 'Minimum length is 3';
DataDictionaryEN["MaxLength"] = 'Maximum length is 100';
DataDictionaryEN["Ok"] = 'Ok';
DataDictionaryEN['MoreOptions'] = 'More options ▼';
DataDictionaryEN['HideOptions'] = 'Hide options ▲';
DataDictionaryEN['Search...'] = 'Search...';
DataDictionaryEN["Language"] = 'العربية';
DataDictionaryEN["vldDeleteActiveFailed"] = 'Failed to delete active item';
DataDictionaryEN["vldDontHavePermission"] = 'You Dont Have Permissions for this page';
DataDictionaryEN["vldLoginSessionExpired"] = 'Your login session has been expired';
DataDictionaryEN["vldDeleteLinkedItem"] = 'Failed to delete item ,linked to other entities : <br>';
DataDictionaryEN["vldDeactivateLinkedItem"] = 'Failed to deactivate item ,linked to other entities : <br>';
DataDictionaryEN["OptionalFieldEdit"] = "Select";
DataDictionaryEN["vldChangeHeaderTaxClass"] = "You have changed a tax class for this document.<br> Please verify the tax classes on each detail line and make any necessary changes.";
DataDictionaryEN["Failed"] = "Failed";
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Setup Page section//////////////////////////////////////////////////////////////
DataDictionaryEN["SPSetup"] = 'Setup';
DataDictionaryEN["SPUserManagement"] = 'User Management';
DataDictionaryEN["SPUsers"] = 'Users';
DataDictionaryEN["SPRols"] = 'Roles';
DataDictionaryEN["SPTeams"] = 'Teams';
DataDictionaryEN["SPPermissions"] = 'Permissions';
DataDictionaryEN["SPDataAdminstration"] = 'Data Adminstration';
DataDictionaryEN["SPLovs"] = 'Lovs';
DataDictionaryEN["SPEntitylock"] = 'Entity lock';
DataDictionaryEN["SPInventory"] = 'Inventory';
DataDictionaryEN["SPSearchInventory"] = 'Search Inventory';
DataDictionaryEN["SPStockUpdate"] = 'Stock Update';
DataDictionaryEN["SPInventory"] = 'Inventory';
DataDictionaryEN["SReturnedItems"] = 'Returned Items-VO\'s';

DataDictionaryEN["SPEmailConfiguration"] = 'Email Configuration';
DataDictionaryEN["SPSMSConfiguration"] = 'SMS Configuration';
DataDictionaryEN["SPTemplates"] = 'Templates';
DataDictionaryEN["SPSendMassEmails"] = 'Send Mass Emails';
DataDictionaryEN["SPGroups"] = 'Groups';
DataDictionaryEN["Groups&Marketing"] = 'Groups & Marketing';
DataDictionaryEN["SPKit"] = 'Kit';
DataDictionaryEN["SPCategory"] = 'Category';
DataDictionaryEN['FullName'] = 'Full Name';
DataDictionaryEN['Users'] = 'Users';
DataDictionaryEN['ShowAllUser'] = 'Show All User';
DataDictionaryEN["ContactPerson"] = 'Contact Person';
DataDictionaryEN["ContactPersonMob"] = 'Contact Person Mobile';
DataDictionaryEN["AssetRegister"] = 'Asset Register';
DataDictionaryEN["pending"] = "Pending";
DataDictionaryEN["Inprogress"] = "Inprogress";
DataDictionaryEN["failed"] = "Failed";
DataDictionaryEN["success"] = "Success";
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Page Titels///////////////////////////////////////////////////////////////////////
DataDictionaryEN["QuickSearch"] = 'Quick Search';
DataDictionaryEN["CompanyPageTitle"] = 'Company';
DataDictionaryEN["AddCompany"] = 'Add Company';
DataDictionaryEN["Total"] = 'Total  ';
DataDictionaryEN["of"] = '  of  ';
DataDictionaryEN["Showmore"] = 'Show more';
DataDictionaryEN["loading"] = 'loading...';
DataDictionaryEN["SlideDown"] = 'Slide Down';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Tabs Add Link///////////////////////////////////////////////////////////////////
DataDictionaryEN["NoDataFound"] = 'No Data Found';
DataDictionaryEN["AddnewCommunication"] = 'Add new Communication';
DataDictionaryEN["AddSales"] = 'Add Sales';
DataDictionaryEN["Addnewaddress"] = 'Add new address';
DataDictionaryEN["newphone"] = 'new phone';
DataDictionaryEN["Addnewnote"] = 'Add new note';
DataDictionaryEN["Addnewemail"] = 'Add new email';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Tabs Names//////////////////////////////////////////////////////////////////////
DataDictionaryEN["Summary"] = 'Summary';
DataDictionaryEN["Persons"] = 'Persons';
DataDictionaryEN["Communications"] = 'Comm - unications';
DataDictionaryEN["Sales"] = 'Sales';
DataDictionaryEN["TradingTerms"] = 'Trading Terms';
DataDictionaryEN["Addresses"] = 'Addresses';
DataDictionaryEN["Phones"] = 'Phones';
DataDictionaryEN["Emails"] = 'Emails';
DataDictionaryEN["Attachments"] = 'Attachments';
DataDictionaryEN["Notes"] = 'Notes';
DataDictionaryEN["More"] = 'More';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Email Tab///////////////////////////////////////////////////////////////////////
DataDictionaryEN["EmailAddress"] = 'Email Address';
DataDictionaryEN["Type"] = 'Type';
DataDictionaryEN["Business"] = 'Business';
DataDictionaryEN["Personal"] = 'Personal';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Adress Tab///////////////////////////////////////////////////////////////////////
DataDictionaryEN["Country"] = 'Country';
DataDictionaryEN["City"] = 'City';
DataDictionaryEN["StreetAddress"] = 'Street Address';
DataDictionaryEN["NearBy"] = 'Near By';
DataDictionaryEN["Place"] = 'Place';
DataDictionaryEN["BuildingNumber"] = 'Building Number';
DataDictionaryEN["POBox"] = 'P.O.Box';
DataDictionaryEN["ZipCode"] = 'Zip Code/Postcode';
DataDictionaryEN["Region"] = 'Region';
DataDictionaryEN["Area"] = 'Area';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Attachment Tab///////////////////////////////////////////////////////////////////////
DataDictionaryEN["filesfromyourcomputer"] = 'files from your computer';
DataDictionaryEN["Dropfileshere"] = 'Drop files here';
DataDictionaryEN["Addfolder"] = 'Add folder';
DataDictionaryEN["DocumentRename"] = 'Rename';
DataDictionaryEN["DocumentDelete"] = 'Delete';
DataDictionaryEN["DeleteFolder"] = 'Delete Folder';
DataDictionaryEN["DeleteDocument"] = 'Delete Document';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Phone Tab///////////////////////////////////////////////////////////////////////
DataDictionaryEN["Number"] = 'Number';
DataDictionaryEN["Mobile"] = 'Mobile';
DataDictionaryEN["LandLine"] = 'Land Line';
DataDictionaryEN["Fax"] = 'Fax';
DataDictionaryEN["PhCountry"] = 'Country';
DataDictionaryEN["PhArea"] = 'Area';
DataDictionaryEN["PhPhone"] = 'Phone';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Note Tab///////////////////////////////////////////////////////////////////////
DataDictionaryEN["CreatedDate"] = 'Created Date';
DataDictionaryEN["CreatedBy"] = 'Created By';
////////////////////////////////////////////////Common tabs/////////////////////////////////////////////////////////////
DataDictionaryEN['email'] = 'Email';
DataDictionaryEN['AddNewemail'] = 'Add New email';
DataDictionaryEN['note'] = 'Note';
DataDictionaryEN['AddNewnote'] = 'Add New note';
DataDictionaryEN['address'] = 'Address';
DataDictionaryEN['AddNewaddress'] = 'Add New address';
DataDictionaryEN['phone'] = 'phone';
DataDictionaryEN['AddNewphone'] = 'Add New phone';
DataDictionaryEN['Attach'] = 'Attachment';
DataDictionaryEN['AddNewAttach'] = 'Add New Attachment';
DataDictionaryEN['Communication'] = 'Communication';
DataDictionaryEN['AddNewCommunication'] = 'Add New Communication';

DataDictionaryEN['Persons'] = 'Persons';
DataDictionaryEN['AddNewPersons'] = 'Add New Persons';
DataDictionaryEN['ProjectsOpporunities'] = 'Projects Opportunities';
DataDictionaryEN['AddNewProjectsOpporunities'] = 'Add New Opporunities';
DataDictionaryEN['Cases'] = 'Cases';
DataDictionaryEN['AddNewCase'] = 'Add New Case';
DataDictionaryEN['Appliances'] = 'Appliances';
DataDictionaryEN['AddNewAppliances'] = 'Add New Appliances';

DataDictionaryEN["PersonalEmail"] = 'Personal Email';
DataDictionaryEN["BusinessEmail"] = 'Business Email';
DataDictionaryEN["Email"] = 'Email';
DataDictionaryEN["Address"] = 'Address';
DataDictionaryEN["Country"] = 'Country';
DataDictionaryEN["City"] = 'City';
DataDictionaryEN["StreetAddress"] = 'Street Address';
DataDictionaryEN["Phone"] = 'Phone';
DataDictionaryEN["Home"] = 'Home';
DataDictionaryEN["Fax"] = 'Fax';
DataDictionaryEN["Mobile"] = 'Mobile';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Inventory and Search Inventory Section//////////////////////////////////////////
////////////////////////////////////////Inventory View list and View Item///////////////////////////////////////////////
DataDictionaryEN["InventorySearch"] = 'Inventory Search';
DataDictionaryEN["Search"] = 'Search';
DataDictionaryEN["AddInventory"] = 'Add Item';
DataDictionaryEN["Inventory"] = 'Inventory';
DataDictionaryEN["HeaderItemGroup"] = 'Item Group : ';
DataDictionaryEN["HeaderItemDescription"] = 'Item Description : ';
DataDictionaryEN["InventoryDetails"] = 'Inventory Details';
DataDictionaryEN["ItemGroup"] = 'Item Group';
DataDictionaryEN["ItemDescription"] = 'Item Description';
DataDictionaryEN["DimensionsLength"] = 'Dimensions Length';
DataDictionaryEN["DimensionsWidth"] = 'Dimensions Width';
DataDictionaryEN["ListPrice"] = 'List Price';
DataDictionaryEN["PriceWithoutSalesTax"] = 'Price Without Sales Tax';
DataDictionaryEN["AvailableQuantityShowroom"] = 'Available Quantity Showroom';
DataDictionaryEN["AvailableQuantityWarehouse"] = 'Available Quantity Warehouse';
DataDictionaryEN["TotalQuantity"] = 'Total Available Quantity ';
DataDictionaryEN["TotalOnSalesOrders"] = 'Quantity on Sales Orders';
DataDictionaryEN["SalesCommission"] = 'Sales Commission (JD)';
DataDictionaryEN["MaximumDiscount"] = 'Maximum Discount ';
DataDictionaryEN["SpecialOffer"] = 'Special Offer';
DataDictionaryEN["Yes"] = 'Yes';
DataDictionaryEN["No"] = 'No';

DataDictionaryEN["ItemType"] = 'Item Type';
DataDictionaryEN["ItemNumber"] = 'Item Number';

DataDictionaryEN["UploadItemsLog"] = 'UploadItemsLog';
DataDictionaryEN["AddUploadItemsLog"] = 'Add UploadItemsLog';
DataDictionaryEN['UploadItemsLogDetails'] = 'UploadItemsLog Details';
DataDictionaryEN['UploadItemsLog'] = 'Upload Items Log';
DataDictionaryEN['Type'] = 'Type';
DataDictionaryEN['Note'] = 'Note';
DataDictionaryEN['Status'] = 'Status';
DataDictionaryEN['Errors'] = 'Errors';
DataDictionaryEN['LogFileName'] = 'Log File Name';
DataDictionaryEN["AlreadyExist"] = ' Already Exist ';
DataDictionaryEN["File"] = "File";
DataDictionaryEN["UploadItems"] = "Upload Items";

///////////////////////////////////////// Asset English Dictionary////////////////////////////////////////////////
DataDictionaryEN["Asset"] = 'Asset';
DataDictionaryEN["AddAsset"] = 'Add Asset';
DataDictionaryEN['AssetDetails'] = 'Asset Details';
DataDictionaryEN['Asset'] = 'Asset';
DataDictionaryEN['#'] = '#';
DataDictionaryEN['Description'] = 'Description';
DataDictionaryEN['Categroy'] = 'Categroy';
DataDictionaryEN['Location'] = 'Location';
DataDictionaryEN['AcquisitionDate'] = 'Acquisition Date';
DataDictionaryEN['OriginalValue'] = 'Original Value';
DataDictionaryEN['AddOn'] = 'Add On';
DataDictionaryEN['Total'] = 'Total ';
DataDictionaryEN['DepreciationRate'] = 'Depreciation Rate';
DataDictionaryEN['MonthlyDepreciation'] = 'Monthly Depreciation';
DataDictionaryEN['AccumulatedDepreciation'] = 'Accumulated Depreciation';
DataDictionaryEN['NetValue'] = 'Net Value';
DataDictionaryEN['Source'] = 'Source';
DataDictionaryEN["AlreadyExist"] = ' Already Exist ';

DataDictionaryEN["LineNumber"] = 'Line Number';


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Opportunity Section///////////////////////////////////////////////////////////////////
////////////////////////////////////////Opportunity View list and View Item///////////////////////////////////////////////////
DataDictionaryEN["Opportunity"] = 'Opportunity';
DataDictionaryEN["AddOpportunity"] = 'Add Opportunity';
DataDictionaryEN['OpportunityDetails'] = 'Opportunity Details';
DataDictionaryEN['Opportunity'] = 'Opportunity';
DataDictionaryEN['Company'] = 'Company';
DataDictionaryEN['Contact'] = 'Contact';

DataDictionaryEN['Person'] = 'Person';
DataDictionaryEN['ApprovedContractorPerson'] = 'Approved Contractor Person';
DataDictionaryEN['ApprovedContractorCompany'] = 'Approved Contractor Company';
DataDictionaryEN['ProjectName'] = 'Project Name';
DataDictionaryEN['ProjectScope'] = 'Project Scope';
DataDictionaryEN['ProjectDetails'] = 'Project Details';
DataDictionaryEN['OpenedDate'] = 'Opened Date';
DataDictionaryEN['ClosedDate'] = 'Closed Date';
DataDictionaryEN['Certainty'] = 'Certainty %';
DataDictionaryEN['Details'] = 'Details';
DataDictionaryEN['MainProductInterestValues'] = 'Main Product Interest Values';
DataDictionaryEN['Source'] = 'Source';
DataDictionaryEN['DecisionTimeframe'] = 'Decision Timeframe';
DataDictionaryEN['Priority'] = 'Priority';
DataDictionaryEN['MainCompetitor'] = 'Main Competitor';
DataDictionaryEN['EstimatedClosedDate'] = 'Estimated Closed Date';
DataDictionaryEN['EstimatedEndDate'] = 'Estimated End Date';
DataDictionaryEN['EstimatedStartDate'] = 'Estimated Start Date';
DataDictionaryEN['Quotes'] = 'Quotes';
DataDictionaryEN['AddNewQuotes'] = 'Add New Quotes';
DataDictionaryEN['Orders'] = 'Orders';
DataDictionaryEN['AddNewOrders'] = 'Add New Orders';
DataDictionaryEN['Reports'] = 'Reports';
DataDictionaryEN['AddNewReports'] = 'Add New Reports';
DataDictionaryEN['Resources'] = 'Resources';
DataDictionaryEN['AddNewResources'] = 'Add New Resources';
DataDictionaryEN["CompanyOrPersonIsRequired"] = 'company or person is required';
DataDictionaryEN['ConsultantCompany'] = 'Consultant Company';
DataDictionaryEN['ConsultantPerson'] = 'Consultant Person';
DataDictionaryEN['ExpiredFixedAssetsReport'] = 'Expired Fixed Assets';
DataDictionaryEN['HeaderStatus'] = 'Status: ';
DataDictionaryEN['HeaderStage'] = 'Stage: ';
DataDictionaryEN['HeaderProjectName'] = 'Project Name: ';
DataDictionaryEN['PercentageValidation'] = 'Maximum value is 100';
DataDictionaryEN['AccountTransactions'] = 'Account Transactions';
DataDictionaryEN["CashFlowStatements"] = "Cash Flow Statements";
DataDictionaryEN["FromPeriod"] = "From Period";
DataDictionaryEN["ToPeriod"] = "To Period";
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////Opportunity Progress Links/////////////////////////////////////////////////////
DataDictionaryEN["SaleApproved"] = 'Sale Approved';
DataDictionaryEN["SaleLost"] = 'Sale Lost';
DataDictionaryEN["ReserveItems"] = 'Reserve Items';
DataDictionaryEN["Quoted"] = 'Quoted';
DataDictionaryEN["Delivered"] = 'Delivered';
DataDictionaryEN["Design"] = 'Design';
DataDictionaryEN["Completed"] = 'Completed';
DataDictionaryEN["TechnicalSubmitted"] = 'Technical Submitted';
DataDictionaryEN["Ordered"] = 'Ordered';

DataDictionaryEN["SaleWorkflow"] = 'Sale Work flow';
DataDictionaryEN["AddSalesOrderToContinue"] = 'Add Sales Order to continue';
DataDictionaryEN["OpportunityProgress"] = 'Opportunity Progress';
DataDictionaryEN["Stage"] = 'Stage';
DataDictionaryEN["Status"] = 'Status';
DataDictionaryEN["lblStageDefault"] = 'Lead';
DataDictionaryEN["lblStatusDefault"] = 'In Progress';
DataDictionaryEN["DeliveryDate"] = 'Delivery Date';
DataDictionaryEN["LostReason"] = 'Lost Reason';
DataDictionaryEN["TrackingNote"] = 'Tracking Note';

DataDictionaryEN['AssignedTo'] = 'Assigned To';
DataDictionaryEN['ContractDate'] = 'Contract Date';
DataDictionaryEN['ActualStartDate'] = 'Actual Start Date';
DataDictionaryEN['ActualEndDate'] = 'Actual End Date';
DataDictionaryEN['CompletionPercentage'] = 'Completion Percentage %';
DataDictionaryEN['DeliveryComments'] = 'Delivery Comments';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////Opportunity Tabs/////////////////////////////////////////////////////
DataDictionaryEN["Progress"] = 'Progress';
DataDictionaryEN["Orders"] = 'Orders';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Quotation Section///////////////////////////////////////////////////////////////////
////////////////////////////////////////Quotation List /////////////////////////////////////////////////////////////////////
DataDictionaryEN["Quotation"] = 'Quotation';
DataDictionaryEN["AddQuotation"] = 'Add Quotation';
DataDictionaryEN['QuotationDetails'] = 'Quotation Details';
DataDictionaryEN['Opportunity'] = 'Opportunity';
DataDictionaryEN['ReferenceNumber'] = 'Reference Number';
DataDictionaryEN['Currency'] = 'Currency';
DataDictionaryEN['PriceList'] = 'Price List';
DataDictionaryEN['ContractorPerson'] = 'Contractor Person';
DataDictionaryEN['ContractorCompany'] = 'Contractor Company';
DataDictionaryEN['TermsAndConditions'] = 'Terms And Conditions';
DataDictionaryEN['Attnto'] = 'Attn To';
DataDictionaryEN['EstimatedDeliveryDate'] = 'Estimated Delivery Date';
DataDictionaryEN['DurationOfScheduledVisitsInMonths'] = 'Duration Of Scheduled Visits In Months';
DataDictionaryEN['NumberOfScheduledVisits'] = 'Number Of Scheduled Visits';
DataDictionaryEN['TotalQuotePrice'] = 'Total Quote Price';
DataDictionaryEN['NetPrice'] = 'Net Price';
DataDictionaryEN['DiscountAmount'] = 'Discount Amount';
DataDictionaryEN['DiscountPercentage'] = 'Discount %';
DataDictionaryEN['AddNewOrders'] = 'Add New Orders';
DataDictionaryEN['HReferenceNo'] = 'Reference Number :';
DataDictionaryEN['HOpportunity'] = 'Opportunity :';
DataDictionaryEN['QuotationItems'] = 'Quotation Items';
DataDictionaryEN['Generated'] = 'Generated';
DataDictionaryEN['Generate'] = 'Generate ';
DataDictionaryEN['ActionConvertToOrder'] = 'Convert To Order';
DataDictionaryEN['Item'] = 'Item';
DataDictionaryEN['ReferenceNumber'] = 'Reference number ';
DataDictionaryEN['Location'] = 'Location';
DataDictionaryEN['Quantity'] = 'Quantity';
DataDictionaryEN['UnitPrice'] = 'Unit Price';
DataDictionaryEN['PercentageDiscount'] = 'Percentage discount';
DataDictionaryEN['ItemTotalprice'] = 'Item Total price';
DataDictionaryEN['TotalQuotePrice'] = 'Total Quote Price';
DataDictionaryEN['NetPrice'] = 'Net Price';
DataDictionaryEN['Generatedsuccessfully'] = 'Generated successfully';
DataDictionaryEN['QuotationOptionalItems'] = 'Quotation Optional Items';
DataDictionaryEN['OptionalItemsDiscountPercentage'] = 'Optional Items Discount Percentage';
DataDictionaryEN['OptionalItemsDiscountAmount'] = 'Optional Items Discount Amount';
DataDictionaryEN['OptionalItemsTotalQuotePrice'] = 'Optional Items Total Quote Price';
DataDictionaryEN['OptionalItemsNetPrice'] = 'Optional Items Net Price';
DataDictionaryEN['Validationerror'] = 'Validation error';
DataDictionaryEN['optionalItemsValidation'] = 'You did not add any optional Items, are you sure you want to save?';
DataDictionaryEN['TermsAndConditionsText'] = 'Terms and conditions text';
DataDictionaryEN['Clone'] = 'Clone';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////SalesOrder Section///////////////////////////////////////////////////////////////////
DataDictionaryEN["SalesOrder"] = 'SalesOrder';
DataDictionaryEN["AddSalesOrder"] = 'Add SalesOrder';
DataDictionaryEN['SalesOrderDetails'] = 'SalesOrder Details';
DataDictionaryEN['SalesOrder'] = 'Sales Order';
DataDictionaryEN['Qoutation'] = 'Quotation';
DataDictionaryEN['Opportunity'] = 'Opportunity';
DataDictionaryEN['ReferenceNumber'] = 'Reference Number';
DataDictionaryEN['Currency'] = 'Currency';
DataDictionaryEN['PriceList'] = 'Price List';
DataDictionaryEN['TermsAndConditions'] = 'Terms And Conditions';
DataDictionaryEN['Attnto'] = 'Attnto.';
DataDictionaryEN['EstimatedDeliveryDate'] = 'Estimated Delivery Date';
DataDictionaryEN['DurationOfScheduledVisitsInMonths'] = 'Duration of Scheduled Visits in months';
DataDictionaryEN['AgreedNumberofServiceVisits'] = 'Agreed Number of Service Visits';
DataDictionaryEN['TotalPrice'] = 'Total Price';
DataDictionaryEN['NetPrice'] = 'Net Price';
DataDictionaryEN['DiscountAmount'] = 'Discount Amount';
DataDictionaryEN['DiscountPercentage'] = 'Discount Percentage';
DataDictionaryEN['IsFinal'] = 'Is Final';
DataDictionaryEN['OptionalItemsTotalPrice'] = 'Optional Items Total Price';
DataDictionaryEN['OptionalItemsNetPrice'] = 'Optional Items Net Price';
DataDictionaryEN['OptionalItemsDiscountAmount'] = 'Optional Items Discount Amount';
DataDictionaryEN['OptionalItemsDiscountPercentage'] = 'Optional Items Discount Percentage';
DataDictionaryEN['VariationOrders'] = 'Variation Orders';
DataDictionaryEN['AddNewVariationOrders'] = 'Add New Variation Orders';
DataDictionaryEN['HQuotation'] = 'Quotation: ';
DataDictionaryEN['HFinal'] = 'Final Order';
DataDictionaryEN['MarkFinal'] = 'Mark Final';
DataDictionaryEN['MarkedFinalsuccessfully'] = 'Marked Final successfully';
DataDictionaryEN['OrderAlredayExist'] = 'The current quotation already has sales order';
DataDictionaryEN['CanNotDeleteGeneratedQuotation'] = 'Can not delete generated quotation';
DataDictionaryEN['CanNotEditGeneratedQuotation'] = 'Can not edit generated quotation';
DataDictionaryEN['quotationAlreadyGenerated'] = 'quotation already generated';
DataDictionaryEN['CanNoDeleteFinalOrder'] = 'Can not delete final Order';
DataDictionaryEN['CanNotEditFinalOrder'] = 'Can not edit final order';
DataDictionaryEN['OrderAlreadyMarkedAsFinal'] = 'Order already marked as final';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// OrderDeliveredItems English Dictionary////////////////////////////////////////////////
DataDictionaryEN["OrderDeliveredItems"] = 'Order Delivered Items';
DataDictionaryEN["AddOrderDeliveredItems"] = 'Add Order Delivered Items';
DataDictionaryEN['OrderDeliveredItemsDetails'] = 'Order Delivered Items Details';
DataDictionaryEN['DeliveredItem'] = 'Delivered Item';
DataDictionaryEN['Opportunity'] = 'Opportunity';
DataDictionaryEN['Stage'] = 'Stage';
DataDictionaryEN['Quotation'] = 'Quotation';
DataDictionaryEN['SalesOrder'] = 'Sales Order';
DataDictionaryEN['Item'] = 'Item';
DataDictionaryEN['Quantity'] = 'Quantity';
DataDictionaryEN['SerialNo'] = 'Serial No';
DataDictionaryEN['DeliveryDate'] = 'Delivery Date';
DataDictionaryEN['IsKit'] = 'Is Kit';
DataDictionaryEN['QuantityToDeliver'] = 'Quantity To Deliver';
DataDictionaryEN['OrderedQuantity'] = 'Ordered Quantity';
DataDictionaryEN['AvailableToDeliver'] = 'Available To Deliver';
DataDictionaryEN['ExceedsNeededQuantity'] = 'Exceeds needed quantity';
DataDictionaryEN['GreaterThanZero'] = 'should be greater than 0';
DataDictionaryEN['ItemsAlreadyAdded'] = 'Items already added';
DataDictionaryEN['SelectItems'] = 'Select Items to be delivered';
DataDictionaryEN['checkItemsQuantities'] = 'Check Items quantities';
DataDictionaryEN['ClickToAddItems'] = 'Click To Add Items';
DataDictionaryEN['NotAllItemsDelivered'] = 'Not all items delivered';
DataDictionaryEN['addItems'] = 'Delivered Items';
DataDictionaryEN['ThereIsNoAnyDeliveredItems'] = 'There is no any delivered items';
DataDictionaryEN['Report'] = 'Report';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Stock Section///////////////////////////////////////////////////////////////////
////////////////////////////////////////Stock List /////////////////////////////////////////////////////////////////////
DataDictionaryEN["StockUpdate"] = 'Stock Update';
DataDictionaryEN["StockUpdateDetails"] = 'Stock Update Details';
DataDictionaryEN["SItemGroup"] = 'Item Group';
DataDictionaryEN["SItem"] = 'Item Description';
DataDictionaryEN["TransactionType"] = 'Transaction Type';
DataDictionaryEN["NewStockAction"] = 'New Stock Action';
DataDictionaryEN["MoveFrom"] = 'Move From';
DataDictionaryEN["MoveTo"] = 'Move To';
DataDictionaryEN["QuantityAdded"] = 'Quantity Added';
DataDictionaryEN["TransactionDate"] = 'Date';
DataDictionaryEN["Details"] = 'Details';
DataDictionaryEN["AddStock"] = 'Add Stock';
DataDictionaryEN["NewItems"] = 'New Items';
DataDictionaryEN["MoveFromAndToShouldBeDifferant"] = 'Move From and Move to should be different';
DataDictionaryEN["WarehouseQuantityNotAvailable"] = 'This Quantity are not available in the warehouse';
DataDictionaryEN["ShowRoomQuantityNotAvailable"] = 'This Quantity are not available in the showroom';
DataDictionaryEN["ShouldBePositiveValueToBeMoved"] = 'Should be positive value to be moved';
DataDictionaryEN["ShouldBePositiveValueToBeAdded"] = 'Should be positive value to be added';
DataDictionaryEN["ShouldBeNegativeValueToBeRemoved"] = 'Should be negative value to be removed';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Stock Links /////////////////////////////////////////////////////////////////////
DataDictionaryEN["Reset"] = 'Reset';
DataDictionaryEN["Search"] = 'Search';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////User section//////////////////////////////////////////////////////////////////
DataDictionaryEN["User"] = 'User';
DataDictionaryEN["AddUser"] = 'Add User';
DataDictionaryEN["UserDetails"] = 'User Details';
DataDictionaryEN["UUserName"] = 'User Name';
DataDictionaryEN["UFirstName"] = 'First Name';
DataDictionaryEN["UMiddleName"] = 'Middle Name';
DataDictionaryEN["ULastName"] = 'Last Name';
DataDictionaryEN["UPassword"] = 'Password';
DataDictionaryEN["UConfirmPassword"] = 'Confirm Password';
DataDictionaryEN["UTitle"] = 'Title';
DataDictionaryEN["DateOfBirth"] = 'Date Of Birth';
DataDictionaryEN["UEmail"] = 'Email';
DataDictionaryEN["URole"] = 'Role';
DataDictionaryEN["UTeam"] = 'Team';
DataDictionaryEN["UManager"] = 'Manager';
DataDictionaryEN["UIsDefault"] = 'Is Default';
DataDictionaryEN["UIsManager"] = 'Is Manager';
DataDictionaryEN["UNote"] = 'Note';
DataDictionaryEN["UHeaderUserName"] = 'User Name : ';
DataDictionaryEN["UHeaderName"] = 'Name : ';
DataDictionaryEN["UHeaderEMail"] = 'Email : ';
DataDictionaryEN["USpecialCharError"] = 'can\'t contain spaces or special charachters';
DataDictionaryEN["USPasswordConfirmation"] = 'password doesn\'t match confirmation';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////Team section//////////////////////////////////////////////////////////////////
DataDictionaryEN["TManager"] = 'Manager';
DataDictionaryEN["TDescription"] = 'Description';
DataDictionaryEN["TeamsList"] = 'Teams List';
DataDictionaryEN["TArabicName"] = 'Arabic Name';
DataDictionaryEN["TName"] = 'Name';
DataDictionaryEN["TAddnewTeam"] = 'Add new Team';
DataDictionaryEN["TTeams"] = 'Teams';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////Role section//////////////////////////////////////////////////////////////////
DataDictionaryEN["Roles"] = 'Roles';
DataDictionaryEN["RDescription"] = 'Description';
DataDictionaryEN["RolesList"] = 'Roles List';
DataDictionaryEN["RArabicName"] = 'Arabic Name';
DataDictionaryEN["RName"] = 'Name';
DataDictionaryEN["RAddnewRole"] = 'Add new Role';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////Lovs section//////////////////////////////////////////////////////////////////
DataDictionaryEN["LOVs"] = 'LOVs';
DataDictionaryEN["LOVsValueAR"] = 'Value AR';
DataDictionaryEN["LOVsValue"] = 'Value';
DataDictionaryEN["LOVsContentType"] = 'Content Type';
DataDictionaryEN["AddnewLOV"] = 'Add new LOV';
DataDictionaryEN["SystemContentsCannotBeDeleted"] = 'System contents cannot be deleted !';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////Lock Entity section///////////////////////////////////////////////////////////
DataDictionaryEN["UnlockForAllUsers"] = 'Unlock for all users';
DataDictionaryEN["SelectUser"] = 'Select user';
DataDictionaryEN["Unlock"] = 'Unlock';
DataDictionaryEN["Date"] = 'Date';
DataDictionaryEN["LockedBy"] = 'LockedBy';
DataDictionaryEN["UnlockAll"] = 'Unlock All ';
DataDictionaryEN["UnlockedSuccessfully"] = 'Unlocked successfully';
DataDictionaryEN["FailedToUnlock"] = 'Failed to unlock';
DataDictionaryEN["EntityLock"] = 'Entity lock';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////Dash Board section///////////////////////////////////////////////////////////
DataDictionaryEN["ToDoList"] = 'To do list';
DataDictionaryEN["DBReferenceNo"] = 'Reference No.';
DataDictionaryEN["DBDetails"] = 'Details';
DataDictionaryEN["DBVisitType"] = 'Visit Type';
DataDictionaryEN["DBStatus"] = 'Status';
DataDictionaryEN["DBShowAll"] = 'Show all';
DataDictionaryEN["MyProfile"] = 'My Profile';
DataDictionaryEN["ChangePassword"] = 'Change password';
DataDictionaryEN["DBUserName"] = 'User Name';
DataDictionaryEN["DBFirstName"] = 'First Name';
DataDictionaryEN["DBMiddleName"] = 'Middle Name';
DataDictionaryEN["DBLastName"] = 'Last Name';
DataDictionaryEN["DBOldPassword"] = 'Old Password';
DataDictionaryEN["DBNewPassword"] = 'New Password';
DataDictionaryEN["DBConfirm"] = 'Confirm';
DataDictionaryEN["DBTitle"] = 'Title';
DataDictionaryEN["DBManager"] = 'Manager';
DataDictionaryEN["DBDateOfBirth"] = 'Date Of Birth';
DataDictionaryEN["DBEmail"] = 'Email';
DataDictionaryEN["DBRole"] = 'Role';
DataDictionaryEN["DBTeam"] = 'Team';
DataDictionaryEN["DBManager"] = 'Manager';
DataDictionaryEN["DBIsManager"] = 'Is Manager';
DataDictionaryEN["DBSales"] = 'Sales';
DataDictionaryEN["DBInvoiceNo"] = 'Invoice No.';
DataDictionaryEN["DBCompany"] = 'Company';
DataDictionaryEN["DBPerson"] = 'Person';
DataDictionaryEN["DBBranch"] = 'Branch';
DataDictionaryEN["DBDescription"] = 'Description';
DataDictionaryEN["SalesGraph"] = 'Sales Graph';
DataDictionaryEN["DBClientCareCasesGraph"] = 'Client Care Cases Graph';
DataDictionaryEN["DBClientCareCase"] = 'Client Care Case';
DataDictionaryEN["DBType"] = 'Type';
DataDictionaryEN["ChequeDate"] = 'Cheque Date';
DataDictionaryEN["TransactionDate"] = 'Transaction Date';

DataDictionaryEN["ChequeDateFrom"] = 'Cheque Date From';
DataDictionaryEN["TransactionDateFrom"] = 'Transaction Date From';
DataDictionaryEN["ChequeDateTo"] = 'Cheque Date To';
DataDictionaryEN["TransactionDateTo"] = 'Transaction Date To';
DataDictionaryEN["PDCReport"] = "PDC Cheque Report";

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////Permission  section///////////////////////////////////////////////////////////
DataDictionaryEN["PRole"] = 'Role';
DataDictionaryEN["CollapseAll"] = 'Collapse all';
DataDictionaryEN["SelectAll"] = 'Select all';
DataDictionaryEN["UnSelectAll"] = 'UnSelect all';
DataDictionaryEN["Permissions"] = 'Permissions';
DataDictionaryEN["UnCollapseAll"] = 'UnCollapse all';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////SalesOrder  section///////////////////////////////////////////////////////////
DataDictionaryEN["SalesOrderDetails"] = 'Sales Order Details ';
DataDictionaryEN["SOOrderNumber"] = 'Order Number';
DataDictionaryEN["SOSales"] = 'Sales';
DataDictionaryEN["SOIsTaxExempt"] = 'Is Tax Exempt ?';
DataDictionaryEN["SOTotalInvoiceValue"] = 'Total Invoice Value';
DataDictionaryEN["SOComments"] = 'Other Comments';
DataDictionaryEN["SOSalesOrderItem"] = 'Sales Order Item';
DataDictionaryEN["SOItemGroup"] = 'Item Group';
DataDictionaryEN["SOItem"] = 'Item';
DataDictionaryEN["SODimensionsLength"] = 'Dimensions Length';
DataDictionaryEN["SODimensionsWidth"] = 'Dimensions Width';
DataDictionaryEN["SOListPrice"] = 'List Price';
DataDictionaryEN["SOPriceWithoutTax"] = 'Price Without Tax';
DataDictionaryEN["SOActualDimensionsLength"] = 'Actual Dimensions Length';
DataDictionaryEN["SOActualDimensionsWidth"] = 'Actual Dimensions Width';
DataDictionaryEN["SOQuantity"] = 'Quantity';
DataDictionaryEN["SOIsGiveaway"] = 'Is Giveaway ?';
DataDictionaryEN["SODiscountPercentage"] = 'Discount Percentage %';
DataDictionaryEN["SOMax"] = 'Max. % =';
DataDictionaryEN["SODiscountValue"] = 'Discount Value';
DataDictionaryEN["SONetPrice"] = 'Net Price Total';
DataDictionaryEN["SOInventoryType"] = 'Inventory Type';
DataDictionaryEN["SONotes"] = 'Notes';
DataDictionaryEN["SOListPricewithtax"] = 'List Price with tax';
DataDictionaryEN["SOListPricewithouttax"] = 'List Price without tax';
DataDictionaryEN["SOTotalPrice"] = 'Total Price';
DataDictionaryEN["SOSaleStage"] = 'Sales Stage';
DataDictionaryEN["couldNotSaveWithoutAddItemMsg"] = 'You should add at least one item to save';
DataDictionaryEN["addItemMsg"] = 'Add Item';
DataDictionaryEN["AddSalesOrder"] = 'Add Sales Order';
DataDictionaryEN["SalesOrder"] = 'Sales Order';
DataDictionaryEN["deleteNonLeadSaleSalesOrder"] = 'Can not delete non lead stage sales order';
DataDictionaryEN["InstallationOrder"] = 'Installation Order';
DataDictionaryEN["ProductionOrder"] = 'Production Order';
DataDictionaryEN["FrunitureInstallationOrder"] = 'Fruniture Installation Order';
DataDictionaryEN["PostponeRequest"] = 'Postpone Request';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////Calender  section///////////////////////////////////////////////////////////
DataDictionaryEN["Rejected"] = 'Rejected';
DataDictionaryEN["Closed"] = 'Closed';
DataDictionaryEN["Inactive"] = 'Inactive';
DataDictionaryEN["Active"] = 'Active';

DataDictionaryEN["Calendar"] = 'Calendar';
DataDictionaryEN["AddTask"] = 'Add Task';
DataDictionaryEN["AllDay"] = 'all-day';
DataDictionaryEN["am"] = 'am';
DataDictionaryEN["pm"] = 'pm';
DataDictionaryEN["AM"] = 'AM';
DataDictionaryEN["PM"] = 'PM';
DataDictionaryEN["Today"] = 'Today';
DataDictionaryEN["Month"] = 'Month';
DataDictionaryEN["Week"] = 'Week';
DataDictionaryEN["Day"] = 'Day';
////////DaysShort
DataDictionaryEN["Sun"] = 'Sun';
DataDictionaryEN["Mon"] = 'Mon';
DataDictionaryEN["Tue"] = 'Tue';
DataDictionaryEN["Wed"] = 'Wed';
DataDictionaryEN["Thu"] = 'Thu';
DataDictionaryEN["Fri"] = 'Fri';
DataDictionaryEN["Sat"] = 'Sat';

////////Days
DataDictionaryEN["Sunday"] = 'Sunday';
DataDictionaryEN["Monday"] = 'Monday';
DataDictionaryEN["Tuesday"] = 'Tuesday';
DataDictionaryEN["Wednesday"] = 'Wednesday';
DataDictionaryEN["Thursday"] = 'Thursday';
DataDictionaryEN["Friday"] = 'Friday';
DataDictionaryEN["Saturday"] = 'Saturday';

////////monthNames
DataDictionaryEN["January"] = 'January';
DataDictionaryEN["February"] = 'February';
DataDictionaryEN["March"] = 'March';
DataDictionaryEN["April"] = 'April';
DataDictionaryEN["May"] = 'May';
DataDictionaryEN["June"] = 'June';
DataDictionaryEN["July"] = 'July';
DataDictionaryEN["August"] = 'August';
DataDictionaryEN["September"] = 'September';
DataDictionaryEN["October"] = 'October';
DataDictionaryEN["November"] = 'November';
DataDictionaryEN["December"] = 'December';



////////monthNamesShort
DataDictionaryEN["Jan"] = 'Jan';
DataDictionaryEN["Feb"] = 'Feb';
DataDictionaryEN["Mar"] = 'Mar';
DataDictionaryEN["Apr"] = 'Apr';
DataDictionaryEN["May"] = 'May';
DataDictionaryEN["Jun"] = 'Jun';
DataDictionaryEN["Jul"] = 'Jul';
DataDictionaryEN["Aug"] = 'Aug';
DataDictionaryEN["Sep"] = 'Sep';
DataDictionaryEN["Oct"] = 'Oct';
DataDictionaryEN["Nov"] = 'Nov';
DataDictionaryEN["Dec"] = 'Dec';

////////Dive Add Task
DataDictionaryEN["CalStartDate"] = 'Start Date';
DataDictionaryEN["CalEndDate"] = 'End Date';
DataDictionaryEN["CalType"] = 'Type';
DataDictionaryEN["CalPriority"] = 'Priority';
DataDictionaryEN["CalStatus"] = 'Status';
DataDictionaryEN["CalAction"] = 'Action';
DataDictionaryEN["CalVisitType"] = 'Visit Type';
DataDictionaryEN["CalArea"] = 'Area';
DataDictionaryEN["CalDescription"] = 'Description';
DataDictionaryEN["CalMeetingResults"] = 'Meeting Results';
DataDictionaryEN["CalHistoryUpdated"] = 'History Updated';
DataDictionaryEN["CalAssignedBy"] = 'AssignedBy';
DataDictionaryEN["CalCreatedDate"] = 'Created Date';
DataDictionaryEN["CalLastUpdatedBy"] = 'Last Updated By';
DataDictionaryEN["CalLastUpdatedDate"] = 'Last Updated Date';
DataDictionaryEN["CalCreatedBy"] = 'Created By';
DataDictionaryEN["CalRef-NO"] = 'Ref-NO';
DataDictionaryEN["CalClosedBy"] = 'Closed By';
DataDictionaryEN["CalClosedDate"] = 'Closed Date';
DataDictionaryEN["Discussion"] = 'Discussion';
DataDictionaryEN["DeleteTask"] = 'Delete Task';
DataDictionaryEN["FaildDeleteTask"] = 'faild to delete this task';
DataDictionaryEN["errorLoadingCalender"] = 'error while loading calendar';
DataDictionaryEN["CalSearch"] = 'Search';
DataDictionaryEN["CalAllTask"] = 'All Tasks';
DataDictionaryEN["CalByMe"] = 'By Me';
DataDictionaryEN["CalToMe"] = 'To Me';

DataDictionaryEN["ClAssignedTo"] = 'Assigned To';
DataDictionaryEN["AssignedBy"] = 'Assigned By';
DataDictionaryEN["Type"] = 'Type';
DataDictionaryEN["Priority"] = 'Priority';
DataDictionaryEN["DateFrom"] = 'Date From';
DataDictionaryEN["DateTo"] = 'Date To';
DataDictionaryEN["TimeFrom"] = 'Time From';
DataDictionaryEN["TimeTo"] = 'Time To';
DataDictionaryEN["ShowHideFilter"] = ' Advance search';
DataDictionaryEN["Assignment"] = 'Task Assignment';
DataDictionaryEN["TaskList"] = 'Task List';

DataDictionaryEN["CalStartDateTime"] = 'Start Date & Time';
DataDictionaryEN["CalEndDateTime"] = 'End Date & Time';
DataDictionaryEN["CalendarListView"] = 'List view';
DataDictionaryEN["CalendarView"] = 'Calendar view';
DataDictionaryEN["EndDateErrorMsg"] = 'End date should be equal or after the start date';
DataDictionaryEN["Print"] = 'Print';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////Advanced Search   section/////////////////////////////////////////////////////
DataDictionaryEN["AdvFilterYourResults"] = 'Filter your results';
DataDictionaryEN["AdvSortingField"] = 'Sorting field';
DataDictionaryEN["AdvAscending"] = 'Ascending';
DataDictionaryEN["AdvFrom"] = 'from';
DataDictionaryEN["AdvTo"] = 'to';
DataDictionaryEN["LnkReset"] = 'Reset';
DataDictionaryEN["LnkSearch"] = 'Search';
DataDictionaryEN["AdvFromTo"] = ' From-To';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////OnScreen Notification section//////////////////////////////////////////////////
DataDictionaryEN["OnScreenNotTotalOf"] = 'Total of ';
DataDictionaryEN["SnoozeAll"] = 'Snooze all ';
DataDictionaryEN["Snooze"] = 'Snooze';
DataDictionaryEN["Dismiss"] = 'Dismiss';
DataDictionaryEN["DismissAll"] = 'Dismiss all ';
DataDictionaryEN["Nmin"] = 'min ';
DataDictionaryEN["Nhour"] = 'hour';
DataDictionaryEN["Nday"] = 'day';
DataDictionaryEN["Nweek"] = 'week';
DataDictionaryEN["Totalof0"] = 'Total of 0';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryEN["ChromeRecommend"] = 'For better performance, we recommend using Google Chrome browser';
/////////////////////////////////////////Report  section////////////////////////////////////////////////////////////////
DataDictionaryEN["DailySalesSummary"] = 'Daily Sales Summary';
DataDictionaryEN["OrderDateFrom"] = 'Order Date From';
DataDictionaryEN["OrderDateTo"] = 'Order Date To';
DataDictionaryEN["OpportunityNumber"] = 'Sales Number';
DataDictionaryEN["TotalAmount"] = 'Total Amount';
DataDictionaryEN["FilterYourResults"] = 'Filter your results';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////Client Care section////////////////////////////////////////////////////////////////
DataDictionaryEN["lblCaseType"] = 'Case Type';
DataDictionaryEN["lblReferenceNumber"] = 'Reference Number';
DataDictionaryEN["lblCasesDetails"] = 'Case Details';
DataDictionaryEN["lblCustomerMood"] = 'Customer Mood';
DataDictionaryEN["lblSale"] = 'Sale';
DataDictionaryEN["lblDateOfSale"] = 'Date Of Sale';
DataDictionaryEN["lblDateReceived"] = 'Date Received';
DataDictionaryEN["lblRequiredDeliveryDate"] = 'Required Delivery Date';
DataDictionaryEN["lblActualDeliveryDate"] = 'Actual Delivery Date';
DataDictionaryEN["lblDateOfWithdrawalOfGoods"] = 'Date Of Withdrawal Of Goods';
DataDictionaryEN["lblEffectiveDateOfTheWithdrawalOfGoods"] = 'Effective Date Of The Withdrawal Of Goods';
DataDictionaryEN["lblProposedSolution"] = 'Proposed Solution';
DataDictionaryEN["lblReason"] = 'Reason';
DataDictionaryEN["lblShowroomOffiecerComments"] = 'Showroom Officer Comments';
DataDictionaryEN["lblWarehouseManagerComments"] = 'Warehouse Manager Comments';
DataDictionaryEN["lblQAMangerComments"] = 'QA Manger Comments';
DataDictionaryEN["lblReasonRejected"] = 'Reason Rejected';
DataDictionaryEN["HeaderCaseType"] = 'Case Type: ';
DataDictionaryEN["HeaderStatus"] = 'Status: ';
DataDictionaryEN["AddCase"] = 'Add Case';
DataDictionaryEN["Cases"] = 'Cases';
DataDictionaryEN["StatusPending"] = 'Pending';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////Reports Section////////////////////////////////////////////////////////////////
DataDictionaryEN["btnSearch"] = 'Search';
DataDictionaryEN["lnkReset"] = 'Reset';
DataDictionaryEN["CreateDateFrom"] = 'Create Date From';
DataDictionaryEN["CreateDateTo"] = 'Create Date To';
DataDictionaryEN["BusinessEmail"] = 'Business Email';
DataDictionaryEN["PersonalEmail"] = 'Personal Email';

/////////////////////////////////////////Company Report
DataDictionaryEN["CompanyName"] = 'Company Name';
DataDictionaryEN["Type"] = 'Type';
DataDictionaryEN["Status"] = 'Status';
DataDictionaryEN["Active"] = 'Active';
DataDictionaryEN["Inactive"] = 'Inactive';
DataDictionaryEN["WebSite"] = 'WebSite';
DataDictionaryEN["LandLineNumber"] = 'Home';
DataDictionaryEN["CompaniesReport"] = 'Full Customers List (Companies)';
/////////////////////////////////////////Person Report
DataDictionaryEN["FirstName"] = 'First Name';
DataDictionaryEN["LastName"] = 'Last Name';
DataDictionaryEN["MobileNumber"] = 'Mobile Number';
DataDictionaryEN["Gender"] = 'Gender';
DataDictionaryEN["MaritalStatus"] = 'Marital Status';
DataDictionaryEN["Nationality"] = 'Nationality';
DataDictionaryEN["Country"] = 'Country';
DataDictionaryEN["City"] = 'City';
DataDictionaryEN["PersonsReport"] = 'Full Customers List (Persons)';

/////////////////////////////////////////StockUpdate Report
DataDictionaryEN["TransactionDateFrom"] = 'Transaction Date From';
DataDictionaryEN["TransactionDateTo"] = 'Transaction Date To';
DataDictionaryEN["TransactionType"] = 'Transaction Type';
DataDictionaryEN["NewStockAction"] = 'New Stock Action';
DataDictionaryEN["MoveFrom"] = 'Move From';
DataDictionaryEN["MoveTo"] = 'Move To';
DataDictionaryEN["QuantityAdded"] = 'Quantity Added';
DataDictionaryEN["Details"] = 'Details';
DataDictionaryEN["StockUpdateReportTitle"] = 'Stock Update History';
/////////////////////////////////////////SoldPieces Report
DataDictionaryEN["SoldPiecesReportTitle"] = 'Sold Pieces Per Month';
/////////////////////////////////////////Calender Report
DataDictionaryEN["CalenderReportTitle"] = 'Calender Report Title';
DataDictionaryEN["StartDateFrom"] = 'Start Date From';
DataDictionaryEN["StartDateTo"] = 'Start Date To';
DataDictionaryEN["ReferenceNo"] = 'Reference No';
DataDictionaryEN["StartTimeFrom"] = 'Start Time From';
DataDictionaryEN["StartTimeTo"] = 'Start Time To';
DataDictionaryEN["Area"] = 'Area';
DataDictionaryEN["EndDateFrom"] = 'End Date From';
DataDictionaryEN["EndDateTo"] = 'End Date To';
DataDictionaryEN["EndTimeFrom"] = 'End Time From';
DataDictionaryEN["EndTimeTo"] = 'End Time To';
DataDictionaryEN["VisitType"] = 'Visit Type';
DataDictionaryEN["Actions"] = 'Actions';
DataDictionaryEN["AssignedTo"] = 'Assigned To';
DataDictionaryEN["TaskType"] = 'Report Type';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////SMS Configuration  section////////////////////////////////////////////////////////////////
DataDictionaryEN["SMSConfigurationDetails"] = 'SMS Configuration Details';
DataDictionaryEN["AddParameter"] = 'Add Parameter';
DataDictionaryEN["Name"] = 'Name';
DataDictionaryEN["SMSUrl"] = 'SMSUrl';
DataDictionaryEN["Key"] = 'Key';
DataDictionaryEN["Value"] = 'Value';
DataDictionaryEN["Option"] = 'Option';
DataDictionaryEN["HName"] = 'Name :';
DataDictionaryEN["HDescription"] = 'Description :';
DataDictionaryEN["AddSMSConfiguration"] = 'Add SMS Configuration';
DataDictionaryEN["SMSConfiguration"] = 'SMS Configuration';
DataDictionaryEN["Remove"] = 'Remove';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////Template Details  section////////////////////////////////////////////////////////////////
DataDictionaryEN["TemplateDetails"] = 'Template Details';
DataDictionaryEN["TemplateName"] = 'Template Name';
DataDictionaryEN["Type"] = 'Type';
DataDictionaryEN["Email"] = 'Email';
DataDictionaryEN["SMS"] = 'SMS';
DataDictionaryEN["TermsAndCondition"] = 'Terms and conditions';
DataDictionaryEN["Description"] = 'Description';
DataDictionaryEN["Template"] = 'Template';
DataDictionaryEN["SMSTemplate"] = 'SMS Template';
DataDictionaryEN["maximumLengthForArabic"] = '** The maximum length for arabic characters is 110';
DataDictionaryEN["maximumLengthForEnglish"] = '** The maximum length for english characters is 300';
DataDictionaryEN["HTemplateName"] = 'Template Name : ';
DataDictionaryEN["HType"] = 'Type : ';
DataDictionaryEN["NotificationTemplates"] = 'Notification Templates';
DataDictionaryEN["AddTemplate"] = 'Add Template';
DataDictionaryEN["TemplateErrorMsg"] = "Please insert template text";
DataDictionaryEN["Error"] = "Error";
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////Send Notification section////////////////////////////////////////////////////////
DataDictionaryEN["Details"] = 'Details';
DataDictionaryEN["Subject"] = 'Subject';
DataDictionaryEN["Description"] = 'Description';
DataDictionaryEN["Company"] = 'Company';
DataDictionaryEN["Person"] = 'Person';
DataDictionaryEN["Group"] = 'Group';
DataDictionaryEN["All"] = 'All';
DataDictionaryEN["Email&Template"] = 'Email & Template';
DataDictionaryEN["Type"] = 'Type';
DataDictionaryEN["Email"] = 'Email';
DataDictionaryEN["EmailType"] = 'Email Type';
DataDictionaryEN["SMS"] = 'SMS';
DataDictionaryEN["BusinessEmail"] = 'Business Email';
DataDictionaryEN["PersonalEmail"] = 'Personal Email';
DataDictionaryEN["Both"] = 'Both';
DataDictionaryEN["Template"] = 'Template';
DataDictionaryEN["SendSMSAndEmails"] = 'Send sms and emails';
DataDictionaryEN["Your"] = 'Your ';
DataDictionaryEN["email"] = 'email';
DataDictionaryEN["sms"] = 'sms';
DataDictionaryEN["hasBeenSentSuccessfully"] = ' has been sent successfully';
DataDictionaryEN["pleaseSelectCompanyOrPersonOrGroup"] = 'please select company or person or group';
DataDictionaryEN["Required"] = 'Required';
DataDictionaryEN["Contacts"] = 'Contacts';
DataDictionaryEN["Send"] = 'Send';
DataDictionaryEN["Reset"] = 'Reset';
DataDictionaryEN["GroupDescriptionName"] = 'Description';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////Email Configuration section////////////////////////////////////////////////////////
DataDictionaryEN["EmailConfigurationDetails"] = 'Email Configuration Details';
DataDictionaryEN["EmailConfiguration"] = 'Email Configuration';
DataDictionaryEN["AddEmailConfiguration"] = 'Add Email Configuration';
DataDictionaryEN["Email"] = 'Email';
DataDictionaryEN["DisplayName"] = 'Display Name';
DataDictionaryEN["UserName"] = 'User Name';
DataDictionaryEN["Password"] = 'Password';
DataDictionaryEN["Host"] = 'Host';
DataDictionaryEN["Port"] = 'Port';
DataDictionaryEN["EnableSSL"] = 'Enable SSL';
DataDictionaryEN["Timeout"] = 'Timeout';
DataDictionaryEN["SendInternalNotifications"] = 'Send Internal Notifications';
DataDictionaryEN["Description"] = 'Description';
DataDictionaryEN["HEmail"] = 'Email : ';
DataDictionaryEN["HDisplayName"] = 'Display Name : ';
DataDictionaryEN["Description"] = 'Description';
DataDictionaryEN["AlreadyExists"] = 'Already exists';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////Group section////////////////////////////////////////////////////////
DataDictionaryEN["GroupDetails"] = 'Group Details';
DataDictionaryEN["FullView"] = 'Full View';
DataDictionaryEN["GroupName"] = 'Group Name';
DataDictionaryEN["Entity"] = 'Entity';
DataDictionaryEN["Description"] = 'Description';
DataDictionaryEN["HEntity"] = 'Entity';
DataDictionaryEN["HGroupName"] = 'Group Name';
DataDictionaryEN["ContactsGroups"] = 'Contacts Groups';
DataDictionaryEN["AddContactsGroup"] = 'Add Contacts Group';
DataDictionaryEN["ShowResult"] = 'Show Result';
DataDictionaryEN["SearchCriteria"] = 'Search Criteria';
DataDictionaryEN["Field"] = 'Field';
DataDictionaryEN["Value1"] = 'Value 1';
DataDictionaryEN["Value2"] = 'Value 2';
DataDictionaryEN["Yes"] = 'Yes';
DataDictionaryEN["No"] = 'No';
DataDictionaryEN["True"] = 'True';
DataDictionaryEN["False"] = 'False';
DataDictionaryEN["All"] = 'All';
DataDictionaryEN["Filter"] = 'Apply';
DataDictionaryEN["Operation"] = 'Operation';
DataDictionaryEN["Group"] = 'Group';
DataDictionaryEN["Options"] = 'Options';
DataDictionaryEN["AND"] = 'AND';
DataDictionaryEN["OR"] = 'OR';
DataDictionaryEN["Start"] = 'Start';
DataDictionaryEN["End"] = 'End';
DataDictionaryEN["Fields"] = 'Fields';
DataDictionaryEN["Add"] = 'Add';
DataDictionaryEN["Clear"] = 'Clear';

DataDictionaryEN["GroupContacts"] = 'Group Contacts';
DataDictionaryEN["Mobile"] = 'Mobile';
DataDictionaryEN["BusinessEmail"] = 'Business Email';
DataDictionaryEN["ContactName"] = 'Contact Name';
DataDictionaryEN["Contacts"] = 'Contacts';
DataDictionaryEN["BracketsCountError"] = 'Brackets Count Error';
DataDictionaryEN["BracketsCountErrorMsg"] = 'No. of opened bracket doesn\'t match no. of closed brackets';
DataDictionaryEN["BracketsError"] = 'Brackets Error';
DataDictionaryEN["BracketsCombinationError"] = 'Brackets combination error';
DataDictionaryEN["Equal"] = 'Equal';
DataDictionaryEN["Contains"] = 'Contains';
DataDictionaryEN["StartWith"] = 'Start With';
DataDictionaryEN["EndWith"] = 'End With';
DataDictionaryEN["Between"] = 'Between';
DataDictionaryEN["Before"] = 'Before';
DataDictionaryEN["After"] = 'After';
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// ReturnedItem////////////////////////////////////////////////
DataDictionaryEN["ReturnedItem"] = 'Returned Item-VO\'s';
DataDictionaryEN["AddReturnedItem"] = 'Add Returned Item-VO\'s';
DataDictionaryEN['ReturnedItemDetails'] = 'Returned Item-VO\'s Details';
DataDictionaryEN['ReturnedItem'] = 'Returned Item-VO\'s';
DataDictionaryEN['SalesOrder'] = 'Sales Order';
DataDictionaryEN['OrderNumber'] = 'Order Number';
DataDictionaryEN['ReturnReason'] = 'Return Reason';

DataDictionaryEN["AlreadyExist"] = ' Already Exist ';

DataDictionaryEN["TotalReturnedItems"] = 'Total of Returned Items';
DataDictionaryEN["ModelNumber"] = 'Model Number';
DataDictionaryEN["ProductName"] = 'Product Name';
DataDictionaryEN["Quantity"] = 'Quantity';
DataDictionaryEN["ReturnQuantity"] = 'Return Quantity';
DataDictionaryEN["ReturnLocation"] = 'Return Location';
DataDictionaryEN["ErrorAddReturndItem"] = 'Can\'t add returned item';
DataDictionaryEN['Details'] = 'Details';
DataDictionaryEN['AddNewDetails'] = 'Add New Details';
DataDictionaryEN['TotalReturendItems'] = 'Total';
DataDictionaryEN['DeliveredQuantity'] = 'Delivered Quantity';
DataDictionaryEN['QuantityError'] = 'mustn\'t exceed delivered quantity';


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////Inventory///////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryEN["Inventory"] = 'Inventory';
DataDictionaryEN["AddInventory"] = 'Add Inventory';
DataDictionaryEN['InventoryDetails'] = 'Inventory Details';
DataDictionaryEN['Inventory'] = 'Inventory';
DataDictionaryEN['ProductName'] = 'Product Name';
DataDictionaryEN['ProductNameAr'] = 'Product Name Ar';
DataDictionaryEN['Description'] = 'Description';
DataDictionaryEN['Image'] = 'Image';
DataDictionaryEN['Cost'] = 'Cost';
DataDictionaryEN['Unit'] = 'Unit';
DataDictionaryEN['DefaultWarrantyInMonths'] = 'Default Warranty In Months';
DataDictionaryEN['ModelNumber'] = 'Model Number';
DataDictionaryEN['AvailableQuantity'] = 'Available Quantity';
DataDictionaryEN['AvailableQuantityBonded'] = 'Available Quantity Bonded';
DataDictionaryEN['AvailableQuantityKhalda'] = 'Available Quantity Khalda';
DataDictionaryEN['QuantityOnSalesOrder'] = 'Quantity On Sales Order';
DataDictionaryEN['QuantityOrderedOnWay'] = 'Quantity Ordered On Way';
DataDictionaryEN['SuspendedItem'] = 'Suspended Item';
DataDictionaryEN['Sku'] = 'SKU';
DataDictionaryEN['RequireSerialNumberAtDelivery'] = 'Require Serial Number At Delivery';
DataDictionaryEN['StockableItem'] = 'Stockable Item';
DataDictionaryEN['ArabicDescription'] = 'Arabic Description';
DataDictionaryEN['ProductCategory'] = 'Product Category';
DataDictionaryEN['ProductSubCategory'] = 'Product Sub Category';
DataDictionaryEN['ParentProduct'] = 'Product Parent';
DataDictionaryEN["AlreadyExist"] = ' Already Exist ';
DataDictionaryEN["ImageFormatIsNotAllowed"] = ' Image format is not allowed ';
DataDictionaryEN['PriceList'] = 'Price list';
DataDictionaryEN['AddNewPricelist'] = 'Add New Price list';
DataDictionaryEN['ImageSizeError'] = 'Image size error';
DataDictionaryEN['MaximumImageSize'] = 'Maximum size is';
DataDictionaryEN['kb'] = ' kb';

// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ///////////////////////////////////////// InventoryKit English Dictionary////////////////////////////////////////////////
DataDictionaryEN['ModelNumber'] = 'Model Number';
DataDictionaryEN['Inventory'] = 'Product';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// /////////////////////////////////////////InventoryPriceList///////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryEN["InventoryPriceList"] = 'Inventory Price List';
DataDictionaryEN["AddInventoryPriceList"] = 'Add Inventory Price List';
DataDictionaryEN['InventoryPriceListDetails'] = 'Inventory Price List Details';
DataDictionaryEN['InventoryPriceList'] = 'Inventory Price List';
DataDictionaryEN['PriceList'] = 'Price List';
DataDictionaryEN['Currency'] = 'Currency';
DataDictionaryEN['ListedPrice'] = 'Listed Price';
DataDictionaryEN['MinimumPrice'] = 'Minimum Price';
DataDictionaryEN['MaximumPrice'] = 'Maximum Price';
DataDictionaryEN['Inventory'] = 'Inventory';
DataDictionaryEN["AlreadyExist"] = ' Already Exist ';
DataDictionaryEN['ValidatePrice'] = ' Must be less than maximum price';
DataDictionaryEN['PriceListTax'] = 'Price List Tax';
DataDictionaryEN['PriceCheck'] = 'Price Check';
DataDictionaryEN['RoundToMultipleOf'] = 'Round To a Multiple Of';
DataDictionaryEN['Quantity'] = 'Quantity Purchased';
DataDictionaryEN['Weight'] = 'Weight';
DataDictionaryEN['Discount'] = 'Discount';
DataDictionaryEN['Markup'] = 'Markup';
DataDictionaryEN['Percentage'] = 'Percentage';
DataDictionaryEN['Amount'] = 'Amount';
DataDictionaryEN['CustomerClass'] = 'Customer Class';


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// /////////////////////////////////////////InventoryOptions///////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryEN["IcOptions"] = 'Options';
DataDictionaryEN["Processing"] = 'Processing';
DataDictionaryEN["DocumentNumbers"] = 'Document Numbers';
DataDictionaryEN['MaxNoOfIcSegments'] = 'Max. No. of Segments';
DataDictionaryEN['MaxNoOfIcSegmentCodes'] = 'Max. No. of Segment Codes (Per Segment)';
DataDictionaryEN['ICOptions'] = "Inventory Control Options";
DataDictionaryEN['Processing'] = "Processing";
DataDictionaryEN['FunctionalCurrency'] = " Functional Currency";
DataDictionaryEN['DefaultRateType'] = "Default Rate Type";
DataDictionaryEN['MultiCurrency'] = "  Multi Currency";
DataDictionaryEN['ValidateLotExpiryDate'] = "Validate Lot Expiry Date";



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// InventoryStock////////////////////////////////////////////////
DataDictionaryEN["InventoryStock"] = 'Inventory Stock';
DataDictionaryEN["AddInventoryStock"] = 'Add Inventory Stock';
DataDictionaryEN['InventoryStockDetails'] = 'Inventory Stock Update';
DataDictionaryEN['InventoryStock'] = 'Inventory Stock';
DataDictionaryEN['Inventory'] = 'Product / Model number';
DataDictionaryEN['TransactionType'] = 'Transaction Type';
DataDictionaryEN['MoveFrom'] = 'Move From';
DataDictionaryEN['MoveTo'] = 'Move To';
DataDictionaryEN['Quantity'] = 'Quantity';
DataDictionaryEN['Details'] = 'Details';
DataDictionaryEN['Stock'] = 'Stock';
DataDictionaryEN["AlreadyExist"] = ' Already Exist ';
DataDictionaryEN['QtyValidation'] = 'Quantity musn\'t be more than availabe quantity ';
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////Kit////////////////////////////////////////////////
DataDictionaryEN["Kit"] = 'Kit';
DataDictionaryEN["AddKit"] = 'Add Kit';
DataDictionaryEN['KitDetails'] = 'Kit Details';
DataDictionaryEN['Kit'] = 'Kit';
DataDictionaryEN['KitName'] = 'Kit Name';
DataDictionaryEN['KitDescription'] = 'Kit Description';
DataDictionaryEN['KitModelNumber'] = 'Kit Model Number';
DataDictionaryEN["AlreadyExist"] = ' Already Exist ';
DataDictionaryEN['Inventory'] = 'Inventory';
DataDictionaryEN['AddNewInventory'] = 'Add New Inventory';
DataDictionaryEN['Pricelist'] = 'Price list';
DataDictionaryEN['AddNewPricelist'] = 'Add New Price list';
DataDictionaryEN['Product'] = 'Product/Model number';



// /////////////////////////////////////////KitPriceList///////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryEN["InventoryPriceList"] = 'Kit Price List';
DataDictionaryEN["AddInventoryPriceList"] = 'Add Kit Price List';
DataDictionaryEN['InventoryPriceListDetails'] = 'Kit Price List Details';
DataDictionaryEN['InventoryPriceList'] = 'Kit Price List';
DataDictionaryEN['PriceList'] = 'Price List';
DataDictionaryEN['Currency'] = 'Currency';
DataDictionaryEN['ListedPrice'] = 'Listed Price';
DataDictionaryEN['MinimumPrice'] = 'Minimum Price';
DataDictionaryEN['MaximumPrice'] = 'Maximum Price';
DataDictionaryEN['Inventory'] = 'Product';
DataDictionaryEN["AlreadyExist"] = ' Already Exist ';
DataDictionaryEN['ValidatePrice'] = ' Must be less than maximum price';



// ////////////////////////////////////////Category///////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryEN["Category"] = 'Category';
DataDictionaryEN["AddCategory"] = 'Add Category';
DataDictionaryEN['CategoryDetails'] = 'Category Details';
DataDictionaryEN['Category'] = 'Category';
DataDictionaryEN['CategoryNameEn'] = 'Category Name';
DataDictionaryEN['CategoryNameAr'] = 'Category Name Arabic';
DataDictionaryEN['DescriptionAr'] = 'Description Arabic';
DataDictionaryEN['DescriptionEn'] = 'Description';
DataDictionaryEN['Entity'] = 'Entity';
DataDictionaryEN["AlreadyExist"] = ' Already Exist ';
DataDictionaryEN['CategoryNameEn'] = 'Category Name';
DataDictionaryEN['CategoryNameAr'] = 'Category Name Arabic';


// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// /////////////////////////////////////////SubCategory///////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryEN["SubCategory"] = 'Sub Category';
DataDictionaryEN["AddSubCategory"] = 'Add Sub Category';
DataDictionaryEN['SubCategoryDetails'] = 'Sub Category Details';
DataDictionaryEN['SubCategory'] = 'Sub Category';
DataDictionaryEN['SubCategoryValueAr'] = 'Sub Category Value Arabic';
DataDictionaryEN['SubCategoryValueEn'] = 'Sub Category Value';
DataDictionaryEN['DescriptionAr'] = 'Description Arabic';
DataDictionaryEN['DescriptionEn'] = 'Description';
DataDictionaryEN['Parent'] = 'Parent';
DataDictionaryEN["ParentSubCategory"] = 'Parent';
DataDictionaryEN["AlreadyExist"] = ' Already Exist ';
DataDictionaryEN['SubCategoryValueAr'] = 'Sub Category Value Arabic';
DataDictionaryEN['SubCategoryValueEn'] = 'Sub Category Value';
DataDictionaryEN['SubCategoryEn'] = 'Sub Category';
DataDictionaryEN['AddNewSubCategory'] = 'Add Sub Category';

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////Fixed Assets///////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryEN["AddFixedAssets"] = 'Add Fixed Assets';
DataDictionaryEN["TypeName"] = 'Type Name';
DataDictionaryEN["Warranty"] = 'Warranty';
DataDictionaryEN["Model"] = 'Model';
DataDictionaryEN["Location"] = 'Location';
DataDictionaryEN["SerialNumber"] = 'Serial Number';
DataDictionaryEN["License"] = 'License';
DataDictionaryEN["ProcurementDate"] = 'Procurement Date';
DataDictionaryEN["LicenseExpirationDate"] = 'License Expiration Date';
DataDictionaryEN["Notes"] = 'Notes';
DataDictionaryEN["FixedAssetsDetails"] = 'Fixed Assets Details';
DataDictionaryEN["FixedAssets"] = 'Fixed Assets';
DataDictionaryEN["TypeNameHeader"] = " Type Name : ";
DataDictionaryEN["SerialNumberHeader"] = " Serial Number : ";


////////////////////////////////////////////////////////////////////////////////
///////////////////////////////Reports/////////////////////////////////////////
DataDictionaryEN["CompanyListReport"] = "Full Customers List (Companies)";
DataDictionaryEN["PersonName"] = " English Person Name";
DataDictionaryEN["PersonNameAr"] = "Arabic Person Name"
DataDictionaryEN["PersonListReport"] = "Full Customers List (Persons)";
DataDictionaryEN["CompanyListReport"] = "Full Customers List (Companies)";
DataDictionaryEN["PersonName"] = " English Person Name";
DataDictionaryEN["PersonNameAr"] = "Arabic Person Name"
DataDictionaryEN["PersonListReport"] = "Full Customers List (Persons)";
DataDictionaryEN["TaskListReport"] = "Schedule Task Report";

DataDictionaryEN['CompanyNameEnglish'] = 'Company Name ';
DataDictionaryEN["InterestedIn"] = "Interested In ";
DataDictionaryEN["ContactType"] = "Contact Type";
DataDictionaryEN["Anniversary"] = "Anniversary";
DataDictionaryEN["Nationality"] = "Nationality";
DataDictionaryEN["NotifyforBirthday"] = "Notify for Birthday";
DataDictionaryEN["NotifyForAnniversary"] = "Notify For Anniversary";
DataDictionaryEN["FixedAssetsReport"] = 'Fixed Assets List Report';
DataDictionaryEN["CreatedDateFrom"] = "Created Date From";
DataDictionaryEN["CreatedDateTo"] = "Created Date To";


DataDictionaryEN["Customer"] = 'Customer';
DataDictionaryEN["false"] = 'false'
DataDictionaryEN["true"] = 'true';
DataDictionaryEN["Http"] = 'http://'


////********************************************************************************************////
////***************************Inventory Control Definitions************************************////
////********************************************************************************************////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// IcUnitOfMeasures English Dictionary////////////////////////////////////////////////
DataDictionaryEN["IcUnitOfMeasures"] = 'Unit Of Measures';
DataDictionaryEN["AddIcUnitOfMeasures"] = 'Add  Unit Of Measure';
DataDictionaryEN['IcUnitOfMeasuresDetails'] = 'Unit Of Measures Details';
DataDictionaryEN['UnitOfMeasure'] = 'Unit Of Measure';
DataDictionaryEN['UnitOfMeasure'] = 'Unit Of Measure';
DataDictionaryEN['DefaultConversionFactor'] = 'Conversion Factor';
DataDictionaryEN["AlreadyExist"] = 'Already Exist';
DataDictionaryEN["UnitOfMeasure"] = "Unit Of Measure";
DataDictionaryEN["HUnitOfMeasure"] = "Unit Of Measure :  ";
DataDictionaryEN["HConversionFactor"] = "Conversion Factor :  ";

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// IcWieghtUnitOfMeasures English Dictionary////////////////////////////////////////////////
DataDictionaryEN["IcWieghtUnitOfMeasures"] = 'Weight Unit Of Measures';
DataDictionaryEN["AddIcWieghtUnitOfMeasures"] = 'Add Weight Unit Of Measures';
DataDictionaryEN['IcWieghtUnitOfMeasuresDetails'] = 'Weight Unit Of Measures Details';
DataDictionaryEN['WeightUnitOfMeasure'] = 'Weight Unit Of Measure';
DataDictionaryEN['WeightUnitOfMeasure'] = 'Weight Unit Of Measure';
DataDictionaryEN['WeightUnitOfMeasureDescription'] = ' Description';
DataDictionaryEN['WeightConversionFactor'] = ' Conversion Factor';
DataDictionaryEN['DefaultUnitOfMeasure'] = 'Default Unit Of Measure';
DataDictionaryEN['HDefaultUnitOfMeasure'] = 'Default Unit Of Measure : ';
DataDictionaryEN["AlreadyExist"] = ' Already Exist ';
//////List Header English Dictionary JS
DataDictionaryEN['WeightUnitOfMeasure'] = 'Weight Unit Of Measure';
DataDictionaryEN['DefaultUnitOfMeasureValueTitel'] = "Equals";
DataDictionaryEN['ICSetup'] = "Inventory Setup";

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// IcLocation English Dictionary////////////////////////////////////////////////
DataDictionaryEN["IcLocation"] = 'Location';
DataDictionaryEN["AddIcLocation"] = 'Add Location';
DataDictionaryEN['IcLocationDetails'] = 'Location Details';
DataDictionaryEN['Location'] = 'Location';
DataDictionaryEN['LocationCode'] = 'Location Code';
DataDictionaryEN['LocationName'] = 'Location Name';
DataDictionaryEN['LocationAddress'] = 'Address';
DataDictionaryEN['LocationCountry'] = 'Country';
DataDictionaryEN['LocationCity'] = 'City';
DataDictionaryEN['LocationPhone'] = 'Phone';
DataDictionaryEN['LocationFax'] = 'Fax';
DataDictionaryEN['LocationEmail'] = 'Email';
DataDictionaryEN['LocationType'] = 'Type';
DataDictionaryEN['LocationContactName'] = 'Contact Name';
DataDictionaryEN['LocationContactPhone'] = 'Contact Phone';
DataDictionaryEN['LocationContactFax'] = 'Contact Fax';
DataDictionaryEN['LocationContactEmail'] = 'Contact Email';
DataDictionaryEN['LocationStatus'] = 'Status';
DataDictionaryEN["AlreadyExist"] = ' Already Exist ';
DataDictionaryEN["Contact"] = ' Contact ';
DataDictionaryEN["HLocationCode"] = 'Location Code : ';
DataDictionaryEN['HLocationName'] = 'Location Name : ';
DataDictionaryEN['HLocationType'] = 'Type : ';
DataDictionaryEN['Integration'] = 'Integration';
DataDictionaryEN['LocationIntegration'] = 'Override GL Segments';
DataDictionaryEN['OverrideGLSegments'] = 'Override GL Segments';

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// IcWarrantyInfo English Dictionary////////////////////////////////////////////////
DataDictionaryEN["IcWarrantyInfo"] = ' Warranty';
DataDictionaryEN["AddIcWarrantyInfo"] = 'Add Warranty';
DataDictionaryEN['IcWarrantyInfoDetails'] = 'Warranty Details';
DataDictionaryEN['WarrantyInfo'] = 'Warranty ';
DataDictionaryEN['WarrantyCode'] = 'Warranty Code';
DataDictionaryEN['WarrantyDescription'] = 'Warranty Description';
DataDictionaryEN["AlreadyExist"] = ' Already Exist ';
DataDictionaryEN['IsLifeTime'] = 'Is Life Time';
DataDictionaryEN['WarrantyDays'] = 'Warranty Days';
DataDictionaryEN['EffictiveDays'] = 'Effictive Days';
DataDictionaryEN['HWarrantyCode'] = 'Warranty Code : ';
DataDictionaryEN['HWarrantyDays'] = 'Warranty Days : ';
DataDictionaryEN['HIsLifeTime'] = 'Is Life Time : ';
//////List Header English Dictionary JS


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// IcSegment English Dictionary////////////////////////////////////////////////
DataDictionaryEN["Segmant"] = "Segment";
DataDictionaryEN["SegmentNumber"] = "Segment #";
DataDictionaryEN["SegmentName"] = "Segment Name";
DataDictionaryEN["SegmentLength"] = "Segment Length";
DataDictionaryEN["SegmentCode"] = "Segment Code";
DataDictionaryEN['IcMaximumNumberOfSegmentsAllowed'] = 'Maximum number of segments allowed is '
DataDictionaryEN['IcSegmentEditingDisabled'] = 'Editing disabled, All Segments are linked to other module';
DataDictionaryEN['IcSegmentNameMustBeUnique'] = 'Segment name must be unique';
DataDictionaryEN['IcSegmentLengthMustBe'] = 'length must be ';
DataDictionaryEN['#'] = '#';
DataDictionaryEN['HasReference'] = 'Has Reference';
DataDictionaryEN['SegmentNameH'] = 'Segment Name: ';
DataDictionaryEN['SegmentCodeH'] = 'Segment Code: ';
DataDictionaryEN['IcSegmentReferenceSegmentVldMsg'] = 'Cannot create reference to the same Segment';

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// IcSegmentCode English Dictionary////////////////////////////////////////////////
DataDictionaryEN["SegmentCodeDescription"] = "Segment Code Description";
DataDictionaryEN["SegmentCode"] = "Segment Code";
DataDictionaryEN['SegmentName'] = 'Segment Name';
DataDictionaryEN['SegmentCodeDetails'] = 'Segment Code Details';
DataDictionaryEN['AddNewSegmentCode'] = 'Add New Segment Code';
DataDictionaryEN['ReferenceSegment'] = 'Reference Segment';
DataDictionaryEN['ReferenceSegmentCode'] = 'Reference Segment Code';
DataDictionaryEN['ReferenceSegmentCodeDescription'] = 'Ref. Segment Code Desc.';
DataDictionaryEN['IcSegmentCode'] = 'Segment Code : ';
DataDictionaryEN['IcSegmentCodeDescription'] = 'Description : ';

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// ItemStructure English Dictionary////////////////////////////////////////////////
DataDictionaryEN["ItemStructure"] = 'Item Structure';
DataDictionaryEN["AddItemStructure"] = 'Add Item Structure';
DataDictionaryEN['ItemStructureDetails'] = 'Structure Details';
DataDictionaryEN['ItemStucture'] = 'Item Stucture';
DataDictionaryEN['ItemStructureCode'] = 'Structure Code';
DataDictionaryEN['ItemStructureDescription'] = 'Description';
DataDictionaryEN['ItemStructureDelimiter'] = 'Delimiter';

DataDictionaryEN["AlreadyExist"] = ' Already Exist ';
//////List Header English Dictionary JS
DataDictionaryEN['ItemStructureCode'] = 'Structure Code';
DataDictionaryEN["HItemStructureCode"] = 'Structure Code : ';
DataDictionaryEN["HItemStructureDescription"] = 'Description : ';
DataDictionaryEN["HItemStructureDelimiter"] = 'Delimiter : ';
DataDictionaryEN["ItemManagement"] = "Item Management";
DataDictionaryEN['IcItemStructureLengthExceeds'] = 'Structure Length Exceeds ';

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// IcItemCard English Dictionary////////////////////////////////////////////////
DataDictionaryEN["IcItemCard"] = 'Item'; //'Item Card';
DataDictionaryEN["AddIcItemCard"] = 'Add Item'; //'Add Item Card';
DataDictionaryEN['IcItemCardDetails'] = 'Item Details'; //'Item Card Details';
DataDictionaryEN["ItemCard"] = "Item"; //"Item Card";
DataDictionaryEN['StructureCode'] = 'Structure Code';
DataDictionaryEN['ItemNumber'] = 'Item Number';
DataDictionaryEN['ItemDescription'] = 'Item Description';
DataDictionaryEN['CoastingMethod'] = 'Costing Method';
DataDictionaryEN['DefaultPriceList'] = 'Default Price List';
DataDictionaryEN['DefaultPickingSequence'] = 'Default Picking Sequence';
DataDictionaryEN['UnitWeight'] = 'Unit Weight';
DataDictionaryEN['WeightUnitOfMeasure'] = 'Weight Unit Of Measure';
DataDictionaryEN['AlternateItem'] = 'Alternate Item';
DataDictionaryEN['AdditionalItemInformation'] = 'Additional Item Information';
DataDictionaryEN['IsActive'] = 'Is Active';
DataDictionaryEN['StockItem'] = 'Stock Item';
DataDictionaryEN['SerialNumber'] = 'Serial Number';
DataDictionaryEN['LotNumber'] = 'Lot Number';
DataDictionaryEN['Sellable'] = 'Sellable';
DataDictionaryEN['KittingItem'] = 'Kitting Item';
DataDictionaryEN['ItemCardTax'] = 'Item Tax'; //'Item Card Tax';
DataDictionaryEN['ItemCardLocation'] = 'Item Location'; //'Item Card Location';

DataDictionaryEN["AlreadyExist"] = ' Already Exist ';
//////List Header English Dictionary JS
DataDictionaryEN['HItemNumber'] = 'Item Number : ';
DataDictionaryEN['HItemDescription'] = 'Item Description : ';
DataDictionaryEN['HCoastingMethod'] = 'Costing Method : ';
DataDictionaryEN['HDefaultPriceList'] = 'Default Price List : ';
DataDictionaryEN['StockingUnitOFMeasure'] = 'Stocking Unit Of Measure';
DataDictionaryEN["ItemUnitOFMeasure"] = "Item Unit Of Measures";
DataDictionaryEN["ItemTax"] = "Item Taxes";
DataDictionaryEN["TaxAuthority"] = "Tax Authority";
DataDictionaryEN["TaxAuthorityDesc"] = "Tax Authority Description";
DataDictionaryEN["SalesTaxClass"] = "Sales Tax Class";
DataDictionaryEN["PurchaseTaxClass"] = "Purchase Tax Class";
DataDictionaryEN["locationCosting"] = "Location And Costing";
DataDictionaryEN["Allowed"] = "Allowed";
DataDictionaryEN["PickingSequence"] = "Picking Sequence";
DataDictionaryEN["CostUnitOfMeasure"] = "Cost Unit Of Measure";
DataDictionaryEN["StandardCost"] = "Standard Cost";
DataDictionaryEN["MostRecentCost"] = "Most Recent Cost";
DataDictionaryEN["LastUnitCost"] = "Last Unit Cost";
DataDictionaryEN["AllLocations"] = "Automatically Include All Locations ";
DataDictionaryEN["btnCreate"] = "Create";
DataDictionaryEN["ItemBarCode"] = "Bar Code";
DataDictionaryEN["ItemSku"] = "Sku";
DataDictionaryEN["TaxAuthorityDescription"] = "Tax Authority Description";
DataDictionaryEN["ItemImage"] = "Item Image";
DataDictionaryEN["ValidateQuantityOnBoQ"] = "Validate Quantity on BoQ";
DataDictionaryEN["ConstructItemNumber"] = "Create Number";
DataDictionaryEN['LengthMustBe'] = 'Length must be ';
DataDictionaryEN['HNumber'] = 'Number: ';
DataDictionaryEN['IcItemNumberMaxLengthAllowedIs'] = 'Max item number length allowed is ';

DataDictionaryEN['HasVarianceSegments'] = 'Has Variance Segments';
DataDictionaryEN['NoOfVarianceSegments'] = 'No. of Variance Segments';

DataDictionaryEN['ItemNumberFormat'] = 'Item Number Format ';
DataDictionaryEN['IcUOMConversionFactorVld'] = 'Conversion Factor for the stocking UOM has to be 1.';
DataDictionaryEN['IcUOMRequiredVld'] = 'You have to select a stocking UOM (with conversion factor=1).';
DataDictionaryEN['IcItemSerialsCancelMsg'] = 'Serials entered will be lost! Are you sure you want to Cancel?';

DataDictionaryEN['ManufacturingCompany'] = 'Manufacturing Company';
DataDictionaryEN['Capacity'] = 'Capacity';

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// IcPriceList English Dictionary////////////////////////////////////////////////
DataDictionaryEN["IcPriceList"] = 'Price List';
DataDictionaryEN["AddIcPriceList"] = 'Add Price List';
DataDictionaryEN['IcPriceListDetails'] = 'Price List Details';
DataDictionaryEN['PriceList'] = 'Price List';
DataDictionaryEN['PriceListCode'] = 'Price List Code';
DataDictionaryEN['PriceListDescription'] = 'Price List Description';
DataDictionaryEN['PriceBy'] = 'Price By';
DataDictionaryEN['PriceDecimals'] = 'Price Decimals';
DataDictionaryEN['RoundingMethod'] = 'Rounding Method';
DataDictionaryEN['SellingPriceBasedOn'] = 'Selling Price Based On';
DataDictionaryEN['DiscountOnPriceBy'] = 'Discount On Price By';
DataDictionaryEN['PricingDeterminedBy'] = 'Pricing Determined By';
DataDictionaryEN["AlreadyExist"] = ' Already Exist ';
//////List Header English Dictionary JS
DataDictionaryEN['PriceListCode'] = 'Price List Code';

////Tab English Dictionary JS
DataDictionaryEN["HPriceListCode"] = "Price List Code : ";
DataDictionaryEN["HPriceListDescription"] = "Price List Description : ";
DataDictionaryEN["HPriceBy"] = "Price By : ";
DataDictionaryEN["Taxes"] = "Taxes";
DataDictionaryEN["CustomerTaxClass"] = "Customer Tax Class";

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// IcPriceListGroup English Dictionary////////////////////////////////////////////////
DataDictionaryEN['IcPriceListGroup'] = 'Price List Group';
DataDictionaryEN['PriceListGroup'] = 'Price List Group';
DataDictionaryEN['HPriceListGroup'] = 'Price List Group: ';
DataDictionaryEN['HPriceList'] = 'Price List: ';
DataDictionaryEN['HItemCard'] = 'Item: '; //'Item Card: ';
DataDictionaryEN['HCurrency'] = 'Currency: ';
DataDictionaryEN['PriceListGroupId'] = 'Id';
DataDictionaryEN['AddIcPriceListGroup'] = 'Add Price List Group';
DataDictionaryEN['IcPriceListGroupDetails'] = 'Price List Group Details';
DataDictionaryEN['BasePriceType'] = 'Base Price Type';
DataDictionaryEN['BasePrice'] = 'Base Price';
DataDictionaryEN['PricingUnit'] = 'Pricing Unit';
DataDictionaryEN['SalePriceType'] = 'Sale Price Type';
DataDictionaryEN['SalePrice'] = 'Sale Price';
DataDictionaryEN['SaleUnit'] = 'Sale Unit';
DataDictionaryEN['SaleStarts'] = 'Sale Starts';
DataDictionaryEN['SaleEnds'] = 'Sale Ends';
DataDictionaryEN['MarkupCost'] = 'Markup Cost';
DataDictionaryEN['MarkupWeightUnit'] = 'Markup Weight Unit';
DataDictionaryEN['MarkupFactor'] = 'Markup Factor';
DataDictionaryEN['PriceListStarts'] = 'Price List Starts';
DataDictionaryEN['PriceListEnds'] = 'Price List Ends';
DataDictionaryEN['PriceListGroupTax'] = 'Taxes';
DataDictionaryEN['PriceListGroupDiscounts'] = 'Discounts';
DataDictionaryEN['Items'] = 'Items';
DataDictionaryEN['PriceListGroupItems'] = 'Price List Group Items';
DataDictionaryEN['Margin'] = 'Margin';
DataDictionaryEN['MarginLabel'] = 'Margin (%)';
DataDictionaryEN['FixedPrice'] = 'Fixed Price';
DataDictionaryEN['CostBaseType'] = 'Cost Base Type';
DataDictionaryEN['ItemCards'] = 'Item(s)';

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// IcItemLocationCosting English Dictionary////////////////////////////////////////////////
DataDictionaryEN["IcItemLocationCostingDetails"] = 'Location & Costing';
DataDictionaryEN["HItemLocationCosting"] = 'Location & Costing Id: ';
DataDictionaryEN["HLocation"] = 'Location: ';
DataDictionaryEN["HItemCard"] = 'Item: '; //'Item Card: ';
DataDictionaryEN["AddIcItemLocationCostingInfo"] = 'Add Item Location And Costing';
DataDictionaryEN["ItemLocationCostingId"] = "Id";
DataDictionaryEN["ItemLocationCosting"] = "Item Location & Costing";
DataDictionaryEN['InUse'] = 'In Use';
DataDictionaryEN['AverageCost'] = 'Average Cost';
DataDictionaryEN['QuantityOnHand'] = 'Quantity On Hand';
DataDictionaryEN['QuantityOnPurchaseOrder'] = 'Quantity On Purchase Order';
DataDictionaryEN['QuantityOnSellOrder'] = 'Quantity On Sell Order';
DataDictionaryEN['QuantityAvailableToShip'] = 'Quantity Available To Ship';
DataDictionaryEN['QuantityCommitted'] = 'Quantity Committed';
DataDictionaryEN['IcItemLocationCosting'] = 'Item Location Costing';
DataDictionaryEN['QtyOnHand'] = 'Qty On Hand';

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// IcAccountSet English Dictionary////////////////////////////////////////////////
DataDictionaryEN["IcAccountSet"] = 'Account Set';
DataDictionaryEN["AddIcAccountSet"] = 'Add Account Set';
DataDictionaryEN['IcAccountSetDetails'] = 'Account Set Details';
DataDictionaryEN['HAccountSetCode'] = 'Account Set Code: ';
DataDictionaryEN['HDescription'] = 'Description: ';
DataDictionaryEN['AccountSet'] = 'Account Set';
DataDictionaryEN['AccountSetCode'] = 'Account Set Code';
DataDictionaryEN['Description'] = 'Description';
DataDictionaryEN['CostingMethod'] = 'Costing Method';
DataDictionaryEN['InventoryControlAccount'] = 'Inventory Control Account';
DataDictionaryEN['PayableClearingAccount'] = 'Payable Clearing Account';
DataDictionaryEN['AdjustmentWriteOffAccount'] = 'Adjustment Write Off Account';
//DataDictionaryEN['NonStockClearingAccount'] = 'Non Stock Clearing Account';
DataDictionaryEN['NonStockClearingAccount'] = 'Additional Cost Account';
DataDictionaryEN['TransferClearingAccount'] = 'Transfer Clearing Account';
DataDictionaryEN['ShipmentClearingAccount'] = 'Shipment Clearing Account';

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// IcCategory English Dictionary////////////////////////////////////////////////
DataDictionaryEN["IcCategory"] = 'Category';
DataDictionaryEN["CategoryCode"] = 'Category Code';
DataDictionaryEN['IcCategoryDetails'] = "Category Details";
DataDictionaryEN["HCategoryCode"] = 'Category Code: ';
DataDictionaryEN["AddIcCategory"] = "Add Category";
DataDictionaryEN["SalesAccount"] = "Sales";
DataDictionaryEN["ReturnsAccount"] = "Returns";
DataDictionaryEN["CostOfGoodsSoldAccount"] = "Cost Of Goods Sold";
DataDictionaryEN["CostVarianceAccount"] = "Cost Variance";
DataDictionaryEN["DamagedGoodsAccount"] = "Damaged Goods";
DataDictionaryEN["InternalUsageAccount"] = "Internal Usage";
DataDictionaryEN["UOM"] = "UOM";
DataDictionaryEN["Discounts"] = "Discounts";

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// IcTransaction English Dictionary////////////////////////////////////////////////
DataDictionaryEN['ICTransactions'] = "Transactions";
DataDictionaryEN['IcReceipt'] = "Receipt";
DataDictionaryEN['Post'] = 'Post';
DataDictionaryEN['SaveAndPost'] = 'Save & Post';
DataDictionaryEN['postedsuccessfully'] = 'Posted Successfully';
DataDictionaryEN['FaildToPost'] = 'Failed To Post';
DataDictionaryEN['IcShipment'] = 'Shipment';
DataDictionaryEN['IcAdjustment'] = 'Adjustment';
DataDictionaryEN['IcTransfer'] = 'Transfer';

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// IcReceipt English Dictionary////////////////////////////////////////////////
DataDictionaryEN['AddIcReceipt'] = 'Add Receipt';
DataDictionaryEN['Reference'] = 'Reference';
DataDictionaryEN['PostingDate'] = 'Posting Date';
DataDictionaryEN['ReceiptDate'] = 'Receipt Date';
DataDictionaryEN['YearPeriod'] = 'Year/Period';
DataDictionaryEN['AdditionalCost'] = 'Additional Cost';
DataDictionaryEN['TotalCost'] = 'Total Cost';
DataDictionaryEN['TotalExtendedCost'] = 'Total Extended Cost';
DataDictionaryEN['IcReceiptDetails'] = 'Receipt Details';
DataDictionaryEN['ReceiptNumber'] = 'Receipt Number';
DataDictionaryEN['HReceiptNumber'] = 'Receipt Number: ';
DataDictionaryEN['HReceiptDate'] = 'Receipt Date: ';
DataDictionaryEN['Posted'] = 'Posted';
DataDictionaryEN['NotPosted'] = 'Pending';
DataDictionaryEN['AdditionalCostAction'] = 'Additional Cost Action';
DataDictionaryEN['TotalReturnCost'] = 'Total Return Cost';
DataDictionaryEN['QuantityReceived'] = 'Quantity Received';
DataDictionaryEN['UnitCost'] = 'Unit Cost';
DataDictionaryEN['ExtendedCost'] = 'Extended Cost';
DataDictionaryEN['Comments'] = 'Comments';
DataDictionaryEN['QuantityReturned'] = 'Quantity Returned';
DataDictionaryEN['ReturnCost'] = 'Return Cost';
DataDictionaryEN['ItemCardDescription'] = 'Item Description';
DataDictionaryEN['AdjustedCost'] = 'Adjusted Cost';
DataDictionaryEN['TotalAdjusted'] = 'Total Adjusted';
DataDictionaryEN['PONumber'] = 'PO Number';
DataDictionaryEN['AutomaticProration'] = 'Automatic Additional Cost Proration';
DataDictionaryEN['ManualProration'] = 'Manual Proration';
DataDictionaryEN['IcAccountsDoesNotExist'] = 'Following GL Account(s) does not exist: ';
DataDictionaryEN['CreateAutomaticAPInvoice'] = 'Create Automatic AP Invoice';

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// IcShipment English Dictionary////////////////////////////////////////////////
DataDictionaryEN['ShipmentNumber'] = 'Shipment Number';
DataDictionaryEN['ShipmentDate'] = 'Shipment Date';
DataDictionaryEN['HShipmentNumber'] = 'Shipment Number: ';
DataDictionaryEN['HShipmentDate'] = 'Shipment Date: ';
DataDictionaryEN['AddIcShipment'] = 'Add Shipment';
DataDictionaryEN['ShipmentType'] = 'Entry Type';
DataDictionaryEN['ExtendedPrice'] = 'Extended Price';
DataDictionaryEN['IcShipmentDetails'] = 'Shipment Details';
DataDictionaryEN['CreateAutomaticARInvoice'] = 'Create Automatic AR Invoice';
DataDictionaryEN['DiscountPercent'] = 'Discount Percent';

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// IcShipment English Dictionary////////////////////////////////////////////////
DataDictionaryEN['AdjustmentNumber'] = 'Adjustment Number';
DataDictionaryEN['AdjustmentDate'] = 'Adjustment Date';
DataDictionaryEN['HAdjustmentNumber'] = 'Adjustment Number: ';
DataDictionaryEN['HAdjustmentDate'] = 'Adjustment Date: ';
DataDictionaryEN['AddIcAdjustment'] = 'Add Adjustment';
DataDictionaryEN['IcAdjustmentDetails'] = 'Adjustment Details';
DataDictionaryEN['AdjustmentType'] = 'Adjustment Type';
DataDictionaryEN['BucketType'] = 'Bucket Type';
DataDictionaryEN['DocumentNo'] = 'Document No.';
DataDictionaryEN['DocumentLine'] = 'Document Line';
DataDictionaryEN['CostDate'] = 'Cost Date';
DataDictionaryEN['CostAdjutment'] = 'Cost Adjutment';
DataDictionaryEN['CostUOM'] = 'Cost UOM';
DataDictionaryEN['AdjustmentAccount'] = 'Adjustment Account';
DataDictionaryEN['AdjustmentAccountDescription'] = 'Account Description';

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// IcTransfer English Dictionary////////////////////////////////////////////////
DataDictionaryEN['TransferNumber'] = 'Transfer Number';
DataDictionaryEN['TransferDate'] = 'Transfer Date';
DataDictionaryEN['HTransferNumber'] = 'Transfer Number: ';
DataDictionaryEN['HTransferDate'] = 'Transfer Date: ';
DataDictionaryEN['AddIcTransfer'] = 'Add Transfer';
DataDictionaryEN['IcTransferDetails'] = 'Transfer Details';
DataDictionaryEN['FromLocation'] = 'From Location';
DataDictionaryEN['GitLocation'] = 'GIT Location';
DataDictionaryEN['ToLocation'] = 'To Location';
DataDictionaryEN['RequestedQuantity'] = 'Requested Quantity';
DataDictionaryEN['OutstandingQuantity'] = 'Outstanding Quantity';
DataDictionaryEN['TransferQuantity'] = 'Transfer Quantity';
DataDictionaryEN['TransferUnitOfMeasure'] = 'Transfer Unit Of Measure';
DataDictionaryEN['TransferredToDate'] = 'Transferred To Date';
DataDictionaryEN['ManualProration'] = 'Manual Proration';
DataDictionaryEN['ReceivedToDate'] = 'Received To Date';
DataDictionaryEN['UnitWeight'] = 'Unit Weight';
DataDictionaryEN['ExtendedWeight'] = 'Extended Weight';
DataDictionaryEN['WeightUnitOfMeasure'] = 'Weight Unit Of Measure';
DataDictionaryEN['WeightConversionFactor'] = 'Weight Conversion Factor';
DataDictionaryEN['TransferType'] = 'Transfer Type';
DataDictionaryEN['SelectedTransfer'] = 'Transfer';
DataDictionaryEN['ExpectedArrivalDate'] = 'Expected Arrival Date';
DataDictionaryEN['ProrationMethod'] = 'Proration Method';

///////////////////////////////////////////////////////////////////////////////
DataDictionaryEN['PeriodicProcessing'] = 'Periodic Processing';
DataDictionaryEN['IcDayEndProcessing'] = 'Day End Processing';

///////////////////////////////////////////////////////////////////////////////
DataDictionaryEN['IcItemCardSerialsAdd'] = 'Add Item Serials';
DataDictionaryEN['NextSerialNumber'] = 'Serial Number';
DataDictionaryEN['ItemSerialNumber'] = 'Item Serial Number';
DataDictionaryEN['Serials'] = 'Serials';
DataDictionaryEN['ItemSerials'] = 'Item Serials';
DataDictionaryEN['InsertSerialsManually'] = 'Insert Serials Manually';
DataDictionaryEN['IcItemCardSerialsSelect'] = 'Select Item Serials';
DataDictionaryEN['InsertLotsManually'] = 'Insert Lots Manually';

//////////////////////////////Validation Msgs//////////////////////////////
DataDictionaryEN['SaveError'] = 'Save Error';
DataDictionaryEN['ValidationError'] = 'Validation Error';
DataDictionaryEN['InvalidOperation'] = 'Invalid Operation';
DataDictionaryEN['InvalidData'] = 'Invalid Data';
DataDictionaryEN['IcSerialQuantityMismatchError'] = 'Quantity doesn\'t match the selected serials in the followng Receipt line(s) :<br/>';
DataDictionaryEN['IcLotQuantityMismatchError'] = 'Quantity doesn\'t match the selected lots in the following Receipt line(s) :<br/>';
DataDictionaryEN['IcReceiptNumberAlreadyExists'] = 'Receipt Number already exists.';
DataDictionaryEN['IcSerialsQuantityRequiredValidationMsg'] = 'To Pick Serials you must set Quantity first.';
DataDictionaryEN['IcSerialsQuantityItemLocationRequiredValidationMsg'] = 'To Pick Serials you must set Item, Location, Quantity and UOM first.';
DataDictionaryEN['IcReceiptAdditionalCostManualProrationError'] = 'Sum of Manual Proration values has to be equal to total receipt additional cost.';
DataDictionaryEN['IcReceiptQuantityReturnedValidationMsg'] = 'Invalid Quantity, Quantity Returned cannot be greater than Quantity received.';
DataDictionaryEN['IcNegativeInventoryLevelsNotAllowedErrorMsg'] = 'Negative Inventory Levels not allowed, Please update the following Item-Location combination(s) :<br/>';
DataDictionaryEN['IcNegativeInventoryLevelsWarningMsg'] = 'This operation will result on Negative Inventory Levels as a result of the following combination(s):<br/>';
DataDictionaryEN['DoYouWantToProceed'] = 'Do you want to proceed?';
DataDictionaryEN['IcSerialsNotAvailableErrorMessage'] = 'Following Serial(s) are not available for this transaction: <br/>';
DataDictionaryEN['IcSerialsLineNoLabel'] = 'Line No.: ';
DataDictionaryEN['IcSerialsSerialLitsLabel'] = ' , Serial(s): ';
DataDictionaryEN['IcTransferTypeRequired'] = 'Select Transaction Type First.';
DataDictionaryEN['IcTransferNumberAlreadyExist'] = 'Transfer Number already exists.';
DataDictionaryEN['InvalidInput'] = 'Invalid Input';
DataDictionaryEN['IcTransferLockedForDEP'] = 'Selected Transfer is Locked until Day End Processing is performed';
DataDictionaryEN['IcTransferCompletedValidationMsg'] = 'Selected Transfer is already completed or all quantities have been transferred';
DataDictionaryEN['IcAdjustmentNumberAlreadyExist'] = 'Adjustment Number already exists.';
DataDictionaryEN['InvalidValue'] = 'Invalid Value';
DataDictionaryEN['IcShipmentNumberAlreadyExist'] = 'Shipment Number already exists.';
DataDictionaryEN['NotSellable'] = 'Not Sellable';
DataDictionaryEN['NotActive'] = 'Not Active';
DataDictionaryEN['IcTransferSelectAdjustmentTypeValidationMsg'] = 'You have to select Adjustment Type first.';
DataDictionaryEN['IcSerializedItemCostingMethodValidationMsg'] = 'FIFO/LIFO Item cannot be Serialized';
DataDictionaryEN['IcNegativeInventoryLevelsNotAllowedForBucketErrorMsg'] = 'This operation will result on Negative Inventory Levels as a result of the following combination(s):<br/>(Negative Inventory Levels not allowed for FIFO/LIFO Items, Please update their values)<br/>';
DataDictionaryEN['AreYouSureYouWantToCancel'] = 'Are you sure you want to cancel?';
DataDictionaryEN['PriceListCurrencyCombinationAlreadyExist'] = 'PriceList & Currency Combination Already Exist';
DataDictionaryEN['MaximumNumberOfDiscountsIs'] = 'Maximum number of Discounts is ';
DataDictionaryEN['IcDayEndProcessingUnsuccessful'] = 'Day End Processing Unsuccessful';
DataDictionaryEN['IcDayEndProcessingCompletedSuccessfully'] = 'Day End Processing Completed Successfully';
DataDictionaryEN['IcDayEndProcessingCompletedNoTransactions'] = 'Day End Processing Completed. No Transactions found.';
DataDictionaryEN['DayEndProcessing'] = 'Day End Processing';
DataDictionaryEN['DayEndProcessingConfirmMsg'] = 'Are you sure you want to run Day End Processing?';
DataDictionaryEN['IcSerialNumberAlreadyExists'] = 'Serial Number already exists.';
DataDictionaryEN['IcMaximumNumberOfSerialsVldMsg'] = 'Maximum number of Serials is ';
DataDictionaryEN['IcSerialNumberAlreadyInserted'] = 'Serial Number has already been inserted.'
DataDictionaryEN['IcSerialNumberNotAvailable'] = 'Serial Number doesn\'t exist/Not available.';
DataDictionaryEN['IcItemLocationCombinationAlreadyExist'] = 'Item and Location Combination Already Exist';
DataDictionaryEN['IcBaseUOMConversionFactorVld'] = 'Base UOM has to have conversion factor = 1';
DataDictionaryEN['IcDocumentNumbersPrefixRequired'] = 'Prefix is Required';
DataDictionaryEN['IcDocumentNumbersPrefixMinLength'] = 'Prefix Min Length is 3';
DataDictionaryEN['IcDocumentNumbersPrefixMaxLength'] = 'Prefix Max Length is 3';
DataDictionaryEN['IcDocumentNumbersLengthRequired'] = 'Length is Required';
DataDictionaryEN['IcDocumentNumbersNextNumberRequired'] = 'Next Number is Required';
DataDictionaryEN['IcPriceListDiscountMaxNumberVld'] = 'Maximum number of Discounts is ';
DataDictionaryEN['IcDiscountMaxValueVld'] = 'Max value is ';
DataDictionaryEN['Item'] = 'Item';
DataDictionaryEN['Location'] = 'Location';
DataDictionaryEN['LineNo'] = 'Line No.';
DataDictionaryEN['AreYouSureYouWantToDelete'] = 'Are you sure you want to delete?';
DataDictionaryEN['IcSegmentNameUniqueVld'] = 'Segment Name must be unique';
DataDictionaryEN['IcSegmentLengthVld'] = 'Segment length is ';
DataDictionaryEN['IcSegmentCodeAlreadyExist'] = 'Segment Code Already Exist';
DataDictionaryEN['TooLarge'] = 'Too Large';
DataDictionaryEN['IcAutomaticInvoiceNotCreated'] = 'Automatic Invoice was not created';
DataDictionaryEN['ApInvoiceNumberAlreadyExists'] = 'Invoice Number Already Exists (Reference)';
DataDictionaryEN['vldInvalidFile'] = 'Invlid File , Please Select Valid File !';
DataDictionaryEN['IcLotsQuantityItemLocationRequiredValidationMsg'] = 'To Pick Lots you must set Item, Location, Quantity and UOM first.';
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// Item Number Construction English Dictionary////////////////////////////////////////////////
DataDictionaryEN['ConstructNumberFromSegments'] = 'Construct Number';
DataDictionaryEN['N/A'] = 'N/A';

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// Item Number Construction English Dictionary////////////////////////////////////////////////
DataDictionaryEN['FromItem'] = 'From Item';
DataDictionaryEN['ToItem'] = 'To Item';
DataDictionaryEN['IcQuantityOnHandReport'] = 'Quantity On Hand Report';
DataDictionaryEN['IcSalesReport'] = 'Sales Report';
DataDictionaryEN['FromQuantity'] = 'Quantity From';
DataDictionaryEN['ToQuantity'] = 'Quantity To';
DataDictionaryEN['FromValue'] = 'Value From';
DataDictionaryEN['ToValue'] = 'Value To';
DataDictionaryEN['IcItemDeliveryReport'] = 'Item Delivery Report';
DataDictionaryEN['IcSellingPriceListReport'] = 'Selling Price List Report';
DataDictionaryEN['FromPriceList'] = 'From Price List';
DataDictionaryEN['ToPriceList'] = 'To Price List';
DataDictionaryEN['IcItemSellingPriceReport'] = 'Item Selling Price Report';

////********************************************************************************************////
////************************* End of Inventory Control Definitions******************************////
////********************************************************************************************////


//********************************************************************************************************************************
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////ERP Section///////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////Common Services////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryEN["CommonServices"] = "Common Services";
DataDictionaryEN["TAXSERVICES"] = "TAX SERVICES";
DataDictionaryEN["TaxAuthority"] = "Tax Authority";
DataDictionaryEN["TaxAuthorityCode"] = "Tax Authority Code";
DataDictionaryEN["Description"] = "Description";
DataDictionaryEN["ReportingCurrency"] = "Reporting Currency";
DataDictionaryEN["MaximumTaxAllowable"] = "Maximum Tax Allowable";
DataDictionaryEN["MinimumTaxAllowable"] = "Minimum Tax Allowable";
DataDictionaryEN["AllowTaxInPrice"] = "Allow Tax In Price";
DataDictionaryEN["TaxBase"] = "Tax Base";
DataDictionaryEN["LiabilityAccount"] = "Liability Account";
DataDictionaryEN["TaxRecoverable"] = "Tax Recoverable";
DataDictionaryEN["RecoverableAccount"] = "Recoverable Account";
DataDictionaryEN["RecoverablePercentage"] = "Recoverable Percentage";
DataDictionaryEN["ExpenseSeparately"] = "Expense Separately";
DataDictionaryEN["ExpenseAccount"] = "Expense Account";
DataDictionaryEN["TaxClasses"] = "Tax Classes";
DataDictionaryEN["TaxAuthority"] = "Tax Authority";
DataDictionaryEN["TransactionType"] = "Transaction Type";
DataDictionaryEN["ClassType"] = "Class Type";
DataDictionaryEN["TaxRates"] = "Tax Rates";
DataDictionaryEN["TaxGroups"] = "Tax Groups";
DataDictionaryEN["FiscalCalendar"] = "Fiscal Calendar";
DataDictionaryEN["Currency"] = "Currency";
DataDictionaryEN["CurrencyRate"] = "Currency Rate";
DataDictionaryEN["DateMatched"] = "Date Matched";
DataDictionaryEN["RateOperation"] = "Rate Operation";
DataDictionaryEN["CompanyProfile"] = "Company Profile";
DataDictionaryEN["FunctionalCurrency"] = "Functional Currency";
DataDictionaryEN["CompanyName"] = "Company Name";
DataDictionaryEN["Address"] = "Address";
DataDictionaryEN["City"] = "City";
DataDictionaryEN["Country"] = "Country";
DataDictionaryEN["Phone"] = "Phone";
DataDictionaryEN["FaxNumber"] = "Fax Number";
DataDictionaryEN["RegistrationNumber"] = "Registration Number";
DataDictionaryEN["CompanyLogo"] = "Company Logo";
DataDictionaryEN["VATNumber"] = "VAT Number";
DataDictionaryEN['FISCALCALENDAR'] = 'FISCAL CALENDAR';
DataDictionaryEN['CURRENCIES'] = 'CURRENCIES';
DataDictionaryEN['COMPANYPROFILE'] = 'COMPANY PROFILE';
DataDictionaryEN['REPORTS'] = 'REPORTS';
DataDictionaryEN['CurrencyExchangeRates'] = 'Currency Exchange Rates';
DataDictionaryEN['TRANSACTIONALSETUP'] = 'TRANSACTIONAL SETUP';
DataDictionaryEN['AddTaxAuthority'] = 'Add Tax Authority';
DataDictionaryEN['TaxAuthorityDefinition'] = 'Tax Authority Definition';
DataDictionaryEN['Clone'] = 'Clone';
DataDictionaryEN['TaxAuthorityCode'] = 'Tax Authority Code';
DataDictionaryEN['Accounts'] = 'Accounts';
DataDictionaryEN['TaxProfile'] = 'Tax Profile';
DataDictionaryEN['Header'] = 'Header';
DataDictionaryEN['Details'] = 'Details';
DataDictionaryEN['AddTaxClassHeader'] = 'Add Tax Class Header';
DataDictionaryEN['AddTaxClass'] = 'Add Tax Class';
DataDictionaryEN['Classnumber'] = 'Class Number';
DataDictionaryEN['Exempt'] = 'Exempt';
DataDictionaryEN['Addnewdetails'] = 'Add new details';
DataDictionaryEN['TaxAuthorityDescription'] = 'Tax Authority Description';
DataDictionaryEN['vldMinimumTaxAllowable'] = 'minimum tax allowable must be less than maximum tax allowable';
DataDictionaryEN['vldNoTaxRate'] = 'No tax rate to save';
DataDictionaryEN['AddTaxRate'] = 'Add Tax Rate';
DataDictionaryEN['vldNorowFound'] = 'No row Found';
DataDictionaryEN['vldOneRowFound'] = 'one row found';
DataDictionaryEN['vldNoDetalilsFound'] = 'No Detalils Found';
DataDictionaryEN['TaxGroupCode'] = 'Tax Group Code';
DataDictionaryEN['AddTaxGroup'] = 'Add Tax Group';
DataDictionaryEN['Authority'] = 'Authority';
DataDictionaryEN['IsTaxable'] = 'Is Taxable';
DataDictionaryEN['AuthorityDescription'] = 'Authority Description';
DataDictionaryEN['StartDate'] = 'Start Date';
DataDictionaryEN['EndDate'] = 'End Date';
DataDictionaryEN['Year'] = 'Year';
DataDictionaryEN['Period'] = 'Period';
DataDictionaryEN['Locked'] = 'Locked';
DataDictionaryEN['Period1'] = 'Period1';
DataDictionaryEN['Period2'] = 'Period2';
DataDictionaryEN['Period3'] = 'Period3';
DataDictionaryEN['Period4'] = 'Period4';
DataDictionaryEN['Period5'] = 'Period5';
DataDictionaryEN['Period6'] = 'Period6';
DataDictionaryEN['Period7'] = 'Period7';
DataDictionaryEN['Period8'] = 'Period8';
DataDictionaryEN['Period9'] = 'Period9';
DataDictionaryEN['Period10'] = 'Period10';
DataDictionaryEN['Period11'] = 'Period11';
DataDictionaryEN['Period12'] = 'Period12';
DataDictionaryEN['AddFiscalCalendar'] = 'Add Fiscal Calendar';
DataDictionaryEN['Symbol'] = 'Symbol';
DataDictionaryEN['DecimalPlaces'] = 'Decimal Places';
DataDictionaryEN['AddCurrency'] = 'Add Currency';
DataDictionaryEN['AddCurrencyRate'] = 'Add Currency Rate';
DataDictionaryEN['LockAdjustmentPeriod'] = 'Lock Adjustment Period';

//////////////////////////////////////////////General Ledger////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryEN["GeneralLedger"] = "General Ledger";
DataDictionaryEN["TRANSACTIONALSETUP"] = "TRANSACTIONAL SETUP";
DataDictionaryEN["MASTERDATASETUP"] = "MASTER DATA SETUP";
DataDictionaryEN["MASTERDATADEFINITION"] = "MASTER DATA DEFINITION";
DataDictionaryEN["TRANSACTIONSPROCESSING"] = "TRANSACTIONS PROCESSING";
DataDictionaryEN["PERIODICPROCESSING"] = "PERIODIC PROCESSING";
DataDictionaryEN["Options"] = "Options";
DataDictionaryEN["AccountSegment"] = "Account Segment";
DataDictionaryEN["AccountStructure"] = "Account Structure";
DataDictionaryEN["SegmentDelimiter"] = "Segment Delimiter";
DataDictionaryEN["IsMultiCurrency"] = "Is Multi Currency";
DataDictionaryEN["UseAccountClass"] = "Use Account Class";
DataDictionaryEN["DefaultClosingAccount"] = "Default Closing Account";
DataDictionaryEN["ContractDebitAccount"] = "Contract Debit Account";
DataDictionaryEN["DeferredIncomeAccount"] = "Deferred Income Account";
DataDictionaryEN["EarnedRevenueAccount"] = "Earned Revenue Account";
DataDictionaryEN["Un-EarnedRevenueAccount"] = "Un-Earned Revenue Account";
DataDictionaryEN["Posting"] = "Posting";
DataDictionaryEN["AllowPostingToPreviousYear"] = "Allow Posting To Previous Year";
DataDictionaryEN["AllowProvisionalPosting"] = "Allow Provisional Posting";
DataDictionaryEN["YearsToKeepHistory"] = "Years To Keep History";
DataDictionaryEN["ForceListingOfBatches"] = "Force Listing Of Batches";
DataDictionaryEN["CurrentYear"] = "Current Year";
DataDictionaryEN["OldestYearOfFiscalSet"] = "Oldest Year Of Fiscal Set";
DataDictionaryEN["LastBatchNumber"] = "Last Batch Number";
DataDictionaryEN["PostingSequence"] = "Posting Sequence";
DataDictionaryEN["Segment"] = " Segment";
DataDictionaryEN["SegmentName"] = "Segment Name";
DataDictionaryEN["SegmentLength"] = "Segment Length";
DataDictionaryEN["Description"] = "Description";
DataDictionaryEN["SegmentNumber"] = "SegmentNumber";
DataDictionaryEN["Class"] = "Class";
DataDictionaryEN["ClassName"] = "Class Name";
DataDictionaryEN["ClassLength"] = "Class Length";
DataDictionaryEN["Description"] = "Description";
DataDictionaryEN["ClassNumber"] = "Class Number";
DataDictionaryEN["SegmentCode"] = "Segment Code";
DataDictionaryEN["Code"] = "Code";
DataDictionaryEN["ClassCodes"] = " Class Codes";
DataDictionaryEN["AccountStructure"] = "Account Structure";
DataDictionaryEN["StructureCode"] = "Structure Code";
DataDictionaryEN["SegmentName"] = "Segment Name";
DataDictionaryEN["AccountGroups"] = "Account Groups";
DataDictionaryEN["GroupCode"] = "Group Code";
DataDictionaryEN["SourceCode"] = "Source Code";
DataDictionaryEN["Account"] = "Account";
DataDictionaryEN["NormalBalance"] = "Normal Balance";
DataDictionaryEN["AccountNumber"] = "Account Number";
DataDictionaryEN["AccountType"] = "Account Type";
DataDictionaryEN["Status"] = "Status";
DataDictionaryEN["MultiCurrency"] = "Multi Currency";
DataDictionaryEN["RollUp"] = "Roll Up";
DataDictionaryEN["AccountGroupDescription"] = "Account Group Description";
DataDictionaryEN["AccountDescription"] = "Account Description";
DataDictionaryEN["HasAccountClass"] = "Has Account Class";
DataDictionaryEN["AutoAllocation"] = "Auto Allocation";
DataDictionaryEN["PostIn"] = "Post In";
DataDictionaryEN["AccountClass"] = "Account Class";
DataDictionaryEN["AllocatedAccounts"] = "Allocated Accounts";
DataDictionaryEN["Percent "] = "Percent ";
DataDictionaryEN["AllocationPercent"] = "Allocation Percent";
DataDictionaryEN["Add"] = "Add";
DataDictionaryEN["BatchListing"] = "Batch Listing";
DataDictionaryEN["BatchNumber"] = "Batch Number";
DataDictionaryEN["SourceLedger"] = "Source Ledger";
DataDictionaryEN["No.OfEntries"] = "No. Of Entries";
DataDictionaryEN["TotalDebits"] = "Total Debits";
DataDictionaryEN["TotalCredits"] = "Total Credits";
DataDictionaryEN["ReadyToPost"] = "Ready To Post";
DataDictionaryEN["Status"] = "Status";
DataDictionaryEN["Printed"] = "Printed";
DataDictionaryEN["PostingSequence"] = "Posting Sequence";
DataDictionaryEN["LastEdit"] = "Last Edit";
DataDictionaryEN["JournalEntry"] = "Journal Entry";
DataDictionaryEN["EntryNumber"] = "Entry #";
DataDictionaryEN["Date"] = "Date";
DataDictionaryEN["SourceCode"] = "Source Code";
DataDictionaryEN["EntryDescription"] = "Entry Description";
DataDictionaryEN["EntryTotal"] = "Entry Total";
DataDictionaryEN["SourceCurrency"] = "Source Currency";
DataDictionaryEN["SourceDebit"] = "Source Debit";
DataDictionaryEN["sourcecredit"] = "source credit";
DataDictionaryEN["Reference"] = "Reference";
DataDictionaryEN["RateDate"] = "Rate Date";
DataDictionaryEN["Rate"] = "Rate";
DataDictionaryEN["Funct.Debit"] = "Funct. Debit";
DataDictionaryEN["Funct.Credit"] = "Funct. Credit";
DataDictionaryEN["optionalfields"] = "optional fields";
DataDictionaryEN["PostBatchRange"] = "Post Batch Range";
DataDictionaryEN["FromBatch"] = "From Batch";
DataDictionaryEN["ToBatch"] = "To Batch";
DataDictionaryEN["ExcludeBatches"] = "Exclude Batches";
DataDictionaryEN["CreateNewYear"] = "Create New Year";
DataDictionaryEN["TrialBalance"] = "Trial Balance";
DataDictionaryEN["TransactionListing"] = "Transaction Listing";
DataDictionaryEN['Options'] = 'Options';
DataDictionaryEN['SegmentsCodes'] = 'Segments Codes';
DataDictionaryEN['PostRangesofBatches'] = 'Post Ranges of Batches';
DataDictionaryEN['CRMIntegration'] = 'CRM Integration';
DataDictionaryEN['ForceListingOfBatches'] = 'Force Listing Of Batches';
DataDictionaryEN['vldAddFiscal'] = 'Add fiscal calendar first';
DataDictionaryEN['vldSegmentLength'] = 'Value must between 1 and 15';
DataDictionaryEN['vldSegmentNumberExceeded'] = 'Maximum number of segments 5';
DataDictionaryEN['vldClassNumberExceeded'] = 'Maximum number of class 20';
DataDictionaryEN['Addnewcode'] = 'Add New Code';
DataDictionaryEN['vldAccountStructureDefaultSegment'] = 'Account structure must contain {0} segment';
DataDictionaryEN["AddAccountStructure"] = 'Add Account Structure';
DataDictionaryEN["AddAccountGroup"] = 'Add Account Group';
DataDictionaryEN["AddSourceCode"] = 'Add Source Code';
DataDictionaryEN["AccountGroup"] = "Account Group";
DataDictionaryEN['RollUp'] = 'Roll Up';
DataDictionaryEN['ChildAccounts'] = 'Child Accounts';
DataDictionaryEN['Percent'] = 'Percent';
DataDictionaryEN['Transactions'] = 'Transactions';
DataDictionaryEN['FromDate'] = 'From Date';
DataDictionaryEN['ToDate'] = 'To Date';
DataDictionaryEN['Posted'] = 'Posted';
DataDictionaryEN['NotPosted'] = 'NotPosted';
DataDictionaryEN['Debits'] = 'Debits';
DataDictionaryEN['Credits'] = 'Credits';
DataDictionaryEN['Balance'] = 'Balance';
DataDictionaryEN['AllCurrency'] = 'All Currency';
DataDictionaryEN['SpecifiedCurrency'] = 'Specified Currency';
DataDictionaryEN['TotalPercent'] = 'Total Percent';
DataDictionaryEN['AddAccount'] = 'Add Account';
DataDictionaryEN['vldAllocationPercentError'] = 'The percentages for the allocation accounts must total 100 percent.';
DataDictionaryEN['vldAccountNumberError'] = 'Account Number Format ( {0} )';
DataDictionaryEN['Open'] = 'Open';
DataDictionaryEN['Post'] = 'Post';
DataDictionaryEN['Prov.Post'] = 'Prov.Post';
DataDictionaryEN['Clone'] = 'Clone';
DataDictionaryEN['Reverse'] = 'Reverse';
DataDictionaryEN['NewBatch'] = 'New Batch';
DataDictionaryEN['BatchList'] = 'Batch List';
DataDictionaryEN['vldConfirmPostBatch'] = 'Do you want to post this batch ?';
DataDictionaryEN['vldPostNotReadyBatch'] = 'This Batch is not set to Ready To Post,Do you want to post this Batch ?';
DataDictionaryEN['vldPostNotPrintedBatch'] = 'Fail to post you must print the batch list first ?';
DataDictionaryEN['Deleted'] = 'Deleted';
DataDictionaryEN['PrintedSuccessfully'] = 'Printed successfully';
DataDictionaryEN['Posted'] = 'Posted';
DataDictionaryEN['PostedSuccessfully'] = 'Posted successfully';
DataDictionaryEN['vldReadyToPostEmptyBatch'] = 'You cannot set this batch as Ready To Post. Batch is empty.';
DataDictionaryEN['clonedSuccessfully'] = 'Cloned successfully';
DataDictionaryEN['AddJournalEntry'] = 'Add Journal Entry';
DataDictionaryEN['ReturntoBatchList'] = 'Return To Batch List';
DataDictionaryEN['BatchListNumber'] = 'Batch List Number';
DataDictionaryEN['BatchListDescription'] = 'Batch List Description';
DataDictionaryEN['SourceCredit'] = 'Source Credit';
DataDictionaryEN['Refrence'] = 'Reference';
DataDictionaryEN['Funct.Currency'] = 'Funct. Currency';
DataDictionaryEN['OptionalFields'] = 'Optional Fields';
DataDictionaryEN['BatchListInfo'] = 'Batch List Info';
DataDictionaryEN['AccountClassCodes'] = 'Account Class Codes';
DataDictionaryEN['PostBatchRange'] = 'Post Batch Range';
DataDictionaryEN["vldFromBatchError"] = 'From batch must be less than to batch';
DataDictionaryEN["vldPostBatchRangeError"] = 'Error , selected range doesn\'t contains batch to post';
DataDictionaryEN["YearEndCondition1"] = 'Please create new year in fiscal calendar befor you can proceed.';
DataDictionaryEN["YearEndCondition2"] = 'This process will create and post entries to move balance from income statments accounts to retained earninig accounts .';
DataDictionaryEN["YearEndConfirmation"] = 'Are you sure you want to create new year?';
DataDictionaryEN["PasswordConfirmation"] = "Password Confirmation";
DataDictionaryEN["EnterPasswordMsg"] = 'Please enter your password : ';
DataDictionaryEN["vldPasswordInvalid"] = 'Invalid Password';
DataDictionaryEN["completedSuccessfully"] = 'Completed Successfully';
DataDictionaryEN["vldCreateFiscal"] = 'You must create a fiscal calendar for {0} befor  proceed.';
DataDictionaryEN["vldNoAccountAllocated"] = 'No Account to be allocated in selected range';
DataDictionaryEN["vldAllocatedCompleted"] = 'Batch allocation completed successfully';
DataDictionaryEN["Process"] = 'Process';
DataDictionaryEN['FromAccount'] = 'From Account';
DataDictionaryEN['ToAccount'] = 'To Account';
DataDictionaryEN['BatchDescription'] = 'Batch Description';
DataDictionaryEN['JournalEntryDate'] = 'Journal Entry Date';
DataDictionaryEN['AutoAllocationBatch'] = 'Auto Allocation Batch';
DataDictionaryEN["AccountReport"] = 'Trial Balance';
DataDictionaryEN["BatchListReport"] = "Batch List Report";
DataDictionaryEN["AddJournalEntryHeader"] = "Add Journal Entry Header";
DataDictionaryEN["IsRestricted"] = "Is Restricted";
DataDictionaryEN["AllowedUser"] = "Allowed Users";
DataDictionaryEN["AccountDescriptionAr"] = "Arabic Account Description";
DataDictionaryEN["IncomeAccountReport"] = "Income Statements";
DataDictionaryEN["BalanceSheetAccountReport"] = "Balance Sheet";
DataDictionaryEN["AccDescription"] = "Acc. Description";

//////////////////////////////////////////////Cash Management////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryEN["CashManagement"] = "Cash Management";
DataDictionaryEN["DefaultBankCode"] = "Default Bank Code";
DataDictionaryEN["SortDepositDetailsby"] = "Sort Deposit Details by";
DataDictionaryEN["DefaultGLAccount"] = "Default GL Account";
DataDictionaryEN["TransferAdjustmentGLaccount"] = "Transfer Adjustment GL account";
DataDictionaryEN["NextPostingSequence"] = "Next Posting Sequence";
DataDictionaryEN["DocumentType"] = "Document Type";
DataDictionaryEN["CreditCardType"] = "Credit Card Type";
DataDictionaryEN["CreditCard"] = "Credit Card";
DataDictionaryEN["Bank"] = "Bank";
DataDictionaryEN["BankCode"] = "Bank Code";
DataDictionaryEN["TransitNumber"] = "Transit Number";
DataDictionaryEN["InActive"] = "In Active";
DataDictionaryEN["BankAccountNumber"] = "Bank Account Number";
DataDictionaryEN["NextDepositNumber"] = "Next Deposit Number";
DataDictionaryEN["Error\Write-offSpread"] = "Error\Write-off Spread";
DataDictionaryEN["Multi-currency"] = "Multi-currency";
DataDictionaryEN["StatementCurrency"] = "Statement Currency";
DataDictionaryEN["BankAccount"] = "Bank Account";
DataDictionaryEN["Write-offAccount"] = "Write-off Account";
DataDictionaryEN["CreditCardChargesAccount"] = "Credit Card Charges Account";
DataDictionaryEN["City\Country"] = "City\Country";
DataDictionaryEN["State\Province"] = "State\Province";
DataDictionaryEN["Contact"] = "Contact";
DataDictionaryEN["PhoneNumber"] = "Phone Number";
DataDictionaryEN["Address"] = "Address";
DataDictionaryEN["LastClosingStatementBalance"] = "Last Closing Statement Balance";
DataDictionaryEN["Deposits"] = "Deposits";
DataDictionaryEN["Withdrawals"] = "Withdrawals";
DataDictionaryEN["CurrentBalance"] = "Current Balance";
DataDictionaryEN["ChequesStocks"] = "Cheques Stocks";
DataDictionaryEN["ChequeNumber"] = "Cheque Number";
DataDictionaryEN["ChequeStockCode"] = "Cheque Stock Code";
DataDictionaryEN["ChequeFrom"] = "Cheque From";
DataDictionaryEN["ExchangeGain"] = "Exchange Gain";
DataDictionaryEN["ExchangeLoss"] = "Exchange Loss";
DataDictionaryEN["RoundingAccount"] = "Rounding Account";
DataDictionaryEN["BankEntryBatch"] = "Bank Entry Batch";
DataDictionaryEN["BankEntry"] = "Bank Entry";
DataDictionaryEN["BatchNumber"] = "Batch Number";
DataDictionaryEN["TransferBatch"] = "Transfer Batch";
DataDictionaryEN["FromBank"] = "From Bank";
DataDictionaryEN["ToBank"] = "To Bank";
DataDictionaryEN["TransferAmount"] = "Transfer Amount";
DataDictionaryEN["DepositAmount"] = "Deposit Amount";
DataDictionaryEN["FunctionalTransferAmount"] = "Functional Transfer Amount";
DataDictionaryEN["FunctionalDepositAmount"] = "Functional Deposit Amount";
DataDictionaryEN["ReverseTransactions "] = "Reverse Transactions ";
DataDictionaryEN["BankReconciliation"] = "Bank Reconciliation";
DataDictionaryEN["StatementBalance"] = "Statement Balance";
DataDictionaryEN["BookBalance"] = "Book Balance";
DataDictionaryEN["DepositOutstanding"] = "Deposit Outstanding";
DataDictionaryEN["BankEntriesNotPosted"] = "Bank Entries Not Posted";
DataDictionaryEN["WithdrawalsOutstanding"] = "Withdrawals Outstanding";
DataDictionaryEN["AdjustedBookBalance"] = "Adjusted Book Balance";
DataDictionaryEN["AdjustedStatementBalance"] = "Adjusted Statement Balance";
DataDictionaryEN["OutOfBalanceBy"] = "Out Of Balance By";
DataDictionaryEN["UnclearItems"] = "Unclear Items";
DataDictionaryEN['ReverseTransactions'] = 'Reverse Transactions';
DataDictionaryEN['Reconciliation'] = 'Reconciliation';
DataDictionaryEN['Processing'] = 'Processing';
DataDictionaryEN['Documents'] = 'Documents';
DataDictionaryEN['Addnewdocument'] = 'Add new document';
DataDictionaryEN['DocumentType'] = 'Document Type';
DataDictionaryEN['Length'] = 'Length';
DataDictionaryEN['Prefix'] = 'Prefix';
DataDictionaryEN['NextNumber'] = 'Next Number';
DataDictionaryEN['AddCreditCardType'] = 'Add Credit Card Type';
DataDictionaryEN['ErrorWriteOff'] = 'Error\Write-off Spread';
DataDictionaryEN['Multicurrency'] = 'Multi-currency';
DataDictionaryEN['StatementCurrency'] = 'Statement Currency';
DataDictionaryEN['Write-offAccount'] = 'Write-off Account';
DataDictionaryEN['LastClosingStatmentBalance'] = 'Last Closing Statement Balance';
DataDictionaryEN['+Deposits'] = '+Deposits';
DataDictionaryEN['-Withdrawals'] = '-Withdrawals';
DataDictionaryEN['AddBank'] = 'Add Bank';
DataDictionaryEN['AddChequeStock'] = 'Add Cheque Stock';
DataDictionaryEN['BankDetails'] = 'Bank Details';
DataDictionaryEN['CityCountry'] = 'City\Country';
DataDictionaryEN['StateProvince'] = 'State\Province';
DataDictionaryEN['vldDeactivateConnectedItem'] = "Failed to set to inactive, item connected to other entities";
DataDictionaryEN['vldAddCurrencyToBank'] = 'Bank is not multicurrency,you can\'t add currency.';
DataDictionaryEN['vldFailedCreateNewBatch'] = 'Failed to create new batch';
DataDictionaryEN['vldFailedToLoadBatchList'] = 'Failed to create new batch';
DataDictionaryEN['BankEntryBatchList'] = 'Bank Entry Batch List';
DataDictionaryEN['BankTransferBatchList'] = 'Bank Transfer Batch List';
DataDictionaryEN['EntryDate'] = 'Entry Date';
DataDictionaryEN['TransactionNumber'] = 'Entry #';
DataDictionaryEN['Line'] = 'Line';
DataDictionaryEN['AccountName'] = 'Account Name';
DataDictionaryEN['AddBankEntryHeader'] = 'Add Bank Entry Header';
DataDictionaryEN['AddBankEntry'] = 'Add Bank Entry';
DataDictionaryEN['ThisIsa'] = 'This is a';
DataDictionaryEN['AddTransferEntry'] = 'Add Transfer Entry';
DataDictionaryEN['TransferEntry'] = 'Transfer Entry';
DataDictionaryEN['vldToAmountError'] = 'Value mustn\'t be more than deposit amount';
DataDictionaryEN['vldFromAmountError'] = 'Value mustn\'t be more than transfer amount';
DataDictionaryEN['vldTransferAmount'] = 'Transfer amount must be equal to deposit amount';
DataDictionaryEN['AddTransferEntry'] = 'Add Transfer Entry';
DataDictionaryEN['TransferEntry'] = 'Transfer Entry';
DataDictionaryEN['NoTransSelected'] = 'No transaction selected';
DataDictionaryEN["vldSuccessfullyReversed"] = 'Transaction reversed successfully';
DataDictionaryEN["vldFailedToReverse"] = 'Failed to reverse,some transaction are reversed';
DataDictionaryEN['Module'] = 'Module';
DataDictionaryEN['BankDescription'] = 'Bank Description';
DataDictionaryEN['CheckNumber'] = 'Check Number';
DataDictionaryEN['CheckAmount'] = 'Check Amount';
DataDictionaryEN['StartReversing'] = 'Start Reversing';
DataDictionaryEN['FromCheckNumber'] = 'From Check Number';
DataDictionaryEN['ToCheckNumber'] = 'To Check Number';
DataDictionaryEN['FromPaymentDate'] = 'From Payment Date';
DataDictionaryEN['ToPaymentDate'] = 'To Payment Date';
DataDictionaryEN['FromVendor'] = 'From Vendor';
DataDictionaryEN['ToVendor'] = 'To Vendor';
DataDictionaryEN['FromCustomer'] = 'From Customer';
DataDictionaryEN['ToCustomer'] = 'To Customer';
DataDictionaryEN['FromAmount'] = 'From Amount';
DataDictionaryEN['ToAmount'] = 'To Amount';
DataDictionaryEN['ReceiptEntry'] = 'Receipt Entry';
DataDictionaryEN['TransferEntry'] = 'Transfer Entry';
DataDictionaryEN['BankEntry'] = 'Bank Entry';
DataDictionaryEN['Searchcriteria'] = 'Search criteria';
DataDictionaryEN['UnclearItems'] = 'Unclear Items';
DataDictionaryEN['CheckNumber'] = 'Check#';
DataDictionaryEN['Mark'] = 'Mark';
DataDictionaryEN['ToCheck'] = 'To Check';
DataDictionaryEN['FromCheck'] = 'From Check';
DataDictionaryEN['QuickClearing'] = 'Quick Clearing';
DataDictionaryEN['AdjustedStatementBalance'] = 'Adjusted Statement Balance';
DataDictionaryEN['AdjustedBookBalance'] = 'Adjusted Book Balance';
DataDictionaryEN['WithdrawalsOutstanding'] = '-Withdrawals Outstanding';
DataDictionaryEN['BankEntriesNotPosted'] = 'Bank Entries Not Posted';
DataDictionaryEN['ReconciliationDate'] = 'Reconciliation Date';
DataDictionaryEN['StatementDate'] = 'Statement Date';
DataDictionaryEN['DepositOutstanding'] = '+Deposit Outstanding';
DataDictionaryEN['ReconciliationDescription'] = 'Reconciliation Description';
DataDictionaryEN['Withdrawal'] = 'Withdrawal';
DataDictionaryEN['Deposit'] = 'Deposit';
DataDictionaryEN['Reconciled'] = 'Reconciled';
DataDictionaryEN['ClearedAmount'] = 'Cleared Amount';
DataDictionaryEN['Difference'] = 'Difference';
DataDictionaryEN['DifferenceReason'] = 'Difference Reason';
DataDictionaryEN["AddBankReconciliation"] = "Add Bank Reconciliation";
DataDictionaryEN["BankStatement"] = "Bank Statement";
DataDictionaryEN["Statement"] = "Statement";
DataDictionaryEN["Continue"] = "Continue";
DataDictionaryEN["GetChecks"] = "Get Checks";
DataDictionaryEN['As'] = 'As';
DataDictionaryEN['vldOutOfBalanceError'] = 'Out off balance must be 0 to post';
DataDictionaryEN['alreadyPosted'] = "Already Posted";
DataDictionaryEN['BankRequired'] = "Select Bank";
DataDictionaryEN['GLAccount'] = 'GL Account';
DataDictionaryEN['Amount'] = 'Amount';

DataDictionaryEN['TransferEntryDetails'] = 'Transfer Entry Details';
DataDictionaryEN['vldInsertEntryNumber'] = 'Insert Entry number';
DataDictionaryEN['vldSelectEntryDate'] = 'Insert Entry date';
DataDictionaryEN['vldBankRequired'] = 'Insert bank code';
DataDictionaryEN['vldClearedAmount'] = 'Value must be less than or equal diffrence';
DataDictionaryEN['vldClearedAmountDiffrence'] = 'Diffrence must be 0 or you should select diffrence reason';
DataDictionaryEN['vldNoChecksFound'] = 'No checks found';

//////////////////////////////////////////////Account Receivable//////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryEN["AccountReceivable"] = "Account Receivable";
DataDictionaryEN["Options"] = "Options";
DataDictionaryEN["AllowEditofImportedBatches"] = "Allow Edit of Imported Batches";
DataDictionaryEN["AllowEditofExternalBatches"] = "Allow Edit of External Batches";
DataDictionaryEN["DefaultPostingDate"] = "Default Posting Date";
DataDictionaryEN["CalculateTaxAmountsAutomatically"] = "Calculate Tax Amounts Automatically";
DataDictionaryEN["AllowPrintingofInvoices"] = "Allow Printing of Invoices";
DataDictionaryEN["AllowEditAfterInvoicePrinted"] = "Allow Edit After Invoice Printed";
DataDictionaryEN["DefaultTransactionType"] = "Default Transaction Type";
DataDictionaryEN["DefaultOrderofOpenDocuments"] = "Default Order of Open Documents";
DataDictionaryEN["DefaultPaymentCode"] = "Default Payment Code";
DataDictionaryEN["DefaultBankCode"] = "Default Bank Code";
DataDictionaryEN["DefaultPostingDate"] = "Default Posting Date";
DataDictionaryEN["AllowPrintingofReceipts"] = "Allow Printing of Receipts";
DataDictionaryEN["AllowEditAfterReceiptPrinted"] = "Allow Edit After Receipt Printed";
DataDictionaryEN["CheckForDuplicatedChecks"] = "Check For Duplicated Checks";
DataDictionaryEN["SortChecksBy"] = "Sort Checks By";
DataDictionaryEN["PaymentCode"] = "Payment Code";
DataDictionaryEN["PaymentCode"] = "Payment Code";
DataDictionaryEN["PaymentType"] = "Payment Type";
DataDictionaryEN["PaymentTerms"] = "Payment Terms";
DataDictionaryEN["PaymentTerm"] = "Payment Term";
DataDictionaryEN["DueDateType"] = "Due Date Type";
DataDictionaryEN["No.ofDaysForInvoiceDueDate"] = "No. of Days For Invoice Due Date";
DataDictionaryEN["DiscountType"] = "Discount Type";
DataDictionaryEN["CalculateBaseforDiscountWithTax"] = "Calculate Base for Discount With Tax";
DataDictionaryEN["No.OfDaysForDiscountDueDate"] = "No. Of Days For Discount Due Date";
DataDictionaryEN["DiscountPercentage"] = "Discount Percentage";
DataDictionaryEN["AccountsSets"] = "Accounts Sets";
DataDictionaryEN["ReceivablesControlAccount"] = "Receivables Control Account";
DataDictionaryEN["InvoiceDiscountAccount"] = "Invoice Discount Account";
DataDictionaryEN["PrepaymentAccount"] = "Prepayment Account";
DataDictionaryEN["ReceiptsDiscountAccount"] = "Receipts Discount Account ";
DataDictionaryEN["CurrencyCode"] = "Currency Code";
DataDictionaryEN["Un-RealizedExchangeGain"] = "Un-Realized Exchange Gain";
DataDictionaryEN["Un-RealizedExchangeLoss"] = "Un-Realized Exchange Loss";
DataDictionaryEN["ExchangeGain"] = "Exchange Gain";
DataDictionaryEN["ExchangeLoss"] = "Exchange Loss ";
DataDictionaryEN["ExchangeRounding"] = "Exchange Rounding";
DataDictionaryEN["Write-OffAccount"] = "Write-Off Account";
DataDictionaryEN["SalesPerson"] = "SalesPerson";
DataDictionaryEN["EmployeeName"] = "Employee Name ";
DataDictionaryEN["EmployeeNumber"] = "Employee Number";
DataDictionaryEN["AnnualSalesTarget"] = "Annual Sales Target";
DataDictionaryEN["CustomerGroups"] = "Customer Groups";
DataDictionaryEN["AccountType"] = "Account Type";
DataDictionaryEN["AccountSet"] = "Account Set";
DataDictionaryEN["TermCode"] = "Term Code";
DataDictionaryEN["TaxGroup"] = "Tax Group";
DataDictionaryEN["PrintStatements"] = "Print Statements";
DataDictionaryEN["CreditLimit"] = "Credit Limit";
DataDictionaryEN["OverdueLimit"] = "Overdue Limit";
DataDictionaryEN["Customer "] = "Customer ";
DataDictionaryEN["CustomerNumber"] = "Customer Number";
DataDictionaryEN["CustomerName"] = "Customer Name";
DataDictionaryEN["ShortName"] = "Short Name";
DataDictionaryEN["Group"] = "Group";
DataDictionaryEN["OnHold"] = "On Hold";
DataDictionaryEN["StartDate"] = "Start Date";
DataDictionaryEN["Website"] = "Website";
DataDictionaryEN["Email"] = "Email";
DataDictionaryEN["AccountType"] = "Account Type";
DataDictionaryEN["DeliveryMethod"] = "Delivery Method";
DataDictionaryEN["Invoice"] = "Invoice";
DataDictionaryEN["Receipt"] = "Receipt";
DataDictionaryEN["Document"] = "Document";
DataDictionaryEN["DocumentType"] = "Document Type";
DataDictionaryEN["AppliedAmount"] = "Applied Amount";
DataDictionaryEN["ReceiptDescription"] = "Receipt Description";
DataDictionaryEN["DefaultBank"] = "Default Bank";
DataDictionaryEN["ReceiptType"] = "Receipt Type";
DataDictionaryEN["ExchangeRate"] = "Exchange Rate";
DataDictionaryEN["ReceiptDate"] = "Receipt Date";
DataDictionaryEN["PostingDate"] = "Posting Date";
DataDictionaryEN["PaymentType"] = "Payment Type";
DataDictionaryEN["ReceiptAmount"] = "Receipt Amount";
DataDictionaryEN["ReferenceNumber"] = "Reference Number";
DataDictionaryEN["Apply"] = "Apply";
DataDictionaryEN["Note"] = "Note";
DataDictionaryEN["AdjustmentBachlist"] = "Adjustment Bach list";
DataDictionaryEN["RefundBatchList"] = "Refund Batch List";
DataDictionaryEN["AllowRefundWithoutApply"] = "Allow Refund Without Apply";
DataDictionaryEN["RefundAmount"] = "Refund Amount";
DataDictionaryEN["PostingDate"] = "Posting Date";
DataDictionaryEN["Type"] = "Type";
DataDictionaryEN["OriginalAmount"] = "Original Amount";
DataDictionaryEN["CurrentBalance"] = "Current Balance";
DataDictionaryEN["PaymentType"] = "Payment Type";
DataDictionaryEN["PaymentAmount"] = "Payment Amount";
DataDictionaryEN["NetBalance"] = "Net Balance";
DataDictionaryEN["YearEnd"] = "Year End";
DataDictionaryEN["Revaluation"] = "Revaluation";
DataDictionaryEN["CustomerStatement"] = "Customer Statement";
DataDictionaryEN[" CustomerTransactions"] = " Customer Transactions";
DataDictionaryEN["CustomerAgeing"] = "Customer Ageing";
DataDictionaryEN['CustomerGroup'] = 'Customer Group';
DataDictionaryEN['InvoiceBatchList'] = 'Invoice Batch List';
DataDictionaryEN['ReceiptBatchList'] = 'Receipt Batch List';
DataDictionaryEN['AdjustmentBatchList'] = 'Adjustment Batch List';
DataDictionaryEN['CustomerTransactions'] = 'Customer Transactions';
DataDictionaryEN['DebitNote'] = 'Debit Note';
DataDictionaryEN['CreditNote'] = 'Credit Note';
DataDictionaryEN['Receipts'] = 'Receipts';
DataDictionaryEN['Prepayment'] = 'Prepayment';
DataDictionaryEN['Miscellaneous'] = 'Miscellaneous';
DataDictionaryEN['UnappliedCash'] = 'Unapplied Cash';
DataDictionaryEN['Adjustment'] = 'Adjustment';
DataDictionaryEN['Refund'] = 'Refund';
DataDictionaryEN['AddPaymentCode'] = 'Add Payment Code';
DataDictionaryEN['AddPaymentTerm'] = 'Add Payment Term';
DataDictionaryEN['AddAccountSet'] = 'Add Account Set';
DataDictionaryEN['AddSalesPerson'] = 'Add Sales Person';
DataDictionaryEN['Commission'] = 'Commission';
DataDictionaryEN['Employee#'] = 'Employee#';
DataDictionaryEN['NumberOfRates'] = 'Number Of Rates 1-5';
DataDictionaryEN['PaidCommission'] = 'Paid Commission';
DataDictionaryEN['OnSalesOf'] = 'On Sales Of';
DataDictionaryEN['To'] = 'To';
DataDictionaryEN['TaxClass'] = 'Tax Class';
DataDictionaryEN['AddCustomerGroup'] = 'Add Customer Group';
DataDictionaryEN['vldTaxGroupCurrencyDiffrent'] = 'Tax group currency {0} is different than the selected currency {1} , do you want to continue?';
DataDictionaryEN['NotFound'] = 'Not Found';
DataDictionaryEN['Invalid'] = 'Invalid';
DataDictionaryEN['vldBatchNotFound'] = 'Batch not found';
DataDictionaryEN['vldBatchIs'] = 'Batch Is {0}';
DataDictionaryEN['vldBatchReadyToPost'] = 'Batch is set to ready to post';
DataDictionaryEN['vldDocumentType'] = 'Invalid document type';
DataDictionaryEN['vldDocumentCustomer'] = 'This document is not belongs to the selected customer';
DataDictionaryEN['vldDocumentNotPosted'] = 'This Document is not posted';
DataDictionaryEN['vldDateWithinLockedPeriod'] = 'Date With in locked period';
DataDictionaryEN['Contact/Address'] = 'Contact/Address';
DataDictionaryEN['Invoicing'] = 'Invoicing';
DataDictionaryEN['AddCustomer'] = 'Add Customer';
DataDictionaryEN['Zip/PostalCode'] = 'Zip/Postal Code';
DataDictionaryEN['Telephone'] = 'Telephone';
DataDictionaryEN['State/Prov'] = 'State/Prov';
DataDictionaryEN['TermsCode'] = 'Terms Code';
DataDictionaryEN['TotalOutstandingBalanceExceedsTheCreditLimit'] = 'Total Outstanding Balance Exceeds The Credit Limit';
DataDictionaryEN['ARTransactionsOverdueByN'] = 'AR Transactions Overdue By N';
DataDictionaryEN['DaysOrMoreAndExceedTheOverdueLimitOfNAmount'] = 'Days Or More And Exceed The Overdue Limit Of N Amount';
DataDictionaryEN['TotalInvoices'] = 'Total Invoices';
DataDictionaryEN['TotalReceipts'] = 'Total Receipts';
DataDictionaryEN['OutstandingBalanceExceedsLimit'] = 'Outstanding Balance Exceeds Limit';
DataDictionaryEN['CustomerType'] = 'Customer Type';
DataDictionaryEN['CheckforDuplicatePOs'] = 'Check for Duplicate POs';
DataDictionaryEN['DocumentDate'] = 'Document Date';
DataDictionaryEN['AllocatedAmount'] = 'Allocated Amount';
DataDictionaryEN['orMoreandExceedTheoverduelimitofnamount'] = 'or More and Exceed The overdue limit of n amount';
DataDictionaryEN['Address-Contact'] = 'Address-Contact';
DataDictionaryEN['vldEditPrintedBatchError'] = 'can not edit printed batch';
DataDictionaryEN['TotalWithTax'] = 'Total With Tax';
DataDictionaryEN['Batch'] = 'Batch';
DataDictionaryEN['vldExceedError'] = 'Mustn\'t exceed {0}';
DataDictionaryEN['vldCustomerOnHoldError'] = 'Customer {0} is onhold, do you want to continue?';
DataDictionaryEN['vldExceedCreditLimit'] = 'Exceeds the credit limit, do you want to continue?';
DataDictionaryEN['ApplyToDocument'] = 'Apply To Document';
DataDictionaryEN['SourceValue'] = 'Source Value';
DataDictionaryEN['AddInvoice'] = 'Add Invoice';
DataDictionaryEN['BatchDate'] = 'Batch Date';
DataDictionaryEN['vldInvoiceRequired'] = 'Please select invoice';
DataDictionaryEN['NotExist'] = 'Not Exist';
DataDictionaryEN['vldAppliedAmountError'] = 'Applied amount must be greater than zero';
DataDictionaryEN['vldAppliedAmountGreaterThanAmount'] = 'Total applied amount ({0}) for invoice no. {1} is greater than invoice available amount to apply ({2})';
DataDictionaryEN['AddReceipt'] = 'Add Receipt';
DataDictionaryEN['PrepaymentAmount'] = 'Prepayment Amount';
DataDictionaryEN['DocumentNumber'] = 'Document Number';
DataDictionaryEN['GLAccountDescription'] = 'GL Account Description';
DataDictionaryEN['RefundNumber'] = 'Refund Number';
DataDictionaryEN['CashAmount'] = 'Cash Amount';
DataDictionaryEN['AddRefund'] = 'Add Refund';
DataDictionaryEN['AddAdjustmentHeader'] = 'Add Adjustment Header';
DataDictionaryEN['AddAdjustment'] = 'Add Adjustment';
DataDictionaryEN['Adjust'] = 'Adjust';
DataDictionaryEN['AdjustmentNumber'] = 'Adjustment Number';
DataDictionaryEN['vldSelectAdjustment'] = 'Please select adjustment entry';
DataDictionaryEN['vldAdjustmentNotFound'] = 'adjustment entry not found';
DataDictionaryEN['vldInvoiceLineNotBelongToEntry'] = 'Invoice Line is not belongs to entry document';
DataDictionaryEN['vldRefundRequiredError'] = 'Please select refund entry';
DataDictionaryEN['vldRefundNotFound'] = 'Refund entry not found';
DataDictionaryEN['vldReceiptRequiredError'] = 'Please select receipt';
DataDictionaryEN['vldReceiptNotPostedError'] = 'Receipt is not posted';
DataDictionaryEN['vldRefundAmountRequired'] = 'Amount must be greater than zero';
DataDictionaryEN['YearEnd'] = 'Year End';
DataDictionaryEN['YearEndNote'] = 'This process will reset all batches numbers .';
DataDictionaryEN['YearEndConfirm'] = 'This process will reset all batches numbers, Are you sure?';
DataDictionaryEN['resetBatchSuccessfully'] = 'All batches numbers are reste successfully';
DataDictionaryEN['vldYearEndOpendInvoices'] = 'There are some opend invoice batches.';
DataDictionaryEN['vldYearEndOpendReceipts'] = 'There are some opend receipt batches.';
DataDictionaryEN['vldYearEndOpendAdjustment'] = 'There are some opend adjustment batches.';
DataDictionaryEN['vldYearEndOpendRefund'] = 'There are some opend refund batches.';
DataDictionaryEN['vldReEvalFailed'] = 'Revaluation failed';
DataDictionaryEN['vldReEvalSuccessfullly'] = 'Reevaluated successfully';
DataDictionaryEN['vldNoTransNeedEval'] = 'There are no transactions need revaluation';
DataDictionaryEN['vldCurrencyIsFunc'] = 'Currency is functional currency';
DataDictionaryEN['CreditNote'] = 'Credit Note';
DataDictionaryEN['DebitNote'] = 'Debit Note';
DataDictionaryEN['DepositSlip'] = 'Deposit Slip';

DataDictionaryEN['vldReceiptBankNotExist'] = 'Receipt {0} : bank is not exists';
DataDictionaryEN['vldReceiptBankInactive'] = 'Receipt {0} : bank is inactive';
DataDictionaryEN['vldReceiptBankAccNotExist'] = 'Receipt {0} : bank account is not exists';
DataDictionaryEN['vldReceiptBankAccInactive'] = 'Receipt {0} : bank account is inactive';
DataDictionaryEN['vldReceiptCustomerNotExist'] = 'Receipt {0} : customer is not exists';
DataDictionaryEN['vldReceiptCustomerInactive'] = 'Receipt {0} : customer is inactive';
DataDictionaryEN['vldReceiptDateLocked'] = 'Receipt {0} : document date is exists within a locked period';
DataDictionaryEN['vldInvoiceDateBeforOldest'] = 'invoice {0} : document date is before oldest year';
DataDictionaryEN['vldReceiptDateNotInCurrent'] = 'Receipt {0} : document date is not within the current year';
DataDictionaryEN['vldReceiptNoDetails'] = 'Receipt {0} : document does not have any details';
DataDictionaryEN['vldAccSetNoDeclaredReceivableAcc'] = 'Receipt {0} : customer account set has no declared receivable account control';
DataDictionaryEN['vldAccSetReceivableAccInactive'] = 'Receipt {0} : customer account set receivable account control is inactive';
DataDictionaryEN['vldNoDeclaredLibilityAccount'] = 'Receipt {0} : customer tax authority has no declared liability account';
DataDictionaryEN['vldReceiptAccNotExist'] = 'Receipt {0} : account is not exists';
DataDictionaryEN['vldReceiptAccInactive'] = 'Receipt {0} : account {1} is inactive';


DataDictionaryEN['vldAdjustmentBankNotExist'] = 'Adjustment {0} : bank is not exists';
DataDictionaryEN['vldAdjustmentBankInactive'] = 'Adjustment {0} : bank is inactive';
DataDictionaryEN['vldAdjustmentBankAccNotExist'] = 'Adjustment {0} : bank account is not exists';
DataDictionaryEN['vldAdjustmentBankAccInactive'] = 'Adjustment {0} : bank account is inactive';
DataDictionaryEN['vldAdjustmentCustomerNotExist'] = 'Adjustment {0} : customer is not exists';
DataDictionaryEN['vldAdjustmentCustomerInactive'] = 'Adjustment {0} : customer is inactive';
DataDictionaryEN['vldAdjustmentDateLocked'] = 'Adjustment {0} : document date is exists within a locked period';
DataDictionaryEN['vldAdjustmentDateBeforOldest'] = 'invoice {0} : document date is before oldest year';
DataDictionaryEN['vldAdjustmentDateNotInCurrent'] = 'Adjustment {0} : document date is not within the current year';
DataDictionaryEN['vldAdjustmentNoDetails'] = 'Adjustment {0} : document does not have any details';
DataDictionaryEN['vldAdjAccSetNoDeclaredReceivableAcc'] = 'Adjustment {0} : customer account set has no declared receivable account control';
DataDictionaryEN['vldAdjustmentAccNotExist'] = 'Adjustment {0} : account is not exists';
DataDictionaryEN['vldAdjustmentAccInactive'] = 'Adjustment {0} : account {1} is inactive';

DataDictionaryEN['vldRefundBankNotExist'] = 'Refund {0} : bank is not exists';
DataDictionaryEN['vldRefundBankInactive'] = 'Refund {0} : bank is inactive';
DataDictionaryEN['vldRefundBankAccNotExist'] = 'Refund {0} : bank account is not exists';
DataDictionaryEN['vldRefundBankAccInactive'] = 'Refund {0} : bank account is inactive';
DataDictionaryEN['vldRefundCustomerNotExist'] = 'Refund {0} : customer is not exists';
DataDictionaryEN['vldRefundCustomerInactive'] = 'Refund {0} : customer is inactive';
DataDictionaryEN['vldRefundDateLocked'] = 'Refund {0} : document date is exists within a locked period';
DataDictionaryEN['vldRefundDateBeforOldest'] = 'invoice {0} : document date is before oldest year';
DataDictionaryEN['vldRefundDateNotInCurrent'] = 'Refund {0} : document date is not within the current year';
DataDictionaryEN['vldRefundNoDetails'] = 'Refund {0} : document does not have any details';
DataDictionaryEN['vldRefAccSetNoDeclaredReceivableAcc'] = 'Refund {0} : customer account set has no declared receivable account control';
DataDictionaryEN['vldRefundAccNotExist'] = 'Refund {0} : account is not exists';
DataDictionaryEN['vldRefundAccInactive'] = 'Refund {0} : account {1} is inactive';

DataDictionaryEN['vldInvoiceBankNotExist'] = 'Invoice {0} : bank is not exists';
DataDictionaryEN['vldInvoiceBankInactive'] = 'Invoice {0} : bank is inactive';
DataDictionaryEN['vldInvoiceBankAccNotExist'] = 'Invoice {0} : bank account is not exists';
DataDictionaryEN['vldInvoiceBankAccInactive'] = 'Invoice {0} : bank account is inactive';
DataDictionaryEN['vldInvoiceCustomerNotExist'] = 'Invoice {0} : customer is not exists';
DataDictionaryEN['vldInvoiceCustomerInactive'] = 'Invoice {0} : customer is inactive';
DataDictionaryEN['vldInvoiceDateLocked'] = 'Invoice {0} : document date is exists within a locked period';
DataDictionaryEN['vldInvoiceDateBeforOldest'] = 'invoice {0} : document date is before oldest year';
DataDictionaryEN['vldInvoiceDateNotInCurrent'] = 'Invoice {0} : document date is not within the current year';
DataDictionaryEN['vldInvoiceNoDetails'] = 'Invoice {0} : document does not have any details';
DataDictionaryEN['vldInvAccSetNoDeclaredReceivableAcc'] = 'Invoice {0} : customer account set has no declared receivable account control';
DataDictionaryEN['vldInvoiceAccNotExist'] = 'Invoice {0} : account is not exists';
DataDictionaryEN['vldInvoiceAccInactive'] = 'Invoice {0} : account {1} is inactive';
DataDictionaryEN['vldInvoiceAccCurrency'] = 'invoice {0} : customer account set has no declared currency';
DataDictionaryEN['vldInvoiceTaxClassNoRate'] = 'invoice {0} : tax class {1} has no declared rate';
DataDictionaryEN["AutoDistributeReceipt"] = "Auto Distribute Receipt";
DataDictionaryEN["NoDetailsSelected"] = 'No Details To Distribute Amount';
DataDictionaryEN["vldAppliedAmountMoreThanReceipt"] = "Applied Amount Mustn't be more than Receipt Amount";
DataDictionaryEN["vldReceiptAmountFullyApplied"] = "Receipt Amount Must Be fully Applied On Invoices";
DataDictionaryEN["DocumentBalance"] = "Document Balance";
DataDictionaryEN["vldReceiptAmountFullyDistributed"] = "Receipt {0} : Receipt Amount Must Be fully Distributed";
DataDictionaryEN["DayOfMonth"] = "Day Of Month";
DataDictionaryEN["DayOfWeek"] = "Day Of Week";
DataDictionaryEN["vldTermMonthDays"] = "Select Valid Month Day between 1-30";
DataDictionaryEN["vldTermWeekDays"] = "Select Valid Week Day between 1-7";
DataDictionaryEN["DueDate"] = "Due Date";
DataDictionaryEN["NoDetails"] = "No Details Inserted";

//////////////////////////////////////////////Account Payable//////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryEN["AccountPayable"] = "Account Payable";
DataDictionaryEN["VendorGroup"] = "Vendor Group";
DataDictionaryEN["Vendor"] = "Vendor";
DataDictionaryEN["VendorNumber "] = "Vendor Number ";
DataDictionaryEN["VendorName"] = "Vendor Name";
DataDictionaryEN["LegalName"] = "Legal Name";
DataDictionaryEN["DuplicateVendorAndAmountCheck"] = "Duplicate Vendor And Amount Check";
DataDictionaryEN["DuplicateVendorAndDateCheck"] = "Duplicate Vendor And Date Check";
DataDictionaryEN["AccountPayableInvoice"] = "Account Payable Invoice";
DataDictionaryEN["UndistributedAmount"] = "Undistributed Amount";
DataDictionaryEN["Comments"] = "Comments";
DataDictionaryEN["DocumentTotal"] = "Document Total";
DataDictionaryEN["SourceAmountWithTax"] = "Amount With Tax";
DataDictionaryEN["PaymentBatchList"] = "Payment Batch List";
DataDictionaryEN["miscellaneous"] = "miscellaneous";
DataDictionaryEN["Payeename"] = "Payee name";
DataDictionaryEN["PayeeName"] = "Payee Name";
DataDictionaryEN["PaymentType"] = "Payment Type";
DataDictionaryEN["ApplyDocument"] = "Apply Document";
DataDictionaryEN["PendingAmount"] = "Pending Amount";
DataDictionaryEN["VendorStatement"] = "Vendor Statement";
DataDictionaryEN["VendorTransactions"] = "Vendor Transactions";
DataDictionaryEN["VendorAgeing"] = "Vendor Ageing";
DataDictionaryEN['IncludePendingTransactions'] = 'Include Pending Transactions';
DataDictionaryEN['AgeUnappliedCreditDebitNote'] = 'Age Unapplied Credit Debit Note';
DataDictionaryEN['DefaultDocumentType'] = 'Default Document Type';
DataDictionaryEN['DefaultPaymentTerm'] = 'Default Payment Term';
DataDictionaryEN['PaymentOptions'] = 'Payment Options';
DataDictionaryEN['AllowAdjustmentsinPaymentBatches'] = 'Allow Adjustments in Payment Batches';
DataDictionaryEN['ApplyDoc'] = 'Apply Doc';
DataDictionaryEN['Misc.'] = 'Misc.';
DataDictionaryEN['Payment'] = 'Payment';
DataDictionaryEN['PayablesControlAccount'] = 'Payables Control Account';
DataDictionaryEN['PurchaseDiscountAccount'] = 'Purchase Discount Account';
DataDictionaryEN['AddVendorGroup'] = 'Add Vendor Group';
DataDictionaryEN['VendorGroups'] = 'Vendor Groups';
DataDictionaryEN['VendorNumber'] = 'Vendor Number';
DataDictionaryEN['AddressLine1'] = 'Address Line 1';
DataDictionaryEN['AddressLine2'] = 'Address Line 2';
DataDictionaryEN['AddressLine3'] = 'Address Line 3';
DataDictionaryEN['AddressLine4'] = 'Address Line 4';
DataDictionaryEN['State/Prov.'] = 'State/Prov.';
DataDictionaryEN['PostalCode'] = 'Postal Code';
DataDictionaryEN['TotalPayments'] = 'Total Payments';
DataDictionaryEN['VendorBalance'] = 'Vendor Balance';
DataDictionaryEN['PaymentTermCode'] = 'Payment Term Code';
DataDictionaryEN['CreditLimitAmount'] = 'Credit Limit Amount';
DataDictionaryEN['AddVendor'] = 'Add Vendor';
DataDictionaryEN['vldVendorOnHoldError'] = 'Vendor {0} is onhold, do you want to continue?';
DataDictionaryEN['Warning'] = 'Warning';
DataDictionaryEN['vldDisAmountNotEqualToDocumentTot'] = 'The ditributed amount does not equal the document total , Continue ?';
DataDictionaryEN['vldVendorExceedCreditLimit'] = 'Vendor exceeds the credit limit, do you want to continue?';
DataDictionaryEN['vldAddDetailsLine'] = 'You must add details line';
DataDictionaryEN['TaxAmount'] = 'Tax Amount';
DataDictionaryEN['Exchangerate'] = 'Exchange rate';
DataDictionaryEN['SourceAmount'] = 'Source Amount';
DataDictionaryEN['VendorAmount'] = 'Vendor Amount';
DataDictionaryEN['AddPayment'] = 'Add Payment';
DataDictionaryEN['DocumentNo.'] = 'Document No.';
DataDictionaryEN['vldAppliedPrepaymentError'] = 'Applied amount mustn\'t be greater than Prepayment pending amount';
DataDictionaryEN['PostingDate'] = 'Posting Date';
DataDictionaryEN['vldYearEndOpendPayments'] = 'There are some opend payment batches.';

DataDictionaryEN['vldPostEmptyBatch'] = 'You can\'t post batch,batch is empty';
DataDictionaryEN['vldPostPostedBatch'] = 'Batch already posted';
DataDictionaryEN['vldPostDeletedBatch'] = 'Batch already deleted';
DataDictionaryEN['vldEditReadyToPostBatch'] = 'You can\'t edit batch ,batch is ready to post';
DataDictionaryEN['vldPostLockedEntries'] = 'Can\'t post batch ,some of entries in locked period of fiscal calendar';
DataDictionaryEN['vldPostBeforOldesetEntries'] = 'Can\'t post batch ,some of entries befor oldest year';
DataDictionaryEN['vldPostEntriesWithInactiveAcc'] = 'Can\'t post batch ,some of entries account are in active';
DataDictionaryEN['vldPostBeforCurrentYear'] = 'Can\'t post batch ,some of entries befor current year';
DataDictionaryEN['vldPostUnbalancedBatch'] = 'Can\'t post batch , Un balanced batch';
DataDictionaryEN['vldAdjustmentPeriodLocked'] = 'Can\'t post batch , Adjustment Period Is Locked';

DataDictionaryEN['vldApInvoiceBankNotExist'] = 'Invoice {0} : bank is not exists';
DataDictionaryEN['vldApInvoiceBankInactive'] = 'Invoice {0} : bank is inactive';
DataDictionaryEN['vldApInvoiceBankAccNotExist'] = 'Invoice {0} : bank account is not exists';
DataDictionaryEN['vldApInvoiceBankAccInactive'] = 'Invoice {0} : bank account is inactive';
DataDictionaryEN['vldApInvoiceVendorNotExist'] = 'Invoice {0} : vendor is not exists';
DataDictionaryEN['vldApInvoiceVendorInactive'] = 'Invoice {0} : vendor is inactive';
DataDictionaryEN['vldApInvoiceDateLocked'] = 'Invoice {0} : document date is exists within a locked period';
DataDictionaryEN['vldApInvoiceDateBeforOldest'] = 'invoice {0} : document date is before oldest year';
DataDictionaryEN['vldApInvoiceDateNotInCurrent'] = 'Invoice {0} : document date is not within the current year';
DataDictionaryEN['vldApInvoiceNoDetails'] = 'Invoice {0} : document does not have any details';
DataDictionaryEN['vldApInvAccSetNoDeclaredReceivableAcc'] = 'Invoice {0} : vendor account set has no declared payables account control';
DataDictionaryEN['vldApInvoiceAccNotExist'] = 'Invoice {0} : account is not exists';
DataDictionaryEN['vldApInvoiceAccInactive'] = 'Invoice {0} : account {1} is inactive';
DataDictionaryEN['vldApInvoiceAccCurrency'] = 'invoice {0} : vendor account set has no declared currency';
DataDictionaryEN['vldApInvoiceTaxClassNoRate'] = 'invoice {0} : tax class {1} has no declared rate';
DataDictionaryEN['vldApInvoiceDisAmount'] = 'invoice {0} : ditributed amount doesn\'t equal document total amount';



DataDictionaryEN['vldPaymentBankNotExist'] = 'Payment {0} : bank is not exists';
DataDictionaryEN['vldPaymentBankInactive'] = 'Payment {0} : bank is inactive';
DataDictionaryEN['vldPaymentBankAccNotExist'] = 'Payment {0} : bank account is not exists';
DataDictionaryEN['vldPaymentBankAccInactive'] = 'Payment {0} : bank account is inactive';
DataDictionaryEN['vldPaymentvendorNotExist'] = 'Payment {0} : vendor is not exists';
DataDictionaryEN['vldPaymentvendorInactive'] = 'Payment {0} : vendor is inactive';
DataDictionaryEN['vldPaymentDateLocked'] = 'Payment {0} : document date is exists within a locked period';
DataDictionaryEN['vldPaymentDateBeforOldest'] = 'Payment {0} : document date is before oldest year';
DataDictionaryEN['vldPaymentDateNotInCurrent'] = 'Payment {0} : document date is not within the current year';
DataDictionaryEN['vldPaymentNoDetails'] = 'Payment {0} : document does not have any details';
DataDictionaryEN['vldApInvAccSetNoDeclaredPayablesAcc'] = 'Payment {0} : vendor account set has no declared payables account control';
DataDictionaryEN['vldApInvAccSetReceivableAccInactive'] = 'Payment {0} : vendor account set payables account control is inactive';
DataDictionaryEN['vldApInvNoDeclaredLibilityAccount'] = 'Payment {0} : vendor tax authority has no declared liability account';
DataDictionaryEN['vldPaymentAccNotExist'] = 'Payment {0} : account is not exists';
DataDictionaryEN['vldPaymentAccInactive'] = 'Payment {0} : account {1} is inactive';

///////////////////////////////////////////// Reports ///////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryEN['FromCurrency'] = 'From Currency';
DataDictionaryEN['ToCurrency'] = 'To Currency';
DataDictionaryEN['RateDateFrom'] = 'Rate Date From';
DataDictionaryEN['RateDateTo'] = 'Rate Date To';
DataDictionaryEN['CurrencyExchangeRates'] = 'Currency Exchange Rates';
DataDictionaryEN['Cashmanagmentreports'] = 'Cash Managment Reports';
DataDictionaryEN['Transactionlisting'] = 'Transaction listing';
DataDictionaryEN['BankReconciliation'] = 'Bank Reconciliation';
DataDictionaryEN['Accountreceivablereports'] = 'Account Receivable Reports';
DataDictionaryEN['Customeraging'] = 'Customer Aging';
DataDictionaryEN['CustomerStatement'] = 'Customer Statement';
DataDictionaryEN['CustomerTransactions'] = 'Customer Transactions';
DataDictionaryEN['Statmentdatefrom'] = 'Statement Date From';
DataDictionaryEN['Statmentdateto'] = 'Statment date to';
DataDictionaryEN['Batch'] = 'Batch';
DataDictionaryEN['Fromentrydate'] = 'From Entry Date';
DataDictionaryEN['Toentrydate'] = 'To Entry Date';
DataDictionaryEN['AgeingColumns'] = 'Ageing Columns:';
DataDictionaryEN['Current'] = 'Current';
DataDictionaryEN['days'] = 'days';
DataDictionaryEN['Customers'] = 'Customers';
DataDictionaryEN['Asofage'] = 'As of age';
DataDictionaryEN['CutoffDate'] = 'Cut off Date';
DataDictionaryEN['Includeallinvoices'] = 'Include all invoices';
DataDictionaryEN['VendorCurrency'] = 'Vendor Currency';
DataDictionaryEN['PrintAmountsIn'] = 'Print Amounts In';
DataDictionaryEN['Vendors'] = 'Vendors';
DataDictionaryEN['FromDocumentDate'] = 'From Document Date';
DataDictionaryEN['CustomerTransactionsReport'] = 'Customer Transactions Report';
DataDictionaryEN['CustomerStatementReport'] = 'Customer Statement Report';
DataDictionaryEN['CustomerAgingReport'] = 'Customer Aging Report';
DataDictionaryEN['Transactionlistingreport'] = 'Transaction listing report';
DataDictionaryEN['VendorTransactionsReport'] = 'Vendor Transactions Report';
DataDictionaryEN['VendorStatmentReport'] = 'Vendor Statment Report';
DataDictionaryEN['VendorAgingReport'] = 'Vendor Aging Report';
DataDictionaryEN['vldGreaterPeriod'] = 'must be greater than {0} period';
DataDictionaryEN['Period+1'] = 'Period+1';
DataDictionaryEN['Period+2'] = 'Period+2';
DataDictionaryEN['Period+3'] = 'Period+3';
DataDictionaryEN['Over'] = 'Over';
DataDictionaryEN['BankReconciliationreport'] = 'Bank Reconciliation report';
DataDictionaryEN['From'] = 'From';

///////////////////////////////////////////// Common ///////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryEN['OptionalField'] = 'Optional Field';
DataDictionaryEN['OptionalFieldDescription'] = 'Optional Field Description';
DataDictionaryEN['OptionalFieldDetails'] = 'Value';
DataDictionaryEN['OptionalFieldDetailsDescription'] = 'Value Description';
DataDictionaryEN['ClickToShowHide'] = '(Click to Show/Hide)';
DataDictionaryEN["SessionDate"] = 'Session Date';
DataDictionaryEN["TotalDiscountAmount"] = 'Total Discount Amount';


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////End ERP Section///////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************************************************************************************************************


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// IcAdditionalCostType English Dictionary////////////////////////////////////////////////
DataDictionaryEN["IcAdditionalCostType"] = 'Additional Cost Type';
DataDictionaryEN["AddIcAdditionalCostType"] = 'Add Additional Cost Type';
DataDictionaryEN['IcAdditionalCostTypeDetails'] = 'Additional Cost Type Details';
DataDictionaryEN['AdditionalCostType'] = 'Additional Cost Type';
DataDictionaryEN['AdditionalCost'] = 'Additional Cost';
DataDictionaryEN['Account'] = 'Account';
DataDictionaryEN['TaxClass'] = 'Tax Class';
DataDictionaryEN["AlreadyExist"] = ' Already Exist ';
DataDictionaryEN["IncludeExculdeProration"] = "Include Proration";
DataDictionaryEN["DocumentReference"] = "Document Reference";
DataDictionaryEN["ValidateLength"] = "Check Length";

DataDictionaryEN["IcReceiptReturnDetails"] = "Receipt Return";
DataDictionaryEN["AddIcReceiptReturn"] = "Add Receipt Return";
DataDictionaryEN["IcReceiptReturn"] = "Receipt Return";
DataDictionaryEN["ReturnReference"] = "Return Reference";
DataDictionaryEN["ParentReceipt"] = "ParentReceipt";
DataDictionaryEN["ReturnAllQuantity"] = "Return All Quantity";
DataDictionaryEN["Shipment"] = "Shipment";
DataDictionaryEN["ReturnedPrice"] = "Returned Price";
DataDictionaryEN["Return"] = "Return";


////////////////////////////////////////////////ItemSerials/////////////////////////////////////////////
DataDictionaryEN["ItemSerialsManagement"] = 'Item Serials';
DataDictionaryEN["HSerial"] = 'Serial: ';
DataDictionaryEN["GetTransactions"] = 'Transactions';
DataDictionaryEN["ItemSerialsManagementResults"] = 'Item Serials Results';
DataDictionaryEN["HAvgCost"] = 'Avg Cost';
DataDictionaryEN["TransactionId"] = 'Transaction Id';
DataDictionaryEN["SerialTransactions"] = 'Serial Transactions';


////////////////////////////////////////////////ItemLots/////////////////////////////////////////////
DataDictionaryEN["ItemLotManagement"] = 'Item Lots';
DataDictionaryEN["HLot"] = 'Lot: ';
DataDictionaryEN["ItemLotManagementResults"] = 'Item Lots Results';
DataDictionaryEN["LotTransactions"] = 'Lot Transactions';
DataDictionaryEN["LotNo"] = 'Lot No';
DataDictionaryEN["ExpiryDate"] = 'Expiry Date';
DataDictionaryEN["NextLotNumber"] = "Lot Number";
DataDictionaryEN["IcItemCardLotsAdd"] = "Add Item Lot";
DataDictionaryEN['IcLotNumberAlreadyExists'] = 'Lot Number already exists.';
DataDictionaryEN['IcMaximumNumberOfLotsVldMsg'] = 'Maximum number of Lot is ';
DataDictionaryEN['IcLotNumberAlreadyInserted'] = 'Lot Number has already been inserted.'
DataDictionaryEN['IcLotNumberNotAvailable'] = 'Lot Number doesn\'t exist/Not available.';
DataDictionaryEN["ItemLots"] = "Item Lots";


DataDictionaryEN["SystemAlert"] = 'System Alert';
DataDictionaryEN["QuantitySpecifiedExceedsAvailableQuantity"] = 'Quantity Specified Exceeds Available Serials, Please adjust the quantity then try again. Available Quantity: ';

DataDictionaryEN["NoRelatedSerialsFound"] = 'No Related Serials Found for this Tansaction';
DataDictionaryEN["vldLotAvailableQnty"] = 'Quantity more than lot available quantity';


DataDictionaryEN["ExpiryDateFrom"] = 'Expiry Date From: ';
DataDictionaryEN["ExpiryDateTo"] = 'Expiry Date To: ';

DataDictionaryEN["NoRelatedLotsFound"] = 'No Related Lots Found';
DataDictionaryEN["QuantitySpecifiedExceedsAvailableQuantityLots"] = 'Quantity Specified Exceeds Available Lots, Please adjust the quantity then try again. Available Quantity: ';

DataDictionaryEN["ReturnedQuantityIsGreaterThanQuantityError"] = 'Returned Quantity Cannot be greater than quantity';

DataDictionaryEN["AnItemCardCannotbeSerializedandLotted"] = 'An Item Card Cannot be Serialized and Lotted';
DataDictionaryEN["vldLotAvaliabiltyErrors"] = "The listed lots are not avaliable :";

DataDictionaryEN["MovingAverage"] = 'Moving Average';
DataDictionaryEN["vldUndefinedClosingAcc"] = "Insert Default Closing Account First";
DataDictionaryEN["IcReports"] = "Reports And Inquiries";


/////////////////////////////////////////////////////
/////////// Inventory control options

DataDictionaryEN["ICOptions"] = "Inventory Control Options";
DataDictionaryEN["processing"] = "Processing";
DataDictionaryEN["FunctionalCurrency"] = "Functional Currency";
DataDictionaryEN["DefaultRateType"] = "Default Rate Type";
DataDictionaryEN["MultiCurrency"] = "Multi Currency";
DataDictionaryEN["ValidateLotExpiryDate"] = "Validate Lot Expiry Date";
DataDictionaryEN["AllowFractionalQuantities"] = "Allow Fractional Quantities";
DataDictionaryEN["AlternateAmount1Name"] = "Alternate Amount 1 Name";
DataDictionaryEN["AllowItemsatAllLocations"] = "Allow Items at All Locations";
DataDictionaryEN["AlternateAmount2Name"] = "Alternate Amount 2 Name";
DataDictionaryEN["AllowNegativeInventoryLevels"] = "Allow Negative Inventory Levels";
DataDictionaryEN["BaseSegment"] = "Base Segment ";
DataDictionaryEN["AllowReceiptofNonStockItems"] = "Allow Receipt of Non Stock Items";
DataDictionaryEN["ActionAdditionalCostForItemsonReceiptReturns"] = "Action Additional Cost For Items on Receipt Returns";
DataDictionaryEN["KeepTransactionHistory"] = "Keep Transaction History";
DataDictionaryEN["DefaultGoodsinTransitLocation"] = "Default Goods in Transit Location";
DataDictionaryEN["PromptForDeleteDuringPosting"] = "Prompt For Delete During Posting";
DataDictionaryEN["DefaultPostingDate"] = "Default Posting Date ";
DataDictionaryEN["OnlyUseDefinedUOM"] = "Only Use Defined UOM";
DataDictionaryEN["BaseUOM"] = "Base UOM";
DataDictionaryEN["DefaultWeightUOM"] = "Default Weight UOM";
DataDictionaryEN["ItemStatistics"] = "Item Statistics";
DataDictionaryEN["KeepItemStatistics"] = "Keep Item Statistics";
DataDictionaryEN["AllowEditofStatistics"] = "Allow Edit of Statistics";
DataDictionaryEN["AccumulateBy"] = "Accumulate By";
DataDictionaryEN["PeriodType"] = "Period Type";
DataDictionaryEN["Costing"] = "Costing";
DataDictionaryEN["CostItemsDuring"] = "Cost Items During";
DataDictionaryEN["CreateSubledgerTransactionsandAuditInfo.During"] = "Create Subledger Transactions and Audit Info. During";
DataDictionaryEN["ItemTaxOptions"] = "Item Tax Options";
DataDictionaryEN["DefaultTaxAuthority"] = "Default Tax Authority";
DataDictionaryEN["DefaultPurchaseTaxClass"] = "Default Purchase Tax Class";
DataDictionaryEN["DefaultSalesTaxClass"] = "Default Sales Tax Class";
DataDictionaryEN["Confirm"] = "Confirm";

DataDictionaryEN["ExcludeZeroBalances"] = "Exclude Ending Zero Balances";


DataDictionaryEN["ReturnedQtyError"] = 'Returned Quantity for all selected items must be greater than 0';

DataDictionaryEN["APDocumentNumber"] = "Document #";

DataDictionaryEN["VldReceiptRowError"] = "Please enter all the mandatory fields, and related serials or lots in order to add new line";
DataDictionaryEN["ReceiptReturnNumber"] = "Return Number";
DataDictionaryEN["ReceiptReturnDate"] = " Return Date";
DataDictionaryEN['HReceiptReturnNumber'] = 'Return Number: ';
DataDictionaryEN['HReceiptREturnDate'] = 'Return Date: ';
DataDictionaryEN["AREntryNumber"] = "Document #";
DataDictionaryEN["ChequeReverse"] = "Cheque Reverse";


DataDictionaryEN["POS"] = 'Point Of Sale';

DataDictionaryEN["SyncConfiguration"] = 'Sync Configuration';
DataDictionaryEN["Store"] = 'Store';
DataDictionaryEN["Register"] = 'Register';
DataDictionaryEN["GiftVoucher"] = 'Gift Voucher';
DataDictionaryEN["GenerateBarcode"] = 'Generate Barcode';
DataDictionaryEN["RegisterStatus"] = 'Register Status';
DataDictionaryEN["QuickKeys"] = 'Quick Key';
DataDictionaryEN["BackEndSync"] = 'Back End Sync';
DataDictionaryEN["POSSync"] = 'POS Sync';
DataDictionaryEN["SyncSchedule"] = 'Sync Schedule';

DataDictionaryEN["POSSYNCHRONIZATION"] = 'POS SYNCHRONIZATION';

DataDictionaryEN["IsPosUser"] = 'Is POS User';

///////////////////////////////////////// Shifts English Dictionary////////////////////////////////////////////////
DataDictionaryEN["Shifts"] = 'Shifts';
DataDictionaryEN["AddShifts"] = 'Add Shifts';
DataDictionaryEN['ShiftsDetails'] = 'Shifts Details';
DataDictionaryEN['Shift'] = 'Shift';
DataDictionaryEN['ShiftName'] = 'Shift Name';
DataDictionaryEN['ShiftDescription'] = 'Shift Description';
DataDictionaryEN['StartTime'] = 'Start Time';
DataDictionaryEN['EndTime'] = 'End Time';
DataDictionaryEN['ValidateShift'] = 'Validate Shift';
DataDictionaryEN['HShiftName'] = 'Shift Name: ';
DataDictionaryEN['UserStoresShifts'] = 'User stores & Shifts';
DataDictionaryEN['DublicatedShiftName'] = 'Shift name already exist';
DataDictionaryEN['AddStoreAndShift'] = 'Add store and shift';
DataDictionaryEN['Dublicated'] = 'Shift already used';







////////////////////////////////////var DataDictionaryEN = new Array();

////////////////////////////////////////Linkes//////////////////////////////////////////////////////////////////////////
DataDictionaryEN["LinkLang"] = 'عربي';
DataDictionaryEN["Edit"] = 'Edit';
DataDictionaryEN["Save"] = 'Save';
DataDictionaryEN["Cancel"] = 'Cancel';
DataDictionaryEN["Close"] = 'Close';
DataDictionaryEN["Delete"] = 'Delete';
DataDictionaryEN["Clear"] = 'Clear';
DataDictionaryEN["Reassign"] = 'Reassign';
DataDictionaryEN["Back"] = 'Back';
DataDictionaryEN["Add"] = 'Add';
DataDictionaryEN["FaildToSave"] = 'failed to save';
DataDictionaryEN["SMNoDataFound"] = 'no data found';
DataDictionaryEN["RoleNotFound"] = 'role not found';
DataDictionaryEN["savedsuccessfully"] = 'saved successfully';
DataDictionaryEN["deletesuccessfully"] = 'deleted successfully';
DataDictionaryEN["Areyousure"] = 'Are you sure ?';
DataDictionaryEN["ConfirmMsgCancle"] = 'Cancel';
DataDictionaryEN["ConfirmMsgConfirm"] = 'Confirm';
DataDictionaryEN["ThisRecordIsLockedby"] = 'This record is locked by ';
DataDictionaryEN["at"] = ', at ';
DataDictionaryEN["on"] = ', on ';
DataDictionaryEN["systemError"] = 'system error';
DataDictionaryEN["RecordDeleted"] = 'Record Deleted';
DataDictionaryEN["RecordHasBeenDeleted"] = 'This record has been deleted, the window will be refreshed after you click ok.';
DataDictionaryEN["RecordChanged"] = 'Record Changed';
DataDictionaryEN["RecordHasBeenChanged"] = 'This record has been changed, the window will be refreshed after you click ok.';
DataDictionaryEN["DeleteSuperUserMsg"] = 'Supper user can not be deleted.';

DataDictionaryEN["YouDontHavePermissions"] = "You Dont Have Permissions for this page";
DataDictionaryEN["SessionExpired"] = "Your loggin session has been expired";

DataDictionaryEN["required"] = 'required';
DataDictionaryEN["numeric"] = 'numeric';
DataDictionaryEN["integer"] = 'integer';
DataDictionaryEN["lengthMustBe"] = 'length must be';
DataDictionaryEN["invalidEmail"] = 'invalid email';
DataDictionaryEN["invalidWebsite"] = 'invalid website';
DataDictionaryEN["HistoryUpdated"] = 'History Updated';
DataDictionaryEN["LastUpdatedDate"] = 'Last Updated Date';
DataDictionaryEN["LastUpdatedBy"] = 'Last Updated By';
DataDictionaryEN["True"] = 'True';
DataDictionaryEN["False"] = 'False';
DataDictionaryEN["MinLength"] = 'Minimum length is 3';
DataDictionaryEN["MaxLength"] = 'Maximum length is 100';
DataDictionaryEN["Ok"] = 'Ok';
DataDictionaryEN["DashBoard"] = 'Dashboard';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Setup Page section//////////////////////////////////////////////////////////////
DataDictionaryEN["SPSetup"] = 'Setup';
DataDictionaryEN["SPUserManagement"] = 'User Management';
DataDictionaryEN["SPUsers"] = 'Users';
DataDictionaryEN["SPRols"] = 'Roles';
DataDictionaryEN["SPTeams"] = 'Teams';
DataDictionaryEN["SPPermissions"] = 'Permissions';
DataDictionaryEN["SPDataAdminstration"] = 'Data Administration';
DataDictionaryEN["SPLovs"] = 'LOVs';
DataDictionaryEN["SPEntitylock"] = 'Entity lock';
DataDictionaryEN["SPInventory"] = 'Inventory';
DataDictionaryEN["SPSearchInventory"] = 'Search Inventory';
DataDictionaryEN["SPStockUpdate"] = 'Stock Update';
DataDictionaryEN["SPInventory"] = 'Inventory';
DataDictionaryEN["SReturnedItems"] = 'Returned Items-VO\'s';

DataDictionaryEN["SPEmailConfiguration"] = 'Email Configuration';
DataDictionaryEN["SPSMSConfiguration"] = 'SMS Configuration';
DataDictionaryEN["SPTemplates"] = 'Templates';
DataDictionaryEN["SPSendMassEmails"] = 'Send Mass Emails';
DataDictionaryEN["SPGroups"] = 'Groups';
DataDictionaryEN["Groups&Marketing"] = 'Groups & Marketing';
DataDictionaryEN["SPKit"] = 'Kit';
DataDictionaryEN["SPCategory"] = 'Category';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Page Titels///////////////////////////////////////////////////////////////////////
DataDictionaryEN["QuickSearch"] = 'Quick Search';
DataDictionaryEN["CompanyPageTitle"] = 'Company';
DataDictionaryEN["AddCompany"] = 'Add Company';
DataDictionaryEN["Total"] = 'Total  ';
DataDictionaryEN["of"] = '  of  ';
DataDictionaryEN["Showmore"] = 'Show more';
DataDictionaryEN["loading"] = 'loading...';
DataDictionaryEN["SlideDown"] = 'Slide Down';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Tabs Add Link///////////////////////////////////////////////////////////////////
DataDictionaryEN["AddPerson"] = 'Add Person';
DataDictionaryEN["NoDataFound"] = 'No Data Found';
DataDictionaryEN["AddnewCommunication"] = 'Add New Communication';
DataDictionaryEN["AddSales"] = 'Add Sales';
DataDictionaryEN["Addnewaddress"] = 'Add New Address';
DataDictionaryEN["newphone"] = 'New Phone';
DataDictionaryEN["Addnewnote"] = 'Add New Note';
DataDictionaryEN["Addnewemail"] = 'Add New Email';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Tabs Names//////////////////////////////////////////////////////////////////////
DataDictionaryEN["Summary"] = 'Summary';
DataDictionaryEN["Persons"] = 'Persons';
DataDictionaryEN["Communications"] = 'Communication';
DataDictionaryEN["Sales"] = 'Sales';
DataDictionaryEN["TradingTerms"] = 'Trading Terms';
DataDictionaryEN["Addresses"] = 'Addresses';
DataDictionaryEN["Phones"] = 'Phones';
DataDictionaryEN["Emails"] = 'Emails';
DataDictionaryEN["Attachments"] = 'Attachments';
DataDictionaryEN["Notes"] = 'Notes';
DataDictionaryEN["More"] = 'More';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Email Tab///////////////////////////////////////////////////////////////////////
DataDictionaryEN["EmailAddress"] = 'Email Address';
DataDictionaryEN["Type"] = 'Type';
DataDictionaryEN["Business"] = 'Business';
DataDictionaryEN["Personal"] = 'Personal';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Adress Tab///////////////////////////////////////////////////////////////////////
DataDictionaryEN["Country"] = 'Country';
DataDictionaryEN["City"] = 'City';
DataDictionaryEN["StreetAddress"] = 'Street Address';
DataDictionaryEN["NearBy"] = 'Near By';
DataDictionaryEN["Place"] = 'Place';
DataDictionaryEN["BuildingNumber"] = 'Building Number';
DataDictionaryEN["POBox"] = 'P.O.Box';
DataDictionaryEN["ZipCode"] = 'Zip Code/Postcode';
DataDictionaryEN["Region"] = 'Region';
DataDictionaryEN["Area"] = 'Area';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Attachment Tab///////////////////////////////////////////////////////////////////////
DataDictionaryEN["filesfromyourcomputer"] = 'files from your computer';
DataDictionaryEN["Dropfileshere"] = 'Drop files here';
DataDictionaryEN["Addfolder"] = 'Add folder';
DataDictionaryEN["DocumentRename"] = 'Rename';
DataDictionaryEN["DocumentDelete"] = 'Delete';
DataDictionaryEN["DeleteFolder"] = 'Delete Folder';
DataDictionaryEN["DeleteDocument"] = 'Delete Document';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Phone Tab///////////////////////////////////////////////////////////////////////
DataDictionaryEN["Number"] = 'Number';
DataDictionaryEN["Mobile"] = 'Mobile';
DataDictionaryEN["LandLine"] = 'Land Line';
DataDictionaryEN["Fax"] = 'Fax';
DataDictionaryEN["PhCountry"] = 'Country';
DataDictionaryEN["PhArea"] = 'Area';
DataDictionaryEN["PhPhone"] = 'Phone';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Note Tab///////////////////////////////////////////////////////////////////////
DataDictionaryEN["CreatedDate"] = 'Created Date';
DataDictionaryEN["CreatedBy"] = 'Created By';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Company View list and View Item//////////////////////////////////////////////////
DataDictionaryEN["Company"] = 'Company';
DataDictionaryEN["AddCompany"] = 'Add Company';
DataDictionaryEN['CompanyDetails'] = 'Company Details';
DataDictionaryEN['CompanyNameEnglish'] = 'Company Name English';
DataDictionaryEN['CompanyNameArabic'] = 'Company Name Arabic';
DataDictionaryEN['Website'] = 'Website';
DataDictionaryEN['CompanyType'] = 'Company Type';
DataDictionaryEN['DecisionMaker'] = 'Decision Maker';
DataDictionaryEN['SalesManAccountManager'] = 'Account Manager';
DataDictionaryEN['Abbreviation'] = 'Abbreviation';
DataDictionaryEN['RegistrationNumber'] = 'Registration Number';
DataDictionaryEN['RegistrationDate'] = 'Registration Date';
DataDictionaryEN['Segment'] = 'Segment';
DataDictionaryEN['Source'] = 'Source';
DataDictionaryEN['YearlyRevenue'] = 'Yearly Revenue';
DataDictionaryEN['EmployeeNumber'] = 'Number of employees';
DataDictionaryEN['Status'] = 'Status';
DataDictionaryEN['IsBranch'] = 'Is Branch ?';
DataDictionaryEN['ParentCompany'] = 'Parent Company';
DataDictionaryEN['PaymentTerms'] = 'Payment Terms';
DataDictionaryEN['PaymentType'] = 'Payment Type';
DataDictionaryEN["AlreadyExist"] = ' Already Exist ';
DataDictionaryEN["HeaderRegistrationNumber"] = 'Registration Number: ';
DataDictionaryEN["HeaderRegistrationDate"] = 'Registration Date: ';
DataDictionaryEN["DublicatatedName"] = 'Company name is already exist';
DataDictionaryEN["HeaderCompanyName"] = 'Company Name: ';
DataDictionaryEN["HeaderWebsite"] = 'Website: ';
DataDictionaryEN["CountryOfOrigion"] = 'Country Of Origion';
////Tab English Dictionary JS


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Company Person Tab//////////////////////////////////////////////////////////////
DataDictionaryEN["Company"] = 'Company';
DataDictionaryEN["Email"] = 'Email';
DataDictionaryEN["Mobile"] = 'Mobile';
DataDictionaryEN["LandLine"] = 'Land Line';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Company Communication Tab///////////////////////////////////////////////////////
DataDictionaryEN["StartDate"] = 'Start Date';
DataDictionaryEN["EndDate"] = 'End Date';
DataDictionaryEN["Type"] = 'Type';
DataDictionaryEN["Priority"] = 'Priority';
DataDictionaryEN["Status"] = 'Status';
DataDictionaryEN["Action"] = 'Action';
DataDictionaryEN["View"] = 'View';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Company Trading terms Tab///////////////////////////////////////////////////////
DataDictionaryEN["PaymentType"] = 'Payment Type';
DataDictionaryEN["PaymentTerm"] = 'Payment Term';
DataDictionaryEN["CreditLimit"] = 'Credit Limit';
DataDictionaryEN["CompanyNameInCommercialRegister"] = 'Company Name In Commercial Register';
DataDictionaryEN["TradeName"] = 'Trade Name';
DataDictionaryEN["CompanyNationalNumber"] = 'Company National Number';
DataDictionaryEN["OwnerName"] = 'Owner Name';
DataDictionaryEN["TradingTerms"] = 'Trading Terms';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Company Sales Tab///////////////////////////////////////////////////////////////
DataDictionaryEN["Company"] = 'Company';
DataDictionaryEN["InvoiceNumber"] = 'Invoice Number';
DataDictionaryEN["SalesRepresentative"] = 'Sales Representative';
DataDictionaryEN["Branch"] = 'Branch';
DataDictionaryEN["InvoiceDate"] = 'Invoice Date';
////////////////////////////////////////////////Common tabs/////////////////////////////////////////////////////////////
DataDictionaryEN['email'] = 'Email';
DataDictionaryEN['AddNewemail'] = 'Add New Email';
DataDictionaryEN['note'] = 'Note';
DataDictionaryEN['AddNewnote'] = 'Add New Note';
DataDictionaryEN['address'] = 'Address';
DataDictionaryEN['AddNewaddress'] = 'Add New Address';
DataDictionaryEN['phone'] = 'phone';
DataDictionaryEN['AddNewphone'] = 'Add New Phone';
DataDictionaryEN['Attach'] = 'Attachment';
DataDictionaryEN['AddNewAttach'] = 'Add New Attachment';
DataDictionaryEN['Communication'] = 'Communication';
DataDictionaryEN['AddNewCommunication'] = 'Add New Communication';

DataDictionaryEN['Persons'] = 'Persons';
DataDictionaryEN['AddNewPersons'] = 'Add New Person';
DataDictionaryEN['ProjectsOpporunities'] = 'Projects Opportunities';
DataDictionaryEN['AddNewProjectsOpporunities'] = 'Add New Opportunity';
DataDictionaryEN['Cases'] = 'Cases';
DataDictionaryEN['AddNewCase'] = 'Add New Case';
DataDictionaryEN['Appliances'] = 'Appliances';
DataDictionaryEN['AddNewAppliances'] = 'Add New Appliances';

DataDictionaryEN["PersonalEmail"] = 'Personal Email';
DataDictionaryEN["BusinessEmail"] = 'Business Email';
DataDictionaryEN["Email"] = 'Email';
DataDictionaryEN["Address"] = 'Address';
DataDictionaryEN["Country"] = 'Country';
DataDictionaryEN["City"] = 'City';
DataDictionaryEN["StreetAddress"] = 'Street Address';
DataDictionaryEN["Phone"] = 'Phone';
DataDictionaryEN["Home"] = 'Land Line';
DataDictionaryEN["Fax"] = 'Fax';
DataDictionaryEN["Mobile"] = 'Mobile';
DataDictionaryEN["NoResultsFound"] = 'No Results Found';
DataDictionaryEN["SearchTxtBox"] = "Search ...";
DataDictionaryEN["InvalidDateOfBirth"] = "Date of birth mustn't be in future";
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Person Section//////////////////////////////////////////////////////////////////
////////////////////////////////////////Person View list and View Item//////////////////////////////////////////////////

DataDictionaryEN["HeaderGender"] = 'Gender : ';
DataDictionaryEN["HeaderBirthDate"] = 'Birth Date : ';
DataDictionaryEN["DateOfBirthError"] = 'shouldn\'t be in future';
DataDictionaryEN["Person"] = 'Person';
DataDictionaryEN["AddPerson"] = 'Add Person';
DataDictionaryEN['PersonDetails'] = 'Person Details';
DataDictionaryEN['Company'] = 'Company';
DataDictionaryEN['FirstNameEn'] = 'First Name ';
DataDictionaryEN['MiddleNameEn'] = 'Middle Name';
DataDictionaryEN['LastNameEn'] = 'Last Name ';
DataDictionaryEN['FirstNameAr'] = 'First Name Arabic';
DataDictionaryEN['MiddleNameAr'] = 'Middle Name Arabic';
DataDictionaryEN['LastNameAr'] = 'Last Name Arabic';
DataDictionaryEN['FullNameEn'] = 'Full Name English';
DataDictionaryEN['FullNameAr'] = 'Full Name Arabic';
DataDictionaryEN['FullName'] = 'Full Name';
DataDictionaryEN['NickName'] = 'Nick Name';
DataDictionaryEN['SalesMan'] = 'Sales Man';
DataDictionaryEN['PersonType'] = 'Person Type';
DataDictionaryEN['PrimaryContactEmployee'] = 'Primary Contact Employee';
DataDictionaryEN['Salutation'] = 'Salutation';
DataDictionaryEN['Gender'] = 'Gender';
DataDictionaryEN['Source'] = 'Source';
DataDictionaryEN['DateOfBirth'] = 'Date Of Birth';
DataDictionaryEN['Religion'] = 'Religion';
DataDictionaryEN['MaritalStatus'] = 'Marital Status';
DataDictionaryEN['Department'] = 'Department';
DataDictionaryEN['Position'] = 'Position';
DataDictionaryEN['ContactImage'] = 'Contact Image';
DataDictionaryEN['CustomerReferralCompany'] = 'Customer Referral Company';
DataDictionaryEN['CustomerReferralPerson'] = 'Customer Referral Person';
DataDictionaryEN['EmployeeUser'] = 'Employee User';
DataDictionaryEN['ScoutingComments'] = 'Scouting Comments';
DataDictionaryEN['DublicatatedFullName'] = 'Person full name is already exist';
DataDictionaryEN['HeaderPersonName'] = 'Person Name: ';
DataDictionaryEN['HeaderPersonEmail'] = 'Business Email: ';

DataDictionaryEN['HearAboutUs'] = 'How did you hear about us';
DataDictionaryEN['BestTimeToContact'] = 'Best Time to Contact';
DataDictionaryEN['AccountManager'] = 'Account Manager';
DataDictionaryEN['IsVip'] = 'VIP Class';
DataDictionaryEN['VIPNormal'] = 'Normal';
DataDictionaryEN['FacebookPage'] = 'Facebook Page';
DataDictionaryEN['LinkedInPage'] = 'Linkedin page';
DataDictionaryEN['TwitterAccount'] = 'Twitter Account';
DataDictionaryEN['Website'] = 'website';
DataDictionaryEN["InvalidAnniversaryDate"] = "Anniversary mustn't be befor date of birth";



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Inventory and Search Inventory Section//////////////////////////////////////////
////////////////////////////////////////Inventory View list and View Item///////////////////////////////////////////////
DataDictionaryEN["InventorySearch"] = 'Inventory Search';
DataDictionaryEN["Search"] = 'Search';
DataDictionaryEN["AddInventory"] = 'Add Item';
DataDictionaryEN["Inventory"] = 'Inventory';
DataDictionaryEN["HeaderItemGroup"] = 'Item Group : ';
DataDictionaryEN["HeaderItemDescription"] = 'Item Description : ';
DataDictionaryEN["InventoryDetails"] = 'Inventory Details';
DataDictionaryEN["ItemGroup"] = 'Item Group';
DataDictionaryEN["ItemDescription"] = 'Item Description';
DataDictionaryEN["DimensionsLength"] = 'Dimensions Length';
DataDictionaryEN["DimensionsWidth"] = 'Dimensions Width';
DataDictionaryEN["ListPrice"] = 'List Price';
DataDictionaryEN["PriceWithoutSalesTax"] = 'Price Without Sales Tax';
DataDictionaryEN["AvailableQuantityShowroom"] = 'Available Quantity Showroom';
DataDictionaryEN["AvailableQuantityWarehouse"] = 'Available Quantity Warehouse';
DataDictionaryEN["TotalQuantity"] = 'Total Available Quantity ';
DataDictionaryEN["TotalOnSalesOrders"] = 'Quantity on Sales Orders';
DataDictionaryEN["SalesCommission"] = 'Sales Commission (JD)';
DataDictionaryEN["MaximumDiscount"] = 'Maximum Discount ';
DataDictionaryEN["SpecialOffer"] = 'Special Offer';
DataDictionaryEN["Yes"] = 'Yes';
DataDictionaryEN["No"] = 'No';

DataDictionaryEN["ItemType"] = 'Item Type';
DataDictionaryEN["ItemNumber"] = 'Item Number';


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Opportunity Section///////////////////////////////////////////////////////////////////
////////////////////////////////////////Opportunity View list and View Item///////////////////////////////////////////////////
DataDictionaryEN["Opportunity"] = 'Opportunity';
DataDictionaryEN["AddOpportunity"] = 'Add Opportunity';
DataDictionaryEN['OpportunityDetails'] = 'Opportunity Details';
DataDictionaryEN['Opportunity'] = 'Opportunity';
DataDictionaryEN['Company'] = 'Company';
DataDictionaryEN['Person'] = 'Person';
DataDictionaryEN['ApprovedContractorPerson'] = 'Approved Contractor Person';
DataDictionaryEN['ApprovedContractorCompany'] = 'Approved Contractor Company';
DataDictionaryEN['ProjectName'] = 'Project Name';
DataDictionaryEN['ProjectScope'] = 'Project Scope';
DataDictionaryEN['ProjectDetails'] = 'Project Details';
DataDictionaryEN['OpenedDate'] = 'Opened Date';
DataDictionaryEN['Certainty'] = 'Certainty %';
DataDictionaryEN['Details'] = 'Details';
DataDictionaryEN['MainProductInterestValues'] = 'Main Product Interest Values';
DataDictionaryEN['Source'] = 'Source';
DataDictionaryEN['DecisionTimeframe'] = 'Decision Timeframe';
DataDictionaryEN['Priority'] = 'Priority';
DataDictionaryEN['MainCompetitor'] = 'Main Competitor';
DataDictionaryEN['EstimatedClosedDate'] = 'Estimated Closed Date';
DataDictionaryEN['EstimatedEndDate'] = 'Estimated End Date';
DataDictionaryEN['EstimatedStartDate'] = 'Estimated Start Date';
DataDictionaryEN['Quotes'] = 'Quotes';
DataDictionaryEN['AddNewQuotes'] = 'Add New Quotes';
DataDictionaryEN['Orders'] = 'Orders';
DataDictionaryEN['AddNewOrders'] = 'Add New Orders';
DataDictionaryEN['Reports'] = 'Reports';
DataDictionaryEN['AddNewReports'] = 'Add New Reports';
DataDictionaryEN['Resources'] = 'Resources';
DataDictionaryEN['AddNewResources'] = 'Add New Resources';
DataDictionaryEN["CompanyOrPersonIsRequired"] = 'company or person is required';
DataDictionaryEN['ConsultantCompany'] = 'Consultant Company';
DataDictionaryEN['ConsultantPerson'] = 'Consultant Person';
DataDictionaryEN['ExpiredFixedAssetsReport'] = 'Expired Fixed Assets';
DataDictionaryEN['HeaderStatus'] = 'Status: ';
DataDictionaryEN['HeaderStage'] = 'Stage: ';
DataDictionaryEN['HeaderProjectName'] = 'Project Name: ';
DataDictionaryEN['PercentageValidation'] = 'Maximum value is 100';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////Opportunity Progress Links/////////////////////////////////////////////////////
DataDictionaryEN["SaleApproved"] = 'Sale Approved';
DataDictionaryEN["SaleLost"] = 'Sale Lost';
DataDictionaryEN["ReserveItems"] = 'Reserve Items';
DataDictionaryEN["Quoted"] = 'Quoted';
DataDictionaryEN["Delivered"] = 'Delivered';
DataDictionaryEN["Design"] = 'Design';
DataDictionaryEN["Completed"] = 'Completed';
DataDictionaryEN["TechnicalSubmitted"] = 'Technical Submitted';
DataDictionaryEN["Ordered"] = 'Ordered';

DataDictionaryEN["SaleWorkflow"] = 'Sale Work flow';
DataDictionaryEN["AddSalesOrderToContinue"] = 'Add Sales Order to continue';
DataDictionaryEN["OpportunityProgress"] = 'Opportunity Progress';
DataDictionaryEN["Stage"] = 'Stage';
DataDictionaryEN["Status"] = 'Status';
DataDictionaryEN["lblStageDefault"] = 'Lead';
DataDictionaryEN["lblStatusDefault"] = 'In Progress';
DataDictionaryEN["DeliveryDate"] = 'Delivery Date';
DataDictionaryEN["LostReason"] = 'Lost Reason';
DataDictionaryEN["TrackingNote"] = 'Tracking Note';

DataDictionaryEN['AssignedTo'] = 'Assigned To';
DataDictionaryEN['ContractDate'] = 'Contract Date';
DataDictionaryEN['ActualStartDate'] = 'Actual Start Date';
DataDictionaryEN['ActualEndDate'] = 'Actual End Date';
DataDictionaryEN['CompletionPercentage'] = 'Completion Percentage %';
DataDictionaryEN['DeliveryComments'] = 'Delivery Comments';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////Opportunity Tabs/////////////////////////////////////////////////////
DataDictionaryEN["Progress"] = 'Progress';
DataDictionaryEN["Orders"] = 'Orders';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Quotation Section///////////////////////////////////////////////////////////////////
////////////////////////////////////////Quotation List /////////////////////////////////////////////////////////////////////
DataDictionaryEN["Quotation"] = 'Quotation';
DataDictionaryEN["AddQuotation"] = 'Add Quotation';
DataDictionaryEN['QuotationDetails'] = 'Quotation Details';
DataDictionaryEN['Opportunity'] = 'Opportunity';
DataDictionaryEN['ReferenceNumber'] = 'Reference Number';
DataDictionaryEN['Currency'] = 'Currency';
DataDictionaryEN['PriceList'] = 'Price List';
DataDictionaryEN['ContractorPerson'] = 'Contractor Person';
DataDictionaryEN['ContractorCompany'] = 'Contractor Company';
DataDictionaryEN['TermsAndConditions'] = 'Terms And Conditions';
DataDictionaryEN['Attnto'] = 'Attn To';
DataDictionaryEN['EstimatedDeliveryDate'] = 'Estimated Delivery Date';
DataDictionaryEN['DurationOfScheduledVisitsInMonths'] = 'Duration Of Scheduled Visits In Months';
DataDictionaryEN['NumberOfScheduledVisits'] = 'Number Of Scheduled Visits';
DataDictionaryEN['TotalQuotePrice'] = 'Total Quote Price';
DataDictionaryEN['NetPrice'] = 'Net Price';
DataDictionaryEN['DiscountAmount'] = 'Discount Amount';
DataDictionaryEN['DiscountPercentage'] = 'Discount %';
DataDictionaryEN['AddNewOrders'] = 'Add New Orders';
DataDictionaryEN['HReferenceNo'] = 'Reference Number :';
DataDictionaryEN['HOpportunity'] = 'Opportunity :';
DataDictionaryEN['QuotationItems'] = 'Quotation Items';
DataDictionaryEN['Generated'] = 'Generated';
DataDictionaryEN['Generate'] = 'Generate ';
DataDictionaryEN['ActionConvertToOrder'] = 'Convert To Order';
DataDictionaryEN['Item'] = 'Item';
DataDictionaryEN['ReferenceNumber'] = 'Reference number ';
DataDictionaryEN['Location'] = 'Location';
DataDictionaryEN['Quantity'] = 'Quantity';
DataDictionaryEN['UnitPrice'] = 'Unit Price';
DataDictionaryEN['PercentageDiscount'] = 'Percentage discount';
DataDictionaryEN['ItemTotalprice'] = 'Item Total price';
DataDictionaryEN['TotalQuotePrice'] = 'Total Quote Price';
DataDictionaryEN['NetPrice'] = 'Net Price';
DataDictionaryEN['Generatedsuccessfully'] = 'Generated successfully';
DataDictionaryEN['QuotationOptionalItems'] = 'Quotation Optional Items';
DataDictionaryEN['OptionalItemsDiscountPercentage'] = 'Optional Items Discount Percentage';
DataDictionaryEN['OptionalItemsDiscountAmount'] = 'Optional Items Discount Amount';
DataDictionaryEN['OptionalItemsTotalQuotePrice'] = 'Optional Items Total Quote Price';
DataDictionaryEN['OptionalItemsNetPrice'] = 'Optional Items Net Price';
DataDictionaryEN['Validationerror'] = 'Validation error';
DataDictionaryEN['optionalItemsValidation'] = 'You did not add any optional Items, are you sure you want to save?';
DataDictionaryEN['TermsAndConditionsText'] = 'Terms and conditions text';
DataDictionaryEN['Clone'] = 'Clone';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////SalesOrder Section///////////////////////////////////////////////////////////////////
DataDictionaryEN["SalesOrder"] = 'SalesOrder';
DataDictionaryEN["AddSalesOrder"] = 'Add SalesOrder';
DataDictionaryEN['SalesOrderDetails'] = 'SalesOrder Details';
DataDictionaryEN['SalesOrder'] = 'Sales Order';
DataDictionaryEN['Qoutation'] = 'Quotation';
DataDictionaryEN['Opportunity'] = 'Opportunity';
DataDictionaryEN['ReferenceNumber'] = 'Reference Number';
DataDictionaryEN['Currency'] = 'Currency';
DataDictionaryEN['PriceList'] = 'Price List';
DataDictionaryEN['TermsAndConditions'] = 'Terms And Conditions';
DataDictionaryEN['Attnto'] = 'Attnto.';
DataDictionaryEN['EstimatedDeliveryDate'] = 'Estimated Delivery Date';
DataDictionaryEN['DurationOfScheduledVisitsInMonths'] = 'Duration of Scheduled Visits in months';
DataDictionaryEN['AgreedNumberofServiceVisits'] = 'Agreed Number of Service Visits';
DataDictionaryEN['TotalPrice'] = 'Total Price';
DataDictionaryEN['NetPrice'] = 'Net Price';
DataDictionaryEN['DiscountAmount'] = 'Discount Amount';
DataDictionaryEN['DiscountPercentage'] = 'Discount Percentage';
DataDictionaryEN['IsFinal'] = 'Is Final';
DataDictionaryEN['OptionalItemsTotalPrice'] = 'Optional Items Total Price';
DataDictionaryEN['OptionalItemsNetPrice'] = 'Optional Items Net Price';
DataDictionaryEN['OptionalItemsDiscountAmount'] = 'Optional Items Discount Amount';
DataDictionaryEN['OptionalItemsDiscountPercentage'] = 'Optional Items Discount Percentage';
DataDictionaryEN['VariationOrders'] = 'Variation Orders';
DataDictionaryEN['AddNewVariationOrders'] = 'Add New Variation Orders';
DataDictionaryEN['HQuotation'] = 'Quotation: ';
DataDictionaryEN['HFinal'] = 'Final Order';
DataDictionaryEN['MarkFinal'] = 'Mark Final';
DataDictionaryEN['MarkedFinalsuccessfully'] = 'Marked Final successfully';
DataDictionaryEN['OrderAlredayExist'] = 'The current quotation already has sales order';
DataDictionaryEN['CanNotDeleteGeneratedQuotation'] = 'Can not delete generated quotation';
DataDictionaryEN['CanNotEditGeneratedQuotation'] = 'Can not edit generated quotation';
DataDictionaryEN['quotationAlreadyGenerated'] = 'quotation already generated';
DataDictionaryEN['CanNoDeleteFinalOrder'] = 'Can not delete final Order';
DataDictionaryEN['CanNotEditFinalOrder'] = 'Can not edit final order';
DataDictionaryEN['OrderAlreadyMarkedAsFinal'] = 'Order already marked as final';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// OrderDeliveredItems English Dictionary////////////////////////////////////////////////
DataDictionaryEN["OrderDeliveredItems"] = 'Order Delivered Items';
DataDictionaryEN["AddOrderDeliveredItems"] = 'Add Order Delivered Items';
DataDictionaryEN['OrderDeliveredItemsDetails'] = 'Order Delivered Items Details';
DataDictionaryEN['DeliveredItem'] = 'Delivered Item';
DataDictionaryEN['Opportunity'] = 'Opportunity';
DataDictionaryEN['Stage'] = 'Stage';
DataDictionaryEN['Quotation'] = 'Quotation';
DataDictionaryEN['SalesOrder'] = 'Sales Order';
DataDictionaryEN['Item'] = 'Item';
DataDictionaryEN['Quantity'] = 'Quantity';
DataDictionaryEN['SerialNo'] = 'Serial No';
DataDictionaryEN['DeliveryDate'] = 'Delivery Date';
DataDictionaryEN['IsKit'] = 'Is Kit';
DataDictionaryEN['QuantityToDeliver'] = 'Quantity To Deliver';
DataDictionaryEN['OrderedQuantity'] = 'Ordered Quantity';
DataDictionaryEN['AvailableToDeliver'] = 'Available To Deliver';
DataDictionaryEN['ExceedsNeededQuantity'] = 'Exceeds needed quantity';
DataDictionaryEN['GreaterThanZero'] = 'should be greater than 0';
DataDictionaryEN['ItemsAlreadyAdded'] = 'Items already added';
DataDictionaryEN['SelectItems'] = 'Select Items to be delivered';
DataDictionaryEN['checkItemsQuantities'] = 'Check Items quantities';
DataDictionaryEN['ClickToAddItems'] = 'Click To Add Items';
DataDictionaryEN['NotAllItemsDelivered'] = 'Not all items delivered';
DataDictionaryEN['addItems'] = 'Delivered Items';
DataDictionaryEN['ThereIsNoAnyDeliveredItems'] = 'There is no any delivered items';
DataDictionaryEN['Report'] = 'Report';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Stock Section///////////////////////////////////////////////////////////////////
////////////////////////////////////////Stock List /////////////////////////////////////////////////////////////////////
DataDictionaryEN["StockUpdate"] = 'Stock Update';
DataDictionaryEN["StockUpdateDetails"] = 'Stock Update Details';
DataDictionaryEN["SItemGroup"] = 'Item Group';
DataDictionaryEN["SItem"] = 'Item Description';
DataDictionaryEN["TransactionType"] = 'Transaction Type';
DataDictionaryEN["NewStockAction"] = 'New Stock Action';
DataDictionaryEN["MoveFrom"] = 'Move From';
DataDictionaryEN["MoveTo"] = 'Move To';
DataDictionaryEN["QuantityAdded"] = 'Quantity Added';
DataDictionaryEN["TransactionDate"] = 'Date';
DataDictionaryEN["Details"] = 'Details';
DataDictionaryEN["AddStock"] = 'Add Stock';
DataDictionaryEN["NewItems"] = 'New Items';
DataDictionaryEN["MoveFromAndToShouldBeDifferant"] = 'Move From and Move to should be different';
DataDictionaryEN["WarehouseQuantityNotAvailable"] = 'This Quantity are not available in the warehouse';
DataDictionaryEN["ShowRoomQuantityNotAvailable"] = 'This Quantity are not available in the showroom';
DataDictionaryEN["ShouldBePositiveValueToBeMoved"] = 'Should be positive value to be moved';
DataDictionaryEN["ShouldBePositiveValueToBeAdded"] = 'Should be positive value to be added';
DataDictionaryEN["ShouldBeNegativeValueToBeRemoved"] = 'Should be negative value to be removed';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////Stock Links /////////////////////////////////////////////////////////////////////
DataDictionaryEN["Reset"] = 'Reset';
DataDictionaryEN["Search"] = 'Search';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////User section//////////////////////////////////////////////////////////////////
DataDictionaryEN["User"] = 'Users';
DataDictionaryEN["AddUser"] = 'Add New User';
DataDictionaryEN["UserDetails"] = 'User Details';
DataDictionaryEN["UUserName"] = 'User Name';
DataDictionaryEN["UFirstName"] = 'First Name';
DataDictionaryEN["UMiddleName"] = 'Middle Name';
DataDictionaryEN["ULastName"] = 'Last Name';
DataDictionaryEN["UPassword"] = 'Password';
DataDictionaryEN["UConfirmPassword"] = 'Confirm Password';
DataDictionaryEN["UTitle"] = 'Title';
DataDictionaryEN["DateOfBirth"] = 'Date Of Birth';
DataDictionaryEN["UEmail"] = 'Business Email';
DataDictionaryEN["URole"] = 'Role';
DataDictionaryEN["UTeam"] = 'Team';
DataDictionaryEN["UManager"] = 'Manager';
DataDictionaryEN["UIsDefault"] = 'Is Default';
DataDictionaryEN["UIsManager"] = 'Is Manager';
DataDictionaryEN["UNote"] = 'Note';
DataDictionaryEN["UHeaderUserName"] = 'User Name: ';
DataDictionaryEN["UHeaderName"] = 'Name: ';
DataDictionaryEN["UHeaderEMail"] = 'Business Email: ';
DataDictionaryEN["USpecialCharError"] = 'can\'t contain spaces or special charachters';
DataDictionaryEN["USPasswordConfirmation"] = 'password doesn\'t match with confirmation';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////Team section//////////////////////////////////////////////////////////////////
DataDictionaryEN["TManager"] = 'Manager';
DataDictionaryEN["TDescription"] = 'Description';
DataDictionaryEN["TeamsList"] = 'Teams List';
DataDictionaryEN["TArabicName"] = 'Arabic Name';
DataDictionaryEN["TName"] = 'Name';
DataDictionaryEN["TAddnewTeam"] = 'Add New Team';
DataDictionaryEN["TTeams"] = 'Teams';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////Role section//////////////////////////////////////////////////////////////////
DataDictionaryEN["Roles"] = 'Roles';
DataDictionaryEN["RDescription"] = 'Description';
DataDictionaryEN["RolesList"] = 'Roles List';
DataDictionaryEN["RArabicName"] = 'Arabic Name';
DataDictionaryEN["RName"] = 'Name';
DataDictionaryEN["RAddnewRole"] = 'Add New Role';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////Lovs section//////////////////////////////////////////////////////////////////
DataDictionaryEN["LOVs"] = 'LOVs';
DataDictionaryEN["LOVsValueAR"] = 'Value AR';
DataDictionaryEN["LOVsValue"] = 'Value';
DataDictionaryEN["LOVsContentType"] = 'Content Type';
DataDictionaryEN["AddnewLOV"] = 'Add New LOV';
DataDictionaryEN["SystemContentsCannotBeDeleted"] = 'System contents cannot be deleted !';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////Lock Entity section///////////////////////////////////////////////////////////
DataDictionaryEN["UnlockForAllUsers"] = 'Unlock for all users';
DataDictionaryEN["SelectUser"] = 'Select user';
DataDictionaryEN["Unlock"] = 'Unlock';
DataDictionaryEN["Date"] = 'Date';
DataDictionaryEN["LockedBy"] = 'LockedBy';
DataDictionaryEN["UnlockAll"] = 'Unlock All ';
DataDictionaryEN["UnlockedSuccessfully"] = 'Unlocked successfully';
DataDictionaryEN["FailedToUnlock"] = 'Failed to unlock';
DataDictionaryEN["EntityLock"] = 'Entity lock';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////Dash Board section///////////////////////////////////////////////////////////
DataDictionaryEN["ToDoList"] = 'To do list';
DataDictionaryEN["DBReferenceNo"] = 'Reference No.';
DataDictionaryEN["DBDetails"] = 'Details';
DataDictionaryEN["DBVisitType"] = 'Visit Type';
DataDictionaryEN["DBStatus"] = 'Status';
DataDictionaryEN["DBShowAll"] = 'Show all';
DataDictionaryEN["MyProfile"] = 'My Profile';
DataDictionaryEN["ChangePassword"] = 'Change password';
DataDictionaryEN["DBUserName"] = 'User Name';
DataDictionaryEN["DBFirstName"] = 'First Name';
DataDictionaryEN["DBMiddleName"] = 'Middle Name';
DataDictionaryEN["DBLastName"] = 'Last Name';
DataDictionaryEN["DBOldPassword"] = 'Old Password';
DataDictionaryEN["DBNewPassword"] = 'New Password';
DataDictionaryEN["DBConfirm"] = 'Confirm New Password';
DataDictionaryEN["DBTitle"] = 'Title';
DataDictionaryEN["DBManager"] = 'Manager';
DataDictionaryEN["DBDateOfBirth"] = 'Date Of Birth';
DataDictionaryEN["DBEmail"] = 'Email';
DataDictionaryEN["DBRole"] = 'Role';
DataDictionaryEN["DBTeam"] = 'Team';
DataDictionaryEN["DBManager"] = 'Manager';
DataDictionaryEN["DBIsManager"] = 'Is Manager';
DataDictionaryEN["DBSales"] = 'Sales';
DataDictionaryEN["DBInvoiceNo"] = 'Invoice No.';
DataDictionaryEN["DBCompany"] = 'Company';
DataDictionaryEN["DBPerson"] = 'Person';
DataDictionaryEN["DBBranch"] = 'Branch';
DataDictionaryEN["DBDescription"] = 'Description';
DataDictionaryEN["SalesGraph"] = 'Opportunity Graph';
DataDictionaryEN["DBClientCareCasesGraph"] = 'Client Care Cases Graph';
DataDictionaryEN["DBClientCareCase"] = 'Client Care Case';
DataDictionaryEN["DBType"] = 'Type';
DataDictionaryEN["DBDate"] = 'Created Date';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////Permission  section///////////////////////////////////////////////////////////
DataDictionaryEN["PRole"] = 'Role';
DataDictionaryEN["CollapseAll"] = 'Collapse all';
DataDictionaryEN["SelectAll"] = 'Select all';
DataDictionaryEN["UnSelectAll"] = 'Expand all';
DataDictionaryEN["Permissions"] = 'Permissions';
DataDictionaryEN["UnCollapseAll"] = 'UnCollapse all';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////SalesOrder  section///////////////////////////////////////////////////////////
DataDictionaryEN["SalesOrderDetails"] = 'Sales Order Details ';
DataDictionaryEN["SOOrderNumber"] = 'Order Number';
DataDictionaryEN["SOSales"] = 'Sales';
DataDictionaryEN["SOIsTaxExempt"] = 'Is Tax Exempt ?';
DataDictionaryEN["SOTotalInvoiceValue"] = 'Total Invoice Value';
DataDictionaryEN["SOComments"] = 'Other Comments';
DataDictionaryEN["SOSalesOrderItem"] = 'Sales Order Item';
DataDictionaryEN["SOItemGroup"] = 'Item Group';
DataDictionaryEN["SOItem"] = 'Item';
DataDictionaryEN["SODimensionsLength"] = 'Dimensions Length';
DataDictionaryEN["SODimensionsWidth"] = 'Dimensions Width';
DataDictionaryEN["SOListPrice"] = 'List Price';
DataDictionaryEN["SOPriceWithoutTax"] = 'Price Without Tax';
DataDictionaryEN["SOActualDimensionsLength"] = 'Actual Dimensions Length';
DataDictionaryEN["SOActualDimensionsWidth"] = 'Actual Dimensions Width';
DataDictionaryEN["SOQuantity"] = 'Quantity';
DataDictionaryEN["SOIsGiveaway"] = 'Is Giveaway ?';
DataDictionaryEN["SODiscountPercentage"] = 'Discount Percentage %';
DataDictionaryEN["SOMax"] = 'Max. % =';
DataDictionaryEN["SODiscountValue"] = 'Discount Value';
DataDictionaryEN["SONetPrice"] = 'Net Price Total';
DataDictionaryEN["SOInventoryType"] = 'Inventory Type';
DataDictionaryEN["SONotes"] = 'Notes';
DataDictionaryEN["SOListPricewithtax"] = 'List Price with tax';
DataDictionaryEN["SOListPricewithouttax"] = 'List Price without tax';
DataDictionaryEN["SOTotalPrice"] = 'Total Price';
DataDictionaryEN["SOSaleStage"] = 'Sales Stage';
DataDictionaryEN["couldNotSaveWithoutAddItemMsg"] = 'You should add at least one item to save';
DataDictionaryEN["addItemMsg"] = 'Add Item';
DataDictionaryEN["AddSalesOrder"] = 'Add Sales Order';
DataDictionaryEN["SalesOrder"] = 'Sales Order';
DataDictionaryEN["deleteNonLeadSaleSalesOrder"] = 'Can not delete non lead stage sales order';
DataDictionaryEN["InstallationOrder"] = 'تثبيت طلبيه';
DataDictionaryEN["ProductionOrder"] = 'تصنيع فرشات';
DataDictionaryEN["FrunitureInstallationOrder"] = 'تثبيت طلبية اثاث';
DataDictionaryEN["PostponeRequest"] = 'طلب تأجيل';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////Calender  section///////////////////////////////////////////////////////////
DataDictionaryEN["Rejected"] = 'Rejected';
DataDictionaryEN["Closed"] = 'Closed';
DataDictionaryEN["Inactive"] = 'Inactive';
DataDictionaryEN["Active"] = 'Active';

DataDictionaryEN["StatusProgress"] = 'Status Progress';
DataDictionaryEN["Calendar"] = 'Calendar';
DataDictionaryEN["AddTask"] = 'Add Task';
DataDictionaryEN["AllDay"] = 'all-day';
DataDictionaryEN["am"] = ' am';
DataDictionaryEN["pm"] = ' pm';
DataDictionaryEN["AM"] = ' AM';
DataDictionaryEN["PM"] = ' PM';
DataDictionaryEN["Today"] = 'Today';
DataDictionaryEN["Month"] = 'Month';
DataDictionaryEN["Week"] = 'Week';
DataDictionaryEN["Day"] = 'Day';
////////DaysShort
DataDictionaryEN["Sun"] = 'Sun';
DataDictionaryEN["Mon"] = 'Mon';
DataDictionaryEN["Tue"] = 'Tue';
DataDictionaryEN["Wed"] = 'Wed';
DataDictionaryEN["Thu"] = 'Thu';
DataDictionaryEN["Fri"] = 'Fri';
DataDictionaryEN["Sat"] = 'Sat';

////////Days
DataDictionaryEN["Sunday"] = 'Sunday';
DataDictionaryEN["Monday"] = 'Monday';
DataDictionaryEN["Tuesday"] = 'Tuesday';
DataDictionaryEN["Wednesday"] = 'Wednesday';
DataDictionaryEN["Thursday"] = 'Thursday';
DataDictionaryEN["Friday"] = 'Friday';
DataDictionaryEN["Saturday"] = 'Saturday';

////////monthNames
DataDictionaryEN["January"] = 'January';
DataDictionaryEN["February"] = 'February';
DataDictionaryEN["March"] = 'March';
DataDictionaryEN["April"] = 'April';
DataDictionaryEN["May"] = 'May';
DataDictionaryEN["June"] = 'June';
DataDictionaryEN["July"] = 'July';
DataDictionaryEN["August"] = 'August';
DataDictionaryEN["September"] = 'September';
DataDictionaryEN["October"] = 'October';
DataDictionaryEN["November"] = 'November';
DataDictionaryEN["December"] = 'December';



////////monthNamesShort
DataDictionaryEN["Jan"] = 'Jan';
DataDictionaryEN["Feb"] = 'Feb';
DataDictionaryEN["Mar"] = 'Mar';
DataDictionaryEN["Apr"] = 'Apr';
DataDictionaryEN["May"] = 'May';
DataDictionaryEN["Jun"] = 'Jun';
DataDictionaryEN["Jul"] = 'Jul';
DataDictionaryEN["Aug"] = 'Aug';
DataDictionaryEN["Sep"] = 'Sep';
DataDictionaryEN["Oct"] = 'Oct';
DataDictionaryEN["Nov"] = 'Nov';
DataDictionaryEN["Dec"] = 'Dec';

////////Dive Add Task
DataDictionaryEN["CalStartDate"] = 'Start Date';
DataDictionaryEN["CalEndDate"] = 'End Date';
DataDictionaryEN["CalType"] = 'Type';
DataDictionaryEN["CalPriority"] = 'Priority';
DataDictionaryEN["CalStatus"] = 'Status';
DataDictionaryEN["CalAction"] = 'Action';
DataDictionaryEN["CalVisitType"] = 'Visit Type';
DataDictionaryEN["CalArea"] = 'Area';
DataDictionaryEN["CalDescription"] = 'Description';
DataDictionaryEN["CalMeetingResults"] = 'Meeting Results';
DataDictionaryEN["CalHistoryUpdated"] = 'History Updated';
DataDictionaryEN["CalAssignedBy"] = 'AssignedBy';
DataDictionaryEN["CalCreatedDate"] = 'Created Date';
DataDictionaryEN["CalLastUpdatedBy"] = 'Last Updated By';
DataDictionaryEN["CalLastUpdatedDate"] = 'Last Updated Date';
DataDictionaryEN["CalCreatedBy"] = 'Created By';
DataDictionaryEN["CalRef-NO"] = 'Ref-NO';
DataDictionaryEN["CalClosedBy"] = 'Closed By';
DataDictionaryEN["CalClosedDate"] = 'Closed Date';
DataDictionaryEN["Discussion"] = 'Discussion';
DataDictionaryEN["DeleteTask"] = 'Delete Task';
DataDictionaryEN["FaildDeleteTask"] = 'faild to delete this task';
DataDictionaryEN["errorLoadingCalender"] = 'error while loading calendar';
DataDictionaryEN["CalSearch"] = 'Search';
DataDictionaryEN["CalAllTask"] = 'All Tasks';
DataDictionaryEN["CalByMe"] = 'By Me';
DataDictionaryEN["CalToMe"] = 'To Me';

DataDictionaryEN["ClAssignedTo"] = 'Assigned To';
DataDictionaryEN["AssignedBy"] = 'Assigned By';
DataDictionaryEN["Type"] = 'Type';
DataDictionaryEN["Priority"] = 'Priority';
DataDictionaryEN["DateFrom"] = 'Date From';
DataDictionaryEN["DateTo"] = 'Date To';
DataDictionaryEN["TimeFrom"] = 'Time From';
DataDictionaryEN["TimeTo"] = 'Time To';
DataDictionaryEN["ShowHideFilter"] = ' Advance search';
DataDictionaryEN["Assignment"] = 'Task Assignment';
DataDictionaryEN["TaskList"] = 'Task List';

DataDictionaryEN["CalStartDateTime"] = 'Start Date & Time';
DataDictionaryEN["CalEndDateTime"] = 'End Date & Time';
DataDictionaryEN["CalendarListView"] = 'List view';
DataDictionaryEN["CalendarView"] = 'Calendar view';
DataDictionaryEN["EndDateErrorMsg"] = 'End date should be equal or after the start date';
DataDictionaryEN["Print"] = 'Print';
DataDictionaryEN["CalenderDefultTaskStatus"] = 'Active';
DataDictionaryEN["CalenderDefultTaskType"] = 'Task';
DataDictionaryEN["CalenderDefultPriority"] = 'Normal';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////Advanced Search   section/////////////////////////////////////////////////////
DataDictionaryEN["AdvFilterYourResults"] = 'Filter your results';
DataDictionaryEN["AdvSortingField"] = 'Sorting field';
DataDictionaryEN["AdvAscending"] = 'Ascending';
DataDictionaryEN["AdvFrom"] = 'from';
DataDictionaryEN["AdvTo"] = 'to';
DataDictionaryEN["LnkReset"] = 'Reset';
DataDictionaryEN["LnkSearch"] = 'Search';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////OnScreen Notification section//////////////////////////////////////////////////
DataDictionaryEN["OnScreenNotTotal"] = 'Total ';
DataDictionaryEN["OnScreenOf"] = ' of ';
DataDictionaryEN["SnoozeAll"] = 'Snooze all ';
DataDictionaryEN["Snooze"] = 'Snooze';
DataDictionaryEN["Dismiss"] = 'Dismiss';
DataDictionaryEN["DismissAll"] = 'Dismiss all ';
DataDictionaryEN["Nmin"] = 'min ';
DataDictionaryEN["Nhour"] = 'hour';
DataDictionaryEN["Nday"] = 'day';
DataDictionaryEN["Nweek"] = 'week';
DataDictionaryEN["Totalof0"] = 'Total 0 of 0';
DataDictionaryEN["DismissAllConfirmation"] = 'Dismiss all listed notifications';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryEN["ChromeRecommend"] = 'For better performance, we recommend using Google Chrome browser';
/////////////////////////////////////////Report  section////////////////////////////////////////////////////////////////
DataDictionaryEN["DailySalesSummary"] = 'Daily Sales Summary';
DataDictionaryEN["OrderDateFrom"] = 'Order Date From';
DataDictionaryEN["OrderDateTo"] = 'Order Date To';
DataDictionaryEN["OpportunityNumber"] = 'Sales Number';
DataDictionaryEN["TotalAmount"] = 'Total Amount';
DataDictionaryEN["FilterYourResults"] = 'Filter your results';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////Client Care section////////////////////////////////////////////////////////////////
DataDictionaryEN["lblCaseType"] = 'Case Type';
DataDictionaryEN["lblReferenceNumber"] = 'Reference Number';
DataDictionaryEN["lblCasesDetails"] = 'Case Details';
DataDictionaryEN["lblCustomerMood"] = 'Customer Mood';
DataDictionaryEN["lblSale"] = 'Sale';
DataDictionaryEN["lblDateOfSale"] = 'Date Of Sale';
DataDictionaryEN["lblDateReceived"] = 'Date Received';
DataDictionaryEN["lblRequiredDeliveryDate"] = 'Required Delivery Date';
DataDictionaryEN["lblActualDeliveryDate"] = 'Actual Delivery Date';
DataDictionaryEN["lblDateOfWithdrawalOfGoods"] = 'Date Of Withdrawal Of Goods';
DataDictionaryEN["lblEffectiveDateOfTheWithdrawalOfGoods"] = 'Effective Date Of The Withdrawal Of Goods';
DataDictionaryEN["lblProposedSolution"] = 'Proposed Solution';
DataDictionaryEN["lblReason"] = 'Reason';
DataDictionaryEN["lblShowroomOffiecerComments"] = 'Showroom Officer Comments';
DataDictionaryEN["lblWarehouseManagerComments"] = 'Warehouse Manager Comments';
DataDictionaryEN["lblQAMangerComments"] = 'QA Manger Comments';
DataDictionaryEN["lblReasonRejected"] = 'Reason Rejected';
DataDictionaryEN["HeaderCaseType"] = 'Case Type: ';
DataDictionaryEN["HeaderStatus"] = 'Status: ';
DataDictionaryEN["AddCase"] = 'Add Case';
DataDictionaryEN["Cases"] = 'Cases';
DataDictionaryEN["StatusPending"] = 'Pending';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////Reports Section////////////////////////////////////////////////////////////////
DataDictionaryEN["btnSearch"] = 'Search';
DataDictionaryEN["lnkReset"] = 'Reset';
DataDictionaryEN["CreateDateFrom"] = 'Create Date From';
DataDictionaryEN["CreateDateTo"] = 'Create Date To';
DataDictionaryEN["BusinessEmail"] = 'Business Email';
DataDictionaryEN["PersonalEmail"] = 'Personal Email';

/////////////////////////////////////////Company Report
DataDictionaryEN["CompanyName"] = 'Company Name';
DataDictionaryEN["Type"] = 'Type';
DataDictionaryEN["Status"] = 'Status';
DataDictionaryEN["Active"] = 'Active';
DataDictionaryEN["Inactive"] = 'Inactive';
DataDictionaryEN["WebSite"] = 'WebSite';
DataDictionaryEN["LandLineNumber"] = 'Land Line';
DataDictionaryEN["CompaniesReport"] = 'Full Customers List (Companies)';
/////////////////////////////////////////Person Report
DataDictionaryEN["FirstName"] = 'First Name';
DataDictionaryEN["LastName"] = 'Last Name';
DataDictionaryEN["MobileNumber"] = 'Mobile Number';
DataDictionaryEN["Gender"] = 'Gender';
DataDictionaryEN["MaritalStatus"] = 'Marital Status';
DataDictionaryEN["Nationality"] = 'Nationality';
DataDictionaryEN["Country"] = 'Country';
DataDictionaryEN["City"] = 'City';
DataDictionaryEN["PersonsReport"] = 'Full Customers List (Persons)';

/////////////////////////////////////////StockUpdate Report
DataDictionaryEN["TransactionDateFrom"] = 'Transaction Date From';
DataDictionaryEN["TransactionDateTo"] = 'Transaction Date To';
DataDictionaryEN["TransactionType"] = 'Transaction Type';
DataDictionaryEN["NewStockAction"] = 'New Stock Action';
DataDictionaryEN["MoveFrom"] = 'Move From';
DataDictionaryEN["MoveTo"] = 'Move To';
DataDictionaryEN["QuantityAdded"] = 'Quantity Added';
DataDictionaryEN["Details"] = 'Details';
DataDictionaryEN["StockUpdateReportTitle"] = 'Stock Update History';
/////////////////////////////////////////SoldPieces Report
DataDictionaryEN["SoldPiecesReportTitle"] = 'Sold Pieces Per Month';
/////////////////////////////////////////Calender Report
DataDictionaryEN["CalenderReportTitle"] = 'تقارير الزيارات و الأجراءات اليومية';
DataDictionaryEN["StartDateFrom"] = 'Start Date From';
DataDictionaryEN["StartDateTo"] = 'Start Date To';
DataDictionaryEN["ReferenceNo"] = 'Reference No';
DataDictionaryEN["StartTimeFrom"] = 'Start Time From';
DataDictionaryEN["StartTimeTo"] = 'Start Time To';
DataDictionaryEN["Area"] = 'Area';
DataDictionaryEN["EndDateFrom"] = 'End Date From';
DataDictionaryEN["EndDateTo"] = 'End Date To';
DataDictionaryEN["EndTimeFrom"] = 'End Time From';
DataDictionaryEN["EndTimeTo"] = 'End Time To';
DataDictionaryEN["VisitType"] = 'Visit Type';
DataDictionaryEN["Actions"] = 'Actions';
DataDictionaryEN["AssignedTo"] = 'Assigned To';
DataDictionaryEN["TaskType"] = 'Report Type';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////SMS Configuration  section////////////////////////////////////////////////////////////////
DataDictionaryEN["SMSConfigurationDetails"] = 'SMS Configuration Details';
DataDictionaryEN["AddParameter"] = 'Add Parameter';
DataDictionaryEN["Name"] = 'Name';
DataDictionaryEN["SMSUrl"] = 'SMS Url';
DataDictionaryEN["Key"] = 'Key';
DataDictionaryEN["Value"] = 'Value';
DataDictionaryEN["Option"] = 'Option';
DataDictionaryEN["HName"] = 'Name :';
DataDictionaryEN["HDescription"] = 'Description :';
DataDictionaryEN["AddSMSConfiguration"] = 'Add SMS Configuration';
DataDictionaryEN["SMSConfiguration"] = 'SMS Configuration';
DataDictionaryEN["Remove"] = 'Remove';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////Template Details  section////////////////////////////////////////////////////////////////
DataDictionaryEN["TemplateDetails"] = 'Template Details';
DataDictionaryEN["TemplateName"] = 'Template Name';
DataDictionaryEN["Type"] = 'Type';
DataDictionaryEN["Email"] = 'Email';
DataDictionaryEN["SMS"] = 'SMS';
DataDictionaryEN["TermsAndCondition"] = 'Terms and conditions';
DataDictionaryEN["Description"] = 'Description';
DataDictionaryEN["Template"] = 'Template';
DataDictionaryEN["SMSTemplate"] = 'SMS Template';
DataDictionaryEN["maximumLengthForArabic"] = '** The maximum length for arabic characters is 110';
DataDictionaryEN["maximumLengthForEnglish"] = '** The maximum length for english characters is 300';
DataDictionaryEN["HTemplateName"] = 'Template Name : ';
DataDictionaryEN["HType"] = 'Type : ';
DataDictionaryEN["NotificationTemplates"] = 'Notification Templates';
DataDictionaryEN["AddTemplate"] = 'Add Template';
DataDictionaryEN["TemplateErrorMsg"] = "Please insert template text";
DataDictionaryEN["Error"] = "Error";
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////Send Notification section////////////////////////////////////////////////////////
DataDictionaryEN["Details"] = 'Details';
DataDictionaryEN["Subject"] = 'Subject';
DataDictionaryEN["Description"] = 'Description';
DataDictionaryEN["Company"] = 'Company';
DataDictionaryEN["Person"] = 'Person';
DataDictionaryEN["Group"] = 'Group';
DataDictionaryEN["All"] = 'All';
DataDictionaryEN["Email&Template"] = 'Email & Template';
DataDictionaryEN["Type"] = 'Type';
DataDictionaryEN["Email"] = 'Email';
DataDictionaryEN["EmailType"] = 'Email Type';
DataDictionaryEN["SMS"] = 'SMS';
DataDictionaryEN["BusinessEmail"] = 'Business Email';
DataDictionaryEN["PersonalEmail"] = 'Personal Email';
DataDictionaryEN["Both"] = 'Both';
DataDictionaryEN["Template"] = 'Template';
DataDictionaryEN["SendSMSAndEmails"] = 'Send sms and emails';
DataDictionaryEN["Your"] = 'Your ';
DataDictionaryEN["email"] = 'email';
DataDictionaryEN["sms"] = 'sms';
DataDictionaryEN["hasBeenSentSuccessfully"] = ' has been sent successfully';
DataDictionaryEN["pleaseSelectCompanyOrPersonOrGroup"] = 'please select company or person or group';
DataDictionaryEN["Required"] = 'Required';
DataDictionaryEN["Contacts"] = 'Contacts';
DataDictionaryEN["Send"] = 'Send';
DataDictionaryEN["Reset"] = 'Reset';
DataDictionaryEN["GroupDescriptionName"] = 'Description';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////Email Configuration section////////////////////////////////////////////////////////
DataDictionaryEN["EmailConfigurationDetails"] = 'Email Configuration Details';
DataDictionaryEN["EmailConfiguration"] = 'Email Configuration';
DataDictionaryEN["AddEmailConfiguration"] = 'Add Email Configuration';
DataDictionaryEN["Email"] = 'Email';
DataDictionaryEN["DisplayName"] = 'Display Name';
DataDictionaryEN["UserName"] = 'User Name';
DataDictionaryEN["Password"] = 'Password';
DataDictionaryEN["Host"] = 'Host';
DataDictionaryEN["Port"] = 'Port';
DataDictionaryEN["EnableSSL"] = 'Enable SSL';
DataDictionaryEN["Timeout"] = 'Timeout';
DataDictionaryEN["SendInternalNotifications"] = 'Send Internal Notifications';
DataDictionaryEN["Description"] = 'Description';
DataDictionaryEN["HEmail"] = 'Email : ';
DataDictionaryEN["HDisplayName"] = 'Display Name : ';
DataDictionaryEN["Description"] = 'Description';
DataDictionaryEN["AlreadyExists"] = 'Already exists';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////Group section////////////////////////////////////////////////////////
DataDictionaryEN["GroupDetails"] = 'Group Details';
DataDictionaryEN["FullView"] = 'Full View';
DataDictionaryEN["GroupName"] = 'Group Name';
DataDictionaryEN["Entity"] = 'Entity';
DataDictionaryEN["Description"] = 'Description';
DataDictionaryEN["HEntity"] = 'Entity';
DataDictionaryEN["HGroupName"] = 'Group Name';
DataDictionaryEN["ContactsGroups"] = 'Contacts Groups';
DataDictionaryEN["AddContactsGroup"] = 'Add a new group';
DataDictionaryEN["ShowResult"] = 'Show Result';
DataDictionaryEN["SearchCriteria"] = 'Search Criteria';
DataDictionaryEN["Field"] = 'Field';
DataDictionaryEN["Value1"] = 'Value 1';
DataDictionaryEN["Value2"] = 'Value 2';
DataDictionaryEN["Yes"] = 'Yes';
DataDictionaryEN["No"] = 'No';
DataDictionaryEN["True"] = 'True';
DataDictionaryEN["False"] = 'False';
DataDictionaryEN["All"] = 'All';
DataDictionaryEN["Filter"] = 'Apply';
DataDictionaryEN["Operation"] = 'Operation';
DataDictionaryEN["Group"] = 'Group';
DataDictionaryEN["Options"] = 'Options';
DataDictionaryEN["AND"] = 'AND';
DataDictionaryEN["OR"] = 'OR';
DataDictionaryEN["Start"] = 'Start';
DataDictionaryEN["End"] = 'End';
DataDictionaryEN["Fields"] = 'Fields';
DataDictionaryEN["Add"] = 'Add';
DataDictionaryEN["Clear"] = 'Clear';

DataDictionaryEN["GroupContacts"] = 'Group Contacts';
DataDictionaryEN["Mobile"] = 'Mobile';
DataDictionaryEN["BusinessEmail"] = 'Business Email';
DataDictionaryEN["ContactName"] = 'Contact Name';
DataDictionaryEN["Contacts"] = 'Contacts';
DataDictionaryEN["BracketsCountError"] = 'Brackets Count Error';
DataDictionaryEN["BracketsCountErrorMsg"] = 'No. of opened bracket doesn\'t match no. of closed brackets';
DataDictionaryEN["BracketsError"] = 'Brackets Error';
DataDictionaryEN["BracketsCombinationError"] = 'Brackets combination error';
DataDictionaryEN["Equal"] = 'Equal';
DataDictionaryEN["Contains"] = 'Contains';
DataDictionaryEN["StartWith"] = 'Start With';
DataDictionaryEN["EndWith"] = 'End With';
DataDictionaryEN["Between"] = 'Between';
DataDictionaryEN["Before"] = 'Before';
DataDictionaryEN["After"] = 'After';
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// ReturnedItem////////////////////////////////////////////////
DataDictionaryEN["ReturnedItem"] = 'Returned Item-VO\'s';
DataDictionaryEN["AddReturnedItem"] = 'Add Returned Item-VO\'s';
DataDictionaryEN['ReturnedItemDetails'] = 'Returned Item-VO\'s Details';
DataDictionaryEN['ReturnedItem'] = 'Returned Item-VO\'s';
DataDictionaryEN['SalesOrder'] = 'Sales Order';
DataDictionaryEN['OrderNumber'] = 'Order Number';
DataDictionaryEN['ReturnReason'] = 'Return Reason';

DataDictionaryEN["AlreadyExist"] = ' Already Exist ';

DataDictionaryEN["TotalReturnedItems"] = 'Total of Returned Items';
DataDictionaryEN["ModelNumber"] = 'Model Number';
DataDictionaryEN["ProductName"] = 'Product Name';
DataDictionaryEN["Quantity"] = 'Quantity';
DataDictionaryEN["ReturnQuantity"] = 'Return Quantity';
DataDictionaryEN["ReturnLocation"] = 'Return Location';
DataDictionaryEN["ErrorAddReturndItem"] = 'Can\'t add returned item';
DataDictionaryEN['Details'] = 'Details';
DataDictionaryEN['AddNewDetails'] = 'Add New Details';
DataDictionaryEN['TotalReturendItems'] = 'Total';
DataDictionaryEN['DeliveredQuantity'] = 'Delivered Quantity';
DataDictionaryEN['QuantityError'] = 'mustn\'t exceed delivered quantity';


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////Inventory///////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryEN["Inventory"] = 'Inventory';
DataDictionaryEN["AddInventory"] = 'Add Inventory';
DataDictionaryEN['InventoryDetails'] = 'Inventory Details';
DataDictionaryEN['Inventory'] = 'Inventory';
DataDictionaryEN['ProductName'] = 'Product Name';
DataDictionaryEN['ProductNameAr'] = 'Product Name Ar';
DataDictionaryEN['Description'] = 'Description';
DataDictionaryEN['Image'] = 'Image';
DataDictionaryEN['Cost'] = 'Cost';
DataDictionaryEN['Unit'] = 'Unit';
DataDictionaryEN['DefaultWarrantyInMonths'] = 'Default Warranty In Months';
DataDictionaryEN['ModelNumber'] = 'Model Number';
DataDictionaryEN['AvailableQuantity'] = 'Available Quantity';
DataDictionaryEN['AvailableQuantityBonded'] = 'Available Quantity Bonded';
DataDictionaryEN['AvailableQuantityKhalda'] = 'Available Quantity Khalda';
DataDictionaryEN['QuantityOnSalesOrder'] = 'Quantity On Sales Order';
DataDictionaryEN['QuantityOrderedOnWay'] = 'Quantity Ordered On Way';
DataDictionaryEN['SuspendedItem'] = 'Suspended Item';
DataDictionaryEN['Sku'] = 'SKU';
DataDictionaryEN['RequireSerialNumberAtDelivery'] = 'Require Serial Number At Delivery';
DataDictionaryEN['StockableItem'] = 'Stockable Item';
DataDictionaryEN['ArabicDescription'] = 'Arabic Description';
DataDictionaryEN['ProductCategory'] = 'Product Category';
DataDictionaryEN['ProductSubCategory'] = 'Product Sub Category';
DataDictionaryEN['ParentProduct'] = 'Product Parent';
DataDictionaryEN["AlreadyExist"] = ' Already Exist ';
DataDictionaryEN["ImageFormatIsNotAllowed"] = ' Image format allowed ';
DataDictionaryEN['PriceList'] = 'Price list';
DataDictionaryEN['AddNewPricelist'] = 'Add New Price list';
DataDictionaryEN['ImageSizeError'] = 'Image size error';
DataDictionaryEN['MaximumImageSize'] = 'Maximum size is';
DataDictionaryEN['kb'] = ' kb';

// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ///////////////////////////////////////// InventoryKit English Dictionary////////////////////////////////////////////////
DataDictionaryEN['ModelNumber'] = 'Model Number';
DataDictionaryEN['Inventory'] = 'Product';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// /////////////////////////////////////////InventoryPriceList///////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryEN["InventoryPriceList"] = 'Inventory Price List';
DataDictionaryEN["AddInventoryPriceList"] = 'Add Inventory Price List';
DataDictionaryEN['InventoryPriceListDetails'] = 'Inventory Price List Details';
DataDictionaryEN['InventoryPriceList'] = 'Inventory Price List';
DataDictionaryEN['PriceList'] = 'Price List';
DataDictionaryEN['Currency'] = 'Currency';
DataDictionaryEN['ListedPrice'] = 'Listed Price';
DataDictionaryEN['MinimumPrice'] = 'Minimum Price';
DataDictionaryEN['MaximumPrice'] = 'Maximum Price';
DataDictionaryEN['Inventory'] = 'Inventory';
DataDictionaryEN["AlreadyExist"] = ' Already Exist ';
DataDictionaryEN['ValidatePrice'] = ' Must be less than maximum price';


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// InventoryStock////////////////////////////////////////////////
DataDictionaryEN["InventoryStock"] = 'Inventory Stock';
DataDictionaryEN["AddInventoryStock"] = 'Add Inventory Stock';
DataDictionaryEN['InventoryStockDetails'] = 'Inventory Stock Update';
DataDictionaryEN['InventoryStock'] = 'Inventory Stock';
DataDictionaryEN['Inventory'] = 'Product / Model number';
DataDictionaryEN['TransactionType'] = 'Transaction Type';
DataDictionaryEN['MoveFrom'] = 'Move From';
DataDictionaryEN['MoveTo'] = 'Move To';
DataDictionaryEN['Quantity'] = 'Quantity';
DataDictionaryEN['Details'] = 'Details';
DataDictionaryEN['Stock'] = 'Stock';
DataDictionaryEN["AlreadyExist"] = ' Already Exist ';
DataDictionaryEN['QtyValidation'] = 'Quantity musn\'t be more than availabe quantity ';
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////Kit////////////////////////////////////////////////
DataDictionaryEN["Kit"] = 'Kit';
DataDictionaryEN["AddKit"] = 'Add Kit';
DataDictionaryEN['KitDetails'] = 'Kit Details';
DataDictionaryEN['Kit'] = 'Kit';
DataDictionaryEN['KitName'] = 'Kit Name';
DataDictionaryEN['KitDescription'] = 'Kit Description';
DataDictionaryEN['KitModelNumber'] = 'Kit Model Number';
DataDictionaryEN["AlreadyExist"] = ' Already Exist ';
DataDictionaryEN['Inventory'] = 'Inventory';
DataDictionaryEN['AddNewInventory'] = 'Add New Inventory';
DataDictionaryEN['Pricelist'] = 'Price list';
DataDictionaryEN['AddNewPricelist'] = 'Add New Price list';
DataDictionaryEN['Product'] = 'Product/Model number';



// /////////////////////////////////////////KitPriceList///////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryEN["InventoryPriceList"] = 'Kit Price List';
DataDictionaryEN["AddInventoryPriceList"] = 'Add Kit Price List';
DataDictionaryEN['InventoryPriceListDetails'] = 'Kit Price List Details';
DataDictionaryEN['InventoryPriceList'] = 'Kit Price List';
DataDictionaryEN['PriceList'] = 'Price List';
DataDictionaryEN['Currency'] = 'Currency';
DataDictionaryEN['ListedPrice'] = 'Listed Price';
DataDictionaryEN['MinimumPrice'] = 'Minimum Price';
DataDictionaryEN['MaximumPrice'] = 'Maximum Price';
DataDictionaryEN['Inventory'] = 'Product';
DataDictionaryEN["AlreadyExist"] = ' Already Exist ';
DataDictionaryEN['ValidatePrice'] = ' Must be less than maximum price';



// ////////////////////////////////////////Category///////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryEN["Category"] = 'Category';
DataDictionaryEN["AddCategory"] = 'Add Category';
DataDictionaryEN['CategoryDetails'] = 'Category Details';
DataDictionaryEN['Category'] = 'Category';
DataDictionaryEN['CategoryNameEn'] = 'Category Name';
DataDictionaryEN['CategoryNameAr'] = 'Category Name Arabic';
DataDictionaryEN['DescriptionAr'] = 'Description Arabic';
DataDictionaryEN['DescriptionEn'] = 'Description';
DataDictionaryEN['Entity'] = 'Entity';
DataDictionaryEN["AlreadyExist"] = ' Already Exist ';
DataDictionaryEN['CategoryNameEn'] = 'Category Name';
DataDictionaryEN['CategoryNameAr'] = 'Category Name Arabic';


// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// /////////////////////////////////////////SubCategory///////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryEN["SubCategory"] = 'Sub Category';
DataDictionaryEN["AddSubCategory"] = 'Add Sub Category';
DataDictionaryEN['SubCategoryDetails'] = 'Sub Category Details';
DataDictionaryEN['SubCategory'] = 'Sub Category';
DataDictionaryEN['SubCategoryValueAr'] = 'Sub Category Value Arabic';
DataDictionaryEN['SubCategoryValueEn'] = 'Sub Category Value';
DataDictionaryEN['DescriptionAr'] = 'Description Arabic';
DataDictionaryEN['DescriptionEn'] = 'Description';
DataDictionaryEN['Parent'] = 'Parent';
DataDictionaryEN["ParentSubCategory"] = 'Parent';
DataDictionaryEN["AlreadyExist"] = ' Already Exist ';
DataDictionaryEN['SubCategoryValueAr'] = 'Sub Category Value Arabic';
DataDictionaryEN['SubCategoryValueEn'] = 'Sub Category Value';
DataDictionaryEN['SubCategoryEn'] = 'Sub Category';
DataDictionaryEN['AddNewSubCategory'] = 'Add Sub Category';

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////Fixed Assets///////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryEN["AddFixedAssets"] = 'Add Fixed Assets';
DataDictionaryEN["TypeName"] = 'Type Name';
DataDictionaryEN["Warranty"] = 'Warranty';
DataDictionaryEN["Model"] = 'Model';
DataDictionaryEN["Location"] = 'Location';
DataDictionaryEN["SerialNumber"] = 'Serial Number';
DataDictionaryEN["License"] = 'License';
DataDictionaryEN["ProcurementDate"] = 'Procurement Date';
DataDictionaryEN["LicenseExpirationDate"] = 'License Expiration Date';
DataDictionaryEN["Notes"] = 'Notes';
DataDictionaryEN["FixedAssetsDetails"] = 'Fixed Assets Details';
DataDictionaryEN["FixedAssets"] = 'Fixed Assets';
DataDictionaryEN["TypeNameHeader"] = " Type Name: ";
DataDictionaryEN["SerialNumberHeader"] = " Serial Number: ";
DataDictionaryEN["ProcurementDateValidation"] = 'Procurement date should be before the license expiration date';

////////////////////////////////////////////////////////////////////////////////
///////////////////////////////Reports/////////////////////////////////////////
DataDictionaryEN["CompanyListReport"] = "Full Customers List (Companies)";
DataDictionaryEN["PersonName"] = " English Person Name";
DataDictionaryEN["PersonNameAr"] = "Arabic Person Name"
DataDictionaryEN["PersonListReport"] = "Full Customers List (Persons)";
DataDictionaryEN["CompanyListReport"] = "Full Customers List (Companies)";
DataDictionaryEN["PersonName"] = " English Person Name";
DataDictionaryEN["PersonNameAr"] = "Arabic Person Name"
DataDictionaryEN["PersonListReport"] = "Full Customers List (Persons)";
DataDictionaryEN["TaskListReport"] = "Schedule Task Report";

DataDictionaryEN['CompanyNameEnglish'] = 'Company Name ';
DataDictionaryEN["InterestedIn"] = "Interested In ";
DataDictionaryEN["ContactType"] = "Contact Type";
DataDictionaryEN["Anniversary"] = "Anniversary";
DataDictionaryEN["Nationality"] = "Nationality";
DataDictionaryEN["NotifyforBirthday"] = "Notify for Birthday";
DataDictionaryEN["NotifyForAnniversary"] = "Notify For Anniversary";
DataDictionaryEN["FixedAssetsReport"] = 'Fixed Assets List Report';
DataDictionaryEN["CreatedDateFrom"] = "Created Date From";
DataDictionaryEN["CreatedDateTo"] = "Created Date To";


DataDictionaryEN["Customer"] = 'Customer';
DataDictionaryEN["false"] = 'false'
DataDictionaryEN["true"] = 'true';
DataDictionaryEN["Http"] = 'http://'
////////////////////////////////////////Walkin section//////////////////////////////////////////////////////////////////
DataDictionaryEN["WalkIn"] = "WalkIn's";
DataDictionaryEN["WalkInSearch"] = "WalkIn's Search";
DataDictionaryEN["ResetWalkin"] = 'Reset';
DataDictionaryEN["NumberOfWalkIn"] = 'Number Of Walk In';
DataDictionaryEN["WalkInDateFrom"] = 'Walk In Date From';
DataDictionaryEN["WalkInDateTo"] = 'Walk In Date To';
DataDictionaryEN["WalkInTimeFrom"] = 'Walk In Time From';
DataDictionaryEN["WalkInTimeTo"] = 'Walk In Time To';
DataDictionaryEN["Filter"] = 'Apply';
DataDictionaryEN["WalkInDate"] = 'Walk In Date';
DataDictionaryEN["Comments"] = 'Comments';
DataDictionaryEN["WalkInTime"] = 'Walk In Time';
DataDictionaryEN["WalkInDateError"] = 'shouldn\'t be after walkIn date to';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// Giveaways English Dictionary////////////////////////////////////////////////
DataDictionaryEN["Giveaways"] = 'Giveaways';
DataDictionaryEN["AddGiveaways"] = 'Add New Giveaway';
DataDictionaryEN['GiveawaysDetails'] = 'Giveaways Details';
DataDictionaryEN['Giveaways'] = 'Giveaways';
DataDictionaryEN['Giveaway'] = 'Giveaway';
DataDictionaryEN['Company'] = 'Company';
DataDictionaryEN['Person'] = 'Person';
DataDictionaryEN['DateGiven'] = 'Date Given';
DataDictionaryEN['Branch'] = 'Branch';
DataDictionaryEN['SalesRepresentative'] = 'Sales Representative';
DataDictionaryEN['GiveawayOthers'] = 'Giveaway Others';
DataDictionaryEN['Comment'] = 'Comment';

DataDictionaryEN["AlreadyExist"] = ' Already Exist ';
//////List Header English Dictionary JS
DataDictionaryEN['HGiveaway'] = 'Giveaway: ';
DataDictionaryEN['HCompany'] = 'Company: ';
DataDictionaryEN['HPerson'] = 'Person: ';
DataDictionaryEN['HBranch'] = 'Branch: ';

////Tab English Dictionary JS

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryEN['GroupSettings'] = 'Group Settings';


///////////////////////////////////////// ClientCare English Dictionary////////////////////////////////////////////////
DataDictionaryEN["ClientCare"] = 'Client Care';
DataDictionaryEN["AddClientCare"] = 'Add Client Care';
DataDictionaryEN['ClientCareDetails'] = 'Client Care Details';
DataDictionaryEN['ClientCare'] = 'Client Care';
DataDictionaryEN['CaseType'] = 'Case Type';
DataDictionaryEN['ReferanceNumber'] = 'Reference Number';
DataDictionaryEN['Company'] = 'Company';
DataDictionaryEN['Person'] = 'Person';
DataDictionaryEN['Opportunity'] = 'Opportunity';
DataDictionaryEN['Description'] = 'Description';
DataDictionaryEN['OpenDate'] = 'Open Date';
DataDictionaryEN['Source'] = 'Source';
DataDictionaryEN['Priority'] = 'Priority';

DataDictionaryEN["AlreadyExist"] = ' Already Exist ';
//////List Header English Dictionary JS
DataDictionaryEN['HeaderCaseType'] = 'Case Type: ';
DataDictionaryEN['HeaderCompany'] = 'Company: ';

////Tab English Dictionary JS

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// ClientCareProgress English Dictionary////////////////////////////////////////////////
DataDictionaryEN["ClientCareProgress"] = 'Client Care Progress';
DataDictionaryEN["AddClientCareProgress"] = 'Add Client Care Progress';
DataDictionaryEN['ClientCareProgressDetails'] = 'Client Care Progress Details';
DataDictionaryEN['ClientCareProgress'] = 'Client Care Progress';
DataDictionaryEN['ClientCare'] = 'Client Care';
DataDictionaryEN['Stage'] = 'Stage';
DataDictionaryEN['Status'] = 'Status';
DataDictionaryEN['AssignTo'] = 'Assign To';
DataDictionaryEN['TrackingNote'] = 'Tracking Note';
DataDictionaryEN['SolutionType'] = 'Solution Type';
DataDictionaryEN['SolutionDetails'] = 'Solution Details';
DataDictionaryEN['CloseReason'] = 'Close Reason';
DataDictionaryEN['CloseComment'] = 'Close Comment';
DataDictionaryEN['HoldReason'] = 'Hold Reason';
DataDictionaryEN['HoldComment'] = 'Hold Comment';
DataDictionaryEN['ReminderDate'] = 'Reminder Date';
DataDictionaryEN['Logged'] = 'Logged';
DataDictionaryEN['InProgress'] = 'In Progress';
DataDictionaryEN['CaseProgress'] = 'Case Progress';

DataDictionaryEN["AlreadyExist"] = ' Already Exist ';

///////Actions
DataDictionaryEN['CaseSolved'] = 'Solved';
DataDictionaryEN['CaseInprogress'] = 'In progress';
DataDictionaryEN['CaseHold'] = 'On Hold';
DataDictionaryEN['CaseUnHold'] = 'Un Hold';
DataDictionaryEN['CaseClosed'] = 'Closed';
DataDictionaryEN['CaseCompleted'] = 'Completed';
DataDictionaryEN['CaseInvetegation'] = 'Investigation';
DataDictionaryEN['CaseContactManger'] = 'Contact Customer';
DataDictionaryEN['CaseAssignToManager'] = 'Assign To Manager';
DataDictionaryEN['CaseConfirmWithCustomer'] = 'Confirm With Customer';
DataDictionaryEN['CaseConfirm'] = 'Confirm';
//////List Header English Dictionary JS

////Tab English Dictionary JS

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// Opportunity English Dictionary////////////////////////////////////////////////
DataDictionaryEN["Opportunity"] = 'Opportunity';
DataDictionaryEN["AddOpportunity"] = 'Add Opportunity';
DataDictionaryEN['OpportunityDetails'] = 'Opportunity Details';
DataDictionaryEN['Opportunity'] = 'Opportunity';
DataDictionaryEN['ReferanceNumber'] = 'Reference Number';
DataDictionaryEN['Company'] = 'Company';
DataDictionaryEN['Person'] = 'Person';
DataDictionaryEN['ProposalNumber'] = 'Proposal Number';
DataDictionaryEN['Description'] = 'Description';
DataDictionaryEN['OpportunityType'] = 'Opportunity Type';
DataDictionaryEN['Source'] = 'Source';
DataDictionaryEN['Details'] = 'Details';
DataDictionaryEN['Country'] = 'Country';
DataDictionaryEN['Product'] = 'Product';
DataDictionaryEN['RelatedPresales'] = 'Related Presales';
DataDictionaryEN['OpenDate'] = 'Open Date';
DataDictionaryEN['RelatedOpportunity'] = 'Related Opportunity';

DataDictionaryEN["AlreadyExist"] = ' Already Exist ';
//////List Header English Dictionary JS
DataDictionaryEN['HCompany'] = 'Company: ';
DataDictionaryEN['HPerson'] = 'Person: ';
DataDictionaryEN['HProposalNumber'] = 'Proposal Number: ';
DataDictionaryEN['HOpenDate'] = 'Open Date: ';

////Tab English Dictionary JS
DataDictionaryEN['SourceCompany'] = 'Customer(Company)';
DataDictionaryEN['SourcePerson'] = 'Customer(Person)';
DataDictionaryEN['SourceEmployee'] = 'Employee';
DataDictionaryEN['AddNewOpportunity'] = 'Add New Opportunity';


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////// OpportunityProgress English Dictionary////////////////////////////////////////////////
DataDictionaryEN["OpportunityProgress"] = 'Opportunity Progress';
DataDictionaryEN["AddOpportunityProgress"] = 'Add Opportunity Progress';
DataDictionaryEN['OpportunityProgressDetails'] = 'Opportunity Progress Details';
DataDictionaryEN['OpportunityProgress'] = 'Opportunity Progress';
DataDictionaryEN['Opportunity'] = 'Opportunity';
DataDictionaryEN['TrackingNote'] = 'Tracking Note';
DataDictionaryEN['Stage'] = 'Stage';
DataDictionaryEN['Status'] = 'Status';
DataDictionaryEN['AssignTo'] = 'Assign To';
DataDictionaryEN['LastAction'] = 'Last Action';
DataDictionaryEN['LostReason'] = 'Lost Reason';
DataDictionaryEN['LostDate'] = 'Lost Date';
DataDictionaryEN['HoldReason'] = 'Hold Reason';
DataDictionaryEN['HoldComment'] = 'Hold Comment';
DataDictionaryEN['ClosedReason'] = 'Closed Reason';
DataDictionaryEN['ClosedComment'] = 'Closed Comment';
DataDictionaryEN['ReminderTime'] = 'Reminder Time';
DataDictionaryEN['Forecast'] = 'Forecast';
DataDictionaryEN['Certinty'] = 'certainty';
DataDictionaryEN['ClosedDueDate'] = 'Close Due Date';
DataDictionaryEN['QuotationNumber'] = 'Quotation Number';
DataDictionaryEN['DemoDescription'] = 'Description';

DataDictionaryEN["AlreadyExist"] = ' Already Exist ';
//////List Header English Dictionary JS
DataDictionaryEN['TrackingNote'] = 'Tracking Note';
DataDictionaryEN['Stage'] = 'Stage';
DataDictionaryEN['Status'] = 'Status';

////Opportunity Progress Action Status
DataDictionaryEN['OppInprogress'] = 'In progress';
DataDictionaryEN['OppClosed'] = 'Closed';
DataDictionaryEN['OppHold'] = 'Pending';

////Opportunity Progress Action Stages
DataDictionaryEN['OppLead'] = 'Lead';
DataDictionaryEN['OppQualify'] = 'Qualify';
DataDictionaryEN['OppAssessment'] = 'Assessment';
DataDictionaryEN['OppDemoPresentation'] = 'Demo';
DataDictionaryEN['OppOfferPreparation'] = 'Offer Preparation ';
DataDictionaryEN['OppProposalSubmitted'] = 'Proposal Submitted';
DataDictionaryEN['OppNegotiating'] = 'Negotiating';
DataDictionaryEN['OppDealWon'] = 'Won';
DataDictionaryEN['OppDealLost'] = 'Lost';
DataDictionaryEN['OppOnHold'] = 'Hold';
DataDictionaryEN['OppFollowUp'] = 'Follow Up';
DataDictionaryEN['OppContractPreperation'] = 'Contract Preperation';
DataDictionaryEN['OppPendingPayment'] = 'Pending Payment';
DataDictionaryEN['OppClosedLost'] = 'Closed Lost';

DataDictionaryEN['UnHoldRecord'] = 'UnHold Record';
DataDictionaryEN['ConfrmUnHold'] = 'are you sure to UnHold this Record ?';


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// Currency English Dictionary////////////////////////////////////////////////
DataDictionaryEN["Currency"] = 'Currency';
DataDictionaryEN["AddCurrency"] = 'Add Currency';
DataDictionaryEN['CurrencyDetails'] = 'Currency Details';
DataDictionaryEN['Currency'] = 'Currency';
DataDictionaryEN['CurencyCode'] = 'Currency Code';
DataDictionaryEN['CurencyName'] = 'Currency Name';
DataDictionaryEN['IsDefult'] = 'Is Default';
DataDictionaryEN['Description'] = 'Description';

DataDictionaryEN["AlreadyExist"] = ' Already Exist ';
//////List Header English Dictionary JS
DataDictionaryEN['HCurencyCode'] = 'Curency Code: ';
DataDictionaryEN['HIsDefult'] = 'Is Default: ';
DataDictionaryEN['HCurrencyName'] = 'Currency Name: ';

////Tab English Dictionary JS
DataDictionaryEN['YouDonotHavePermission'] = 'you donot have permission';
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////// Sales Tax /////////////////////////////////////////////////////
DataDictionaryEN["SalesTax"] = 'Sales Tax';
DataDictionaryEN["AddSalesTax"] = 'Add Sales Tax';
DataDictionaryEN['SalesTaxDetails'] = 'Sales Tax Details';
DataDictionaryEN['Description'] = 'Description';
DataDictionaryEN['TaxRate'] = 'Tax Rate';
DataDictionaryEN['HeaderDescription'] = 'Description: ';
DataDictionaryEN['HeaderTaxRate'] = 'Tax Rate: ';


///////////////////////////////////////// Product English Dictionary////////////////////////////////////////////////
DataDictionaryEN["Product"] = 'Product';
DataDictionaryEN["AddProduct"] = 'Add Product';
DataDictionaryEN['ProductDetails'] = 'Product Details';
DataDictionaryEN['Product'] = 'Product';
DataDictionaryEN['ItemNumber'] = 'Product Number /Code';
DataDictionaryEN['ItemDescription'] = 'Product Description';
DataDictionaryEN['Category'] = 'Category';
DataDictionaryEN['SubCategory'] = 'Sub Category';
DataDictionaryEN['SubCategory2'] = 'Sub Category2';
DataDictionaryEN['Location'] = 'Location';
DataDictionaryEN['Image'] = 'Image';
DataDictionaryEN['OtherDetails'] = 'Other Details';
DataDictionaryEN['UnitOfMeasure'] = 'Unit Of Measure';

DataDictionaryEN["AlreadyExist"] = ' Already Exist ';
//////List Header English Dictionary JS
DataDictionaryEN['HItemNumber'] = 'Product Number /Code: ';
DataDictionaryEN['HItemDescription'] = 'Product Description: ';
DataDictionaryEN['HOtherDetails'] = 'Other Details: ';

////Tab English Dictionary JS

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// SocialMedia English Dictionary////////////////////////////////////////////////
DataDictionaryEN["SocialMedia"] = 'Social Media';
DataDictionaryEN["AddSocialMedia"] = 'Add Social Media';
DataDictionaryEN['SocialMediaDetails'] = 'Social Media Details';
DataDictionaryEN['SocialMedia'] = 'Social Media';
DataDictionaryEN['SocialNetworkAcountValue'] = 'Network Account';
DataDictionaryEN['SocialNetworkAcountType'] = 'Account Type';
DataDictionaryEN['newSocialMedia'] = 'Add New Social Media';
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryEN["ShowAllUser"] = 'Show All Users';
DataDictionaryEN["Users"] = 'Users';
DataDictionaryEN["AllUsers"] = 'All Users';
DataDictionaryEN["TeamUsers"] = 'Team Users';
DataDictionaryEN["RoleUsers"] = 'Role Users';
///////////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryEN["AreyousureRole"] = 'are you sure to delete this role ?';
DataDictionaryEN["AreyousureTeam"] = 'are you sure to delete this Team ?';

DataDictionaryEN["MinLengthIs"] = 'min length is ';
DataDictionaryEN["prefxDeleteTeamMsg"] = 'this team already have ';
DataDictionaryEN["suffixDeleteTeamMsg"] = ' users ,you can\'t continue to delete !';
DataDictionaryEN["prefxDeleteRoleMsg"] = 'this role already have ';
DataDictionaryEN["suffixDeleteRoleMsg"] = ' users ,you can\'t continue to delete !';


DataDictionaryEN["default"] = 'Default';
DataDictionaryEN["CantDeleteRoot"] = 'Could not delete main root !!';
DataDictionaryEN["ContinueDeletefolder"] = 'After delete this folder all files will be moved to the root,  Are you sure to continue?';
DataDictionaryEN["CantRenameRoot"] = 'Could not rename main root !!';
DataDictionaryEN["RenameFolder"] = 'Rename Folder';

DataDictionaryEN["CanNotDeleteEntity"] = 'Failed to delete item ,linked to other entities !!';

DataDictionaryEN["HeaderBusinessEmail"] = 'Business Email : ';
DataDictionaryEN["HeaderLandLine"] = 'Land Line : ';

DataDictionaryEN["MoreThan100"] = 'Must be less than 100';
DataDictionaryEN["DeletedSuccessfully"] = 'Deleted Successfully';
DataDictionaryEN["Total?of?"] = 'Total ? of ?';
DataDictionaryEN["Total0of0"] = 'Total 0 of 0';

DataDictionaryEN['UsersCountExceedsAllowed'] = 'Can not add more users!';
DataDictionaryEN['ClosedDateValidation'] = 'Close date does not match open date';

DataDictionaryEN["CloseDateBeforeStartDate"] = 'Close Due Date cannot be Before Open Date';

DataDictionaryEN["Thisrecordislockedby"] = 'This record is locked by ';
DataDictionaryEN["YouDontHavePermissionsforthispage"] = 'You Dont Have Permissions for this page';
DataDictionaryEN["att"] = ', at ';
DataDictionaryEN["Yourlogginsessionhasbeenexpired"] = 'Your loggin session has been expired';

DataDictionaryEN["FileSizeError"] = 'File Size Error';
DataDictionaryEN["ExceedsTheMaximumDocumentSize"] = 'Exceeds The Maximum Document Size';

DataDictionaryEN["Language"] = 'العربية';

DataDictionaryEN["HideOptions"] = 'Hide options ▲';
DataDictionaryEN["MoreOptions"] = 'More options ▼';


DataDictionaryEN["SortingField"] = 'Sorting Field';
DataDictionaryEN["Ascending"] = 'Ascending';
DataDictionaryEN["Filter"] = 'Apply';


DataDictionaryEN["CalenderDefultTaskStatus"] = 'Active';
DataDictionaryEN["CalenderDefultTaskType"] = 'Task';
DataDictionaryEN["CalenderDefultPriority"] = 'Normal';
DataDictionaryEN["Teams"] = 'Teams';
DataDictionaryEN["Logout"] = 'Logout';
//////////////////////////////////////////////////////////////////////////////////
DataDictionaryEN["Service"] = 'Service';
DataDictionaryEN["MassMail"] = 'Mass Mail';
DataDictionaryEN["Teams"] = 'Teams';
DataDictionaryEN["Calender&Communications"] = 'Calender / Communications';
DataDictionaryEN["Setup"] = 'Setup';


////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////// Store English Dictionary////////////////////////////////////////////////
DataDictionaryEN["Store"] = 'Store';
DataDictionaryEN["AddStore"] = 'Add Store';
DataDictionaryEN['StoreDetails'] = 'Store Details';
DataDictionaryEN['StoreId'] = 'Store Id';
DataDictionaryEN['StoreName'] = 'Store Name';
DataDictionaryEN['StoreNameAr'] = 'Store Name Ar';
DataDictionaryEN['StoreCode'] = 'Store Code';
DataDictionaryEN['DefaultCurrency'] = 'Default Currency';
DataDictionaryEN['DefaultCurrCode'] = 'Default Currency Code';
DataDictionaryEN['DefaultCurrSymbol'] = 'Default Currency Symbol';
DataDictionaryEN['Location'] = 'Location';
DataDictionaryEN['LocationCode'] = 'Location Code';
DataDictionaryEN['LocationDesc'] = 'Location Description';
DataDictionaryEN['IpAddress'] = 'Ip Address';
DataDictionaryEN['MacAddress'] = 'Mac Address';
DataDictionaryEN['DataBaseInstanceName'] = 'Data Base Instance Name';
DataDictionaryEN['DataBaseName'] = 'Data Base Name';
DataDictionaryEN['DataBaseUserName'] = 'Data Base User Name';
DataDictionaryEN['DataBasePassword'] = 'Data Base Password';
DataDictionaryEN['DefaultTax'] = 'Default Tax';
DataDictionaryEN['DisplayPricses'] = 'Display Pricses';
DataDictionaryEN['CashierDiscounts'] = 'Cashier Discounts';
DataDictionaryEN['LegalEntity'] = 'Legal Entity';
DataDictionaryEN['WeightScaleFormula'] = 'Weight Scale Formula';
DataDictionaryEN['IsUsed'] = 'Is Used';
DataDictionaryEN['HStoreName'] = 'Store Name: ';
DataDictionaryEN['HStoreCode'] = 'Store Code: ';


DataDictionaryEN['Currency'] = 'Currency';
DataDictionaryEN['City'] = 'City';
DataDictionaryEN['DublicatedStoreNameorCode'] = 'Dublicated Store Name or Code';

////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////// Register English Dictionary////////////////////////////////////////////////
DataDictionaryEN["Register"] = 'Register';
DataDictionaryEN["AddRegister"] = 'Add Register';
DataDictionaryEN['RegisterDetails'] = 'Register Details';
DataDictionaryEN['RegisterName'] = 'Register Name';
DataDictionaryEN['RegisterNameAr'] = 'Register Name Ar';
DataDictionaryEN['RegiserCode'] = 'Regiser Code';
DataDictionaryEN['CashLoan'] = 'Cash Loan';
DataDictionaryEN['QuickKeys'] = 'Quick Keys';
DataDictionaryEN['IpAddress'] = 'Ip Address';
DataDictionaryEN['MacAddress'] = 'Mac Address';
DataDictionaryEN['DataBaseInstanceName'] = 'Data Base Instance Name';
DataDictionaryEN['DataBaseName'] = 'Data Base Name';
DataDictionaryEN['DataBaseUserName'] = 'Data Base User Name';
DataDictionaryEN['DataBasePassword'] = 'Data Base Password';
DataDictionaryEN['Branch'] = 'Branch';
DataDictionaryEN['InvoiceNumberFormat'] = 'Invoice Number Format';
DataDictionaryEN['LaybyNumberFormat'] = 'Layby Number Format';
DataDictionaryEN['PrintRecipt'] = 'Print Recipt';
DataDictionaryEN['EmailRecipt'] = 'Email Recipt';
DataDictionaryEN['IsUsed'] = 'Is Used';
DataDictionaryEN['IsNew'] = 'Is New';
DataDictionaryEN['HRegisterName'] = 'Name: ';
DataDictionaryEN['HRegiserCode'] = 'Code: ';
DataDictionaryEN['HStore'] = 'Store: ';

///////////////////////////////////////// GiftVoucher English Dictionary////////////////////////////////////////////////
DataDictionaryEN["GiftVoucher"] = 'Gift Voucher';
DataDictionaryEN["AddGiftVoucher"] = 'Add Gift Voucher';
DataDictionaryEN['GiftVoucherDetails'] = 'Gift Voucher Details';
DataDictionaryEN['GiftVoucher'] = 'Gift Voucher';
DataDictionaryEN['GiftNumber'] = 'Gift Number';
DataDictionaryEN['GiftName'] = 'Gift Name';
DataDictionaryEN['GiftAmount'] = 'Gift Amount';
DataDictionaryEN['ExpireDate'] = 'Expire Date';
DataDictionaryEN['IsUsed'] = 'Is Used';
DataDictionaryEN['HGiftNumber'] = 'Gift Number: ';
DataDictionaryEN['HGiftName'] = 'Gift Name: ';
DataDictionaryEN['DublicatedGiftNumber'] = 'Gift number already exist';
DataDictionaryEN["ValidateExpireDate"] = "Expire Date must be in future";

///////////////////////////////////////// Shifts English Dictionary////////////////////////////////////////////////
DataDictionaryEN["Shifts"] = 'Shifts';
DataDictionaryEN["AddShifts"] = 'Add Shifts';
DataDictionaryEN['ShiftsDetails'] = 'Shifts Details';
DataDictionaryEN['Shift'] = 'Shift';
DataDictionaryEN['ShiftName'] = 'Shift Name';
DataDictionaryEN['ShiftDescription'] = 'Shift Description';
DataDictionaryEN['StartTime'] = 'Start Time';
DataDictionaryEN['EndTime'] = 'End Time';
DataDictionaryEN['ValidateShift'] = 'Validate Shift';
DataDictionaryEN['HShiftName'] = 'Shift Name: ';
DataDictionaryEN['UserStoresShifts'] = 'User stores & Shifts';
DataDictionaryEN['DublicatedShiftName'] = 'Shift name already exist';
DataDictionaryEN['AddStoreAndShift'] = 'Add store and shift';
DataDictionaryEN['Dublicated'] = 'Shift already used';

///////////////////////////////////////// GenerateBarcode English Dictionary////////////////////////////////////////////////
DataDictionaryEN['GenerateBarcode'] = 'Generate Barcode';
DataDictionaryEN['JD'] = 'JD';
/////////////////////////////////////////////////////////////////////////////////////
DataDictionaryEN['Registers'] = 'Registers';
DataDictionaryEN['Stores'] = 'Stores';
DataDictionaryEN['POSSync'] = 'POS Sync';
DataDictionaryEN['RegisterStatus'] = 'Register Status';
DataDictionaryEN['RegisterData'] = 'Sync Register Data';
DataDictionaryEN['AdminData'] = 'Sync Admin Data';
DataDictionaryEN['GettaxMatrix'] = 'Get tax Matrix';

DataDictionaryEN['GetItemPriceList'] = 'Get Item Price List';
DataDictionaryEN['Gettransferdata'] = 'Get Transfer Data';
DataDictionaryEN['PostToAccpac'] = 'Post To Sage Accpac';

DataDictionaryEN['PostPark'] = 'Post Park';
DataDictionaryEN['PostPettyCash'] = 'Post Petty Cash';
DataDictionaryEN['GetToAccpac'] = 'Get From Sage Accpac';
DataDictionaryEN['AccpacServerIp'] = 'Accpac Server Ip';

DataDictionaryEN['Getbankaccount'] = 'Get Bank Account';
DataDictionaryEN['Getcategory'] = 'Get Category';
DataDictionaryEN['Getcurrency'] = 'Get Currency';
DataDictionaryEN['Getcurrencyrates'] = 'Get Currency Rates';
DataDictionaryEN['Getcustomer'] = 'Get Customer';
DataDictionaryEN['Getglaccounts'] = 'Get GL accounts';
DataDictionaryEN['Getlocation'] = 'Get Location';
DataDictionaryEN['Getpricelist'] = 'Get Price List';
DataDictionaryEN['Gettaxauthorities'] = 'Get Tax Authorities';

DataDictionaryEN['Getitemcard'] = 'Get Item Card';
DataDictionaryEN['Getitempricelist'] = 'Get Item Price List';
DataDictionaryEN['Getitemstock'] = 'Get Item Stock';
DataDictionaryEN['Getitemtax'] = 'Get Item Tax';

DataDictionaryEN['Gettaxmatrix'] = 'Get Tax Matrix';

DataDictionaryEN['starsync'] = 'Star Sync';
DataDictionaryEN['SyncTransactionData'] = 'Sync Transaction Data';
DataDictionaryEN['SyncUserManagment'] = 'Sync User Managment';
DataDictionaryEN['CloseRegisterData'] = 'Close Register Data';
DataDictionaryEN['CustomerData'] = 'Customer Data';
DataDictionaryEN['SyncUsers'] = 'Sync Users';
DataDictionaryEN['SyncRoles'] = 'Sync Roles';
DataDictionaryEN['SyncTeams'] = 'Sync Teams';
DataDictionaryEN['SyncPermissions'] = 'Sync Permissions';
DataDictionaryEN['SyncShifts'] = 'Sync Shifts';
DataDictionaryEN['SyncContactManagment'] = 'Sync Contact Managment';
DataDictionaryEN['SyncCompany'] = 'Sync Company';
DataDictionaryEN['syncPerson'] = 'sync Person';
DataDictionaryEN['SyncPOSAdministration'] = 'Sync POS Administration';
DataDictionaryEN['SyncStores'] = 'Sync Stores';
DataDictionaryEN['SyncRegisters'] = 'Sync Registers';
DataDictionaryEN['SyncQuickKeys'] = 'Sync Quick Keys';
DataDictionaryEN['Paymentdata'] = 'Sync Payment data';
DataDictionaryEN['SyncInventoryData'] = 'Sync ERP Data';
DataDictionaryEN['SyncItems'] = 'Sync Items';
DataDictionaryEN['SyncItemsSerial'] = 'Sync Items Serial';
DataDictionaryEN['SyncPriceList'] = 'Sync Price List';
DataDictionaryEN['SyncItemPriceList'] = 'Sync Item Price List';
DataDictionaryEN['SyncItemTax'] = 'Sync Item Tax';
DataDictionaryEN['SyncCurrency'] = 'Sync Currency';
DataDictionaryEN['SyncCurrencyRates'] = 'Sync Currency Rates';
DataDictionaryEN['TaxAuth'] = 'sync Tax Authority';
DataDictionaryEN['Taxmatrix'] = 'Tax Matrix';
DataDictionaryEN['AccpacCustomerData'] = 'Sync ERP Customer Data';
DataDictionaryEN['Sync'] = 'Sync';
DataDictionaryEN['Alert'] = 'Alert';
DataDictionaryEN['SyncronizationValidationRegistersMessage'] = 'Store IpAddress is invalid';
DataDictionaryEN['SyncronizationValidationDataMessage'] = 'Please Select Data To Start The Synchronization Process !!';
DataDictionaryEN['SyncronizationConnectionError'] = 'Syncronization Connection Error';
DataDictionaryEN['SyncronizationError'] = 'Syncronization Error';
DataDictionaryEN['DataUpdatedMessage'] = 'Data Updated Message';
DataDictionaryEN['ShowFullDetails'] = 'Show Full Details';

/////////////////////////////////////PosSystemConfigration eArabic Dictionary/////////////////////////////////////////////////////
DataDictionaryEN["PosSystemConfigration"] = 'System Configration';
DataDictionaryEN['PosSystemConfigrationDetails'] = 'System Configration Details';
DataDictionaryEN['GiftVoucherOverAmount'] = 'Gift Voucher Over Amount';
DataDictionaryEN['LaybyMinPercentage'] = 'Layby Min Percentage';
DataDictionaryEN['LoginMethod'] = 'Login Method';
DataDictionaryEN['AfterCloseRegisterValue'] = 'After Close Register Value';
DataDictionaryEN['InvoiceNumberFormat'] = 'Invoice Number Format';
DataDictionaryEN['LaybyNumberFormat'] = 'Layby Number Format';
DataDictionaryEN['CashCustomer'] = 'Cash Customer';
DataDictionaryEN['AccpacStagingIp'] = 'ERP Staging Ip';
DataDictionaryEN['PosServerIp'] = 'Pos Server Ip';
DataDictionaryEN['AccpacDataBaseUsername'] = 'POS Data Base Username';
DataDictionaryEN['AccpacDataBasePassword'] = 'Accpac Data Base Password';
DataDictionaryEN['PosServerDataBaseUsername'] = 'Pos Server Data Base Username';
DataDictionaryEN['AccpacCompanyName'] = 'Accpac Company Name';
DataDictionaryEN['AccpacUserName'] = 'Accpac UserName';
DataDictionaryEN['AccpacUserPassword'] = 'Accpac User Password';
DataDictionaryEN['AccpacDataBaseInstance'] = 'POS DataBase Instance ';
DataDictionaryEN['PosServerDataBasePassword'] = 'Pos Server Data Base Password';
DataDictionaryEN['PosServerDataBaseName'] = 'Pos Server Data Base Name';

DataDictionaryEN['AccpacDataBaseName'] = 'POS Data Base Name';
DataDictionaryEN['SystemConfigration'] = 'System Configration';
DataDictionaryEN['ItemCard'] = 'Item Card';

DataDictionaryEN['Prefix'] = 'Prefix';
DataDictionaryEN['D1'] = 'D1';
DataDictionaryEN['D2'] = 'D2';
DataDictionaryEN['D3'] = 'D3'
DataDictionaryEN['SC'] = 'SC';
DataDictionaryEN['RC'] = 'RC';
DataDictionaryEN['Length'] = 'Length';
DataDictionaryEN['Required'] = 'Required';
DataDictionaryEN['Example'] = 'Example';
DataDictionaryEN['InvalidPercentage'] = 'Invalid Percentage';
////////////////////////////////////////////////// QuickKeys/ //////////////////////////////////////
DataDictionaryEN['BankAccount'] = 'Bank Account';
DataDictionaryEN['GLAccount'] = 'GL Account';
DataDictionaryEN["QuickKeys"] = 'Quick Keys';
DataDictionaryEN["AddQuickKeys"] = 'Add Quick Keys';
DataDictionaryEN['QuickKeysDetails'] = 'Quick Keys Details';
DataDictionaryEN['QuickKey'] = 'Quick Key';
DataDictionaryEN['Name'] = 'Name';
DataDictionaryEN['QuickKeyProduct'] = 'Quick Key Product';
DataDictionaryEN['Tab'] = 'Tab';
DataDictionaryEN['CssClass'] = 'Css Class';
DataDictionaryEN['Product'] = 'Product';
DataDictionaryEN['TabName'] = 'Tab Name';
DataDictionaryEN['ItemNumber'] = 'Item Number';
DataDictionaryEN['ItemDesc'] = 'Item Desc';
DataDictionaryEN['ItemBarcode'] = 'Item Barcode';
DataDictionaryEN['HName'] = 'Name: ';
DataDictionaryEN['DublicatedName'] = 'Dublicated Name';
DataDictionaryEN['Items'] = 'Items';
DataDictionaryEN['Addnewtab'] = 'Add New Tab';
DataDictionaryEN['QuickKeyValidationMessage'] = 'Add Items!';
DataDictionaryEN['Rename'] = 'Rename';
DataDictionaryEN['Default'] = 'Default';
DataDictionaryEN['BackEndSync'] = 'Back End Sync';
DataDictionaryEN['IcCurrency'] = 'Currency';
DataDictionaryEN['IcItemCard'] = 'Item Card';
DataDictionaryEN['IcPriceListGroup'] = 'Price List Group';
DataDictionaryEN['IcItemPriceList'] = 'Item Price List';
DataDictionaryEN['IcItemTax'] = 'Item Tax';
DataDictionaryEN['IcLocation'] = 'Location';
DataDictionaryEN['IcStock'] = 'Stock';
DataDictionaryEN['IcCurrencyRate'] = 'Currency Rate';
DataDictionaryEN['IcCustomer'] = 'Customer';
DataDictionaryEN['LastSyncDate'] = 'Last Sync Date';
DataDictionaryEN['Sync'] = 'Sync';
DataDictionaryEN['Succeed'] = 'Succeed';
DataDictionaryEN['Failed'] = 'Failed';
DataDictionaryEN['noStoresMessage'] = 'Please Select Store To Start The Synchronization Process !!';
DataDictionaryEN['GiftVoucherData'] = 'Gift Voucher Data';
DataDictionaryEN['ARCustomerData'] = "AR Customer Data";
DataDictionaryEN['SyncConfiguration'] = "Configuration Data";
DataDictionaryEN['StockData'] = "Stock Data";
DataDictionaryEN['SyncLocation'] = "Location Data";
DataDictionaryEN['StockData'] = "Sync stock";
DataDictionaryEN['SyncLocation'] = "Sync locations";
DataDictionaryEN['ArCashCustomer'] = "AR Cash Customer "
DataDictionaryEN['ArVisaCustomer'] = "AR Visa Customer "
DataDictionaryEN['ArMixedCustomer'] = "AR Mixed Customer "

DataDictionaryEN['ItemStructure'] = "Item Structure";
DataDictionaryEN['SyncScheduleDetails'] = "SyncSchedule Details";
DataDictionaryEN['Pending'] = 'Pending';
DataDictionaryEN["POSAdministrationData"] = "POS Setup";
DataDictionaryEN["POSSetup"] = "CONFIGURATION";
DataDictionaryEN["CONTACTINFORMATION"] = "POS CONTACT";

DataDictionaryEN["TaxableClass"] = "Taxable Class";
DataDictionaryEN["NonTaxableClass"] = "Non Taxable Class";


DataDictionaryEN["BankCashAccount"] = "Bank Cash Account";
DataDictionaryEN["BankVisaAccount"] = "Bank Visa Account";
DataDictionaryEN["BankMasterAccount"] = "Bank Master Account";
DataDictionaryEN["BankAmix"] = "Bank Amex";

DataDictionaryEN["BankNational"] = "Bank National";
DataDictionaryEN["BankCheque"] = "Bank Cheque";

DataDictionaryEN["SyncConfiguration"] = "Sync Configuration";


DataDictionaryEN['PosSyncTime1'] = 'Pos Sync Time1';
DataDictionaryEN['PosSyncTime2'] = 'Pos Sync Time2';
DataDictionaryEN['PosSyncTime3'] = 'Pos Sync Time3';
DataDictionaryEN['PosSyncTime4'] = 'Pos Sync Time4';
DataDictionaryEN['PosSyncTime5'] = 'Pos Sync Time5';
DataDictionaryEN['PosDataToSync'] = 'Pos Data To Sync';
DataDictionaryEN['SyncDataFromAccpac'] = 'Sync Data From ERP';
DataDictionaryEN['AccpacSyncTime1'] = 'ERP Sync Time1';
DataDictionaryEN['AccpacSyncTime2'] = 'ERP Sync Time2';
DataDictionaryEN['AccpacSyncTime3'] = 'ERP Sync Time3';
DataDictionaryEN['AccpacSyncTime4'] = 'ERP Sync Time4';
DataDictionaryEN['AccpacSyncTime5'] = 'ERP Sync Time5';
DataDictionaryEN['AccpacDataToSyncIds'] = 'ERP Data To Sync Ids';
DataDictionaryEN['AccpacDataToSync'] = 'ERP Data To Sync';
DataDictionaryEN['Status'] = 'Status';

DataDictionaryEN['PosDataToSync'] = 'Pos Data To Sync';
DataDictionaryEN['SyncDataFromAccpac'] = 'Sync Data From ERP';
DataDictionaryEN['AccpacSync'] = 'Sage Accpac Sync';
DataDictionaryEN['POSSync'] = 'POS Sync';

DataDictionaryEN['Faild'] = 'failed';
DataDictionaryEN['Success'] = 'success';

DataDictionaryEN['SyncDateFrom'] = 'Sync Date From';
DataDictionaryEN['SyncDateTo'] = 'Sync Date To';
DataDictionaryEN['PosSyncLogSearch'] = 'Sync Log Search';
DataDictionaryEN['PosSyncLog'] = 'Sync Log';
DataDictionaryEN['Message'] = 'Message';
DataDictionaryEN['NoNewUpdate'] = 'No New Update';
DataDictionaryEN['POSConfiguration'] = "POS System Configuration";
DataDictionaryEN["ERPIntegration"] = "ERP Integration";
DataDictionaryEN["POSShipmentPostResult"] = "Created Transactions Numbers : <br>";
DataDictionaryEN["NoInvoiceToPost"] = "No Invoice To Post";
DataDictionaryEN["PostInvoice"] = ' Post Invoice ';
DataDictionaryEN["PostRefund"] = ' Post Refund ';
DataDictionaryEN["PostOnAccount"] = ' Post On Account ';
DataDictionaryEN["InvoiceInquiry"] = 'POS Transaction Log';
DataDictionaryEN["InvoiceDateFrom"] = 'Invoice Date From';
DataDictionaryEN["InvoiceDateTo"] = 'Invoice Date To';
DataDictionaryEN["syncCompany"] = 'sync Company';
DataDictionaryEN["POSInquiries"] = 'POS Inquiries';
DataDictionaryEN["CloseRegisterInquiry"] = 'Close Register Report';
DataDictionaryEN["IsIdentical"] = 'Is Identical';
DataDictionaryEN["OpenedCashLoan"] = 'Opened Cash Loan';
DataDictionaryEN["DifferenceAmount"] = 'Difference Amount';
DataDictionaryEN["OpenedDateFrom"] = 'Opened Date From';
DataDictionaryEN["OpenedDateTo"] = 'Opened Date To';
DataDictionaryEN["ClosedDateFrom"] = 'Closed Date From';
DataDictionaryEN["ClosedDateTo"] = 'Closed Date To';
DataDictionaryEN["DiscountAccount"] = 'Discount Account';
DataDictionaryEN["PleaseCheckAdditionalCostVendors"] = 'Please Check Additional Cost Vendors, Vendors Must have same Currency as it\'s Receipt';
DataDictionaryEN["CurrencyMismatch"] = 'Currency Mismatch';
DataDictionaryEN["Totalresultscounter"] = 'Total results counter: ';
DataDictionaryEN["PhysicalLocation"] = 'Physical Location';
DataDictionaryEN["UsernameandPassword"] = 'User name and Password';
DataDictionaryEN["SyncEntities"] = "Sync Entities";
DataDictionaryEN["SyncBy"] = "Sync By";
DataDictionaryEN["SyncDate"] = "Sync Date";
DataDictionaryEN["TaxAuthority"] = "Tax Authority";
DataDictionaryEN["APTransactionNumber"] = "Entry #";
DataDictionaryEN["ParentInvoiceNotPostedYet"] = 'Parent invoice of one or more of the refund invoices is/are not posted yet, please post them then post refund again.';
DataDictionaryEN["ShipmentDiscountExceedsTotalAmount"] = 'Shipment Discount Exceeds Total Amount';
DataDictionaryEN["DetailsCurrenciesAreUsedBySystem"] = 'Failed to delete; Details Currencies Are Used By ERP system data';
DataDictionaryEN["IcReceiptDiscountExceedsReceiptAmount"] = 'Receipt Discount Exceeds it\'s Amount, resulting in a negative total cost!';
DataDictionaryEN["CurrencyAlreadyExists"] = 'Currency Cannot be duplicated for the same header currency';
DataDictionaryEN["QuantityInLocationsIsNotEnoughtoFullyReturnReceipt"] = 'Quantities In Locations are Not Enough to Fully Return Receipt, Negative Quantities are not allowed !';
DataDictionaryEN["PostExchange"] = 'Post Exchange';
DataDictionaryEN["ExchangeAccount"] = 'Exchange Clearing Account';
DataDictionaryEN["TotalAfterDiscount"] = 'Total After Discount';
DataDictionaryEN["TotalAfterTax"] = 'Total After Tax and Discount';
DataDictionaryEN["TotalExtendedPrice"] = 'Total Extended Price';
DataDictionaryEN["ItemFrom"] = 'Item from';
DataDictionaryEN["ItemTo"] = 'Item to';
DataDictionaryEN["InvoiceReport"] = 'Invoice report';
DataDictionaryEN["TotalDailySales"] = 'POS Receipts Summary';
DataDictionaryEN["DiscountReport"] = 'Discount report';
DataDictionaryEN["PettyCashReport"] = 'Petty Cash Report';
DataDictionaryEN["DailySalesPerItem"] = 'POS Sales Details Report';
DataDictionaryEN["Dailysales"] = 'POS Sales Summary Report';
DataDictionaryEN["ChequeDetails"] = 'Cheque details report';
DataDictionaryEN["AllStores"] = 'All stores';
DataDictionaryEN["LessThanDiscount"] = 'Less than discount';
DataDictionaryEN["GreaterThanDiscount"] = 'Greater than discount';
DataDictionaryEN["AllItem"] = 'All item';
DataDictionaryEN["LastValueInserted"] = 'Last Value Inserted';
DataDictionaryEN["MustBeGreaterThan0"] = 'Quantity Must Be Greater Than 0';
DataDictionaryEN["ItemsType"] = 'Items Type';
DataDictionaryEN["ItemTransactionsReport"] = 'Item Transactions Report';
DataDictionaryEN["Skype"] = 'Skype';
DataDictionaryEN["ItemIsNotAvailableInTheSourceLocation"] = 'Item Is Not Available In The Source Location';
DataDictionaryEN["ChartOfAccounts"] = 'Chart Of Accounts';
DataDictionaryEN["AcountNumber"] = 'Acount Number';
DataDictionaryEN["AcountDescription"] = 'Acount Description';
DataDictionaryEN["CustomerReport"] = 'Customer Report';
DataDictionaryEN["CustomerNumber"] = 'Customer Number';
DataDictionaryEN["CustomerName"] = 'Customer Name';
DataDictionaryEN["VendorReport"] = 'Vendor Report';
DataDictionaryEN["vendorNumber"] = 'vendor Number';
DataDictionaryEN["vendorName"] = 'vendor Name';
DataDictionaryEN["ItemReport"] = 'Item Report';
DataDictionaryEN["ItemNumber"] = 'Item Number';
DataDictionaryEN["ItemDescription"] = 'Item Description';
DataDictionaryEN["SegmentCodeFrom"] = 'Segment Code From';
DataDictionaryEN["SegmentCodeTo"] = 'Segment Code To';
DataDictionaryEN["StoreFrom"] = 'Store From';
DataDictionaryEN["StoreTo"] = 'Store To';
DataDictionaryEN["RegisterFrom"] = 'Register From';
DataDictionaryEN["RegisterTo"] = 'Register To';
DataDictionaryEN["SummarySalesPerItem"] = 'Summary Sales Per Item';
DataDictionaryEN["AccessCode"] = 'Access Code';
DataDictionaryEN["FromBrand"] = 'From Brand';
DataDictionaryEN["ToBrand"] = 'To Brand';
DataDictionaryEN["FromCategory"] = 'From Category';
DataDictionaryEN["ToCategory"] = 'To Category';
DataDictionaryEN["FromColor"] = 'From Color';
DataDictionaryEN["ToColor"] = 'To Color';
DataDictionaryEN["FromSize"] = 'From Size';
DataDictionaryEN["ToSize"] = 'To Size';
DataDictionaryEN["AthletiqueSalesReport"] = 'Details Sales Report';
DataDictionaryEN["PostAll"] = 'Post All';

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////Start Purchase Order Module ////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////PO Invoice English Dictionary JS///////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryEN["PoInvoice"] = 'Invoices';
DataDictionaryEN["AddPoInvoice"] = 'Add New Invoice';
DataDictionaryEN['PoInvoiceDetails'] = 'Invoice Details';
DataDictionaryEN['Invoice'] = 'Invoice';
DataDictionaryEN['InvoiceReference'] = 'Invoice Reference';
DataDictionaryEN['InvoiceTotalAmount'] = 'Invoice Total Amount';
DataDictionaryEN['Vendor'] = 'Vendor';
DataDictionaryEN['ReceiptIds'] = 'Receipt Ids';
DataDictionaryEN['InvoiceDate'] = 'Invoice Date';
DataDictionaryEN['InvoicingDate'] = 'Invoicing Date';
DataDictionaryEN['RelatedPO'] = 'Related PO';
DataDictionaryEN['ReceiptItemLine'] = 'Receipt Item Line';
DataDictionaryEN['InvoiceTotalAmount'] = 'Invoice Total Amount';
DataDictionaryEN['PostingDate'] = 'Posting Date';
DataDictionaryEN['Description'] = 'Description';
DataDictionaryEN['DocumentReferenceNumber'] = 'Invoice Reference Number';
DataDictionaryEN['InvoiceNumber'] = 'Invoice Number';
DataDictionaryEN['TotalAmountWithTax'] = 'Total Amount With Tax';
DataDictionaryEN['TotalAmountAfterDiscount'] = 'Total Amount After Discount';
DataDictionaryEN['TotalAmountBeforDiscount'] = 'Total Amount Befor Discount';
DataDictionaryEN['DiscountAmount'] = 'Discount Amount';
DataDictionaryEN['DiscountPercentage'] = 'Discount Percentage';
//////List Header English Dictionary JS
DataDictionaryEN['InvoiceReference'] = 'Invoice Reference';
DataDictionaryEN['InvoiceTotalAmount'] = 'Invoice Total Amount';
////Tab English Dictionary JS
DataDictionaryEN['AdditionalCost'] = 'Additional Cost';
DataDictionaryEN['AddNewAdditionalCost'] = 'Add New AdditionalCost';

///////////////////////////////////////////////////////////////////PO entity/////////////////////////////////////////////////////////////////////////////////////////////

DataDictionaryEN["PurchaseOrderHeader"] = 'Purchase Order';
DataDictionaryEN["AddPurchaseOrderHeader"] = 'Add Purchase Order';
DataDictionaryEN['PurchaseOrderHeaderDetails'] = 'Purchase Order Details';
DataDictionaryEN['ReferenceNumber'] = 'Reference Number';
DataDictionaryEN['PurchaseDate'] = 'Purchase Order Date';
DataDictionaryEN['Currency'] = 'Currency';
DataDictionaryEN['Status'] = 'Status';
DataDictionaryEN['AutoGenerateReceipt'] = 'Auto Generate Receipt';
DataDictionaryEN['AutoGenerateInvoice'] = 'Auto Generate Invoice';
DataDictionaryEN['Vendor'] = 'Vendor';
DataDictionaryEN['TaxClass'] = 'Tax Class';
DataDictionaryEN['ExchangeRate'] = 'Exchange Rate';
DataDictionaryEN['Description'] = 'Description';
DataDictionaryEN['HeaderDiscountAmount'] = 'Discount Amount';
DataDictionaryEN['HeaderDiscountPercentage'] = 'Discount Percentage';
DataDictionaryEN['NumberOfLines'] = 'Number Of Lines';
DataDictionaryEN['TotalCost'] = 'Total Cost';
DataDictionaryEN['TotalQuantity'] = 'Total Quantity';
DataDictionaryEN['TotalAmountWithTax'] = 'Total Amount With Tax';
DataDictionaryEN['TotalAmountAfterDiscount'] = 'Total Amount After Discount';
DataDictionaryEN['TotalAmountBeforeDiscount'] = 'Total Amount Without Discount';
DataDictionaryEN['IsPosted'] = 'Is Posted';
DataDictionaryEN['PostingDate'] = 'Posting Date';
DataDictionaryEN['ItemCard'] = 'Item Card';
DataDictionaryEN['ItemDescription'] = 'Item Description';
DataDictionaryEN['Location'] = 'Location';
DataDictionaryEN['Quantity'] = 'Quantity';
DataDictionaryEN['UnitOfMeasure'] = 'Unit Of Measure';
DataDictionaryEN['UnitCost'] = 'Unit Cost';
DataDictionaryEN['TaxClass'] = 'Tax Class';
DataDictionaryEN['UnitDiscountAmount'] = 'Discount Amount';
DataDictionaryEN['UnitDiscountPercentage'] = 'Discount Percentage';
DataDictionaryEN['ReceivedQuantity'] = 'Received Quantity';
DataDictionaryEN['OutstandingQuantity'] = 'Outstanding Quantity';
DataDictionaryEN['UnitTotalAmountBeforeDiscount'] = 'Total Amount Before Discount';
DataDictionaryEN['UnitTotalAmountAfterDiscount'] = 'Total Amount After Discount';
DataDictionaryEN['UnitTotalAmountWithTax'] = 'Total Amount With Tax';
DataDictionaryEN['Comments'] = 'Comments';
DataDictionaryEN['IsLastOne'] = 'Is Last One';
DataDictionaryEN['POStatusNew'] = 'New';
DataDictionaryEN['POStatusInProgress'] = 'In Progress';
DataDictionaryEN['POStatusCompleted'] = 'Completed';

DataDictionaryEN['HeaderReferenceNumber'] = "Purchase Order Number: ";
DataDictionaryEN['HeaderPODate'] = "Purchase Order Date: ";
DataDictionaryEN['HeaderStatus'] = "Status: ";

DataDictionaryEN['PODetails'] = "Purchase Order Lines Details  ";
/////////////////////////////////////////English Dictionary JS///////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryEN["PoReceipt"] = 'Receipts';
DataDictionaryEN["Receipt"] = 'Receipt';
DataDictionaryEN["ReceiptSummary"] = 'Receipt Summary';
DataDictionaryEN["AddPoReceipt"] = 'Add New Receipt';
DataDictionaryEN["ReceiptDetails"] = 'Receipt Details';
DataDictionaryEN["ReceiptLineDetails"] = 'Receipt Line Details';
DataDictionaryEN['PoReceiptDetails'] = 'Receipt Details';
DataDictionaryEN['SummaryPoReceiptDetails'] = 'PO Summary Receipt Details';
DataDictionaryEN['Receipt'] = 'Receipt';
DataDictionaryEN['ReceiptStatus'] = 'Receipt Status';
DataDictionaryEN['Reference'] = 'Reference Number';
DataDictionaryEN['ReceiptDate'] = 'Receipt Date';
DataDictionaryEN['PurchaseOrder'] = 'Purchase Order';
DataDictionaryEN['PurchaseOrderReference'] = 'Purchase Order Reference';
DataDictionaryEN['CreateAutomaticInvoice'] = 'Create Automatic Invoice';
DataDictionaryEN['AutomaticAdditionalCostProration'] = 'Automatic Additional Cost Proration';
DataDictionaryEN['Vendor'] = 'Vendor';
DataDictionaryEN['Description'] = 'Description';
DataDictionaryEN['HeaderDiscountAmount'] = 'Discount Amount';
DataDictionaryEN['HeaderDiscountPercentage'] = 'Discount Percentage';
DataDictionaryEN['NumberOfLines'] = 'Number Of Lines';
DataDictionaryEN['TotalCost'] = 'Total Cost';
DataDictionaryEN['TotalRQuantity'] = 'Total Recived Quantity';
DataDictionaryEN['TotalAmountWithTax'] = 'Total Amount With Tax';
DataDictionaryEN['TotalAmountAfterDiscount'] = 'Total Amount After Discount';
DataDictionaryEN['TotalAmountBeforeDiscount'] = 'Total Amount Without Discount';
//////List Header English Dictionary JS
DataDictionaryEN['HReference'] = 'Reference Number : ';
DataDictionaryEN['HReceiptDate'] = 'Receipt Date : ';
DataDictionaryEN['HTotalCost'] = 'Total Cost : ';
////Tab English Dictionary JS
DataDictionaryEN['AdditionalCostAmount'] = 'Additional Cost Amount';
DataDictionaryEN['AddNewAdditionalCost'] = 'Add New AdditionalCost';
DataDictionaryEN['LoadReceipts'] = 'Load Receipts';
DataDictionaryEN['PurchaseOrderQuantity'] = 'PO Qty';
DataDictionaryEN['ReceivedQuantity'] = 'Received Qty';
DataDictionaryEN['OutstandingQuantity'] = 'Outstanding Quantity';
DataDictionaryEN['UOM'] = 'UOM';
DataDictionaryEN['PleasLoadReceipts'] = 'Please Load Receipts';
DataDictionaryEN['InvoiceDocumentNumber'] = 'Invoice Document Number';

DataDictionaryEN['PostingPO'] = 'Save and posting Purchase Order';
DataDictionaryEN['AreYouSureToPostPO'] = 'Are you sure you want to save and post purchase order?';
DataDictionaryEN['LockedPeriod'] = 'Posting date can not be within a locked period';
DataDictionaryEN['OriginalQuantity'] = 'Original Quantity';
DataDictionaryEN['PurchaseOrderQuantity'] = 'Purchase Order Quantity';
DataDictionaryEN['OutstandingQuantity'] = 'Outstanding Quantity';
DataDictionaryEN['EditQuantity'] = 'Edit Quantity';
DataDictionaryEN['POInquery'] = 'Purchase Order Status';


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////// PO Receipt Return //////////////////////////////////////
DataDictionaryEN["ReceiptReturn"] = 'Receipt Return';
DataDictionaryEN["PoReceiptReturn"] = 'PO Receipt Return';
DataDictionaryEN["AddPoReceiptReturn"] = 'Add PO Receipt Return';
DataDictionaryEN['PoReceiptReturnDetails'] = 'PO Receipt Return Details';
DataDictionaryEN['PoReceiptReturn'] = 'PO Receipt Return';

DataDictionaryEN['ReturnReference'] = 'Return Reference';
DataDictionaryEN['ReturnDate'] = 'Return Date';
DataDictionaryEN['ReturnDescription'] = 'Return Description';
DataDictionaryEN['ReturnAllQuantity'] = 'Return All Quantity';
DataDictionaryEN['Vendor'] = 'Vendor';
DataDictionaryEN['TaxClass'] = 'Tax Class';
DataDictionaryEN['Currency'] = 'Currency';
DataDictionaryEN['ExchangeRate'] = 'Exchange Rate';
DataDictionaryEN['ReceiptTotalAmount'] = 'Receipt Total Amount';
DataDictionaryEN['ReturnedCost'] = 'Returned Cost';
DataDictionaryEN['ReturnedQuantity'] = 'Returned Quantity';
DataDictionaryEN['RelatedInvoice'] = 'Related Invoice';
DataDictionaryEN['RelatedInvoicesIds'] = 'Related Invoices Ids';
DataDictionaryEN['PoReceiptAdditionalCost'] = 'PO Receipt Additional Cost';
DataDictionaryEN['AdditionalCostAction'] = 'Additional Cost Action';
DataDictionaryEN['IsPosted'] = 'Is Posted';
DataDictionaryEN['PostingDate'] = 'Posting Date';

//////////////////////////////////////////////InqueryPage //////////////////////////////////////////
DataDictionaryEN['PurchaseOrderStatus'] = 'Purchase Order Status';
DataDictionaryEN['PurchaseOrderReference'] = 'Purchase Order Reference';
DataDictionaryEN['ReceiptReference'] = 'Receipt Reference';
DataDictionaryEN['InvoiceReference'] = 'Invoice Reference';
DataDictionaryEN['Results'] = 'Results';
DataDictionaryEN['Transaction'] = 'Transaction';
DataDictionaryEN['TransactionDate'] = 'Transaction Date';
DataDictionaryEN['TypeCreatedDate'] = 'Type Create Date';
DataDictionaryEN['TypeTransactionDate'] = 'Type Transaction Date';
DataDictionaryEN['TypeTotalAmount'] = 'Type Total Amount';
DataDictionaryEN['ExceedsUnitTotal'] = 'Exceeds Ext. cost';
DataDictionaryEN['ExceedsPOTotalAmount'] = 'Exceeds PO Total Amount';
DataDictionaryEN['ExceedsReciptTotalAmount'] = 'Exceeds Total Amount';

//////////////////////////////////////// End Purchase Order ///////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



DataDictionaryEN["ParentInvoiceNotPostedYet"] = 'Parent invoice of one or more of the refund invoices is/are not posted yet, please post them then post refund again.';

DataDictionaryEN["ShipmentDiscountExceedsTotalAmount"] = 'Shipment Discount Exceeds Total Amount';



DataDictionaryEN["DetailsCurrenciesAreUsedBySystem"] = 'Failed to delete; Details Currencies Are Used By ERP system data';


DataDictionaryEN["IcReceiptDiscountExceedsReceiptAmount"] = 'Receipt Discount Exceeds it\'s Amount, resulting in a negative total cost!';


DataDictionaryEN["CurrencyAlreadyExists"] = 'Currency Cannot be duplicated for the same header currency';

DataDictionaryEN["QuantityInLocationsIsNotEnoughtoFullyReturnReceipt"] = 'Quantities In Locations are Not Enough to Fully Return Receipt, Negative Quantities are not allowed !';

DataDictionaryEN["POSummary"] = "Purchase Order Summary";

/////////////////////////////////////////English Dictionary JS///////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryEN["PoReturnReceipt"] = 'Receipt Return ';
DataDictionaryEN["AddPoReturnReceipt"] = 'Add New Return Receipt ';
DataDictionaryEN['PoReturnReceiptDetails'] = 'Receipt Return Details';
DataDictionaryEN['PoReturnReceipt'] = 'Receipt Return ';
DataDictionaryEN['ReturnReferenceNumber'] = 'Return Reference Number';
DataDictionaryEN['Receipt'] = 'Receipt';
DataDictionaryEN['ReturnDescription'] = 'Return Description';
DataDictionaryEN['ReturnDate'] = 'Return Date';
DataDictionaryEN['ReturnAllQuantity'] = 'Return All Quantity';
DataDictionaryEN['Vendor'] = 'Vendor';
DataDictionaryEN['TaxClass'] = 'Tax Class';
DataDictionaryEN['Currency'] = 'Currency';
DataDictionaryEN['ExchangeRate'] = 'Exchange Rate';
DataDictionaryEN['ReceiptTotalQuantity'] = 'Receipt Total Quantity';
DataDictionaryEN['ReturnTotalQuantity'] = 'Return Total Quantity';
DataDictionaryEN['ReceiptTotalAmountWithTax'] = 'Receipt Total Amount With Tax';
DataDictionaryEN['ReceiptTotalAmountAfterDiscount'] = 'Receipt Total Amount After Discount';
DataDictionaryEN['ReceiptTotalAmountBeforeDiscount'] = 'Receipt Total Amount Before Discount';
DataDictionaryEN['ReturnTotalAmountWithTax'] = 'Return Total Amount With Tax';
DataDictionaryEN['ReturnTotalAmountAfterDiscount'] = 'Return Total Amount After Discount';
DataDictionaryEN['ReturnTotalAmountBeforeDiscount'] = 'Return Total Amount Before Discount';
DataDictionaryEN['RelatedInvoice'] = 'Related Invoice';
DataDictionaryEN['InvoiceReference'] = 'Invoice Reference';
DataDictionaryEN['InvoiceAmount'] = 'Invoice Amount';
DataDictionaryEN['NumberOfLines'] = 'Number Of Lines';

DataDictionaryEN["AlreadyExist"] = ' Already Exist ';
//////List Header English Dictionary JS
DataDictionaryEN['HReferenceNumber'] = 'Reference Number : ';
DataDictionaryEN['HReturnDate'] = 'Return Date : ';
DataDictionaryEN['HReturnTotalAmountWithTax'] = 'Return Total Amount With Tax : ';
DataDictionaryEN['ReceiptTotalAmount'] = 'Receipt Total Amount';
DataDictionaryEN['ReturnQuantity'] = 'Return Quantity';
DataDictionaryEN['ReturnTotalCost'] = 'Return Total Cost';
DataDictionaryEN['HReturnTotalCost'] = 'Return Total Cost : ';
DataDictionaryEN["ReceiptReturnLineDetails"] = 'Receipt Return Line Details';

DataDictionaryEN["ErrorPostDate"] = 'Posting date must be greater than or equal transaction date';

////Tab English Dictionary JS

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryEN['PoUploadLog'] = 'Po Upload Log';
DataDictionaryEN['CompletedWithError'] = 'Completed With Error';
DataDictionaryEN['ExportTemplate'] = 'Export Template';
DataDictionaryEN['AdditionalCostClearingAccount'] = 'Additional Cost Clearing Account';

DataDictionaryEN['NonProratedAdditionalCostTotal'] = 'Non Prorated Additional Cost Total';
DataDictionaryEN['LineTotalCost'] = 'Line Total Cost';
DataDictionaryEN['TotalCostInfo'] = 'this total includes the Receipt total after the discount including the prorated additional Cost and the Tax.';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////English Dictionary JS///////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryEN["CustomFild"] = 'Custom Field';
DataDictionaryEN["AddCustomFild"] = 'Add Custom Field'; 
DataDictionaryEN['CustomFildDetails'] = 'Custom Field Details';
DataDictionaryEN['CustomFild'] = 'Custom Field';
DataDictionaryEN['FildName'] = 'Field Name';
DataDictionaryEN['FildType'] = 'Field Type';
DataDictionaryEN['IsRequired'] = 'Is Required';
DataDictionaryEN['Validation'] = 'Validation';
DataDictionaryEN['Action'] = 'Action';
DataDictionaryEN['CreartedDate'] = 'Crearted Date';
DataDictionaryEN['IsSysetm'] = 'Is Sysetm';   
  
DataDictionaryEN["AlreadyExist"] = ' Already Exist ';
//////List Header English Dictionary JS 

////Tab English Dictionary JS 
DataDictionaryEN["AdjustmentReport"] = 'Adjustment Report';

DataDictionaryEN['ErrorNumberDetailsLine'] = 'Details must less than 100 line';

DataDictionaryEN['ErrorQtyZero'] = 'po cannot be "saved" or "saved asd posted" having no quantity determined';


DataDictionaryEN["PostingPODateMissing"] = 'Posting Date Missing';
DataDictionaryEN["EditThenPost"] = 'To proceed with posting, please edit to add posting date then Save and Post';

DataDictionaryEN["RollupAccountsReport"] = 'Rollup Accounts';
DataDictionaryEN["RollupAccountsReportt"] = 'Rollup Accounts Report';

DataDictionaryEN["VendorBalanceReport"] = 'Vendor Balance';
DataDictionaryEN["VendorBalanceReportt"] = 'Vendor Balance Report';
DataDictionaryEN["CustomerBalanceReport"] = 'Customer Balance';
DataDictionaryEN["CustomerBalanceReportt"] = 'Customer Balance Report'; DataDictionaryEN['HReturnTotalCost'] = 'Return Total Cost : ';

DataDictionaryEN['AdjustmentDateFrom'] = 'Adjustment Date From';
DataDictionaryEN['AdjustmentDateTo'] = 'Adjustment Date To';

DataDictionaryEN["ItemLocation0Qty"] = 'Item has 0 quantity on this location. Cannot Decrease';
DataDictionaryEN["ItemLocationQtyLessThanAvailable"] = 'Exceeds Available quantity';

DataDictionaryEN["ItemLocation0Cost"] = 'Item has 0 Cost on this location. Cannot Decrease';
DataDictionaryEN["ItemLocationCostLessThanAvailable"] = 'Average Cost for this item location is less than this';

DataDictionaryEN["AdjustmentReport"] = 'Adjustment Report';

DataDictionaryEN["AutoGenerated"] = 'Auto Generated';

DataDictionaryEN["POHeaderDiscountPercentage"] = 'Header Discount Percentage';
DataDictionaryEN["POHeaderDiscountAmount"] = 'Header Discount Amount';

DataDictionaryEN["AreYouSureYouWantToContinueWithPosting"] = 'Are You Sure You Want to Continue With Posting ! ';

DataDictionaryEN["CustomField"] = "Custom Field";
DataDictionaryEN["CustomFieldTo"] = "Custom Field To";
DataDictionaryEN["Adjustment"] = "Adjustment";
DataDictionaryEN["Receipt"] = "Receipt";
DataDictionaryEN["OriginalBank"] = "Original Bank";
DataDictionaryEN["PettyCash"] = "Petty Cash";
DataDictionaryEN["CustomerFrom"] = "Customer From";
DataDictionaryEN["CustomerTo"] = "Customer To";
DataDictionaryEN["TransctionNumber"] = "Transaction Number";
DataDictionaryEN["TransactionType"] = "Transaction Type";
DataDictionaryEN["TransactionPayments"] = "Transaction Payment";
DataDictionaryEN["OriginalTransactionNumber"] = "Returned Invoice No.";
DataDictionaryEN["subtotal"] = "Total Before Disc & Tax";
DataDictionaryEN["ItemsDiscount"] = "Item Discount";
DataDictionaryEN["HeaderDiscount"] = "Header Discount";
DataDictionaryEN["SalesTax"] = "Sales Tax";
DataDictionaryEN["GrandTotal"] = "Grand Total";
DataDictionaryEN["OnAccount"] = "On Account";
DataDictionaryEN["Cash"] = "Cash Amount";
DataDictionaryEN["Credit"] = "Credit Amount";
DataDictionaryEN["ChequeAmount"] = "Cheque Amount";
DataDictionaryEN["TotalPayment"] = "Total Payments";
DataDictionaryEN["TotalCash"] = "Total Cash";
DataDictionaryEN["TotalItemDiscount"] = "Total Item Discount";
DataDictionaryEN["TotalOnAccount"] = "Total On Account";
DataDictionaryEN["TotalInvoiceDiscount"] = "Total Invoice Discount";
DataDictionaryEN["TotalCreditCard"] = "Total Credit Cards";
DataDictionaryEN["TotalTax"] = "Total Sales Tax";
DataDictionaryEN["TotalCheques"] = "Total Cheque";
DataDictionaryEN["TotalSales"] = "Total Sales";
DataDictionaryEN["TotalPayments"] = "Total Payments";
DataDictionaryEN["Refresh"] = "Refresh";
DataDictionaryEN["QuantityInquiry"] = "Quantity Inquiry";
DataDictionaryEN["ItemName"] = "Item Name";
DataDictionaryEN["ICQuantity"] = "ERP Current Avaliable Qty";
DataDictionaryEN["StagingQuantity"] = "POS Staging Sales Qty";
DataDictionaryEN["RegistersQuantity"] = "POS Registers Qty";
DataDictionaryEN["POSStore"] = "POS Stores";
DataDictionaryEN["PendingQty"] = "POS Register Sales Qty";
DataDictionaryEN["SyncedQuantity"] = "Synced Qty";
DataDictionaryEN["NotSyncedQuantity"] = "Not Synced Transactions";
DataDictionaryEN["TotalPOSQuantity"] = "POS Module Current Available Qty";
DataDictionaryEN["ERPSummation"] = "ERP Summation Qty";
DataDictionaryEN["StagingOutQuantity"] = "POS Staging Return Qty";
DataDictionaryEN["PendingOutQty"] = "POS Register Return Qty";
DataDictionaryEN["exceedtheavailbleQuantity"] = "exceed the available quantity in the source location";
DataDictionaryEN["ItemLocationsummary"] = "Item Location Summary";
DataDictionaryEN["ICAverageCost"] = "Average Cost";
DataDictionaryEN["InvoiceNumberTo"] = "Invoice Number To";
DataDictionaryEN["InvoiceNumberFrom"] = "Invoice Number From";



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////Start Purchase Order Module ////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////PO Invoice English Dictionary JS///////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryEN["PoInvoice"] = 'Invoices';
DataDictionaryEN["PoInvoicePTitle"] = 'PO Invoices';
DataDictionaryEN["AddPoInvoice"] = 'Add New Invoice';
DataDictionaryEN['PoInvoiceDetails'] = 'Invoice Details';
DataDictionaryEN['Invoice'] = 'Invoice';
DataDictionaryEN['InvoiceReference'] = 'Invoice Reference';
DataDictionaryEN['InvoiceTotalAmount'] = 'Invoice Total Amount';
DataDictionaryEN['Vendor'] = 'Vendor';
DataDictionaryEN['ReceiptIds'] = 'Receipt Ids';
DataDictionaryEN['InvoiceDate'] = 'Invoice Date';
DataDictionaryEN['InvoicingDate'] = 'Invoicing Date';
DataDictionaryEN['RelatedPO'] = 'Related PO';
DataDictionaryEN['ReceiptItemLine'] = 'Receipt Item Line';
DataDictionaryEN['InvoiceTotalAmount'] = 'Invoice Total Amount';
DataDictionaryEN['PostingDate'] = 'Posting Date';
DataDictionaryEN['Description'] = 'Description';
DataDictionaryEN['DocumentReferenceNumber'] = 'Invoice Reference Number';
DataDictionaryEN['InvoiceNumber'] = 'Invoice Number';
DataDictionaryEN['TotalAmountWithTax'] = 'Total Amount With Tax';
DataDictionaryEN['TotalAmountAfterDiscount'] = 'Total Amount After Discount';
DataDictionaryEN['TotalAmountBeforDiscount'] = 'Total Amount Befor Discount';
DataDictionaryEN['DiscountAmount'] = 'Discount Amount';
DataDictionaryEN['DiscountPercentage'] = 'Discount Percentage';
//////List Header English Dictionary JS
DataDictionaryEN['InvoiceReference'] = 'Invoice Reference';
DataDictionaryEN['InvoiceTotalAmount'] = 'Invoice Total Amount';
////Tab English Dictionary JS
DataDictionaryEN['AdditionalCost'] = 'Additional Cost';
DataDictionaryEN['AddNewAdditionalCost'] = 'Add New AdditionalCost';

///////////////////////////////////////////////////////////////////PO entity/////////////////////////////////////////////////////////////////////////////////////////////

DataDictionaryEN["PurchaseOrderHeader"] = 'Purchase Order';
DataDictionaryEN["AddPurchaseOrderHeader"] = 'Add New Purchase Order';
DataDictionaryEN['PurchaseOrderHeaderDetails'] = 'Purchase Order Details';
DataDictionaryEN['ReferenceNumber'] = 'Reference Number';
DataDictionaryEN['PurchaseDate'] = 'Purchase Order Date';
DataDictionaryEN['Currency'] = 'Currency';
DataDictionaryEN['Status'] = 'Status';
DataDictionaryEN['AutoGenerateReceipt'] = 'Auto Generate Receipt';
DataDictionaryEN['AutoGenerateInvoice'] = 'Auto Generate Invoice';
DataDictionaryEN['Vendor'] = 'Vendor';
DataDictionaryEN['TaxClass'] = 'Tax Class';
DataDictionaryEN['ExchangeRate'] = 'Exchange Rate';
DataDictionaryEN['Description'] = 'Description';
DataDictionaryEN['HeaderDiscountAmount'] = 'Discount Amount';
DataDictionaryEN['HeaderDiscountPercentage'] = 'Discount Percentage';
DataDictionaryEN['NumberOfLines'] = 'Number Of Lines';
DataDictionaryEN['TotalCost'] = 'Total Cost';
DataDictionaryEN['TotalQuantity'] = 'Total Quantity';
DataDictionaryEN['TotalAmountWithTax'] = 'Total Amount With Tax';
DataDictionaryEN['TotalAmountAfterDiscount'] = 'Total Amount After Discount';
DataDictionaryEN['TotalAmountBeforeDiscount'] = 'Total Amount Without Discount';
DataDictionaryEN['IsPosted'] = 'Is Posted';
DataDictionaryEN['PostingDate'] = 'Posting Date';
DataDictionaryEN['ItemCard'] = 'Item Card';
DataDictionaryEN['ItemDescription'] = 'Item Description';
DataDictionaryEN['Location'] = 'Location';
DataDictionaryEN['Quantity'] = 'Quantity';
DataDictionaryEN['UnitOfMeasure'] = 'Unit Of Measure';
DataDictionaryEN['UnitCost'] = 'Unit Cost';
DataDictionaryEN['TaxClass'] = 'Tax Class';
DataDictionaryEN['UnitDiscountAmount'] = 'Discount Amount';
DataDictionaryEN['UnitDiscountPercentage'] = 'Discount Percentage';
DataDictionaryEN['ReceivedQuantity'] = 'Received Quantity';
DataDictionaryEN['OutstandingQuantity'] = 'Outstanding Quantity';
DataDictionaryEN['UnitTotalAmountBeforeDiscount'] = 'Total Amount Before Discount';
DataDictionaryEN['UnitTotalAmountAfterDiscount'] = 'Total Amount After Discount';
DataDictionaryEN['UnitTotalAmountWithTax'] = 'Total Amount With Tax';
DataDictionaryEN['Comments'] = 'Comments';
DataDictionaryEN['IsLastOne'] = 'Is Last One';
DataDictionaryEN['POStatusNew'] = 'New';
DataDictionaryEN['POStatusInProgress'] = 'In Progress';
DataDictionaryEN['POStatusCompleted'] = 'Completed';

DataDictionaryEN['HeaderReferenceNumber'] = "Purchase Order Number: ";
DataDictionaryEN['HeaderPODate'] = "Purchase Order Date: ";
DataDictionaryEN['HeaderStatus'] = "Status: ";

DataDictionaryEN['PODetails'] = "Purchase Order Lines Details  ";
/////////////////////////////////////////English Dictionary JS///////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryEN["PoReceipt"] = 'Receipts';
DataDictionaryEN["PoReceiptPTitle"] = 'PO Receipts';
DataDictionaryEN["Receipt"] = 'Receipt';
DataDictionaryEN["ReceiptSummary"] = 'Receipt Summary';
DataDictionaryEN["AddPoReceipt"] = 'Add New Receipt';
DataDictionaryEN["ReceiptDetails"] = 'Receipt Details';
DataDictionaryEN["ReceiptLineDetails"] = 'Receipt Line Details';
DataDictionaryEN['PoReceiptDetails'] = 'Receipt Details';
DataDictionaryEN['SummaryPoReceiptDetails'] = 'PO Summary Receipt Details';
DataDictionaryEN['Receipt'] = 'Receipt';
DataDictionaryEN['ReceiptStatus'] = 'Receipt Status';
DataDictionaryEN['Reference'] = 'Reference Number';
DataDictionaryEN['ReceiptDate'] = 'Receipt Date';
DataDictionaryEN['PurchaseOrder'] = 'Purchase Order';
DataDictionaryEN['PurchaseOrderReference'] = 'Purchase Order Reference';
DataDictionaryEN['CreateAutomaticInvoice'] = 'Create Automatic Invoice';
DataDictionaryEN['AutomaticAdditionalCostProration'] = 'Automatic Additional Cost Proration';
DataDictionaryEN['Vendor'] = 'Vendor';
DataDictionaryEN['Description'] = 'Description';
DataDictionaryEN['HeaderDiscountAmount'] = 'Discount Amount';
DataDictionaryEN['HeaderDiscountPercentage'] = 'Discount Percentage';
DataDictionaryEN['NumberOfLines'] = 'Number Of Lines';
DataDictionaryEN['TotalCost'] = 'Total Cost';
DataDictionaryEN['TotalRQuantity'] = 'Total Recived Quantity';
DataDictionaryEN['TotalAmountWithTax'] = 'Total Amount With Tax';
DataDictionaryEN['TotalAmountAfterDiscount'] = 'Total Amount After Discount';
DataDictionaryEN['TotalAmountBeforeDiscount'] = 'Total Amount Without Discount';
//////List Header English Dictionary JS
DataDictionaryEN['HReference'] = 'Reference Number : ';
DataDictionaryEN['HReceiptDate'] = 'Receipt Date : ';
DataDictionaryEN['HTotalCost'] = 'Total Cost : ';
////Tab English Dictionary JS
DataDictionaryEN['AdditionalCostAmount'] = 'Additional Cost Total';
DataDictionaryEN['AddNewAdditionalCost'] = 'Add New AdditionalCost';
DataDictionaryEN['LoadReceipts'] = 'Load Receipts';
DataDictionaryEN['PurchaseOrderQuantity'] = 'PO Qty';
DataDictionaryEN['ReceivedQuantity'] = 'Received Qty';
DataDictionaryEN['OutstandingQuantity'] = 'Outstanding Quantity';
DataDictionaryEN['UOM'] = 'Unit Of Measure';
DataDictionaryEN['PleasLoadReceipts'] = 'Please Load Receipts';
DataDictionaryEN['InvoiceDocumentNumber'] = 'Invoice Document Number';

DataDictionaryEN['PostingPO'] = 'Save and posting Purchase Order';
DataDictionaryEN['AreYouSureToPostPO'] = 'Are you sure you want to save and post purchase order?';
DataDictionaryEN['PostingOE'] = 'Save and posting Order Entry';
DataDictionaryEN['AreYouSureToPostOE'] = 'Are you sure you want to save and post order entry?';
DataDictionaryEN['LockedPeriod'] = 'Posting date can not be within a locked period';
DataDictionaryEN['OriginalQuantity'] = 'Original Quantity';
DataDictionaryEN['PurchaseOrderQuantity'] = 'Purchase Order Quantity';
DataDictionaryEN['OutstandingQuantity'] = 'Outstanding Quantity';
DataDictionaryEN['EditQuantity'] = 'Edit Quantity';
DataDictionaryEN['POInquery'] = 'Purchase Order Status';


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////// PO Receipt Return //////////////////////////////////////
DataDictionaryEN["ReceiptReturn"] = 'Receipt Return';
DataDictionaryEN["PoReceiptReturn"] = 'PO Receipt Return';
DataDictionaryEN["AddPoReceiptReturn"] = 'Add PO Receipt Return';
DataDictionaryEN['PoReceiptReturnDetails'] = 'PO Receipt Return Details';
DataDictionaryEN['PoReceiptReturn'] = 'PO Receipt Return';

DataDictionaryEN['ReturnReference'] = 'Return Reference';
DataDictionaryEN['ReturnDate'] = 'Return Date';
DataDictionaryEN['ReturnDescription'] = 'Return Description';
DataDictionaryEN['ReturnAllQuantity'] = 'Return All Quantity';
DataDictionaryEN['Vendor'] = 'Vendor';
DataDictionaryEN['TaxClass'] = 'Tax Class';
DataDictionaryEN['Currency'] = 'Currency';
DataDictionaryEN['ExchangeRate'] = 'Exchange Rate';
DataDictionaryEN['ReceiptTotalAmount'] = 'Receipt Total Amount';
DataDictionaryEN['ReturnedCost'] = 'Returned Cost';
DataDictionaryEN['ReturnedQuantity'] = 'Returned Quantity';
DataDictionaryEN['RelatedInvoice'] = 'Related Invoice';
DataDictionaryEN['RelatedInvoicesIds'] = 'Related Invoices Ids';
DataDictionaryEN['PoReceiptAdditionalCost'] = 'PO Receipt Additional Cost';
DataDictionaryEN['AdditionalCostAction'] = 'Additional Cost Action';
DataDictionaryEN['IsPosted'] = 'Is Posted';
DataDictionaryEN['PostingDate'] = 'Posting Date';

//////////////////////////////////////////////InqueryPage //////////////////////////////////////////
DataDictionaryEN['PurchaseOrderStatus'] = 'PO Transaction Status';
DataDictionaryEN['PurchaseOrderReference'] = 'Purchase Order Reference';
DataDictionaryEN['ReceiptReference'] = 'Receipt Reference';
DataDictionaryEN['InvoiceReference'] = 'Invoice Reference';
DataDictionaryEN['Results'] = 'Results';
DataDictionaryEN['Transaction'] = 'Transaction';
DataDictionaryEN['TransactionDate'] = 'Transaction Date';
DataDictionaryEN['TypeCreatedDate'] = 'Type Create Date';
DataDictionaryEN['TypeTransactionDate'] = 'Type Transaction Date';
DataDictionaryEN['TypeTotalAmount'] = 'Transaction Total Amount';
DataDictionaryEN['ExceedsUnitTotal'] = 'Exceeds Ext. cost';
DataDictionaryEN['ExceedsPOTotalAmount'] = 'Exceeds PO Total Amount';
DataDictionaryEN['ExceedsReciptTotalAmount'] = 'Exceeds Total Amount';

//////////////////////////////////////// End Purchase Order ///////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



DataDictionaryEN["ParentInvoiceNotPostedYet"] = 'Parent invoice of one or more of the refund invoices is/are not posted yet, please post them then post refund again.';

DataDictionaryEN["ShipmentDiscountExceedsTotalAmount"] = 'Shipment Discount Exceeds Total Amount';



DataDictionaryEN["DetailsCurrenciesAreUsedBySystem"] = 'Failed to delete; Details Currencies Are Used By ERP system data';


DataDictionaryEN["IcReceiptDiscountExceedsReceiptAmount"] = 'Receipt Discount Exceeds it\'s Amount, resulting in a negative total cost!';


DataDictionaryEN["CurrencyAlreadyExists"] = 'Currency Cannot be duplicated for the same header currency';

DataDictionaryEN["QuantityInLocationsIsNotEnoughtoFullyReturnReceipt"] = 'Quantities In Locations are Not Enough to Fully Return Receipt, Negative Quantities are not allowed !';

DataDictionaryEN["POSummary"] = "Purchase Order Summary";

/////////////////////////////////////////English Dictionary JS///////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryEN["PoReturnReceipt"] = 'Receipt Return ';
DataDictionaryEN["PoReturnReceiptPTitle"] = 'PO Receipt Return ';
DataDictionaryEN["AddPoReturnReceipt"] = 'Add New Return Receipt ';
DataDictionaryEN['PoReturnReceiptDetails'] = 'Receipt Return Details';
DataDictionaryEN['PoReturnReceipt'] = 'Receipt Return ';
DataDictionaryEN['ReturnReferenceNumber'] = 'Return Reference Number';
DataDictionaryEN['Receipt'] = 'Receipt';
DataDictionaryEN['ReturnDescription'] = 'Return Description';
DataDictionaryEN['ReturnDate'] = 'Return Date';
DataDictionaryEN['ReturnAllQuantity'] = 'Return All Quantity';
DataDictionaryEN['Vendor'] = 'Vendor';
DataDictionaryEN['TaxClass'] = 'Tax Class';
DataDictionaryEN['Currency'] = 'Currency';
DataDictionaryEN['ExchangeRate'] = 'Exchange Rate';
DataDictionaryEN['ReceiptTotalQuantity'] = 'Receipt Total Quantity';
DataDictionaryEN['ReturnTotalQuantity'] = 'Return Total Quantity';
DataDictionaryEN['ReceiptTotalAmountWithTax'] = 'Receipt Total Amount With Tax';
DataDictionaryEN['ReceiptTotalAmountAfterDiscount'] = 'Receipt Total Amount After Discount';
DataDictionaryEN['ReceiptTotalAmountBeforeDiscount'] = 'Receipt Total Amount Before Discount';
DataDictionaryEN['ReturnTotalAmountWithTax'] = 'Return Total Amount With Tax';
DataDictionaryEN['ReturnTotalAmountAfterDiscount'] = 'Return Total Amount After Discount';
DataDictionaryEN['ReturnTotalAmountBeforeDiscount'] = 'Return Total Amount Before Discount';
DataDictionaryEN['RelatedInvoice'] = 'Related Invoice';
DataDictionaryEN['InvoiceReference'] = 'Invoice Reference';
DataDictionaryEN['InvoiceAmount'] = 'Invoice Amount';
DataDictionaryEN['NumberOfLines'] = 'Number Of Lines';

DataDictionaryEN["AlreadyExist"] = ' Already Exist ';
//////List Header English Dictionary JS
DataDictionaryEN['HReferenceNumber'] = 'Reference Number : ';
DataDictionaryEN['HReturnDate'] = 'Return Date : ';
DataDictionaryEN['HReturnTotalAmountWithTax'] = 'Return Total Amount With Tax : ';
DataDictionaryEN['ReceiptTotalAmount'] = 'Receipt Total Amount';
DataDictionaryEN['ReturnQuantity'] = 'Return Quantity';
DataDictionaryEN['ReturnTotalCost'] = 'Return Total Cost';
DataDictionaryEN['HReturnTotalCost'] = 'Return Total Cost : ';
DataDictionaryEN["ReceiptReturnLineDetails"] = 'Receipt Return Line Details';

DataDictionaryEN["ErrorPostDate"] = 'Posting date must be greater than or equal transaction date';

////Tab English Dictionary JS

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryEN['PoUploadLog'] = 'PO Upload Log';
DataDictionaryEN['CompletedWithError'] = 'Completed With Error';
DataDictionaryEN['ExportTemplate'] = 'Export Template';
DataDictionaryEN['AdditionalCostClearingAccount'] = 'Additional Cost Clearing Account';

DataDictionaryEN['NonProratedAdditionalCostTotal'] = 'Non Prorated Additional Cost Total';
DataDictionaryEN['LineTotalCost'] = 'Line Total Cost';
DataDictionaryEN['TotalCostInfo'] = 'this total includes the Receipt total after the discount including the prorated additional Cost and the Tax.';

/////////////////////////////////////////English Dictionary JS///////////////////////////////////////////////////////////////////////////////////////////////////
DataDictionaryEN["OeOrderEntry"] = 'OeOrderEntry';
DataDictionaryEN["AddOeOrderEntry"] = 'Add OeOrderEntry';
DataDictionaryEN['OeOrderEntryDetails'] = 'Order Entry Details';
DataDictionaryEN['OeOrderEntry'] = 'OE Order Entry';
DataDictionaryEN['OeReference'] = 'OE Reference';
DataDictionaryEN['OeDate'] = 'OE Date';
DataDictionaryEN['PostingDate'] = 'Posting Date';
DataDictionaryEN['DeliveryDate'] = 'Delivery Date';
DataDictionaryEN['OeStatus'] = 'OE Status';
DataDictionaryEN['Customer'] = 'Customer';
DataDictionaryEN['TaxClass'] = 'Tax Class';
DataDictionaryEN['Currency'] = 'Currency';
DataDictionaryEN['ExchangeRate'] = 'Exchange Rate';
DataDictionaryEN['Description'] = 'Description';
DataDictionaryEN['DiscountAmount'] = 'Discount Amount';
DataDictionaryEN['DiscountPercentage'] = 'Discount Percentage';
DataDictionaryEN['TotalAmountBeforeDiscount'] = 'Total Amount Before Discount';
DataDictionaryEN['TotalAmountAfterDiscount'] = 'Total Amount After Discount';
DataDictionaryEN['TotalAmountWithTax'] = 'Total Amount With Tax';
DataDictionaryEN['TotalQuantity'] = 'Total Quantity';
DataDictionaryEN['TotalOrderProfitability'] = 'Total Order Profitability';

DataDictionaryEN["AlreadyExist"] = ' Already Exist ';

DataDictionaryEN['OrderEntry'] = 'Order Entry';
DataDictionaryEN['AddOrderEntry'] = 'Add New Order Entry';
DataDictionaryEN['OEAmountDetails'] = 'Order Entry Amount Details';
DataDictionaryEN['OELineDetails'] = 'Order Entry Line Details';
DataDictionaryEN['OEQuantity'] = 'Order Entry Quantity';
DataDictionaryEN['ShippedQuantity'] = 'Shipped Quantity';
DataDictionaryEN['EstimatedProfitability'] = 'Estimated Profitability';
DataDictionaryEN['EstimatedExtendedProfitability'] = 'Estimated Extended Profitability';
DataDictionaryEN['ActualProfitability'] = 'Actual Profitability';
DataDictionaryEN['ActualExtendedProfitability'] = 'Actual Extended Profitability';
DataDictionaryEN['ReservedQuantity'] = 'Reserved Quantity';
DataDictionaryEN['AvailableQuantity'] = 'Available Quantity';
DataDictionaryEN['LineTotal'] = 'Line Total';
DataDictionaryEN['ErrorReservedQty'] = 'Reserved Quantity must be less than or equal Order Quantity !!';
DataDictionaryEN['DeliveryDateChange'] = 'Delivery Date Change';
DataDictionaryEN['AreYouSureToReflectDeliveryDate'] = 'Do you want to reflect this date to all lines ?';
DataDictionaryEN['ErrorDeliveryDate'] = 'Delivery Date must be same as or after the Order Entry Date And the Posting Date';
DataDictionaryEN['OEErrorPostDate'] = 'Posting Date must be same as or after the Order Entry Date';
 DataDictionaryEN['LineTotal'] = 'Line Total';
DataDictionaryEN['OrderEntrySummary'] = 'Order Entry Summary';
DataDictionaryEN['OELinesDetails'] = 'Order Entry Lines Details';
DataDictionaryEN['HOeDate'] = 'OE Date : ';
DataDictionaryEN['HOeReference'] = 'OE Reference : ';
//////List Header English Dictionary JS
DataDictionaryEN['OESaved'] = 'Saved';
DataDictionaryEN['OESaved'] = 'Saved';
DataDictionaryEN['successfully'] = 'successfully';
DataDictionaryEN['Faild'] = 'Faild';
DataDictionaryEN['StatusInfo'] = 'The status will be automatically updated as the following : Saved, Ready to deliver, Partially delivered and Fully delivered according to the user actions.';
DataDictionaryEN['UnitCostInfo'] = 'This represents the average \n cost at this time and does \n not represent the actual \n real cost ,and reflect the cost of \n the item stocking unit of measure.';
DataDictionaryEN['ExtendedCostInfo'] = 'This represents the average \n cost (Extended) at this time \n and does not represent the \n actual real cost.';
DataDictionaryEN['OEDateLockedPeriod'] = ' can not be within a locked period';
DataDictionaryEN['gridDelDateError1'] = 'Delivery Date in lines :';
DataDictionaryEN['gridDelDateError2'] = ' must be same as or after the Order Entry Date';
DataDictionaryEN['invldQtyReserved'] = 'Invalid Reseved Quantity in lines :';
DataDictionaryEN['PleseReselectCustomer'] = 'Please Reselect Customer';

DataDictionaryEN['gridVldLineDouplication'] = 'there is a duplication in the lines :';
////Tab English Dictionary JS

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////
DataDictionaryEN["AddOeInvoice"] = 'Add New Invoice';
DataDictionaryEN["RelatedOE"] = 'Related Order Entry';
DataDictionaryEN["OrderEntryDiscountAmount"] = 'OE Header Discount Percentage';
DataDictionaryEN["ShipmentItemLine"] = 'Shipment Item Lines Details';



DataDictionaryEN["OeShipmentDate"] = "Shipment Date";
DataDictionaryEN["OeShiomentStatus"] = "Shipment Status"
DataDictionaryEN["OEShipment"] = "OE Shipment";
DataDictionaryEN["AddOEShipment"] = "Add New Shipment";
DataDictionaryEN["CreateAutoInvoice"] = "Create Automatic Invoice";
DataDictionaryEN["TotalShipmentProfitability"] = "Total Shipment Profitability";
DataDictionaryEN["CreateAutoInvoice1"] = "CreateAutoInvoice";
DataDictionaryEN["OeShipmentReference"] = "Reference";
DataDictionaryEN["OEInvoiceReferenceNumber"] = "Invoice Reference Number";
DataDictionaryEN["TotalShippedQuantity"] = "Total Shipped Quantity";
DataDictionaryEN["ShippedMustBeLessThanOustandingOrder"] = "Shipped quantity must be equals or less than order out-standing quantity in lines ";
DataDictionaryEN["ShippedMustBeLessThanOustandingOrder1"] = "must be equals or less than order out-standing quantity";
DataDictionaryEN["OEShipmentGridOustandingError1"] = "shipped quantity in lines ";
DataDictionaryEN["ShipmentAddLinesPlease"] = "pease insert one line at least";
DataDictionaryEN["OeShipmentDetails"] = "Shipment Details";
DataDictionaryEN["OeShipmentEntrySummary"] = "Shipment Summary";
DataDictionaryEN["ShipmetLinesDetails"] = "Shipment Lines Details";
DataDictionaryEN["ShipmentAmountDetails"] = "Shipment Amount Details";
DataDictionaryEN["ShipmentLineDetails"] = "Shipment Lines Details";
DataDictionaryEN["OEShipmentLotQuantityMismatchError"] = "Quantity doesn't match the selected lots in the followng shipment line(s) :<br/>";
DataDictionaryEN["OeShipmentLotQuantityMismatchError"] = "Quantity doesn\'t match the selected lots in the following shipment line(s) :<br/>;";

DataDictionaryEN["OeShipmentErrorDeliveryDate"] = "Delivery date must be same as or after the shipment date";
DataDictionaryEN["OEShipmentErrorPostDate"] = "Posting Date must be same as or after the shipment Date";
DataDictionaryEN["OeShipmentSerialQuantityMismatchError"] = "Quantity doesn't match the selected serials in the followng shipment line(s) :<br/>";
DataDictionaryEN["OeShipmentSerialQuantityMismatchError"] = "Quantity doesn't match the selected serials in the followng shipment line(s) :<br/>";
DataDictionaryEN["HeaderOeShipmentReference"] = "Reference : ";
DataDictionaryEN["HeaderOeShipmentDate"] = "Shipment Date : ";
DataDictionaryEN["PostingOeShipment"] = "Save and posting OE Shipment";

DataDictionaryEN["AreYouSureToPostOEShipment"] = "Are you sure you want to save and post OE shipment?";
DataDictionaryEN["InvalidShipmentQtyLine"] = "Shipment quantity should be equal or more than zero in line : ";
DataDictionaryEN["ShipmentOeOrderEntry"] = "Order Entry";
DataDictionaryEN["InvalidShipmentAvailableQtyLine"] = "Shipped quantity must be less than available quantity in line ";
DataDictionaryEN["AreYouSureToPostOEShipment"] = "Are you sure you want to save and post OE shipment?";
DataDictionaryEN["OeShipmentQuantityShouldBeMoreThanZero"] = "Total shipment quantity should be more than zero";


DataDictionaryEN["LoadShipments"] = "Load Shipments";
DataDictionaryEN["PleasLoadShipment"] = "Please Load Shipment";
DataDictionaryEN["OEDisInfo"] = "This discount is added from \n the related Order Entry";
DataDictionaryEN["Shipments"] = "Shipments";
DataDictionaryEN["OEInvPostCfrm"] = "Are you sure you want to post this invoice ?";
DataDictionaryEN["PostConfirm"] = "Post Invoice";
DataDictionaryEN["InvoicedShipment"] = "one of the following shipments already invoiced before : <br/>";
DataDictionaryEN["CurrentAvailableQuantity"] = "Current Available Quantity";
DataDictionaryEN["OriginalQtyOnHand"] = "Quantity On Hand";
DataDictionaryEN["PendingOrderQty"] = "Pending Orders";

DataDictionaryEN["PendingOrderQtyInfo"] = "This quantity represents the remaining amount \n used in this and other Order entries for the same \n item from the same location.";
DataDictionaryEN["OriginalQtyOnHandInfo"] = "This quantity represents the quantity on hand \n for this item under this location when this \n order entry is created.";
DataDictionaryEN["CurrentAvailableQuantityInfo"] = "This quantity represents the quantity on hand \n for this item under this location in the inventory.";
DataDictionaryEN["OeInvoice"] = "OE Invoice";

DataDictionaryEN["OEInqueryStatus"] = "OE Transaction Status";
DataDictionaryEN["OEOrderReference"] = "Order Entry Reference";
DataDictionaryEN["OrderEntryStatus"] = "Order Entry Status";
DataDictionaryEN["OEShipmentReference"] = "Shipment Reference";
DataDictionaryEN["OEShipmentReturnINquery"] = "Shipment Return";
DataDictionaryEN["OEShipmentINqueryReference"] = "Shipment";
DataDictionaryEN["OEInvoiceINqueryReference"] = "Invoice";
DataDictionaryEN["OeInqueryStatus"] = "Transaction Status";
DataDictionaryEN["OeInqueryTransactionType"] = "Transaction Type";
DataDictionaryEN["OeInqueryTransactionCreatedDate"] = "Transaction Created Date";
DataDictionaryEN["OeInqueryTransactionDate"] = "Transaction Date";
DataDictionaryEN["OeDeliverDateTo"] = "Deliver Date To";
DataDictionaryEN["OeDeliverDateFrom"] = "Deliver Date From";
DataDictionaryEN["OEInqueryDeliveryStatus"] = "OE Delivery Status";
DataDictionaryEN["OeOrderDaysRemaining"] = "Days Remaining / Delay";
DataDictionaryEN["OeOrderDaysRemainingStatus"] = "Days Remaining Status";
DataDictionaryEN["OeDeliveryDate"] = "Delivery Date";
DataDictionaryEN["OeDeliveryStatusHeader"] = "Order";
DataDictionaryEN["OeDeliveryStatusDetails"] = "Line";

DataDictionaryEN['ExceedsOETotalAmount'] = 'Exceeds OE Total Amount';

DataDictionaryEN["ShipmentReturn"] = "Shipment Return";
DataDictionaryEN["ShipmentReturnPTitle"] = "OE Shipment Return";
DataDictionaryEN["AddShipmentReturn"] = "Add New Shipment Return";
DataDictionaryEN["ReturnDate"] = "Return Date";
DataDictionaryEN["ShipmentDocumentNumber"] = "Reference Number";

DataDictionaryEN["shipmentDate"] = "shipment Date";
DataDictionaryEN["ShipmentReturnDetails"] = "Shipment Return Details";
DataDictionaryEN["ShipmentStatus"] = "Shipment Status";
DataDictionaryEN["ReturnAllQuantity"] = "Return All Quantity";
DataDictionaryEN["OEShipPostCfrm"] = "Are you sure you want to post this shipment return ?";
DataDictionaryEN["SLReference"] = "Reference";
DataDictionaryEN["ReturnedShipment"] = "The selected shipment already returned !!";
DataDictionaryEN['OeUploadLog'] = 'OE Upload Log';
DataDictionaryEN['HeaderDetailsDiscountAmount'] = 'Header Discount';
DataDictionaryEN['IsReturned'] = 'Is Returned';
DataDictionaryEN["OEShipRPostCfrm"] = "Are you sure you want to post this shipment return ?";
DataDictionaryEN["PostShipRConfirm"] = "Post shipment return";

DataDictionaryEN['DownArrow'] = '▼';
DataDictionaryEN['UpArrow'] = '▲';
DataDictionaryEN["LineCountValid"] = "You cannot insert more than 100 line !!";
DataDictionaryEN["invldItemQty"] = "Item Quantity must be greater than zero in the following lines (";
DataDictionaryEN['MaxIs7'] = 'max value is 7';
DataDictionaryEN['PercentageSembol'] = '%';
DataDictionaryEN["Returned"] = 'Returned';
DataDictionaryEN["UnitPriceInfo"] = 'This price reflect the line price for \n the selected unit of measure';
DataDictionaryEN["Deliverdateinfo"] = "The  delivery date should be same as or more than the order date";
DataDictionaryEN["TransactionReference"] = "Transaction Reference";
DataDictionaryEN["InqueCustomer"] = "Customer No.";
DataDictionaryEN["InqueryItem"] = "Item No.";
DataDictionaryEN["PleaseSelectCritiera"] = "Please Select Search Criteria";
DataDictionaryEN["ImportFile"] = 'Import File';
DataDictionaryEN["Link"] = 'Link';
DataDictionaryEN['POStatusSaved'] = 'Saved';
DataDictionaryEN['ReciptStatusSaved'] = 'Saved';
DataDictionaryEN['RelatedPo'] = 'Related PO';
DataDictionaryEN['POReference'] = 'PO Reference';
DataDictionaryEN['ReciptStatus'] = 'Receipt Status';
DataDictionaryEN['ReturnReciptStatusSaved'] = 'Saved';

DataDictionaryEN["POInqueryTransactionType"] = "Transaction Type";
DataDictionaryEN["POInqueryTransactionCreatedDate"] = "Transaction Created Date";
DataDictionaryEN["POInqueryTransactionDate"] = "Transaction Date";

DataDictionaryEN["Error"] = 'Error';
DataDictionaryEN["OutQtyErrorMsg"] = 'QuantityReceived must be less than or equal Outstanding Qty :';
DataDictionaryEN["PendingPOQty"] = "Pending PO's";

DataDictionaryEN["PendingPOQtyInfo"] = "This quantity represents the remaining amount \n used in this and other Purchase Orders for the same \n item from the same location.";
DataDictionaryEN["PendingPOOrderQtyInfo"] = "This quantity represents the remaining amount \n used in Order entries for the same \n item from the same location.";
DataDictionaryEN["OriginalPOQtyOnHandInfo"] = "This quantity represents the quantity on hand \n for this item under this location when this \n purchase order is created.";
DataDictionaryEN["TransactionProcessing"] = "Transaction Processing";
DataDictionaryEN["lblFooter"] = "CCE © 2010-2016";
DataDictionaryEN["ModuleSetup"] = "Module Setup";
DataDictionaryEN["MasterDataSetup"] = "Master Data Setup";
DataDictionaryEN["ReportsAndInquiries"] = "Reports And Inquiries";
DataDictionaryEN["CentrixSetup"] = "Centrix Setup";
DataDictionaryEN["EntityLockController"] = "Entity Lock";
DataDictionaryEN["InvoiceType"] = "Invoice Type";
DataDictionaryEN['LoadReceiptsAdd'] = 'Load Details ...';
DataDictionaryEN["IsInvoiced"] = "Invoiced";
DataDictionaryEN["Worning"] = "Warning";
DataDictionaryEN["ReciptListIsEmpty"] = "Please load the receipt details !!";
DataDictionaryEN["AdditionalCostListIsEmpty"] = "Additional Cost List Is Empty !!";
DataDictionaryEN["ChooseAdditionalCostListToInvoiced"] = "Please select the additional cost who want to invoiced !!";
DataDictionaryEN["NoAdditionalCost"] = "There is no additional cost for this vendor !!";
DataDictionaryEN["Rcpt#"] = "Rcpt #";
DataDictionaryEN["Ship#"] = "Ship #";
DataDictionaryEN["HReceiptDate"] = "Receipt Date : ";
DataDictionaryEN["HStatus"] = "Status : ";
DataDictionaryEN["PostingPOInv"] = "Save and posting PO Invoice";
DataDictionaryEN["AreYouSureToPostPOInv"] = "Are you sure you want to save and post Invoice?";
DataDictionaryEN["PostingPORcpt"] = "Save and posting PO Receipt";
DataDictionaryEN["AreYouSureToPostPORcpt"] = "Are you sure you want to save and post Receipt?";

DataDictionaryEN["InvoicedReceipt"] = "one of the following Receipt already invoiced before : <br/>";
DataDictionaryEN["PoRReference"] = "Reference";
DataDictionaryEN["ReceiptReturnExist"] = "The following receipt already returned before : <br/>";
DataDictionaryEN["PostInvalidOutQty"] = "Please Refresh this page !! <br/> There is no outstanding Quantity in the following lines: <br/>";
DataDictionaryEN['POStatusInfo'] = 'The status will be automatically updated as the following : Saved, Ready to Receive, Partially Received and Fully Received according to the user actions.';
DataDictionaryEN['PORcptStatusInfo'] = 'The status will be automatically updated as the following : Saved, Posted, Invoiced according to the user actions.';
DataDictionaryEN['OeShipStatusInfo'] = 'The status will be automatically updated as the following : Saved, Posted, Invoiced according to the user actions.';

DataDictionaryEN["ReferenceHeaderTitle"] = "Reference : ";
DataDictionaryEN["DateHeaderTitle"] = "Date : ";
DataDictionaryEN["TotalCostHeaderTitle"] = "Total Cost : ";
DataDictionaryEN["TotalPriceHeaderTitle"] = "Total Price : ";

DataDictionaryEN["SideListReference"] = "Reference";
DataDictionaryEN["SideListDate"] = "Date";
DataDictionaryEN["SideListStatus"] = "Status";
DataDictionaryEN["SideListTotalCost"] = "Total Cost";
DataDictionaryEN["SideListTotalPrice"] = "Total Price";
DataDictionaryEN["APBatchList"] = "AP Payment Batch List";
DataDictionaryEN["AccpacSync"] = "Sage Accpac Synchronization ";
DataDictionaryEN["Synchronization"] = "Synchronization";
DataDictionaryEN["UpdateRegisters"] = "Update Registers";
DataDictionaryEN["UpdateAccpac"] = "Update Accpac";
DataDictionaryEN["updateRegisterConfirmationMessage"] = "This will update rgisters with the updated data from Sage Accpac 300 <br/> Are you sure to continue ?!";
DataDictionaryEN["updateAccpacConfirmationMessage"] = "This will update Accpac with the updated transaction from Sage POS <br/> Are you sure to continue ?!";
DataDictionaryEN["SynchronizationData"] = "The Synchronization Process will be started in 30 seconds";
DataDictionaryEN["SyncedInProgress"] = "the synchornization process is in progress";
DataDictionaryEN["Refresh"] = "Refresh";
DataDictionaryEN["PostPettyCash"] = "Post Petty Cash";
DataDictionaryEN["SyncActivityLog"] = "Sync Activity Log";
DataDictionaryEN["TotalDailySalesSummation"] = "POS Daily Receipts";
DataDictionaryEN["DailySalesSummation"] = " POS Daily Sales";

