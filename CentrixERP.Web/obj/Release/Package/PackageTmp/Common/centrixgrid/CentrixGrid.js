
(function ($) {

    jQuery.fn.Grid = function (options) {
        var grid = this;
        this.table = []; //the html table that will hold the grid
        this.rows = []; //list of all table rows as object and related data.this array will be used for retrieve all data
        this.mode = 'view'; //to witch between edit and view modes
        this.emptyRowTemplate = '<tr class="tr-empty"><td></td></tr>';

        var defaults = $.extend({

            headerTeamplte: '<table></table>',
            rowTemplate: '<tr></tr>',
            dataSource: [],                     //data source
            defaultRowsCount: 5,                //default free inserted rows at the end of the table
            rowControls: [],                    //array of controls that describe each row cotrols
            //service: '',                        
            //serviceArgs: '',
            //dataSource: [],                   
            selector: '.',                      //html select that will be used to select control from template
            //viewedit: 've',                   //
            orderedGrid: true,                  //if the grid has order cells    
            rowAdded: null,                     //callback fired after each append row. used to ex. set AC multiselect items
            rowDeleted: null,                   //callback fired after each remove row. used to ex. set AC multiselect items
            appendBefore: 'tr:last',
            preventDelete: false,               //prevent delte row on press Delete Key
            preventAdd: false,                  //prevent add new row when press tab key on the last row
            addDefaultRow: true,                //prevent add default row
            autoInsertEmptyRow: true,            //prvent add new row after deleting all rows
            setNowDate: false, //Fill Datepicker by default
            headerNotFreezed: false,
            checkBeforInsertNewRow: null
        }, options);

        grid.init = function () {
            this.defaults = $.extend({}, defaults, options);
            this.Grid();
        }

        grid.setMode = function (mode) {
            grid.mode = mode;
        }

        grid.edit = function (mode) {
            grid.mode = 'edit';
            grid.table.find('.viewedit-display-none').show();
            grid.table.find('.viewedit-display-block').hide();
        }

        grid.view = function (mode) {
            grid.mode = 'view';
            grid.table.find('.viewedit-display-none').hide();
            grid.table.find('.viewedit-display-block').show();
        }

        grid.validate = function () {
            return saveForm(grid.table.find('tr.r-data'));
        }

        grid.insertRow = function (dataRow) {
            if (!grid.defaults.preventAdd) {
                var rowControls = $.extend(true, {}, grid.defaults.rowControls);
                var row = grid.initRow($(grid.defaults.rowTemplate), dataRow, rowControls, grid.defaults.selector);
                grid.addRow(row, rowControls, grid.defaults.selector, dataRow, grid.table.find('tr.r-data').length);
                grid.setOrder();
            }
        }

        grid.appendFirst = function (dataRow) {
            var rowControls = $.extend(true, {}, grid.defaults.rowControls);
            var row = grid.initRow($(grid.defaults.rowTemplate), dataRow, rowControls, grid.defaults.selector);
            grid.addRowFirst(row, rowControls, grid.defaults.selector, dataRow, grid.table.find('tr.r-data').length);
            grid.setOrder();
        }

        grid.appendDataSource = function (datasource) {
            $.each(datasource, function (index, item) {
                var rowControls = $.extend(true, {}, grid.defaults.rowControls);
                var row = grid.initRow($(grid.defaults.rowTemplate), this, rowControls, grid.defaults.selector);
                grid.addRow(row, rowControls, grid.defaults.selector, this, index);


                grid.setOrder();
            });
        }

        grid.deleteRow = function (index) {
          //  debugger;
            //validation not null
            var row = grid.rows[index];
            row.template.next().find('input:first').focus()
            row.template.remove();

            //if the deleted row is the last row in the grid,if yes add new row
            //this will sure that the grid will always has one row at least
            if (grid.table.find('tr.r-data').length == 0 && grid.defaults.autoInsertEmptyRow) {
                //grid.addRow(row, controls, selector, null, null);
                grid.insertRow({});
            }

            //remove the deleted row from the grid as well as related data
            grid.rows.splice(index, 1);
            //reorder the grid
            grid.setOrder();

            //call row added callback 
            if (typeof grid.defaults.rowDeleted == 'function') {
                grid.defaults.rowDeleted(grid, index);
            }
        }

        grid.destroy = function () {
            if (grid.table.length)
                grid.table.remove();
            this.table = [];
            this.rows = [];
            this.mode = 'view';
            this.defaults.dataSource = null;
            this.Grid();
        }


        grid.Grid = function () {
            var self = this,
            rowTemplate = grid.defaults.rowTemplate,
            controls = grid.defaults.rowControls;

            grid.table = $(grid.defaults.headerTeamplte)
            $(self).append(grid.table);


            if (!grid.defaults.dataSource || grid.defaults.dataSource.length <= 0) {
                var rowControls = $.extend(true, {}, controls);
                var row = grid.initRow($(rowTemplate), null, rowControls, grid.defaults.selector);
                if (grid.defaults.addDefaultRow) {
                    grid.addRow(row, rowControls, grid.defaults.selector, null);
                }
            }

            if (grid.defaults.dataSource && grid.defaults.dataSource.length > 0) {

                $.each(grid.defaults.dataSource, function (index, dataRow) {
                    //clone controls array into new array
                    var rowControls = $.extend(true, {}, controls);
                    var row = grid.initRow($(rowTemplate), dataRow, rowControls, grid.defaults.selector);
                    grid.setRowActions(row, controls, grid.defaults.selector, index);
                    row.addClass('r-data');

                    if (index % 2 == 0) {
                        row.find('td').addClass('td-r1-data-table');
                    }
                    else row.find('td').addClass('td-r2-data-table');
                    if (grid.mode == 'edit' || grid.defaults.viewedit == 'e') {
                        row.find('.viewedit-display-none').show();
                        row.find('.viewedit-display-block').hide();
                    }
                    else {
                        row.find('.viewedit-display-none').hide();
                        row.find('.viewedit-display-block').show();
                    }

                    //call row added callback 
                    var extraInfo = {};
                    if (typeof grid.defaults.rowAdded == 'function') {
                        grid.defaults.rowAdded(row, rowControls, dataRow, grid, extraInfo, index + 1);
                    }
                    $(grid.table).find(grid.defaults.appendBefore).before(row);

                    //push the new row to the rows array
                    grid.rows.push({
                        template: row,
                        rowData: dataRow,
                        rowControls: rowControls,
                        extraInfo: extraInfo
                    });
                });

                grid.setOrder();
            }

            //append the empty rows at the end of the table
            //            var emptyTemplate = $(grid.emptyRowTemplate);
            //            emptyTemplate.find('td').attr('colspan', '2')
            //            for (var i = 0; i < grid.defaults.defaultRowsCount; i++) {
            //                grid.table.append(emptyTemplate.clone());
            //            }
        }

        grid.addRow = function (currentRow, controls, selector, rowData, index) {

            //current index =-1 if theres no rows in the grid
            //currentRow.index() the row that trigger blur event
            //check if its the last row in the grid if yes then add new row

            var rowIndex = currentRow.index();
            if (rowIndex == -1 || (((currentRow.index() == grid.table.find('tr.r-data').length - 1) && !grid.defaults.headerNotFreezed) || ((currentRow.index() == grid.table.find('tr.r-data').length) && grid.defaults.headerNotFreezed))) {

                var rowTemplate = $(grid.defaults.rowTemplate).clone();
                var rowControls = $.extend(true, {}, controls);
                var row = grid.initRow(rowTemplate, rowData, rowControls, selector, rowIndex + 1);
                row.addClass('r-data');
                if (index || index == 0) {
                    if (index % 2 == 0) {
                        row.find('td').addClass('td-r1-data-table');
                    }
                    else row.find('td').addClass('td-r2-data-table');
                }
                else {
                    if (rowIndex % 2 == 0) {
                        row.find('td').addClass('td-r1-data-table');
                    }
                    else row.find('td').addClass('td-r2-data-table');
                }

                grid.setRowActions(row, rowControls, grid.defaults.selector, (rowIndex < 0) ? 0 : rowIndex);



                if (grid.mode == 'edit' || grid.defaults.viewedit == 'e') {
                    row.find('.viewedit-display-none').show();
                    row.find('.viewedit-display-block').hide();
                }
                else {
                    row.find('.viewedit-display-none').hide();
                    row.find('.viewedit-display-block').show();
                }

                //call row added callback 
                var extraInfo = {};
                if (typeof grid.defaults.rowAdded == 'function') {
                    grid.defaults.rowAdded(row, rowControls, rowData, grid, extraInfo, rowIndex + 1);
                }

                //append after row in the grid if there is at leaset one row,otherwise append after the header
                if ($(grid.table).find('.r-data:last').length) {
                    //$(grid.table).find('.r-data:last').after(row);
                    $(grid.table).find(grid.defaults.appendBefore).before(row);
                }
                else {
                    //$(grid.table).find('tr:first').after(row);
                    $(grid.table).find(grid.defaults.appendBefore).before(row);
                }

                //push the new row to the rows array
                grid.rows.push({
                    template: row,
                    rowData: rowData,
                    rowControls: rowControls,
                    extraInfo: extraInfo
                });
                //Updated 6/11/2014 to enable focus if first elemnt is AC
                //row.find('input:first').focus();
                row.find(':first').focus();
                row.find('.line-number').text(rowIndex == -1 ? 1 : (currentRow.index() + 1));
                //grid.setOrder();

            }
        }

        grid.addRowFirst = function (currentRow, controls, selector, rowData, index) {
            //current index =-1 if theres no rows in the grid
            //currentRow.index() the row that trigger blur event
            //check if its the last row in the grid if yes then add new row

            var rowIndex = currentRow.index();
            if (rowIndex == -1 || (currentRow.index() == grid.table.find('tr.r-data').length)) {

                var rowTemplate = $(grid.defaults.rowTemplate);
                var rowControls = $.extend(true, {}, controls);
                var row = grid.initRow(rowTemplate, rowData, rowControls, selector);
                row.addClass('r-data');
                row.addClass('new-row');
                if (index || index == 0) {
                    if (index % 2 == 0) {
                        row.find('td').addClass('td-r1-data-table');
                    }
                    else row.find('td').addClass('td-r2-data-table');
                }
                grid.setRowActions(row, rowControls, grid.defaults.selector, (rowIndex < 0) ? 0 : rowIndex);



                if (grid.mode == 'edit' || grid.defaults.viewedit == 'e') {
                    row.find('.viewedit-display-none').show();
                    row.find('.viewedit-display-block').hide();
                }
                else {
                    row.find('.viewedit-display-none').hide();
                    row.find('.viewedit-display-block').show();
                }

                //call row added callback 
                var extraInfo = {};
                if (typeof grid.defaults.rowAdded == 'function') {
                    grid.defaults.rowAdded(row, rowControls, rowData, grid, extraInfo, rowIndex + 1);
                }

                if ($(grid.table).find('.r-data:last').length) {
                    $(grid.table).find('.r-data:first').before(row);
                }
                else { $(grid.table).find(grid.defaults.appendBefore).before(row); }

                //push the new row to the rows array
                grid.rows.push({
                    template: row,
                    rowData: {},
                    rowControls: rowControls,
                    extraInfo: extraInfo
                });
                row.find('input:first').focus();
                row.find('.line-number').text(rowIndex == -1 ? 1 : (currentRow.index() + 1));
                //grid.setOrder();

            }
        }
        //set row actions such as click,blur,etc.....
        grid.setRowActions = function (row, controls, selector, rowIndex) {

            //if (row.index() == this.table.find('tr').length - 1) {
            //            row.find('input[type="text"]:last').keydown(function (e) {
            //                if (e.which == 9 && !grid.defaults.preventAdd) {
            //                    grid.addRow(row, controls, selector, null, null);
            //                }
            //            });
            //Changed By Haneen
            row.find('input[type="text"],textarea').keydown(function (e) {
                if ($(row.find('input[type="text"]:enabled:last')).is(this)) {
                    if (grid.defaults.checkBeforInsertNewRow) grid.defaults.checkBeforInsertNewRow(row, controls, rowIndex, grid);
                    if (e.which == 9 && !grid.defaults.preventAdd) {
                        grid.addRow(row, controls, selector, null, null);
                    }
                }
            });

            row.click(function () {
                grid.table.find('tr.r-data').removeClass('td-data-hover');
                row.addClass('td-data-hover');
            });

            row.keyup(function (e) {
                if (e.which == 46 && !grid.defaults.preventDelete) {
              //  debugger
                    var index = row.index();
                    var rowToDelete = grid.get()[index];
                    row.next().find('input:first').focus()
                    row.remove();

                    //if there deleted row is the last row in the grid,if yes add new row
                    //this will sure that the grid will always has one row at least
                    if (grid.table.find('tr.r-data').length == 0 && grid.defaults.autoInsertEmptyRow) {
                        grid.addRow(row, controls, selector, null, null);
                    }

                    //remove the deleted row from the grid as well as related data
                    grid.rows.splice(index, 1);
                    //reorder the grid
                    grid.setOrder();

                    //call row added callback 
                    if (typeof grid.defaults.rowDeleted == 'function') {

                        grid.defaults.rowDeleted(grid, index, rowToDelete);
                    }

                }
            });
        };

        //order the table row number cell
        grid.setOrder = function () {
            if (grid.defaults.orderedGrid) {
                $.each(grid.table.find('tr.r-data'), function (index, item) {
                    $(item).find('.line-number').text(index + 1);
                });
            }
        }

        grid.getControls = function () {
            return grid.rows;
        };

        //get the grid objects array as array of item {template:the row its self,list of all availabel properties according to default controls}
        grid.get = function () {
            var result = [];
            $.each(grid.rows, function (index, row) {
                // //debugger;
                var item = {};
                item.template = row.template;
                item.dataRow = row.rowData;
                item.controls = row.rowControls;
                item.extraInfo = row.extraInfo;

                $.each(row.rowControls, function (ix, control) {

                    if (control.type == 'ac') {
                        if (control.multiSelect) {
                            item[control.controlId + 'Ids'] = control.control.get_ItemsIds();
                            item[control.controlId + 'Items'] = control.control.get_Items();
                        }
                        else {
                            item[control.controlId + 'Id'] = control.control.get_ItemValue();
                            item[control.controlId + 'Item'] = control.control.get_Item();
                        }
                        //eval('item.' + control.controlId + ' = 7');
                    }
                    else if (control.type == 'text') {
                        //added by mieassar 
                        //percentage class for spacific controls (max 99.9)
                        //numeric class for any numeric value
                        //integer class for any integer value

                        if (control.numeric || control.percentage)
                            item[control.controlId] = control.control.autoNumericGet();
                        else if (control.integer)
                            item[control.controlId] = parseInt(control.control.val().replace(/,/g, ''));
                        else
                            item[control.controlId] = control.control.val();
                    }
                    else if (control.type == 'date') {
                        item[control.controlId] = control.control.val();
                    }
                    else if (control.type == 'check') {
                        item[control.controlId] = control.control.prop('checked');
                    }
                });
                result.push(item);


            });
            return result;
            //console.log(result);
        };


        //set row controls types and events
        grid.initRow = function (row, rowData, controls, selector, rowIndex) {
            row = row.clone();
            var acs = [];
            $.each(controls, function (index, control) {
                switch (control.type) {
                    case 'ac':
                        {
                            var ac;
                            if (control.dataType) {
                                ac = fillDataTypeContentList(row.find('select' + selector + control.controlId)
                                , control.dataTypeId, control.required, control.multiSelect, control.hasDefaultItem);
                                //init data type content
                                ac.onChange(function (Item) {
                                    if (typeof control.onChange == 'function') {
                                        control.onChange(ac.selectedItem, row, controls, rowIndex, rowData);
                                    }
                                });
                            }
                            else {
                                ac = row.find('select' + selector + control.controlId).AutoComplete({
                                    serviceURL: control.service,
                                    required: control.required,
                                    multiSelect: control.multiSelect,
                                    staticParams: true,    //changed by mieassar
                                    setFirstItemAsDefault: control.setFirstItemAsDefault
                                    //true: if there is prameters for the AC method
                                    //false: if there is no prameters for the AC method
                                });
                                ac.set_Args(control.serviceArgs);
                                ac.onChange(function (Item) {
                                    if (typeof control.onChange == 'function') {
                                        //control.onChange(ac.selectedItem, row);
                                        control.onChange(ac.selectedItem, row, controls, rowIndex);
                                    }
                                });
                            }

                            acs.push({
                                controlName: control.controlId.replace(selector, '') + 'AC',
                                control: ac,
                                multiSelect: control.multiSelect
                            });
                            control.control = ac;
                            break;
                        }
                        break;
                    case 'text':
                        {
                            control.control = row.find('input' + selector + control.controlId);

                            //added by mieassar
                            // percentage for percentage inputs
                            // integer for integer inputs
                            // numeric for numeric inputs
                            if (control.percentage) {
                                control.control.autoNumeric({ vMax: '99.99' });
                            }

                            else if (control.integer) {
                                control.control.autoNumeric({ aPad: false });
                                control.control.blur(function () { control.control.autoNumericSet(parseInt(control.control.autoNumericGet())); });
                            }

                            else if (control.numeric) {
                                control.control.autoNumeric();
                            }

                            //call back function when leave the control
                            if (typeof control.blur == 'function') {
                                control.control.blur(function () {
                                    control.blur($(this), row, grid, controls, rowData, rowIndex);
                                })
                            }

                            break;
                        }
                    case 'date':
                        {
                            control.control = row.find('input' + selector + control.controlId);
                            LoadDatePicker(control.control);
                            if (grid.defaults.setNowDate)
                                setNowDate(control.control);
                            break;
                        }
                    case 'check':
                        {
                            control.control = row.find('input' + selector + control.controlId);

                            if (typeof control.click == 'function') {
                                control.control.click(function () {
                                    control.click($(this), row, grid, controls, rowData, rowIndex);
                                });
                            }
                            break;
                        }
                }

            });

            if (rowData)
                row = mapEntityInfoToControls(rowData, row, acs);
            return row;
        }

        grid.init();
        return grid;
    };
})(jQuery);




//load items grid
function InnerPage_Load3() {

    //in order to work with the grid you should describe the controls that the row template will have
    var data = [{
        ModelNumberId: 3,
        ModelNumber: '123',
        Date: '/Date(1371472695673)/',
        Price: 500.3,
        Active: 'Inactive',
        ActiveValue: false,
        Items: [{ label: 'it1', value: 1 }, { label: 'it2', value: 2}]
    },
            {
                ModelNumberId: 3,
                ModelNumber: '123',
                Date: '/Date(1371472695673)/',
                Price: 500.3,
                Comment: 'asdasdad',
                Active: 'Active',
                ActiveValue: true
            },
        {
            ModelNumberId: 3,
            ModelNumber: '123',
            Date: '/Date(1371472695673)/',
            Price: 500.3
        }
        ];


    var control = [
    {
        controlId: 'ModelNumber',
        type: 'ac',
        required: true,
        service: systemConfig.ApplicationURL + systemConfig.dataTypeContentServiceURL + "FindAll",
        serviceArgs: '{parentId:3}',
        onChange: function (item, template) {
            if (item)
                template.find('.Price2').text(item.label);
            else
                template.find('.Price2').text('');
        }
    },


    {
        controlId: 'Items',
        type: 'ac',
        required: true,
        multiSelect: true,
        dataTypeId: 5,
        dataType: true,
        //service: systemConfig.ApplicationURL + systemConfig.dataTypeContentServiceURL + "FindAll",
        //serviceArgs: '{parentId:3}',
        onChange: function (item, template) {
            //becuase its autocomplete,this the onchnage function
            //item is the selceted item
            //template is the currenct row of the grid
            //            if (item)
            //                template.find('.Price2').text(item.label);
            //            else
            //                template.find('.Price2').text('');
        }
    }, {
        controlId: 'Price',
        type: 'text',
        required: true,
        blur: null,
        numeric: true
    },
        {
            controlId: 'Date',
            type: 'date',
            required: true

        }
        ,
        {
            controlId: 'Comment',
            type: 'text',
            required: true
        }
,
        {
            controlId: 'Active',
            type: 'check'
        }
];


    //after create array of controls, we need init the grid
    grid = $('#grid').Grid({
        headerTeamplte: $('#h-template').html(), //header template must has the table tag
        rowTemplate: $('#r-template1').text(),
        rowControls: control,
        dataSource: data, //this the data source that comes from API,i will set it for null just fro testing
        selector: '.',
        rowAdded: function (row, controls, dataRow, grid) {

            if (!dataRow)
                return;
            $.each(controls, function (ix, ctr) {
                if (ctr.controlId == 'Items') {
                    ctr.control.set_Items(dataRow.Items);
                }
            });
        },
        rowDeleted: function (grid) {//this events fired after each row has been deleted from the grid,if you have some custom action 
            //do something     
        }
    });
}