﻿

function InnerPage_Load() {

    // setPageTitle(getResource("ERPIntegration"));
    $('.Top-Mid-section').remove();

    setSelectedMenuItem($('.pos-icon'));

    $('.btn-inv-post').click(function () {
        
        showConfirmMsg("Post Invoice", "Are You Sure ?", function () {

            StartAccpacPost('1,7');
        }, null);

    });

    $('.btn-refund-post').click(function () {
        showConfirmMsg("Post Refund", "Are You Sure ?", function () {
            StartAccpacPost(POSInvoiceType.Refund);
        }, null);

    });

    $('.btn-onaccount-post').click(function () {
        showConfirmMsg("Post On Account", "Are You Sure ?", function () {
            StartAccpacPost(POSInvoiceType.OnAccount);
        }, null);

    });

    $('.btn-exchange-post').click(function () {
        showConfirmMsg("Post Exchange", "Are You Sure ?", function () {

            StartAccpacPost(POSInvoiceType.Exchange);
        }, null);

    });
    $('.btn-PettyCash-post').click(function () {
        showConfirmMsg("Post Petty Cash", "Are You Sure ?", function () {

            StartAccpacPost("8")
        }, null);
    });



        





    $('.lnk-update-registers').click(function () {
        debugger

        NewcheckPermissions(this, systemEntities.UpdateRegisters, -1);

        if (havePer == 0) {
            showStatusMsg(getResource('vldDontHavePermission'));
            return;

        }
        if (!checkifAvailable()) {
            showOkayMsg(getResource("SystemAlert"), getResource('SyncedInProgress'))
            return;
        }
       

        showConfirmMsg(getResource("ConfirmMsgConfirm"), getResource("updateRegisterConfirmationMessage"), function () {
          
            CreateSyncRecord(1);
        });
    });

    $('.lnk-update-accpac').click(function () {
      debugger
      NewcheckPermissions(this, systemEntities.UpdateAccpac, -1);
        debugger
      if (havePer == 0) {
          showStatusMsg(getResource('vldDontHavePermission'));
          return;

      }
      if (!checkifAvailable()) {
            showOkayMsg(getResource("SystemAlert"), getResource('SyncedInProgress'))
            return;
        }




        showConfirmMsg(getResource("ConfirmMsgConfirm"), getResource("updateAccpacConfirmationMessage"), function () {
            CreateSyncRecord(2);
        });
    });

    setInterval(function () {
        checkthesynchronizationStatus();
    }, 60000
    );
  checkthesynchronizationStatus();
}





function CreateSyncRecord(type) {
   
 




    post(systemConfig.ApplicationURL + systemConfig.SyncScheduleServiceURL + "AddNewSchedule", formatString("{Type:'{0}',UserId:'{1}'}", type, LoggedInUser.Id), function (retuned) {
        if (type == 1) {
            disbaleButton($('.lnk-update-registers'), $('.lnk-update-registers-container'));
            showOkayMsg(getResource("SystemAlert"), getResource('SynchronizationData'));

        }
        else if (type == 2) {
            disbaleButton($('.lnk-update-accpac'), $('.lnk-update-accpac-container'));
            showOkayMsg(getResource("SystemAlert"), getResource('SynchronizationData'));

        }

    });

}


function disbaleButton(button, container) {
    $(container).addClass('BtnContainer-hover');
    $(button).addClass('Inactive');
    checkthesynchronizationStatus();
    
}


function checkifAvailable() {
    var sumbit = true;
    if ($('.lnk-update-registers').hasClass('Inactive') || $('.lnk-update-accpac').hasClass('Inactive'))
        sumbit = false;
    return sumbit;
}

function checkthesynchronizationStatus() {
    ajaxSetup();
    $.ajax({
        type: "POST",
        url: systemConfig.ApplicationURL + systemConfig.SyncScheduleServiceURL + "Checksyncstatus",
        contentType: "application/json; charset=utf-8",
        data: '{}',
        dataType: "json",
        success: function (data) {
            getStatusSuccess(data);
        },
        error: function (e) {
        }
    });
}


function getStatusSuccess(data) {
    var result = eval("(" + data.d + ")");

    if (result.result == null)
        return;
    var action = result.result.action;
    var startTime = result.result.startTime;
    if (action == 1) {
        $('.lnk-update-registers-container').addClass('BtnContainer-hover');
        $('.lnk-update-registers').addClass('Inactive');

        $('.lnk-update-registers').html(getResource("UpdateRegisters") + ' - ' + diff(startTime));
        $('#register-startSync').html(startTime);
    }
    else if (action == 2) {

        $('.lnk-update-accpac-container').addClass('BtnContainer-hover');
        $('.lnk-update-accpac').addClass('Inactive');
        $('.lnk-update-accpac').html(getResource("UpdateAccpac") + ' - ' + diff(startTime));
        $('#accpac-startSync').html(startTime);

    }

}

function GetDateTime() {
    var d2 = new Date('2038-01-19 03:14:07');
    var d1 = new Date('2038-01-19 03:10:07');

    var seconds = (d2 - d1) / 1000;
    return seconds
}

function diff(start) {
    var d = new Date(); // for now
    var hours = d.getHours(); // => 9
    var minutes = d.getMinutes(); // =>  30
    var seconds = d.getSeconds(); // => 51

    var end = hours + ":" + minutes;
    start = start.split(":");
    end = end.split(":");
    var startDate = new Date(0, 0, 0, start[0], start[1], 0);
    var endDate = new Date(0, 0, 0, end[0], end[1], 0);
    var diff = endDate.getTime() - startDate.getTime();
    var hours = Math.floor(diff / 1000 / 60 / 60);
    diff -= hours * 1000 * 60 * 60;
    var minutes = Math.floor(diff / 1000 / 60);

    // If using time pickers with 24 hours format, add the below line get exact hours
    if (hours < 0)
        hours = hours + 24;

    return (hours <= 9 ? "0" : "") + hours + ":" + (minutes <= 9 ? "0" : "") + minutes;
}
function StartAccpacPost(invType) {
    debugger;
    showLoading($('.Main-Div'));
    postAccpacTransaction(systemConfig.ApplicationURL + systemConfig.AccpacURL + "postInvoicetoAccpac", formatString("{invType:'{0}'}", invType), function (retuned) {
        hideLoading();
        if (retuned == "502") {
            debugger
            showOkayMsg(getResource("SystemAlert"), getResource("NoInvoiceToPost"));
            hideLoading();
            //   showStatusMsg(getResource("NoInvoiceToPost"));
            return;
        }

        if (retuned.statusCode.Code == 1 || retuned != "502") {
        
            var result = retuned.result;
            if (result != null) {

                if (StartAccpacPost)
                showOkayMsg(getResource("Completed"), retuned.d);
                var resultText = getResource("POSShipmentPostResult");
                var shipNumbers = '';

                resultText = resultText + shipNumbers;
                showOkayMsg(getResource("Completed"), resultText);
            }
        }


    }, null
    , null);
}



var POSInvoiceType = {
    Invoice: 1,
    Refund: 5,
    OnAccount: 7,
    Exchange: 10
};