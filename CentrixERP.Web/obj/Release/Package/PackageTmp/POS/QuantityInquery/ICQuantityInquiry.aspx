﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ICQuantityInquiry.aspx.cs"
    Inherits="SagePOS.Server.Web.POS.QuantityInquery.QuantityInquiry" MasterPageFile="~/Common/MasterPage/ERPSite.master" %>

<asp:Content ID="PageHeader" runat="server" ContentPlaceHolderID="PageHeader">
    <script src="JS/ICQuantityInquiry.js" type="text/javascript"></script>
    <script src="../../Common/DataDictionary/DataDictionary-ar.js" type="text/javascript"></script>
    <script src="../../Common/DataDictionary/DataDictionary-en.js" type="text/javascript"></script>
    <link href="../CSS/POSMain.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="PageBody" runat="server" ContentPlaceHolderID="PageBody">
    <div class="">
        <div class="Details-div">
            <span class="details-div-title" resourcekey="AdvFilterYourResults"></span>
            <div class="Action-div">
                <ul>
                </ul>
            </div>
        </div>
        <table cellspacing="0" cellpadding="0" border="0" class="Data-Table">
            <tr>
                <th>
                    <span class="label-title" resourcekey="Location"></span>
                </th>
                <td>
                    <select class="lst Location">
                    </select>
                </td>
                <th>
                    <span class="label-title" resourcekey="ItemNumber"></span>
                </th>
                <td>
                    <select class="lst ItemNumber">
                    </select>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" border="0" class="Data-Table">
            <tbody>
                <tr>
                    <td class="search-buttons" colspan="6">
                        <div class="filter-reset">
                            <input type="button" class="btnSearch button2" id="btnSearch" value="Search" />
                            <input value="Reset"  class="lnkReset lnkReset-InvoiceInquiry button-cancel button2"
                                type="button" />
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <span class="display-none NoDataFound NoDataFound-Inquiry" resourcekey="NoDataFound">
    </span>
</asp:Content>
