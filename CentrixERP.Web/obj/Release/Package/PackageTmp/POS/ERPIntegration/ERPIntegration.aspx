﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ERPIntegration.aspx.cs"
    Inherits="SagePOS.Server.Web.POS.ERPIntegration.ERPIntegration" MasterPageFile="~/Common/MasterPage/ERPSite.master" %>

<asp:Content ID="PageHeader" runat="server" ContentPlaceHolderID="PageHeader">
    <script src="JS/erpIntegration.js" type="text/javascript"></script>
    <script src="../../Common/DataDictionary/DataDictionary-ar.js" type="text/javascript"></script>
    <script src="../../Common/DataDictionary/DataDictionary-en.js" type="text/javascript"></script>
    <link href="../CSS/POSMain.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="PageBody" runat="server" ContentPlaceHolderID="PageBody">
    <div class="">
        <div class="Details-div">
            <span class="details-div-title" resourcekey="AdvFilterYourResults"></span>
            <div class="Action-div">
                <ul>
                </ul>
            </div>
        </div>
        <table cellspacing="0" cellpadding="0" border="0" class="Data-Table">
            <tbody>
                <tr>
                    <th style="text-align: left !important">
                        <span class="label-title" resourcekey="subtotal"></span>
                    </th>
                    <td>
                        <span class="label-title Subtotal">0.000</span> <span class="label-title currency">
                        </span>
                    </td>
                    <th style="text-align: left !important">
                        <span class="label-title" resourcekey="TotalCash"></span>
                    </th>
                    <td>
                        <span class="label-title totalCash">0.000</span> <span class="label-title currency">
                        </span>
                    </td>
                </tr>
                <tr>
                    <th style="text-align: left !important">
                        <span class="label-title" resourcekey="TotalItemDiscount"></span>
                    </th>
                    <td>
                        <span class="label-title itemDiscount">0.000</span> <span class="label-title currency">
                        </span>
                    </td>
                    <th style="text-align: left !important">
                        <span class="label-title" resourcekey="TotalOnAccount"></span>
                    </th>
                    <td>
                        <span class="label-title onAccount">0.000</span> <span class="label-title currency">
                        </span>
                    </td>
                </tr>
                <tr>
                    <th style="text-align: left !important">
                        <span class="label-title" resourcekey="TotalInvoiceDiscount"></span>
                    </th>
                    <td>
                        <span class="label-title invoicediscount">0.000</span> <span class="label-title currency">
                        </span>
                    </td>
                    <th style="text-align: left !important">
                        <span class="label-title" resourcekey="TotalCreditCard"></span>
                    </th>
                    <td>
                        <span class="label-title totalcreditcard">0.000</span> <span class="label-title currency">
                        </span>
                    </td>
                </tr>
                <tr>
                    <th style="text-align: left !important">
                        <span class="label-title " resourcekey="TotalTax"></span>
                    </th>
                    <td>
                        <span class="label-title totaltax">0.000</span> <span class="label-title currency">
                        </span>
                    </td>
                    <th style="text-align: left !important">
                        <span class="label-title" resourcekey="TotalCheques"></span>
                    </th>
                    <td style="text-align: left !important">
                        <span class="label-title totalcheque">0.000</span> <span class="label-title currency">
                        </span>
                    </td>
                </tr>
                <tr>
                </tr>
                <tr>
                    <th style="text-align: left !important">
                        <span class="label-title" style="color: green;" resourcekey="TotalSales"></span>
                    </th>
                    <td>
                        <span class="label-title TotalSales" style="color: green;">0.000</span>
                         <span class="label-title currency" style="color: green;"></span>
                    </td>
                    
                    <th style="text-align: left !important">
                        <span class="label-title" style="color: red;" resourcekey="TotalPayments"></span>
                    </th>
                    <td>
                        <span class="label-title totalPayment" style="color: red;">0.000</span>
                           <span class="label-title currency" style="color: red;"></span>
                    </td>
                   
                </tr>
                <tr>
                    <td class="search-buttons"  colspan="6">
                        <div class="filter-reset">
                            <input type="button" class="btnSearch button2" id="btnSearch" value="Refresh" />
                            <input value="Reset" style="display:none;" class="lnkReset lnkReset-InvoiceInquiry button-cancel button2"
                                type="button" />
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <span class="display-none NoDataFound NoDataFound-Inquiry" resourcekey="NoDataFound">
    </span>
</asp:Content>
