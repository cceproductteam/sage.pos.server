﻿var InvoiceReportSchema;
var ExportPageURL;
var FromLocationAC;
var ToLocationAC;
var FromCustomerAC;
var ToCustomerAC;
var POSInvoiceServiceURL;
var AccpacURLService = "";
var table;
var syncLogNumber = "";
var detailsTable;

function InnerPage_Load() {

    NewcheckPermissions(this, systemEntities.AccpagSyncLog, -1);
    AccpacURLService = systemConfig.ApplicationURL_Common + "Common/WebServices/AccpacPostTransactions.asmx/";
    syncLogNumber = getQSKey('sid');
    $('.synclogNumber').html(syncLogNumber);
    BuildSearchCriteria();
        
   
    if (havePer == 0) {

        $('.white-container-data').hide();
        showStatusMsg(getResource('vldDontHavePermission'));
        return;
    }
    initControls();
    setPageTitle(getResource("AccpacSyncLog"));
    InvoiceReportSchema = systemConfig.ReportSchema.TotalDailySales;
    setSelectedMenuItem('.pos-icon');

    $('#btn_close').click(function () {
        $('.popup-div').hide();
    });
}

function initControls() {

    LoadDatePicker('input:.date', 'mm-dd-yy');

    $('.lnkReset').click(resetForm);
    $('#btnSearch').click(BuildSearchCriteria);
    $('.Quick-Search-Controls').live('keydown', function (e) {
        if (e.keyCode == 13 || e.which == 13) {
            $('#btnSearch').click();
        }
    });


    //POSInvoiceServiceURL = systemConfig.ApplicationURL + "Common/WebServices/InvoiceWebService.asmx/";
   

    $('#btnSearch').attr('value', getResource("Search"));
    $('.lnkReset').attr('value', getResource("Reset"));
}

function BuildSearchCriteria() {
    var url = AccpacURLService + "GetAccpacSyncLogDetails";
    var data = formatString('{AccpacSyncUniqueNumber:"{0}"}', syncLogNumber);
    post(url, data, FindResultsSuccess, function () { }, null);
    return;
   
    var SearchCriteria = '{';
    for (var key in argsCriteria) {
        if (argsCriteria.hasOwnProperty(key))
            if (argsCriteria[key] != "") {
                SearchCriteria = SearchCriteria + ('"' + key + '":' + '"' + argsCriteria[key] + '",');
            }
    }


    if (SearchCriteria != '{') {
        SearchCriteria = SearchCriteria.slice(0, -1);
    }
    SearchCriteria = SearchCriteria + '}';
    var exportPageURL = ExportPageURL + formatString("schemaID={0}&SearchCriteria={1}", InvoiceReportSchema, SearchCriteria);
    window.open(exportPageURL, '_blank');
}

function resetForm() {
    $('.SyncLogDateTo').val('');
    $('.SyncLogDateFrom').val('');




}


function getdate(date) {
    var arr = date.split('-');
    return arr[1] + '-' + arr[0] + '-' + arr[2];
}


function FindResultsSuccess(returnedValue) {
 
    if (table)
        table.destroy();
    table = $('.result').DataTable({
        "bFilter": false,
        "data": returnedValue,
        "columns": [
           
         { "data": "SyncData" },
     
            { "data": "AddedCounts" },
            { "data": "Updatecounts" },
               { "data": "RemoveCounts" }


        ],
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {

            if (aData.AddedCounts > 0) {
                $('td:eq(1)', nRow).html('<a href="#"</a>' + aData.AddedCounts);
                $('td:eq(1)', nRow).on("click", function () { showDetailsWindow(aData.SyncData, 1) });
                $('td:eq(1)', nRow).css('text-align', 'left');

            }

            if (aData.Updatecounts > 0) {
                $('td:eq(2)', nRow).html('<a href="#"</a>' + aData.Updatecounts);
                $('td:eq(2)', nRow).on("click", function () { showDetailsWindow(aData.SyncData, 2) });
             $('td:eq(2)', nRow).css('text-align', 'left');
             
               
            }
            if (aData.RemoveCounts > 0) {
                $('td:eq(3)', nRow).html('<a href="#"</a>' + aData.RemoveCounts);
                $('td:eq(3)', nRow).on("click", function () { showDetailsWindow(aData.SyncData, 3) });
                $('td:eq(3)', nRow).css('text-align', 'left');
             

            }
            return nRow;

        }
       
    });
    $('.table').find('tr').find('td').css('text-align', 'left');
    $('.footer').hide();

}


function showDetailsWindow(syncEntity, action) {
    var url = AccpacURLService + "getSyncScheduleentityDetails";
    var data = formatString('{entity:"{0}",action:{1},logNumber:"{2}"}',syncEntity,action ,syncLogNumber);
    post(url, data, FindDetailsSuccess, function () { }, null);
    $('.details-div').show();
}

function FindDetailsSuccess(data)
{
 
    if (detailsTable)
        detailsTable.destroy();
   

    detailsTable = $('.details-result').DataTable({

        "data": data,
        "columns": [

        

            { "data": "SyncDataObject" }
           


        ],
        dom: 'Bfrtip',
        buttons: [

            {
                extend: 'collection',
                text: 'Export',
                buttons: [
                    'copy',
                    'excel',
                    'csv',
                    'pdf',
                    'print'
                ]
            }
        ],
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $('td:eq(0)', nRow).css('text-align', 'left');
        }

         
       
    });




    $('.details-result').find('tr').find('td').css('text-align', 'left');
    $('.footer').hide();

    $('.dt-buttons').find('a').removeClass('buttons-collection dt-button');
    $('.dt-buttons').find('a').addClass(' button2 custom-buttons');
    $('#DataTables_Table_1_wrapper').find('div').removeClass("dt-buttons");
    

    $('.custom-buttons').bind('click', function () {
        $('.dt-button-collection').find('a').removeClass("buttons-html5 dt-button").addClass('button3');
    });
}




