var PosSystemConfigrationServiceURL;
var LoginMethodAC, AfterCloseRegisterValueAC, CashCustomerAC, GiftVoucherOverAmountAC, TaxableClassAC, NonTaxableClassAC, TaxableClassNameAC, NonTaxableClassNameAC, ExchangeAccountAC;
var DiscountAccountAC;
var ResultCount = 10;
var PosSystemConfigrationId, selectedEntityId = -1;
var lastUpdatedDate = null;
var isAdd = true;
var AddingItemStructureAC;
var TaxAuthorityAC;

function InnerPage_Load() {

    setSelectedMenuItem($(".pos-icon"));
    PosSystemConfigrationServiceURL = systemConfig.ApplicationURL_Common + systemConfig.SystemConfigrationServiceURL;
    currentEntity = systemEntities.SystemConfigrationEntityId;
    setPageTitle(getResource("POSConfiguration"));
    checkPermissions(this, systemEntities.SystemConfigrationEntityId, -1, $('.PosSystemConfigration-info'), function (returnedData) {
        template = returnedData;
        initControls(template, isAdd);       
        LoadSystemConfigrationData();
    });

    updatedEntityCallBack = function () {
        LoadSystemConfigrationData();
    };
}



function initControls(template, isAdd) {
    $('.historyUpdated').append(HistoryUpdatedListViewTemplate.clone());
    preventInputChars(template.find('input:.integer'));
    LoginMethodAC = fillDataTypeContentList(template.find('select:.LoginMethod'), systemConfig.dataTypes.LoginMethod, false, false);
    AfterCloseRegisterValueAC = fillDataTypeContentList(template.find('select:.AfterCloseRegisterValue'), systemConfig.dataTypes.AfterCloseRegisterValue, true, false);
    //CashCustomerAC = template.find('select:.CashCustomer').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.IntegrationWebServiceURL + 'FindAllLite', required: true, multiSelect: false });
    TaxableClassNameAC = template.find('select:.TaxableClassName').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.POSCUSTURL + 'FindAllLite', required: false, multiSelect: false });
    NonTaxableClassNameAC = template.find('select:.NonTaxableClassName').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.POSCUSTURL + 'FindAllLite', required: false, multiSelect: false });
    TaxAuthorityAC = fillACList('select:.TaxAuthority', systemConfig.ApplicationURL + systemConfig.POSCUSTURL + "FindAllLite", false, true);
    AddingItemStructureAC = fillDataTypeContentList(template.find('select:.AddingItemStructure'), systemConfig.dataTypes.ItemStructurer, false, false);

    DiscountAccountAC = template.find('select:.DiscountAccount').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.POSCUSTURL + 'FindAllLite', required: false, multiSelect: false });
    ExchangeAccountAC = template.find('select:.ExchangeAccount').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.POSCUSTURL + 'FindAllLite', required: false, multiSelect: false });
    CashCustomerAC = template.find('select:.CashCustomer').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.POSCUSTURL + 'FindAllLite', required: true, multiSelect: false });
    GiftVoucherOverAmountAC = fillDataTypeContentList(template.find('select:.GiftVoucherOverAmount'), systemConfig.dataTypes.GiftVoucherOverAmount, false, false);
    FillListSCFormat();
    FillListRCFormat();

    TaxableClassNameAC.set_Args(formatString("{TransactionTypeId:{0}, ClassType:{1}}", systemConfig.dataTypes.POSCUSTURL, systemConfig.WebKeys.CustomerClassType));
    NonTaxableClassNameAC.set_Args(formatString("{TransactionTypeId:{0}, ClassType:{1}}", systemConfig.dataTypes.POSCUSTURL, systemConfig.WebKeys.CustomerClassType));
 
 
    $("#INFlstSC,#INFlstRC").change(function () { FormateINFExample(); });
    $("#LNFlstSC,#LNFlstRC").change(function () { FormateLNFExample(); });

    $('input.INFormat').blur(function () {
        FormateINFExample();
    });
    $('input.LNFormat').blur(function () {
        FormateLNFExample();
    });


    TaxAuthorityAC.onChange(function (item) {
        if (item) {
            TaxableClassNameAC.clear();
            NonTaxableClassNameAC.clear();
            TaxableClassNameAC.set_Args(formatString("{TransactionTypeId:{0}, ClassType:{1},TaxAuthorityId:{2}}", systemConfig.dataTypes.SalesTaxClass, systemConfig.WebKeys.CustomerClassType,TaxAuthorityAC.get_ItemValue()));
            NonTaxableClassNameAC.set_Args(formatString("{TransactionTypeId:{0}, ClassType:{1},TaxAuthorityId:{2}}", systemConfig.dataTypes.SalesTaxClass, systemConfig.WebKeys.CustomerClassType, TaxAuthorityAC.get_ItemValue()));

        }

    });

}

function FillListSCFormat() {
    $("#INFlstSC").append(new Option("", "0"));
    $("#INFlstSC").append(new Option("SC", "1"));

    $("#LNFlstSC").append(new Option("", "0"));
    $("#LNFlstSC").append(new Option("SC", "1"));
}

function FillListRCFormat() {
    $("#INFlstRC").append(new Option("", "0"));
    $("#INFlstRC").append(new Option("RC", "1"));

    $("#LNFlstRC").append(new Option("", "0"));
    $("#LNFlstRC").append(new Option("RC", "1"));
}

function FormateINFExample() {
    var INFString = "";
    
  var INFPrefix = ($('input.INFPrefix').val() != '' && $('input.INFPrefix').val() != null)? $('input.INFPrefix').val() + '^' : 'blank^';
  var INFValD1 = ($('input.INFValD1').val() != '' && $('input.INFValD1').val() != null) ? $('input.INFValD1').val() + '^' : 'blank^';
  var INFlstSC = ($("#INFlstSC :selected").text() != '' && $("#INFlstSC :selected").text() != null) ? $("#INFlstSC :selected").text() + '^' : 'blank^';
  var INFValD2 = ($('input.INFValD2').val() != '' && $('input.INFValD2').val() != null) ? $('input.INFValD2').val() + '^' : 'blank^';
  var INFlstRC = ($("#INFlstRC :selected").text() != '' && $("#INFlstRC :selected").text() != null) ? $("#INFlstRC :selected").text() + '^' : 'blank^';
  var INFValD3 = ($('input.INFValD3').val() != '' && $('input.INFValD3').val() != null) ? $('input.INFValD3').val() + '^' : 'blank^';


   INFString = INFPrefix + INFValD1 + INFlstSC + INFValD2 + INFlstRC + INFValD3;
    var len = "";
    for (i = 0; i < $('input.INFLength').val(); i++) {
        len = len + '0';
    }

    $('input.InvoiceNumberFormat').val(INFString + len);
    INFString = (INFString + len).replace(/\^/g, '');
    INFString = INFString.replace(/blank/g, '');
    $('span.InvoiceNumberFormat').text(INFString);
   
}

function FormateLNFExample() {
    var LNFString = "";

    var LNFPrefix = ($('input.LNFPrefix').val() != '' && $('input.LNFPrefix').val() != null) ? $('input.LNFPrefix').val() + '^' : 'blank^';
    var LNFValD1 = ($('input.LNFValD1').val() != '' && $('input.LNFValD1').val() != null) ? $('input.LNFValD1').val() + '^' : 'blank^';
    var LNFlstSC = ($("#LNFlstSC :selected").text() != '' && $("#LNFlstSC :selected").text() != null) ? $("#LNFlstSC :selected").text() + '^' : 'blank^';
    var LNFValD2 = ($('input.LNFValD2').val() != '' && $('input.LNFValD2').val() != null) ? $('input.LNFValD2').val() + '^' : 'blank^';
    var LNFlstRC = ($("#LNFlstRC :selected").text() != '' && $("#LNFlstRC :selected").text() != null) ? $("#LNFlstRC :selected").text() + '^' : 'blank^';
    var LNFValD3 = ($('input.LNFValD3').val() != '' && $('input.LNFValD3').val() != null) ? $('input.LNFValD3').val() + '^' : 'blank^';


    LNFString = LNFPrefix + LNFValD1 + LNFlstSC + LNFValD2 + LNFlstRC + LNFValD3;

    var len = "";
    for (i = 0; i < $('input.LNFLength').val(); i++) {
        len = len + '0';
    }

    $('input.LaybyNumberFormat').val(LNFString + len);
    LNFString = (LNFString + len).replace(/\^/g, '');
    LNFString = LNFString.replace(/blank/g, '');
    $('span.LaybyNumberFormat').text(LNFString);
 
}


function LoadSystemConfigrationData() {
    $('.viewedit-display-block').show();
    $('.viewedit-display-none').hide();
    $('.save-cancel-pnl,.InvoiceNumberFormatRequired , .InvoiceNumberFormatRequired').hide();


    showLoading($('.white-container-data'));
    $.ajax({
        type: "POST",
        url: systemConfig.ApplicationURL_Common + systemConfig.SystemConfigrationServiceURL + "FindAllLite",
        contentType: "application/json; charset=utf-8",
        data: formatString('{ keyword:"", page:1, resultCount:10}'),
        dataType: "json",
        success: function (data) {
            hideLoading();
            var ConfigObj = eval("(" + data.d + ")").result;
            if (ConfigObj != null) {
                LoadData(ConfigObj);
                $('.LaybyMinPercentage').text(ConfigObj.LaybyMinPercentage + '%');
                isAdd = false;
            }
            else {
                selectedEntityId = -1;
                PosSystemConfigrationId = -1;
                currentEntityLastUpdatedDate = "";
                isAdd = true;
                LoginMethodAC.set_Item({ label: getResource("UsernameandPassword"), value: systemConfig.dataTypes.UsernamePassword });
            }


            $('.ActionEdit-link').live("click", function () {
                showLoading($('.PosSystemConfigration-info'));
                if (selectedEntityId > 0) {
                    lockEntity(currentEntity, selectedEntityId, function () {
                        $('.viewedit-display-block').hide();
                        $('.viewedit-display-none').show();
                        $('.TabbedPanelsContent').find('.cx-control:visible').first().focus();
                    }, currentEntityLastUpdatedDate, function () { LoadSystemConfigrationData(); }, function () { LoadSystemConfigrationData(); });
                }
                else {
                    $('.viewedit-display-block').hide();
                    $('.viewedit-display-none').show();
                    $('.TabbedPanelsContent').find('.cx-control:visible').first().focus();
                }
                hideLoading();
            });
            $('.ActionSave-link').unbind().click(function () { SaveEntity(isAdd, template); return false; });

            $(template).find('.ActionCancel-link').unbind().click(function () {
                $('.validation').remove();
                LoadSystemConfigrationData();
                return false;
            });

        },
        error: function (e) {
            hideLoading();
        }
    });

}

function SaveEntity(isAdd, template) {
debugger
    var controlTemplate;
    var valid = true;

    controlTemplate = $('.PosSystemConfigration-info');
    showLoading('.PosSystemConfigration-info');

    if ($('input.InvoiceNumberFormat').val() == '' || $('input.InvoiceNumberFormat').val() == null) {
        valid = false;
        $('.InvoiceNumberFormatRequired').show();
    }





    if (!saveForm(controlTemplate)) {
        hideLoading();
        valid = false;
    }

    
//     if ($('input.LaybyMinPercentage').val() > 100) {
//        valid = false;
//        addPopupMessage($('input.LaybyMinPercentage'), getResource("InvalidPercentage"));
//    }
    
   
    if (!valid) {
        hideLoading();
        return false;
    }

    PosSystemConfigrationId = -1;
    if (!isAdd) {
        PosSystemConfigrationId = selectedEntityId;
        lastUpdatedDate = getEntityLockDateFormat(currentEntityLastUpdatedDate);
    }
    debugger
    var PosSystemConfigrationObj = {
        GiftVoucherOverAmount: GiftVoucherOverAmountAC.get_ItemValue(),
        LaybyMinPercentage: controlTemplate.find('input.LaybyMinPercentage').autoNumericGet(),
        LoginMethodId: LoginMethodAC.get_ItemValue(),
        AfterCloseRegisterValueId: AfterCloseRegisterValueAC.get_ItemValue(),
        InvoiceNumberFormat: controlTemplate.find('input.InvoiceNumberFormat').val(),
        LaybyNumberFormat: controlTemplate.find('input.LaybyNumberFormat').val(),
        CashCustomerId: CashCustomerAC.selectedItem.CustomerNumber, //CashCustomerAC.get_ItemValue(),
        AccpacStagingIp: controlTemplate.find('input.AccpacStagingIp').val(),
        PosServerIp: controlTemplate.find('input.PosServerIp').val(),
        AccpacDataBaseUsername: controlTemplate.find('input.AccpacDataBaseUsername').val(),
        AccpacDataBasePassword: controlTemplate.find('input.AccpacDataBasePassword').val(),
        PosServerDataBaseUsername: controlTemplate.find('input.PosServerDataBaseUsername').val(),
        PosServerDataBasePassword: controlTemplate.find('input.PosServerDataBasePassword').val(),
        PosServerDataBaseName: controlTemplate.find('input.PosServerDataBaseName').val(),
        AccpacDataBaseName: controlTemplate.find('input.AccpacDataBaseName').val(),
        AddingItemStructureId: AddingItemStructureAC.get_ItemValue(),
        TaxableClass: TaxableClassNameAC.get_ItemValue(),
        NonTaxableClass: NonTaxableClassNameAC.get_ItemValue(),
        DiscountAccountId: DiscountAccountAC.get_ItemValue(),
        ExchangeAccountId: ExchangeAccountAC.get_ItemValue(),
        AccpacDataBaseInstance: controlTemplate.find('input.AccpacDataBaseInstance').val(),
        AccpacCompanyName: controlTemplate.find('input.AccpacCompanyName').val(),
        AccpacUserName: controlTemplate.find('input.AccpacUserName').val(),
        AccpacUserPassword: controlTemplate.find('input.AccpacUserPassword').val(),
        AccpacServerIp : controlTemplate.find('input.AccpacServerIp').val(),
        TaxAuthorityId: TaxAuthorityAC.get_ItemValue()
    };

    var data = formatString('{id:{0}, PosSystemConfigrationInfo:"{1}"}', selectedEntityId, escape(JSON.stringify(PosSystemConfigrationObj)));


    if (PosSystemConfigrationId > 0)
        url = PosSystemConfigrationServiceURL + "Edit";
    else
        debugger
            url = PosSystemConfigrationServiceURL + "Add";
    
    post(url, data, function (returned) { saveSuccess(returned, isAdd, template); }, saveError, getEntitylockAjaxHeaders(lastUpdatedDate));
}

function saveSuccess(returnedValue, isAdd, template) {
    $('.save-cancel-pnl,.InvoiceNumberFormatRequired , .InvoiceNumberFormatRequired').hide();

    if (returnedValue.statusCode.Code == 501) {
        hideLoading();
        return false;
    }
    else if (returnedValue.statusCode.Code == 0) {
        hideLoading();
        if (returnedValue.statusCode.message == 'validation' && returnedValue.result) {
            filterError(returnedValue.result, template);
        }
        showStatusMsg(getResource("FaildToSave"));
        return false;
    }

    showStatusMsg(getResource("savedsuccessfully"));
    hideLoading();
    LoadSystemConfigrationData();

}

 function saveError(ex) {
     hideLoading();
     handleLockEntityError(ex, deletedEntityCallBack, updatedEntityCallBack);
 }



 function filterError(errors, template) {
     var allMsg = '';
     template.find('.validation').remove();
     $.each(errors, function (index, error) {
         if (error.GlobalMessage) {
             allMsg += '<span>' + error.Message + '<br/></span>';
         }
         else if (error.ControlName == 'GiftVoucherOverAmount') {
             addPopupMessage(template.find('input.GiftVoucherOverAmount'), getResource("required"));
         }
         else if (error.ControlName == 'LaybyMinPercentage') {
             addPopupMessage(template.find('input.LaybyMinPercentage'), getResource("required"));
         }
         else if (error.ControlName == 'LoginMethod') {
             addPopupMessage(template.find('select.LoginMethod'), getResource("required"));
         }
         else if (error.ControlName == 'AfterCloseRegisterValue') {
             addPopupMessage(template.find('select.AfterCloseRegisterValue'), getResource("required"));
         }
         else if (error.ControlName == 'InvoiceNumberFormat') {
             addPopupMessage(template.find('input.InvoiceNumberFormat'), getResource("required"));
         }
         else if (error.ControlName == 'LaybyNumberFormat') {
             addPopupMessage(template.find('input.LaybyNumberFormat'), getResource("required"));
         }
         else if (error.ControlName == 'CashCustomer') {
             addPopupMessage(template.find('input.CashCustomer'), getResource("required"));
         }
         else if (error.ControlName == 'AccpacStagingIp') {
             addPopupMessage(template.find('input.AccpacStagingIp'), getResource("required"));
         }
         else if (error.ControlName == 'PosServerIp') {
             addPopupMessage(template.find('input.PosServerIp'), getResource("required"));
         }
         else if (error.ControlName == 'AccpacDataBaseUsername') {
             addPopupMessage(template.find('input.AccpacDataBaseUsername'), getResource("required"));
         }
         else if (error.ControlName == 'AccpacDataBasePassword') {
             addPopupMessage(template.find('input.AccpacDataBasePassword'), getResource("required"));
         }
         else if (error.ControlName == 'PosServerDataBaseUsername') {
             addPopupMessage(template.find('input.PosServerDataBaseUsername'), getResource("required"));
         }
         else if (error.ControlName == 'PosServerDataBasePassword') {
             addPopupMessage(template.find('input.PosServerDataBasePassword'), getResource("required"));
         }
         else if (error.ControlName == 'PosServerDataBaseName') {
             addPopupMessage(template.find('input.PosServerDataBaseName'), getResource("required"));
         }
         else if (error.ControlName == 'AccpacDataBaseName') {
             addPopupMessage(template.find('input.AccpacDataBaseName'), getResource("required"));
         }
         else if (error.ControlName == 'IsSysetm') {
             addPopupMessage(template.find('input.IsSysetm'), getResource("required"));
         }

     });

     hideLoading();
     if (allMsg.trim()) {
         showOkayMsg('Validation error', allMsg.trim());
     }
 }

 function LoadData(dataObject) {
 debugger
     $('.InvoiceNumberFormatRequired , .InvoiceNumberFormatRequired').hide();

     selectedEntityId = dataObject.PosConfigId;
     PosSystemConfigrationId = dataObject.PosConfigId;
     currentEntityLastUpdatedDate = dataObject.UpdatedDate || dataObject.Updated_Date;
     FormatINFStrings(dataObject.InvoiceNumberFormat);
     FormatLNFStrings(dataObject.LaybyNumberFormat);

     if (dataObject != null) {
         currentEntityLastUpdatedDate = dataObject.UpdatedDate || dataObject.Updated_Date;
         for (var prop in dataObject) {
             var className = '.' + prop;
             var lblValue = dataObject[prop];
             var lblValueId = prop + 'Id';

             if ($(className).hasClass('lst')) {
                 var valudId = dataObject[lblValueId];
                 if (valudId > 0) {
                     var item = { label: lblValue, value: valudId };
                     eval(prop + 'AC.set_Item(' + JSON.stringify(item) + ')');
                 }
             }
             else if ($(className).hasClass('txt') || $(className).hasClass('integer')) {
                 if ($(className).hasClass('integer')) {
                     if (!lblValue)
                         lblValue = 0;
                     $('.' + prop).autoNumericSet(lblValue);
                 }
                 else $('.' + prop).val(lblValue);
             }
             else if ($(className).hasClass('txt') || $(className).hasClass('numeric')) {
                 if (lblValue)
                     $('.' + prop).autoNumericSet(lblValue);
             }
             else if ($(className).hasClass('date-time')) {
                 if (lblValue)
                     lblValue = getDate('dd-mm-yy hh:mm tt', formatDate(lblValue));
                 $('.' + prop).val(lblValue);
             }
             else if ($(className).hasClass('date')) {
                 if (lblValue)
                     lblValue = getDate('dd-mm-yy', formatDate(lblValue));
                 $('.' + prop).val(lblValue);
             }
             else if ($(className).hasClass('chk')) {
                 $('.Y' + prop + ',.N' + prop).hide();
                 lblValue = eval(dataObject[prop]);
                 if (lblValue) {
                     $('.' + prop).attr('checked', 'checked');
                     $('.Y' + prop).show();
                     $('.N' + prop).hide();
                 }
                 else {
                     $('.Y' + prop).hide();
                     $('.N' + prop).show();
                 }
                 lblValue = "";
             }
             else if ($(className).hasClass('radio')) {
                 $(className).attr('name', 'entity-group' + entity.Id);
                 template.find('.' + lblValue).attr({ checked: true });
             }

             else if ($(className).hasClass('Format')) {
                 $('.' + prop).val(lblValue);
                 if (lblValue != null && lblValue != '' && lblValue != undefined && lblValue.indexOf('^') > 0) {
                     lblValue = lblValue.replace(/\^/g, '');
                     lblValue = lblValue.replace(/blank/g, '');
                 }
                 $('.' + prop).textFormat(lblValue);
                
             }

             // Fill In Labels
             if (lblValue) {                
                 $('.' + prop).textFormat(lblValue);
             }

         }
     }
 }


 function FormatINFStrings(value) {
     if (value != null && value  != '') {
         parts = value.split("^");
         $('input.INFPrefix').val((parts[0] == 'blank') ? '' : parts[0]);
         $('input.INFValD1').val((parts[1] == 'blank') ? '' : parts[1]);
         $("#INFlstSC option:contains('" + ((parts[2] == 'blank') ? '' : parts[2]) + "')").attr("selected", "selected");
         $('input.INFValD2').val((parts[3] == 'blank') ? '' : parts[3]);
         $("#INFlstRC option:contains('" + ((parts[4] == 'blank') ? '' : parts[4]) + "')").attr("selected", "selected");
         $('input.INFValD3').val((parts[5] == 'blank') ? '' : parts[5]);
         $('input.INFLength').val((parts[6] == 'blank') ? '' : parts[6].length);
     }

 }
 function FormatLNFStrings(value) {
     if (value != null && value != '') {
         parts = value.split("^");
         $('input.LNFPrefix').val((parts[0] == 'blank') ? '' : parts[0]);
         $('input.LNFValD1').val((parts[1] == 'blank') ? '' : parts[1]);
         $("#LNFlstSC option:contains('" + ((parts[2] == 'blank') ? '' : parts[2]) + "')").attr("selected", "selected");
         $('input.LNFValD2').val((parts[3] == 'blank') ? '' : parts[3]);
         $("#LNFlstRC option:contains('" + ((parts[4] == 'blank') ? '' : parts[4]) + "')").attr("selected", "selected");
         $('input.LNFValD3').val((parts[5] == 'blank') ? '' : parts[5]);
         $('input.LNFLength').val((parts[6] == 'blank') ? '' : parts[6].length);
     }
 }
