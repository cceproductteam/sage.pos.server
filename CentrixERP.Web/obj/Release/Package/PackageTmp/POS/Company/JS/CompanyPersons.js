﻿var PersonServiceURL;

//function getCompanyPersons(companyId) {

////    showLoading($('.TabbedPanels'));
////    $('.companyPersons-tab').children().remove();
////    var data = formatString('{keyword:"", page:1, resultCount:50,argsCriteria:"{ PersonId:0, ExciptId:0,CompanyId:{0}}"}', companyId);
////    var url = systemConfig.ApplicationURL_Common + systemConfig.PersonsServiceURL + "FindAllLite";

//    post(url, data,
//     function (data) {
//         if (data.result != null) {
////             var personObj = data.result;
////             $('.companyPersons-tab').parent().find('.DragDropBackground').remove();
////             $.each(personObj, function (index, item) {
////                 var template = $(CompanyPersonsTemplate).clone();

////                 template.css('cursor', 'default');
////                 template = mapEntityInfoToControls(item, template);
//                 template.find('.FullNameEn').text(Truncate(item.FullNameEn, 50));
//                 template.find('.EmailBusiness').text(Truncate(item.BusinessEmail, 30));

//                 var countryCode = item.CountryMobileNumber != null && item.CountryMobileNumber != "" ? "+(" + item.CountryMobileNumber + ")" : "";
//                 var areaCode = item.AreaMobileNumber != null && item.AreaMobileNumber != "" ? item.AreaMobileNumber + "-" : "";
//                 var phoneNumber = item.MobileNumber != null ? item.MobileNumber : "";
//                 var FullPhoneNumber = countryCode + areaCode + phoneNumber;

//                 template.find('.MobileNumber').text(FullPhoneNumber);
//                 ViewPerson(item, template);
//                 $('.companyPersons-tab').append(template);
//             });
//         }
//         hideLoading();

//     }, '', '');
//}


//function ViewPerson(Item, template) {
//    var id = (Item != null) ? Item.Id : 0;
//    template.find('.ViewPerson').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.PersonList + "?key=" + id);
//    template.find('.Business-email-to').attr('href', 'mailto:' + Item.BusinessEmail);
//}



function getCompanyPersons(companyId) {

    PersonServiceURL = systemConfig.ApplicationURL_Common + systemConfig.PersonsServiceURL;
    $('.companyPersons-tab').children().remove();
    showLoading($('.TabbedPanels'));

    var data = formatString('{keyword:"", page:1, resultCount:50,argsCriteria:"{ PersonId:0, ExciptId:0,CompanyId:{0}}"}', companyId);
    var url = systemConfig.ApplicationURL_Common + systemConfig.PersonsServiceURL + "FindAllLite";
    post(url ,data, function (returned) {
        CompanySuccess(returned);
    }, function () { }, getEntitylockAjaxHeaders(getEntityLockDateFormat(lastUpdatedDate)));



}


function CompanySuccess(data) {

    var companyObj = data.result;
    addCompanyPersonTab(null);
    if (companyObj != null) {
        $('.companyPersons-tab').parent().find('.DragDropBackground').remove();
        $.each(companyObj, function (index, item) {
            addCompanyPersonTab(item);
        });
    }

    hideLoading();

    attachTabEvents();
}


function addCompanyPersonTab(item) {

    var id = (item != null) ? item.PersonId : 0;
    var template = $(CompanyPersonsTemplate).clone();
    var updatedDate = null;

    LoadDatePicker(template.find('input:.date'), 'dd-mm-yy');

    if (item != null) {
        template.find('.historyUpdated').append(HistoryUpdatedListViewTemplate.clone());
        template = mapEntityInfoToControls(item, template);
    
        updatedDate = item.UpdatedDate;


    }
    else {
        template.addClass('addnew');
        template.hide();
        LoadDatePicker('input:.date', 'dd-mm-yy');

        $('.add-new-persons').click(function () {
            template.find('.cx-control-red-border').removeClass('cx-control-red-border');            
            setAddLinkEvent(template);
            return false;
        });
    }



    template.find('.save-tab-lnk').on('click', function () {
        //debugger;
        savePersons(id, studentId, SessionAC, ProgramAC, InterviewResultAC, AppearanceAC, AttitudeAC, EnglishAC, ComputerAC, CommitmentAC, TypeAC, updatedDate, template);

        return false;
    });

    if (item != null) {
        template.find('.delete-tab-lnk').click(function () {
            showConfirmMsg(DeleteTitleMessage, DeleteMessage, function () {
                showLoading($('.TabbedPanels'));
                lockForDelete(systemEntities.StudentSessionEntityId, item.StudentSessionId, function () {
                    deleteSession(id, studentId)
                    template.remove();

                }, function () {
                    template.remove();
                });
            });
            return false;
        });

        template.find('.edit-tab-lnk').unbind().click(function () {

            showLoading($('.TabbedPanels'));
            lockEntity(systemEntities.StudentSessionEntityId, item.StudentSessionId, function () {

                template.find('.edit-tab-lnk').hide();
                template.find('.save-tab-lnk').show();
                template.find('.cancel-tab-lnk').show();
                template.find('.delete-tab-lnk').hide();

                if (!template.hasClass('Tab-listSelected'))
                    template.find('.itemheader').click();

                $('.WhenEdit').show();

                template.find('.viewedit-display-none').show();
                template.find('.viewedit-display-block').hide();
                template.find('.cx-control:visible').first().focus();

                if (ProgramAC.get_ItemValue != -1) {
                    SessionAC.Disabled(false);
                }


            }, item.UpdatedDate, function () {
                getSession(studentId);
            }, function () {
                template.remove();
            });
            return false;
        });

    }
    template.find('.cancel-tab-lnk').click(function () {
        if (item == null) {
            template.find('.delete-tab-lnk').show();
            template.find('.itemheader').click();
            $('.addnew').hide();
        }
        else {
            showLoading($('.TabbedPanels'));
            UnlockEntity(systemEntities.StudentSessionEntityId, item.StudentSessionId, function () {

                template.find('.delete-tab-lnk').show();
                template.find('.itemheader').click();

                $('.addnew').hide();
                $('.validation').remove();

            });
        }
        return false;
    });


    $('.Session-tab').append(template);
}

function getSessionSuccess(template, returned) {
    var sessnDetailObj = returned.result;
    if (!sessnDetailObj) return;
    template.find('span.Location').text(sessnDetailObj.Location);
    template.find('span.SessionDate').text(getDate("dd-mm-yy", formatDate(sessnDetailObj.SessionDate)));
    template.find('span.StartTIme').text(sessnDetailObj.StartTime);
    template.find('span.EndTIme').text(sessnDetailObj.EndTime);
    template.find('span.Duration').text(sessnDetailObj.Duration);


    template.find('span.Governoratee').text(StudentGovernorate);


}

function getStatus(StatusId) {
    if (StatusId)
        return getResource("Yes");
    else
        return getResource("No");
}


function getStudentProgSuccess(template, returned) {
    var StudentProgResult = returned.result;

    if (StudentProgResult != null && StudentProgResult.length > 1) {

        template.find('input.ExLoyacId').attr('checked', 'checked');
        template.find('span.ExLoyacId').text(getResource("Yes"));
    }
    return;
}

function saveSession(id, studentId, SessionAC, ProgramAC, InterviewResultAC, AppearanceAC, AttitudeAC, EnglishAC, ComputerAC, CommitmentAC, TypeAC, lastUpdatedDate, template) {
    if (!saveForm(template)) {
        return false;
    }


    showLoading($('.TabbedPanels'));

    var StudentSessionObj = {

        StudentSessionId: id,
        StudentId: studentId,
        SessionId: SessionAC.get_ItemValue(),
        ProgramId: ProgramAC.get_ItemValue(),
        GovernorateId: StudentGovernorateId,
        InterviewResultId: InterviewResultAC.get_ItemValue(),
        ExLoyacId: template.find('input:.ExLoyacId').is(':checked'),
        AppearanceId: AppearanceAC.get_ItemValue(),
        AttitudeId: AttitudeAC.get_ItemValue(),
        EnglishId: EnglishAC.get_ItemValue(),
        ComputerId: ComputerAC.get_ItemValue(),
        CommitmentId: CommitmentAC.get_ItemValue(),
        GeneralSkills: template.find('input:.GeneralSkills').val().trim(),
        PreviousVolunteerWork: template.find('input:.PreviousVolunteerWork').val().trim(),
        SummerCourseId: template.find('input:.SummerCourseId').is(':checked'),
        WorkExperienceId: template.find('input:.WorkExperienceId').is(':checked'),
        TypeId: TypeAC.get_ItemValue(),
        CompanyName: template.find('input:.CompanyName').val().trim(),
        StartDate: getDate("mm-dd-yy", getDotNetDateFormat(template.find('input:.StartDate').val())),
        PreferenceCompanies: template.find('input:.PreferenceCompanies').val().trim(),
        UnpreferenceCompanies: template.find('input:.UnpreferenceCompanies').val().trim(),
        Notes: template.find('input:.Notes').val().trim()
    };

    var data = formatString('{id:{0},studentId:{1}, StudentSessionInfo:"{2}"}', id, studentId, escape(JSON.stringify(StudentSessionObj)));
    var url = SessionServiceURL + ((id > 0) ? "Edit" : "Add");
    post(url, data, function (returned) {
        saveSessionSucess(studentId, template, returned);

    }, function (ex) {
        saveSessionError(ex, studentId)
    }, getEntitylockAjaxHeaders(getEntityLockDateFormat(lastUpdatedDate)));

}



function saveSessionSucess(studentId, template, returned) {
    if (returned.statusCode.Code == 501) {

        hideLoading();
        showStatusMsg(getResource("StudentAlreadyHasSessionSameTime"));

        return false;
    }

    showStatusMsg(getResource("savedsuccessfully"));
    hideLoading();
    getSession(studentId);
    template.find('.edit-tab-lnk').show();
    template.hide();
}
function saveSessionError(ex, studentId) {
    handleLockEntityError(ex, function () { template.remove(); }, function () { getSession(studentId); });
}


function deleteSession(id, studentId) {

    var url = SessionServiceURL + "Delete";
    var data = formatString('{id:{0}}', id);
    post(url, data, function (returned) {
        deleteSessionSuccess(studentId);


    }, function (ex) {
        handleLockEntityError(ex, function () { template.remove(); }, function () { getSession(studentId); });
    }, getEntitylockAjaxHeaders(getEntityLockDateFormat(lastUpdatedDate)));
}


function deleteSessionSuccess(studentId) {
    showStatusMsg(getResource("DeletedSuccessfully"));
    getSession(studentId);
}


