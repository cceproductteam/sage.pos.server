﻿var CompanyServiceURL;
var CompanyViewItemTemplate, CompanyPersonsTemplate, CompanyRelatedOpportunities, RelatedGiveawaysTemplate, RelatedCaseTemplate, RelatedOpportunityTemplate;
var CompanyTypeAC, DecisionMakerAC, SalesManAccountManagerAC, SegmentAC, SourceAC, YearlyRevenueAC, EmployeeNumberAC, StatusAC, ParentCompanyAC, PaymentTermsAC,
PaymentTypeAC, CountryAC, CustomerReferralCompanyAC, CustomerReferralPersonAC, EmployeeUserAC, CountryOfOrigionAC;
var ItemListTemplate;
var addCompanyMode = false;
var CompanyId = -1, CustomerReferralCompanyId = -1;
var CompanyName = '';
var PhoneNumber = '';
var InterestedInAC;
var lastUpdatedDate = null;



function InnerPage_Load() {

    setAddLinkAction(getResource("AddCompany"), 'add-new-item-large');

    $('span:.page-title').html(getResource("Company"));
    setSelectedMenuItem($(".pos-icon"))
    CompanyServiceURL = systemConfig.ApplicationURL_Common + systemConfig.CompanyServiceURL;

    currentEntity = systemEntities.CompanyEntityId;


    //Advanced search Data
    mKey = 'comp';
    InnerPage_Load2();
    $('.QuickSearch-holder').show();
    LoadSearchForm2(mKey, false);
    //Advanced search Data

    GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.AddCompanyTemplate, function () {
        GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.CompanyListItemTemplate, loadData, 'ItemListTemplate');
        GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.CompanyPersonsListViewItemTemplate, null, 'CompanyPersonsTemplate')
        GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.RelatedGiveawaysListViewItemTemplate, null, 'RelatedGiveawaysTemplate')
        GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.RelatedCaseListViewItemTemplate, null, 'RelatedCaseTemplate')
        GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.RelatedOpportunityListViewItemTemplate, null, 'RelatedOpportunityTemplate')

    }, 'AddItemTemplate');

    $('.result-block').live("click", function () {
        CompanyId = $(this).attr('id');
    });

    deletedEntityCallBack = function () {
        removeDeletedEntity();
    };

    updatedEntityCallBack = function () {
        getEntityData(selectedEntityId);
    };

    $('li:.email-tabs').live('click', function () {
        EmailTemplate = $(EmailTemplate);
        checkPermissions(this, systemEntities.EmailEntityId, -1, EmailTemplate, function (template) {
            getEmails(systemEntities.CompanyEntityId, selectedEntityId);
        });
    });
   
    
    $('li:.phone-tabs').live('click', function () {
        PhoneTemplate = $(PhoneTemplate);
        checkPermissions(this, systemEntities.PhoneEntityId, -1, PhoneTemplate, function (template) {
            getPhones(systemEntities.CompanyEntityId, selectedEntityId);
        });
    });
    


   

}

function loadData(data) {

    GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.CompanyListViewItemTemplate, function () {
        checkPermissions(this, systemEntities.CompanyEntityId, -1, CompanyViewItemTemplate, function (returnedData) {
            CompanyViewItemTemplate = returnedData;
            loadEntityServiceURL = CompanyServiceURL + "FindByIdLite";
            entityItemListTemplate = data;
            entityItemViewTemplate = returnedData;
            findAllEntityServiceURL = CompanyServiceURL + "FindAllLite";
            deleteEntityServiceURL = CompanyServiceURL + "Delete";
            selectEntityCallBack = getCompanyInfo;
            listEntityCallBack = callBackAfterList;
            loadDefaultEntityOptions();
        });
    }, 'CompanyViewItemTemplate');

}

function initControls(template, isAdd) {


    var controlTemplate = isAdd ? template : $('.company-info');
    LoadDatePicker('input:.date', 'dd-mm-yy');
    $('.numeric').autoNumeric();
    $('.integer').autoNumeric({ aPad: false });
    preventInputChars(controlTemplate.find('input:.integer'));
    preventInputChars(controlTemplate.find('input:.noChars'));


    if (!addCompanyMode)
        controlTemplate.find('.historyUpdated').append(HistoryUpdatedListViewTemplate.clone());
    //CompanyTypeAC = fillDataTypeContentList(controlTemplate.find('select:.CompanyType'), systemConfig.dataTypes.ContactType, true, false);
    //CountryOfOrigionAC = controlTemplate.find('select:.CountryOfOrigion').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.CountryServiceURL + "FindAll", required: false, autoLoad: false });
    DecisionMakerAC = controlTemplate.find('select:.DecisionMaker').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.PersonsServiceURL + 'FindAllLite' });
    DecisionMakerAC.set_Args(formatString('{ CompanyId:{0}}', CompanyId));
    if (isAdd) {
        //CountryAC = controlTemplate.find('select:.Country').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.CountryServiceURL + "FindAll", required: false, autoLoad: false });
    }
    SalesManAccountManagerAC = controlTemplate.find('select:.SalesManAccountManager').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.UsersWebService + 'FindUserAllLite' });
    SegmentAC = fillDataTypeContentList(controlTemplate.find('select:.Segment'), systemConfig.dataTypes.Segment, true, false);
    SourceAC = fillDataTypeContentList(controlTemplate.find('select:.Source'), systemConfig.dataTypes.Source, true, false);
    YearlyRevenueAC = fillDataTypeContentList(controlTemplate.find('select:.YearlyRevenue'), systemConfig.dataTypes.YearlyRevenue, false, false);
    EmployeeNumberAC = fillDataTypeContentList(controlTemplate.find('select:.EmployeeNumber'), systemConfig.dataTypes.EmployeeNumber, false, false);
    StatusAC = fillDataTypeContentList(controlTemplate.find('select:.Status'), systemConfig.dataTypes.CompanyStatus, false, false);
    //InterestedInAC = fillDataTypeContentList(controlTemplate.find('select:.InterestedIn'), systemConfig.dataTypes.InterestedIn, false, true);


}

function initAddControls(template) {

    var Label = getResource('Customer');

    DecisionMakerAC = template.find('select:.DecisionMaker').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.PersonsServiceURL + 'FindAllLite' });
   // CompanyTypeAC = fillDataTypeContentList(template.find('select:.CompanyType'), systemConfig.dataTypes.ContactType, true, false);


    //CompanyTypeAC.set_Item({ label: Label, value: 690 });
    DecisionMakerAC.set_Args(formatString('{ CompanyId:{0}}', 90000));

    preventInputChars($('input:.phone-nums'));

    initControls(template, true);
}

function getPhoneFormat(obj) {


    var MCountryCode;
    if (obj.CountryMobileNumber != null && obj.CountryMobileNumber != "") {
        if (obj.CountryMobileNumber.substring(0, 2) == '00')
            MCountryCode = "(" + obj.CountryMobileNumber + ")";
        else
            MCountryCode = "+(" + obj.CountryMobileNumber + ")";
    }
    else
        MCountryCode = "";

    //var MCountryCode = obj.CountryMobileNumber != null && obj.CountryMobileNumber != "" ? "+(" + obj.CountryMobileNumber + ")" : "";
    var MAreaCode = obj.AreaMobileNumber != null && obj.AreaMobileNumber != "" ? obj.AreaMobileNumber + "-" : "";
    var MFullPhoneNumber = obj.MobileNumber != null ? obj.MobileNumber : "";
    var FullPhoneNumber = MCountryCode + MAreaCode + MFullPhoneNumber;
    $('.MobileNumber').text(FullPhoneNumber);


    var HCountryCode;
    if (obj.CountryHomeNumber != null && obj.CountryHomeNumber != "") {
        if (obj.CountryHomeNumber.substring(0, 2) == '00')
            HCountryCode = "(" + obj.CountryHomeNumber + ")";
        else
            HCountryCode = "+(" + obj.CountryHomeNumber + ")";
    }
    else
        HCountryCode = "";
    //var HCountryCode = obj.CountryHomeNumber != null && obj.CountryHomeNumber != "" ? "+(" + obj.CountryHomeNumber + ")" : "";
    var HAreaCode = obj.AreaHomeNumber != null && obj.AreaHomeNumber != "" ? obj.AreaHomeNumber + "-" : "";
    var HFullPhoneNumber = obj.HomeNumber != null ? obj.HomeNumber : "";
    var FullPhoneNumber = HCountryCode + HAreaCode + HFullPhoneNumber;
    $('.HomeNumber').text(FullPhoneNumber);

    var FCountryCode;
    if (obj.CountryFaxNumber != null && obj.CountryFaxNumber != "") {
        if (obj.CountryFaxNumber.substring(0, 2) == '00')
            FCountryCode = "(" + obj.CountryFaxNumber + ")";
        else
            FCountryCode = "+(" + obj.CountryFaxNumber + ")";
    }
    else
        FCountryCode = "";
    // var FCountryCode = obj.CountryFaxNumber != null && obj.CountryFaxNumber != "" ? "+(" + obj.CountryFaxNumber + ")" : "";
    var FAreaCode = obj.AreaFaxNumber != null && obj.AreaFaxNumber != "" ? obj.AreaFaxNumber + "-" : "";
    var FFullPhoneNumber = obj.FaxNumber != null ? obj.FaxNumber : "";
    var FullPhoneNumber = FCountryCode + FAreaCode + FFullPhoneNumber;
    $('.FaxNumber').text(FullPhoneNumber);

}

function getCompanyInfo(item) {

    if (item != null) {

        $('.Business-email-to').attr('href', 'mailto:' + item.BusinessEmail);
        $('.Personal-email-to').attr('href', 'mailto:' + item.PersonalEmail);
        getPhoneFormat(item);


        var controlTemplate = $('.company-info');
        $('.HeaderBusinessEmail').text(Truncate(item.BusinessEmail, 10));
        var countryCode = item.CountryMobileNumber != null && item.CountryMobileNumber != "" ? "+(" + item.CountryMobileNumber + ")" : "";
        var areaCode = item.AreaMobileNumber != null && item.AreaMobileNumber != "" ? item.AreaMobileNumber + "-" : "";
        var PhoneNumber = item.MobileNumber != null && item.MobileNumber != "" ? item.MobileNumber : "";
        var FullPhoneNumber = countryCode + areaCode + PhoneNumber;

        var HCountryCode;
        if (item.CountryHomeNumber != null && item.CountryHomeNumber != "") {
            if (item.CountryHomeNumber.substring(0, 2) == '00')
                HCountryCode = "(" + item.CountryHomeNumber + ")";
            else
                HCountryCode = "+(" + item.CountryHomeNumber + ")";
        }
        else
            HCountryCode = "";
        //var HCountryCode = obj.CountryHomeNumber != null && obj.CountryHomeNumber != "" ? "+(" + obj.CountryHomeNumber + ")" : "";
        var HAreaCode = item.AreaHomeNumber != null && item.AreaHomeNumber != "" ? item.AreaHomeNumber + "-" : "";
        var HFullPhoneNumber = item.HomeNumber != null ? item.HomeNumber : "";
        var FullPhoneNumber = HCountryCode + HAreaCode + HFullPhoneNumber;
        $('.HeaderLandLine').text(FullPhoneNumber);


        $('.HeaderCompanyName').text(Truncate(item.CompanyNameEnglish, 10));

        CompanyName = item.CompanyNameEnglish;

        CompanyId = item.CompanyId;

        CustomerReferralCompanyId = item.CustomerReferralCompanyId;
        if (item.IntresetedInList != null) {
            //InterestedInAC.set_Items(item.IntresetedInList);
            $.each(item.IntresetedInList, function (index, itemm) {

                var label = (Lang == systemConfig.WebKeys.Ar) ? itemm.DataTypeContentAr : itemm.DataTypeContent;
                $('.InterestedIn').text($('.InterestedIn').html() + label + ',');
            });
            $('.InterestedIn').text($('.InterestedIn').html().slice(0, $('.InterestedIn').html().length - 1));
        }
        if (item.CompanytypesList != null) {

            CompanyTypeAC.set_Item(item.CompanytypesList[0]);
            $.each(item.CompanytypesList, function (index, item2) {
                var label = (Lang == systemConfig.WebKeys.Ar) ? item2.DataTypeContentAr : item2.DataTypeContent;
                $('.CompanyTypeList').text($('.CompanyTypeList').html() + label);

            });

        }

        $('a:.add-new-persons').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.PersonList + '?cId=' + selectedEntityId);
        //$('.sendEmail-link').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.SendNotifications + '?cid=' + selectedEntityId);
        $('.sendEmail-link').click(function () {
            window.location = systemConfig.ApplicationURL + systemConfig.pageURLs.SendNotifications + '?cid=' + selectedEntityId;
        });


        if(item.Website == null){item.Website="";}
        if (item.Website.substring(0, 7) == 'http://' || item.Website.substring(0, 8) == 'https://')
            $('a:.Website-link').attr('href', item.Website);
        else
            $('a:.Website-link').attr('href', 'http://' + item.Website);    
        //xxxxxxx

        $('.Website').text(item.Website);
        $('a:.DecisionMaker-link').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.PersonList + "?key=" + item.DecisionMakerId);
        $('a:.SalesManAccountManager-link').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.UserList + "?key=" + item.SalesManAccountManagerId);



        refreshListItem(item);

        if ($('.result-block').length > 0 && getQSKey('fdb') > 0 && isFirstTimeAdd) {
            $('.add-entity-button').first().click();
        }
    }

}

function refreshListItem(item) {
    /*   selectedEntityItemList.find('.result-data1 label').text(Truncate(item.CompanyNameEnglish, 30));
    //selectedEntityItemList.find('.result-data2 label').text(Truncate(item.Website, 30));
    selectedEntityItemList.find('.result-data2 label').text((item.BusinessEmail != null) ? ' ' + Truncate(item.BusinessEmail, 30) : '');
    selectedEntityItemList.find('.result-data3 label').text(PhoneNumber);*/
}

function setListItemTemplateValue(entityTemplate, entityItem) {


    entityTemplate.find('.result-data1').addClass('first-td-padding');
    entityTemplate.find('.result-data2').addClass('second-td-padding');
    
    // PhoneNumber = (entityItem.CountryMobileNumber + entityItem.AreaMobileNumber + entityItem.MobileNumber == '0') ? '' : entityItem.CountryMobileNumber + ' ' + entityItem.AreaMobileNumber + ' ' + entityItem.MobileNumber;
    var countryCode = entityItem.CountryMobileNumber != null && entityItem.CountryMobileNumber != "" ? "+(" + entityItem.CountryMobileNumber + ")" : "";
    var areaCode = entityItem.AreaMobileNumber != null && entityItem.AreaMobileNumber != "" ? entityItem.AreaMobileNumber + "-" : "";
    var PhoneNumber = entityItem.MobileNumber != null && entityItem.MobileNumber != "" ? entityItem.MobileNumber : "";
    var FullPhoneNumber = countryCode + areaCode + PhoneNumber;
    //$('.HeaderLandLine').text(FullPhoneNumber);
    //PhoneNumber = (entityItem.CountryHomeNumber + entityItem.AreaMobileNumber + entityItem.HomeNumber == '0') ? '' : entityItem.CountryHomeNumber + ' ' + entityItem.AreaHomeNumber + ' ' + entityItem.HomeNumber;

    CompanyName = entityItem.CompanyNameEnglish;
    entityTemplate.find('.result-data1 label').text(Truncate(CompanyName, 30));

    entityTemplate.find('.result-data2 label').text((entityItem.BusinessEmail != null) ? ' ' + Truncate(entityItem.BusinessEmail, 30) : '');
    var BEmail = entityItem.BusinessEmail != null ? entityItem.BusinessEmail : '';
    entityTemplate.find('.result-data2 label').attr('href', 'mailto:' + BEmail);
    entityTemplate.find('.result-data3 label').text(FullPhoneNumber);
    $('.ResultGridTableDiv table.GridTable').css("float", "left");
    return entityTemplate;
}

function setListItemServiceData(keyword, pageNumber, resultCount) {
    if (!QuickSearch) {

        return formatString('{ keyword: "{0}", page:{1}, resultCount:{2} ,argsCriteria:"{ CompanyId:{3}}"}', keyword, pageNumber, resultCount, filterEntityId);
    }
    else {
        return formatString('{mKey:"{0}",SearchCriteriaObj:"{1}",SortType:{2},SortFields:"{3}",TotalRecords:{4},PageNumber:{5}}', mKey, SearchCriteriaObj, SortType, SortFields, resultCount, pageNumber);
    }
}



function SaveEntity(isAdd) {

    var controlTemplate;
    var valid = true;

    if (isAdd) {
        controlTemplate = $('.add-company-info');
        showLoading('.add-new-item');
    }
    else {
        controlTemplate = $('.company-info');
        showLoading('.Result-info-container');
    }


    // checkIdValidation(controlTemplate);
//    if (CompanyTypeAC.get_Item() == null || CompanyTypeAC.get_Item() == '') {
//        addPopupMessage(controlTemplate.find('select:.CompanyType').parent().find('.ui-combobox'), getResource("Required"));
//        valid = false;
//    }

    if (!saveForm(controlTemplate)) {
        hideLoading();
        valid = false;
    }


    if (!valid) {
        return false;
    }

    var CompanyId = selectedEntityId;
    if (addCompanyMode) {
        showLoading($('#demo1'));
    }
    else {
        lastUpdatedDate = getEntityLockDateFormat(currentEntityLastUpdatedDate);
    }



    var CompanyObj = {
        CompanyNameEnglish: controlTemplate.find('input.CompanyNameEnglish').val().trim(),
        CompanyNameArabic: controlTemplate.find('input.CompanyNameArabic').val(),
        Website: controlTemplate.find('input.Website').val(),
        CompanyType:0,// CompanyTypeAC.get_ItemValue(),
        DecisionMaker: DecisionMakerAC.get_ItemValue(),
        SalesManAccountManager: SalesManAccountManagerAC.get_ItemValue(),
        Abbreviation: controlTemplate.find('input.Abbreviation').val(),
        RegistrationNumber: controlTemplate.find('input.RegistrationNumber').val(),
        RegistrationDate: getDotNetDateFormat(controlTemplate.find('input.RegistrationDate').val()),
        Segment: SegmentAC.get_ItemValue(),
        Source: SourceAC.get_ItemValue(),
        YearlyRevenue: YearlyRevenueAC.get_ItemValue(),
        EmployeeNumber: EmployeeNumberAC.get_ItemValue(),
        Status: StatusAC.get_ItemValue(),
        CountryOfOrigion: 0,//CountryOfOrigionAC.get_ItemValue(),
        FacebookPage: controlTemplate.find('input.FacebookPage').val(),
        LinkedInPage: controlTemplate.find('input.LinkedInPage').val(),
        TwitterAccount: controlTemplate.find('input.TwitterAccount').val(),
        InterestedIn: 0,//InterestedInAC.get_ItemsIds()
    };



    var data = formatString('{id:{0}, CompanyInfo:"{1}"}', selectedEntityId, escape(JSON.stringify(CompanyObj)));

    if (!isAdd)
        url = CompanyServiceURL + "Edit";
    else
        url = CompanyServiceURL + "Add";

    post(url, data, function (returnedValue) {
        
        saveSuccess(controlTemplate, returnedValue);
    }, saveError, getEntitylockAjaxHeaders(lastUpdatedDate));
}

function saveSuccess(controlTemplate, returnedValue) {
    var companyObject = returnedValue.result;
    if (returnedValue.statusCode.Code == 501) {

        hideLoading();
        addPopupMessage(controlTemplate.find('input.CompanyNameEnglish'), getResource("DublicatatedName"));
        addPopupMessage(controlTemplate.find('input.CompanyNameArabic'), getResource("DublicatatedName"));
        return false;
    }
    else if (returnedValue.statusCode.Code == 0) {
        hideLoading();
        if (returnedValue.statusCode.message == 'validation' && returnedValue.result) {
            filterError(returnedValue.result,controlTemplate);
        }
        showStatusMsg(getResource("FaildToSave"));
        return false;
    }

    CompanyId = returnedValue.result;
    if (!AddMode) {

        showStatusMsg(getResource("savedsuccessfully"));
        hideLoading();
        loadEntityData(loadEntityServiceURL, entityItemViewTemplate, selectedEntityId, getCompanyInfo);
        //setListItemTemplateValue(selectedEntityItemList, obj);

    }
    else {
        saveCompanyContactInfo(CompanyId, controlTemplate, companyObject);
        hideLoading();
        isAdd = false;
        $('.add-new-item').hide();
        $('.add-new-item').empty();

    }
}

function saveError(ex) {
    hideLoading();
    handleLockEntityError(ex, deletedEntityCallBack, updatedEntityCallBack);
}

function saveCompanyContactInfo(CompanyId, controlTemplate, companyObject) {

   

    var HomePhone = {
        PhoneID: -1,
        PhoneNumber: controlTemplate.find('input:.PphoneNumber').val().trim(),
        AreaCode: controlTemplate.find('input:.PareaCode').val().trim(),
        CountryCode: controlTemplate.find('input:.PcountryCode').val().trim(),
        Type: systemConfig.dataTypes.LandLineNumber
    };

    var FaxPhone = {
        PhoneID: -1,
        PhoneNumber: controlTemplate.find('input:.SphoneNumber').val().trim(),
        AreaCode: controlTemplate.find('input:.SareaCode').val().trim(),
        CountryCode: controlTemplate.find('input:.ScountryCode').val().trim(),
        Type: systemConfig.dataTypes.FaxNumber
    };

    var MobilePhone = {
        PhoneID: -1,
        PhoneNumber: controlTemplate.find('input:.MphoneNumber').val().trim(),
        AreaCode: controlTemplate.find('input:.MareaCode').val().trim(),
        CountryCode: controlTemplate.find('input:.McountryCode').val().trim(),
        Type: systemConfig.dataTypes.MobileNumber
    };


    var BusinessEmail = {
        Email_Id: -1,
        Type: 758,
        EmailAddress: controlTemplate.find('input:.BusinessEmail').val().trim()
    };

    var PersonalEmail = {
        Email_Id: -1,
        Type: 757,
        EmailAddress: controlTemplate.find('input:.PersonalEmail').val().trim()
    };



    var url = CompanyServiceURL + "saveCompanyContactInfo";
    var data = formatString('{CompanyId:{0}, AddressObj:"{1}",HomePhoneObj:"{2}",FaxPhone:"{3}",MobilePhone:"{4}",BusinessEmail:"{5}",PersonalEmail:"{6}"}', companyObject.Id,
      null,
      escape(JSON.stringify(HomePhone)),
      escape(JSON.stringify(FaxPhone)),
      escape(JSON.stringify(MobilePhone)),
      escape(JSON.stringify(BusinessEmail)),
      escape(JSON.stringify(PersonalEmail))
     );


    post(url, data,
     function (returnedValue) {

         appendNewListRow(companyObject);

         //  window.location = systemConfig.ApplicationURL + systemConfig.pageURLs.CompanyList + "?key=" + CompanyId + "&mood=add";
     }, saveError, '');

}



function filterError(errors,template) {

    var allMsg = '';
    $('.validation').remove();
    if (!template)
        template = $('.Company-info');

    $.each(errors, function (index, error) {

        
        if (error.GlobalMessage) {
            allMsg += '<span>' + error.Message + '<br/></span>';
        }
        else if (error.ControlName == 'CompanyNameEnglish') {
            addPopupMessage(template.find('input.CompanyNameEnglish'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'CompanyNameArabic') {
            addPopupMessage(template.find('input.CompanyNameArabic'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'Website') {
            addPopupMessage(template.find('input.Website'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'CompanyType') {
            addPopupMessage(template.find('select.CompanyType'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'DecisionMaker') {
            addPopupMessage(template.find('select.DecisionMaker'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'SalesManAccountManager') {
            addPopupMessage(template.find('select.SalesManAccountManager'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'Abbreviation') {
            addPopupMessage(template.find('input.Abbreviation'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'RegistrationNumber') {
            addPopupMessage(template.find('input.RegistrationNumber'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'RegistrationDate') {
            addPopupMessage(template.find('input.RegistrationDate'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'Segment') {
            addPopupMessage(template.find('select.Segment'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'Source') {
            addPopupMessage(template.find('select.Source'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'YearlyRevenue') {
            addPopupMessage(template.find('select.YearlyRevenue'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'EmployeeNumber') {
            addPopupMessage(template.find('select.EmployeeNumber'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'Status') {
            addPopupMessage(template.find('select.Status'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'IsBranch') {
            addPopupMessage(template.find('input.IsBranch'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'ParentCompany') {
            addPopupMessage(template.find('select.ParentCompany'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'PaymentTerms') {
            addPopupMessage(template.find('select.PaymentTerms'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'PaymentType') {
            addPopupMessage(template.find('select.PaymentType'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'CustomerReferralCompanyId') {
            addPopupMessage(template.find('select.CustomerReferralCompany'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'CustomerReferralPersonId') {
            addPopupMessage(template.find('select.CustomerReferralPerson'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'EmployeeUserId') {
            addPopupMessage(template.find('select.EmployeeUser'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'ScoutingComments') {
            addPopupMessage(template.find('textarea.ScoutingComments'), getResource(error.ResourceKey));
        }
    });

    hideLoading();
    if (allMsg.trim()) {
        showOkayMsg('Validation error', allMsg.trim());
    }
}



function getCompanyPersons(companyId) {

    showLoading($('.TabbedPanels'));
    $('.companyPersons-tab').children().remove();
    var data = formatString('{keyword:"", page:1, resultCount:50,argsCriteria:"{ PersonId:0, ExciptId:0,CompanyId:{0}}"}', companyId);
    var url = systemConfig.ApplicationURL_Common + systemConfig.PersonsServiceURL + "FindAllLite";

    post(url, data,
     function (data) {
         if (data.result != null) {
             var personObj = data.result;
             $('.companyPersons-tab').parent().find('.DragDropBackground').remove();
             $.each(personObj, function (index, item) {
                 var template = $(CompanyPersonsTemplate).clone();

                 template.css('cursor', 'default');
                 template = mapEntityInfoToControls(item, template);
                 template.find('.FullNameEn').text(Truncate(item.FullNameEn, 50));
                 template.find('.EmailBusiness').text(Truncate(item.BusinessEmail, 30));

                 var countryCode = item.CountryMobileNumber != null && item.CountryMobileNumber != "" ? "+(" + item.CountryMobileNumber + ")" : "";
                 var areaCode = item.AreaMobileNumber != null && item.AreaMobileNumber != "" ? item.AreaMobileNumber + "-" : "";
                 var phoneNumber = item.MobileNumber != null ? item.MobileNumber : "";
                 var FullPhoneNumber = countryCode + areaCode + phoneNumber;

                 template.find('.MobileNumber').text(FullPhoneNumber);
                 ViewPerson(item, template);
                 $('.companyPersons-tab').append(template);
             });
         }
         hideLoading();

     }, '', '');
}


function ViewPerson(Item, template) {
    var id = (Item != null) ? Item.Id : 0;
    template.find('.ViewPerson').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.PersonList + "?key=" + id);
    template.find('.Business-email-to').attr('href', 'mailto:' + Item.BusinessEmail);
}




function checkPermissions(Sender, EntityId, ParentId, Template, Callback) {
    if ($(Sender).attr('pChecked') != 'checked') {
        //$(Sender).attr('pChecked', 'checked');
        var data = formatString('{EntityId:{0},ParentId:{1}}', EntityId, ParentId);
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: PemissionWebServiceURL + "checkUnAvailablePermissions",
            data: data,
            datatype: "json",
            success: function (data) {
                var returnedData = eval("(" + data.d + ")");
                if (returnedData.statusCode.Code == 1) {
                    if (returnedData.result != null) {
                        UnAvailablePermissionsList = returnedData.result;
                        Template = ApplyUserPermissions(Template);
                    }
                    else {
                        $('.add-entity-button').show();
                        $('.TabbedPanelsContent:visible').find('.Tab-action-header').show();
                    }
                }
                else {
                    //if 0: then there is no permissions at all
                    // alert('no access');
                }
                if (Callback) {
                    Callback(Template);
                }
            },
            error: function (e) { }
        });
    }
    else {
        if (Callback)
            Callback(Template);
    }
}


function callBackAfterList() {
    if ($('.result-block').length <= 0 && getQSKey('fdb') > 0 && isFirstTimeAdd) {
        $('.add-entity-button').first().click();
    }
}