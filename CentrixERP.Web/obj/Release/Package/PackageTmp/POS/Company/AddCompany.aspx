<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddCompany.aspx.cs" MasterPageFile="~/Common/MasterPage/Site.master"
    Inherits="Centrix.StandardEdition.CRM.Web.CRM.Company.AddCompany" %>

<asp:Content runat="server" ContentPlaceHolderID="PageHeader" ID="PageHeader">
    <script type="text/javascript" src="JS/Company.js?v=<%=Centrix_Version %>"></script>
    <script src="../../Common/JScript/CommonTabs.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <script type="text/javascript">
        addCompanyMode = true;
           
    </script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="PageBody" ID="PageBody">
    <div id='content' class="Company-info">
        <div class="Add-new-form">
            <div class="Add-title">
                <span resourcekey="CompanyDetails"></span>
                <div class="Action-div">
                    <ul>
                        <li><span class="ActionSave-link save-tab-lnk"></span><a class="ActionSave-link save-tab-lnk">
                            <span resourcekey="Save"></span></a></li>
                        <li><span class="ActionCancel-link cancel-tab-lnk"></span><a class="ActionCancel-link cancel-tab-lnk">
                            <span resourcekey="Cancel"></span></a></li>
                    </ul>
                </div>
            </div>
            <table border="0" cellpadding="0" cellspacing="0" class="Add-new-Table Company-info">
                <tr>
                    <th>
                        <span class="label-title" resourcekey="CompanyNameEnglish"></span><span class="smblreqyuired viewedit-display-none">
                            *</span>
                    </th>
                    <td>
                        <input class="cx-control CompanyNameEnglish txt required  string " maxlength="60"
                            minlength="3" type="text" />
                    </td>
                    <th class="display-none">
                        <span class="label-title" resourcekey="CompanyNameArabic"></span>
                    </th>
                    <td class="display-none">
                        <input class="cx-control CompanyNameArabic txt  string " maxlength="100" minlength="3"
                            type="text" />
                    </td>
                    <th>
                        <span class="label-title" resourcekey="Website"></span>
                      <%--  <span class="label-data" resourcekey="Http"></span>--%>
                    </th>
                    <td>
                        <input class="cx-control Website  txt string " maxlength="100" minlength="3" type="text" />
                    </td>
                    <th>
                        <span class="label-title" resourcekey="ContactType"></span><span class="smblreqyuired viewedit-display-none">
                            *</span>
                    </th>
                    <td>
                        <div class="">
                            <select class="CompanyType lst">
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                <th>
                        <span class="label-title" resourcekey="Status"></span>
                    </th>
                    <td>
                        <div class="">
                            <select class="Status lst">
                            </select>
                        </div>
                    </td>
                    <th class="display-none">
                        <span class="label-title" resourcekey="DecisionMaker"></span>
                    </th>
                    <td class="display-none">
                        <div>
                            <select class="DecisionMaker  lst">
                            </select>
                        </div>
                    </td>
                    <th class="">
                        <span class="label-title" resourcekey="SalesManAccountManager"></span>
                    </th>
                    <td class="">
                        <div class="">
                            <select class="SalesManAccountManager lst">
                            </select>
                        </div>
                    </td>
                   
                    <th class="display-none">
                        <span class="label-title" resourcekey="Abbreviation"></span>
                    </th>
                    <td class="display-none">
                        <input class="cx-control Abbreviation txt string " maxlength="60" minlength="3" type="text" />
                    </td>
                </tr>
                <tr>
                    <th class="display-none">
                        <span class="label-title" resourcekey="RegistrationNumber"></span>
                    </th>
                    <td class="display-none">
                        <input class="cx-control RegistrationNumber txt string " maxlength="60" minlength="3"
                            type="text" />
                    </td>
                    <th class="display-none">
                        <span class="label-title" resourcekey="RegistrationDate"></span>
                    </th>
                    <td class="display-none">
                        <div class="">
                            <input class="cx-control RegistrationDate date" type="text" />
                        </div>
                    </td>
                    <th class="">
                        <span class="label-title" resourcekey="Segment"></span><span class="smblreqyuired viewedit-display-none">
                            *</span>
                    </th>
                    <td class="">
                        <div class="">
                            <select class="Segment lst">
                            </select>
                        </div>
                    </td>
                     <th class="">
                        <span class="label-title" resourcekey="YearlyRevenue"></span>
                    </th>
                    <td class="">
                        <div>
                            <select class="YearlyRevenue lst">
                            </select>
                        </div>
                    </td>
                     <th class="">
                        <span class="label-title" resourcekey="EmployeeNumber"></span>
                    </th>
                    <td class="">
                        <div class="">
                            <select class="EmployeeNumber lst">
                            </select>
                        </div>
                    </td>
                </tr>
                
                <tr>
                     <th>
                        <span class="label-title" resourcekey="CountryOfOrigion"></span>
                    </th>
                    <td>
                     <span class="label-data  nowrap viewedit-display-block CountryOfOrigion"></span>
                        <div class="viewedit-display-none">
                            <select class="CountryOfOrigion lst">
                            </select>
                        </div>
                    </td>
                    <th>
                        <span class="label-title" resourcekey="InterestedIn"></span>
                    </th>
                    <td>
                        <select class="InterestedIn lst">
                        </select>
                    </td>
                    <th class="">
                        <span class="label-title" resourcekey="Source"></span><span class="smblreqyuired viewedit-display-none">
                            *</span>
                    </th>
                    <td class="">
                        <div>
                            <select class="Source lst">
                            </select>
                        </div>
                    </td>
                   
                    <th class="display-none pnlEmployeeUser">
                        <span class="label-title" resourcekey="EmployeeUser"></span><span class="smblreqyuired viewedit-display-none">
                            *</span>
                    </th>
                    <td class="display-none pnlEmployeeUser">
                        <div class="">
                            <select class="EmployeeUser  lst">
                            </select>
                        </div>
                    </td>
                    <th class="display-none pnlScoutingComments">
                        <span class="label-title" resourcekey="ScoutingComments"></span><span class="smblreqyuired viewedit-display-none">
                            *</span>
                    </th>
                    <td class="display-none pnlScoutingComments">
                        <textarea class="cx-control ScoutingComments txt string" maxlength="250" minlength="3"></textarea>
                    </td>
                </tr>
                <tr class="display-none pnlCustomerReferral">
                    <th>
                        <span class="label-title" resourcekey="CustomerReferralCompany"></span><span class="smblreqyuired viewedit-display-none">
                            *</span>
                    </th>
                    <td>
                        <div class="">
                            <select class="CustomerReferralCompany  lst">
                            </select>
                        </div>
                    </td>
                    <th>
                        <span class="label-title" resourcekey="CustomerReferralPerson"></span><span class="smblreqyuired viewedit-display-none">
                            *</span>
                    </th>
                    <td>
                        <div class="">
                            <select class="CustomerReferralPerson  lst">
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                
                <th>
                            <span class="label-title" resourcekey="FacebookPage"></span>
                        </th>
                        <td>
                            <span class="label-data nowrap viewedit-display-block FacebookPage"></span>
                            <input class="cx-control FacebookPage  txt   string viewedit-display-none" maxlength="60"
                                minlength="3" type="text" />
                        </td>
                
                       
                        <th>
                            <span class="label-title" resourcekey="LinkedInPage"></span>
                        </th>
                        <td>
                            <span class="label-data nowrap viewedit-display-block LinkedInPage"></span>
                            <input class="cx-control LinkedInPage  txt  string viewedit-display-none" maxlength="60"
                                minlength="3" type="text" />
                        </td>
                         <th>
                            <span class="label-title" resourcekey="TwitterAccount"></span>
                        </th>
                        <td>
                            <span class="label-data nowrap viewedit-display-block TwitterAccount"></span>
                            <input class="cx-control TwitterAccount  txt   string viewedit-display-none" maxlength="60"
                                minlength="3" type="text" />
                        </td>
                </tr>
            </table>
        </div>
        <div class="five-px">
        </div>
        <div class="Add-new-form email-info">
            <div class="Add-title">
                <asp:Label ID="Label3" runat="server" resourcekey="Email"></asp:Label>
            </div>
            <table border="0" cellpadding="0" cellspacing="0" class="Add-new-Table">
                <tr>
                    <th>
                        <span class="label-title" resourcekey="BusinessEmail"></span>
                    </th>
                    <td>
                        <input class="cx-control BusinessEmail email string viewedit-display-none" type="text"
                            maxlength="60" />
                    </td>
                    <th>
                        <span class="label-title" resourcekey="PersonalEmail"></span>
                    </th>
                    <td>
                        <input class="cx-control PersonalEmail email string viewedit-display-none" type="text"
                            maxlength="60" />
                    </td>
                    <th>
                    </th>
                    <td>
                    </td>
                </tr>
            </table>
        </div>
        <div class="five-px">
        </div>
        <div class="Add-new-form">
            <div class="Add-title">
                <asp:Label ID="Label1" runat="server" resourcekey="Address"></asp:Label>
            </div>
            <table border="0" cellpadding="0" cellspacing="0" class="Add-new-Table">
                <tr>
                    <th>
                        <span class="label-title" resourcekey="Country"></span>
                    </th>
                    <td>
                        <div class="viewedit-display-block">
                            <select class="Country lst">
                            </select>
                        </div>
                    </td>
                    <th>
                        <span class="label-title" resourcekey="City"></span>
                    </th>
                    <td>
                        <input class="cx-control City  txt viewedit-display-block" maxlength="60" type="text" />
                    </td>
                    <th>
                        <span class="label-title" resourcekey="StreetAddress"></span>
                    </th>
                    <td>
                        <input class="cx-control StreetAddress txt viewedit-display-block" maxlength="60"
                            type="text" />
                    </td>
                </tr>
                <tr>
                  
                    <th class="display-none">
                        <span class="label-title" resourcekey="NearBy"></span>
                    </th>
                    <td class="display-none">
                        <span class="NearBy nowrap viewedit-display-block label-data"></span>
                        <input class="cx-control NearBy txt viewedit-display-none" type="text" maxlength="60" />
                    </td>
                    <th class="display-none">
                        <span class="label-title" resourcekey="Place"></span>
                    </th>
                    <td class="display-none">
                        <span class="Place nowrap viewedit-display-block label-data"></span>
                        <input class="cx-control Place txt viewedit-display-none" type="text" maxlength="60" />
                    </td>
                    <th class="display-none">
                        <span class="label-title" resourcekey="BuildingNumber"></span>
                    </th>
                    <td class="display-none">
                        <span class="BuildingNumber nowrap viewedit-display-block label-data"></span>
                        <input class="cx-control BuildingNumber integer txt viewedit-display-none" type="text"
                            maxlength="10" />
                    </td>
                </tr>
                <tr>
                    <th class="display-none">
                        <span class="label-title" resourcekey="POBox"></span>
                    </th>
                    <td class="display-none">
                        <span class="POBox nowrap viewedit-display-block label-data"></span>
                        <input class="cx-control POBox txt viewedit-display-none" type="text" maxlength="60" />
                    </td>
                    <th class="display-none">
                        <span class="label-title" resourcekey="ZipCode"></span>
                    </th>
                    <td class="display-none">
                        <span class="ZipCode nowrap viewedit-display-block label-data"></span>
                        <input class="cx-control ZipCode txt viewedit-display-none" type="text" maxlength="60" />
                    </td>
                    <th class="display-none">
                        <span class="label-title" resourcekey="Region"></span>
                    </th>
                    <td class="display-none">
                        <span class="Region nowrap viewedit-display-block label-data"></span>
                        <input class="cx-control Region txt viewedit-display-none" type="text" maxlength="60" />
                    </td>
                </tr>
                <tr>
                    <th class="display-none">
                        <span class="label-title" resourcekey="Area"></span>
                    </th>
                    <td class="display-none">
                        <span class="Area nowrap viewedit-display-block label-data"></span>
                        <input class="cx-control Area txt viewedit-display-none" type="text" maxlength="60" />
                    </td>
                </tr>
            </table>
        </div>
        <div class="five-px">
        </div>
        <div class="Add-new-form">
            <div class="Add-title">
                <asp:Label ID="Label2" runat="server" resourcekey="Phone"></asp:Label>
            </div>
            <table border="0" cellpadding="0" cellspacing="0" class="Add-new-Table">
                <tr>
                    <th>
                        <span class="label-title" resourcekey="Home"></span>
                    </th>
                    <td>
                        <input type="text" class="cx-control PcountryCode CountryCode txt country-num phone-format"
                            maxlength="6" />
                        <input type="text" class="cx-control PareaCode AreaCode txt city-num phone-format" maxlength="6" />
                        <input type="text" class="cx-control PphoneNumber phone-num txt phone-format" maxlength="8" />
                    </td>
                    <th>
                        <span class="label-title" resourcekey="Mobile"></span>
                    </th>
                    <td>
                        <input type="text" class="cx-control McountryCode  country-num phone-format" maxlength="6" />
                        <input type="text" class="cx-control MareaCode  city-num phone-format" maxlength="6" />
                        <input type="text" class="cx-control MphoneNumber  phone-num phone-format" maxlength="8" />
                    </td>
                    <th>
                        <span class="label-title" resourcekey="Fax"></span>
                    </th>
                    <td>
                        <input class="cx-control ScountryCode country-num phone-format" type="text" maxlength="6"
                            minlength="3" />
                        <input class="cx-control SareaCode AreaCode city-num phone-format" type="text" maxlength="6"
                            minlength="6" />
                        <input class="cx-control SphoneNumber phone-num phone-format" maxlength="8" type="text" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
