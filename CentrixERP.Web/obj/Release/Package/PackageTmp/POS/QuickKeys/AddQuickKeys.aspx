<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddQuickKeys.aspx.cs" MasterPageFile="~/Common/MasterPage/ERPSite.Master"
    Inherits="Centrix.POS.Server.Web.Server.QuickKeys.AddQuickKeys" %>

<asp:Content runat="server" ContentPlaceHolderID="PageHeader" ID="PageHeader">
    <link href="CSS/QuickKey_<%=Lang %>.css?v=<%=Centrix_Version %>" rel="stylesheet"
        type="text/css" />
    <script src="../../Common/JScript/CommonTabs.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <script src="JS/AddQuickKeys.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <script type="text/javascript">
        addQuickKeysMode = true;           
    </script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="PageBody" ID="PageBody">
    <div class="StoreSync-info-container">
        <div class="Add-title">
            <span class="HName" resourcekey="HName"></span><span class="QuickKey"></span>
            <div class="Action-div">
                <ul>
                    <li class="edit-pnl viewedit-display-block"><span class="ActionEdit-link"></span><a
                        class="ActionEdit-link"><span resourcekey="Edit"></span></a></li>
                    <li class="save-cancel-pnl VieweditDisplayNone"><span class="ActionSave-link"></span>
                        <a class="ActionSave-link"><span resourcekey="Save"></span></a></li>
                    <li class="save-cancel-pnl VieweditDisplayNone"><span class="ActionCancel-link"></span>
                        <a class="ActionCancel-link"><span resourcekey="Cancel"></span></a></li>
                    <li class="Back-pnl"><span class="ActionBack-link"></span><a class="ActionBack-link save-title">
                        <span resourcekey="Back"></span></a></li>
                </ul>
            </div>
        </div>
        <div class="QuickKey-selection VieweditDisplayNone">
            <table border="0" cellpadding="0" cellspacing="0" class="Data-Table2col add-QuickKeys-info">
                <tr>
                    <th>
                        <span class="qk-span label-title" resourcekey="Name"></span><span class="smblreqyuired VieweditDisplayNone">
                            *</span>
                    </th>
                    <td>
                        <input type="text" class="cx-control txt required string Name" />
                    </td>
                </tr>
                <tr>
                    <th>
                        <span class="qk-span label-title" resourcekey="Search"></span>
                    </th>
                    <td>
                        <input type="text" class="item-search" />
                    </td>
                </tr>
                <tr>
                    <th>
                        <span class="qk-span" resourcekey="Items"></span>
                    </th>
                    <td>
                        <div class="itemSelection">
                            <div>
                                <select class="lstItems" multiple="multiple" size="20" style="min-width: 170px; height: 370px;">
                                </select>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="MoveItems">
                            <input type="button" class="QK-saveBtn add-item"  />
                            <%--  <input type="button" class="button-cancel add-all" value=">>" />--%>
                        </div>
                    </td>
                </tr>
                <%--   <tr>
                    <td colspan="2">
                        <input type="button" class="QK-saveBtn" value="save" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="button" class="QK-cancelBtn" value="cancel">
                    </td>
                </tr>--%>
            </table>
        </div>
        <!---->
        <!---->
        <div class="ItemsContainer fullwidth">
            <div class="ItemsTabs">
                <div class="AddNewTab VieweditDisplayNone">
                </div>
                <ul class="ItemsTabs-ul">
                </ul>
            </div>
            <div class="TabsItemsContainer">
            </div>
        </div>
        <!---->
    </div>
    <div class="confermitaion-msg-box cmb-QK">
        <div class="cmb-title">
            <span resourcekey="Addnewtab"></span>
        </div>
        <div class="cmb-desc">
            <span class="cmb-desc-qk-span" resourcekey="TabName"></span>
            <input type="text" class="txtName" /></div>
        <div class="cmb-controls">
            <input type="button"  class="button2 confirm-button">&nbsp;<input type="button"
                class="button-cancel cancel-qk"></div>
    </div>
</asp:Content>
