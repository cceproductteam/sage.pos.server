var ContactServiceURL;
var ContactViewItemTemplate;
var GenderAC;
var ContactTypeAC;
 var ItemListTemplate;
 var AddItemTemplate;
var addContactMode = false;
var typeContact;
var typeGender;
var ContactIdmode = 0;
function InnerPage_Load() {
    setSelectedMenuItem('.pos-icon');
    ContactServiceURL = systemConfig.ApplicationURL_Common + systemConfig.ContactServiceURL;
    currentEntity = 3188;



    //Advanced search Data
            mKey = 'Contact';//replace it with entity modual key
            InnerPage_Load2();
            $('.QuickSearch-holder').show();
            LoadSearchForm2(mKey, false);
    //Advanced search Data


    setPageTitle(getResource("Contact"));
    debugger
    setAddLinkAction(getResource("AddContact", 'add-new-item-large'));

    GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.AddContactItemTemplate, null, 'AddItemTemplate');
    GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.ContactListItemTemplate, loadData, 'ItemListTemplate');
    
    $('.result-block').live("click", function () {
        ContactId = $(this).parent().attr('id');
    });

    deletedEntityCallBack = function () {
        removeDeletedEntity();
    };

    updatedEntityCallBack = function () {
        getEntityData(selectedEntityId);
    };
    
$('li:.Communication-tab').live('click', function () {
            checkPermissions(this, systemEntities.TaskId, -1, $('.Action-div'), function (template) {
                getCommunication(selectedEntityId, -1 , selectedEntityId, ContactName);
            });
        });$('li:.note-tabs').live('click', function () {
            NoteTemplate = $(NoteTemplate);
            checkPermissions(this, systemEntities.NoteEntityId, -1, NoteTemplate, function (template) {
                getNotes(systemEntities.ContactEntityId,selectedEntityId);
            });
        });


}

function loadData(data) {
    debugger
    GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.ContactListViewItemTemplate, function () {
        checkPermissions(this, currentEntity, -1, ContactViewItemTemplate, function (returnedData) {
            ContactViewItemTemplate = returnedData;
            loadEntityServiceURL = ContactServiceURL + "FindByIdLite";
            entityItemListTemplate = data;
            entityItemViewTemplate = returnedData;
            findAllEntityServiceURL = ContactServiceURL + "FindAllLiteFirst";
            deleteEntityServiceURL = ContactServiceURL + "Delete";
            selectEntityCallBack = getContactInfo;
            loadDefaultEntityOptions();
        });
    }, 'ContactViewItemTemplate');

}


function initControls(template, isAdd) {
debugger
if (!template)
    template = $('.TabbedPanels');

GenderAC = fillDataTypeContentList(template.find('select:.Gender'), systemConfig.dataTypes.Gender, true ,  false);


ContactTypeAC = fillDataTypeContentList(template.find('select:.ContactType'), systemConfig.dataTypes.CustomerType, true, false);
typeContact = ContactTypeAC.get_Item();
typeGender = GenderAC.get_Item();



    if (!isAdd)
        $('.historyUpdated').append(HistoryUpdatedListViewTemplate.clone());

  
    LoadDatePicker(template.find('input:.date'), 'dd-mm-yy');
    template.find('.numeric').autoNumeric();
    preventInputChars(template.find('input:.integer'));


//    if ($('.ContactType').find(":selected").val() == 1) {

//        $('.CompanyName').addClass('ui-autocomplete-disabled')
    //    }

   
    ContactTypeAC.onChange(function () {
        debugger

        typeContact = ContactTypeAC.get_Item();

        debugger
        if (typeContact != null) {
            if (typeContact.value == 1) {
               
           
                template.find('.Companyinput').hide();
                template.find('.CompanyName').removeClass("required");
                template.find('.persontr').show();
                template.find('.Gendersection').show();
                template.find('.FirstName').addClass("required");
                template.find('.LastName').addClass("required");
                GenderAC.set_Required(true);


                
            }
            else {
                //                template.find('.FirstName').css('display', 'none');
                //                template.find('.LastName').css('display', 'none');
                //                template.find('.CompanyName').css('display', 'block');
                //             
                //                template.find('.r2').css('display', 'none');
                //                template.find('.CompanyNamelabel').css('display', 'block');
                //                template.find('.FirstNamelabel').css('display', 'none');
                //                template.find('.LastNamelabel').css('display', 'none');
//                //                template.find('.Genderlbl').css('display', 'none');
//                $('.persontr').css('display', 'none');
//                $('.Gendersection').css('display', 'none');
//                $('.Companyinput').css('display', 'block');


                template.find('.persontr').hide();
                template.find('.Gendersection').hide();
                template.find('.Companyinput').show();
                template.find('.CompanyName').addClass("required");
                template.find('.FirstName').removeClass("required");
                template.find('.LastName').removeClass("required");
              
                GenderAC.set_Required(false);

            }

        }
    });

}

function initAddControls(template) {
debugger
    initControls(template, true);
}


function getContactInfo(item) {
    debugger
    if (item != null) {
   
                 $('.FirstName').text(Truncate(item.FirstName, 15));
                 $('.Email').text(Truncate(item.Email, 15));
                 $('.PhoneNumber').text(Truncate(item.PhoneNumber, 15));

                 

                 debugger
                     if (item.ContactTypeId == 1) {
                         //            template.find('.CompanyName').hide();
                    
                         $('.Companyinput').css('display', 'none');
//                         $('.CompanyName').css('display', 'none');

//                         $('.CompanyNamelabel').css('display', 'none');
////                         $('.FirstName').css('display', 'block');
////                         $('.LastName').css('display', 'block');

//                         $('.FirstNamelabel').css('display', 'block');
//                         $('.LastNamelabel').css('display', 'block');
//                         $('.Genderlbl').css('display', 'block');
                     }
                     else {

                         $('.persontr').css('display', 'none');
                         $('.Gendersection').css('display', 'none');
//                         $('.FirstName').css('display', 'none');
//                        $('.LastName').css('display', 'none');
//                         $('.CompanyName').css('display', 'block');

//                       $('.r2').css('display', 'none');
//                       $('.CompanyNamelabel').css('display', 'block');
//                        $('.FirstNamelabel').css('display', 'none');
//                         $('.LastNamelabel').css('display', 'none');
//                         $('.Genderlbl').css('display', 'none');

                     }

                 

        

    }
    if ($('.result-block').length > 0 && (getQSKey('fdb') > 0) && isFirstTimeAdd) {
        $('.add-entity-button').first().click();
    }

}


function setListItemTemplateValue(entityTemplate, entityItem) {

    debugger

    entityTemplate.find('.result-data1 label').text(Truncate((entityItem.ContactTypeId == 1 ? entityItem.FirstName + " " + entityItem.LastName : entityItem.CompanyName), 20));
     entityTemplate.find('.result-data2 label').textFormat(Truncate(entityItem.Email,20));
     entityTemplate.find('.result-data3 label').textFormat(Truncate(entityItem.PhoneNumber, 20));

     return entityTemplate;
 }

function callBackAfterList() {
    debugger
     if ($('.result-block').length <= 0 && (getQSKey('fdb') > 0) && isFirstTimeAdd) {
         {
             $('.add-entity-button').first().click();
         }
     }
 }


function setListItemServiceData(keyword, pageNumber, resultCount) {
    debugger
     if (!QuickSearch) {
         return formatString('{ keyword: "{0}", page:{1}, resultCount:{2}}', keyword, pageNumber, resultCount);
     }
     else {
         return formatString('{mKey:"{0}",SearchCriteriaObj:"{1}",SortType:{2},SortFields:"{3}",TotalRecords:{4},PageNumber:{5}}', mKey, SearchCriteriaObj, SortType, SortFields, resultCount, pageNumber);
     }
 }



function SaveEntity(isAdd, template) {
    debugger
     $('.validation').remove();
     var controlTemplate;
     var validForm = true;  

     if (isAdd)
         controlTemplate = $('.add-Contact-info');
     else
         controlTemplate = $('.Contact-info');

     

     if (!saveForm(controlTemplate))
         validForm = false;

     if (!validForm) {
         return false;
         hideLoading();
     }

     addContactMode = isAdd;
     var ContactId = -1;
     var lastUpdatedDate = null;

     if (addContactMode) {
         showLoading('.add-new-item');
     }
     else {
         debugger
         showLoading('.Result-info-container');
         lastUpdatedDate = null;
     }

     var ContactObj;
     CheckMode();
     if (typeContact.value == 1) {

         ContactObj = {

             FirstName: controlTemplate.find('input.FirstName').val(),
             LastName: controlTemplate.find('input.LastName').val(),
             Email: controlTemplate.find('input.Email').val(),
             Gender: GenderAC.get_ItemValue(),
             PhoneNumber: controlTemplate.find('input.PhoneNumber').val(),
             CompanyName: '',
             ContactType: ContactTypeAC.get_ItemValue(),
             ContactId: ContactIdmode
         };
     }

     else {
       ContactObj = {
            FirstName: '',
             LastName: '',
             Email: controlTemplate.find('input.Email').val(),
             Gender: -1 ,
             PhoneNumber: controlTemplate.find('input.PhoneNumber').val(),
             CompanyName: controlTemplate.find('input.CompanyName').val(),
             ContactType: ContactTypeAC.get_ItemValue(),
             ContactId: ContactIdmode
         };
     }

    debugger
     var data = formatString('{id:{0}, ContactInfo:"{1}"}', UserId, escape(JSON.stringify(ContactObj)));

     if (ContactObj.ContactId > 0)
         url = ContactServiceURL + "Edit";
     else
         url = ContactServiceURL + "Add";

     post(url, data, saveSuccess, saveError);
 }

 function saveSuccess(returnedValue) {
     debugger
     if (returnedValue.statusCode.Code == 501) {
         
         hideLoading();
         return false;
     }
     else if (returnedValue.statusCode.Code == 0) {
         hideLoading();
         if (returnedValue.statusCode.message == 'validation' && returnedValue.result) {
             filterError(returnedValue.result);
         }
         showStatusMsg(getResource("FaildToSave"));
         return false;
     }
     if (!addContactMode) {
         showStatusMsg(getResource("savedsuccessfully"));
         hideLoading();
         loadEntityData(loadEntityServiceURL, entityItemViewTemplate, selectedEntityId, getContactInfo);

     }
     else {
         hideLoading();
         addContactMode = false;
         $('.add-new-item').hide();
         $('.add-new-item').empty();
         appendNewListRow(returnedValue.result);
         showStatusMsg(getResource("savedsuccessfully"));
     }
 }

 function saveError(ex) {
     hideLoading();
     handleLockEntityError(ex, deletedEntityCallBack, updatedEntityCallBack);
 }




 function CheckMode() {
     debugger
     if ($('.add-new-item').css("display") != "block")
         ContactIdmode = selectedEntityId;

     else
         ContactIdmode = 0;

 }



 function filterError(errors) {
     var allMsg = '';
     $('.validation').remove();
     $.each(errors, function (index, error) {
         var template = $('.Contact-info');
         if (error.GlobalMessage) {
             allMsg += '<span>' + error.Message + '<br/></span>';
         }
        else if (error.ControlName == 'FirstName') {
addPopupMessage(template.find('input.FirstName'),getResource(error.ResourceKey));
}
else if (error.ControlName == 'LastName') {
addPopupMessage(template.find('input.LastName'),getResource(error.ResourceKey));
}
else if (error.ControlName == 'Email') {
addPopupMessage(template.find('input.Email'),getResource(error.ResourceKey));
}
else if (error.ControlName == 'Gender') {
addPopupMessage(template.find('input.Gender'),getResource(error.ResourceKey));
}
else if (error.ControlName == 'PhoneNumber') {
addPopupMessage(template.find('input.PhoneNumber'),getResource(error.ResourceKey));
}
else if (error.ControlName == 'CompanyName') {
addPopupMessage(template.find('input.CompanyName'),getResource(error.ResourceKey));
}
else if (error.ControlName == 'ContactType') {
addPopupMessage(template.find('input.ContactType'),getResource(error.ResourceKey));
}

     });

     hideLoading();
     if (allMsg.trim()) {
         showOkayMsg('Validation error', allMsg.trim());
     }
 }









