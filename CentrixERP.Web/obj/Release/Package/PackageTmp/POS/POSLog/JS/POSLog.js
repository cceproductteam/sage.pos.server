﻿var WalkInId = -1, UserId = -1;
var identityIndex = 0;
var SyncLogItemList = new Array();
var SyncLogServiceURL;
var SyncLogtemplate
var indexToUpdate = -1;
var editMood = false;
//var SyncLogItemTemplate;
var lastUpdatedDate;
var LastUpdatedDate = null;
var isEmptyList = false;
var StoreAC, EntityAC;

function InnerPage_Load() {
    setSelectedMenuItem('.pos-icon');
    $('.search-box').hide();
   // setSelectedMenuItem('.WalkIns-icon');
    currentEntity = systemEntities.WalkInId;
    SyncLogServiceURL = systemConfig.ApplicationURL_Common + systemConfig.PosSyncLogWebService;
    $('.add-entity-button').remove();
    checkPermissions(this, systemEntities.WalkInId, -1, $('.Action-div'), function (template) { initControls(); });
    
    setPageTitle(getResource("PosSyncLog"));
    deletedEntityCallBack = function () {
        removeDeletedEntity();
    };

    updatedEntityCallBack = function () {
        getEntityData(selectedEntityId);
    };

}

function initControls() {
    hideDefaultLinks();
    StoreAC = $('select:.Store').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.StoreServiceURL + 'FindAllLite', required: false, multiSelect: true });
    EntityAC = $('select:.Entity').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.AdvancedSearchWebServiceURL + 'FindAllEntity', required: false, multiSelect: true });
    EntityAC.set_Args(formatString('{ PosEntity:{0}}', true));
   // fillDateList($('select:.lstFTime,select:.lstTime'));
    //$('select:.lstFTime,select:.lstTime').empty();
//        var emptyTime = $('<option>').html('');
//        $('select:.lstFTime,select:.lstTime').append(emptyTime);


    $('input:.integerMask').autoNumeric({ aPad: false });
    SyncLogtemplate = $('.SyncLogItemRow');
    $('.SyncLogItemRow').remove();
    LoadDatePicker('input:.SyncDateFrom', 'dd-mm-yy');
    LoadDatePicker('input:.SyncDateTo', 'dd-mm-yy');
   // LoadDatePicker('input:.WalkInDateTo', 'dd-mm-yy');

//    var now = new Date();
//    $('input:.WalkInDate').val(getDate('dd-mm-yy', now));
    

   $('.ActionReset-link').click(function () {
       clearContent();
       $('.delete-link').hide();
   });

   $('.lnkSearch').click(function () {
       
       $('.validation').remove();
       $('.cx-control-red-border').removeClass('cx-control-red-border');
       var walkInDateFrom = $('input.SyncDateFrom').val() != "" ? getDate("dd-mm-yy", getDotNetDateFormat($('input.SyncDateFrom').val())) : "";
       var walkInDateTo = $('input.SyncDateTo').val() != "" ? getDate("dd-mm-yy", getDotNetDateFormat($('input.SyncDateTo').val())) : "";
       if (DateDiff(walkInDateTo, walkInDateFrom) > 0 && walkInDateFrom != "" && walkInDateTo != "") {
           addPopupMessage($('input.WalkInDateFrom'), getResource("WalkInDateError"));
           hideLoading();
           return false;
       }
       
       findEntityItem(true);
   });

   
   findEntityItem(false);
}

function DateDiff(date1, date2) {
    var Startdate = new Date(date1.replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
    var EndDate = new Date(date2.replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
    return EndDate.getTime() - Startdate.getTime();
}



function hideDefaultLinks() {
    $('.Action-div').show();
    $('.search-div').show();
    $('.ActionSave-link').hide();
    $('.delete-link').hide();
    $('.ActionCancel-link').hide();
    $('.ActionEdit-link').Show();
    $('.SyncLog-info').hide();
    $('.delete-link').hide();
    //$('.clear-link').hide();
    
}

function ShowDefultLiks() {
    $('.ActionSave-link').Show();
    $('.delete-link').hide();
    $('.ActionCancel-link').Show();
    $('.ActionEdit-link').hide();
    $('.SyncLog-info').Show();
   // $('.clear-link').Show();
    $('.search-div').hide();
}

function SyncLogItem() { }
//function SyncLogItem() {
//    this.ItemIdentity = ++identityIndex;
//    this.WalkInId = -1;
//    this.NumberOfWalkIn = -1;
//    this.WalkInDate = '';
//    this.Comments = '';
//    this.CreatedBy = -1;
//}

//

function clearContent() {

//    indexToUpdate = -1;
//    WalkInId = -1;
    $('textarea:.Comments').val('');
    $('input:.SyncDateFrom ').val('');
    $('input:.SyncDateTo').val('');
    $('input:.Message').val('');
    EntityAC.clear();
    StoreAC.clear();
}

function FillItemData(Item, template) {
    template = mapEntityInfoToControls(Item, template);
    template.css('cursor', 'pointer');
    AddRowClass(template);
    $('.ItemList-info').append(template);
}

function AddRowClass(template) {

    if ($('.ItemList-info tr').length % 2) {
        template.find('td').addClass('td-r1-data-table');
    }
    else {
        template.find('td').addClass('td-r2-data-table');
    }
}


function findEntityItem(isSearchMood) {
   
    var data;
    if (isSearchMood) {
        //var NumberOfWalkIn = $('input:.SNumberOfWalkIn').val() != '' ? getNumericValue('input:.SNumberOfWalkIn') : -1;
        var DateFrom = $('input:.SyncDateFrom').val() != '' ? getDate("mm-dd-yy", getDotNetDateFormat($('input:.SyncDateFrom').val())) : '';
        var DateTo = $('input:.SyncDateTo').val() != '' ? getDate("mm-dd-yy", getDotNetDateFormat($('input:.SyncDateTo').val())) : '';
        var StoreIds = StoreAC.get_ItemsIds();
        var EntityIds = EntityAC.get_ItemsIds();
        var status;
        var Message = $('input:.Message').val();
        var success = $('input:.Success').is(':checked');
        var faild = $('input:.Faild').is(':checked');
        if ((success && faild)||(!success && !faild))
            status = 3;
        else if (success && !faild)
            status = 1;
        else if (!success && faild)
            status = 2;
        //var WalkInTime = $('select:.lstFTime').val() != '--:--' ? $('select:.lstFTime').val() : '';


       // data = formatString('{argsCriteria:"{StoreIds: "{0}",SyncEntityIDs:"{1}"}"}', "23,24","115,113");
        data = formatString('{StoreIds:"{0}",SyncEntityIDs:"{1}",DateFrom:"{2}",DateTo:"{3}",status:{4},Message:"{5}"}', StoreIds, EntityIds, DateFrom, DateTo, status, Message);
    }
    else { data = formatString('{StoreIds:"{0}",SyncEntityIDs:"{1}",DateFrom:"{2}",DateTo:"{3}",status:{4},Message:"{5}"}', '', '', '', '', 3,''); }
   // data = formatString('{argsCriteria:"{StoreIds: "{0}",SyncEntityIDs:"{1}"}"}', "23,24", "115,113");

    //data = formatString('{argsCriteria:"{NumberOfWalkIn: {0}}",StoreIds:"{1}",SyncEntityIDs:"{2}"}', '', "23,24", "115,113");
   // data = formatString('{StoreIds:"{0}",SyncEntityIDs:"{1}",DateFrom:"{2}",DateTo:"{3}",status:{4}}', StoreIds, EntityIds, DateFrom, DateTo,false);
    
   // data = formatString(data, '');
    var url = SyncLogServiceURL + "FindPosSyncLogAllLite";
    showLoading($('.TabbedPanelsContent'));
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: url,
        data: data,
        datatype: "json",
        success: function (data) {
            hideLoading();
            var returnedValue = eval("(" + data.d + ")").result;
            SyncLogItemList = new Array();
            identityIndex = 0
            if (returnedValue != null) {
                $('.DragDropBackground').hide();
                isEmptyList = false;
                LastUpdatedDate = returnedValue[0].UpdatedDate
                $('.SyncLogItemRow').remove();
                $.each(returnedValue, function (index, item) {

                    var template = $(SyncLogtemplate).clone().click(function () {

                        var d = $(this).children().find('.WalkInDate').text().split("-");
                        var date = d[1] + '-' + d[0] + '-' + d[2];
                        $('.selected').removeClass('selected');
                        $(this).addClass('selected');
//                        $('input:.NumberOfWalkIn').val($(this).children().find('.NumberOfWalkIn').text());
//                        $('textarea:.Comments').val($(this).children().find('.Comments').text());
//                        $('input:.WalkInDate').val($(this).children().find('.WalkInDate').text());
//                        $('select:.lstTime').val($(this).children().find('.WalkInTime').text());
//                        WalkInId = $(this).children().find('.WalkInId').text();
//                        indexToUpdate = Number($(this).children().find('.ItemIdentity').text()) - 1;
//                        if (editMood)
//                            $('.delete-link').Show();
                    });
                    if (item != null) {

                        var syncLogItem = new SyncLogItem();
                        syncLogItem.NumberOfWalkIn = item.NumberOfWalkIn;
                        syncLogItem.Status = item.Status ? getResource("Success") : getResource("Faild");
                        syncLogItem.Store = item.Store;
                        syncLogItem.Message = item.Message;
                        syncLogItem.CreatedBy = item.CreatedBy;
                        syncLogItem.CreatedDate = item.CreatedDate;
                        syncLogItem.SyncDate = item.SyncDate;
                        syncLogItem.StoreName = item.StoreName;
                        syncLogItem.EntityName = item.EntityName;
                        //SyncLogItem.WalkInId = item.WalkInId;
                        SyncLogItemList.push(syncLogItem);

                        FillItemData(syncLogItem, template);
                    }

                });
            }
            else {
                isEmptyList = true;
                $('.DragDropBackground').show();
                $('.SyncLogItemRow').remove();
            }

        },
        error: function (ex) {
        }
    });
}



