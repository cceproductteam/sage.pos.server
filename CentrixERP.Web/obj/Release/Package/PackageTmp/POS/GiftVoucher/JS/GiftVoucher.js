var GiftVoucherServiceURL;
var GiftVoucherViewItemTemplate;

var ItemListTemplate;
var GiftVoucherId = selectedEntityId;
var lastUpdatedDate = null;
var addGiftVoucherMode = false;

function InnerPage_Load() {
    setSelectedMenuItem('.pos-icon');
    GiftVoucherServiceURL = systemConfig.ApplicationURL_Common + systemConfig.GiftVoucherServiceURL;
    currentEntity = systemEntities.GiftVoucherEntityId;
    //Advanced search Data
    mKey = 'GiftVoucher'; //replace it with entity modual key
    InnerPage_Load2();
    $('.QuickSearch-holder').show();
    LoadSearchForm2(mKey, false);
    //Advanced search Data

    setPageTitle(getResource("GiftVoucher"));
    setAddLinkAction(getResource("AddGiftVoucher"), 'add-new-item-large');

    GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.AddGiftVoucherItemTemplate, function () {
        GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.GiftVoucherListItemTemplate, loadData, 'ItemListTemplate');
    }, 'AddItemTemplate');

    $('.result-block').live("click", function () {
        GiftVoucherId = $(this).attr('id');
    });

    deletedEntityCallBack = function () {
        removeDeletedEntity();
    };

    updatedEntityCallBack = function () {
        getEntityData(selectedEntityId);
    };

}

function loadData(data) {

    GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.GiftVoucherListViewItemTemplate, function () {
        checkPermissions(this, systemEntities.GiftVoucherEntityId, -1, GiftVoucherViewItemTemplate, function (returnedData) {
            GiftVoucherViewItemTemplate = returnedData;
            loadEntityServiceURL = GiftVoucherServiceURL + "FindByIdLite";
            entityItemListTemplate = data;
            entityItemViewTemplate = returnedData;
            findAllEntityServiceURL = GiftVoucherServiceURL + "FindAllLite";
            deleteEntityServiceURL = GiftVoucherServiceURL + "Delete";
            selectEntityCallBack = getGiftVoucherInfo;
            listEntityCallBack = callBackAfterList;
            loadDefaultEntityOptions();
        });
    }, 'GiftVoucherViewItemTemplate');
}

function initControls(template, isAdd) {
    if (!template) template = $('.TabbedPanels');
    if (!isAdd)
        $('.historyUpdated').append(HistoryUpdatedListViewTemplate.clone());
    LoadDatePicker(template.find('input:.date'), 'dd-mm-yy');
    template.find('.numeric').autoNumeric();
    preventInputChars(template.find('input:.integer'));
}

function initAddControls(template) {
    initControls(template, true);
}

function getGiftVoucherInfo(item) {

    if ($('.result-block').length > 0 && getQSKey('fdb') > 0 && isFirstTimeAdd) {
        $('.add-entity-button').first().click();
    }
    if (item != null) {
    } 
}

 function setListItemTemplateValue(entityTemplate, entityItem) {
     entityTemplate.find('.result-data1 label').text(Truncate(entityItem.GiftNumber, 20));
     entityTemplate.find('.result-data2 label').text(Truncate(entityItem.GiftName, 20));
     entityTemplate.find('.result-data3 label').textFormat(entityItem.GiftAmount);
     entityTemplate.find('.result-data4 label').text(getDate('dd-mm-yy hh:mm tt', formatDate(entityItem.ExpireDate)));
     return entityTemplate;
 }

 function setListItemServiceData(keyword, pageNumber, resultCount) {
     if (!QuickSearch) {
         return formatString('{ keyword: "{0}", page:{1}, resultCount:{2} ,argsCriteria:"{ GiftVoucherId:{3}}"}', keyword, pageNumber, resultCount, filterEntityId);
     }
     else {
         return formatString('{mKey:"{0}",SearchCriteriaObj:"{1}",SortType:{2},SortFields:"{3}",TotalRecords:{4},PageNumber:{5}}', mKey, SearchCriteriaObj, SortType, SortFields, resultCount, pageNumber);
     }
 }



 function SaveEntity(isAdd, template) {
     var controlTemplate;
     var valid = true;

     if (isAdd) {
         controlTemplate = $('.add-GiftVoucher-info');
         showLoading('.add-new-item');
     }
     else {
         controlTemplate = $('.GiftVoucher-info');
         showLoading('.Result-info-container');
     }
     
     if (!saveForm(controlTemplate)) {
         hideLoading();
         valid = false;
     }

     if (controlTemplate.find('input.ExpireDate').val() != "") {
         ExpireDate = getDate("dd-mm-yy", getDotNetDateFormat(controlTemplate.find('input.ExpireDate').val()));
         if (DateDiff(ExpireDate, getDate("dd-mm-yy", new Date())) > 0) {
             addPopupMessage(controlTemplate.find('input.ExpireDate'), getResource("ValidateExpireDate"));
             hideLoading();
             valid = false;
         }
     }

     addGiftVoucherMode = isAdd;
     if (!valid) {
         hideLoading();
         return false;
     }

     GiftVoucherId = -1;

     if (!isAdd) {
         GiftVoucherId = selectedEntityId;
         lastUpdatedDate = getEntityLockDateFormat(currentEntityLastUpdatedDate);
     }


     var GiftVoucherObj = {
         GiftNumber: controlTemplate.find('input.GiftNumber').val(),
         GiftName: controlTemplate.find('input.GiftName').val(),
         GiftAmount: controlTemplate.find('input.GiftAmount').autoNumericGet(),
         ExpireDate: getDotNetDateFormat(controlTemplate.find('input.ExpireDate').val())
     };
     var data = formatString('{id:{0}, GiftVoucherInfo:"{1}"}', isAdd ? -1 : selectedEntityId, escape(JSON.stringify(GiftVoucherObj)));

     if (GiftVoucherId > 0)
         url = GiftVoucherServiceURL + "Edit";
     else
         url = GiftVoucherServiceURL + "Add";

     post(url, data, function (returnedValue) { saveSuccess(template, returnedValue); }, saveError, getEntitylockAjaxHeaders(lastUpdatedDate));
 }

 function saveSuccess(template,returnedValue) {
     if (returnedValue.statusCode.Code == 501) {
         addPopupMessage(template.find('input.GiftNumber'), getResource("DublicatedGiftNumber"));
         hideLoading();
         return false;
     }
     else if (returnedValue.statusCode.Code == 0) {
         hideLoading();
         if (returnedValue.statusCode.message == 'validation' && returnedValue.result) {
             filterError(returnedValue.result,template);
         }
         showStatusMsg(getResource("FaildToSave"));
         return false;
     }
     if (!addGiftVoucherMode) {
         showStatusMsg(getResource("savedsuccessfully"));
         hideLoading();
         loadEntityData(loadEntityServiceURL, entityItemViewTemplate, selectedEntityId, getGiftVoucherInfo);
     }
     else {
         hideLoading();
         addGiftVoucherMode = false;
         $('.add-new-item').hide();
         $('.add-new-item').empty();
         appendNewListRow(returnedValue.result);
         showStatusMsg(getResource("savedsuccessfully"));
     }
 }

 function saveError(ex) {
     hideLoading();
     handleLockEntityError(ex, deletedEntityCallBack, updatedEntityCallBack);
 }



 function filterError(errors, template) {
     var allMsg = '';
     template.find('.validation').remove();
     $.each(errors, function (index, error) {
         if (error.GlobalMessage) {
             allMsg += '<span>' + error.Message + '<br/></span>';
         }
         else if (error.ControlName == 'GiftNumber') {
             addPopupMessage(template.find('input.GiftNumber'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'GiftName') {
             addPopupMessage(template.find('input.GiftName'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'GiftAmount') {
             addPopupMessage(template.find('input.GiftAmount'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'ExpireDate') {
             addPopupMessage(template.find('input.ExpireDate'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'IsUsed') {
             addPopupMessage(template.find('input.IsUsed'), getResource(error.ResourceKey));
         }

     });

     hideLoading();
     if (allMsg.trim()) {
         showOkayMsg('Validation error', allMsg.trim());
     }
 }

 function callBackAfterList() {
     if ($('.result-block').length <= 0 && getQSKey('fdb') > 0 && isFirstTimeAdd) {
         $('.add-entity-button').first().click();
     }
 }


