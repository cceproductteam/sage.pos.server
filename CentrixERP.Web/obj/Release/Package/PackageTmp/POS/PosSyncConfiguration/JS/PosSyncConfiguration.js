var PosSyncConfigurationServiceURL;
var PosSyncConfigurationViewItemTemplate;
var PosSyncTime1AC;
var PosSyncTime2AC;
var PosSyncTime3AC;
var PosSyncTime4AC;
var PosSyncTime5AC;
var AccpacSyncTime1AC;
var AccpacSyncTime2AC;
var AccpacSyncTime3AC;
var AccpacSyncTime4AC;
var AccpacSyncTime5AC;

var ItemListTemplate;
var PosSyncConfigurationId = selectedEntityId;
var lastUpdatedDate = null;
var addPosSyncConfigurationMode = false;

function InnerPage_Load() {
    $('#demo2,.TabbedPanelsTabGroup,.TMS-right').hide();

    setSelectedMenuItem('.pos-icon');
    PosSyncConfigurationServiceURL = systemConfig.ApplicationURL_Common + systemConfig.PosSyncConfigurationServiceURL;
    currentEntity = systemEntities.PosSyncConfigurationEntityId;
    //Advanced search Data
    //        mKey = 'PosSyncConfiguration';//replace it with entity modual key
    //        InnerPage_Load2();
    //        $('.QuickSearch-holder').show();
    // LoadSearchForm2(mKey, false);
    //Advanced search Data

    setPageTitle(getResource("PosSyncConfiguration"));
    setAddLinkAction(getResource("AddPosSyncConfiguration"), 'add-new-item');

    GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.AddPosSyncConfigurationItemTemplate, function () {
        GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.PosSyncConfigurationListItemTemplate, loadData, 'ItemListTemplate');
    }, 'AddItemTemplate');

    $('.result-block').live("click", function () {
        PosSyncConfigurationId = $(this).attr('id');
    });

    deletedEntityCallBack = function () {
        removeDeletedEntity();
    };

    updatedEntityCallBack = function () {
        getEntityData(selectedEntityId);
    };
    
   

}

function loadData(data) {
   
    GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.PosSyncConfigurationListViewItemTemplate, function () {
        checkPermissions(this, systemEntities.PosSyncConfigurationEntityId, -1, PosSyncConfigurationViewItemTemplate, function (returnedData) {
            PosSyncConfigurationViewItemTemplate = returnedData;
            loadEntityServiceURL = PosSyncConfigurationServiceURL + "FindByIdLite";
            entityItemListTemplate = data;
            entityItemViewTemplate = returnedData;
            findAllEntityServiceURL = PosSyncConfigurationServiceURL + "FindAllLite";
            deleteEntityServiceURL = PosSyncConfigurationServiceURL + "Delete";
            selectEntityCallBack = getPosSyncConfigurationInfo;
            loadDefaultEntityOptions();
        });
    }, 'PosSyncConfigurationViewItemTemplate');

}

function initControls(template, isAdd) {
    $('.Result-info-container').css('width', '100%');
    if (!template) template = $('.TabbedPanels');
    if (!isAdd)
        $('.historyUpdated').append(HistoryUpdatedListViewTemplate.clone());
    LoadDatePicker(template.find('input:.date'), 'dd-mm-yy');
    template.find('.numeric').autoNumeric();
    template.find('.integer').autoNumeric({ aPad: false });
    PosSyncTime1AC = $('select:.PosSyncTime1').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.PosSyncConfigurationServiceURL + 'getTime', required: true, multiSelect: false });
    PosSyncTime2AC = $('select:.PosSyncTime2').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.PosSyncConfigurationServiceURL + 'getTime', required: true, multiSelect: false });
    PosSyncTime3AC = $('select:.PosSyncTime3').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.PosSyncConfigurationServiceURL + 'getTime', required: true, multiSelect: false });
    PosSyncTime4AC = $('select:.PosSyncTime4').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.PosSyncConfigurationServiceURL + 'getTime', required: true, multiSelect: false });
    PosSyncTime5AC = $('select:.PosSyncTime5').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.PosSyncConfigurationServiceURL + 'getTime', required: true, multiSelect: false });
    AccpacSyncTime1AC = $('select:.AccpacSyncTime1').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.PosSyncConfigurationServiceURL + 'getTime', required: true, multiSelect: false });
    AccpacSyncTime2AC = $('select:.AccpacSyncTime2').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.PosSyncConfigurationServiceURL + 'getTime', required: true, multiSelect: false });
    AccpacSyncTime3AC = $('select:.AccpacSyncTime3').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.PosSyncConfigurationServiceURL + 'getTime', required: true, multiSelect: false });
    AccpacSyncTime4AC = $('select:.AccpacSyncTime4').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.PosSyncConfigurationServiceURL + 'getTime', required: true, multiSelect: false });
    AccpacSyncTime5AC = $('select:.AccpacSyncTime5').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.PosSyncConfigurationServiceURL + 'getTime', required: true, multiSelect: false });

   

}


function initAddControls(template) {
    initControls(template, true);
}

function getPosSyncConfigurationInfo(item) {
    if (item != null) {

        var trainindIdArray = item.AccpacDataToSyncIds.split(',');
        //$('.YSyncInvoice,.NSyncInvoice,.YSyncRefund,.NSyncRefund,.YSyncPettyCash,.NSyncPettyCash').hide();
        $.each(trainindIdArray, function (index, value) {



            if (value == systemConfig.dataTypes.SyncInvoice) {
                $('.SyncInvoice').prop('checked', true);
                $('.CheckSyncInvoice').text(getResource('Yes'));
            }

            if (value == systemConfig.dataTypes.SyncRefund) {
                $('.SyncRefund').prop('checked', true);
                $('.CheckSyncRefund').text(getResource('Yes'));
            }
            if (value == systemConfig.dataTypes.SyncPettyCash) {
                $('.SyncPettyCash').prop('checked', true);
                $('.CheckSyncPettyCash').text(getResource('Yes'));
                //$('.NSyncPettyCash').hide();
            }
        });

        $('.PosDataToSyncCheck').prop('checked', false);
        if (item.PosDataToSync == systemConfig.dataTypes.SyncAll) {
            $('.PosDataToSync').text('All');
            $('.SyncAll').prop('checked', true);
        }
        else if (item.PosDataToSync == systemConfig.dataTypes.SyncRegisterData) {
            $('.PosDataToSync').text('Register Data');
            $('.SyncRegisterData').prop('checked', true);
        }
        else if (item.PosDataToSync == systemConfig.dataTypes.SyncAdminData) {
            $('.PosDataToSync').text('Admin Data')
            $('.SyncAdminData').prop('checked', true);
        }

    }

}

 function setListItemTemplateValue(entityTemplate, entityItem) {

     entityTemplate.find('.result-data1 label').text(Truncate(entityItem.PosDataToSync, 20));
     if (entityItem.SyncDataFromAccpac) {
         $('.YSyncDataFromAccpac').show();
         $('.NSyncDataFromAccpac').hide();

     }
     else {
         $('.YSyncDataFromAccpac').hide();
         $('.NSyncDataFromAccpac').show();
     }
//     entityTemplate.find('.result-data2 label').textFormat(Truncate(entityItem.,20));
//     entityTemplate.find('.result-data3 label').textFormat(Truncate(entityItem., 20));
     return entityTemplate;
 }

 function setListItemServiceData(keyword, pageNumber, resultCount) {
     if (!QuickSearch) {
         return formatString('{ keyword: "{0}", page:{1}, resultCount:{2} ,argsCriteria:"{ PosSyncConfigurationId:{3}}"}', keyword, pageNumber, resultCount, filterEntityId);
     }
     else {
         return formatString('{mKey:"{0}",SearchCriteriaObj:"{1}",SortType:{2},SortFields:"{3}",TotalRecords:{4},PageNumber:{5}}', mKey, SearchCriteriaObj, SortType, SortFields, resultCount, pageNumber);
     }
 }



 function SaveEntity(isAdd, template) {
     if (!template)
         template = $('.TabbedPanels');

     if (!saveForm(template))
         return false;

     PosSyncConfigurationId = isAdd ? -1 : selectedEntityId;


     showLoading(template.parent());
     lastUpdatedDate = getEntityLockDateFormat(currentEntityLastUpdatedDate);

     var PosDataToSync;
     var AccpacDataToSyncIds = '';
     var AccpacDataToSync = '';
     if ($('input:.SyncAll').is(':checked'))
         PosDataToSync = systemConfig.dataTypes.SyncAll
     else if ($('input:.SyncRegisterData').is(':checked'))
         PosDataToSync = systemConfig.dataTypes.SyncRegisterData
     else if ($('input:.SyncAdminData').is(':checked'))
         PosDataToSync = systemConfig.dataTypes.SyncAdminData
     else
         PosDataToSync = 0


     if ($('input:.SyncInvoice').is(':checked')) {
         AccpacDataToSyncIds = AccpacDataToSyncIds + ',' + systemConfig.dataTypes.SyncInvoice;
         AccpacDataToSync = AccpacDataToSync + ',Invoice';
     }
     if ($('input:.SyncRefund').is(':checked')) {
         AccpacDataToSyncIds = AccpacDataToSyncIds + ',' + systemConfig.dataTypes.SyncRefund;
         AccpacDataToSync = AccpacDataToSync + ',Refund';
     }
     if ($('input:.SyncPettyCash').is(':checked')) {
         AccpacDataToSyncIds = AccpacDataToSyncIds + ',' + systemConfig.dataTypes.SyncPettyCash;
         AccpacDataToSync = AccpacDataToSync + ',Petty Cash';
     }


     var PosSyncConfigurationObj = {

         PosSyncTime1Id: PosSyncTime1AC.get_ItemValue(),
         PosSyncTime2Id: PosSyncTime2AC.get_ItemValue(),
         PosSyncTime3Id: PosSyncTime3AC.get_ItemValue(),
         PosSyncTime4Id: PosSyncTime4AC.get_ItemValue(),
         PosSyncTime5Id: PosSyncTime5AC.get_ItemValue(),
         PosDataToSync: PosDataToSync,
         SyncDataFromAccpac: $('input:.SyncDataFromAccpac').is(':checked'),
         AccpacSyncTime1Id: AccpacSyncTime1AC.get_ItemValue(),
         AccpacSyncTime2Id: AccpacSyncTime2AC.get_ItemValue(),
         AccpacSyncTime3Id: AccpacSyncTime3AC.get_ItemValue(),
         AccpacSyncTime4Id: AccpacSyncTime4AC.get_ItemValue(),
         AccpacSyncTime5Id: AccpacSyncTime5AC.get_ItemValue(),
         AccpacDataToSyncIds: AccpacDataToSyncIds,//$('input.AccpacDataToSyncIds').val(),
         AccpacDataToSync:AccpacDataToSync// $('textarea.AccpacDataToSync').val()
         //Status:0// $('input.Status').val()
     };
     var data = formatString('{id:{0}, PosSyncConfigurationInfo:"{1}"}', isAdd ? -1 : selectedEntityId, escape(JSON.stringify(PosSyncConfigurationObj)));

     if (PosSyncConfigurationId > 0)
         url = PosSyncConfigurationServiceURL + "Edit";
     else
         url = PosSyncConfigurationServiceURL + "Add";

     post(url, data, function (returned) { saveSuccess(returned, isAdd, template); }, saveError, getEntitylockAjaxHeaders(lastUpdatedDate));
 }

 function saveSuccess(returnedValue, isAdd, template) {
     if (returnedValue.statusCode.Code == 501) {

         hideLoading();
         return false;
     }
     else if (returnedValue.statusCode.Code == 0) {
         hideLoading();
         if (returnedValue.statusCode.message == 'validation' && returnedValue.result) {
             filterError(returnedValue.result, template);
         }
         showStatusMsg(getResource("FaildToSave"));
         return false;
     }
     if (!addPosSyncConfigurationMode) {
         showStatusMsg(getResource("savedsuccessfully"));
         hideLoading();
         loadEntityData(loadEntityServiceURL, entityItemViewTemplate, selectedEntityId, getPosSyncConfigurationInfo);
     }
     else {
         appendNewListRow(returnedValue.result);
         hideLoading();
         $('.add-new-item').hide();
         $('.add-new-item').empty();
     }
 }

 function saveError(ex) {
     hideLoading();
     handleLockEntityError(ex, deletedEntityCallBack, updatedEntityCallBack);
 }



 function filterError(errors, template) {
     var allMsg = '';
     template.find('.validation').remove();
     $.each(errors, function (index, error) {
         // var template = $('.PosSyncConfiguration-info');
         if (error.GlobalMessage) {
             allMsg += '<span>' + error.Message + '<br/></span>';
         }
         else if (error.ControlName == 'PosSyncTime1') {
             addPopupMessage(template.find('select.PosSyncTime1'), getResource("required"));
         }
         else if (error.ControlName == 'PosSyncTime2') {
             addPopupMessage(template.find('select.PosSyncTime2'), getResource("required"));
         }
         else if (error.ControlName == 'PosSyncTime3') {
             addPopupMessage(template.find('select.PosSyncTime3'), getResource("required"));
         }
         else if (error.ControlName == 'PosSyncTime4') {
             addPopupMessage(template.find('select.PosSyncTime4'), getResource("required"));
         }
         else if (error.ControlName == 'PosSyncTime5') {
             addPopupMessage(template.find('select.PosSyncTime5'), getResource("required"));
         }
         else if (error.ControlName == 'PosDataToSync') {
             addPopupMessage(template.find('input.PosDataToSync'), getResource("required"));
         }
         else if (error.ControlName == 'SyncDataFromAccpac') {
             addPopupMessage(template.find('input.SyncDataFromAccpac'), getResource("required"));
         }
         else if (error.ControlName == 'AccpacSyncTime1') {
             addPopupMessage(template.find('select.AccpacSyncTime1'), getResource("required"));
         }
         else if (error.ControlName == 'AccpacSyncTime2') {
             addPopupMessage(template.find('select.AccpacSyncTime2'), getResource("required"));
         }
         else if (error.ControlName == 'AccpacSyncTime3') {
             addPopupMessage(template.find('select.AccpacSyncTime3'), getResource("required"));
         }
         else if (error.ControlName == 'AccpacSyncTime4') {
             addPopupMessage(template.find('select.AccpacSyncTime4'), getResource("required"));
         }
         else if (error.ControlName == 'AccpacSyncTime5') {
             addPopupMessage(template.find('select.AccpacSyncTime5'), getResource("required"));
         }
         else if (error.ControlName == 'AccpacDataToSyncIds') {
             addPopupMessage(template.find('input.AccpacDataToSyncIds'), getResource("required"));
         }
         else if (error.ControlName == 'AccpacDataToSync') {
             addPopupMessage(template.find('input.AccpacDataToSync'), getResource("required"));
         }
         else if (error.ControlName == 'Status') {
             addPopupMessage(template.find('input.Status'), getResource("required"));
         }

     });

     hideLoading();
     if (allMsg.trim()) {
         showOkayMsg('Validation error', allMsg.trim());
     }
 }






