var UserId = -1, UserItemTemplate, ItemListTemplate;
var RoleNameAC, TeamNameAC, ManagerAC;
var userServiceURL;
var addUserMode = false;
var vldUserNameMsg;
var vldPasswordMsg;
var UserName = "";

function InnerPage_Load() {
    vldUserNameMsg = getResource("USpecialCharError");
    vldPasswordMsg = getResource("USpecialCharError");
    setSelectedMenuItem('.pos-icon');
    setPageTitle(getResource("User"));
    setAddLinkAction(getResource("AddUser"), 'add-new-item');
    currentEntity = systemEntities.UserEntityId;
    userServiceURL = systemConfig.ApplicationURL_Common + systemConfig.UsersServiceURL;

    $('.result-block').live("click", function () {
        userId = $(this).attr('id');

    });


    //mKey = 'user';
    //InnerPage_Load2();
   // $('.QuickSearch-holder').show();
  //  LoadSearchForm2(mKey, false);

    $('.MoreSearchOptions').remove();
    //systemConfig.HTMLTemplateURLs.AddUserTemplate
    GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.AddUserTemplate, function () {
        GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.UserListItemTemplate, loadData, 'ItemListTemplate');
        GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.UserStoresShiftsTabTemplate, function () { }, 'UserStoresShiftsTabTemplate');

    }, 'AddItemTemplate');

    $('li:.email-tabs').click(function () {
        EmailTemplate = $(EmailTemplate);
        //checkPermissions(this, systemEntities.EmailEntityId, -1, EmailTemplate, function (template) {
            getEmails(systemEntities.UserEntityId, selectedEntityId);
        //});
    });


    $('li:.note-tabs').click(function () {
        EmailTemplate = $(EmailTemplate);
        checkPermissions(this, systemEntities.NoteEntityId, -1, NoteTemplate, function (template) {
            getNotes(systemEntities.UserEntityId, selectedEntityId);
        });
    });

    $('li:.Attach-tabs').live('click', function () {
        checkPermissions(this, systemEntities.AttachmentsEntityId, -1, $('.Action-div'), function (template) {
            getAttachments(systemEntities.UserEntityId, selectedEntityId, 'User', systemEntities.UserEntityId);
        });
    });

    $('li:.Communication-tab').live('click', function () {
        checkPermissions(this, systemEntities.TaskId, -1, $('.Action-div'), function (template) {
            var userItem = { label: UserName, value: selectedEntityId };
            getCommunication(selectedEntityId, systemEntities.UserEntityId, selectedEntityId, UserName, userItem);
        });
    });

    $('li:.StoresShiftsTab').live('click', function () {
        UserStoresShiftsTabTemplate = $(UserStoresShiftsTabTemplate);
        checkPermissions(this, systemEntities.UserStoresShiftsEntityId, -1, UserStoresShiftsTabTemplate, function (template) {
            getUserStoreShift(selectedEntityId);
        });
    });


    deletedEntityCallBack = function () {
        removeDeletedEntity();
    };

    updatedEntityCallBack = function () {
        getEntityData(selectedEntityId);
    };

}



function loadData(data) {

    GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.UserListViewItemTemplate, function () {
        checkPermissions(this, systemEntities.UserEntityId, -1, UserItemTemplate, function (template) {
            UserItemTemplate = template;
            loadEntityServiceURL = userServiceURL + "FindUserByIdLite";
            entityItemListTemplate = data;
            entityItemViewTemplate = UserItemTemplate;
            findAllEntityServiceURL = userServiceURL + "FindUserAllLite";
            deleteEntityServiceURL = userServiceURL + "Delete";

            selectEntityCallBack = setUserInfo;
            loadDefaultEntityOptions();
        });
    }, 'UserItemTemplate');

}


function initControls(template, isAdd) {
    if (!template)
        template = $('.TabbedPanelsContent').first();
    template.find('.UserEmail').text('');
    $('.historyUpdated').append(HistoryUpdatedListViewTemplate);
    RoleNameAC = template.find('select:.RoleName').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.RoleServiceURL + "FindAllLite", required: true });
    TeamNameAC = template.find('select:.TeamName').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.TeamServiceURL + "FindAllLite", required: false });
    ManagerAC = template.find('select:.Manager').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.UsersServiceURL + "FindUserAllLite", required: false });
    ManagerAC.set_Args(formatString('{isManager:{0}}', true));

    if (!addUserMode)
        ManagerAC.set_Args(formatString('{isManager:{0},ExceptUserId:{1}}', true, Number(selectedEntityId)));

    TeamNameAC.onChange(function () {
        var item = TeamNameAC.get_Item();
        if (item != null) {
            if (item.ManagerId > 0) {
                template.find('span:.Manager').text(item.Manager);
            }
            else {
                template.find('span:.Manager').text('');
            }
        }
        else {
            template.find('.UManager-field').hide();
            template.find('span:.Manager').text('');
        }
    });


    template.find('.UserName').keydown(function (e) {
        if (e.keyCode == 32) {
            e.preventDefault();
        }
    });

    template.find('.UserName').live('blur', function (e) {
        template.find('.cx-control-red-border').removeClass("cx-control-red-border");
        template.find('.validation').hide();
        if ($(this).val() != '' && !checkValidValue($(this).val()))
            addPopupMessage($(this), vldUserNameMsg, false);

    });

}

function checkValidValue(userName) {
    //    var specialReg = /^\s*([0-9a-zA-Z]*)\s*$/;
    var specialReg = /^([a-zA-Z0-9]+)$/;
    return specialReg.test(userName);
}

function SaveEntity(isAdd, template) {
    if (!template)
        template = $('.TabbedPanelsContent').first();

    if (!saveForm(template.find('.user-info')))
        return false;



    var password = template.find('.UserPassword').val();
    var confirmPassword = template.find('.ConfirmPassword').val();

    if (password != confirmPassword) {
        addPopupMessage(template.find('input:.ConfirmPassword'), getResource("USPasswordConfirmation"), false);
        return false;
    }

    if (template.find('.UserPassword').val().trim().length < 3) {
        addPopupMessage(template.find('input:.UserPassword'), getResource("MinLengthIs") + 3, false);
        return false;
    }

    if (template.find('input.DobValue').val() != "") {
        DobValue = getDate("dd-mm-yy", getDotNetDateFormat(template.find('input.DobValue').val()));
        if (DateDiff(getDate("dd-mm-yy", new Date()), DobValue) > 0) {
            addPopupMessage(template.find('input.DobValue'), getResource("DateOfBirthError"), true);
            hideLoading();
            return false;
        }
    }

    var userId = selectedEntityId;
    var roleId = -1, teamId = -1, managerId = -1;
    var userName = template.find('input:.UserName').val();
    var firstName = template.find('input:.FirstnameEnglish').val().trim();
    var middleName = template.find('input:.MiddleNameEnglish').val().trim();
    var lastName = template.find('input:.LastNameEnglish').val().trim();
    var note = template.find('textArea:.Notes').val().trim();
    var title = template.find('input:.Title').val().trim();
    var dob = template.find('input:.DobValue').val().trim();
    var email = template.find('input:.UserEmail').val().trim();
    var acessCode = template.find('input:.AccessCode').val().trim();
    var isDefault = template.find('input[type="checkbox"]:.IsDefault').is(':checked');
    var isManager = template.find('input[type="checkbox"]:.IsManager').is(':checked');
    var isPosUser = template.find('input[type="checkbox"]:.IsPosUser').is(':checked');

    if (RoleNameAC.get_Item() != null)
        roleId = RoleNameAC.get_Item().value;
    if (TeamNameAC.get_Item() != null)
        teamId = TeamNameAC.get_Item().value;
    if (ManagerAC.get_Item() != null)
        managerId = ManagerAC.get_Item().value;

    var User = template.find('input:.UserName').val();
    if (!checkValidValue(User)) {
        !checkValidValue(User) ? addPopupMessage(template.find('input:.UserName'), vldUserNameMsg, false) : '';
        return false;
    }

    if (isAdd)
        userId = -1;
    var lastUpdatedDate = null;
    debugger;
    var data = '{userId:{0}, userName:"{1}", password:"{2}", firstName:"{3}", middleName:"{4}", lastName:"{5}", title:"{6}", dob:"{7}", note:"{8}", roleId:{9}, teamId:{10}, email:"{11}", UserId:{12},ManagerId:{13},IsManager:{14},IsDefault:{15},IsPosUser:{16},accessCode:"{17}"}';
    data = formatString(data, userId, userName, password, firstName, middleName, lastName, title, dob, note, roleId, teamId, email, UserId, managerId, isManager, isDefault, isPosUser, acessCode);
    if (isAdd) {
        showLoading(template.parent());
    }
    else {
        showLoading($('.TabbedPanels'));
        lastUpdatedDate = getEntityLockDateFormat(currentEntityLastUpdatedDate);
    }
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: userServiceURL + "AddEditUser",
        headers: getEntitylockAjaxHeaders(lastUpdatedDate),
        data: data,
        datatype: "json",
        success: function (data) {
            var returnedValue = eval("(" + data.d + ")");

            if (returnedValue.statusCode.Code == 501) {
                addPopupMessage(template.find('input:.UserName'), getResource("AlreadyExists"), false);
                hideLoading();
                return false;
            }
            else if (returnedValue.statusCode.Code == 406) {
                showStatusMsg(getResource(returnedValue.statusCode.message));
                hideLoading();
                template.find('.user-info').css('opacity', '1');
                return false;
            }
            else if (returnedValue.statusCode.Code == 403) {
                $.each(returnedValue.result, function (index, Item) {
                    addPopupMessage(template.find('.' + index), Item, false);

                });
                hideLoading();
                template.find('.user-info').css('opacity', '1');
                return false;
            }
            showStatusMsg(getResource("savedsuccessfully"));
            if (!isAdd) {
                hideLoading();
                loadEntityData(loadEntityServiceURL, entityItemViewTemplate, selectedEntityId, setUserInfo);
            }
            else {
                appendNewListRow(returnedValue.result);
                hideLoading();
                $('.add-new-item').hide();
                $('.add-new-item').empty();
            }



        },
        error: function (ex) {
            $('.loader').hide();
            handleLockEntityError(ex, deletedEntityCallBack, updatedEntityCallBack);
        }
    });
}

function updateListingData(obj) {

    selectedEntityItemList.find('.result-data1 label').html(obj.UserName);
    selectedEntityItemList.find('.result-data2 label').html(Truncate(obj.FullName, 30));
    selectedEntityItemList.find('.result-data3 label').html(obj.UserEmail);
    var UEmail = obj.UserEmail != null ? obj.UserEmail : '';
    selectedEntityItemList.find('.result-data3 .HBusiness-email-to').attr('href', 'mailto:' + UEmail);
    selectedEntityItemList.find('.result-data4 label').html(obj.TeamName);
    selectedEntityItemList.find('.result-data5 label').html(obj.RoleName);
}

////////////////////////////////////
function setListItemTemplateValue(entityTemplate, entityItem) {

    entityTemplate.find('.result-data1 label').html(Truncate(entityItem.UserName, 30));
    entityTemplate.find('.result-data2 label').html(Truncate(entityItem.FullName, 30));
    entityTemplate.find('.result-data3 label').html(entityItem.UserEmail);
    var UEmail = entityItem.UserEmail != null ? entityItem.UserEmail : '';
    entityTemplate.find('.result-data3 .HBusiness-email-to').attr('href', 'mailto:' + UEmail);
    entityTemplate.find('.result-data4 label').html(entityItem.TeamName);
    entityTemplate.find('.result-data5 label').html(entityItem.RoleName);

    return entityTemplate;
}

function setListItemServiceData(keyword, pageNumber, resultCount) {
    debugger
    if (!QuickSearch) {
        return formatString('{ keyword: "{0}", page:{1}, resultCount:{2},argsCriteria:"{ UserId:{3}}" }', keyword, pageNumber, resultCount, filterEntityId);
    }
    else { return formatString('{mKey:"{0}",SearchCriteriaObj:"{1}",SortType:{2},SortFields:"{3}",TotalRecords:{4},PageNumber:{5}}', mKey, SearchCriteriaObj, SortType, SortFields, resultCount, pageNumber); }
}


function setUserInfo(user) {
    if (user != null) {
        //        if (user.ManagerId > 0) {
        //            $('.UManager-field').show();
        //            $('span:.Manager').text(user.Manager);
        //        }
        //        else {
        //            $('.UManager-field').hide();
        //            $('span:.Manager').text('');
        //        }

        if (user.IsPosUser) {
            $('span.IsPosUser').text(getResource("Yes"));
            $('input:.IsPosUser').attr('checked', true);
        }
        else {
            $('span.IsPosUser').text(getResource("No"));
            $('input:.IsPosUser').attr('checked', false);
        }

        UserName = user.FullName;
        $('a:.TeamName-link').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.TeamsList);
      
        $('a:.Manager-link').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.UserList + "?key=" + user.ManagerId);
        LoadDatePicker('input:.DobValue', 'dd-mm-yy');
        $('.HaderUserName').text(Truncate(user.UserName, 10));
        $('.FullNameTruncated').text(Truncate(user.FullName, 10));
        $('.UserEmailTruncated').text(Truncate(user.UserEmail, 10));
        $('.email-to').attr('href', "mailto:" + user.UserEmail);

        $('.ActionDelete-link').click(function () {
            if (user.IsSupperUser) {
                showStatusMsg(getResource("DeleteSuperUserMsg"));
                return false;
            }
        });

        if (user.IsSupperUser)
            $('.ActionDelete-link').hide();

        updateListingData(user);
    }
}

function DateDiff(date1, date2) {
    var Startdate = new Date(date1.replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
    var EndDate = new Date(date2.replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
    return EndDate.getTime() - Startdate.getTime();
}


function initAddControls(template, isAdd) {
debugger
    LoadDatePicker(template.find('input:.DobValue'), 'dd-mm-yy');
    initControls(template, isAdd);
}


function getUserStoreShift(userId) {
    $('.StoreShiftTab').children().remove();
    showLoading($('.TabbedPanels'));
    $.ajax({
        type: "POST",
        url: systemConfig.ApplicationURL_Common + systemConfig.UserStoreShifsServiceURL + "FindAllLite",
        contentType: "application/json; charset=utf-8",
        data: formatString('{ keyword: "", page:1, resultCount:100,argsCriteria:"{UserId:{0}}"}', userId),
        dataType: "json",
        success: function (data) {
            var StoresShiftsList = eval("(" + data.d + ")").result;
            AddTab(null, userId);
            if (StoresShiftsList != null) {
                $('.StoreShiftTab').parent().find('.DragDropBackground').remove();
                $.each(StoresShiftsList, function (index, item) {
                    AddTab(item, userId);
                });
            }
            attachTabEvents();
            hideLoading();
        },
        error: function (e) {
        }
    });

}

function AddTab(Item, userId) {
    var TabTemplate = $(UserStoresShiftsTabTemplate).clone();
    var updatedDate = null;
    var UserStoreShiftId = (Item != null) ? Item.UserStoreShiftId : -1;

    var StoreAC = TabTemplate.find('select:.Store').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.StoreServiceURL + 'FindAllLite', required: true, multiSelect: false });
    var ShiftAC = TabTemplate.find('select:.Shift').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.ShiftsServiceURL + 'FindAllLite', required: true, multiSelect: false });


    TabTemplate.find('.ShiftRequired').hide();
    ShiftAC.set_Required(false);


    if (Item != null) {
        TabTemplate.find('.historyUpdated').append(HistoryUpdatedListViewTemplate.clone());
        TabTemplate = mapEntityInfoToControls(Item, TabTemplate, [{ controlName: 'StoreAC', control: StoreAC }, { controlName: 'ShiftAC', control: ShiftAC}]);
        updatedDate = Item.UpdatedDate;

        if (Item.ValidateShift) {
            TabTemplate.find('.ShiftRequired').show();
            ShiftAC.set_Required(true);
            TabTemplate.find('#ValidateShift').attr('checked', true);
        }
        else {
            TabTemplate.find('.ShiftRequired').hide();
            ShiftAC.set_Required(false);
            TabTemplate.find('#ValidateShift').attr('checked', false);
        }
    }
    else {
        TabTemplate.find('.historyUpdated').remove();
        TabTemplate.find('.Tab-basics-info').remove();
        TabTemplate.addClass('addnew');
        TabTemplate.hide();

        $('.add-new-StoreShift').click(function () {
            TabTemplate.find('.cx-control-red-border').removeClass('cx-control-red-border');
            setAddLinkEvent(TabTemplate);
            return false;
        });
    }

    TabTemplate.find('.save-tab-lnk').on('click', function () {
        var updatedDate = Item ? Item.UpdatedDate : null;
        saveStoreShifts(userId, UserStoreShiftId, TabTemplate, updatedDate, StoreAC, ShiftAC);
        return false;
    });

    TabTemplate.find('.delete-tab-lnk').click(function () {
        showConfirmMsg(DeleteTitleMessage, DeleteMessage, function () {
            showLoading($('.TabbedPanels'));
            lockForDelete(systemEntities.UserStoresShiftsEntityId, Item.UserStoreShiftId, function () {
                deleteStoreShifts(userId, UserStoreShiftId)
            }, function () {
                TabTemplate.remove();
            });
        });
        return false;
    });

    TabTemplate.find('.edit-tab-lnk').click(function () {
   
        showLoading($('.TabbedPanels'));
        lockEntity(systemEntities.UserStoresShiftsEntityId, Item.UserStoreShiftId, function () {
            TabTemplate.find('.edit-tab-lnk').hide();
            TabTemplate.find('.save-tab-lnk').show();
            TabTemplate.find('.cancel-tab-lnk').show();
            TabTemplate.find('.delete-tab-lnk').hide();

            if (!TabTemplate.hasClass('Tab-listSelected'))
                TabTemplate.find('.itemheader').click();

            TabTemplate.find('.viewedit-display-none').show();
            TabTemplate.find('.viewedit-display-block').hide();
            preventInputChars(TabTemplate.find('input:.integer'));

            TabTemplate.find('.cx-control:visible').first().focus();
        }, Item.UpdatedDate, function () {
            getUserStoreShift(userId);
        }, function () {
            TabTemplate.remove();
        });
        return false;
    });


    TabTemplate.find('.cancel-tab-lnk').click(function () {
        if (Item == null) {
            TabTemplate.find('.delete-tab-lnk').show();
            TabTemplate.find('.itemheader').click();
            $('.addnew').hide();
        }
        else {
            showLoading($('.TabbedPanels'));
            UnlockEntity(systemEntities.UserStoresShiftsEntityId, UserStoreShiftId, function () {
                getUserStoreShift(userId);
            });
        }
        return false;
    });

    TabTemplate.find("#ValidateShift").click(function () {
        if (TabTemplate.find('#ValidateShift').is(':checked')) {
            TabTemplate.find('.ShiftRequired').show();
            ShiftAC.set_Required(true);
        }
        else {
            TabTemplate.find('.ShiftRequired').hide();
            ShiftAC.set_Required(false);
        }
    });
    $('.StoreShiftTab').append(TabTemplate);
}

function deleteStoreShifts(userId, StoreShiftId) {
    $.ajax({
        type: "POST",
        url: systemConfig.ApplicationURL_Common + systemConfig.UserStoreShifsServiceURL + "Delete",
        contentType: "application/json; charset=utf-8",
        data: formatString('{id:{0}}', StoreShiftId),
        dataType: "json",
        success: function (data) {
            getUserStoreShift(userId);
        },
        error: function (e) {
        }
    });
}


function saveStoreShifts(userId, storeShiftId, template, UpdatedDate, storeAC, shiftAC) {
    debugger;
    var isAdd = (storeShiftId > 0) ? false : true;

    var valid = true;


    if (!saveForm(template)) {
        valid = false;
    }

    var validateShiftValue = template.find("input[type='checkbox']").is(':checked');
    if (validateShiftValue && shiftAC.get_Item() == null) {
        // shiftAC.add_Error('Required');
        valid = false;
    }

    if (!valid) {
        hideLoading();
        return false;
    }

    showLoading($('.TabbedPanels'));
    var StoreShift = {
        UserId: userId,
        UserStoreShiftId: storeShiftId,
        StoreId: storeAC.get_ItemValue(),
        ShiftId: (validateShiftValue) ? shiftAC.get_ItemValue() : -1,
        ValidateShift: validateShiftValue
    };

    var data = formatString('{id:{0},UserStoreShifsInfo:"{1}"}', storeShiftId, escape(JSON.stringify(StoreShift)));

    $.ajax({
        type: "POST",
        url: systemConfig.ApplicationURL_Common + systemConfig.UserStoreShifsServiceURL + ((!isAdd) ? "Edit" : "Add"),
        contentType: "application/json; charset=utf-8",
        data: data,
        dataType: "json",
        headers: getEntitylockAjaxHeaders(getEntityLockDateFormat(UpdatedDate)),
        success: function (returnedValue) {
            var data = eval('(' + returnedValue.d + ')')
            if (data.statusCode.Code == 501) {
                hideLoading();
                shiftAC.add_Error("Dublicated");
                return false;
            }
            else if (data.statusCode.Code == 0) {
                hideLoading();
                showStatusMsg(getResource("FaildToSave"));
                return false;
            }
            else {
                debugger;
                showStatusMsg(getResource("savedsuccessfully"));
                getUserStoreShift(userId);

               
            }
        },
        error: function (ex) {
            handleLockEntityError(ex, function () { template.remove(); }, function () { getUserStoreShift(userId); });
        }
    });
}