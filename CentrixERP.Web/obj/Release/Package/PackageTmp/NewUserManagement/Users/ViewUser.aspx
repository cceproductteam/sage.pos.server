<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewUser.aspx.cs" Inherits="Centrix.UM.Web.NewUserManagement.Users.ViewUser"
    MasterPageFile="~/Common/MasterPage/Site.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="PageHeader" ID="PageHeader">
    <script src="JS/Users.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <script src="JS/ViewUser.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <script type="text/javascript">
        userId = '<%=UserId %>';
    </script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="PageBody" ID="PageBody">
    <div class="Add-new-form">
        <div class="Add-title">
            <span>User Details</span>
            <div class="Action-div">
                <ul>
                    <li><span class="ActionCancel-link viewedit-display-block"></span><a class="ActionCancel-link ">
                        Cancel</a> </li>
                </ul>
            </div>
        </div>
        <table border="0" cellpadding="0" cellspacing="0" class="Data-Table3col user-info">
            <tr>
                <th class="label-title">
                    User Name
                </th>
                <td>
                    <span class="label-data UserName viewedit-display-block"></span>
                </td>
                <th class="label-title">
                    First Name
                </th>
                <td>
                    <span class="label-data FirstnameEnglish viewedit-display-block"></span>
                </td>
                <th class="label-title">
                    Middle Name
                </th>
                <td>
                    <span class="label-data MiddleNameEnglish viewedit-display-block"></span>
                </td>
            </tr>
            <tr>
                <th class="label-title">
                    Last Name
                </th>
                <td>
                    <span class="label-data LastNameEnglish viewedit-display-block"></span>
                </td>
                <th class="label-title">
                    Password
                </th>
                <td>
                    <span class="label-data UserPassword viewedit-display-block">********</span>
                </td>
                <th class="label-title">
                    Title
                </th>
                <td>
                    <span class="label-data Title viewedit-display-block"></span>
                </td>
            </tr>
            <tr>
                <th>
                    <span class="label-title">Date Of Birth</span>
                </th>
                <td>
                    <span class="label-data DobValue date viewedit-display-block"></span>
                </td>
                <th>
                    <span class="label-title">Email</span>
                </th>
                <td>
                    <a href="mailto:" class="link UserEmail viewedit-display-block"></a>
                </td>
                <th>
                    <span class="label-title">Role</span>
                </th>
                <td>
                    <span class="label-data RoleName viewedit-display-block"></span>
                </td>
            </tr>
            <tr>
                <th>
                    <span class="label-title">Team</span>
                </th>
                <td>
                    <span class="label-data TeamName viewedit-display-block"></span>
                </td>
                <th>
                    <span class="label-title">Manager</span>
                </th>
                <td>
                    <span class="label-data Manager viewedit-display-block"></span>
                </td>
                <th>
                    <span class="label-title">IsDefault</span>
                </th>
                <td>
                    <span class="label-data IsDefault viewedit-display-block"></span>
                </td>
            </tr>
            <tr class="display-none">
                <th>
                    <span class="label-title">IsManager</span>
                </th>
                <td>
                    <span class="label-data IsManager viewedit-display-block"></span>
                </td>
                <th>
                    <span class="label-title">Note</span>
                </th>
                <td>
                    <span class="Notes viewedit-display-block label-data"></span>
                </td>
            </tr>
        </table>
    </div>
    <span class="detail-sub-title">History Updated</span>
          <table border="0" cellpadding="0" cellspacing="0" class="Data-Table user-info">
             <tr>
                    <th>
                        <span class="label-title ">Created Date</span>
                    </th>
                    <td>
                        <span class="label-data date CreatedDate Crearted_Date"></span>
                      
                    </td>
                    <th>
                        <span class="label-title">Created By</span>
                    </th>
                    <td>
                      <span class="label-data nowrap CreatedByName CreatedBy "></span>
                    </td>
                </tr>
                 <tr class="UpdatingHistory" >
                    <th>
                        <span class="label-title ">Last Updated Date</span>
                    </th>
                    <td>
                        <span class="label-data date UpdatedDate Updated_Date"></span>
                      
                    </td>
                    <th>
                        <span class="label-title">Last Updated By</span>
                    </th>
                    <td>
                      <span class="label-data nowrap UpdatedByName UpdatedBy"></span>
                    </td>
                </tr>

            </table>
</asp:Content>
