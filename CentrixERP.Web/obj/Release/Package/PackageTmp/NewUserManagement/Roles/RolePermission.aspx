<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RolePermission.aspx.cs"
    Inherits="Centrix.UM.Web.NewUserManagement.Roles.RolePermission" MasterPageFile="~/Common/MasterPage/ERPSite.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="PageHeader">
    <script src="JS/RolePermission.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <link href="css/permission_<%=Lang %>.css?v=<%=Centrix_Version %>" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="PageBody">
    <div class="Permissions-action-div">
        <div class="action-control">
            <ul>
                <li class="classInline"><span class="lbl-title" resourcekey="PRole"><span class="smblreqyuired viewedit-display-none">*</span></span>
                    <span><select class="RoleAC"></select>  </span></li>
                <li class="classInline" >
                    <input type="checkbox" class="collapse-all-roles"/><span class="lbl-title" resourcekey="UnCollapseAll"></span></li>
            
                <li class="classInline hliunColl">
                    <input type="checkbox" class="check-all-checkbox-perm" /><span class="lbl-title" resourcekey="SelectAll"></span></li>
        
                <li class="btn-save-permission display-none" ><span class="ActionSave-link"></span><a class="save-title"><span resourcekey="Save"></span> </a></li>
                
                <li class="btn-edit-permissions classInline"><span class="ActionEdit-link edit-tab-lnk "></span><a class="save-title edit-tab-lnk ActionEdit-link"><span resourcekey="Edit"></span></a></li>
          
                    </ul>

        </div>
    </div>
    <div class="default-permi">
        <div class="perm-group" id="General-Perm">
            <div class="PermDetails-div">
                <div class="perm-group-title">
                    <span class="perm-group-title-span"><span class="group-name" resourcekey="Permissions"></span></span>
                </div>
            </div>
            <div class="perm-group-items view-group-permi">
                <ul class="permission-list">
                </ul>
            </div>
        </div>
    </div>
    <div class="permissions-container">
        <div class="Catetogry-perm">
            <div class="category-perm-group">
                <div class="categoryPermDetails-div">
                    <div class="category-perm-group-title">
                        <span class="category-perm-group-title-span"><span class="group-name"></span></span>
                        <span class="collaps-arrow"></span>
                    </div>
                    
                </div>
                <div class="Permissions">
                        <div class="perm-div-1Col permission-items">
                        </div>
                        <div class="perm-div-2Col permission-items">
                        </div>
                        <div class="perm-div-3Col permission-items">
                        </div>
                    </div>
            </div>
        </div>
    </div>
</asp:Content>
