﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InvoiceInquiry.aspx.cs" Inherits="SagePOSServer.Web.NewReports.InvoiceInquiry.InvoiceInquiry" MasterPageFile="~/Common/MasterPage/ERPSite.master" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="PageHeader">
    <script src="../../Common/DataDictionary/DataDictionary-ar.js" type="text/javascript"></script>
    <script src="../../Common/DataDictionary/DataDictionary-en.js" type="text/javascript"></script>
    <link href="../CSS/ReportCSS.css?v=<%=Centrix_Version %>" rel="stylesheet" type="text/css" />
    <script src="JS/POSTotalDailySales.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <script src="../../Common/JScript/DataTables/JS/jquery.dataTables.min.js"></script>
    <script src="../../Common/JScript/DataTables/JS/jquery.dataTables.js"></script>
    <script src="../../Common/JScript/DataTables/JS/dataTables.buttons.min.js"></script>
    <script src="../../Common/JScript/DataTables/JS/buttons.flash.min.js"></script>
    <script src="../../Common/JScript/DataTables/JS/jszip.min.js"></script>
    <script src="../../Common/JScript/DataTables/JS/vfs_fonts.js"></script>
    <script src="../../Common/JScript/DataTables/JS/vfs_fonts.js"></script>
    <script src="../../Common/JScript/DataTables/JS/buttons.html5.min.js"></script>
    <script src="../../Common/JScript/DataTables/JS/buttons.print.min.js"></script>
    <script src="../../Common/JScript/DataTables/JS/dataTables.material.min.js"></script>
    <link href="../../Common/JScript/DataTables/CSS/dataTables.material.css" rel="stylesheet" />
    <link href="../../Common/JScript/DataTables/CSS/material.min.css" rel="stylesheet" />
    <link href="../../Common/JScript/DataTables/CSS/buttons.dataTables.min.css" rel="stylesheet" />
    



    <style>
        .THead-Th {
            color: black !important;
            text-align: left !important;
        }

        .dt-buttons {
            float: right !important;
        }
    </style>
    <script src="JS/InvoiceInquiry.js"></script>

</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="PageBody">

    <div class="">
        <div class="">
            <div class="Details-div">
                <span class="details-div-title" resourcekey="AdvFilterYourResults"></span>
                </div>
            <table cellspacing="0" cellpadding="0" border="0" class="Data-Table">
                <tbody>
                    <tr>
                        <th>
                            <span class="label-title" resourcekey="DateFrom"></span>
                        </th>
                        <td>
                            <input class="cx-control DateFrom  date" type="text">
                        </td>
                        <th>
                            <span class="label-title" resourcekey="DateTo"></span>
                        </th>
                        <td>
                            <input class="cx-control DateTo  date" type="text">
                        </td>
                    </tr>
                    <tr>
                                                <th>
                            <span class="label-title" resourcekey="AccpacSyncUniqueNumber"></span>
                        </th>
                        <td>
                            <input class="cx-control AccpacSyncUniqueNumber  string" type="text">
                        </td>
                        <th>
                            <span class="label-title" resourcekey="Posted"></span>
                        </th>
                        <td>
                          
 <input type="checkbox" class="cx-control Posted" / >

                              


                        </td>

                        <th>
                            <span class="label-title" resourcekey="NotPosted"></span>
                        </th>
                        <td>
        <input type="checkbox" class="cx-control NotPosted" />
                            </td>
                    </tr>
                <div class="Action-div">
                    <ul>
                    </ul>
                </div>
            
            <table cellspacing="0" cellpadding="0" border="0" class="Data-Table">
                <tbody>

                    <tr>
                        <td>
                            <div class="filter-reset">
                                 <input type="button" style="left:191%;" class="lnkReset-InvoiceInquiry button-cancel button2" id="btnSearch" value="Refresh" />
                                <input type="button"style="display:none"   class="lnkReset-InvoiceInquiry button-cancel button2" id="btnRefresh" value="Refresh" />
                            </div>
                        </td>
                    </tr>

                </tbody>
            </table>
            <br />
            <div class='table'>
                <table class="display result mdl-data-table dataTable " cellspacing="0" width="100%">
                    <thead class="THeadGridTable">
                        <tr>
                            <th class="THead-Th">Store
                            </th>
                           

                            <th class="THead-Th">Invoice #
                            </th>
                            <th class="THead-Th">Date
                            </th>
                            <th class="THead-Th">Type
                            </th>
                           
                             <th class="THead-Th">Register Name
                            </th>
                                   <th class="THead-Th">Sync Number
                            </th>
                                   <th class="THead-Th">Posted
                            </th>

                                 <th class="THead-Th">AccPac Shipment Number
                            </th>
                                 <th class="THead-Th">AccPac Order Entry Number
                            </th>
                                 <th class="THead-Th">AccPac Invoice Number
                            </th>
                               <th class="THead-Th">AccPac Pre Pyament Number
                            </th>
                               <th class="THead-Th">AccPac Refund Number
                            </th>
                                           </tr>
                    </thead>


                </table>
            </div>



        </div>
    </div>


</asp:Content>
