﻿var table;
var datefrom, dateto, AccpacSyncUniqueNumber, Posted = 2;
var PostedCheck = 0;

var NotPosted = 0;


function InnerPage_Load() {
    initControls();
    setPageTitle(getResource("InvoiceInquiry"));
    setSelectedMenuItem('.pos-icon');
}

function initControls() {
    LoadDatePicker('input:.date', 'mm-dd-yy');

    $('#btnSearch').click(BuildSearchCriteria);
    $('.Quick-Search-Controls').live('keydown', function (e) {
        if (e.keyCode == 13 || e.which == 13) {
            $('#btnSearch').click();
        }
    });
    POSInvoiceServiceURL = systemConfig.ApplicationURL + "Common/WebServices/InvoiceWebService.asmx/";
    //$('#btnSearch').attr('value', getResource("Refresh"));
    
    BuildSearchCriteria();
}

function BuildSearchCriteria() {
    debugger;
    datefrom = $('.DateFrom').val();
    dateto = $('.DateTo').val();
    AccpacSyncUniqueNumber = $('.AccpacSyncUniqueNumber').val();
    PostedCheck = $('.Posted').is(":checked");
    NotPosted = $('.NotPosted').is(":checked");
    if ($('.Posted').is(":checked")) {
        Posted = 1;
    }
  
    
  if ($('.NotPosted').is(":checked")) {

        Posted = 0;
    }

  

  if (PostedCheck && NotPosted) {
      Posted = 2;
  }


    var url = POSInvoiceServiceURL + "GETInvoiceInquir";
    var data = formatString('{datefrom:"{0}",dateto :"{1}" , AccpacSyncUniqueNumber:"{2}" , Posted:{3}}', datefrom, dateto, AccpacSyncUniqueNumber, Posted);
    post(url, data, FindResultsSuccess, function () { }, null);
    return;
}

function FindResultsSuccess(returnedValue) {
  


 debugger

    if (table)
        table.destroy();

    table = $('.result').DataTable({
        "bFilter": false,
        "data": returnedValue,
        "columns": [
            { "data": "StoreName" },
            { "data": "InvoiceNumber" },
            { "data": "InvoiceDate" },
            { "data": "TransactionType" },
            { "data": "RegisterName" },
            { "data": "SyncNumber" },
            { "data": "SyncStatus" },
            { "data": "AccpacShipmentNo" },
            { "data": "AccpacOrderentryNo" },
            { "data": "AccpacInvoiceNo" },
            { "data": "AccpacPrepyamentNo" },
            { "data": "AccpacRefundNo" }
                                
        ],
        dom: 'Bfrtip',
        buttons: [

            {
                extend: 'collection',
                text: 'Export',
                buttons: [
                    'copy',
                    'excel',
                    'csv',
                    'pdf',
                    'print'
                ]
            }
        ],

        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            debugger
            switch (aData.SyncStatus) {
                case 'No':
                    $(nRow).css('color', 'red')
                    break;
                case 'Yes':
                    $(nRow).css('color', 'green')
                    break;
               
            }
        }
        //"drawCallback": function (settings) {
        //    var api = this.api();
        //    var rows = api.rows({ page: 'current' }).nodes();
        //    var last = null;

        //    api.column(0, { page: 'current' }).data().each(function (group, i) {
        //        if (last !== group) {
        //            $(rows).eq(i).before(
        //                '<tr class="group"><td colspan="5">' + group + '</td></tr>'
        //            );

        //            last = group;
        //        }
        //    });
        //}

    });
    $('.table').find('tr').find('td').css('text-align', 'left');
    $('.footer').hide();

}
