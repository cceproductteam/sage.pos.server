﻿var InvoiceReportSchema;
var ExportPageURL;
var FromLocationAC;
var ToLocationAC;
var FromCustomerAC;
var ToCustomerAC;
var POSInvoiceServiceURL;
var table;

function InnerPage_Load() {
    NewcheckPermissions(this, systemEntities.POSDailyReceiptsreports, -1);
    
    if (havePer == 0) {

        $('.white-container-data').hide();
        showStatusMsg(getResource('vldDontHavePermission'));

    }
    initControls();
    setPageTitle(getResource("TotalDailySales"));
    InvoiceReportSchema = systemConfig.ReportSchema.TotalDailySales;
    setSelectedMenuItem('.pos-icon');
}

function initControls() {
    LoadDatePicker('input:.date', 'mm-dd-yy');
    StoreAC = $('select:.Store').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.StoreServiceURL + 'FindAllLite', required: true, multiSelect: false });

    $('.lnkReset').click(resetForm);
    $('#btnSearch').click(BuildSearchCriteria);
    $('.Quick-Search-Controls').live('keydown', function (e) {
        if (e.keyCode == 13 || e.which == 13) {
            $('#btnSearch').click();
        }
    });

    POSInvoiceServiceURL = systemConfig.ApplicationURL + "Common/WebServices/InvoiceWebService.asmx/";
    $('#btnSearch').attr('value', getResource("Search"));
    $('.lnkReset').attr('value', getResource("Reset"));
}

function BuildSearchCriteria() {
    if (!saveForm($('.Data-Table'))) {
        $('.validation').css({ top: "10%", left: "300px" });
        return false;
    }

    $('.search-criteria').val('');
    var argsCriteria = new Object();
    //argsCriteria.ExportPageURL = ExportPageURL;



    if ($('.InvoiceDateTo').val() != null)
        argsCriteria.to_date = $('.InvoiceDateTo').val();

    if ($('.InvoiceDateFrom').val() != null)
        argsCriteria.from_date = $('.InvoiceDateFrom').val();

    if (StoreAC.get_Item() != null) {
        argsCriteria.store_ids = StoreAC.get_Item().value;
        argsCriteria.store_name = StoreAC.get_Item().label;
    }


    var url = POSInvoiceServiceURL + "GetPOSTotalDailySales";
    var data = formatString('{storeId:"{0}",datefrom :"{1}" , dateto:"{2}"}', argsCriteria.store_ids, argsCriteria.from_date, argsCriteria.to_date);
    post(url, data, FindResultsSuccess, function () { }, null);
    return;
    argsCriteria.all_stores = $('input.AllStores').prop("checked") ? true : false



    var SearchCriteria = '{';
    for (var key in argsCriteria) {
        if (argsCriteria.hasOwnProperty(key))
            if (argsCriteria[key] != "") {
                SearchCriteria = SearchCriteria + ('"' + key + '":' + '"' + argsCriteria[key] + '",');
            }
    }


    if (SearchCriteria != '{') {
        SearchCriteria = SearchCriteria.slice(0, -1);
    }
    SearchCriteria = SearchCriteria + '}';
    var exportPageURL = ExportPageURL + formatString("schemaID={0}&SearchCriteria={1}", InvoiceReportSchema, SearchCriteria);
    window.open(exportPageURL, '_blank');
}

function resetForm() {
    $('.InvoiceDateTo').val('');
    $('.InvoiceDateFrom').val('');
    $('input.AllStores').attr("checked", false)
    StoreAC.clear();

}


function getdate(date) {
    var arr = date.split('-');
    return arr[1] + '-' + arr[0] + '-' + arr[2];
}


function FindResultsSuccess(returnedValue) {
    if (table)
        table.destroy();
    table = $('.result').DataTable({
        "bFilter": false,
        "data": returnedValue,
        "columns": [
              { "data": "StoreName" },
            { "data": "RegisterName" },
            { "data": "Date" },
             { "data": "TransactionNumber" },
          
            { "data": "PaymentTypeName" },
               { "data": "Currency" },
                { "data": "Amount" },
                { "data": "StoreCurrency" },
           
            { "data": "AmountStoreCurrency" }

        ],
        dom: 'Bfrtip',
        buttons: [

            {
                extend: 'collection',
                text: 'Export',
                buttons: [
                    'copy',
                    'excel',
                    'csv',
                    'pdf',
                    'print'
                ],
                "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $('td:eq(0)', nRow).css('text-align', 'left');
                }
            }
        ]
    });
    $('.table').find('tr').find('td').css('text-align', 'left');
    $('.footer').hide();

    $('.dt-buttons').find('a').removeClass('buttons-collection dt-button');
    $('.dt-buttons').find('a').addClass(' button2 custom-buttons-search');
    $('#DataTables_Table_0_wrapper').find('div').removeClass("dt-buttons");
    $('.custom-buttons-search').bind('click', function () {
        $('.dt-button-collection').find('a').removeClass("buttons-html5 dt-button").addClass('button3');
    });

}