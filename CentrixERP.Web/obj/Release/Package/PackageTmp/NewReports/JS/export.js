﻿$(document).ready(function () {
    $("#spinner").bind("ajaxSend", function () {
        $(this).show();
    }).bind("ajaxStop", function () {
        $(this).hide();
    }).bind("ajaxError", function () {
        $(this).hide();
    });
  
});


//Function that is called on Successful AJAX method call.  These are referenced in the "CallServerMethodBeforePrint" function that is created from code behind and will exist in the final rendering of the page.
function ServerCallSucceeded(result, context) {
    window.frames['frmPrint'].document.location.href = iFrameURL;
    window.frames['frmPrint'].focus();
    var timeout = window.setTimeout("window.frames[\"frmPrint\"].focus();window.frames[\"frmPrint\"].print();", 500);
    window.setTimeout("ServerCallAfterPrint(this)", 2000);
}

function ServerCallSucceededAfterPrint(result, context) {
}

//Function that is called on failure or error in AJAX method call. These are referenced in the "CallServerMethodBeforePrint" function that is created from code behind and will exist in the final rendering of the page.
function ServerCallFailed(result, context) {
}

function ServerCallBeforePrint(btn) {
    $('#spinner').show();
    var context = new Object();
    //example of passing multiple args
    context.flag = new Array('Today', 'Tomorrow');
    //This "CallServerMethodBeforePrint" function is created from code behind and will exist in the final rendering of the page
    CallServerMethodBeforePrint(context.flag, context);
}

function ServerCallAfterPrint(btn) {

    var context = new Object();
    //example of passing multiple args
    context.flag = new Array('Today', 'Tomorrow');
    //This "CallServerAfterPrint" function is created from code behind and will exist in the final rendering of the page
    CallServerAfterPrint(context.flag, context);
    $('#spinner').hide();
}

function printPDF(btn) {
    ServerCallBeforePrint(btn);
}

function showPrintButton() {
    var table = $("table[title='Refresh']");
    var parentTable = $(table).parents('table');
    var parentDiv = $(parentTable).parents('div').parents('div').first();
    var btnPrint = $("<input type='button' id='btnPrint' name='btnPrint' value='Print' style=\"font-family:Verdana;font-size:8pt;width:86px\"/>");
    var btnClose = $("<input type='button' id='btnClose' name='btnClose'value='Close' style=\"font-family:Verdana;font-size:8pt;width:86px\"/>");
    btnPrint.click(function () {
        printPDF(this);
    });
    btnClose.click(function () {
        window.close();
    });
    if (parentDiv.find("input[value='Print']").length == 0) {
        parentDiv.append('<table cellpadding="0" cellspacing="0" toolbarspacer="true" style="display:inline-block;width:6px;"><tbody><tr><td></td></tr></tbody></table>');
        parentDiv.append('<div id="customDiv" class=" " style="display:inline-block;font-family:Verdana;font-size:8pt;vertical-align:inherit;"><table cellpadding="0" cellspacing="0"><tbody><tr><td><span style="cursor:pointer;" class="HighlightDiv" onclick="javascript:printPDF(this);" ><img src="../Common/Images/printer.png" alt="Print Report" title="Print Report" width="18px" height="18px" style="margin-top:4px"/></span></td></tr></tbody></table></div>');
        parentDiv.append('<table cellpadding="0" cellspacing="0" toolbarspacer="true" style="display:inline-block;width:10px;"><tbody><tr><td></td></tr></tbody></table>');
    }
}
function cfnReportsViewer_ViewReport(selectedTreeKeyGuidValue, ReportName, VenueExamCounter) {
    var windowWidth = 1000;
    var windowHeight = 800;
    var left = (screen.width / 2) - (windowWidth / 2);
    var top = (screen.height / 2) - (windowHeight / 2);
    var myForm = document.getElementById("frmReportViewer");
    if (myForm) {
        myForm.target = "PopupReport";
    }
    $("#hfAccessObjectGuid").val(selectedTreeKeyGuidValue);
    $("#hfReportName").val(ReportName);
    $("#hfVenueExamCounter").val(VenueExamCounter);
    var thePopup = window.open("about:blank", "PopupReport", 'scrollbars=yes,status=yes,toolbar=yes,menubar=no,location=no,resizable=no,fullscreen=yes, width=' + windowWidth + ', height=' + windowHeight + ', top=' + top + ', left=' + left);
    window.setTimeout(document.getElementById("frmReportViewer").submit(), 500);
    return false;
}