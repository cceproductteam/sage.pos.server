﻿var InvoiceReportSchema;
var ExportPageURL;
var FromLocationAC;
var ToLocationAC;
var FromCustomerAC;
var ToCustomerAC;
var POSInvoiceServiceURL;
var table;

function InnerPage_Load() {
    NewcheckPermissions(this, systemEntities.POSDailyReceiptsreports, -1);

    if (havePer == 0) {

        $('.white-container-data').hide();
        showStatusMsg(getResource('vldDontHavePermission'));

    }
    initControls();
    setPageTitle(getResource("DailySalesSummation"));
    InvoiceReportSchema = systemConfig.ReportSchema.TotalDailySales;
    setSelectedMenuItem('.pos-icon');
}

function initControls() {
    LoadDatePicker('input:.date', 'mm-dd-yy');
    StoreAC = $('select:.Store').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.StoreServiceURL + 'FindAllLite', required: true, multiSelect: false });

    $('.lnkReset').click(resetForm);
    $('#btnSearch').click(BuildSearchCriteria);
    $('.Quick-Search-Controls').live('keydown', function (e) {
        if (e.keyCode == 13 || e.which == 13) {
            $('#btnSearch').click();
        }
    });

    POSInvoiceServiceURL = systemConfig.ApplicationURL + "Common/WebServices/InvoiceWebService.asmx/";
    $('#btnSearch').attr('value', getResource("Search"));
    $('.lnkReset').attr('value', getResource("Reset"));
}

function BuildSearchCriteria() {
    if (!saveForm($('.Data-Table'))) {
        $('.validation').css({ top: "10%", left: "300px" });
        return false;
    }

    $('.search-criteria').val('');
    var argsCriteria = new Object();
    //argsCriteria.ExportPageURL = ExportPageURL;



    if ($('.InvoiceDateTo').val() != null)
        argsCriteria.to_date = $('.InvoiceDateTo').val();

    if ($('.InvoiceDateFrom').val() != null)
        argsCriteria.from_date = $('.InvoiceDateFrom').val();

    if (StoreAC.get_Item() != null) {
        argsCriteria.store_ids = StoreAC.get_Item().value;
        argsCriteria.store_name = StoreAC.get_Item().label;
    }


    var url = POSInvoiceServiceURL + "GetPOSDaileSalesSummationReport";
    var data = formatString('{storeId:"{0}",datefrom :"{1}" , dateto:"{2}"}', argsCriteria.store_ids, argsCriteria.from_date, argsCriteria.to_date);
    post(url, data, FindResultsSuccess, function () { }, null);
    return;
    argsCriteria.all_stores = $('input.AllStores').prop("checked") ? true : false



    var SearchCriteria = '{';
    for (var key in argsCriteria) {
        if (argsCriteria.hasOwnProperty(key))
            if (argsCriteria[key] != "") {
                SearchCriteria = SearchCriteria + ('"' + key + '":' + '"' + argsCriteria[key] + '",');
            }
    }


    if (SearchCriteria != '{') {
        SearchCriteria = SearchCriteria.slice(0, -1);
    }
    SearchCriteria = SearchCriteria + '}';
    var exportPageURL = ExportPageURL + formatString("schemaID={0}&SearchCriteria={1}", InvoiceReportSchema, SearchCriteria);
    window.open(exportPageURL, '_blank');
}

function resetForm() {
    $('.InvoiceDateTo').val('');
    $('.InvoiceDateFrom').val('');
    $('input.AllStores').attr("checked", false)
    StoreAC.clear();

}


function getdate(date) {
    var arr = date.split('-');
    return arr[1] + '-' + arr[0] + '-' + arr[2];
}


function FindResultsSuccess(returnedValue) {

    debugger;
    if (($('.dataTables_empty').text() == "No data available in table") && returnedValue == null) {
        return;
    }

    if (returnedValue == null && table) {
        table.clear();
        table.draw();
    }
    table = $('.result').DataTable({
        "bFilter": false,
        "data": returnedValue,
        "destroy": true,
        "columns": [
              { "data": "StoreName" },
            { "data": "RegisterName" },
            { "data": "InvoiceDate" },
            { "data": "TotalSales" },
               { "data": "TotalRefund" },
                { "data": "TotalExchange" },
                { "data": "Total" }



        ],
        "columnDefs": [
        { "className": "dt-center", "targets": "_all" }
        ],
        dom: 'Bfrtip',
        buttons: [

            {
                extend: 'collection',
                text: 'Export',
                buttons: [
<<<<<<< HEAD
             {
                 extend: 'csv',
                 title: getResource("DailySalesSummation"),
             },
             {
                 extend: 'copy',
                 title: getResource("DailySalesSummation"),
             },
             {
                 extend: 'excel',
                 title: getResource("DailySalesSummation"),
             },
              {
                  extend: 'pdf',
                  title: getResource("DailySalesSummation"),
              },
              {
                  extend: 'print',
                  title: getResource("DailySalesSummation"),
              },
=======
                    'copy',
                    'excel',
                    'csv',
                    'pdf',
                    'print'
>>>>>>> fa8684950922d126387c9349cd0262141a38caf4
                ],

            }
        ],
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
<<<<<<< HEAD
=======
            debugger
>>>>>>> fa8684950922d126387c9349cd0262141a38caf4
            $('td:eq(3)', nRow).html(parseFloat(aData.TotalSales).toFixed(3));
            $('td:eq(4)', nRow).html(parseFloat(aData.TotalRefund).toFixed(3));
            $('td:eq(5)', nRow).html(parseFloat(aData.TotalExchange).toFixed(3));
            $('td:eq(6)', nRow).html(parseFloat(aData.Total).toFixed(3));


            return nRow
        }
    });

    $('.footer').hide();
    $('.dt-buttons').find('a').removeClass('buttons-collection dt-button');
    $('.dt-buttons').find('a').addClass(' button2 custom-buttons-search');
    $('#DataTables_Table_0_wrapper').find('div').removeClass("dt-buttons");
    $('#DataTables_Table_0_wrapper').find('div').addClass("filter-reset");
 
    $('.custom-buttons-search').bind('click', function () {
        $('.dt-button-collection').find('a').removeClass("buttons-html5 dt-button").addClass('button3');
    });
}


