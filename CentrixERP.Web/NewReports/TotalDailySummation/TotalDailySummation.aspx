﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="POSTotalDailySales.aspx.cs"
    Inherits="SagePOSServer.Web.POSTotalDailySales.POSTotalDailySales"
    MasterPageFile="~/Common/MasterPage/ERPSite.master" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="PageHeader">
    <script src="../../Common/DataDictionary/DataDictionary-ar.js" type="text/javascript"></script>
    <script src="../../Common/DataDictionary/DataDictionary-en.js" type="text/javascript"></script>
    <link href="../CSS/ReportCSS.css?v=<%=Centrix_Version %>" rel="stylesheet" type="text/css" />
    <script src="JS/POSTotalDailySales.js?v=<%=Centrix_Version %>" type="text/javascript"></script>

    <script src="../../Common/JScript/DataTables/JS/jquery.dataTables.min.js"></script>

    <script src="../../Common/JScript/DataTables/JS/jquery.dataTables.js"></script>
    <script src="../../Common/JScript/DataTables/JS/dataTables.buttons.min.js"></script>

    <script src="../../Common/JScript/DataTables/JS/buttons.flash.min.js"></script>
            <script src="../../Common/JScript/DataTables/JS/buttons.flash.min.js"></script>
     <script src="../../Common/JScript/DataTables/JS/pdfmake.min.js"></script>

    <script src="../../Common/JScript/DataTables/JS/jszip.min.js"></script>
    <script src="../../Common/JScript/DataTables/JS/vfs_fonts.js"></script>
    <script src="../../Common/JScript/DataTables/JS/vfs_fonts.js"></script>

    <script src="../../Common/JScript/DataTables/JS/buttons.html5.min.js"></script>
    <script src="../../Common/JScript/DataTables/JS/buttons.print.min.js"></script>


    <script src="../../Common/JScript/DataTables/JS/dataTables.material.min.js"></script>
    <link href="../../Common/JScript/DataTables/CSS/dataTables.material.css" rel="stylesheet" />
    <link href="../../Common/JScript/DataTables/CSS/material.min.css" rel="stylesheet" />
    <link href="../../Common/JScript/DataTables/CSS/buttons.dataTables.min.css" rel="stylesheet" />

    <style>
        .THead-Th {
            color: black !important;
            text-align: left !important;
        }

        .dt-buttons {
            float: right !important;
        }
    </style>
    <script type="text/javascript">
        ExportPageURL = '<%=ExportPageURL %>';
    </script>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="PageBody">
    <div class="">
        <div class="">
            <div class="Details-div">
              
                <div class="Action-div">
                    <ul>
                    </ul>
                </div>
            </div>
             <fieldset style="border: groove; width: 55%; border-color: #EEEEEE; border-radius: 15px;">
                <legend>Search</legend>
            <table cellspacing="0" cellpadding="0" border="0" class="Data-Table">
                <tbody>
                    <tr>
                        <th>
                            <span class="label-title" resourcekey="InvoiceDateFrom"></span>
                        </th>
                        <td>
                            <input class="cx-control InvoiceDateFrom  date" type="text">
                        </td>
                        <th>
                            <span class="label-title" resourcekey="InvoiceDateTo"></span>
                        </th>
                        <td>
                            <input class="cx-control InvoiceDateTo  date" type="text">
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <span class="label-title" resourcekey="Store"></span>
                        </th>
                        <td>
                            <select class="lst Store">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="filter-reset"  style="width: 724%;">
                                <input type="button" class="lnkReset-InvoiceInquiry button-cancel button2" id="btnSearch" value="Search" />
                                <input value="Reset" class="lnkReset lnkReset-InvoiceInquiry button-cancel button2"
                                    type="button" />
                            </div>
                        </td>
                    </tr>

                </tbody>
            </table>
                </fieldset>
            <br />
            <div class='table'>
                <table class="display result mdl-data-table dataTable " cellspacing="0" width="100%">
                    <thead class="THeadGridTable">
                        <tr>
                            <th class="THead-Th">Store Name
                            </th>
                            <th class="THead-Th">Register Name
                            </th>
                            <th class="THead-Th">Date
                            </th>
                            <th class="THead-Th">Receipt Type
                            </th>
                            <th class="THead-Th">Currency
                            </th>
                            <th class="THead-Th">Amount
                            </th>
                            <th class="THead-Th">Store Currency
                            </th>
                            <th class="THead-Th">Amount in Store Currency
                            </th>

                        </tr>
                    </thead>


                </table>
            </div>



        </div>
    </div>
</asp:Content>
