﻿var InvoiceReportSchema;
var ExportPageURL;
var FromLocationAC;
var ToLocationAC;
var FromCustomerAC;
var ToCustomerAC;
var POSInvoiceServiceURL;
var table;

function InnerPage_Load() {
    NewcheckPermissions(this, systemEntities.POSDailyReceiptsreports, -1);
    
    if (havePer == 0) {

        $('.white-container-data').hide();
        showStatusMsg(getResource('vldDontHavePermission'));

    }
    initControls();
    setPageTitle(getResource("TotalDailySalesSummation"));
    InvoiceReportSchema = systemConfig.ReportSchema.TotalDailySales;
    setSelectedMenuItem('.pos-icon');
}

function initControls() {
    LoadDatePicker('input:.date', 'mm-dd-yy');
    StoreAC = $('select:.Store').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.StoreServiceURL + 'FindAllLite', required: true, multiSelect: false });

    $('.lnkReset').click(resetForm);
    $('#btnSearch').click(BuildSearchCriteria);
    $('.Quick-Search-Controls').live('keydown', function (e) {
        if (e.keyCode == 13 || e.which == 13) {
            $('#btnSearch').click();
        }
    });

    POSInvoiceServiceURL = systemConfig.ApplicationURL + "Common/WebServices/InvoiceWebService.asmx/";
    $('#btnSearch').attr('value', getResource("Search"));
    $('.lnkReset').attr('value', getResource("Reset"));
}

function BuildSearchCriteria() {
    if (!saveForm($('.Data-Table'))) {
        $('.validation').css({ top: "10%", left: "300px" });
        return false;
    }

    $('.search-criteria').val('');
    var argsCriteria = new Object();
    //argsCriteria.ExportPageURL = ExportPageURL;

    if ($('.InvoiceDateTo').val() != null)
        argsCriteria.to_date = $('.InvoiceDateTo').val();

    if ($('.InvoiceDateFrom').val() != null)
        argsCriteria.from_date = $('.InvoiceDateFrom').val();

    if (StoreAC.get_Item() != null) {
        argsCriteria.store_ids = StoreAC.get_Item().value;
        argsCriteria.store_name = StoreAC.get_Item().label;
    }


<<<<<<< HEAD:CentrixERP.Web/NewReports/TotalDailySales/JS/POSTotalDailySales.js
    var url = POSInvoiceServiceURL + "GetPOSTotalDailySales";
=======
    var url = POSInvoiceServiceURL + "GetPOSTotalDailySalesSummation";
>>>>>>> fa8684950922d126387c9349cd0262141a38caf4:CentrixERP.Web/NewReports/TotalDailySummation/JS/POSTotalDailySales.js
    var data = formatString('{storeId:"{0}",datefrom :"{1}" , dateto:"{2}"}', argsCriteria.store_ids, argsCriteria.from_date, argsCriteria.to_date);
    post(url, data, FindResultsSuccess, function () { }, null);
    return;
    argsCriteria.all_stores = $('input.AllStores').prop("checked") ? true : false



    var SearchCriteria = '{';
    for (var key in argsCriteria) {
        if (argsCriteria.hasOwnProperty(key))
            if (argsCriteria[key] != "") {
                SearchCriteria = SearchCriteria + ('"' + key + '":' + '"' + argsCriteria[key] + '",');
            }
    }


    if (SearchCriteria != '{') {
        SearchCriteria = SearchCriteria.slice(0, -1);
    }
    SearchCriteria = SearchCriteria + '}';
    var exportPageURL = ExportPageURL + formatString("schemaID={0}&SearchCriteria={1}", InvoiceReportSchema, SearchCriteria);
    window.open(exportPageURL, '_blank');
}

function resetForm() {
    $('.InvoiceDateTo').val('');
    $('.InvoiceDateFrom').val('');
    $('input.AllStores').attr("checked", false)
    StoreAC.clear();

}


function getdate(date) {
    var arr = date.split('-');
    return arr[1] + '-' + arr[0] + '-' + arr[2];
}


function FindResultsSuccess(returnedValue) {
<<<<<<< HEAD:CentrixERP.Web/NewReports/TotalDailySales/JS/POSTotalDailySales.js

    if (($('.dataTables_empty').text() == "No data available in table") && returnedValue == null) {
        return;
    }
=======
    if (table)
        table.destroy();
>>>>>>> fa8684950922d126387c9349cd0262141a38caf4:CentrixERP.Web/NewReports/TotalDailySummation/JS/POSTotalDailySales.js

    if (returnedValue == null && table) {
        table.clear();
        table.draw();
    }
<<<<<<< HEAD:CentrixERP.Web/NewReports/TotalDailySales/JS/POSTotalDailySales.js
=======

>>>>>>> fa8684950922d126387c9349cd0262141a38caf4:CentrixERP.Web/NewReports/TotalDailySummation/JS/POSTotalDailySales.js
    table = $('.result').DataTable({
        "bFilter": false,
        "data": returnedValue,
        "destroy": true,
        "columns": [
              { "data": "StoreName" },
            { "data": "RegisterName" },
<<<<<<< HEAD:CentrixERP.Web/NewReports/TotalDailySales/JS/POSTotalDailySales.js
            { "data": "Date" },
             { "data": "TransactionNumber" },
          
=======
            { "data": "Date" },          
>>>>>>> fa8684950922d126387c9349cd0262141a38caf4:CentrixERP.Web/NewReports/TotalDailySummation/JS/POSTotalDailySales.js
            { "data": "PaymentTypeName" },
               { "data": "Currency" },
                { "data": "Amount" },
                { "data": "StoreCurrency" },
           
            { "data": "AmountStoreCurrency" }

        ],
        "columnDefs": [
        { "className": "dt-center", "targets": "_all" }
      ],
        dom: 'Bfrtip',
        buttons: [

            {
                extend: 'collection',
              
                text: 'Export',
                buttons: [
<<<<<<< HEAD:CentrixERP.Web/NewReports/TotalDailySales/JS/POSTotalDailySales.js
             {
                 extend: 'csv',
                 title: getResource("TotalDailySales"),
             },
             {
                 extend: 'copy',
                 title: getResource("TotalDailySales"),
             },
             {
                 extend: 'excel',
                 title: getResource("TotalDailySales"),
             },
              {
                  extend: 'pdf',
                  title: getResource("TotalDailySales"),
              },
              {
                  extend: 'print',
                  title: getResource("TotalDailySales"),
              },
                ],
                "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
          
=======
                    'copy',
                    'excel',
                    'csv',
                    'pdf',
                    'print'
                ],
                "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $('td:eq(0)', nRow).css('text-align', 'left');
>>>>>>> fa8684950922d126387c9349cd0262141a38caf4:CentrixERP.Web/NewReports/TotalDailySummation/JS/POSTotalDailySales.js
                }
            }
        ]
    });

    $('.footer').hide();
    $('.dt-buttons').find('a').removeClass('buttons-collection dt-button');
    $('.dt-buttons').find('a').addClass(' button2 custom-buttons-search');
    $('#DataTables_Table_0_wrapper').find('div').removeClass("dt-buttons");
    $('#DataTables_Table_0_wrapper').find('div').addClass("filter-reset");
<<<<<<< HEAD:CentrixERP.Web/NewReports/TotalDailySales/JS/POSTotalDailySales.js
    $('.dataTables_empty').css("text-align", "left");
=======
>>>>>>> fa8684950922d126387c9349cd0262141a38caf4:CentrixERP.Web/NewReports/TotalDailySummation/JS/POSTotalDailySales.js
    $('.custom-buttons-search').bind('click', function () {
        $('.dt-button-collection').find('a').removeClass("buttons-html5 dt-button").addClass('button3');
    });

}