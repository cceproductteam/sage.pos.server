﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CloseRegisterReport.aspx.cs" Inherits="SagePOSServer.Web.CloseRegisterReport.CloseRegisterReport" MasterPageFile="~/Common/MasterPage/ERPSite.master" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="PageHeader">
    <script src="../../Common/DataDictionary/DataDictionary-ar.js" type="text/javascript"></script>
    <script src="../../Common/DataDictionary/DataDictionary-en.js" type="text/javascript"></script>
    <link href="../CSS/ReportCSS.css?v=<%=Centrix_Version %>" rel="stylesheet" type="text/css" />
    <script src="JS/CloseRegisterReport.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <script src="../../Common/JScript/DataTables/JS/jquery.dataTables.min.js"></script>

    <script src="../../Common/JScript/DataTables/JS/jquery.dataTables.js"></script>
    <script src="../../Common/JScript/DataTables/JS/dataTables.buttons.min.js"></script>

    <script src="../../Common/JScript/DataTables/JS/buttons.flash.min.js"></script>
        <script src="../../Common/JScript/DataTables/JS/buttons.flash.min.js"></script>
     <script src="../../Common/JScript/DataTables/JS/pdfmake.min.js"></script>

    <script src="../../Common/JScript/DataTables/JS/jszip.min.js"></script>
    <script src="../../Common/JScript/DataTables/JS/vfs_fonts.js"></script>
    <script src="../../Common/JScript/DataTables/JS/vfs_fonts.js"></script>

    <script src="../../Common/JScript/DataTables/JS/buttons.html5.min.js"></script>
    <script src="../../Common/JScript/DataTables/JS/buttons.print.min.js"></script>


    <script src="../../Common/JScript/DataTables/JS/dataTables.material.min.js"></script>
    <link href="../../Common/JScript/DataTables/CSS/dataTables.material.css" rel="stylesheet" />
    <link href="../../Common/JScript/DataTables/CSS/material.min.css" rel="stylesheet" />
     <link href="../../Common/JScript/DataTables/CSS/jquery.dataTables.min" rel="stylesheet" />
    <link href="../../Common/JScript/DataTables/CSS/buttons.dataTables.min.css" rel="stylesheet" />
       
    <style>
        .THead-Th {
            color: black !important;
            text-align: left !important;
        }

        .dt-buttons {
            float: right !important;
        }
    </style>
    <script type="text/javascript">
        ExportPageURL = '<%=ExportPageURL %>';
    </script>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="PageBody">
    <div class="">
        <div class="">
            <div class="Details-div">

                <div class="Action-div">
                    <ul>
                    </ul>
                </div>
            </div>
            <fieldset style="border: groove; width: 55%; border-color: #EEEEEE; border-radius: 15px;">
                <legend>Search</legend>
                <table cellspacing="0" cellpadding="0" border="0" class="Data-Table">
                    <tbody>
                        <tr>
                            <th>
                                <span class="label-title" resourcekey="Store"></span>
                            </th>
                            <td>
                                <select class="lst Store">
                                </select>
                            </td>
                             <th>
                                <span class="label-title" resourcekey="Register"></span>
                            </th>
                            <td>
                                <select class="lst Register">
                                </select>
                            </td>

                            <th style="display: none;" >
                                <span class="label-title" resourcekey="ItemNumber"></span>
                            </th>
                            <td style="display: none;">
                                <select class="lst ItemNumber">
                                </select>
                            </td>

                        </tr>
                        <tr style="display: none;" >
                            <th>
                                <span class="label-title" resourcekey="InvoiceNumberFrom"></span>
                            </th>
                            <td>
                                <input class="cx-control InvoiceNumber  string" type="text">
                            </td>
                            <th>
                                <span class="label-title" resourcekey="InvoiceNumberTo"></span>
                            </th>
                            <td>
                                <input class="cx-control InvoiceNumberTo  string" type="text">
                            </td>

                        </tr>
                        <tr style="display: none;">
                            <th>
                                <span class="label-title" resourcekey="ItemStructure"></span>
                            </th>
                            <td>
                                <select class="lst ItemStructure">
                                </select>
                            </td>
                            <th>
                                <span class="label-title" resourcekey="Segment"></span>
                            </th>
                            <td>
                                <select class="lst Segment">
                                </select>
                            </td>
                        </tr>
                        <tr style="display: none;">
                            <th>
                                <span class="label-title" resourcekey="SegmentCodeFrom"></span>
                            </th>
                            <td>
                                <select class="lst SegmentCodeFrom">
                                </select>
                            </td>
                            <th>
                                <span class="label-title" resourcekey="SegmentCodeTo"></span>
                            </th>
                            <td>
                                <select class="lst SegmentCodeTo">
                                </select>
                            </td>
                        </tr>
                        <tr style="display: none">
                            <th>
                                <span id="span6" resourcekey="LessThanDiscount"></span>
                            </th>
                            <td>
                                <input class="cx-control txtLessThanDiscount  money" type="text" />
                            </td>
                            <th>
                                <span id="span7" resourcekey="GreaterThanDiscount" font-bold="true"></span>
                            </th>
                            <td>
                                <input class="cx-control txtGreaterThanDiscount  money" type="text" />
                            </td>
                        </tr>
                        <tr style="display: none;">
                            <th>
                                <span class="label-title" resourcekey="OpenedCloseRegister"></span>
                            </th>
                            <td>
                                <input type="checkbox" class="cx-control AllItem chk " />
                            </td>
                        </tr>
                        <tr>
                            <th>
                                <span class="label-title" resourcekey="ClosedCloseRegister"></span>
                            </th>
                            <td>
                                <input class="cx-control InvoiceDateFrom  date " type="text">
                            </td>
                            <th>
                                <span class="label-title" resourcekey="OpenedCloseRegister"></span>
                            </th>
                            <td>
                                <input class="cx-control InvoiceDateTo  date  " type="text">
                            </td>

                        </tr>

                        <tr>
                            <td>
                                <div class="filter-reset">
                                    <input type="button" class="lnkReset-InvoiceInquiry button-cancel button2" id="btnSearch" value="Search" />
                                    <input value="Reset" class="lnkReset lnkReset-InvoiceInquiry button-cancel button2"
                                        type="button" />
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </fieldset>
            <br />
            <div class='table'>
                <table class="display result mdl-data-table dataTable " cellspacing="0" width="100%">
                    <thead class="THeadGridTable">
                        <tr>
                            

                               <th class="THead-Th">Session Number
                            </th>
                            <th class="THead-Th">Store Name
                            </th>
                            <th class="THead-Th">Register Name 
                            </th>
                                 <th class="THead-Th">User Name 
                            </th>
                           
                                 <th class="THead-Th">Opened Date
                            </th>

                               <th class="THead-Th">Closed Date
                            </th>
                            
                               <th class="THead-Th">Total Transaction
                            </th>
                                    <th class="THead-Th">Counted
                            </th>
                            
                             <th class="THead-Th">Difference
                            </th>
                          
                            
                          

                         

                             
                        </tr>
                    </thead>


                </table>
            </div>

        </div>
    </div>
</asp:Content>
