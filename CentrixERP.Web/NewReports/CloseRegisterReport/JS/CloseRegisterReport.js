﻿var InvoiceReportSchema;
var ExportPageURL;
var FromLocationAC;
var ToLocationAC;
var FromCustomerAC;
var ToCustomerAC;
var ItemAC;
var table;


function InnerPage_Load() {

    NewcheckPermissions(this, systemEntities.POSsalesDetailsReports, -1);

    if (havePer == 0) {

        $('.white-container-data').hide();
        showStatusMsg(getResource('vldDontHavePermission'));

    }
    initControls();
    setPageTitle(getResource("CloseRegisterReport"));
    InvoiceReportSchema = systemConfig.ReportSchema.DailySalesPerItem;
    setSelectedMenuItem('.pos-icon');
        $('.mdl-data-table').css("white-space", "normal");
    //$('.mdl-data-table').css("font-size", "11.5px");


//    $('#btnSearch').css("white-space" , "normal");
}

function initControls() {
    LoadDatePicker('input:.date', 'mm-dd-yy');
    ItemAC = $('select:.ItemNumber').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.BOItemCardWebServiceURL + 'FindAllLite', required: false, multiSelect: true });

    ItemStructureAC = $('select:.ItemStructure').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.ItemStructureServiceURL + 'FindAllLite', required: false });

    SegmentAC = $('select:.Segment').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.IcSegmentServiceURL + 'FindAllLite', required: false });
    SegmentCodeFromAC = $('select:.SegmentCodeFrom').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.IcSegmentCodeServiceURL + 'FindAllLite', required: false });
    SegmentCodeToAC = $('select:.SegmentCodeTo').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.IcSegmentCodeServiceURL + 'FindAllLite', required: false});
    StoreAC = $('select:.Store').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.StoreServiceURL + 'FindAllLite', required: false, multiSelect: false });
    RegisterAC = $('select:.Register').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.RegisterServiceURL + 'FindAllLite', required: false, multiSelect: false });

    POSInvoiceServiceURL = systemConfig.ApplicationURL + "Common/WebServices/CloseRegisterWebService.asmx/";


    $('.lnkReset').click(resetForm);
    $('#btnSearch').click(BuildSearchCriteria);
    $('.Quick-Search-Controls').live('keydown', function (e) {
        if (e.keyCode == 13 || e.which == 13) {
            $('.btnSearch').click();
        }
    });

    $('.btnSearch').attr('value', getResource("Search"));
    $('.lnkReset').attr('value', getResource("Reset"));
}

function BuildSearchCriteria() {


    if (!saveForm($('.Quick-Search-Controls'))) {
        return false;
    }


    $('.search-criteria').val('');
    var argsCriteria = new Object();
    //argsCriteria.ExportPageURL = ExportPageURL;

    if (ItemAC.get_Item() != null) {
        var itemNumbers = '';
        $.each(ItemAC.get_Items(), function (index, itemm) {
            itemNumbers += itemm.ItemNumber + ',';

        });
        argsCriteria.item_numbers = itemNumbers;
    
    }
    if (ItemStructureAC.get_Item() != null)
        argsCriteria.item_structure = ItemStructureAC.get_Item().ItemStuctureId;
    if (SegmentAC.get_Item() != null)
        argsCriteria.segments = SegmentAC.get_Item().SegmentId;
    if (SegmentCodeFromAC.get_Item() != null)
        argsCriteria.segment_code_from = SegmentCodeFromAC.get_Item().SegmentCodeId;
    if (SegmentCodeToAC.get_Item() != null)
        argsCriteria.segment_code_to = SegmentCodeToAC.get_Item().SegmentCodeId;

    if (StoreAC.get_Item() != null) {
        argsCriteria.store_id = StoreAC.get_Item().value;
        argsCriteria.store_name = StoreAC.get_Item().label;
    }
    if (RegisterAC.get_Item() != null) {
        argsCriteria.register_id = RegisterAC.get_Item().value;
        argsCriteria.register_name = RegisterAC.get_Item().label;
    }

    if ($('.InvoiceNumber').val() != null)
        argsCriteria.invoice_number = $('.InvoiceNumber').val();

    if ($('.InvoiceNumberTo').val() != null)
        argsCriteria.to_invoice_number = $('.InvoiceNumberTo').val();

      

    if ($('.txtGreaterThanDiscount').val() != null)
        argsCriteria.greater_than_discount = $('.txtGreaterThanDiscount').val();

    if ($('.txtLessThanDiscount').val() != null)
        argsCriteria.less_than_discount = $('.txtLessThanDiscount').val();


    if ($('.InvoiceDateFrom').val() != null)
        argsCriteria.from_date = $('.InvoiceDateFrom').val();
    if ($('.InvoiceDateTo').val() != null)
        argsCriteria.to_date = $('.InvoiceDateTo').val();
    argsCriteria.all_items = $('input.AllItem').prop("checked") ? true : false
    if (!argsCriteria.store_id)
        argsCriteria.store_id = -1;
    if (argsCriteria.register_id == undefined)
        argsCriteria.register_id = null
    if (!argsCriteria.register_id)
        argsCriteria.register_id = -1;

    if (!argsCriteria.item_numbers)
        argsCriteria.item_numbers = null;
    var url = POSInvoiceServiceURL + "CloseRegisterInquiryResultsNew";
    var data = formatString('{storeId:"{0}",datefrom:"{1}",dateto:"{2}" , registerId:{3}}', argsCriteria.store_id, argsCriteria.from_date, argsCriteria.to_date, argsCriteria.register_id);

    post(url, data, FindResultsSuccess, function () { }, null);
    return;
    var SearchCriteria = '{';
    for (var key in argsCriteria) {
        if (argsCriteria.hasOwnProperty(key))
            if (argsCriteria[key] != "") {
                SearchCriteria = SearchCriteria + ('"' + key + '":' + '"' + argsCriteria[key] + '",');
            }
    }


    if (SearchCriteria != '{') {
        SearchCriteria = SearchCriteria.slice(0, -1);
    }
    SearchCriteria = SearchCriteria + '}';
    var exportPageURL = ExportPageURL + formatString("schemaID={0}&SearchCriteria={1}", InvoiceReportSchema, SearchCriteria);
    window.open(exportPageURL, '_blank');
}

function resetForm() {
    $('.InvoiceNumber,.InvoiceNumberTo').val('');
    $('.txtGreaterThanDiscount').val('');
    $('.txtGreaterThanDiscount').val('');
    $('.InvoiceDateFrom').val('');
    $('.InvoiceDateTo').val('')
    $('input.AllItem').attr("checked", false);
    ItemStructureAC.clear()
    SegmentAC.clear()
    SegmentCodeFromAC.clear()
    SegmentCodeToAC.clear();
    StoreAC.clear();
    ItemAC.clear();
    RegisterAC.clear();


  
}
    


function getdate(date) {
    var arr = date.split('-');
    return arr[1] + '-' + arr[0] + '-' + arr[2];
}

function FindResultsSuccess(returnedValue) {

debugger;

if (($('.dataTables_empty').text() == "No data available in table") && returnedValue.result == null) {
        return;
    }
    debugger

    if (returnedValue.result == null && table) {
        table.clear();
        table.draw();
    }

    table = $('.result').DataTable({
        "bFilter": false,
        "data": returnedValue.result,
        "destroy": true,
        widths: [200, 'auto', 'auto', 'auto', 6000, 6000, 'auto', 'auto', 'auto', 'auto'],
        "columnDefs": [
        {"className": "dt-center", "targets": "_all"}
        ],
        "columns": [
            
        { "data": "SessionNumber" },
            { "data": "StoreName" },
            { "data": "RegisterName" },
  
        { "data": "UserName" },
        
        { "data": "OpenedDate" },
            { "data": "ClosedDate" },
      
                     { "data": "TotalPrice" },
                     { "data": "amountSum" },
           { "data": "DifferenceAmount" }
   

        ],

        dom: 'Bfrtip',
        buttons: [

          {
              extend: 'collection',
              text: 'Export',
              buttons: [
                {
                    extend: 'copyHtml5',
<<<<<<< HEAD
                    title: getResource("CloseRegisterReport"),
=======
>>>>>>> fa8684950922d126387c9349cd0262141a38caf4
                    exportOptions: {
                        columns: ':visible'
                    },
        
                },
          {
              extend: 'excelHtml5',
<<<<<<< HEAD
              title: getResource("CloseRegisterReport"),
=======
>>>>>>> fa8684950922d126387c9349cd0262141a38caf4
              exportOptions: {
                  columns: ':visible'
              },

          },

                    {
                        extend: 'csvHtml5',
<<<<<<< HEAD
                        title: getResource("CloseRegisterReport"),
=======
>>>>>>> fa8684950922d126387c9349cd0262141a38caf4
                        exportOptions: {
                            columns: ':visible',
                          
                        },

                    },
          {

              extend: 'pdfHtml5',
<<<<<<< HEAD
              title: getResource("CloseRegisterReport"),
=======
>>>>>>> fa8684950922d126387c9349cd0262141a38caf4
            
              exportOptions: {
                  columns: ':visible',
        
                 
              },
       
              download: 'open',

          },
           {
<<<<<<< HEAD
               extend: 'print',
               title: getResource("CloseRegisterReport"),
=======
              extend: 'print',
>>>>>>> fa8684950922d126387c9349cd0262141a38caf4
              exportOptions: {
                  columns: ':visible'
              },

              autoPrint: true
          }
              ],

            

          }],

        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            if  (aData.DifferenceAmount >= 0) {
                
                    $(nRow).css('color', 'green')
                }
                   
            else
            {
                    $(nRow).css('color', 'red')                
            }


            if (aData.ClosedDate == null)

            {
<<<<<<< HEAD
    
=======
                debugger
>>>>>>> fa8684950922d126387c9349cd0262141a38caf4
                $('td:eq(5)', nRow).html("Still Open");

            }

            if (aData.OpenedDate != null) {
<<<<<<< HEAD
         
=======
                debugger
>>>>>>> fa8684950922d126387c9349cd0262141a38caf4
                $('td:eq(4)', nRow).html(aData.OpenedDate.replace(/\ص/g, 'AM').replace(/\م/g, 'PM'));

            }
            if (aData.ClosedDate != null) {
<<<<<<< HEAD
        
                $('td:eq(5)', nRow).html(aData.ClosedDate.replace(/\ص/g, 'AM').replace(/\م/g, 'PM'));
=======
                debugger
                $('td:eq(5)', nRow).html(aData.OpenedDate.replace(/\ص/g, 'AM').replace(/\م/g, 'PM'));
>>>>>>> fa8684950922d126387c9349cd0262141a38caf4

            }

           


            if (aData.TotalPrice != '') {


                $('td:eq(6)', nRow).html(aData.TotalPrice + " " + "(" + aData.defaultcurrsymbol + ")");
            }

            if (aData.amountSum != '') {


                $('td:eq(7)', nRow).html(aData.amountSum + " " + "(" + aData.defaultcurrsymbol + ")");
            }


            if (aData.DifferenceAmount != '') {


                $('td:eq(8)', nRow).html(aData.DifferenceAmount + " " + "(" + aData.defaultcurrsymbol + ")");
            }
             

            },
       
  
            "columnDefs": [
        { "className": "dt-center", "targets": "_all" , "sWidth": "10%"}
      ],

           

    });
   
    $('.footer').hide();

    $('.dt-buttons').find('a').removeClass('buttons-collection dt-button');
    $('.dt-buttons').find('a').addClass(' button2 custom-buttons-search2 ');
    $('#DataTables_Table_0_wrapper').find('div').removeClass("dt-buttons");
                //$('.dataTables_empty').css("text-align","left");
    $('#DataTables_Table_0_wrapper').find('div').addClass("filter-reset");
    $('.custom-buttons-search2 ').bind('click', function () {
        $('.dt-button-collection').find('a').removeClass("buttons-html5 dt-button").addClass('button3');
    });

}