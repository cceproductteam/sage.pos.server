﻿var InvoiceReportSchema;
var ExportPageURL;
var FromLocationAC;
var ToLocationAC;
var FromCustomerAC;
var ToCustomerAC;
var RegisterAC, CustomerAC, PersonAC;
var table;

function InnerPage_Load() {

    NewcheckPermissions(this, systemEntities.POSDailysales, -1);

    if (havePer == 0)
    {

        $('.white-container-data').hide();
        showStatusMsg(getResource('vldDontHavePermission'));

    }
        initControls()
        setPageTitle(getResource("Dailysales"));
        InvoiceReportSchema = systemConfig.ReportSchema.Dailysales;
        setSelectedMenuItem('.pos-icon');

        $('.mdl-data-table').css("font-size", "13.5px");

        $('.mdl-data-table').css("white-space", "");
}

function initControls() {
    LoadDatePicker('input:.date', 'mm-dd-yy');
    ItemAC = $('select:.ItemNumber').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.BOItemCardWebServiceURL + 'FindAllLite', required: false, multiSelect: true });
    StoreAC = $('select:.Store').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.StoreServiceURL + 'FindAllLite', required: false, multiSelect: false });
    RegisterAC = $('select:.Register').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.RegisterServiceURL + 'FindAllLite', required: false, multiSelect: true });
    CustomerAC = $('select:.Customer').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.PersonServiceURL + 'FindAllLite', required: false, multiSelect: true });
    PersonAC = $('select:.Person').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.UsersServiceURL + 'FindUserAllLite', required: false, multiSelect: true });

    POSInvoiceServiceURL = systemConfig.ApplicationURL + "Common/WebServices/InvoiceWebService.asmx/";
    $('.lnkReset').click(resetForm);
    $('#btnSearch').click(BuildSearchCriteria);
    $('.Quick-Search-Controls').live('keydown', function (e) {
        if (e.keyCode == 13 || e.which == 13) {
            $('.btnSearch').click();
        }
    });

    $('.btnSearch').attr('value', getResource("Search"));
    $('.lnkReset').attr('value', getResource("Reset"));
}

function BuildSearchCriteria() {


    if (!saveForm($('.Quick-Search-Controls'))) {
        return false;
    }


    $('.search-criteria').val('');
    var argsCriteria = new Object();
    //argsCriteria.ExportPageURL = ExportPageURL;

    if (ItemAC.get_Item() != null) {
        var itemNumbers = '';
        $.each(ItemAC.get_Items(), function (index, itemm) {
            itemNumbers += itemm.ItemNumber + ',';

        });
        argsCriteria.item_number = itemNumbers;

    }
   

    if ($('.InvoiceDateTo').val() != null)
        argsCriteria.to_date = $('.InvoiceDateTo').val();

    if ($('.InvoiceDateFrom').val() != null)
        argsCriteria.from_date = $('.InvoiceDateFrom').val();

    if (StoreAC.get_Item() != null) {
        argsCriteria.store_id = StoreAC.get_Item().value;
        argsCriteria.store_name = StoreAC.get_Item().label;
    }
    if (RegisterAC.get_Item() != null)
        argsCriteria.register_id = RegisterAC.get_ItemsIds();
    if (CustomerAC.get_Item() != null)
        argsCriteria.customer_id = CustomerAC.get_ItemsIds();
    if (PersonAC.get_Item() != null)
        argsCriteria.sales_man_ids = PersonAC.get_ItemsIds();

    if ($('.InvoiceNumber').val() != null) {
        argsCriteria.invoice_number = $('.InvoiceNumber').val();
    }

    if ($('.InvoiceNumberTo').val() != null) {
        argsCriteria.invoice_number_to = $('.InvoiceNumberTo').val();
    }
    if (!argsCriteria.store_id)
        argsCriteria.store_id = -1;


    if (!argsCriteria.item_number)
        argsCriteria.item_number = null;

    var url = POSInvoiceServiceURL + "GetPOSSummaryDailySales";
    var data = formatString('{storeId:"{0}",itemNumber :"{1}" , invoiceNumber:"{2}",invoiceDateFrom:"{3}",datefrom:"{4}",dateTo:"{5}"}', argsCriteria.store_id, argsCriteria.item_number, argsCriteria.invoice_number, argsCriteria.invoice_number, argsCriteria.from_date, argsCriteria.to_date);
    post(url, data, FindResultsSuccess, function () { }, null);
    return;

        
    var SearchCriteria = '{';
    for (var key in argsCriteria) {
        if (argsCriteria.hasOwnProperty(key))
            if (argsCriteria[key] != "") {
                SearchCriteria = SearchCriteria + ('"' + key + '":' + '"' + argsCriteria[key] + '",');
            }
    }


    if (SearchCriteria != '{') {
        SearchCriteria = SearchCriteria.slice(0, -1);
    }
    SearchCriteria = SearchCriteria + '}';
    var exportPageURL = ExportPageURL + formatString("schemaID={0}&SearchCriteria={1}", InvoiceReportSchema, SearchCriteria);
    window.open(exportPageURL, '_blank');
}

function resetForm() {
    StoreAC.clear();
    RegisterAC.clear();
    CustomerAC.clear();
    ItemAC.clear();
    PersonAC.clear();
    $('.ItemNumber').val('');
    $('.InvoiceDateTo').val('');
    $('.InvoiceDateFrom').val('');
    $('.InvoiceNumberTo,.InvoiceNumber').val('');
}


function getdate(date) {
    var arr = date.split('-');
    return arr[1] + '-' + arr[0] + '-' + arr[2];
}

function FindResultsSuccess(returnedValue) {
    var table;

    if (table)
        table.destroy();


    if (returnedValue == null && table) {
        table.clear();
        table.draw();
    }
    table = $('.result').DataTable({
        "bFilter": false,
        "data": returnedValue,
        "columns": [
            { "data": "StoreName" },
            { "data": "InvoiceNumber" },
            { "data": "InvoiceDate" },
            { "data": "TransactionType" },
            { "data": "SalesManName" },
            { "data": "TotalUnitPrice" },
            { "data": "ItemDiscount" },
            { "data": "TaxAmount" },
            { "data": "GrandTotal" },
             { "data": "Payments" },
        ],
        dom: 'Bfrtip',
        buttons: [

            {
                extend: 'collection',
                text: 'Export',
                buttons: [
            {
                extend: 'csv',
                title: getResource("Dailysales"),
            },
            {
                extend: 'copy',
                title: getResource("Dailysales"),
            },
            {
                extend: 'excel',
                title: getResource("Dailysales"),
            },
             {
                 extend: 'pdf',
                 title: getResource("Dailysales"),
             },
             {
                 extend: 'print',
                 title: getResource("Dailysales"),
             },
                ],
            }
        ],


    });
    $('.table').find('tr').find('td').css('text-align', 'left');
    $('.footer').hide();

    $('.dt-buttons').find('a').removeClass('buttons-collection dt-button');
    $('.dt-buttons').find('a').addClass(' button2 custom-buttons-search2 ');

    $('#DataTables_Table_0_wrapper').find('div').removeClass("dt-buttons");
    $('#DataTables_Table_0_wrapper').find('div').addClass("filter-reset");
    $('.custom-buttons-search2 ').bind('click', function () {
        $('.dt-button-collection').find('a').removeClass("buttons-html5 dt-button").addClass('button3');
    });


}