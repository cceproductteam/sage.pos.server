﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GenerateBarcode.aspx.cs"
    MasterPageFile="~/Common/MasterPage/ERPSite.master" Inherits="Centrix.POS.Server.Web.POS.GenerateBarcode.GenerateBarcode" %>

<asp:Content runat="server" ContentPlaceHolderID="PageHeader" ID="PageHeader">
    <script src="JS/jquery.PrintArea.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <script src="JS/GenerateBarcode.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <link href="CSS/GenerateCode_<%=Lang %>.css?v=<%=Centrix_Version %>" rel="stylesheet"
        type="text/css" />

       
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="PageBody" ID="PageBody">
    <div class="white-container" style="height: 560px;">
        <div class="white-container-data" style="height: 560px;">
            <div class="Action-div">
                <ul>
                    <li class="print-pnl viewedit-display-block"><span class="ActionPrint-link"></span><a
                        class="ActionPrint-link"><span resourcekey="Print"></span></a></li>
                </ul>
            </div>
            <div class="TabbedPanelsContentFieldset" style="height: 560px;">
                <div class="TabbedPanelsContent-info">
                    <fieldset class="Data-Fieldset generatebarcode-Fieldset">
                        <table border="0" cellpadding="0" cellspacing="0" class="Data-Table generateBarcode-table">
                            <tr>
                                <th>
                                    <span class="label-title" resourcekey="Item"></span><span class="smblreqyuired viewedit-display-none">
                                        *</span>
                                </th>
                                <td>
                                    <div>
                                        <select class="Item  lst">
                                        </select>
                                    </div>
                                </td>
                                <th>
                                    <span class="label-title" resourcekey="PriceList"></span><span class="smblreqyuired viewedit-display-none">
                                        *</span>
                                </th>
                                <td>
                                    <div>
                                        <select class="PriceList  lst">
                                        </select>
                                    </div>
                                </td>
                                <th>
                                    <span class="label-title" resourcekey="Currency"></span><span class="smblreqyuired viewedit-display-none">
                                        *</span>
                                </th>
                                <td>
                                    <div>
                                        <select class="Currency  lst">
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                            <td></td>
                                <td>
                                    <div>
                                        <button id="btn-generate-bar-code" class="check-perm-ok-btn button"><span id="Label63" resourcekey="GenerateBarcode">
                                           </span></button>
                                    </div>
                                </td>
                                <td></td>
                                <td colspan="3">
                                    <div class="viewDiv FloatLeft">
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
