﻿
function InnerPage_Load() {
    setSelectedMenuItem('.pos-icon');
    currentEntity = systemEntities.GenerateBarcodeEntityId;
    setPageTitle(getResource("GenerateBarcode"));
    GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.BarCodeTemplate, function () {
        checkPermissions(this, systemEntities.GenerateBarcodeEntityId, -1, $('.generatebarcode-Fieldset'), function (returnedData) {
            template = returnedData;
            ItemAC = template.find('select:.Item').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.ItemServiceURL + 'FindAllLite', required: true, multiSelect: false });
            
                

            IPriceListAC = template.find('select:.PriceList').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.IcPriceListServiceURL + 'FindAllLite', required: true, multiSelect: false });
            CurrencyAC = template.find('select:.Currency').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.CurrencyWebService + 'FindAllLite', required: true, multiSelect: false });



        });

        $('.ActionPrint-link').live("click", function () {
            if (ItemAC.get_Item() != null) {
                printBarcode();
            }
            return;
        });

    }, 'BarCodeTemplate');


    $('#btn-generate-bar-code').click(function () {
        if (ItemAC.get_Item() != null && IPriceListAC.get_Item() != null && CurrencyAC.get_Item() != null) {
            
            
            if (ItemAC.get_ItemValue() > 0)
                generateBarcode(ItemAC.get_Item(),IPriceListAC.get_Item(),CurrencyAC.get_Item());
        }
    });
  


}

function generateBarcode(itemObj,itemObj1,itemObj2) {
    var barCodeObj={
        PriceListGroupId:itemObj1.PriceListId,
        ItemCardId:itemObj.ItemCardId,
        CurrencyId: itemObj2.curencyId


    }



    var data = formatString('{keyword:{0}, page:{1},resultCount:{2},argsCriteria:"{ItemCardId:{3},CurrencyId:{4},PriceListGroupId:{5}}"}', null, 1, 30, itemObj.ItemCardId, itemObj2.curencyId, -1); //JSON.stringify(barCodeObj))

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: systemConfig.ApplicationURL_Common + systemConfig.IcPriceListGroupServiceURL + "FindAllLite",
        data: data,
        datatype: "json",
        success: function (data) {
            var returnedData = eval("(" + data.d + ")");
            var result = returnedData.result;
            if (result != null) {
                $('.viewDiv').html('');
                debugger
                var TaxAmount = parseFloat(result[0].BasePrice * (result[0].TaxRate)).toFixed(3);
                BarCodeTemplate.find('.price').html(parseFloat(Number(result[0].BasePrice) + Number(TaxAmount)).toFixed(3))
                BarCodeTemplate.find('.barCode').html('*' + itemObj.ItemBarCode + '*');
                BarCodeTemplate.find('.barCode').css('font-family', 'IDAutomationHC39M');
                $('.viewDiv').append(BarCodeTemplate);
            }
            else {
                $('.viewDiv').html('no data found');
                $('.viewDiv').addClass('addlabel');
            }

        },
        error: function (e) {
        }
    });
   
}

//function generateBarcode(itemObj) {
//    var TaxAmount = parseFloat(itemObj.Price * (itemObj.Tax / 100)).toFixed(3);
//    BarCodeTemplate.find('.price').html(parseFloat(Number(itemObj.Price) + Number(TaxAmount)).toFixed(3));
//    BarCodeTemplate.find('.barCode').html('*' + itemObj.ItemBarCode + '*');
//    BarCodeTemplate.find('.barCode').css('font-family', 'IDAutomationHC39M');
//    $('.viewDiv').append(BarCodeTemplate);
//}


function printBarcode() {
    var windowUrl = 'Print';
    var uniqueName = new Date()
    var windowName = 'Print' + uniqueName.getTime();
    $('.viewDiv').printArea();

}