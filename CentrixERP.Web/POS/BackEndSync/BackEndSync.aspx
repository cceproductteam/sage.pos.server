﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BackEndSync.aspx.cs" MasterPageFile="~/Common/MasterPage/ERPSite.master"
    Inherits="Centrix.POS.Server.Web.POS.BackEndSync.BackEndSync" %>

<asp:Content runat="server" ContentPlaceHolderID="PageHeader" ID="PageHeader">
    <script src="JS/BackEndSync.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <script src="../../Common/JScript/NewJS/CommonTabs.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <link href="CSS/BackEndSync_<%=Lang %>.css?v=<%=Centrix_Version %>" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="PageBody" ID="PageBody">
    <div id='content' class="BackEndSync-div display-none">
      <div class="Add-new-form">
            <div class="Add-title">
              
                <div class="Action-div">
                    <ul>
                        <li class="sync-all"><span class="ActionSave-link save-tab-lnk"></span><a class="ActionSave-link save-tab-lnk">
                            <span resourcekey="Sync"></span></a></li>
                  <%--      <li><span class="ActionCancel-link cancel-tab-lnk"></span><a class="ActionCancel-link cancel-tab-lnk">
                            <span resourcekey="Cancel"></span></a></li>--%>
                    </ul>
                </div>
            </div>
     <%--   <fieldset class="Data-Fieldset">--%>
            <table border="0" cellpadding="0" cellspacing="0" class="BackEndSync-info">
                <tr>
                    <td>
                        <span class="label-title" resourcekey="Entity"></span>
                    </td>
                    <td>
                        <span class="label-title" resourcekey="LastSyncDate"></span>
                    </td>
                    <td>
                        <span class="label-title" resourcekey="Status"></span>
                    </td>
                    <th>
                      <input type="checkbox" class="cx-control checkSyncAll chk " /> <span class="label-title" resourcekey="All"></span>
                     </th>
                </tr>
                <tr>
                    <th>
                        <span class="label-title" resourcekey="IcCurrency"></span>
                    </th>
                    <td>
                        <span class="label-title date CurrencyLastSyncDate"></span>
                    </td>
                    <td>
                        <span class="label-title CurrencyStatus"></span>
                    </td>
                   <th>
                       <%-- <a class="syncBtn SyncCurrency label-title" resourcekey="Sync"></a>--%>
                        <input type="checkbox" class="cx-control SyncCurrency chk " />
                    </th>
                </tr>
                <tr>
                    <th>
                        <span class="label-title" resourcekey="IcItemCard"></span>
                    </th>
                    <td>
                        <span class="label-title date ItemCardLastSyncDate"></span>
                    </td>
                    <td>
                        <span class="label-title ItemCardStatus"></span>
                    </td>
                     <th>
                     <%--   <a class="syncBtn SyncItemCard label-title" resourcekey="Sync"></a>--%>
                        <input type="checkbox" class="cx-control SyncItemCard chk " />
                    </th>
                </tr>
                <tr>
                    <th>
                        <span class="label-title" resourcekey="IcPriceListGroup"></span>
                    </th>
                    <td>
                        <span class="label-title date PriceListGroupLastSyncDate"></span>
                    </td>
                    <td>
                        <span class="label-title PriceListGroupStatus"></span>
                    </td>
                     <th>
                      <%--  <a class="syncBtn SyncPriceListGroup label-title" resourcekey="Sync"></a>--%>
                        <input type="checkbox" class="cx-control SyncPriceListGroup chk " />
                    </th>
                </tr>
                <tr>
                    <th>
                        <span class="label-title" resourcekey="IcItemPriceList"></span>
                    </th>
                    <td>
                        <span class="label-title date ItemPriceListLastSyncDate"></span>
                    </td>
                    <td>
                        <span class="label-title ItemPriceListStatus"></span>
                    </td>
                     <th>
                      <%--  <a class="syncBtn SyncItemPriceList label-title" resourcekey="Sync"></a>--%>
                        <input type="checkbox" class="cx-control SyncItemPriceList chk " />
                    </th>
                </tr>
                <tr>
                  <th>
                        <span class="label-title" resourcekey="IcItemTax"></span>
                    </th>
                    <td>
                        <span class="label-title date ItemTaxLastSyncDate"></span>
                    </td>
                    <td>
                        <span class="label-title ItemTaxStatus"></span>
                    </td>
                    <th>
                      <%--  <a class="syncBtn SyncItemTax label-title" resourcekey="Sync"></a>--%>
                        <input type="checkbox" class="cx-control SyncItemTax chk " />
                    </th>
                </tr>
                <tr>
                   <th>
                        <span class="label-title" resourcekey="IcLocation"></span>
                    </th>
                    <td>
                        <span class="label-title date LocationLastSyncDate"></span>
                    </td>
                    <td>
                        <span class="label-title LocationStatus"></span>
                    </td>
                    <th>
                      <%--  <a class="syncBtn SyncLocation label-title" resourcekey="Sync"></a>--%>
                        <input type="checkbox" class="cx-control SyncLocation chk " />
                    </th>
                </tr>
                <tr>
                    <th>
                        <span class="label-title" resourcekey="IcStock"></span>
                    </th>
                    <td>
                        <span class="label-title date StockLastSyncDate"></span>
                    </td>
                    <td>
                        <span class="label-title StockStatus"></span>
                    </td>
                    <th>
                  <%--      <a class="syncBtn SyncStock label-title" resourcekey="Sync"></a>--%>
                        <input type="checkbox" class="cx-control SyncStock chk " />
                    </th>
                </tr>
                <tr>
                   <th>
                        <span class="label-title" resourcekey="IcCurrencyRate"></span>
                    </th>
                    <td>
                        <span class="label-title date CurrencyRateLastSyncDate"></span>
                    </td>
                    <td>
                        <span class="label-title CurrencyRateStatus"></span>
                    </td>
                    <th>
                       <%-- <a class="syncBtn SyncCurrencyRate label-title" resourcekey="Sync"></a>--%>
                        <input type="checkbox" class="cx-control SyncCurrencyRate chk " />
                    </th>
                </tr>
                <tr>
                   <th>
                        <span class="label-title" resourcekey="IcCustomer"></span>
                    </th>
                    <td>
                        <span class="label-title date CustomerLastSyncDate"></span>
                    </td>
                    <td>
                        <span class="label-title CustomerStatus"></span>
                    </td>
                    <th>
                       <%-- <a class="syncBtn SyncCustomer label-title" resourcekey="Sync"></a>--%>
                        <input type="checkbox" class="cx-control SyncCustomer chk " />
                    </th>
                </tr>
                 <tr>
                   <th>
                        <span class="label-title" resourcekey="BankAccount">Bank Account</span>
                    </th>
                    <td>
                        <span class="label-title date BankAccountLastSyncDate"></span>
                    </td>
                    <td>
                        <span class="label-title BankAccountStatus"></span>
                    </td>
                    <th>
                       <%-- <a class="syncBtn SyncCustomer label-title" resourcekey="Sync"></a>--%>
                        <input type="checkbox" class="cx-control SyncBankAccount chk " />
                    </th>
                </tr>
                 <tr>
                   <th>
                        <span class="label-title" resourcekey="GLAccount">GL Account</span>
                    </th>
                    <td>
                        <span class="label-title date GLAccountLastSyncDate"></span>
                    </td>
                    <td>
                        <span class="label-title GLAccountStatus"></span>
                    </td>
                    <th>
                       <%-- <a class="syncBtn SyncCustomer label-title" resourcekey="Sync"></a>--%>
                        <input type="checkbox" class="cx-control SyncGLAccount chk " />
                    </th>
                </tr>

            </table>
       <%-- </fieldset>--%>
    </div>
    </div>
</asp:Content>
