﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SagePOS.Common.Web;
using SagePOS.Server.Configuration;

namespace Centrix.POS.Server.Web.POS.Person
{
    public partial class PersonList : BasePageLoggedIn
    {
        public PersonList()
            : base(Enums_S3.Permissions.SystemModules.None) { }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}