﻿var StartSyncglobalTime, CurrentGlobalDate, globalAction, CurrentSyncType = -1, gloabalSeconds = 0;

function InnerPage_Load() {

    // setPageTitle(getResource("ERPIntegration"));
    $('.Top-Mid-section').remove();

    setSelectedMenuItem($('.pos-icon'));

    $('.btn-inv-post').click(function () {
        
        showConfirmMsg("Post Invoice", "Are You Sure ?", function () {

            StartAccpacPost('1,7');
        }, null);

    });

    $('.btn-refund-post').click(function () {
        showConfirmMsg("Post Refund", "Are You Sure ?", function () {
            StartAccpacPost(POSInvoiceType.Refund);
        }, null);

    });

    $('.btn-onaccount-post').click(function () {
        showConfirmMsg("Post On Account", "Are You Sure ?", function () {
            StartAccpacPost(POSInvoiceType.OnAccount);
        }, null);

    });

    $('.btn-exchange-post').click(function () {
        showConfirmMsg("Post Exchange", "Are You Sure ?", function () {

            StartAccpacPost(POSInvoiceType.Exchange);
        }, null);

    });
    $('.btn-PettyCash-post').click(function () {
        showConfirmMsg("Post Petty Cash", "Are You Sure ?", function () {

            StartAccpacPost("8")
        }, null);
    });


    $('.lnk-update-registers').click(function () {
        NewcheckPermissions(this, systemEntities.UpdateRegisters, -1);
        if (havePer == 0) {
            showStatusMsg(getResource('vldDontHavePermission'));
            return;

        }
        if (!checkifAvailable()) {
            showOkayMsg(getResource("SystemAlert"), getResource('SyncedInProgress'))
            return;
        }
       

        showConfirmMsg(getResource("ConfirmMsgConfirm"), getResource("updateRegisterConfirmationMessage"), function () {
          
            CreateSyncRecord(1);
            CurrentSyncType = 1;
        });
    });

    $('.lnk-update-accpac').click(function () {
      NewcheckPermissions(this, systemEntities.UpdateAccpac, -1);
      if (havePer == 0) {
          showStatusMsg(getResource('vldDontHavePermission'));
          return;

      }
      if (!checkifAvailable()) {
            showOkayMsg(getResource("SystemAlert"), getResource('SyncedInProgress'))
            return;
        }




        showConfirmMsg(getResource("ConfirmMsgConfirm"), getResource("updateAccpacConfirmationMessage"), function () {
            CreateSyncRecord(2);
            CurrentSyncType = 2;
        });
    });

    setInterval(function () {
        checkthesynchronizationStatus();
    }, 30000
    );

    checkthesynchronizationStatus();
    setInterval(function () {
        GetDateTime(CurrentGlobalDate, StartSyncglobalTime, globalAction)
    }, 1000);
  
 
}





function CreateSyncRecord(type) {
  
    post(systemConfig.ApplicationURL + systemConfig.SyncScheduleServiceURL + "AddNewSchedule", formatString("{Type:'{0}',UserId:'{1}'}", type, LoggedInUser.Id), function (retuned) {
        if (type == 1) {
            disbaleButton($('.lnk-update-registers'), $('.lnk-update-registers-container'));
            showOkayMsg(getResource("SystemAlert"), getResource('SynchronizationData'));
            CurrentSyncType = 1;

        }
        else if (type == 2) {
            disbaleButton($('.lnk-update-accpac'), $('.lnk-update-accpac-container'));
            showOkayMsg(getResource("SystemAlert"), getResource('SynchronizationData'));
            CurrentSyncType = 2;

        }
        gloabalSeconds = 0;

    });

}


function disbaleButton(button, container) {
    $(container).addClass('BtnContainer-hover');
    $(button).addClass('Inactive');
    checkthesynchronizationStatus();
    
}


function checkifAvailable() {
    var sumbit = true;
    if ($('.lnk-update-registers').hasClass('Inactive') || $('.lnk-update-accpac').hasClass('Inactive'))
        sumbit = false;
    return sumbit;
}

function checkthesynchronizationStatus() {
    ajaxSetup();
    $.ajax({
        type: "POST",
        url: systemConfig.ApplicationURL + systemConfig.SyncScheduleServiceURL + "Checksyncstatus",
        contentType: "application/json; charset=utf-8",
        data: formatString("{action:'{0}'}", CurrentSyncType),
        dataType: "json",
        success: function (data) {
            getStatusSuccess(data);
        },
        error: function (e) {
        }
    });
}


function getStatusSuccess(data) {
    var result = eval("(" + data.d + ")");
  
        CurrentGlobalDate = result.result.serverDateTime;
        var status = result.result.SyncStatus;
        var SyncNumber = result.result.SyncNumber;
        if ($('.lnk-update-accpac').hasClass('Inactive') && result.result.result == null) {
            showOkayMsg(getResource("SystemAlert"), getResource("DataHasBeenUpdated"));
            globalAction = CurrentGlobalDate = StartSyncglobalTime = null;
            $('.lnk-update-registers').html(getResource("UpdateRegisters"));
            $('.lnk-update-accpac').html(getResource("UpdateAccpac"));
            $('.lnk-update-accpac,.lnk-update-registers').removeClass('Inactive');
            $('.lnk-update-registers-container,.lnk-update-accpac-container').removeClass('BtnContainer-hover');
            return;
        }
        if (SyncNumber != "" && SyncNumber != "No Data" && result.result.result == null) {
            showOkayMsg(getResource("SystemAlert"), getResource("SyncedUpdated") + SyncNumber);
            globalAction = CurrentGlobalDate = StartSyncglobalTime = null;
            $('.lnk-update-registers').html(getResource("UpdateRegisters"));
            $('.lnk-update-accpac').html(getResource("UpdateAccpac"));
            $('.lnk-update-accpac,.lnk-update-registers').removeClass('Inactive');
            $('.lnk-update-registers-container,.lnk-update-accpac-container').removeClass('BtnContainer-hover')
            return;
        }
        else if (SyncNumber == "No Data") {
            showOkayMsg(getResource("SystemAlert"), getResource("NoDatatobeUpdate"));
            globalAction = CurrentGlobalDate = StartSyncglobalTime = null;
            $('.lnk-update-registers').html(getResource("UpdateRegisters"));
            $('.lnk-update-accpac').html(getResource("UpdateAccpac"));
            $('.lnk-update-accpac,.lnk-update-registers').removeClass('Inactive');
            $('.lnk-update-registers-container,.lnk-update-accpac-container').removeClass('BtnContainer-hover');
            return;
        }
  
    var action = globalAction = result.result.result.action;
    var startTime = StartSyncglobalTime = result.result.result.startTime;


    if (action == 1) {
        $('.lnk-update-registers-container').addClass('BtnContainer-hover');
        $('.lnk-update-registers').addClass('Inactive');
        CurrentSyncType = 1;
        //$('.lnk-update-registers').html(getResource("UpdateRegisters") + ' - ' + GetDateTime(currentDate, startTime, action));
        $('#register-startSync').html(startTime);
    }
    else if (action == 2) {

        $('.lnk-update-accpac-container').addClass('BtnContainer-hover');
        $('.lnk-update-accpac').addClass('Inactive');
        //$('.lnk-update-accpac').html(getResource("UpdateAccpac") + ' - ' + GetDateTime(currentDate, startTime, action));
        $('#accpac-startSync').html(startTime);
        CurrentSyncType = 2;

    }

}


function diff(start) {
    var d = new Date(); // for now
    var hours = d.getHours(); // => 9
    var minutes = d.getMinutes(); // =>  30
    var seconds = d.getSeconds(); // => 51
    var end = hours + ":" + minutes;
    start = start.split(":");
    end = end.split(":");
    var startDate = new Date(0, 0, 0, start[0], start[1], 0);
    var endDate = new Date(0, 0, 0, end[0], end[1], 0);
    var diff = endDate.getTime() - startDate.getTime();
    var hours = Math.floor(diff / 1000 / 60 / 60);
    diff -= hours * 1000 * 60 * 60;
    var minutes = Math.floor(diff / 1000 / 60);

    // If using time pickers with 24 hours format, add the below line get exact hours
    if (hours < 0)
        hours = hours + 24;

    return (hours <= 9 ? "0" : "") + hours + ":" + (minutes <= 9 ? "0" : "") + minutes;
}
function StartAccpacPost(invType) {
    showLoading($('.Main-Div'));
    postAccpacTransaction(systemConfig.ApplicationURL + systemConfig.AccpacURL + "postInvoicetoAccpac", formatString("{invType:'{0}'}", invType), function (retuned) {
        hideLoading();
        if (retuned == "502") {
            showOkayMsg(getResource("SystemAlert"), getResource("NoInvoiceToPost"));
            hideLoading();
            return;
        }

        if (retuned.statusCode.Code == 1 || retuned != "502") {
        
            var result = retuned.result;
            if (result != null) {

                if (StartAccpacPost)
                showOkayMsg(getResource("Completed"), retuned.d);
                var resultText = getResource("POSShipmentPostResult");
                var shipNumbers = '';

                resultText = resultText + shipNumbers;
                showOkayMsg(getResource("Completed"), resultText);
            }
        }


    }, null
    , null);
}



var POSInvoiceType = {
    Invoice: 1,
    Refund: 5,
    OnAccount: 7,
    Exchange: 10
};


function GetDateTime(CurrenDate, StartDate, action) {
    if (!CurrenDate && !StartDate) {
        $('.lnk-update-registers').html(getResource("UpdateRegisters"));
        $('.lnk-update-accpac').html(getResource("UpdateAccpac"));
        $('.lnk-update-registers').removeClass('Inactive');
        $('.lnk-update-accpac').removeClass('Inactive');
        gloabalSeconds = 0;
        return;
    }
     
    var currentDate = new Date(CurrenDate);
    //var currentDate = new Date();
    var startDate = new Date(StartDate);

    var Currentminutes = currentDate.getMinutes();
    var CurrentSeconds = currentDate.getSeconds() + 1;
    var curenetHours = currentDate.getHours();

    var Startminutes = startDate.getMinutes();
    var StartCurrentSeconds = startDate.getSeconds();
    var startdatehours = startDate.getHours();

    var currentTime = curenetHours + ':' + Currentminutes + ':' + CurrentSeconds;

    var startTime = startdatehours + ':' + Startminutes + ':' + StartCurrentSeconds;

    var diffminutes = Currentminutes - Startminutes;
    var diffseccounts = CurrentSeconds - StartCurrentSeconds;
    var sec = (Date.parse(currentDate) - Date.parse(startDate)) / 60000;


    var difference = difference = Math.abs(toSeconds(startTime) - (toSeconds(currentTime)));
    if (currentDate < startDate)
        difference = Math.abs(toSeconds(startTime) - (toSeconds(currentTime)));
    else

        difference = Math.abs((toSeconds(currentTime)) - toSeconds(startTime));


    var hours = Math.floor(difference / 3600); // an hour has 3600 seconds
    var minutes = Math.floor((difference % 3600) / 60); // a minute has 60 seconds
    var seconds = difference % 60;
    // seconds = seconds + 1;

    gloabalSeconds = gloabalSeconds + 1;
    var globalminutes = Math.floor((gloabalSeconds % 3600) / 60);
    var secondsss = gloabalSeconds % 60;

    diffseccounts = diffseccounts;
    if (diffminutes < 10) {
        diffminutes = diffminutes
    }
    var Time = "";
    var AMPM = "";
    var lang = "en";
    if (diffminutes < 10) {
        diffminutes = diffminutes
    }
  
    //Time = Math.abs(minutes) + ":" + Math.abs(gloabalSeconds) + " ";

    Time = Math.abs(globalminutes) + ":" + Math.abs(secondsss) + " ";
    DateTime = Time;
   

    if (action == 1) {
        $('.lnk-update-registers-container').addClass('BtnContainer-hover');
        $('.lnk-update-registers').addClass('Inactive');

        $('.lnk-update-registers').html(getResource("UpdateRegisters") + ' - ' + Time);
        $('#register-startSync').html(Time);
    }
    else if (action == 2) {

        $('.lnk-update-accpac-container').addClass('BtnContainer-hover');
        $('.lnk-update-accpac').addClass('Inactive');
        $('.lnk-update-accpac').html(getResource("UpdateAccpac") + ' - ' + Time);
        $('#accpac-startSync').html(Time);

    }

}


function toSeconds(time_str) {
    // Extract hours, minutes and seconds
    var parts = time_str.split(':');
    // compute  and return total seconds
    return parts[0] * 3600 + // an hour has 3600 seconds
    parts[1] * 60 + // a minute has 60 seconds
    +
    parts[2]; // seconds
}