﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SagePOS.Common.Web;


namespace SagePOS.Server.Web.POS.POSSetup
{
    public partial class POSSetup : BasePageLoggedIn
    {
        public POSSetup()
            : base(SagePOS.Server.Configuration.Enums_S3.Permissions.SystemModules.None) { }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}