﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="POSSetup.aspx.cs" Inherits="SagePOS.Server.Web.POS.POSSetup.POSSetup"
    MasterPageFile="~/Common/MasterPage/ERPSite.master" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="PageHeader">
    <%--  <link href="CSS/ar-setup.css" rel="stylesheet" type="text/css" />--%>
    <link href="../../Common/Settings/CSS/featherlight.css" rel="stylesheet" type="text/css" />
    <link href="../../Common/Settings/CSS/new_style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(document).ready(function () {
            $('.Top-Mid-section').remove();
        });
        function InnerPage_Load() {
            setSelectedMenuItem('.pos-icon');

        }

        !function (d, s, id) { var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https'; if (!d.getElementById(id)) { js = d.createElement(s); js.id = id; js.src = p + '://platform.twitter.com/widgets.js'; fjs.parentNode.insertBefore(js, fjs); } }(document, 'script', 'twitter-wjs');

        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
			m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//stats.g.doubleclick.net/dc.js', 'ga');

        ga('create', 'UA-5342062-6', 'noelboss.github.io');
        ga('send', 'pageview');
    </script>
    <script src="../../Common/Settings/JS/featherlight.js" type="text/javascript"></script>
    <script src="JS/erpIntegration.js" type="text/javascript"></script>
    <style>
        .Inactive {
            pointer-events: none;
            cursor: default;
        }
    </style>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="PageBody">
    <div id="pageContainerInner">
        <div class="ModulesBlock">
            <div class="iconBlock">
                <img src="../../Common/Settings/images/TransactionProcess.png" alt="" width="84"
                    height="84">
            </div>
            <h2>
                <span resourcekey="Synchronization"></span>
            </h2>
            <div class="BtnsBox">
                 <div class="BtnContainer">
                    <a href="javascript:;" class="mod btn-refund-post" resourcekey="PostRefund"></a>
                    <a href="#" class="help btn btn-default"></a>
                </div>
                 <div class="BtnContainer">
                    <a href="javascript:;" class="mod btn-exchange-post" resourcekey="PostExchange"></a><a href="#" class="help btn btn-default"></a>
                </div>
                <div class="BtnContainer">
                    <a href="javascript:;" class="mod btn-inv-post" resourcekey="PostInvoice"></a><a
                        href="#" class="help btn btn-default"></a>
                </div>

                <%-- <div class="BtnContainer">
                    <a href="javascript:;" class="mod btn-inv-post" resourcekey="PostInvoice"></a><a
                        href="#" class="help btn btn-default"></a>
                </div>
                <div class="BtnContainer">
                    <a href="javascript:;" class="mod btn-refund-post" resourcekey="PostRefund"></a>
                    <a href="#" class="help btn btn-default"></a>
                </div>--%>
                <%--    <div class="BtnContainer">
                    <a href="javascript:;" class="mod btn-exchange-post" resourcekey="PostExchange"></a><a href="#" class="help btn btn-default"></a>
                </div>
                 <div class="BtnContainer">
                    <a href="javascript:;" class="mod btn-PettyCash-post" resourcekey="PostPettyCash"></a><a href="#" class="help btn btn-default"></a>
                </div>--%>

                <div class="BtnContainer lnk-update-registers-container">
                    <a href="#" class="mod lnk-update-registers" resourcekey="UpdateRegisters"></a><a href="#"
                        class="help btn btn-default"></a>
                    <span id="register-startSync" style="display: none;"></span>
                </div>
                <div class="BtnContainer lnk-update-accpac-container">
                    <a href="#" class="mod  lnk-update-accpac" resourcekey="UpdateAccpac"></a><a href="#" class="help btn btn-default"></a>
                    <span id="accpac-startSync" style="display: none;"></span>
                </div>

                <div class="BtnContainer">
                    <a href="../Register/RegisterStatus.aspx" class="mod" resourcekey="RegisterStatus"></a><a href="#" class="help btn btn-default"></a>
                </div>



                <div class="BtnContainer">
                    <a href="../SyncActivityLog/SyncActivityLog.aspx" class="mod" resourcekey="SyncActivityLog"></a><a href="#"
                        class="help btn btn-default"></a>
                </div>

                <div class="BtnContainer">
                    <a href="../../NewReports/InvoiceInquiry/InvoiceInquiry.aspx" class="mod" resourcekey="InvoiceInquiry"></a><a href="#" class="help btn btn-default"></a>
                </div>

                <br />

                <br />
                <br />


            </div>
        </div>
        <div class="ModulesBlock">
            <div class="iconBlock">
                <img src="../../Common/Settings/images/reports.png" alt="" width="84" height="84">
            </div>
            <h2>
                <span resourcekey="IcReports"></span>
            </h2>
            <div class="BtnsBox">

                <div class="BtnContainer">
                    <a href="../../NewReports/TotalDailySales/POSTotalDailySales.aspx" class="mod" resourcekey="TotalDailySales"></a><a href="#" class="help btn btn-default"></a>
                </div>

                <div class="BtnContainer">
                    <a href="../../NewReports/TotalDailySummation/TotalDailySummation.aspx" class="mod" resourcekey="TotalDailySalesSummation"></a><a href="#" class="help btn btn-default"></a>
                </div>

                <div class="BtnContainer">
                    <a href="../../NewReports/DailySalesPerItem/POSDailySalesPerItem.aspx" class="mod"
                        resourcekey="DailySalesPerItem"></a><a href="#" class="help btn btn-default"></a>
                </div>
                <div class="BtnContainer">
                    <a href="../../NewReports/Dailysales/POSDailysales.aspx" class="mod" resourcekey="Dailysales"></a><a href="#" class="help btn btn-default"></a>
                </div>
                <div class="BtnContainer">
                    <a href="../../NewReports/POSDailySalesSummation/POSDailySalesSummation.aspx" class="mod" resourcekey="DailySalesSummation"></a><a href="#" class="help btn btn-default"></a>
                </div>
                <div class="BtnContainer">
                    <a href="../../NewReports/PosPettyCash/PosPettyCash.aspx" class="mod" resourcekey="PettyCashreport"></a><a href="#" class="help btn btn-default"></a>
                </div>

                <div class="BtnContainer">
                    <a href="../../NewReports/CloseRegisterReport/CloseRegisterReport.aspx" class="mod" resourcekey="CloseRegisterReport"></a><a href="#" class="help btn btn-default"></a>
                </div>
            </div>
        </div>


        <div class="ModulesBlock">
            <div class="iconBlock">

                <img src="../../Common/Settings/images/transactionalSetup.png" alt="" width="84" height="84">
            </div>
            <h2>
                <span resourcekey="ModuleSetup"></span>
            </h2>
            <div class="BtnContainer">
                <a href="../Store/StoreList.aspx" class="mod" resourcekey="Store"></a><a href="#"
                    class="help btn btn-default"></a>
            </div>
            <div class="BtnContainer">
                <a href="../Register/RegisterList.aspx" class="mod" resourcekey="Register"></a><a
                    href="#" class="help btn btn-default"></a>
            </div>
            <div class="BtnContainer" style="display: none">
                <a href="../GiftVoucher/GiftVoucherList.aspx" class="mod" resourcekey="GiftVoucher"></a><a href="#" class="help btn btn-default"></a>
            </div>

            <div class="BtnContainer">
                <a href="../QuickKeys/QuickKeysList.aspx" class="mod" resourcekey="QuickKeys"></a>
                <a href="#" class="help btn btn-default"></a>
            </div>
            <div class="BtnContainer">
                <a href="../SystemConfigration/SystemConfigration.aspx" class="mod" resourcekey="POSConfiguration"></a>
                <%--   <a href="#" class="help btn btn-default"data-featherlight="#fl3" ></a>--%>
                <a href="#" class="help btn btn-default"></a>
            </div>
            <div class="BtnContainer">
                <a href="../../NewUserManagement/Users/UsersList.aspx" class="mod" resourcekey="SPUsers"></a>
                <a href="#" class="help btn btn-default"></a>
            </div>
            <%-- <div class="BtnContainer">
                    <a href="../../NewUserManagement/Roles/RolesList.aspx" class="mod" resourcekey="SPRols"></a>
                    <a href="#" class="help btn btn-default"></a>
                </div>--%>
            <div class="BtnContainer">
                <a href="../../newUserManagement/Roles/RolePermission.aspx" class="mod" resourcekey="SPPermissions"></a>
                <a href="#" class="help btn btn-default"></a>
            </div>
            <div class="BtnContainer">
                <a href="../Contact/ContactList.aspx" class="mod" resourcekey="Contact"></a><a href="#"
                    class="help btn btn-default"></a>
            </div>
        </div>


    </div>
    <div class="ModulesBlock" style="display: none">
        <div class="iconBlock">
            <img src="../../Common/Settings/images/dataDefinition.png" alt="" width="84" height="84">
        </div>
        <h2>
            <span resourcekey="MasterDataSetup"></span>
        </h2>
        <div class="BtnsBox">


            <div class="BtnContainer" style="display: none">
                <a href="../../NewUserManagement/Teams/TeamsList.aspx" class="mod" resourcekey="SPTeams"></a>
                <a href="#" class="help btn btn-default"></a>

                <%--  <div class="BtnContainer">
                    <a href="../../newUserManagement/Shifts/ShiftsList.aspx" class="mod" resourcekey="Shifts"></a>
                    <a href="#" class="help btn btn-default"></a>
                </div>--%>
            </div>
            <div class="BtnContainer" style="display: none">
                <a href="../../Common/Settings/ERPEntityLockList.aspx" class="mod" resourcekey="SPEntitylock"></a><a href="#" class="help btn btn-default"></a>
            </div>

        </div>
    </div>
    </div>
    <iframe class="lightbox" src="index.html" width="600" height="400" id="fl3" style="border: none;"
        webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
</asp:Content>
