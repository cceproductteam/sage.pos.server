<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddContact.aspx.cs" MasterPageFile="~/Common/MasterPage/ERPSite.master"
    Inherits="Centrix.StandardEdition.CRM.Web.CRM.Contact.AddContact" %>

<asp:Content runat="server" ContentPlaceHolderID="PageHeader" ID="PageHeader">
    <script type="text/javascript" src="JS/Contact.js?v=<%=Centrix_Version %>"></script>
    <script src="../../Common/JScript/CommonTabs.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <script type="text/javascript">
        addContactMode = true;
           
    </script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="PageBody" ID="PageBody">
   <div class="Add-title">
    <span resourcekey="AddContact"></span>
    <div class="Action-div">
        <ul>
            <li><span class="ActionSave-link"></span><a class="ActionSave-link save-title"><span
                resourcekey="Save"></span></a></li>
            <li><span class="ActionCancel-link"></span><a class="ActionCancel-link save-title"><span
                resourcekey="Cancel"></span></a></li>
        </ul>
    </div>
</div>
<div class="FormScroll-Div">
    <div class="Add-new-form">
        <fieldset class="Data-Fieldset">
            <table border="0" cellpadding="0" cellspacing="0" class="Data-Table3col add-Contact-info">
            			<tr>
				<th>
					<span class="label-title" resourcekey="FirstName"></span>
				</th>
				<td>
					<input class="cx-control FirstName  txt    string " maxlength="50" minlength="3"
                            type="text"  />
				</td>
				<th>
					<span class="label-title" resourcekey="LastName"></span>
					<span class="smblreqyuired viewedit-display-none">*</span>
				</th>
				<td>
					<input class="cx-control LastName  txt required   string " maxlength="50" minlength="3"
                            type="text"  />
				</td>
				<th>
					<span class="label-title" resourcekey="Email"></span>
				</th>
				<td>
					<input class="cx-control Email  txt    string " maxlength="30" minlength="3"
                            type="text"  />
				</td>
			</tr>
			<tr>
				<th>
					<span class="label-title" resourcekey="Gender"></span>
				</th>
				<td>
					<input class="cx-control Gender  txt   integer  " maxlength="" minlength="3"
                            type="text"  />
				</td>
				<th>
					<span class="label-title" resourcekey="PhoneNumber"></span>
				</th>
				<td>
					<input class="cx-control PhoneNumber  txt   integer  " maxlength="" minlength="3"
                            type="text"  />
				</td>
				<th>
					<span class="label-title" resourcekey="CompanyName"></span>
				</th>
				<td>
					<input class="cx-control CompanyName  txt    string " maxlength="50" minlength="3"
                            type="text"  />
				</td>
			</tr>
			<tr>
				<th>
					<span class="label-title" resourcekey="ContactType"></span>
				</th>
				<td>
					<input class="cx-control ContactType  txt   integer  " maxlength="" minlength="3"
                            type="text"  />
				</td>
			</tr>
             </table>
        </fieldset>
    </div>
</div>
</asp:Content>
