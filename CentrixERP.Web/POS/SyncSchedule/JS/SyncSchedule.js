var SyncScheduleServiceURL;
var SyncScheduleViewItemTemplate;

var ItemListTemplate;
var SyncScheduleId = selectedEntityId;
var lastUpdatedDate = null;
var addSyncScheduleMode = false;

function InnerPage_Load() {
    $('#demo2,.TabbedPanelsTabGroup,.TMS-right').hide();
    setSelectedMenuItem('.pos-icon');
    SyncScheduleServiceURL = systemConfig.ApplicationURL_Common + systemConfig.SyncScheduleServiceURL;
    currentEntity = systemEntities.SyncScheduleEntityId;
    //Advanced search Data
    //        mKey = 'SyncSchedule';//replace it with entity modual key
    //        InnerPage_Load2();
    //        $('.QuickSearch-holder').show();
    // LoadSearchForm2(mKey, false);
    //Advanced search Data

    setPageTitle(getResource("SyncSchedule"));
    setAddLinkAction(getResource("AddSyncSchedule"), 'add-new-item');

    GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.AddSyncScheduleItemTemplate, function () {
        GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.SyncScheduleListItemTemplate, loadData, 'ItemListTemplate');
    }, 'AddItemTemplate');

    $('.result-block').live("click", function () {
        SyncScheduleId = $(this).attr('id');
    });

    deletedEntityCallBack = function () {
        removeDeletedEntity();
    };

    updatedEntityCallBack = function () {
        getEntityData(selectedEntityId);
    };
    


}

function loadData(data) {

    GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.SyncScheduleListViewItemTemplate, function () {
        checkPermissions(this, systemEntities.SyncScheduleEntityId, -1, SyncScheduleViewItemTemplate, function (returnedData) {
            SyncScheduleViewItemTemplate = returnedData;
            loadEntityServiceURL = SyncScheduleServiceURL + "FindByIdLite";
            entityItemListTemplate = data;
            entityItemViewTemplate = returnedData;
            findAllEntityServiceURL = SyncScheduleServiceURL + "FindAllLite";
            deleteEntityServiceURL = SyncScheduleServiceURL + "Delete";
            selectEntityCallBack = getSyncScheduleInfo;
            loadDefaultEntityOptions();
        });
    }, 'SyncScheduleViewItemTemplate');

}

function initControls(template, isAdd) {
    $('.Result-info-container').css('width', '100%');
    if (!template) template = $('.TabbedPanels');
    if (!isAdd)
        $('.historyUpdated').append(HistoryUpdatedListViewTemplate.clone());
    LoadDatePicker(template.find('input:.date'), 'dd-mm-yy');
    template.find('.numeric').autoNumeric();
    template.find('.integer').autoNumeric({ aPad: false });
    SyncTemplate = $('.SyncItemRow');
    $('.SyncItemRow').remove();
    getAllSync();

}


function initAddControls(template) {
    initControls(template, true);
}

 function getSyncScheduleInfo(item) {
     if (item != null) {
//         $('.HSyncEntitiesIds').text(Truncate(item.SyncEntitiesIds, 15));
//         $('.H').text(Truncate(item., 15));
//         $('.H').text(Truncate(item., 15));
     }
 
  }

 function setListItemTemplateValue(entityTemplate, entityItem) {

//     entityTemplate.find('.result-data1 label').text(Truncate(entityItem.SyncEntities, 20));
//     entityTemplate.find('.result-data2 label').textFormat(Truncate(entityItem.SyncEntitiesIds,20));
//     entityTemplate.find('.result-data3 label').textFormat(Truncate(entityItem., 20));
     return entityTemplate;
 }

 function setListItemServiceData(keyword, pageNumber, resultCount) {
     if (!QuickSearch) {
         return formatString('{ keyword: "{0}", page:{1}, resultCount:{2} ,argsCriteria:"{ SyncScheduleId:{3}}"}', keyword, pageNumber, resultCount, filterEntityId);
     }
     else {
         return formatString('{mKey:"{0}",SearchCriteriaObj:"{1}",SortType:{2},SortFields:"{3}",TotalRecords:{4},PageNumber:{5}}', mKey, SearchCriteriaObj, SortType, SortFields, resultCount, pageNumber);
     }
 }



 function SaveEntity(isAdd, template) {
     if (!template)
         template = $('.TabbedPanels');

     var isValid = $('input:.SyncInvoice').is(':checked') || $('input:.SyncRefund').is(':checked') || $('input:.SyncPettyCash').is(':checked');

     if (!isValid) {
         showStatusMsg(getResource("NoDataSelectedToSync"));
         return false;
     }

     SyncScheduleId = isAdd ? -1 : selectedEntityId;


     showLoading(template.parent());
     lastUpdatedDate = getEntityLockDateFormat(currentEntityLastUpdatedDate);
     var AccpacDataToSyncIds = '';
     var AccpacDataToSync = '';

     if ($('input:.SyncInvoice').is(':checked')) {
         AccpacDataToSyncIds = AccpacDataToSyncIds + ',' + systemConfig.dataTypes.SyncInvoice;
         AccpacDataToSync = AccpacDataToSync + ',Invoice';
     }
     if ($('input:.SyncRefund').is(':checked')) {
         AccpacDataToSyncIds = AccpacDataToSyncIds + ',' + systemConfig.dataTypes.SyncRefund;
         AccpacDataToSync = AccpacDataToSync + ',Refund';
     }
     if ($('input:.SyncPettyCash').is(':checked')) {
         AccpacDataToSyncIds = AccpacDataToSyncIds + ',' + systemConfig.dataTypes.SyncPettyCash;
         AccpacDataToSync = AccpacDataToSync + ',Petty Cash';
     }

     var SyncScheduleObj = {

         SyncEntities: AccpacDataToSync,
         SyncEntitiesIds: AccpacDataToSyncIds,
         Status:1
     };
     var data = formatString('{id:{0}, SyncScheduleInfo:"{1}"}', isAdd ? -1 : selectedEntityId, escape(JSON.stringify(SyncScheduleObj)));

     if (SyncScheduleId > 0)
         url = SyncScheduleServiceURL + "Edit";
     else
         url = SyncScheduleServiceURL + "Add";

     post(url, data, function (returned) { saveSuccess(returned,isAdd,template); }, saveError, getEntitylockAjaxHeaders(lastUpdatedDate));
 }

 function saveSuccess(returnedValue, isAdd, template) {
     if (returnedValue.statusCode.Code == 501) {

         hideLoading();
         return false;
     }
     else if (returnedValue.statusCode.Code == 0) {
         hideLoading();
         if (returnedValue.statusCode.message == 'validation' && returnedValue.result) {
             filterError(returnedValue.result, template);
         }
         showStatusMsg(getResource("FaildToSave"));
         return false;
     }
     if (!addSyncScheduleMode) {
         showStatusMsg(getResource("savedsuccessfully"));
         hideLoading();
         loadEntityData(loadEntityServiceURL, entityItemViewTemplate, selectedEntityId, getSyncScheduleInfo);
     }
     else {
         appendNewListRow(returnedValue.result);
         hideLoading();
         $('.add-new-item').hide();
         $('.add-new-item').empty();
     }
 }

 function saveError(ex) {
     hideLoading();
     handleLockEntityError(ex, deletedEntityCallBack, updatedEntityCallBack);
 }



 function filterError(errors, template) {
     var allMsg = '';
     template.find('.validation').remove();
     $.each(errors, function (index, error) {
         // var template = $('.SyncSchedule-info');
         if (error.GlobalMessage) {
             allMsg += '<span>' + error.Message + '<br/></span>';
         }
         else if (error.ControlName == 'SyncEntities') {
             addPopupMessage(template.find('input.SyncEntities'), getResource("required"));
         }
         else if (error.ControlName == 'SyncEntitiesIds') {
             addPopupMessage(template.find('input.SyncEntitiesIds'), getResource("required"));
         }
         else if (error.ControlName == 'Status') {
             addPopupMessage(template.find('input.Status'), getResource("required"));
         }

     });

     hideLoading();
     if (allMsg.trim()) {
         showOkayMsg('Validation error', allMsg.trim());
     }
 }



 function getAllSync() {
     data = '';
     $.ajax({
         type: "POST",
         url: systemConfig.ApplicationURL_Common + systemConfig.SyncScheduleServiceURL + "FindAllSync",
         contentType: "application/json; charset=utf-8",
         data: formatString('{ id: "{0}" }', 1),
         dataType: "json",
         success: function (data) {

             var returnedValue = eval("(" + data.d + ")");
             syncItemList = new Array();

             if (returnedValue != null) {
                 $('.SyncItemRow').remove();
                 if (returnedValue.result != null) {
                     $.each(returnedValue.result, function (index, item) {
                     
                         var SyncStatus;
                         if (item.Status == 4)
                             SyncStatus = getResource('failed');
                         else if (item.Status == 3)
                             SyncStatus = getResource('success');
                         else if (item.Status == 2)
                             SyncStatus = getResource('Inprogress');
                         else if (item.Status == 1)
                             SyncStatus = getResource('pending');

                         //alert(item.Status);
                         var template = $(SyncTemplate).clone();
                         var ItemObj = new SyncItem();
                         ItemObj.ItemIdentity = index + 1;
                         ItemObj.syncId = item.SyncScheduleId;
                         ItemObj.BoSyncScheduleId = item.SyncScheduleId;
                         ItemObj.SyncEntities = item.SyncEntities;
                         ItemObj.Status = SyncStatus;
                         ItemObj.CreatedDate = item.CreatedDate;
                         ItemObj.CreatedByName = item.CreatedByName;
                         syncItemList.push(ItemObj);

                         FillSyncItemData(ItemObj, template);


                     });
                 }
             }
         },
         error: {}
     });

 }

 function FillSyncItemData(Item, template) {

     template = mapEntityInfoToControls(Item, template);
    // debugger;
    // template.find('.Status').text(Item.Status);
     template.css('cursor', 'pointer');
     AddRowClass(template);
     $('.ItemList-info').append(template);
 }

 function AddRowClass(template) {

     if ($('.ItemList-info tr').length % 2) {
         template.find('td').addClass('td-r1-data-table');
     }
     else {
         template.find('td').addClass('td-r2-data-table');
     }
 }
 function SyncItem() {
     this.ItemIdentity = 1;
     this.syncId = -1;
     this.BoSyncScheduleId = -1;
     this.SyncEntities = '';
     this.Status = '';
     this.CreatedDate = -1;
     this.CreatedByName = '';
 }

