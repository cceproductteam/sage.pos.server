﻿
function getRelatedGiveAways(CompanyId) {

    $('.GiveAwaysTemp-tab').children().remove();
    showLoading($('.TabbedPanels'));
    $.ajax({
        type: "POST",
        url: systemConfig.ApplicationURL_Common + systemConfig.GiveawaysServiceURL + "FindAllLite",
        contentType: "application/json; charset=utf-8",
        data: formatString('{ keyword: "", page:1, resultCount:10,argsCriteria:"{CompanyId:{0}}"}', CompanyId),
        dataType: "json",
        success: function (data) {
            var RelatedObj = eval("(" + data.d + ")").result;
            AddRelatedGiveAwaysTab(null, CompanyId);
            
            if (RelatedObj != null) {
                $('.GiveAwaysTemp-tab').parent().find('.DragDropBackground').remove();
                $.each(RelatedObj, function (index, item) {
                    AddRelatedGiveAwaysTab(item, CompanyId);
                });
            }
            hideLoading();
        },
        error: function (e) {
        }
    });
}

function AddRelatedGiveAwaysTab(item, CompanyId) {

    var template = $(RelatedGiveawaysTemplate).clone();
    template.find('.field-company').hide();
    ViewRelatedGiveAway(item, template, CompanyId);
    if (item != null) {
        template.css('cursor', 'default');
        template = mapEntityInfoToControls(item, template);
        template.find('.Company').text(Truncate(item.Company, 20));
        template.find('.Branch').text(Truncate(item.Branch, 20));
        template.find('.SalesRepresentative').text(Truncate(item.SalesRepresentative, 20));
        $('.GiveAwaysTemp-tab').append(template);
    }
    
}

function ViewRelatedGiveAway(Item, template, CompanyId) {
    var id = (Item != null) ? Item.Id : 0;
    $('a:.add-new-Giveaways').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.GiveawaysList + "?cid=" + CompanyId);
    if (Item != null) {
        template.find('a:.company-link').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.CompanyList + "?key=" + Item.CompanyId);
        template.find('a:.person-link').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.PersonList + "?key=" + Item.PersonId);
        template.find('a:.Giveaway-link').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.GiveawaysList + "?key=" + Item.GiveawaysId);
       
    }
}
