﻿
var RegisterCount = 0, LastStore = 0;
var SyncCriteriaArray = new Array(), RegisterUserNamePaswwordArray = new Array();
var RegisterFullDataObject;
var StoreAC, StoreId = 0, SearchedRegisters = 0;
var RegistersIp = "", NotSyncedMode = false;
var ResultMessage = "";
var ResultCount = 10000000;

function RegisterData() {
    this.RegisterId = 0;
    this.UserName = "";
    this.Password = "";
    this.RegisterIp = "";
    this.RegisterName = "";
    this.InstanceName = "";
    this.DataBaseName = "";
    this.StoreName = "";
    this.IPAddress = "";

}

function InnerPage_Load() {

    NewcheckPermissions(this, systemEntities.RegisterStatus, -1);

    if (havePer == 0) {

        $('.white-container-data').hide();
        showStatusMsg(getResource('vldDontHavePermission'));
        return;
    }

    setSelectedMenuItem('.RegisterStatus-icon', '.TabLinks-RegisterStatus');
    currentEntity = systemEntities.RegisterstatusEntityId;
    setPageTitle(getResource("Registerstatus"));
    checkPermissions(this, systemEntities.RegisterstatusEntityId, -1, $('.RegisterStatus-Fieldset'), function (returnedData) {
        template = returnedData;
        GetRegisters(-1);
    });
}


function GetRegisters(storeId) {
    showLoading($('.white-container-data'));
    $.ajax({
        type: "POST",
        url: systemConfig.ApplicationURL_Common + systemConfig.RegisterStatusServiceURL + "FindAllLite",
        contentType: "application/json; charset=utf-8",
        data: formatString('{ keyword: "", page:1, resultCount:{0},argsCriteria:"{StoreId:{1}}"}', ResultCount, storeId),
        dataType: "json",
        success: function (data) {
            hideLoading();
            $('.RegisterStatus-Fieldset').show();
            showLoading($('.reg-regName-container'));
            var RegistersList = eval("(" + data.d + ")").result;
            if (RegistersList != null) {
                loadAllRegisters(RegistersList);
            }
          
        },
        error: function (e) {
        }
    });
}

function loadAllRegisters(Registers) {
     RegisterFullDataObject = Registers;
     $.each(RegisterFullDataObject, function (index, item) {
          CheckIPConnectivity(item, true);
      });
  }

  function CheckIPConnectivity(item, isFirstCheck) {      
      var string = new Array();
      string = item.IpAddress.split("\\");
      $.ajax({
          type: "POST",
          url: systemConfig.ApplicationURL_Common + systemConfig.POSSyncServiceURL + "CheckIPConnectivity",
          contentType: "application/json; charset=utf-8",
          data: formatString('{IpAddress:"{0}"}', string[0]),
          dataType: "json",
          success: function (data) {
              if (data != null) {
                  var isAvailable = eval("(" + data.d + ")");
                  if (isFirstCheck)
                      LoadRegister(item, isAvailable);
                  else {
                      ChangeStatus(item, isAvailable);
                  }
              }
          },
          error: function (e) {
              hideLoading();
          }
      });
  }


  function LoadRegister(RegisterObj, IsAvailable) {
    var string = RegisterObj.IpAddress.split("\\");
    var Container = $('.reg-regName-container');
    ///////////////////////
    /////append stores data ///////
    /////////////////////
    var storeDiv = $('<div>').attr({ class: 'stores-title', id: 'store-div-' + RegisterObj.StoreId });
    var storeSpan = $('<span>').attr({ class: 'stores-title-span' });
    var storeSyncAllChk = $('<input>').attr({ type: 'checkbox', class: 'store-selectAll', id: 'store-sync-' + RegisterObj.StoreId }).change(function () {
        if ($(this).attr('checked'))
            $('#registerul-' + RegisterObj.StoreId).find('input:enabled').attr('checked', true);
        else
            $('#registerul-' + RegisterObj.StoreId).find('input:enabled').attr('checked', false);

        $('#registerul-' + RegisterObj.StoreId).find('input:enabled').change();

    });
    var spanLabel = $('<span>').html('&nbsp;' + RegisterObj.Store);
    if ($('.reg-regName-container').find('#store-div-' + RegisterObj.StoreId).length > 0) {

    }
    else {
        $(storeSpan).append(spanLabel); //storeSyncAllChk
        $(storeDiv).append(storeSpan);
        $(Container).append(storeDiv);

    }
    //////////////////////////////////////////
    //////append registers data ////////////////////
    /////////////////////////
    var registersUL = $('<ul>').attr({ class: 'reg-Result-list', id: 'registerul-' + RegisterObj.StoreId });
    var registerLi = $('<li>');
    var registerDiv = $('<div>').attr({ class: 'reg-Result-list-div ', id: 'reg-div-' + RegisterObj.value });
    if (IsAvailable)
        $(registerDiv).addClass('connected-reg');
    else
        $(registerDiv).addClass('disconnected-reg');

    var statusDiv = $('<div>').attr({ class: 'regStatus connected-img' });
    var registerSyncCheck = $('<input>').attr({ type: 'checkbox', id: 'chSync-' + RegisterObj.value }).change(function () {
    });

    if (IsAvailable)
        $(statusDiv).addClass('connected-img');
    else {
        $(statusDiv).addClass('disconnected-img');
       // $(registerSyncCheck).attr({ 'disabled': 'disabled' });
    }
    var RegisterImg = $('<span>').attr({ class: 'register-img', id: 'register-img-' + RegisterObj.value }).css('cursor', 'pointer').click(function () {
       // $(registerSyncCheck).attr('disabled', 'true');
        $('#connection-div-' + RegisterObj.value).show();
        $('#reg-div-' + RegisterObj.value).addClass('reg-img-opacity');
        CheckIPConnectivity(RegisterObj, false);
    });
    var RegisterName = $('<span>').attr({ class: 'reg-name' }).html(RegisterObj.label);
    var RegisterIp = $('<span>').attr({ class: 'reg-ip' }).html(string[0]);
    var CheckconnectionDiv = $('<div>').attr({ class: 'testConnection display-none', id: 'connection-div-' + RegisterObj.value });
    var CheckconnectionImage = $('<img>').attr({ src: "../Images/loading.gif" });
    $(CheckconnectionDiv).append(CheckconnectionImage);
    $(registerDiv).append(statusDiv, RegisterImg, RegisterName, RegisterIp, CheckconnectionDiv); //registerSyncCheck,
    $(registerLi).append(registerDiv);
    if ($('.reg-regName-container').find('#registerul-' + RegisterObj.StoreId).length > 0) {
        $('#registerul-' + RegisterObj.StoreId).append(registerLi);
    }
    else {
        $(registersUL).append(registerLi);
        $(Container).append(registersUL);
    }

    LastStore = RegisterObj.StoreId;
    RegisterCount = RegisterCount + 1;
    if (RegisterCount == RegisterFullDataObject.length) {
        $('.loaderBox,.black_overlay').hide();
        hideLoading();
        RegisterCount = 0;
    }
    if (SearchedRegisters == RegisterCount) {
        hideLoading();
        RegisterCount = 0;
    }
}

function ChangeStatus(Register, IsAvailable) {
    $('#chSync-' + Register.value).removeAttr('disabled');
    $('#reg-div-' + Register.value).addClass('connected-reg');
    $('#reg-div-' + Register.value).removeClass('disconnected-reg');
    $('#reg-div-' + Register.value).find('.regStatus').removeClass("disconnected-img");
    $('#connection-div-' + Register.value).hide();
   // $('#register-img-' + Register.value).removeClass('reg-img-opacity');
    $('#reg-div-' + Register.value).removeClass('reg-img-opacity');
    if (!IsAvailable) {
        $('#chSync-' + Register.value).attr('disabled', 'true');
        $('#reg-div-' + Register.value).removeClass('connected-reg');
        $('#reg-div-' + Register.value).addClass('disconnected-reg');
        $('#reg-div-' + Register.value).find('.regStatus').addClass("disconnected-img");
    }
}