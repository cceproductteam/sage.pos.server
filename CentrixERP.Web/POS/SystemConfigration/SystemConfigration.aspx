<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SystemConfigration.aspx.cs"
    MasterPageFile="~/Common/MasterPage/ERPSite.master" Inherits="Centrix.POS.Server.Web.POS.SystemConfigration.SystemConfigration" %>

<asp:Content runat="server" ContentPlaceHolderID="PageHeader" ID="PageHeader">
    <script type="text/javascript" src="JS/PosSystemConfigration.js?v=<%=Centrix_Version %>"></script>
    <link href="CSS/SystemConfigration.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="PageBody" ID="PageBody">
    <div id='content' class="PosSystemConfigration-info">
        <div class="Add-title">
            <div class="Action-div">
                <ul>
                    <li class="edit-pnl viewedit-display-block"><span class="ActionEdit-link"></span><a
                        class="ActionEdit-link"><span resourcekey="Edit"></span></a></li>
                    <li class="save-cancel-pnl viewedit-display-none"><span class="ActionSave-link"></span>
                        <a class="ActionSave-link"><span resourcekey="Save"></span></a></li>
                    <li class="save-cancel-pnl viewedit-display-none"><span class="ActionCancel-link"></span>
                        <a class="ActionCancel-link"><span resourcekey="Cancel"></span></a></li>
                </ul>
            </div>
        </div>
        <fieldset class="Data-Fieldset">
            <table border="0" cellpadding="0" cellspacing="0" class="Data-Table3col PosSystemConfigration-info">
                <tr>
                    <th>
                        <span class="label-title" resourcekey="AccpacServerIp"></span>
                        <span class="smblreqyuired viewedit-display-none">*</span>

                    </th>
                    <td>
                        <span class="label-data nowrap viewedit-display-block AccpacServerIp"></span>
                        <input class="cx-control AccpacServerIp  txt   required string viewedit-display-none" maxlength="100"
                            minlength="3" type="text" />
                    </td>

                    <th>
                        <span class="label-title" resourcekey="AfterCloseRegisterValue"></span><span class="smblreqyuired viewedit-display-none">*</span>
                    </th>
                    <td>
                        <span class="label-data nowrap viewedit-display-block AfterCloseRegisterValue"></span>
                        <div class="viewedit-display-none">
                            <select class="AfterCloseRegisterValue lst">
                            </select>
                        </div>
                    </td>
                    <th>
                        <span class="label-title" resourcekey="CashCustomer"></span><span class="smblreqyuired viewedit-display-none">*</span>
                    </th>
                    <td>
                        <span class="label-data txt viewedit-display-block CashCustomer string"></span>
                        <div class="viewedit-display-none">
                            <select class="CashCustomer lst">
                            </select>
                        </div>
                    </td>
                </tr>
                <tr style="display: none">
                    <th>
                        <span class="label-title" resourcekey="GiftVoucherOverAmount"></span><span class="smblreqyuired viewedit-display-none">*</span>
                    </th>
                    <td>
                        <span class="label-data nowrap viewedit-display-block number GiftVoucherOverAmount"></span>
                        <div class="viewedit-display-none">
                            <select class="GiftVoucherOverAmount lst">
                            </select>
                        </div>
                        <%-- <input class="cx-control GiftVoucherOverAmount  txt  required integer  viewedit-display-none"
                            maxlength="" minlength="3" type="text" />--%>
                    </td>
                    <th>
                        <span class="label-title" resourcekey="LaybyMinPercentage"></span><span class="smblreqyuired viewedit-display-none">*</span>
                    </th>
                    <td>
                        <span class="label-data viewedit-display-block money LaybyMinPercentage"></span>
                        <input class="cx-control LaybyMinPercentage  txt numeric viewedit-display-none"
                            maxlength="3" minlength="1" type="text" /><span class="label-data viewedit-display-none">%</span>
                    </td>
                    <th class="label-title" style="display: none">
                        <span class="label-title" resourcekey="TaxAuthority"></span><span class="smblreqyuired viewedit-display-none">*</span>
                    </th>
                    <td style="display: none">
                        <span class="label-data txt viewedit-display-block  TaxAuthority"></span>
                        <div class="viewedit-display-none">
                            <select class="TaxAuthority lst">
                            </select>
                        </div>
                    </td>



                </tr>
                <tr style="display: none">
                    <th>
                        <span class="label-title" resourcekey="TaxableClass"></span><span class="smblreqyuired viewedit-display-none">*</span>
                    </th>
                    <td>
                        <span class="label-data txt viewedit-display-block TaxableClassName string"></span>
                        <div class="viewedit-display-none">
                            <select class="TaxableClassName lst">
                            </select>
                        </div>
                    </td>
                    <th>
                        <span class="label-title" resourcekey="NonTaxableClass"></span><span class="smblreqyuired viewedit-display-none">*</span>
                    </th>
                    <td>
                        <span class="label-data txt viewedit-display-block NonTaxableClassName string"></span>
                        <div class="viewedit-display-none">
                            <select class="NonTaxableClassName lst">
                            </select>
                        </div>
                    </td>
                    <th>
                        <span class="label-title" resourcekey="LoginMethod"></span><span class="smblreqyuired viewedit-display-none">*</span>
                    </th>
                    <td>
                        <span class="label-data nowrap viewedit-display-block LoginMethod"></span>
                        <div class="viewedit-display-none">
                            <select class="LoginMethod lst">
                            </select>
                        </div>
                    </td>
                </tr>
                <tr style="display: none">

                    <th>
                        <span class="label-title" resourcekey="DiscountAccount"></span><span class="smblreqyuired viewedit-display-none">*</span>
                    </th>
                    <td>
                        <span class="label-data txt viewedit-display-block DiscountAccount string"></span>
                        <div class="viewedit-display-none">
                            <select class="DiscountAccount lst">
                            </select>
                        </div>
                    </td>

                    <th>
                        <span class="label-title" resourcekey="ItemStructure"></span><span class="smblreqyuired viewedit-display-none">*</span>
                    </th>
                    <td>
                        <span class="label-data txt viewedit-display-block AddingItemStructure string"></span>
                        <div class="viewedit-display-none">
                            <select class="AddingItemStructure lst">
                            </select>
                        </div>
                    </td>
                    <th>
                        <span class="label-title" resourcekey="ExchangeAccount"></span><span class="smblreqyuired viewedit-display-none">*</span>
                    </th>
                    <td>
                        <span class="label-data txt viewedit-display-block ExchangeAccount string"></span>
                        <div class="viewedit-display-none">
                            <select class="ExchangeAccount lst">
                            </select>
                        </div>
                    </td>
                </tr>


                <tr>
                    <th>
                        <span class="label-title" resourcekey="InvoiceNumberFormat"></span><span class="smblreqyuired viewedit-display-none">*</span>
                    </th>
                    <td>
                        <span class="label-data nowrap InvoiceNumberFormat viewedit-display-block"></span>
                        <table border="0" cellpadding="0" cellspacing="0" class="PhoneCodesTable viewedit-display-none"
                            style="display: table;">
                            <tr>
                                <td>
                                    <span class="label-data nowrap viewedit-display-none" resourcekey="Prefix"></span>
                                    <br />
                                    <input class="cx-control INFormat INFPrefix t-country-num  txt viewedit-display-none string"
                                        maxlength="10" minlength="1" type="text" />
                                </td>
                                <td>
                                    <span class="label-data nowrap viewedit-display-none" resourcekey="D1"></span>
                                    <br />
                                    <input class="cx-control INFormat INFValD1 t-country-num txt viewedit-display-none string"
                                        maxlength="1" minlength="1" type="text" />
                                </td>
                                <td>
                                    <span class="label-data nowrap viewedit-display-none" resourcekey="SC"></span>
                                    <br />
                                    <select class="INFlstSC INFormat viewedit-display-none t-country-num" id="INFlstSC">
                                    </select>
                                </td>
                                <td>
                                    <span class="label-data nowrap viewedit-display-none" resourcekey="D2"></span>
                                    <br />
                                    <input class="cx-control INFormat INFValD2 t-country-num txt viewedit-display-none string"
                                        maxlength="1" minlength="1" type="text" />
                                </td>
                                <td>
                                    <span class="label-data nowrap viewedit-display-none" resourcekey="RC"></span>
                                    <br />
                                    <select class="INFlstRC INFormat INFormat t-country-num viewedit-display-none" id="INFlstRC">
                                    </select>
                                </td>
                                <td>
                                    <span class="label-data nowrap viewedit-display-none" resourcekey="D3"></span>
                                    <br />
                                    <input class="cx-control INFormat INFValD3 t-country-num txt viewedit-display-none string"
                                        maxlength="1" minlength="1" type="text" />
                                </td>
                                <td>
                                    <span class="label-data nowrap viewedit-display-none" resourcekey="Length"></span>
                                    <br />
                                    <input class="cx-control INFormat INFLength  t-country-num txt viewedit-display-none integer"
                                        maxlength="1" minlength="1" type="text" />
                                </td>
                            </tr>
                        </table>
                        <span class="smblreqyuired InvoiceNumberFormatRequired display-none" resourcekey="Required"></span>
                    </td>
                    <th>
                        <span class="label-title viewedit-display-none" resourcekey="Example"></span>
                    </th>
                    <td>
                        <span class="label-data nowrap InvoiceNumberFormat viewedit-display-none"></span>
                        <input class="cx-control InvoiceNumberFormat Format display-none string" type="text" />
                    </td>
                </tr>
                <tr>

                    <th>
                        <span class="label-title" resourcekey="AccpacDataBaseInstance"></span>
                        <span class="smblreqyuired viewedit-display-none">*</span>
                    </th>
                    <td>
                        <span class="label-data nowrap viewedit-display-block AccpacDataBaseInstance"></span>
                        <input class="cx-control AccpacDataBaseInstance  required txt    string viewedit-display-none" maxlength="100"
                            minlength="3" type="text" />
                    </td>
                    <th>
                        <span class="label-title" resourcekey="AccpacDataBaseName"></span>
                        <span class="smblreqyuired viewedit-display-none">*</span>
                    </th>
                    <td>
                        <span class="label-data nowrap viewedit-display-block AccpacDataBaseName"></span>
                        <input class="cx-control AccpacDataBaseName required txt    string viewedit-display-none"
                            maxlength="4000" minlength="3" type="text" />
                    </td>
                    <th>
                        <span class="label-title" resourcekey="AccpacDataBaseUsername"></span>
                        <span class="smblreqyuired viewedit-display-none">*</span>
                    </th>
                    <td>
                        <span class="label-data nowrap viewedit-display-block AccpacDataBaseUsername"></span>
                        <input class="cx-control AccpacDataBaseUsername required txt    string viewedit-display-none"
                            maxlength="100" minlength="3" type="text" />
                    </td>
                </tr>



                <tr style="display: none">
                    <th>
                        <span class="label-title" resourcekey="LaybyNumberFormat"></span><span class="smblreqyuired viewedit-display-none">*</span>
                    </th>
                    <td>
                        <span class="label-data nowrap LaybyNumberFormat viewedit-display-block"></span>
                        <table border="0" cellpadding="0" cellspacing="0" class="PhoneCodesTable viewedit-display-none"
                            style="display: table;">
                            <tr>
                                <td>
                                    <span class="label-data nowrap viewedit-display-none" resourcekey="Prefix"></span>
                                    <br />
                                    <input class="cx-control  LNFormat LNFPrefix t-country-num  txt viewedit-display-none string"
                                        maxlength="10" minlength="1" type="text" />
                                </td>
                                <td>
                                    <span class="label-data nowrap viewedit-display-none" resourcekey="D1"></span>
                                    <br />
                                    <input class="cx-control LNFormat LNFValD1 t-country-num txt viewedit-display-none string"
                                        maxlength="1" minlength="1" type="text" />
                                </td>
                                <td>
                                    <span class="label-data nowrap viewedit-display-none" resourcekey="SC"></span>
                                    <br />
                                    <select class="LNFlstSC LNFormat viewedit-display-none t-country-num" id="LNFlstSC">
                                    </select>
                                </td>
                                <td>
                                    <span class="label-data nowrap viewedit-display-none" resourcekey="D2"></span>
                                    <br />
                                    <input class="cx-control LNFormat LNFValD2 t-country-num txt viewedit-display-none string"
                                        maxlength="1" minlength="1" type="text" />
                                </td>
                                <td>
                                    <span class="label-data nowrap viewedit-display-none" resourcekey="RC"></span>
                                    <br />
                                    <select class="LNFlstRC LNFormat t-country-num viewedit-display-none" id="LNFlstRC">
                                    </select>
                                </td>
                                <td>
                                    <span class="label-data nowrap viewedit-display-none" resourcekey="D3"></span>
                                    <br />
                                    <input class="cx-control LNFormat LNFValD3 t-country-num txt viewedit-display-none string"
                                        maxlength="1" minlength="1" type="text" />
                                </td>
                                <td>
                                    <span class="label-data nowrap viewedit-display-none" resourcekey="Length"></span>
                                    <br />
                                    <input class="cx-control LNFormat LNFLength  t-country-num txt viewedit-display-none integer"
                                        maxlength="1" minlength="1" type="text" />
                                </td>
                            </tr>
                        </table>
                        <span class="smblreqyuired LaybyNumberFormatRequired display-none" resourcekey="Required"></span>
                    </td>
                    <th>
                        <span class="label-title viewedit-display-none" resourcekey="Example"></span>
                    </th>
                    <td>
                        <span class="label-data nowrap LaybyNumberFormat viewedit-display-none"></span>
                        <input class="cx-control LaybyNumberFormat Format display-none string" type="text" />
                    </td>
                </tr>

                <tr>




                    <th>
                        <span class="label-title" resourcekey="AccpacDataBasePassword"></span>
                        <span class="smblreqyuired viewedit-display-none">*</span>
                    </th>
                    <td>
                        <span class="label-data nowrap viewedit-display-block">******</span>
                        <input class="cx-control AccpacDataBasePassword  txt    string viewedit-display-none"
                            maxlength="100" minlength="3" type="password" />
                    </td>
                    <th>
                        <span class="label-title" resourcekey="AccpacCompanyName"></span>

                        <span class="smblreqyuired viewedit-display-none">*</span>
                    </th>
                    <td>
                        <span class="label-data nowrap viewedit-display-block AccpacCompanyName">
                            <span class="smblreqyuired viewedit-display-none">*</span>
                        </span>
                        <input class="cx-control AccpacCompanyName  required txt string viewedit-display-none"
                            maxlength="100" minlength="3" type="text" />
                    </td>
                    <th>
                        <span class="label-title" resourcekey="AccpacUserName"></span>
                        <span class="smblreqyuired viewedit-display-none">*</span>
                    </th>
                    <td></span>

                        <input class="cx-control AccpacUserName  txt  required  string viewedit-display-none"
                            maxlength="4000" minlength="3" type="text" />
                    </td>
                </tr>
                <tr>
                </tr>
                <tr>

                    <th>
                        <span class="label-title" resourcekey="AccpacUserPassword"></span>
                        <span class="smblreqyuired viewedit-display-none">*</span>
                    </th>
                    <td>
                        <span class="label-data nowrap viewedit-display-block">******</span>
                        <input class="cx-control AccpacUserPassword  required  txt    string viewedit-display-none"
                            maxlength="4000" minlength="3" type="password" />
                    </td>
                </tr>
                <tr>


                    <th style="display: none">
                        <span class="label-title" resourcekey="IsSysetm"></span><span class="smblreqyuired viewedit-display-none">*</span>
                    </th>
                    <td style="display: none">
                        <span class="label-data nowrap viewedit-display-block  YIsSysetm" resourcekey="Yes"></span><span class="label-data nowrap viewedit-display-block  NIsSysetm" resourcekey="No"></span>
                        <input type="checkbox" class="cx-control IsSysetm chk viewedit-display-none" />
                    </td>
                </tr>
            </table>
        </fieldset>
        <br />
        <fieldset class="Data-Fieldset">
            <div class="">
                <span class="detail-sub-title" resourcekey="SyncConfiguration"></span>
                <table border="0" cellpadding="0" cellspacing="0" class="Data-Table3col PosSystemConfigration-info">
                    <tr>
                        <td>
                            <span class="label-title" resourcekey="IsInvoicePosted"></span>
                        </td>
                        <td>
                             <span class="label-data nowrap viewedit-display-block IsInvoicesPosted"></span>
                            <input type="checkbox" class="viewedit-display-none IsInvoicesPosted" />
                        </td>
                        <td>
                            <span class="label-title" resourcekey="IsRefundPosted"></span>
                        </td>
                        <td>
                                 <span class="label-data nowrap viewedit-display-block IsRefundPosted"></span>
                            <input type="checkbox" class="viewedit-display-none IsRefundPosted" />
                        </td>
                        <td>
                          
                            <span class="label-title" resourcekey="IsExchangesPosted"></span>
                        </td>
                        <td>
                              <span class="label-data nowrap viewedit-display-block IsExchangePosted"></span>
                           <input type="checkbox" class="viewedit-display-none IsExchangePosted" />
                        </td>
                        <td>
                            <span class="label-title" resourcekey="isReceiptsPosted"></span>
                        </td>
                        <td>
                            <span class="label-data nowrap viewedit-display-block IsReceiptsPosted"></span>
                              <input type="checkbox" class="viewedit-display-none IsReceiptsPosted" />
                        </td>
                    </tr>
                </table>
            </div>
        </fieldset>

        <fieldset class="Data-Fieldset">
            <div class="historyUpdated">
            </div>
        </fieldset>
        <div class="five-px">
        </div>
    </div>
</asp:Content>
