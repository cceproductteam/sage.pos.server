<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GiftVoucherList.aspx.cs"
    MasterPageFile="~/Common/MasterPage/ERPSite.master" Inherits="Centrix.POS.Server.Web.GiftVoucher.GiftVoucherList" %>

<asp:Content runat="server" ContentPlaceHolderID="PageHeader" ID="PageHeader">
    <script type="text/javascript" src="JS/GiftVoucher.js?v=<%=Centrix_Version %>"></script>
    <script src="../../Common/JScript/CommonTabs.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <script src="../../Common/AdvancedSearch/JS/AdvancedSearch.js?v=<%=Centrix_Version %>"
        type="text/javascript"></script>
    <link href="../../Common/CSS/Task/calendar_<%=Lang %>.css?v=<%=Centrix_Version %>"
        rel="stylesheet" type="text/css" />
    <script src="../../Common/JScript/Task/Task.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <script src="../../Common/JScript/Task/calendar.js?v=<%=Centrix_Version %>"
        type="text/javascript"></script>
    <script src="../../Common/JScript/Task/fullcalendar.js?v=<%=Centrix_Version %>"
        type="text/javascript"></script>
    <script src="../../Common/JScript/CommunicationTab.js?v=<%=Centrix_Version %>"
        type="text/javascript"></script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="PageBody" ID="PageBody">
    <div id="demo2" class="result-container">
        <div class="SearchDivContainer">
            <div class="SD-SearchDiv">
                <input type="text" class="SD-ipnutText-MainSearch search-text-box searchTextbox" /></div>
            <div class="Quick-Search-Div">
            </div>
            <div class="MoreSearchOptions">
                <a class="more-options"><span resourcekey="MoreOptions"></span></a>
            </div>
        </div>
        <div class="GridTableDiv-header">
            <table class="GridTableFloating" cellpadding="0" cellspacing="0" border="0">
                <thead class="THeadGridTable">
                    <tr>
                        <th class="THead-Th th-number">
                            <span>#</span>
                        </th>
                        <th class="THead-Th">
                            <span resourcekey="GiftNumber"></span>
                        </th>
                        <th class="THead-Th">
                            <span resourcekey="GiftName"></span>
                        </th>
                        <th class="THead-Th">
                            <span resourcekey="GiftAmount"></span>
                        </th>
                        <th class="THead-Th">
                            <span resourcekey="ExpireDate"></span>
                        </th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="GridTableDiv-body ResultGridTableDiv">
            <table class="TBodyGridTable Result-list" cellpadding="0" cellspacing="0" border="0">
                <thead class="THeadGridTable">
                    <tr>
                        <th class="THead-Th th-number">
                            <span>#</span>
                        </th>
                        <th class="THead-Th">
                            <span resourcekey="GiftNumber"></span>
                        </th>
                        <th class="THead-Th">
                            <span resourcekey="GiftName"></span>
                        </th>
                        <th class="THead-Th">
                            <span resourcekey="GiftAmount"></span>
                        </th>
                        <th class="THead-Th">
                            <span resourcekey="ExpireDate"></span>
                        </th>
                    </tr>
                </thead>
                <tbody class="TBodyGridTable result-list">
                </tbody>
            </table>
        </div>
        <div class="total-result-div TotalDiv">
            <span></span>
        </div>
    </div>
    <div id='content'>
        <div class="Result-info-container">
            <div class="result-name">
                <ul>
                    <li class="nameLI"><span resourcekey="HGiftNumber"></span><span class="GiftNumber"></span>
                    </li>
                    <li class="emailLI"><span resourcekey="HGiftName"></span><span class="GiftName"></span>
                    </li>
                </ul>
            </div>
            <div style="float: right;" id='page_navigation'>
            </div>
            <div class="ClearDiv">
            </div>
            <div class="test">
                <div class="QuickSearchDiv">
                </div>
                <div id="TabbedPanels1" class="TabbedPanels">
                    <ul class="TabbedPanelsTabGroup">
                        <li class="TabbedPanelsTab" tabindex="1">
                            <div class="TabbedPanelsTabSelected-arrow" style="display: block;">
                            </div>
                            <span class="TabbedPanelsTab-img Summery-tab"></span><span resourcekey="Summary">
                            </span></li>
                       <%-- <li class="TabbedPanelsTab more-tabs">
                            <div class="TabbedPanelsTabSelected-arrow">
                            </div>
                            <span class="TabbedPanelsTab-img More-tab"></span><span class="more-title">More</span>
                            <span class="More-tabs-arrow"></span></li>--%>
                    </ul>
                    <div class="TabbedPanelsContentGroup">
                    </div>
                    <!--More-->
                    <div class="More-holder">
                        <div class="moreTabsList">
                            <ul class="moreTabs-ul">
                            </ul>
                        </div>
                    </div>
                    <!--End More-->
                </div>
            </div>
        </div>
    </div>
    <div class="add-new-div">
        <div class="add-new-item display-none">
        </div>
    </div>
</asp:Content>
