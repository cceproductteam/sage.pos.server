﻿

<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Common/MasterPage/ERPSite.master"
    CodeBehind="POSLogList.aspx.cs" Inherits="Centrix.POS.Server.Web.POS.POSLog.POSLogList" %>

<asp:Content runat="server" ContentPlaceHolderID="PageHeader" ID="PageHeader">
    <script src="JS/POSLog.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <link href="Css/POSLog_<%=Lang %>.css?v=<%=Centrix_Version %>" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="PageBody" ID="PageBody">
    <div id='content' class="View-info">
        <div class="TabbedPanelsContent">
            <div class="TabbedPanelsContent-info">
                <div class="Add-new-form" style="min-height: 450px;">
                    <div class="Details-div">
                        <span class="details-div-title" resourcekey="PosSyncLogSearch"></span>
                        <div class="Action-div display-none">
                            <ul>
                              
                                <li><span class="ActionReset-link clear-link"></span><a class="ActionReset-link clear-link">
                                    <span resourcekey="ResetWalkin"></span></a></li>
                              
                            </ul>
                        </div>
                    </div>
                    <div class="search-div">
                        <table border="0" cellpadding="0" cellspacing="0" class="Data-Table3col">
                            <tr>
                                <th class="label-title verticalAlign-top">
                                    <span class="label-title" resourcekey="Store"></span>
                                </th>
                                <td>
                                   <div class="viewedit-display-none">
                                <select class="Store  lst">
                                </select>
                            </div>
                                </td>
                                <th class="label-title verticalAlign-top">
                                    <span class="label-title" resourcekey="SyncDateFrom"></span>
                                </th>
                                <td>
                                    <div class="viewedit-display-none">
                                        <input class="cx-control SyncDateFrom date viewedit-display-none" type="text" />
                                    </div>
                                </td>
                                <th class="label-title verticalAlign-top">
                                    <span class="label-title" resourcekey="SyncDateTo"></span>
                                </th>
                                <td>
                                    <div class="viewedit-display-none">
                                        <input class="cx-control SyncDateTo date viewedit-display-none" type="text" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th class="label-title verticalAlign-top">
                                    <span class="label-title" resourcekey="Entity"></span>
                                </th>
                                <td>
                                    <div class="viewedit-display-none">
                                <select class="Entity  lst">
                                </select>
                            </div>
                                </td>
                                 <th class="label-title verticalAlign-top">
                                    <span class="label-title" resourcekey="Status"></span>
                                </th>
                                <td>
                            <input type="checkbox" class="cx-control Success chk" />
                                <span class="label-title" resourcekey="Success"></span>
                               <input type="checkbox" class="cx-control Faild chk" />
                                <span class="label-title" resourcekey="Faild"></span>
                                </td>
                                <th class="label-title verticalAlign-top">
                                    <span class="label-title" resourcekey="Message"></span>
                                </th>
                                <td>
                                    <div class="viewedit-display-none">
                                        <input class="cx-control Message string viewedit-display-none" type="text" />
                                    </div>
                                </td>
                            </tr>
                            
                            <tr>
                                <td colspan="6" class="search-buttons">
                                    <div class="filter-reset">
                                        <a class="lnkSearch button2" resourcekey="Filter"></a>&nbsp;
                                        <%-- <input class="button-cancel LnkReset" value="Reset" type="button" />--%>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <table border="0" cellpadding="0" cellspacing="0" class="Data-Table3col SyncLog-info display-none">
                        <tr>
                            <th class="label-title verticalAlign-top">
                                <span class="label-title" resourcekey="Store"></span><span class="smblreqyuired viewedit-display-none">
                                    *</span>
                            </th>
                            <td>
                                <span class="label-data NumberOfWalkIn viewedit-display-block"></span>
                                <input class="cx-control NumberOfWalkIn integerMask integer number required viewedit-display-none"
                                    maxlength="5" type="text" />
                            </td>
                            <th class="label-title verticalAlign-top">
                                <span class="label-title" resourcekey="WalkInDate"></span><span class="smblreqyuired viewedit-display-none">
                                    *</span>
                            </th>
                            <td>
                                <span class="label-data WalkInDate viewedit-display-block"></span>
                                <div class="viewedit-display-none">
                                    <input class="cx-control WalkInDate date required viewedit-display-none" type="text" />
                                </div>
                            </td>
                            <th class="label-title verticalAlign-top">
                                <span class="label-title" resourcekey="WalkInTime"></span><span class="smblreqyuired viewedit-display-none">
                                    *</span>
                            </th>
                            <td>
                                <div class="viewedit-display-none">
                                    <select class="lstTime viewedit-display-none Time-select">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th class="label-title verticalAlign-top">
                                <span class="label-title" resourcekey="Comments"></span>
                            </th>
                            <td>
                                <span class="label-data Comments viewedit-display-block"></span>
                                <textarea class="cx-control Comments txt viewedit-display-none" maxlength="500"></textarea>
                            </td>
                        </tr>
                    </table>
                    <table width="100%" border="0" cellpadding="0" cellspacing="1" class="details-grid">
                        <tr id="-2">
                            <th class="th-data-table th-SyncLog-num">
                                #
                            </th>
                            <th class="th-data-table-SyncLog">
                                <span resourcekey="Store"></span>
                            </th>
                            <th class="th-data-table-SyncLog">
                                <span resourcekey="Entity"></span>
                            </th>
                            <th class="th-data-table-SyncLog">
                                <span resourcekey="Date"></span>
                            </th>
                            <th class="th-data-table-SyncLog ">
                                <span resourcekey="Status"></span>
                            </th>
                            <th class="th-data-table-SyncLog">
                                <span resourcekey="Message"></span>
                            </th>
                            <%--<th class="th-data-table-SyncLog">
                                <span resourcekey="CreatedBy"></span>
                            </th>--%>
                        </tr>
                        <tr id="-2">
                            <td colspan="7" class="td-data-border">
                            </td>
                        </tr>
                        <tr id="-2">
                            <td colspan="7" class="td-data-border">
                                <div class="DragDropBackground display-none">
                                    <span class="background-text" resourcekey="NoDataFound"></span>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <div class="details-div">
                    <table width="100%" border="0" cellpadding="0" cellspacing="1" class="ItemList-info ">
                        <tr class="ContentRow SyncLogItemRow">
                            <td class="center-content td-SyncLog-num">
                                <span class="ItemIdentity"></span><span class="WalkInId" style="display: none"></span>
                            </td>
                            <td class="text-align-center td-SyncLog-table">
                                <span class="StoreName  td-SyncLog-table"></span>
                            </td>
                           
                            <td class="text-align-center td-SyncLog-table">
                                <span class="EntityName"></span>
                            </td>
                             <td class="text-align-center td-SyncLog-table">
                                <span class="SyncDate date nowrap-noWidth"></span>
                            </td>
                            <td class="text-align-center td-SyncLog-table">
                                <span class="Status nowrap viewedit-display-block"></span>
                            </td>
                            <td class="text-align-center td-SyncLog-table">
                                <span class="Message"></span>
                            </td>
                           <%-- <td class="text-align-center td-SyncLog-table">
                                <span class="CreatedBy nowrap-noWidth"></span>
                            </td>--%>
                        </tr>
                    </table>
                    </div>
                </div>
                <div class="five-px">
                </div>
            </div>
        </div>
    </div>
</asp:Content>
