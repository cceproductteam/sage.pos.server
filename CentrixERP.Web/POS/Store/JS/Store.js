var StoreServiceURL;
var StoreViewItemTemplate;
var DefaultCurrencyAC;
var LocationAC;
var ItemListTemplate;
var StoreId = selectedEntityId;
var lastUpdatedDate = null;
var addStoreMode = false;
var ArVisaCustomerAc, ArCashCustomerAC, ArMixedCustomerAC, GLAccountAC, BankAccountAC,GlNationalAC,GlCheckAC;

function InnerPage_Load() {
    setSelectedMenuItem('.pos-icon');
    StoreServiceURL = systemConfig.ApplicationURL_Common + systemConfig.StoreServiceURL;
    currentEntity = systemEntities.Store;

    setPageTitle(getResource("Store"));
    setAddLinkAction(getResource("AddStore"), 'add-new-item-large');

    GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.AddStoreItemTemplate, function () {
        GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.StoreListItemTemplate, loadData, 'ItemListTemplate');
        GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.RegisterTabTemplate, '', 'RegisterTemplate');

    }, 'AddItemTemplate');


    $('li:.RegisterTab').live('click', function () {
        RegisterTemplate = $(RegisterTemplate);
        checkPermissions(this, systemEntities.RegisterEntityId, -1, RegisterTemplate, function (template) {
            getRegisters(selectedEntityId);
        });
    });

    //Advanced search Data
    mKey = 'Store';
    InnerPage_Load2();
    $('.QuickSearch-holder').show();
    LoadSearchForm2(mKey, false);
    //Advanced search Data


    $('.result-block').live("click", function () {
        StoreId = $(this).attr('id');
    });

    deletedEntityCallBack = function () {
        removeDeletedEntity();
    };

    updatedEntityCallBack = function () {
        getEntityData(selectedEntityId);
    };
}

function loadData(data) {

    GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.StoreListViewItemTemplate, function () {
        checkPermissions(this, systemEntities.Store, -1, StoreViewItemTemplate, function (returnedData) {
            StoreViewItemTemplate = returnedData;
            loadEntityServiceURL = StoreServiceURL + "FindByIdLite";
            entityItemListTemplate = data;
            entityItemViewTemplate = returnedData;
            findAllEntityServiceURL = StoreServiceURL + "FindAllLite";
            deleteEntityServiceURL = StoreServiceURL + "Delete";
            selectEntityCallBack = getStoreInfo;
            listEntityCallBack = callBackAfterList;
            loadDefaultEntityOptions();
        });
    }, 'StoreViewItemTemplate');

}

function initControls(template, isAdd) {
    
    if (!template) template = $('.TabbedPanels');
    if (!isAdd)
        $('.historyUpdated').append(HistoryUpdatedListViewTemplate.clone());
    LoadDatePicker(template.find('input:.date'), 'dd-mm-yy');
    preventInputChars(template.find('input:.integer'));
    template.find('.numeric').autoNumeric();
    debugger;
    DefaultCurrencyAC = template.find('select:.DefaultCurrency').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.DefaultCurrencWebServiceURL + 'FindAllLite', required: true, multiSelect: false });
    LocationAC = template.find('select:.Location').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.LocationWebServiceURL + 'FindAllLite', required: true, multiSelect: false });
    
    if (isAdd == true) {

        LocationAC.Disabled(false);
        LocationAC.set_Required(false);
        DefaultCurrencyAC.set_Required(false);
        DefaultCurrencyAC.Disabled(false);
    }
    else {

        LocationAC.Disabled(true);
        DefaultCurrencyAC.Disabled(true);
        LocationAC.set_Required(false);
        DefaultCurrencyAC.set_Required(false);
    }
    ArCashCustomerAC = template.find('select:.ArCashCustomer').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.GLAccountWebServiceURL + 'FindAllLite', required: true, multiSelect: false });
    ArVisaCustomerAC = template.find('select:.ArVisaCustomer').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.GLAccountWebServiceURL + 'FindAllLite', required: true, multiSelect: false });
    ArMixedCustomerAC = template.find('select:.ArMixedCustomer').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.GLAccountWebServiceURL + 'FindAllLite', required: true, multiSelect: false });
    GlAccountAC = template.find('select:.GlAccount').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.GLAccountWebServiceURL + 'FindAllLite', required: true, multiSelect: false });
    //BankAccountAC = template.find('select:.BankAccount').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.BankAccountWebServiceURL + 'FindAllLite', required: true, multiSelect: false });
    GlNationalAC = template.find('select:.GlNational').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.GLAccountWebServiceURL + 'FindAllLite', required: true, multiSelect: false });
    GlCheckAC = template.find('select:.GlCheck').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.GLAccountWebServiceURL + 'FindAllLite', required: true, multiSelect: false });
}

function initAddControls(template) {
    initControls(template, true);
}

function getStoreInfo(item) {

    if ($('.result-block').length > 0 && getQSKey('fdb') > 0 && isFirstTimeAdd) {
        $('.add-entity-button').first().click();
    }
    if (item != null) { }
}

function setListItemTemplateValue(entityTemplate, entityItem) {
    entityTemplate.find('.result-data1 label').text(Truncate(entityItem.StoreName, 20));
    entityTemplate.find('.result-data2 label').textFormat(Truncate(entityItem.StoreCode, 20));
    entityTemplate.find('.result-data3 label').textFormat(Truncate(entityItem.DefaultCurrCode, 20));
    return entityTemplate;
}

function setListItemServiceData(keyword, pageNumber, resultCount) {
    if (!QuickSearch) {
        return formatString('{ keyword: "{0}", page:{1}, resultCount:{2} ,argsCriteria:"{ StoreId:{3}}"}', keyword, pageNumber, resultCount, filterEntityId);
    }
    else {
        return formatString('{mKey:"{0}",SearchCriteriaObj:"{1}",SortType:{2},SortFields:"{3}",TotalRecords:{4},PageNumber:{5}}', mKey, SearchCriteriaObj, SortType, SortFields, resultCount, pageNumber);
    }
}



 function SaveEntity(isAdd, template) {
     var controlTemplate;
     var valid = true;

     if (isAdd) {
         controlTemplate = $('.add-Store-info');
         showLoading('.add-new-item');

     }
     else {
         controlTemplate = $('.Store-info');
         showLoading('.Result-info-container');
     }


     if (!saveForm(controlTemplate)) {
         hideLoading();
         valid = false;
     }

     addStoreMode = isAdd;
     if (!valid) {
         hideLoading();
         return false;
     }

     StoreId = -1;

     if (!isAdd) {
         StoreId = selectedEntityId;
         lastUpdatedDate = getEntityLockDateFormat(currentEntityLastUpdatedDate);
     }



     var StoreObj = {

         StoreName: controlTemplate.find('input.StoreName').val(),
         StoreNameAr: controlTemplate.find('input.StoreNameAr').val(),
         StoreCode: controlTemplate.find('input.StoreCode').val(),
         DefaultCurrencyId: DefaultCurrencyAC.get_ItemValue(),
         DefaultCurrCode: "",
         DefaultCurrSymbol: "",
         LocationId: LocationAC.get_ItemValue(),
         LocationCode: LocationAC.get_Item().LocationCode,
         LocationDesc: LocationAC.get_Item().LocationName,
         IpAddress: controlTemplate.find('input.IpAddress').val(),
         MacAddress: controlTemplate.find('input.MacAddress').val(),
         DataBaseInstanceName: controlTemplate.find('input.DataBaseInstanceName').val(),
         DataBaseName: controlTemplate.find('input.DataBaseName').val(),
         DataBaseUserName: controlTemplate.find('input.DataBaseUserName').val(),
         DataBasePassword: controlTemplate.find('input.DataBasePassword').val(),
         ArCashCustomerId: ArCashCustomerAC.get_Item().value,
         ArVisaCustomerId: ArVisaCustomerAC.get_Item().value,
         ArMixedCustomerId: ArMixedCustomerAC.get_Item().value,
         GLAccountId: GlAccountAC.get_Item().value,
         BankAccountId:0,// BankAccountAC.get_Item().value
         //DefaultTax: $('input.DefaultTax').val(),
         //DisplayPricses: $('input.DisplayPricses').val(),
         //CashierDiscounts: $('input:.CashierDiscounts').is(':checked'),
         //LegalEntityId: $('input.LegalEntityId').val(),
         //WeightScaleFormulaId: $('input.WeightScaleFormulaId').val(),
         //IsUsed: $('input:.IsUsed').is(':checked')
         GlNationalId: GlNationalAC.get_Item().value, // BankAccountAC.get_Item().value
         GlCheckId: GlCheckAC.get_Item().value
         

     };
     var data = formatString('{id:{0}, StoreInfo:"{1}"}', isAdd ? -1 : selectedEntityId, escape(JSON.stringify(StoreObj)));

     if (StoreId > 0)
         url = StoreServiceURL + "Edit";
     else
         url = StoreServiceURL + "Add";

     post(url, data, function (returnedValue) { saveSuccess( controlTemplate,returnedValue  ); }, saveError, getEntitylockAjaxHeaders(lastUpdatedDate));
 }

 function saveSuccess(template, returnedValue) {
  var StoreObject = returnedValue.result;
     if (returnedValue.statusCode.Code == 501) {         
         hideLoading();
         showStatusMsg(getResource("DublicatedStoreNameorCode"));
         return false;
     }
     else if (returnedValue.statusCode.Code == 0) {
         hideLoading();
         if (returnedValue.statusCode.message == 'validation' && returnedValue.result) {
             filterError(returnedValue.result,template);
         }
         showStatusMsg(getResource("FaildToSave"));
         return false;
     }
     
     if (!addStoreMode) {
         showStatusMsg(getResource("savedsuccessfully"));
         hideLoading();
         loadEntityData(loadEntityServiceURL, entityItemViewTemplate, selectedEntityId, getStoreInfo);
     }
     else {
         hideLoading();
         addStoreMode = false;
         $('.add-new-item').hide();
         $('.add-new-item').empty();
         appendNewListRow(returnedValue.result);
         showStatusMsg(getResource("savedsuccessfully"));

     }
 }

 function saveError(ex) {
     hideLoading();
     handleLockEntityError(ex, deletedEntityCallBack, updatedEntityCallBack);
 }



 function filterError(errors, template) {
     var allMsg = '';
     template.find('.validation').remove();
     $.each(errors, function (index, error) {
         // var template = $('.Store-info');
         if (error.GlobalMessage) {
             allMsg += '<span>' + error.Message + '<br/></span>';
         }
         else if (error.ControlName == 'StoreName') {
             addPopupMessage(template.find('input.StoreName'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'StoreNameAr') {
             addPopupMessage(template.find('input.StoreNameAr'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'StoreCode') {
             addPopupMessage(template.find('input.StoreCode'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'DefaultCurrency') {
             addPopupMessage(template.find('select.DefaultCurrency'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'DefaultCurrCode') {
             addPopupMessage(template.find('input.DefaultCurrCode'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'DefaultCurrSymbol') {
             addPopupMessage(template.find('input.DefaultCurrSymbol'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'Location') {
             addPopupMessage(template.find('select.Location'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'LocationCode') {
             addPopupMessage(template.find('input.LocationCode'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'LocationDesc') {
             addPopupMessage(template.find('input.LocationDesc'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'IpAddress') {
             addPopupMessage(template.find('input.IpAddress'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'MacAddress') {
             addPopupMessage(template.find('input.MacAddress'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'DataBaseInstanceName') {
             addPopupMessage(template.find('input.DataBaseInstanceName'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'DataBaseName') {
             addPopupMessage(template.find('input.DataBaseName'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'DataBaseUserName') {
             addPopupMessage(template.find('input.DataBaseUserName'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'DataBasePassword') {
             addPopupMessage(template.find('input.DataBasePassword'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'DefaultTax') {
             addPopupMessage(template.find('input.DefaultTax'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'DisplayPricses') {
             addPopupMessage(template.find('input.DisplayPricses'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'CashierDiscounts') {
             addPopupMessage(template.find('input.CashierDiscounts'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'LegalEntity') {
             addPopupMessage(template.find('input.LegalEntity'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'WeightScaleFormula') {
             addPopupMessage(template.find('input.WeightScaleFormula'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'IsUsed') {
             addPopupMessage(template.find('input.IsUsed'), getResource(error.ResourceKey));
         }

     });

     hideLoading();
     if (allMsg.trim()) {
         showOkayMsg('Validation error', allMsg.trim());
     }
 }


 function callBackAfterList() {
     if ($('.result-block').length <= 0 && getQSKey('fdb') > 0 && isFirstTimeAdd) {
         $('.add-entity-button').first().click();
     }
 }


 function getRegisters(storeId) {
     debugger;
     $('.registers-tab').children().remove();
     showLoading($('.TabbedPanels'));
     $.ajax({
         type: "POST",
         url: systemConfig.ApplicationURL_Common + systemConfig.RegisterServiceURL + "FindAllLite",
         contentType: "application/json; charset=utf-8",
         data: formatString('{ keyword: "", page:1, resultCount:10,argsCriteria:"{StoreId:{0}}"}', storeId),
         dataType: "json",
         success: function (data) {
             var RegistersList = eval("(" + data.d + ")").result;
             AddRegisterTab(null, storeId);
             if (RegistersList != null) {
                 $('.registers-tab').parent().find('.DragDropBackground').remove();
                 $.each(RegistersList, function (index, item) {
                     AddRegisterTab(item, storeId);
                 });
             }
             hideLoading();
         },
         error: function (e) {
         }
     });

 }



 function AddRegisterTab(item, StoreId) {
     debugger;

     var template = $(RegisterTemplate).clone();
     var id = (item != null) ? item.Id : 0;
     $('a:.add-new-register').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.RegisterList + "?sId=" + StoreId);

     if (item != null) {
         template = mapEntityInfoToControls(item, template);
         template.find('a:.ViewRegister-link').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.RegisterList + "?key=" + item.RegisterId);
         $('.registers-tab').append(template);
     }
 }


 function getICcurrency() {
//     $.ajax({
//         type: "POST",
//         url: systemConfig.ApplicationURL_Common + systemConfig.IntegrationWebServiceURL + "IcCurrencyFindAll",
//         contentType: "application/json; charset=utf-8",
//         data: formatString('{}'),
//         dataType: "json",
//         success: function (data) {

//             var List = eval("(" + data.d + ")").result;
//         },
//         error: function (e) {
//         }
//     });
 }
