<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StoreList.aspx.cs" MasterPageFile="~/Common/MasterPage/ERPSite.master"
    Inherits="Centrix.POS.Server.Web.POS.Store.StoreList" %>

<asp:Content runat="server" ContentPlaceHolderID="PageHeader" ID="PageHeader">
    <script type="text/javascript" src="JS/Store.js?v=<%=Centrix_Version %>"></script>
    <script src="../../Common/JScript/CommonTabs.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <script src="../../Common/AdvancedSearch/JS/AdvancedSearch.js?v=<%=Centrix_Version %>"></script>
 
 
 
   
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="PageBody" ID="PageBody">
    <div id="demo2" class="result-container">
        <div class="SearchDivContainer">
            <div class="SD-SearchDiv">
                <input type="text" class="SD-ipnutText-MainSearch search-text-box searchTextbox" /></div>
            <div class="Quick-Search-Div">
            </div>
            <div class="MoreSearchOptions">
                <a class="more-options"><span resourcekey="MoreOptions"></span></a>
            </div>
        </div>
        <div class="GridTableDiv-header">
            <table class="GridTableFloating" cellpadding="0" cellspacing="0" border="0">
                <thead class="THeadGridTable">
                    <tr>
                        <th class="THead-Th th-number">
                            <span>#</span>
                        </th>
                        <th class="THead-Th">
                            <span resourcekey="StoreName"></span>
                        </th>
                        <th class="THead-Th">
                            <span resourcekey="StoreCode"></span>
                        </th>
                        <th class="THead-Th">
                            <span resourcekey="DefaultCurrCode"></span>
                        </th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="GridTableDiv-body">
            <table class="TBodyGridTable Result-list" cellpadding="0" cellspacing="0" border="0">
                <thead class="THeadGridTable">
                    <tr>
                        <tr>
                            <th class="THead-Th th-number">
                                <span>#</span>
                            </th>
                            <th class="THead-Th">
                                <span resourcekey="StoreName"></span>
                            </th>
                            <th class="THead-Th">
                                <span resourcekey="StoreCode"></span>
                            </th>
                            <th class="THead-Th">
                                <span resourcekey="DefaultCurrCode"></span>
                            </th>
                        </tr>
                    </tr>
                </thead>
                <tbody class="TBodyGridTable result-list">
                </tbody>
            </table>
        </div>
        <div class="total-result-div TotalDiv">
            <span></span>
        </div>
    </div>
    <div id='content'>
        <div class="Result-info-container">
            <div class="result-name">
                <ul>
                    <li class="nameLI"><span resourcekey="HStoreName"></span><span class="StoreName"></span>
                    </li>
                    <li class="emailLI"><span resourcekey="HStoreCode"></span><span class="StoreCode"></span>
                    </li>
                </ul>
            </div>
            <div style="float: right;" id='page_navigation'>
            </div>
            <div class="ClearDiv">
            </div>
            <div class="test">
                <div class="QuickSearchDiv">
                </div>
                <div id="TabbedPanels1" class="TabbedPanels">
                    <ul class="TabbedPanelsTabGroup">
                        <li class="TabbedPanelsTab" tabindex="1">
                            <div class="TabbedPanelsTabSelected-arrow" style="display: block;">
                            </div>
                            <span class="TabbedPanelsTab-img Summery-tab"></span><span resourcekey="Summary">
                            </span></li>
                           <%-- <li class="TabbedPanelsTab RegisterTab" tabindex="2">
                            <div class="TabbedPanelsTabSelected-arrow">
                            </div>
                            <span class="TabbedPanelsTab-img register-tab"></span><span resourcekey="Register"></span> </li>--%>

                        <%--<li class="TabbedPanelsTab more-tabs">
                            <div class="TabbedPanelsTabSelected-arrow">
                            </div>
                            <span class="TabbedPanelsTab-img More-tab"></span><span class="more-title">More</span>
                            <span class="More-tabs-arrow"></span></li>--%>
                    </ul>
                    <div class="TabbedPanelsContentGroup">
                    </div>
                    <!--More-->
                    <div class="More-holder">
                        <div class="moreTabsList">
                            <ul class="moreTabs-ul">
                            </ul>
                        </div>
                    </div>
                    <!--End More-->
                </div>
            </div>
        </div>
    </div>
    <div class="add-new-div">
        <div class="add-new-item display-none">
        </div>
    </div>
</asp:Content>
