﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="POSSetup.aspx.cs" Inherits="CentrixERP.Web.POS.POSSetup.POSSetup"
    MasterPageFile="~/Common/MasterPage/ERPSite.master" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="PageHeader">
    <link href="CSS/ar-setup.css" rel="stylesheet" type="text/css" />
    <script src="JS/erpIntegration.js" type="text/javascript"></script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="PageBody">
    <div class="Adminstration-main-div">
        <div class="administrative-settings">
            <div class="link-title">
            </div>
            <ul>
                <li class="right-col">
                    <div>
                        <div class="icon master-data-setup">
                        </div>
                        <div class="link-title" resourcekey="POSAdministrationData">
                        </div>
                        <div class="settings-links">
                            <ul>
                                <li><a href="../Store/StoreList.aspx" resourcekey="Store"></a></li>
                                <li>|</li>
                                <li><a href="../Register/RegisterList.aspx" resourcekey="Register"></a></li>
                                <li>|</li>
                                <%-- <li><a href="../GiftVoucher/GiftVoucherList.aspx" resourcekey="GiftVoucher"></a>
                                </li>
                                <li>|</li>--%>
                                <li><a href="../CustomFild/CustomFildList.aspx" resourcekey="CustomFild"></a></li>
                                <li>|</li>
                                <%--  <li><a href="../QuickKeys/QuickKeysList.aspx" resourcekey="QuickKeys"></a></li>
                                <li>|</li>--%>
                                <li><a href="../SystemConfigration/SystemConfigration.aspx" resourcekey="POSConfiguration">
                                </a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="right-col">
                    <div>
                        <div class="icon company-profile">
                        </div>
                        <div class="link-title" resourcekey="POSSYNCHRONIZATION">
                        </div>
                        <div class="settings-links">
                            <ul>
                                <%--<li><a href="../BackEndSync/BackEndSync.aspx" resourcekey="BackEndSync"></a></li>
                                  <li>|</li>--%>
                                <li><a href="../POSSync/POSSync.aspx" resourcekey="POSSync"></a></li>
                                <li>|</li>
                                <%--  <li><a href="../SyncSchedule/SyncScheduleList.aspx" resourcekey="SyncSchedule"></a>
                                </li>--%>
                                <%--   <li>|</li>--%>
                                <li><a href="../Register/RegisterStatus.aspx" resourcekey="RegisterStatus"></a></li>
                                <%-- <li>|</li>
                                <li><a href="../PosSyncConfiguration/PosSyncConfigurationList.aspx" resourcekey="SyncConfiguration">
                                </a></li>--%>
                                <%-- <li>|</li>
                                <li><a href="../POSLog/POSLogList.aspx" resourcekey="PosSyncLog"></a></li>--%>
                                <li>|</li>
                                <li><a href="../ERPIntegration/ERPIntegration.aspx" resourcekey="InvoiceInquiry"></a>
                                </li>
                                <li>|</li>
                                <li><a href="../QuantityInquery/QuantityInquiry.aspx" resourcekey="QuantityInquiry">
                                </a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="left-col">
                    <div>
                        <div class="icon transactions-processing">
                        </div>
                        <div class="link-title" resourcekey="CONTACTINFORMATION">
                        </div>
                        <div class="settings-links">
                            <ul>
                                <li><a href="../Company/CompanyList.aspx" resourcekey="Company"></a></li>
                                <li>|</li>
                                <%--<li><a href="../Register/RegisterStatus.aspx" resourcekey="RegisterStatus"></a></li>
                                    <li>|</li>--%>
                                <li><a href="../Person/PersonList.aspx" resourcekey="Person"></a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="right-col">
                    <div>
                        <div class="icon transactions-processing">
                        </div>
                        <div class="link-title" resourcekey="ERPIntegration">
                        </div>
                        <div class="settings-links">
                            <ul>
                                <li><a href="javascript:;" class="btn-inv-post" resourcekey="PostInvoice"></a></li>
                                <li>|</li>
                                <li><a href="javascript:;" class="btn-refund-post" resourcekey="PostRefund"></a>
                                </li>
                              <%--  <li>|</li>
                                  <li><a href="javascript:;" class="btn-onaccount-post" resourcekey="PostOnAccount"></a>
                                </li>--%>
                                <li>|</li>
                                <li><a href="javascript:;" class="btn-exchange-post" resourcekey="PostExchange"></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="left-col">
                    <div>
                        <div class="icon transactions-processing">
                        </div>
                        <div class="link-title" resourcekey="POSInquiries">
                        </div>
                        <div class="settings-links">
                            <ul>
                                <li><a href="../../NewReports/CloseRegister/CloseRegisterInquiry.aspx" resourcekey="CloseRegisterInquiry">
                                </a></li>
                                <li>|</li>
                                <%--    <li><a href="../../NewReports/POSInvoice/POSInvoice.aspx" resourcekey="InvoiceReport">
                                </a></li>
                                <li>|</li>--%>
                                <%--<br />--%>
                              <li><a href="../../NewReports/TotalDailySales/POSTotalDailySales.aspx" resourcekey="TotalDailySales">
                                </a></li>
                              <li>|</li>
                               <%--  <li><a href="../../NewReports/DiscountReport/POSDiscountReport.aspx" resourcekey="DiscountReport">
                                </a></li>--%>
                                <%--   <br />--%>
                                  <br />
                                <li><a href="../../NewReports/Dailysales/POSDailysales.aspx" resourcekey="Dailysales">
                                </a></li>
                                <li>|</li>
                                <li><a href="../../NewReports/DailySalesPerItem/POSDailySalesPerItem.aspx" resourcekey="DailySalesPerItem">
                                </a></li>
                                <li>|</li>
                                <%--  <li>|</li>--%>
                                <%-- <li><a href="../../NewReports/SummarySalesPerItem/SummarySalesPerItem.aspx" resourcekey="SummarySalesPerItem">
                                </a></li>--%>
                               <br />
                                <li><a href="../../NewReports/CustomField/CustomFieldReport.aspx" resourcekey="CustomField">
                                </a></li>
                                <%--<li>|</li>
                                <li><a href="../../NewReports/ChequeDetails/POSChequeDetails.aspx" resourcekey="ChequeDetails">
                                </a></li>--%>
                                <li>|</li>
                                <li><a href="../../NewReports/GenerateBarcode/GenerateBarcode.aspx" resourcekey="GenerateBarcode">
                                </a></li>
                                <br />
                            </ul>
                        </div>
                    </div>
                </li>
                <%-- <li class="left-col">
                    <div>
                        <div class="icon reports" style="display:none;">
                        </div>
                        <div class="link-title" resourcekey="REPORTS" style="display:none;"></div>
                        <div class="settings-links">
                            <ul>
                                <%--<li><a href="../../NewReports/Vendor/VendorStatement.aspx" resourcekey="VendorStatement"></a></li>
                                <li> | <a href="../../NewReports/Vendor/VendorTransaction.aspx" resourcekey="VendorTransactions"></a></li>
                                <li>| <a href="../../NewReports/Vendor/VendorAging.aspx" resourcekey="VendorAgeing"></a></li>
                            </ul>
                        </div>
                    </div>
                </li>--%>
            </ul>
        </div>
        <div class="admin-bottom-img">
        </div>
    </div>
</asp:Content>
