﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CentrixERP.Common.Web;
using CentrixERP.Configuration;


namespace CentrixERP.Web.POS.ICQuantityInquiry
{
    public partial class ICQuantityInquiry : BasePageLoggedIn
    {
        public ICQuantityInquiry()
            : base(Enums_S3.Permissions.SystemModules.None) { }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}