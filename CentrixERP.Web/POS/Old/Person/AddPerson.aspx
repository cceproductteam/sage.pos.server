<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddPerson.aspx.cs" MasterPageFile="~/Common/MasterPage/Site.master"
    Inherits="Centrix.StandardEdition.CRM.Web.CRM.Person.AddPerson" %>

<asp:Content runat="server" ContentPlaceHolderID="PageHeader" ID="PageHeader">
    <script type="text/javascript" src="JS/Person.js?v=<%=Centrix_Version %>"></script>
    <script src="../../Common/JScript/CommonTabs.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <script type="text/javascript">
        addPersonMode = true;
           
    </script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="PageBody" ID="PageBody">
    <div id='content' class="Person-info">
        <div class="Add-new-form">
            <div class="Add-title">
                <span resourcekey="PersonDetails"></span>
                <div class="Action-div">
                    <ul>
                        <li><span class="ActionSave-link save-tab-lnk"></span><a class="ActionSave-link save-tab-lnk">
                            <span resourcekey="Save"></span></a></li>
                        <li><span class="ActionCancel-link cancel-tab-lnk"></span><a class="ActionCancel-link cancel-tab-lnk">
                            <span resourcekey="Cancel"></span></a></li>
                    </ul>
                </div>
            </div>
            <table border="0" cellpadding="0" cellspacing="0" class="Add-new-Table Person-info">
                <tr>
                    <th>
                        <span class="label-title" resourcekey="Company"></span>
                    </th>
                    <td>
                     <a class="company-link"> <span class="label-data Company lnkLabel viewedit-display-block company-filed"></span></a>
                        <div class="viewedit-display-none company-list">
                            <select class="Company lst">
                            </select>
                        </div>
                    </td>
                     <th class="">
                        <span class="label-title" resourcekey="Salutation"></span>
                    </th>
                    <td class="">
                        <div class="">
                            <select class="Salutation lst">
                            </select>
                        </div>
                    </td>
                   
                    <th>
                        <span class="label-title" resourcekey="Gender"></span>
                    </th>
                    <td>
                        <div class="">
                            <select class="Gender lst">
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                
                 
                   
                     <th>
                        <span class="label-title" resourcekey="MaritalStatus"></span>
                    </th>
                    <td>
                        <div class="">
                            <select class="MaritalStatus lst">
                            </select>
                        </div>
                    </td>
                    <th>
                        <span class="label-title" resourcekey="DateOfBirth"></span>
                         <span class="smblreqyuired BirthdayRequired display-none"> *</span>
                    </th>
                    <td>
                        <div class="">
                            <input class="cx-control DateOfBirth  date  " type="text" />
                        </div>
                    </td>
                     <th>
                        <span class="label-title" resourcekey="Anniversary"></span>
                         <span class="smblreqyuired AnniversaryRequired display-none"> *</span>
                    </th>
                    <td>
                        <div class="">
                            <input class="cx-control Anniversary date " type="text" />
                          
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>
                        <span class="label-title" resourcekey="FirstNameEn"></span><span class="smblreqyuired viewedit-display-none">
                            *</span>
                    </th>
                    <td>
                        <input class="cx-control FirstNameEn  txt required string " maxlength="60" minlength="3"
                            type="text" />
                    </td>

                    <th class="">
                        <span class="label-title" resourcekey="MiddleNameEn"></span>
                    </th>
                    <td class="">
                        <input class="cx-control MiddleNameEn  txt  string " maxlength="60" minlength="3"
                            type="text" />
                    </td>
                    <th>
                        <span class="label-title" resourcekey="LastNameEn"></span><span class="smblreqyuired viewedit-display-none">
                            *</span>
                    </th>
                    <td>
                        <input class="cx-control LastNameEn  txt required   string " maxlength="60" minlength="3"
                            type="text" />
                    </td>
                   
                </tr>
                <tr>
                    <th class="display-none">
                        <span class="label-title" resourcekey="FirstNameAr"></span><span class="smblreqyuired viewedit-display-none">
                            *</span>
                    </th>
                    <td class="display-none">
                        <input class="cx-control FirstNameAr  txt    string " maxlength="60" minlength="3"
                            type="text" />
                    </td>
                    <th class="display-none">
                        <span class="label-title" resourcekey="MiddleNameAr"></span>
                    </th>
                    <td class="display-none">
                        <input class="cx-control MiddleNameAr  txt  string " maxlength="60" minlength="3"
                            type="text" />
                    </td>
                    <th class="display-none">
                        <span class="label-title" resourcekey="LastNameAr"></span><span class="smblreqyuired viewedit-display-none">
                            *</span>
                    </th>
                    <td class="display-none">
                        <input class="cx-control LastNameAr  txt    string " maxlength="60" minlength="3"
                            type="text" />
                    </td>
                </tr>
                <tr>
                   
                    <th>
                        <span class="label-title" resourcekey="ContactType"></span>
                    </th>
                    <td>
                        <div class="">
                            <select class="ContactType lst">
                            </select>
                        </div>
                    </td>
                    <th>
                        <span class="label-title" resourcekey="InterestedIn"></span>
                    </th>
                    <td>
                        <div class="">
                            <select class="InterestedIn lst">
                            </select>
                        </div>
                    </td>
                    <th class="">
                        <span class="label-title" resourcekey="Department"></span>
                    </th>
                    <td class="">
                        <div class="">
                            <select class="Department lst">
                            </select>
                        </div>
                    </td>
                   
                </tr>
                <tr>
                    <th>
                        <span class="label-title" resourcekey="Nationality"></span>
                    </th>
                    <td>
                        <div class="">
                            <select class="Nationality lst">
                            </select>
                        </div>
                    </td>
                    <th>
                        <span class="label-title" resourcekey="NotifyforBirthday"></span>
                         
                    </th>
                    <td>
                        <input type="checkbox" class="NotifyForBirthday" />
                    </td>
                    <th>
                        <span class="label-title" resourcekey="NotifyForAnniversary"></span>
                       
                    </th>
                    <td>
                        <input type="checkbox" class="NotifyForAnniversary" />
                    </td>
                    
                    <th class="display-none">
                        <span class="label-title" resourcekey="Religion"></span>
                    </th>
                    <td class="display-none">
                        <div class="">
                            <select class="Religion lst">
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                <th>
                      <span class="label-title" resourcekey="Position"></span>
                    </th>
                    <td class="">
                        <div class="">
                            <select class="Position lst">
                            </select>
                        </div>
                    </td>

                    <th class="">
                            <span class="label-title" resourcekey="AccountManager"></span>
                        </th>
                        <td class="">
                            <span class="label-data viewedit-display-block AccountManager"></span>
                            <div class="viewedit-display-none">
                                <select class="AccountManager lst">
                                </select>
                            </div>
                        </td>
                        <th class="">
                            <span class="label-title" resourcekey="HearAboutUs"></span>
                        </th>
                        <td class="">
                            <span class="label-data viewedit-display-block HearAboutUs"></span>
                            <div class="viewedit-display-none">
                                <select class="HearAboutUs lst">
                                </select>
                            </div>
                        </td>
                        
                       
                </tr>
                <tr>
                 <th class="">
                            <span class="label-title" resourcekey="BestTimeToContact"></span>
                        </th>
                        <td class="">
                            <span class="label-data viewedit-display-block BestTimeToContact"></span>
                            <div class="viewedit-display-none">
                                <select class="BestTimeToContact lst">
                                </select>
                            </div>
                        </td>
                       
                        <th>
                            <span class="label-title" resourcekey="IsVip"></span>
                        </th>
                        <td>
                            <span class="label-data viewedit-display-block VipClass"></span>
                             <div class="viewedit-display-none">
                                <select class="VipClass lst">
                                </select>
                            </div>
                            <%--<input type="checkbox" class="IsVip viewedit-display-none" />--%>
                        </td>
                         <th>
                            <span class="label-title" resourcekey="FacebookPage"></span>
                        </th>
                        <td>
                            <span class="label-data nowrap viewedit-display-block FacebookPage"></span>
                            <input class="cx-control FacebookPage  txt   string viewedit-display-none" maxlength="60"
                                minlength="3" type="text" />
                        </td>
                </tr>
                <tr>
                       
                        <th>
                            <span class="label-title" resourcekey="LinkedInPage"></span>
                        </th>
                        <td>
                            <span class="label-data nowrap viewedit-display-block LinkedInPage"></span>
                            <input class="cx-control LinkedInPage  txt  string viewedit-display-none" maxlength="60"
                                minlength="3" type="text" />
                        </td>
                         <th>
                            <span class="label-title" resourcekey="TwitterAccount"></span>
                        </th>
                        <td>
                            <span class="label-data nowrap viewedit-display-block TwitterAccount"></span>
                            <input class="cx-control TwitterAccount  txt   string viewedit-display-none" maxlength="60"
                                minlength="3" type="text" />
                        </td>
                        <th>
                            <span class="label-title" resourcekey="Website"></span>
                        </th>
                        <td>
                            <span class="label-data nowrap viewedit-display-block Website"></span>
                            <input class="cx-control Website  txt  string viewedit-display-none" maxlength="60"
                                minlength="3" type="text" />
                        </td>
                    </tr>
                   
                <tr>
                    <th class="display-none">
                        <span class="label-title" resourcekey="Source"></span>
                    </th>
                    <td class="display-none">
                        <div class="">
                            <select class="Source lst">
                            </select>
                        </div>
                    </td>
                    <th class="display-none">
                        <span class="label-title" resourcekey="PrimaryContactEmployee"></span>
                    </th>
                    <td class="display-none">
                        <div class="">
                            <select class="PrimaryContactEmployee lst">
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="display-none">
                        <span class="label-title" resourcekey="ContactImage"></span>
                    </th>
                    <td class="display-none">
                        <input class="cx-control ContactImage" type="text" />
                    </td>
                </tr>
            </table>
        </div>
        <div class="five-px">
        </div>
        <div class="Add-new-form email-info">
            <div class="Add-title">
                <asp:Label ID="Label3" runat="server" resourcekey="Email"></asp:Label>
            </div>
            <table border="0" cellpadding="0" cellspacing="0" class="Add-new-Table">
                <tr>
                    <th>
                        <span class="label-title" resourcekey="BusinessEmail"></span>
                    </th>
                    <td>
                        <input class="cx-control BusinessEmail email string viewedit-display-none" type="text"
                            maxlength="60" />
                    </td>
                    <th>
                        <span class="label-title" resourcekey="PersonalEmail"></span>
                    </th>
                    <td>
                        <input class="cx-control PersonalEmail email string viewedit-display-none" type="text"
                            maxlength="60" />
                    </td>
                    <th>
                    </th>
                    <td>
                    </td>
                </tr>
            </table>
        </div>
        <div class="five-px">
        </div>
        <div class="Add-new-form">
            <div class="Add-title">
                <asp:Label ID="Label1" runat="server" resourcekey="Address"></asp:Label>
            </div>
            <table border="0" cellpadding="0" cellspacing="0" class="Add-new-Table">
                <tr>
                    <th>
                        <span class="label-title" resourcekey="Country"></span>
                    </th>
                    <td>
                        <div class="viewedit-display-block">
                            <select class="Country lst">
                            </select>
                        </div>
                    </td>
                    <th>
                        <span class="label-title" resourcekey="City"></span>
                    </th>
                    <td>
                        <input class="cx-control City  txt viewedit-display-block" maxlength="60" type="text" />
                    </td>
                    <th>
                        <span class="label-title" resourcekey="StreetAddress"></span>
                    </th>
                    <td>
                        <input class="cx-control StreetAddress txt viewedit-display-block" maxlength="60"
                            type="text" />
                    </td>
                </tr>
                <tr>
                  
                    <th class="display-none">
                        <span class="label-title" resourcekey="NearBy"></span>
                    </th>
                    <td class="display-none">
                        <span class="NearBy nowrap viewedit-display-block label-data"></span>
                        <input class="cx-control NearBy txt viewedit-display-none" type="text" maxlength="60" />
                    </td>
                    <th class="display-none">
                        <span class="label-title" resourcekey="Place"></span>
                    </th>
                    <td class="display-none">
                        <span class="Place nowrap viewedit-display-block label-data"></span>
                        <input class="cx-control Place txt viewedit-display-none" type="text" maxlength="60" />
                    </td>
                    <th class="display-none">
                        <span class="label-title" resourcekey="BuildingNumber"></span>
                    </th>
                    <td class="display-none">
                        <span class="BuildingNumber nowrap viewedit-display-block label-data"></span>
                        <input class="cx-control BuildingNumber integer txt viewedit-display-none" type="text"
                            maxlength="10" />
                    </td>
                </tr>
                <tr>
                    <th class="display-none">
                        <span class="label-title" resourcekey="POBox"></span>
                    </th>
                    <td class="display-none">
                        <span class="POBox nowrap viewedit-display-block label-data"></span>
                        <input class="cx-control POBox txt viewedit-display-none" type="text" maxlength="60" />
                    </td>
                    <th class="display-none">
                        <span class="label-title" resourcekey="ZipCode"></span>
                    </th>
                    <td class="display-none">
                        <span class="ZipCode nowrap viewedit-display-block label-data"></span>
                        <input class="cx-control ZipCode txt viewedit-display-none" type="text" maxlength="60" />
                    </td>
                    <th class="display-none">
                        <span class="label-title" resourcekey="Region"></span>
                    </th>
                    <td class="display-none">
                        <span class="Region nowrap viewedit-display-block label-data"></span>
                        <input class="cx-control Region txt viewedit-display-none" type="text" maxlength="60" />
                    </td>
                </tr>
                <tr>
                    <th class="display-none">
                        <span class="label-title" resourcekey="Area"></span>
                    </th>
                    <td class="display-none">
                        <span class="Area nowrap viewedit-display-block label-data"></span>
                        <input class="cx-control Area txt viewedit-display-none" type="text" maxlength="60" />
                    </td>
                </tr>
            </table>
        </div>
        <div class="five-px">
        </div>
        <div class="Add-new-form">
            <div class="Add-title">
                <asp:Label ID="Label2" runat="server" resourcekey="Phone"></asp:Label>
            </div>
            <table border="0" cellpadding="0" cellspacing="0" class="Add-new-Table">
                <tr>
                    <th>
                        <span class="label-title" resourcekey="Home"></span>
                    </th>
                    <td>
                        <input type="text" class="cx-control PcountryCode CountryCode txt country-num phone-format"
                            maxlength="6" />
                        <input type="text" class="cx-control PareaCode AreaCode txt city-num phone-format" maxlength="6" />
                        <input type="text" class="cx-control PphoneNumber phone-num txt phone-format" maxlength="8" />
                    </td>
                    <th>
                        <span class="label-title" resourcekey="Mobile"></span>
                    </th>
                    <td>
                        <input type="text" class="cx-control McountryCode  country-num phone-format" maxlength="6" />
                        <input type="text" class="cx-control MareaCode  city-num phone-format" maxlength="6" />
                        <input type="text" class="cx-control MphoneNumber  phone-num phone-format" maxlength="8" />
                    </td>
                    <th>
                        <span class="label-title" resourcekey="Fax"></span>
                    </th>
                    <td>
                        <input class="cx-control ScountryCode country-num phone-format" type="text" maxlength="6"
                            minlength="3" />
                        <input class="cx-control SareaCode AreaCode city-num phone-format" type="text" maxlength="6"
                            minlength="6" />
                        <input class="cx-control SphoneNumber phone-num phone-format" maxlength="8" type="text" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
