﻿var PersonServiceURL;
var PersonViewItemTemplate, ItemListTemplate, RelatedOpportunities, RelatedGiveawaysTemplate, RelatedCaseTemplate, RelatedOpportunityTemplate;
var CompanyAC, SalesManAC, SalutationAC, GenderAC, SourceAC, ReligionAC, MaritalStatusAC, DepartmentAC, PositionAC, CustomerReferralCompanyAC, CustomerReferralPersonAC, EmployeeUserAC, PersonTypeAC, PrimaryContactEmployeeAC;
var HearAboutUsAC, BestTimeToContactAC, AccountManagerAC, VipClassAC;
var addPersonMode = false;
var PersonId = -1, CustomerReferralCompanyId = -1;
var PersonName = '', CompanyName = '', CompanyId = -1, ContactEmployeeName = "", ContactEmployeeId = -1;
var ContactTypeAC, NationalityAC, InterestedInAC, IsBusinessEmail = false, IsPersonalEmail = false, IsBusinessEmailDuplication = false, IsPersonalEmailDupliaction = false;
var IsMobileDuplication = false;
var lastUpdatedDate = null;

function InnerPage_Load() {

    //setAddLinkAction(getResource("AddPerson"), 'add-new-item-large');
    $('span:.page-title').html(getResource("Person"));
    setSelectedMenuItem($(".pos-icon"));
 
    PersonServiceURL = systemConfig.ApplicationURL_Common + systemConfig.PersonServiceURL;

    currentEntity = systemEntities.PersonEntityId;


    //Advanced search Data
    mKey = 'pers';
    InnerPage_Load2();
    $('.QuickSearch-holder').show();
    LoadSearchForm2(mKey, false);
    //Advanced search Data

    GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.AddPersonTemplate, function () {
        GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.PersonListItemTemplate, loadData, 'ItemListTemplate');
        GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.RelatedGiveawaysListViewItemTemplate, null, 'RelatedGiveawaysTemplate')
        GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.RelatedCaseListViewItemTemplate, null, 'RelatedCaseTemplate')
        GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.RelatedOpportunityListViewItemTemplate, null, 'RelatedOpportunityTemplate')

    }, 'AddItemTemplate');


    $('.result-block').live("click", function () {
        PersonId = $(this).attr('id');
    });

    deletedEntityCallBack = function () {
        removeDeletedEntity();
    };

    updatedEntityCallBack = function () {
        getEntityData(selectedEntityId);
    };


    $('li:.email-tabs').live('click', function () {
        EmailTemplate = $(EmailTemplate);
        checkPermissions(this, systemEntities.EmailEntityId, -1, EmailTemplate, function (template) {
            getEmails(systemEntities.PersonEntityId, selectedEntityId);
        });
    });
    $('li:.note-tabs').live('click', function () {
        NoteTemplate = $(NoteTemplate);
        checkPermissions(this, systemEntities.NoteEntityId, -1, NoteTemplate, function (template) {
            getNotes(systemEntities.PersonEntityId, selectedEntityId);
        });
    });
    $('li:.address-tabs').live('click', function () {
        AddressTemplate = $(AddressTemplate);
        checkPermissions(this, systemEntities.AddressEntityId, -1, AddressTemplate, function (template) {
            getAddresses(systemEntities.PersonEntityId, selectedEntityId);
        });
    });
    $('li:.phone-tabs').live('click', function () {
        PhoneTemplate = $(PhoneTemplate);
        checkPermissions(this, systemEntities.PhoneEntityId, -1, PhoneTemplate, function (template) {
            getPhones(systemEntities.PersonEntityId, selectedEntityId);
        });
    });
    $('li:.Attach-tabs').live('click', function () {

        checkPermissions(this, systemEntities.AttachmentsEntityId, -1, $('.Action-div'), function (template) {
            getAttachments(systemEntities.PersonEntityId, selectedEntityId, 'Person', systemEntities.PersonEntityId);
        });
    });


    $('li:.Communication-tab').live('click', function () {
        checkPermissions(this, systemEntities.TaskId, -1, $('.Action-div'), function (template) {
            var personItem = { label: PersonName, value: selectedEntityId };
            if (CompanyId > 0) {
                personItem.Company = CompanyName;
                personItem.CompanyId = CompanyId;
            }
            getCommunication(selectedEntityId, systemEntities.PersonEntityId, selectedEntityId, PersonName, personItem);
        });
    });
    $('li:.Giveaways-tabs').live('click', function () {

        checkPermissions(this, systemEntities.GiveawaysEntityId, -1, RelatedGiveawaysTemplate, function (template) {
            RelatedGiveawaysTemplate = template;
            getRelatedGiveAways(selectedEntityId);
        });
    });
    $('li:.Cases-tabs').live('click', function () {

        checkPermissions(this, systemEntities.ClientCareEntityId, -1, RelatedCaseTemplate, function (template) {
            RelatedCaseTemplate = template;
            getRelatedCase(selectedEntityId);
        });
    });

    $('li:.Opportunity-tabs').live('click', function () {

        checkPermissions(this, systemEntities.OpportunityEntityId, -1, RelatedOpportunityTemplate, function (template) {
            RelatedOpportunityTemplate = template;
            getRelatedOpportunities(selectedEntityId);
        });
    });

    $('li:.SocialMedia-tabs').live('click', function () {

        SocialMediaTemplate = $(SocialMediaTemplate);
        checkPermissions(this, systemEntities.SocialMediaEntityID, -1, SocialMediaTemplate, function (template) {
            getSocialMedia(systemEntities.PersonEntityId, selectedEntityId);
        });
    });

}


function notifyForBirthDateCheckd(isChecked, editMood, controlTemplate) {
    if (isChecked) {
        controlTemplate.find('.DateOfBirth').addClass('required');
        if (editMood) {
            controlTemplate.find('.BirthdayRequired').addClass('viewedit-display-none');
            controlTemplate.find('span:.YNotifyForBirthday').show();
            controlTemplate.find('span:.NNotifyForBirthday').hide();
        }
        else {
            controlTemplate.find('.BirthdayRequired').show();
        }

    }
    else {
        controlTemplate.find('.DateOfBirth').removeClass('required');
        if (editMood) {
            controlTemplate.find('.BirthdayRequired').removeClass('viewedit-display-none');
            controlTemplate.find('span:.YNotifyForBirthday').hide();
            controlTemplate.find('span:.NNotifyForBirthday').show();
        }
        else {
            controlTemplate.find('.BirthdayRequired').hide();
        }

    }
}

function notifyForAnniversaryCheckd(isChecked, editMood, controlTemplate) {
    if (isChecked) {
        controlTemplate.find('.Anniversary').addClass('required');
        if (editMood) {
            controlTemplate.find('.AnniversaryRequired').addClass('viewedit-display-none');
            controlTemplate.find('span:.YNotifyForAnniversary').show();
            controlTemplate.find('span:.NNotifyForAnniversary').hide();
        }
        else {
            controlTemplate.find('.AnniversaryRequired').show();
        }

    }
    else {
        controlTemplate.find('.Anniversary').removeClass('required');
        if (editMood) {
            controlTemplate.find('.AnniversaryRequired').removeClass('viewedit-display-none');
            controlTemplate.find('span:.YNotifyForAnniversary').hide();
            controlTemplate.find('span:.NNotifyForAnniversary').show();
        }
        else {
            controlTemplate.find('.AnniversaryRequired').hide();
        }
    }
}


function loadData(data) {

    GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.PersonListViewItemTemplate, function () {
        //        checkPermissions(this, systemEntities.SendMassMail, -1, PersonViewItemTemplate, function (returnedData) {

        //        });

        checkPermissions(this, systemEntities.PersonEntityId, -1, PersonViewItemTemplate, function (returnedData) {
            PersonViewItemTemplate = returnedData;
            loadEntityServiceURL = PersonServiceURL + "FindByIdLite";
            entityItemListTemplate = data;
            entityItemViewTemplate = returnedData;
            findAllEntityServiceURL = PersonServiceURL + "FindAllLite";
            deleteEntityServiceURL = PersonServiceURL + "Delete";
            selectEntityCallBack = getPersonInfo;
            listEntityCallBack = callBackAfterList;
            loadDefaultEntityOptions();
        });
    }, 'PersonViewItemTemplate');
}

function initControls(template, isAdd) {

    var controlTemplate = isAdd ? template : $('.person-info');
    LoadDatePicker('input:.date', 'dd-mm-yy');
    $('.numeric').autoNumeric();
    $('.integer').autoNumeric({ aPad: false });
    preventInputChars(controlTemplate.find('input:.integer'));
    preventInputChars(controlTemplate.find('input:.noChars'));

    //SetPhoneMask(controlTemplate.find('.phone-format'));


    if (!addPersonMode)
        controlTemplate.find('.historyUpdated').append(HistoryUpdatedListViewTemplate.clone());


    CompanyAC = controlTemplate.find('select:.Company').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.CompanyServiceURL + 'FindAllLite' });
    PrimaryContactEmployeeAC = controlTemplate.find('select:.PrimaryContactEmployee').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.UsersWebService + 'FindUserAllLite' });
    VipClassAC = fillDataTypeContentList(controlTemplate.find('select:.VipClass'), systemConfig.dataTypes.VipClass, false, false);

    //  if (addPersonMode) {
    //       VipClassAC.set_Item({ label: getResource('VIPNormal'), value: systemConfig.dataTypes.NormalVipClass });

    //      CountryAC = controlTemplate.find('select:.Country').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.CountryServiceURL + "FindAll", required: false, autoLoad: false });
    //    }
    //VipClassAC.defaultItemLoaded = true;
    //systemConfig.dataTypes.VipClass
    AccountManagerAC = controlTemplate.find('select:.AccountManager').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.UsersWebService + 'FindUserAllLite' });
    //BestTimeToContactAC = fillDataTypeContentList(controlTemplate.find('select:.BestTimeToContact'), systemConfig.dataTypes.BestTimeToContact, false, false);
    HearAboutUsAC = fillDataTypeContentList(controlTemplate.find('select:.HearAboutUs'), systemConfig.dataTypes.Source, false, false);

    SourceAC = fillDataTypeContentList(controlTemplate.find('select:.Source'), systemConfig.dataTypes.Source, false, false);
    SalutationAC = fillDataTypeContentList(controlTemplate.find('select:.Salutation'), systemConfig.dataTypes.Salutation, false, false);
    MaritalStatusAC = fillDataTypeContentList(controlTemplate.find('select:.MaritalStatus'), systemConfig.dataTypes.MaritalStatus, false, false);
    //DepartmentAC = fillDataTypeContentList(controlTemplate.find('select:.Department'), systemConfig.dataTypes.Department, false, false);
    PositionAC = fillDataTypeContentList(controlTemplate.find('select:.Position'), systemConfig.dataTypes.Position, false, false);
    GenderAC = fillDataTypeContentList(controlTemplate.find('select:.Gender'), systemConfig.dataTypes.Gender, false, false);
    ReligionAC = fillDataTypeContentList(controlTemplate.find('select:.Religion'), systemConfig.dataTypes.Religion, false, false);
    //ContactTypeAC = fillDataTypeContentList(controlTemplate.find('select:.ContactType'), systemConfig.dataTypes.ContactType, false, false);
    //InterestedInAC = fillDataTypeContentList(controlTemplate.find('select:.InterestedIn'), systemConfig.dataTypes.InterestedIn, false, true);
    //NationalityAC = controlTemplate.find('select:.Nationality').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.CountryServiceURL + "FindAll", required: false });
    //    if (getQSKey('cId') && getQSKey('cId') > 0) {

    //        setCompany(getQSKey('cId'), controlTemplate);
    //        controlTemplate.find('a:.company-link').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.CompanyList + "?key=" + Number(getQSKey('cId')));

    //    }

    controlTemplate.find('input.NotifyForBirthday').bind('change', function () {
        if (controlTemplate.find('input.NotifyForBirthday').attr('checked')) {
            controlTemplate.find('input.NotifyForBirthday').attr('checkeed', true);
            notifyForBirthDateCheckd(true, false, controlTemplate);
        }
        else {
            controlTemplate.find('input.NotifyForBirthday').attr('checkeed', false);
            notifyForBirthDateCheckd(false, false, controlTemplate);
        }
    });
    controlTemplate.find('input.NotifyForAnniversary').bind('change', function () {
        if (controlTemplate.find('input.NotifyForAnniversary').attr('checked')) {
            controlTemplate.find('input.NotifyForAnniversary').attr('checkeed', true);
            notifyForAnniversaryCheckd(true, false, controlTemplate);
        }
        else {
            controlTemplate.find('input.NotifyForAnniversary').attr('checkeed', false);
            notifyForAnniversaryCheckd(false, false, controlTemplate);
        }
    });

    if (addPersonMode) {
        var Label = getResource('Customer');
        if (Lang == systemConfig.WebKeys.Ar)
            Label = getResource('Customer');
        //ContactTypeAC.set_Item({ label: Label, value: 690 });
    }
    controlTemplate.find('input.BusinessEmail').bind('blur', function () {
        if (controlTemplate.find(this).val() != '') {

            CheckEmailDuplication(controlTemplate.find(this).val(), 1);
            IsBusinessEmail = true;
            IsBusinessEmailDuplication = false;
        }
    });
    controlTemplate.find('input.PersonalEmail ').bind('blur', function () {
        if (controlTemplate.find(this).val() != '') {
            CheckEmailDuplication(controlTemplate.find(this).val(), 2);
            IsPersonalEmail = true;
            IsPersonalEmailDupliaction = false;
        }
    });
    controlTemplate.find('input.MphoneNumber').bind('blur', function () {
        var countryCode = controlTemplate.find('input.McountryCode').val();
        var areaCode = controlTemplate.find('input.MareaCode').val();
        var phoneNumber = controlTemplate.find('input.MphoneNumber').val();
        var mobileNumber = countryCode + areaCode + phoneNumber;
        if (mobileNumber != '') {
            IsMobileDuplication = false;
            CheckMobilelDuplication(mobileNumber.trim());
        }
    });


}


function initAddControls(template) {

    preventInputChars($('input:.phone-nums'));
    //CountryAC = template.find('select:.Country').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.CountryServiceURL + "FindAll", required: false, autoLoad: false });

    if (getQSKey('cId') && getQSKey('cId') > 0) {

        setCompany(getQSKey('cId'), template, function (comapnObj) {

            template.find('a:.company-link').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.CompanyList + "?key=" + Number(getQSKey('cId')));

            initControls(template, true);
            CompanyAC.set_Item({ label: companobj.label, value: companobj.CompanyId });
            CompanyWorkFlow(companobj.CompanyNameEnglish, template);

            VipClassAC.set_Item({ label: getResource('VIPNormal'), value: systemConfig.dataTypes.NormalVipClass });


        });


    }
    else {

        initControls(template, true);
        VipClassAC.set_Item({ label: getResource('VIPNormal'), value: systemConfig.dataTypes.NormalVipClass });
    }
}

function getPhoneFormat(obj) {


    var MCountryCode;
    if (obj.CountryMobileNumber != null && obj.CountryMobileNumber != "") {
        if (obj.CountryMobileNumber.substring(0, 2) == '00')
            MCountryCode = "(" + obj.CountryMobileNumber + ")";
        else
            MCountryCode = "+(" + obj.CountryMobileNumber + ")";
    }
    else
        MCountryCode = "";

    //var MCountryCode = obj.CountryMobileNumber != null && obj.CountryMobileNumber != "" ? "+(" + obj.CountryMobileNumber + ")" : "";
    var MAreaCode = obj.AreaMobileNumber != null && obj.AreaMobileNumber != "" ? obj.AreaMobileNumber + "-" : "";
    var MFullPhoneNumber = obj.MobileNumber != null ? obj.MobileNumber : "";
    var FullPhoneNumber = MCountryCode + MAreaCode + MFullPhoneNumber;
    $('.MobileNumber').text(FullPhoneNumber);


    var HCountryCode;
    if (obj.CountryHomeNumber != null && obj.CountryHomeNumber != "") {
        if (obj.CountryHomeNumber.substring(0, 2) == '00')
            HCountryCode = "(" + obj.CountryHomeNumber + ")";
        else
            HCountryCode = "+(" + obj.CountryHomeNumber + ")";
    }
    else
        HCountryCode = "";
    //var HCountryCode = obj.CountryHomeNumber != null && obj.CountryHomeNumber != "" ? "+(" + obj.CountryHomeNumber + ")" : "";
    var HAreaCode = obj.AreaHomeNumber != null && obj.AreaHomeNumber != "" ? obj.AreaHomeNumber + "-" : "";
    var HFullPhoneNumber = obj.HomeNumber != null ? obj.HomeNumber : "";
    var FullPhoneNumber = HCountryCode + HAreaCode + HFullPhoneNumber;
    $('.HomeNumber').text(FullPhoneNumber);

    var FCountryCode;
    if (obj.CountryFaxNumber != null && obj.CountryFaxNumber != "") {
        if (obj.CountryFaxNumber.substring(0, 2) == '00')
            FCountryCode = "(" + obj.CountryFaxNumber + ")";
        else
            FCountryCode = "+(" + obj.CountryFaxNumber + ")";
    }
    else
        FCountryCode = "";
    // var FCountryCode = obj.CountryFaxNumber != null && obj.CountryFaxNumber != "" ? "+(" + obj.CountryFaxNumber + ")" : "";
    var FAreaCode = obj.AreaFaxNumber != null && obj.AreaFaxNumber != "" ? obj.AreaFaxNumber + "-" : "";
    var FFullPhoneNumber = obj.FaxNumber != null ? obj.FaxNumber : "";
    var FullPhoneNumber = FCountryCode + FAreaCode + FFullPhoneNumber;
    $('.FaxNumber').text(FullPhoneNumber);

}


function getPersonInfo(obj) {
    if (obj != null) {
        $('.Business-email-to').attr('href', 'mailto:' + obj.BusinessEmail);
        $('.Personal-email-to').attr('href', 'mailto:' + obj.PersonalEmail);
        getPhoneFormat(obj);

        var controlTemplate = $('.person-info');


        CompanyName = obj.Company;
        CompanyId = obj.CompanyId;
        PersonId = obj.PersonId;
        ContactEmployeeName = obj.PrimaryContactEmployee;
        ContactEmployeeId = obj.PrimaryContactEmployeeValue;

        PersonName = obj.FullNameEn;

        $('.HeaderPersonName').text(Truncate(PersonName, 20));
        $('.HeaderPersonEmail').text(Truncate((obj.BusinessEmail != null) ? ' ' + obj.BusinessEmail : '', 20));


        if (obj.IntresetedInList != null) {
            //InterestedInAC.set_Items(obj.IntresetedInList);
            $.each(obj.IntresetedInList, function (index, item) {
                var label = (Lang == systemConfig.WebKeys.Ar) ? item.DataTypeContentAr : item.DataTypeContent;
                $('.InterestedIn').text($('.InterestedIn').html() + label + ',');
            });
            $('.InterestedIn').text($('.InterestedIn').html().slice(0, $('.InterestedIn').html().length - 1));
        }
        // setListItemTemplateValue(selectedEntityItemList, obj);

        $('a:.Website-link').attr('href', 'http://' + obj.Website);
        $('.Website').text('http://' + obj.Website);
        $('a:.Company-link').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.CompanyList + "?key=" + obj.CompanyId);
        $('a:.PrimaryContactEmployee').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.UserList + "?key=" + ContactEmployeeId);
        //$('a:.sendEmail-link').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.SendNotifications + '?pid=' + selectedEntityId);
        $('.sendEmail-link').click(function () {
            window.location = systemConfig.ApplicationURL + systemConfig.pageURLs.SendNotifications + '?pid=' + selectedEntityId;

        });
        $('input.IsVip').attr('checkeed', obj.IsVip);
        $('input.NotifyForBirthday').attr('checked', obj.NotifyForBirthday);
        //$('input.NotifyForAnniversary').attr('checkeed', obj.NotifyForAnniversary);
        $('input.NotifyForAnniversary').attr('checked', obj.NotifyForAnniversary);
        $('span:.NotifyForBirthday').text(getResource(obj.NotifyForBirthday));
        $('span:.NotifyForAnniversary').text(getResource(obj.NotifyForAnniversary));
        $('a:.AccountManager-link').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.UserList + "?key=" + obj.AccountManagerId);

        if (obj.NotifyForAnniversary) {
            notifyForAnniversaryCheckd(true, true, controlTemplate);
        }
        else {
            notifyForAnniversaryCheckd(false, true, controlTemplate);
        }

        if (obj.NotifyForBirthday)
            notifyForBirthDateCheckd(true, true, controlTemplate);
        else
            notifyForBirthDateCheckd(false, true, controlTemplate);

    }
    refreshListItem(obj);

    if ($('.result-block').length > 0 && (getQSKey('cId') > 0 || getQSKey('fdb') > 0) && isFirstTimeAdd) {
        $('.add-entity-button').first().click();
    }

}

function refreshListItem(item) {
    /*    var countryCode = item.CountryMobileNumber != null && item.CountryMobileNumber != "" ? "+(" + item.CountryMobileNumber + ")" : "";
    var areaCode = item.AreaMobileNumber != null && item.AreaMobileNumber != "" ? item.AreaMobileNumber + "-" : "";
    var PhoneNumber = item.MobileNumber != null && item.MobileNumber != "" ? item.MobileNumber : "";
    FullPhoneNumber = countryCode + areaCode + PhoneNumber;


    selectedEntityItemList.find('.result-data1 label').text(Truncate(item.FullNameEn, 20));
    selectedEntityItemList.find('.result-data2 label').text(Truncate(item.Position, 50));
    selectedEntityItemList.find('.result-data3 label').text((item.BusinessEmail != null) ? ' ' + Truncate(item.BusinessEmail, 20) : '');
    selectedEntityItemList.find('.result-data4 label').text(FullPhoneNumber);*/
}

function setListItemTemplateValue(entityTemplate, entityItem) {

    var countryCode = entityItem.CountryMobileNumber != null && entityItem.CountryMobileNumber != "" ? "+(" + entityItem.CountryMobileNumber + ")" : "";
    var areaCode = entityItem.AreaMobileNumber != null && entityItem.AreaMobileNumber != "" ? entityItem.AreaMobileNumber + "-" : "";
    var PhoneNumber = entityItem.MobileNumber != null && entityItem.MobileNumber != "" ? entityItem.MobileNumber : "";
    FullPhoneNumber = countryCode + areaCode + PhoneNumber;
    PersonName = entityItem.FullNameEn;
    entityTemplate.find('.result-data1 label').text(Truncate(PersonName, 20));
    entityTemplate.find('.result-data2 label').text(Truncate(entityItem.Position, 50));
    entityTemplate.find('.result-data3 label').text((entityItem.BusinessEmail != null) ? ' ' + Truncate(entityItem.BusinessEmail, 20) : '');
    var BEmail = entityItem.BusinessEmail != null ? entityItem.BusinessEmail : '';
    entityTemplate.find('.result-data3 label').attr('href', 'mailto:' + BEmail);
    entityTemplate.find('.result-data4 label').text(FullPhoneNumber); // textFormat((entityItem.MobileNumber != null) ? entityItem.MobileNumber : (entityItem.PhoneNumber != null) ? entityItem.PhoneNumber : '');

    $('.ResultGridTableDiv table.GridTable').css("float", "left");
    return entityTemplate;

}

function setListItemServiceData(keyword, pageNumber, resultCount) {
    if (!QuickSearch) {
        return formatString('{ keyword: "{0}", page:{1}, resultCount:{2} ,argsCriteria:"{ PersonId:{3}}"}', keyword, pageNumber, resultCount, filterEntityId);
    }
    else {
        return formatString('{mKey:"{0}",SearchCriteriaObj:"{1}",SortType:{2},SortFields:"{3}",TotalRecords:{4},PageNumber:{5}}', mKey, SearchCriteriaObj, SortType, SortFields, resultCount, pageNumber);
    }
}


function SaveEntity(isAdd, template) {

    var controlTemplate;
    var validForm = true;

    if (isAdd) {
        controlTemplate = $('.add-person-info');
        showLoading('.add-new-item');
    }
    else {
        controlTemplate = $('.person-info');
        showLoading('.Result-info-container');
    }


    if (!saveForm(controlTemplate)) {
        hideLoading();
        validForm = false;
    }
    if (AddMode) {
        if (IsBusinessEmailDuplication) {
            addPopupMessage(controlTemplate.find('input:.BusinessEmail'), getResource("AlreadyExist"));
            validForm = false;
        }
        if (IsPersonalEmailDupliaction) {
            addPopupMessage(controlTemplate.find('input:.PersonalEmail'), getResource("AlreadyExist"));
            validForm = false;
        }
        if (IsMobileDuplication) {
            addPopupMessage(controlTemplate.find('input.MphoneNumber'), getResource("AlreadyExist"));
            validForm = false;
        }

    }

    var dateOfBirth = controlTemplate.find('input:.DateOfBirth').val();
    if (dateOfBirth != "" && dateOfBirth != null)
        DifferantDate = DateDiff(getDate("dd-mm-yy", getDotNetDateFormat(dateOfBirth)), getDate("dd-mm-yy", new Date()));
    if (DifferantDate < 0) {
        addPopupMessage(controlTemplate.find('input:.DateOfBirth'), getResource("InvalidDateOfBirth"));
        validForm = false;
    }

    var anniversaryDate = controlTemplate.find('input:.Anniversary').val();
    if (anniversaryDate != "" && anniversaryDate != null)
    DifferantDate = DateDiff(getDate("dd-mm-yy", getDotNetDateFormat(dateOfBirth)), getDate("dd-mm-yy", getDotNetDateFormat(anniversaryDate)));
    if (DifferantDate < 0) {
        addPopupMessage(controlTemplate.find('input:.Anniversary'), getResource("InvalidAnniversaryDate"));
        validForm = false;
    }

    if (!validForm) {
        hideLoading();
        return false;
    }
    var PersonId = isAdd ? -1 : selectedEntityId;


    lastUpdatedDate = getEntityLockDateFormat(currentEntityLastUpdatedDate);


    if (controlTemplate.find('input.DateOfBirth').val() != "" && controlTemplate.find('input.DateOfBirth').val() != undefined) {
        dateOfBirth = getDate("dd-mm-yy", getDotNetDateFormat(controlTemplate.find('input.DateOfBirth').val()));
        if (DateDiff(getDate("dd-mm-yy", new Date()), dateOfBirth) > 0) {
            addPopupMessage(controlTemplate.find('input.DateOfBirth'), getResource("DateOfBirthError"));
            hideLoading();
            return false;
        }
    }

    var PersonObj = {

        CompanyId: CompanyAC.get_ItemValue(),
        FirstNameEn: controlTemplate.find('input.FirstNameEn').val().trim(),
        MiddleNameEn: controlTemplate.find('input.MiddleNameEn').val().trim(),
        LastNameEn: controlTemplate.find('input.LastNameEn').val().trim(),
        FirstNameAr: controlTemplate.find('input.FirstNameAr').val().trim(),
        MiddleNameAr: controlTemplate.find('input.MiddleNameAr').val().trim(),
        LastNameAr: controlTemplate.find('input.LastNameAr').val().trim(),
        FullNameEn: controlTemplate.find('input.FullNameEn').val(),
        FullNameAr: controlTemplate.find('input.FullNameAr').val(),
        NickName: controlTemplate.find('input.NickName').val(),
        PrimaryContactEmployee: PrimaryContactEmployeeAC.get_ItemValue(),
        SalutationId: SalutationAC.get_ItemValue(),
        GenderId: GenderAC.get_ItemValue(),
        SourceId: SourceAC.get_ItemValue(),
        DateOfBirth: getDate("mm-dd-yy", getDotNetDateFormat(controlTemplate.find('input.DateOfBirth').val())),
        ReligionId: ReligionAC.get_ItemValue(),
        MaritalStatusId: MaritalStatusAC.get_ItemValue(),
        DepartmentId: 0, ///DepartmentAC.get_ItemValue(),
        PositionId: PositionAC.get_ItemValue(),
        ContactImage: -1,
        InterstedIn: 0, //InterestedInAC.get_ItemsIds(),
        Anniversary: getDate("mm-dd-yy", getDotNetDateFormat(controlTemplate.find('input:.Anniversary').val())),
        NationalityId: 0, // NationalityAC.get_ItemValue(),
        ContactTypeId: 0, //ContactTypeAC.get_ItemValue(),
        NotifyForBirthday: controlTemplate.find('input.NotifyForBirthday').is(':checked'),
        NotifyForAnniversary: controlTemplate.find('input.NotifyForAnniversary').is(':checked'),
        //IsVip: $('input.IsVip').is(':checked'),
        VipClassId: VipClassAC.get_ItemValue(),
        HearAboutUsId: HearAboutUsAC.get_ItemValue(),
        BestTimeToContactId: 0,//BestTimeToContactAC.get_ItemValue(),
        AccountManagerId: AccountManagerAC.get_ItemValue(),
        FacebookPage: controlTemplate.find('input.FacebookPage').val(),
        LinkedInPage: controlTemplate.find('input.LinkedInPage').val(),
        TwitterAccount: controlTemplate.find('input.TwitterAccount').val(),
        Website: controlTemplate.find('input.Website').val()

    };


    var data = formatString('{id:{0}, PersonInfo:"{1}"}', (isAdd ? -1 : selectedEntityId), escape(JSON.stringify(PersonObj)));

    if (!isAdd)
        url = PersonServiceURL + "Edit";
    else
        url = PersonServiceURL + "Add";

    post(url, data, function (returnedValue) {
        saveSuccess(controlTemplate, returnedValue);
    }, saveError, getEntitylockAjaxHeaders(lastUpdatedDate));
}

function DateDiff(date1, date2) {
    var Startdate = new Date(date1.replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
    var EndDate = new Date(date2.replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
    return EndDate.getTime() - Startdate.getTime();
}


function saveSuccess(controlTemplate, returnedValue) {
    var personObject = returnedValue.result;

    if (returnedValue.statusCode.Code == 501) {
        hideLoading();
        addPopupMessage(controlTemplate.find('input.FirstNameEn'), getResource("DublicatatedFullName"));
        // addPopupMessage($('input.FirstNameAr'), getResource("DublicatatedFullName"));
        return false;
    }
    else if (returnedValue.statusCode.Code == 0) {
        hideLoading();
        if (returnedValue.statusCode.message == 'validation' && returnedValue.result) {
            filterError(returnedValue.result, controlTemplate);
        }
        showStatusMsg(getResource("FaildToSave"));
        return false;
    }
    PersonId = returnedValue.result;
    if (!AddMode) {
        showStatusMsg(getResource("savedsuccessfully"));
        hideLoading();
        loadEntityData(loadEntityServiceURL, entityItemViewTemplate, selectedEntityId, getPersonInfo);

    }
    else {
        debugger;
        PersonId = returnedValue.result.Id;
        savePersonContactInfo(PersonId, controlTemplate, personObject);
        hideLoading();
        AddMode = false;
        $('.add-new-item').hide();
        $('.add-new-item').empty();
    }

    IsBusinessEmailDuplication = false;
    IsPersonalEmailDupliaction = false;


}

function saveError(ex) {
    hideLoading();
    handleLockEntityError(ex, deletedEntityCallBack, updatedEntityCallBack);
}


function filterError(errors, template) {
    var allMsg = '';
    $('.validation').remove();
    if (!template) template = $('.Person-info');
    $.each(errors, function (index, error) {
        if (error.GlobalMessage) {
            allMsg += '<span>' + error.Message + '<br/></span>';
        }
        else if (error.ControlName == 'CompanyId') {
            addPopupMessage(template.find('select.CompanyId'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'FirstNameEn') {
            addPopupMessage(template.find('input.FirstNameEn'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'MiddleNameEn') {
            addPopupMessage(template.find('input.MiddleNameEn'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'LastNameEn') {
            addPopupMessage(template.find('input.LastNameEn'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'FirstNameAr') {
            addPopupMessage(template.find('input.FirstNameAr'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'MiddleNameAr') {
            addPopupMessage(template.find('input.MiddleNameAr'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'LastNameAr') {
            addPopupMessage(template.find('input.LastNameAr'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'FullNameEn') {
            addPopupMessage(template.find('input.FullNameEn'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'FullNameAr') {
            addPopupMessage(template.find('input.FullNameAr'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'NickName') {
            addPopupMessage(template.find('input.NickName'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'SalesManId') {
            addPopupMessage(template.find('select.SalesMan'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'PersonType') {
            addPopupMessage(template.find('select.PersonType'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'PrimaryContactEmployee') {
            addPopupMessage(template.find('input.PrimaryContactEmployee'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'SalutationId') {
            addPopupMessage(template.find('select.Salutation'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'GenderId') {
            addPopupMessage(template.find('select.Gender'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'SourceId') {
            addPopupMessage(template.find('select.Source'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'DateOfBirth') {
            addPopupMessage(template.find('input.DateOfBirth'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'ReligionId') {
            addPopupMessage(template.find('select.Religion'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'MaritalStatusId') {
            addPopupMessage(template.find('select.MaritalStatus'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'DepartmentId') {
            addPopupMessage(template.find('select.Department'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'PositionId') {
            addPopupMessage(template.find('select.Position'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'ContactImageId') {
            addPopupMessage(template.find('select.ContactImage'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'CustomerReferralCompanyId') {
            addPopupMessage(template.find('select.CustomerReferralCompany'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'CustomerReferralPersonId') {
            addPopupMessage(template.find('select.CustomerReferralPerson'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'EmployeeUserId') {
            addPopupMessage(template.find('select.EmployeeUser'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'ScoutingComments') {
            addPopupMessage(template.find('textarea.ScoutingComments'), getResource(error.ResourceKey));
        }

    });

    hideLoading();
    if (allMsg.trim()) {
        showOkayMsg('Validation error', allMsg.trim());
    }
}


function savePersonContactInfo(PersonId, controlTemplate, personObject) {

    debugger;

    var HomePhone = {
        PhoneID: -1,
        PhoneNumber: controlTemplate.find('input:.PphoneNumber').val().trim(),
        AreaCode: controlTemplate.find('input:.PareaCode').val().trim(),
        CountryCode: controlTemplate.find('input:.PcountryCode').val().trim(),
        Type: systemConfig.dataTypes.LandLineNumber
    };

    var FaxPhone = {
        PhoneID: -1,
        PhoneNumber: controlTemplate.find('input:.SphoneNumber').val().trim(),
        AreaCode: controlTemplate.find('input:.SareaCode').val().trim(),
        CountryCode: controlTemplate.find('input:.ScountryCode').val().trim(),
        Type: systemConfig.dataTypes.FaxNumber
    };

    var MobilePhone = {
        PhoneID: -1,
        PhoneNumber: controlTemplate.find('input:.MphoneNumber').val().trim(),
        AreaCode: controlTemplate.find('input:.MareaCode').val().trim(),
        CountryCode: controlTemplate.find('input:.McountryCode').val().trim(),
        Type: systemConfig.dataTypes.MobileNumber
    };


    var BusinessEmail = {
        Email_Id: -1,
        Type: 758,
        EmailAddress: controlTemplate.find('input:.BusinessEmail').val().trim()
    };

    var PersonalEmail = {
        Email_Id: -1,
        Type: 757,
        EmailAddress: controlTemplate.find('input:.PersonalEmail').val().trim()
    };



    var url = PersonServiceURL + "savePersonContactInfo";
    var data = formatString('{PersonId:{0}, AddressObj:"{1}",HomePhoneObj:"{2}",FaxPhone:"{3}",MobilePhone:"{4}",BusinessEmail:"{5}",PersonalEmail:"{6}"}', personObject.Id,
     null,
      escape(JSON.stringify(HomePhone)),
      escape(JSON.stringify(FaxPhone)),
      escape(JSON.stringify(MobilePhone)),
      escape(JSON.stringify(BusinessEmail)),
      escape(JSON.stringify(PersonalEmail))
     );


    post(url, data,
     function (returnedValue) {
         appendNewListRow(personObject);

         //  window.location = systemConfig.ApplicationURL + systemConfig.pageURLs.PersonList + "?key=" + PersonId + "&mood=add";
     }, saveError, '');

}

function setCompany(companyId, controlTemplate, callback) {
    var url = systemConfig.ApplicationURL_Common + systemConfig.CompanyServiceURL + "FindByIdLite";
    var data = formatString('{ id: "{0}" }', Number(companyId));
    post(url, data,
     function (data) {
         companobj = data.result;
         if (callback)
             callback(companobj);
     }, '', '');
}

function CheckEmailDuplication(Email, Type) {
    var data = formatString('{ email: "{0}",EmailType:"{1}" }', Email, Type);
    var url = systemConfig.ApplicationURL_Common + systemConfig.PersonsServiceURL + "CheckEmailDuplication"
    post(url, data, DuplicationSuccess, DuplicationError, null);

}

function DuplicationSuccess(returnedValue) {
    if (returnedValue != null) {
        if (returnedValue.statusCode.Code == 501) {
            hideLoading();
            if (IsBusinessEmail) {

                IsBusinessEmailDuplication = true;
            }
            if (IsPersonalEmail) {

                IsPersonalEmailDupliaction = true;
            }

        }
        IsBusinessEmail = false;
        IsPersonalEmail = false;
    }
}

function DuplicationError() {

}


function CheckMobilelDuplication(PhoneNumber) {
    var data = formatString('{ PhoneNumber: "{0}" }', PhoneNumber);
    var url = systemConfig.ApplicationURL_Common + systemConfig.PersonsServiceURL + "CheckMobileDuplication"
    post(url, data, DuplicationMobileSuccess, DuplicationMobileError, null);

}


function DuplicationMobileSuccess(returndValue) {
    if (returndValue != null) {
        if (returndValue.statusCode.Code == 501) {
            hideLoading();
            IsMobileDuplication = true;

        }
    }

}


function DuplicationMobileError() { }

function CompanyWorkFlow(CompanyName, controlTemplate) {

    controlTemplate.find('.company-filed').show();
    controlTemplate.find('.company-list').hide();
    controlTemplate.find('.Company').text(CompanyName);
    controlTemplate.find('.company-required').hide();
    // $('.person-required').hide();
}


function checkPermissions(Sender, EntityId, ParentId, Template, Callback) {
    if ($(Sender).attr('pChecked') != 'checked') {
        //$(Sender).attr('pChecked', 'checked');
        var data = formatString('{EntityId:{0},ParentId:{1}}', EntityId, ParentId);
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: PemissionWebServiceURL + "checkUnAvailablePermissions",
            data: data,
            datatype: "json",
            success: function (data) {
                var returnedData = eval("(" + data.d + ")");
                if (returnedData.statusCode.Code == 1) {
                    if (returnedData.result != null) {
                        UnAvailablePermissionsList = returnedData.result;
                        Template = ApplyUserPermissions(Template);
                    }
                    else {
                        $('.add-entity-button').show();
                        $('.TabbedPanelsContent:visible').find('.Tab-action-header').show();
                    }
                }
                else {
                    //if 0: then there is no permissions at all
                    // alert('no access');
                }
                if (Callback) {
                    Callback(Template);
                }
            },
            error: function (e) { }
        });
    }
    else {
        if (Callback)
            Callback(Template);
    }
}

function addPopupMessage2(element, message, isList) {
    if (isList) {
        var selectWidth = Number(element.css('width').replace('px', '')); //- 100;
        element.addClass('cx-control-red-border');
        element = element.parent().parent();
        var validation = $('<div>').attr({ 'class': 'validation' })//.css('width', (selectWidth / 2) - 20 + 'px')
    .append($('<span>').html(message), $('<span>'));
        if (validation.width() >= (selectWidth / 2))
            validation.css('width', (selectWidth / 3) - 5);
        validation.insertAfter(element);

    }
    else {
        var validation = $('<div>').attr({ 'class': 'validation' })//.css('width', ((Number(element.css('width').replace('px', '')) / 2) - 10) + 'px')
    .append($('<span>').html(message), $('<span>'));

        validation.insertAfter(element);
        if ($(validation).width() >= ($(element).width() / 2))
            $(validation).css('width', ($(element).width() / 3) + 10);
        element.addClass('cx-control-red-border')
    }
}

function callBackAfterList() {
    if ($('.result-block').length <= 0 && (getQSKey('cId') > 0 || getQSKey('fdb') > 0) && isFirstTimeAdd) {
        $('.add-entity-button').first().click();
        //   listEntityCallBack = null;
    }
}