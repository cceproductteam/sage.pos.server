﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CompanyList.aspx.cs" MasterPageFile="~/Common/MasterPage/ERPSite.master"  Inherits="Centrix.POS.Server.Web.POS.Company.CompanyList" %>

<asp:Content runat="server" ContentPlaceHolderID="PageHeader" ID="PageHeader">
    <script type="text/javascript" src="JS/Company.js?v=<%=Centrix_Version %>"></script>
    <script type="text/javascript" src="JS/RelatedGiveAways.js?v=<%=Centrix_Version %>"></script>
    <script type="text/javascript" src="JS/RelatedCase.js?v=<%=Centrix_Version %>"></script>
    <script src="../../Common/JScript/CommonTabs.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <script src="../../Common/AdvancedSearch/JS/AdvancedSearch.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <link href="../../Common/CSS/Task/calendar_<%=Lang %>.css?v=<%=Centrix_Version %>" rel="stylesheet" type="text/css" />
    <script src="../../Common/JScript/Task/Task.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <script src="../../Common/JScript/Task/calendar.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <script src="../../Common/JScript/Task/fullcalendar.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <script src="../../Common/JScript/CommunicationTab.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <script src="JS/RelatedOpportunities.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="PageBody" ID="PageBody">
    <div id="demo2" class="result-container">
       <div class="SearchDivContainer">
            <div class="SD-SearchDiv">
                <input type="text" class="SD-ipnutText-MainSearch search-text-box searchTextbox" /></div>
            <div class="Quick-Search-Div">
            </div>
            <div class="MoreSearchOptions">
                <a class="more-options"><span resourcekey="MoreOptions"></span></a>
            </div>
        </div>
        <div class="GridTableDiv-header">
            <table class="GridTableFloating" cellpadding="0" cellspacing="0" border="0">
                <thead class="THeadGridTable">
                    <tr>
                        <th class="THead-Th th-number">
                            <span>#</span>
                        </th>
                        <th class="THead-Th">
                            <span resourcekey="CompanyName"></span>
                        </th>
                    
                        <th class="THead-Th">
                            <span resourcekey="BusinessEmail"></span>
                        </th>
                         <th class="THead-Th">
                            <span resourcekey="Mobile"></span>
                        </th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="GridTableDiv-body ResultGridTableDiv">
            <table class="TBodyGridTable Result-list" cellpadding="0" cellspacing="0" border="0">
                <thead class="THeadGridTable">
                    <tr>
                        <th class="THead-Th th-number">
                            <span>#</span>
                        </th>
                        <th class="THead-Th">
                            <span>Company Name</span>
                        </th>
                   
                        <th class="THead-Th">
                            <span>Business Email</span>
                        </th>
                         <th class="THead-Th">
                            <span>Mobile</span>
                        </th>
                    </tr>
                </thead>
                <tbody class="TBodyGridTable result-list">
                </tbody>
            </table>
        </div>
        <div class="total-result-div TotalDiv">
            <span></span>
        </div>
    </div>
    <div id='content'>
        <div class="Result-info-container">
            <div class="result-name">
                <ul>
                    <li class="nameLI"><span resourcekey="HeaderCompanyName"></span><span class="HeaderCompanyName">
                    </span></li>
                    <li class="emailLI"><span resourcekey="HeaderBusinessEmail"></span><span class="HeaderBusinessEmail">
                    </span></li>
                    <li class="emailLI"><span resourcekey="HeaderLandLine PhoneDirection"></span><span class="HeaderLandLine">
                    </span></li>
                </ul>
            </div>
            <div style="float: right;" id='page_navigation'>
            </div>
            <div class="ClearDiv">
            </div>
            <div class="test">
                <div class="QuickSearchDiv">
                </div>
                <span class="SlideUp" title="Slide Up"></span>
                <div id="TabbedPanels1" class="TabbedPanels">
                    <ul class="TabbedPanelsTabGroup">
                        <li class="TabbedPanelsTab" tabindex="1">
                            <div class="TabbedPanelsTabSelected-arrow" style="display: block;">
                            </div>
                            <span class="TabbedPanelsTab-img Summery-tab"></span><span resourcekey="Summary">
                            </span></li>
                            
                             
                          
                             
                        <li class="TabbedPanelsTab email-tabs" tabindex="6">
                            <div class="TabbedPanelsTabSelected-arrow">
                            </div>
                            <span class="TabbedPanelsTab-img  Mail-tab"></span><span resourcekey="email"></span>
                        </li>
                        
                        
                        <li class="TabbedPanelsTab phone-tabs" tabindex="9">
                            <div class="TabbedPanelsTabSelected-arrow">
                            </div>
                            <span class="TabbedPanelsTab-img Phone-tab"></span><span resourcekey="phone"></span>
                        </li>
                         
                           
                         
                          

                    

                    </ul>
                    <div class="TabbedPanelsContentGroup">
                    </div>
                    <!--More-->
                    <div class="More-holder">
                        <div class="moreTabsList">
                            <ul class="moreTabs-ul">
                            </ul>
                        </div>
                    </div>
                    <!--End More-->
                </div>
                <span class="SlideDown" style="" title="Slide Down"></span>
            </div>
        </div>
    </div>
    <div class="add-new-div">
        <div class="add-new-item display-none">
        </div>
    </div>
</asp:Content>
