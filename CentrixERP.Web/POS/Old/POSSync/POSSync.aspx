﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="POSSync.aspx.cs" MasterPageFile="~/Common/MasterPage/ERPSite.master"
    Inherits="Centrix.POS.Server.Web.POS.POSSync.POSSync" %>

<asp:Content runat="server" ContentPlaceHolderID="PageHeader" ID="PageHeader">
    <script src="JS/POSSync.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <link href="../CSS/POSMain.css?v=<%=Centrix_Version %>" rel="stylesheet" type="text/css" />
    <link href="CSS/POSSync_<%=Lang %>.css?v=<%=Centrix_Version %>" rel="stylesheet"
        type="text/css" />
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="PageBody" ID="PageBody">
    <div class="white-container" style="height: 560px;">
        <div class="white-container-data" style="height: 560px;">
            <div class="sync-progress-div">
                <table class="sync-result-table" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <th>
                            <span resourcekey="Store"></span>
                        </th>
                        <th>
                            <span resourcekey="IpAddress"></span>
                        </th>
                        <th>
                            <span resourcekey="Status"></span>
                        </th>
                    </tr>
                </table>
                <%-- <a class="show-full-details"><span resourcekey="ShowFullDetails"></span>--%>
                <%--</a><a class="Close">
                    <span resourcekey="Close"></span></a>--%>
            </div>
            
            <div class="loaderBox displayNone">
                <span resourcekey="loading"></span>
                <img src="../Images/loading.gif" /></div>
            <div class="StoreSync-info-container POSSync-Fieldset displayNone" style="height: 675px;">
                <div class="Stores-list-container" style="height: 660px;">
                    <div class="register-filter-search">
                        <div class="registerSearch">
                            <input type="text" class="searchBox" />
                            <a href="#" class="searchIcon"></a>
                        </div>
                    </div>
                    <div class="reg-regName-container" style="height: 500px;">
                    </div>
                </div>
                <div class="sync-container">
                    <div class="sync-tabs">
                        <ul>
                            <li class="syncRegDataTab SelectedTab">
                                <input type="checkbox" class="syncRegData" /><span resourcekey="RegisterData"></span></li>
                            <li class="syncAdminData">
                                <input type="checkbox" class="ChSyncAdminData" /><span resourcekey="AdminData"></span></li>
                        </ul>
                    </div>
                    <div class="sync-action-Div">
                        <ul>
                            <li class="Sync-pnl viewedit-display-block"><span class="ActionSync-link"></span><a
                                class="ActionSync-link"><span resourcekey="Sync"></span></a></li>
                        </ul>
                    </div>
                    <div class="control-content-sync-list syncRegDataTabList">
                        <ul>
                            <li class="sync-row2"><span resourcekey="SyncTransactionData"></span></li>
                            <li class="sync-row2"><span resourcekey="CloseRegisterData"></span></li>
                            <li class="sync-row2"><span resourcekey="CustomerData"></span></li>
                        </ul>
                    </div>
                    <div class="control-content-sync-list syncAdminDataList">
                        <ul>
                            <li class="sync-row1">
                                <input type="checkbox" class="SyncUserManagement" /><span resourcekey="SyncUserManagment"></span></li>
                         <%--   <li class="sync-row2"><span resourcekey="SyncUsers"></span></li>
                            <li class="sync-row2"><span resourcekey="SyncRoles"></span></li>
                            <li class="sync-row2"><span resourcekey="SyncTeams"></span></li>
                            <li class="sync-row2"><span resourcekey="SyncPermissions"></span></li>
                            <li class="sync-row2"><span resourcekey="SyncShifts"></span></li>--%>
                        </ul>
                     <%--   <ul>
                            <li class="sync-row1">
                                <input type="checkbox" class="SyncContactManagment" /><span resourcekey="SyncContactManagment"></span></li>
                            <li class="sync-row2"><span resourcekey="syncPerson"></span></li>
                            <li class="sync-row2"><span resourcekey="syncCompany"></span></li>
                            <li class="sync-row2">
                        </ul>--%>
                        <ul>
                            <li class="sync-row1">
                                <input type="checkbox" class="SyncPOSAdministration" /><span resourcekey="SyncPOSAdministration"></span></li>
                          <%--  <li class="sync-row2"><span resourcekey="SyncStores"></span></li>
                            <li class="sync-row2"><span resourcekey="SyncRegisters"></span></li>
                            <li class="sync-row2"><span resourcekey="SyncQuickKeys"></span></li>
                           
                            <li class="sync-row2"><span resourcekey="GiftVoucherData"></span></li>
                          
                              
                            <li class="sync-row2"><span resourcekey="SyncConfiguration"></span></li>
                            <li class="sync-row2"><span resourcekey="LOVs"></span></li>--%>
                        </ul>
                        <ul>
                            <li class="sync-row1">
                                <input type="checkbox" class="SyncInventoryData" /><span resourcekey="SyncInventoryData"></span></li>
                           <%-- <li class="sync-row2"><span resourcekey="SyncLocation"></span></li>--%>
                            <%--<li class="sync-row2"><span resourcekey="SyncItems"></span></li>--%>
                          <%--  <li class="sync-row2"><span resourcekey="SyncItemsSerial"></span></li>--%>
                            <%--<li class="sync-row2"><span resourcekey="StockData"></span></li>--%>
                        <%--    <li class="sync-row2"><span resourcekey="SyncPriceList"></span></li>--%>
                           <%-- <li class="sync-row2"><span resourcekey="SyncItemPriceList"></span></li>
                            <li class="sync-row2"><span resourcekey="SyncItemTax"></span></li>
                            <li class="sync-row2"><span resourcekey="SyncCurrency"></span></li>
                            <li class="sync-row2"><span resourcekey="SyncCurrencyRates"></span></li>
                            <li class="sync-row2"><span resourcekey="ARCustomerData"></span></li>--%>
                        </ul>
                       
                    </div>
                    <%-- <div class="syncBtn">
                        <input type="button" class="button2"  /></div>--%>
                </div>
            </div>
        </div>
    </div>

     <div class="white_content_sync">
                <div class="confermitaion-msg-box_sync">
                    <div class="cmb-title">
                        <span>Delete record</span></div>
                    <div class="cmb-desc">
                        <span>
                            <table width="100%" border="0" cellpadding="0" cellspacing="1" class="details-grid"
                                style="display: none">
                                <thead class="THeadGridTable">
                                    <tr>
                                        <th class="th-data-table-sync">
                                            Store
                                        </th>
                                        <th class="th-data-table-sync">
                                            data
                                        </th>
                                        <th class="th-data-table-sync">
                                            status
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                            <table width="100%" border="0" cellpadding="0" cellspacing="1" class="details-grid  tbl_result">
                                <thead class="THeadGridTable">
                                    <tr>
                                        <th class="th-data-table-sync">
                                            Store
                                        </th>
                                        <th class="th-data-table-sync">
                                            data
                                        </th>
                                        <th class="th-data-table-sync">
                                            status
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                        </span>
                    </div>
                    <div class="cmb-controls">
                <input type="button" value="Ok" class="button2 confirm-button">
                </div>
                </div>
            </div>
</asp:Content>
