﻿var MethodName = '';
function InnerPage_Load() {
    setSelectedMenuItem('.pos-icon');
    setPageTitle(getResource("BackEndSync"));
    $('.ActionSave-link ,.save-tab-lnk').show();
    checkPermissions(this, systemEntities.BackEndSync, -1, $('.BackEndSync-info'), function (returnedData) {
        template = returnedData;
        $('.BackEndSync-div').show();
        initControls(template);
        getSyncDataHistory();
    });

}

function initControls(template) {
    $('input:.checkSyncAll').unbind('click').click(function () {
       // alert();
        if ($('input:.checkSyncAll').is(':checked'))
            $("input:.chk").prop("checked", true);
          else
              $("input:.chk").prop("checked", false);
      });
      $('.sync-all').unbind('click').click(function () {
          SyncAll();
      });

//    $('a:.SyncCurrency').unbind('click').click(function () {
//        MethodName = 'IcCurrencyFindAll';
//        SyncData();
//        return;
//    });

//    $('a:.SyncItemCard').unbind('click').click(function () {
//        MethodName = 'IcItemCardFindAll';
//        SyncData();
//        return;
//    });

//    $('a:.SyncPriceListGroup').unbind('click').click(function () {
//        MethodName = 'IcPriceListGroupFindAll';
//        SyncData();
//        return;
//    });


//    $('a:.SyncItemPriceList').unbind('click').click(function () {
//        MethodName = 'IcItemPriceListFindAll';
//        SyncData();
//        return;
//    });

//    $('a:.SyncItemTax').unbind('click').click(function () {
//        MethodName = 'IcItemTaxFindAll';
//        SyncData();
//        return;
//    });

//    $('a:.SyncLocation').unbind('click').click(function () {
//        MethodName = 'IcLocationFindAll';
//        SyncData();
//        return;
//    });

//    $('a:.SyncStock').unbind('click').click(function () {
//        MethodName = 'IcStockFindAll';
//        SyncData();
//        return;
//    });

//    $('a:.SyncCurrencyRate').unbind('click').click(function () {
//        MethodName = 'IcCurrencyRateFindAll';
//        SyncData();
//        return;
//    });

//    $('a:.SyncCustomer').unbind('click').click(function () {
//        MethodName = 'IcCustomerFindAll';
//        SyncData();
//        return;
//    });
}

function SyncData() {

   // showLoading('.BackEndSync-div');
    if (MethodName != '' || MethodName != null) {
        $.ajax({
            type: "POST",
            url: systemConfig.ApplicationURL_Common + systemConfig.IntegrationWebServiceURL + MethodName,
            contentType: "application/json; charset=utf-8",
            data: formatString('{}'),
            dataType: "json",
            success: function (data) {
               // hideLoading();
                MethodName = '';
                var List = eval("(" + data.d + ")").result;
                //getSyncDataHistory();
            },
            error: function (e) {
                MethodName = '';
                //hideLoading();
            }
        });
    }
}

function getSyncDataHistory() {
    showLoading('.BackEndSync-div');
    $.ajax({
        type: "POST",
        url: systemConfig.ApplicationURL_Common + systemConfig.IntegrationWebServiceURL + 'getSyncDataList',
        contentType: "application/json; charset=utf-8",
        data: formatString('{}'),
        dataType: "json",
        success: function (data) {
            hideLoading();
            var List = eval("(" + data.d + ")").result;
            if (List != null) {
                $.each(List, function (index, item) {
                    if (item.EntityId == systemEntities.IcCurrency) {
                        $('.CurrencyLastSyncDate').text(getDate('dd-mm-yy hh:mm tt', formatDate(item.LastSyncDate)));
                        $('.CurrencyStatus').text(getStatus(item.Status));
                    }
                    if (item.EntityId == systemEntities.IcItemCard) {
                        $('.ItemCardLastSyncDate').text(getDate('dd-mm-yy hh:mm tt', formatDate(item.LastSyncDate)));
                        $('.ItemCardStatus').text(getStatus(item.Status));
                    }
                    if (item.EntityId == systemEntities.IcCustomer) {
                        $('.CustomerLastSyncDate').text(getDate('dd-mm-yy hh:mm tt', formatDate(item.LastSyncDate)));
                        $('.CustomerStatus').text(getStatus(item.Status));
                    }
                    if (item.EntityId == systemEntities.IcCurrencyRateDetails) {
                        $('.CurrencyRateLastSyncDate').text(getDate('dd-mm-yy hh:mm tt', formatDate(item.LastSyncDate)));
                        $('.CurrencyRateStatus').text(getStatus(item.Status));
                    }
                    if (item.EntityId == systemEntities.IcItemPriceList) {
                        $('.ItemPriceListLastSyncDate').text(getDate('dd-mm-yy hh:mm tt', formatDate(item.LastSyncDate)));
                        $('.ItemPriceListStatus').text(getStatus(item.Status));
                    }
                    if (item.EntityId == systemEntities.IcItemTax) {
                        $('.ItemTaxLastSyncDate').text(getDate('dd-mm-yy hh:mm tt', formatDate(item.LastSyncDate)));
                        $('.ItemTaxStatus').text(getStatus(item.Status));
                    }
                    if (item.EntityId == systemEntities.IcLocation) {
                        $('.LocationLastSyncDate').text(getDate('dd-mm-yy hh:mm tt', formatDate(item.LastSyncDate)));
                        $('.LocationStatus').text(getStatus(item.Status));
                    }
                    if (item.EntityId == systemEntities.IcPriceList) {
                        $('.PriceListGroupLastSyncDate').text(getDate('dd-mm-yy hh:mm tt', formatDate(item.LastSyncDate)));
                        $('.PriceListGroupStatus').text(getStatus(item.Status));
                    }
                    if (item.EntityId == systemEntities.IcStock) {
                        $('.StockLastSyncDate').text(getDate('dd-mm-yy hh:mm tt', formatDate(item.LastSyncDate)));
                        $('.StockStatus').text(getStatus(item.Status));
                    }

                    if (item.EntityId == 1141) {
                        $('.GLAccountLastSyncDate').text(getDate('dd-mm-yy hh:mm tt', formatDate(item.LastSyncDate)));
                        $('.GLAccountStatus').text(getStatus(item.Status));
                    }

                    if (item.EntityId == 1142) {
                        $('.BankAccountLastSyncDate').text(getDate('dd-mm-yy hh:mm tt', formatDate(item.LastSyncDate)));
                        $('.BankAccountStatus').text(getStatus(item.Status));
                    }
                });
            }
        },
        error: function (e) {
            hideLoading();
            MethodName = '';
        }

       
    });
}

function getStatus(id) {
    if (id == 1) return getResource('Succeed');
    if (id == 2) return getResource('Failed');
    if (id == 3) return getResource('NoNewUpdate');
}

function SyncAll() {

    var isValid = $('input:.SyncCurrency').is(':checked') || $('input:.SyncItemCard').is(':checked') || $('input:.SyncPriceListGroup').is(':checked') || $('input:.SyncItemPriceList').is(':checked') || $('input:.SyncItemTax').is(':checked') || $('input:.SyncLocation').is(':checked') || $('input:.SyncStock').is(':checked') || $('input:.SyncCurrencyRate').is(':checked') || $('input:.SyncCustomer').is(':checked') || $('input:.SyncGLAccount').is(':checked') || $('input:.SyncBankAccount').is(':checked');

    if (!isValid) {
        showStatusMsg(getResource("NoDataSelectedToSync"));
        return
    }

    MethodNameArray = '';
    if ($('input:.SyncCurrency').is(':checked')) {
        MethodNameArray = MethodNameArray + ',IcCurrencyFindAll'
    }
    if ($('input:.SyncItemCard').is(':checked')) {
        MethodNameArray = MethodNameArray + ',IcItemCardFindAll'
    }
    if ($('input:.SyncPriceListGroup').is(':checked')) {
        MethodNameArray = MethodNameArray + ',IcPriceListGroupFindAll'
    }
    if ($('input:.SyncItemPriceList').is(':checked')) {
        MethodNameArray = MethodNameArray + ',IcItemPriceListFindAll'
    }
    if ($('input:.SyncItemTax').is(':checked')) {
        MethodNameArray = MethodNameArray + ',IcItemTaxFindAll'
    }
    if ($('input:.SyncLocation').is(':checked')) {
        MethodNameArray = MethodNameArray + ',IcLocationFindAll'
    }
    if ($('input:.SyncStock').is(':checked')) {
        MethodNameArray = MethodNameArray + ',IcStockFindAll'
    }
    if ($('input:.SyncCurrencyRate').is(':checked')) {
        MethodNameArray = MethodNameArray + ',IcCurrencyRateFindAll'
    }

    if ($('input:.SyncCustomer').is(':checked')) {
        MethodNameArray = MethodNameArray + ',IcCustomerFindAll'
    }

    if ($('input:.SyncBankAccount').is(':checked')) {
        MethodNameArray = MethodNameArray + ',IcBankFindAll'
    }

    if ($('input:.SyncGLAccount').is(':checked')) {
        MethodNameArray = MethodNameArray + ',IcGLAcountFindAll'
    }
    
    


    var MethodNameList = MethodNameArray.split(",")
    showLoading('.BackEndSync-div');
    $.each(MethodNameList, function (index, value) {
        MethodName = value;
        if (MethodName != '' && MethodName != null)
            SyncData();
    });
    window.setTimeout(function () {
        getSyncDataHistory();
        
    }, 6000);
}

