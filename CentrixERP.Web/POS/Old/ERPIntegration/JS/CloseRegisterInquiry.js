﻿var POSCloseRegisterServiceURL;

var CloseRegisterInquiryHeaderTemplate;
var CloseRegisterInquiryResultsGridDetailsTemplate;
var itemsGrid = null;

var StoreAC, RegisterAC;


function InnerPage_Load() {
    setPageTitle(getResource("InvoiceInquiry"));
    setSelectedMenuItem('.pos-icon');
    POSCloseRegisterServiceURL = systemConfig.ApplicationURL + systemConfig.POSPostInvoiceToShipmentServiceURL;


    GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.POSCloseRegisterListViewItemTemplate, function () {
        $('.white-container-data').append(CloseRegisterInquiryHeaderTemplate);
    }, 'CloseRegisterInquiryHeaderTemplate');

    GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.POSCloseRegisterResultGridTemplate, function () {
    }, 'CloseRegisterInquiryResultsGridDetailsTemplate');



    initControls();

    $('.btnSearch').attr('value', getResource('Search'));
    $('.lnkReset').attr('value', getResource('Reset'));
}

function initControls() {

    LoadDatePicker('input:.date');

    StoreAC = $('select:.Store').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.StoreServiceURL + 'FindAllLite', required: false });
    RegisterAC = $('select:.Register').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.RegisterServiceURL + 'FindAllLite', required: false });


    var OpenedDateFrom, OpenedDateTo, ClosedDateFrom, ClosedDateTo;

    $('input:.btnSearch').click(function () {
        OpenedDateFrom = ($('input:.OpenedDateFrom').val() != "") ? getDate('mm-dd-yy', getDotNetDateFormat($('input:.OpenedDateFrom').val())) : getDate('mm-dd-yy', formatDate($('input:.OpenedDateFrom').val()));
        OpenedDateTo = ($('input:.OpenedDateTo').val() != "") ? getDate('mm-dd-yy', getDotNetDateFormat($('input:.OpenedDateTo').val())) : getDate('mm-dd-yy', formatDate($('input:.OpenedDateTo').val()));


        ClosedDateFrom = ($('input:.ClosedDateFrom').val() != "") ? getDate('mm-dd-yy', getDotNetDateFormat($('input:.ClosedDateFrom').val())) : getDate('mm-dd-yy', formatDate($('input:.ClosedDateFrom').val()));
        ClosedDateTo = ($('input:.ClosedDateTo').val() != "") ? getDate('mm-dd-yy', getDotNetDateFormat($('input:.ClosedDateTo').val())) : getDate('mm-dd-yy', formatDate($('input:.ClosedDateTo').val()));

        var url = POSCloseRegisterServiceURL + "CloseRegisterInquiryResults";
        var data = formatString('{storeIds:"{0}",registerIds:"{1}",openedDateFrom :"{2}" , openedDateTo:"{3}",closedDateFrom :"{4}" , closedDateTo:"{5}"}', StoreAC.get_Item().value, RegisterAC.get_Item().value, OpenedDateFrom, OpenedDateTo, ClosedDateFrom, ClosedDateTo);

        post(url, data, FindResultsSuccess, function () { }, null);
    });

    $('input:.lnkReset').click(function () {
        StoreAC.clear();
        RegisterAC.clear();
        $('.OpenedDateFrom').val('');
        $('.OpenedDateTo').val('');

        $('.ClosedDateFrom').val('');
        $('.ClosedDateTo').val('');
    });
}

function loadItemsGrid(Template, datasource, rowTemplate, headerTemplate) {

    var control = [
  {
      controlId: 'StoreName',
      type: 'text'

  },
   {
       controlId: 'OpenedDate',
       type: 'text'

   },
    {
        controlId: 'ClosedDate',
        type: 'text'

    },
     {
         controlId: 'OpenedCashLoan',
         type: 'text',
         numeric: true


     },
      {
          controlId: 'TotalPrice',
          type: 'text',
          numeric: true
      },
      {
          controlId: 'IsIdentical',
          type: 'text'

      },

      {
          controlId: 'DifferenceAmount',
          type: 'text',
          numeric: true

      }

 ];
    //after create array of controls, we need init the grid

      itemsGrid = Template.find('#items-grid').Grid({
          headerTeamplte: Template.find('#h-template').html(),
          rowTemplate: CloseRegisterInquiryResultsGridDetailsTemplate,
          rowControls: control,
          dataSource: datasource,
          selector: '.',
          appendBefore: '.footer-row',
          rowDeleted: function (grid) {

              var Count = 0;
              $.each(grid.table.find('tr.r-data'), function (index, item) {
                  Count = Count + 1;
              });
              if (grid.rows.length == 1) {
                  grid.defaults.preventDelete = true;
              } else grid.defaults.preventDelete = false;
          },
          rowAdded: function (row, controls, dataRow, grid) {

              if (!dataRow)
                  row.find('.viewedit-display-none').show();

              var Count = 0;
              $.each(grid.table.find('tr.r-data'), function (index, item) {
                  row.find('.StoreName').unbind().click(function () {
                      window.open(systemConfig.ApplicationURL + systemConfig.pageURLs.StoreList + "?key=" + item.StoreId);

                  });

                  Count = Count + 1;
              });





              if (grid.rows.length == 1) {
                  grid.defaults.preventDelete = true;
              } else grid.defaults.preventDelete = false;

          }, preventAdd: false
      });

}


function FindResultsSuccess(returnedValue) {   //Here Create the details grid , Fill it with corresponding data
    $('#items-grid').empty();


    var result = returnedValue.result;
    if (result && result.length > 0) {
        $('.NoDataFound').hide();
        loadItemsGrid($('.gridContainerDiv'), result);


        $($('table')[2]).wrap("<div class='grid-height'></div>");  //To Control the grid height


        $($('.StoreName')[0]).click(function () {
            window.open(systemConfig.ApplicationURL + systemConfig.pageURLs.StoreList + "?key=" + $($('.StoreId')[0]).text());

        });

        $.each($('.OpenedDate'), function (ix, itm) {
            $($('.OpenedDate')[ix]).text(getDate("dd-mm-yy", formatDate($($('.OpenedDate')[ix]).text())));
        });

        $.each($('.ClosedDate'), function (ix, itm) {
            $($('.ClosedDate')[ix]).text(getDate("dd-mm-yy", formatDate($($('.ClosedDate')[ix]).text())));
        });


    }
    else {
        $('.NoDataFound').show();
        $('.NoDataFound').attr('style', 'display: inline-block !important;');
    }





}



function getdate(date) {
    var arr = date.split('-');
    return arr[1] + '-' + arr[0] + '-' + arr[2];
}




