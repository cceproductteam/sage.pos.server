﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CentrixERP.Configuration;
using CentrixERP.Common.Web;

namespace Centrix.POS.Server.Web.POS.GenerateBarcode
{
    public partial class GenerateBarcode : BasePageLoggedIn
    {
        public GenerateBarcode()
            : base(Enums_S3.Permissions.SystemModules.None) { }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}