﻿var InvoiceReportSchema;
var ExportPageURL;
var FromLocationAC;
var ToLocationAC;
var FromCustomerAC;
var ToCustomerAC;
var POSInvoiceServiceURL;
var AccpacURLService;
var table;

function InnerPage_Load() {

    NewcheckPermissions(this, systemEntities.AccpagSyncLog, -1);

    if (havePer == 0) {

        $('.white-container-data').hide();
        showStatusMsg(getResource('vldDontHavePermission'));
        return;
    }
    initControls();
    setPageTitle(getResource("SyncActivityLog"));
    InvoiceReportSchema = systemConfig.ReportSchema.TotalDailySales;
    setSelectedMenuItem('.pos-icon');
}

function initControls() {

    LoadDatePicker('input:.date', 'mm-dd-yy');

    $('.lnkReset').click(resetForm);
    $('#btnSearch').click(BuildSearchCriteria);
    $('.Quick-Search-Controls').live('keydown', function (e) {
        if (e.keyCode == 13 || e.which == 13) {
            $('#btnSearch').click();
        }
    });


    //POSInvoiceServiceURL = systemConfig.ApplicationURL + "Common/WebServices/InvoiceWebService.asmx/";
    AccpacURLService = systemConfig.ApplicationURL_Common + systemConfig.AccpacURL;

    $('#btnSearch').attr('value', getResource("Search"));
    $('.lnkReset').attr('value', getResource("Reset"));
}

function BuildSearchCriteria() {
    if (!saveForm($('.Data-Table'))) {
        $('.validation').css({ top: "10%", left: "300px" });
        return false;
    }

    $('.search-criteria').val('');
    var argsCriteria = new Object();
    //argsCriteria.ExportPageURL = ExportPageURL;
    if ($('.SyncLogDateTo').val() != null)
        argsCriteria.to_date = $('.SyncLogDateTo').val();

    if ($('.SyncLogDateFrom').val() != null)
        argsCriteria.from_date = $('.SyncLogDateFrom').val();

    if ($('.AccpacSyncUniqueNumber').val() != null)
        argsCriteria.AccpacSyncUniqueNumber = $('.AccpacSyncUniqueNumber').val();


    var url = AccpacURLService + "GetAccpacSyncLog";
    var data = formatString('{dateto :"{0}" , datefrom:"{1}",AccpacSyncUniqueNumber:"{2}"}', argsCriteria.to_date, argsCriteria.from_date ,argsCriteria.AccpacSyncUniqueNumber );
    post(url, data, FindResultsSuccess, function () { }, null);
    return;
    



    var SearchCriteria = '{';
    for (var key in argsCriteria) {
        if (argsCriteria.hasOwnProperty(key))
            if (argsCriteria[key] != "") {
                SearchCriteria = SearchCriteria + ('"' + key + '":' + '"' + argsCriteria[key] + '",');
            }
    }


    if (SearchCriteria != '{') {
        SearchCriteria = SearchCriteria.slice(0, -1);
    }
    SearchCriteria = SearchCriteria + '}';
    var exportPageURL = ExportPageURL + formatString("schemaID={0}&SearchCriteria={1}", InvoiceReportSchema, SearchCriteria);
    window.open(exportPageURL, '_blank');
}

function resetForm() {
    $('.SyncLogDateTo').val('');
    $('.SyncLogDateFrom').val('');
    $('.AccpacSyncUniqueNumber').val('');



}


function getdate(date) {
    var arr = date.split('-');
    return arr[1] + '-' + arr[0] + '-' + arr[2];
}


function FindResultsSuccess(returnedValue) {
    if (table)
        table.destroy();
    table = $('.result').DataTable({
        "bFilter": false,
        "data": returnedValue,
        "columns": [
           
       
           
           {
               "data": "AccpacSyncUniqueNumber",
               
            },
          
            { "data": "CreatedDate" }


        ],

        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {

            if (aData.AccpacSyncUniqueNumber) {
                $('td:eq(0)', nRow).html('<a href="../AccpacSyncLog/AccpacSyncLog.aspx?sid=' + aData.AccpacSyncUniqueNumber + '" target="_blank">' +
                aData.AccpacSyncUniqueNumber + '</a>');
                $('td:eq(0)', nRow).css('text-align', 'left');
                $('td:eq(1)', nRow).css('text-align', 'left');
                return nRow;

            }
        }
    });
    $('.table').find('tr').find('td').css('text-align', 'left');
    $('.footer').hide();

}