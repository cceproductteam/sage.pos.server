var QuickKeysServiceURL;
var QuickKeysViewItemTemplate;

var ItemListTemplate;
var QuickKeysId = selectedEntityId;
var lastUpdatedDate = null;
var addQuickKeysMode = false;

function InnerPage_Load() {
    setSelectedMenuItem('.pos-icon');
    QuickKeysServiceURL = systemConfig.ApplicationURL_Common + systemConfig.QuickKeysServiceURL;
    currentEntity = systemEntities.QuickKeysEntityId;
    //Advanced search Data
    mKey = 'QuickKeys'; //replace it with entity modual key
    InnerPage_Load2();
    $('.QuickSearch-holder').show();
    LoadSearchForm2(mKey, false);
    //Advanced search Data

    setPageTitle(getResource("QuickKeys"));
    setAddKeyLinkAction(getResource("AddQuickKeys"));

    GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.AddQuickKeysItemTemplate, function () {
        GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.QuickKeysListItemTemplate, loadData, 'ItemListTemplate');
    }, 'AddItemTemplate');

    $('.result-block').live("click", function () {
        QuickKeysId = $(this).attr('id');
    });

    deletedEntityCallBack = function () {
        removeDeletedEntity();
    };

    updatedEntityCallBack = function () {
        getEntityData(selectedEntityId);
    };

}

function loadData(data) {
    GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.QuickKeysListViewItemTemplate, function () {
        debugger
        QuickKeysViewItemTemplate.find('.ActionViewEdit-link').show();
        debugger
        checkPermissions(this, systemEntities.QuickKeysEntityId, -1, QuickKeysViewItemTemplate, function (returnedData) {
            QuickKeysViewItemTemplate = returnedData;
            loadEntityServiceURL = QuickKeysServiceURL + "FindByIdLite";
            entityItemListTemplate = data;
            entityItemViewTemplate = returnedData;
            findAllEntityServiceURL = QuickKeysServiceURL + "FindAllLite";
            deleteEntityServiceURL = QuickKeysServiceURL + "Delete";
            selectEntityCallBack = getQuickKeysInfo;
            listEntityCallBack = callBackAfterList;
            loadDefaultEntityOptions();
        });
    }, 'QuickKeysViewItemTemplate');
}

function initControls(template, isAdd) {
    if (!template) template = $('.TabbedPanels');
    if (!isAdd)
        $('.historyUpdated').append(HistoryUpdatedListViewTemplate.clone());
    LoadDatePicker(template.find('input:.date'), 'dd-mm-yy');
    template.find('.numeric').autoNumeric();
    preventInputChars(template.find('input:.integer'));


    template.find('.ActionViewEdit-link').unbind('click').live("click", function () {
        viewMode = false;
        AddMode = false;
        window.location = systemConfig.ApplicationURL_Common + systemConfig.pageURLs.AddQuickKeys + "?key=" + selectedEntityId;
        //        lockEntity(currentEntity, selectedEntityId, function () {
        //            window.location = systemConfig.ApplicationURL_Common + systemConfig.pageURLs.AddQuickKeys + "?key=" + selectedEntityId;
        //        }, currentEntityLastUpdatedDate, function () {
        //            loadEntityData(loadEntityServiceURL, entityItemViewTemplate, selectedEntityId, function (dataObject) {
        //                currentEntityLastUpdatedDate = dataObject.UpdatedDate || dataObject.Updated_Date;
        //                if (selectEntityCallBack) selectEntityCallBack(dataObject); $('.TabbedPanelsTab:first').click();
        //            });
        //        }, deletedEntityCallBack);
    });
}


function initAddControls(template) {
    initControls(template, true);
}

function getQuickKeysInfo(item) {
    $('.ActionViewEdit-link').unbind('click');
    if ($('.result-block').length > 0 && getQSKey('fdb') > 0 && isFirstTimeAdd) {
        $('.add-entity-button').first().click();
    }
     if (item != null) {
     }
 
  }

  function setListItemTemplateValue(entityTemplate, entityItem) {

      entityTemplate.find('.result-data1 label').text(Truncate(entityItem.Name, 20));
      return entityTemplate;
  }

 function setListItemServiceData(keyword, pageNumber, resultCount) {
     if (!QuickSearch) {
         return formatString('{ keyword: "{0}", page:{1}, resultCount:{2} ,argsCriteria:"{ QuickKeyId:{3}}"}', keyword, pageNumber, resultCount, filterEntityId);
     }
     else {
         return formatString('{mKey:"{0}",SearchCriteriaObj:"{1}",SortType:{2},SortFields:"{3}",TotalRecords:{4},PageNumber:{5}}', mKey, SearchCriteriaObj, SortType, SortFields, resultCount, pageNumber);
     }
 }



 function SaveEntity(isAdd, template) {
     var controlTemplate;
     var valid = true;

     if (isAdd) {
         controlTemplate = $('.add-QuickKeys-info');
         showLoading('.add-new-item');
     }
     else {
         controlTemplate = $('.QuickKeys-info');
         showLoading('.Result-info-container');
     }

     if (!saveForm(controlTemplate)) {
         hideLoading();
         valid = false;
     }

     addQuickKeysMode = isAdd;
     if (!valid) {
         return false;
     }

     QuickKeysId = -1;
     if (!isAdd) {
         QuickKeysId = selectedEntityId;
         lastUpdatedDate = getEntityLockDateFormat(currentEntityLastUpdatedDate);
     }

     var QuickKeysObj = {
         Name: controlTemplate.find('input.Name').val()
     };
     var data = formatString('{id:{0}, QuickKeysInfo:"{1}"}', isAdd ? -1 : selectedEntityId, escape(JSON.stringify(QuickKeysObj)));

     if (QuickKeysId > 0)
         url = QuickKeysServiceURL + "Edit";
     else
         url = QuickKeysServiceURL + "Add";
     post(url, data, function (returned) { saveSuccess(returned, isAdd, template); }, saveError, getEntitylockAjaxHeaders(lastUpdatedDate));
 }

 function saveSuccess(returnedValue,isAdd,template) {
     if (returnedValue.statusCode.Code == 501) {
         addPopupMessage(template.find('input.Name'), getResource("DublicatedName"));
         hideLoading();
         return false;
     }
     else if (returnedValue.statusCode.Code == 0) {
         hideLoading();
         if (returnedValue.statusCode.message == 'validation' && returnedValue.result) {
             filterError(returnedValue.result,template);
         }
         showStatusMsg(getResource("FaildToSave"));
         return false;
     }
     if (!addQuickKeysMode) {
         showStatusMsg(getResource("savedsuccessfully"));
         hideLoading();
         loadEntityData(loadEntityServiceURL, entityItemViewTemplate, selectedEntityId, getQuickKeysInfo);
     }
     else {
         hideLoading();
         addQuickKeysMode = false;
         $('.add-new-item').hide();
         $('.add-new-item').empty();
         appendNewListRow(returnedValue.result);
         showStatusMsg(getResource("savedsuccessfully"));
     }
 }

 function saveError(ex) {
     hideLoading();
     handleLockEntityError(ex, deletedEntityCallBack, updatedEntityCallBack);
 }



 function filterError(errors, template) {
     var allMsg = '';
     template.find('.validation').remove();
     $.each(errors, function (index, error) {
         if (error.GlobalMessage) {
             allMsg += '<span>' + error.Message + '<br/></span>';
         }
         else if (error.ControlName == 'Name') {
             addPopupMessage(template.find('input.Name'), getResource("required"));
         }

     });

     hideLoading();
     if (allMsg.trim()) {
         showOkayMsg('Validation error', allMsg.trim());
     }
 }

 function callBackAfterList() {
     if ($('.result-block').length <= 0 && getQSKey('fdb') > 0 && isFirstTimeAdd) {
         $('.add-entity-button').first().click();
     }
 }



 function setAddKeyLinkAction(label) {
     $('.addicon').show();
     $('.add-entity-button .addlabel').html(label);
     $('.add-entity-button').click(function () {
         isFirstTimeAdd = false;
         AddMode = true;
         window.location = systemConfig.ApplicationURL_Common + systemConfig.pageURLs.AddQuickKeys;
         return false;
     });

 }
