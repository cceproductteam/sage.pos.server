﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CentrixERP.Common.Web;
using CentrixERP.Configuration;

namespace CentrixERP.Web.POS.ERPIntegration
{
    public partial class CloseRegisterInquiry : BasePageLoggedIn
    {
        public CloseRegisterInquiry()
            : base(Enums_S3.Permissions.SystemModules.None) { }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}