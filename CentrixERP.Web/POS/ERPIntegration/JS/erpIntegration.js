﻿var POSInvoiceServiceURL;

var InvoiceInquiryHeaderTemplate;
var InvoiceInquiryResultsGridDetailsTemplate;
var itemsGrid = null;
var TotalBeforediscountandTax = 0, TotalCash = 0, TotalItemDiscount = 0, TotalOnAccount = 0, TotalInvoicediscount = 0, 
TotalCreditCard = 0, TotalSalesTax = 0, TotalCheque = 0;


function InnerPage_Load() {
    setPageTitle(getResource("InvoiceInquiry"));
    setSelectedMenuItem('.pos-icon');
    POSInvoiceServiceURL = systemConfig.ApplicationURL + systemConfig.POSPostInvoiceToShipmentServiceURL;


    GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.POSInvoiceListViewItemTemplate, function () {
        $('.white-container-data').append(InvoiceInquiryHeaderTemplate);
    }, 'InvoiceInquiryHeaderTemplate');

    GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.POSInvoiceResultGridTemplate, function () {
    }, 'InvoiceInquiryResultsGridDetailsTemplate');
    initControls();
    $('.btnSearch').attr('value', getResource('Refresh'));
    $('.lnkReset').attr('value', getResource('Reset'));
    $('.btnSearch').click();
}

function initControls() {

    LoadDatePicker('input:.date');

   //StoreAC = $('select:.Store').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.StoreServiceURL + 'FindAllLite', required: false, multiSelect: true });


    var InvoiceDateFrom;
    var InvoiceDateTo;

    $('input:.btnSearch').click(function () {
        TotalBeforediscountandTax = 0, TotalCash = 0, TotalItemDiscount = 0, TotalOnAccount = 0, TotalInvoicediscount = 0,
TotalCreditCard = 0, TotalSalesTax = 0, TotalCheque = 0;
        showLoading($('.Main-Div'));
        //InvoiceDateFrom = ($('input:.InvoiceDateFrom').val() != "") ? getDate('mm-dd-yy', getDotNetDateFormat($('input:.InvoiceDateFrom').val())) : getDate('mm-dd-yy', formatDate($('input:.InvoiceDateFrom').val()));
        //InvoiceDateTo = ($('input:.InvoiceDateTo').val() != "") ? getDate('mm-dd-yy', getDotNetDateFormat($('input:.InvoiceDateTo').val())) : getDate('mm-dd-yy', formatDate($('input:.InvoiceDateTo').val()));

        var url = POSInvoiceServiceURL + "InvoiceInquiryResults";
        var data = formatString('{storeIds:"{0}",InvoiceDateFrom :"{1}" , InvoiceDateTo:"{2}"}', -1, '', '');

        post(url, data, FindResultsSuccess, function () { }, null);
    });

    $('input:.lnkReset').click(function () {
        StoreAC.clear();

        $('.InvoiceDateFrom').val('');
        $('.InvoiceDateTo').val('');
    });
}

function loadItemsGrid(Template, datasource, rowTemplate, headerTemplate) {

    var control = [
  {
      controlId: 'InvoiceNumber',
      type: 'text'

  },
   {
       controlId: 'Type',
       type: 'text'

   },
    {
        controlId: 'InvoiceDate',
        type: 'text'

    },

    {
        controlId: 'TotalPrice',
        type: 'text',
        numeric: true

    },
     {
         controlId: 'ItemsDiscount',
         type: 'text',
         numeric: true

     },
      {
          controlId: 'Tax',
          type: 'text',
          numeric: true

      },
     {
         controlId: 'OnAccountAmount',
         type: 'text',
         numeric: true

     },
        {
            controlId: 'CashAmount',
            type: 'text',
            numeric: true

        },

      {
          controlId: 'Store',
          type: 'text'

      },
      {
          controlId: 'Register',
          type: 'text'

      },

      {
          controlId: 'Total',
          type: 'text'

      }


 ];
    //after create array of controls, we need init the grid

      itemsGrid = Template.find('#items-grid').Grid({
          headerTeamplte: Template.find('#h-template').html(),
          rowTemplate: InvoiceInquiryResultsGridDetailsTemplate,
          rowControls: control,
          dataSource: datasource,
          selector: '.',
          appendBefore: '.footer-row',
          rowDeleted: function (grid) {

              var Count = 0;
              $.each(grid.table.find('tr.r-data'), function (index, item) {
                  Count = Count + 1;
              });
              if (grid.rows.length == 1) {
                  grid.defaults.preventDelete = true;
              } else grid.defaults.preventDelete = false;
          },
          rowAdded: function (row, controls, dataRow, grid) {

              TotalInvoicediscount += dataRow.AmountDiscount;
              TotalBeforediscountandTax += dataRow.TotalPrice;
              TotalCash += dataRow.CashAmount;
              TotalItemDiscount += dataRow.ItemsDiscount;
              TotalOnAccount += dataRow.OnAccountAmount;
              TotalCreditCard += dataRow.CreditCardAmount;
              TotalSalesTax += dataRow.Tax;
              TotalCheque += dataRow.ChequeAmount;

              var TotalLine = dataRow.TotalPrice - dataRow.ItemsDiscount - dataRow.AmountDiscount + dataRow.Tax;
              var totalPaymenbt = dataRow.CashAmount + dataRow.OnAccountAmount + dataRow.CreditCardAmount + dataRow.ChequeAmount;
              $('.Subtotal').html(parseFloat(TotalBeforediscountandTax).toFixed(3));
              $('.totalCash').html(parseFloat(TotalCash).toFixed(3));
              $('.itemDiscount').html(parseFloat(TotalItemDiscount).toFixed(3));
              $('.onAccount').html(parseFloat(TotalOnAccount).toFixed(3));
              $('.invoicediscount').html(parseFloat(TotalInvoicediscount).toFixed(3));
              $('.totalcreditcard').html(parseFloat(TotalCreditCard).toFixed(3));
              $('.totaltax').html(parseFloat(TotalSalesTax).toFixed(3));
              $('.totalcheque').html(parseFloat(TotalCheque).toFixed(3));
              $('.totalPayment').html(parseFloat(TotalCheque + TotalCreditCard + TotalOnAccount + TotalCash).toFixed(3));
              $('.TotalSales').html(parseFloat(TotalBeforediscountandTax - TotalInvoicediscount - TotalItemDiscount + TotalSalesTax).toFixed(3));
              row.find('.invoice').css('color', 'green');
              row.find('.payment').css('color', 'red');
              row.find('.Total').css('font-weight', 'bold');
              row.find('.TotalPayment').css('font-weight', 'bold');

              row.find('.Total').html(parseFloat(TotalLine).toFixed(3));
              row.find('.TotalPayment').html(parseFloat(totalPaymenbt).toFixed(3));
              row.find('.DifAmount').html(parseFloat(TotalLine - totalPaymenbt).toFixed(3));
              if (dataRow.Type == "Refund") {
                  row.find('.Type').css('color', 'red');
              }

              if (dataRow.Type == "Invoice" && dataRow.InvoiceNumber.includes("(")) {
                  row.find('.Type').css('color', 'red');
              }
              if (!dataRow) {
                  row.find('.viewedit-display-none').show();
              }

              var Count = 0;
              $.each(grid.table.find('tr.r-data'), function (index, item) {

                  row.find('.ShipmentNumber').unbind().click(function () {
                      window.open(systemConfig.ApplicationURL + systemConfig.pageURLs.IcShipmentList + "?key=" + item.ShipmentId);

                  });

                  Count = Count + 1;
              });


              if (grid.rows.length == 1) {
                  grid.defaults.preventDelete = true;
              } else grid.defaults.preventDelete = false;

          }, preventAdd: false
      });
}


function FindResultsSuccess(returnedValue) {   //Here Create the details grid , Fill it with corresponding data
    hideLoading();

    if (returnedValue.result != null)
        $('.currency').html(returnedValue.result[0].Currency);
    $('#items-grid').empty();
    var result = returnedValue.result;
    if (result && result.length > 0) {
        $('.NoDataFound').hide();
        loadItemsGrid($('.gridContainerDiv'), result);


        $($('table')[2]).wrap("<div class='grid-height'></div>");  //To Control the grid height


        $($('.ShipmentNumber')[0]).click(function () {
            window.open(systemConfig.ApplicationURL + systemConfig.pageURLs.IcShipmentList + "?key=" + $($('.ShipmentId')[0]).text());

        });

        $.each($('.InvoiceDate'), function (ix, itm) {
            $($('.InvoiceDate')[ix]).text(getDate("dd-mm-yy", formatDate($($('.InvoiceDate')[ix]).text())));
        });


    }
    else {
        $('.NoDataFound').show();
        $('.NoDataFound').attr('style', 'display: inline-block !important;');
    }





}



function getdate(date) {
    var arr = date.split('-');
    return arr[1] + '-' + arr[0] + '-' + arr[2];
}




