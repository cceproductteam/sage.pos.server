﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CloseRegisterInquiry.aspx.cs" 
Inherits="CentrixERP.Web.POS.ERPIntegration.CloseRegisterInquiry" MasterPageFile="~/Common/MasterPage/ERPSite.master" %>

<asp:Content ID="PageHeader" runat="server" ContentPlaceHolderID="PageHeader">
       <script src="../../Common/DataDictionary/DataDictionary-ar.js" type="text/javascript"></script>
    <script src="../../Common/DataDictionary/DataDictionary-en.js" type="text/javascript"></script>
    <link href="../CSS/POSMain.css" rel="stylesheet" type="text/css" />
    <script src="JS/CloseRegisterInquiry.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="PageBody" runat="server" ContentPlaceHolderID="PageBody">

<div class="">
            <div class="Details-div">
                <span class="details-div-title" resourcekey="AdvFilterYourResults"></span>
                <div class="Action-div">
                    <ul>
                    </ul>
                </div>
            </div>
            <table cellspacing="0" cellpadding="0" border="0" class="Data-Table">
                <tbody>
                    <tr>
                        <th>
                            <span class="label-title" resourcekey="Store"></span>
                        </th>
                        <td>
                            <select class="lst Store">
                            </select>
                        </td>
                         


                          <th>
                            <span class="label-title" resourcekey="Register"></span>
                        </th>
                        <td>
                            <select class="lst Register">
                            </select>
                        </td>

                        
                    
                    </tr>
                    <tr>

                     <th>
                            <span class="label-title" resourcekey="OpenedDateFrom"></span>
                        </th>
                    <td>
                    <input class="cx-control OpenedDateFrom  date" type="text" />
                    </td>

                     
                          <th>
                            <span class="label-title" resourcekey="OpenedDateTo"></span>
                        </th>
                    <td>
                    <input class="cx-control OpenedDateTo  date" type="text" />
                    </td>
                  
                    </tr>

                       <tr>

                     <th>
                            <span class="label-title" resourcekey="ClosedDateFrom"></span>
                        </th>
                    <td>
                    <input class="cx-control ClosedDateFrom  date" type="text" />
                    </td>

                     
                          <th>
                            <span class="label-title" resourcekey="ClosedDateTo"></span>
                        </th>
                    <td>
                    <input class="cx-control ClosedDateTo  date" type="text" />
                    </td>
                  
                    </tr>
                    <tr>
                        <td class="search-buttons" colspan="6">
                            <div class="filter-reset">
                                <input type="button" class="btnSearch button2" id="btnSearch" value="Search" />
                                <input value="Reset" class="lnkReset lnkReset-InvoiceInquiry button-cancel button2" type="button" />
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>


        <span class="display-none NoDataFound NoDataFound-Inquiry" resourcekey="NoDataFound"></span>




</asp:Content>