﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DashBoard2.aspx.cs" Inherits="SagePOSServer.Web.POS.DashBoard.DashBoard2" MasterPageFile="~/Common/MasterPage/ERPSite.master" %>

<asp:Content runat="server" ContentPlaceHolderID="PageHeader" ID="PageHeader">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="CSS/bootstrap.min.css" rel="stylesheet" />
    <link href="CSS/font-awesome.min.css" rel="stylesheet" />
    <link href="CSS/nprogress.css" rel="stylesheet" />
    <link href="CSS/daterangepicker.css" rel="stylesheet" />
    <link href="CSS/custom.css" rel="stylesheet" />
    <a href="CSS/bootstrap.min.css.map"></a>
    <script src="JS/jquery.min.js"></script>
    <script src="JS/bootstrap.min.js"></script>
    <script src="JS/fastclick.js"></script>
    <script src="JS/nprogress.js"></script>
    <script src="JS/Chart.min.js"></script>
    <script src="JS/jquery.sparkline.min.js"></script>
    <script src="JS/jquery.flot.js"></script>
    <script src="JS/jquery.flot.pie.js"></script>
    <script src="JS/jquery.flot.time.js"></script>
    <script src="JS/jquery.flot.stack.js"></script>
    <script src="JS/jquery.flot.resize.js"></script>
    <script src="JS/jquery.flot.orderBars.js"></script>
    <script src="JS/jquery.flot.spline.min.js"></script>
    <script src="JS/curvedLines.js"></script>
    <script src="JS/date.js"></script>
    <script src="JS/moment.min.js"></script>
    <script src="JS/daterangepicker.js"></script>
    <script src="JS/custom.js"></script>
    <script src="JS/DashBoard2.js"></script>


</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="PageBody" ID="PageBody">
    <div id="body-div" class="nav-md">
        <div class="container body">
            <div class="main_container">
                <div class="col-md-3 left_col">
                    <div class="left_col scroll-view">
                        <div class="navbar nav_title" style="border: 0;">
                            <img id="header-icon" class=" logo-icon" src="../../Common/Images/cl.png" class="" />

<<<<<<< HEAD
                            <a class="site_title" style="left: 11% !important; position: absolute;"><span>Sage POS</span></a>
=======
                            <a  class="site_title" style="left: 11% !important; position: absolute;"><span>Sage POS</span></a>
>>>>>>> fa8684950922d126387c9349cd0262141a38caf4
                        </div>

                        <div class="clearfix"></div>

                        <!-- menu profile quick info -->
                        <div class="profile clearfix">
                            <div class="profile_pic">
                            </div>
                            <div class="profile_info">

                                <h2><span class="user-name-new"></span></h2>
                            </div>
                        </div>
                        <!-- /menu profile quick info -->

                        <br />

                        <!-- sidebar menu -->
                        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                            <div class="menu_section">
                                <h3></h3>
                                <ul class="nav side-menu">
                                    <li><a><i class="fa fa-home"></i>Module Setup <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu">
                                            <li><a href="../Store/StoreList.aspx">Stores</a></li>
                                            <li><a href="../Register/RegisterList.aspx">Registers</a></li>
                                            <li><a href="../QuickKeys/QuickKeysList.aspx">Quick Keys</a></li>
                                            <li><a href="../SystemConfigration/SystemConfigration.aspx">Configuration</a></li>
                                            <li><a href="../../NewUserManagement/Users/UsersList.aspx">Users</a></li>
                                            <li><a href="../../NewUserManagement/Roles/RolePermission.aspx">Permissions</a></li>
                                            <li><a href="../Contact/ContactList.aspx">Contacts</a></li>
                                        </ul>
                                    </li>
                                    <li><a><i class="fa fa-sitemap"></i>Synchronization <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu">
                                            <li><a href="#" class="lnk-update-accpac">Update Accpac</a></li>
                                            <li><a href="#" class="lnk-update-registers">Update Registers </a></li>
                                            <li><a href="../Register/RegisterStatus.aspx">Register Status </a></li>
                                            <li><a href="../SyncActivityLog/SyncActivityLog.aspx">Activity Log</a></li>
                                            <li><a href="../../NewReports/InvoiceInquiry/InvoiceInquiry.aspx">POS Transaction Log</a></li>
                                        </ul>
                                    </li>

                                    <li><a><i class="fa fa-desktop"></i>Reports & Inquires <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu">
                                            <li><a href="../../NewReports/TotalDailySales/POSTotalDailySales.aspx">POS Receipts Summary</a></li>
                                            <li><a href="../../NewReports/TotalDailySummation/TotalDailySummation.aspx">POS Daily Receipts</a></li>
                                            <li><a href="../../NewReports/DailySalesPerItem/POSDailySalesPerItem.aspx">POS Sales Details Report</a></li>
                                            <li><a href="../../NewReports/Dailysales/POSDailysales.aspx">POS Sales Summary Report</a></li>
                                            <li><a href="../../NewReports/POSDailySalesSummation/POSDailySalesSummation.aspx">POS Daily Sales</a></li>
                                            <li><a href="../../NewReports/PosPettyCash/PosPettyCash.aspx">Petty Cash Report</a></li>
                                            <li><a href="../../NewReports/CloseRegisterReport/CloseRegisterReport.aspx">Close Register Report</a></li>

                                        </ul>
                                    </li>


                                </ul>
                            </div>


                        </div>
                        <!-- /sidebar menu -->

                        <!-- /menu footer buttons -->
                        <div class="sidebar-footer hidden-small">
                        </div>
                        <!-- /menu footer buttons -->
                    </div>
                </div>

                <!-- top navigation -->
                <div class="top_nav">
                    <div class="nav_menu">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>
                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <span class="user-name-header"></span>
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu pull-right">
                                    <li><a href="../../Logout.aspx"><i class="fa fa-sign-out pull-right"></i>Log Out</a></li>
                                </ul>
                            </li>


                        </ul>

                    </div>
                </div>
                <!-- /top navigation -->

                <!-- page content -->
                <div class="right_col" role="main">
                    <div class="">
                        <div class="row top_tiles">
                            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="tile-stats">
<<<<<<< HEAD
                                    <div class="icon"><i class="fa fa-shopping-bag"></i></div>
                                    <div class="count" id="stores"></div>
                                    <h3>Stores</h3>
                                    <p>Number of active store</p>
=======
                                    <div class="icon"><i class="fa fa-caret-square-o-right"></i></div>
                                    <div class="count" id="stores"></div>
                                    <h3>Stores</h3>
                                    <p>Number of store</p>
>>>>>>> fa8684950922d126387c9349cd0262141a38caf4
                                </div>
                            </div>
                            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="tile-stats">
<<<<<<< HEAD
                                    <div class="icon"><i class="fa fa-television"></i></div>
                                    <div class="count" id="registers"></div>
                                    <h3>Registers</h3>
                                    <p>Number of active registers</p>
=======
                                    <div class="icon"><i class="fa fa-comments-o"></i></div>
                                    <div class="count" id="registers"></div>
                                    <h3>Registers</h3>
                                    <p>Number of registers</p>
>>>>>>> fa8684950922d126387c9349cd0262141a38caf4
                                </div>
                            </div>
                            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="tile-stats">
<<<<<<< HEAD
                                    <div class="icon"><i class="fa fa-signal"></i></div>
                                    <div class="count" id="Users"></div>
                                    <h3>Sales </h3>
=======
                                    <div class="icon"><i class="fa fa-sort-amount-desc"></i></div>
                                    <div class="count" id="Users"></div>
                                    <h3>Transactions</h3>
>>>>>>> fa8684950922d126387c9349cd0262141a38caf4
                                    <p>Number of transactions</p>
                                </div>
                            </div>
                            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="tile-stats">
<<<<<<< HEAD
                                    <div class="icon"><i class="fa fa-refresh"></i></div>
                                    <div class="count" id="quickkeys"></div>
                                    <h3>Refund & Exchange </h3>
                                    <p>Number of transactions</p>
=======
                                    <div class="icon"><i class="fa fa-check-square-o"></i></div>
                                    <div class="count" id="quickkeys"></div>
                                    <h3>Sales</h3>
                                    <p>Total Sales</p>
>>>>>>> fa8684950922d126387c9349cd0262141a38caf4
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="x_panel">
                                    <div class="x_title">
<<<<<<< HEAD
                                        <h2>Sales Summary <small></small></h2>
                                        <ul class="nav navbar-right panel_toolbox">
=======
                                        <h2>Transaction Summary <small></small></h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                           
>>>>>>> fa8684950922d126387c9349cd0262141a38caf4
                                            </li>
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                                <ul class="dropdown-menu storelist" role="menu">
<<<<<<< HEAD
=======
                                                    
>>>>>>> fa8684950922d126387c9349cd0262141a38caf4
                                                </ul>
                                            </li>

                                        </ul>
                                        <div class="filter">
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <div class="col-md-9 col-sm-12 col-xs-12" style="width: 100% !important;">
                                            <div class="demo-container" style="height: 280px">
                                                <div id="placeholder33x" class="demo-placeholder"></div>
                                            </div>


                                        </div>



                                    </div>
                                </div>
                            </div>
                        </div>

<<<<<<< HEAD

=======
>>>>>>> fa8684950922d126387c9349cd0262141a38caf4
                    </div>
                </div>

            </div>
        </div>
    </div>
</asp:Content>
