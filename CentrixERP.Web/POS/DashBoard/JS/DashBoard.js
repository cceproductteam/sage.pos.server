﻿
function InnerPage_Load() {  
    setSelectedMenuItem($(".dash-board"));
    setPageTitle(getResource("DashBoard"));
    $('.white-container-data').find('img').hide();
    $('.white-container-data').find('img').hide();
    GetDashBoardBoxes();
    GetResgistersSales();
}


function GetDashBoardBoxes()
{
    var POSInvoiceServiceURL = systemConfig.ApplicationURL + "Common/WebServices/InvoiceWebService.asmx/";
    var url = POSInvoiceServiceURL + "GetDashBoardBoxesMapper";
    post(url, "", FindResultsSuccess, function () { }, null);
}

function GetResgistersSales() {
    var POSInvoiceServiceURL = systemConfig.ApplicationURL + "Common/WebServices/InvoiceWebService.asmx/";
    var url = POSInvoiceServiceURL + "GetRegisterSales";
    post(url, "", FindRegisterSalesSuccess, function () { }, null);
}


function FindResultsSuccess(data) {
    $('.transaction-count').html(data.Stores);
    $('.registers').html(data.Registers);
    $('.Users').html(data.Users);
    $('.quickkeys').html(data.QuickKeys);
}

function FindRegisterSalesSuccess(data)
{
    //var myData = new Array(['Asia', 437], ['Europe', 322], ['North America', 233], ['Latin America', 110], ['Africa', 34], ['Middle East', 20], ['Aus/Oceania', 19]);
    var arr = $.map(data, function (el) { return el });
    var jsonData = JSON.stringify(data);
    var myData = arr;
    var colors = ['#b2c962', '#a6c962', '#b2c962', '#a6c962', '#b2c962', '#a6c962', '#b2c962'];
    var myChart = new JSChart('graph', 'bar');

    var yourDataPoints = [];
    $.each(data, function (index, Info) {
        yourDataPoints.push({
            x: Info.RegisterName,
            y: Info.Total
            
        });
    });

    var JsonArray = $.map(data, function (el) { return el });
    myChart.setDataArray(yourDataPoints);
   // myChart.setDataJSON(yourDataPoints);
    myChart.colorizeBars(colors);
    myChart.setTitle('Internet usage by World Region (millions of users)');
    myChart.setTitleColor('#8E8E8E');
    myChart.setAxisNameX('');
    myChart.setAxisNameY('');
    myChart.setAxisColor('#C4C4C4');
    myChart.setAxisNameFontSize(16);
    myChart.setAxisNameColor('#999');
    myChart.setAxisValuesColor('#777');
    myChart.setAxisColor('#B5B5B5');
    myChart.setAxisWidth(1);
    myChart.setBarValuesColor('#2F6D99');
    myChart.setBarOpacity(0.5);
    myChart.setAxisPaddingTop(60);
    myChart.setAxisPaddingBottom(40);
    myChart.setAxisPaddingLeft(45);
    myChart.setTitleFontSize(11);
    myChart.setBarBorderWidth(0);
    myChart.setBarSpacingRatio(50);
    myChart.setBarOpacity(0.9);
    myChart.setFlagRadius(6);
    myChart.setTooltip(['North America', 'U.S.A and Canada']);
    myChart.setTooltipPosition('nw');
    myChart.setTooltipOffset(3);
    myChart.setSize(616, 321);
    myChart.setBackgroundImage('chart_bg.jpg');
    myChart.draw();

}