﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DashBoard.aspx.cs" Inherits="SagePOS.Server.API.POS.DashBoard.DashBoard" MasterPageFile="~/Common/MasterPage/ERPSite.master" %>

<asp:Content runat="server" ContentPlaceHolderID="PageHeader" ID="PageHeader">
    <script src="JS/jscharts.js"></script>
    <script src="JS/DashBoard.js"></script>
    <link href="CSS/Boxes.css" rel="stylesheet" />


    <style>
        .dashboard-stat2 {
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            -ms-border-radius: 4px;
            -o-border-radius: 4px;
            border-radius: 4px;
            background: #fff;
            padding: 15px 15px 30px;
        }

            .dashboard-stat2 .display .number {
                float: left;
                display: inline-block;
            }

                .dashboard-stat2 .display .number h3 {
                    margin: 0 0 2px;
                    padding: 0;
                    font-size: 40px;
                    font-weight: 400;
                }

        .col-lg-3 {
            width: 24%;
        }

        .dashboard-stat2, .dashboard-stat2 .display {
            margin-bottom: 20px;
        }

        .row {
            margin-left: -15px;
            margin-right: -15px;
        }

    </style>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="PageBody" ID="PageBody">
    <div class="blocks"  style="margin-left: 17px;    margin-top: 10px;" >
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" style="display:inline-block;">
            <div class="dashboard-stat2 " style="background-color: #EEEEEE;border: 1px solid #b2c962;">
                <div class="display">
                    <div class="number">
                        <h3 class="font-green-sharp" style="color: #a6c962 !important;">
                            <span data-counter="counterup" class="stores" ></span>
                            <small class="font-green-sharp" style="color: #a6c962 !important;"></small>
                        </h3>
                        <small>Store</small>
                    </div>

                </div>

            </div>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12"  style="display:inline-block;">
            <div class="dashboard-stat2 " style="background-color: #EEEEEE;border: 1px solid #b2c962;">
                <div class="display">
                    <div class="number">
                        <h3 class="font-green-sharp" style="color: #a6c962 !important;">
                            <span data-counter="counterup"  class="registers"></span>
                            <small class="font-green-sharp" style="color: #a6c962 !important;"></small>
                        </h3>
                        <small>Register</small>
                    </div>

                </div>

            </div>
        </div>

         <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12"  style="display:inline-block;">
            <div class="dashboard-stat2 " style="background-color: #EEEEEE;border: 1px solid #b2c962;">
                <div class="display">
                    <div class="number">
                        <h3 class="font-green-sharp" style="color: #a6c962 !important;">
                            <span data-counter="counterup" class="Users"></span>
                            <small class="font-green-sharp" style="color: #a6c962 !important;"></small>
                        </h3>
                        <small>User</small>
                    </div>

                </div>

            </div>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12"  style="display:inline-block;">
            <div class="dashboard-stat2 " style="background-color: #EEEEEE;border: 1px solid #b2c962;">
                <div class="display">
                    <div class="number">
                        <h3 class="font-green-sharp" style="color: #a6c962 !important;">
                            <span data-counter="counterup" class="quickkeys"></span>
                            <small class="font-green-sharp" style="color: #a6c962 !important;"></small>
                        </h3>
                        <small>Quick Key</small>
                    </div>

                </div>

            </div>
        </div>

   </div>

    <table>


        <tr>

            <td>
                <div id="graph">Loading graph...</div>
            </td>
            <td>
                <div id="Pieghaph">Loading graph...</div>
            </td>
        </tr>

    </table>


    <script type="text/javascript">



        //var myData = new Array(['Asia', 437], ['Europe', 322], ['North America', 233], ['Latin America', 110], ['Africa', 34], ['Middle East', 20], ['Aus/Oceania', 19]);
        //var colors = ['#b2c962', '#a6c962', '#b2c962', '#a6c962', '#b2c962', '#a6c962', '#b2c962'];
        //var myChart = new JSChart('graph', 'bar');
        //myChart.setDataArray(myData);
        //myChart.colorizeBars(colors);
        //myChart.setTitle('Internet usage by World Region (millions of users)');
        //myChart.setTitleColor('#8E8E8E');
        //myChart.setAxisNameX('');
        //myChart.setAxisNameY('');
        //myChart.setAxisColor('#C4C4C4');
        //myChart.setAxisNameFontSize(16);
        //myChart.setAxisNameColor('#999');
        //myChart.setAxisValuesColor('#777');
        //myChart.setAxisColor('#B5B5B5');
        //myChart.setAxisWidth(1);
        //myChart.setBarValuesColor('#2F6D99');
        //myChart.setBarOpacity(0.5);
        //myChart.setAxisPaddingTop(60);
        //myChart.setAxisPaddingBottom(40);
        //myChart.setAxisPaddingLeft(45);
        //myChart.setTitleFontSize(11);
        //myChart.setBarBorderWidth(0);
        //myChart.setBarSpacingRatio(50);
        //myChart.setBarOpacity(0.9);
        //myChart.setFlagRadius(6);
        //myChart.setTooltip(['North America', 'U.S.A and Canada']);
        //myChart.setTooltipPosition('nw');
        //myChart.setTooltipOffset(3);
        //myChart.setSize(616, 321);
        //myChart.setBackgroundImage('chart_bg.jpg');
        //myChart.draw();



        ///////////////
        ///Pie Chart
        var myData = new Array(['North America', 69], ['Australia/Oceania', 54], ['Europe', 40], ['Latin America', 20], ['Asia', 12], ['Middle East', 10], ['Africa', 4], ['World average', 18]);
        var colors = ['#b2c962', '#a6c962', '#b5e20f', '#607a04', '#1BA4F9', '#41B2FA', '#63C1FA', '#83CDFA'];
        var myChart = new JSChart('Pieghaph', 'pie');
        myChart.setDataArray(myData);
        myChart.colorizePie(colors);
        myChart.setTitle('Internet penetration by World Region in 2007 (%)');
        myChart.setTitleColor('#8E8E8E');
        myChart.setTitleFontSize(11);
        myChart.setTextPaddingTop(30);
        myChart.setPieUnitsColor('#8F8F8F');
        myChart.setPieValuesColor('#6E6E6E');
        myChart.setSize(616, 321);
        myChart.setPiePosition(308, 190);
        myChart.setPieRadius(85);
        myChart.setBackgroundImage('chart_bg.jpg');
        myChart.draw();


        $('.white-container-data').find('img').hide();

    </script>


</asp:Content>
