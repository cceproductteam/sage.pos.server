﻿var StoreCount = 0, LastStore = 0;
var SyncCriteriaArray = new Array(), StoresUserNamePaswwordArray = new Array();
var StoreFullDataObject;
var StoreAC, StoreId = 0, SearchedStores = 0;
var StoreIp = "", NotSyncedMode = false;
var ResultMessage = '';
var table;
var ResultCount = 10000000;

function StoreData() {
    this.StoreId = 0;
    this.StoreName = "";
    this.IpAddress = "";
    this.LocationId = -1;
    this.DataBaseInstanceName = "";
    this.DataBaseName = "";
    this.DataBaseUserName = "";
    this.DataBasePassword = "";

}

function SyncStoreData() {
    debugger
    this.StoreId = 0;

  
    this.BankAccount = ($('.BankAccount').attr('checked')) ? true : false;
    this.Category = ($('.Category').attr('checked')) ? true : false;
    this.Currency = ($('.Currency').attr('checked')) ? true : false;
    this.CurrencyRates = ($('.CurrencyRates').attr('checked')) ? true : false;
    this.Customer = ($('.Customer').attr('checked')) ? true : false;

    this.GLAccounts = ($('.GLAccounts').attr('checked')) ? true : false;
    this.Location = ($('.Location').attr('checked')) ? true : false;

    this.PriceList = ($('.PriceList').attr('checked')) ? true : false;

    this.TaxAuthorities = ($('.TaxAuthorities').attr('checked')) ? true : false;

    this.taxMatrix = ($('.taxMatrix').attr('checked')) ? true : false;
    this.ItemCard = ($('.ItemCard').attr('checked')) ? true : false;
    this.ItemPriceList = ($('.ItemPriceList').attr('checked')) ? true : false;
    this.ItemStock = ($('.ItemStock').attr('checked')) ? true : false;
    this.ItemTax = ($('.ItemTax').attr('checked')) ? true : false;
    this.TransferData = ($('.TransferData').attr('checked')) ? true : false;


    /////////////////////////////////////////////////////
    //////////Register Data/////////////////////////
    //this.GetTransactions = ($('.syncRegData').attr('checked')) ? true : false;
    //this.GetCloseRegister = ($('.syncRegData').attr('checked')) ? true : false;
    //this.GetCustomers = ($('.syncRegData').attr('checked')) ? true : false;
    ////////////User Management/////////////////////////////////
    /////////////////////////////////////////////////////////
    //this.GetUserData = ($('.SyncUserManagement').attr('checked')) ? true : false;
    //this.GetRoles = ($('.SyncUserManagement').attr('checked')) ? true : false;
    //this.GetTeams = ($('.SyncUserManagement').attr('checked')) ? true : false;
    //this.GetPermissions = ($('.SyncUserManagement').attr('checked')) ? true : false;
    //this.GetShifts = ($('.SyncUserManagement').attr('checked')) ? true : false;
    //this.GetUserShifts = ($('.SyncUserManagement').attr('checked')) ? true : false;
    /////////////////////////////////////////////////////////////
    /////////////////////////Contact Managment////////////////
    //this.GetPersonData = ($('.SyncContactManagment').attr('checked')) ? true : false;
    //this.GetCompanyData = ($('.SyncContactManagment').attr('checked')) ? true : false;
    /////////////////////////////////////////////////////
    /////////////////POS Administration Data//////////
    //this.GetStores = ($('.SyncPOSAdministration').attr('checked')) ? true : false;
    //this.GetRegisters = ($('.SyncPOSAdministration').attr('checked')) ? true : false;
    //this.GetQuickKeys = ($('.SyncPOSAdministration').attr('checked')) ? true : false;
    //this.GetPaymentData = false;
    //this.GetVouchers = ($('.SyncPOSAdministration').attr('checked')) ? true : false;
    //this.GetConfigData = ($('.SyncPOSAdministration').attr('checked')) ? true : false;
    //this.GetLovs = ($('.SyncPOSAdministration').attr('checked')) ? true : false;
    //////////////////////////////////////////////////////////////
    ////////////////Master Data ///////////////////////////////
    //this.GetItems = ($('.SyncInventoryData').attr('checked')) ? true : false;
    //this.GetPriceList = ($('.SyncInventoryData').attr('checked')) ? true : false;
    //this.GetItemPriceList = ($('.SyncInventoryData').attr('checked')) ? true : false;
    //this.GetItemTax = ($('.SyncInventoryData').attr('checked')) ? true : false;
    //this.GetCurrencyData = ($('.SyncInventoryData').attr('checked')) ? true : false;
    //this.GetCurrencyRates = ($('.SyncInventoryData').attr('checked')) ? true : false;
    //this.GetTaxMatrix = ($('.SyncInventoryData').attr('checked')) ? true : false;
    //this.GetTaxAuth = ($('.SyncInventoryData').attr('checked')) ? true : false;
    //this.GetAccpacCustomerData = ($('.SyncInventoryData').attr('checked')) ? true : false;
    //this.GetStockData = ($('.SyncInventoryData').attr('checked')) ? true : false;
    //this.GetLocationData = ($('.SyncInventoryData').attr('checked')) ? true : false;
    //this.GetItemSerial = false;
    ///////////////////////////////////////////////////////////////////////////////////////
    /////////////////////System Configuration///////////////////////////
 
    //this.GetStoreTransactions = $(chGetStoreTrnasctionData).attr('checked');
    this.DataBaseUserName = "";
    this.DataBasePassword = "";
    this.StoreName = "";
    this.DataBaseInstanceName = "";
    this.DataBaseName = "";
    this.IpAddress = "";
    this.SyncMode = 0;
}

function getPostDate() {


    this.PostInvoice = ($('.PostInvoice').attr('checked')) ? true : false;
    this.PostRefund = ($('.PostRefund').attr('checked')) ? true : false;
    this.PostExchange = ($('.PostExchange').attr('checked')) ? true : false;
    this.PostPettyCash = ($('.PostPettyCash').attr('checked')) ? true : false;

    this.Status = 0;
    this.SyncLog = 0;

}

function InnerPage_Load() {
    setSelectedMenuItem('.pos-icon');
    currentEntity = systemEntities.POSSyncEntityId;
    setPageTitle(getResource("POSSync"));
    checkPermissions(this, systemEntities.POSSyncEntityId, -1, $('.POSSync-Fieldset'), function (returnedData) {
        template = returnedData;
        initControls();

        $('.POSSync-Fieldset').show();
    });
    GetSyncData();
    $('.ActionSync-link').click(function () {
        $('.tbl_result td').empty();
        $('.show-full-details').remove();
        $('.Close').remove();
        SyncData();
    });

    $('#buttonin').mouseup(function () {


       

        SetAccpacSync();
       
    });

    $('#buttonS').mouseup(function () {




        GetAccpacData();

    });
}

function initControls() {
  
    $('.syncAdminData').click(function () {
        $('.table').hide();
        if ($(this).hasClass('SelectedTab')) { }
        else {
            $(this).addClass('SelectedTab');
            $('.syncRegDataTab').removeClass('SelectedTab');
            $('.syncAdminDataList').show();
            $('.syncRegDataTabList').hide();
        }
    });

    $('.syncRegDataTab').click(function () {
        $('.table').show();
        if ($(this).hasClass('SelectedTab')) { }
        else {
            $(this).addClass('SelectedTab');
            $('.syncAdminData').removeClass('SelectedTab');
            $('.syncAdminDataList').hide();
            $('.syncRegDataTabList').show();
        }
    });
    $('.syncRegData').change(function () {
        if ($(this).attr('checked'))
            $('.syncRegDataTabList').find('input').attr('checked', true);
        else
            $('.syncRegDataTabList').find('input').attr('checked', false);

    });

    $('.ChSyncAdminData').change(function () {
        if ($(this).attr('checked'))
            $('.syncAdminDataList').find('input').attr('checked', true);
        else
            $('.syncAdminDataList').find('input').attr('checked', false);

    });

    $('.all-stores').change(function () {
        if ($(this).attr('checked')) {
            $('.store-selectAll').attr('checked', true);
        }
        else {
            $('.store-selectAll').attr('checked', false);
        }
        $('.store-selectAll').change();
    });
    $('.searchIcon').click(function () {
        SearchStores($('.searchBox').val());
    });

    $('.searchBox').keydown(function (event) {
        if (event.keyCode == 13 || event.which == 13)
            $('.searchIcon').click();
    });


}


function GetStores() {
    showLoading($('.white-container-data'));
    $('.reg-regName-container').empty();
    $.ajax({
        type: "POST",
        url: systemConfig.ApplicationURL_Common + systemConfig.StoreServiceURL + "FindAllLite",
        contentType: "application/json; charset=utf-8",
        data: formatString('{ keyword: "", page:1, resultCount:{0},argsCriteria:""}', ResultCount),
        dataType: "json",
        success: function (data) {
            hideLoading();
           
            showLoading($('.Stores-list-container'));
            var StoresList = eval("(" + data.d + ")").result;
            if (StoresList != null) {
                loadAllStores(StoresList);
            }
        },
        error: function (e) {
        }
    });
}


function GetSyncData() {
    debugger
   

    $.ajax({
        type: "POST",
        url: systemConfig.ApplicationURL_Common + systemConfig.AccpacSyncSchedule + "FindAllLite",
        contentType: "application/json; charset=utf-8",
        data: formatString('{ keyword: "", page:1, resultCount:{0},argsCriteria:""}', ResultCount),
        dataType: "json",
        success: function (data) {
 
            var  AccpacSynkoj= eval("(" + data.d + ")").result;
            if (AccpacSynkoj != null) {
                //loadAllStores(StoresList);
                loadAccpacSync(AccpacSynkoj);

            }
        },
        error: function (e) {
        }
    });
}



function loadAllStores(Stores) {
    StoreFullDataObject = Stores;
    var Container = $('.reg-regName-container');
    ///////////////////////
    /////append stores data ///////
    /////////////////////
    $('.table').show();
    var storeDiv = $('<div>').attr({ class: 'stores-title', id: 'all-store-div' });
    var storeSpan = $('<span>').attr({ class: 'stores-title-span' });
    var storeSyncAllChk = $('<input>').attr({ type: 'checkbox', class: 'store-selectAll', id: 'all-store-sync' }).change(function () {
        if ($(this).attr('checked')) {
            $('#registerul-all').find('input:enabled').attr('checked', true);
            StoreIp = 'all';
        }
        else {
            $('#registerul-all').find('input:enabled').attr('checked', false);
            StoreIp = '';
        }

        $('#registerul-all').find('input:enabled').change();

    });
    var spanLabel = $('<span>').html('&nbsp;' + getResource('Stores'));
    var registersUL = $('<ul>').attr({ class: 'reg-Result-list', id: 'registerul-all' });

    $(storeSpan).append(storeSyncAllChk, spanLabel);
    $(storeDiv).append(storeSpan);
    $(Container).append(storeDiv);
    $(Container).append(registersUL);

    $.each(StoreFullDataObject, function (index, item) {
        CheckIPConnectivity(item, true);
    });
}


function loadAccpacSync(obj) {
    debugger;
    $('.table').show();
    FindResultsSuccess(obj);

    //StoreFullDataObject = obj;
    //if (StoreFullDataObject[0].PostInvoice == false)
    //    $('.PostInvoicetd').html("false");
    //else
    //    $('.PostInvoicetd').html("true");
    //if (StoreFullDataObject[0].PostRefund == false)
    //    $('.PostRefundtd').html("false");
    //else
    //    $('.PostRefundtd').html("true");
    //if (StoreFullDataObject[0].PostExchang == false)
    //    $('.PostExchangetd').html("false");
    //else
    //    $('.PostExchangetd').html("true");
    //if (StoreFullDataObject[0].PostPettyCash == false)
    //    $('.PostPettyCashtd').html("false");
    //else
    //    $('.PostPettyCashtd').html("true");


    //$('.statusvalue').html('Pending');
}

function FindResultsSuccess(returnedValue) {
    debugger
    if (table)
        table.destroy();
    table = $('.result').DataTable({
        "bFilter": false,
        "data": returnedValue,
        "columns": [
            { "data": "PostInvoice" },
            { "data": "PostRefund" },
            { "data": "PostExchange" },
            { "data": "PostPettyCash" },
            { "data": "Status" }
         

        ],
        dom: 'Bfrtip',
        buttons: [

           {
               extend: 'collection',
               text: 'Export',
               buttons: [
                   'copy',
                   'excel',
                   'csv',
                   'pdf',
                   'print'
               ]
           }
        ]
     
        
    });
    $('.table').find('tr').find('td').css('text-align', 'left');
    $('.footer').hide();

}


function CheckIPConnectivity(item, isFirstCheck) {
    var string = new Array();
    string = item.IpAddress.split("\\");
    $.ajax({
        type: "POST",
        url: systemConfig.ApplicationURL_Common + systemConfig.POSSyncServiceURL + "CheckIPConnectivity",
        contentType: "application/json; charset=utf-8",
        data: formatString('{IpAddress:"{0}"}', string[0]),
        dataType: "json",
        success: function (data) {
            if (data != null) {
                var isAvailable = eval("(" + data.d + ")");
                if (isFirstCheck)
                    LoadStores(item, isAvailable);
                else {
                    ChangeStatus(item, isAvailable);
                }
            }
        },
        error: function (e) {
        }
    });
}



function LoadStores(StoreObj, IsAvailable) {
    var string = StoreObj.IpAddress.split("\\");
    var ULContainer = $('#registerul-all');

    //////////////////////////////////////////
    //////append registers data ////////////////////
    /////////////////////////

    var registerLi = $('<li>');
    var registerDiv = $('<div>').attr({ class: 'reg-Result-list-div ', id: 'reg-div-' + StoreObj.value });
    if (IsAvailable)
        $(registerDiv).addClass('connected-reg');
    else
        $(registerDiv).addClass('disconnected-reg');

    var statusDiv = $('<div>').attr({ class: 'regStatus connected-img' });
    var registerSyncCheck = $('<input>').attr({ type: 'checkbox', id: 'chSync-' + StoreObj.value }).change(function () {
        SyncCriteriaArray = new Array();
        $.each(SyncCriteriaArray, function (index, item) {
            if (item.StoreId == StoreObj.value) {
                SyncCriteriaArray.splice(index, 1);
            }
        });
        if ($(this).attr('checked')) {
            var ISExists = false;
            $.each(StoresUserNamePaswwordArray, function (index, item) {
                if (item.StoreId == StoreObj.value) {
                    ISExists = true;
                }
            });

            if (!ISExists) {
                StoreIp = StoreIp + ',' + StoreObj.IpAddress;
                var stoObject = new StoreData();
                stoObject.StoreId = StoreObj.value;
                stoObject.StoreName = StoreObj.StoreName;
                stoObject.IpAddress = StoreObj.IpAddress;
                stoObject.LocationId = StoreObj.LocationId;
                stoObject.DataBaseInstanceName = StoreObj.DataBaseInstanceName;
                stoObject.DataBaseName = StoreObj.DataBaseName;
                stoObject.DataBaseUserName = StoreObj.DataBaseUserName;
                stoObject.DataBasePassword = StoreObj.DataBasePassword;
                StoresUserNamePaswwordArray.push(stoObject);
            }
        }
        else {
            var index = 0;
            while (true) {
                if (StoresUserNamePaswwordArray[index]) {
                    if (StoresUserNamePaswwordArray[index].StoreId == StoreObj.value) {
                        StoresUserNamePaswwordArray.splice(index, 1);
                        StoreIp = StoreIp.replace(StoreObj.IpAddress, '');
                    }
                    else {
                        index++;
                    }
                }
                else
                    break;
            }

        }
    });

    if (IsAvailable)
        $(statusDiv).addClass('connected-img');
    else {
        $(statusDiv).addClass('disconnected-img');
        $(registerSyncCheck).attr({ 'disabled': 'disabled' });
    }
    var RegisterImg = $('<span>').attr({ class: 'register-img', id: 'register-img-' + StoreObj.value }).css('cursor', 'pointer').click(function () {
        $(registerSyncCheck).attr('disabled', 'true');
        $('#connection-div-' + StoreObj.value).show();
        $('#reg-div-' + StoreObj.value).addClass('reg-img-opacity');
        CheckIPConnectivity(StoreObj, false);
    });

    var StoreName = $('<span>').attr({ class: 'reg-name' }).html(StoreObj.label);
    var StoreIpvalue = $('<span>').attr({ class: 'reg-ip' }).html(string[0]);
    var CheckconnectionDiv = $('<div>').attr({ class: 'testConnection', id: 'connection-div-' + StoreObj.value });
    var CheckconnectionImage = $('<img>').attr({ src: "../Images/loading.gif" });
    $(CheckconnectionDiv).append(CheckconnectionImage);
    $(registerDiv).append(statusDiv, registerSyncCheck, RegisterImg, StoreName, StoreIpvalue, CheckconnectionDiv);
    $(registerLi).append(registerDiv);
    if ($('.reg-regName-container').find('#registerul-all').length > 0) {
        $('#registerul-all').append(registerLi);
    }
    else {
        $(ULContainer).append(registerLi);
    }
    LastStore = StoreObj.StoreId;
    StoreCount = StoreCount + 1;
    if (StoreCount == StoreFullDataObject.length) {
        $('.loaderBox,.black_overlay').hide();
        hideLoading();
        StoreCount = 0;
    }
    if (SearchedStores == StoreCount) {
        hideLoading();
        StoreCount = 0;
    }
}

function ChangeStatus(Store, IsAvailable) {
    $('#chSync-' + Store.value).removeAttr('disabled');
    $('#reg-div-' + Store.value).addClass('connected-reg');
    $('#reg-div-' + Store.value).removeClass('disconnected-reg');
    $('#reg-div-' + Store.value).find('.regStatus').removeClass("disconnected-img");
    $('#connection-div-' + Store.value).hide();
    $('#reg-div-' + Store.value).removeClass('reg-img-opacity');
    if (!IsAvailable) {
        $('#chSync-' + Store.value).attr('disabled', 'true');
        $('#reg-div-' + Store.value).removeClass('connected-reg');
        $('#reg-div-' + Store.value).addClass('disconnected-reg');
        $('#reg-div-' + Store.value).find('.regStatus').addClass("disconnected-img");
    }
}

////////////////////////////////////
////////////////Search Registers /////////////////////////
///////////////////////////////////////////////////////////
function SearchStores(keyword) {
    if (keyword != null && keyword != '') {
        var isExists = false;
        LastStore = 0;
        SearchedStores = 0;
        $('#registerul-all').empty();
        showLoading($('.reg-regName-container'));
        $.each(StoreFullDataObject, function (ix, item) {
            var Name = item.StoreName.toUpperCase();
            if (Name.indexOf(keyword.toUpperCase()) > -1 || item.IpAddress.indexOf(keyword) > -1) {
                SearchedStores = SearchedStores + 1;
                CheckIPConnectivity(item, true);
                isExists = true;
            }
        });
        if (!isExists) {
            setNoDataFoundResult();
        }
    } else {
        LastStore = 0;
        SearchedStores = 0;
        GetStores();
    }
}

function setNoDataFoundResult() {
    var Container = $('#registerul-all');
    var storeDiv = $('<div>').attr({ class: 'stores-title' });
    var storeSpan = $('<span>').attr({ class: 'stores-title-span' });
    var spanLabel = $('<span>').html('&nbsp;' + getResource("SMNoDataFound"));
    $(storeSpan).append(spanLabel);
    $(storeDiv).append(storeSpan)
    $(Container).append(storeDiv);
    hideLoading();
}


function OkBtnClick() {
    window.location = 'NewPOSSync.aspx';
    StoresUserNamePaswwordArray = new Array();
    SyncCriteriaArray = new Array();
    NotSyncedMode = false;

}




function GetAccpacData()
{
    debugger
  

    var SyncObjec1t = new SyncStoreData();
    debugger
  
    var IsCheckedItem = false
    $('.syncAdminDataList').find('input:checkbox').each(function (index, item) {
        if ($(item).attr('checked')) {
            IsCheckedItem = true;
        }

    });
    if (!IsCheckedItem) {
        showOkayMsg(getResource("Alert"), getResource("SyncronizationValidationDataMessage"));
        return;
    }
   
   
    $.ajax({
        type: "POST",
        url: systemConfig.ApplicationURL_Common + systemConfig.AccpacURL + "getAccpacData",
        contentType: "application/json; charset=utf-8",

        data: formatString('{enitiyName:"{0}"}', escape(JSON.stringify(SyncObjec1t))),
        dataType: "json",
        success: function (data) {
            if (data != null) {
                var dataget = data.d;
                showStatusMsg(getResource("successfully"));
              

            }
            else {
                showStatusMsg(getResource("Faild"));
                
            }
            },
        
        error: function (e) {
        }
    });
}


function SetAccpacSync() {
debugger
    var postdate = new getPostDate();

    var IsCheckedItempost = false
    $('.syncRegDataTabList').find('input:checkbox').each(function (index, item) {
        if ($(item).attr('checked')) {
            IsCheckedItempost = true;
        }

    });
    if (!IsCheckedItempost) {
        showOkayMsg(getResource("Alert"), getResource("SyncronizationValidationDataMessage"));
        return;
    }

    $.ajax({
        type: "POST",
        url: systemConfig.ApplicationURL_Common + systemConfig.AccpacSyncSchedule + "Add",
        contentType: "application/json; charset=utf-8",

        data: formatString('{id:{0},AccpacSyncScheduleInfo:"{1}"}', 0, escape(JSON.stringify(postdate))),
        
        dataType: "json",
        success: function (data) {
            if (data != null) {
                var dataget = data.d;
                
               

          
           showStatusMsg(getResource("savedsuccessfully"));
                setTimeout(function () { location.reload(); }, 2000);
               

            }
            else {
                showStatusMsg(getResource("FaildToSave"));
            }
        },

        error: function (e) {
        }
    });
}




function SyncData() {
    if (StoreIp == "") {
        showOkayMsg(getResource("Alert"), getResource("noStoresMessage"));
        return;
    }

    var IsChecked = false;
    $('.syncRegDataTab ').find('input:checkbox').each(function (index, item) {
        if ($(item).attr('checked')) {
            IsChecked = true;
        }

    })
    $('.syncAdminDataList').find('input:checkbox').each(function (index, item) {
        if ($(item).attr('checked')) {
            IsChecked = true;
        }

    });
    if (!IsChecked) {
        showOkayMsg(getResource("Alert"), getResource("SyncronizationValidationDataMessage"));
        return;
    }
    var IsExists = false;
    if (SyncCriteriaArray.length <= 0) {
        $.each(StoresUserNamePaswwordArray, function (index, item) {
            var SyncObject = new SyncStoreData();
            SyncObject.StoreId = item.StoreId;
            SyncObject.UserName = item.DataBaseUserName;
            SyncObject.Password = item.DataBasePassword;
            SyncObject.StoreName = item.StoreName;
            SyncObject.DataBaseInstanceName = item.DataBaseInstanceName;
            SyncObject.DataBaseName = item.DataBaseName;
            SyncObject.IpAddress = item.IpAddress;
            SyncCriteriaArray.push(SyncObject);

        });
    }
    else {
        $.each(StoresUserNamePaswwordArray, function (index, item) {
            $.each(SyncCriteriaArray, function (index, SyncItem) {
                if (item.StoreId == SyncItem.StoreId) {
                    IsExists = true;
                }
            });
            if (!IsExists) {
                var SyncObject = new SyncStoreData();
                SyncObject.StoreId = item.StoreId;
                SyncObject.UserName = item.DataBaseUserName;
                SyncObject.Password = item.DataBasePassword;
                SyncObject.StoreName = item.StoreName;
                SyncObject.DataBaseInstanceName = item.DataBaseInstanceName;
                SyncObject.DataBaseName = item.DataBaseName;
                SyncObject.IpAddress = item.IpAddress;
                SyncCriteriaArray.push(SyncObject);
            }

        });
    }
    ShowRegisterProgress(StoresUserNamePaswwordArray);


    SyncFinalData();
    //    $.each(SyncCriteriaArray, function (ix, item) {
    //        
    //        SyncFinalData(item);
    //        
    //        //ShowResult(Result, item);

    //    });



}

function ShowRegisterProgress(StoresUserNamePaswwordArray) {
    $.each(StoresUserNamePaswwordArray, function (index, item) {
        $('.sync-result-table').find('.sync-result-tr-' + item.StoreId).remove();
    })
    var Container = $('.sync-progress-div');
    $.each(SyncCriteriaArray, function (ix, item) {
        var table = $('.sync-result-table');
        var tr = $('<tr>').attr({ class: 'sync-result-tr-' + item.StoreId });
        var StorenameTd = $('<td>').html(item.StoreName);
        var IPAddressTd = $('<td>').html(item.IpAddress);
        var StatusTd = $('<td>').attr({ class: 'status-td' }).html('Pending...');
        $(tr).append(StorenameTd, IPAddressTd, StatusTd);
        $(table).append(tr);
    });
    $('.black_overlay').show();
    $(Container).show();
}

function SyncFinalData() {



    //        
    //        SyncFinalData(item);
    //        
    //        //ShowResult(Result, item);

    //    });

    var storeCounts = SyncCriteriaArray.length - 1;
    $.each(SyncCriteriaArray, function (ix, item) {



        $('.sync-result-tr-' + item.StoreId).find('.status-td').html('In Progress ...');
        var data = '{SyncData:' + JSON.stringify(item) + ',storeIds:' + item.StoreId + '}';
        var url = systemConfig.ApplicationURL_Common + systemConfig.POSSyncServiceURL + "SyncData";
        post(url, data,
       function (data) {
           var result = eval(data);
           ShowResult(result, item);
           if (ix == storeCounts) {
               appendResultControls();
           }

       }, '', '');

    });

}

function appendResultControls() {
    var div = $('.sync-progress-div')
    var aShow = $('<a>').attr({ class: 'show-full-details' }).click(function () {
        //$('.confermitaion-msg-box').show();
        showOkayMsgSync(getResource("Alert"), ResultMessage);
        $('.sync-progress-div').empty().hide();
        ResultMessage = "";
        RegisterCount = 0;
        $('.confirm-button').unbind('click');
        $('.confirm-button').bind('click', function () { OkBtnClick() });
    });
    aShow.html(getResource('ShowFullDetails'))

    var aClose = $('<a>').attr({ class: 'Close' }).click(function () {

        $('.black_overlay').hide();
        $('Div.sync-progress-div').hide();
        ResultMessage = "";
        RegisterCount = 0;
        OkBtnClick();

    });
    aClose.html(getResource('Close'))
    div.append(aShow, aClose);




}


//////////////////////////////////
/////show result////////
///////////////
//need to spacify the resources values in common dictionary
function ShowResult(Result, StoreObj) {
    debugger;
    $('.table').hide();
    jQuery.each(Result, function (i, val) {
        //var status = val.status ? 'success' : 'fail'
        

        
        var table = $('.tbl_result');
        var tr = $('<tr>').attr({ class: 'th-data-table-sync' });
        var StorenameTd = $('<td>').attr({ class: 'th-data-table-sync' }).html(val.storeName);
        
        var data = $('<td>').attr({ class: 'th-data-table-sync' }).html(val.data);
        
        var StatusTd = $('<td>').attr({ class: 'th-data-table-sync' }).html(val.status);
        $(tr).append(StorenameTd, data, StatusTd);
        $(table).append(tr);


        if (val.status.toLowerCase().indexOf("fail") > -1) {

            $('.sync-result-tr-' + StoreObj.StoreId).find('.status-td').html('Failed');

        }
        else {
            $('.sync-result-tr-' + StoreObj.StoreId).find('.status-td').html('Success');
        }

    });


   



    //$('.sync-progress-div').append('<a class="show-full-details"><span resourcekey="ShowFullDetails"></span>');




    //$('.sync-progress-div').show();

    //    var IsError = false;
    //        var stringarray = new Array();
    //        stringarray = item.split(',');
    //        var ErrorMessage = "";
    //        if (stringarray[1] == 1) {
    //            ErrorMessage = getResource("SyncronizationConnectionError");
    //            $('.sync-result-tr-' + StoreObj.StoreId).find('.status-td').html('Faild');
    //            $('.sync-reg-status-' + StoreObj.StoreId).html('Faild');
    //        }
    //        if (stringarray[1] == 2) {
    //            ErrorMessage = getResource("SyncronizationError");
    //            $('.sync-result-tr-' + StoreObj.StoreId).find('.status-td').html('Faild');
    //            $('.sync-reg-status-' + StoreObj.StoreId).html('Faild');
    //        }
    //        if (stringarray[1] == 3) {
    //            ErrorMessage = getResource("DataUpdatedMessage");

    //        }
    //        if (stringarray[1] == 0) {
    //            $('.sync-result-tr-' + StoreObj.StoreId).find('.status-td').html('Success');
    //            $('.sync-reg-status-' + StoreObj.StoreId).html('Success');
    //            ErrorMessage = "Success";
    //        }

    //        if (stringarray[1] != 0)
    //            IsError = true;

    //    for (i = 0; i < Result.length;i++ )
    //        {
    //            ResultMessage += Result[i].storeId + " " + Result[i].data + " " + Result[i].status ? "true" : "false" + '<br\>';
    //        }

    // ResultMessage += stringarray[0] + " ---  " + ErrorMessage + '<br\>';
    //    });
}



function showOkayMsgSync(title) {
    var pnl = $(".white_content_sync:first").clone();
    pnl.find('.cmb-title span').html(title);
    pnl.find('.button-cancel').hide();
    pnl.find('.button-cancel').hide();
    $(".black_overlay").hide();
    $(".white_content").after(pnl);
    pnl.show();
}
