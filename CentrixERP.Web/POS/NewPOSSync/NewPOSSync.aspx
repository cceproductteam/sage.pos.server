﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewPOSSync.aspx.cs" MasterPageFile="~/Common/MasterPage/ERPSite.master"
    Inherits="Centrix.POS.Server.Web.POS.NewPOSSync.NewPOSSync" %>

<asp:Content runat="server" ContentPlaceHolderID="PageHeader" ID="PageHeader">
    <script src="JS/NewPOSSync.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <link href="../CSS/POSMain.css?v=<%=Centrix_Version %>" rel="stylesheet" type="text/css" />
    <link href="CSS/NewPOSSync_<%=Lang %>.css?v=<%=Centrix_Version %>" rel="stylesheet"
        type="text/css" />
     <script src="../../Common/JScript/DataTables/JS/jquery.dataTables.min.js"></script>
 
    <script src="../../Common/JScript/DataTables/JS/jquery.dataTables.js"></script>
    <script src="../../Common/JScript/DataTables/JS/dataTables.buttons.min.js"></script>

    <script src="../../Common/JScript/DataTables/JS/buttons.flash.min.js"></script>


    <script src="../../Common/JScript/DataTables/JS/jszip.min.js"></script>
    <script src="../../Common/JScript/DataTables/JS/vfs_fonts.js"></script>
    <script src="../../Common/JScript/DataTables/JS/vfs_fonts.js"></script>

    <script src="../../Common/JScript/DataTables/JS/buttons.html5.min.js"></script>
    <script src="../../Common/JScript/DataTables/JS/buttons.print.min.js"></script>


    <script src="../../Common/JScript/DataTables/JS/dataTables.material.min.js"></script>
    <link href="../../Common/JScript/DataTables/CSS/dataTables.material.css" rel="stylesheet" />
    <link href="../../Common/JScript/DataTables/CSS/material.min.css" rel="stylesheet" />
    <link href="../../Common/JScript/DataTables/CSS/buttons.dataTables.min.css" rel="stylesheet" />
       <style>
    .THead-Th
    {
            color: black !important;
    text-align: left !important;
    }
     .dt-buttons {
        float:right !important;
        }
    </style>

</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="PageBody" ID="PageBody">
    <div class="white-container" style="height: 560px;">
        <div class="white-container-data" style="height: 560px;">
            <div class="sync-progress-div">
                <table class="sync-result-table" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <th>
                            <span resourcekey="Store"></span>
                        </th>
                        <th>
                            <span resourcekey="IpAddress"></span>
                        </th>
                        <th>
                            <span resourcekey="Status"></span>
                        </th>
                    </tr>
                </table>
                <%-- <a class="show-full-details"><span resourcekey="ShowFullDetails"></span>--%>
                <%--</a><a class="Close">
                    <span resourcekey="Close"></span></a>--%>
            </div>

            <div class="loaderBox displayNone">
                <span resourcekey="loading"></span>
                <img src="../Images/loading.gif" />
            </div>
            <div class="StoreSync-info-container POSSync-Fieldset displayNone" style="height: 675px; display: none">
                <div class="Stores-list-container" style="height: 660px; display: none">
                    <div class="register-filter-search">
                        <div class="registerSearch">
                            <input type="text" class="searchBox" />
                            <a href="#" class="searchIcon"></a>
                        </div>
                    </div>
                    <div class="reg-regName-container" style="height: 500px;">
                    </div>
                </div>
                <div class="sync-container">
                    <div class="sync-tabs">
                        <ul>
                            <li class="syncRegDataTab SelectedTab">
                                <input type="checkbox" class="syncRegData" /><span resourcekey="PostToAccpac"></span></li>
                            <li class="syncAdminData">
                                <input type="checkbox" class="ChSyncAdminData" /><span resourcekey="GetToAccpac"></span></li>
                        </ul>
                    </div>
                    <div class="sync-action-Div">
                        <ul>
                            <li class="Sync-pnl viewedit-display-block" style="display: none"><span class="ActionSync-link"></span><a
                                class="ActionSync-link"><span resourcekey="Sync"></span></a></li>

                        </ul>
                    </div>
                    <div class="control-content-sync-list syncRegDataTabList">
                        <ul>

                            <li class="sync-row2">
                                <input type="checkbox" class="SyncTransactionData" /><span resourcekey="PostInvoice"></span>
                                <input type="checkbox" class="CloseRegisterData" /><span resourcekey="PostRefund"></span>
                                <input type="checkbox" class="PostExchange" /><span resourcekey="PostExchange"></span>
                                <input type="checkbox" class="PostPettyCash" /><span resourcekey="PostPettyCash"></span>

                                <input type="button" class="buttonin" id="buttonin" value="Add">
                            </li>



                        </ul>
                    </div>
                    <div class="control-content-sync-list syncAdminDataList">
                          <ul>
                             <li class="sync-row1">
                        <table cellpadding ="4" cellspacing ="4"> 
                          
                            <tr>
                                <th> <input type="checkbox" class="BankAccount" /></th>
                                <td><span resourcekey="Getbankaccount"></span></td>
                                <th><input type="checkbox" class="Category" /></th>
                                <td><span resourcekey="Getcategory"></span></td>
                                <th> <input type="checkbox" class="Currency" /></th>
                                <td><span resourcekey="Getcurrency"></span></td>
                                <th> <input type="checkbox" class="CurrencyRates" /></th>
                                <td><span resourcekey="Getcurrencyrates"></span></td>
                                </tr>
                            <tr>
                                <th> <input type="checkbox" class="GLAccounts" /></th>
                                <td><span resourcekey="Getglaccounts"></span></td>
                                <th>   <input type="checkbox" class="Customer" /></th>
                                <td><span resourcekey="Getcustomer"></span></td>
                                <th>   <input type="checkbox" class="Location" /></th>
                                <td><span resourcekey="Getlocation"></span></td>
                                <th> <input type="checkbox" class="PriceList" /></th>
                                <td><span resourcekey="Getpricelist"></span></td>
                            </tr>
                            <tr>

                                <th><input type="checkbox" class="TaxAuthorities" /></th>
                                <td><span resourcekey="Gettaxauthorities"></span></td>
                                <th> <input type="checkbox" class="taxMatrix" /></th>
                                <td><span resourcekey="GettaxMatrix"></span></td>
                                <th>    <input type="checkbox" class="ItemCard" /></th>
                                <td><span resourcekey="Getitemcard"></span></td>
                                <th> <input type="checkbox" class="ItemPriceList" /></th>
                                <td><span resourcekey="GetItemPriceList"></span></td>
                            </tr>
                            <tr>
                                <th> <input type="checkbox" class="ItemStock" /></th>
                                <td><span resourcekey="Getitemstock"></span></td>
                                <th><input type="checkbox" class="ItemTax" /></th>
                                <td><span resourcekey="Getitemtax"></span></td>
                                <th> <input type="checkbox" class="TransferData" /></th>
                                <td><span resourcekey="Gettransferdata"></span></td>

                            </tr>

                                 </li>
                                </ul>

                        </table>

                           <div class="ActionGet syncBtn">
                                <input type="button" class="buttonacc" id="buttonS" value="Star Sync">
                            </div>
                      <%--  <ul>
                            <li class="sync-row1">
                                <input type="checkbox" class="BankAccount" /><span resourcekey="Getbankaccount"></span>--%>

                              <%--  <input type="checkbox" class="Category" /><span resourcekey="Getcategory"></span>--%>
                         <%--   <input type="checkbox" class="Currency" /><span resourcekey="Getcurrency"></span>--%>
                 <%--                  <span resourcekey="Getcurrencyrates"></span>
                               <%--  <input type="checkbox" class="GLAccounts" /><span resourcekey="Getglaccounts"></span>--%>
                                 <%-- <input type="checkbox" class="Customer" /><span resourcekey="Getcustomer"></span>
                                 <input type="checkbox" class="Location" /><span resourcekey="Getlocation"></span>
                                   <input type="checkbox" class="PriceList" /><span resourcekey="Getpricelist"></span>--%>
                            <%--     <input type="checkbox" class="TaxAuthorities" /><span resourcekey="Gettaxauthorities"></span>
                                      <input type="checkbox" class="taxMatrix" /><span resourcekey="GettaxMatrix"></span>--%>
                                   <%--       <input type="checkbox" class="ItemCard" /><span resourcekey="Getitemcard"></span>
                                              <input type="checkbox" class="ItemPriceList" /><span resourcekey="GetItemPriceList"></span>--%>
                  <%--                             <input type="checkbox" class="ItemStock" /><span resourcekey="Getitemstock"></span>
                                                <input type="checkbox" class="ItemTax" /><span resourcekey="Getitemtax"></span>
                                              <input type="checkbox" class="TransferData" /><span resourcekey="Gettransferdata"></span>--%>
                       <%--     </li>
                                     
                            </ul>--%>
 
          
      

                         
                    </div>
                    <%--raedddd--%>


                    <div class='table' style="display: none;">
                        <table class="display result mdl-data-table dataTable" cellspacing="0" width="100%">
                            <thead class="THeadGridTable">
                                <tr>
                                    <th class="THead-Th">
                                        <span resourcekey="PostInvoice"></span>
                                    </th>

                                    <th class="THead-Th">
                                        <span resourcekey="PostRefund"></span>
                                    </th>

                                    <th class="THead-Th">
                                        <span resourcekey="PostExchange"></span>
                                    </th>

                                    <th class="THead-Th">
                                        <span resourcekey="PostPettyCash"></span>
                                    </th>
                                    <th class="THead-Th">
                                        <span resourcekey="Status"></span>
                                    </th>



                                </tr>
                            </thead>
                            
                         


                        </table>
                    </div>




                    <%--end--%>
                </div>
            </div>
        </div>
    </div>

    <div class="white_content_sync">
        <div class="confermitaion-msg-box_sync">
            <div class="cmb-title">
                <span>Delete record</span>
            </div>
            <div class="cmb-desc">
                <span>
                    <table width="100%" border="0" cellpadding="0" cellspacing="1" class="details-grid">
                        <thead class="THeadGridTable">
                            <tr>
                                <th class="th-data-table-sync">Store
                                </th>
                                <th class="th-data-table-sync">data
                                </th>
                                <th class="th-data-table-sync">status
                                </th>
                            </tr>
                        </thead>
                    </table>
                    <table width="100%" border="0" cellpadding="0" cellspacing="1" class="details-grid  tbl_result">
                        <thead class="THeadGridTable">
                            <tr>
                                <th class="th-data-table-sync">Store
                                </th>
                                <th class="th-data-table-sync">data
                                </th>
                                <th class="th-data-table-sync">status
                                </th>
                            </tr>
                        </thead>
                    </table>
                </span>
            </div>
            <div class="cmb-controls">
                <input type="button" value="Ok" class="button2 confirm-button">
            </div>
        </div>
    </div>
</asp:Content>
