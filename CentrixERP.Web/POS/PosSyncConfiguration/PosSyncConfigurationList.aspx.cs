using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SagePOS.Common.Web;
using SagePOS.Manger.Common.Business.Entity;
using E = SagePOS.Server.Business.Entity;
using IM = SagePOS.Server.Business.IManager;
using SagePOS.Server.Configuration;


namespace Centrix.POS.Server.Web.PosSyncConfiguration
{
    public partial class PosSyncConfigurationList : BasePageLoggedIn
    {
        public PosSyncConfigurationList()
            : base(Enums_S3.Permissions.SystemModules.None) { }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}