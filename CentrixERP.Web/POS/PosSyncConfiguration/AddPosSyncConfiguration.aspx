<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddPosSyncConfiguration.aspx.cs" MasterPageFile="~/Common/MasterPage/Site.master"
    Inherits="Centrix.POS.Server.Web.Server.PosSyncConfiguration.AddPosSyncConfiguration" %>
    
<asp:Content runat="server" ContentPlaceHolderID="PageHeader" ID="PageHeader">
    <script type="text/javascript" src="JS/PosSyncConfiguration.js?v=<%=Centrix_Version %>"></script>
    <script src="../../Common/JScript/CommonTabs.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <script type="text/javascript">
        addPosSyncConfigurationMode = true;
           
    </script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="PageBody" ID="PageBody">
    <div id='content' class="PosSyncConfiguration-info">
        <div class="Add-new-form">
            <div class="Add-title">
                <span resourcekey="PosSyncConfigurationDetails"> </span>
                    <div class="Action-div">
                        <ul>
                            <li><span class="ActionSave-link save-tab-lnk"></span><a class="ActionSave-link save-tab-lnk">
                                <span resourcekey="Save"> </span> </a> </li>
                            <li><span class="ActionCancel-link cancel-tab-lnk"></span><a class="ActionCancel-link cancel-tab-lnk">
                                <span resourcekey="Cancel"> </span> </a> </li>
                        </ul>
                    </div>
            </div>
            <table border="0" cellpadding="0" cellspacing="0" class="Add-new-Table PosSyncConfiguration-info">
            			<tr>
				<th>
					<span class="label-title" resourcekey="PosSyncTime1"></span>
				</th>
				<td>
					
                        <div class="">
                            <select class="PosSyncTime1  lst">
                            </select>
                        </div>
				</td>
				<th>
					<span class="label-title" resourcekey="PosSyncTime2"></span>
				</th>
				<td>
					
                        <div class="">
                            <select class="PosSyncTime2  lst">
                            </select>
                        </div>
				</td>
				<th>
					<span class="label-title" resourcekey="PosSyncTime3"></span>
				</th>
				<td>
					
                        <div class="">
                            <select class="PosSyncTime3  lst">
                            </select>
                        </div>
				</td>
			</tr>
			<tr>
				<th>
					<span class="label-title" resourcekey="PosSyncTime4"></span>
				</th>
				<td>
					
                        <div class="">
                            <select class="PosSyncTime4  lst">
                            </select>
                        </div>
				</td>
				<th>
					<span class="label-title" resourcekey="PosSyncTime5"></span>
				</th>
				<td>
					
                        <div class="">
                            <select class="PosSyncTime5  lst">
                            </select>
                        </div>
				</td>
				<th>
					<span class="label-title" resourcekey="PosDataToSync"></span>
				</th>
				<td>
					<input class="cx-control PosDataToSync  txt   integer  " maxlength="" minlength="3"
                            type="text"  />
				</td>
			</tr>
			<tr>
				<th>
					<span class="label-title" resourcekey="SyncDataFromAccpac"></span>
				</th>
				<td>
					<input type="checkbox" class="cx-control SyncDataFromAccpac chk " />
                       
				</td>
				<th>
					<span class="label-title" resourcekey="AccpacSyncTime1"></span>
				</th>
				<td>
					
                        <div class="">
                            <select class="AccpacSyncTime1  lst">
                            </select>
                        </div>
				</td>
				<th>
					<span class="label-title" resourcekey="AccpacSyncTime2"></span>
				</th>
				<td>
					
                        <div class="">
                            <select class="AccpacSyncTime2  lst">
                            </select>
                        </div>
				</td>
			</tr>
			<tr>
				<th>
					<span class="label-title" resourcekey="AccpacSyncTime3"></span>
				</th>
				<td>
					
                        <div class="">
                            <select class="AccpacSyncTime3  lst">
                            </select>
                        </div>
				</td>
				<th>
					<span class="label-title" resourcekey="AccpacSyncTime4"></span>
				</th>
				<td>
					
                        <div class="">
                            <select class="AccpacSyncTime4  lst">
                            </select>
                        </div>
				</td>
				<th>
					<span class="label-title" resourcekey="AccpacSyncTime5"></span>
				</th>
				<td>
					
                        <div class="">
                            <select class="AccpacSyncTime5  lst">
                            </select>
                        </div>
				</td>
			</tr>
			<tr>
				<th>
					<span class="label-title" resourcekey="AccpacDataToSyncIds"></span>
				</th>
				<td>
					<input class="cx-control AccpacDataToSyncIds  txt    string " maxlength="100" minlength="3"
                            type="text"  />
				</td>
				<th>
					<span class="label-title" resourcekey="AccpacDataToSync"></span>
				</th>
				<td>
					<textarea class="cx-control AccpacDataToSync  txt " maxlength="500" minlength="3"></textarea>
				</td>
				<th>
					<span class="label-title" resourcekey="Status"></span>
				</th>
				<td>
					<input class="cx-control Status  txt   integer  " maxlength="" minlength="3"
                            type="text"  />
				</td>
			</tr>
            </table>
        </div>
        <div class="five-px">
        </div>
    </div>
</asp:Content>
