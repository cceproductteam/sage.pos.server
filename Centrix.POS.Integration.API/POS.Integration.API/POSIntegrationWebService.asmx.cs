using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using SF.Framework;
using E = Centrix.Inventory.Business.Entity;
using Centrix.Inventory.Business.IManager;
using System.Reflection;
using CentrixERP.Configuration;
using SF.FrameworkEntity;
using CX.Framework;
using Centrix.Inventory.Business.Entity;
using CentrixERP.AdministrativeSettings.Business.Entity;
using CentrixERP.AdministrativeSettings.Business.IManager;
using CentrixERP.AccountsReceivable.Business.Entity;
using CentrixERP.AccountsReceivable.Business.IManager;
using System.Web.Script.Serialization;
using CentrixERP.Common.API;

namespace CentrixERP.POS.Integration.API
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    [EntityAttribute(Enums_S3.Entity.None)]
    public class POSIntegrationWebService 
    {
        IIcItemCardManager iIcItemCardManager = null;
        IIcPriceListGroupManager iIcPriceListGroupManager = null;
        IIcPriceListGroupItemManager iIcPriceListGroupItemManager = null;
        IIcItemTaxManager iIcItemTaxManager = null;
        IIcLocationManager iIcLocationManager = null;
        IIcItemLocationCostingManager iIcItemLocationCostingManager = null;
        ICurrencyManager iCurrencyManager = null;
        ICurrencyRateDetailsManager iCurrencyRateDetailsManager = null;
        ICustomerManager iCustomerManager = null;
        public JavaScriptSerializer javaScriptSerializer;


        public POSIntegrationWebService()
        {
            iIcItemCardManager = (IIcItemCardManager)SF.Framework.IoC.Instance.Resolve(typeof(IIcItemCardManager));
            iIcPriceListGroupManager = (IIcPriceListGroupManager)SF.Framework.IoC.Instance.Resolve(typeof(IIcPriceListGroupManager));
            iIcPriceListGroupItemManager = (IIcPriceListGroupItemManager)SF.Framework.IoC.Instance.Resolve(typeof(IIcPriceListGroupItemManager));
            iIcItemTaxManager = (IIcItemTaxManager)SF.Framework.IoC.Instance.Resolve(typeof(IIcItemTaxManager));
            iIcLocationManager = (IIcLocationManager)SF.Framework.IoC.Instance.Resolve(typeof(IIcLocationManager));
            iIcItemLocationCostingManager = (IIcItemLocationCostingManager)SF.Framework.IoC.Instance.Resolve(typeof(IIcItemLocationCostingManager));
            iCurrencyManager = (ICurrencyManager)SF.Framework.IoC.Instance.Resolve(typeof(ICurrencyManager));
            iCurrencyRateDetailsManager = (ICurrencyRateDetailsManager)SF.Framework.IoC.Instance.Resolve(typeof(ICurrencyRateDetailsManager));
            iCustomerManager = (ICustomerManager)SF.Framework.IoC.Instance.Resolve(typeof(ICustomerManager));
            javaScriptSerializer = new JavaScriptSerializer();
        }
        
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public List<E.POSIntegrationItemCardLite> POSIntegrationItemCardFindAll(DateTime LastSyncDate)
        {
            
            List<E.POSIntegrationItemCardLite> IcItemCardList = iIcItemCardManager.POSIntegrationItemCardFindAll(LastSyncDate,0);
            return IcItemCardList;

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public List<E.POSIntegrationPriceListGroup> POSIntegrationPriceListGroupFindAll(DateTime LastSyncDate)
        {
            List<E.POSIntegrationPriceListGroup> List = iIcPriceListGroupManager.POSIntegrationPriceListGroupFindAll(LastSyncDate);
            return List;

        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public List<E.POSIntegrationItemPriceList> POSIntegrationItemPriceListFindAll(DateTime LastSyncDate)
        {
            List<E.POSIntegrationItemPriceList> List = iIcPriceListGroupItemManager.POSIntegrationItemPriceListFindAll(LastSyncDate,1);
            return List;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public List<E.POSIntegrationItemTax> POSIntegrationItemTaxFindAll(DateTime LastSyncDate)
        {
            List<E.POSIntegrationItemTax> List = iIcItemTaxManager.POSIntegrationItemTaxFindAll(LastSyncDate,1);
            return List;
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public List<E.POSIntegrationLocation> POSIntegrationLocationFindAll(DateTime LastSyncDate)
        {
            List<E.POSIntegrationLocation> List = iIcLocationManager.POSIntegrationLocationFindAll(LastSyncDate);
            return List;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public List<E.POSIntegrationItemLocationCosting> POSIntegrationStockFindAll(DateTime LastSyncDate)
        {
            List<E.POSIntegrationItemLocationCosting> List = iIcItemLocationCostingManager.POSIntegrationItemLocationCostingFindAll(LastSyncDate,0);
            return List;
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public List<POSIntegrationCurrency> POSIntegrationCurrencyFindAll(DateTime LastSyncDate)
        {
            List<POSIntegrationCurrency> List = iCurrencyManager.POSIntegrationCurrencyFindAll(LastSyncDate);
            return List;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public List<POSIntegrationCurrencyRate> POSIntegrationCurrencyRateFindAll(DateTime LastSyncDate)
        {
            List<POSIntegrationCurrencyRate> List = iCurrencyRateDetailsManager.POSIntegrationCurrencyRateFindAll(LastSyncDate);
            return List;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public List<POSIntegrationCustomer> POSIntegrationCustomerFindAll(DateTime LastSyncDate)
        {
            List<POSIntegrationCustomer> List = iCustomerManager.POSIntegrationCustomerFindAll(LastSyncDate);
            return List;
        }

       

    }
}
