﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Centrix.Inventory.Business.Entity;
using CentrixERP.AdministrativeSettings.Business.Entity;
using CentrixERP.AdministrativeSettings.Business.IManager;
using CentrixERP.AccountsReceivable.Business.Entity;
using CentrixERP.AccountsReceivable.Business.IManager;
using System.Web.Script.Serialization;
using CentrixERP.Common.API;
using CentrixERP.Configuration;
using Centrix.Inventory.Business.Entity;
using Centrix.Inventory.Business.IManager;
using System.Web.Script.Services;
using CentrixERP.POS.Integration.API.Classes;
using CentrixERP.API;
using Centrix.Inventory.API;
using CentrixERP.API.Common.BaseClasses;
using CX.Framework;
using Centrix.POS.Standalone.Client.Business.IManager;
using Centrix.POS.Standalone.Client.Business.Entity;


namespace CentrixERP.POS.Integration.API
{
    /// <summary>
    /// Summary description for WritePOSTransactionsWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    [EntityAttribute(Enums_S3.Entity.None)]
    public class WritePOSTransactionsWebService : WebServiceBaseClass
    {
        IIcShipmentManager iIcShipmentManager = null;
        IIcCommonQueriesManager iIcCommonQueries = null;
        IIcDocumentNumbersManager iIcDocumentNumbersManager = null;
        IBusinessManagerBase<IcItemCardSerial, int> iIcItemCardSerialManager = null;
        IBusinessManagerBase<IcItemCardSerialTransaction, int> iIcItemCardSerialTransactionManager = null;
        IIcLocationManager iIcLocationManager = null;
        IIcItemCardManager iIcItemCardManager = null;
        IIcAccountingIntegrationManager iIcAccountingIntegrationManager = null;
        IArInvoiceManager iArInvoiceManager = null;
        IInvoiceManager iInvoiceManager = null;
        IInvoiceItemManager iInvoiceItemManager = null;
        ICloseRegisterManager iCloseRegisterManager = null;
        IIcReceiptManager iIcReceiptManager = null;
        IIcItemTaxManager iIcItemTaxManager = null;
        ICustomerManager iCustomerManager = null;
        IInvoiceBatchListManager iInvoiceBatchListManager = null;
        IArInvoiceDetailsManager iArInvoiceDetailsManager = null;
 

        public WritePOSTransactionsWebService()
        {

            iIcShipmentManager = (IIcShipmentManager)CX.Framework.IoC.Instance.Resolve(typeof(IIcShipmentManager));
            iIcCommonQueries = (IIcCommonQueriesManager)CX.Framework.IoC.Instance.Resolve(typeof(IIcCommonQueriesManager));
            iIcDocumentNumbersManager = (IIcDocumentNumbersManager)SF.Framework.IoC.Instance.Resolve(typeof(IIcDocumentNumbersManager));
            iIcItemCardSerialManager = (IBusinessManagerBase<IcItemCardSerial, int>)CX.Framework.IoC.Instance.Resolve(typeof(IBusinessManagerBase<IcItemCardSerial, int>));
            iIcItemCardSerialTransactionManager = (IBusinessManagerBase<IcItemCardSerialTransaction, int>)CX.Framework.IoC.Instance.Resolve(typeof(IBusinessManagerBase<IcItemCardSerialTransaction, int>));
            iIcLocationManager = (IIcLocationManager)SF.Framework.IoC.Instance.Resolve(typeof(IIcLocationManager));
            iIcItemCardManager = (IIcItemCardManager)SF.Framework.IoC.Instance.Resolve(typeof(IIcItemCardManager));
            iIcAccountingIntegrationManager = (IIcAccountingIntegrationManager)CX.Framework.IoC.Instance.Resolve(typeof(IIcAccountingIntegrationManager));
            iArInvoiceManager = (IArInvoiceManager)SF.Framework.IoC.Instance.Resolve(typeof(IArInvoiceManager));
            iInvoiceManager = (IInvoiceManager)SF.Framework.IoC.Instance.Resolve(typeof(IInvoiceManager));
            iInvoiceItemManager = (IInvoiceItemManager)SF.Framework.IoC.Instance.Resolve(typeof(IInvoiceItemManager));
            iCloseRegisterManager = (ICloseRegisterManager)SF.Framework.IoC.Instance.Resolve(typeof(ICloseRegisterManager));
            iIcReceiptManager = (IIcReceiptManager)CX.Framework.IoC.Instance.Resolve(typeof(IIcReceiptManager));
            iIcItemTaxManager = (IIcItemTaxManager)SF.Framework.IoC.Instance.Resolve(typeof(IIcItemTaxManager));
            iCustomerManager = (ICustomerManager)SF.Framework.IoC.Instance.Resolve(typeof(ICustomerManager));
            iInvoiceBatchListManager = (IInvoiceBatchListManager)SF.Framework.IoC.Instance.Resolve(typeof(IInvoiceBatchListManager));
            iArInvoiceDetailsManager = (IArInvoiceDetailsManager)SF.Framework.IoC.Instance.Resolve(typeof(IArInvoiceDetailsManager));
            
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string PostPOSInvoices(string invType)
        {
            var validShipmentQty = true;
            List<InvoiceIntegration> invoiceList = new List<InvoiceIntegration>();
            try
            {
                invoiceList = iInvoiceManager.InvoiceIntegrationFindAll(new InvoiceCriteria() { Status = 1, InvoiceType = invType });

            }
            catch (Exception ex)
            {

                throw ex;
            }
            

            if (invoiceList == null || invoiceList.Count() == 0) return this.AttachStatusCode(null, 2, null);
            List<InvoiceItemIntegration> invoiceItemList = new List<InvoiceItemIntegration>();
            List<ShipmentReturnObject> result = new List<ShipmentReturnObject>();
            List<ShipmentReturnObject> result2 = new List<ShipmentReturnObject>();
            if (invoiceList == null) invoiceList = new List<InvoiceIntegration>();
            ShipmentReturnObject obj = new ShipmentReturnObject();
            ShipmentReturnObject obj2 = new ShipmentReturnObject();
            //  ReceiptReturnObject obj2 = new ReceiptReturnObject();

            bool parentNotPosted = false;


            List<InvoiceItemIntegration> invoiceItemListExchanged = new List<InvoiceItemIntegration>();
            List<InvoiceItemIntegration> invoiceItemListNew = new List<InvoiceItemIntegration>();



            //Go check if all refund invoices have been posted before proceeding

            if (invType == "5") //Refund
                parentNotPosted = Convert.ToBoolean(iInvoiceManager.CheckParentShipmentValidity());

            if (invType == "5" && parentNotPosted)
                return this.AttachStatusCode(null, 5, null);  //If parent shipment not posted , Don't proceed posting shipment return !

            foreach (InvoiceIntegration item in invoiceList)
            {

                if (item.InvoiceStatus == 10)
                {
                    invoiceItemListExchanged = iInvoiceItemManager.InvoiceItemIntegrationFindAll(new InvoiceItemCriteria() { InvoiceUniqueNumber = item.InvoiceUniqueNumber, LocationId = item.LocationId, IsExchanged = true });
                    invoiceItemListNew = iInvoiceItemManager.InvoiceItemIntegrationFindAll(new InvoiceItemCriteria() { InvoiceUniqueNumber = item.InvoiceUniqueNumber, LocationId = item.LocationId, IsExchanged = false });


                    // if (CheckQuantityValidation(item, invoiceItemListExchanged) && CheckQuantityValidation(item, invoiceItemListNew))
                    if (CheckQuantityValidation(item, invoiceItemListNew))
                    {
                        validShipmentQty = true;
                        iIcItemCardManager.PostPOSShipment(UserId, item.InvoiceStatus, item.InvoiceId);
                       // obj2 = AddReceiptSingle(item, invoiceItemListExchanged, item.InvoiceStatus);
                       // obj = AddShipmentSingle(item, invoiceItemListNew, item.InvoiceStatus);


                        //result.Add(obj2);
                    }
                    else
                    {
                        validShipmentQty = false;
                        result.Add(new ShipmentReturnObject() { ShipmentId = -1, ShipmentOrReceipt = -1, status = false, TransReff = item.InvoiceNumber });
                    }


                }

                else
                {
                    invoiceItemList = iInvoiceItemManager.InvoiceItemIntegrationFindAll(new InvoiceItemCriteria() { InvoiceUniqueNumber = item.InvoiceUniqueNumber, LocationId = item.LocationId });

                    if (item.InvoiceStatus == 5)
                        validShipmentQty = true;
                    else
                        validShipmentQty = CheckQuantityValidation(item, invoiceItemList);

                    IcShipment Shipment = new Centrix.Inventory.Business.Entity.IcShipment();

                    if (validShipmentQty)
                        iIcItemCardManager.PostPOSShipment(UserId, item.InvoiceStatus, item.InvoiceId);
                    //if (invType == 7)
                    //{
                    //    obj = AddShipmentSingle(item, invoiceItemList, invType);
                    //}
                  
                    else
                        result.Add(new ShipmentReturnObject() { ShipmentId = -1, ShipmentOrReceipt = -1, status = false, TransReff = item.InvoiceNumber });
                }

                if (validShipmentQty)
                    result.Add(obj);
                try
                {
                    if (obj.ShipmentId > 0)
                        iInvoiceManager.UpdateInvoiceTransNumber(item.InvoiceId, obj.ShipmentId, obj.TransReff);
                }
                catch (Exception)
                {

                }

            }


            return this.AttachStatusCode(result, 1, null);

        }


        private string AddShipmentBulk(List<POSShipment> shipmentList, List<POSShipmentDetails> details)
        {
            IcShipmentWebService shipmentWebService = new IcShipmentWebService();

            //IcShipment myIcShipment = this.javaScriptSerializer.Deserialize<E.IcShipment>(this.unEscape((IcShipmentInfo)));

            //  var automaticInvoiceCreated = false;
            //  var createAutomaticInvoiceEnabled = myIcShipment.CreateAutomaticARInvoice;
            //  var automaticInvoiceRegionEntered = false;
            //  var savedShipmenttId = 0;

            //  var result = this.CheckIfValidYearPeriodValue(myIcShipment.PostingDate.Value.Year, myIcShipment.PostingDate.Value.Month);
            //  if (result != CentrixERP.Configuration.DataTypeContectEnumerations.BatchPostingErrors.NoError)
            //  {
            //      return this.AttachStatusCode(null, (int)result, null);
            //  }
            //  else
            //  {
            //      UpdateShipmentYearPeriodValue(ref myIcShipment);
            //  }

            //  //List<ValidationError> errors = iIcShipmentManager.Validate(myIcShipment);
            //  //if (errors != null && errors.Count > 0)
            //  //{
            //  //    return this.AttachStatusCode(errors, 0, "validation");
            //  //}

            //  List<Object> optionalFieldErrors = new List<object>();
            //  List<object> optionalErrors = new List<object>();
            //  optionalErrors = ValidateOptionalField(myIcShipment.OptionalFieldEntityList);
            //  if (optionalErrors.Count > 0)
            //      optionalFieldErrors.Add(optionalErrors);
            //  foreach (var shipmentDetail in myIcShipment.ShipmentDetails)
            //  {
            //      optionalErrors = ValidateOptionalField(shipmentDetail.OptionalFieldEntityList);
            //      if (optionalErrors.Count > 0)
            //          optionalFieldErrors.Add(optionalErrors);
            //  }
            //  if (optionalFieldErrors.Count > 0)
            //      return this.AttachStatusCode(optionalFieldErrors, (int)Enums_S3.StatusCode.Codes.OptioanlFieldDetailsError, "OptionalFieldErrors");

            //  try
            //  {
            //      PrepareShipmentForSaving(add, id, myIcShipment, postingAction);

            //      IcShipmentCriteria shipmentCriteria = new IcShipmentCriteria();
            //      shipmentCriteria.Keyword = myIcShipment.ShipmentNumber;
            //      shipmentCriteria.pageNumber = 1;
            //      shipmentCriteria.resultCount = 10;
            //      if (add)
            //      {
            //          List<IcShipmentLite> duplicateShipments = iIcShipmentManager.FindAllLight<IcShipmentLite>(shipmentCriteria);//.ToList();
            //          if (duplicateShipments != null)
            //          {
            //              if (duplicateShipments.Count > 0)
            //              { throw new SF.Framework.Exceptions.RecordExsitsException("Duplicate Shipment Number"); }
            //          }
            //      }

            //      myIcShipment.Status = false;
            //     // iIcShipmentManager.BeginTransaction();
            //      iIcShipmentManager.Save(myIcShipment);
            //      if (add)
            //      {

            //          UpdateDocumentNextNumber((myIcShipment.ShipmentTypeId == (int)Enums_S3.InventoryControl.ShipmentType.Return) ? (int)Enums_S3.InventoryControl.DocumentNumbers.ShipmentReturnNumber : (int)Enums_S3.InventoryControl.DocumentNumbers.ShipmentNumber);
            //      }

            //      if (postingAction)
            //      {
            //          CheckTransactionAccountsResultCriteria checkCriteria = new CheckTransactionAccountsResultCriteria();
            //          checkCriteria.TransactionId = myIcShipment.ShipmentId;
            //          if (myIcShipment.ShipmentTypeId == (int)Enums_S3.InventoryControl.ShipmentType.Shipment)
            //          { checkCriteria.TransactionType = (int)Enums_S3.InventoryControl.TransactionType.Shipment; }
            //          else if (myIcShipment.ShipmentTypeId == (int)Enums_S3.InventoryControl.ShipmentType.Return)
            //          { checkCriteria.TransactionType = (int)Enums_S3.InventoryControl.TransactionType.ShipmentReturn; }

            //          List<CheckTransactionAccountsResult> checkTransactionAccountsResults = iIcLocationManager.CheckTransactionAccounts(checkCriteria);

            //          foreach (var resultLine in checkTransactionAccountsResults)
            //          {
            //              if (!resultLine.AccountIsValid)
            //              {
            //                  iIcShipmentManager.RollbackTransaction();
            //                  return this.AttachStatusCode(checkTransactionAccountsResults, (int)Enums_S3.InventoryControl.InventoryErrors.GLAccountsDoesNotExist, null);
            //              }
            //          }
            //      }

            //      iIcShipmentManager.CopySessionDetails((ISessionDetails)iIcItemCardSerialTransactionManager);
            //      HandleItemCardSerialTransactions(myIcShipment, postingAction);

            //      if (postingAction)
            //      {
            //          PostShipment(myIcShipment);
            //      }

            //      #region Optional Field
            //      //Save Header Optional Field
            //      if (add && myIcShipment.OptionalFieldEntityList != null)
            //      { OptionalFieldSave(myIcShipment.OptionalFieldEntityList, myIcShipment.ShipmentId); }
            //      //Save Details Optional Feild
            //      foreach (E.IcShipmentDetail detail in myIcShipment.ShipmentDetails)
            //      {
            //          if (detail.OptionalFieldChanged)
            //              OptionalFieldSave(detail.OptionalFieldEntityList, detail.ShipmentDetailId, (int)Enums_S3.Entity.IcShipmentDetail, null);
            //      }
            //      #endregion

            //      iIcShipmentManager.CopySessionDetails((ISessionDetails)iIcCommonQueries);


            //      //iIcShipmentManager.CommitTransaction();
            //      savedShipmenttId = myIcShipment.ShipmentId;

            //      bool DEPIsEnabled = iIcCommonQueries.GetIfGLPostingOnDayEndProcessingIsEnabled(null);

            //      //Create Automatic AR Invoice

            //      if (myIcShipment.ShipmentTypeId != (int)Enums_S3.InventoryControl.ShipmentType.Return)
            //      {
            //          if (postingAction && !DEPIsEnabled && myIcShipment.CreateAutomaticARInvoice)
            //          {
            //              automaticInvoiceRegionEntered = true;
            //              iIcShipmentManager.CopySessionDetails((ISessionDetails)iIcAccountingIntegrationManager);
            //              Centrix.Inventory.Business.Entity.ArAutomaticInvoice arAutomaticInvoice = iIcAccountingIntegrationManager.GetAutomaticARInvoiceByShipmentId(myIcShipment.ShipmentId, LoggedInUser.UserId);

            //              var createdInvoiceId = iIcAccountingIntegrationManager.CreateAutomaticARInvoice(arAutomaticInvoice);

            //              ArInvoiceLite arInvoice = iArInvoiceManager.FindByIdLite(createdInvoiceId, null);
            //              ArInvoice invoice = new ArInvoice();
            //              invoice.ArInvoiceId = createdInvoiceId;
            //              invoice.TotalInWords = ConvertNumbersToWords(arInvoice.TotalAmountWithTax, Enums_S3.Configuration.Language.ar);
            //              iArInvoiceManager.InvoicePartiallyUpdate(invoice);

            //              IcShipmentCriteria crit = new IcShipmentCriteria();
            //              crit.EntityId = myIcShipment.ShipmentId;
            //              crit.ShipmentId = myIcShipment.ShipmentId;
            //              crit.InvoiceId = createdInvoiceId;
            //              iIcShipmentManager.ExecSP(IcShipment.UpdateAutomaticInvoiceIdSPName, crit);
            //          }
            //          automaticInvoiceCreated = true;
            //      }

            //     // iIcShipmentManager.CommitTransaction();

            //      if (!add)
            //          this.UnLockEntity(Enums_S3.Entity.IcShipment, id);
            //      return this.FindByIdLite(myIcShipment.ShipmentId);

            return "";
        }






        private ShipmentReturnObject AddShipmentSingle(InvoiceIntegration invoiceObj, List<InvoiceItemIntegration> details, int Type)
        {
            var add = true;
            var postingAction = true;

            var shipmentQtyAvailable = true;

            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();

            string IcShipmentInfo = "";
            IcShipment myIcShipment = null;

            if (Type == 10)
                myIcShipment = mappShipmentDataExchanged(invoiceObj, details, (int)Enums_S3.InventoryControl.ShipmentType.Shipment);
            else if (Type == 1 || Type == 7)
                myIcShipment = mappShipmentData(invoiceObj, details, (int)Enums_S3.InventoryControl.ShipmentType.Shipment);
            else
                myIcShipment = mappShipmentData(invoiceObj, details, (int)Enums_S3.InventoryControl.ShipmentType.Return);

            //IcShipment myIcShipment =new Centrix.Inventory.Business.Entity.IcShipment();
            //map properties

            var automaticInvoiceCreated = false;
            var createAutomaticInvoiceEnabled = myIcShipment.CreateAutomaticARInvoice;
            var automaticInvoiceRegionEntered = false;
            var savedShipmenttId = 0;

            var result = this.CheckIfValidYearPeriodValue(myIcShipment.PostingDate.Value.Year, myIcShipment.PostingDate.Value.Month);
            if (result != CentrixERP.Configuration.DataTypeContectEnumerations.BatchPostingErrors.NoError)
            {
                return new ShipmentReturnObject { status = false, TransReff = result.ToString(), ShipmentOrReceipt = 1 };
            }
            else
            {
                UpdateShipmentYearPeriodValue(ref myIcShipment);
            }


            try
            {
                PrepareShipmentForSaving(add, 0, myIcShipment, postingAction);

                IcShipmentCriteria shipmentCriteria = new IcShipmentCriteria();
                shipmentCriteria.Keyword = myIcShipment.ShipmentNumber;
                shipmentCriteria.pageNumber = 1;
                shipmentCriteria.resultCount = 10;
                if (add)
                {
                    List<IcShipmentLite> duplicateShipments = iIcShipmentManager.FindAllLight<IcShipmentLite>(shipmentCriteria);//.ToList();
                    if (duplicateShipments != null)
                    {
                        if (duplicateShipments.Count > 0)
                        { throw new SF.Framework.Exceptions.RecordExsitsException("Duplicate Shipment Number"); }
                    }
                }

                myIcShipment.Status = false;
                // iIcShipmentManager.BeginTransaction();
                iIcShipmentManager.BeginTransaction();
                iIcShipmentManager.Save(myIcShipment);
                iIcShipmentManager.CommitTransaction();
                if (add)
                {

                    UpdateDocumentNextNumber((myIcShipment.ShipmentTypeId == (int)Enums_S3.InventoryControl.ShipmentType.Return) ? (int)Enums_S3.InventoryControl.DocumentNumbers.ShipmentReturnNumber : (int)Enums_S3.InventoryControl.DocumentNumbers.ShipmentNumber);
                }

                if (postingAction)
                {
                    CheckTransactionAccountsResultCriteria checkCriteria = new CheckTransactionAccountsResultCriteria();
                    checkCriteria.TransactionId = myIcShipment.ShipmentId;
                    if (myIcShipment.ShipmentTypeId == (int)Enums_S3.InventoryControl.ShipmentType.Shipment)
                    { checkCriteria.TransactionType = (int)Enums_S3.InventoryControl.TransactionType.Shipment; }
                    else if (myIcShipment.ShipmentTypeId == (int)Enums_S3.InventoryControl.ShipmentType.Return)
                    { checkCriteria.TransactionType = (int)Enums_S3.InventoryControl.TransactionType.ShipmentReturn; }

                    List<CheckTransactionAccountsResult> checkTransactionAccountsResults = iIcLocationManager.CheckTransactionAccounts(checkCriteria);

                    foreach (var resultLine in checkTransactionAccountsResults)
                    {
                        if (!resultLine.AccountIsValid)
                        {
                            iIcShipmentManager.RollbackTransaction();
                            return new ShipmentReturnObject { status = false, TransReff = Enums_S3.InventoryControl.InventoryErrors.GLAccountsDoesNotExist.ToString(), ShipmentOrReceipt = 1 };
                        }
                    }
                }

                iIcShipmentManager.CopySessionDetails((ISessionDetails)iIcItemCardSerialTransactionManager);
                HandleItemCardSerialTransactions(myIcShipment, postingAction);


                if (postingAction)
                {
                    PostShipment(myIcShipment, Type, invoiceObj.InvoiceId);
                }


                #region Optional Field
                ////Save Header Optional Field
                //if (add && myIcShipment.OptionalFieldEntityList != null)
                //{ OptionalFieldSave(myIcShipment.OptionalFieldEntityList, myIcShipment.ShipmentId); }
                ////Save Details Optional Feild
                //foreach (E.IcShipmentDetail detail in myIcShipment.ShipmentDetails)
                //{
                //    if (detail.OptionalFieldChanged)
                //        OptionalFieldSave(detail.OptionalFieldEntityList, detail.ShipmentDetailId, (int)Enums_S3.Entity.IcShipmentDetail, null);
                //}
                #endregion

                iIcShipmentManager.CopySessionDetails((ISessionDetails)iIcCommonQueries);


                //iIcShipmentManager.CommitTransaction();
                savedShipmenttId = myIcShipment.ShipmentId;

                bool DEPIsEnabled = iIcCommonQueries.GetIfGLPostingOnDayEndProcessingIsEnabled(null);

                //Create Automatic AR Invoice

                if (Type == 7 || (Type == 5 && invoiceObj.CustomerId > 0))
                {
                    if (postingAction && !DEPIsEnabled && myIcShipment.CreateAutomaticARInvoice)
                    {
                        automaticInvoiceRegionEntered = true;
                        iIcShipmentManager.CopySessionDetails((ISessionDetails)iIcAccountingIntegrationManager);
                        Centrix.Inventory.Business.Entity.ArAutomaticInvoice arAutomaticInvoice = iIcAccountingIntegrationManager.GetAutomaticARInvoiceByShipmentId(myIcShipment.ShipmentId, LoggedInUser.UserId);
                        if (Type == 5)
                            arAutomaticInvoice.DocumentTypeId = (int)APEnums.InvoiceType.CreditNote;

                        var createdInvoiceId = iIcAccountingIntegrationManager.CreateAutomaticARInvoice(arAutomaticInvoice);

                        ArInvoiceLite arInvoice = iArInvoiceManager.FindByIdLite(createdInvoiceId, null);
                        ArInvoice invoice = new ArInvoice();
                        invoice.ArInvoiceId = createdInvoiceId;
                        invoice.TotalInWords = ConvertNumbersToWords(arInvoice.TotalAmountWithTax, Enums_S3.Configuration.Language.ar);
                        iArInvoiceManager.InvoicePartiallyUpdate(invoice);

                        IcShipmentCriteria crit = new IcShipmentCriteria();
                        crit.EntityId = myIcShipment.ShipmentId;
                        crit.ShipmentId = myIcShipment.ShipmentId;
                        crit.InvoiceId = createdInvoiceId;
                        iIcShipmentManager.ExecSP(IcShipment.UpdateAutomaticInvoiceIdSPName, crit);

                        double cashTotal = ((details != null && details.Count() > 0) ? details.First().CashTotal : 0);
                        if (cashTotal > 0)
                        {
                            Centrix.Inventory.Business.Entity.ArAutomaticInvoice arAutomaticCreditNote = iIcAccountingIntegrationManager.GetAutomaticARInvoiceCashPayment(myIcShipment.ShipmentId, LoggedInUser.UserId, cashTotal, invoiceObj.NonTaxableClassId, (myIcShipment.ShipmentTypeId == (int)Enums_S3.InventoryControl.ShipmentType.Shipment), invoiceObj.InvoiceId, invoiceObj.ChangeAmount);
                            string[] CNAccountsIds = invoiceObj.CreditNoteAccountIds.Split(',');



                            for (int i = 0; i < arAutomaticCreditNote.DetailLines.Count; i++)
                            {
                                if (CNAccountsIds[i] != null && CNAccountsIds[i].Trim() != "")
                                    arAutomaticCreditNote.DetailLines[i].AccountId = Convert.ToInt32(CNAccountsIds[i].Trim());
                            }
                            iIcAccountingIntegrationManager.CreateAutomaticARInvoice(arAutomaticCreditNote);

                        }
                    }
                    automaticInvoiceCreated = true;
                }

                if (myIcShipment.ShipmentTypeId == (int)Enums_S3.InventoryControl.ShipmentType.Return)
                {
                    IcShipment parentShipment = iIcShipmentManager.FindById(myIcShipment.ParentShipmentId, null);

                    if (parentShipment != null)
                    {

                        foreach (IcShipmentDetail shipmentDetail in parentShipment.ShipmentDetails)
                        {
                            if (shipmentDetail == null) continue;
                            IcShipmentDetail newDetails = (from item in myIcShipment.ShipmentDetails where item.OldDetailsId == shipmentDetail.ShipmentDetailId select item).FirstOrDefault();
                            if (newDetails != null)
                            {
                                shipmentDetail.MarkModified();
                                if (shipmentDetail.QuantityReturned == null) shipmentDetail.QuantityReturned = 0;
                                shipmentDetail.QuantityReturned += newDetails.Quantity;
                            }

                        }
                        parentShipment.MarkModified();
                        iIcShipmentManager.Save(parentShipment);
                    }
                }

                // iIcShipmentManager.CommitTransaction();

                if (!add)
                    this.UnLockEntity(Enums_S3.Entity.IcShipment, 0);
                IcShipmentLite lite = iIcShipmentManager.FindByIdLight<IcShipmentLite>(myIcShipment.ShipmentId, null);



                return new ShipmentReturnObject { status = true, TransReff = lite.ShipmentNumber, ShipmentId = lite.ShipmentId, ShipmentOrReceipt = 1 };
                //return this.AttachStatusCode(lite, (int)result, null);




            }
            catch (Exception ex)
            {

                return new ShipmentReturnObject { status = false, TransReff = ex.Message, ShipmentOrReceipt = 1 };
            }

        }

        private void UpdateShipmentYearPeriodValue(ref IcShipment shipment)
        {
            IcCommonQueriesCriteria crit = new IcCommonQueriesCriteria();
            crit.Date = shipment.PostingDate;
            string yearPeriodValue = iIcCommonQueries.GetYearPeriodValue(crit);

            shipment.YearPeriod = yearPeriodValue;
            shipment.Year = int.Parse(yearPeriodValue.Split('-')[0]);
            shipment.Period = int.Parse(yearPeriodValue.Split('-')[1]);
        }

        private IcShipment PrepareShipmentForSaving(bool add, int id, IcShipment myIcShipment, bool postingAction)
        {
            if (!add)
            {
                this.LockEntity(Enums_S3.Entity.IcShipment, id);
                myIcShipment.MarkOld();
                myIcShipment.MarkModified();
                myIcShipment.ShipmentId = id;
                myIcShipment.UpdatedBy = LoggedInUser.UserId;
                myIcShipment.UpdatedDate = DateTime.Now;

                foreach (var shipmentDetail in myIcShipment.ShipmentDetails)
                {
                    shipmentDetail.Shipment = myIcShipment;
                    shipmentDetail.UpdatedBy = LoggedInUser.UserId;
                    shipmentDetail.UpdatedDate = DateTime.Now;

                    if (shipmentDetail.LocationId < 0)
                        shipmentDetail.LocationId = null;

                    if (shipmentDetail.ShipmentDetailId == 0)
                    {
                        shipmentDetail.CreatedBy = LoggedInUser.UserId;
                        shipmentDetail.CreatedDate = DateTime.Now;
                    }
                }

                IcShipment oldShipment = iIcShipmentManager.FindById(id, null);
                var deletedDetails = oldShipment == null ? new List<IcShipmentDetail>() : oldShipment.ShipmentDetails.Where(detail => detail != null)
                                                .Where(deletedItem => !myIcShipment.ShipmentDetails.Select(old => old.ShipmentDetailId).Contains(deletedItem.ShipmentDetailId) & !deletedItem.FlagDeleted);
                foreach (var deletedDetail in deletedDetails)
                {
                    deletedDetail.FlagDeleted = true;
                    myIcShipment.ShipmentDetails.Add(deletedDetail);
                }

                myIcShipment.ShipmentNumber = oldShipment == null ? myIcShipment.ShipmentNumber : oldShipment.ShipmentNumber;
            }
            else
            {
                myIcShipment.MarkNew();
                myIcShipment.CreatedBy = LoggedInUser.UserId;
                myIcShipment.CreatedDate = DateTime.Now;

                if (myIcShipment.ShipmentTypeId == (int)Enums_S3.InventoryControl.ShipmentType.Shipment)
                    myIcShipment.ShipmentNumber = GenerateShipmentNumber(false);
                else
                    myIcShipment.ShipmentNumber = GenerateShipmentNumber(true);

                foreach (var shipmentDetail in myIcShipment.ShipmentDetails)
                {
                    shipmentDetail.MarkNew();
                    shipmentDetail.Shipment = myIcShipment;
                    shipmentDetail.CreatedBy = LoggedInUser.UserId;
                    shipmentDetail.CreatedDate = DateTime.Now;

                    if (shipmentDetail.LocationId < 0)
                        shipmentDetail.LocationId = null;
                }
            }
            myIcShipment.Status = false;


            //************Handle Serials*************/
            var currentDateDate = DateTime.Now;
            IcShipment oldMatchingShipment = iIcShipmentManager.FindById(id, null);
            IList<IcItemCardSerialTransaction> ItemCardSerialTransactions = new List<IcItemCardSerialTransaction>();

            foreach (var shipmentDetail in myIcShipment.ShipmentDetails)
            {
                //if (oldMatchingShipment != null)
                //{
                //    var matchingShipmentDetail = oldMatchingShipment.ShipmentDetails.Where(detail => detail != null
                //                                        && detail.ShipmentDetailId == shipmentDetail.ShipmentDetailId).SingleOrDefault();
                //    if (!add && !myIcShipment.Status.Value)
                //    {
                //        var deletedSerials = matchingShipmentDetail.RelatedSerials.Where(serial => serial != null)
                //                                        .Where(deletedSerial => !shipmentDetail.RelatedSerials.Select(old => old.ShipmentDetailSerialId).Contains(deletedSerial.ShipmentDetailSerialId) & !deletedSerial.FlagDeleted);
                //        foreach (var deletedSerial in deletedSerials)
                //        {
                //            deletedSerial.ItemCardSerial.FlagDeleted = true;
                //            deletedSerial.FlagDeleted = true;
                //            shipmentDetail.RelatedSerials.Add(deletedSerial);
                //        }
                //    }
                //}
                if (shipmentDetail.RelatedSerials != null)
                {
                    foreach (var serial in shipmentDetail.RelatedSerials.Where(ser => ser != null))
                    {
                        var matchingShipmentDetail = oldMatchingShipment == null ? null : oldMatchingShipment.ShipmentDetails.Where(detail => detail != null
                                                            && detail.ShipmentDetailId == shipmentDetail.ShipmentDetailId).SingleOrDefault();

                        var matchingShipmentDetailSerial = matchingShipmentDetail == null ? null : matchingShipmentDetail.RelatedSerials.Where(x => x != null && x.ItemCardSerial.ItemCardSerialId == serial.ItemCardSerial.ItemCardSerialId).FirstOrDefault();

                        if (matchingShipmentDetailSerial != null)
                        {
                            serial.ShipmentDetailSerialId = matchingShipmentDetailSerial.ShipmentDetailSerialId;
                            //if (postingAction && myIcShipment.PostType == (int)Enums_S3.InventoryControl.ReceiptPostType.Return)
                            //{
                            //    serial.ItemCardSerial.Status = (int)Enums_S3.InventoryControl.SerialItemStatus.NotAvailable;
                            //}
                            serial.ShipmentDetail = shipmentDetail;
                        }
                        serial.ItemCardSerial.ItemCardSerialId = iIcItemCardManager.GetItemSerialIdfromNumberAndItem(serial.ItemCardSerial.ItemSerialNumber, shipmentDetail.ItemCardId);

                        if (shipmentDetail.Shipment.ShipmentTypeId == (int)Enums_S3.InventoryControl.ShipmentType.Shipment)
                            iIcItemCardManager.UpdateItemCardSerialStatus(serial.ItemCardSerial.ItemCardSerialId, 4);
                        else if (shipmentDetail.Shipment.ShipmentTypeId == (int)Enums_S3.InventoryControl.ShipmentType.Return)
                            iIcItemCardManager.UpdateItemCardSerialStatus(serial.ItemCardSerial.ItemCardSerialId, 5);


                        serial.ShipmentDetail = shipmentDetail;
                        if (serial.ItemCardSerial == null)
                        { serial.ItemCardSerial = new IcItemCardSerial(); }
                        serial.ItemCardSerial.ItemCardId = shipmentDetail.ItemCardId;

                        //Only Update location if posting action of shipment return
                        if (postingAction)
                        {
                            serial.ItemCardSerial.LocationId = shipmentDetail.LocationId.Value;
                        }
                        else
                        {
                            IcItemCardSerial originalSerial = iIcItemCardSerialManager.FindById(serial.ItemCardSerial.ItemCardSerialId, null);
                            serial.ItemCardSerial.LocationId = originalSerial.LocationId;
                        }


                        //if (serial.ItemCardSerial.ItemCardSerialId == 0)
                        //{
                        //    serial.CreatedBy = LoggedInUser.UserId;
                        //    serial.CreatedDate = currentDateDate;
                        //    serial.ItemCardSerial.CreatedBy = LoggedInUser.UserId;
                        //    serial.ItemCardSerial.CreatedDate = currentDateDate;
                        //}
                        //else
                        //{
                        //    serial.CreatedDate = currentDateDate;
                        //}

                        if (serial.ShipmentDetailSerialId == 0)
                        {
                            serial.CreatedBy = LoggedInUser.UserId;
                            serial.CreatedDate = currentDateDate;
                        }

                        serial.UpdatedBy = LoggedInUser.UserId;
                        serial.UpdatedDate = currentDateDate;

                        if (myIcShipment.ShipmentTypeId == (int)Enums_S3.InventoryControl.ShipmentType.Shipment)
                        {
                            if (postingAction)
                            { serial.ItemCardSerial.Status = (int)Enums_S3.InventoryControl.SerialItemStatus.NotAvailable; }
                            else
                            { serial.ItemCardSerial.Status = (int)Enums_S3.InventoryControl.SerialItemStatus.Available; }
                        }
                        else if (myIcShipment.ShipmentTypeId == (int)Enums_S3.InventoryControl.ShipmentType.Return)
                        {
                            if (postingAction)
                            { serial.ItemCardSerial.Status = (int)Enums_S3.InventoryControl.SerialItemStatus.Available; }
                            else
                            { serial.ItemCardSerial.Status = (int)Enums_S3.InventoryControl.SerialItemStatus.NotAvailable; }
                        }
                    }
                }
            }

            return myIcShipment;
        }


        private string GenerateShipmentNumber(bool isReturnShipment)
        {
            IcDocumentNumbersCriteria criteria = new IcDocumentNumbersCriteria();
            if (!isReturnShipment)
            {
                criteria.EntityId = (int)Enums_S3.InventoryControl.DocumentNumbers.ShipmentNumber;
                criteria.DocumentNumbersId = (int)Enums_S3.InventoryControl.DocumentNumbers.ShipmentNumber;
            }
            else
            {
                criteria.EntityId = (int)Enums_S3.InventoryControl.DocumentNumbers.ShipmentReturnNumber;
                criteria.DocumentNumbersId = (int)Enums_S3.InventoryControl.DocumentNumbers.ShipmentReturnNumber;
            }
            IcDocumentNumbersLite DocNumber = iIcDocumentNumbersManager.FindAllLite(criteria).FirstOrDefault();

            var generatedNum = DocNumber.Prefix;
            generatedNum += DocNumber.NextNumber.ToString().PadLeft(DocNumber.Length - DocNumber.Prefix.Length, '0');

            return generatedNum;
        }


        private void UpdateDocumentNextNumber(int documentTypeId)
        {
            try
            {
                iIcDocumentNumbersManager.UpdateDocumentNextNumber(documentTypeId);
            }
            catch (Exception ex) { }
        }

        private void PostShipment(IcShipment Shipment, int invType, int invId)
        {
            IcShipmentCriteria crit = new IcShipmentCriteria();
            crit.ShipmentId = Shipment.ShipmentId;
            crit.EntityId = Shipment.ShipmentId;
            crit.CreatedBy = LoggedInUser.UserId;
            crit.PosInvId = invId;
            crit.PosInvType = invType;
            // iIcShipmentManager.Post(false, crit);
            iIcItemCardManager.PostIcShipment(Shipment.ShipmentId, UserId, false, invType, invId);
        }


        private bool CheckQuantityValidation(InvoiceIntegration invoice, List<InvoiceItemIntegration> List)
        {
            bool originalAvailableQty = true;
            if (List != null)
            {
                if (List.Count > 0)
                {
                    foreach (InvoiceItemIntegration item in List)
                    {
                        originalAvailableQty = iIcItemCardManager.ValidateQuantityLocation(item.ItemId, invoice.LocationId, (int)item.Quantity);
                        if (!originalAvailableQty)
                            break;
                    }
                }
            }

            return originalAvailableQty;
        }


        private IcShipment mappShipmentData(InvoiceIntegration invoice, List<InvoiceItemIntegration> List, int Type)
        {
            IcShipment shipment = new Centrix.Inventory.Business.Entity.IcShipment();
            shipment.Reference = invoice.InvoiceNumber;
            shipment.Description = "POS-Shipment";
            shipment.CurrencyId = invoice.CurrencyId;
            shipment.CustomerId = invoice.CustomerId;
            shipment.PriceListId = -1;
            shipment.DiscountAmount = invoice.AmountDiscount;
            shipment.DiscountPercentage = invoice.PercentageDiscount;
            shipment.ShipmentDate = invoice.InvoiceDate;
            shipment.PostingDate = invoice.InvoiceDate;
            shipment.CreateAutomaticARInvoice = true;
            shipment.ShipmentTypeId = Type;
            shipment.TaxClassId = invoice.TaxClassId;
            shipment.TotalTaxAmount = invoice.TotalTaxAmount;
            shipment.TotalDetailsAmount = invoice.GrandTotal;
            shipment.ExchangeRate = 1;

            shipment.ShipmentDetails = new List<IcShipmentDetail>();
            foreach (InvoiceItemIntegration item in List)
            {
                IcShipmentDetail shipmentItem = new Centrix.Inventory.Business.Entity.IcShipmentDetail();
                shipmentItem.ItemCardId = item.ItemId;
                shipmentItem.ItemPriceListId = item.PriceListId;
                shipmentItem.ItemUnitOfMeasureId = item.UnitOfMeasureId;
                shipmentItem.Quantity = item.Quantity;
                shipmentItem.LocationId = invoice.LocationId;
                shipmentItem.UnitPrice = item.UnitPrice;
                shipmentItem.ExtendedPrice = item.UnitPrice * item.Quantity;
                shipmentItem.UnitCost = item.UnitCost;
                shipmentItem.ExtendedCost = item.UnitCost * item.Quantity;
                shipmentItem.CategoryId = item.CategoryId;
                shipmentItem.TaxClassId = item.TaxClassId;
                shipmentItem.LineDiscountPercent = item.LineDiscountPercent;
                shipmentItem.LineTotalAfterDiscount = item.LineTotalAfterDiscount;
                shipmentItem.DiscountAmount = item.DiscountAmount;
                shipmentItem.TaxRate = item.TaxRate;
                shipmentItem.TaxAmount = item.TaxAmount;
                shipmentItem.TotalLineAmount = item.TotalLineAmount;
                if (item.IsSerial)
                {
                    shipmentItem.RelatedSerials = new List<Centrix.Inventory.Business.Entity.IcShipmentDetailSerial>();
                    shipmentItem.RelatedSerials.Add(new Centrix.Inventory.Business.Entity.IcShipmentDetailSerial()
                    {
                        CreatedBy = UserId,
                        ItemCardSerial = new Centrix.Inventory.Business.Entity.IcItemCardSerial() { ItemCardId = item.ItemId, ItemSerialNumber = item.SerialNumber, LocationId = shipmentItem.LocationId.Value, Status = 2, StockDate = DateTime.Now, CreatedDate = DateTime.Now },
                        ShipmentDetail = shipmentItem,
                        CreatedDate = DateTime.Now
                    });
                }


                shipment.ShipmentDetails.Add(shipmentItem);
            }
            return shipment;
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string InvoiceInquiryResults(string storeIds, DateTime? InvoiceDateFrom, DateTime? InvoiceDateTo)
        {
            List<InvoiceInquiryLite> InvoiceResultList = iInvoiceManager.InvoiceInquiryResults(storeIds, InvoiceDateFrom, InvoiceDateTo);
            return this.AttachStatusCode(InvoiceResultList, 1, null);


        }

        private void HandleItemCardSerialTransactions(IcShipment shipment, bool PostingAction)
        {
            int currentEntityId = (int)Enums_S3.Entity.IcShipmentDetail;
            int currentTransactionTypeId = 0;
            if (shipment.ShipmentTypeId == (int)Enums_S3.InventoryControl.ShipmentType.Shipment)
            { currentTransactionTypeId = (int)Enums_S3.InventoryControl.TransactionType.Shipment; }
            else if (shipment.ShipmentTypeId == (int)Enums_S3.InventoryControl.ShipmentType.Return)
            { currentTransactionTypeId = (int)Enums_S3.InventoryControl.TransactionType.ShipmentReturn; }

            IList<IcItemCardSerialTransaction> ItemCardSerialTransactions = new List<IcItemCardSerialTransaction>();
            IList<IcItemCardSerialTransaction> allDeletedSerials = new List<IcItemCardSerialTransaction>();
            var currentDate = DateTime.Now;
            foreach (var shipmentDetail in shipment.ShipmentDetails)
            {
                IcItemCardSerialTransactionCriteria itemCardSerialTransactionCriteria = new IcItemCardSerialTransactionCriteria();
                itemCardSerialTransactionCriteria.RelatedEntityId = currentEntityId;
                itemCardSerialTransactionCriteria.EntityValueId = shipmentDetail.ShipmentDetailId;

                IList<IcItemCardSerialTransactionLite> originalRelatedSerialTransactions = iIcItemCardSerialTransactionManager.FindAllLight<IcItemCardSerialTransactionLite>(itemCardSerialTransactionCriteria);
                var origionalreturnedSerialTransactions = originalRelatedSerialTransactions == null ? null : originalRelatedSerialTransactions.Where(x => x.TransactionTypeId == currentTransactionTypeId);
                if (shipmentDetail.RelatedSerials == null) shipmentDetail.RelatedSerials = new List<IcShipmentDetailSerial>();
                foreach (var shipmentDetailSerial in shipmentDetail.RelatedSerials)
                {

                    iIcItemCardManager.UpdateItemCardSerialStatus(shipmentDetailSerial.ItemCardSerial.ItemCardSerialId, currentTransactionTypeId);

                    var matchingItem = originalRelatedSerialTransactions == null ? null : originalRelatedSerialTransactions.Where(x => x.ItemCardSerialId == shipmentDetailSerial.ItemCardSerial.ItemCardSerialId).SingleOrDefault();

                    if (matchingItem == null) //New Item
                    {
                        IcItemCardSerialTransaction itemCardSerialTrans = new IcItemCardSerialTransaction();
                        itemCardSerialTrans.CreatedBy = LoggedInUser.UserId;
                        itemCardSerialTrans.CreatedDate = currentDate;
                        itemCardSerialTrans.ItemCardSerialId = shipmentDetailSerial.ItemCardSerial.ItemCardSerialId;
                        itemCardSerialTrans.RelatedEntityId = currentEntityId;
                        itemCardSerialTrans.EntityValueId = shipmentDetail.ShipmentDetailId;
                        itemCardSerialTrans.TransactionTypeId = currentTransactionTypeId;
                        ItemCardSerialTransactions.Add(itemCardSerialTrans);
                    }

                }

                var deletedSerials = originalRelatedSerialTransactions == null ? new List<IcItemCardSerialTransactionLite>() : originalRelatedSerialTransactions.Where(relatedSerial => relatedSerial != null)
                                                .Where(deletedItem => !shipmentDetail.RelatedSerials.Select(old => old.ItemCardSerial.ItemCardSerialId).Contains(deletedItem.ItemCardSerialId) ||
                                                    shipmentDetail.RelatedSerials.Where(ser => ser.FlagDeleted == true).Select(old => old.ItemCardSerial.ItemCardSerialId).Contains(deletedItem.ItemCardSerialId));

                foreach (var deletedSerial in deletedSerials)
                {

                    IcItemCardSerialTransaction itemCardSerialTrans = new IcItemCardSerialTransaction();
                    itemCardSerialTrans.ItemCardSerialId = deletedSerial.ItemCardSerialId;
                    itemCardSerialTrans.RelatedEntityId = deletedSerial.RelatedEntityId;
                    itemCardSerialTrans.EntityValueId = deletedSerial.EntityValueId;
                    itemCardSerialTrans.TransactionTypeId = deletedSerial.TransactionTypeId;
                    itemCardSerialTrans.ItemCardSerialTransactionId = deletedSerial.ItemCardSerialTransactionId;
                    itemCardSerialTrans.FlagDeleted = true;
                    itemCardSerialTrans.MarkDeleted();
                    ItemCardSerialTransactions.Add(itemCardSerialTrans);
                }
                //}
            }


            foreach (var itemCardSerialTransaction in ItemCardSerialTransactions)
            {
                if (itemCardSerialTransaction.ItemCardSerialTransactionId == 0)
                    iIcItemCardSerialTransactionManager.Save(itemCardSerialTransaction);
                else
                    iIcItemCardSerialTransactionManager.Update(itemCardSerialTransaction);
            }
        }



        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string CloseRegisterInquiryResults(string storeIds, string registerIds, DateTime? openedDateFrom, DateTime? openedDateTo, DateTime? closedDateFrom, DateTime? closedDateTo)
        {
            List<CloseRegisterInquiryLite> CloseRegisterResultList = iCloseRegisterManager.CloseRegisterInquiryResults(storeIds, registerIds, openedDateFrom, openedDateTo, closedDateFrom, closedDateTo);
            return this.AttachStatusCode(CloseRegisterResultList, 1, null);


        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string QuantityInquiryResults(int locationId,int storeId,int ItemId)
        {
            List<POSQuantityInQuiryLite> POSResultList = iInvoiceManager.FindInquiryResults(locationId, storeId, ItemId);
            return this.AttachStatusCode(POSResultList, 1, null);


        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string ICQuantityInquiryResults(int locationId, int storeId, int ItemId)
        {
            List<ICQuantityInQuiryLite> POSResultList = iInvoiceManager.FindICInquiryResults(locationId, storeId, ItemId);
            return this.AttachStatusCode(POSResultList, 1, null);


        }

        private ShipmentReturnObject AddReceiptSingle(InvoiceIntegration invoiceObj, List<InvoiceItemIntegration> details, int Type)
        {
            var add = true;
            var postingAction = true;
            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();

            string IcReceiptInfo = "";
            IcReceipt myIcReceipt = null;

            if (Type == 10)
                myIcReceipt = mappReceiptData(invoiceObj, details, (int)Enums_S3.InventoryControl.ReceiptPostType.Posted);


            var automaticInvoiceCreated = false;


            var savedReceiptId = 0;

            var result = this.CheckIfValidYearPeriodValue(myIcReceipt.PostingDate.Value.Year, myIcReceipt.PostingDate.Value.Month);
            if (result != CentrixERP.Configuration.DataTypeContectEnumerations.BatchPostingErrors.NoError)
            {
                return new ShipmentReturnObject { status = false, TransReff = result.ToString(), ShipmentOrReceipt = 2 };
            }
            else
            {
                UpdateReceiptYearPeriodValue(ref myIcReceipt);
            }


            try
            {
                PrepareReceiptForSaving(add, 0, myIcReceipt, postingAction);

                IcReceiptCriteria rcpCriteria = new IcReceiptCriteria();
                rcpCriteria.Keyword = myIcReceipt.ReceiptNumber;
                rcpCriteria.pageNumber = 1;
                rcpCriteria.resultCount = 10;
                if (add)
                {
                    List<ReceiptLite> duplicateReceipts = iIcReceiptManager.FindAllLight<ReceiptLite>(rcpCriteria);
                    if (duplicateReceipts != null)
                    {
                        if (duplicateReceipts.Count > 0)
                        { throw new SF.Framework.Exceptions.RecordExsitsException("Duplicate Receipt Number"); }
                    }
                }

                myIcReceipt.Status = false;
                // iIcShipmentManager.BeginTransaction();
                iIcReceiptManager.BeginTransaction();
                iIcReceiptManager.Save(myIcReceipt);
                iIcReceiptManager.CommitTransaction();
                if (add)
                {

                    UpdateDocumentNextNumber((int)Enums_S3.InventoryControl.DocumentNumbers.ReceiptNumber);
                }

                if (postingAction)
                {
                    CheckTransactionAccountsResultCriteria checkCriteria = new CheckTransactionAccountsResultCriteria();
                    checkCriteria.TransactionId = myIcReceipt.ReceiptId;
                    if (myIcReceipt.ReceiptTypeId == (int)Enums_S3.InventoryControl.ReceiptPostType.Posted)
                    { checkCriteria.TransactionType = (int)Enums_S3.InventoryControl.TransactionType.Receipt; }


                    List<CheckTransactionAccountsResult> checkTransactionAccountsResults = iIcLocationManager.CheckTransactionAccounts(checkCriteria);

                    foreach (var resultLine in checkTransactionAccountsResults)
                    {
                        if (!resultLine.AccountIsValid)
                        {
                            iIcReceiptManager.RollbackTransaction();
                            return new ShipmentReturnObject { status = false, TransReff = Enums_S3.InventoryControl.InventoryErrors.GLAccountsDoesNotExist.ToString(), ShipmentOrReceipt = 2 };
                        }
                    }
                }

                iIcReceiptManager.CopySessionDetails((ISessionDetails)iIcItemCardSerialTransactionManager);
                HandleItemCardSerialTransactions2(myIcReceipt, postingAction);


                if (postingAction)
                {
                    PostReceipt(myIcReceipt, Type, invoiceObj.InvoiceId);
                }




                iIcReceiptManager.CopySessionDetails((ISessionDetails)iIcCommonQueries);


                savedReceiptId = myIcReceipt.ReceiptId;

                bool DEPIsEnabled = iIcCommonQueries.GetIfGLPostingOnDayEndProcessingIsEnabled(null);




                if (!add)
                    this.UnLockEntity(Enums_S3.Entity.IcShipment, 0);
                ReceiptLite lite = iIcReceiptManager.FindByIdLight<ReceiptLite>(myIcReceipt.ReceiptId, null);



                return new ShipmentReturnObject { status = true, TransReff = lite.ReceiptNumber, ShipmentId = lite.ReceiptId, ShipmentOrReceipt = 2 };





            }
            catch (Exception ex)
            {

                return new ShipmentReturnObject { status = false, TransReff = ex.Message, ShipmentOrReceipt = 2 };
            }

        }


        private IcReceipt mappReceiptData(InvoiceIntegration invoice, List<InvoiceItemIntegration> List, int Type)
        {
            IcReceipt receipt = new Centrix.Inventory.Business.Entity.IcReceipt();
            receipt.Reference = invoice.InvoiceNumber;
            receipt.Description = "POS-Receipt";
            receipt.CurrencyId = invoice.CurrencyId;
            receipt.VendorId = invoice.CustomerId;  //Here
            receipt.AutomaticProration = true;
            // receipt.
            if (receipt.AdditionalCostTypeReceipts != null && receipt.AdditionalCostTypeReceipts.Count() > 0)
                receipt.AdditionalCost = (float)(from item in receipt.AdditionalCostTypeReceipts where item.IncludeExculdeProration.Value select item.Amount).ToList().Sum();
            else
                receipt.AdditionalCost = 0;

            receipt.DiscountAmount = invoice.AmountDiscount;
            receipt.DiscountPercentage = invoice.PercentageDiscount;
            receipt.ReceiptDate = invoice.InvoiceDate;
            receipt.PostingDate = invoice.InvoiceDate;
            receipt.CreateAutomaticAPInvoice = false;
            receipt.ReceiptTypeId = Type;
            receipt.TaxClassId = invoice.TaxClassId;
            receipt.TotalTaxAmount = 0;
            receipt.TotalDetailsAmount = 0;
            receipt.TotalCost = 0;

            receipt.TotalExtendedCost = 0;
            receipt.ExchangeRate = 1;
            receipt.PostType = 1;
            receipt.TotalDiscountAmount = invoice.AmountDiscount;



            receipt.ReceiptDetails = new List<IcReceiptDetail>();
            foreach (InvoiceItemIntegration item in List)
            {
                IcReceiptDetail receiptItem = new Centrix.Inventory.Business.Entity.IcReceiptDetail();
                double unitAvgCost = (double)iIcItemCardManager.FindItemCardAverageCost(item.ItemId, invoice.LocationId);
                receiptItem.ItemCardId = item.ItemId;
                //receiptItem.ItemPriceListId = item.PriceListId;
                receiptItem.ItemUnitOfMeasureId = item.UnitOfMeasureId;
                receiptItem.QuantityReceived = item.Quantity;
                receiptItem.OriginalQuantity = item.Quantity;
                receiptItem.LocationId = invoice.LocationId;
                receiptItem.UnitCost = unitAvgCost;
                receiptItem.ExtendedCost = (unitAvgCost * item.Quantity);
                receiptItem.TaxClassId = item.TaxClassId;
                receiptItem.LineDiscountPercent = item.LineDiscountPercent;
                receiptItem.LineTotalAfterDiscount = (unitAvgCost * item.Quantity);
                receiptItem.DiscountAmount = item.DiscountAmount;
                receiptItem.TaxRate = item.TaxRate;
                receiptItem.TaxAmount = (unitAvgCost * item.Quantity * -1 * (item.TaxRate / 100));
                receiptItem.TotalLineAmount = (unitAvgCost * item.Quantity) + (unitAvgCost * item.Quantity * ((-1 * item.TaxRate) / 100));
                receiptItem.AdditionalCost = 0;
                receiptItem.LineCost = (unitAvgCost * item.Quantity);
                receiptItem.AdditionalCost = 0;
                receiptItem.ManualProration = 0;

                if (item.IsSerial)
                {
                    receiptItem.RelatedSerials = new List<Centrix.Inventory.Business.Entity.IcReceiptDetailSerial>();
                    receiptItem.RelatedSerials.Add(new Centrix.Inventory.Business.Entity.IcReceiptDetailSerial()
                    {
                        CreatedBy = UserId,
                        ItemCardSerial = new Centrix.Inventory.Business.Entity.IcItemCardSerial() { ItemCardId = item.ItemId, ItemSerialNumber = item.SerialNumber, LocationId = receiptItem.LocationId, Status = 2, StockDate = DateTime.Now, CreatedDate = DateTime.Now },
                        ReceiptDetail = receiptItem,
                        CreatedDate = DateTime.Now
                    });
                }

                receipt.TotalCost += receiptItem.TotalLineAmount;
                receipt.TotalDiscountAmount += receiptItem.DiscountAmount;
                receipt.TotalDetailsAmount += receiptItem.TotalLineAmount;
                receipt.TotalExtendedCost += receiptItem.ExtendedCost;
                receipt.TotalTaxAmount += receiptItem.TaxAmount;
                receipt.ReceiptDetails.Add(receiptItem);
            }


            return receipt;
        }



        private void UpdateReceiptYearPeriodValue(ref IcReceipt rcp)
        {
            IcCommonQueriesCriteria crit = new IcCommonQueriesCriteria();
            crit.Date = rcp.PostingDate;
            string yearPeriodValue = iIcCommonQueries.GetYearPeriodValue(crit);

            rcp.YearPeriod = yearPeriodValue;
            rcp.Year = int.Parse(yearPeriodValue.Split('-')[0]);
            rcp.Period = int.Parse(yearPeriodValue.Split('-')[1]);
        }




        private IcReceipt PrepareReceiptForSaving(bool add, int id, IcReceipt myIcReceipt, bool postingAction)
        {
            if (!add)
            {
                this.LockEntity(Enums_S3.Entity.IcReceipt, id);
                myIcReceipt.MarkOld();
                myIcReceipt.MarkModified();
                myIcReceipt.ReceiptId = id;
                myIcReceipt.UpdatedBy = LoggedInUser.UserId;
                myIcReceipt.UpdatedDate = DateTime.Now;

                foreach (var receiptDetail in myIcReceipt.ReceiptDetails)
                {
                    receiptDetail.Receipt = myIcReceipt;
                    receiptDetail.UpdatedBy = LoggedInUser.UserId;
                    receiptDetail.UpdatedDate = DateTime.Now;

                    if (receiptDetail.LocationId < 0)
                        receiptDetail.LocationId = -1;

                    if (receiptDetail.ReceiptDetailId == 0)
                    {
                        receiptDetail.CreatedBy = LoggedInUser.UserId;
                        receiptDetail.CreatedDate = DateTime.Now;
                    }
                }

                IcReceipt oldReceipt = iIcReceiptManager.FindById(id, null);
                var deletedDetails = oldReceipt == null ? new List<IcReceiptDetail>() : oldReceipt.ReceiptDetails.Where(detail => detail != null)
                                                .Where(deletedItem => !myIcReceipt.ReceiptDetails.Select(old => old.ReceiptDetailId).Contains(deletedItem.ReceiptDetailId) & !deletedItem.FlagDeleted);
                foreach (var deletedDetail in deletedDetails)
                {
                    deletedDetail.FlagDeleted = true;
                    myIcReceipt.ReceiptDetails.Add(deletedDetail);
                }

                myIcReceipt.ReceiptNumber = oldReceipt == null ? myIcReceipt.ReceiptNumber : oldReceipt.ReceiptNumber;
            }
            else
            {
                myIcReceipt.MarkNew();
                myIcReceipt.CreatedBy = LoggedInUser.UserId;
                myIcReceipt.CreatedDate = DateTime.Now;

                if (myIcReceipt.ReceiptTypeId == (int)Enums_S3.InventoryControl.ReceiptPostType.Posted)
                    myIcReceipt.ReceiptNumber = GenerateReceiptNumber();


                foreach (var receiptDetail in myIcReceipt.ReceiptDetails)
                {
                    receiptDetail.MarkNew();
                    receiptDetail.Receipt = myIcReceipt;
                    receiptDetail.CreatedBy = LoggedInUser.UserId;
                    receiptDetail.CreatedDate = DateTime.Now;

                    if (receiptDetail.LocationId < 0)
                        receiptDetail.LocationId = -1;
                }
            }
            myIcReceipt.Status = false;


            //************Handle Serials*************/
            var currentDateDate = DateTime.Now;
            IcReceipt oldMatchingReceipt = iIcReceiptManager.FindById(id, null);
            IList<IcItemCardSerialTransaction> ItemCardSerialTransactions = new List<IcItemCardSerialTransaction>();

            foreach (var receiptDetail in myIcReceipt.ReceiptDetails)
            {

                if (receiptDetail.RelatedSerials != null)
                {
                    foreach (var serial in receiptDetail.RelatedSerials.Where(ser => ser != null))
                    {
                        var matchingReceiptDetail = oldMatchingReceipt == null ? null : oldMatchingReceipt.ReceiptDetails.Where(detail => detail != null
                                                            && detail.ReceiptDetailId == receiptDetail.ReceiptDetailId).SingleOrDefault();

                        var matchingReceiptDetailSerial = matchingReceiptDetail == null ? null : matchingReceiptDetail.RelatedSerials.Where(x => x != null && x.ItemCardSerial.ItemCardSerialId == serial.ItemCardSerial.ItemCardSerialId).FirstOrDefault();

                        if (matchingReceiptDetailSerial != null)
                        {
                            serial.ReceiptDetailSerialId = matchingReceiptDetailSerial.ReceiptDetailSerialId;


                            serial.ReceiptDetail = receiptDetail;
                        }
                        serial.ItemCardSerial.ItemCardSerialId = iIcItemCardManager.GetItemSerialIdfromNumberAndItem(serial.ItemCardSerial.ItemSerialNumber, receiptDetail.ItemCardId);

                        if (receiptDetail.Receipt.ReceiptTypeId == (int)Enums_S3.InventoryControl.ReceiptPostType.Posted)
                            iIcItemCardManager.UpdateItemCardSerialStatus(serial.ItemCardSerial.ItemCardSerialId, 1);



                        serial.ReceiptDetail = receiptDetail;
                        if (serial.ItemCardSerial == null)
                        { serial.ItemCardSerial = new IcItemCardSerial(); }
                        serial.ItemCardSerial.ItemCardId = receiptDetail.ItemCardId;

                        //Only Update location if posting action of shipment return
                        if (postingAction)
                        {
                            serial.ItemCardSerial.LocationId = receiptDetail.LocationId;
                        }
                        else
                        {
                            IcItemCardSerial originalSerial = iIcItemCardSerialManager.FindById(serial.ItemCardSerial.ItemCardSerialId, null);
                            serial.ItemCardSerial.LocationId = originalSerial.LocationId;
                        }




                        if (serial.ReceiptDetailSerialId == 0)
                        {
                            serial.CreatedBy = LoggedInUser.UserId;
                            serial.CreatedDate = currentDateDate;
                        }

                        serial.UpdatedBy = LoggedInUser.UserId;
                        serial.UpdatedDate = currentDateDate;

                        if (myIcReceipt.ReceiptTypeId == (int)Enums_S3.InventoryControl.ReceiptPostType.Posted)
                        {
                            if (postingAction)
                            { serial.ItemCardSerial.Status = (int)Enums_S3.InventoryControl.SerialItemStatus.Available; }
                            else
                            { serial.ItemCardSerial.Status = (int)Enums_S3.InventoryControl.SerialItemStatus.NotAvailable; }
                        }

                    }
                }
            }

            return myIcReceipt;
        }




        private string GenerateReceiptNumber()
        {
            IcDocumentNumbersCriteria criteria = new IcDocumentNumbersCriteria();

            criteria.EntityId = (int)Enums_S3.InventoryControl.DocumentNumbers.ReceiptNumber;
            criteria.DocumentNumbersId = (int)Enums_S3.InventoryControl.DocumentNumbers.ReceiptNumber;

            IcDocumentNumbersLite DocNumber = iIcDocumentNumbersManager.FindAllLite(criteria).FirstOrDefault();

            var generatedNum = DocNumber.Prefix;
            generatedNum += DocNumber.NextNumber.ToString().PadLeft(DocNumber.Length - DocNumber.Prefix.Length, '0');

            return generatedNum;
        }


        private void PostReceipt(IcReceipt receipt, int invType, int invId)
        {
            IcReceiptCriteria crit = new IcReceiptCriteria();
            crit.ReceiptId = receipt.ReceiptId;
            crit.EntityId = receipt.ReceiptId;
            crit.CreatedBy = LoggedInUser.UserId;
            crit.PosInvId = invId;
            crit.PosInvType = invType;
            iIcItemCardManager.PostIcReceipt(receipt.ReceiptId, UserId, false, invType, invId);
        }




        private void HandleItemCardSerialTransactions2(IcReceipt receipt, bool PostingAction)
        {
            int currentEntityId = (int)Enums_S3.Entity.IcReceiptDetail;
            int currentTransactionTypeId = 0;
            if (receipt.ReceiptTypeId == (int)Enums_S3.InventoryControl.ReceiptPostType.Posted)
            { currentTransactionTypeId = (int)Enums_S3.InventoryControl.TransactionType.Receipt; }


            IList<IcItemCardSerialTransaction> ItemCardSerialTransactions = new List<IcItemCardSerialTransaction>();
            IList<IcItemCardSerialTransaction> allDeletedSerials = new List<IcItemCardSerialTransaction>();
            var currentDate = DateTime.Now;
            foreach (var receiptDetail in receipt.ReceiptDetails)
            {
                IcItemCardSerialTransactionCriteria itemCardSerialTransactionCriteria = new IcItemCardSerialTransactionCriteria();
                itemCardSerialTransactionCriteria.RelatedEntityId = currentEntityId;
                itemCardSerialTransactionCriteria.EntityValueId = receiptDetail.ReceiptDetailId;

                IList<IcItemCardSerialTransactionLite> originalRelatedSerialTransactions = iIcItemCardSerialTransactionManager.FindAllLight<IcItemCardSerialTransactionLite>(itemCardSerialTransactionCriteria);
                var origionalreturnedSerialTransactions = originalRelatedSerialTransactions == null ? null : originalRelatedSerialTransactions.Where(x => x.TransactionTypeId == currentTransactionTypeId);
                if (receiptDetail.RelatedSerials == null) receiptDetail.RelatedSerials = new List<IcReceiptDetailSerial>();
                foreach (var receiptDetailSerial in receiptDetail.RelatedSerials)
                {

                    iIcItemCardManager.UpdateItemCardSerialStatus(receiptDetailSerial.ItemCardSerial.ItemCardSerialId, currentTransactionTypeId);

                    var matchingItem = originalRelatedSerialTransactions == null ? null : originalRelatedSerialTransactions.Where(x => x.ItemCardSerialId == receiptDetailSerial.ItemCardSerial.ItemCardSerialId).SingleOrDefault();

                    if (matchingItem == null) //New Item
                    {
                        IcItemCardSerialTransaction itemCardSerialTrans = new IcItemCardSerialTransaction();
                        itemCardSerialTrans.CreatedBy = LoggedInUser.UserId;
                        itemCardSerialTrans.CreatedDate = currentDate;
                        itemCardSerialTrans.ItemCardSerialId = receiptDetailSerial.ItemCardSerial.ItemCardSerialId;
                        itemCardSerialTrans.RelatedEntityId = currentEntityId;
                        itemCardSerialTrans.EntityValueId = receiptDetail.ReceiptDetailId;
                        itemCardSerialTrans.TransactionTypeId = currentTransactionTypeId;
                        ItemCardSerialTransactions.Add(itemCardSerialTrans);
                    }

                }

                var deletedSerials = originalRelatedSerialTransactions == null ? new List<IcItemCardSerialTransactionLite>() : originalRelatedSerialTransactions.Where(relatedSerial => relatedSerial != null)
                                                .Where(deletedItem => !receiptDetail.RelatedSerials.Select(old => old.ItemCardSerial.ItemCardSerialId).Contains(deletedItem.ItemCardSerialId) ||
                                                    receiptDetail.RelatedSerials.Where(ser => ser.FlagDeleted == true).Select(old => old.ItemCardSerial.ItemCardSerialId).Contains(deletedItem.ItemCardSerialId));

                foreach (var deletedSerial in deletedSerials)
                {

                    IcItemCardSerialTransaction itemCardSerialTrans = new IcItemCardSerialTransaction();
                    itemCardSerialTrans.ItemCardSerialId = deletedSerial.ItemCardSerialId;
                    itemCardSerialTrans.RelatedEntityId = deletedSerial.RelatedEntityId;
                    itemCardSerialTrans.EntityValueId = deletedSerial.EntityValueId;
                    itemCardSerialTrans.TransactionTypeId = deletedSerial.TransactionTypeId;
                    itemCardSerialTrans.ItemCardSerialTransactionId = deletedSerial.ItemCardSerialTransactionId;
                    itemCardSerialTrans.FlagDeleted = true;
                    itemCardSerialTrans.MarkDeleted();
                    ItemCardSerialTransactions.Add(itemCardSerialTrans);
                }
                //}
            }


            foreach (var itemCardSerialTransaction in ItemCardSerialTransactions)
            {
                if (itemCardSerialTransaction.ItemCardSerialTransactionId == 0)
                    iIcItemCardSerialTransactionManager.Save(itemCardSerialTransaction);
                else
                    iIcItemCardSerialTransactionManager.Update(itemCardSerialTransaction);
            }
        }



        private IcShipment mappShipmentDataExchanged(InvoiceIntegration invoice, List<InvoiceItemIntegration> List, int Type)
        {
            IcShipment shipment = new Centrix.Inventory.Business.Entity.IcShipment();
            shipment.Reference = invoice.InvoiceNumber;
            shipment.Description = "POS-Shipment";
            shipment.CurrencyId = invoice.CurrencyId;
            shipment.CustomerId = invoice.CustomerId;
            shipment.PriceListId = -1;
            shipment.DiscountAmount = invoice.AmountDiscount;
            shipment.DiscountPercentage = invoice.PercentageDiscount;
            shipment.ShipmentDate = invoice.InvoiceDate;
            shipment.PostingDate = invoice.InvoiceDate;
            shipment.CreateAutomaticARInvoice = false;
            shipment.ShipmentTypeId = Type;
            shipment.TaxClassId = invoice.TaxClassId;
            shipment.TotalTaxAmount = 0;
            shipment.TotalDetailsAmount = 0;
            shipment.ExchangeRate = 1;

            shipment.ShipmentDetails = new List<IcShipmentDetail>();
            foreach (InvoiceItemIntegration item in List)
            {
                IcShipmentDetail shipmentItem = new Centrix.Inventory.Business.Entity.IcShipmentDetail();
                shipmentItem.ItemCardId = item.ItemId;
                shipmentItem.ItemPriceListId = item.PriceListId;
                shipmentItem.ItemUnitOfMeasureId = item.UnitOfMeasureId;
                shipmentItem.Quantity = item.Quantity;
                shipmentItem.LocationId = invoice.LocationId;
                shipmentItem.UnitPrice = item.UnitPrice;
                shipmentItem.ExtendedPrice = item.UnitPrice * item.Quantity;
                shipmentItem.UnitCost = item.UnitCost;
                shipmentItem.ExtendedCost = item.UnitCost * item.Quantity;
                shipmentItem.CategoryId = item.CategoryId;
                shipmentItem.TaxClassId = item.TaxClassId;
                shipmentItem.LineDiscountPercent = item.LineDiscountPercent;
                shipmentItem.LineTotalAfterDiscount = item.LineTotalAfterDiscount;
                shipmentItem.DiscountAmount = item.DiscountAmount;
                shipmentItem.TaxRate = item.TaxRate;
                shipmentItem.TaxAmount = item.TaxAmount;
                shipmentItem.TotalLineAmount = item.TotalLineAmount;
                if (item.IsSerial)
                {
                    shipmentItem.RelatedSerials = new List<Centrix.Inventory.Business.Entity.IcShipmentDetailSerial>();
                    shipmentItem.RelatedSerials.Add(new Centrix.Inventory.Business.Entity.IcShipmentDetailSerial()
                    {
                        CreatedBy = UserId,
                        ItemCardSerial = new Centrix.Inventory.Business.Entity.IcItemCardSerial() { ItemCardId = item.ItemId, ItemSerialNumber = item.SerialNumber, LocationId = shipmentItem.LocationId.Value, Status = 2, StockDate = DateTime.Now, CreatedDate = DateTime.Now },
                        ShipmentDetail = shipmentItem,
                        CreatedDate = DateTime.Now
                    });
                }

                shipment.TotalDetailsAmount += (double)shipmentItem.TotalLineAmount;
                shipment.TotalTaxAmount += (double)shipmentItem.TaxAmount;
                shipment.ShipmentDetails.Add(shipmentItem);
            }
            return shipment;
        }

        public Centrix.Inventory.Business.Entity.ArAutomaticInvoice createAutomaticInvoice(IcShipment myIcShipment, InvoiceIntegration InvoiceInfo)
        {
            Centrix.Inventory.Business.Entity.ArAutomaticInvoice arAutomaticInvoice = new Centrix.Inventory.Business.Entity.ArAutomaticInvoice();
            CustomerLite customer = iCustomerManager.FindByIdLite(myIcShipment.CustomerId.Value, null);

            iIcShipmentManager.CopySessionDetails((ISessionDetails)iIcCommonQueries);

            var customerCurrencyCrit = new IcAccountingIntegrationCriteria();
            customerCurrencyCrit.CustomerId = customer.CustomerId;
            var customerCurrencyId = iIcAccountingIntegrationManager.GetCustomerCurrency(customerCurrencyCrit);
            int taxAuthorityId = customer.TaxAuthorityId;

            var currencyExchangeRate = myIcShipment.ExchangeRate; //  1.0;


            arAutomaticInvoice.BatchDescription = myIcShipment.Description + "-" + InvoiceInfo.InvoiceNumber;
            arAutomaticInvoice.CreatedBy = UserId;
            //arAutomaticInvoice.DocumentNumber = myIcShipment.ShipmentNumber;

            //Ap Invoice
            arAutomaticInvoice.InvoiceDocumentNumber = myIcShipment.Reference;
            arAutomaticInvoice.DocumentDate = myIcShipment.ShipmentDate.Value;
            arAutomaticInvoice.PostingDate = myIcShipment.PostingDate.Value;
            arAutomaticInvoice.ExchangeRate = currencyExchangeRate;


            arAutomaticInvoice.DocumentTypeId = (int)APEnums.InvoiceType.Invoice;

            //if (myIcShipment.ShipmentTypeId == (int)Enums_S3.InventoryControl.ShipmentType.Return)
            //    arAutomaticInvoice.DocumentTypeId = (int)APEnums.InvoiceType.CreditNote;

            arAutomaticInvoice.PaymentTermId = customer.PaymentTermCodeId;
            arAutomaticInvoice.TaxClassId = customer.TaxClassId;
            arAutomaticInvoice.CustomerId = customer.CustomerId;
            arAutomaticInvoice.DiscountAmount = myIcShipment.DiscountAmount;
            arAutomaticInvoice.DiscountPercentage = myIcShipment.DiscountPercentage;
            //Ap Lines
            arAutomaticInvoice.DetailLines = new List<Centrix.Inventory.Business.Entity.ArAutomaticInvoiceDetail>();

        
            foreach (var shipmentDetail in myIcShipment.ShipmentDetails.Where(x => x != null))
            {
                Centrix.Inventory.Business.Entity.ArAutomaticInvoiceDetail arAutomaticInvoiceDetail = new Centrix.Inventory.Business.Entity.ArAutomaticInvoiceDetail();
                arAutomaticInvoiceDetail.SourceAmountWithTax = (double)((shipmentDetail.ExtendedPrice.Value) * currencyExchangeRate);
                var allItemTaxes = iIcItemTaxManager.FindByParentId(shipmentDetail.ItemCardId, typeof(IcItemCard), null);
                arAutomaticInvoiceDetail.SourceAmount = shipmentDetail.ExtendedPrice.Value;
                arAutomaticInvoiceDetail.TaxClassId = shipmentDetail.TaxClassId;// allItemTaxes.First().SalesTaxClassId;
                arAutomaticInvoiceDetail.LineDiscountPercent = shipmentDetail.LineDiscountPercent.Value;
                arAutomaticInvoiceDetail.LineTotalAfterDiscount = shipmentDetail.LineTotalAfterDiscount.Value;
                arAutomaticInvoiceDetail.TotalLineAmount = shipmentDetail.TotalLineAmount.Value;
                arAutomaticInvoiceDetail.TaxRate = shipmentDetail.TaxRate.Value;
                arAutomaticInvoiceDetail.TaxAmount = shipmentDetail.TaxAmount.Value;
                arAutomaticInvoiceDetail.DiscountAmount = shipmentDetail.DiscountAmount.Value;
                arAutomaticInvoiceDetail.SourceCurrencyId = myIcShipment.CurrencyId;
                arAutomaticInvoiceDetail.AccountId = iArInvoiceManager.fnGetItemOverridedAccountId(shipmentDetail.ItemCardId, shipmentDetail.LocationId.Value, 2);//GetItemOverridedAccountIdFunctionName

                arAutomaticInvoice.DetailLines.Add(arAutomaticInvoiceDetail);
         
            }

            return arAutomaticInvoice;
        }

        public int CreateAutomaticARInvoice(Centrix.Inventory.Business.Entity.ArAutomaticInvoice automaticInvoice,InvoiceIntegration InvoiceInfo)
        {
            //batch

            IcAccountingIntegrationCriteria crit = new IcAccountingIntegrationCriteria();
            ArInvoice arInvoice = new ArInvoice();
            InvoiceBatchList batch = new InvoiceBatchList();

            iInvoiceBatchListManager.Save(batch);
            batch.Description = automaticInvoice.BatchDescription;
            batch.CreatedBy = automaticInvoice.CreatedBy;
            var createdInvoiceBatchId = batch.InvoiceBatchListId;

            //invoice header
            crit = new IcAccountingIntegrationCriteria();
            arInvoice.InvoiceBatchListId = createdInvoiceBatchId;
            //crit.DocumentNumber = automaticInvoice.InvoiceDocumentNumber;
            arInvoice.CustomerId = automaticInvoice.CustomerId;
            arInvoice.DocumentDate = automaticInvoice.DocumentDate;
            arInvoice.PostingDate = automaticInvoice.PostingDate;
            arInvoice.DocumentTypeId = automaticInvoice.DocumentTypeId;
            arInvoice.PaymentTermId = automaticInvoice.PaymentTermId;
            arInvoice.CustomerTaxClassId = automaticInvoice.TaxClassId;
            arInvoice.CreatedBy = automaticInvoice.CreatedBy;
            arInvoice.DiscountAmount = automaticInvoice.DiscountAmount;
            arInvoice.DiscountPercentage = automaticInvoice.DiscountPercentage;
            arInvoice.TotalInWords = automaticInvoice.TotalInWords;
            arInvoice.ExchangeRate = automaticInvoice.ExchangeRate.Value;
            iArInvoiceManager.Save(arInvoice);
            var createdInvoiceId = arInvoice.ArInvoiceId;
          //arInvoice  var createdInvoiceId = this.ExecSPWithReturnValue<int>(IcAccountingIntegration.ARInvoiceAddSPName, crit);

            //invoice details
            var counter = 1;
            foreach (var detailLine in automaticInvoice.DetailLines)
            {
                ArInvoiceDetails arInvoicedetails = new ArInvoiceDetails();

                arInvoicedetails.AccountId = detailLine.AccountId;
                arInvoicedetails.TaxClassId = detailLine.TaxClassId;
                arInvoicedetails.Amount = detailLine.SourceAmount;
                arInvoicedetails.LineNumber = counter;
                arInvoicedetails.ArInvoiceId = createdInvoiceId;
                arInvoicedetails.LineDiscountPercent = detailLine.LineDiscountPercent;
                arInvoicedetails.DiscountAmount = detailLine.DiscountAmount;
                arInvoicedetails.LineTotalAfterDiscount = detailLine.LineTotalAfterDiscount;
                arInvoicedetails.TotalLineAmount = detailLine.TotalLineAmount;
             
                arInvoicedetails.TaxRate = detailLine.TaxRate;

                //this.ExecSPWithReturnValue<int>(IcAccountingIntegration.ARInvoiceDetailAddSPName, crit);
              //  this.ExecSP(IcAccountingIntegration.ARInvoiceDetailAddSPName, crit);
                iArInvoiceDetailsManager.Save(arInvoicedetails);
                counter++;
            }


            iArInvoiceManager.CalucalteInvoiceTotal(createdInvoiceBatchId, createdInvoiceId);

            return createdInvoiceId;
        }
    }
}




