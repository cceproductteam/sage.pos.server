﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CentrixERP.POS.Integration.API.Classes
{
    public class POSShipment
    {
        public string Refference;
        public int CurrencyId;
        public int ArcustomerId;
        public int PriceListId;
        public int locaitonId;
        public double AmountDiscout;
        public double PercentageDiscount;
        public string Note;
        public DateTime? ShipmentDate;
    }

    public class POSShipmentDetails
    {
        public int ItemId;
        public int CategoryId;
        public int UnitOfMeasureId;
        public double Quantity;
        public double UnitPrice;
    }


    public class ShipmentReturnObject
    {
        public string TransReff;
        public bool status;
        public int ShipmentId;

        public int ShipmentOrReceipt;
    }


    public class ReceiptReturnObject
    {
        public string TransReff;
        public bool status;
        public int ReceiptId;
    }
}