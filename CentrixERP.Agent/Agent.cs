﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.ServiceProcess;
using CentrixERP.Agent.Workers;
using System.Configuration;
using SF.Framework;

namespace CentrixERP.Agent
{
    public partial class Agent : ServiceBase
    {
        System.Timers.Timer itemWorker = null;
        System.Timers.Timer SyncRemidarDate = null;


        protected override void OnStart(string[] args)
        {
            Loggin.logPath = ConfigurationManager.AppSettings["AgentLogPath"];
            double workerSleep = ConfigurationManager.AppSettings["WorkerSleepTime"].ToNumber();
            workerSleep = workerSleep * 60 * 1000;
            InitWorkers(workerSleep);


            Loggin.Log("CentrixERP Agent started at " + DateTime.Now.ToString("dd-MM-yyyy hh:mm tt"));
        }

        protected override void OnStop()
        {
            Loggin.Log("CentrixERP Agent stoped at " + DateTime.Now.ToString("dd-MM-yyyy hh:mm tt"));
        }

        private void InitWorkers(double workerSleep)
        {
            itemWorker = new System.Timers.Timer();
            itemWorker.Elapsed += new System.Timers.ElapsedEventHandler(itemWorker_Elapsed);
            itemWorker.Interval = workerSleep;
            itemWorker.Enabled = true;


            SyncRemidarDate = new System.Timers.Timer();
            SyncRemidarDate.Elapsed += new System.Timers.ElapsedEventHandler(SyncRemidarDate_Elapsed);
            SyncRemidarDate.Interval = workerSleep;
            SyncRemidarDate.Enabled = true;
        }

        void itemWorker_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            IcWorker worker = new IcWorker();
            worker.DoWork();
        }

        void SyncRemidarDate_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (!SyncWorker.IsBusy)
            {
                SF.Framework.Logger.Instance.Info("Centrix Sync Worker not busy");
                try
                {
                    SyncWorker.IsBusy = true;
                    SyncWorker worker = new SyncWorker();
                    worker.DoWork();
                    SyncWorker.IsBusy = false;
                }
                catch (Exception ex)
                {
                    SF.Framework.Logger.Instance.Info("Centrix Sync Worker error occured , msg " + ex.Message);
                    SF.Framework.Logger.Instance.Error("Centrix Sync Worker error occured , msg " + ex.Message, ex);
                    SyncWorker.IsBusy = false;
                }
            }
            else
            {
                SF.Framework.Logger.Instance.Info("Centrix Sync Worker is busy");
            }

        }

        public Agent()
        {
            InitializeComponent();
        }
    }
}
