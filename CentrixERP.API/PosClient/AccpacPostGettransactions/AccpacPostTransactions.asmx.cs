﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using SF.Framework;

using CentrixERP.Common.API;
using System.Reflection;
using SagePOS.Server.Configuration;
using SF.FrameworkEntity;

using System.Xml.Serialization;
using POSSage300;

using System.Xml;
using System.IO;
using System.Web.Script.Serialization;
using System.Xml.Linq;
using E = SagePOS.Server.Business.Entity;
using SagePOS.Server.Business.IManager;
using SagePOS.Server.API.Common.BaseClasses;
using SagePOS.Server.Business.Entity;


namespace SagePOS.Server.API
{
    /// <summary>
    /// Summary description for AccpacPostTransactions
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class AccpacPostTransactions : POSWebServiceBaseClass
    {

        IInvoiceManager iInvoiceManager = null;
        IInvoiceItemManager iInvoiceItemManager = null;
        IInvoicePaymentsManager iInvoicePaymentsManager = null;
        IPosSystemConfigrationManager iPosSystemConfigrationManager = null;
        IStoreManager iStoreManager = null;
        IAccpacSyncScheduleManager iAccpacSyncScheduleManager = null;


        POSSage300BOL oIntegrationObj = new POSSage300BOL();
        string userid;
        string pwd;
        string erpCompany;

        string erpMachineName;
        string entity;
        string cashCustomer;

        public AccpacPostTransactions()
        {

            iInvoiceManager = (IInvoiceManager)IoC.Instance.Resolve(typeof(IInvoiceManager));
            iInvoiceItemManager = (IInvoiceItemManager)IoC.Instance.Resolve(typeof(IInvoiceItemManager));
            iInvoicePaymentsManager = (IInvoicePaymentsManager)IoC.Instance.Resolve(typeof(IInvoicePaymentsManager));
            iPosSystemConfigrationManager = (IPosSystemConfigrationManager)IoC.Instance.Resolve(typeof(IPosSystemConfigrationManager));
            iAccpacSyncScheduleManager = (IAccpacSyncScheduleManager)IoC.Instance.Resolve(typeof(IAccpacSyncScheduleManager));
            iStoreManager = (IStoreManager)IoC.Instance.Resolve(typeof(IStoreManager));
            List<E.PosSystemConfigrationLite> PosSystemConfigrationList = iPosSystemConfigrationManager.FindAllLite(null);

            //userid = "ADMIN";
            //pwd = "AMDIN";
            //erpCompany = "SAMDAT";
            //erpMachineName = "CCE-FREE-1";

            userid = PosSystemConfigrationList[0].AccpacUserName;
            pwd = PosSystemConfigrationList[0].AccpacUserPassword;
            erpCompany = PosSystemConfigrationList[0].AccpacCompanyName;
            erpMachineName = PosSystemConfigrationList[0].AccpacServerIp;

            entity = "";
            cashCustomer = PosSystemConfigrationList[0].CashCustomerId;

        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string postInvoicetoAccpac(string invType)
        {

            if (invType == "5")
            {

                E.InvoiceCriteria invoicecriteriaREFund = new E.InvoiceCriteria();
                invoicecriteriaREFund = new E.InvoiceCriteria();
                invoicecriteriaREFund.Status = 5;
                invoicecriteriaREFund.InvoiceStatuses = "5";
                invoicecriteriaREFund.isPostedToAccpac = false;

                List<E.InvoiceLite> myInvoicelistREFund = iInvoiceManager.FindAllLite(invoicecriteriaREFund);

                if (myInvoicelistREFund != null)
                {

                    postAccpacRefund();
                    return "502";

                }
            }
            if (invType == "10")
            {
                E.InvoiceCriteria invoicecriteriaExchange = new E.InvoiceCriteria();
                invoicecriteriaExchange = new E.InvoiceCriteria();
                invoicecriteriaExchange.Status = 10;
                invoicecriteriaExchange.InvoiceStatuses = "10";
                invoicecriteriaExchange.isPostedToAccpac = false;

                List<E.InvoiceLite> myInvoicelistExchange = iInvoiceManager.FindAllLite(invoicecriteriaExchange);


                if (myInvoicelistExchange != null)
                {

                    Exchange();
                    return "502";

                }

            }

            if (invType == "8")
            {
                APPayment();
            }

            string Response = "NO Data";
            string Response1 = "NO Data";

            var results = new { Response, Response1 };

            Payload xmlob = new Payload();
            xmlob = new Payload();
            xmlob.oeorder = new Oeorder();

            xmlob.oeorder.Oeorderdetail = new List<Oeorderdetail>();

            XmlDocument ReturnXml = new XmlDocument();
            //ReturnXml = new XmlDocument();

            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();

            StringWriter textWriter = new StringWriter();
            XmlSerializer xmlSerializer = new XmlSerializer(xmlob.GetType(), new XmlRootAttribute("payload"));

            var emptyNamepsaces = new XmlSerializerNamespaces();
            POSSage300BOL oIntegrationObj = new POSSage300BOL();
            oIntegrationObj = new POSSage300BOL();
            string search = "";
            string Xmlwithoutheder = "";
            string sData = "";
            XElement oXmlNode_SHIPMENTNO = null;
            //Get Order Number
            XElement oXmlNode_OrderNumber = null;
            //Get Customer ID.
            XElement oXmlNode_InvoiceNumber = null;

            XElement oXmlNode_CustomerID = null;
            //Get Order Unique Number
            XElement oXmlNode_ORDUNIQ = null;
            var returnObj = new XmlDocument();
            //System.Xml.Linq.XDocument xDoc = new XDocument();
            XElement oXmlNode_accpacPrePaymnentNumber = null;
            XElement oXmlNode_OrderPrePaymentBatchNo = null;

            string shipmentNumber = "";
            string accpacorderNumber = "";
            string accpacInvoiceNumber = "";
            //prepament varrrr 
            XmlDocument BatchNoXML = new XmlDocument();
            string strBankCode = "";
            string strCurrencyCode = "";
            string OrderPrePaymentXMLBatchNo = "";
            string BatchNo = "";
            string BatchNoToPrePayment = "";
            string OrderPrePaymentBatchNo = "";
            string OrderNumberoeOrderPrepayment = "";
            string AccpacPrePaymnentNumber = "";
            var ReturnObjPrePayment = new XmlDocument();
            XmlDocument ReturnXmlPrePayment = new XmlDocument();
            if (invType == "1,7")
            {
                E.InvoiceCriteria invoicecriteria = new E.InvoiceCriteria();

                XDocument xDoc = new XDocument();
                XDocument xDocPrePaayment = new XDocument();

                invoicecriteria = new E.InvoiceCriteria();
                invoicecriteria.InvoiceStatuses = "1,7";
                invoicecriteria.isPostedToAccpac = false;


                List<E.InvoiceLite> myInvoice = iInvoiceManager.FindAllLite(invoicecriteria);
                if (myInvoice == null)
                    myInvoice = new List<E.InvoiceLite>();
                for (var counter = 0; counter < myInvoice.Count(); counter++)
                {
                    xmlob = new Payload();
                    xmlob.oeorder = new Oeorder();
                    xmlob.oeorder.Oeorderdetail = new List<Oeorderdetail>();
                    E.InvoiceItemCriteria criteria = new E.InvoiceItemCriteria();


                    criteria = new E.InvoiceItemCriteria();
                    criteria.InvoiceId = myInvoice[counter].InvoiceId;
                    StoreLite currenctStore = new StoreLite();
                    currenctStore = iStoreManager.FindByIdLite(myInvoice[counter].StoreId, null);
                    List<E.InvoiceItemLite> myinvoiceitemList = iInvoiceItemManager.FindAllLite(criteria);
                    if (myinvoiceitemList == null)
                        continue;


                    E.InvoicePaymentsCriteria invoicePaymentsCriteria = new E.InvoicePaymentsCriteria();
                    invoicePaymentsCriteria = new E.InvoicePaymentsCriteria();
                    invoicePaymentsCriteria.InvoiceId = myInvoice[counter].InvoiceId;
                    List<E.InvoicePaymentsLite> myinviocepayment = iInvoicePaymentsManager.FindAllLite(invoicePaymentsCriteria);


                    if (myinviocepayment == null)
                        continue;
                    //xmlob.oeOrderPrepayment = new OeOrderPrepayment();

                    xmlob.oeorder.DESC = "POS Transaction";
                    xmlob.oeorder.REFERENCE = myInvoice[counter].InvoiceNumber;
                    xmlob.oeorder.TEMPLATE = "ACTIVE";
                    xmlob.oeorder.ORDDATE = myInvoice[counter].InvoiceDate.Value.ToString("yyyy-MM-dd");

                    if (myInvoice[counter].InvoiceStatus == 7)
                        xmlob.oeorder.CUSTOMER = myInvoice[counter].AccpacCustId;
                    else
                        xmlob.oeorder.CUSTOMER = "NEW";

                    xmlob.oeorder.SHPNAME = " ";
                    xmlob.oeorder.SHPADDR1 = "A";
                    xmlob.oeorder.SHPADDR2 = " ";
                    xmlob.oeorder.SHPADDR3 = " ";
                    xmlob.oeorder.SHPADDR4 = " ";
                    xmlob.oeorder.SHPCITY = " ";
                    xmlob.oeorder.SHPSTATE = " ";
                    xmlob.oeorder.SHPZIP = " ";
                    xmlob.oeorder.SHPCOUNTRY = " ";
                    xmlob.oeorder.SHPPHONE = " ";
                    xmlob.oeorder.SHPEMAIL = "";
                    xmlob.oeorder.BILEMAIL = " ";
                    xmlob.oeorder.SHPSTATE = " ";
                    xmlob.oeorder.SHPCONTACT = " ";
                    xmlob.oeorder.BILNAME = " ";
                    xmlob.oeorder.BILADDR1 = " ";
                    xmlob.oeorder.BILADDR2 = " ";
                    xmlob.oeorder.BILADDR3 = " ";
                    xmlob.oeorder.BILADDR4 = " ";
                    xmlob.oeorder.BILCITY = " ";
                    xmlob.oeorder.BILSTATE = " ";
                    xmlob.oeorder.BILZIP = " ";
                    xmlob.oeorder.BILCOUNTRY = " ";
                    xmlob.oeorder.BILCONTACT = " ";
                    xmlob.oeorder.TYPE = "1";
                    xmlob.oeorder.LOCATION = currenctStore.LocationCode;
                    xmlob.oeorder.INVDISCPER = myInvoice[counter].PercentageDiscount.ToString();

                    //if (xmlob.Oeorder.Oeorderdetail.Count() >=0)
                    //{}
                    //xmlob.oeorder.Oeorderdetail.Add(new Oeorderdetail { LINETYPE = "1", ITEM = "A17600", QTYORDERED = "1", LOCATION = "1", PRIUNTPRC = "100", INVDISC = "0", ORDUNIT = "Box" });
                    if (myinvoiceitemList != null)
                    {

                        foreach (InvoiceItemLite item in myinvoiceitemList)
                        {
                            if (item.IsSerial == false)
                                xmlob.oeorder.Oeorderdetail.Add(new Oeorderdetail { LINETYPE = "1", ITEM = item.ItemNo, UNFMTITEM = item.BarCode, QTYORDERED = item.Quantity.ToString(), PRICELIST = "CANADA", PRIUNTPRC = item.OrginalPrice, LOCATION = currenctStore.LocationCode, INVDISC = Math.Round(item.AmountDiscount, 3).ToString(), ORDUNIT = item.UnitofMeasure });
                            else
                            {

                                Oeorderdetail obj = new Oeorderdetail();
                                obj.OedetailsSerial.Add(new OeDetailsSerial { SERIALNUMF = item.SerialNumber, Quantity = "1" });
                                obj.LINETYPE = "1";
                                obj.ITEM = item.ItemNo;
                                obj.UNFMTITEM = item.BarCode;
                                obj.QTYORDERED = item.Quantity.ToString();
                                obj.PRICELIST = "CANADA";
                                obj.PRIUNTPRC = item.OrginalPrice;
                                obj.LOCATION = currenctStore.LocationCode;
                                obj.INVDISC = Math.Round(item.AmountDiscount, 3).ToString();
                                obj.ORDUNIT = item.UnitofMeasure;

                                xmlob.oeorder.Oeorderdetail.Add(obj);
                            }

                        }
                    }
                    ns.Add("", "");

                    textWriter = new StringWriter();
                    emptyNamepsaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
                    xmlSerializer.Serialize(textWriter, xmlob, emptyNamepsaces);
                    entity = "order";
                    search = "";
                    Xmlwithoutheder = "";
                    Xmlwithoutheder = textWriter.ToString().Replace("\r\n", string.Empty);

                    sData = "";
                    sData = Xmlwithoutheder.Substring(39, Xmlwithoutheder.Length - 39);
                    //string sData = "";
                    //sData = "<payload> <oeorder> <DESC>POSOrder</DESC> <REFERENCE>400000051</REFERENCE> <TEMPLATE>ACTIVE</TEMPLATE> <SHPNAME></SHPNAME> <SHPADDR1>23 Uber Club</SHPADDR1> <SHPADDR2></SHPADDR2> <SHPCITY>Holtsville</SHPCITY> <SHPCOUNTRY>US</SHPCOUNTRY> <SHPZIP>00501</SHPZIP> <SHPPHONE>827341122</SHPPHONE> <SHPEMAIL>JeffBricks@gmai.com</SHPEMAIL> <SHPSTATE>New York</SHPSTATE> <SHPCONTACT>Jeff Bricks</SHPCONTACT> <BILNAME>Jeff Bricks</BILNAME> <BILADDR1>220 Uber Club</BILADDR1> <BILADDR2></BILADDR2> <BILSTATE>New York</BILSTATE> <BILCITY>Holtsville</BILCITY> <BILZIP>00501</BILZIP> <BILCOUNTRY>US</BILCOUNTRY> <BILPHONE>827341122</BILPHONE> <BILCONTACT>Sara Bricks</BILCONTACT> <BILEMAIL>JeffBricks@gmai.com</BILEMAIL> <TYPE>1</TYPE> <LOCATION>1</LOCATION> <GOSHIPALL>1</GOSHIPALL> <OECOMMAND>4</OECOMMAND> <oeorderdetail> <LINETYPE>1</LINETYPE> <ITEM>A1-103/0</ITEM> <UNFMTITEM>A11030</UNFMTITEM> <QTYORDERED>2</QTYORDERED> <LOCATION>1</LOCATION> <PRIUNTPRC>10</PRIUNTPRC> <INVDISC>0.00</INVDISC> <ORDUNIT>Ea.</ORDUNIT> </oeorderdetail> </oeorder> </payload>";
                    returnObj = new XmlDocument();
                    try
                    {
                        returnObj = oIntegrationObj.PushToERP(entity, userid, pwd, erpCompany, erpMachineName, sData);
                    }
                    catch (Exception ex)
                    {

                        throw ex;
                    }


                    //oIntegrationObj.PullERPDataAndPushToPOS("currency", "", "ADMIN", "ADMIN", "SAMDAT", "10.10.16.11");
                    ReturnXml = new XmlDocument();
                    ReturnXml = returnObj;
                    //ReturnXml = oIntegrationObj.PushToERP(entity, userid, pwd, erpCompany, erpMachineName, sData);

                    xDoc = XDocument.Parse(ReturnXml.InnerXml.ToString());
                    Response = "";
                    Response = xDoc.ToString();

                    oXmlNode_SHIPMENTNO = null;
                    //Get Order Number
                    oXmlNode_OrderNumber = null;
                    //Get Customer ID.
                    oXmlNode_InvoiceNumber = null;

                    oXmlNode_CustomerID = null;
                    //Get Order Unique Number
                    oXmlNode_ORDUNIQ = null;


                    shipmentNumber = "";
                    accpacorderNumber = "";
                    accpacInvoiceNumber = "";

                    oXmlNode_SHIPMENTNO = xDoc.Descendants("SHIPMENTNO").SingleOrDefault();
                    //Get Order Number
                    oXmlNode_OrderNumber = xDoc.Descendants("ORDNUMBER").SingleOrDefault();
                    //Get Customer ID.
                    oXmlNode_InvoiceNumber = xDoc.Descendants("INVOICENO").SingleOrDefault();

                    //Get Customer ID.
                    oXmlNode_CustomerID = xDoc.Descendants("CUSTOMER").SingleOrDefault();
                    //Get Order Unique Number
                    oXmlNode_ORDUNIQ = xDoc.Descendants("ORDUNIQ").SingleOrDefault();
                    try
                    {
                        shipmentNumber = oXmlNode_SHIPMENTNO.Value.ToString();
                    }
                    catch (Exception ex)
                    {
                        //return Convert.ToString(oXmlNode_SHIPMENTNO);

                        continue;

                    }


                    accpacorderNumber = oXmlNode_OrderNumber.Value.ToString();
                    accpacInvoiceNumber = oXmlNode_InvoiceNumber.Value.ToString();



                    OrderNumberoeOrderPrepayment = oXmlNode_OrderNumber.Value.ToString();

                    oXmlNode_OrderPrePaymentBatchNo = null;
                    OrderPrePaymentXMLBatchNo = "";
                    for (int q = 0; q < myinviocepayment.Count(); q++)
                    {
                        entity = "orderprepayment";
                        strBankCode = "CCB";
                        strCurrencyCode = myinviocepayment[q].Currency;
                        oXmlNode_OrderPrePaymentBatchNo = null;
                        OrderPrePaymentXMLBatchNo = "";
                        OrderPrePaymentXMLBatchNo = oIntegrationObj.CreateARReceiptNo(userid, pwd, erpCompany, erpMachineName, "Created For POS", strBankCode, strCurrencyCode);

                        try
                        {
                            BatchNo = OrderPrePaymentXMLBatchNo;
                        }
                        catch (Exception ex)
                        {

                            continue;
                        }
                        BatchNoXML.LoadXml(BatchNo);
                        System.Xml.Linq.XDocument xDoc1 = System.Xml.Linq.XDocument.Parse(BatchNoXML.InnerXml.ToString());
                        OrderPrePaymentBatchNo = OrderPrePaymentXMLBatchNo.ToString();
                        oXmlNode_OrderPrePaymentBatchNo = xDoc1.Descendants("ARReceiptBatchNumber").SingleOrDefault();
                        try
                        {
                            BatchNoToPrePayment = oXmlNode_OrderPrePaymentBatchNo.Value.ToString();
                        }
                        catch (Exception)
                        {

                            continue;
                        }

                        double m = myinviocepayment[q].Amount;
                        m = Math.Truncate((m * 1000) / 1000);
                        sData = "<payload>";
                        sData += "<oeOrderPrepayment>";
                        sData += "<ORDUNIQ>" + oXmlNode_ORDUNIQ.Value.ToString() + "</ORDUNIQ>";
                        sData += "<CUSTOMER>" + oXmlNode_CustomerID.Value.ToString() + "</CUSTOMER>";
                        sData += "<BATCHNUM>" + BatchNoToPrePayment + "</BATCHNUM>";
                        sData += "<DOCTOTAL>" + Math.Round(myinviocepayment[q].AvailableAmount, 3).ToString() + "</DOCTOTAL>";
                        sData += "<RECPAMOUNT>" + Math.Round(myinviocepayment[q].AvailableAmount, 3).ToString() + "</RECPAMOUNT>";
                        sData += "<BANKCODE>" + "CCB" + "</BANKCODE>";
                        sData += "<RECPDATE>" + Convert.ToDateTime(myinviocepayment[q].CreatedDate).ToString("yyyy-MM-dd") + "</RECPDATE>";
                        sData += "<RECPTYPE>" + getAccpacPaymentType(myinviocepayment[q].PaymentType, myinviocepayment[q].CreditCardType) + "</RECPTYPE>";
                        sData += "<CHECKNUM>" + "checknum" + "</CHECKNUM>";
                        sData += "<CRATETYPE>" + "SP" + "</CRATETYPE>";
                        sData += "<CRATE>" + "1" + "</CRATE>";
                        sData += "<RATETYPE>" + "SP" + "</RATETYPE>";
                        sData += "<CRATEOPER>" + "1" + "</CRATEOPER>";
                        sData += "</oeOrderPrepayment>";
                        sData += "</payload>";

                        if (myInvoice[q].InvoiceStatus != 7)
                        {
                            entity = "orderprepayment";
                            try
                            {
                                ReturnObjPrePayment = oIntegrationObj.PushToERP(entity, userid, pwd, erpCompany, erpMachineName, sData);
                            }
                            catch (Exception ex)
                            {

                                continue;
                            }


                            ReturnXmlPrePayment = new XmlDocument();
                            ReturnXmlPrePayment = ReturnObjPrePayment;
                            try
                            {

                                xDocPrePaayment = XDocument.Parse(ReturnXmlPrePayment.InnerXml.ToString());
                                Response1 = xDocPrePaayment.ToString();
                                oXmlNode_accpacPrePaymnentNumber = xDocPrePaayment.Descendants("DOCNBR").SingleOrDefault();
                            }

                            catch (Exception ex)
                            {


                                continue;
                            }
                            if (oXmlNode_accpacPrePaymnentNumber != null)
                                AccpacPrePaymnentNumber += oXmlNode_accpacPrePaymnentNumber.Value.ToString() + ",";
                        }

                        iInvoiceManager.UpdateAccpacInvoiceNumber(myInvoice[counter].InvoiceUniqueNumber, accpacInvoiceNumber, accpacorderNumber, shipmentNumber, AccpacPrePaymnentNumber, null);
                    }
                    return "502";
                }
                return "502";
            }
            return "";
        }
            
       


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string getAccpacData(string enitiyName)
        {
            E.AccpacGetentityName entity = this.javaScriptSerializer.Deserialize<E.AccpacGetentityName>(this.unEscape((enitiyName)));

            //string[] accpacentities = {""};
            int RsltGetDate = 501;
            string search = "INACTIVE eq 0";
            XmlDocument ReturnObjGet = new XmlDocument();
            XmlDocument xmlDocGet;

            XDocument xDocGEtFa = new XDocument();
            string ResponseGet;
            try
            {
                if (entity.BankAccount)
                {
                    ReturnObjGet = oIntegrationObj.PullERPDataAndPushToPOS("bankaccount", "", userid, pwd, erpCompany, erpMachineName);
                    RsltGetDate = 200;
                }
                if (entity.Category)
                {
                    ReturnObjGet = oIntegrationObj.PullERPDataAndPushToPOS("category", "", userid, pwd, erpCompany, erpMachineName);

                    RsltGetDate = 200;
                }
                if (entity.Currency)
                {
                    ReturnObjGet = oIntegrationObj.PullERPDataAndPushToPOS("currency", "", userid, pwd, erpCompany, erpMachineName);
                    RsltGetDate = 200;
                }
                if (entity.CurrencyRates)
                {
                    ReturnObjGet = oIntegrationObj.PullERPDataAndPushToPOS("currencyrates", "INACTIVE eq 0", userid, pwd, erpCompany, erpMachineName);
                    RsltGetDate = 200;
                }
                if (entity.Customer)
                {
                    ReturnObjGet = oIntegrationObj.PullERPDataAndPushToPOS("customer", "SWHOLD eq 0", userid, pwd, erpCompany, erpMachineName);
                    RsltGetDate = 200;
                }
                if (entity.GLAccounts)
                {
                    ReturnObjGet = oIntegrationObj.PullERPDataAndPushToPOS("glaccounts", "", userid, pwd, erpCompany, erpMachineName);
                    RsltGetDate = 200;
                }
                if (entity.ItemCard)
                {

                    ReturnObjGet = oIntegrationObj.PullERPDataAndPushToPOS("itemcard", "INACTIVE eq 0", userid, pwd, erpCompany, erpMachineName);
                    ReturnObjGet = oIntegrationObj.PullERPDataAndPushToPOS("itemserials", "INACTIVE eq 0", userid, pwd, erpCompany, erpMachineName);

                    RsltGetDate = 200;

                }
                if (entity.ItemPriceList)
                {
                    ReturnObjGet = oIntegrationObj.PullERPDataAndPushToPOS("itempricelist", "INACTIVE eq 0", userid, pwd, erpCompany, erpMachineName);
                    RsltGetDate = 200;
                }
                if (entity.ItemStock)
                {
                    ReturnObjGet = oIntegrationObj.PullERPDataAndPushToPOS("itemstock", "INACTIVE eq 0", userid, pwd, erpCompany, erpMachineName);

                    RsltGetDate = 200;
                }
                if (entity.ItemTax)
                {
                    ReturnObjGet = oIntegrationObj.PullERPDataAndPushToPOS("itemtax", "INACTIVE eq 0", userid, pwd, erpCompany, erpMachineName);
                    RsltGetDate = 200;
                }
                if (entity.Location)
                {
                    ReturnObjGet = oIntegrationObj.PullERPDataAndPushToPOS("location", "", userid, pwd, erpCompany, erpMachineName);
                    RsltGetDate = 200;
                }
                if (entity.PriceList)
                {
                    ReturnObjGet = oIntegrationObj.PullERPDataAndPushToPOS("pricelist", "", userid, pwd, erpCompany, erpMachineName);
                    RsltGetDate = 200;
                }
                if (entity.TaxAuthorities)
                {
                    ReturnObjGet = oIntegrationObj.PullERPDataAndPushToPOS("taxauthorities", "", userid, pwd, erpCompany, erpMachineName);
                    RsltGetDate = 200;
                }
                if (entity.taxMatrix)
                {
                    ReturnObjGet = oIntegrationObj.PullERPDataAndPushToPOS("taxmatrix", "INACTIVE eq 0", userid, pwd, erpCompany, erpMachineName);
                    RsltGetDate = 200;
                }
                if (entity.TransferData)
                {
                    ReturnObjGet = oIntegrationObj.PullERPDataAndPushToPOS("transferdata", "INACTIVE eq 0", userid, pwd, erpCompany, erpMachineName);
                    RsltGetDate = 200;
                }

                xmlDocGet = new XmlDocument();
                xmlDocGet = ReturnObjGet;
                xDocGEtFa = XDocument.Parse(xmlDocGet.InnerXml.ToString());
                ResponseGet = xDocGEtFa.ToString();


                return ResponseGet + "userid=" + userid + " ,PWD=" + pwd + " ,machinename=" + erpMachineName + ",company=" + erpCompany;

            }

            catch (Exception ex)
            {
                throw ex;
            }
        }


        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string GetAccpacSyncLog(string datefrom, string dateto, string AccpacSyncUniqueNumber)
        {
            List<E.AccpacSyncLogLite> listAccpacSyncLog = new List<E.AccpacSyncLogLite>();
            listAccpacSyncLog = iAccpacSyncScheduleManager.FindAllLiteLog(datefrom, dateto, AccpacSyncUniqueNumber);

            return this.javaScriptSerializer.Serialize(listAccpacSyncLog);



        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string GetAccpacSyncLogDetails(string AccpacSyncUniqueNumber)
        {
            List<E.SyncScheduleDetails> listAccpacSyncLog = new List<E.SyncScheduleDetails>();
            ISyncScheduleManager issyncSchduleManager = (ISyncScheduleManager)IoC.Instance.Resolve(typeof(ISyncScheduleManager));
            listAccpacSyncLog = issyncSchduleManager.getSyncScheduleDetails(AccpacSyncUniqueNumber);
            return this.javaScriptSerializer.Serialize(listAccpacSyncLog);
        }


        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string GetAcitivtySyncLog(string datefrom, string dateto, string AccpacSyncUniqueNumber)
        {
            List<E.AccpacSyncLogLite> listAccpacSyncLog = new List<E.AccpacSyncLogLite>();
            listAccpacSyncLog = iAccpacSyncScheduleManager.FindAllLiteLog(datefrom, dateto, AccpacSyncUniqueNumber);

            return this.javaScriptSerializer.Serialize(listAccpacSyncLog);



        }


        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string getSyncScheduleentityDetails(string entity, int action, string logNumber)
        {
            List<E.SyncLogEntityDetails> listAccpacSyncLog = new List<E.SyncLogEntityDetails>();
            ISyncScheduleManager issyncSchduleManager = (ISyncScheduleManager)IoC.Instance.Resolve(typeof(ISyncScheduleManager));
            listAccpacSyncLog = issyncSchduleManager.getSyncScheduleentityDetails(entity, action, logNumber);

            return this.javaScriptSerializer.Serialize(listAccpacSyncLog);



        }



        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string Exchange()
        {
            string Response = "";
            string Response1 = "";
            string Response2 = "";
            var results = new { Response, Response2 };
            string search = "";
            string Xmlwithoutheder = "";
            string sData = "";
            XElement oXmlNode_SHIPMENTNO = null;
            //Get Order Number
            XElement oXmlNode_OrderNumber = null;
            //Get Customer ID.
            XElement oXmlNode_InvoiceNumber = null;

            XElement oXmlNode_CustomerID = null;
            //Get Order Unique Number
            XElement oXmlNode_ORDUNIQ = null;
            var returnObj = new XmlDocument();
            var returnObj1 = new XmlDocument();
            //System.Xml.Linq.XDocument xDoc = new XDocument();
            XElement oXmlNode_accpacPrePaymnentNumber = null;
            string LocationCode = "3";
            string CustomerId = "NEW";

            int InvoiceIdBYRE = 0;

            XElement oXmlNode_OrderPrePaymentBatchNo = null;

            string strCurrencyCode = "";
            string strBankCode = "";
            string OrderPrePaymentBatchNo = "";
            XmlDocument BatchNoXML = new XmlDocument();
            string shipmentNumber = "";
            string accpacorderNumber = "";
            string accpacInvoiceNumber = "";
            string OrderNumberoeOrderPrepayment = "";
            string accpacOrderUniqueNumber = "";
            E.InvoiceItemCriteria exioviceitemrefund = new E.InvoiceItemCriteria();
            E.InvoiceItemCriteria exioviceitempostinv = new E.InvoiceItemCriteria();
            E.InvoiceCriteria exinvoicecriteria = new E.InvoiceCriteria();

            E.InvoiceLite exinvoice = new E.InvoiceLite();

            exinvoicecriteria = new E.InvoiceCriteria();
            exinvoicecriteria.Status = 10;
            exinvoicecriteria.InvoiceStatuses = "10";

            exinvoicecriteria.isPostedToAccpac = false;

            string OrderPrePaymentXMLBatchNo = "";
            //refund sectiion 



            var Response3 = "";
            var results1 = new { Response2, Response3 };
            Payload xmlserrefund = new Payload();

            xmlserrefund = new Payload();
            xmlserrefund.Oecreditordebitnote = new Oecreditordebitnote();

            xmlserrefund.Oecreditordebitnote.Header = new Header();
            xmlserrefund.Oecreditordebitnote.Header.Detail = new List<Detail>();
            XmlDocument ReturnXml = new XmlDocument();
            XmlDocument ReturnXm1l = new XmlDocument();
            //ReturnXml = new XmlDocument();
            XmlDocument ReturnXmlPrePayment = new XmlDocument();

            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();

            StringWriter textWriter = new StringWriter();
            XmlSerializer xmlSerializer = new XmlSerializer(xmlserrefund.GetType(), new XmlRootAttribute("payload"));

            var emptyNamepsaces = new XmlSerializerNamespaces();
            POSSage300BOL oIntegrationObj = new POSSage300BOL();
            oIntegrationObj = new POSSage300BOL();

            XElement oXmlNode_accpacRefundNumber = null;

            E.InvoiceCriteria invoicecriteria = new E.InvoiceCriteria();

            XDocument xDoc = new XDocument();
            XDocument xDoc2 = new XDocument();
            XDocument xDocPrePaayment = new XDocument();
            XmlDocument ReturnObjPrePayment = new XmlDocument();

            string BatchNoToPrePayment = "";
            string BatchNo = "";
            string InvoiceUniqueNumber = "";
            //end of refund 


            Payload xmlob = new Payload();


            xmlob = new Payload();
            xmlob.oeorder = new Oeorder();

            xmlob.oeorder.Oeorderdetail = new List<Oeorderdetail>();

            List<E.InvoiceLite> exinvoicecriteriaList = iInvoiceManager.FindAllLite(exinvoicecriteria);
            if (exinvoicecriteriaList != null)
            {
                foreach (E.InvoiceLite currentInvoicerefund in exinvoicecriteriaList)
                {

                    StoreLite currenctStore = new StoreLite();
                    currenctStore = iStoreManager.FindByIdLite(currentInvoicerefund.StoreId, null);
                    exioviceitemrefund = new E.InvoiceItemCriteria();
                    exioviceitemrefund.InvoiceId = currentInvoicerefund.InvoiceId;
                    exioviceitemrefund.IsExchanged = true;
                    InvoiceIdBYRE = 0;
                    InvoiceUniqueNumber = "";
                    InvoiceIdBYRE = currentInvoicerefund.InvoiceId;
                    InvoiceUniqueNumber = currentInvoicerefund.InvoiceUniqueNumber;

                    xmlserrefund = new Payload();
                    xmlserrefund.Oecreditordebitnote = new Oecreditordebitnote();
                    xmlserrefund.Oecreditordebitnote.Header = new Header();
                    xmlserrefund.Oecreditordebitnote.Header.Detail = new List<Detail>();
                    xmlserrefund.Oecreditordebitnote.Header.POSID = "";
                    xmlserrefund.Oecreditordebitnote.Header.ADJTYPE = "1";
                    xmlserrefund.Oecreditordebitnote.Header.CUSTOMER = "NEW";
                    xmlserrefund.Oecreditordebitnote.Header.INVNUMBER = currentInvoicerefund.AccpacInvoiceNo;

                    List<E.InvoiceItemLite> myinvoiceitemList = iInvoiceItemManager.FindAllLite(exioviceitemrefund);
                    if (myinvoiceitemList != null)
                    {
                        foreach (E.InvoiceItemLite currentItem in myinvoiceitemList)
                        {
                            if (currentItem.IsSerial == false)
                                xmlserrefund.Oecreditordebitnote.Header.Detail.Add(new Detail { LINETYPE = "1", RETURNTYPE = "1", ITEM = currentItem.ItemNo, LOCATION = currenctStore.LocationCode, QTYRETURN = currentItem.Quantity.ToString(), CRDUNIT = currentItem.UnitofMeasure, PRICELIST = "CANADA", PRIUNTPRC = System.Math.Abs(Math.Round(currentItem.OrginalPrice, 3)), UNITWEIGHT = "0", DISCPER = currentItem.PercentageDiscount.ToString(), SHIPVIA = "CCT", SHIPTRACK = "TRK0001", MANITEMNO = "MA13100", CUSTITEMNO = "A13100", EXTCRDMISC = "" });
                            else
                            {
                                Detail obj = new Detail();
                                obj.OECreditNoteSerial.Add(new OECreditNoteSerial { SERIALNUMF = currentItem.SerialNumber });
                                obj.LINETYPE = "1";
                                obj.RETURNTYPE = "1";
                                obj.ITEM = currentItem.ItemNo;
                                obj.LOCATION = currenctStore.LocationCode;
                                obj.QTYRETURN = currentItem.Quantity.ToString();
                                obj.CRDUNIT = currentItem.UnitofMeasure;
                                obj.PRICELIST = "CANADA";
                                obj.PRIUNTPRC = System.Math.Abs(Math.Round(currentItem.OrginalPrice, 3));
                                obj.UNITWEIGHT = "0";
                                obj.DISCPER = currentItem.PercentageDiscount.ToString();
                                obj.SHIPVIA = "CCT";
                                obj.SHIPTRACK = "TRK0001";
                                obj.MANITEMNO = "MA13100";
                                obj.CUSTITEMNO = "A13100";
                                obj.EXTCRDMISC = "";
                                xmlserrefund.Oecreditordebitnote.Header.Detail.Add(obj);
                            }


                        }
                        textWriter = new StringWriter();
                        emptyNamepsaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
                        xmlSerializer.Serialize(textWriter, xmlserrefund, emptyNamepsaces);
                        returnObj = new XmlDocument();
                        sData = "";
                        entity = "oecreditordebitnote";
                        search = "";
                        Xmlwithoutheder = "";
                        Xmlwithoutheder = textWriter.ToString().Replace("\r\n", string.Empty);

                        sData = "";
                        sData = Xmlwithoutheder.Substring(39, Xmlwithoutheder.Length - 39);

                        //sData = "<payload><oecreditordebitnote><header><POSID></POSID><ADJTYPE>2</ADJTYPE> <CUSTOMER>1200</CUSTOMER><INVNUMBER>IN0000000000157</INVNUMBER>  <detail>  <LINETYPE>1</LINETYPE> <RETURNTYPE>1</RETURNTYPE>  <ITEM>A1-103/0</ITEM><PRICELIST>USA</PRICELIST> <LOCATION>2</LOCATION> <QTYRETURN>2</QTYRETURN><CRDUNIT>Ea.</CRDUNIT><WEIGHTUNIT>lbs.</WEIGHTUNIT> <PRIUNTPRC>15.56</PRIUNTPRC><UNITWEIGHT>16.56</UNITWEIGHT><DISCPER>2</DISCPER> <SHIPVIA>CCT</SHIPVIA> <SHIPTRACK>TRK0001</SHIPTRACK><MANITEMNO>MA13100</MANITEMNO><CUSTITEMNO>A13100</CUSTITEMNO><EXTCRDMISC></EXTCRDMISC> </detail><detail><LINETYPE>2</LINETYPE><ITEM>NTF</ITEM> <EXTCRDMISC>50</EXTCRDMISC><DISCPER>5</DISCPER></detail> </header> </oecreditordebitnote></payload>";
                        returnObj = new XmlDocument();
                        returnObj = oIntegrationObj.PushToERP(entity, userid, pwd, erpCompany, erpMachineName, sData);


                        ReturnXml = new XmlDocument();
                        ReturnXml = returnObj;


                        xDoc = XDocument.Parse(ReturnXml.InnerXml.ToString());

                        string accpacRefundNumber = "";
                        try
                        {
                            oXmlNode_accpacRefundNumber = xDoc.Descendants("CRDNUMBER").SingleOrDefault();
                            accpacRefundNumber = oXmlNode_accpacRefundNumber.Value.ToString();
                        }

                        catch (Exception ex)

                        {

                            continue;

                        }

                        iInvoiceManager.UpdateAccpacInvoiceNumber(currentInvoicerefund.InvoiceUniqueNumber, "", "", "", "", accpacRefundNumber);

                        Response = xDoc.ToString();

                    }
                    xmlob.oeorder.DESC = "POS Transction";
                    xmlob.oeorder.REFERENCE = currentInvoicerefund.InvoiceNumber;
                    xmlob.oeorder.TEMPLATE = "ACTIVE";
                    xmlob.oeorder.ORDDATE = currentInvoicerefund.InvoiceDate.Value.ToString("yyyy-MM-dd");
                    xmlob.oeorder.CUSTOMER = "NEW";
                    xmlob.oeorder.SHPNAME = " ";
                    xmlob.oeorder.SHPADDR1 = "";
                    xmlob.oeorder.SHPADDR2 = " ";
                    xmlob.oeorder.SHPADDR3 = " ";
                    xmlob.oeorder.SHPADDR4 = " ";
                    xmlob.oeorder.SHPCITY = " ";
                    xmlob.oeorder.SHPSTATE = " ";
                    xmlob.oeorder.SHPZIP = " ";
                    xmlob.oeorder.SHPCOUNTRY = " ";
                    xmlob.oeorder.SHPPHONE = " ";
                    xmlob.oeorder.SHPEMAIL = "";
                    xmlob.oeorder.BILEMAIL = " ";
                    xmlob.oeorder.SHPSTATE = " ";
                    xmlob.oeorder.SHPCONTACT = " ";
                    xmlob.oeorder.BILNAME = " ";
                    xmlob.oeorder.BILADDR1 = " ";
                    xmlob.oeorder.BILADDR2 = " ";
                    xmlob.oeorder.BILADDR3 = " ";
                    xmlob.oeorder.BILADDR4 = " ";
                    xmlob.oeorder.BILCITY = " ";
                    xmlob.oeorder.BILSTATE = " ";
                    xmlob.oeorder.BILZIP = " ";
                    xmlob.oeorder.BILCOUNTRY = " ";
                    xmlob.oeorder.BILCONTACT = " ";
                    xmlob.oeorder.TYPE = "1";
                    xmlob.oeorder.LOCATION = currenctStore.LocationCode;
                    xmlob.oeorder.INVDISCPER = currentInvoicerefund.PercentageDiscount.ToString();
                 
                    xmlob.oeorder.OECOMMAND = "1";


                    exioviceitempostinv = new E.InvoiceItemCriteria();
                    exioviceitempostinv.InvoiceId = currentInvoicerefund.InvoiceId;
                    exioviceitempostinv.IsExchanged = false;

                    List<E.InvoiceItemLite> myinvoiceitemListtonew = iInvoiceItemManager.FindAllLite(exioviceitempostinv);
                    if (myinvoiceitemListtonew != null)
                    {
                        foreach (E.InvoiceItemLite currentInvoiceItemnewinv in myinvoiceitemListtonew)
                        {

                            if (currentInvoiceItemnewinv.IsSerial == false)
                                xmlob.oeorder.Oeorderdetail.Add(new Oeorderdetail { LINETYPE = "1", ITEM = currentInvoiceItemnewinv.ItemNo, UNFMTITEM = currentInvoiceItemnewinv.BarCode, QTYORDERED = currentInvoiceItemnewinv.Quantity.ToString(), PRICELIST = "CANADA", PRIUNTPRC = currentInvoiceItemnewinv.OrginalPrice, LOCATION = currenctStore.LocationCode, INVDISC = Math.Round(currentInvoiceItemnewinv.AmountDiscount, 3).ToString(), ORDUNIT = currentInvoiceItemnewinv.UnitofMeasure });
                            else
                            {

                                Oeorderdetail obj = new Oeorderdetail();
                                obj.OedetailsSerial.Add(new OeDetailsSerial { SERIALNUMF = currentInvoiceItemnewinv.SerialNumber, Quantity = "1" });
                                obj.LINETYPE = "1";
                                obj.ITEM = currentInvoiceItemnewinv.ItemNo;
                                obj.UNFMTITEM = currentInvoiceItemnewinv.BarCode;
                                obj.QTYORDERED = currentInvoiceItemnewinv.Quantity.ToString();
                                obj.PRICELIST = "CANADA";
                                obj.PRIUNTPRC = currentInvoiceItemnewinv.OrginalPrice;
                                obj.LOCATION = currenctStore.LocationCode;
                                obj.INVDISC = Math.Round(currentInvoiceItemnewinv.AmountDiscount, 3).ToString();
                                obj.ORDUNIT = currentInvoiceItemnewinv.UnitofMeasure;

                                xmlob.oeorder.Oeorderdetail.Add(obj);




                            }
                        }
                        ns.Add("", "");

                        textWriter = new StringWriter();
                        emptyNamepsaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
                        xmlSerializer.Serialize(textWriter, xmlob, emptyNamepsaces);


                        entity = "order";
                        search = "";
                        Xmlwithoutheder = "";
                        Xmlwithoutheder = textWriter.ToString().Replace("\r\n", string.Empty);

                        sData = "";
                        sData = Xmlwithoutheder.Substring(39, Xmlwithoutheder.Length - 39);

                        returnObj1 = new XmlDocument();
                        returnObj1 = oIntegrationObj.PushToERP(entity, userid, pwd, erpCompany, erpMachineName, sData);

                        ReturnXm1l = new XmlDocument();
                        ReturnXm1l = returnObj1;


                        xDoc2 = XDocument.Parse(ReturnXm1l.InnerXml.ToString());
                        Response2 = "";
                        Response2 = xDoc2.ToString();


                        oXmlNode_SHIPMENTNO = xDoc2.Descendants("SHIPMENTNO").SingleOrDefault();
                        //Get Order Number
                        oXmlNode_OrderNumber = xDoc2.Descendants("ORDNUMBER").SingleOrDefault();
                        //Get Customer ID.
                        oXmlNode_InvoiceNumber = xDoc2.Descendants("INVOICENO").SingleOrDefault();

                        //Get Customer ID.
                        oXmlNode_CustomerID = xDoc2.Descendants("CUSTOMER").SingleOrDefault();
                        //Get Order Unique Number
                        oXmlNode_ORDUNIQ = xDoc2.Descendants("ORDUNIQ").SingleOrDefault();

                        try
                        {
                            shipmentNumber = oXmlNode_SHIPMENTNO.Value.ToString();
                            accpacorderNumber = oXmlNode_OrderNumber.Value.ToString();
                            accpacInvoiceNumber = oXmlNode_InvoiceNumber.Value.ToString();
                            accpacOrderUniqueNumber = oXmlNode_ORDUNIQ.Value.ToString();
                            iInvoiceManager.UpdateAccpacInvoiceNumber(currentInvoicerefund.InvoiceUniqueNumber, accpacInvoiceNumber, accpacorderNumber, shipmentNumber, null, null);
                        }
                        catch (Exception ex)

                        {

                            continue;
                        }



                        if (!OrderPrePaymentXMLBatchNo.Contains("ERROR"))
                        {

                            OrderNumberoeOrderPrepayment = oXmlNode_OrderNumber.Value.ToString();

                        }
                        //iInvoiceManager.UpdateAccpacInvoiceNumber(currentInvoicerefund.InvoiceUniqueNumber, accpacInvoiceNumber, accpacorderNumber, shipmentNumber, null, null);
                        E.InvoicePaymentsCriteria invoicePaymentsCriterianew = new E.InvoicePaymentsCriteria();
                        invoicePaymentsCriterianew = new E.InvoicePaymentsCriteria();
                        invoicePaymentsCriterianew.InvoiceId = InvoiceIdBYRE;
                        List<E.InvoicePaymentsLite> myinviocepaymentnew = iInvoicePaymentsManager.FindAllLite(invoicePaymentsCriterianew);
                        if (myinviocepaymentnew != null)
                        {

                          
                            string AccpacPrePaymnentNumber = "";
                            for (int q = 0; q < myinviocepaymentnew.Count(); q++)
                            {
                                entity = "orderprepayment";
                                strBankCode = "CCB";
                                strCurrencyCode = myinviocepaymentnew[q].Currency;

                                oXmlNode_OrderPrePaymentBatchNo = null;
                                OrderPrePaymentXMLBatchNo = "";
                                OrderPrePaymentXMLBatchNo = oIntegrationObj.CreateARReceiptNo(userid, pwd, erpCompany, erpMachineName, "Created For POS", strBankCode, strCurrencyCode);

                                try
                                {
                                    BatchNo = OrderPrePaymentXMLBatchNo;
                                }
                                catch (Exception ex)
                                {
                                    continue;


                                }
                                BatchNoXML.LoadXml(BatchNo);
                                System.Xml.Linq.XDocument xDoc1 = System.Xml.Linq.XDocument.Parse(BatchNoXML.InnerXml.ToString());
                                OrderPrePaymentBatchNo = OrderPrePaymentXMLBatchNo.ToString();
                                oXmlNode_OrderPrePaymentBatchNo = xDoc1.Descendants("ARReceiptBatchNumber").SingleOrDefault();
                                try
                                {
                                    BatchNoToPrePayment = oXmlNode_OrderPrePaymentBatchNo.Value.ToString();
                                }
                                catch (Exception ex)
                                {
                                    continue;
                                }
                                double m = myinviocepaymentnew[q].Amount;
                                m = Math.Truncate((m * 1000) / 1000);
                                sData = "<payload>";
                                sData += "<oeOrderPrepayment>";
                                sData += "<ORDUNIQ>" + oXmlNode_ORDUNIQ.Value.ToString() + "</ORDUNIQ>";
                                sData += "<CUSTOMER>" + oXmlNode_CustomerID.Value.ToString() + "</CUSTOMER>";
                                sData += "<BATCHNUM>" + BatchNoToPrePayment + "</BATCHNUM>";
                                sData += "<DOCTOTAL>" + Math.Round(myinviocepaymentnew[q].Amount, 3).ToString() + "</DOCTOTAL>";
                                sData += "<RECPAMOUNT>" + Math.Round(myinviocepaymentnew[q].Amount, 3).ToString() + "</RECPAMOUNT>";
                                sData += "<BANKCODE>" + "CCB" + "</BANKCODE>";
                                sData += "<RECPDATE>" + Convert.ToDateTime(myinviocepaymentnew[q].CreatedDate).ToString("yyyy-MM-dd") + "</RECPDATE>";
                                sData += "<RECPTYPE>" + getAccpacPaymentType(myinviocepaymentnew[q].PaymentType, myinviocepaymentnew[q].CreditCardType) + "</RECPTYPE>";
                                sData += "<CHECKNUM>" + "checknum" + "</CHECKNUM>";
                                sData += "<CRATETYPE>" + "SP" + "</CRATETYPE>";
                                sData += "<CRATE>" + "1" + "</CRATE>";
                                sData += "<RATETYPE>" + "SP" + "</RATETYPE>";
                                sData += "<CRATEOPER>" + "1" + "</CRATEOPER>";
                                sData += "</oeOrderPrepayment>";
                                sData += "</payload>";


                                entity = "orderprepayment";


                                try
                                {
                                    ReturnObjPrePayment = oIntegrationObj.PushToERP(entity, userid, pwd, erpCompany, erpMachineName, sData);

                                }
                                catch (Exception ex)
                                {
                                    continue;

                                }
                                ReturnXmlPrePayment = new XmlDocument();
                                ReturnXmlPrePayment = ReturnObjPrePayment;
                                try
                                {

                                    xDocPrePaayment = XDocument.Parse(ReturnXmlPrePayment.InnerXml.ToString());
                                    Response1 = xDocPrePaayment.ToString();
                                    oXmlNode_accpacPrePaymnentNumber = xDocPrePaayment.Descendants("DOCNBR").SingleOrDefault();
                                }

                                catch (Exception ex)
                                {
                                    continue;
                                }
                                if (oXmlNode_accpacPrePaymnentNumber != null)
                                    AccpacPrePaymnentNumber += oXmlNode_accpacPrePaymnentNumber.Value.ToString() + ",";
                            }



                            iInvoiceManager.UpdateAccpacInvoiceNumber(currentInvoicerefund.InvoiceUniqueNumber, accpacInvoiceNumber, accpacorderNumber, shipmentNumber, AccpacPrePaymnentNumber, null);

                        }

                    }

                }
            }
            results = new { Response, Response2 };

            return results.ToString();
        }
 















        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string getinvoicetype(int stu)
        {

            E.InvoiceCriteria invoicecriteriacheck = new E.InvoiceCriteria();
            invoicecriteriacheck = new E.InvoiceCriteria();
            invoicecriteriacheck.Status = stu;
            invoicecriteriacheck.isPostedToAccpac = false;

            List<E.InvoiceLite> myInvoiceinvoicecriteriacheck = iInvoiceManager.FindAllLite(invoicecriteriacheck);

            try
            {
                return myInvoiceinvoicecriteriacheck[0].InvoiceUniqueNumber;
            }
            catch (Exception ex)
            {

                return ex.ToString();
            }
        }
        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string postAccpacRefund()
        {
            string Response = "";
            string Xmlwithoutheder;

            string Response1 = "";
            var results = new { Response, Response1 };
            Payload xmlserrefund = new Payload();

            xmlserrefund = new Payload();
            xmlserrefund.Oecreditordebitnote = new Oecreditordebitnote();

            xmlserrefund.Oecreditordebitnote.Header = new Header();
            xmlserrefund.Oecreditordebitnote.Header.Detail = new List<Detail>();
            XmlDocument ReturnXml = new XmlDocument();
            //ReturnXml = new XmlDocument();

            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();

            StringWriter textWriter = new StringWriter();
            XmlSerializer xmlSerializer = new XmlSerializer(xmlserrefund.GetType(), new XmlRootAttribute("payload"));

            var emptyNamepsaces = new XmlSerializerNamespaces();
            POSSage300BOL oIntegrationObj = new POSSage300BOL();
            oIntegrationObj = new POSSage300BOL();

            XElement oXmlNode_accpacRefundNumber = null;

            E.InvoiceCriteria invoicecriteria = new E.InvoiceCriteria();

            XDocument xDoc = new XDocument();
            XDocument xDocPrePaayment = new XDocument();

            invoicecriteria = new E.InvoiceCriteria();
            invoicecriteria.Status = 5;
            invoicecriteria.InvoiceStatuses = "5";
            invoicecriteria.isPostedToAccpac = false;

            List<E.InvoiceLite> myInvoicelist = iInvoiceManager.FindAllLite(invoicecriteria);
            string sData = "";

            foreach (E.InvoiceLite currentInvoice in myInvoicelist)
            {
                StoreLite currenctStore = new StoreLite();
                currenctStore = iStoreManager.FindByIdLite(currentInvoice.StoreId, null);
                E.InvoiceCriteria invcriteria = new InvoiceCriteria();
                invcriteria.StoreId = currentInvoice.StoreId;
                E.InvoiceLite OriginalInvoice = iInvoiceManager.FindByIdLite(currentInvoice.ParentInvoiceId, invcriteria);
                if (OriginalInvoice == null || string.IsNullOrEmpty(OriginalInvoice.AccpacInvoiceNo))
                    continue;
                xmlserrefund = new Payload();
                xmlserrefund.Oecreditordebitnote = new Oecreditordebitnote();
                xmlserrefund.Oecreditordebitnote.Header = new Header();
                xmlserrefund.Oecreditordebitnote.Header.Detail = new List<Detail>();
                xmlserrefund.Oecreditordebitnote.Header.POSID = "";
                xmlserrefund.Oecreditordebitnote.Header.ADJTYPE = "1";
                xmlserrefund.Oecreditordebitnote.Header.CUSTOMER = "NEW";
                xmlserrefund.Oecreditordebitnote.Header.INVNUMBER = OriginalInvoice.AccpacInvoiceNo;
                xmlserrefund.Oecreditordebitnote.Header.CRDDATE = currentInvoice.InvoiceDate.Value.ToString("yyyy-MM-dd");
                xmlserrefund.Oecreditordebitnote.Header.POSTDATE = currentInvoice.InvoiceDate.Value.ToString("yyyy-MM-dd");
                xmlserrefund.Oecreditordebitnote.Header.CRDDISCPER = currentInvoice.PercentageDiscount.ToString();
                E.InvoiceItemCriteria criteria = new E.InvoiceItemCriteria();


                criteria = new E.InvoiceItemCriteria();
                criteria.InvoiceId = currentInvoice.InvoiceId;
                List<E.InvoiceItemLite> myinvoiceitemList = iInvoiceItemManager.FindAllLite(criteria);


                if (myinvoiceitemList.Count > 0)
                {

                    foreach (E.InvoiceItemLite currentItem in myinvoiceitemList)
                    {

                        if (currentItem.IsSerial == false)
                            xmlserrefund.Oecreditordebitnote.Header.Detail.Add(new Detail { LINETYPE = "1", RETURNTYPE = "1", ITEM = currentItem.ItemNo, LOCATION = currenctStore.LocationCode, QTYRETURN = currentItem.Quantity.ToString(), CRDUNIT = currentItem.UnitofMeasure, PRICELIST = "CANADA", PRIUNTPRC = System.Math.Abs(currentItem.OrginalPrice), UNITWEIGHT = "0", DISCPER = currentItem.PercentageDiscount.ToString(), SHIPVIA = "CCT", SHIPTRACK = "TRK0001", MANITEMNO = "MA13100", CUSTITEMNO = "A13100", EXTCRDMISC = "" });
                        else
                        {
                            Detail obj = new Detail();
                            obj.OECreditNoteSerial.Add(new OECreditNoteSerial { SERIALNUMF = currentItem.SerialNumber });
                            obj.LINETYPE = "1";
                            obj.RETURNTYPE = "1";
                            obj.ITEM = currentItem.ItemNo;
                            obj.LOCATION = currenctStore.LocationCode;
                            obj.QTYRETURN = currentItem.Quantity.ToString();
                            obj.CRDUNIT = currentItem.UnitofMeasure;
                            obj.PRICELIST = "CANADA";
                            obj.PRIUNTPRC = System.Math.Abs(Math.Round(currentItem.OrginalPrice, 3));
                            obj.UNITWEIGHT = "0";
                            obj.DISCPER = currentItem.PercentageDiscount.ToString();
                            obj.SHIPVIA = "CCT";
                            obj.SHIPTRACK = "TRK0001";
                            obj.MANITEMNO = "MA13100";
                            obj.CUSTITEMNO = "A13100";
                            obj.EXTCRDMISC = "";
                            xmlserrefund.Oecreditordebitnote.Header.Detail.Add(obj);
                        }

                    }

                }


                //ReturnXml = new XmlDocument();

                textWriter = new StringWriter();
                emptyNamepsaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
                xmlSerializer.Serialize(textWriter, xmlserrefund, emptyNamepsaces);

                var returnObj = new XmlDocument();

                entity = "oecreditordebitnote";
                string search = "";
                Xmlwithoutheder = "";
                Xmlwithoutheder = textWriter.ToString().Replace("\r\n", string.Empty);

                sData = "";
                sData = Xmlwithoutheder.Substring(39, Xmlwithoutheder.Length - 39);

                //sData = "<payload><oecreditordebitnote><header><POSID></POSID><ADJTYPE>2</ADJTYPE> <CUSTOMER>1200</CUSTOMER><INVNUMBER>IN0000000000157</INVNUMBER>  <detail>  <LINETYPE>1</LINETYPE> <RETURNTYPE>1</RETURNTYPE>  <ITEM>A1-103/0</ITEM><PRICELIST>USA</PRICELIST> <LOCATION>2</LOCATION> <QTYRETURN>2</QTYRETURN><CRDUNIT>Ea.</CRDUNIT><WEIGHTUNIT>lbs.</WEIGHTUNIT> <PRIUNTPRC>15.56</PRIUNTPRC><UNITWEIGHT>16.56</UNITWEIGHT><DISCPER>2</DISCPER> <SHIPVIA>CCT</SHIPVIA> <SHIPTRACK>TRK0001</SHIPTRACK><MANITEMNO>MA13100</MANITEMNO><CUSTITEMNO>A13100</CUSTITEMNO><EXTCRDMISC></EXTCRDMISC> </detail><detail><LINETYPE>2</LINETYPE><ITEM>NTF</ITEM> <EXTCRDMISC>50</EXTCRDMISC><DISCPER>5</DISCPER></detail> </header> </oecreditordebitnote></payload>";
                returnObj = new XmlDocument();
                returnObj = oIntegrationObj.PushToERP(entity, userid, pwd, erpCompany, erpMachineName, sData);


                ReturnXml = new XmlDocument();
                ReturnXml = returnObj;


                xDoc = XDocument.Parse(ReturnXml.InnerXml.ToString());

                string accpacRefundNumber = "";
                oXmlNode_accpacRefundNumber = xDoc.Descendants("CRDNUMBER").SingleOrDefault();
                try
                {
                    accpacRefundNumber = oXmlNode_accpacRefundNumber.Value.ToString();
                }
                catch (Exception ex)
                {

                    continue;
                }


                iInvoiceManager.UpdateAccpacInvoiceNumber(currentInvoice.InvoiceUniqueNumber, "", "", "", "", accpacRefundNumber);


            }
            //return textWriter.ToString().Replace("\r\n", string.Empty);


            return sData;
        }




        public string APPayment()
        {
            string r = "";
            string sData = "";
            XmlDocument Returnre = new XmlDocument();

            E.InvoiceCriteria invoicecriteriaAP = new E.InvoiceCriteria();
            E.InvoiceItemCriteria invoiceitemcriteriaAP = new E.InvoiceItemCriteria();
            Payload XMLAPPayment = new Payload();
            XMLAPPayment = new Payload();
            XMLAPPayment.Appayments = new Appayments();
            XMLAPPayment.Appayments.Appaymentheader = new Appaymentheader();
            XMLAPPayment.Appayments.Appaymentheader.Appaymentdetail = new List<Appaymentdetail>();

            XmlDocument ReturnXmlAP = new XmlDocument();
            //ReturnXml = new XmlDocument();

            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();

            StringWriter textWriter = new StringWriter();
            XmlSerializer xmlSerializer = new XmlSerializer(XMLAPPayment.GetType(), new XmlRootAttribute("payload"));

            var emptyNamepsaces = new XmlSerializerNamespaces();
            POSSage300BOL oIntegrationObj = new POSSage300BOL();


            XmlDocument BatchNoXMLAP = new XmlDocument();
            entity = "appayment";
            string strBankCode = "CCB";
            string strCurrencyCode = "CAD";
            oIntegrationObj = new POSSage300BOL();



            invoicecriteriaAP = new E.InvoiceCriteria();

            invoicecriteriaAP.InvoiceStatuses = "8";

            invoicecriteriaAP.isPostedToAccpac = false;
            //List<E.InvoiceLite> myInvoicelistAR = new List<E.InvoiceLite>();
            //myInvoicelistAR = new List<E.InvoiceLite>();
            List<E.InvoiceLite> myInvoicelistAP = iInvoiceManager.FindAllLite(invoicecriteriaAP);
            //if (myInvoicelistAP == null)

            //{


            //}
            foreach (E.InvoiceLite APInvoiceList in myInvoicelistAP)
            {

                string strAPXMLBatchNo = oIntegrationObj.CreateAPPaymentBatchNo(userid, pwd, erpCompany, erpMachineName, "Created For POS", strBankCode, strCurrencyCode);
                string BatchNoAP = "";
                string APBatchNo = "";


                XElement oXmlNode_APBatchNoBatchNo = null;
                BatchNoXMLAP = new XmlDocument();
                if (strAPXMLBatchNo.Contains("ERROR"))
                    continue;

                BatchNoAP = strAPXMLBatchNo;

                BatchNoXMLAP.LoadXml(BatchNoAP);

                System.Xml.Linq.XDocument xDoc1AP = System.Xml.Linq.XDocument.Parse(BatchNoXMLAP.InnerXml.ToString());

                APBatchNo = strAPXMLBatchNo.ToString();

                oXmlNode_APBatchNoBatchNo = xDoc1AP.Descendants("APPaymentBatchNumber").SingleOrDefault();
                string PARABatchNoAR = "";


                PARABatchNoAR = oXmlNode_APBatchNoBatchNo.Value.ToString();


                XMLAPPayment.Appayments.APBATCHNO = PARABatchNoAR;
                XMLAPPayment.Appayments.Appaymentheader.RMITTYPE = "4";
                XMLAPPayment.Appayments.Appaymentheader.TEXTRMIT = "Created from POS";
                XMLAPPayment.Appayments.Appaymentheader.IDVEND = "100";
                XMLAPPayment.Appayments.Appaymentheader.IDRMITTO = "POBOX";
                XMLAPPayment.Appayments.Appaymentheader.NAMERMIT = "1000";
                XMLAPPayment.Appayments.Appaymentheader.DATERMIT = Convert.ToDateTime(APInvoiceList.CreatedDate).ToString("yyyy-MM-dd");
                XMLAPPayment.Appayments.Appaymentheader.DATEBUS = Convert.ToDateTime(APInvoiceList.InvoiceDate).ToString("yyyy-MM-dd");
                XMLAPPayment.Appayments.Appaymentheader.PAYMCODE = "CASH";
                XMLAPPayment.Appayments.Appaymentheader.CASHACCT = Math.Round(Math.Abs((double)APInvoiceList.TotalPrice), 3);
                XMLAPPayment.Appayments.Appaymentheader.DOCTYPE = "2";
                XMLAPPayment.Appayments.Appaymentheader.DATEACTVPP = "2016-12-04";
                XMLAPPayment.Appayments.Appaymentheader.IDINVCMTCH = "DOC198";
                XMLAPPayment.Appayments.Appaymentheader.AMTPPAYTC = "100";

                // invoiceitemcriteriaAP = new E.InvoiceItemCriteria();
                // invoiceitemcriteriaAP.InvoiceId = APInvoiceList.InvoiceId;

                //List<E.InvoiceItemLite> myInvoiceIemlistAP = iInvoiceItemManager.FindAllLite(invoiceitemcriteriaAP);
                //foreach (E.InvoiceItemLite APInvoiceItemLiteList in myInvoiceIemlistAP)
                //{
                //XMLAPPayment.Appayments.Appaymentheader.Appaymentdetail.Add(new Appaymentdetail { GLDESC = "GLDESC", IDDISTCODE = "AMEX", IDACCT = "6010", AMTDISTTC = "0", GLREF = "est 0001" });

                //}


                textWriter = new StringWriter();
                emptyNamepsaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
                xmlSerializer.Serialize(textWriter, XMLAPPayment, emptyNamepsaces);

                string Xmlwithoutheder = "";
                Xmlwithoutheder = textWriter.ToString().Replace("\r\n", string.Empty);


                sData = Xmlwithoutheder.Substring(39, Xmlwithoutheder.Length - 39);
                XDocument xDocAP = new XDocument();
                xDocAP = XDocument.Parse(sData.ToString());
                if (!strAPXMLBatchNo.Contains("ERROR"))
                {
                    //Get new AP Payment Batch Number
                    XmlDocument APBatchNoXML = new XmlDocument();
                    APBatchNoXML.LoadXml(strAPXMLBatchNo);
                    string strAPPaymentBatchNo = APBatchNoXML.GetElementsByTagName("APPaymentBatchNumber").Item(0).InnerText;

                    XElement node = (from x in xDocAP.Descendants("APBATCHNO") select x).SingleOrDefault();

                    node.SetValue(PARABatchNoAR);
                    sData = string.Empty;
                    sData = xDocAP.ToString();
                    //sData = "<payload> <appayments> <APBATCHNO>82</APBATCHNO> <appaymentheader> <RMITTYPE>4</RMITTYPE> <TEXTRMIT>Created from POS</TEXTRMIT> <IDVEND>1200</IDVEND> <IDRMITTO>POBOX</IDRMITTO> <NAMERMIT>1000</NAMERMIT> <DATERMIT>2016-08-17</DATERMIT> <DATEBUS>2016-08-17</DATEBUS> <PAYMCODE>CASH</PAYMCODE> <CASHACCT /> <DOCTYPE>2016-06-17</DOCTYPE> <IDINVCMTCH>DOC198</IDINVCMTCH> <appaymentdetail> <GLDESC>GLDESC</GLDESC> <IDDISTCODE>AMEX</IDDISTCODE> <IDACCT>6010</IDACCT> <AMTDISTTC>0</AMTDISTTC> <GLREF>est 0001</GLREF> </appaymentdetail> </appaymentheader> </appayments> </payload>";     

                    ReturnXmlAP = oIntegrationObj.PushToERP(entity, userid, pwd, erpCompany, erpMachineName, sData);


                    Returnre = new XmlDocument();
                    Returnre = ReturnXmlAP;

                    XDocument xDocAPt = new XDocument();
                    xDocAPt = XDocument.Parse(Returnre.InnerXml.ToString());
                    decimal strAPBatchNo = Convert.ToDecimal(PARABatchNoAR);

                    iInvoiceManager.UpdateAccpacInvoiceNumber(APInvoiceList.InvoiceUniqueNumber, "", "", "", "", PARABatchNoAR);

                    //string PostingMsg = oIntegrationObj.PostAPPaymentBatch(userid, pwd, erpCompany, erpMachineName, strAPBatchNo); 




                    r = xDocAPt.ToString();





                }
            }
            return r;

        }




        private string getAccpacEntityName(int entityId)
        {
            string entity = "";
            if (entityId == 1)
                entity = "bankaccount";
            else if (entityId == 2)
                entity = "category";
            else if (entityId == 3)
                entity = "currency";
            else if (entityId == 4)
                entity = "currencyrates";
            else if (entityId == 5)
                entity = "customer";
            else if (entityId == 6)
                entity = "glaccounts";
            else if (entityId == 7)
                entity = "itemcard";
            else if (entityId == 8)
                entity = "itempricelist";
            else if (entityId == 9)
                entity = "itemserials";
            else if (entityId == 10)
                entity = "itemstock";
            else if (entityId == 11)
                entity = "itemtax";
            else if (entityId == 12)
                entity = "location";
            else if (entityId == 13)
                entity = "pricelist";
            else if (entityId == 14)
                entity = "taxauthorities";
            else if (entityId == 15)
                entity = "taxmatrix";
            else if (entityId == 16)
                entity = "transferdata";
            return entity;
        }





        [Serializable]
        [XmlRoot(ElementName = "oeorderdetail")]
        public class Oeorderdetail
        {

            [XmlElement(ElementName = "LINETYPE", Order = 1)]
            public string LINETYPE { get; set; }
            [XmlElement(ElementName = "ITEM", Order = 2)]
            public string ITEM { get; set; }
            [XmlElement(ElementName = "UNFMTITEM", Order = 3)]
            public string UNFMTITEM { get; set; }
            [XmlElement(ElementName = "QTYORDERED", Order = 4)]
            public string QTYORDERED { get; set; }
            [XmlElement(ElementName = "LOCATION", Order = 5)]
            public string LOCATION { get; set; }
            [XmlElement(ElementName = "PRICELIST", Order = 6)]
            public string PRICELIST { get; set; }
            [XmlElement(ElementName = "PRIUNTPRC", Order = 7)]
            public double PRIUNTPRC { get; set; }
            [XmlElement(ElementName = "INVDISC", Order = 8)]
            public string INVDISC { get; set; }
            [XmlElement(ElementName = "ORDUNIT", Order = 9)]
            public string ORDUNIT { get; set; }
            [XmlElement(ElementName = "serialnumber", Order = 10)]
            public List<OeDetailsSerial> OedetailsSerial = new List<OeDetailsSerial>();



        }

        [Serializable]
        [XmlRoot(ElementName = "serialnumber")]
        public class OeDetailsSerial
        {
            [XmlElement(ElementName = "SERIALNUMF")]
            public string SERIALNUMF { get; set; }
            [XmlElement(ElementName = "QTY")]
            public string Quantity { get; set; }

        }

        [XmlRoot(ElementName = "oeorder")]
        public class Oeorder
        {
            [XmlElement(ElementName = "CUSTOMER")]
            public string CUSTOMER { get; set; }
            [XmlElement(ElementName = "DESC")]
            public string DESC { get; set; }
            [XmlElement(ElementName = "REFERENCE")]
            public string REFERENCE { get; set; }

            [XmlElement(ElementName = "ORDDATE")]
            public string ORDDATE { get; set; }
            [XmlElement(ElementName = "TEMPLATE")]
            public string TEMPLATE { get; set; }
            [XmlElement(ElementName = "SHPNAME")]
            public string SHPNAME { get; set; }
            [XmlElement(ElementName = "SHPADDR1")]
            public string SHPADDR1 { get; set; }
            [XmlElement(ElementName = "SHPADDR2")]
            public string SHPADDR2 { get; set; }

            [XmlElement(ElementName = "SHPADDR3")]
            public string SHPADDR3 { get; set; }
            [XmlElement(ElementName = "SHPADDR4")]
            public string SHPADDR4 { get; set; }
            [XmlElement(ElementName = "SHPCITY")]
            public string SHPCITY { get; set; }
            [XmlElement(ElementName = "SHPCOUNTRY")]
            public string SHPCOUNTRY { get; set; }
            [XmlElement(ElementName = "SHPZIP")]
            public string SHPZIP { get; set; }
            [XmlElement(ElementName = "SHPPHONE")]
            public string SHPPHONE { get; set; }
            [XmlElement(ElementName = "SHPEMAIL")]
            public string SHPEMAIL { get; set; }
            [XmlElement(ElementName = "SHPSTATE")]
            public string SHPSTATE { get; set; }
            [XmlElement(ElementName = "SHPCONTACT")]
            public string SHPCONTACT { get; set; }
            [XmlElement(ElementName = "BILNAME")]
            public string BILNAME { get; set; }
            [XmlElement(ElementName = "BILADDR1")]
            public string BILADDR1 { get; set; }
            [XmlElement(ElementName = "BILADDR2")]
            public string BILADDR2 { get; set; }
            [XmlElement(ElementName = "BILADDR3")]
            public string BILADDR3 { get; set; }
            [XmlElement(ElementName = "BILADDR4")]
            public string BILADDR4 { get; set; }
            [XmlElement(ElementName = "BILSTATE")]
            public string BILSTATE { get; set; }
            [XmlElement(ElementName = "BILCITY")]
            public string BILCITY { get; set; }
            [XmlElement(ElementName = "BILZIP")]
            public string BILZIP { get; set; }
            [XmlElement(ElementName = "BILCOUNTRY")]
            public string BILCOUNTRY { get; set; }
            [XmlElement(ElementName = "BILPHONE")]
            public string BILPHONE { get; set; }
            [XmlElement(ElementName = "BILCONTACT")]
            public string BILCONTACT { get; set; }
            [XmlElement(ElementName = "BILEMAIL")]
            public string BILEMAIL { get; set; }
            [XmlElement(ElementName = "TYPE")]
            public string TYPE { get; set; }
            [XmlElement(ElementName = "LOCATION")]
            public string LOCATION { get; set; }

            [XmlElement(ElementName= "INVDISCPER")]
            public string INVDISCPER { get; set; }

            [XmlElement(ElementName = "GOSHIPALL")]
            public string GOSHIPALL { get; set; }
            [XmlElement(ElementName = "OECOMMAND")]
            public string OECOMMAND { get; set; }
            [XmlElement(ElementName = "oeorderdetail")]
            public List<Oeorderdetail> Oeorderdetail { get; set; }


        }
        [XmlRoot(ElementName = "oeOrderPrepayment")]
        public class OeOrderPrepayment
        {
            [XmlElement(ElementName = "ORDUNIQ")]
            public string ORDUNIQ { get; set; }
            [XmlElement(ElementName = "CUSTOMER")]
            public string CUSTOMER { get; set; }
            [XmlElement(ElementName = "BATCHNUM")]
            public string BATCHNUM { get; set; }
            [XmlElement(ElementName = "DOCTOTAL")]
            public string DOCTOTAL { get; set; }
            [XmlElement(ElementName = "RECPAMOUNT")]
            public string RECPAMOUNT { get; set; }
            [XmlElement(ElementName = "BANKCODE")]
            public string BANKCODE { get; set; }
            [XmlElement(ElementName = "RECPDATE")]
            public string RECPDATE { get; set; }
            [XmlElement(ElementName = "RECPTYPE")]
            public string RECPTYPE { get; set; }
            [XmlElement(ElementName = "CHECKNUM")]
            public string CHECKNUM { get; set; }
            [XmlElement(ElementName = "CRATETYPE")]
            public string CRATETYPE { get; set; }
            [XmlElement(ElementName = "CRATE")]
            public string CRATE { get; set; }
            [XmlElement(ElementName = "RATETYPE")]
            public string RATETYPE { get; set; }
            [XmlElement(ElementName = "CRATEOPER")]
            public string CRATEOPER { get; set; }
        }



        [XmlRoot(ElementName = "detail")]
        public class Detail
        {
            [XmlElement(ElementName = "LINETYPE", Order = 1)]
            public string LINETYPE { get; set; }
            [XmlElement(ElementName = "RETURNTYPE", Order = 2)]
            public string RETURNTYPE { get; set; }
            [XmlElement(ElementName = "ITEM", Order = 3)]
            public string ITEM { get; set; }
            [XmlElement(ElementName = "PRICELIST", Order = 4)]
            public string PRICELIST { get; set; }
            [XmlElement(ElementName = "LOCATION", Order = 5)]
            public string LOCATION { get; set; }
            [XmlElement(ElementName = "QTYRETURN", Order = 6)]
            public string QTYRETURN { get; set; }
            [XmlElement(ElementName = "CRDUNIT", Order = 7)]
            public string CRDUNIT { get; set; }
            [XmlElement(ElementName = "PRIUNTPRC", Order = 8)]
            public double PRIUNTPRC { get; set; }
            [XmlElement(ElementName = "UNITWEIGHT", Order = 9)]
            public string UNITWEIGHT { get; set; }
            [XmlElement(ElementName = "DISCPER", Order = 10)]
            public string DISCPER { get; set; }
            [XmlElement(ElementName = "SHIPVIA", Order = 11)]
            public string SHIPVIA { get; set; }
            [XmlElement(ElementName = "SHIPTRACK", Order = 12)]
            public string SHIPTRACK { get; set; }
            [XmlElement(ElementName = "MANITEMNO", Order = 13)]
            public string MANITEMNO { get; set; }
            [XmlElement(ElementName = "CUSTITEMNO", Order = 14)]
            public string CUSTITEMNO { get; set; }
            [XmlElement(ElementName = "EXTCRDMISC", Order = 15)]
            public string EXTCRDMISC { get; set; }
            [XmlElement(ElementName = "WEIGHTUNIT", Order = 16)]
            public string WEIGHTUNIT { get; set; }
            [XmlElement(ElementName = "serialnumber", Order = 17)]
            public List<OECreditNoteSerial> OECreditNoteSerial = new List<AccpacPostTransactions.OECreditNoteSerial>();
        }


        [XmlRoot(ElementName = "header")]
        public class Header
        {
            [XmlElement(ElementName = "POSID")]
            public string POSID { get; set; }
            [XmlElement(ElementName = "ADJTYPE")]
            public string ADJTYPE { get; set; }
            [XmlElement(ElementName = "CUSTOMER")]
            public string CUSTOMER { get; set; }
            [XmlElement(ElementName = "INVNUMBER")]
            public string INVNUMBER { get; set; }

            [XmlElement(ElementName = "CRDDATE")]
            public string CRDDATE { get; set; }

            [XmlElement(ElementName = "POSTDATE")]
            public string POSTDATE { get; set; }

            [XmlElement(ElementName = "CRDDISCPER")]
            public string CRDDISCPER { get; set; }

            [XmlElement(ElementName = "detail")]
            public List<Detail> Detail { get; set; }
        }

        [XmlRoot(ElementName = "oecreditordebitnote")]
        public class Oecreditordebitnote
        {
            [XmlElement(ElementName = "header")]
            public Header Header { get; set; }
        }

        [XmlRoot(ElementName = "arreceiptdetail")]
        interface Arreceiptdetail
        {
            [XmlElement(ElementName = "IDINVC")]
            string IDINVC { get; set; }
            [XmlElement(ElementName = "AMTPAYM")]
            string AMTPAYM { get; set; }
            [XmlElement(ElementName = "AMTERNDISC")]
            string AMTERNDISC { get; set; }
        }

        [Serializable]
        [XmlRoot(ElementName = "serialnumber")]
        public class OECreditNoteSerial
        {
            [XmlElement(ElementName = "SERIALNUMF")]
            public string SERIALNUMF { get; set; }


        }





        [XmlRoot(ElementName = "arreceiptheader")]
        public class Arreceiptheader
        {
            [XmlElement(ElementName = "POSID")]
            public string POSID { get; set; }
            [XmlElement(ElementName = "RMITTYPE")]
            public string RMITTYPE { get; set; }
            [XmlElement(ElementName = "TEXTRMIT")]
            public string TEXTRMIT { get; set; }
            [XmlElement(ElementName = "IDCUST")]
            public string IDCUST { get; set; }
            [XmlElement(ElementName = "TEXTPAYOR")]
            public string TEXTPAYOR { get; set; }
            [XmlElement(ElementName = "DATERMIT")]
            public string DATERMIT { get; set; }
            [XmlElement(ElementName = "DATEBUS")]
            public string DATEBUS { get; set; }
            [XmlElement(ElementName = "CODEPAYM")]
            public string CODEPAYM { get; set; }
            [XmlElement(ElementName = "IDRMIT")]
            public string IDRMIT { get; set; }
            [XmlElement(ElementName = "PAYMTYPE")]
            public string PAYMTYPE { get; set; }
            [XmlElement(ElementName = "CODECURNBC")]
            public string CODECURNBC { get; set; }
            //[XmlElement(ElementName = "arreceiptdetail")]
            //public List<Arreceiptdetail> Arreceiptdetailll { get; set; }
            [XmlElement(ElementName = "IDINVC")]
            public string IDINVC { get; set; }
            [XmlElement(ElementName = "AMTPAYM")]
            public string AMTPAYM { get; set; }
            [XmlElement(ElementName = "AMTERNDISC")]
            public string AMTERNDISC { get; set; }


        }

        [XmlRoot(ElementName = "arreceipts")]
        public class Arreceipts
        {
            [XmlElement(ElementName = "ARRECEIPTNO")]
            public string ARRECEIPTNO { get; set; }
            [XmlElement(ElementName = "arreceiptheader")]
            public List<Arreceiptheader> Arreceiptheader { get; set; }

        }







        //AP 
        [XmlRoot(ElementName = "appaymentdetail")]
        public class Appaymentdetail
        {
            [XmlElement(ElementName = "GLDESC")]
            public string GLDESC { get; set; }
            [XmlElement(ElementName = "IDDISTCODE")]
            public string IDDISTCODE { get; set; }
            [XmlElement(ElementName = "IDACCT")]
            public string IDACCT { get; set; }
            [XmlElement(ElementName = "AMTDISTTC")]
            public string AMTDISTTC { get; set; }
            [XmlElement(ElementName = "GLREF")]
            public string GLREF { get; set; }
        }

        [XmlRoot(ElementName = "appaymentheader")]
        public class Appaymentheader
        {
            [XmlElement(ElementName = "RMITTYPE")]
            public string RMITTYPE { get; set; }
            [XmlElement(ElementName = "TEXTRMIT")]
            public string TEXTRMIT { get; set; }
            [XmlElement(ElementName = "IDVEND")]
            public string IDVEND { get; set; }
            [XmlElement(ElementName = "IDRMITTO")]
            public string IDRMITTO { get; set; }
            [XmlElement(ElementName = "NAMERMIT")]
            public string NAMERMIT { get; set; }
            [XmlElement(ElementName = "DATERMIT")]
            public string DATERMIT { get; set; }
            [XmlElement(ElementName = "DATEBUS")]
            public string DATEBUS { get; set; }
            [XmlElement(ElementName = "PAYMCODE")]
            public string PAYMCODE { get; set; }
            [XmlElement(ElementName = "CASHACCT")]
            public double CASHACCT { get; set; }
            [XmlElement(ElementName = "DOCTYPE")]
            public string DOCTYPE { get; set; }
            [XmlElement(ElementName = "IDINVCMTCH")]
            public string IDINVCMTCH { get; set; }

            [XmlElement(ElementName = "DATEACTVPP")]
            public string DATEACTVPP { get; set; }

            [XmlElement(ElementName = "AMTPPAYTC")]
            public string AMTPPAYTC { get; set; }


            [XmlElement(ElementName = "appaymentdetail")]


            public List<Appaymentdetail> Appaymentdetail { get; set; }
        }

        [XmlRoot(ElementName = "appayments")]
        public class Appayments
        {
            [XmlElement(ElementName = "APBATCHNO")]
            public string APBATCHNO { get; set; }
            [XmlElement(ElementName = "appaymentheader")]
            public Appaymentheader Appaymentheader { get; set; }
        }









        //END OF AP
        [XmlRoot(ElementName = "payload")]
        public class Payload
        {
            [XmlElement(ElementName = "oeorder")]
            public Oeorder oeorder { get; set; }
            [XmlElement(ElementName = "oeOrderPrepayment")]
            public OeOrderPrepayment oeOrderPrepayment { get; set; }

            [XmlElement(ElementName = "oecreditordebitnote")]
            public Oecreditordebitnote Oecreditordebitnote { get; set; }

            [XmlElement(ElementName = "arreceipts")]
            public Arreceipts Arreceipts { get; set; }
            [XmlElement(ElementName = "appayments")]
            public Appayments Appayments { get; set; }


        }



        public string getAccpacPaymentType(int POSPaymentType, string CreditCardType)
        {
            string PaymentType = "";
            if (POSPaymentType == 16)
                PaymentType = "CHECK";
            else if (POSPaymentType == 9)
                PaymentType = "CASH";
            else if (POSPaymentType == 10)
            {
                if (CreditCardType == "AMIX")
                    PaymentType = "AMEX";
                else if (CreditCardType == "Visa Card")
                    PaymentType = "VISA";
                else if (CreditCardType == "Master Card")
                    PaymentType = "MASTER";
                else if (CreditCardType == "National")
                    PaymentType = "NATIIONAL";
            }
            return PaymentType;
        }


        public string PostPosPayment(string invoiceUniqueNumber)
        {
            List<E.InvoicePaymentIntegration> paymentLites = new List<InvoicePaymentIntegration>();
            paymentLites = iInvoicePaymentsManager.InvoicePaymentIntgration(invoiceUniqueNumber);
            XmlDocument ReturnXml = new XmlDocument();
            string sData = "";
            string entity = "arreceipts";
            string batchNumber = "";
            string ARReceiptNo = "";
            string AccpacPrePaymnentNumber = "";

            if (paymentLites != null)
            {
                  
                foreach (InvoicePaymentIntegration pay in paymentLites)
                {

                    batchNumber = oIntegrationObj.CreateARReceiptNo(userid, pwd, erpCompany, erpMachineName, "Created For POS haetham test", paymentLites[0].CashBank, paymentLites[0].Currency);
                    if (!batchNumber.Contains("ERROR"))
                    {
                        XmlDocument BatchNoXML = new XmlDocument();
                        BatchNoXML.LoadXml(batchNumber);
                        ARReceiptNo = BatchNoXML.GetElementsByTagName("ARReceiptBatchNumber").Item(0).InnerText;

                    }
                    else
                    {
                        continue;
                    }
                    sData = "<payload>";
                    sData += "<arreceipts>";
                    sData += "<ARRECEIPTNO>" + ARReceiptNo + "</ARRECEIPTNO>";
                    sData += "<arreceiptheader>";
                    sData += "<POSID></POSID>";
                    sData += "<RMITTYPE>2</RMITTYPE>";
                    sData += "<DOCTYPE>4</DOCTYPE>";
                    sData += "<TEXTRMIT>"+ pay .InvoiceUnqiueNumber+ "</TEXTRMIT>";
                    sData += "<IDCUST>" + pay.CashCustomer + "</IDCUST>";
                    sData += "<TEXTPAYOR>POS Customer</TEXTPAYOR>";
                    sData += "<DATERMIT>" + Convert.ToDateTime(pay.CreatedDate).ToString("yyyy-MM-dd") + "</DATERMIT>";
                    sData += "<DATEBUS>" + Convert.ToDateTime(pay.CreatedDate).ToString("yyyy-MM-dd") + "</DATEBUS>";
                    sData += "<CODEPAYM>" + getAccpacPaymentType(pay.PaymentType, pay.CreditCardType) + "</CODEPAYM>";
                    sData += "<IDRMIT>CHQ0000001</IDRMIT>";
                    sData += "<PAYMTYPE>CA</PAYMTYPE>";
                    sData += "<AMTRMIT>" + Math.Round(pay.AvailableAmount, 3).ToString() + "</AMTRMIT>";
                    sData += "<CODECURNBC>" + pay.Currency + "</CODECURNBC>";
                    sData += "<IDINVCMTCH>" + pay.AccpacOrderEntryNumber + "</IDINVCMTCH>";
                    sData += "</arreceiptheader>";
                    sData += "</arreceipts>";
                    sData += "</payload>";
                    ReturnXml = oIntegrationObj.PushToERP(entity, userid, pwd, erpCompany, erpMachineName, sData);
                    XDocument xDocPrePaayment = XDocument.Parse(ReturnXml.InnerXml.ToString());
                    string PostingMsg = oIntegrationObj.PostARReceiptBatch(userid, pwd, erpCompany, erpMachineName, Convert.ToDecimal(ARReceiptNo));
                    //AccpacPrePaymnentNumber+= xDocPrePaayment.Descendants("DOCNBR").SingleOrDefault();
                }

                iInvoiceManager.UpdateAccpacInvoiceNumber(invoiceUniqueNumber, "", "", "", AccpacPrePaymnentNumber, null);


            }
            return ReturnXml.InnerXml.ToString();



        }

    }
}













































