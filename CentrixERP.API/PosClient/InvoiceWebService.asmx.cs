using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;

using SagePOS.Server.Configuration;
using SF.FrameworkEntity;

using SagePOS.Server.API.Common.BaseClasses;
using E=SagePOS.Server.Business.Entity;
using SagePOS.Server.Business.IManager;
using SF.Framework;
using CentrixERP.Common.API;
using System.Reflection;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

using SagePOS.Server.Business.Manager;

namespace SagePOS.Server.API
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    [EntityAttribute(Enums_S3.Entity.Invoice)]
    public class InvoiceWebService : POSWebServiceBaseClass
    {
        IInvoiceManager iInvoiceManager = null;
        IInvoiceCustomFeildsManager iCustomerfieldManager = null;
        public InvoiceWebService()
        {
            this.javaScriptSerializer.MaxJsonLength = int.MaxValue;


            iInvoiceManager = (IInvoiceManager)IoC.Instance.Resolve(typeof(IInvoiceManager));
            iCustomerfieldManager = (IInvoiceCustomFeildsManager)IoC.Instance.Resolve(typeof(IInvoiceCustomFeildsManager));
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string Delete(int id)
        {
            //this.LockEntityForDelete(Enums_S3.Entity.Invoice, id);
            E.Invoice myInvoice = iInvoiceManager.FindById(id, null);
            myInvoice.InvoiceId = id;
            myInvoice.MarkDeleted();
            try
            {
                iInvoiceManager.Save(myInvoice);
                return AttachStatusCode(true, 1, null);
            }
            catch (Exception ex)
            {
                return AttachStatusCode(null, 0, HandleException(ex));
            }



        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindAllLite(string keyword, int page, int resultCount, string argsCriteria)
        {
            E.InvoiceCriteria criteria = null;
            if (!SF.Framework.String.IsEmpty(argsCriteria))
            {
                criteria = this.javaScriptSerializer.Deserialize<E.InvoiceCriteria>(argsCriteria);
            }
            else
                criteria = new E.InvoiceCriteria();

            criteria.Keyword = keyword;
            criteria.pageNumber = page;
            criteria.resultCount = resultCount;


            List<E.InvoiceLite> InvoiceList = iInvoiceManager.FindAllLite(criteria);
            return this.AttachStatusCode(InvoiceList, 1, null);

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindByIdLite(int id)
        {
            E.InvoiceLite myInvoice = iInvoiceManager.FindByIdLite(id, null);
            return this.AttachStatusCode(myInvoice, 1, null);

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string Add(int id, string InvoiceInfo)
        {
            return Save(true, id, InvoiceInfo);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string Edit(int id, string InvoiceInfo)
        {
            if (id <= 0)
            {
                ValidationError error = new ValidationError()
                {
                    ControlName = "",
                    GlobalMessage = true,
                    Message = "Please select Invoice"
                };
                List<ValidationError> errors = new List<ValidationError>(){
                error
                };
                return this.AttachStatusCode(errors, 0, "validation");
            }
            else
            {
                return Save(false, id, InvoiceInfo);
            }
        }

        private string Save(bool add, int id, string InvoiceInfo)
        {
            E.Invoice myInvoice = this.javaScriptSerializer.Deserialize<E.Invoice>(this.unEscape((InvoiceInfo)));
            if (!add)
            {
                this.LockEntity(Enums_S3.Entity.Invoice, id);
                myInvoice.MarkOld();
                myInvoice.MarkModified();
                myInvoice.InvoiceId = id;
                myInvoice.UpdatedBy = UserId;

                myInvoice.UpdatedBy = LoggedInUser.UserId;
            }
            else
            {
                myInvoice.CreatedBy = LoggedInUser.UserId;
            }


            List<ValidationError> errors = iInvoiceManager.Validate(myInvoice);
            if (errors != null && errors.Count > 0)
            {
                return this.AttachStatusCode(errors, 0, "validation");
            }


            try
            {
                iInvoiceManager.Save(myInvoice);
                if (!add)
                    this.UnLockEntity(Enums_S3.Entity.Invoice, id);
                return this.AttachStatusCode(myInvoice.InvoiceId, 1, null);

            }
            catch (SF.Framework.Exceptions.RecordExsitsException ex)
            {
                return this.AttachStatusCode(null, 501, null);
            }
            catch (Exception ex)
            {

                return this.AttachStatusCode(null, 0, HandleException(ex));
            }
        }


        private E.InvoiceItem getInvoiceItem(object[] items, int Id, int storeId, int status)
        {
            E.InvoiceItem item = null;
            foreach (Dictionary<string, Object> obj in items)
            {
                item = new E.InvoiceItem();
                if (obj["InvoiceItemId"].ToString().ToNumber() == Id)
                {
                    if (Id == -1)
                        obj["InvoiceItemId"] = "0";

                    foreach (KeyValuePair<string, Object> val in obj)
                    {
                        PropertyInfo pro = item.GetType().GetProperty(val.Key);
                        if (pro == null)
                            continue;

                        object value = null;
                        if (val.Value != null && !string.IsNullOrEmpty(val.Value.ToString()))
                            value = val.Value;

                        pro.SetValue(item, GetPropertyValue(pro.PropertyType, value), null);
                    }
                    item.InvoiceStatus = status;
                    item.StoreId = storeId;
                    break;
                }
            }
            return item;
        }

        private E.InvoicePayments getPaymentItem(object[] items, int Id)
        {
            E.InvoicePayments paymentItem = null;
            foreach (Dictionary<string, Object> obj in items)
            {
                paymentItem = new E.InvoicePayments();
                if (obj["InvoicePaymentsId"].ToString().ToNumber() == Id)
                {
                    if (Id == -1)
                        obj["InvoicePaymentsId"] = "0";

                    foreach (KeyValuePair<string, Object> val in obj)
                    {
                        PropertyInfo pro = paymentItem.GetType().GetProperty(val.Key);
                        if (pro == null)
                            continue;

                        object value = null;
                        if (val.Value != null && !string.IsNullOrEmpty(val.Value.ToString()))
                            value = val.Value;

                        pro.SetValue(paymentItem, GetPropertyValue(pro.PropertyType, value), null);

                    }
                    break;

                }
            }
            return paymentItem;
        }

        private E.InvoiceCustomFeilds getCustomFieldItem(object[] items, int Id)
        {
            E.InvoiceCustomFeilds paymentItem = null;
            foreach (Dictionary<string, Object> obj in items)
            {
                paymentItem = new E.InvoiceCustomFeilds();
                if (obj["CustomActionId"].ToString().ToNumber() == Id)
                {
                    if (Id == -1)
                        obj["CustomActionId"] = "0";

                    foreach (KeyValuePair<string, Object> val in obj)
                    {
                        PropertyInfo pro = paymentItem.GetType().GetProperty(val.Key);
                        if (pro == null)
                            continue;

                        object value = null;
                        if (val.Value != null && !string.IsNullOrEmpty(val.Value.ToString()))
                            value = val.Value;

                        pro.SetValue(paymentItem, GetPropertyValue(pro.PropertyType, value), null);

                    }
                    break;

                }
            }
            return paymentItem;
        }



       
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string AddEditInvoice(int InvoiceId, string InvoiceInfo, int UserId, int StoreId, int RegisterId)
        {
            IInvoiceManager InvoiceManager = (IInvoiceManager)IoC.Instance.Resolve(typeof(IInvoiceManager));
            IInvoicePaymentsManager invoicePaymentManager = (IInvoicePaymentsManager)IoC.Instance.Resolve(typeof(IInvoicePaymentsManager));
            IInvoiceItemManager invoiceItemManager = (IInvoiceItemManager)IoC.Instance.Resolve(typeof(IInvoiceItemManager));
            E.InvoiceItemCriteria ItemCriteria = new E.InvoiceItemCriteria();
            E.InvoicePaymentsCriteria paymentCriteria = new E.InvoicePaymentsCriteria();
            ItemCriteria.InvoiceId = InvoiceId;
            paymentCriteria.InvoiceId = InvoiceId;
            E.Invoice myInvoice = this.javaScriptSerializer.Deserialize<E.Invoice>(this.unEscape((InvoiceInfo)));


            if (InvoiceId <= 0)
            {

                myInvoice.CreatedBy = UserId;
                myInvoice.StoreId = StoreId;
                myInvoice.RegisterId = RegisterId;
                myInvoice.InvoiceDate = DateTime.Now;
            }
            else
            {
                myInvoice = InvoiceManager.FindById(InvoiceId, null);
                myInvoice.UpdatedBy = UserId;
                myInvoice.MarkModified();
                myInvoice.InvoiceDate = DateTime.Now;
            }


            if (InvoiceId > 0)
            {
                List<E.InvoicePayments> paymentList = invoicePaymentManager.FindAll(paymentCriteria);
                if (paymentList != null && paymentList.Count() > 0)
                    myInvoice.InvoicePaymentList = paymentList;
                myInvoice.InvoiceItemList = invoiceItemManager.FindAll(ItemCriteria);
            }
             List<string> ids= (from E.InvoiceItem item in myInvoice.InvoiceItemList select item.InvoiceItemId.ToString()).ToList();
             List<string> paymentids = new List<string>();
             if (myInvoice.InvoicePaymentList != null)
             {
                 paymentids = (from E.InvoicePayments payment in myInvoice.InvoicePaymentList select payment.InvoicePaymentsId.ToString()).ToList();
             }
  

            foreach (E.InvoiceItem item in myInvoice.InvoiceItemList)
            {
                var contain = from string Id in ids where item.InvoiceItemId == Convert.ToInt32(Id) select Id;
                if (contain == null || contain.Count() == 0)
                {
                    
                    item.MarkDeleted();
                }
            }

         


            for (int i = 0; i < myInvoice.InvoiceItemList.Count; i++)
            {
                E.InvoiceItem item = myInvoice.InvoiceItemList[i];
                var contain = from string Id in ids
                              where item.InvoiceItemId == Convert.ToInt32(Id)
                                  && Convert.ToInt32(Id) != -1
                              select Id;
                if (contain != null && contain.Count() != 0)
                {
                  
                    myInvoice.InvoiceItemList[i].MarkOld();
                    myInvoice.InvoiceItemList[i].MarkModified();
                }
            }


            string message = ValidateInvoice(myInvoice);
            if (myInvoice.InvoicePaymentList == null)
                myInvoice.InvoicePaymentList = new List<E.InvoicePayments>();
            if (message != null)
                return this.AttachStatusCode(null, 110, message);


            try
            {

                InvoiceManager.Save(myInvoice);

            }

            catch (Exception ex)
            {
                throw ex;

            }
            E.InvoiceLite myInvoiceLite = InvoiceManager.FindByIdLite((int)myInvoice.InvoiceId, null);


            var invoiceObject = new
            {
                invoiceId = myInvoiceLite.InvoiceId,
                invoiceNumber = myInvoiceLite.InvoiceNumber,

                dateInvoice = myInvoice.InvoiceDate.Value.ToString("dd/MM/yyyy hh:mm")

            };


            return this.javaScriptSerializer.Serialize(invoiceObject);
        }


        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string GetInvoiceInfo(int InvoiceId)
        {



            IInvoiceManager InvoiceManager = (IInvoiceManager)IoC.Instance.Resolve(typeof(IInvoiceManager));
            IInvoiceItemManager invoiceItemManager = (IInvoiceItemManager)IoC.Instance.Resolve(typeof(IInvoiceItemManager));
            E.InvoiceLite myInvoice = InvoiceManager.FindByIdLite(InvoiceId, null);
            E.InvoiceLite ParentInvoice = InvoiceManager.FindByIdLite(myInvoice.ParentInvoiceId, null);
            string ParentInvoiceNumber = "";
            if (ParentInvoice != null)
                ParentInvoiceNumber = ParentInvoice.InvoiceNumber;

            IPersonManager person = (IPersonManager)IoC.Instance.Resolve(typeof(IPersonManager));
            ICompanyManager company = (ICompanyManager)IoC.Instance.Resolve(typeof(ICompanyManager));

            E.Person personInfo = null;
            E.Company companyInfo = null;

            if (myInvoice.Contactid != -1)
            {
                personInfo = new E.Person();
                personInfo = person.FindById(myInvoice.Contactid, null);
            }
            if (myInvoice.Contactid != -1)
            {
                companyInfo = new E.Company();
                companyInfo = company.FindById(myInvoice.Contactid, null);
            }

            string mobile = string.Empty;
            if (personInfo != null)
            {
                if (!string.IsNullOrEmpty(personInfo.MobileNumber))
                    mobile = personInfo.MobileNumber;
            }
            else if (companyInfo != null)
            {
                if (!string.IsNullOrEmpty(companyInfo.MobileNumber))
                    mobile = companyInfo.MobileNumber;
            }

            E.InvoiceItemCriteria itemCriteria = new E.InvoiceItemCriteria();
            itemCriteria.InvoiceId = InvoiceId;
            List<E.InvoiceItemLite> myInvoiceItemList = invoiceItemManager.FindAllLite(itemCriteria);
            E.InvoiceCriteria myCriteria = new E.InvoiceCriteria();

            if (myInvoice != null)
            {
                if (myInvoiceItemList != null)
                {
                    var InvoiceItemObj = from InvItem in myInvoiceItemList
                                         select new
                                         {
                                             InvoiceItemId = InvItem.InvoiceItemId,
                                             InvoiceId = InvItem.InvoiceId,
                                             ItemName = InvItem.ItemName,
                                             Quantity = InvItem.Quantity,
                                             OrginalPrice = InvItem.OrginalPrice,
                                             PercentageDiscount = InvItem.PercentageDiscount,
                                             AmountDiscount = InvItem.AmountDiscount,
                                             TotalPrice = InvItem.TotalPrice,
                                             UnitPrice = InvItem.UnitPrice,
                                             GrandTotal = InvItem.TotalPrice,
                                             //MinPrice = InvItem.MinPrice,
                                             TaxPercentage = InvItem.TaxPercentage,
                                             BarCode = InvItem.BarCode,
                                             IsRefunded = InvItem.IsRefunded,
                                             ParentItemId = InvItem.ParentItemId,
                                             RefundedQuantity = InvItem.RefundedQuantity,
                                             UnitId = InvItem.UnitId,
                                             TaxAmount = InvItem.TaxAmount,
                                             ItemNumber = InvItem.ItemNo,
                                             TaxBeforeChange = InvItem.TaxBeforeChange,
                                             IsSerial = InvItem.IsSerial,
                                             SerialNumber = InvItem.SerialNumber,
                                             InvoiceDiscountPerItem = InvItem.InvoiceDiscountPerItem
                                         };

                    var InvoiceObj = new
                    {

                        InvoiceId = myInvoice.InvoiceId,
                        Contactid = myInvoice.Contactid,

                        ContactName = myInvoice.ContactName,

                        TotalPrice = myInvoice.TotalPrice,
                        GrandTotal = myInvoice.GrandTotal,
                        TaxAmount = myInvoice.TaxAmount,
                        Note = myInvoice.Note,
                        PercentageDiscount = myInvoice.PercentageDiscount,
                        AmountDiscount = myInvoice.AmountDiscount,
                        Status = myInvoice.InvoiceStatus,

                        ChangeAmount = myInvoice.ChangeAmount,
                        InvoiceNumber = myInvoice.InvoiceNumber,
                        SalesManId = myInvoice.SaleManId,
                        SalesManName = myInvoice.SalesManName,



                        ItemsDiscount = myInvoice.ItemsDiscount,
                        InvoiceDate = myInvoice.InvoiceDate,
                        InvoiceItems = InvoiceItemObj,
                        MobileNumber = mobile,
                        IsTaxableInvoice = myInvoice.IsTaxableInvoice,
                        OriginalInvoiceNumber = ParentInvoiceNumber
                    };

                    return this.javaScriptSerializer.Serialize(InvoiceObj);
                }
                else
                {
                    var InvoiceObj = new
                    {
                        InvoiceId = myInvoice.InvoiceId,

                        Contactid = myInvoice.Contactid,
                        ContactName = myInvoice.ContactName,
                        TotalPrice = myInvoice.TotalPrice,
                        GrandTotal = myInvoice.GrandTotal,
                        TaxAmount = myInvoice.TaxAmount,
                        Note = myInvoice.Note,
                        PercentageDiscount = myInvoice.PercentageDiscount,
                        AmountDiscount = myInvoice.AmountDiscount,
                        Status = myInvoice.InvoiceStatus,

                        ChangeAmount = myInvoice.ChangeAmount,
                        InvoiceNumber = myInvoice.InvoiceNumber,
                        SalesManId = myInvoice.SaleManId,
                        SalesManName = myInvoice.SalesManName,


                        ItemsDiscount = myInvoice.ItemsDiscount,
                        InvoiceDate = myInvoice.InvoiceDate,
                        InvoiceItems = "",
                        MobileNumber = mobile,
                        IsTaxableInvoice = myInvoice.IsTaxableInvoice,
                        OriginalInvoiceNumber = ParentInvoiceNumber

                    };
                    return this.javaScriptSerializer.Serialize(InvoiceObj);


                }

            }
            else
            {
                return null;
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string GetLastInvoice(int RegisterId)
        {
            IInvoiceManager InvoiceManager = (IInvoiceManager)IoC.Instance.Resolve(typeof(IInvoiceManager));
            IInvoiceItemManager invoiceItemManager = (IInvoiceItemManager)IoC.Instance.Resolve(typeof(IInvoiceItemManager));

            E.InvoiceCriteria invCriteria = new E.InvoiceCriteria();
            invCriteria.RegisterId = RegisterId;
            E.Invoice myInvoice = InvoiceManager.FindLastInvoice(invCriteria);
            E.InvoiceLite ParentInvoice = InvoiceManager.FindByIdLite(myInvoice.ParentInvoiceId, null);
            string ParentInvoiceNumber = "";
            if (ParentInvoice != null)
                ParentInvoiceNumber = ParentInvoice.InvoiceNumber;
            if (myInvoice != null)
            {
                E.InvoiceItemCriteria itemCriteria = new E.InvoiceItemCriteria();
                itemCriteria.InvoiceId = myInvoice.InvoiceId;

                IInvoicePaymentsManager invoicePaymentManager = (IInvoicePaymentsManager)IoC.Instance.Resolve(typeof(IInvoicePaymentsManager));
                E.InvoicePaymentsCriteria paymentCriteria = new E.InvoicePaymentsCriteria();
                paymentCriteria.InvoiceId = myInvoice.InvoiceId;
                List<E.InvoicePaymentsLite> myPaymentList = invoicePaymentManager.FindAllLite(paymentCriteria);

                List<E.InvoiceItemLite> myInvoiceItemList = invoiceItemManager.FindAllLite(itemCriteria);



                E.InvoiceCriteria myCriteria = new E.InvoiceCriteria();

                IPersonManager person = (IPersonManager)IoC.Instance.Resolve(typeof(IPersonManager));
                ICompanyManager company = (ICompanyManager)IoC.Instance.Resolve(typeof(ICompanyManager));

                E.Person personInfo = null;
                E.Company companyInfo = null;

                E.PersonCriteria personCriteria = new E.PersonCriteria();
                //personCriteria.PersonId = myInvoice.PersonId;

                //CompanyCriteria companyCriteria = new CompanyCriteria();
                //companyCriteria.CompanyId = myInvoice.CompanyId;
                if (myInvoice.Contactid != -1)
                {
                    personInfo = new E.Person();

                    personInfo = person.FindById(myInvoice.Contactid, null);
                }
                if (myInvoice.Contactid != -1)
                {
                    companyInfo = new E.Company();
                    companyInfo = company.FindById(myInvoice.Contactid, null);
                }

                string mobile = string.Empty;
                if (personInfo != null)
                {
                    if (!string.IsNullOrEmpty(personInfo.MobileNumber))
                        mobile = personInfo.MobileNumber;
                }
                else if (companyInfo != null)
                {
                    if (!string.IsNullOrEmpty(companyInfo.MobileNumber))
                        mobile = companyInfo.MobileNumber;
                }
                if (myInvoiceItemList != null)
                {
                    var InvoiceItemObj = from InvItem in myInvoiceItemList
                                         select new
                                         {
                                             InvoiceItemId = InvItem.InvoiceItemId,
                                             InvoiceId = InvItem.InvoiceId,
                                             ItemName = InvItem.ItemName,
                                             Quantity = InvItem.Quantity,
                                             OrginalPrice = InvItem.OrginalPrice,
                                             PercentageDiscount = InvItem.PercentageDiscount,
                                             AmountDiscount = InvItem.AmountDiscount,
                                             TotalPrice = InvItem.TotalPrice,
                                             UnitPrice = InvItem.UnitPrice,
                                             GrandTotal = InvItem.TotalPrice,
                                             //MinPrice = InvItem.MinPrice,
                                             TaxPercentage = InvItem.TaxPercentage,
                                             BarCode = InvItem.BarCode,
                                             IsRefunded = InvItem.IsRefunded,
                                             ParentItemId = InvItem.ParentItemId,
                                             RefundedQuantity = InvItem.RefundedQuantity,
                                             UnitId = InvItem.UnitId,
                                             TaxAmount = InvItem.TaxAmount,
                                             ItemNumber = InvItem.ItemNo,
                                             IsSerial = InvItem.IsSerial,
                                             SerialNumber = InvItem.SerialNumber,
                                             InvoiceDiscountPerItem = InvItem.InvoiceDiscountPerItem,

                                         };

                    var invoicePaymentObject = from payItem in myPaymentList
                                               select new
                                               {
                                                   PaymentTypeName = payItem.PaymenttypeName,
                                                   AmountDefaultCurrency = payItem.Amount,
                                                   CreditCardType = payItem.CreditCardType,
                                                   PaymentType = payItem.PaymentType

                                               };

                    var InvoiceObj = new
                    {
                        InvoiceId = myInvoice.InvoiceId,
                        Contactid = myInvoice.Contactid,
                        ContactName = myInvoice.ContactName,
                        MobileNumber = myInvoice.MobileNumber,
                        TotalPrice = myInvoice.TotalPrice,
                        GrandTotal = myInvoice.GrandTotal,
                        TaxAmount = myInvoice.TaxAmount,
                        Note = myInvoice.Note,
                        PercentageDiscount = myInvoice.PercentageDiscount,
                        AmountDiscount = myInvoice.AmountDiscount,
                        Status = myInvoice.Status,

                        ChangeAmount = myInvoice.ChangeAmount,
                        InvoiceNumber = myInvoice.InvoiceNumber,
                        SalesManId = myInvoice.SalesManId,
                        SalesManName = myInvoice.SalesManName,



                        ItemsDiscount = myInvoice.ItemsDiscount,
                        InvoiceDate = myInvoice.InvoiceDate.Value.ToString("dd/MM/yyyy hh:mm"),
                        InvoiceItems = InvoiceItemObj,
                        InvoicePayment = invoicePaymentObject,

                        OriginalInvoiceNumber = ParentInvoiceNumber
                    };

                    return this.javaScriptSerializer.Serialize(InvoiceObj);
                }
                else
                {
                    var InvoiceObj = new
                    {
                        InvoiceId = myInvoice.InvoiceId,

                        Contactid = myInvoice.Contactid,
                        ContactName = myInvoice.ContactName,
                        MobileNumber = myInvoice.MobileNumber,
                        TotalPrice = myInvoice.TotalPrice,
                        GrandTotal = myInvoice.GrandTotal,
                        TaxAmount = myInvoice.TaxAmount,
                        Note = myInvoice.Note,
                        PercentageDiscount = myInvoice.PercentageDiscount,
                        AmountDiscount = myInvoice.AmountDiscount,
                        Status = myInvoice.Status,

                        ChangeAmount = myInvoice.ChangeAmount,
                        InvoiceNumber = myInvoice.InvoiceNumber,
                        SalesManId = myInvoice.SalesManId,
                        SalesManName = myInvoice.SalesManName,



                        InvoiceItems = "",
                        InvoiceDate = myInvoice.InvoiceDate.Value.ToString("dd/MM/yyyy hh:mm"),
                        ItemsDiscount = myInvoice.ItemsDiscount,

                        OriginalInvoiceNumber = ParentInvoiceNumber

                    };
                    return this.javaScriptSerializer.Serialize(InvoiceObj);


                }

            }
            else
            {
                return null;
            }
        }


        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindInvoiceByNumber(string invoiceNumber, int StoreId)
        {

            try
            {

                IInvoiceManager InvoiceManager = (IInvoiceManager)IoC.Instance.Resolve(typeof(IInvoiceManager));
                IInvoiceItemManager invoiceItemManager = (IInvoiceItemManager)IoC.Instance.Resolve(typeof(IInvoiceItemManager));


                E.InvoiceCriteria invCriteria = new E.InvoiceCriteria();
                invCriteria.StoreId = StoreId;
                invCriteria.InvoiceNumber = invoiceNumber;
                E.Invoice myInvoice = InvoiceManager.FindInvoiceByNumber(invCriteria);
                E.InvoiceLite ParentInvoice = InvoiceManager.FindByIdLite(myInvoice.ParentInvoiceId, null);
                string ParentInvoiceNumber = "";
                if (ParentInvoice != null)
                    ParentInvoiceNumber = ParentInvoice.InvoiceNumber;

                if (myInvoice != null)
                {

                    myInvoice = InvoiceManager.FindById(myInvoice.InvoiceId, null);


                    E.InvoiceItemCriteria itemCriteria = new E.InvoiceItemCriteria();
                    itemCriteria.InvoiceId = myInvoice.InvoiceId;

                    List<E.InvoiceItemLite> myInvoiceItemList = invoiceItemManager.FindAllLite(itemCriteria);
                    E.InvoiceCriteria myCriteria = new E.InvoiceCriteria();


                    IInvoicePaymentsManager invoicePaymentManager = (IInvoicePaymentsManager)IoC.Instance.Resolve(typeof(IInvoicePaymentsManager));
                    E.InvoicePaymentsCriteria paymentCriteria = new E.InvoicePaymentsCriteria();
                    paymentCriteria.InvoiceId = myInvoice.InvoiceId;
                    List<E.InvoicePaymentsLite> myPaymentList = invoicePaymentManager.FindAllLite(paymentCriteria);

                    IPersonManager person = (IPersonManager)IoC.Instance.Resolve(typeof(IPersonManager));
                    ICompanyManager company = (ICompanyManager)IoC.Instance.Resolve(typeof(ICompanyManager));

                    E.Person personInfo = null;
                    E.Company companyInfo = null;


                    if (myInvoice.Contactid != -1 && myInvoice.Contactid != 0)
                    {
                        personInfo = new E.Person();
                        personInfo = person.FindById(myInvoice.Contactid, null);
                    }
                    if (myInvoice.Contactid != -1 && myInvoice.Contactid != 0)
                    {
                        companyInfo = new E.Company();
                        companyInfo = company.FindById(myInvoice.Contactid, null);
                    }

                    string mobile = string.Empty;
                    if (personInfo != null)
                    {
                        if (!string.IsNullOrEmpty(personInfo.MobileNumber))
                            mobile = personInfo.MobileNumber;
                    }
                    else if (companyInfo != null)
                    {
                        if (!string.IsNullOrEmpty(companyInfo.MobileNumber))
                            mobile = companyInfo.MobileNumber;
                    }

                    var invoicePaymentObject = from payItem in myPaymentList
                                               select new
                                               {
                                                   PaymentTypeName = payItem.PaymenttypeName,
                                                   AmountDefaultCurrency = payItem.Amount
                                               };

                    if (myInvoiceItemList != null)
                    {


                        var InvoiceItemObj = from InvItem in myInvoiceItemList
                                             select new
                                             {
                                                 InvoiceItemId = InvItem.InvoiceItemId,
                                                 InvoiceId = InvItem.InvoiceId,
                                                 ItemName = InvItem.ItemName,
                                                 Quantity = InvItem.Quantity,
                                                 OrginalPrice = InvItem.OrginalPrice,
                                                 PercentageDiscount = InvItem.PercentageDiscount,
                                                 AmountDiscount = InvItem.AmountDiscount,
                                                 TotalPrice = InvItem.TotalPrice,
                                                 UnitPrice = InvItem.UnitPrice,
                                                 GrandTotal = InvItem.TotalPrice,
                                                 //MinPrice = InvItem.MinPrice,
                                                 TaxPercentage = InvItem.TaxPercentage,
                                                 BarCode = InvItem.BarCode,
                                                 IsRefunded = InvItem.IsRefunded,
                                                 ParentItemId = InvItem.ParentItemId,
                                                 RefundedQuantity = InvItem.RefundedQuantity,
                                                 UnitId = InvItem.UnitId,
                                                 TaxAmount = InvItem.TaxAmount,
                                                 ItemNumber = InvItem.ItemNo,
                                                 IsSerial = InvItem.IsSerial,
                                                 SerialNumber = InvItem.SerialNumber,
                                                 IsExchanged = InvItem.IsExchanged,
                                                 InvoiceDiscountPerItem = InvItem.InvoiceDiscountPerItem,


                                             };

                        var InvoiceObj = new
                        {

                            InvoiceId = myInvoice.InvoiceId,

                            Contactid = myInvoice.Contactid,
                            ContactName = myInvoice.ContactName,
                            MobileNumber = myInvoice.MobileNumber,
                            TotalPrice = myInvoice.TotalPrice,
                            GrandTotal = myInvoice.GrandTotal,
                            TaxAmount = myInvoice.TaxAmount,
                            Note = myInvoice.Note,
                            PercentageDiscount = myInvoice.PercentageDiscount,
                            AmountDiscount = myInvoice.AmountDiscount,
                            Status = myInvoice.Status,

                            ChangeAmount = myInvoice.ChangeAmount,
                            InvoiceNumber = myInvoice.InvoiceNumber,
                            SalesManId = myInvoice.SalesManId,
                            SalesManName = myInvoice.SalesManName,



                            InvoiceItems = InvoiceItemObj,
                            InvoiceCreatedDate = (myInvoice.InvoiceDate.HasValue) ? myInvoice.InvoiceDate.Value.ToString("dd-MM-yyyy") : "",
                            CustomerId = myInvoice.Contactid,
                            ERPCustomerID = myInvoice.ContactName,
                            ItemsDiscount = myInvoice.ItemsDiscount,
                            InvoiceDate = myInvoice.InvoiceDate.Value.ToString("dd/MM/yyyy hh:mm"),
                            InvoicePayment = invoicePaymentObject,

                            OriginalInvoiceNumber = ParentInvoiceNumber,

                            IsTaxableInvoice = myInvoice.IsTaxableInvoice


                        };

                        return this.javaScriptSerializer.Serialize(InvoiceObj);
                    }
                    else
                    {
                        var InvoiceObj = new
                        {
                            InvoiceId = myInvoice.InvoiceId,
                            Contactid = myInvoice.Contactid,
                            ContactName = myInvoice.ContactName,
                            MobileNumber = myInvoice.MobileNumber,
                            TotalPrice = myInvoice.TotalPrice,
                            GrandTotal = myInvoice.GrandTotal,
                            TaxAmount = myInvoice.TaxAmount,
                            Note = myInvoice.Note,
                            PercentageDiscount = myInvoice.PercentageDiscount,
                            AmountDiscount = myInvoice.AmountDiscount,
                            Status = myInvoice.Status,

                            ChangeAmount = myInvoice.ChangeAmount,
                            InvoiceNumber = myInvoice.InvoiceNumber,
                            SalesManId = myInvoice.SalesManId,
                            SalesManName = myInvoice.SalesManName,


                            InvoiceItems = "",
                            CustomerId = myInvoice.Contactid,

                            ERPCustomerID = myInvoice.Contactid,
                            ItemsDiscount = myInvoice.ItemsDiscount,
                            InvoiceDate = myInvoice.InvoiceDate.Value.ToString("dd/MM/yyyy hh:mm"),

                            OriginalInvoiceNumber = ParentInvoiceNumber,
                            IsTaxableInvoice = myInvoice.IsTaxableInvoice,
                            InvoicePayment = invoicePaymentObject

                        };
                        return this.javaScriptSerializer.Serialize(InvoiceObj);


                    }

                }
                else
                {
                    return null;
                }


            }
            catch (Exception ex)
            {

                return ex.ToString();
            }
        }



        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string InvoiceSearchCriteria(string InvoiceCriteria, int RegisterId, string phoneNumber)
        {

            IInvoiceManager myManager = (IInvoiceManager)IoC.Instance.Resolve(typeof(IInvoiceManager));
            E.InvoiceCriteria myCriteria = new E.InvoiceCriteria();
            if (!SF.Framework.String.IsEmpty(InvoiceCriteria))
            {
                myCriteria = this.javaScriptSerializer.Deserialize<E.InvoiceCriteria>(this.unEscape(InvoiceCriteria));

            }
            else
                myCriteria = new E.InvoiceCriteria();
            myCriteria.RegisterId = RegisterId;


            List<E.Invoice> myInvoiceList = myManager.FindAllQl(myCriteria, phoneNumber);


            IInvoicePaymentsManager invoicePaymentManager = (IInvoicePaymentsManager)IoC.Instance.Resolve(typeof(IInvoicePaymentsManager));
            E.InvoicePaymentsCriteria paymentCriteria = new E.InvoicePaymentsCriteria();
            if (myCriteria.InvoiceId != null)
                paymentCriteria.InvoiceId = (int)myCriteria.InvoiceId;

            List<E.InvoicePaymentsLite> myPaymentList = invoicePaymentManager.FindAllLite(paymentCriteria);

            if (myInvoiceList != null)
            {

                if (myPaymentList != null)
                {
                    var invoicePaymentObject = from payItem in myPaymentList
                                               select new
                                               {
                                                   PaymentTypeName = payItem.PaymenttypeName,
                                                   AmountDefaultCurrency = payItem.Amount
                                               };
                }

                var InvoiceObj = from E.Invoice inv in myInvoiceList
                                 select new
                                 {
                                     InvoiceId = inv.InvoiceId,
                                     Contactid = inv.Contactid,
                                     ContactName = inv.ContactName,
                                     MobileNumber = inv.MobileNumber,
                                     TotalPrice = inv.TotalPrice,
                                     GrandTotal = inv.GrandTotal,
                                     TaxAmount = inv.TaxAmount,
                                     Note = inv.Note,
                                     PercentageDiscount = inv.PercentageDiscount,
                                     AmountDiscount = inv.AmountDiscount,
                                     Status = inv.Status,

                                     ChangeAmount = inv.ChangeAmount,



                                     InvoiceCreatedDate = (inv.InvoiceDate.HasValue) ? inv.InvoiceDate.Value.ToString("dd-MM-yyyy hh:mm:ss tt") : "",
                                     ItemsDiscount = inv.ItemsDiscount,
                                     InvoiceTotalDiscount = inv.ItemsDiscount + inv.AmountDiscount,
                                     TransactionTypeName = inv.TransactionTypeName,
                                     InvoiceNumber = inv.InvoiceNumber,
                                     CustomerName = (!string.IsNullOrEmpty(inv.ContactName)) ? inv.ContactName : inv.ContactName,

                                     CustomerId = inv.ContactName,
                                     InvoiceDate = inv.InvoiceDate.Value.ToShortDateString(),
                                     InvoicePayment = ""
                                 };
                return this.javaScriptSerializer.Serialize(InvoiceObj);
            }
            else
            {
                return null;
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string GetInvoicesByCustomer(int CustomerId, int Status, int CustomerType)
        {
            IInvoiceManager myManager = (IInvoiceManager)IoC.Instance.Resolve(typeof(IInvoiceManager));
            E.InvoiceCriteria InvoiceCriteria = new E.InvoiceCriteria();
            InvoiceCriteria.CustomerId = CustomerId;
            InvoiceCriteria.Status = Status;
            //InvoiceCriteria.CustomerType = CustomerType;
            List<E.Invoice> myInvoiceList = myManager.FindByCustomer(InvoiceCriteria);
            if (myInvoiceList != null)
            {
                var InvoiceObj = from E.Invoice invObj in myInvoiceList
                                 select new
                                 {
                                     GrandTotal = invObj.GrandTotal,
                                     CreatedDate = invObj.CreatedDate.Value.ToString("dd/MM/yyyy"),

                                     InvoiceId = invObj.InvoiceId,

                                 };


                return this.javaScriptSerializer.Serialize(InvoiceObj);
            }
            else
            {
                return null;
            }

        }


        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string GetLayByPayment(int invoiceId)
        {
            IInvoicePaymentsManager myManager = (IInvoicePaymentsManager)IoC.Instance.Resolve(typeof(IInvoicePaymentsManager));
            E.InvoicePaymentsCriteria criteria = new E.InvoicePaymentsCriteria();
            criteria.InvoiceId = invoiceId;
            List<E.InvoicePaymentsLite> invoicePaymentsList = myManager.FindAllLite(criteria);
            if (invoicePaymentsList != null)
            {
                var paymentObj = from E.InvoicePaymentsLite payObj in invoicePaymentsList
                                 select new
                                 {
                                     InvoicePaymentsId = payObj.InvoicePaymentsId,

                                     Amount = payObj.Amount,
                                     AmountDefaultCurrency = payObj.AmountDefaultCurrency,
                                     Currency = payObj.Currency,
                                     PaymentTypeName = payObj.PaymenttypeName,
                                     GiftNumber = payObj.GiftNumber,
                                     RegisterId = payObj.RegisterId
                                 };


                return this.javaScriptSerializer.Serialize(paymentObj);

            }
            return null;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string SearchRefund(string personName, string invoiceNumber, string dateFrom, string dateTo, bool isRefund)
        {
            E.InvoiceCriteria criteria = null;
            criteria = new E.InvoiceCriteria();
            criteria.ContactName = personName;
            criteria.InvoiceNumber = invoiceNumber;
            criteria.DateTo = dateTo;
            criteria.DateFrom = dateFrom;
            criteria.isRefund = isRefund;
            List<E.InvoiceLite> invoiceList = iInvoiceManager.searchRefund(criteria);
            if (invoiceList != null)
            {
                var invoice = from E.Invoice invoiceObj in invoiceList
                              select new
                              {
                                  InvoiceNumber = invoiceObj.InvoiceNumber,
                                  ContactName = (invoiceObj.ContactName != null) ? invoiceObj.ContactName : invoiceObj.ContactName,
                                  InvoiceDate = invoiceObj.InvoiceDate,
                                  GrandTotal = invoiceObj.GrandTotal


                              };


                return this.javaScriptSerializer.Serialize(invoiceList);

            }
            return null;
            //return this.AttachStatusCode(invoiceList, 1, null);
        }


        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public bool CheckIPConnectivity(int StoreId)
        {
            bool registerStatus = true;

            System.Net.NetworkInformation.Ping ping = new System.Net.NetworkInformation.Ping();
            try
            {
                if (ping.Send("192.168.2.229").Status != System.Net.NetworkInformation.IPStatus.Success)
                    registerStatus = false;
            }
            catch (Exception ex)
            {
                registerStatus = false;

            }

            return registerStatus;

        }


        public string ValidateInvoice(E.Invoice myInvoice)
        {
            string message = null;
            double totalItemPrice = myInvoice.InvoiceItemList.Sum(item => item.TotalPrice);
            double totaPayments = 0;
            int invoicePaymentCounts = 0;
            if (myInvoice.InvoicePaymentList != null)
            {
                totaPayments = myInvoice.InvoicePaymentList.Sum(pay => pay.AmountDefaultCurrency);
                invoicePaymentCounts = myInvoice.InvoicePaymentList.Count;
            }
            double TaxItemAmount = myInvoice.InvoiceItemList.Sum(item => item.TaxAmount);
            double totalitemDiscount = myInvoice.InvoiceItemList.Sum(item => item.AmountDiscount);
            int invoiceitemsCount = myInvoice.InvoiceItemList.Count;
        
            if (myInvoice.InvoiceItemList.Count == 0 && myInvoice.Status != 8)
            {
                message = "Invoice items is empty";
                return message;
            }

            if (myInvoice.InvoicePaymentList ==null && myInvoice.Status != 3)
            {
                message = "Invoice payments is empty";
                return message;
            }


            if (totalItemPrice != myInvoice.GrandTotal && myInvoice.Status != 8)
            {
                message = "Total Item Price is not equal to total invoice price";
                return message;
            }

            if (myInvoice.GrandTotal != (totaPayments + myInvoice.ChangeAmount) && myInvoice.Status != 3)
            {
                message = "Total payment  is not equal to total invoice price";
                return message;
            }
            if (myInvoice.TaxAmount != TaxItemAmount && myInvoice.Status != 8)
            {
                message = "Total item tax  is not equal to total invoice tax";
                return message;
            }
            if (myInvoice.ItemsDiscount != totalitemDiscount && myInvoice.Status != 8)
            {
                message = "Total item discount  is not equal to total invoice item discount";
                return message;
            }

            if (myInvoice.PaymentLineCounts != invoicePaymentCounts && myInvoice.Status != 3)
            {
                message = "payment line count is not equal to invoice payments count";
                return message;
            }
            if (myInvoice.ItemLineCounts != invoiceitemsCount && myInvoice.Status != 8)
            {
                message = "item line count is not equal to invoice items count";
                return message;
            }
            return message;

        }


        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string GetPOSTotalDailySales(int storeId, string datefrom, string dateto)
        {
            List<E.POSDailyRecieptsReport> list = new List<E.POSDailyRecieptsReport>();
            list = iInvoiceManager.FindPOSDailySalesReport(datefrom, dateto, storeId);

            return this.javaScriptSerializer.Serialize(list);



        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string GetPOSTotalDailySalesSummation(int storeId, string datefrom, string dateto)
        {
            List<E.POSDailyRecieptsReport> list = new List<E.POSDailyRecieptsReport>();
            list = iInvoiceManager.FindPOSDailySalesReportSummation(datefrom, dateto, storeId);

            return this.javaScriptSerializer.Serialize(list);



        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string GetPOSDaileSalesSummationReport(int storeId, string datefrom, string dateto)
        {
            List<E.POSDaileSalesSummation> list = new List<E.POSDaileSalesSummation>();
            list = iInvoiceManager.FindPOSDaileSalessummation(datefrom, dateto, storeId);

            return this.javaScriptSerializer.Serialize(list);



        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string GetPOSPettyCashReport(int storeId, string datefrom, string dateto, int registerId)
        {
            List<E.POSPettyCash> list = new List<E.POSPettyCash>();
            list = iInvoiceManager.FindPOSPettyCash(datefrom, dateto, storeId, registerId);

            return this.javaScriptSerializer.Serialize(list);



        }





        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string GETInvoiceInquir(string datefrom, string dateto, string AccpacSyncUniqueNumber, int Posted , string InvoiceNumber)
        {
            List<E.POSDailySalesReport> list = new List<E.POSDailySalesReport>();
            list = iInvoiceManager.InvoiceInquirSalesReport(datefrom, dateto, AccpacSyncUniqueNumber, Posted, InvoiceNumber);

            return this.javaScriptSerializer.Serialize(list);



        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string GetPOSDailySales(int storeId, string itemNumber,string invoiceNumber ,string invoiceDateFrom,string datefrom,string dateTo)
        {
            List<E.POSDailySalesReport> list = new List<E.POSDailySalesReport>();
            list = iInvoiceManager.FindPOSDailySalesPerItemReport(storeId,itemNumber, invoiceNumber,invoiceNumber,datefrom,dateTo) ;

            return this.javaScriptSerializer.Serialize(list);
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string GetPOSSummaryDailySales(int storeId, string itemNumber, string invoiceNumber, string invoiceDateFrom, string datefrom, string dateTo)
        {
            List<E.POSDailySalesSummary> list = new List<E.POSDailySalesSummary>();
            list = iInvoiceManager.FindPOSDailySalesSummaryReport(storeId, itemNumber, invoiceNumber, invoiceNumber, datefrom, dateTo);

            return this.javaScriptSerializer.Serialize(list);
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string GetDashBoardBoxesMapper()
        {
            E.DashBoarBoxes list = new E.DashBoarBoxes();
            list = iInvoiceManager.FindDashBoardBoxed();

            return this.javaScriptSerializer.Serialize(list);
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string GetRegisterSales(int storeId)
        {
            List<E.RegistersSales> list = new List<E.RegistersSales>();
            list = iInvoiceManager.FindRegistersSales(storeId);

            return this.javaScriptSerializer.Serialize(list);
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string GetStoresSales()
        {
            List<E.StoreSales> list = new List<E.StoreSales>();
            list = iInvoiceManager.FindStoresSales();

            return this.javaScriptSerializer.Serialize(list);
        }






    }
}

