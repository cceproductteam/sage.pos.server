using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;

using E = SagePOS.Server.Business.Entity;
using SagePOS.Server.Business.IManager;
using CentrixERP.Common.API;
using System.Reflection;
using SagePOS.Server.Configuration;
using SF.FrameworkEntity;
using SagePOS.Server.API.Common.BaseClasses;
using SF.Framework;
using CentrixERP.Common.Business.IManager;
using SagePOS.Manger.Common.Business.Entity;



namespace SagePOS.Server.API
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.  
    [System.Web.Script.Services.ScriptService]
    [EntityAttribute(Enums_S3.Entity.Contact)]
    public class ContactWebService : POSWebServiceBaseClass
    {
        IContactManager iContactManager = null;
        IDataTypeContentManager iDataTypeContentManager = null;
        public ContactWebService()
        {
            iContactManager = (IContactManager)IoC.Instance.Resolve(typeof(IContactManager));
            iDataTypeContentManager = (IDataTypeContentManager)IoC.Instance.Resolve(typeof(IDataTypeContentManager));
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string Delete(int id)
        {

            E.Contact myContact = iContactManager.FindById(id, null);
            myContact.ContactId = id;
            myContact.MarkDeleted();
            try
            {
                iContactManager.Save(myContact);
                return AttachStatusCode(true, 1, null);
            }
            catch (Exception ex)
            {
                return AttachStatusCode(null, 0, HandleException(ex));
            }

        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindAllLiteTransactionSearch(string keyword, int page, int resultCount, string argsCriteria)
        {
            E.ContactCriteria criteria = null;
            if (!SF.Framework.String.IsEmpty(argsCriteria))
            {
                criteria = this.javaScriptSerializer.Deserialize<E.ContactCriteria>(argsCriteria);
            }
            else
                criteria = new E.ContactCriteria();

            criteria.Keyword = keyword;
            criteria.pageNumber = page;
            criteria.resultCount = resultCount;


            List<E.ContactLite> PersonList = iContactManager.FindAllLite(criteria);
            if (PersonList != null)
            {

                var person = from E.ContactLite ContacObj in PersonList
                             select new
                             {
                                 value = ContacObj.ContactId,

                                 label = (ContacObj.ContactTypeId == 1) ? ContacObj.FirstName + " " + (ContacObj.LastName) : ContacObj.CompanyName,

                                 //GrandTotal = PersonObj.GrandTotal,
                             };

                return this.AttachStatusCode(person, 1, null);


            }
            return null;

        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindAllLite(string keyword, int page, int resultCount, string Name, string PhoneNumber, string CompanyName, int ContactType)
        {
            E.ContactCriteria criteria = null;

            criteria = new E.ContactCriteria();

            if (Name == "")
            {
                Name = null;

            }
            if (PhoneNumber == "")
            {

                PhoneNumber = null;
            }
            if (CompanyName == "")
            {

                CompanyName = null;
            }



            criteria.Keyword = keyword;
            criteria.pageNumber = page;
            criteria.resultCount = resultCount;
            criteria.Name = Name;
            criteria.PhoneNumber = PhoneNumber;
            criteria.CompanyName = CompanyName;
            criteria.ContactType = ContactType;


            List<E.ContactLite> ContactList = iContactManager.FindAllLite(criteria);
            return this.AttachStatusCode(ContactList, 1, null);

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindAllLiteFirst(string keyword, int page, int resultCount)
        {
            E.ContactCriteria criteria = null;
            criteria = new E.ContactCriteria();



            criteria.Keyword = keyword;
            criteria.pageNumber = page;
            criteria.resultCount = resultCount;

            //criteria.ContactType = 0;




            List<E.ContactLite> ContactList = iContactManager.FindAllLite(criteria);
            return this.AttachStatusCode(ContactList, 1, null);

        }













        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindByIdLite(int id)
        {



            try
            {

                E.ContactLite myContactLite = iContactManager.FindByIdLite(id, null);
                return this.AttachStatusCode(myContactLite, 1, null);
            }


            catch (Exception ex)
            {

                return ex.ToString();
            }


        }




        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string Add(int id, string ContactInfo)
        {
            return Save(true, id, ContactInfo);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.Edit)]
        public string Edit(int id, string ContactInfo)
        {
            if (id <= 0)
            {
                ValidationError error = new ValidationError()
                {
                    ControlName = "",
                    GlobalMessage = true,
                    Message = "Please select Contact"
                };
                List<ValidationError> errors = new List<ValidationError>(){
                error
                };
                return this.AttachStatusCode(errors, 0, "validation");
            }
            else
            {
                return Save(false, id, ContactInfo);
            }
        }

        private string Save(bool add, int id, string ContactInfo)
        {

            E.Contact myContact = this.javaScriptSerializer.Deserialize<E.Contact>(this.unEscape((ContactInfo)));
           

            if (!add)
            {

                myContact.MarkOld();
                myContact.MarkModified();
                myContact.ContactId = myContact.ContactId;
                myContact.UpdatedBy = id;
            }
            else
            {
                myContact.CreatedBy = id;
               

                if (myContact.ContactType == 1)
                {
                    DataTypeContent dataTypeContentList = iDataTypeContentManager.FindById(myContact.Gender, null);

                    int vailddatatype = dataTypeContentList.DataTypeID;
                    if (vailddatatype != 5)
                    {

                        return this.AttachStatusCode(null, 903, "Gender  is not Vaild");


                    }
                }



                if (myContact.ContactType == 1)
                {
                    string firstnew = myContact.FirstName.Replace(" ", "");
                    string lastnew = myContact.LastName.Replace(" ", "");
                    myContact.CompanyName = "";
                    if ((firstnew.Count() == 0 && lastnew.Count() == 0))
                    {



                        return this.AttachStatusCode(null, 901, "FirstName and LastName is mandatory");
                    }



                }

                else if (myContact.ContactType == 2)
                {
                    string CompanyNamenew = myContact.CompanyName.Replace(" ", "");
                    myContact.FirstName = "";
                    myContact.LastName = "";


                    if (CompanyNamenew.Count() == 0)
                    {
                        return this.AttachStatusCode(null, 902, "CompanyName is mandatory");
                    }


                }
                int resul = 0;
                bool status = int.TryParse(myContact.PhoneNumber, out resul);
                try
                {
                    Int64 number = Convert.ToInt64(myContact.PhoneNumber);
                }
                catch (Exception)
                {

                    return this.AttachStatusCode(null, 903, "Phone number not valid");
                }



                if (!IsValidEmail(myContact.Email))
                    return this.AttachStatusCode(null, 903, "email not valid");




                E.ContactCriteria criteriatosave = new E.ContactCriteria();
                criteriatosave = new E.ContactCriteria();
                criteriatosave.Email = myContact.Email;

                List<E.ContactLite> ContactListemail = iContactManager.FindAllLite(criteriatosave);

                if (ContactListemail != null)
                {

                    return this.AttachStatusCode(null, 600, "this Email is Already Exist ");

                }

                criteriatosave = new E.ContactCriteria();
                criteriatosave.PhoneNumber = myContact.PhoneNumber;
                List<E.ContactLite> ContactListphone = iContactManager.FindAllLite(criteriatosave);

                if (ContactListphone != null)
                {

                    return this.AttachStatusCode(null, 700, "this PhoneNumber is Already Exist");

                }
                if (myContact.CompanyName != null && myContact.ContactType == 2)
                {
                    criteriatosave = new E.ContactCriteria();

                    criteriatosave.CompanyName = myContact.CompanyName;
                    List<E.ContactLite> ContactListcom = iContactManager.FindAllLite(criteriatosave);
                    if (ContactListcom != null)
                    {
                        return this.AttachStatusCode(null, 800, "this CompanyName is Already Exist ");

                    }


                }

                myContact.CreatedBy = id;

            }


            //List<ValidationError> errors = iContactManager.Validate(myContact);
            //if (errors != null && errors.Count > 0)
            //{
            //    return this.AttachStatusCode(errors, 0, "validation");
            //}






            try
            {
                if (myContact.ContactType == 1)
                    myContact.ContactName = myContact.FirstName + " " + myContact.LastName;
                else
                    myContact.ContactName = myContact.CompanyName;
             
                iContactManager.Save(myContact);
                this.UnLockEntity(Enums_S3.Entity.Contact, myContact.ContactId);

                return this.AttachStatusCode(myContact, 1, null);

            }
            catch (SF.Framework.Exceptions.RecordExsitsException ex)
            {
                return this.AttachStatusCode(null, 501, null);
            }
            catch (Exception ex)
            {

                return this.AttachStatusCode(null, 0, HandleException(ex));
            }
        }


        bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
    }
}

