using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;

using E = SagePOS.Server.Business.Entity;
using SagePOS.Server.Business.IManager;
using CentrixERP.Common.API;
using System.Reflection;
using SagePOS.Server.Configuration;
using SF.FrameworkEntity;
using SagePOS.Server.API.Common.BaseClasses;
using SF.Framework;



namespace SagePOS.Server.API
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    [EntityAttribute(Enums_S3.Entity.Invoice)]
    public class TransactionTypeWebService : POSWebServiceBaseClass
    {
        ITransactionTypeManager iTransactionTypeManager = null;
        public TransactionTypeWebService()
        {
            iTransactionTypeManager = (ITransactionTypeManager)IoC.Instance.Resolve(typeof(ITransactionTypeManager));
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string Delete(int id)
        {
           
            E.TransactionType myTransactionType = iTransactionTypeManager.FindById(id, null);
            myTransactionType.TransactionTypeId = id;
            myTransactionType.MarkDeleted();
            try
            {
                iTransactionTypeManager.Save(myTransactionType);
                return AttachStatusCode(true, 1, null);
            }
            catch (Exception ex)
            {
                return AttachStatusCode(null, 0, HandleException(ex));
            }

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindAllLite(string keyword, int page, int resultCount, string argsCriteria)
        {
            E.TransactionTypeCriteria criteria = null;
            if (!SF.Framework.String.IsEmpty(argsCriteria))
            {
                criteria = this.javaScriptSerializer.Deserialize<E.TransactionTypeCriteria>(argsCriteria);
            }
            else
                criteria = new E.TransactionTypeCriteria();

            criteria.Keyword = keyword;
            criteria.pageNumber = page;
            criteria.resultCount = resultCount;


            List<E.TransactionTypeLite> TransactionTypeList = iTransactionTypeManager.FindAllLite(criteria);
            return this.AttachStatusCode(TransactionTypeList, 1, null);

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindByIdLite(int id)
        {
            E.TransactionTypeLite myTransactionType = iTransactionTypeManager.FindByIdLite(id, null);
            return this.AttachStatusCode(myTransactionType, 1, null);

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string Add(int id, string TransactionTypeInfo)
        {
            return Save(true, id, TransactionTypeInfo);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.Edit)]
        public string Edit(int id, string TransactionTypeInfo)
        {
            if (id <= 0)
            {
                ValidationError error = new ValidationError()
                {
                    ControlName = "",
                    GlobalMessage = true,
                    Message = "Please select TransactionType"
                };
                List<ValidationError> errors = new List<ValidationError>(){
                error
                };
                return this.AttachStatusCode(errors, 0, "validation");
            }
            else
            {
                return Save(false, id, TransactionTypeInfo);
            }
        }

        private string Save(bool add, int id, string TransactionTypeInfo)
        {
            E.TransactionType myTransactionType = this.javaScriptSerializer.Deserialize<E.TransactionType>(this.unEscape((TransactionTypeInfo)));
            if (!add)
            {
             
                myTransactionType.MarkOld();
                myTransactionType.MarkModified();
                myTransactionType.TransactionTypeId = id;
                myTransactionType.UpdatedBy = LoggedInUser.UserId;
            }
            else
            {
                myTransactionType.CreatedBy = LoggedInUser.UserId;
            }


            List<ValidationError> errors = iTransactionTypeManager.Validate(myTransactionType);
            if (errors != null && errors.Count > 0)
            {
                return this.AttachStatusCode(errors, 0, "validation");
            }


            try
            {
                iTransactionTypeManager.Save(myTransactionType);
             
                return this.AttachStatusCode(myTransactionType.TransactionTypeId, 1, null);

            }
            catch (SF.Framework.Exceptions.RecordExsitsException ex)
            {
                return this.AttachStatusCode(null, 501, null);
            }
            catch (Exception ex)
            {

                return this.AttachStatusCode(null, 0, HandleException(ex));
            }
        }
    }
}

