﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using CentrixERP.Common.API;
using Centrix.UM.Business.Entity;
using Centrix.UM.Business.IManager;
using SF.Framework;
using System.Web.Script.Serialization;
using SagePOS.Server.Business.IManager;
using SagePOS.Server.Business.Entity;
using CentrixERP.Common.Business.IManager;
using SagePOS.Server.Configuration;
using SagePOS.Manger.Common.Business.Entity;
using SagePOS.Server.API.Common.BaseClasses;

namespace SagePOS.Server.API
{
    /// <summary>
    /// Summary description for SystemConfigrationPOS
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class SystemConfigrationPOS : POSWebServiceBaseClass
    {


        public IUserManager userManager = (IUserManager)IoC.Instance.Resolve(typeof(IUserManager));
        public IPosSystemConfigrationManager iPosSystemConfigrationManager = (IPosSystemConfigrationManager)IoC.Instance.Resolve(typeof(IPosSystemConfigrationManager));

        public IUserLogginManager iUserLogginManager = IoC.Instance.Resolve<IUserLogginManager>();

        public IDataTypeContentManager iDataTypeContentManager = (IDataTypeContentManager)IoC.Instance.Resolve(typeof(IDataTypeContentManager));

        public IDataTypeManager iDataTypeManager = (IDataTypeManager)IoC.Instance.Resolve(typeof(IDataTypeManager));
        public IPOSPaymentCodeManager iPaymentCodeManager = (IPOSPaymentCodeManager)IoC.Instance.Resolve(typeof(IPOSPaymentCodeManager));
        public ICustomFildManager iCustomFildManager = (ICustomFildManager)IoC.Instance.Resolve(typeof(ICustomFildManager));
        public IRoleManager roleManager = (IRoleManager)IoC.Instance.Resolve(typeof(IRoleManager));
            public IRolePermissionManager rolePermissionManager = (IRolePermissionManager)IoC.Instance.Resolve(typeof(IRolePermissionManager));

        public ICloseRegisterManager iCloseRegisterManager = (ICloseRegisterManager)IoC.Instance.Resolve(typeof(ICloseRegisterManager));

         public ILocalConfigurationManager iLocalConfigurationManager = (ILocalConfigurationManager)IoC.Instance.Resolve(typeof(ILocalConfigurationManager));
         public IStoreManager iStoreManager = (IStoreManager)IoC.Instance.Resolve(typeof(IStoreManager));
   
         public IRegisterManager iRegisterManager =        (IRegisterManager)IoC.Instance.Resolve(typeof(IRegisterManager));
         public IQuickKeysProductManager iQuickKeysProductManager = (IQuickKeysProductManager)IoC.Instance.Resolve(typeof(IQuickKeysProductManager));


         public IInvoiceManager iInvoiceManager =  (IInvoiceManager)IoC.Instance.Resolve(typeof(IInvoiceManager));
         public IPOSCurrencyRateDetailsManager CurrencyRateManager = (IPOSCurrencyRateDetailsManager)IoC.Instance.Resolve(typeof(IPOSCurrencyRateDetailsManager));


        public JavaScriptSerializer javaScriptSerializer;

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string[] GetAllSystemConfigration(string keyword, int page, int resultCount, int dataTypeContentparentId,string keywordpaymentcode, int pagepaymentcode, int resultCountpaymentcode, string argsCriteriapaymentcode, string keywordCustomFields, int pageCustomFields, int resultCountCustomFields, string argsCriteriaCustomFields, string keywordlocalconfiguration, int pagelocalconfiguration, int resultCountlocalconfiguration, string argsCriterialocalconfiguration, int RegisterIdlocalconfiguration, int RegisterIdRegisterStatus, int UserIdRegisterStatus, int RolePermissionid, int idfindindByIdLiteCloseRegister, int storeId, string keywordQuickKey, int pageQuickKey, int resultCountQuickKey, string argsCriteriaQuickKey, int RegisteByidID, string keywordInvoice, int pageInvoice, int resultCountInvoice, string argsCriteriaInvoice, int CloseRegisterIdTotal, int StoreIdeRegisterData, int RegisterIdeRegisterData, int UserIdeRegisterData, int CloseRegisterIdeRegisterData, int CurrencyCode)

                    
        {


            javaScriptSerializer = new JavaScriptSerializer();
            javaScriptSerializer.MaxJsonLength = 2147483644;
            
            //getlogeeduser
            string GetLoggedUsert = "";

            string PosSystemCofig = "";
            string datataypePOS = "";

            string PaymentCode = "";
            string RolePermission = "";
             string CustomFields ="";
             string localConfiguration = "";

             string CheckRegister = "";


             string CloseRegisterbyid = "";
            string ByIdStore=  "";
            string  QuickKey = "";

            string Registerbyid = "";


            string FindAllInvoices = "";

            string CloseRegisterTotal = "";
            string CloseRegisterData = "";
            string RatesByCurrencyCode = "";
            //var results = new { GetLoggedUsert, PosSystemCofig };


            //GetLoggedUsert =  GetLoggedInUser();

            PosSystemCofig = FindAllLitePosSystemConfigration(keyword, page, resultCount);

            datataypePOS = FindAlldataTypeContent(dataTypeContentparentId);

           PaymentCode = FindAllLitePaymentCode(keywordpaymentcode, pagepaymentcode, resultCountpaymentcode, argsCriteriapaymentcode);
          
           CustomFields = FindAllLiteCustomFields(keywordCustomFields,pageCustomFields,resultCountCustomFields,argsCriteriaCustomFields);

           localConfiguration = FindAllLiteLocalConfiguration(keywordlocalconfiguration,pagelocalconfiguration,resultCountlocalconfiguration,argsCriterialocalconfiguration,RegisterIdlocalconfiguration);
           CheckRegister = CheckRegisterStatus(RegisterIdRegisterStatus, UserIdRegisterStatus);
           //RolePermission = FindRolePermissionLite(RolePermissionid);
           CloseRegisterbyid = FindByIdLiteCloseRegister(idfindindByIdLiteCloseRegister);
           ByIdStore = FindByIdLiteStore(storeId);
            QuickKey = FindAllLiteQuickKey(keywordQuickKey,pageQuickKey,resultCountQuickKey,argsCriteriaQuickKey);
            Registerbyid = FindByIdLiteRegister(RegisteByidID);

            FindAllInvoices = FindAllLiteInvoice(keywordInvoice,pageInvoice,resultCountInvoice,argsCriteriaInvoice);
            CloseRegisterTotal = GetCloseRegisterTotal(CloseRegisterIdTotal);
            CloseRegisterData = GetCloseRegisterData(StoreIdeRegisterData, RegisterIdeRegisterData, UserIdeRegisterData, CloseRegisterIdeRegisterData);
            RatesByCurrencyCode = GetRatesByCurrencyCode(CurrencyCode);
             //results = new { GetLoggedUsert, PosSystemCofig };
            string[] Objectarrry = new string[] { GetLoggedUsert, PosSystemCofig, datataypePOS, PaymentCode, CustomFields, localConfiguration, CheckRegister, CloseRegisterbyid, ByIdStore, QuickKey, Registerbyid, FindAllInvoices, CloseRegisterTotal, CloseRegisterData, RatesByCurrencyCode };
             int a = Objectarrry.Length;
             return Objectarrry;

        
        
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string GetLoggedInUser(int userid)
        {
            UserLite user = userManager.FindByIdLite(userid, null);

            return AttachStatusCode(user, 1, null);

        }


        public string FindAllLitePosSystemConfigration(string keyword, int page, int resultCount)
        {
            PosSystemConfigrationCriteria criteria = new PosSystemConfigrationCriteria();
            criteria.Keyword = keyword;
            criteria.pageNumber = new int?(page);
            criteria.resultCount = new int?(resultCount);
            List<PosSystemConfigrationLite> PosSystemConfigrationList = this.iPosSystemConfigrationManager.FindAllLite(criteria);
            string result;
            if (PosSystemConfigrationList != null)
            {
                result = AttachStatusCode(PosSystemConfigrationList.First<PosSystemConfigrationLite>(), 1, null);
            }
            else
            {
                result = AttachStatusCode(null, 1, null);
            }
            return result;
        }


        public string FindAlldataTypeContent(int dataTypeContentparentId)
		{
            List<DataTypeContent> dataTypeContentList = this.iDataTypeContentManager.FindByParentId<int>(dataTypeContentparentId, typeof(DataType), null);
			string result2;
			if (dataTypeContentList != null && dataTypeContentList.Count > 0)
			{
				var result = from DataTypeContent content in dataTypeContentList
				select new
				{
					label = ((Configuration.Configuration.Lang  == Enums_S3.Configuration.Language.ar) ? content.DataTypeContentAR : content.DataTypeContentEN),
					value = content.DataTypeContentID,
					categoryId = -1,
					category = ""
				};
                result2 = AttachStatusCode(result, 0, null);
			}
			else
			{
				result2 = AttachStatusCode(null, 0, null);
			}
			return result2;
		}


   public string FindAllLitePaymentCode(string keyword, int page, int resultCount, string argsCriteria)
   {
       POSPaymentCodeCriteria criteria;
       if (!argsCriteria.IsEmpty())
       {
           criteria = this.javaScriptSerializer.Deserialize<POSPaymentCodeCriteria>(argsCriteria);
       }
       else
       {
           criteria = new POSPaymentCodeCriteria();
       }
       criteria.Keyword = keyword;
       criteria.pageNumber = new int?(page);
       criteria.resultCount = new int?(resultCount);
       List<POSPaymentCodeLite> PaymentCodeList = this.iPaymentCodeManager.FindAllLite(criteria);
       return AttachStatusCode(PaymentCodeList, 1, null);
   }



 

        public string FindAllLiteCustomFields(string keywordCustomFields, int pageCustomFields, int resultCountCustomFields, string argsCriteriaCustomFields)
        {
            CustomFildCriteria criteria;
            if (!argsCriteriaCustomFields.IsEmpty())
            {
                criteria = this.javaScriptSerializer.Deserialize<CustomFildCriteria>(argsCriteriaCustomFields);
            }
            else
            {
                criteria = new CustomFildCriteria();
            }
            criteria.Keyword = (keywordCustomFields);
            criteria.pageNumber = (pageCustomFields);
            criteria.resultCount = (pageCustomFields);
            List<CustomFildLite> CustomFildList = this.iCustomFildManager.FindAllLite(criteria);
            return AttachStatusCode(CustomFildList, 1, null);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindRolePermissionLite(int roleId)
        {
            RolePermissionCriteria criteria = new RolePermissionCriteria();
            criteria.getAllPermissions = true;
            List<RolePermissionLite> list = this.rolePermissionManager.FindAllRolePermission(roleId, criteria);
            string result;
            if (list != null)
            {
                var entites = from RolePermissionLite litePermission in list
                              group litePermission by new
                              {
                                  litePermission.EntityId,
                                  litePermission.EntityName,
                                  litePermission.EntityNameAr
                              } into entity
                              select new
                              {
                                  EntityId = entity.Key.EntityId,
                                  EntityName = entity.Key.EntityName,
                                  EntityNameAr = entity.Key.EntityNameAr,
                                  Permissions = from RolePermissionLite permission in list
                                                where entity.Key.EntityId == ((permission.ParentId != 0) ? permission.ParentId : permission.EntityId)
                                                select new
                                                {
                                                    permission.PermissionId,
                                                    permission.EntityPermissionId,
                                                    permission.PermissionName,
                                                    permission.PermissionNameAr,
                                                    permission.PermissionDescription,
                                                    permission.DefaultPermission,
                                                    permission.HasPermission,
                                                    permission.RolePermissionId,
                                                    permission.ModuleName,
                                                    permission.ModuleNameAR,
                                                    permission.ModuleId
                                                }
                              };
                result = AttachStatusCode(entites, 1, null);
            }
            else
            {
                result = AttachStatusCode(null, 1, null);
            }
            return result;
        }



        public string FindAllLiteLocalConfiguration(string keywordlocalconfiguration, int pagelocalconfiguration, int resultCountlocalconfiguration, string argsCriterialocalconfiguration, int RegisterIdlocalconfiguration)
        {
            LocalConfigurationCriteria criteria = null;
            if (!SF.Framework.String.IsEmpty(argsCriterialocalconfiguration))
            {
                criteria = this.javaScriptSerializer.Deserialize<LocalConfigurationCriteria>(argsCriterialocalconfiguration);
            }
            else
                criteria = new LocalConfigurationCriteria();

            criteria.Keyword = keywordlocalconfiguration;
            criteria.pageNumber = pagelocalconfiguration;
            criteria.resultCount = resultCountlocalconfiguration;
            criteria.RegisterId = RegisterIdlocalconfiguration;


            List<LocalConfigurationLite> LocalConfigurationList = iLocalConfigurationManager.FindAllLite(criteria);
            return AttachStatusCode(LocalConfigurationList, 1, null);

        }

        public string CheckRegisterStatus(int RegisterIdRegisterStatus, int UserIdRegisterStatus)
        {
            ICloseRegisterManager myManager = (ICloseRegisterManager)IoC.Instance.Resolve(typeof(ICloseRegisterManager));
            CloseRegisterCriteria myCriteria = new CloseRegisterCriteria();
            myCriteria.UserId = UserIdRegisterStatus;
            myCriteria.RegisterId = RegisterIdRegisterStatus;
            List<CloseRegister> myList = myManager.FindAll(myCriteria);


            if (myList != null && myList.Count > 0)
            {
                var closeregisterData = new
                {
                    closeRegisterId = myList.First().CloseRegisterId,
                    RegisterId = myList.First().RegisterId,
                    OpenedCashLoan = myList.First().OpenedCashLoan,
                    OpenDate = myList.First().OpenedDate.Value.ToString("dd/MM/yyyy hh:mm:ss"),
                    CloseDate = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss"),
                    SessionNumber = myList.First().SessionNumber
                };

                return this.javaScriptSerializer.Serialize(closeregisterData);
            }
            else
                return null;

        }

        public string FindByIdLiteCloseRegister(int idfindindByIdLiteCloseRegister)
        {
            CloseRegisterLite myCloseRegister = iCloseRegisterManager.FindByIdLite(idfindindByIdLiteCloseRegister, null);
            return this.AttachStatusCode(myCloseRegister, 1, null);

        }
        public string FindByIdLiteStore(int id)
        {
            StoreLite myStore = this.iStoreManager.FindByIdLite(id, null);
            return  AttachStatusCode(myStore, 1, null);
        }



       public string FindAllLiteQuickKey(string keyword, int page, int resultCount, string argsCriteria)
		{
			QuickKeysProductCriteria criteria;
			if (!SF.Framework.String.IsEmpty(argsCriteria))
			{
				criteria = this.javaScriptSerializer.Deserialize<QuickKeysProductCriteria>(argsCriteria);
			}
			else
			{
				criteria = new QuickKeysProductCriteria();
			}
			criteria.Keyword = keyword;
			criteria.pageNumber = new int?(page);
			criteria.resultCount = new int?(resultCount);
			List<QuickKeysProductLite> QuickKeysProductList = iQuickKeysProductManager.FindAllLite(criteria);
			return AttachStatusCode(QuickKeysProductList, 1, null);
		}

       public string FindByIdLiteRegister(int id)
       {
           RegisterLite myRegister = this.iRegisterManager.FindByIdLite(id, null);
           return AttachStatusCode(myRegister, 1, null);
       }

       public string FindAllLiteInvoice(string keyword, int page, int resultCount, string argsCriteriaInvoice)
       {
           InvoiceCriteria criteria = null;
           if (!SF.Framework.String.IsEmpty(argsCriteriaInvoice))
           {
               criteria = this.javaScriptSerializer.Deserialize<InvoiceCriteria>(argsCriteriaInvoice);
           }
           else
               criteria = new InvoiceCriteria();

           criteria.Keyword = keyword;
           criteria.pageNumber = page;
           criteria.resultCount = resultCount;


           List<InvoiceLite> InvoiceList = iInvoiceManager.FindAllLite(criteria);
           return this.AttachStatusCode(InvoiceList, 1, null);

       }

       public string GetCloseRegisterTotal(int CloseRegisterId)
       {
           ICloseRegisterManager myManager = (ICloseRegisterManager)IoC.Instance.Resolve(typeof(ICloseRegisterManager));
           CloseRegisterTotals Totals = new CloseRegisterTotals();
           Totals = myManager.FindCloseRegisterSalesTotal(CloseRegisterId);
           if (Totals != null)
               return this.javaScriptSerializer.Serialize(Totals);
           else
               return null;
       }


       public string GetCloseRegisterData(int StoreId, int RegisterId, int UserId, int CloseRegisterId)
       {

           ICloseRegisterManager myManager = (ICloseRegisterManager)IoC.Instance.Resolve(typeof(ICloseRegisterManager));
           List<CloseRegiserDetails> myDetailsList = null;
           CloseRegister myCloseRegister = null;
           CloseRegisterCriteria myCriteria = new CloseRegisterCriteria();

           myCloseRegister = myManager.FindById(CloseRegisterId, myCriteria);
           myCriteria.RegisterId = RegisterId;
           myCriteria.StoreId = StoreId;
           myCriteria.UserId = UserId;
           myCriteria.CloseRegisterId = CloseRegisterId;
           myDetailsList = myManager.GetCloseRegisterDetailsPaymentType(myCriteria);
           if (myCloseRegister != null)
           {
               var CloseRegisterObj = new
               {
                   openDate = (myCloseRegister.OpenedDate != null) ? myCloseRegister.OpenedDate.Value.ToString() : "",
                   closeDate = (myCloseRegister.ClosedDate != null) ? myCloseRegister.ClosedDate.Value.ToString() : ""
               };

               var DetailsObj = from Details in myDetailsList
                                select new
                                {
                                    PaymentType = Details.PaymentType,
                                    TotalPrice = Details.TotalPrice,
                                    Name = Details.PaymentTypeName,
                                    Currency = Details.Currency,
                                    CurrencyDefaultPrice = Details.CurrencyDefaultPrice,
                                    cashLoan = Details.CashLoan,
                                    invoiceStatus = Details.InvoiceStatus,
                                    CreditCardType = Details.CreditCardType,
                                    InvoiceCount = Details.InvoiceCount
                                };


               var CloseRegisterDetails = new
               {
                   CloseRegisterDetailsObject = DetailsObj,
                   CloseRegisterObject = CloseRegisterObj
               };
               return this.javaScriptSerializer.Serialize(CloseRegisterDetails);
           }
           else
           {
               return null;
           }
       }
       public string GetRatesByCurrencyCode(int CurrencyCode)
       {
           POSCurrencyRateDetailsCriteria criteria = new POSCurrencyRateDetailsCriteria();
           criteria.Keyword = "";
           criteria.pageNumber = new int?(1);
           criteria.resultCount = new int?(10);
           criteria.CurrencyId = CurrencyCode;
           string result;
           try
           {
               List<POSCurrencyRateDetailsLite> BoCurrencyRateList = this.CurrencyRateManager.FindAllLite(criteria);
               result = AttachStatusCode(BoCurrencyRateList, 1, null);
           }
           catch (Exception ex)
           {
               result = (ex.ToString());
           }
           return result;
       }





        //custome
        public string AttachStatusCode(object returnData, int messageCode, string message)
        {

            this.javaScriptSerializer = new JavaScriptSerializer();
            var Code = new
            {
                statusCode = new
                {
                    Code = messageCode,
                    message = message
                },
                result = returnData
            };
            return this.javaScriptSerializer.Serialize(Code);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindAll(int parentId)
        {
            List<DataTypeContent> dataTypeContentList = iDataTypeContentManager.FindByParentId(parentId, typeof(DataType), null);

            if (dataTypeContentList != null && dataTypeContentList.Count > 0)
            {
                var result = from DataTypeContent content in dataTypeContentList
                             select new
                             {
                                 label = (SagePOS.Server.Configuration.Configuration.Lang == SagePOS.Server.Configuration.Enums_S3.Configuration.Language.ar) ? content.DataTypeContentAR : content.DataTypeContentEN,
                                 value = content.DataTypeContentID,
                                 categoryId = -1,
                                 category = "",

                             };

                return this.AttachStatusCode(result, 0, null);
            }
            else
                return this.AttachStatusCode(null, 0, null);

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindUserAllLite(string keyword, int page, int resultCount, string argsCriteria)
        {
            UserCriteria userCriteria = null;
            if (!SF.Framework.String.IsEmpty(argsCriteria))
            {
                userCriteria = this.javaScriptSerializer.Deserialize<UserCriteria>(argsCriteria);
            }
            else
                userCriteria = new UserCriteria();


            userCriteria.Keyword = keyword;
            userCriteria.pageNumber = page;
            userCriteria.resultCount = resultCount;

            List<UserLite> usersList = userManager.FindAllLite(userCriteria);

            if (usersList != null)
            {
                var UserList = from UserLite Lite in usersList
                               select new
                               {
                                   Id = Lite.Id,
                                   UserId = Lite.UserId,
                                   UserName = Lite.UserName,
                                   Password = Lite.Password,
                                   FirstnameEnglish = Lite.FirstnameEnglish,
                                   MiddleNameEnglish = Lite.MiddleNameEnglish,
                                   LastNameEnglish = Lite.LastNameEnglish,
                                   FirstNameArabic = Lite.FirstNameArabic,
                                   MiddleNameArabic = Lite.MiddleNameArabic,
                                   LastNameArabic = Lite.LastNameArabic,
                                   Dob = Lite.Dob,
                                   DobValue = Lite.DobValue,
                                   Title = Lite.Title,
                                   Notes = Lite.Notes,
                                   CreatedBy = Lite.CreatedById,
                                   UpdatedBy = Lite.UpdatedById,
                                   UpdatedDate = Lite.UpdatedDate,
                                   CreatedDate = Lite.CreatedDate,
                                   UpdatedByName = Lite.UpdatedByName,
                                   CreatedByName = Lite.CreatedByName,
                                   RoleNameId = Lite.RoleNameId,
                                   TeamNameId = Lite.TeamNameId,
                                   ReceiveNoyificationsValue = Lite.ReceiveNoyificationsValue,
                                   ReceiveNoyifications = Lite.ReceiveNoyifications,
                                   FullName = Lite.FullName,
                                   RoleName = Lite.RoleName,
                                   TeamName = Lite.TeamName,
                                   UserEmail = Lite.UserEmail,
                                   LastRows = Lite.LastRows,
                                   TotalRecords = Lite.TotalRecords,
                                   label = Lite.FirstnameEnglish + " " + Lite.LastNameEnglish,
                                   ManagerId = Lite.ManagerId,
                                   Manager = Lite.Manager,
                                   IsDefault = Lite.IsDefault,
                                   IsDefaultValue = Lite.IsDefaultValue,
                                   IsManager = Lite.IsManager,
                                   IsManagerValue = Lite.IsManagerValue,
                                   value = Lite.UserId,
                                   categoryId = 2,
                                   category = "",
                                   IsSupperUser = Lite.IsSupperUser
                               };
                return this.AttachStatusCode(UserList, 0, null);
            }
            return this.AttachStatusCode(null, 0, null);
        }
    }
}
