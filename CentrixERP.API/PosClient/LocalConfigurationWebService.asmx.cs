using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using SF.Framework;
//using E = Centrix.POS.Standalone.Client.Business.Entity;

using E = SagePOS.Server.Business.Entity;
//using Centrix.POS.Standalone.Client.Business.IManager;

using SagePOS.Server.Business.IManager;
using SagePOS.Server.Business.Entity;

using CentrixERP.Common.API;
using System.Reflection;
using SagePOS.Server.Configuration;
using SF.FrameworkEntity;
using System.Web.Script.Serialization;
using System.Net;
using System.Net.Sockets;
using System.Net.NetworkInformation;

using SagePOS.Server.API.Common.BaseClasses;
using SagePOS.Server.Configuration;
using System.Web.Configuration;
using System.Configuration;
using System.IO;
using System.Security.AccessControl;
using System.Security.Principal;



namespace SagePOS.Server.API
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
 
    public class LocalConfigurationWebService : POSWebServiceBaseClass
    {
        ILocalConfigurationManager iLocalConfigurationManager = null;
        public LocalConfigurationWebService()
        {
            iLocalConfigurationManager = (ILocalConfigurationManager)IoC.Instance.Resolve(typeof(ILocalConfigurationManager));
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string Delete(int id)
        {
           
            E.LocalConfiguration myLocalConfiguration = iLocalConfigurationManager.FindById(id, null);
            //myLocalConfiguration.LocalConfigurationId = id;
            myLocalConfiguration.MarkDeleted();
            try
            {
                iLocalConfigurationManager.Save(myLocalConfiguration);
                return AttachStatusCode(true, 1, null);
            }
            catch (Exception ex)
            {
                return AttachStatusCode(null, 0, HandleException(ex));
            }

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindAllLite(string keyword, int page, int resultCount, string argsCriteria, int RegisterId)
        {
            E.LocalConfigurationCriteria criteria = null;
            if (!SF.Framework.String.IsEmpty(argsCriteria))
            {
                criteria = this.javaScriptSerializer.Deserialize<E.LocalConfigurationCriteria>(argsCriteria);
            }
            else
                criteria = new E.LocalConfigurationCriteria();

            criteria.Keyword = keyword;
            criteria.pageNumber = page;
            criteria.resultCount = resultCount;
            criteria.RegisterId = RegisterId;


            List<E.LocalConfigurationLite> LocalConfigurationList = iLocalConfigurationManager.FindAllLite(criteria);
            return this.AttachStatusCode(LocalConfigurationList, 1, null);

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindByIdLite(int id)
        {
            E.LocalConfigurationLite myLocalConfiguration = iLocalConfigurationManager.FindByIdLite(id, null);
            return this.AttachStatusCode(myLocalConfiguration, 1, null);

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string Add(int id, string LocalConfigurationInfo)
        {
            return Save(true, id, LocalConfigurationInfo);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string Edit(int id, string LocalConfigurationInfo)
        {
            if (id <= 0)
            {
                ValidationError error = new ValidationError()
                {
                    ControlName = "",
                    GlobalMessage = true,
                    Message = "Please select LocalConfiguration"
                };
                List<ValidationError> errors = new List<ValidationError>(){
                error
                };
                return this.AttachStatusCode(errors, 0, "validation");
            }
            else
            {
                return Save(false, id, LocalConfigurationInfo);
            }
        }

        private string Save(bool add, int id, string LocalConfigurationInfo)
        {
            E.LocalConfiguration myLocalConfiguration = this.javaScriptSerializer.Deserialize<E.LocalConfiguration>(this.unEscape((LocalConfigurationInfo)));
            if (!add)
            {
               
                myLocalConfiguration.MarkOld();
                myLocalConfiguration.MarkModified();
                //myLocalConfiguration.LocalConfigurationId = id;
                myLocalConfiguration.UpdatedBy = LoggedInUser.UserId;
            }
            else
            {
                myLocalConfiguration.CreatedBy = LoggedInUser.UserId;
            }


            List<ValidationError> errors = iLocalConfigurationManager.Validate(myLocalConfiguration);
            if (errors != null && errors.Count > 0)
            {
                return this.AttachStatusCode(errors, 0, "validation");
            }


            try
            {
                iLocalConfigurationManager.Save(myLocalConfiguration);
                if (!add)

                    return this.AttachStatusCode(-1, 1, null); 

            }
            catch (SF.Framework.Exceptions.RecordExsitsException ex)
            {
                return this.AttachStatusCode(null, 501, null);
            }
            catch (Exception ex)
            {

                return this.AttachStatusCode(null, 0, HandleException(ex));
            }
            return null;
        }


        //[WebMethod(EnableSession = true)]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        //public string CheckRegisterSyncMode(string IpAddress, string UserName, string PassWord, string DataBaseName)
        //{

        //    //ILocalConfigurationManager myManager = (ILocalConfigurationManager)IoC.Instance.Resolve(typeof(ILocalConfigurationManager));
        //    //RegisterCriteria myCriteria = new RegisterCriteria();
        //    //JavaScriptSerializer ser = new JavaScriptSerializer();
        //    ////myCriteria.IpAddress = Server.UrlDecode(IpAddress);
        //    ////myCriteria.RegisterUserName = UserName;
        //    ////myCriteria.RegisterPassword = PassWord;
        //    ////myCriteria.DataBaseName = DataBaseName;
        //    //int ErrorType = 0;
        //    //string SyncMode = "";
        //    //try
        //    //{
        //    //   // E.LocalConfiguration mylocalConfig = myManager.CheckRegisterSyncMode(myCriteria);
        //    //    if (mylocalConfig != null)
        //    //        SyncMode = mylocalConfig.SyncMode.ToString();
        //    //    else
        //    //        SyncMode = "1";

        //    //}
        //    catch (Exception ex)
        //    {
        //        if (!System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
        //        {
        //            ErrorType = 1;//Connection Error
        //        }
        //        if (ex.GetType() == typeof(System.Data.SqlClient.SqlException))
        //        {
        //            ErrorType = 2;
        //        }
        //        if (ex.GetType() == typeof(SF.Framework.Exceptions.RecordNotAffected))
        //        {
        //            ErrorType = 3;
        //        }

        //    }

        //    var Result = new
        //    {
        //        SyncMode,
        //        ErrorType
        //    };

        //    return ser.Serialize(Result);
        //}


        //[WebMethod(EnableSession = true)]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        //public string ResetRegisterConfiguration()
        //{
        //    ILocalConfigurationManager myManager = (ILocalConfigurationManager)IoC.Instance.Resolve(typeof(ILocalConfigurationManager));
        //    IPosConfigurationManager myConfigMgr = (IPosConfigurationManager)IoC.Instance.Resolve(typeof(IPosConfigurationManager));
        //    E.LocalConfigurationCriteria myCriteria = new E.LocalConfigurationCriteria();
        //    List<PosConfigurationLite> myPOsConfigList = myConfigMgr.FindAllLite(null);
        //    JavaScriptSerializer ser = new JavaScriptSerializer();
        //    myCriteria.IPAddress = GetIPAddress();
        //    myCriteria.ServerDataBaseName = myPOsConfigList.First().PosServerDataBaseName;
        //    myCriteria.ServerIP = myPOsConfigList.First().PosServerDataBaseInstanceName;
        //    myCriteria.ServerPassword = myPOsConfigList.First().PosServerDataBasePassword;
        //    myCriteria.ServerUserName = myPOsConfigList.First().PosServerDataBaseUserName;

        //    int IsReset = 0;
        //    int ErrorType = 0;
        //    try
        //    {
        //        IsReset = myManager.ResetLocalconfiguration(myCriteria);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (!System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
        //        {
        //            ErrorType = 1;//Connection Error
        //        }
        //        if (ex.GetType() == typeof(System.Data.SqlClient.SqlException))
        //        {
        //            ErrorType = 2;
        //        }
        //        if (ex.GetType() == typeof(SF.Framework.Exceptions.RecordNotAffected))
        //        {
        //            ErrorType = 3;
        //        }
        //    }
        //    var resultObject = new
        //    {
        //        IsReset,
        //        ErrorType

        //    };
        //    return ser.Serialize(resultObject);
        //}


        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public int SaveLocalConfiguration(int ConfigId, int UserId, Object[] ConfigData)
        {
            ILocalConfigurationManager myManager = (ILocalConfigurationManager)IoC.Instance.Resolve(typeof(ILocalConfigurationManager));
            IRegisterManager regManager = (IRegisterManager)IoC.Instance.Resolve(typeof(IRegisterManager));
            IPosSystemConfigrationManager myConfigMgr = (IPosSystemConfigrationManager)IoC.Instance.Resolve(typeof(IPosSystemConfigrationManager));
            List<PosSystemConfigrationLite> myPOsConfigList = myConfigMgr.FindAllLite(null);
            E.LocalConfiguration myConfig = new E.LocalConfiguration();
            RegisterCriteria regCriteria = new RegisterCriteria();
            myConfig = getConfigItem(ConfigData);
            myConfig.IpAddress = myPOsConfigList.First().PosServerIp;


            string fullUrl=WebConfigurationManager.AppSettings["Root"]+"Web.config";
            GrantAccess(fullUrl);
            System.Configuration.Configuration  objConfig =WebConfigurationManager.OpenWebConfiguration("~");
            AppSettingsSection objAppsettings =(AppSettingsSection)objConfig.GetSection("appSettings");

            objAppsettings.Settings["RegisterId"].Value = myConfig.RegisterId.ToString();
            objConfig.Save();

            
            //myConfig.PosServerPass = myPOsConfigList.First().PosServerDataBasePassword;
            //myConfig.PosServerUserName = myPOsConfigList.First().PosServerDataBaseUsername;
            //myConfig.POSServerDataBaseName = myPOsConfigList.First().PosServerDataBaseName;
            int ErrorType = 0;
            if (ConfigId <= 0)
            {
                myConfig.CreatedBy = UserId;

            }
            else
            {
                myConfig = myManager.FindById(ConfigId, null);
                myConfig.UpdatedBy = UserId;
                myConfig.MarkModified();
            }

            myConfig.IpAddress = GetIPAddress();
            myConfig.MacAddress = GetMACAddress();
            regCriteria.RegisterId = myConfig.RegisterId;

            try
            {

                myManager.Save(myConfig);
                //if (!regManager.CheckUsedRegisters(regCriteria))
                //    myManager.Save(myConfig);
                //else

                //    ErrorType = 4;

            }
            catch (Exception ex)
            {
                if (!System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
                {
                    ErrorType = 1;//Connection Error
                }
                if (ex.GetType() == typeof(System.Data.SqlClient.SqlException))
                {
                    ErrorType = 2;
                }
                if (ex.GetType() == typeof(SF.Framework.Exceptions.RecordNotAffected))
                {
                    ErrorType = 3;
                }
            }


            return ErrorType;
        }

        private E.LocalConfiguration getConfigItem(object[] items)
        {
            E.LocalConfiguration configItem = null;
            foreach (Dictionary<string, Object> obj in items)
            {
                configItem = new E.LocalConfiguration();
                foreach (KeyValuePair<string, Object> val in obj)
                {
                    PropertyInfo pro = configItem.GetType().GetProperty(val.Key);
                    if (pro == null)
                        continue;

                    object value = null;
                    if (val.Value != null && !string.IsNullOrEmpty(val.Value.ToString()))
                        value = val.Value;

                    pro.SetValue(configItem, GetPropertyValue(pro.PropertyType, value), null);

                }
                break;
            }
            return configItem;
        }

        private object GetPropertyValue(Type propertyType, object value)
        {
            if (propertyType == typeof(double))
            {
                value = Convert.ToDouble(value);
            }
            else if (propertyType == typeof(int))
            {
                value = Convert.ToInt32(value);
            }
            else if (propertyType == typeof(string))
            {
                value = Convert.ToString(value);
            }
            else if (propertyType == typeof(bool))
            {
                value = Convert.ToBoolean(value);
            }
            else if (propertyType == typeof(decimal))
            {
                value = Convert.ToDecimal(value);
            }
            else if (propertyType == typeof(DateTime))
            {
                value = Convert.ToDateTime(value);
            }

            return value;
        }

        public string GetIPAddress()
        {
            IPHostEntry host;
            string localIP = "";
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    localIP = ip.ToString();
                }
            }
            return localIP;
        }

        public string GetMACAddress()
        {
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
            string sMacAddress = string.Empty;
            foreach (NetworkInterface adapter in nics)
            {
                if (sMacAddress == string.Empty)// only return MAC Address from first card
                {
                    IPInterfaceProperties properties = adapter.GetIPProperties();
                    sMacAddress = adapter.GetPhysicalAddress().ToString();
                }
            } return sMacAddress;
        }

        private bool GrantAccess(string fullPath)
        {
            DirectoryInfo dInfo = new DirectoryInfo(fullPath);
            DirectorySecurity dSecurity = dInfo.GetAccessControl();
            dSecurity.AddAccessRule(new FileSystemAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null), FileSystemRights.FullControl, InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit, PropagationFlags.NoPropagateInherit, AccessControlType.Allow));
            dInfo.SetAccessControl(dSecurity);
            return true;
        }


    }
}

