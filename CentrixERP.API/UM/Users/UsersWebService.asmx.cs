using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using CentrixERP.Common.API;
using Centrix.UM.Business.IManager;
using Centrix.UM.Business.Entity;
using C = SagePOS.Server.Configuration;
using SF.Framework;
using System.Web.Script.Services;
using SF.CustomScriptControls;
using System.Web.Script.Serialization;
using System.Configuration;
using SagePOS.Server.Configuration;
using System.Text.RegularExpressions;
using SagePOS.Manger.Common.Business.Entity;
using SagePOS.Server.API.Common.BaseClasses;

namespace Centrix.UM.API
{
    /// <summary>
    /// Summary description for UsersWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    [EntityAttribute(Enums_S3.Entity.User)]
    public class UsersWebService : WebServiceBaseClass
    {
        IUserManager userManager = null;

        public UsersWebService()
        {
            userManager = (IUserManager)IoC.Instance.Resolve(typeof(IUserManager));
        }

        
        [WebMethod]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string Delete(int id)
        {

            User myUser = userManager.FindById(id, null);
            if (!myUser.IsSupperUser)
            {
                this.LockEntityForDelete(Enums_S3.Entity.User, id);
                myUser.UserId = id;
                myUser.MarkDeleted();
                try
                {
                    userManager.Save(myUser);
                   this.UnLockEntity(Enums_S3.Entity.User, id);
                   return AttachStatusCode(true, 1, null);
                }
                catch (SF.Framework.Exceptions.RecordNotAffected ex)
                {
                   // this.UnLockEntity(Enums_S3.Entity.User, id);
                    return AttachStatusCode(false, 0, null);
                }
                catch (Exception ex)
                {
                    return AttachStatusCode(null, 0, HandleException(ex));
                }
            }
            else
                return AttachStatusCode(false, 0, null);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string SearchUserTeam(string keyword, int from, int to)
        {
            List<AutoCompleteItem> returnedList = new List<AutoCompleteItem>();

            UserCriteria userCriteria = new UserCriteria() { Keyword = keyword };
            List<User> userList = userManager.FindAll(userCriteria);

            ITeamManager teamManager = (ITeamManager)IoC.Instance.Resolve(typeof(ITeamManager));
            TeamCriteria teamCriteria = new TeamCriteria() { keyword = keyword };
            List<Team> teamList = teamManager.FindAll(teamCriteria);

            if (userList != null)
            {
                returnedList.AddRange((from User user in userList
                                       orderby user.TeamId
                                       select new AutoCompleteItem()
                                       {
                                           label = user.Fullname,
                                           value = user.UserId,
                                           categoryId = 1,
                                           category = "User"
                                       }).ToList());

            }
            if (teamList != null)
            {
                returnedList.AddRange((from Team team in teamList
                                       orderby team.Name
                                       select new AutoCompleteItem()
                                       {
                                           label = team.Name,
                                           value = team.TeamId,
                                           categoryId = 2,
                                           category = "Team"
                                       }).ToList());
            }
            return this.AttachStatusCode(returnedList, 0, null);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string SearchUsers(string keyword, int from, int to, string args)
        {
            UserCriteria Usercriteria = new UserCriteria()
            {
                Name = keyword,
                Keyword = keyword
            };
            List<User> UserList = userManager.FindAll(Usercriteria);

            if (UserList != null)
            {
                var temp = from User U in UserList
                           select new AutoCompleteItem()
                                  {
                                      label = U.FullNameEn,
                                      value = U.UserId,
                                      categoryId = 2,
                                      category = ""
                                  };
                return this.AttachStatusCode(temp, 1, null);
            }

            return this.AttachStatusCode(null, 1, null);
        }

        [WebMethod]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public bool VerifyOldPassword(int UserId, string OldPassword)
        {
            User user = userManager.FindById(UserId, null);
            if (OldPassword == user.Password)
                return true;
            else
                return false;
        }

        [WebMethod]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public bool VerifyNewPassword(string NewPassword, string ConfirmNewPassword)
        {
            if (NewPassword == ConfirmNewPassword)
                return true;
            else
                return false;
        }

        //[WebMethod]
        //[EntityMethodAttribute(Enums_S3.DefaultPermissions.Edit)]
        //public bool ChangePassword(int UserId, string OldPassword, string NewPassword, string ConfirmNewPassword)
        //{
        //    if (VerifyOldPassword(UserId, OldPassword))
        //    {
        //        if (VerifyNewPassword(NewPassword, ConfirmNewPassword))
        //        {
        //            User user = userManager.FindById(UserId, null);
        //            user.Password = NewPassword.StripHTML();
        //            try
        //            {
        //                user.MarkModified();
        //                user.UpdatedBy = UserId;
        //                userManager.Save(user);
        //                return true;
        //            }
        //            catch (Exception ex) { return false; }

        //        }

        //        else
        //        {
        //            return false; //two passwords doesnt match
        //        }

        //    }
        //    else
        //    {
        //        return false;//wrong password
        //    }

        //}

        [WebMethod]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public bool SaveCurrentUserSettings(int UserId, bool RecieveNotification)
        {
            User user = userManager.FindById(UserId, null);
            user.ReceiveNoyifications = RecieveNotification;
            try
            {
                user.MarkModified();
                user.UpdatedBy = UserId;
                userManager.Save(user);
                return true;
            }
            catch (Exception ex) { return false; }

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindUserAllLite(string keyword, int page, int resultCount, string argsCriteria)
        {
            UserCriteria userCriteria = null;
            if (!SF.Framework.String.IsEmpty(argsCriteria))
            {
                userCriteria = this.javaScriptSerializer.Deserialize<UserCriteria>(argsCriteria);
            }
            else
                userCriteria = new UserCriteria();


            userCriteria.Keyword = keyword;
            userCriteria.pageNumber = page;
            userCriteria.resultCount = resultCount;

            List<UserLite> usersList = userManager.FindAllLite(userCriteria);

            if (usersList != null)
            {
                var UserList = from UserLite Lite in usersList
                               select new
                               {
                                   Id = Lite.Id,
                                   UserId = Lite.UserId,
                                   UserName = Lite.UserName,
                                   Password = Lite.Password,
                                   FirstnameEnglish = Lite.FirstnameEnglish,
                                   MiddleNameEnglish = Lite.MiddleNameEnglish,
                                   LastNameEnglish = Lite.LastNameEnglish,
                                   FirstNameArabic = Lite.FirstNameArabic,
                                   MiddleNameArabic = Lite.MiddleNameArabic,
                                   LastNameArabic = Lite.LastNameArabic,
                                   Dob = Lite.Dob,
                                   DobValue = Lite.DobValue,
                                   Title = Lite.Title,
                                   Notes = Lite.Notes,
                                   CreatedBy = Lite.CreatedById,
                                   UpdatedBy = Lite.UpdatedById,
                                   UpdatedDate = Lite.UpdatedDate,
                                   CreatedDate = Lite.CreatedDate,
                                   UpdatedByName = Lite.UpdatedByName,
                                   CreatedByName = Lite.CreatedByName,
                                   RoleNameId = Lite.RoleNameId,
                                   TeamNameId = Lite.TeamNameId,
                                   ReceiveNoyificationsValue = Lite.ReceiveNoyificationsValue,
                                   ReceiveNoyifications = Lite.ReceiveNoyifications,
                                   FullName = Lite.FullName,
                                   RoleName = Lite.RoleName,
                                   TeamName = Lite.TeamName,
                                   UserEmail = Lite.UserEmail,
                                   LastRows = Lite.LastRows,
                                   TotalRecords = Lite.TotalRecords,
                                   label = Lite.FirstnameEnglish + " " + Lite.LastNameEnglish,
                                   ManagerId = Lite.ManagerId,
                                   Manager = Lite.Manager,
                                   IsDefault = Lite.IsDefault,
                                   IsDefaultValue = Lite.IsDefaultValue,
                                   IsManager = Lite.IsManager,
                                   IsManagerValue = Lite.IsManagerValue,
                                   value = Lite.UserId,
                                   categoryId = 2,
                                   category = "",
                                   IsSupperUser = Lite.IsSupperUser
                               };
                return this.AttachStatusCode(UserList, 0, null);
            }
            return this.AttachStatusCode(null, 0, null);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindUserByIdLite(int id)
        {
            UserLite user = userManager.FindByIdLite(id, null);
            return this.AttachStatusCode(user, 0, null);
        }

        [WebMethod]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string AddEditUser(int userId, string userName, string password, string firstName,
        string middleName, string lastName, string title, string dob, string note, int roleId, int teamId, string email, int UserId, int ManagerId, bool IsManager, bool IsDefault, bool IsPosUser,string accessCode)
        {

            Centrix.UM.Business.Entity.User myUser = null;
            if (userId > 0)
            {
                myUser = userManager.FindById(userId, null);
                this.LockEntity(Enums_S3.Entity.User, userId);
                myUser.MarkModified();
                myUser.UpdatedBy = UserId;
            }
            else
            {
                if (userManager.CheckUserCount())
                {
                    myUser = new User();
                    myUser.CreatedBy = UserId;
                }
                else
                    return this.AttachStatusCode(0, 406, "UsersCountExceedsAllowed");

            }

            myUser.UserName = userName;
            myUser.Password = password;
            myUser.FirstnameEnglish = firstName;
            myUser.MiddleNameEnglish = middleName;
            myUser.LastNameEnglish = lastName;
            myUser.Title = title;
            myUser.Notes = note;
            myUser.TeamId = teamId;
            myUser.RoleId = roleId;
            myUser.ManagerId = ManagerId;
            myUser.IsManager = IsManager;
            myUser.IsDefault = IsDefault;
            myUser.IsPosUser = IsPosUser;
            myUser.AccessCode = accessCode;

            if (!string.IsNullOrEmpty(dob))
                myUser.Dob = DateTime.ParseExact(dob, C.Configuration.GetConfigKeyValue("DotNetDateTimeFormat"), null);
            else
                myUser.Dob = null;

            try
            {
                Dictionary<string, string> validationUser = userManager.ValidateUser(myUser);
                if (validationUser != null && validationUser.Count() > 0)
                {
                    return this.AttachStatusCode(validationUser, 403, "Invalid Values");
                }
                else
                {
                    userManager.Save(myUser);
                  
                    if (userId < 0) //add mood
                    {
                        //if (!string.IsNullOrEmpty(email))
                        //{
                        //    myUser.MyEmailList = new List<EmailEntity>();
                        //    myUser.MyEmailList.Add(new EmailEntity()
                        //    {
                        //        EntityId = (int)Enums_S3.Entity.User,
                        //        EntityValueId = myUser.UserId,
                        //        EmailType = (int)Configuration.Enums_S3.Email.EmailType.Business,
                        //        EmailObj = new Email
                        //        {
                        //            EmailAddress = email,
                        //            Type = Configuration.Enums_S3.Email.EmailType.Business,
                        //            CreatedBy = UserId
                        //        },
                        //        IsDefault = true,
                        //    });
                        //}

                    }
                  
                    userManager.Save(myUser);
                    this.UnLockEntity(Enums_S3.Entity.User, userId);

                }

            }
            catch (SF.Framework.Exceptions.RecordExsitsException recordExists)
            {
                return this.AttachStatusCode(0, 501, null);
            }
            catch (Exception ex)
            {
                return this.AttachStatusCode(null, 0, ex.Message + Environment.NewLine + ex.StackTrace);
            }
            return FindUserByIdLite(myUser.UserId);
        }
      

        
      

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindUserManager(int id)
        {
            UserLite user = userManager.FindUserManager(id, null);
            if (user !=null && id != user.Id)
                return this.AttachStatusCode(user, 1, null);
            else
                return this.AttachStatusCode(null, 0, null);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string GetLoggedInUser()
        {
            UserLite user = userManager.FindByIdLite(this.UserId, null);
            return this.AttachStatusCode(user, 1, null);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string ChangePassword(string password, string confirmPassword)
        {
            if (SF.Framework.String.IsEmpty(password))
                return this.AttachStatusCode(null, 2, "password is required");
            else if (SF.Framework.String.IsEmpty(confirmPassword))
                return this.AttachStatusCode(null, 3, "confirmed password is required");
            else if (confirmPassword.Trim() != password.Trim())
                return this.AttachStatusCode(null, 4, "invalid Password");


            User user = this.userManager.FindById(LoggedInUser.UserId, null);
            user.MarkModified();
            user.Password = password;

            try
            {
                this.userManager.Save(user);
                return this.AttachStatusCode(null, 1, null);
            }
            catch (Exception)
            {
                return this.AttachStatusCode(null, 0, null);
            }
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindRoleUsersLite(int roleId)
        {
            List<RoleUsersLite> user = userManager.FindRoleUsersLite(roleId);

            return this.AttachStatusCode(user, 0, null);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string SaveUsersRole(int roleId, string RoleUserIds)
        {
            try
            {
                userManager.AddUserRoles(roleId, RoleUserIds);
                return this.AttachStatusCode(null, 0, null);
            }
            catch (Exception)
            {

                return this.AttachStatusCode(null, 0, null);
            }
            

           
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string CheckUserPermissions(string UserName, string Password, string AccessCode, int PermissionId)
        {
            try
            {
                User userInfo = userManager.ClientLogin(UserName, Password, AccessCode);
                bool userPermission = true;
                if (userInfo != null)
                {
                    userPermission = userManager.HasPermission(PermissionId, userInfo);
                }
                else
                {
                    userPermission = false;
                }
                return this.AttachStatusCode(userPermission, 0, null);


            }
            catch (Exception)
            {

                return this.AttachStatusCode(null, 0, null);
            }



        }


    }
}