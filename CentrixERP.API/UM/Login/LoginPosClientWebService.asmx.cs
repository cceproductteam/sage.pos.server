﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using SagePOS.Server.API.Common.BaseClasses;
using Centrix.UM.Business.IManager;
using SagePOS.Server.Business.IManager;
using SagePOS.Server.Business.Entity;
using E = Centrix.UM.Business.Entity;
using System.Net;

using System.Web.UI;


using SF.Framework;
using SagePOS.Server.Configuration;
using System.Configuration;
using CentrixERP.Common.Business.IManager;


using SagePOS.Common.Web;








namespace SagePOS.Server.API.UM
{
    /// <summary>
    /// Summary description for LoginPosClientWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class LoginPosClientWebService : System.Web.Services.WebService
    {



        IUserManager myUserManager = null;
        IUserLogginManager userLogginManager = null;
        IClientLoginManager clientLoginManager = null;
        IInvoiceManager invoiceManager = null;
        public int CurrentYear = DateTime.Now.Year;
        PosSystemConfigration posSystem = new PosSystemConfigration();
        

        public int CurrentLoginMethod; public int UPLoginMethod; public int AccessCodeLoginMethod; public int BothLoginMethod;


        //System.Web.HttpRequest Request;
        //System.Web.HttpResponse Response;
        //System.Web.HttpResponse Response;


         public LoginPosClientWebService() {
        
             
         userLogginManager = IoC.Instance.Resolve<IUserLogginManager>();
         myUserManager = (IUserManager)IoC.Instance.Resolve(typeof(IUserManager));
         // clientLoginManager = (IClientLoginManager)IoC.Instance.Resolve(typeof(IClientLoginManager));
         SF.Framework.Cookies.Abandon("cxuser");
         UPLoginMethod = System.Configuration.ConfigurationManager.AppSettings["UNPLoginMethod"].ToNumber();
         AccessCodeLoginMethod = System.Configuration.ConfigurationManager.AppSettings["ACodeLoginMethod"].ToNumber();
         BothLoginMethod = System.Configuration.ConfigurationManager.AppSettings["BothloginMethod"].ToNumber();
         IPosSystemConfigrationManager configManager = (IPosSystemConfigrationManager)IoC.Instance.Resolve(typeof(IPosSystemConfigrationManager));
         List<PosSystemConfigration> posConfigList = configManager.FindAll(null);
         if (posConfigList != null && posConfigList.Count > 0)
         {

             CurrentLoginMethod = posConfigList.First().LoginMethodId;


         }

        }

        [WebMethod]
        public string Login(string ACode , string UserName , string Password ) {
            

           
            string accessCode = (!string.IsNullOrEmpty(ACode.TrimEnd())) ? ACode.TrimEnd() : null;
            E.User LoggedInUser = null;
            if (CurrentLoginMethod == UPLoginMethod)
                LoggedInUser = myUserManager.ClientLogin(UserName, Password, null);
            else
                LoggedInUser = myUserManager.ClientLogin(UserName, Password.TrimEnd(), accessCode);

            ///bool shift = invoiceManager.checkUserShift(LoggedInUser.UserId);
            if (LoggedInUser != null && (LoggedInUser.IsPosUser || LoggedInUser.IsSupperUser || LoggedInUser.IsAdmin))
            {


                /// if (shift)
                //{
                E.UserLogginCriteria criteria = new E.UserLogginCriteria()
                {
                    UserId = LoggedInUser.UserId
                };

                createNewUserSession(LoggedInUser.UserId);
                // }
                // else
                // {

                // }

                return (LoggedInUser.UserId.ToString());

              

            }
            else
            {
                if (LoggedInUser != null)
                {
                    if (!LoggedInUser.IsPosUser && !LoggedInUser.IsSupperUser)
                        return("-1");
                }

                //lblErrorLogin.Visible = true;

                return ("-1");

            }
        
        
        }

        public void createNewUserSession(int userId)
        {
         
          E.UserLogginCriteria criteria = new E.UserLogginCriteria()
            {
           
                UserId = userId,
                PublicIP = HttpContext.Current.Request.UserAgent,
                LocalIP = HttpContext.Current.Request.UserHostName,
                UserAgent = HttpContext.Current.Request.UserAgent,
            };
            
            

            

            userLogginManager.KillUserSession(userId);
            userLogginManager.CreateUserSession(userId, criteria, true);
            //Response.Redirect(SagePOS.Server.Configuration.Configuration.GetConfigKeyURL("defaultPage"));



        }

        public int GetLoggInMethodValue()
        {
            IPosSystemConfigrationManager configManager = (IPosSystemConfigrationManager)IoC.Instance.Resolve(typeof(IPosSystemConfigrationManager));
            List<PosSystemConfigrationLite> configLite = configManager.FindAllLite(null);
            return configLite.First().LoginMethodId;

        }

       
    }
}
