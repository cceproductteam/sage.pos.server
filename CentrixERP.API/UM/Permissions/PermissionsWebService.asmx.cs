using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using CentrixERP.Common.API;
using System.Web.Script.Services;
using Centrix.UM.Business.IManager;
using Centrix.UM.Business.Entity;
using System.Web.Script.Serialization;
using SF.CustomScriptControls;
using SF.Framework;
using SagePOS.Server.Configuration;
using SagePOS.Server.API.Common.BaseClasses;

namespace Centrix.UM.API
{
    /// <summary>
    /// Summary description for PermissionsWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
     [EntityAttribute(Enums_S3.Entity.None)]
    public class PermissionsWebService : WebServiceBaseClass
    {
        public List<RolePermissionLite> RolePermissionsList = null;
        IRolePermissionManager iRolePermissionManager = null;
        public JavaScriptSerializer javaScriptSerializer;

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string Search(string keyword, int from, int to, string args)
        {
            IPermissionManager myPermissionManager = (IPermissionManager)IoC.Instance.Resolve(typeof(IPermissionManager));
            PermissionCriteria criteriaObj = new PermissionCriteria();
            criteriaObj.KeyWord = keyword;
            List<Permission> PermissionsList = myPermissionManager.FindAll(criteriaObj);
            if (PermissionsList != null)
            {
                var Permissions = from Permission P in PermissionsList orderby P.NameEnglish select new AutoCompleteItem() { label = P.NameEnglish, value = P.PermissionId, categoryId = -1, category = "" };
                //JavaScriptSerializer oSerializer = new JavaScriptSerializer();
                //string sJSON = oSerializer.Serialize(x);
                //return sJSON;
                return this.AttachStatusCode(Permissions, 0, null);
            }
            else
            {
                return this.AttachStatusCode(null, 0, null);
            }

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindEntityPermissions(int id)
        {
            iRolePermissionManager = (IRolePermissionManager)IoC.Instance.Resolve(typeof(IRolePermissionManager));
            javaScriptSerializer = new JavaScriptSerializer();

            RolePermissionCriteria criteria = new RolePermissionCriteria() { EntityId = id };
            RolePermissionsList = iRolePermissionManager.FindAllRolePermission(LoggedInUser.RoleId, criteria);
            if (RolePermissionsList != null)
                return AttachStatusCode(RolePermissionsList, 1, null);
            else
                return AttachStatusCode(null, 0, null);

        }



    }
}
