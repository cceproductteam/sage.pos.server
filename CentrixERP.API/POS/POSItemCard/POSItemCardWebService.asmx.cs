using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using SF.Framework;
using E = SagePOS.Server.Business.Entity;
using SagePOS.Server.Business.IManager;
using CentrixERP.Common.API;
using System.Reflection;
using SagePOS.Server.Configuration;
using SF.FrameworkEntity;
using System.Web.Script.Serialization;
using SagePOS.Server.API.Common.BaseClasses;
//using Centrix.Inventory.Business.IManager;



namespace SagePOS.Server.API
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    [EntityAttribute(Enums_S3.Entity.IcItemCard)]
    public class POSItemCardWebService : POSWebServiceBaseClass
    {
        //IItemCardManager iItemCardManager = null;
        IPOSItemCardManager iIcItemCardManager = null;

        public POSItemCardWebService()
        {
            //iItemCardManager = (IItemCardManager)IoC.Instance.Resolve(typeof(IItemCardManager));
            iIcItemCardManager = (IPOSItemCardManager)IoC.Instance.Resolve(typeof(IPOSItemCardManager));
        }



         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]

        public string FindAllLite(string keyword, int page, int resultCount, string argsCriteria)
        {
            E.IcItemCardCriteria criteria = null;
            if (!SF.Framework.String.IsEmpty(argsCriteria))
            {
                criteria = this.javaScriptSerializer.Deserialize<E.IcItemCardCriteria>(argsCriteria);
            }
            else
                criteria = new E.IcItemCardCriteria();

            criteria.Keyword = keyword;
            criteria.pageNumber = page;
            criteria.resultCount = resultCount;

            try
            {

                List<E.POSItemCardLite> ItemCardList = iIcItemCardManager.FindAllLite(criteria);


                return this.AttachStatusCode(ItemCardList, 1, null);
            }

            catch (Exception ex)
            {

                return ex.ToString();

            }
        }
         [WebMethod(EnableSession = true)]
         [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
  
         public string FindAll()
         {
             List<E.POSItemCard> ItemCardList = iIcItemCardManager.FindAll();
             return this.AttachStatusCode(ItemCardList, 1, null);
         }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]

        public string FindByIdLite(int id)
        {
            E.POSItemCardLite myItemCard = iIcItemCardManager.FindByIdLite(id, null);
            return this.AttachStatusCode(myItemCard, 1, null);

        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string SearchItemCriteria(string ItemCriteria)
        {
            JavaScriptSerializer ser = new JavaScriptSerializer();
            IPOSItemCardManager myItemManager = (IPOSItemCardManager)IoC.Instance.Resolve(typeof(IPOSItemCardManager));
            E.IcItemCardCriteria criteria = null;
            if (!SF.Framework.String.IsEmpty(ItemCriteria))
            {
                criteria = this.javaScriptSerializer.Deserialize<E.IcItemCardCriteria>(this.unEscape(ItemCriteria));
            }
            else
                criteria = new E.IcItemCardCriteria();
            // Criteria = getCriteriaItem(ItemCriteria);

            List<E.BOItemCardQl> ItemCardList = myItemManager.SearchItemQl(criteria);


            if (ItemCardList != null)
            {
                var Items = from E.BOItemCardQl myItemQl in ItemCardList
                            select new
                            {
                                ProductId = -1,
                                ProductDesc = myItemQl.ItemDesc,
                                OrginalPrice = myItemQl.ItemPrice,
                                BarCode = myItemQl.ItemBarCode,
                                TaxId = myItemQl.ItemTaxAuth,
                                TaxPercentage = myItemQl.ItemTaxRate,
                                UnitId = myItemQl.ItemUnit,
                                ItemNumber = myItemQl.ItemNumber,
                                ItemSerial = myItemQl.ItemSerial,
                                availableQuantity=myItemQl.AvailableQuantity

                            };
                string sJSON = this.javaScriptSerializer.Serialize(Items);
                return sJSON;
            }
            else
                return null;
        }


      

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string CheckSerial(string itemSerial, string itemNum, string customer, string currencyCode, string PriceListCode ,bool status)//status false = exchange  else all movment
        {
            IPOSItemCardManager ItemManager = (IPOSItemCardManager)IoC.Instance.Resolve(typeof(IPOSItemCardManager));
            E.IcItemCardCriteria CriteriaQl = new E.IcItemCardCriteria();

            CriteriaQl.ItemNumber = itemNum;
            CriteriaQl.SerialNumber = itemSerial;
            CriteriaQl.CustomerId = customer;
            CriteriaQl.CurrencyCode = currencyCode;
            CriteriaQl.PriceListCode = PriceListCode;
            E.BOItemCardQl myItemCardQl = ItemManager.SearchItemSerial(CriteriaQl,status);
            if (myItemCardQl != null)
            {
                var QlObjectmyItemQl = new
                {
                    ProductId = -1,
                    ItemName = myItemCardQl.ItemDesc,
                    OrginalPrice = myItemCardQl.ItemPrice,
                    BarCode = myItemCardQl.ItemBarCode,
                    TaxId = myItemCardQl.ItemTaxAuth,
                    TaxPercentage = myItemCardQl.ItemTaxRate,
                    UnitId = myItemCardQl.ItemUnit,
                    ItemNumber = myItemCardQl.ItemNumber,
                    IsSerial = myItemCardQl.ItemSerial,
                    SerialNumber = myItemCardQl.SerialNo

                };
                return this.javaScriptSerializer.Serialize(QlObjectmyItemQl);
            }
            else
            {
                return null;
            }
        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string BOItemCardSearch(string keyword, string customer, string currencyCode, string PriceListCode)
        {
            IPOSItemCardManager myManager = (IPOSItemCardManager)IoC.Instance.Resolve(typeof(IPOSItemCardManager));
            E.IcItemCardCriteria itemCardCriteria = new E.IcItemCardCriteria();
            itemCardCriteria.BarCode = keyword;
            itemCardCriteria.CustomerId = customer;
            itemCardCriteria.CurrencyCode = currencyCode;
            itemCardCriteria.PriceListCode = PriceListCode;
            E.BOItemCardQl myItemQl = myManager.FindItemQl(itemCardCriteria);
            if (myItemQl != null)
            {
                var QlObjectmyItemQl = new
                {
                    AvailableQuantity=myItemQl.AvailableQuantity,
                    ProductId = -1,
                    ItemName = myItemQl.ItemDesc,
                    OrginalPrice = myItemQl.ItemPrice,
                    BarCode = myItemQl.ItemBarCode,
                    TaxId = myItemQl.ItemTaxAuth,
                    TaxPercentage = myItemQl.ItemTaxRate,
                    UnitId = myItemQl.ItemUnit,
                    ItemNumber = myItemQl.ItemNumber,
                    IsSerial = myItemQl.ItemSerial,
                    SerialNumber = myItemQl.SerialNo

                };
                return this.javaScriptSerializer.Serialize(QlObjectmyItemQl);
            }
            else
            {
                return null;
            }

        }
    }
}
