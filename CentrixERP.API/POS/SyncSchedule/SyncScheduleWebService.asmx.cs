using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using SF.Framework;
using E = SagePOS.Server.Business.Entity;
using SagePOS.Server.Business.IManager;
using CentrixERP.Common.API;
using System.Reflection;
using SagePOS.Server.Configuration;
using SF.FrameworkEntity;
using SagePOS.Server.API.Common.BaseClasses;
using System.Globalization;



namespace Centrix.POS.Server.API
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    [EntityAttribute(Enums_S3.Entity.SyncSchedule)]
    public class SyncScheduleWebService : WebServiceBaseClass
    {
        ISyncScheduleManager iSyncScheduleManager = null;
        public SyncScheduleWebService()
        {
            iSyncScheduleManager = (ISyncScheduleManager)IoC.Instance.Resolve(typeof(ISyncScheduleManager));
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Delete(int id)
        {
            this.LockEntityForDelete(Enums_S3.Entity.SyncSchedule, id);
            E.SyncSchedule mySyncSchedule = iSyncScheduleManager.FindById(id, null);
            mySyncSchedule.SyncScheduleId = id;
            mySyncSchedule.MarkDeleted();
            try
            {
                iSyncScheduleManager.Save(mySyncSchedule);
                return AttachStatusCode(true, 1, null);
            }
            catch (Exception ex)
            {
                return AttachStatusCode(null, 0, HandleException(ex));
            }

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string FindAllLite(string keyword, int page, int resultCount, string argsCriteria)
        {
            E.SyncScheduleCriteria criteria = null;
            if (!SF.Framework.String.IsEmpty(argsCriteria))
            {
                criteria = this.javaScriptSerializer.Deserialize<E.SyncScheduleCriteria>(argsCriteria);
            }
            else
                criteria = new E.SyncScheduleCriteria();

            criteria.Keyword = keyword;
            criteria.pageNumber = page;
            criteria.resultCount = resultCount;


            List<E.SyncScheduleLite> SyncScheduleList = iSyncScheduleManager.FindAllLite(criteria);
            return this.AttachStatusCode(SyncScheduleList, 1, null);

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string FindByIdLite(int id)
        {
            E.SyncScheduleLite mySyncSchedule = iSyncScheduleManager.FindByIdLite(id, null);
            return this.AttachStatusCode(mySyncSchedule, 1, null);

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.Add)]
        public string Add(int id, string SyncScheduleInfo)
        {
            return Save(true, id, SyncScheduleInfo);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Edit(int id, string SyncScheduleInfo)
        {
            if (id <= 0)
            {
                ValidationError error = new ValidationError()
                {
                    ControlName = "",
                    GlobalMessage = true,
                    Message = "Please select SyncSchedule"
                };
                List<ValidationError> errors = new List<ValidationError>(){
                error
                };
                return this.AttachStatusCode(errors, 0, "validation");
            }
            else
            {
                return Save(false, id, SyncScheduleInfo);
            }
        }

        private string Save(bool add, int id, string SyncScheduleInfo)
        {
            add = true;
            E.SyncSchedule mySyncSchedule = this.javaScriptSerializer.Deserialize<E.SyncSchedule>(this.unEscape((SyncScheduleInfo)));
            if (!add)
            {
                this.LockEntity(Enums_S3.Entity.SyncSchedule, id);
                mySyncSchedule.MarkOld();
                mySyncSchedule.MarkModified();
                mySyncSchedule.SyncScheduleId = id;
                mySyncSchedule.UpdatedBy = LoggedInUser.UserId;
            }
            else
            {
                mySyncSchedule.CreatedBy = LoggedInUser.UserId;
            }


            List<ValidationError> errors = iSyncScheduleManager.Validate(mySyncSchedule);
            if (errors != null && errors.Count > 0)
            {
                return this.AttachStatusCode(errors, 0, "validation");
            }


            try
            {
                iSyncScheduleManager.Save(mySyncSchedule);
                if (!add)
                    this.UnLockEntity(Enums_S3.Entity.SyncSchedule, id);
                return this.FindByIdLite(mySyncSchedule.SyncScheduleId);

            }
            catch (SF.Framework.Exceptions.RecordExsitsException ex)
            {
                return this.AttachStatusCode(null, 501, null);
            }
            catch (Exception ex)
            {

                return this.AttachStatusCode(null, 0, HandleException(ex));
            }
        }



        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string FindAllSync()
        {

            List<E.SyncScheduleLite> SyncScheduleList = iSyncScheduleManager.FindAllLite(null);
            return this.AttachStatusCode(SyncScheduleList, 1, null);

        }



        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string AddNewSchedule(int Type, int UserId) {

            try
            {
                iSyncScheduleManager.addNewSchedule(Type, UserId);
                return AttachStatusCode("Success", 1, null);
            }
            catch (Exception ex)
            {

                throw ex;
            }
           
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string Checksyncstatus(int action)
        {
            try
            {
                string SyncNumber = "";
                int useridoo = 0;
                E.NewSyncSchedule result = iSyncScheduleManager.checkSynchronizationStatus();
                if (result != null)
                {
                    if (result.SyncStatus == 1  )
                    {
                        SyncNumber = iSyncScheduleManager.getLastSyncNumber(result.action);
                        useridoo = result.createdby;
                    }
                     if (SyncNumber != "")
                    {
                        if (result != null)
                        useridoo = result.createdby;
                        result = null;
                    }
                     if (SyncNumber == "" && result.SyncStatus == 1 )
                    {
                        if (result != null)
                         useridoo = result.createdby;
                        SyncNumber = "No Data";
                        result = null;

                    }
                    
                }

                //else   if (result.SyncStatus == 1 && result.isChecked == 1)
                //{
                //    result = null;
                //    SyncNumber = "";
                
                //}

                //else if (result.SyncStatus == 0)

                //{

                //    result = null;
                //    SyncNumber = "";
                //}

                DateTime D = DateTime.Now;
                string serverDateTime = D.ToString("ddd MMM dd yyyy HH:mm:ss", new CultureInfo("en-US"));
                

                object returnObjec = new
                {
                    result = result,
                    SyncNumber = SyncNumber,
                    serverDateTime = serverDateTime,
                    useridoo = useridoo
                };

        

                return AttachStatusCode(returnObjec, 1, null);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }
}

