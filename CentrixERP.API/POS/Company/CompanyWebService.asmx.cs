using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using SF.Framework;
using E = SagePOS.Server.Business.Entity;
using SagePOS.Server.Business.IManager;
using CentrixERP.Common.API;
using System.Reflection;
using SagePOS.Server.Configuration;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;
using SagePOS.Manger.Common.Business.Entity;
using CentrixERP.Common.Business.IManager;
using SagePOS.Server.Configuration;
using SagePOS.Server.API.Common.BaseClasses;

namespace SagePOS.Server.API
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    [EntityAttribute(Enums_S3.Entity.Company)]
    public class CompanyWebService : WebServiceBaseClass
    {
        ICompanyManager iCompanyManager = null;
        public IDataTypeContentManager iDataTypeContentManager = null;
        public CompanyWebService()
        {
            iCompanyManager = (ICompanyManager)IoC.Instance.Resolve(typeof(ICompanyManager));
            iDataTypeContentManager = (IDataTypeContentManager)IoC.Instance.Resolve(typeof(IDataTypeContentManager));
        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string Delete(int id)
        {
            this.LockEntityForDelete(Enums_S3.Entity.Company, id);
            E.Company myCompany = iCompanyManager.FindById(id, null);
            myCompany.CompanyId = id;
            myCompany.MarkDeleted();
            try
            {

                //List<EntityRelations> EntityRelationsList = EntityRelationsCheck.CheckEntityRelations((int)Configuration.Enums_S3.Entity.Company, id, null);
                //if (EntityRelationsList != null && EntityRelationsList.Count() > 0)
                //    return AttachStatusCode(EntityRelationsList, (int)Enums_S3.StatusCode.Codes.FailedToDelete, null);

                iCompanyManager.Save(myCompany);
                UnLockEntity(Enums_S3.Entity.Company, id);
                return AttachStatusCode(true, 1, null);
            }
            catch (Exception ex)
            {
                return AttachStatusCode(null, 0, HandleException(ex));
            }

        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindAllLite(string keyword, int page, int resultCount, string argsCriteria)
        {
            E.CompanyCriteria criteria = null;
            if (!SF.Framework.String.IsEmpty(argsCriteria))
            {
                criteria = this.javaScriptSerializer.Deserialize<E.CompanyCriteria>(argsCriteria);
            }
            else
                criteria = new E.CompanyCriteria();

            criteria.Keyword = keyword;
            criteria.pageNumber = page;
            criteria.resultCount = resultCount;


            List<E.CompanyLite> CompanyList = iCompanyManager.FindAllLite(criteria);
            return this.AttachStatusCode(CompanyList, 1, null);

        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindByIdLite(int id)
        {
            E.CompanyLite myCompany = iCompanyManager.FindByIdLite(id, null);
            myCompany.CompanytypesList = iDataTypeContentManager.FindAllByIds(myCompany.CompanyType);
            myCompany.IntresetedInList = iDataTypeContentManager.FindAllByIds(myCompany.InterestedInId);
            return this.AttachStatusCode(myCompany, 1, null);

        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string Add(int id, string CompanyInfo)
        {
            return Save(true, id, CompanyInfo);
        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.Edit)]
        public string Edit(int id, string CompanyInfo)
        {
            if (id <= 0)
            {
                ValidationError error = new ValidationError()
                {
                    ControlName = "",
                    GlobalMessage = true,
                    Message = "Please select company"
                };
                List<ValidationError> errors = new List<ValidationError>(){
                error
                };
                return this.AttachStatusCode(errors, 0, "validation");
            }
            else
            {
                return Save(false, id, CompanyInfo);
            }
        }
        
        private string Save(bool add, int id, string CompanyInfo)
        {
            E.Company myCompany = this.javaScriptSerializer.Deserialize<E.Company>(this.unEscape((CompanyInfo)));
            if (!add)
            {
                this.LockEntity(Enums_S3.Entity.Company, id);
                myCompany.MarkOld();
                myCompany.MarkModified();
                myCompany.CompanyId = id;
                myCompany.UpdatedBy = LoggedInUser.UserId;
            }
            else
            {
                myCompany.CreatedBy = LoggedInUser.UserId;
            }

            List<ValidationError> errors = iCompanyManager.Validate(myCompany);
            if (errors != null && errors.Count > 0)
            {
                return this.AttachStatusCode(errors, 0, "validation");
            }


            try
            {
                iCompanyManager.Save(myCompany);
                if (!add)
                    this.UnLockEntity(Enums_S3.Entity.Company, id);
               // return this.AttachStatusCode(myCompany.CompanyId, 1, null);
                E.CompanyLite liteObj = this.iCompanyManager.FindByIdLite(myCompany.CompanyId, null);
                return this.AttachStatusCode(liteObj, 1, null);

            }
            catch (SF.Framework.Exceptions.RecordExsitsException ex)
            {
                return this.AttachStatusCode(null, 501, null);
            }
            catch (Exception ex)
            {

                return this.AttachStatusCode(null, 0, HandleException(ex));
            }
        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string saveCompanyContactInfo(int CompanyId, string AddressObj, string HomePhoneObj, string FaxPhone, string MobilePhone, string BusinessEmail, string PersonalEmail)
        {

            Company CompanyEntity = iCompanyManager.FindById(CompanyId, null);
            Address myAddress = this.javaScriptSerializer.Deserialize<Address>(this.unEscape((AddressObj)));
            
            Phone myHomePhone = this.javaScriptSerializer.Deserialize<Phone>(this.unEscape((HomePhoneObj)));
            Phone myFaxPhone = this.javaScriptSerializer.Deserialize<Phone>(this.unEscape((FaxPhone)));
            Phone myMobilePhone = this.javaScriptSerializer.Deserialize<Phone>(this.unEscape((MobilePhone)));

            Email myBusinessEmail = this.javaScriptSerializer.Deserialize<Email>(this.unEscape((BusinessEmail)));
            Email MyPersonalEmail = this.javaScriptSerializer.Deserialize<Email>(this.unEscape((PersonalEmail)));

            if (CompanyEntity != null)
            {
                //CompanyEntity.AddressCompanyList = new List<AddressEntity>();
                //CompanyEntity.PhoneCompanyList = new List<PhoneEntity>();
                //CompanyEntity.EmailCompanyList = new List<EmailEntity>();




                //if (myBusinessEmail != null && !string.IsNullOrEmpty(myBusinessEmail.EmailAddress))
                //{
                //    CompanyEntity.EmailCompanyList.Add(new EmailEntity()
                //    {
                //        EntityId=(int)Enums_S3.Entity.Company,
                //        EntityValueId = CompanyId,
                //        EmailType = (int)Configuration.Enums_S3.Email.EmailType.Business,
                //        IsDefault = true,
                //        CreatedBy = LoggedInUser.UserId,
                //        EmailObj = new Email
                //        {
                //            EmailAddress = myBusinessEmail.EmailAddress,
                //            Type = Configuration.Enums_S3.Email.EmailType.Business,
                //            CreatedBy = LoggedInUser.UserId
                //        },
                //    });
                //}

                //if (MyPersonalEmail != null && !string.IsNullOrEmpty(MyPersonalEmail.EmailAddress))
                //{
                //    CompanyEntity.EmailCompanyList.Add(new EmailEntity()
                //    {
                //        EntityId = (int)Enums_S3.Entity.Company,
                //        EntityValueId = CompanyId,
                //        EmailType = (int)Configuration.Enums_S3.Email.EmailType.Personal,
                //        CreatedBy = LoggedInUser.UserId,
                //        EmailObj = new Email
                //        {
                //            EmailAddress = MyPersonalEmail.EmailAddress,
                //            Type = Configuration.Enums_S3.Email.EmailType.Personal,
                //            CreatedBy = LoggedInUser.UserId
                //        },
                //        IsDefault = true,

                //    });
                //}

                //if (myHomePhone != null && !string.IsNullOrEmpty(myHomePhone.PhoneNumber))
                //{
                //    CompanyEntity.PhoneCompanyList.Add(new PhoneEntity()
                //    {
                //        EntityId = (int)Enums_S3.Entity.Company,
                //        EntityValueId = CompanyId,
                //        PhoneType = (int)Configuration.Enums_S3.Phone.PhoneType.Home,
                //        CreatedBy = LoggedInUser.UserId,
                //        PhoneObj = new Phone
                //        {
                //            CountryCode = myHomePhone.CountryCode,
                //            AreaCode = myHomePhone.AreaCode,
                //            PhoneNumber = myHomePhone.PhoneNumber,
                //            Type = Configuration.Enums_S3.Phone.PhoneType.Home,
                //            CreatedBy = LoggedInUser.UserId
                //        },
                //        IsDefault = true
                //    });
                //}

                //if (myFaxPhone != null && !string.IsNullOrEmpty(myFaxPhone.PhoneNumber))
                //{

                //    CompanyEntity.PhoneCompanyList.Add(new PhoneEntity()
                //    {
                //        EntityId = (int)Enums_S3.Entity.Company,
                //        EntityValueId = CompanyId,
                //        PhoneType = (int)Configuration.Enums_S3.Phone.PhoneType.Fax,
                //        CreatedBy = LoggedInUser.UserId,
                //        PhoneObj = new Phone
                //        {
                //            CountryCode = myFaxPhone.CountryCode,
                //            AreaCode = myFaxPhone.AreaCode,
                //            PhoneNumber = myFaxPhone.PhoneNumber,
                //            Type = Configuration.Enums_S3.Phone.PhoneType.Fax,
                //            CreatedBy = LoggedInUser.UserId
                //        },
                //        IsDefault = true
                //    });
                //}


                //if (myMobilePhone != null && !string.IsNullOrEmpty(myMobilePhone.PhoneNumber))
                //{
                //    CompanyEntity.PhoneCompanyList.Add(new PhoneEntity()
                //    {
                //        EntityId = (int)Enums_S3.Entity.Company,
                //        EntityValueId = CompanyId,
                //        PhoneType = (int)Configuration.Enums_S3.Phone.PhoneType.Mobile,
                //        CreatedBy = LoggedInUser.UserId,
                //        PhoneObj = new Phone
                //        {
                //            CountryCode = myMobilePhone.CountryCode,
                //            AreaCode = myMobilePhone.AreaCode,
                //            PhoneNumber = myMobilePhone.PhoneNumber,
                //            Type = Configuration.Enums_S3.Phone.PhoneType.Mobile,
                //            CreatedBy = LoggedInUser.UserId
                //        },
                //        IsDefault = true
                //    });
                //}




                if (myAddress != null)
                {
                    if (!string.IsNullOrEmpty(myAddress.CityText) || !string.IsNullOrEmpty(myAddress.StreetAddress) || myAddress.CountryId > 0)
                    {
                        //CompanyEntity.AddressCompanyList.Add(new AddressEntity()
                        //{
                        //    EntityId = (int)Enums_S3.Entity.Company,
                        //    EntityValueId = CompanyId,

                        //     AddressObj = new Address
                        //    {
                        //        CreatedBy = LoggedInUser.UserId,
                        //        CountryId = myAddress.CountryId,
                        //        CityText = unEscape(myAddress.CityText),
                        //        StreetAddress = unEscape(myAddress.StreetAddress),
                        //        ZipCode = unEscape(myAddress.ZipCode),
                        //        Region = unEscape(myAddress.Region),
                        //        //Area = unEscape(myAddress.Area),
                        //        Place = unEscape(myAddress.Place),
                        //        NearBy = unEscape(myAddress.NearBy),
                        //        POBox = unEscape(myAddress.POBox),
                        //        BuildingNumber = myAddress.BuildingNumber,
                        //        Floor = unEscape(myAddress.Floor),
                        //        OfficeNumber = unEscape(myAddress.OfficeNumber),
                        //        County = unEscape(myAddress.County),
                        //        Type = unEscape(myAddress.Type)

                        //    },
                        //    IsDefault = true,
                        //    CreatedBy = LoggedInUser.UserId
                        //});
                    }
                }

             


            }

            try
            {
                iCompanyManager.Save(CompanyEntity);
                return this.AttachStatusCode(CompanyEntity.CompanyId, 1, null);
            }
            catch (Exception)
            {

                return this.AttachStatusCode(null, 0, null);
            }


        }


        

    }
}

