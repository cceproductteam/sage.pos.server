using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using SF.Framework;
using E = SagePOS.Server.Business.Entity;
using SagePOS.Server.Business.IManager;
using CentrixERP.Common.API;
using System.Reflection;
using SagePOS.Server.Configuration;
using SF.FrameworkEntity;
using SagePOS.Server.API.Common.BaseClasses;



namespace Centrix.POS.Server.API
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    [EntityAttribute(Enums_S3.Entity.SystemConfigration)]
    public class PosSystemConfigrationWebService : WebServiceBaseClass
    {
        IPosSystemConfigrationManager iPosSystemConfigrationManager = null;
        public PosSystemConfigrationWebService()
        {
            iPosSystemConfigrationManager = (IPosSystemConfigrationManager)IoC.Instance.Resolve(typeof(IPosSystemConfigrationManager));
        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindAllLite(string keyword, int page, int resultCount)
        {
            E.PosSystemConfigrationCriteria criteria = new E.PosSystemConfigrationCriteria();
            criteria.Keyword = keyword;
            criteria.pageNumber = page;
            criteria.resultCount = resultCount;


            List<E.PosSystemConfigrationLite> PosSystemConfigrationList = iPosSystemConfigrationManager.FindAllLite(criteria);
            if (PosSystemConfigrationList != null)
                return this.AttachStatusCode(PosSystemConfigrationList.First(), 1, null);
            else
                return this.AttachStatusCode(null, 1, null);

        }

         [WebMethod(EnableSession = true)]
         [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
         [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
         public string FindAll()
         {
             List<E.PosSystemConfigration> PosSystemConfigrationList = iPosSystemConfigrationManager.FindAll(null);
             if (PosSystemConfigrationList != null)
                 
                 return this.AttachStatusCode(PosSystemConfigrationList, 1, null);
             else
                 return this.AttachStatusCode(null, 1, null);

         }


         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string Add(int id, string PosSystemConfigrationInfo)
        {
            return Save(true, id, PosSystemConfigrationInfo);
        }


         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string Edit(int id, string PosSystemConfigrationInfo)
        {
            if (id <= 0)
            {
                ValidationError error = new ValidationError()
                {
                    ControlName = "",
                    GlobalMessage = true,
                    Message = "Please select PosSystemConfigration"
                };
                List<ValidationError> errors = new List<ValidationError>(){
                error
                };
                return this.AttachStatusCode(errors, 0, "validation");
            }
            else
            {
                return Save(false, id, PosSystemConfigrationInfo);
            }
        }

        private string Save(bool add, int id, string PosSystemConfigrationInfo)
        {
             E.PosSystemConfigration myPosSystemConfigration = this.javaScriptSerializer.Deserialize<E.PosSystemConfigration>(this.unEscape((PosSystemConfigrationInfo)));
            if (!add)
            {
                this.LockEntity(Enums_S3.Entity.SystemConfigration, id);
                myPosSystemConfigration.MarkOld();
                myPosSystemConfigration.MarkModified();
                myPosSystemConfigration.PosConfigId = id;
                myPosSystemConfigration.UpdatedBy = LoggedInUser.UserId;
            }
            else
            {
                myPosSystemConfigration.CreatedBy = LoggedInUser.UserId;
            }


            List<ValidationError> errors = iPosSystemConfigrationManager.Validate(myPosSystemConfigration);
            if (errors != null && errors.Count > 0)
            {
                return this.AttachStatusCode(errors, 0, "validation");
            }


            try
            {
                iPosSystemConfigrationManager.Save(myPosSystemConfigration);
                if (!add)
                    this.UnLockEntity(Enums_S3.Entity.SystemConfigration, id);
                return this.AttachStatusCode(myPosSystemConfigration.PosConfigId, 1, "");

            }
            catch (SF.Framework.Exceptions.RecordExsitsException ex)
            {
                return this.AttachStatusCode(null, 501, null);
            }
            catch (Exception ex)
            {

                return this.AttachStatusCode(null, 0, HandleException(ex));
            }
        }
    }
}

