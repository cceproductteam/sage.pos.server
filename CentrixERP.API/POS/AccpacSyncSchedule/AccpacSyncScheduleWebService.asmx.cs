using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using SF.Framework;

using E = SagePOS.Server.Business.Entity;
using SagePOS.Server.Business.IManager;
using Centrix.Common.API;
using System.Reflection;
using SagePOS.Server.Configuration;
using SF.FrameworkEntity;
using SagePOS.Server.API.Common.BaseClasses;
using CentrixERP.Common.API;




namespace SagePOS.Server.API
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]

    public class AccpacSyncScheduleWebService : WebServiceBaseClass
    {
        IAccpacSyncScheduleManager iAccpacSyncScheduleManager = null;
        public AccpacSyncScheduleWebService()
        {
            iAccpacSyncScheduleManager = (IAccpacSyncScheduleManager)IoC.Instance.Resolve(typeof(IAccpacSyncScheduleManager));
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.Delete)]
        public string Delete(int id)
        {
     
            E.AccpacSyncSchedule myAccpacSyncSchedule = iAccpacSyncScheduleManager.FindById(id, null);
            myAccpacSyncSchedule.TabId = id;
            myAccpacSyncSchedule.MarkDeleted();
            try
            {
                iAccpacSyncScheduleManager.Save(myAccpacSyncSchedule);
                return AttachStatusCode(true, 1, null);
            }
            catch (Exception ex)
            {
                return AttachStatusCode(null, 0, HandleException(ex));
            }

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindAllLite(string keyword, int page, int resultCount, string argsCriteria)
        {
            E.AccpacSyncScheduleCriteria criteria = null;
            if (!SF.Framework.String.IsEmpty(argsCriteria))
            {
                criteria = this.javaScriptSerializer.Deserialize<E.AccpacSyncScheduleCriteria>(argsCriteria);
            }
            else
                criteria = new E.AccpacSyncScheduleCriteria();

            criteria.Keyword = keyword;
            criteria.pageNumber = page;
            criteria.resultCount = resultCount;


            List<E.AccpacSyncScheduleLite> AccpacSyncScheduleList = iAccpacSyncScheduleManager.FindAllLite(criteria);
            return this.AttachStatusCode(AccpacSyncScheduleList, 1, null);

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindByIdLite(int id)
        {
            E.AccpacSyncScheduleLite myAccpacSyncSchedule = iAccpacSyncScheduleManager.FindByIdLite(id, null);
            return this.AttachStatusCode(myAccpacSyncSchedule, 1, null);

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string Add(int id, string AccpacSyncScheduleInfo)
        {
            return Save(true, id, AccpacSyncScheduleInfo);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string Edit(int id, string AccpacSyncScheduleInfo)
        {
            if (id <= 0)
            {
                ValidationError error = new ValidationError()
                {
                    ControlName = "",
                    GlobalMessage = true,
                    Message = "Please select AccpacSyncSchedule"
                };
                List<ValidationError> errors = new List<ValidationError>(){
                error
                };
                return this.AttachStatusCode(errors, 0, "validation");
            }
            else
            {
                return Save(false, id, AccpacSyncScheduleInfo);
            }
        }

        private string Save(bool add, int id, string AccpacSyncScheduleInfo)
        {
            E.AccpacSyncSchedule myAccpacSyncSchedule = this.javaScriptSerializer.Deserialize<E.AccpacSyncSchedule>(this.unEscape((AccpacSyncScheduleInfo)));
            if (!add)
            {
          
                myAccpacSyncSchedule.MarkOld();
                myAccpacSyncSchedule.MarkModified();
                myAccpacSyncSchedule.TabId = id;
                myAccpacSyncSchedule.UpdatedBy = LoggedInUser.UserId;
            }
            else
            {
                myAccpacSyncSchedule.CreatedBy = LoggedInUser.UserId;
            }


            List<ValidationError> errors = iAccpacSyncScheduleManager.Validate(myAccpacSyncSchedule);
            if (errors != null && errors.Count > 0)
            {
                return this.AttachStatusCode(errors, 0, "validation");
            }


            try
            {
                iAccpacSyncScheduleManager.Save(myAccpacSyncSchedule);

                return this.AttachStatusCode(myAccpacSyncSchedule.TabId, 1, null);

            }
            catch (SF.Framework.Exceptions.RecordExsitsException ex)
            {
                return this.AttachStatusCode(null, 501, null);
            }
            catch (Exception ex)
            {

                return this.AttachStatusCode(null, 0, HandleException(ex));
            }
        }
    }
}

