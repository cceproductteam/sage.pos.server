using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using SF.Framework;
using E = SagePOS.Server.Business.Entity;

using Centrix.Common.API;
using System.Reflection;

using SF.FrameworkEntity;
using CentrixERP.Common.API;
using SagePOS.Server.API.Common.BaseClasses;
using SagePOS.Server.Configuration;
using SagePOS.Server.Business.IManager;



namespace SagePOS.Server.API
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]

    public class BoCurrencyWebService : WebServiceBaseClass
    {
        IBoCurrencyManager iBoCurrencyManager = null;
        public BoCurrencyWebService()
        {
            iBoCurrencyManager = (IBoCurrencyManager)IoC.Instance.Resolve(typeof(IBoCurrencyManager));
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string Delete(int id)
        {
            //this.LockEntityForDelete(Enums_S3.Entity.BoCurrency, id);
            E.BoCurrency myBoCurrency = iBoCurrencyManager.FindById(id, null);
            myBoCurrency.CurrId = id;
            myBoCurrency.MarkDeleted();
            try
            {
                iBoCurrencyManager.Save(myBoCurrency);
                return AttachStatusCode(true, 1, null);
            }
            catch (Exception ex)
            {
                return AttachStatusCode(null, 0, HandleException(ex));
            }

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.List)]
        public string FindAllLite(string keyword, int page, int resultCount, string argsCriteria)
        {
            E.BoCurrencyCriteria criteria = null;
           
         criteria = new E.BoCurrencyCriteria();

            criteria.Keyword = keyword;
            //criteria.pageNumber = page;
            //criteria.resultCount = resultCount;


            List<E.BoCurrencyLite> BoCurrencyList = iBoCurrencyManager.FindAllLite(criteria);
            return this.AttachStatusCode(BoCurrencyList, 1, null);

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.View)]
        public string FindByIdLite(int id)
        {
            E.BoCurrencyLite myBoCurrency = iBoCurrencyManager.FindByIdLite(id, null);
            return this.AttachStatusCode(myBoCurrency, 1, null);

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.Add)]
        public string Add(int id, string BoCurrencyInfo)
        {
            return Save(true, id, BoCurrencyInfo);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.Edit)]
        public string Edit(int id, string BoCurrencyInfo)
        {
            if (id <= 0)
            {
                ValidationError error = new ValidationError()
                {
                    ControlName = "",
                    GlobalMessage = true,
                    Message = "Please select BoCurrency"
                };
                List<ValidationError> errors = new List<ValidationError>(){
                error
                };
                return this.AttachStatusCode(errors, 0, "validation");
            }
            else
            {
                return Save(false, id, BoCurrencyInfo);
            }
        }

        private string Save(bool add, int id, string BoCurrencyInfo)
        {
            E.BoCurrency myBoCurrency = this.javaScriptSerializer.Deserialize<E.BoCurrency>(this.unEscape((BoCurrencyInfo)));
            if (!add)
            {
                //this.LockEntity(Enums_S3.Entity.BoCurrency, id);
                myBoCurrency.MarkOld();
                myBoCurrency.MarkModified();
                myBoCurrency.CurrId = id;
                myBoCurrency.UpdatedBy = LoggedInUser.UserId;
            }
            else
            {
                myBoCurrency.CreatedBy = LoggedInUser.UserId;
            }


            List<ValidationError> errors = iBoCurrencyManager.Validate(myBoCurrency);
            if (errors != null && errors.Count > 0)
            {
                return this.AttachStatusCode(errors, 0, "validation");
            }


            try
            {
                iBoCurrencyManager.Save(myBoCurrency);
                //if (!add)
                //    this.UnLockEntity(Enums_S3.Entity.BoCurrency, id);
                return this.AttachStatusCode(myBoCurrency.CurrId, 1, null);

            }
            catch (SF.Framework.Exceptions.RecordExsitsException ex)
            {
                return this.AttachStatusCode(null, 501, null);
            }
            catch (Exception ex)
            {

                return this.AttachStatusCode(null, 0, HandleException(ex));
            }
        }
    }
}

