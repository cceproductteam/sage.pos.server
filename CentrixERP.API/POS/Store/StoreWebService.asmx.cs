using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using SF.Framework;
using E =SagePOS.Server.Business.Entity;
using SagePOS.Server.Business.IManager;
using CentrixERP.Common.API;
using System.Reflection;
using SagePOS.Server.Configuration;
using SF.FrameworkEntity;
using SagePOS.Server.API.Common.BaseClasses;



namespace Centrix.POS.Server.API
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    [EntityAttribute(Enums_S3.Entity.Store)]
    public class StoreWebService : WebServiceBaseClass
    {
        IStoreManager iStoreManager = null;
        public StoreWebService()
        {
            iStoreManager = (IStoreManager)IoC.Instance.Resolve(typeof(IStoreManager));
        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string Delete(int id)
        {
            this.LockEntityForDelete(Enums_S3.Entity.Store, id);
            E.Store myStore = iStoreManager.FindById(id, null);
            myStore.StoreId = id;
            myStore.MarkDeleted();
            try
            {
                iStoreManager.Save(myStore);
                return AttachStatusCode(true, 1, null);
            }
            catch (Exception ex)
            {
                return AttachStatusCode(null, 0, HandleException(ex));
            }

        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindAllLite(string keyword, int page, int resultCount, string argsCriteria)
        {
            E.StoreCriteria criteria = null;
            if (!SF.Framework.String.IsEmpty(argsCriteria))
            {
                criteria = this.javaScriptSerializer.Deserialize<E.StoreCriteria>(argsCriteria);
            }
            else
                criteria = new E.StoreCriteria();

            criteria.Keyword = keyword;
            criteria.pageNumber = page;
            criteria.resultCount = resultCount;


            List<E.StoreLite> StoreList = iStoreManager.FindAllLite(criteria);
            return this.AttachStatusCode(StoreList, 1, null);

        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindByIdLite(int id)
        {
            E.StoreLite myStore = iStoreManager.FindByIdLite(id, null);
            return this.AttachStatusCode(myStore, 1, null);

        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string Add(int id, string StoreInfo)
        {
            return Save(true, id, StoreInfo);
        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string Edit(int id, string StoreInfo)
        {
            if (id <= 0)
            {
                ValidationError error = new ValidationError()
                {
                    ControlName = "",
                    GlobalMessage = true,
                    Message = "Please select Store"
                };
                List<ValidationError> errors = new List<ValidationError>(){
                error
                };
                return this.AttachStatusCode(errors, 0, "validation");
            }
            else
            {
                return Save(false, id, StoreInfo);
            }
        }

        private string Save(bool add, int id, string StoreInfo)
        {
            E.Store myStore = this.javaScriptSerializer.Deserialize<E.Store>(this.unEscape((StoreInfo)));
            if (!add)
            {
                this.LockEntity(Enums_S3.Entity.Store, id);
                myStore.MarkOld();
                myStore.MarkModified();
                myStore.StoreId = id;
                myStore.UpdatedBy = LoggedInUser.UserId;
            }
            else
            {
                myStore.CreatedBy = LoggedInUser.UserId;
            }


            List<ValidationError> errors = iStoreManager.Validate(myStore);
            if (errors != null && errors.Count > 0)
            {
                return this.AttachStatusCode(errors, 0, "validation");
            }


            try
            {
                iStoreManager.Save(myStore);
                if (!add)
                    this.UnLockEntity(Enums_S3.Entity.Store, id);
                return this.FindByIdLite(myStore.StoreId);

            }
            catch (SF.Framework.Exceptions.RecordExsitsException ex)
            {
                return this.AttachStatusCode(null, 501, null);
            }
            catch (SF.Framework.Exceptions.RecordNotAffected ex)
            {
                return this.AttachStatusCode(null, 501, null);
            }
            catch (Exception ex)
            {

                return this.AttachStatusCode(null, 0, HandleException(ex));
            }
        }
    }
}

