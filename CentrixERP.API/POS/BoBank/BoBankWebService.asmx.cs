using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using SF.Framework;
using E = SagePOS.Server.Business.Entity;

using Centrix.Common.API;
using System.Reflection;

using SF.FrameworkEntity;
using CentrixERP.Common.API;
using SagePOS.Server.API.Common.BaseClasses;
using SagePOS.Server.Configuration;
using SagePOS.Server.Business.IManager;
using System.Collections.Generic;

namespace SagePOS.Server.API
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    [EntityAttribute(Enums_S3.Entity.BoBank)]
    public class BoBankWebService : WebServiceBaseClass
    {
        IBoBankManager iBoBankManager = null;
        public BoBankWebService()
        {
            iBoBankManager = (IBoBankManager)IoC.Instance.Resolve(typeof(IBoBankManager));
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.Delete)]
        public string Delete(int id)
        {
            this.LockEntityForDelete(Enums_S3.Entity.BoBank, id);
            E.BoBank myBoBank = iBoBankManager.FindById(id, null);
            myBoBank.Id = id;
            myBoBank.MarkDeleted();
            try
            {
                iBoBankManager.Save(myBoBank);
                return AttachStatusCode(true, 1, null);
            }
            catch (System.Exception ex)
            {
                return AttachStatusCode(null, 0, HandleException(ex));
            }

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.List)]
        public string FindAllLite(string keyword, int page, int resultCount, string argsCriteria)
        {
            E.BoBankCriteria criteria = null;

            criteria = new E.BoBankCriteria();

            criteria.Keyword = keyword;
            criteria.pageNumber = page;
            criteria.resultCount = resultCount;


            List<E.BoBankLite> BoBankList = iBoBankManager.FindAllLite(criteria);
            return this.AttachStatusCode(BoBankList, 1, null);

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.View)]
        public string FindByIdLite(int id)
        {
            E.BoBankLite myBoBank = iBoBankManager.FindByIdLite(id, null);
            return this.AttachStatusCode(myBoBank, 1, null);

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.Add)]
        public string Add(int id, string BoBankInfo)
        {
            return Save(true, id, BoBankInfo);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.Edit)]
        public string Edit(int id, string BoBankInfo)
        {
            if (id <= 0)
            {
                ValidationError error = new ValidationError()
                {
                    ControlName = "",
                    GlobalMessage = true,
                    Message = "Please select BoBank"
                };
                List<ValidationError> errors = new List<ValidationError>(){
                error
                };
                return this.AttachStatusCode(errors, 0, "validation");
            }
            else
            {
                return Save(false, id, BoBankInfo);
            }
        }

        private string Save(bool add, int id, string BoBankInfo)
        {
            E.BoBank myBoBank = this.javaScriptSerializer.Deserialize<E.BoBank>(this.unEscape((BoBankInfo)));
            if (!add)
            {
                this.LockEntity(Enums_S3.Entity.BoBank, id);
                myBoBank.MarkOld();
                myBoBank.MarkModified();
                myBoBank.Id = id;
                myBoBank.UpdatedBy = LoggedInUser.UserId;
            }
            else
            {
                myBoBank.CreatedBy = LoggedInUser.UserId;
            }


            List<ValidationError> errors = iBoBankManager.Validate(myBoBank);
            if (errors != null && errors.Count > 0)
            {
                return this.AttachStatusCode(errors, 0, "validation");
            }


            try
            {
                iBoBankManager.Save(myBoBank);
                if (!add)
                    this.UnLockEntity(Enums_S3.Entity.BoBank, id);
                return this.AttachStatusCode(myBoBank.Id, 1, null);

            }
            catch (SF.Framework.Exceptions.RecordExsitsException ex)
            {
                return this.AttachStatusCode(null, 501, null);
            }
            catch (System.Exception ex)
            {

                return this.AttachStatusCode(null, 0, HandleException(ex));
            }
        }
    }
}

