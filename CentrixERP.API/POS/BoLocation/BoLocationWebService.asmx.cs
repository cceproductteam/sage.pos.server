using System.Web.Services;
using System.Web.Script.Services;
using SF.Framework;
using E = SagePOS.Server.Business.Entity;

using Centrix.Common.API;
using System.Reflection;

using SF.FrameworkEntity;
using CentrixERP.Common.API;
using SagePOS.Server.API.Common.BaseClasses;
using SagePOS.Server.Configuration;
using SagePOS.Server.Business.IManager;
using System.Collections.Generic;

namespace Centrix.SagePOS.API
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    [EntityAttribute(Enums_S3.Entity.BoLocation)]
    public class BoLocationWebService : WebServiceBaseClass
    {
        IBoLocationManager iBoLocationManager = null;
        public BoLocationWebService()
        {
            iBoLocationManager = (IBoLocationManager)IoC.Instance.Resolve(typeof(IBoLocationManager));
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.Delete)]
        public string Delete(int id)
        {
            this.LockEntityForDelete(Enums_S3.Entity.BoLocation, id);
            E.BoLocation myBoLocation = iBoLocationManager.FindById(id, null);
            myBoLocation.Id = id;
            myBoLocation.MarkDeleted();
            try
            {
                iBoLocationManager.Save(myBoLocation);
                return AttachStatusCode(true, 1, null);
            }
            catch (System.Exception ex)
            {
                return AttachStatusCode(null, 0, HandleException(ex));
            }

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.List)]
        public string FindAllLite(string keyword, int page, int resultCount, string argsCriteria)
        {
            E.BoLocationCriteria criteria = null;
            if (!SF.Framework.String.IsEmpty(argsCriteria))
            {
                criteria = this.javaScriptSerializer.Deserialize<E.BoLocationCriteria>(argsCriteria);
            }
            else
                criteria = new E.BoLocationCriteria();

            criteria.Keyword = keyword;
            criteria.pageNumber = page;
            criteria.resultCount = resultCount;


            List<E.BoLocationLite> BoLocationList = iBoLocationManager.FindAllLite(criteria);
            return this.AttachStatusCode(BoLocationList, 1, null);

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.View)]
        public string FindByIdLite(int id)
        {
            E.BoLocationLite myBoLocation = iBoLocationManager.FindByIdLite(id, null);
            return this.AttachStatusCode(myBoLocation, 1, null);

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.Add)]
        public string Add(int id, string BoLocationInfo)
        {
            return Save(true, id, BoLocationInfo);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.Edit)]
        public string Edit(int id, string BoLocationInfo)
        {
            if (id <= 0)
            {
                ValidationError error = new ValidationError()
                {
                    ControlName = "",
                    GlobalMessage = true,
                    Message = "Please select BoLocation"
                };
                List<ValidationError> errors = new List<ValidationError>(){
                error
                };
                return this.AttachStatusCode(errors, 0, "validation");
            }
            else
            {
                return Save(false, id, BoLocationInfo);
            }
        }

        private string Save(bool add, int id, string BoLocationInfo)
        {
            E.BoLocation myBoLocation = this.javaScriptSerializer.Deserialize<E.BoLocation>(this.unEscape((BoLocationInfo)));
            if (!add)
            {
                this.LockEntity(Enums_S3.Entity.BoLocation, id);
                myBoLocation.MarkOld();
                myBoLocation.MarkModified();
                myBoLocation.Id = id;
                myBoLocation.UpdatedBy = LoggedInUser.UserId;
            }
            else
            {
                myBoLocation.CreatedBy = LoggedInUser.UserId;
            }


            List<ValidationError> errors = iBoLocationManager.Validate(myBoLocation);
            if (errors != null && errors.Count > 0)
            {
                return this.AttachStatusCode(errors, 0, "validation");
            }


            try
            {
                iBoLocationManager.Save(myBoLocation);
                if (!add)
                    this.UnLockEntity(Enums_S3.Entity.BoLocation, id);
                return this.AttachStatusCode(myBoLocation.Id, 1, null);

            }
            catch (SF.Framework.Exceptions.RecordExsitsException ex)
            {
                return this.AttachStatusCode(null, 501, null);
            }
            catch (System.Exception ex)
            {

                return this.AttachStatusCode(null, 0, HandleException(ex));
            }
        }
    }
}

