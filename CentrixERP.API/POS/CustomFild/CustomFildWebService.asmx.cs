using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using SF.Framework;


using Centrix.Common.API;
using System.Reflection;

using SF.FrameworkEntity;
using CentrixERP.Common.API;
using SagePOS.Server.Configuration;
using SagePOS.Server.API.Common.BaseClasses;
using SagePOS.Server.Business.IManager;
using E=SagePOS.Server.Business.Entity;



namespace Centrix.PurchaseOrder.API
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    [EntityAttribute(Enums_S3.Entity.CustomFild)]
    public class CustomFildWebService : WebServiceBaseClass
    {
        ICustomFildManager iCustomFildManager = null;
        public CustomFildWebService()
        {
            iCustomFildManager = (ICustomFildManager)IoC.Instance.Resolve(typeof(ICustomFildManager));
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    
        public string Delete(int id)
        {
            this.LockEntityForDelete(Enums_S3.Entity.CustomFild, id);
            E.CustomFild myCustomFild = iCustomFildManager.FindById(id, null);
            myCustomFild.CustomFildId = id;
            myCustomFild.MarkDeleted();
            try
            {
                iCustomFildManager.Save(myCustomFild);
                return AttachStatusCode(true, 1, null);
            }
            catch (Exception ex)
            {
                return AttachStatusCode(null, 0, HandleException(ex));
            }

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
     
        public string FindAllLite(string keyword, int page, int resultCount, string argsCriteria)
        {
            E.CustomFildCriteria criteria = null;
            if (!SF.Framework.String.IsEmpty(argsCriteria))
            {
                criteria = this.javaScriptSerializer.Deserialize<E.CustomFildCriteria>(argsCriteria);
            }
            else
                criteria = new E.CustomFildCriteria();

            criteria.Keyword = keyword;
            criteria.pageNumber = page;
            criteria.resultCount = resultCount;


            List<E.CustomFildLite> CustomFildList = iCustomFildManager.FindAllLite(criteria);
            return this.AttachStatusCode(CustomFildList, 1, null);

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]

        public string FindByIdLite(int id)
        {
            E.CustomFildLite myCustomFild = iCustomFildManager.FindByIdLite(id, null);
            return this.AttachStatusCode(myCustomFild, 1, null);

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]

        public string Add(int id, string CustomFildInfo)
        {
            return Save(true, id, CustomFildInfo);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]

        public string Edit(int id, string CustomFildInfo)
        {
            if (id <= 0)
            {
                ValidationError error = new ValidationError()
                {
                    ControlName = "",
                    GlobalMessage = true,
                    Message = "Please select CustomFild"
                };
                List<ValidationError> errors = new List<ValidationError>(){
                error
                };
                return this.AttachStatusCode(errors, 0, "validation");
            }
            else
            {
                return Save(false, id, CustomFildInfo);
            }
        }

        private string Save(bool add, int id, string CustomFildInfo)
        {
            E.CustomFild myCustomFild = this.javaScriptSerializer.Deserialize<E.CustomFild>(this.unEscape((CustomFildInfo)));
            if (!add)
            {
                this.LockEntity(Enums_S3.Entity.CustomFild, id);
                myCustomFild.MarkOld();
                myCustomFild.MarkModified();
                myCustomFild.CustomFildId = id;
                myCustomFild.UpdatedBy = LoggedInUser.UserId;
            }
            else
            {
                myCustomFild.CreatedBy = LoggedInUser.UserId;
            }


            List<ValidationError> errors = iCustomFildManager.Validate(myCustomFild);
            if (errors != null && errors.Count > 0)
            {
                return this.AttachStatusCode(errors, 0, "validation");
            }


            try
            {
                iCustomFildManager.Save(myCustomFild);
                if (!add)
                    this.UnLockEntity(Enums_S3.Entity.CustomFild, id);
                return this.FindByIdLite(myCustomFild.CustomFildId);

            }
            catch (SF.Framework.Exceptions.RecordExsitsException ex)
            {
                return this.AttachStatusCode(null, 501, null);
            }
            catch (Exception ex)
            {

                return this.AttachStatusCode(null, 0, HandleException(ex));
            }
        }
    }
}

