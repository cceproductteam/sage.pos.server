using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using SF.Framework;
using E = SagePOS.Server.Business.Entity;
using SagePOS.Server.Business.IManager;
using CentrixERP.Common.API;
using System.Reflection;
using SagePOS.Server.Configuration;
using SF.FrameworkEntity;
using SagePOS.Manger.Common.Business.Entity;
using CE = SagePOS.Manger.Common.Business.Entity;
using CentrixERP.Common.Business.IManager;
using SagePOS.Server.Business.Entity;
using CentrixERP.Common.API;
using SagePOS.Server.API.Common.BaseClasses;
using SagePOS.Server.Business.IManager;

namespace SagePOS.Server.API
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    [EntityAttribute(Enums_S3.Entity.Person)]
    public class PersonWebService : WebServiceBaseClass
    {
        IPersonManager iPersonManager = null;
        ICompanyManager iCompanyManager = null;
        IDataTypeContentManager iDataTypeContentManager = null;
        public PersonWebService()
        {
            iPersonManager = (IPersonManager)IoC.Instance.Resolve(typeof(IPersonManager));
            iCompanyManager = (ICompanyManager)IoC.Instance.Resolve(typeof(ICompanyManager));
            iDataTypeContentManager = (IDataTypeContentManager)IoC.Instance.Resolve(typeof(IDataTypeContentManager));
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string Delete(int id)
        {
            this.LockEntityForDelete(Enums_S3.Entity.Person, id);
            E.Person myPerson = iPersonManager.FindById(id, null);
            myPerson.PersonId = id;
            myPerson.MarkDeleted();
            try
            {
                iPersonManager.Save(myPerson);
                UnLockEntity(Enums_S3.Entity.Person, id);
                return AttachStatusCode(true, 1, null);
            }
            catch (Exception ex)
            {
                return AttachStatusCode(null, 0, HandleException(ex));
            }

        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindAllLite(string keyword, int page, int resultCount, string argsCriteria)
        {
            E.PersonCriteria criteria = null;
            if (!SF.Framework.String.IsEmpty(argsCriteria))
            {
                criteria = this.javaScriptSerializer.Deserialize<E.PersonCriteria>(argsCriteria);
            }
            else
                criteria = new E.PersonCriteria();

            criteria.Keyword = keyword;
            criteria.pageNumber = page;
            criteria.resultCount = resultCount;


            List<E.PersonLite> PersonList = iPersonManager.FindAllLite(criteria);
            return this.AttachStatusCode(PersonList, 1, null);

        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindAllLiteTransactionSearch(string keyword, int page, int resultCount, string argsCriteria)
        {
            E.PersonCriteria criteria = null;
            if (!SF.Framework.String.IsEmpty(argsCriteria))
            {
                criteria = this.javaScriptSerializer.Deserialize<E.PersonCriteria>(argsCriteria);
            }
            else
                criteria = new E.PersonCriteria();

            criteria.Keyword = keyword;
            criteria.pageNumber = page;
            criteria.resultCount = resultCount;


            List<E.PersonLite> PersonList = iPersonManager.FindAllLite(criteria);
            if (PersonList != null)
            {

                var person = from E.PersonLite PersonObj in PersonList
                             select new
                             {
                                 value = PersonObj.PersonId,
                                 label=PersonObj.FullNameEn,
                                
                                 //GrandTotal = PersonObj.GrandTotal,
                             };

                return this.AttachStatusCode(person, 1, null);
                

            }
            return null;

        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindByIdLite(int id)
        {
            PersonCriteria criteria = new PersonCriteria();
            criteria.PersonId = id;
            E.PersonLite myPerson = iPersonManager.FindByIdLite(id, criteria);
            myPerson.IntresetedInList = iDataTypeContentManager.FindAllByIds(myPerson.InterestedInId);
            return this.AttachStatusCode(myPerson, 1, null);

        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]

        public string Add(int id, string PersonInfo)
        {
            return Save(true, id, PersonInfo);
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string Edit(int id, string PersonInfo)
        {
            if (id <= 0)
            {
                ValidationError error = new ValidationError()
                {
                    ControlName = "",
                    GlobalMessage = true,
                    Message = "Please select Person"
                };
                List<ValidationError> errors = new List<ValidationError>(){
                error
                };
                return this.AttachStatusCode(errors, 0, "validation");
            }
            else
            {
                return Save(false, id, PersonInfo);
            }
        }

        private string Save(bool add, int id, string PersonInfo)
        {
            E.Person myPerson = this.javaScriptSerializer.Deserialize<E.Person>(this.unEscape((PersonInfo)));
            if (!add)
            {
                this.LockEntity(Enums_S3.Entity.Person, id);
                myPerson.MarkOld();
                myPerson.MarkModified();
                myPerson.PersonId = id;
                myPerson.UpdatedBy = LoggedInUser.UserId;
            }
            else
            {
                myPerson.CreatedBy = LoggedInUser.UserId;
            }


            List<ValidationError> errors = iPersonManager.Validate(myPerson);
            if (errors != null && errors.Count > 0)
            {
                return this.AttachStatusCode(errors, 0, "validation");
            }


            try
            {
                iPersonManager.Save(myPerson);
                if (!add)
                    this.UnLockEntity(Enums_S3.Entity.Person, id);
                return FindByIdLite(myPerson.PersonId);

            }
            catch (SF.Framework.Exceptions.RecordExsitsException ex)
            {
                return this.AttachStatusCode(null, 501, null);
            }
            catch (Exception ex)
            {

                return this.AttachStatusCode(null, 0, HandleException(ex));
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string savePersonContactInfo(int PersonId, string AddressObj, string HomePhoneObj, string FaxPhone, string MobilePhone, string BusinessEmail, string PersonalEmail)
        {

            E.Person PersonEntity = iPersonManager.FindById(PersonId, null);
            Address myAddress = this.javaScriptSerializer.Deserialize<Address>(this.unEscape((AddressObj)));

            Phone myHomePhone = this.javaScriptSerializer.Deserialize<Phone>(this.unEscape((HomePhoneObj)));
            Phone myFaxPhone = this.javaScriptSerializer.Deserialize<Phone>(this.unEscape((FaxPhone)));
            Phone myMobilePhone = this.javaScriptSerializer.Deserialize<Phone>(this.unEscape((MobilePhone)));

            Email myBusinessEmail = this.javaScriptSerializer.Deserialize<Email>(this.unEscape((BusinessEmail)));
            Email MyPersonalEmail = this.javaScriptSerializer.Deserialize<Email>(this.unEscape((PersonalEmail)));

            if (PersonEntity != null)
            {
                //PersonEntity.AddressPersonList = new List<AddressEntity>();
                //PersonEntity.PhonePersonList = new List<PhoneEntity>();
                //PersonEntity.EmailPersonList = new List<EmailEntity>();




                //if (myBusinessEmail != null && !string.IsNullOrEmpty(myBusinessEmail.EmailAddress))
                //{
                //    PersonEntity.EmailPersonList.Add(new EmailEntity()
                //    {
                //        EntityId = (int)Enums_S3.Entity.Person,
                //        EntityValueId = PersonId,
                //        EmailType = (int)Enums_S3.Email.EmailType.Business,
                //        IsDefault = true,
                //        EmailObj = new Email
                //        {
                //            EmailAddress = myBusinessEmail.EmailAddress,
                //            Type = Enums_S3.Email.EmailType.Business,
                //            CreatedBy = LoggedInUser.UserId
                //        },
                //        CreatedBy = LoggedInUser.UserId
                //    });
                //}

                //if (MyPersonalEmail != null && !string.IsNullOrEmpty(MyPersonalEmail.EmailAddress))
                //{
                //    PersonEntity.EmailPersonList.Add(new EmailEntity()
                //    {
                //        EntityId = (int)Enums_S3.Entity.Person,
                //        EntityValueId = PersonId,
                //        EmailType = (int)Enums_S3.Email.EmailType.Personal,
                //        EmailObj = new Email
                //        {
                //            EmailAddress = MyPersonalEmail.EmailAddress,
                //            Type = Enums_S3.Email.EmailType.Personal,
                //            CreatedBy = LoggedInUser.UserId
                //        },
                //        IsDefault = true,
                //        CreatedBy = LoggedInUser.UserId
                //    });
                //}

                //if (myHomePhone != null && !string.IsNullOrEmpty(myHomePhone.PhoneNumber))
                //{
                //    PersonEntity.PhonePersonList.Add(new PhoneEntity()
                //    {
                //        EntityId = (int)Enums_S3.Entity.Person,
                //        EntityValueId = PersonId,
                //        PhoneType = (int)Enums_S3.Phone.PhoneType.Home,
                //        PhoneObj = new Phone
                //        {
                //            CountryCode = myHomePhone.CountryCode,
                //            AreaCode = myHomePhone.AreaCode,
                //            PhoneNumber = myHomePhone.PhoneNumber,
                //            Type = Enums_S3.Phone.PhoneType.Home,
                //            CreatedBy = LoggedInUser.UserId
                //        },
                //        IsDefault = true,
                //        CreatedBy = LoggedInUser.UserId
                //    });
                //}

                //if (myFaxPhone != null && !string.IsNullOrEmpty(myFaxPhone.PhoneNumber))
                //{

                //    PersonEntity.PhonePersonList.Add(new PhoneEntity()
                //    {
                //        EntityId = (int)Enums_S3.Entity.Person,
                //        EntityValueId = PersonId,
                //        PhoneType = (int)Enums_S3.Phone.PhoneType.Fax,
                //        PhoneObj = new Phone
                //        {
                //            CountryCode = myFaxPhone.CountryCode,
                //            AreaCode = myFaxPhone.AreaCode,
                //            PhoneNumber = myFaxPhone.PhoneNumber,
                //            Type = Enums_S3.Phone.PhoneType.Fax,
                //            CreatedBy = LoggedInUser.UserId
                //        },
                //        IsDefault = true,
                //        CreatedBy = LoggedInUser.UserId
                //    });
                //}


                //if (myMobilePhone != null && !string.IsNullOrEmpty(myMobilePhone.PhoneNumber))
                //{
                //    PersonEntity.PhonePersonList.Add(new PhoneEntity()
                //    {
                //        EntityId = (int)Enums_S3.Entity.Person,
                //        EntityValueId = PersonId,
                //        PhoneType = (int)Enums_S3.Phone.PhoneType.Mobile,
                //        PhoneObj = new Phone
                //        {
                //            CountryCode = myMobilePhone.CountryCode,
                //            AreaCode = myMobilePhone.AreaCode,
                //            PhoneNumber = myMobilePhone.PhoneNumber,
                //            Type = Enums_S3.Phone.PhoneType.Mobile,
                //            CreatedBy = LoggedInUser.UserId
                //        },
                //        IsDefault = true,
                //        CreatedBy = LoggedInUser.UserId
                //    });
                //}




                if (myAddress != null)
                {
                    if (!string.IsNullOrEmpty(myAddress.CityText) || !string.IsNullOrEmpty(myAddress.StreetAddress) || myAddress.CountryId > 0)
                    {
                        //PersonEntity.AddressPersonList.Add(new AddressEntity()
                        //{
                        //    EntityId = (int)Enums_S3.Entity.Person,
                        //    EntityValueId = PersonId,
                        //    AddressObj = new Address
                        //    {
                        //        CreatedBy = LoggedInUser.UserId,
                        //        CountryId = myAddress.CountryId,
                        //        CityText = unEscape(myAddress.CityText),
                        //        StreetAddress = unEscape(myAddress.StreetAddress),
                        //        ZipCode = unEscape(myAddress.ZipCode),
                        //        Region = unEscape(myAddress.Region),
                        //        //Area = unEscape(myAddress.Area),
                        //        Place = unEscape(myAddress.Place),
                        //        NearBy = unEscape(myAddress.NearBy),
                        //        POBox = unEscape(myAddress.POBox),
                        //        BuildingNumber = myAddress.BuildingNumber,
                        //        Floor = unEscape(myAddress.Floor),
                        //        OfficeNumber = unEscape(myAddress.OfficeNumber),
                        //        County = unEscape(myAddress.County),
                        //        Type = unEscape(myAddress.Type)
                        //    },
                        //    IsDefault = true,
                        //    CreatedBy = LoggedInUser.UserId

                        //});
                    }
                }
            }

            try
            {
                iPersonManager.Save(PersonEntity);
                return this.AttachStatusCode(PersonEntity.PersonId, 1, null);
            }
            catch (Exception)
            {

                return this.AttachStatusCode(null, 0, null);
            }


        }




        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string CheckEmailDuplication(string email, int EmailType)
        {
            IEmailEntityManager iEmailEntityManager = (IEmailEntityManager)IoC.Instance.Resolve(typeof(IEmailEntityManager));
            CE.EmailEntityCriteria criteria = new CE.EmailEntityCriteria()
               {
                   EntityId = (int)Enums_S3.Entity.Person,
                   EmailAddress = email,
                   typeId = EmailType
               };
            List<CE.EmailEntityLite> list = iEmailEntityManager.FindAllLite(criteria);
            if (list != null && list.Count > 0)
            {
                return this.AttachStatusCode(null, 501, null);
            }
            else
            {
                return null;
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string CheckMobileDuplication(string PhoneNumber)
        {
            IPhoneEntityManager iPhoneEntityManager = (IPhoneEntityManager)IoC.Instance.Resolve(typeof(IPhoneEntityManager));
            CE.PhoneEntityCriteria criteria = new CE.PhoneEntityCriteria()
            {
                EntityId = (int)Enums_S3.Entity.Person,

                PhoneNumber = PhoneNumber,
                PhoneType = (int)Enums_S3.Phone.PhoneType.Mobile
            };

            List<CE.PhoneEntityLite> list = iPhoneEntityManager.FindAllLite(criteria);

            if (list != null && list.Count > 0)
            {
                return this.AttachStatusCode(null, 501, null);
            }
            else
            {
                return null;
            }
        }



        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string SearchPersons(string personName, string mobileNumber)
        {
            E.PersonCriteria criteria = null;
            criteria = new E.PersonCriteria();
            criteria.PersonName = personName;
            criteria.MobileNumber = mobileNumber;
            List<E.PersonLite> PersonList = iPersonManager.FindAllLite(criteria);

            if (PersonList != null)
            {

                var person = from E.PersonLite PersonObj in PersonList
                             select new
                             {
                                 CountryMobileNumber = PersonObj.CountryMobileNumber,
                                 label = PersonObj.FullNameEn,
                                 Email = PersonObj.Email,
                                 MobileNumber = PersonObj.MobileNumber,
                                 value = PersonObj.PersonId,
                             };


                return this.javaScriptSerializer.Serialize(person);

            }
            return null;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string SearchCompany(string companyName, string mobileNumber)
        {
            E.CompanyCriteria criteria = null;
            criteria = new E.CompanyCriteria();
            criteria.CompanyName = companyName;
            criteria.MobileNumber = mobileNumber;
            List<E.CompanyLite> CompanyList = iCompanyManager.FindAllLite(criteria);
            if (CompanyList != null)
            {
                var company = from E.CompanyLite PersonObj in CompanyList
                              select new
                              {
                                  CountryMobileNumber = PersonObj.CountryMobileNumber,
                                  label = PersonObj.CompanyNameEnglish,
                                  Email = PersonObj.Email,
                                  MobileNumber = PersonObj.MobileNumber
                                  //GrandTotal = PersonObj.GrandTotal,
                              };


                return this.javaScriptSerializer.Serialize(company);

            }
            return null;

        }



        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string ContactQuickAdd(string ContactData, int Type, string EmailAddress, string mobileNumber, string CountryCode, string AreaCode, int UserId, int genderId)
        {
            E.Person myPerson = this.javaScriptSerializer.Deserialize<E.Person>(this.unEscape((ContactData)));
            IPersonManager PersonManager = (IPersonManager)IoC.Instance.Resolve(typeof(IPersonManager));

            myPerson.IsQuickAdd = true;
            myPerson.Email = EmailAddress;
            myPerson.MobileNumber = CountryCode + '-' + AreaCode + '-' + mobileNumber;
            myPerson.FirstNameAr = myPerson.FirstNameEn;
            myPerson.LastNameAr = myPerson.LastNameEn;
            myPerson.CreatedBy = UserId;
            myPerson.GenderId = genderId;
            try
            {
                PersonManager.Save(myPerson);
                saveQuickAddPersonContactInfo(myPerson.PersonId, mobileNumber, CountryCode, AreaCode, EmailAddress);
                E.Person ReturnPerson = PersonManager.FindById(myPerson.PersonId, null);
                var PersonObj = new
                {
                    label = ReturnPerson.FullNameEn,
                    value = ReturnPerson.PersonId,
                    categoryId = 1,
                    category = "",
                    FullName = ReturnPerson.FullNameEn,
                    FullNameAr = ReturnPerson.FullNameAr

                };
                return this.javaScriptSerializer.Serialize(PersonObj);
            }
            catch (SF.Framework.Exceptions.RecordExsitsException ex)
            {
                return this.AttachStatusCode(null, 501, null);
            }
        }



        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string saveQuickAddPersonContactInfo(int PersonId, string MobilePhone, string CountryCode, string AreaCode, string BusinessEmail)
        {


            E.Person PersonEntity = iPersonManager.FindById(PersonId, null);
            if (PersonEntity != null)
            {
                //PersonEntity.AddressPersonList = new List<AddressEntity>();
                //PersonEntity.PhonePersonList = new List<PhoneEntity>();
                //PersonEntity.EmailPersonList = new List<EmailEntity>();
                //if (BusinessEmail != null)
                //{
                //    PersonEntity.EmailPersonList.Add(new EmailEntity()
                //    {
                //        EntityId = (int)Enums_S3.Entity.Person,
                //        EntityValueId = PersonId,
                //        EmailType = (int)Enums_S3.Email.EmailType.Business,
                //        IsDefault = true,
                //        EmailObj = new Email
                //        {
                //            EmailAddress = BusinessEmail,
                //            Type =Enums_S3.Email.EmailType.Business,
                //            CreatedBy = LoggedInUser.UserId
                //        },
                //        CreatedBy = LoggedInUser.UserId
                //    });
                //}
                //if (MobilePhone != null)
                //{
                //    PersonEntity.PhonePersonList.Add(new PhoneEntity()
                //    {
                //        EntityId = (int)Enums_S3.Entity.Person,
                //        EntityValueId = PersonId,
                //        PhoneType = (int)Enums_S3.Phone.PhoneType.Mobile,
                //        PhoneObj = new Phone
                //        {
                //            CountryCode = CountryCode,
                //            AreaCode = AreaCode,
                //            PhoneNumber = MobilePhone,
                //            Type =Enums_S3.Phone.PhoneType.Mobile,
                //            CreatedBy = LoggedInUser.UserId
                //        },
                //        IsDefault = true,
                //        CreatedBy = LoggedInUser.UserId
                //    });
                //}

            }

            try
            {
                iPersonManager.Save(PersonEntity);
                return this.AttachStatusCode(PersonEntity.PersonId, 1, null);
            }
            catch (Exception ex)
            {

                return this.AttachStatusCode(null, 0, null);
            }


        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string ContactCompanyQuickAdd(string ContactData, int Type, string EmailAddress, string mobileNumber, string CountryCode, string AreaCode, int UserId)
        {
            E.Company myCompany = this.javaScriptSerializer.Deserialize<E.Company>(this.unEscape((ContactData)));
            ICompanyManager companyManager = (ICompanyManager)IoC.Instance.Resolve(typeof(ICompanyManager));

            myCompany.IsQuickAdd = true;
            myCompany.Email = EmailAddress;
            myCompany.MobileNumber = CountryCode + '-' + AreaCode + '-' + mobileNumber;
            myCompany.CompanyNameEnglish = myCompany.CompanyNameEnglish;
            myCompany.CompanyNameArabic = myCompany.CompanyNameArabic;
            myCompany.CreatedBy = UserId;
            try
            {
                companyManager.Save(myCompany);
                saveQuickAddCompanyContactInfo(myCompany.CompanyId, mobileNumber, CountryCode, AreaCode, EmailAddress);
                E.Company ReturnPerson = companyManager.FindById(myCompany.CompanyId, null);
                var CompanyObj = new
                {
                    label = ReturnPerson.CompanyNameEnglish,
                    value = ReturnPerson.CompanyId,
                    categoryId = 1,
                    category = "",
                    FullName = ReturnPerson.CompanyNameEnglish,
                    FullNameAr = ReturnPerson.CompanyNameArabic

                };
                return this.javaScriptSerializer.Serialize(CompanyObj);
            }
            catch (SF.Framework.Exceptions.RecordExsitsException ex)
            {
                return this.AttachStatusCode(null, 501, null);
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string saveQuickAddCompanyContactInfo(int PersonId, string MobilePhone, string CountryCode, string AreaCode, string BusinessEmail)
        {


            E.Company CompanyEntity = iCompanyManager.FindById(PersonId, null);
            //if (CompanyEntity != null)
            //{
            //CompanyEntity.AddressCompanyList = new List<AddressEntity>();
            //    CompanyEntity.PhoneCompanyList = new List<PhoneEntity>();
            //    CompanyEntity.EmailCompanyList = new List<EmailEntity>();
            //    if (BusinessEmail != null)
            //    {
            //        CompanyEntity.EmailCompanyList.Add(new EmailEntity()
            //        {
            //            EntityId = (int)Enums_S3.Entity.Person,
            //            EntityValueId = PersonId,
            //            EmailType = (int)Enums_S3.Email.EmailType.Business,
            //            IsDefault = true,
            //            EmailObj = new Email
            //            {
            //                EmailAddress = BusinessEmail,
            //                Type = Enums_S3.Email.EmailType.Business,
            //                CreatedBy = LoggedInUser.UserId
            //            },
            //            CreatedBy = LoggedInUser.UserId
            //        });
            //    }
            //    if (MobilePhone != null)
            //    {
            //        CompanyEntity.PhoneCompanyList.Add(new PhoneEntity()
            //        {
            //            EntityId = (int)Enums_S3.Entity.Company,
            //            EntityValueId = PersonId,
            //            PhoneType = (int)Enums_S3.Phone.PhoneType.Mobile,
            //            PhoneObj = new Phone
            //            {
            //                CountryCode = CountryCode,
            //                AreaCode = AreaCode,
            //                PhoneNumber = MobilePhone,
            //                Type = Enums_S3.Phone.PhoneType.Mobile,
            //                CreatedBy = LoggedInUser.UserId
            //            },
            //            IsDefault = true,
            //            CreatedBy = LoggedInUser.UserId
            //        });
            //    }

            //}

            try
            {
                iCompanyManager.Save(CompanyEntity);
                return this.AttachStatusCode(CompanyEntity.CompanyId, 1, null);
            }
            catch (Exception ex)
            {

                return this.AttachStatusCode(null, 0, null);
            }


        }
    }
}

