using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using SF.Framework;
using E = SagePOS.Server.Business.Entity;
using SagePOS.Server.Business.IManager;
using CentrixERP.Common.API;
using System.Reflection;
using SagePOS.Server.Configuration;
using SF.FrameworkEntity;
using SagePOS.Server.API.Common.BaseClasses;
//using CentrixERP.AccountsReceivable.Business.IManager;




namespace SagePOS.Server.API
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    [EntityAttribute(Enums_S3.Entity.PaymentCode)]
    public class PaymentCodeWebService : WebServiceBaseClass
    {
        IPOSPaymentCodeManager iPaymentCodeManager = null;
        public PaymentCodeWebService()
        {
            iPaymentCodeManager = (IPOSPaymentCodeManager)IoC.Instance.Resolve(typeof(IPOSPaymentCodeManager));
        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    
        public string Delete(int id)
        {
            this.LockEntityForDelete(Enums_S3.Entity.PaymentCode, id);
            E.POSPaymentCode myPaymentCode = iPaymentCodeManager.FindById(id, null);
            myPaymentCode.PaymentCodeId = id;
            myPaymentCode.MarkDeleted();
            try
            {
                iPaymentCodeManager.Save(myPaymentCode);
                return AttachStatusCode(true, 1, null);
            }
            catch (Exception ex)
            {
                return AttachStatusCode(null, 0, HandleException(ex));
            }

        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]

        public string FindAllLite(string keyword, int page, int resultCount, string argsCriteria)
        {
            E.POSPaymentCodeCriteria criteria = null;
            if (!SF.Framework.String.IsEmpty(argsCriteria))
            {
                criteria = this.javaScriptSerializer.Deserialize<E.POSPaymentCodeCriteria>(argsCriteria);
            }
            else
                criteria = new E.POSPaymentCodeCriteria();

            criteria.Keyword = keyword;
            criteria.pageNumber = page;
            criteria.resultCount = resultCount;


            List<E.POSPaymentCodeLite> PaymentCodeList = iPaymentCodeManager.FindAllLite(criteria);
            return this.AttachStatusCode(PaymentCodeList, 1, null);

        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]

        public string FindByIdLite(int id)
        {
            E.POSPaymentCodeLite myPaymentCode = iPaymentCodeManager.FindByIdLite(id, null);
            return this.AttachStatusCode(myPaymentCode, 1, null);

        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
      
        public string Add(int id, string PaymentCodeInfo)
        {
            return Save(true, id, PaymentCodeInfo);
        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
 
        public string Edit(int id, string PaymentCodeInfo)
        {
            if (id <= 0)
            {
                ValidationError error = new ValidationError()
                {
                    ControlName = "",
                    GlobalMessage = true,
                    Message = "Please select PaymentCode"
                };
                List<ValidationError> errors = new List<ValidationError>(){
                error
                };
                return this.AttachStatusCode(errors, 0, "validation");
            }
            else
            {
                return Save(false, id, PaymentCodeInfo);
            }
        }

        private string Save(bool add, int id, string PaymentCodeInfo)
        {
            E.POSPaymentCode myPaymentCode = this.javaScriptSerializer.Deserialize<E.POSPaymentCode>(this.unEscape((PaymentCodeInfo)));
            if (!add)
            {
                this.LockEntity(Enums_S3.Entity.PaymentCode, id);
                myPaymentCode.MarkOld();
                myPaymentCode.MarkModified();
                myPaymentCode.PaymentCodeId = id;
                myPaymentCode.UpdatedBy = LoggedInUser.UserId;
            }
            else
            {
                myPaymentCode.CreatedBy = LoggedInUser.UserId;
            }


            List<ValidationError> errors = iPaymentCodeManager.Validate(myPaymentCode);
            if (errors != null && errors.Count > 0)
            {
                return this.AttachStatusCode(errors, 0, "validation");
            }


            try
            {
                iPaymentCodeManager.Save(myPaymentCode);
                if (!add)
                    this.UnLockEntity(Enums_S3.Entity.PaymentCode, id);
                return this.AttachStatusCode(myPaymentCode.PaymentCodeId, 1, null);

            }
            catch (SF.Framework.Exceptions.RecordExsitsException ex)
            {
                return this.AttachStatusCode(null, 501, null);
            }
            catch (Exception ex)
            {

                return this.AttachStatusCode(null, 0, HandleException(ex));
            }
        }


       
    }
}

