using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using SF.Framework;
using E = SagePOS.Server.Business.Entity;
using SagePOS.Server.Business.IManager;
using CentrixERP.Common.API;
using System.Reflection;
using SagePOS.Server.Configuration;
using SF.FrameworkEntity;
using SagePOS.Server.API.Common.BaseClasses;



namespace Centrix.POS.Server.API
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    [EntityAttribute(Enums_S3.Entity.QuickKeysProduct)]
    public class QuickKeysProductWebService : WebServiceBaseClass
    {
        IQuickKeysProductManager iQuickKeysProductManager = null;
        public QuickKeysProductWebService()
        {
            iQuickKeysProductManager = (IQuickKeysProductManager)IoC.Instance.Resolve(typeof(IQuickKeysProductManager));
        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string Delete(int id)
        {
            this.LockEntityForDelete(Enums_S3.Entity.QuickKeysProduct, id);
            E.QuickKeysProduct myQuickKeysProduct = iQuickKeysProductManager.FindById(id, null);
            myQuickKeysProduct.QuickKeyProductId = id;
            myQuickKeysProduct.MarkDeleted();
            try
            {
                iQuickKeysProductManager.Save(myQuickKeysProduct);
                return AttachStatusCode(true, 1, null);
            }
            catch (Exception ex)
            {
                return AttachStatusCode(null, 0, HandleException(ex));
            }

        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindAllLite(string keyword, int page, int resultCount, string argsCriteria)
        {
            E.QuickKeysProductCriteria criteria = null;
            if (!SF.Framework.String.IsEmpty(argsCriteria))
            {
                criteria = this.javaScriptSerializer.Deserialize<E.QuickKeysProductCriteria>(argsCriteria);
            }
            else
                criteria = new E.QuickKeysProductCriteria();

            criteria.Keyword = keyword;
            criteria.pageNumber = page;
            criteria.resultCount = resultCount;


            List<E.QuickKeysProductLite> QuickKeysProductList = iQuickKeysProductManager.FindAllLite(criteria);
            return this.AttachStatusCode(QuickKeysProductList, 1, null);

        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindByIdLite(int id)
        {
            E.QuickKeysProductLite myQuickKeysProduct = iQuickKeysProductManager.FindByIdLite(id, null);
            return this.AttachStatusCode(myQuickKeysProduct, 1, null);

        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string Add(int id, string QuickKeysProductInfo)
        {
            return Save(true, id, QuickKeysProductInfo);
        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string Edit(int id, string QuickKeysProductInfo)
        {
            if (id <= 0)
            {
                ValidationError error = new ValidationError()
                {
                    ControlName = "",
                    GlobalMessage = true,
                    Message = "Please select QuickKeysProduct"
                };
                List<ValidationError> errors = new List<ValidationError>(){
                error
                };
                return this.AttachStatusCode(errors, 0, "validation");
            }
            else
            {
                return Save(false, id, QuickKeysProductInfo);
            }
        }

        private string Save(bool add, int id, string QuickKeysProductInfo)
        {
            E.QuickKeysProduct myQuickKeysProduct = this.javaScriptSerializer.Deserialize<E.QuickKeysProduct>(this.unEscape((QuickKeysProductInfo)));
            if (!add)
            {
                this.LockEntity(Enums_S3.Entity.QuickKeysProduct, id);
                myQuickKeysProduct.MarkOld();
                myQuickKeysProduct.MarkModified();
                myQuickKeysProduct.QuickKeyProductId = id;
                myQuickKeysProduct.UpdatedBy = LoggedInUser.UserId;
            }
            else
            {
                myQuickKeysProduct.CreatedBy = LoggedInUser.UserId;
            }


            List<ValidationError> errors = iQuickKeysProductManager.Validate(myQuickKeysProduct);
            if (errors != null && errors.Count > 0)
            {
                return this.AttachStatusCode(errors, 0, "validation");
            }


            try
            {
                iQuickKeysProductManager.Save(myQuickKeysProduct);
                if (!add)
                    this.UnLockEntity(Enums_S3.Entity.QuickKeysProduct, id);
                return this.FindByIdLite(myQuickKeysProduct.QuickKeyProductId);

            }
            catch (SF.Framework.Exceptions.RecordExsitsException ex)
            {
                return this.AttachStatusCode(null, 501, null);
            }
            catch (Exception ex)
            {

                return this.AttachStatusCode(null, 0, HandleException(ex));
            }
        }
    }
}

