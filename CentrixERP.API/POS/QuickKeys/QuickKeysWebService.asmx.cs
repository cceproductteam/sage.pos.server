using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using SF.Framework;
using E = SagePOS.Server.Business.Entity;
using SagePOS.Server.Business.IManager;
using CentrixERP.Common.API;
using System.Reflection;
using SagePOS.Server.Configuration;
using SF.FrameworkEntity;
using SagePOS.Server.API.Common.BaseClasses;



namespace Centrix.POS.Server.API
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    [EntityAttribute(Enums_S3.Entity.QuickKeys)]
    public class QuickKeysWebService : WebServiceBaseClass
    {
        IQuickKeysManager iQuickKeysManager = null;
        IQuickKeysProductManager iQuickKeysProductManager = null;
        public QuickKeysWebService()
        {
            iQuickKeysManager = (IQuickKeysManager)IoC.Instance.Resolve(typeof(IQuickKeysManager));
            iQuickKeysProductManager = (IQuickKeysProductManager)IoC.Instance.Resolve(typeof(IQuickKeysProductManager));
        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string Delete(int id)
        {
            this.LockEntityForDelete(Enums_S3.Entity.QuickKeys, id);
            E.QuickKeys myQuickKeys = iQuickKeysManager.FindById(id, null);
            myQuickKeys.QuickKeyId = id;
            myQuickKeys.MarkDeleted();
            try
            {
                iQuickKeysManager.Save(myQuickKeys);
                return AttachStatusCode(true, 1, null);
            }
            catch (Exception ex)
            {
                return AttachStatusCode(null, 0, HandleException(ex));
            }

        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindAllLite(string keyword, int page, int resultCount, string argsCriteria)
        {
            E.QuickKeysCriteria criteria = null;
            if (!SF.Framework.String.IsEmpty(argsCriteria))
            {
                criteria = this.javaScriptSerializer.Deserialize<E.QuickKeysCriteria>(argsCriteria);
            }
            else
                criteria = new E.QuickKeysCriteria();

            criteria.Keyword = keyword;
            criteria.pageNumber = page;
            criteria.resultCount = resultCount;


            List<E.QuickKeysLite> QuickKeysList = iQuickKeysManager.FindAllLite(criteria);
            return this.AttachStatusCode(QuickKeysList, 1, null);

        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindByIdLite(int id)
        {
            E.QuickKeysLite myQuickKeys = iQuickKeysManager.FindByIdLite(id, null);
            return this.AttachStatusCode(myQuickKeys, 1, null);

        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string Add(int id, string QuickKeysInfo, string QuickKeyName)
        {
            return Save(true, id, QuickKeysInfo, QuickKeyName);
        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string Edit(int id, string QuickKeysInfo, string QuickKeyName)
        {
            if (id <= 0)
            {
                ValidationError error = new ValidationError()
                {
                    ControlName = "",
                    GlobalMessage = true,
                    Message = "Please select QuickKeys"
                };
                List<ValidationError> errors = new List<ValidationError>(){
                error
                };
                return this.AttachStatusCode(errors, 0, "validation");
            }
            else
            {
                return Save(false, id, QuickKeysInfo, QuickKeyName);
            }
        }

        private string Save(bool add, int id, string QuickKeysInfo, string QuickKeyName)
        {
            E.QuickKeys myQuickKeys = null;

            if (!add)
            {
                if (id > 0)
                    myQuickKeys = iQuickKeysManager.FindById(id, null);

                this.LockEntity(Enums_S3.Entity.QuickKeys, id);
                myQuickKeys.MarkOld();
                myQuickKeys.MarkModified();
                myQuickKeys.QuickKeyId = id;
                myQuickKeys.UpdatedBy = LoggedInUser.UserId;
                myQuickKeys.Name = QuickKeyName;
            }
            else
            {
                myQuickKeys = new E.QuickKeys();
                myQuickKeys.CreatedBy = LoggedInUser.UserId;
                myQuickKeys.Name = QuickKeyName;
            }


            List<ValidationError> errors = iQuickKeysManager.Validate(myQuickKeys);
            if (errors != null && errors.Count > 0)
            {
                return this.AttachStatusCode(errors, 0, "validation");
            }


            try
            {
                iQuickKeysManager.Save(myQuickKeys);
                try
                {
                    SaveQuickKeysProducts(myQuickKeys.QuickKeyId, QuickKeysInfo);
                }
                catch (Exception ex)
                {

                    return this.AttachStatusCode(null, 0, HandleException(ex));
                }
                if (!add)
                    this.UnLockEntity(Enums_S3.Entity.QuickKeys, id);
                return this.FindByIdLite(myQuickKeys.QuickKeyId);

            }
            catch (SF.Framework.Exceptions.RecordExsitsException ex)
            {
                return this.AttachStatusCode(null, 501, null);
            }
            catch (Exception ex)
            {

                return this.AttachStatusCode(null, 0, HandleException(ex));
            }
        }


        private void SaveQuickKeysProducts(int QuickKeyId, string QuickKeysInfo)
        {

            List<E.QuickKeysProduct> QuickKeysProductsOldList = iQuickKeysProductManager.FindByParentId(QuickKeyId, typeof(E.QuickKeys), null);
            if (QuickKeysProductsOldList == null)
                QuickKeysProductsOldList = new List<E.QuickKeysProduct>();


            Object[] QuickKeysProductsNewList = this.javaScriptSerializer.Deserialize<Object[]>(this.unEscape((QuickKeysInfo)));


            List<string> ids = (from Object obj in QuickKeysProductsNewList
                                select ((Dictionary<string, object>)obj)["QuickKeyProductId"].ToString()).ToList();

            if (QuickKeysProductsOldList != null && QuickKeysProductsOldList.Count > 0)
            {
                foreach (E.QuickKeysProduct item in QuickKeysProductsOldList)
                {
                    var contain = from string Id in ids where item.QuickKeyProductId == Convert.ToInt32(Id) select Id;
                    if (contain == null || contain.Count() == 0)
                    {
                        item.MarkDeleted();
                    }
                }

                for (int i = 0; i < QuickKeysProductsOldList.Count; i++)
                {
                    E.QuickKeysProduct item = QuickKeysProductsOldList[i];
                    var contain = from string Id in ids
                                  where item.QuickKeyProductId == Convert.ToInt32(Id)
                                      && Convert.ToInt32(Id) != -1 && Convert.ToInt32(Id) != 0
                                  select Id;
                    if (contain != null && contain.Count() != 0)
                    {
                        QuickKeysProductsOldList[i] = getItem(QuickKeysProductsNewList, item.QuickKeyProductId, QuickKeyId);
                        QuickKeysProductsOldList[i].MarkOld();
                        QuickKeysProductsOldList[i].MarkModified();
                    }
                }

                foreach (string id in ids)
                {
                    var contain = from E.QuickKeysProduct item in QuickKeysProductsOldList
                                  where item.QuickKeyProductId == Convert.ToInt32(id)
                                   && item.QuickKeyProductId != -1 && item.QuickKeyProductId != 0
                                  select item;
                    if (contain == null || contain.Count() == 0)
                    {
                        E.QuickKeysProduct item = getItem(QuickKeysProductsNewList, id.ToNumber(), QuickKeyId);

                        if (item != null)
                            QuickKeysProductsOldList.Add(item);
                    }
                }
            }
            else
            {
                foreach (string id in ids)
                {
                    E.QuickKeysProduct item = getItem(QuickKeysProductsNewList, id.ToNumber(), QuickKeyId);

                    if (item != null)
                        QuickKeysProductsOldList.Add(item);
                }
            }


            try
            {
                foreach (E.QuickKeysProduct obj in QuickKeysProductsOldList)
                {
                    obj.QuickKeyId = QuickKeyId;
                    iQuickKeysProductManager.Save(obj);
                }

                this.UnLockEntity(Enums_S3.Entity.QuickKeys, null);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        private E.QuickKeysProduct getItem(object[] items, int Id,int ParentId)
        {
            E.QuickKeysProduct item = null;
            foreach (Dictionary<string, Object> obj in items)
            {
                item = new E.QuickKeysProduct();
                item.QuickKeyId = ParentId;
                if (obj["QuickKeyProductId"].ToString().ToNumber() == Id)
                {
                    if (Id == -1)
                        obj["QuickKeyProductId"] = "0";

                    foreach (KeyValuePair<string, Object> val in obj)
                    {
                        PropertyInfo pro = item.GetType().GetProperty(val.Key);
                        if (pro == null)
                            continue;

                        object value = null;
                        if (val.Value != null && !string.IsNullOrEmpty(val.Value.ToString()))
                            value = val.Value;

                        pro.SetValue(item, GetPropertyValue(pro.PropertyType, value), null);
                    }
                    break;
                }
            }
            return item;
        }



         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindAllProductsLite(string keyword, int page, int resultCount, string argsCriteria)
        {
            E.QuickKeysProductCriteria criteria = null;
            if (!SF.Framework.String.IsEmpty(argsCriteria))
            {
                criteria = this.javaScriptSerializer.Deserialize<E.QuickKeysProductCriteria>(argsCriteria);
            }
            else
                criteria = new E.QuickKeysProductCriteria();

            criteria.Keyword = keyword;
            criteria.pageNumber = page;
            criteria.resultCount = resultCount;


            List<E.QuickKeysProductLite> QuickKeysProductList = iQuickKeysProductManager.FindAllLite(criteria);
            return this.AttachStatusCode(QuickKeysProductList, 1, null);

        }
    }
}

