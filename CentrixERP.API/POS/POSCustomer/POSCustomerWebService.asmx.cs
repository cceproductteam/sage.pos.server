using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using SF.Framework;
using E = SagePOS.Server.Business.Entity;
using SagePOS.Server.Business.IManager;
using Centrix.Common.API;
using System.Reflection;
using SagePOS.Server.Configuration;
using SF.FrameworkEntity;
using CentrixERP.Common.API;
using SagePOS.Server.API.Common.BaseClasses;



namespace SagePOS.Server.API
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    [EntityAttribute(Enums_S3.Entity.IcCustomer)]
    public class POSCustomerWebService : POSWebServiceBaseClass
    {
        IPOSCustomerManager iPOSCustomerManager = null;
        public POSCustomerWebService()
        {
            iPOSCustomerManager = (IPOSCustomerManager)IoC.Instance.Resolve(typeof(IPOSCustomerManager));
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.Delete)]
        public string Delete(int id)
        {
            this.LockEntityForDelete(Enums_S3.Entity.IcCustomer, id);
            E.POSCustomer myIcCustomer = iPOSCustomerManager.FindById(id, null);
            myIcCustomer.CustomerId = id;
            myIcCustomer.MarkDeleted();
            try
            {
                iPOSCustomerManager.Save(myIcCustomer);
                return AttachStatusCode(true, 1, null);
            }
            catch (Exception ex)
            {
                return AttachStatusCode(null, 0, HandleException(ex));
            }

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindAllLite(string keyword, int page, int resultCount, string argsCriteria)
        {
            E.POSCustomerCriteria criteria = null;
            if (!SF.Framework.String.IsEmpty(argsCriteria))
            {
                criteria = this.javaScriptSerializer.Deserialize<E.POSCustomerCriteria>(argsCriteria);
            }
            else
                criteria = new E.POSCustomerCriteria();

            criteria.Keyword = keyword;
            criteria.pageNumber = page;
            criteria.resultCount = resultCount;
            criteria.CustTaxClass = 2;

            List<E.POSCustomerLite> IcCustomerList = iPOSCustomerManager.FindAllLite(criteria);
            return this.AttachStatusCode(IcCustomerList, 1, null);

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindByIdLite(int id)
        {
            E.POSCustomerLite myIcCustomer = iPOSCustomerManager.FindByIdLite(id, null);
            return this.AttachStatusCode(myIcCustomer, 1, null);

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.Add)]
        public string Add(int id, string IcCustomerInfo)
        {
            return Save(true, id, IcCustomerInfo);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string Edit(int id, string IcCustomerInfo)
        {
            if (id <= 0)
            {
                ValidationError error = new ValidationError()
                {
                    ControlName = "",
                    GlobalMessage = true,
                    Message = "Please select IcCustomer"
                };
                List<ValidationError> errors = new List<ValidationError>(){
                error
                };
                return this.AttachStatusCode(errors, 0, "validation");
            }
            else
            {
                return Save(false, id, IcCustomerInfo);
            }
        }

        private string Save(bool add, int id, string IcCustomerInfo)
        {
            E.POSCustomer myIcCustomer = this.javaScriptSerializer.Deserialize<E.POSCustomer>(this.unEscape((IcCustomerInfo)));
            if (!add)
            {
                this.LockEntity(Enums_S3.Entity.IcCustomer, id);
                myIcCustomer.MarkOld();
                myIcCustomer.MarkModified();
                myIcCustomer.CustomerId = id;
                myIcCustomer.UpdatedBy = LoggedInUser.UserId;
            }
            else
            {
                myIcCustomer.CreatedBy = LoggedInUser.UserId;
            }


            List<ValidationError> errors = iPOSCustomerManager.Validate(myIcCustomer);
            if (errors != null && errors.Count > 0)
            {
                return this.AttachStatusCode(errors, 0, "validation");
            }


            try
            {
                iPOSCustomerManager.Save(myIcCustomer);
                if (!add)
                    this.UnLockEntity(Enums_S3.Entity.IcCustomer, id);
                return this.FindByIdLite(myIcCustomer.CustomerId);

            }
            catch (SF.Framework.Exceptions.RecordExsitsException ex)
            {
                return this.AttachStatusCode(null, 501, null);
            }
            catch (Exception ex)
            {

                return this.AttachStatusCode(null, 0, HandleException(ex));
            }
        }
    }
}

