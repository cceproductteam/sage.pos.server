﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using E = SagePOS.Manger.Common.Business.Entity;
using SagePOS.Server.API.Common.BaseClasses;
using CentrixERP.Common.Business.IManager;
using SF.Framework;
using CentrixERP.Common.API;
using SagePOS.Server.Configuration;

namespace SagePOS.Server.API.Common.DataTypeContent
{
    /// <summary>
    /// Summary description for DataTypeContentWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class DataTypeContentWebService : WebServiceBaseClass
    {
        IDataTypeContentManager iDataTypeContentManager = null;
        IDataTypeManager iDataTypeManager = null;

        public DataTypeContentWebService()
        {
            iDataTypeContentManager = (IDataTypeContentManager)IoC.Instance.Resolve(typeof(IDataTypeContentManager));
            iDataTypeManager = (IDataTypeManager)IoC.Instance.Resolve(typeof(IDataTypeManager));
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]

        public string FindAll(int parentId)
        {
            List<E.DataTypeContent> dataTypeContentList = iDataTypeContentManager.FindByParentId(parentId, typeof(E.DataType), null);

            if (dataTypeContentList != null && dataTypeContentList.Count > 0)
            {
                var result = from E.DataTypeContent content in dataTypeContentList
                             select new
                             {
                                 label = (SagePOS.Server.Configuration.Configuration.Lang == SagePOS.Server.Configuration.Enums_S3.Configuration.Language.ar) ? content.DataTypeContentAR : content.DataTypeContentEN,
                                 value = content.DataTypeContentID,
                                 categoryId = -1,
                                 category = "",

                             };

                return this.AttachStatusCode(result, 0, null);
            }
            else
                return this.AttachStatusCode(null, 0, null);

        }
    }
}