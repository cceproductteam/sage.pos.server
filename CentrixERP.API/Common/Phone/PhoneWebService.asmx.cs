using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using CE = SagePOS.Manger.Common.Business.Entity;
using SF.Framework;
using System.Web.Script.Serialization;
using CentrixERP.Common.Business.IManager;
using P = SagePOS.Server.Business.Factory ;
using SagePOS.Server.Configuration;
using Centrix.Common.API;
using SagePOS.Server.API.Common.BaseClasses;

using SagePOS.Server.API;
using CentrixERP.Common.API;
//using Centrix.CRM.Business.IManager;
//using Centrix.CRM.Business.Entity;

namespace Centrix.Common.API
{
    /// <summary>
    /// Summary description for PhoneWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    [EntityAttribute(Enums_S3.Entity.Phone)]
    public class PhoneWebService : WebServiceBaseClass
    {
        //IPersonManager PersonMgr = null;
        P.PhoneFactory phoneFactory = null;
        IPhoneEntityManager iPhoneEntityManager = null;
        public PhoneWebService()
        {
            //PersonMgr = (IPersonManager)IoC.Instance.Resolve(typeof(IPersonManager));
            //phoneFactory = (P.PhoneFactory)IoC.Instance.Resolve(typeof(P.PhoneFactory));
            iPhoneEntityManager = (IPhoneEntityManager)IoC.Instance.Resolve(typeof(IPhoneEntityManager));
        }

        // [WebMethod(EnableSession = true)]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        //[EntityMethod(Enums_S3.DefaultPermissions.View)]
        //public string FindById(int id, string mKey)
        //{
        //    phoneFactory = new P.PhoneFactory(mKey);
        //    P.IPhone phone = phoneFactory.FindById(id, null);
        //    return this.AttachStatusCode(phone, 0, null);
        //}

        // [WebMethod(EnableSession = true)]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        //[EntityMethod(Enums_S3.DefaultPermissions.Delete)]
        //public string DeletePhone(int id, string mKey)
        //{
        //    phoneFactory = new P.PhoneFactory(mKey);
        //    P.IPhone iPhone = phoneFactory.FindById(id, null);
        //    this.LockEntityForDelete(Enums_S3.Entity.Phone, iPhone.PhoneId);
        //    iPhone.MarkDeleted();
        //    phoneFactory.Save(iPhone);
        //    this.UnLockEntity(Enums_S3.Entity.Phone, iPhone.PhoneId);
        //    return this.AttachStatusCode(null, 0, null);
        //}

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]

        public string FindAll(int ParentId, string mKey)
        {
            phoneFactory = new P.PhoneFactory(mKey);
            List<P.IPhone> phoneList = phoneFactory.FindByParentId(ParentId, null);
            if (phoneList != null && phoneList.Count() > 0)
            {
                var Phones = from P.IPhone phone in phoneList
                             where phone.PhoneObj != null
                             select new
                             {
                                 Id = phone.Id,
                                 PhoneID = phone.PhoneObj.PhoneID,
                                 CountryCode = phone.PhoneObj.CountryCode,
                                 AreaCode = phone.PhoneObj.AreaCode,
                                 PhoneNumber = phone.PhoneObj.PhoneNumber,
                                 FullPhoneNumber = phone.PhoneObj.CountryCode + ' ' + phone.PhoneObj.AreaCode + ' ' + phone.PhoneObj.PhoneNumber,
                                 CreatedBy = phone.PhoneObj.CreatedByName,
                                 UpdatedBy = phone.PhoneObj.UpdatedByName,
                                 Crearted_Date = phone.PhoneObj.Crearted_Date,
                                 Updated_Date = phone.PhoneObj.Updated_Date,
                                 Isdefault = phone.Isdefault,
                                 Description = phone.PhoneObj.Description,
                                 IsdefaultValue = (phone.Isdefault) ? "True" : "False",
                                 PhoneType = phone.PhoneObj.Type.ToString()
                             };

                return this.AttachStatusCode(Phones, 0, null);
            }
            else
            {
                return this.AttachStatusCode(null, 0, null);
            }

        }

        [WebMethod(EnableSession = true)]

        [EntityMethodAttribute(true, "id", ValidDefaultPermissions1 = Enums_S3.DefaultPermissions.Edit, ValidDefaultPermissions2 = Enums_S3.DefaultPermissions.Add)]
        public string AddEditPhone(int id, int moduleId, string mKey, string CountryCode, string AreaCode, string PhoneNumber, string Type, bool IsDefault, string Description, int userId)
        {
            phoneFactory = new P.PhoneFactory(mKey);
            P.IPhone iPhone = null;
            if (id <= 0)
            {
                iPhone = phoneFactory.IEntity;
               // iPhone.PhoneObj = new SagePOS.Manger.Common.Business.Entity.Phone();
                iPhone.PhoneObj = null;
                iPhone.CreatedBy = userId;
                iPhone.PhoneObj.CreatedBy = userId;
            }
            else
            {
                iPhone = phoneFactory.FindById(id, null);
                this.LockEntity(Enums_S3.Entity.Phone, iPhone.PhoneId);
                iPhone.MarkModified();
                iPhone.PhoneObj.MarkModified();
                iPhone.UpdatedBy = userId;
                iPhone.PhoneObj.UpdatedBy = userId;
            }
            iPhone.Id = id;
            iPhone.ModuleId = moduleId;
            iPhone.Isdefault = IsDefault;
            iPhone.PhoneObj.CountryCode = CountryCode;
            iPhone.PhoneObj.AreaCode = AreaCode;
            iPhone.PhoneObj.PhoneNumber = PhoneNumber;
            iPhone.PhoneObj.Description = unEscape(Description);

            iPhone.PhoneObj.Type = (Enums_S3.Phone.PhoneType)Type.ToNumber();

            try
            {
                if (mKey == "ppers" && (Enums_S3.Phone.PhoneType)Type.ToNumber() == Enums_S3.Phone.PhoneType.Mobile)
                {

                    //if (PersonMgr.CheckPersonPhoneExist(CountryCode + AreaCode + PhoneNumber, Type, id))
                    //    phoneFactory.Save(iPhone);
                    //else
                    //    return this.AttachStatusCode(null, 501, null);
                }
                else
                {
                    phoneFactory.Save(iPhone);
                }


                if (id > 0)
                    this.UnLockEntity(Enums_S3.Entity.Phone, iPhone.PhoneId);

                return this.AttachStatusCode(iPhone.PhoneId, 0, null);
            }
            catch (SF.Framework.Exceptions.RecordExsitsException ex)
            {
                return this.AttachStatusCode(null, 501, null);
            }
            catch (Exception ex)
            {
                this.UnLockEntity(Enums_S3.Entity.Phone, iPhone.PhoneId);
                return this.AttachStatusCode(null, 0, null);
            }
        }

        

        //////New Phone Web Service
         [WebMethod(EnableSession = true)]
        
        [EntityMethodAttribute(true, "id", ValidDefaultPermissions1 = Enums_S3.DefaultPermissions.Edit, ValidDefaultPermissions2 = Enums_S3.DefaultPermissions.Add)]
        public string Add(int id, int entityId, int entityValueId, string CountryCode, string AreaCode, string PhoneNumber, int Type, bool IsDefault, string Description)
        {
            return Save(true, id, entityId, entityValueId, CountryCode, AreaCode, PhoneNumber, Type, IsDefault, Description);
        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(true, "id", ValidDefaultPermissions1 = Enums_S3.DefaultPermissions.Edit, ValidDefaultPermissions2 = Enums_S3.DefaultPermissions.Add)]
        public string Edit(int id, int entityId, int entityValueId, string CountryCode, string AreaCode, string PhoneNumber, int Type, bool IsDefault, string Description)
        {
            return Save(false, id, entityId, entityValueId, CountryCode, AreaCode, PhoneNumber, Type, IsDefault, Description);
        }

        private string Save(bool add, int id, int entityId, int entityValueId, string CountryCode, string AreaCode, string PhoneNumber, int Type, bool IsDefault, string Description)
        {
            if (entityId == (int)Enums_S3.Entity.Person)
            {
                CE.PhoneEntityCriteria criteria = new CE.PhoneEntityCriteria()
                {
                    EntityId = entityId,

                    PhoneNumber = CountryCode.Trim() + AreaCode.Trim() + PhoneNumber.Trim(),
                    PhoneType = (int)Enums_S3.Phone.PhoneType.Mobile
                };

                List<CE.PhoneEntityLite> list = this.iPhoneEntityManager.FindAllLite(criteria);
                if (list != null && list.Count > 0 && add)
                {
                    return this.AttachStatusCode(null, 501, null);
                }

                if (!add && list != null && list.Count > 0)
                {
                    foreach (CE.PhoneEntityLite phoneObj in list)
                    {
                        if (phoneObj.EntityValueId != entityValueId && phoneObj.CountryCode.Trim() + phoneObj.AreaCode.Trim() + phoneObj.PhoneNumber.Trim() == CountryCode.Trim() + AreaCode.Trim() + PhoneNumber.Trim())
                        {
                            return this.AttachStatusCode(null, 501, null);
                            break;
                        }

                    }
                }
            }

            CE.PhoneEntity phone = null;
            if (add)
            {
                phone = new CE.PhoneEntity()
                {
                    CreatedBy = LoggedInUser.UserId,
                    EntityId = entityId,
                    EntityValueId = entityValueId,
                    PhoneType=Type,
                    IsDefault = IsDefault,

                    PhoneObj = new CE.Phone()
                    {
                        CreatedBy = LoggedInUser.UserId,
                        CountryCode = CountryCode,
                        AreaCode = AreaCode,
                        PhoneNumber = PhoneNumber,
                        Description = unEscape(Description),
                        Type = (Enums_S3.Phone.PhoneType)Type,
                    }
                };
            }
            else
            {
                phone = this.iPhoneEntityManager.FindById(id, null);
                this.LockEntity(Enums_S3.Entity.Phone, phone.PhoneId);
                phone.MarkModified();
                phone.UpdatedBy = LoggedInUser.UserId;
                phone.IsDefault = IsDefault;
                phone.PhoneType = Type;
                phone.PhoneObj.UpdatedBy = LoggedInUser.UserId;
                phone.PhoneObj.Description = unEscape(Description);
                phone.PhoneObj.AreaCode = AreaCode;
                phone.PhoneObj.CountryCode = CountryCode;
                phone.PhoneObj.PhoneNumber = PhoneNumber;
                phone.PhoneObj.Type = (Enums_S3.Phone.PhoneType)Type;
                phone.PhoneObj.MarkModified();
            }

            List<SF.FrameworkEntity.ValidationError> errors = this.iPhoneEntityManager.Validate(phone);
            if (errors != null && errors.Count > 0)
            {
                return this.AttachStatusCode(errors, 0, "validation");
            }

            try
            {
                List<CE.PhoneEntityLite> myPhone = this.iPhoneEntityManager.FindAllLite(new CE.PhoneEntityCriteria { PhoneType = phone.PhoneType, EntityId = phone.EntityId, EntityValueId = phone.EntityValueId, IsDefault = true });
                this.iPhoneEntityManager.Save(phone);
                if (!add)
                    this.UnLockEntity(Enums_S3.Entity.Phone, phone.PhoneId);

                if (myPhone != null && myPhone.Count > 0)
                    SetPhoneDefualt(phone.PhoneId, IsDefault);
                else
                    SetPhoneDefualt(phone.PhoneId, true);

                return this.AttachStatusCode(phone.PhoneEntityId, 1, null);
            }
            catch (Exception ex)
            {
                this.UnLockEntity(Enums_S3.Entity.Phone, phone.PhoneId);
                return this.AttachStatusCode(null, 1, HandleException(ex));
            }
        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
   
        public string FindAllLite(int entityId, int entityValueId)
        {
            CE.PhoneEntityCriteria criteria = new CE.PhoneEntityCriteria()
            {
                EntityId = entityId,
                EntityValueId = entityValueId
            };
            List<CE.PhoneEntityLite> list = this.iPhoneEntityManager.FindAllLite(criteria);

            if (list != null && list.Count > 0)
            {
                var phones = from CE.PhoneEntityLite phone in list
                             select new
                             {
                                 Id = phone.Id,
                                 PhoneID = phone.PhoneId,
                                 CountryCode = phone.CountryCode,
                                 AreaCode = phone.AreaCode,
                                 PhoneNumber = phone.PhoneNumber,
                                 FullPhoneNumber = phone.CountryCode + ' ' + phone.AreaCode + ' ' + phone.PhoneNumber,
                                 CreatedBy = phone.CreatedByName,
                                 UpdatedBy = phone.UpdatedByName,
                                 Crearted_Date = phone.CreatedDate,
                                 Updated_Date = phone.UpdatedDate,
                                 Isdefault = phone.IsDefault,
                                 Description = phone.Description,
                                 IsdefaultValue = (phone.IsDefault.HasValue) ? (phone.IsDefault.Value ? "True" : "False") : "False",
                                 PhoneTypeId = phone.PhoneTypeId,
                                 PhoneType = phone.PhoneType
                             };
                return this.AttachStatusCode(phones, 1, null);
            }
            else
            {
                return this.AttachStatusCode(null, 1, null);
            }
        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
      
        public string SetPhoneDefualt(int Id, bool IsDefault)
        {
            CE.PhoneEntity phoneEntity = iPhoneEntityManager.FindById(Id, null);
            phoneEntity.UpdatedBy = LoggedInUser.UserId;
            phoneEntity.MarkModified();
            phoneEntity.PhoneType = phoneEntity.PhoneType;
            phoneEntity.IsDefault = IsDefault;
            try
            {
                iPhoneEntityManager.Save(phoneEntity);
                this.UnLockEntity(Enums_S3.Entity.Phone, phoneEntity.PhoneId);
                phoneEntity = iPhoneEntityManager.FindById(Id, null); // new to get the last update date coz no output in update method
                return this.AttachStatusCode(phoneEntity.UpdatedDate, 0, null);
            }
            catch (Exception)
            {
                this.UnLockEntity(Enums_S3.Entity.Phone, phoneEntity.PhoneId);
                return this.AttachStatusCode(null, 0, null);
            }
        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    
        public string Delete(int id)
        {
            CE.PhoneEntity phoneEntity = iPhoneEntityManager.FindById(id, null);
            this.LockEntityForDelete(Enums_S3.Entity.Phone, phoneEntity.PhoneId);
            phoneEntity.MarkDeleted();
            iPhoneEntityManager.Save(phoneEntity);

            CE.PhoneEntityCriteria criteria = new CE.PhoneEntityCriteria()
            {
                EntityId = phoneEntity.EntityId,
                EntityValueId = phoneEntity.EntityValueId
            };
            List<CE.PhoneEntityLite> list = this.iPhoneEntityManager.FindAllLite(criteria);
            CE.PhoneEntityLite defaultPhone = new CE.PhoneEntityLite();
            if (list != null)
                defaultPhone = (from CE.PhoneEntityLite phone in list where phone.PhoneTypeId == phoneEntity.PhoneType && phone.IsDefault.Value select phone).FirstOrDefault();
           
            this.UnLockEntity(Enums_S3.Entity.Phone, phoneEntity.PhoneId);
            return this.AttachStatusCode(defaultPhone, 0, null);

        }

    }
}