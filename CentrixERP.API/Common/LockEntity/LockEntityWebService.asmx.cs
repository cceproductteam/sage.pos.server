﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using SagePOS.Server.Configuration;
using System.Web.Script.Services;
using SagePOS.Manger.Common.Business.Entity;
using CentrixERP.Common.API;
using SagePOS.Server.API.Common.BaseClasses;

namespace SagePOS.Server.API.Common.LockEntity
{
    /// <summary>
    /// Summary description for LockEntityWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [EntityAttribute(Enums_S3.Entity.LockEntity)]
    [System.Web.Script.Services.ScriptService]
    public class LockEntityWebService : WebServiceBaseClass
    {

        /// <summary>
        /// (without permissions)
        /// </summary>
        

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string Lock(int entityId, int? entityValueId)
        {
            EntityLockCriteria criteria = new EntityLockCriteria()
            {
                EntityId = entityId,
                EntityValueId = entityValueId,
                LockedBy = LoggedInUser.UserId
            };

            List<EntityLock> lockedList = this.iEntityLockManager.FindAll(criteria);
            if (lockedList != null && lockedList.Count > 0)
            {

                var lockEntity = new
                {
                    UserName = lockedList[0].UserName,
                    LockedDate = lockedList[0].CreatedDate
                };

                return this.AttachStatusCode(lockEntity, 0, null);
            }
            else
            {
                EntityLock Locked = new EntityLock()
                {
                    CreatedBy = LoggedInUser.UserId,
                    EntityId = entityId,
                    EntityValueId = entityValueId.Value
                };

                try
                {
                    this.iEntityLockManager.Save(Locked);
                    return this.AttachStatusCode(null, 1, null);
                }
                catch (SF.Framework.Exceptions.EntityLockedDeleted)
                {
                    EndResponse(Enums_S3.StatusCode.Codes.EntityLockedDeleted, ((int)Enums_S3.StatusCode.Codes.EntityLockedDeleted).ToString());
                    return null;
                }
                catch (SF.Framework.Exceptions.EntityLockedChanged ex)
                {
                    EndResponse(Enums_S3.StatusCode.Codes.EntityLockedChanged, ((int)Enums_S3.StatusCode.Codes.EntityLockedChanged).ToString());
                    return null;
                }
                catch (SF.Framework.Exceptions.RecordNotAffected ex)
                {
                    return this.AttachStatusCode(null, 1, ex.Message);
                }
                catch (Exception ex)
                {
                    return this.AttachStatusCode(null, 0, ex.Message);
                }
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string UnLock(int entityId, int? entityValueId)
        {
            try
            {
                this.iEntityLockManager.UnLock(entityId, entityValueId);
                return this.AttachStatusCode(true, 1, null);
            }
            catch (SF.Framework.Exceptions.RecordNotAffected)
            {
                return this.AttachStatusCode(null, 1, null);
            }
            catch (Exception ex)
            {
                return this.AttachStatusCode(null, 0, ex.Message);
            }
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string UnLockAll(int userId)
        {
            try
            {
                this.iEntityLockManager.UnLock(userId);
                return this.AttachStatusCode(true, 1, null);
            }
            catch (SF.Framework.Exceptions.RecordNotAffected)
            {
                return this.AttachStatusCode(null, 1, null);
            }
            catch (Exception ex)
            {
                return this.AttachStatusCode(null, 0, ex.Message);
            }
        }


        /// <summary>
        /// Below Methods For Entity Lock page (with permissions)
        /// </summary>
        


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindAllLite(string keyword, int page, int resultCount, string argsCriteria)
        {
            EntityLockCriteria criteria = new EntityLockCriteria();
            if (!SF.Framework.String.IsEmpty(argsCriteria))
                criteria = this.javaScriptSerializer.Deserialize<EntityLockCriteria>(argsCriteria);
            List<EntityLockLite> entityLockList = iEntityLockManager.FindAllLite(criteria);
            if (entityLockList != null)
            {
                var result = from EntityLockLite item in entityLockList
                             group item by new
                             {
                                 item.EntityId,
                                 item.EntityName,
                                 item.EntityViewURL,
                                 item.UIName

                             } into entityList
                             select new
                             {
                                 EntityId = entityList.Key.EntityId,
                                 EntityName = entityList.Key.EntityName,
                                 UIName = entityList.Key.UIName,
                                 EntityViewURL = entityList.Key.EntityViewURL,
                                 EntityLock = from EntityLockLite itemLock in entityLockList
                                              where itemLock.EntityId == entityList.Key.EntityId
                                              select new
                                              {
                                                  UserName = itemLock.UserName,
                                                  EntityValueId = itemLock.EntityValueId,
                                                  Date = itemLock.CreatedDate
                                              }
                             };
                return this.AttachStatusCode(result, 1, "");
            }
            return this.AttachStatusCode(null, 1, "");
        }

       
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string UnLockUserEntity(int entityId, int? entityValueId)
        {
            try
            {
                this.iEntityLockManager.UnLock(entityId, entityValueId);
                return this.AttachStatusCode(true, 1, null);
            }
            catch (SF.Framework.Exceptions.RecordNotAffected)
            {
                return this.AttachStatusCode(null, 1, null);
            }
            catch (Exception ex)
            {
                return this.AttachStatusCode(null, 0, ex.Message);
            }
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string UnLockAllEntities(int userId)
        {
            try
            {
                this.iEntityLockManager.UnLock(userId);
                return this.AttachStatusCode(true, 1, null);
            }
            catch (SF.Framework.Exceptions.RecordNotAffected)
            {
                return this.AttachStatusCode(null, 1, null);
            }
            catch (Exception ex)
            {
                return this.AttachStatusCode(null, 0, ex.Message);
            }
        }

       
    }
}
