﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using SF.Framework;
using System.Configuration;
using System.Reflection;
using System.IO;
using System.Threading;
using System.Web.Script.Services;
using System.Web.Services;
using System.Net;
using System.Text.RegularExpressions;
using System.Text;

using SagePOS.Server.Configuration;

using SagePOS.Server.Business.Entity;
using Centrix.UM.Business.Entity;
using Centrix.UM.Business.IManager;

using E = Centrix.Notifications.Business.Entity;
using SagePOS.Server.Business.Factory ;
using Centrix.Notifications.Business.IManager;
using CentrixERP.Common.Business.IManager;
using SagePOS.Manger.Common.Business.Entity;
using CentrixERP.Common.API;
using CentrixERP.AdministrativeSettings.Business.Entity;
using SagePOS.Common.Web;
using CentrixERP.Common.Business.Manager;

namespace SagePOS.Server.API.Common.BaseClasses
   
{
    public class WebServiceBaseClass : System.Web.Services.WebService
    {

        #region Declarations
        IOnScreenNotificationManager iOnScreenNotificationManager = null;
        INotificationQueueManager iNotificationQueueManager = null;
        IEmailConfigurationManager iEmailConfigurationManager = null;
        IRolePermissionManager iRolePermissionManager = null;
        ISystemEntityManager iSystemEntityManager = null;
        IUserManager iUserManager = null;
        ITeamManager iTeamManager = null;
        public IEntityLockManager iEntityLockManager = null;

       
        public User LoggedInUser { set; get; }
        public JavaScriptSerializer javaScriptSerializer;
        public List<RolePermissionLite> RolePermissionsList = null;
        public Centrix.Notifications.Business.Entity.EmailConfiguration emailConfiguration;

        public string MethodName { get; set; }
        public Enums_S3.Entity CurrentEntity { get; set; }
        public int UserId { get; set; }

        public string NoPermissionValue = null;
        //int SessionNotAuthorizedCode = 0;
        //int NotAuthorizedErrorCode = 0;
        public DateTime? EntityLastUpdatedDate;
        
        //IPhysicalCalenderHeaderManager iPhysicalCalendarHeaderManager = null;
     
        #endregion

        public WebServiceBaseClass()
        {
            javaScriptSerializer = new JavaScriptSerializer();


          SagePOS.Server.Configuration.Enums_S3.UserLoggin.UserLogginStatus userLogginStatus = ValidateRequest();
            //if (userLogginStatus != CentrixERP.Configuration.Enums_S3.UserLoggin.UserLogginStatus.LoggedIn)
            //{

            //    //SessionNotAuthorizedCode = ConfigurationManager.AppSettings["SessionNotAuthorizedCode"].ToNumber();
            //    EndResponse(Enums_S3.StatusCode.Codes.SessionNotAuthorizedCode, ((int)Enums_S3.StatusCode.Codes.SessionNotAuthorizedCode).ToString());

            //}

            //if (HttpContext.Current.Request.Cookies["Lang"] != null && HttpContext.Current.Request.Cookies["Lang"].Value == "ar")
            //{
            //    Lang = Enums_S3.Configuration.Language.ar;
            //}
            //else
            //{
            //    Lang = Enums_S3.Configuration.Language.en;
            //}
            SagePOS.Server.Configuration.Configuration.setLanguage(false);


            iRolePermissionManager = (IRolePermissionManager)IoC.Instance.Resolve(typeof(IRolePermissionManager));
            iUserManager = (IUserManager)IoC.Instance.Resolve(typeof(IUserManager));
            iTeamManager = (ITeamManager)IoC.Instance.Resolve(typeof(ITeamManager));
            iEntityLockManager = IoC.Instance.Resolve<IEntityLockManager>();
            iSystemEntityManager = IoC.Instance.Resolve<ISystemEntityManager>();
            //iPhysicalCalendarHeaderManager = IoC.Instance.Resolve<IPhysicalCalenderHeaderManager>();
           

            if (!string.IsNullOrEmpty(Context.Request.Headers["entityLastUpdatedDate"]))
            {
                try
                {
                    double d = Convert.ToDouble(Context.Request.Headers["entityLastUpdatedDate"]);
                    DateTime dt = new DateTime(1970, 1, 1);
                    this.EntityLastUpdatedDate = dt.AddSeconds(d / 1000);
                }
                catch (Exception)
                {

                }
            }


            LoadRoleEntityPermissions();
            MethodName = HttpContext.Current.Request.PathInfo.Remove(0, 1);
            NoPermissionValue = "has No access permission";

            if (!LoggedInUser.IsSupperUser && !CheckAuthentication())
            {
                //NotAuthorizedErrorCode = ConfigurationManager.AppSettings["NotAuthorizedErrorCode"].ToNumber();
                EndResponse(Enums_S3.StatusCode.Codes.NotAuthorizedErrorCode, ((int)Enums_S3.StatusCode.Codes.NotAuthorizedErrorCode).ToString());
                //EndResponse(NotAuthorizedErrorCode, NotAuthorizedErrorCode.ToString());
            }

            this.iOnScreenNotificationManager = IoC.Instance.Resolve<IOnScreenNotificationManager>();
            this.iNotificationQueueManager = IoC.Instance.Resolve<INotificationQueueManager>();
            iEmailConfigurationManager = IoC.Instance.Resolve<IEmailConfigurationManager>();

            emailConfiguration = iEmailConfigurationManager.FindInternalEmailConfig();

            EntityAttribute[] deleteEntityAttribute = (EntityAttribute[])this.GetType().GetCustomAttributes(typeof(EntityAttribute), false);
            if (MethodName == "Delete" && !string.IsNullOrEmpty(deleteEntityAttribute[0].Entity.ToString()))
            {
                if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Headers["request-data"]))
                {
                    Dictionary<string, object> ValueIDictionary = this.javaScriptSerializer.Deserialize<Dictionary<string, object>>((System.Web.HttpContext.Current.Request.Headers["request-data"]).ToString());
                    string DictValue = ValueIDictionary["id"].ToString();
                    int EntityValueId = DictValue.ToNumber();
                    int EntityId = (int)CurrentEntity;
                    CheckRelatedEntity(EntityId, EntityValueId,true);
                }
            }
        }

        public string unEscape(string value)
        {
            return Server.UrlDecode(value);
        }

        public string AttachStatusCode(object returnData, int messageCode, string message)
        {
            var Code = new
            {
                statusCode = new { Code = messageCode, message = message },
                result = returnData
            };
            return this.javaScriptSerializer.Serialize(Code);
        }

        public void EndResponse(int code, string msg)
        {
            var response = new
            {
                code = code,
                message = msg
            };
            throw new Exception(this.javaScriptSerializer.Serialize(response));
        }

        public void EndResponse(Enums_S3.StatusCode.Codes code, string msg)
        {
            var response = new
            {
                code = (int)code,
                message = msg
            };
            throw new Exception(this.javaScriptSerializer.Serialize(response));
        }

        public object GetPropertyValue(Type propertyType, object value)
        {
            if (value == null)
                return null;

            if (propertyType == typeof(double) || propertyType == typeof(double?))
            {
                value = Convert.ToDouble(value);
            }

            else if (propertyType == typeof(int) || propertyType == typeof(int?))
            {
                value = Convert.ToInt32(value);
            }
            else if (propertyType == typeof(string))
            {
                value = Convert.ToString(value);
            }
            else if (propertyType == typeof(bool) || propertyType == typeof(bool?))
            {
                value = Convert.ToBoolean(value);
            }
            else if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
            {
                value = Convert.ToDateTime(value);
            }

            return value;
        }

        public Object GetPropertyValue(PropertyInfo pro, Object value)
        {
            if (pro.PropertyType == typeof(string))
            {
                return value.ToString();
            }
            else if (pro.PropertyType == typeof(int))
            {
                return (Convert.ToInt32(value));
            }
            else if (pro.PropertyType == typeof(bool))
            {
                return Convert.ToBoolean(value);
            }
            else if (pro.PropertyType == typeof(DateTime?) || pro.PropertyType == typeof(DateTime))
            {
                return Convert.ToDateTime((string)value);
            }
            else if (pro.PropertyType == typeof(decimal))
            {
                return Convert.ToDecimal(value);
            }
            else if (pro.PropertyType == typeof(double))
            {
                return Convert.ToDouble(value);
            }
            return value;
        }

        public Object GetPropertyValue(FieldInfo field, Object value)
        {
            if (field.FieldType == typeof(string))
            {
                return value.ToString();
            }
            else if (field.FieldType == typeof(int))
            {
                return (Convert.ToInt32(value));
            }
            else if (field.FieldType == typeof(bool))
            {
                return Convert.ToBoolean(value);
            }
            else if (field.FieldType == typeof(DateTime?) || field.FieldType == typeof(DateTime))
            {
                return Convert.ToDateTime((string)value);
            }
            else if (field.FieldType == typeof(decimal))
            {
                return Convert.ToDecimal(value);
            }
            return value;
        }

        public string HandleException(Exception exception)
        {
            StringBuilder errorMessage = new StringBuilder();
            if (exception != null)
            {
                errorMessage.AppendLine(exception.Message);
                errorMessage.AppendLine(exception.StackTrace);
                errorMessage.AppendLine();

                Exception tempException = exception.InnerException;
                while (true)
                {
                    //Exception ex = exception.InnerException;
                    if (tempException == null)
                        break;

                    errorMessage.AppendLine(tempException.Message);
                    errorMessage.AppendLine(tempException.StackTrace);
                    errorMessage.AppendLine();

                    tempException = tempException.InnerException;

                }
                return errorMessage.ToString();
            }
            else
                return null;
        }

        public void GetObjectFromDictionary<TEntity>(TEntity entity, Dictionary<string, Object> entityInfo)
        {
            //TEntity entity = Activator.CreateInstance<TEntity>();

            foreach (KeyValuePair<string, object> item in entityInfo)
            {
                PropertyInfo pro = entity.GetType().GetProperty(item.Key);
                if (pro != null)
                {
                    if (item.Value != null)
                    {
                        Object value = GetPropertyValue(pro, item.Value);
                        pro.SetValue(entity, value, null);
                    }
                }
            }

            //return entity;
        }

        public object getErrorValidationObj(string msg, string control)
        {
            var error = new
            {
                Msg = msg,
                Control = control
            };
            return error;
        }

        public object getErrorValidationObj(string msg, string control,string resourcekey)
        {
            var error = new
            {
                Msg = msg,
                Control = control,
                Resourcekey = resourcekey
            };
            return error;
        }

        public object getErrorValidationObj(int line, string msg, bool forAll, string control)
        {
            var error = new
            {
                Line = line,
                Msg = msg,
                Control = control,
                ForAll = forAll
            };
            return error;
        }

        public object getErrorValidationObj(int line, string msg, bool forAll, string control, string resourcekey)
        {
            var error = new
            {
                Line = line,
                Msg = msg,
                Control = control,
                ForAll = forAll,
                Resourcekey = resourcekey
            };
            return error;
        }

        public object getErrorValidationObj(int line, string msg, bool forAll, string control, string resourcekey, params string[] errorValues)
        {
            var error = new
            {
                Line = line,
                Msg = msg,
                Control = control,
                ForAll = forAll,
                Resourcekey = resourcekey,
                ErrorValue = errorValues
            };
            return error;
        }

        public string getEntryStatusName(int entryEditStatus)
        {
            if (entryEditStatus == (int)AREnums.EntryEditStatus.Posted)
            {
                return "posted";
            }
            else if (entryEditStatus == (int)AREnums.EntryEditStatus.Deleted)
            {
                return "deleted";
            }
            else if (entryEditStatus == (int)AREnums.EntryEditStatus.ReadyToPost)
            {
                return "set to ready to post";
            }
            return "open";
        }

        public string getAPEntryStatusName(int entryEditStatus)
        {
            if (entryEditStatus == (int)APEnums.EntryEditStatus.Posted)
            {
                return "posted";
            }
            else if (entryEditStatus == (int)AREnums.EntryEditStatus.Deleted)
            {
                return "deleted";
            }
            else if (entryEditStatus == (int)AREnums.EntryEditStatus.ReadyToPost)
            {
                return "set to ready to post";
            }
            return "open";
        }

        public string ConvertNumbersToWords(double number, SagePOS.Server.Configuration.Enums_S3.Configuration.Language lang)
        {
            NumbersToWords obj = new NumbersToWords(Convert.ToDecimal(number), new CurrencyInfo(CurrencyInfo.Currencies.JOD));
            string result = "";
            if (lang == SagePOS.Server.Configuration.Enums_S3.Configuration.Language.ar)
                result = obj.ConvertToArabic();
            else result = obj.ConvertToEnglish();

            
            return result;
        }
        
        //check if year/period are not loicked
        public DataTypeContectEnumerations.BatchPostingErrors CheckIfValidYearPeriodValue(int year, int month)
        {
            //ifiscalyearmanager.CalendarYearPeriodCheck
            //YearPeriodCheckResult yearPeriodCheckResult = iPhysicalCalendarHeaderManager.CalendarYearPeriodCheck(year, month);

            //if (yearPeriodCheckResult.IsLocked)
            //    return DataTypeContectEnumerations.BatchPostingErrors.EntryInLockedPeriod;
            //if (!yearPeriodCheckResult.BeforOldestYear)
            //    return DataTypeContectEnumerations.BatchPostingErrors.EntryBeforOldesetYear;
            //if (yearPeriodCheckResult.FutureYear)
            //    return DataTypeContectEnumerations.BatchPostingErrors.EntryInFutureYear;
            //if (!yearPeriodCheckResult.InCurrentYear)
            //    return DataTypeContectEnumerations.BatchPostingErrors.AllowPostingToPreviousYear;
            //if (yearPeriodCheckResult.HasNoPeriod)
            //    return DataTypeContectEnumerations.BatchPostingErrors.EntryHasNoPeriod;

            return DataTypeContectEnumerations.BatchPostingErrors.NoError;
        }

        public DataTypeContectEnumerations.BatchTransactionsErrors CheckIfValidYearPeriodValueTransactionYear(int year, int month)
        {
            //ifiscalyearmanager.CalendarYearPeriodCheck
            //YearPeriodCheckResult yearPeriodCheckResult = iPhysicalCalendarHeaderManager.CalendarYearPeriodCheck(year, month);

            //if (yearPeriodCheckResult.IsLocked)
            //    return DataTypeContectEnumerations.BatchTransactionsErrors.EntryInLockedPeriod;
            //if (!yearPeriodCheckResult.BeforOldestYear)
            //    return DataTypeContectEnumerations.BatchTransactionsErrors.EntryBeforOldesetYear;
            //if (yearPeriodCheckResult.FutureYear)
            //    return DataTypeContectEnumerations.BatchTransactionsErrors.EntryInFutureYear;
            //if (!yearPeriodCheckResult.InCurrentYear)
            //    return DataTypeContectEnumerations.BatchTransactionsErrors.AllowPostingToPreviousYear;
            //if (yearPeriodCheckResult.HasNoPeriod)
            //    return DataTypeContectEnumerations.BatchTransactionsErrors.EntryHasNoPeriod;

            return DataTypeContectEnumerations.BatchTransactionsErrors.NoError;
        }

        #region Authentication
        public void LoadRoleEntityPermissions()
        {
            EntityAttribute[] entityAttribute = (EntityAttribute[])this.GetType().GetCustomAttributes(typeof(EntityAttribute), false);
            if (entityAttribute != null && entityAttribute.Count() > 0)
            {
                if (entityAttribute[0] != null)
                {
                    CurrentEntity = entityAttribute[0].Entity;
                    GetUserRolePermission(LoggedInUser.RoleId, (int)CurrentEntity, -1);
                }
            }
        }

        public bool CheckAuthentication()
        {
          //  return true;
            if (HttpContext.Current.Request.Headers["cx-autocomplete"] == "y")
                return true;

            MethodInfo methodInfo = this.GetType().GetMethod(MethodName);
            EntityMethodAttribute[] methodAttribute = (EntityMethodAttribute[])methodInfo.GetCustomAttributes(typeof(EntityMethodAttribute), true);

            if (methodAttribute.Length > 0)
            {
                EntityMethodAttribute attribute = methodAttribute[0];
                int permissionId = attribute.getPermissionId();

                if ((Enums_S3.DefaultPermissions)permissionId == Enums_S3.DefaultPermissions.None)
                    return true;

                if (RolePermissionsList != null && RolePermissionsList.Count() > 0)
                {
                    var existsPermission = from RolePermissionLite rolePer in RolePermissionsList
                                           where rolePer.PermissionId == permissionId && rolePer.HasPermission == true
                                           select rolePer;
                    return existsPermission.Count() > 0;
                }
                else
                    return false;
            }
            else
                return false;

            

        }

        public void GetUserRolePermission(int roleId, int entity, int ParentId)
        {
            RolePermissionCriteria criteria = new RolePermissionCriteria() { EntityId = entity, ParentId = ParentId };
            RolePermissionsList = iRolePermissionManager.FindAllRolePermission(roleId, criteria);
        }

        public string GetUserUnAvailablePermissions(int EntityId, int ParentId)
        {

            if (LoggedInUser.RoleId == 1)

            {
                return this.AttachStatusCode(null, 1, "all permission are available");
            
            }
            GetUserRolePermission(LoggedInUser.RoleId, EntityId, ParentId);
            List<RolePermissionLite> UnAvailablePermissionsList = null;

            if (RolePermissionsList != null)
            {
                UnAvailablePermissionsList = new List<RolePermissionLite>();
                foreach (RolePermissionLite item in RolePermissionsList)
                {
                    if (!item.HasPermission)
                    {
                        UnAvailablePermissionsList.Add(item);
                    }
                }

                if (UnAvailablePermissionsList != null && UnAvailablePermissionsList.Count > 0)
                    return this.AttachStatusCode(UnAvailablePermissionsList, 1, "unavailable permissions List");
                else
                    return this.AttachStatusCode(null, 1, "all permission are available");
            }
            else
                return this.AttachStatusCode(null, 0, "entity doesnt has permissions"); // means that the entity doesnt has permissions at all
        }

        private SagePOS.Server.Configuration.Enums_S3.UserLoggin.UserLogginStatus ValidateRequest()
        {
            IUserLogginManager iUserLogginManager = IoC.Instance.Resolve<IUserLogginManager>();
            iUserLogginManager.ForceCreateNewSession = false;
            UserLogginCriteria criteria = new UserLogginCriteria()
            {
                UserAgent = HttpContext.Current.Request.UserAgent,
                LocalIP = HttpContext.Current.Request.UserHostAddress,
                PublicIP = HttpContext.Current.Request.UserHostName,
                UserId = UserId
            };

            SagePOS.Server.Configuration.Enums_S3.UserLoggin.UserLogginStatus userLogginStatus = iUserLogginManager.ValidateLogginSession(criteria);


            this.LoggedInUser = iUserLogginManager.CurrentLoggedUser;
            this.UserId = iUserLogginManager.CurrentLoggedUser.UserId;
            //return true;


            return userLogginStatus;
        }

        public bool CheckUserLogin(string password)
        {
          // User userLogin = iUserManager.Login(userName, password);
            return iUserManager.CheckUserPassword(LoggedInUser.UserId,password);
        }
        #endregion

        #region Lock
        public void LockEntity(Enums_S3.Entity entity, int? entityValueId)
        {
            EntityLockCriteria criteria = new EntityLockCriteria()
            {
                EntityId = (int)entity,
                EntityValueId = entityValueId,
                LockedBy = LoggedInUser.UserId
            };


            List<EntityLock> lockedList = this.iEntityLockManager.FindAll(criteria);
            if (lockedList != null && lockedList.Count > 0)
            {

                var lockEntity = new
                {
                    Message = 402,
                    UserName = lockedList[0].UserName,
                    LockedDate = lockedList[0].CreatedDate
                };
                EndResponse(402, this.javaScriptSerializer.Serialize(lockEntity));
                //return this.AttachStatusCode(lockEntity, 0, null);
            }
            else
            {
                EntityLock Locked = new EntityLock()
                {
                    CreatedBy = LoggedInUser.UserId,
                    EntityId = (int)entity,
                    EntityValueId = entityValueId.HasValue ? entityValueId.Value : -1
                };
                if (this.EntityLastUpdatedDate != null)
                {
                    Locked.EntityLastUpdatedDate = this.EntityLastUpdatedDate;
                    Locked.EntityLastUpdatedDate = Locked.EntityLastUpdatedDate.Value.AddMilliseconds(-Locked.EntityLastUpdatedDate.Value.Millisecond);
                }


                try
                {
                    this.iEntityLockManager.Save(Locked);
                }
                catch (SF.Framework.Exceptions.EntityLockedDeleted)
                {
                    EndResponse(Enums_S3.StatusCode.Codes.EntityLockedDeleted, ((int)Enums_S3.StatusCode.Codes.EntityLockedDeleted).ToString());
                }
                catch (SF.Framework.Exceptions.EntityLockedChanged ex)
                {
                    EndResponse(Enums_S3.StatusCode.Codes.EntityLockedChanged, ((int)Enums_S3.StatusCode.Codes.EntityLockedChanged).ToString());
                }
                catch (SF.Framework.Exceptions.RecordNotAffected)
                { }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public void LockEntityForDelete(Enums_S3.Entity entity, int? entityValueId)
        {

            EntityLockCriteria criteria = new EntityLockCriteria()
            {
                EntityId = (int)entity,
                EntityValueId = entityValueId,
                LockedBy = LoggedInUser.UserId,

            };

            List<EntityLock> lockedList = this.iEntityLockManager.FindAll(criteria);
            if (lockedList != null && lockedList.Count > 0)
            {

                var lockEntity = new
                {
                    UserName = lockedList[0].UserName,
                    LockedDate = lockedList[0].CreatedDate
                };

                EndResponse(Enums_S3.StatusCode.Codes.EntityLocked, this.javaScriptSerializer.Serialize(lockEntity));
            }
            else
            {
                EntityLock Locked = new EntityLock()
                {
                    CreatedBy = LoggedInUser.UserId,
                    EntityId = (int)entity,
                    EntityValueId = entityValueId.Value,
                    LockForDelete = true
                };

                try
                {
                    this.iEntityLockManager.Save(Locked);
                    //return this.AttachStatusCode(null, 1, null);
                }
                catch (SF.Framework.Exceptions.RecordNotAffected ex)
                {
                    //return this.AttachStatusCode(null, 1, ex.Message);
                }
                catch (SF.Framework.Exceptions.EntityLockedDeleted ex)
                {
                    EndResponse(Enums_S3.StatusCode.Codes.EntityLockedDeleted, ((int)Enums_S3.StatusCode.Codes.EntityLockedDeleted).ToString());
                    //return null;
                }
                catch (SF.Framework.Exceptions.EntityLockedChanged ex)
                {
                    EndResponse(Enums_S3.StatusCode.Codes.EntityLockedChanged, ((int)Enums_S3.StatusCode.Codes.EntityLockedChanged).ToString());
                }
                catch (SF.Framework.Exceptions.EntityLocked ex)
                {
                    EndResponse(Enums_S3.StatusCode.Codes.EntityLocked, ((int)Enums_S3.StatusCode.Codes.EntityLocked).ToString());
                }
                catch (Exception ex)
                {
                    throw ex;
                    //return this.AttachStatusCode(null, 0, ex.Message);
                }
            }
        }

        public void LockEntity(Enums_S3.Entity entity, int? entityValueId, string timeStamp)
        {
            EntityLockCriteria criteria = new EntityLockCriteria()
            {
                EntityId = (int)entity,
                EntityValueId = entityValueId,
                LockedBy = LoggedInUser.UserId
            };

            List<EntityLock> lockedList = this.iEntityLockManager.FindAll(criteria);
            if (lockedList != null && lockedList.Count > 0)
            {

                var lockEntity = new
                {
                    Message = 402,
                    UserName = lockedList[0].UserName,
                    LockedDate = lockedList[0].CreatedDate
                };
                EndResponse(402, this.javaScriptSerializer.Serialize(lockEntity));
                //return this.AttachStatusCode(lockEntity, 0, null);
            }
            else
            {
                EntityLock Locked = new EntityLock()
                {
                    CreatedBy = LoggedInUser.UserId,
                    EntityId = (int)entity,
                    EntityValueId = entityValueId.HasValue ? entityValueId.Value : -1
                };


                try
                {
                    this.iEntityLockManager.Save(Locked);
                }
                catch (SF.Framework.Exceptions.EntityLockedDeleted)
                {
                    EndResponse(Enums_S3.StatusCode.Codes.EntityLockedDeleted, ((int)Enums_S3.StatusCode.Codes.EntityLockedDeleted).ToString());
                }
                catch (SF.Framework.Exceptions.EntityLockedChanged ex)
                {
                    EndResponse(Enums_S3.StatusCode.Codes.EntityLockedChanged, ((int)Enums_S3.StatusCode.Codes.EntityLockedChanged).ToString());
                }
                catch (SF.Framework.Exceptions.RecordNotAffected)
                { }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public void UnLockEntity(Enums_S3.Entity entity, int? entityValueId)
        {
            try
            {
                this.iEntityLockManager.UnLock((int)entity, entityValueId);
            }
            catch (SF.Framework.Exceptions.RecordNotAffected)
            { }
            catch (Exception)
            {
                //throw;
            }
        }
        #endregion

        #region Notifiactions


        public void CheckRelatedEntity(int EntityId, int EntityValueId,bool isDelete)
        {
            List<EntityRelations> EntityRelationsList = this.iSystemEntityManager.CheckEntityRelations(EntityId, EntityValueId, null);
            if (EntityRelationsList != null && EntityRelationsList.Count() > 0)
                EndResponse(EntityRelationsList, (isDelete) ? (int)Enums_S3.StatusCode.Codes.FailedToDelete : (int)Enums_S3.StatusCode.Codes.FailedToInactiveItem, null);

        }

        public void CheckRelatedEntity(int EntityId, int EntityValueId, Enums_S3.StatusCode.Codes StatusCode)
        {
            //int x = (int)Enums_S3.StatusCode.Codes.FailedToDelete;

            List<EntityRelations> EntityRelationsList = iSystemEntityManager.CheckEntityRelations(EntityId, EntityValueId, null);
            if (EntityRelationsList != null && EntityRelationsList.Count() > 0)
                EndResponse(EntityRelationsList, (int)StatusCode, null);
        }

        public void EndResponse(object returnData, int messageCode, string message)
        {
            var response = new
            {
                code = messageCode,
                message = message,
                result = returnData
            };
            throw new Exception(this.javaScriptSerializer.Serialize(response));
        }

        //public bool AddNotification(E.OnScreenNotification notification)
        //{
        //    try
        //    {
        //        //E.OnScreenNotification queue = this.javaScriptSerializer.Deserialize<E.OnScreenNotification>(this.unEscape(notification));
        //        notification.CreatedBy = this.UserId;
        //        this.iOnScreenNotificationManager.Save(notification);
        //        return true;
        //        //return this.AttachStatusCode(notification.OnScreenNotificationId, 1, null);
        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //        //return this.AttachStatusCode(null, 1, null);
        //    }
        //}

        //public bool AddNotification(string subject, string text, DateTime date, Enums_S3.Entity entity, int entityValueId, string QueryString)
        //{

        //    E.OnScreenNotification onscreen = new E.OnScreenNotification()
        //    {
        //        Subject = subject,
        //        Test = text,
        //        NotificationDate = date,
        //        EntityId = (int)entity,
        //        //EntityValueId = 
        //        CreatedBy = LoggedInUser.UserId,
        //        UserId = entityValueId,
        //        Status = (int)Enums_S3.Notification.NotificationStatus.Pending,
        //        QueryString = QueryString
        //    };

        //    try
        //    {
        //        this.iOnScreenNotificationManager.Save(onscreen);
        //        return true;
        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //    }
        //}

        //public bool SendEmail(E.NotificationQueue notification)
        //{
        //    try
        //    {
        //        //E.NotificationQueue queue = this.javaScriptSerializer.Deserialize<E.NotificationQueue>(this.unEscape(notification));
        //        notification.CreatedBy = this.UserId;
        //        notification.Type = (int)Enums_S3.Notification.NotificationType.Email;
        //        notification.EmailConfigId = emailConfiguration.EmailConfigId;
        //        this.iNotificationQueueManager.Save(notification);
        //        return true;
        //        //return this.AttachStatusCode(notification.NotificationQueueId, 1, null);
        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //        //return this.AttachStatusCode(null, 1, null);
        //    }
        //}

        //public bool SendEmail(string subject, string text, string to, int priority, Enums_S3.Notification.NotificationStatus status, int createdBy, DateTime date, Enums_S3.Notification.NotificationType type)
        //{
        //    try
        //    {
        //        E.NotificationQueue noti = new E.NotificationQueue()
        //        {
        //            CreatedBy = createdBy,
        //            NotificationDate = date,
        //            Priority = priority,
        //            Status = (int)status,
        //            Subject = subject,
        //            Text = text,
        //            To = to,
        //            Type = (int)type,
        //            EmailConfigId = emailConfiguration.EmailConfigId
        //        };

        //        this.SendEmail(noti);
        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //    }
        //    return true;
        //}

        //public bool SendSms(E.NotificationQueue notification)
        //{
        //    try
        //    {
        //        //E.NotificationQueue queue = this.javaScriptSerializer.Deserialize<E.NotificationQueue>(this.unEscape(notification));
        //        notification.CreatedBy = this.UserId;
        //        notification.Type = (int)Enums_S3.Notification.NotificationType.SMS;
        //        this.iNotificationQueueManager.Save(notification);
        //        return true;
        //        //return this.AttachStatusCode(notification.NotificationQueueId, 1, null);
        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //        //return this.AttachStatusCode(null, 1, null);
        //    }
        //}

        //public E.Template GetTemplate(int template_number)
        //{
        //    ITemplateManager templateManager = (ITemplateManager)IoC.Instance.Resolve(typeof(ITemplateManager));
        //    E.Template template = templateManager.FindByTemplateNumber(template_number, null);
        //    // HandleTemplatedelimiter(template);
        //    return template;
        //}

        //public E.Template HandleTemplatedelimiter(E.Template template, object Entity)
        //{
        //    E.Template myTemplate = FillTemplateMergeFields(template, Entity);
        //    return myTemplate;
        //}

        //public string FillTemplateMergeFields(string templateCode, object Entity)
        //{
        //    Regex regex = new Regex("%%(.*?)%%");
        //    var matchVariable = regex.Match(templateCode);
        //    string newTemplateCode = templateCode.Replace(matchVariable.ToString(), " " + GetObjectPropertyValue(Entity, matchVariable.ToString().Replace("%", "")) + " ");

        //    string s = matchVariable.Groups[1].ToString();

        //    var newMatchs = regex.Match(newTemplateCode);
        //    if (!string.IsNullOrEmpty(newMatchs.ToString()))
        //        return FillTemplateMergeFields(newTemplateCode, Entity);
        //    else
        //        return newTemplateCode;
        //}

        //public E.Template FillTemplateMergeFields(E.Template template, object Entity)
        //{
        //    string templateCode = template.TemplateCode;
        //    Regex regex = new Regex("%%(.*?)%%");
        //    var matchVariable = regex.Match(templateCode);
        //    string newTemplateCode = templateCode.Replace(matchVariable.ToString(), " " + GetObjectPropertyValue(Entity, matchVariable.ToString().Replace("%", "")) + " ");
        //    E.Template newTemplate = template;
        //    newTemplate.TemplateCode = newTemplateCode;
        //    string s = matchVariable.Groups[1].ToString();

        //    var newMatchs = regex.Match(newTemplate.TemplateCode);
        //    if (!string.IsNullOrEmpty(newMatchs.ToString()))
        //        return FillTemplateMergeFields(newTemplate, Entity);
        //    else
        //        return newTemplate;
        //}

        //private string GetObjectPropertyValue(object obj, string propertyName)
        //{
        //    Type type = obj.GetType();
        //    PropertyInfo info = type.GetProperty(propertyName.Trim());
        //    object value = info.GetValue(obj, null);
        //    return value != null ? value.ToString() : "";
        //}

        //public string GetUserEmail(int userId)
        //{
        //    UserLite liteUser = this.iUserManager.FindByIdLite(userId, null);
        //    if (liteUser != null)
        //        return liteUser.UserEmail;
        //    else
        //        return null;
        //}

        //public string GetLoggedInUserEmail()
        //{
        //    UserLite liteUser = this.iUserManager.FindByIdLite(LoggedInUser.UserId, null);
        //    if (liteUser != null)
        //        return liteUser.UserEmail;
        //    else
        //        return null;
        //}

        //public string GetTeamEmail(int teamId)
        //{
        //    return null;
        //}

        #endregion

        #region OptionalField
        //IOptionalFieldEntityManager iOptionalFieldEntityManager = IoC.Instance.Resolve<IOptionalFieldEntityManager>();
        //public void OptionalFieldSave(List<OptionalFieldEntity> optionalFieldList, int entityValueId)
        //{

        //    foreach (OptionalFieldEntity item in optionalFieldList)
        //    {
        //        item.EntityValueId = entityValueId;
        //        iOptionalFieldEntityManager.Save(item);

        //    }
        //}

        //Save Optional Field In details Line 
        //public List<Object> OptionalFieldSave(List<OptionalFieldEntity> optionalFieldList, int entityValueId, int entityId,int? detailsType)
        //{
        //  //  List<OptionalFieldEntity> oldOptionalFieldList = iOptionalFieldEntityManager.FindAll(new OptionalFieldEntityCriteria() { EntityValueId = entityValueId, EntityId = entityId });

        //    //if (oldOptionalFieldList == null)
        //    //    oldOptionalFieldList = new List<OptionalFieldEntity>();
        //    List<Object> Errors = new List<object>();

        //    DeleteOptionalFieldByEntity(entityId, entityValueId, detailsType);
        //    try
        //    {
                

        //        foreach (OptionalFieldEntity item in optionalFieldList)
        //        {
        //            if (item.OptionalFieldEntityId <= 0)
        //            {

        //                OptionalFieldEntity OptionalFieldEntity = new OptionalFieldEntity
        //                {
        //                    OptionalFieldId = item.OptionalFieldId,
        //                    OptionalFieldDetailsId = item.OptionalFieldDetailsId,
        //                    EntityId = entityId,
        //                    EntityValueId = entityValueId,
        //                    DetailsType=(detailsType.HasValue)?detailsType.Value:-1,
        //                    CreatedBy = UserId
        //                };

        //                iOptionalFieldEntityManager.Save(OptionalFieldEntity);
        //            }
        //        }

               //IEnumerable<int> newIds = (from OptionalFieldEntity item in optionalFieldList
               //                            select item.OptionalFieldEntityId);
               // IEnumerable<int> oldIds = (from OptionalFieldEntity item in oldOptionalFieldList
               //                            select item.OptionalFieldEntityId);
               // IEnumerable<int> ToAdd = null;
               // IEnumerable<int> ToUpdate = null;
               // IEnumerable<int> ToDelete = null;
               // if (oldIds.Count() == 0)
               //     ToAdd = newIds;
               // else
               // {
               //     ToAdd = newIds.Except(oldIds);
               //     ToUpdate = newIds.Intersect(oldIds);
               //     ToDelete = oldIds.Except(newIds);
               // }
               // if (ToDelete != null)
               // {
               //     if (ToDelete.Count() > 0)
               //     {
               //         foreach (int opid in ToDelete)
               //         {
               //             foreach (OptionalFieldEntity item in oldOptionalFieldList)
               //             {
               //                 if (item.OptionalFieldEntityId == opid)
               //                 {
               //                     item.MarkDeleted();
               //                     iOptionalFieldEntityManager.Save(item);
               //                     break;
               //                 }
               //             }
               //         }
               //     }
               // }

               // if (ToUpdate != null)
               // {
               //     if (ToUpdate.Count() > 0)
               //     {
               //         foreach (int opid in ToUpdate)
               //         {
               //             var oldTarget = (from OptionalFieldEntity item in oldOptionalFieldList
               //                              where item.OptionalFieldEntityId == opid
               //                              select item).FirstOrDefault();

               //             var toCopyFromTarget = (from OptionalFieldEntity item in optionalFieldList
               //                                     where item.OptionalFieldEntityId == opid
               //                                     select item).FirstOrDefault();

               //             if (oldTarget != null && toCopyFromTarget != null)
               //             {
               //                 oldTarget.OptionalFieldId = toCopyFromTarget.OptionalFieldId;
               //                 oldTarget.OptionalFieldDetailsId = toCopyFromTarget.OptionalFieldDetailsId;
               //                 oldTarget.UpdatedBy = UserId;
               //                 oldTarget.MarkModified();
               //                 iOptionalFieldEntityManager.Save(oldTarget);

               //             }
               //         }
               //     }
               // }
               // if (ToAdd.Count() > 0)
               // {
               //     foreach (OptionalFieldEntity item in optionalFieldList)
               //     {
               //         if (item.OptionalFieldEntityId <= 0)
               //         {

               //             OptionalFieldEntity OptionalFieldEntity = new OptionalFieldEntity
               //             {
               //                 OptionalFieldId = item.OptionalFieldId,
               //                 OptionalFieldDetailsId = item.OptionalFieldDetailsId,
               //                 EntityId = entityId,
               //                 EntityValueId = entityValueId,
               //                 CreatedBy = UserId
               //             };

               //             iOptionalFieldEntityManager.Save(OptionalFieldEntity);
               //         }
               //     }
               // }
        //    }
        //    catch (Exception ex)
        //    {
        //        Errors.Add(HandleException(ex));
        //        return Errors;
        //    }

        //    return new List<object>();
        //}

        //public List<Object> ValidateOptionalField(List<OptionalFieldEntity> optionalFieldList)
        //{

        //    List<Object> errors = new List<object>();
        //    List<int> validatedList = new List<int>();

        //    if (optionalFieldList != null)
        //        foreach (OptionalFieldEntity item in optionalFieldList)
        //        {
        //            if (item.OptionalFieldId <= 0)
        //                errors.Add(getErrorValidationObj(item.LineNumber, "required", false, "OptionalField", "required"));
        //            if (item.OptionalFieldDetailsId <= 0)
        //                errors.Add(getErrorValidationObj(item.LineNumber, "required", false, "OptionalFieldDetails", "required"));

        //            int optionalFieldExist = (from validitem in validatedList where validitem == item.OptionalFieldId select validitem).FirstOrDefault();
        //            if (optionalFieldExist > 0)
        //                errors.Add(getErrorValidationObj(item.LineNumber, "", false, "OptionalField", "AlreadyExists", item.OptionalFieldId.ToString()));
        //            else
        //                validatedList.Add(item.OptionalFieldId);
        //        }
        //    return errors;
        //}

        //public List<OptionalFieldEntityLite> OptionalFieldFindAllLite(int entityId,int entityValueId,int? detailsType) {
        //    List<OptionalFieldEntityLite> optionalFieldEntityList = iOptionalFieldEntityManager.FindAllLite(new OptionalFieldEntityCriteria()
        //    {
        //        EntityId = entityId,
        //        EntityValueId = entityValueId,
        //        DetailsType = (detailsType.HasValue) ? detailsType.Value : -1
        //    });

        //    return optionalFieldEntityList;
        //}

        //public List<OptionalFieldEntityLite> OptionalFieldFindAllLite(int entityId, string entityValueIds)
        //{
        //    List<OptionalFieldEntityLite> optionalFieldEntityList = iOptionalFieldEntityManager.FindAllLite(new OptionalFieldEntityCriteria()
        //    {
        //        EntityId = entityId,
        //        EntityValueIds = entityValueIds
        //    });

        //    return optionalFieldEntityList;
        //}
      

        //public void DeleteOptionalFieldByEntity(int entityId, int entityValueId,int? detailsType)
        //{
        //    try { iOptionalFieldEntityManager.DeleteByEntity(entityId, entityValueId, detailsType); }
        //    catch (Exception ex) { }
        //}
      
         
        #endregion
    }


    public static class ValueTypeHelper
    {
        public static bool IsNullable<T>(T t) { return false; }
        public static bool IsNullable<T>(T? t) where T : struct { return true; }
        public static TEntity ResetNullableProperties<TEntity>(TEntity myEntity)
        {
            foreach (var prop in myEntity.GetType().GetProperties())
            {
                var propType = prop.PropertyType;
                var nullableProperty = IsNullable(prop); //ValueTypeHelper.IsNullable(prop.GetType());
                var propValue = prop.GetValue(myEntity, null);

                if (propType == typeof(Int32?) && nullableProperty)
                {
                    if ((int)propValue < 0)
                    { prop.SetValue(myEntity, null, null); }
                }
            }
            return myEntity;
        }
    }   

  

   

}