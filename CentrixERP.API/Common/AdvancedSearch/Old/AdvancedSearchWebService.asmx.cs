﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using CentrixERP.API.Common.BaseClasses;
using CentrixERP.Common.Business.IManager;
using SF.Framework;
using System.Web.Script.Services;
using CentrixERP.Common.API;
using CentrixERP.Common.Business.Entity;
using CentrixERP.Common.Business.Factory;
using CentrixERP.Configuration;
using SF.FrameworkEntity;
using CentrixERP.Common.Business.Manager;
using System.Reflection;

namespace CentrixERP.API.Common.AdvancedSearch
{
    /// <summary>
    /// Summary description for AdvancedSearchWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class AdvancedSearchWebService : WebServiceBaseClass
    {
        IEntityControlConfigManager iEntityControlConfigManager = null;
        IEntityControlConfigEventsManager iEntityControlConfigEventsManager = null;

        public AdvancedSearchWebService()
        {
            iEntityControlConfigManager = (IEntityControlConfigManager)IoC.Instance.Resolve(typeof(IEntityControlConfigManager));
            iEntityControlConfigEventsManager = (IEntityControlConfigEventsManager)IoC.Instance.Resolve(typeof(IEntityControlConfigEventsManager));
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string LoadSearchForm(string mKey, bool AdvancedSearchType)
        {
            AdvancedSearchFactory factory = new AdvancedSearchFactory(mKey);
            // factory.EntityId

            EntityControlConfigCriteria ConfigCriteria = new EntityControlConfigCriteria();
            ConfigCriteria.EntityId = factory.EntityId;
            ConfigCriteria.AdvancedSearchType = AdvancedSearchType;

            List<EntityControlConfig_Lite> ControlsList = iEntityControlConfigManager.FindAllLite(ConfigCriteria);

            EntityControlConfigEventsCriteria ConfigEventsCriteria = new EntityControlConfigEventsCriteria();
            ConfigEventsCriteria.EntityId = factory.EntityId;

            List<EntityControlConfigEvents_Lite> EntityEventsList = iEntityControlConfigEventsManager.FindAllLite(ConfigEventsCriteria);

            if (ControlsList != null && ControlsList.Count > 0)
            {
                var List = from EntityControlConfig_Lite lite_control in ControlsList
                           select new
                           {
                               Control = lite_control,
                               ControlEvents = (EntityEventsList != null && EntityEventsList.Count > 0) ? (from EntityControlConfigEvents_Lite item in EntityEventsList
                                                                                                           where item.EntityControlConfigId == lite_control.EntityControlConfigId
                                                                                                           select item) : new List<EntityControlConfigEvents_Lite>()
                           };



                return this.AttachStatusCode(List, 1, null);


            }

            else
                return this.AttachStatusCode(null, 0, null);

        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindAllControls(string mKey, bool AdvancedSearchType)
        {
            AdvancedSearchFactory factory = new AdvancedSearchFactory(mKey);

            EntityControlConfigCriteria ConfigCriteria = new EntityControlConfigCriteria();
            ConfigCriteria.EntityId = factory.EntityId;
            ConfigCriteria.AdvancedSearchType = AdvancedSearchType;

            List<EntityControlConfig_Lite> ControlsList = iEntityControlConfigManager.FindAllLite(ConfigCriteria);
            if (ControlsList != null && ControlsList.Count() > 0)

                return this.AttachStatusCode(ControlsList, 1, null);
            else
                return this.AttachStatusCode(null, 0, null);

        }


        //Added By Ruba Al-Sa'di 2/10/2014
        //Loading Search form Based on the CX.Framework
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string LoadSearchForm2(string mKey, int entityId, bool AdvancedSearchType)
        {
            CX.Framework.AdvancedSearchFactory factory = new CX.Framework.AdvancedSearchFactory(mKey);
            factory.EntityId = entityId;

            EntityControlConfigCriteria ConfigCriteria = new EntityControlConfigCriteria();
            ConfigCriteria.EntityId = factory.EntityId;
            ConfigCriteria.AdvancedSearchType = AdvancedSearchType;

            List<EntityControlConfig_Lite> ControlsList = iEntityControlConfigManager.FindAllLite(ConfigCriteria);

            EntityControlConfigEventsCriteria ConfigEventsCriteria = new EntityControlConfigEventsCriteria();
            ConfigEventsCriteria.EntityId = factory.EntityId;

            List<EntityControlConfigEvents_Lite> EntityEventsList = iEntityControlConfigEventsManager.FindAllLite(ConfigEventsCriteria);

            if (ControlsList != null && ControlsList.Count > 0)
            {
                var List = from EntityControlConfig_Lite lite_control in ControlsList
                           select new
                           {
                               Control = lite_control,
                               ControlEvents = (EntityEventsList != null && EntityEventsList.Count > 0) ? (from EntityControlConfigEvents_Lite item in EntityEventsList
                                                                                                           where item.EntityControlConfigId == lite_control.EntityControlConfigId
                                                                                                           select item) : new List<EntityControlConfigEvents_Lite>()
                           };
                return this.AttachStatusCode(List, 1, null);
            }

            else
                return this.AttachStatusCode(null, 0, null);
        }



       //Advanced Search Based on the CX.Framework
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string AdvancedSearchNew(string mKey, string SearchCriteriaObj, bool SortType, string SortFields, int TotalRecords, int PageNumber)
        {
            CX.Framework.AdvancedSearchFactory factory = new CX.Framework.AdvancedSearchFactory(mKey);
            //factory.EntityCriteria.EntityId = factory.EntityId;
            //factory.EntityCriteria = new CX.Framework.AdvancedSearchCriteria();
            factory.EntityCriteria.pageNumber = PageNumber;
            factory.EntityCriteria.SearchCriteria = unEscape(SearchCriteriaObj);
            factory.EntityCriteria.SortType = SortType;
            factory.EntityCriteria.resultCount = TotalRecords;
            factory.EntityCriteria.SortFields = SortFields;


            MethodInfo method = factory.IManager.GetType().GetMethod("AdvancedSearchLite").MakeGenericMethod(new Type[] { 
            factory.TLightEntity,
            factory.PKType
            });
            Object ResultList = method.Invoke(factory.IManager, new object[] { factory.EntityCriteria });

            return this.AttachStatusCode(ResultList, 1, null);
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string AdvancedSearch(string mKey, string SearchCriteriaObj, bool SortType, string SortFields, int TotalRecords, int PageNumber)
        {
            AdvancedSearchFactory factory = new AdvancedSearchFactory(mKey);
            factory.EntityCriteria.EntityId = factory.EntityId;
            factory.EntityCriteria.pageNumber = PageNumber;
            factory.EntityCriteria.SearchCriteria = unEscape(SearchCriteriaObj);
            factory.EntityCriteria.SortType = SortType;
            factory.EntityCriteria.resultCount = TotalRecords;
            factory.EntityCriteria.SortFields = SortFields;

            List<object> ResultList = factory.IManager.AdvancedSearchLite((CriteriaBase)factory.EntityCriteria);
            return (ResultList != null && ResultList.Count() != 0) ? this.AttachStatusCode(ResultList, 1, null) : this.AttachStatusCode(null, 0, null);


        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string AdvancedSearch2(string mKey, string SearchCriteriaObj, bool SortType, string SortFields, int TotalRecords, int PageNumber, string argsCriteria)
        {
            AdvancedSearchFactory factory = new AdvancedSearchFactory(mKey);
            //if (!string.IsNullOrEmpty(argsCriteria))
            //    factory.EntityCriteria = this.javaScriptSerializer.Deserialize<IAdvancedSearchCriteria>(this.unEscape(argsCriteria));

            factory.EntityCriteria.EntityId = factory.EntityId;
            factory.EntityCriteria.pageNumber = PageNumber;
            factory.EntityCriteria.SearchCriteria = unEscape(SearchCriteriaObj);
            factory.EntityCriteria.SortType = SortType;
            factory.EntityCriteria.resultCount = TotalRecords;
            factory.EntityCriteria.SortFields = SortFields;

            if (!string.IsNullOrEmpty(argsCriteria))
            {
                Dictionary<string, Object> args = this.javaScriptSerializer.Deserialize<Dictionary<string, Object>>(this.unEscape(argsCriteria));
                if (args != null && args.Count > 0)
                {
                    Type criteriaType=factory.EntityCriteria.GetType()  ;
                    foreach (var item in args)
                    {
                        System.Reflection.PropertyInfo pro = criteriaType.GetProperty(item.Key);
                        if (pro == null)
                        {
                            System.Reflection.FieldInfo fieldInfo = criteriaType.GetField(item.Key);
                            if (fieldInfo == null)
                                continue;
                            else
                            {
                                fieldInfo.SetValue(factory.EntityCriteria, GetPropertyValue(fieldInfo, item.Value));
                                //GetPropertyValue
                            }
                        }
                        else
                        {
                            pro.SetValue(factory.EntityCriteria, GetPropertyValue(pro, item.Value), null);
                        }
                    }
                }
            }

            List<object> ResultList = factory.IManager.AdvancedSearchLite((CriteriaBase)factory.EntityCriteria);
            return (ResultList != null && ResultList.Count() != 0) ? this.AttachStatusCode(ResultList, 1, null) : this.AttachStatusCode(null, 0, null);


        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindEntityInfo(string mKey, bool AdvancedSearchType)
        {
            AdvancedSearchFactory factory = new AdvancedSearchFactory(mKey);
            SystemEntityManager EntityMgr = (SystemEntityManager)IoC.Instance.Resolve(typeof(SystemEntityManager));
            SystemEntity systemEntity = EntityMgr.FindById(factory.EntityId, null);

            return (systemEntity != null) ? this.AttachStatusCode(systemEntity, 1, null) : this.AttachStatusCode(null, 0, null);

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindAllEntity(string keyword, int page, int resultCount, string argsCriteria)
        {
            SystemEntityManager EntityMgr = (SystemEntityManager)IoC.Instance.Resolve(typeof(SystemEntityManager));
            SystemEntityCriteria cirteria = new SystemEntityCriteria();
            if (!string.IsNullOrEmpty(argsCriteria))
                cirteria = this.javaScriptSerializer.Deserialize<SystemEntityCriteria>(argsCriteria);
            
            List<SystemEntity> systemEntityList = EntityMgr.FindAll(cirteria);
            return this.AttachStatusCode(systemEntityList, 1, null);

        }
    }
}
