using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using SF.Framework;
using System.Web.Script.Serialization;
using CentrixERP.Common.Business.IManager;
using CE = SagePOS.Manger.Common.Business.Entity;
using E = SagePOS.Server.Business.Factory ;
using CentrixERP.Common.API;
using SagePOS.Server.Configuration;
using SagePOS.Server.API.Common.BaseClasses;


namespace CentrixERP.Common.API
{
    /// <summary>
    /// Summary description for EmailWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    [EntityAttribute(Enums_S3.Entity.Email)]
    public class EmailWebService : WebServiceBaseClass
    {
        E.EmailFactory EmailFactory = null;
        IEmailEntityManager iEmailEntityManager = null;
        public EmailWebService()
        {
            iEmailEntityManager = (IEmailEntityManager)IoC.Instance.Resolve(typeof(IEmailEntityManager));
            //EmailFactory = (E.EmailFactory)IoC.Instance.Resolve(typeof(E.EmailFactory));
        }

        //[WebMethod]
        //[ScriptMethod(ResponseFormat=ResponseFormat.Json)]
        //[EntityMethodAttribute(Enums_S3.DefaultPermissions.View)]
        //public string FindById(int id,string mKey)
        //{
        //    EmailFactory = new E.EmailFactory(mKey);
        //    E.IEmail email = EmailFactory.FindById(id, null);
        //    return this.AttachStatusCode(email, 0, null);

        //}

        //[WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        //[EntityMethodAttribute(Enums_S3.DefaultPermissions.View)]
        //public string FindAll(int ParentId, string mKey)
        //{
        //    EmailFactory = new E.EmailFactory(mKey);
        //    List<E.IEmail> emailList = EmailFactory.FindByParentId(ParentId, null);
        //    if (emailList != null && emailList.Count() > 0)
        //    {
        //        var emails = from E.IEmail email in emailList
        //                     where email.EmailObj != null
        //                     select new
        //                     {
        //                         Id=email.Id,
        //                         EmailId=email.EmailObj.Email_Id,
        //                         EmailAddress = email.EmailObj.EmailAddress,
        //                         EmailType = email.EmailObj.Type.ToString(),
        //                         Description =email.EmailObj.Description,
        //                         CreatedBy = email.EmailObj.CreatedByName,
        //                         UpdatedBy = email.EmailObj.UpdatedByName,
        //                         Crearted_Date = email.EmailObj.Crearted_Date,
        //                         Updated_Date = email.EmailObj.Updated_Date,
        //                         Isdefault = email.Isdefault,
        //                         IsdefaultValue = (email.Isdefault)? "True" : "False"
        //                     };

        //        return this.AttachStatusCode(emails, 0, null);
        //    }
        //    else
        //    {
        //        return this.AttachStatusCode(null, 0, null);

        //    }


        //}

        //[WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        //[EntityMethodAttribute(Enums_S3.DefaultPermissions.Delete)]
        //public string Delete(int id, string mKey)
        //{
        //    EmailFactory = new E.EmailFactory(mKey);
        //    E.IEmail IEmail = EmailFactory.FindById(id, null);
        //    this.LockEntityForDelete(Enums_S3.Entity.Email, IEmail.EmailId);
        //    IEmail.MarkDeleted();
        //    EmailFactory.Save(IEmail);
        //    this.UnLockEntity(Enums_S3.Entity.Email, IEmail.EmailId);
        //    return this.AttachStatusCode(null, 0, null);

        //}

        //[WebMethod]
        //[ScriptMethod(ResponseFormat=ResponseFormat.Json)]
        //[EntityMethodAttribute(true, "id", ValidDefaultPermissions1 = Enums_S3.DefaultPermissions.Edit, ValidDefaultPermissions2 = Enums_S3.DefaultPermissions.Add)]
        //public string AddEditEmail(int id, int moduleId,string EmailAddress,int EmailTypeId, string mKey, bool isDefault, string description, int userId)
        //{
        //    EmailFactory = new E.EmailFactory(mKey);
        //    E.IEmail iEmail = null;
        //    if (id <= 0)
        //    {
        //        iEmail = EmailFactory.IEntity;
        //        iEmail.CreatedBy = userId;
        //        iEmail.EmailObj = new CE.Email();
        //        iEmail.EmailObj.CreatedBy = userId;
        //    }
        //    else
        //    {
        //        iEmail = EmailFactory.FindById(id, null);
        //        this.LockEntity(Enums_S3.Entity.Email, iEmail.EmailId);
        //        iEmail.UpdatedBy = userId;
        //        iEmail.MarkModified();
        //        iEmail.EmailObj.MarkModified();
        //        iEmail.EmailObj.UpdatedBy = userId;
        //    }
        //    iEmail.Id = id;
        //    iEmail.EmailObj.EmailAddress = EmailAddress;
        //    iEmail.EmailObj.Type = (Enums_S3.Email.EmailType)EmailTypeId;
        //    iEmail.EmailObj.Description = unEscape(description);
        //    iEmail.ModuleId = moduleId;
        //    iEmail.Isdefault = isDefault;
        //    iEmail.EmailType = (int)(Enums_S3.Email.EmailType)EmailTypeId;

        //    try
        //    {
        //        if (mKey == "epers")
        //        {
        //            if (PersonMgr.CheckPersonEmailExist("", EmailAddress, id))
        //                return this.AttachStatusCode(null, 501, null);

        //               EmailFactory.Save(iEmail);
        //        }
        //        else
        //        {
        //            //if (mKey == "ecomp")
        //            //{
        //            //    if (CompanyMgr.CheckCompanyEmailExist(EmailAddress, id))
        //            //        return this.AttachStatusCode(null, 501, null);
        //            //}
        //            EmailFactory.Save(iEmail);
        //        }

        //        if (id > 0)
        //            this.UnLockEntity(Enums_S3.Entity.Email, iEmail.EmailId);

        //        return this.AttachStatusCode(iEmail.EmailId, 0, null);
        //    }
        //    catch (SF.Framework.Exceptions.RecordExsitsException ex)
        //    {
        //        return this.AttachStatusCode(null, 501, null);
        //    }
        //    catch (Exception)
        //    {
        //        this.UnLockEntity(Enums_S3.Entity.Email, iEmail.EmailId);
        //        return this.AttachStatusCode(null, 0, null);
        //    }
        //}

        //[WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        //[EntityMethodAttribute(Enums_S3.DefaultPermissions.Edit)]
        //public string SetEmailDefualt(int Id, int moduleId, string mKey, bool IsDefault, int userId)
        //{
        //    EmailFactory = new E.EmailFactory(mKey);
        //    E.IEmail iEmail = EmailFactory.FindById(Id, null);
        //    iEmail.UpdatedBy = userId;

        //    iEmail.MarkModified();

        //    iEmail.Id = Id;
        //    iEmail.ModuleId = moduleId;
        //    iEmail.Isdefault = IsDefault;
        //    iEmail.EmailType = iEmail.EmailType;
        //    try
        //    {
        //        EmailFactory.Save(iEmail);
        //        this.UnLockEntity(Enums_S3.Entity.Email, iEmail.EmailId);
        //        return this.AttachStatusCode(iEmail.EmailId, 0, null);
        //    }
        //    catch (Exception)
        //    {
        //        this.UnLockEntity(Enums_S3.Entity.Email, iEmail.EmailId);
        //        return this.AttachStatusCode(null, 0, null);
        //    }
        //}


        //// New Method
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string Add(int id, int entityId, int entityValueId, string EmailAddress, int EmailTypeId, bool isDefault, string description)
        {
            return Save(true, id, entityId, entityValueId, EmailAddress, EmailTypeId, isDefault, description);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string Edit(int id, int entityId, int entityValueId, string EmailAddress, int EmailTypeId, bool isDefault, string description)
        {
            return Save(false, id, entityId, entityValueId, EmailAddress, EmailTypeId, isDefault, description);
        }

        private string Save(bool add, int id, int entityId, int entityValueId, string EmailAddress, int EmailTypeId, bool isDefault, string description)
        {
           
                CE.EmailEntityCriteria criteria = new CE.EmailEntityCriteria()
                {
                    EntityId = entityId,
                    EntityValueId=entityValueId,
                    EmailAddress = EmailAddress,
                    typeId = EmailTypeId
                };
                List<CE.EmailEntityLite> list = this.iEmailEntityManager.FindAllLite(criteria);
                if (list != null && list.Count > 0 && add)
                {
                    return this.AttachStatusCode(null, 501, null);
                }

                if (!add && list != null && list.Count > 0)
                {
                    foreach (CE.EmailEntityLite emailObj in list)
                    {
                        if (emailObj.EntityValueId != entityValueId && emailObj.EmailAdress == EmailAddress)
                        {
                            return this.AttachStatusCode(null, 501, null);
                            break;
                        }
                    }
                }


            CE.EmailEntity email = null;
            if (add)
            {
                email = new CE.EmailEntity()
                {
                    CreatedBy = LoggedInUser.UserId,
                    EntityId = entityId,
                    EntityValueId = entityValueId,
                    EmailType = EmailTypeId,
                    EmailObj = new CE.Email()
                    {
                        CreatedBy = LoggedInUser.UserId,
                        Description = unEscape(description),
                        EmailAddress = EmailAddress,
                        Type = (SagePOS.Server.Configuration.Enums_S3.Email.EmailType)EmailTypeId
                    }
                };
            }
            else
            {
                email = this.iEmailEntityManager.FindById(id, null);
                this.LockEntity(Enums_S3.Entity.Email, email.EmailId);
                email.MarkModified();
                email.UpdatedBy = LoggedInUser.UserId;
                email.EmailType = EmailTypeId;
                email.EmailObj.Type = (Enums_S3.Email.EmailType)EmailTypeId;
                email.EmailObj.UpdatedBy = LoggedInUser.UserId;
                email.EmailObj.Description = unEscape(description);
                email.EmailObj.EmailAddress = EmailAddress;
                email.EmailType = EmailTypeId;
                email.EmailObj.MarkModified();
            }



            List<SF.FrameworkEntity.ValidationError> errors = this.iEmailEntityManager.Validate(email);
            if (errors != null && errors.Count > 0)
            {
                return this.AttachStatusCode(errors, 0, "validation");
            }

            try
            {
                List<CE.EmailEntityLite> myEmail = this.iEmailEntityManager.FindAllLite(new CE.EmailEntityCriteria { typeId = email.EmailType, EntityId = email.EntityId, EntityValueId = email.EntityValueId, IsDefault = true });
                this.iEmailEntityManager.Save(email);
                if (!add)
                    this.UnLockEntity(Enums_S3.Entity.Email, email.EmailId);


                
                if (myEmail != null && myEmail.Count > 0)
                    SetEmailDefualt(email.EmailId, isDefault);
                else
                    SetEmailDefualt(email.EmailId, true);
                return this.AttachStatusCode(email.EmailEntityId, 1, null);
            }



            catch (Exception ex)
            {
                this.UnLockEntity(Enums_S3.Entity.Email, email.EmailId);
                return this.AttachStatusCode(null, 1, HandleException(ex));
            }
        }



        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindAllLite(int entityId, int entityValueId)
        {
            CE.EmailEntityCriteria criteria = new CE.EmailEntityCriteria()
            {
                EntityId = entityId,
                EntityValueId = entityValueId
            };
            List<CE.EmailEntityLite> list = this.iEmailEntityManager.FindAllLite(criteria);

            if (list != null && list.Count > 0)
            {
                var emails = from CE.EmailEntityLite email in list
                             select new
                             {
                                 Id = email.Id,
                                 EmailId = email.EmailId,
                                 EmailAddress = email.EmailAdress,
                                 EmailType = email.EmailType,
                                 EmailTypeId = email.EmailTypeId,
                                 Description = email.Description,
                                 CreatedBy = email.CreatedByName,
                                 UpdatedBy = email.UpdatedByName,
                                 Crearted_Date = email.CreatedDate,
                                 Updated_Date = email.UpdatedDate,
                                 Isdefault = email.IsDefault,
                                 IsdefaultValue = (email.IsDefault.HasValue) ? (email.IsDefault.Value ? "True" : "False") : "False"
                             };
                return this.AttachStatusCode(emails, 1, null);
            }
            else
            {
                return this.AttachStatusCode(null, 1, null);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string SetEmailDefualt(int Id, bool IsDefault)
        {
            CE.EmailEntity emailEntity = iEmailEntityManager.FindById(Id, null);

            emailEntity.UpdatedBy = LoggedInUser.UserId;
            emailEntity.MarkModified();
            emailEntity.IsDefault = IsDefault;
            emailEntity.EmailType = emailEntity.EmailType;
            try
            {
                iEmailEntityManager.Save(emailEntity);
                this.UnLockEntity(Enums_S3.Entity.Email, emailEntity.EmailId);

                emailEntity = iEmailEntityManager.FindById(Id, null); // new to get the last update date coz no output in update method
                return this.AttachStatusCode(emailEntity.UpdatedDate, 0, null);
            }
            catch (Exception)
            {
                this.UnLockEntity(Enums_S3.Entity.Email, emailEntity.EmailId);
                return this.AttachStatusCode(null, 0, null);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string Delete(int id)
        {
            //EmailFactory = new E.EmailFactory(mKey);
            //E.IEmail IEmail = EmailFactory.FindById(id, null);
            CE.EmailEntity emailEntity = iEmailEntityManager.FindById(id, null);
            this.LockEntityForDelete(Enums_S3.Entity.Email, emailEntity.EmailId);
            emailEntity.MarkDeleted();
            iEmailEntityManager.Save(emailEntity);

            CE.EmailEntityCriteria criteria = new CE.EmailEntityCriteria()
            {
                EntityId = emailEntity.EntityId,
                EntityValueId = emailEntity.EntityValueId
            };
            List<CE.EmailEntityLite> list = this.iEmailEntityManager.FindAllLite(criteria);
            CE.EmailEntityLite defaultEmail = new CE.EmailEntityLite();
            if (list != null)
                defaultEmail = (from CE.EmailEntityLite email in list where email.EmailTypeId == emailEntity.EmailType && email.IsDefault.Value select email).FirstOrDefault();
            this.UnLockEntity(Enums_S3.Entity.Email, emailEntity.EmailId);
            return this.AttachStatusCode(defaultEmail, 0, null);

        }
    }
}
