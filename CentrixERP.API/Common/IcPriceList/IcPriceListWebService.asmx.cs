using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using SF.Framework;
using E = SagePOS.Server.Business.Entity;
using SagePOS.Server.Business.IManager;
using SagePOS.Server.API.Common.BaseClasses;
using System.Reflection;
using SagePOS.Server.Configuration;
using SF.FrameworkEntity;



using CentrixERP.Common.API;
using SagePOS.Server.Configuration;
namespace SagePOS.Server.API
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    [EntityAttribute(Enums_S3.Entity.IcPriceList)]
    public class IcPriceListWebService : WebServiceBaseClass
    {
        IIcPriceListManager iIcPriceListManager = null;
        IIcPriceListDiscountManager iIcPriceListDiscountManager = null;
        public IcPriceListWebService()
        {
            iIcPriceListManager = (IIcPriceListManager)IoC.Instance.Resolve(typeof(IIcPriceListManager));
            iIcPriceListDiscountManager = (IIcPriceListDiscountManager)IoC.Instance.Resolve(typeof(IIcPriceListDiscountManager));
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.Delete)]
        public string Delete(int id)
        {
            this.LockEntityForDelete(Enums_S3.Entity.IcPriceList, id);
            E.IcPriceList myIcPriceList = iIcPriceListManager.FindById(id, null);
            myIcPriceList.PriceListId = id;
            myIcPriceList.MarkDeleted();
            try
            {
                iIcPriceListManager.Save(myIcPriceList);
                UnLockEntity(Enums_S3.Entity.IcPriceList, id);
                return AttachStatusCode(true, 1, null);
            }
            catch (Exception ex)
            {
                return AttachStatusCode(null, 0, HandleException(ex));
            }

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.List)]
        public string FindAllLite(string keyword, int page, int resultCount, string argsCriteria)
        {
            E.IcPriceListCriteria criteria = null;
            if (!SF.Framework.String.IsEmpty(argsCriteria))
            {
                criteria = this.javaScriptSerializer.Deserialize<E.IcPriceListCriteria>(argsCriteria);
            }
            else
                criteria = new E.IcPriceListCriteria();

            criteria.Keyword = keyword;
            criteria.pageNumber = page;
            criteria.resultCount = resultCount;
            
            List<E.IcPriceListLite> IcPriceListList = iIcPriceListManager.FindAllLite(criteria);
            return this.AttachStatusCode(IcPriceListList, 1, null);

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.View)]
        public string FindByIdLite(int id)
        {
            E.IcPriceListLite myIcPriceList = iIcPriceListManager.FindByIdLite(id, null);
            return this.AttachStatusCode(myIcPriceList, 1, null);

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.Add)]
        public string Add(int id, string IcPriceListInfo)
        {
            return Save(true, id, IcPriceListInfo);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.Edit)]
        public string Edit(int id, string IcPriceListInfo)
        {
            if (id <= 0)
            {
                ValidationError error = new ValidationError()
                {
                    ControlName = "",
                    GlobalMessage = true,
                    Message = "Please select IcPriceList"
                };
                List<ValidationError> errors = new List<ValidationError>(){
                error
                };
                return this.AttachStatusCode(errors, 0, "validation");
            }
            else
            {
                return Save(false, id, IcPriceListInfo);
            }
        }

        private string Save(bool add, int id, string IcPriceListInfo)
        {
            E.IcPriceList myIcPriceList = this.javaScriptSerializer.Deserialize<E.IcPriceList>(this.unEscape((IcPriceListInfo)));
            if (!add)
            {
                this.LockEntity(Enums_S3.Entity.IcPriceList, id);


                List<E.PriceListDiscount> oldItems = iIcPriceListDiscountManager.FindByParentId(id, typeof(E.IcPriceList), null);
                HandleItems(oldItems, myIcPriceList.PriceListDiscounts);
                if (oldItems != null)
                {
                    myIcPriceList.PriceListDiscounts.Clear();
                    myIcPriceList.PriceListDiscounts = oldItems;
                }

                foreach(var discount in myIcPriceList.PriceListDiscounts)
                {
                    if (!discount.IsDeleted)
                    {
                        if (myIcPriceList.PricingDeterminedBy == (int)Enums_S3.PriceListPricingDeterminedBy.CustomerType)
                        {
                            discount.Quantity = null;
                        }
                        else
                        {
                            discount.CustomerClass = -1;
                        }

                        if (discount.IsNew)
                        {
                            discount.CreatedBy = LoggedInUser.UserId;
                        }
                        else if (discount.IsModified)
                        {
                            discount.UpdatedBy = LoggedInUser.UserId;
                        }
                    }
                }
                
                myIcPriceList.MarkOld();
                myIcPriceList.MarkModified();
                myIcPriceList.PriceListId = id;
                myIcPriceList.UpdatedBy = LoggedInUser.UserId;
            }
            else
            {
                myIcPriceList.CreatedBy = LoggedInUser.UserId;
            }


            List<ValidationError> errors = iIcPriceListManager.Validate(myIcPriceList);
            if (errors != null && errors.Count > 0)
            {
                return this.AttachStatusCode(errors, 0, "validation");
            }


            try
            {
                iIcPriceListManager.Save(myIcPriceList);
                if (!add)
                    this.UnLockEntity(Enums_S3.Entity.IcPriceList, id);
                return this.FindByIdLite(myIcPriceList.PriceListId);

            }
            catch (SF.Framework.Exceptions.RecordExsitsException ex)
            {
                return this.AttachStatusCode(null, 501, null);
            }
            catch (Exception ex)
            {

                return this.AttachStatusCode(null, 0, HandleException(ex));
            }
        }

        private void HandleItems(List<E.PriceListDiscount> oldItems, List<E.PriceListDiscount> newItems)
        {
            if (oldItems == null)
                oldItems = new List<E.PriceListDiscount>();

            //handle deleted items
            if (newItems == null || newItems.Count == 0)
            {
                foreach (var deleted in oldItems)
                {
                    deleted.MarkDeleted();
                }
            }
            else
            {
                var toDelete = oldItems.Where(deletedItem => !newItems.Select(old => old.DiscountId).Contains(deletedItem.DiscountId));
                foreach (var item in toDelete)
                {
                    item.MarkDeleted();
                }

                var toAdd = newItems.Where(addedItem => addedItem.DiscountId <= 0);
                foreach (var item in toAdd)
                {
                    oldItems.Add(item);
                }

                var toUpdate = newItems.Where(addedItem => oldItems.Select(old => old.DiscountId).Contains(addedItem.DiscountId));
                foreach (var item in toUpdate)
                {
                    if (item.DiscountId <= 0)
                        continue;

                    var toUpdateItem = oldItems.Where(obj => obj.DiscountId == item.DiscountId).FirstOrDefault();
                    if (toUpdateItem != null)
                    {
                        toUpdateItem.MarkModified();
                        toUpdateItem.CustomerClass = item.CustomerClass;
                        toUpdateItem.Quantity = item.Quantity;
                        toUpdateItem.Discount = item.Discount;
                    }
                }

            }
        }
    }
}

