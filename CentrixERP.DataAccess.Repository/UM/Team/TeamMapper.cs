using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.Framework;
using SF.Framework.Data;
using Centrix.UM.Business.Entity;
using Centrix.UM.DataAccess.IRepository;
using System.Data.SqlClient;

namespace Centrix.UM.DataAccess.Repository
{
    public partial class TeamRepository
    {
        public Team GetMapper(SqlDataReader reader)
        {
            Team myTeam = new Team();
            if (reader["team_ID"] != DBNull.Value)
            {
                myTeam.TeamId = (int)reader["team_ID"];
            }
            if (reader["department_ID"] != DBNull.Value)
            {
                myTeam.DepartmentId = (int)reader["department_ID"];
            }
            if (reader["team_name"] != DBNull.Value)
            {
                myTeam.Name = reader["team_name"].ToString();
            }
            if (reader["team_name_arabic"] != DBNull.Value)
            {
                myTeam.NameAr = reader["team_name_arabic"].ToString();
            }
            if (reader["team_description"] != DBNull.Value)
            {
                myTeam.Description = reader["team_description"].ToString();
            }
            if (reader["manager_id"] != DBNull.Value)
            {
                myTeam.ManagerId = SQLDAHelper.Convert_DBTOInt(reader["manager_id"]);
            }

            if (reader["craeted_by"] != DBNull.Value)
            {
                myTeam.CreatedBy = (int)reader["craeted_by"];
            }
            if (reader["updated_by"] != DBNull.Value)
            {
                myTeam.UpdatedBy = (int)reader["updated_by"];
            }           
            return myTeam;
            
        }
        public Team_Lite GetLiteMapper(SqlDataReader reader)
        {
            Team_Lite myTeam = new Team_Lite();
            if (reader["team_ID"] != DBNull.Value)
            {
                myTeam.Id = (int)reader["team_ID"];
                myTeam.value = (int)reader["team_ID"];
            }
            if (reader["department_ID"] != DBNull.Value)
            {
                myTeam.DepartmentId = (int)reader["department_ID"];
            }

            if (reader["department_name"] != DBNull.Value)
            {
                myTeam.Department = (string)reader["department_name"];
            }
            if (reader["team_name"] != DBNull.Value)
            {
                myTeam.Name = reader["team_name"].ToString();
                myTeam.label = reader["team_name"].ToString();
            }
            if (reader["team_name_arabic"] != DBNull.Value)
            {
                myTeam.NameAr = reader["team_name_arabic"].ToString();
            }
            if (reader["team_description"] != DBNull.Value)
            {
                myTeam.Description = reader["team_description"].ToString();
            }
            if (reader["craeted_by"] != DBNull.Value)
            {
                myTeam.CreatedBy = (int)reader["craeted_by"];
            }
            if (reader["full_name"] != DBNull.Value)
            {
                myTeam.CreatedByName = (string)reader["full_name"];
            }

            if (reader["created_date"] != DBNull.Value)
            {
                myTeam.CreatedDate = ((DateTime)reader["created_date"]).GetLocalTime();
            }
           
            if (reader["updated_by"] != DBNull.Value)
            {
                myTeam.UpdatedBy = (int)reader["updated_by"];
            }

            if (reader["updated_date"] != DBNull.Value)
            {
                myTeam.UpdatedDate = ((DateTime)reader["updated_date"]).GetLocalTime();
            }

            if (reader["last_rows"] != DBNull.Value)
            {
                myTeam.LastRows = (bool)reader["last_rows"];
            }
            if (reader["total_count"] != DBNull.Value)
            {
                myTeam.TotalRecords = (int)reader["total_count"];
            }

            if (reader["manager_id"] != DBNull.Value)
            {
                myTeam.ManagerId = SQLDAHelper.Convert_DBTOInt(reader["manager_id"]);
            }
            if (reader["manager_full_name"] != DBNull.Value)
            {
                myTeam.Manager = (string)(reader["manager_full_name"]);
            }

            if (reader["is_management"] != DBNull.Value)
            {
                myTeam.IsManagement = (bool)reader["is_management"];
            }
            if (reader["users_count"] != DBNull.Value)
            {
                myTeam.UsersCount = (int)reader["users_count"];
            }

            return myTeam;

        }

         
    }
}
