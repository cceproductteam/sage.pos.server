using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using Centrix.UM.Business.Entity;
using Centrix.UM.DataAccess.IRepository;
using System.Data.SqlClient;
using System.Data;
using SagePOS.Manger.Common.Business.Entity;


namespace Centrix.UM.DataAccess.Repository
{
    public partial class TeamRepository:ITeamRepository
    {

        #region IRepository<Team,int> Members

        public void Add(ref Team myEntity)
        {

            //SqlConnection conn = new SqlConnection("Data Source=SF-SERVER;Initial Catalog=pharmacydb;Persist Security Info=True;User ID=sa;Password=pocketaps");
            //SqlCommand cmd = new SqlCommand("dbo.SP_Team_Add", conn);
            //cmd.CommandType = System.Data.CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("@TeamName", myEntity.Name);
            //cmd.Parameters.AddWithValue("@Description", myEntity.Description);
            //cmd.Parameters.AddWithValue("@DepartmentID", myEntity.DepartmentId);
            //cmd.Parameters.AddWithValue("@CreatedBy", myEntity.CreatedBy);
            //SqlParameter myparam = new SqlParameter("@Id", System.Data.SqlDbType.Int);
            //myparam.Direction = System.Data.ParameterDirection.Output;

            //cmd.Parameters.Add(myparam);
            SQLDAHelper Instance = new SQLDAHelper(); 
            if (UseCustomConnectionString)
                Instance.SetConnectionString(CustomConnectionString);
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@TeamName", System.Data.SqlDbType.NVarChar, myEntity.Name);
            Instance.AddInParameter("@TeamNameArabic", System.Data.SqlDbType.NVarChar, myEntity.NameAr);
            Instance.AddInParameter("@Description", System.Data.SqlDbType.NVarChar, myEntity.Description);
            Instance.AddInParameter("@DepartmentID", System.Data.SqlDbType.Int, myEntity.DepartmentId);
            Instance.AddInParameter("@CreatedBy", System.Data.SqlDbType.Int, myEntity.CreatedBy);
            Instance.AddInParameter("@manager_id", System.Data.SqlDbType.Int,SQLDAHelper.Convert_IntTODB(myEntity.ManagerId));
            Instance.AddOutParameter("@Id", System.Data.SqlDbType.Int,(object)myEntity.TeamId);
            try
            {
                Instance.ExcuteNonQuery("dbo.SP_Team_Add", conn, false);
                //cmd.Connection.Open();
                //cmd.ExecuteNonQuery();
                //cmd.Connection.Close();
            }
            catch (Exception ex)
            {
                //cmd.Connection.Close();
                conn.Close();
                throw ex;
            }
            myEntity.TeamId = (int)Instance.GetOutParamValue("@Id");
           // myEntity.TeamId = (int)cmd.Parameters["@Id"].Value;

        }

        public void Delete(ref Team myEntity)
        {
             SQLDAHelper Instance = new SQLDAHelper(); 
 SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@Id", System.Data.SqlDbType.Int, myEntity.TeamId);
            try
            {
                Instance.ExcuteNonQuery("dbo.SP_Team_Delete", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteLogical(ref Team myEntity)
        {
             SQLDAHelper Instance = new SQLDAHelper(); 
 SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@Id", System.Data.SqlDbType.Int, myEntity.TeamId);
            try
            {
                Instance.ExcuteNonQuery("dbo.SP_Team_DeleteLogical", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Team> FindAll(CriteriaBase myCriteria)
        {
            List<Team> Teams = new List<Team>();
             SQLDAHelper Instance = new SQLDAHelper(); 
 SqlConnection conn = Instance.GetConnection(false);
            if (myCriteria != null)
            {
                Instance.AddInParameter("@Keyword", System.Data.SqlDbType.NVarChar, ((TeamCriteria)myCriteria).Name);
            }

            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_Team_FindAll", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    Teams.Add(GetMapper(reader));
                }
                reader.Close();
                conn.Close();
                return Teams;
            }
            else
            {
                return null;
            }
        }

        public Team FindById(int Id, CriteriaBase myCriteria)
        {
             SQLDAHelper Instance = new SQLDAHelper(); 
 SqlConnection conn = Instance.GetConnection(false);
            if (myCriteria != null)
            {
                //Instance.AddInParameter("@Name", System.Data.SqlDbType.Int, ((RoleCriteria)myCriteria).Name);
            }
            Instance.AddInParameter("@Id", System.Data.SqlDbType.Int, Id);
            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_Team_FindById", conn);
            if (reader != null && reader.HasRows)
            {
                reader.Read();
                Team myteam = GetMapper(reader);
                reader.Close();
                conn.Close();
                return myteam;
            }
            else
            {
                return null;
            }
        }

        public List<Team> FindByParentId<TParentId>(TParentId ParentId, Type ParentType, CriteriaBase myCriteria)
        {
            throw new NotImplementedException();
        }

        public void Update(ref Team myEntity)
        {
             SQLDAHelper Instance = new SQLDAHelper(); 
 SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@Id", System.Data.SqlDbType.Int, myEntity.TeamId);
            Instance.AddInParameter("@TeamName", System.Data.SqlDbType.NVarChar, myEntity.Name);
            Instance.AddInParameter("@TeamNameArabic", System.Data.SqlDbType.NVarChar, myEntity.NameAr);
            Instance.AddInParameter("@Description", System.Data.SqlDbType.NVarChar, myEntity.Description);
            Instance.AddInParameter("@DepartmentID", System.Data.SqlDbType.Int, myEntity.DepartmentId);
            Instance.AddInParameter("@manager_id", System.Data.SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.ManagerId));
            Instance.AddInParameter("@UpdateBy", System.Data.SqlDbType.Int, myEntity.UpdatedBy);

            try
            {
                Instance.ExcuteNonQuery("dbo.SP_Team_Update", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        public List<Team> FindAll_RecentlyViewed(int userId, int entityId, TeamCriteria myCriteria)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;
            List<Team> List = new List<Team>();
            if (myCriteria != null)
                Instance.AddInParameter("@Keyword", System.Data.SqlDbType.NVarChar, myCriteria.keyword);
            Instance.AddInParameter("@entity_id", System.Data.SqlDbType.Int, entityId);
            Instance.AddInParameter("@user_id", System.Data.SqlDbType.Int, userId);

            reader = Instance.ExcuteReader("SP_Team_RecentlyViewed_FindAll", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                    List.Add(GetMapper(reader));

                reader.Close();
                return List;
            }
            else
                return null;

        }

        public List<Team> FindAllTaskTeams(TeamCriteria myCriteria)
        {
            List<Team> TaskTeamsList = new List<Team>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            if (myCriteria != null)
            {
                if (myCriteria.schedualtaskid != -1)
                    Instance.AddInParameter("@schedualtaskid", System.Data.SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myCriteria.schedualtaskid));

            }

            SqlDataReader reader;
            reader = Instance.ExcuteReader("SP_ScheduleTask_FindAllTeams", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                    TaskTeamsList.Add(GetMapper(reader));
                reader.Close();
                return TaskTeamsList;
            }
            else
                return null;

        }

        public List<Team_Lite> FindAllLite(TeamCriteria myCriteria)
        {
            List<Team_Lite> Teams= new List<Team_Lite>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);

            if (myCriteria != null)
            {
                TeamCriteria criteria = (TeamCriteria)myCriteria;
                if (!string.IsNullOrEmpty(criteria.keyword))
                    Instance.AddInParameter("@Keyword", SqlDbType.NVarChar, criteria.keyword);
                if (!criteria.pageNumber.Equals(-1))
                    Instance.AddInParameter("@page_number", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(criteria.pageNumber));
                if (!criteria.resultCount.Equals(-1))
                    Instance.AddInParameter("@result_count", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(criteria.resultCount));
            }

            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_Team_FindAll_Lite", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    Teams.Add(GetLiteMapper(reader));
                }
                reader.Close();
                return Teams;
            }
            else
            {
                return null;
            }
        }

        public Team_Lite FindByIdLite(int Id)
        {
            Team_Lite team = new Business.Entity.Team_Lite();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);

            Instance.AddInParameter("@Id", System.Data.SqlDbType.Int, Id);
            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_Team_FindById_Lite", conn);

            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                    team = GetLiteMapper(reader);
                reader.Close();
                return team;
            }
            else
                return null;
        }



  //public Email FindTeamEmail(int id)
  //      {
  //          throw new NotImplementedException();
  //      }
  public Email FindTeamEmail(int id)
  {
      Email email = new Email();
      SQLDAHelper Instance = new SQLDAHelper();
      SqlConnection conn = Instance.GetConnection(false);

      Instance.AddInParameter("@team_id", System.Data.SqlDbType.Int, id);
      SqlDataReader reader = Instance.ExcuteReader("dbo.SP_TeamEmail_FindById", conn);

      //if (reader != null && reader.HasRows)
      //{
      //    while (reader.Read())
      //        team = GetLiteMapper(reader);
      //    reader.Close();
      //    return team;
      //}
      //else
      //    return null;

      return null;
  }

  public string CustomConnectionString
  {
      get;
      set;
  }

  public bool UseCustomConnectionString
  {
      get;
      set;
  }
  public Object BeginTransaction()
  {
      return new object();
  }


  public void CommitTransaction(Object object1)
  {
  }

  public void Rollback(Object object1)
  {

  }

  public void KillConnection()
  {

  }


  public bool UseSharedSession { get; set; }


      
    }
}
