using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using Centrix.UM.Business.Entity;
using Centrix.UM.DataAccess.IRepository;
using System.Data.SqlClient;

namespace Centrix.UM.DataAccess.Repository
{
    public partial class AreaRepository : IAreaRepository
    {
        #region IRepository<Area,int> Members

        public void Add(ref Centrix.UM.Business.Entity.Area myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@NameEng", System.Data.SqlDbType.NVarChar, myEntity.NameEnglish);
            Instance.AddInParameter("@NameAr", System.Data.SqlDbType.NVarChar, myEntity.NameArabic);
            Instance.AddInParameter("@Note", System.Data.SqlDbType.NVarChar, myEntity.Note);

            Instance.AddOutParameter("@Id", System.Data.SqlDbType.Int);

            try
            {
                Instance.ExcuteNonQuery("dbo.SP_Area_Add", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            myEntity.AreaId = (int)Instance.GetOutParamValue("@Id");
        }

        public void Delete(ref Centrix.UM.Business.Entity.Area myEntity)
        {
            throw new NotImplementedException();
        }

        public void DeleteLogical(ref Centrix.UM.Business.Entity.Area myEntity)
        {
            throw new NotImplementedException();
        }

        public List<Centrix.UM.Business.Entity.Area> FindAll(CriteriaBase myCriteria)
        {
            List<Area> myArea = new List<Area>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            if (myCriteria != null)
            {
                AreaCriteria myCtr = (AreaCriteria)myCriteria;
                if (!string.IsNullOrEmpty(myCtr.Name))
                {
                    Instance.AddInParameter("@keyword", System.Data.SqlDbType.NVarChar, myCtr.Name);
                }
                Instance.AddInParameter("@UserId", System.Data.SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myCtr.UserId));
            }

            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_Area_FindAll", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    myArea.Add(GetMapper(reader));
                }
                reader.Close();
                return myArea;
            }
            else
            {
                return null;
            }
        }

        public Centrix.UM.Business.Entity.Area FindById(int Id, CriteriaBase myCriteria)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            if (myCriteria != null)
            {
                //Instance.AddInParameter("@Name", System.Data.SqlDbType.Int, ((RoleCriteria)myCriteria).Name);
            }
            Instance.AddInParameter("@Id", System.Data.SqlDbType.Int, Id);

            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_Area_FindById", conn);
            if (reader != null && reader.HasRows)
            {
                reader.Read();
                Area area = GetMapper(reader);
                reader.Close();
                return area;

            }
            else
            {
                return null;
            }
        }

        public List<Centrix.UM.Business.Entity.Area> FindByParentId<TParentId>(TParentId ParentId, Type ParentType, CriteriaBase myCriteria)
        {
            throw new NotImplementedException();
        }

        public void Update(ref Centrix.UM.Business.Entity.Area myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@NameEng", System.Data.SqlDbType.NVarChar, myEntity.NameEnglish);
            Instance.AddInParameter("@NameAr", System.Data.SqlDbType.NVarChar, myEntity.NameArabic);
            Instance.AddInParameter("@Note", System.Data.SqlDbType.NVarChar, myEntity.Note);

            Instance.AddInParameter("@Id", System.Data.SqlDbType.Int, myEntity.AreaId);

            try
            {
                Instance.ExcuteNonQuery("dbo.SP_Area_Update", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string CustomConnectionString
        {
            get;
            set;
        }

        public bool UseCustomConnectionString
        {
            get;
            set;
        }
        public Object BeginTransaction()
        {
            return new object();
        }


        public void CommitTransaction(Object object1)
        {
        }

        public void Rollback(Object object1)
        {

        }

        public void KillConnection()
        {

        }


        public bool UseSharedSession { get; set; }

        #endregion
    }
}
