using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SF.Framework.Data;
using Centrix.UM.Business.Entity;
using Centrix.UM.DataAccess.IRepository;
using System.Data.SqlClient;

namespace Centrix.UM.DataAccess.Repository
{
    public partial class AreaRepository
    {
        public Area GetMapper(SqlDataReader reader)
        {
            Area myArea = new Area();
            if (reader["area_id"] != DBNull.Value)
            {
                myArea.AreaId = (int)reader["area_id"];
            }
            if (reader["area_name_en"] != DBNull.Value)
            {
                myArea.NameEnglish = reader["area_name_en"].ToString();
            }
            if (reader["area_name_ar"] != DBNull.Value)
            {
                myArea.NameArabic = reader["area_name_ar"].ToString();
            }
            if (reader["area_notes"] != DBNull.Value)
            {
                myArea.Note = reader["area_notes"].ToString() ;
            }
            return myArea;
        }
    }
}
