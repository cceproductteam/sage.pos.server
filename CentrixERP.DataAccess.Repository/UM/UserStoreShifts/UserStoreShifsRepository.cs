
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using Centrix.UM.Business.Entity;
using Centrix.UM.DataAccess.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace Centrix.UM.DataAccess.Repository
{
    public partial class UserStoreShifsRepository : RepositoryBaseClass<UserStoreShifs, UserStoreShifsLite, int>, IUserStoreShifsRepository
    {
        #region IRepository< UserStoreShifs, int > Members

        public UserStoreShifsRepository()
        {
            this.AddSPName = "SP_UserStoreShifs_Add";
            this.UpdateSPName = "SP_UserStoreShifs_Update";
            this.DeleteSPName = "SP_UserStoreShifs_Delete";
            this.DeleteLogicalSPName = "SP_UserStoreShifs_DeleteLogical";
            this.FindAllSPName = "SP_UserStoreShifs_FindAll";
            this.FindByIdSPName = "SP_UserStoreShifs_FindById";
            this.FindByParentSPName = "SP_UserStoreShifs_FindByParentId";
            this.FindAllLiteSPName = "SP_UserStoreShifs_FindAll_Lite";
            this.FindByIdLiteSPName = "SP_UserStoreShifs_FindById_Lite";
            this.AdvancedSearchSPName = "SP_UserStoreShifs_AdvancedSearch_Lite";
        }

		#endregion
    }
}