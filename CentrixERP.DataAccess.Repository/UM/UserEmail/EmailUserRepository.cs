using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using Centrix.UM.Business.Entity;
using Centrix.UM.DataAccess.IRepository;
using System.Data.SqlClient;
using System.Data;
using Centrix.UM.DataAccess.Repository;
using SagePOS.Manger.Common.Business.Entity;

namespace Centrix.UM.DataAccess.Repository
{
    public partial class EmailUserRepository : IEmailUserRepository
    {
       #region IRepository< EmailUser, int > Members
    
        public void Add(ref EmailUser myEntity)
            {
                SQLDAHelper Instance = new SQLDAHelper();
                SqlConnection conn = Instance.GetConnection(false);
				Instance.AddInParameter("@email_id", SqlDbType.Int, myEntity.EmailId);
				Instance.AddInParameter("@user_id", SqlDbType.Int, myEntity.UserId);
				Instance.AddInParameter("@is_default", SqlDbType.Bit, myEntity.Isdefault);
				Instance.AddInParameter("@created_by", SqlDbType.Int, myEntity.CreatedBy);
                Instance.AddInParameter("@email_type", System.Data.SqlDbType.Int, myEntity.EmailType);
                Instance.AddOutParameter("@email_user_id",SqlDbType.Int);
                
                Instance.ExcuteNonQuery("dbo.SP_EmailUser_Add", conn, false);
            
                myEntity.EmailUserId = (int)Instance.GetOutParamValue("@email_user_id");
            }
        
        public void Update(ref EmailUser myEntity)
            {
                SQLDAHelper Instance = new SQLDAHelper();
                SqlConnection conn = Instance.GetConnection(false);
				Instance.AddInParameter("@email_user_id", SqlDbType.Int, myEntity.EmailUserId);
				Instance.AddInParameter("@email_id", SqlDbType.Int, myEntity.EmailId);
				Instance.AddInParameter("@user_id", SqlDbType.Int, myEntity.UserId);
				Instance.AddInParameter("@is_default", SqlDbType.Bit, myEntity.Isdefault);
				Instance.AddInParameter("@updated_by", SqlDbType.Int, myEntity.UpdatedBy);
                Instance.AddInParameter("@email_type", System.Data.SqlDbType.Int, myEntity.EmailType);
                Instance.ExcuteNonQuery("dbo.SP_EmailUser_Update", conn, false);         
                
            }
        
        public EmailUser FindById(int Id, CriteriaBase myCriteria)
            {
                SQLDAHelper Instance = new SQLDAHelper();
                SqlConnection conn = Instance.GetConnection(false);
                Instance.AddInParameter("@email_user_id", SqlDbType.Int, Id);
                SqlDataReader reader;
                reader = Instance.ExcuteReader("dbo.sp_EmailUser_FindByID", conn);
            
                if (reader != null && reader.HasRows)
                {
                    reader.Read();
                    EmailUser myEmailuser = GetMapper(reader);
                    reader.Close();
                    return myEmailuser;
                }
                else
                    return null;
            }
        public List< EmailUser > FindAll(CriteriaBase myCriteria)
            {
                List< EmailUser > EmailUserList = new List< EmailUser>();
                SQLDAHelper Instance = new SQLDAHelper();
                SqlConnection conn = Instance.GetConnection(false);
                SqlDataReader reader;
                reader = Instance.ExcuteReader("dbo.sp_EmailUser_FindAll", conn);
            
                if (reader != null && reader.HasRows)
                {
                    while (reader.Read())
                        EmailUserList.Add(GetMapper(reader));
                    reader.Close();
                    return EmailUserList;
                }
                else
                    return null;
    
            }
        
        public List< EmailUser> FindByParentId<TParentId>(TParentId ParentId, Type ParentType, CriteriaBase myCriteria)
            {
                List< EmailUser> EmailUserList = new List< EmailUser>();
                SQLDAHelper Instance = new SQLDAHelper();
                SqlConnection conn = Instance.GetConnection(false);
            
				if (ParentType == typeof(Email))
				{
					Instance.AddInParameter("@email_id", SqlDbType.Int, ParentId);
				}
				if (ParentType == typeof(User))
				{
					Instance.AddInParameter("@user_id", SqlDbType.Int, ParentId);
				}

                SqlDataReader reader = Instance.ExcuteReader("dbo.SP_EmailUser_FindByParentId", conn);
                if (reader != null && reader.HasRows)
                {
                    while (reader.Read())
                    {
                        EmailUserList.Add(GetMapper(reader));
                    }
                    reader.Close();
                    return EmailUserList;
                }
                else
                {
                    return null;
                }
            }
        
        public void Delete(ref EmailUser myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@email_user_id", SqlDbType.Int, myEntity.EmailUserId);

            
            Instance.ExcuteNonQuery("dbo.SP_EmailUser_Delete", conn, false);
            
        }

        public void DeleteLogical(ref EmailUser myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@email_user_id", System.Data.SqlDbType.Int, myEntity.EmailUserId);

            try
            {
                Instance.ExcuteNonQuery("dbo.SP_EmailUser_DeleteLogical", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string CustomConnectionString
        {
            get;
            set;
        }

        public bool UseCustomConnectionString
        {
            get;
            set;
        }
        public Object BeginTransaction()
        {
            return new object();
        }


        public void CommitTransaction(Object object1)
        {
        }

        public void Rollback(Object object1)
        {

        }

        public void KillConnection()
        {

        }


        public bool UseSharedSession { get; set; }

       #endregion
    }
}


