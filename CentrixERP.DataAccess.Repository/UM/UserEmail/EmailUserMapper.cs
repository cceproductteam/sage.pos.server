using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.Framework.Data;
using Centrix.UM.Business.Entity;
using Centrix.UM.DataAccess.IRepository;
using System.Data.SqlClient;
using SF.Framework;

namespace Centrix.UM.DataAccess.Repository
{
    public partial class EmailUserRepository
    {
        public EmailUser GetMapper(SqlDataReader reader)
        {
            EmailUser myEmailUser = new EmailUser();
            if (reader["email_user_id"] != DBNull.Value)
            {
                myEmailUser.EmailUserId = (int)reader["email_user_id"];
            }
            if (reader["email_id"] != DBNull.Value)
            {
                myEmailUser.EmailId = (int)reader["email_id"];
            }

            if (reader["user_id"] != DBNull.Value)
            {
                myEmailUser.UserId = (int)reader["user_id"];
            }

            if (reader["created_by"] != DBNull.Value)
            {
                myEmailUser.CreatedBy = (int)reader["created_by"];
            }
            if (reader["updated_by"] != DBNull.Value)
            {
                myEmailUser.UpdatedBy = (int)reader["updated_by"];
            }
            if (reader["created_date"] != DBNull.Value)
            {
                myEmailUser.CreatedDate = ((DateTime)reader["created_date"]).GetLocalTime();
            }
            if (reader["updated_date"] != DBNull.Value)
            {
                myEmailUser.UpdatedDate = ((DateTime)reader["updated_date"]).GetLocalTime();
            }
            if (reader["is_default"] != DBNull.Value)
            {
                myEmailUser.Isdefault = (bool)reader["is_default"];
            }
            if (reader["email_type"] != DBNull.Value)
            {
                myEmailUser.EmailType = (int)reader["email_type"];
            }

            return myEmailUser;
        }
    }
}
