
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using Centrix.UM.Business.Entity;
using Centrix.UM.DataAccess.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace Centrix.UM.DataAccess.Repository
{
    public partial class ShiftsRepository : RepositoryBaseClass<Shifts, ShiftsLite, int>, IShiftsRepository
    {
        #region IRepository< Shifts, int > Members

        public ShiftsRepository()
        {
            this.AddSPName = "SP_Shifts_Add";
            this.UpdateSPName = "SP_Shifts_Update";
            this.DeleteSPName = "SP_Shifts_Delete";
            this.DeleteLogicalSPName = "SP_Shifts_DeleteLogical";
            this.FindAllSPName = "SP_Shifts_FindAll";
            this.FindByIdSPName = "SP_Shifts_FindById";
            this.FindByParentSPName = "SP_Shifts_FindByParentId";
            this.FindAllLiteSPName = "SP_Shifts_FindAll_Lite";
            this.FindByIdLiteSPName = "SP_Shifts_FindById_Lite";
            this.AdvancedSearchSPName = "SP_Shifts_AdvancedSearch_Lite";
        }

		#endregion
    }
}