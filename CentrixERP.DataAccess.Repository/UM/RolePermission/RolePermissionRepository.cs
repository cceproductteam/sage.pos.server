using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using Centrix.UM.Business.Entity;
using Centrix.UM.DataAccess.IRepository;
using System.Data.SqlClient;
using SagePOS.Manger.Common.Business.Entity;
using System.Data;

namespace Centrix.UM.DataAccess.Repository
{
    public partial class RolePermissionRepository : IRolePermissionRepository
    {

        #region IRepository<RolePermission,int> Members

        public void Add(ref RolePermission myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            if (UseCustomConnectionString)
                Instance.SetConnectionString(CustomConnectionString);
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@RoleId", System.Data.SqlDbType.Int, myEntity.RoleId);
            Instance.AddInParameter("@PermissionId", System.Data.SqlDbType.Int, myEntity.Permissionid);
            Instance.AddInParameter("@createdBy", System.Data.SqlDbType.Int, myEntity.CreatedBy);
            Instance.AddInParameter("@entity_id", System.Data.SqlDbType.Int, myEntity.EntityId);
            Instance.AddOutParameter("@Id", System.Data.SqlDbType.Int,(object)myEntity.RolePermissionId);

            try
            {
                Instance.ExcuteNonQuery("dbo.SP_RolePermission_Add", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            myEntity.RolePermissionId = (int)Instance.GetOutParamValue("@Id");
        }

        public void Delete(ref RolePermission myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@Id", System.Data.SqlDbType.Int, myEntity.RolePermissionId);

            try
            {
                Instance.ExcuteNonQuery("dbo.SP_RolePermission_Delete", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteLogical(ref RolePermission myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            if (UseCustomConnectionString)
                Instance.SetConnectionString(CustomConnectionString);
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@Id", System.Data.SqlDbType.Int, myEntity.RolePermissionId);

            try
            {
                Instance.ExcuteNonQuery("dbo.SP_RolePermission_DeleteLogical", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<RolePermission> FindAll(CriteriaBase myCriteria)
        {
            List<RolePermission> myRolePer = new List<RolePermission>();
            SQLDAHelper Instance = new SQLDAHelper();
            if (UseCustomConnectionString)
                Instance.SetConnectionString(CustomConnectionString);
            SqlConnection conn = Instance.GetConnection(false);
            if (myCriteria != null)
            {
                Instance.AddInParameter("@role_id", System.Data.SqlDbType.Int, ((RolePermissionCriteria)myCriteria).RoleId);
                Instance.AddInParameter("@entityId", System.Data.SqlDbType.Int, ((RolePermissionCriteria)myCriteria).EntityId);
                Instance.AddInParameter("@last_sync_date", System.Data.SqlDbType.DateTime, ((RolePermissionCriteria)myCriteria).lastSyncDate);
            }

            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_RolePermission_FindAll", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    myRolePer.Add(GetMapper(reader));
                }
                reader.Close();
                return myRolePer;
            }
            else
            {
                return null;
            }
        }

        public RolePermission FindById(int Id, CriteriaBase myCriteria)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            if (myCriteria != null)
            {
                //Instance.AddInParameter("@Name", System.Data.SqlDbType.Int, ((RoleCriteria)myCriteria).Name);
            }
            Instance.AddInParameter("@Id", System.Data.SqlDbType.Int, Id);

            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_RolePermission_FindById", conn);
            if (reader != null && reader.HasRows)
            {
                reader.Read();
                RolePermission rolePer = GetMapper(reader);
                reader.Close();
                return rolePer;

            }
            else
            {
                return null;
            }
        }

        public List<RolePermission> FindByParentId<TParentId>(TParentId ParentId, Type ParentType, CriteriaBase myCriteria)
        {
            List<RolePermission> myRolePer = new List<RolePermission>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            if (myCriteria != null)
            {
                //Instance.AddInParameter("@Name", System.Data.SqlDbType.Int, ((RoleCriteria)myCriteria).Name);
            }
            if (ParentType == typeof(Role))
            {
                Instance.AddInParameter("@RoleId", System.Data.SqlDbType.Int, ParentId);

            }
            if (ParentType == typeof(Permission))
            {
                Instance.AddInParameter("@PermissionId", System.Data.SqlDbType.Int, ParentId);

            }
            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_RolePermission_FindByParentId", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    myRolePer.Add(GetMapper(reader));
                }
                reader.Close();
                return myRolePer;
            }
            else
            {
                return null;
            }
        }

        public void Update(ref RolePermission myEntity)
        {
            throw new NotImplementedException();
        }

        public List<RolePermissionLite> FindAllRolePermission(int RoleId, CriteriaBase myCriteria)
        {
            List<RolePermissionLite> myRolePer = new List<RolePermissionLite>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            if (myCriteria != null)
            {
                RolePermissionCriteria criteria = (RolePermissionCriteria)myCriteria;
                if (criteria.EntityId > 0)
                    Instance.AddInParameter("@entity_id", System.Data.SqlDbType.Int, criteria.EntityId);
                if (criteria.ParentId > 0)
                  Instance.AddInParameter("@parent_id", System.Data.SqlDbType.Int, criteria.ParentId);

                Instance.AddInParameter("@get_all_permissions", System.Data.SqlDbType.Bit, criteria.getAllPermissions);

            }
            Instance.AddInParameter("@role_id", System.Data.SqlDbType.Int, RoleId);

            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_RolePermission_FindAll_Lite", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    myRolePer.Add(GetFindAllLiteMapper(reader));
                }
                reader.Close();
                return myRolePer;
            }
            else
            {
                return null;
            }
        }

        public List<RolePermissionLite> FindRolePermission(int RoleId, CriteriaBase myCriteria)
        {
            List<RolePermissionLite> myRolePer = new List<RolePermissionLite>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            if (myCriteria != null)
            {
                RolePermissionCriteria criteria = (RolePermissionCriteria)myCriteria;
                if (criteria.EntityId > 0)
                    Instance.AddInParameter("@entity_id", System.Data.SqlDbType.Int, criteria.EntityId);

            }
            Instance.AddInParameter("@role_id", System.Data.SqlDbType.Int, RoleId);


            SqlDataReader reader = Instance.ExcuteReader("SP_RolePermission_FindRolePermission", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    myRolePer.Add(GetFindRolePermissionMapper(reader));
                }
                reader.Close();
                return myRolePer;
            }
            else
            {
                return null;
            }
        }

        

        public void DeleteByRole(int roleId)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@role_id", System.Data.SqlDbType.Int, roleId);

            try
            {
                Instance.ExcuteNonQuery("dbo.SP_RolePermission_Delete_ByRole", conn, false);
            }
            catch (SF.Framework.Exceptions.RecordExsitsException ex)
            {

            }
            catch (SF.Framework.Exceptions.RecordNotAffected ex)
            {

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(int roleId, string permissionStatusIds,int createdBy)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@permission_ids", System.Data.SqlDbType.NVarChar, permissionStatusIds);
            Instance.AddInParameter("@role_id", System.Data.SqlDbType.Int, roleId);
            Instance.AddInParameter("@created_by", System.Data.SqlDbType.Int, createdBy);
            

            try
            {
                Instance.ExcuteNonQuery("dbo.SP_RolePermission_Save", conn, false);
            }
            catch (SF.Framework.Exceptions.RecordExsitsException ex)
            {

            }
            catch (SF.Framework.Exceptions.RecordNotAffected ex)
            {

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion


        public void SetAllPermissions(bool all, int roleId,int userId)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@role_id", System.Data.SqlDbType.Int, roleId);
            Instance.AddInParameter("@all_permission", System.Data.SqlDbType.Bit, all);
            Instance.AddInParameter("@created_by", System.Data.SqlDbType.Int, userId);

            Instance.ExcuteNonQuery("dbo.SP_RolePermission_SetAllPermissions", conn, false);
        }

        public int LockRolePermissions(CriteriaBase myCriteria)
        {

            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
             RolePermissionCriteria Criteria = null;
            

              if (myCriteria != null)
              {
                Criteria =(RolePermissionCriteria)myCriteria;
              }

            Instance.AddInParameter("@entity_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(Criteria.EntityId));
            Instance.AddInParameter("@user_id", SqlDbType.Int, Criteria.UserId);
            Instance.AddInParameter("@role_id", SqlDbType.Int, Criteria.RoleId);

            if (Criteria.EntityLastUpdatedDate.HasValue)
                Instance.AddInParameter("@updated_date", SqlDbType.DateTime, Criteria.EntityLastUpdatedDate.Value.ToLocalTime());


            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_RolePermission_Lock", conn);
            if (reader != null && reader.HasRows)
            {
                int Status = 0;
                while (reader.Read())
                {
                    Status = (int)reader["status"];
                }
                reader.Close();
                return Status;
            }
            else
            {
                return 0;
            }
          
        }

        public void UpdateRolePermissionUpdatedDate(CriteriaBase myCriteria) 
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            RolePermissionCriteria Criteria = null;
            
            if (myCriteria != null)
            {
                Criteria = (RolePermissionCriteria)myCriteria;
                Instance.AddInParameter("@role_id", SqlDbType.Int, Criteria.RoleId);

                if (Criteria.EntityLastUpdatedDate.HasValue)
                    Instance.AddInParameter("@updated_date", SqlDbType.DateTime, Criteria.EntityLastUpdatedDate.Value.ToLocalTime());
            }

             Instance.ExcuteNonQuery("dbo.SP_Role_Update_PermissionUpdatedDate", conn,false);
          

        }

        public string CustomConnectionString
        {
            get;
            set;
        }

        public bool UseCustomConnectionString
        {
            get;
            set;
        }
        public Object BeginTransaction()
        {
            return new object();
        }


        public void CommitTransaction(Object object1)
        {
        }

        public void Rollback(Object object1)
        {

        }

        public void KillConnection()
        {

        }


        public bool UseSharedSession { get; set; }

    }
}