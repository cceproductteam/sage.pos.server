using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SF.Framework.Data;
using Centrix.UM.Business.Entity;
using Centrix.UM.DataAccess.IRepository;
using System.Data.SqlClient;

namespace Centrix.UM.DataAccess.Repository
{
    public partial class RolePermissionRepository
    {
        public RolePermission GetMapper(SqlDataReader reader)
        {
            RolePermission myRolePer = new RolePermission();
            if (reader["role_permission_id"] != DBNull.Value)
            {
                myRolePer.RolePermissionId = (int)reader["role_permission_id"];
            }
            if (reader["role_id"] != DBNull.Value)
            {
                myRolePer.RoleId = (int)reader["role_id"];
            }
            if (reader["entity_permission_id"] != DBNull.Value)
            {
                myRolePer.Permissionid = (int)reader["entity_permission_id"];
            }
            if (reader["entity_id"] != DBNull.Value)
            {
                myRolePer.EntityId = (int)reader["entity_id"];
            }
            return myRolePer;
        }

        public RolePermissionLite GetFindAllLiteMapper(SqlDataReader reader)
        {
            RolePermissionLite Obj = new RolePermissionLite();

            if (reader["role_id"] != DBNull.Value)
            {
                Obj.RoleId = (int)reader["role_id"];
            }
            if (reader["role_name"] != DBNull.Value)
            {
                Obj.RoleName = (string)reader["role_name"];
            }
            if (reader["role_name_ar"] != DBNull.Value)
            {
                Obj.RoleNameAr = (string)reader["role_name_ar"];
            }
            if (reader["entity_id"] != DBNull.Value)
            {
                Obj.EntityId = (int)reader["entity_id"];
            }
            if (reader["entity_name"] != DBNull.Value)
            {
                Obj.EntityName = (string)reader["entity_name"];
            }
            if (reader["entity_name_ar"] != DBNull.Value)
            {
                Obj.EntityNameAr = (string)reader["entity_name_ar"];
            }
            if (reader["entity_permission_id"] != DBNull.Value)
            {
                Obj.EntityPermissionId = (int)reader["entity_permission_id"];
            }

            if (reader["permission_id"] != DBNull.Value)
            {
                Obj.PermissionId = (int)reader["permission_id"];
            }


           if (reader["permission_name"] != DBNull.Value)
            {
                Obj.PermissionName = (string)reader["permission_name"];
            }
            if (reader["permission_name_ar"] != DBNull.Value)
            {
                Obj.PermissionNameAr = (string)reader["permission_name_ar"];
            }
            if (reader["permission_description"] != DBNull.Value)
            {
                Obj.PermissionDescription = (string)reader["permission_description"];
            }
            if (reader["default_permission"] != DBNull.Value)
            {
                Obj.DefaultPermission = (bool)reader["default_permission"];
            }
            if (reader["role_permission_id"] != DBNull.Value)
            {
                Obj.RolePermissionId = (int)reader["role_permission_id"];
            }
            if (reader["has_permission"] != DBNull.Value)
            {
                Obj.HasPermission = (bool)reader["has_permission"];
            }

            if (reader["css_class"] != DBNull.Value)
            {
                Obj.CssClass = (string)reader["css_class"];
            }

            if (reader["parent_id"] != DBNull.Value)
            {
                Obj.ParentId = SQLDAHelper.Convert_DBTOInt(reader["parent_id"]);
            }
            if (reader["module_name"] != DBNull.Value)
            {
                Obj.ModuleName = (string)reader["module_name"];
            }
            if (reader["module_name_ar"] != DBNull.Value)
            {
                Obj.ModuleNameAR = (string)reader["module_name_ar"];
            }
            if (reader["module_id"] != DBNull.Value)
            {
                Obj.ModuleId = (int)reader["module_id"];
            }


            return Obj;
        }

        public RolePermissionLite GetFindRolePermissionMapper(SqlDataReader reader)
        {
            RolePermissionLite Obj = new RolePermissionLite();

            if (reader["role_id"] != DBNull.Value)
            {
                Obj.RoleId = (int)reader["role_id"];
            }
            if (reader["role_name"] != DBNull.Value)
            {
                Obj.RoleName = (string)reader["role_name"];
            }
            if (reader["role_name_ar"] != DBNull.Value)
            {
                Obj.RoleNameAr = (string)reader["role_name_ar"];
            }
            if (reader["entity_id"] != DBNull.Value)
            {
                Obj.EntityId = (int)reader["entity_id"];
            }
            if (reader["entity_name"] != DBNull.Value)
            {
                Obj.EntityName = (string)reader["entity_name"];
            }
            if (reader["entity_name_ar"] != DBNull.Value)
            {
                Obj.EntityNameAr = (string)reader["entity_name_ar"];
            }
            if (reader["permission_id"] != DBNull.Value)
            {
                Obj.PermissionId = (int)reader["permission_id"];
            }
            if (reader["entity_permission_id"] != DBNull.Value)
            {
                Obj.EntityPermissionId = (int)reader["entity_permission_id"];
            }

            if (reader["permission_name"] != DBNull.Value)
            {
                Obj.PermissionName = (string)reader["permission_name"];
            }
            if (reader["permission_name_ar"] != DBNull.Value)
            {
                Obj.PermissionNameAr = (string)reader["permission_name_ar"];
            }
            if (reader["permission_description"] != DBNull.Value)
            {
                Obj.PermissionDescription = (string)reader["permission_description"];
            }
            if (reader["default_permission"] != DBNull.Value)
            {
                Obj.DefaultPermission = (bool)reader["default_permission"];
            }
            if (reader["role_permission_id"] != DBNull.Value)
            {
                Obj.RolePermissionId = (int)reader["role_permission_id"];
            }
            if (reader["has_permission"] != DBNull.Value)
            {
                Obj.HasPermission = (bool)reader["has_permission"];
            }
            if (reader["css_class"] != DBNull.Value)
            {
                Obj.CssClass = (string)reader["css_class"];
            }

            return Obj;
        }

    }
}
