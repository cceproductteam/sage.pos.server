using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Centrix.UM.Business.Entity;
using SF.Framework.Data;
using System.Data.SqlClient;
using SF.Framework;

namespace Centrix.UM.DataAccess.Repository
{
    public partial class UserLogginRepository
    {
        public UserLoggin GetMapper(SqlDataReader reader)
        {
            UserLoggin obj = new UserLoggin();
			if (reader["user_loggin_id"] != DBNull.Value)
			{
				obj.UserLogginId = (int)reader["user_loggin_id"];
			}
			if (reader["user_id"] != DBNull.Value)
			{
				obj.UserId = (int)reader["user_id"];
			}
			if (reader["user_agent"] != DBNull.Value)
			{
				obj.UserAgent = (string)reader["user_agent"];
			}
			if (reader["local_ip"] != DBNull.Value)
			{
				obj.LocalIp = (string)reader["local_ip"];
			}
			if (reader["public_ip"] != DBNull.Value)
			{
				obj.PublicIp = (string)reader["public_ip"];
			}
			if (reader["last_request"] != DBNull.Value)
			{
                obj.LastRequest = ((DateTime)reader["last_request"]).GetLocalTime();
			}
			if (reader["random_string"] != DBNull.Value)
			{
				obj.RandomString = (string)reader["random_string"];
			}

            return obj;
        }
    }
}
