
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using Centrix.UM.Business.Entity;
using Centrix.UM.DataAccess.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace Centrix.UM.DataAccess.Repository
{
    public partial class UserLogginRepository : IUserLogginRepository
    {
        #region IRepository< UserLoggin, int > Members

        public void Add(ref UserLoggin myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);

			Instance.AddInParameter("@user_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.UserId));
			Instance.AddInParameter("@user_agent", SqlDbType.VarChar, myEntity.UserAgent);
			Instance.AddInParameter("@local_ip", SqlDbType.VarChar, myEntity.LocalIp);
			Instance.AddInParameter("@public_ip", SqlDbType.VarChar, myEntity.PublicIp);
			Instance.AddInParameter("@last_request", SqlDbType.DateTime, myEntity.LastRequest);
			Instance.AddInParameter("@random_string", SqlDbType.VarChar, myEntity.RandomString);
            //Instance.AddInParameter("@created_by", SqlDbType.Int, myEntity.CreatedBy);
            Instance.AddOutParameter("@user_loggin_id", SqlDbType.Int);

            Instance.ExcuteNonQuery("dbo.SP_UserLoggin_Add", conn, false);

            myEntity.UserLogginId = (int)Instance.GetOutParamValue("@user_loggin_id");
        }

        public void Update(ref UserLoggin myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@user_loggin_id", SqlDbType.Int, myEntity.UserLogginId);
			Instance.AddInParameter("@user_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.UserId));
			Instance.AddInParameter("@user_agent", SqlDbType.VarChar, myEntity.UserAgent);
			Instance.AddInParameter("@local_ip", SqlDbType.VarChar, myEntity.LocalIp);
			Instance.AddInParameter("@public_ip", SqlDbType.VarChar, myEntity.PublicIp);
			Instance.AddInParameter("@last_request", SqlDbType.DateTime, myEntity.LastRequest);
			Instance.AddInParameter("@random_string", SqlDbType.VarChar, myEntity.RandomString);
            //Instance.AddInParameter("@updated_by", SqlDbType.Int,myEntity.UpdatedBy);
            
            Instance.ExcuteNonQuery("dbo.SP_UserLoggin_Update", conn, false);
        }

        public UserLoggin FindById(int Id, CriteriaBase myCriteria)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@user_loggin_id", SqlDbType.Int, Id);
            SqlDataReader reader;

            reader = Instance.ExcuteReader("dbo.SP_UserLoggin_FindById", conn);

            if (reader != null && reader.HasRows)
            {
                reader.Read();
                UserLoggin myUserLoggin = GetMapper(reader);
                reader.Close();
                return myUserLoggin;
            }
            else
                return null;
        }

        public List<UserLoggin> FindAll(CriteriaBase myCriteria)
        {
            List<UserLoggin> UserLogginList = new List<UserLoggin>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;
            if (myCriteria != null)
            {
                UserLogginCriteria criteria = (UserLogginCriteria)myCriteria;
                if (criteria.UserId.HasValue)
                    Instance.AddInParameter("@user_id", SqlDbType.Int, criteria.UserId.Value);

                if (!string.IsNullOrEmpty(criteria.RandomString))
                    Instance.AddInParameter("@random_string", SqlDbType.VarChar, criteria.RandomString);
                if (!string.IsNullOrEmpty(criteria.LocalIP))
                    Instance.AddInParameter("@loacl_ip", SqlDbType.VarChar, criteria.LocalIP);
                if (!string.IsNullOrEmpty(criteria.PublicIP))
                    Instance.AddInParameter("@public_ip", SqlDbType.VarChar, criteria.PublicIP);
                if (!string.IsNullOrEmpty(criteria.UserAgent))
                    Instance.AddInParameter("@user_agent", SqlDbType.VarChar, criteria.UserAgent);
            }
            reader = Instance.ExcuteReader("dbo.SP_UserLoggin_FindAll", conn);

            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                    UserLogginList.Add(GetMapper(reader));
                reader.Close();
                return UserLogginList;
            }
            else
                return null;

        }

        public List<UserLoggin> FindByParentId<TParentId>(TParentId ParentId, Type ParentType, CriteriaBase myCriteria)
        {
            List<UserLoggin> UserLogginList = new List<UserLoggin>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);

			if (ParentType == typeof(User))
				Instance.AddInParameter("@user_id", SqlDbType.Int, ParentId);
	

            if (myCriteria != null)
            {
                UserLogginCriteria criteria = (UserLogginCriteria)myCriteria;
            }			

            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_UserLoggin_FindByParentId", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    UserLogginList.Add(GetMapper(reader));
                }
                reader.Close();
                return UserLogginList;
            }
            else
            {
                return null;
            }
        }

        public void Delete(ref UserLoggin myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@user_loggin_id", SqlDbType.Int, myEntity.UserLogginId);

            Instance.ExcuteNonQuery("dbo.SP_UserLoggin_Delete", conn, false);
        }

        public void DeleteLogical(ref UserLoggin myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@user_loggin_id", SqlDbType.Int, myEntity.UserLogginId);

            Instance.ExcuteNonQuery("dbo.SP_UserLoggin_DeleteLogical", conn, false);
        }

        public void KillUserLogging(UserLogginCriteria userLogginCriteria)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);

            if (userLogginCriteria.UserId.HasValue)
                Instance.AddInParameter("@user_id", SqlDbType.Int, userLogginCriteria.UserId.Value);

            if (!string.IsNullOrEmpty(userLogginCriteria.RandomString))
                Instance.AddInParameter("@random_string", SqlDbType.VarChar, userLogginCriteria.RandomString);
            if (!string.IsNullOrEmpty(userLogginCriteria.LocalIP))
                Instance.AddInParameter("@local_ip", SqlDbType.VarChar, userLogginCriteria.LocalIP);
            if (!string.IsNullOrEmpty(userLogginCriteria.PublicIP))
                Instance.AddInParameter("@public_ip", SqlDbType.VarChar, userLogginCriteria.PublicIP);
            if (!string.IsNullOrEmpty(userLogginCriteria.UserAgent))
                Instance.AddInParameter("@user_agent", SqlDbType.VarChar, userLogginCriteria.UserAgent);

            Instance.ExcuteNonQuery("dbo.SP_UserLoggin_Kill", conn, false);
        }

		#endregion


        public void KillUserLogging(int userId)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);

            Instance.AddInParameter("@user_id", SqlDbType.Int, userId);          
            Instance.ExcuteNonQuery("dbo.SP_UserLoggin_Kill", conn, false);
        }

        public string CustomConnectionString
        {
            get;
            set;
        }

        public bool UseCustomConnectionString
        {
            get;
            set;
        }
        public Object BeginTransaction()
        {
            return new object();
        }


        public void CommitTransaction(Object object1)
        {
        }

        public void Rollback(Object object1)
        {

        }

        public void KillConnection()
        {

        }


        public bool UseSharedSession { get; set; }
    }
}