using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using Centrix.UM.Business.Entity;
using Centrix.UM.DataAccess.IRepository;
using System.Data.SqlClient;

namespace Centrix.UM.DataAccess.Repository
{
    public partial class UserAreaRepository:IUserAreaRepository
    {

        #region IRepository<UserArea,int> Members

        public void Add(ref UserArea myEntity)
        {
             SQLDAHelper Instance = new SQLDAHelper(); 
 SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@UserId", System.Data.SqlDbType.Int, myEntity.UserId);
            Instance.AddInParameter("@AreaId", System.Data.SqlDbType.Int, myEntity.AreaId);
            Instance.AddInParameter("@createdBy", System.Data.SqlDbType.Int, myEntity.CreatedBy);

            Instance.AddOutParameter("@Id", System.Data.SqlDbType.Int);

            try
            {
                Instance.ExcuteNonQuery("dbo.SP_UserArea_Add", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            myEntity.UserAreaId = (int)Instance.GetOutParamValue("@Id");
        }

        public void Delete(ref UserArea myEntity)
        {
             SQLDAHelper Instance = new SQLDAHelper(); 
 SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@Id", System.Data.SqlDbType.Int, myEntity.UserAreaId);

            try
            {
                Instance.ExcuteNonQuery("dbo.SP_UserArea_Delete", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }            
        }

        public void DeleteLogical(ref UserArea myEntity)
        {
             SQLDAHelper Instance = new SQLDAHelper(); 
 SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@Id", System.Data.SqlDbType.Int, myEntity.UserAreaId);

            try
            {
                Instance.ExcuteNonQuery("dbo.SP_UserArea_DeleteLogical", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<UserArea> FindAll(CriteriaBase myCriteria)
        {
            List<UserArea> myUserArea = new List<UserArea>();
             SQLDAHelper Instance = new SQLDAHelper(); 
 SqlConnection conn = Instance.GetConnection(false);
            if (myCriteria != null)
            {
                //Instance.AddInParameter("@Name", System.Data.SqlDbType.Int, ((RoleCriteria)myCriteria).Name);
            }

            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_UserArea_FindAll", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    myUserArea.Add(GetMapper(reader));
                }
                reader.Close();
                return myUserArea;
            }
            else
            {
                return null;
            }
        }

        public UserArea FindById(int Id, CriteriaBase myCriteria)
        {
             SQLDAHelper Instance = new SQLDAHelper(); 
 SqlConnection conn = Instance.GetConnection(false);
            if (myCriteria != null)
            {
                //Instance.AddInParameter("@Name", System.Data.SqlDbType.Int, ((RoleCriteria)myCriteria).Name);
            }
            Instance.AddInParameter("@Id", System.Data.SqlDbType.Int, Id);

            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_UserArea_FindById", conn);
            if (reader != null && reader.HasRows)
            {
                reader.Read();
                UserArea userarea= GetMapper(reader);
                reader.Close();
                return userarea;

            }
            else
            {
                return null;
            }
        }

        public List<UserArea> FindByParentId<TParentId>(TParentId ParentId, Type ParentType, CriteriaBase myCriteria)
        {
            List<UserArea> myRolePer = new List<UserArea>();
             SQLDAHelper Instance = new SQLDAHelper(); 
 SqlConnection conn = Instance.GetConnection(false);
            if (myCriteria != null)
            {
                //Instance.AddInParameter("@Name", System.Data.SqlDbType.Int, ((RoleCriteria)myCriteria).Name);
            }
            if (ParentType == typeof(User))
            {
                Instance.AddInParameter("@UserId", System.Data.SqlDbType.Int, ParentId);

            }
            if (ParentType == typeof(Area))
            {
                Instance.AddInParameter("@AreaId", System.Data.SqlDbType.Int, ParentId);

            }
            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_UserArea_FindByParentId", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    myRolePer.Add(GetMapper(reader));
                }
                reader.Close();
                return myRolePer;
            }
            else
            {
                return null;
            }
        }

        public void Update(ref UserArea myEntity)
        {
            throw new NotImplementedException();
        }

        public string CustomConnectionString
        {
            get;
            set;
        }

        public bool UseCustomConnectionString
        {
            get;
            set;
        }
        public Object BeginTransaction()
        {
            return new object();
        }


        public void CommitTransaction(Object object1)
        {
        }

        public void Rollback(Object object1)
        {

        }

        public void KillConnection()
        {

        }


        public bool UseSharedSession { get; set; }

        #endregion
    }
}
