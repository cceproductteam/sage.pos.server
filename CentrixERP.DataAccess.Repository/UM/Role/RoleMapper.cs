using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.Framework;
using SF.Framework.Data;
using Centrix.UM.Business.Entity;
using Centrix.UM.DataAccess.IRepository;
using System.Data.SqlClient;

namespace Centrix.UM.DataAccess.Repository
{
    public partial class RoleRepository
    {
        public Role GetMapper(SqlDataReader reader)
        {
            Role myRole = new Role();
            if (reader["role_id"] != DBNull.Value)
            {
                myRole.RoleId = (int)reader["role_id"];
            }
            if (reader["role_name"] != DBNull.Value)
            {
                myRole.NameEnglish = reader["role_name"].ToString();
            }
            if (reader["role_name_ar"] != DBNull.Value)
            {
                myRole.NameArabic = reader["role_name_ar"].ToString();
            }
            if (reader["role_description"] != DBNull.Value)
            {
                myRole.Description =reader["role_description"].ToString();
            }
            if (reader["permission_level"] != DBNull.Value)
            {
                myRole.PermissionLevel = (int)reader["permission_level"];
            }
            if (reader["created_by"] != DBNull.Value)
            {
                myRole.CreatedBy = (int)reader["created_by"];
            }
            if (reader["updated_by"] != DBNull.Value)
            {
                myRole.UpdatedBy = (int)reader["updated_by"];
            }
            return myRole;
        }

        public Role_Lite GetLiteMapper(SqlDataReader reader)
        {
            Role_Lite myRole = new Role_Lite();
            if (reader["role_id"] != DBNull.Value)
            {
                myRole.Id = (int)reader["role_id"];
                myRole.value = (int)reader["role_id"];
            }
            if (reader["role_name"] != DBNull.Value)
            {
                myRole.NameEnglish = reader["role_name"].ToString();
                myRole.label = reader["role_name"].ToString();
            }
            if (reader["role_name_ar"] != DBNull.Value)
            {
                myRole.NameArabic = reader["role_name_ar"].ToString();
            }
            if (reader["role_description"] != DBNull.Value)
            {
                myRole.Description = reader["role_description"].ToString();
            }
            if (reader["permission_level"] != DBNull.Value)
            {
                myRole.PermissionLevel = (int)reader["permission_level"];
            }
            if (reader["created_by"] != DBNull.Value)
            {
                myRole.CreatedById = (int)reader["created_by"];
            }
            if (reader["updated_by"] != DBNull.Value)
            {
                myRole.UpdatedById = (int)reader["updated_by"];
            }

            if (reader["created_by_name"] != DBNull.Value)
            {
                myRole.CreatedBy = (string)reader["created_by_name"];
            }
            if (reader["updated_by_name"] != DBNull.Value)
            {
                myRole.UpdatedBy= (string)reader["updated_by_name"];
            }

            if (reader["last_rows"] != DBNull.Value)
            {
                myRole.LastRows = (bool)reader["last_rows"];
            }
            if (reader["total_count"] != DBNull.Value)
            {
                myRole.TotalRecords = (int)reader["total_count"];
            }

            if (reader["created_date"] != DBNull.Value)
            {
                myRole.CreatedDate = ((DateTime)reader["created_date"]).GetLocalTime();
            }
            if (reader["updated_date"] != DBNull.Value)
            {
                myRole.UpdatedDate = ((DateTime)reader["updated_date"]).GetLocalTime();
            }

            if (reader["permissions_updated_date"] != DBNull.Value)
            {
                myRole.PermissionsUpdatedDate = ((DateTime)reader["permissions_updated_date"]).GetLocalTime();
            }

            if (reader["users_count"] != DBNull.Value)
            {
                myRole.UsersCount = (int)reader["users_count"];
            }

            return myRole;
        }

        //public Role_Lite GetLiteMapper(SqlDataReader reader)
        //{
        //    Role_Lite myRole = new Role_Lite();
        //    if (reader["role_id"] != DBNull.Value)
        //    {
        //        myRole.Id = (int)reader["role_id"];
        //        myRole.value = (int)reader["role_id"];
        //    }
        //    if (reader["role_name"] != DBNull.Value)
        //    {
        //        myRole.NameEnglish = reader["role_name"].ToString();
        //        myRole.label = reader["role_name"].ToString();
        //    }
        //    if (reader["role_name_ar"] != DBNull.Value)
        //    {
        //        myRole.NameArabic = reader["role_name_ar"].ToString();
        //    }
        //    if (reader["role_description"] != DBNull.Value)
        //    {
        //        myRole.Description = reader["role_description"].ToString();
        //    }
        //    if (reader["permission_level"] != DBNull.Value)
        //    {
        //        myRole.PermissionLevel = (int)reader["permission_level"];
        //    }
        //    if (reader["created_by"] != DBNull.Value)
        //    {
        //        myRole.CreatedById = (int)reader["created_by"];
        //    }
        //    if (reader["updated_by"] != DBNull.Value)
        //    {
        //        myRole.UpdatedById = (int)reader["updated_by"];
        //    }

        //    if (reader["created_by_name"] != DBNull.Value)
        //    {
        //        myRole.CreatedBy = (string)reader["created_by_name"];
        //    }
        //    if (reader["updated_by_name"] != DBNull.Value)
        //    {
        //        myRole.UpdatedBy = (string)reader["updated_by_name"];
        //    }

        //    if (reader["last_rows"] != DBNull.Value)
        //    {
        //        myRole.LastRows = (bool)reader["last_rows"];
        //    }
        //    if (reader["total_count"] != DBNull.Value)
        //    {
        //        myRole.TotalRecords = (int)reader["total_count"];
        //    }

        //    if (reader["created_date"] != DBNull.Value)
        //    {
        //        myRole.CreatedDate = ((DateTime)reader["created_date"]).GetLocalTime();
        //    }
        //    if (reader["updated_date"] != DBNull.Value)
        //    {
        //        myRole.UpdatedDate = ((DateTime)reader["updated_date"]).GetLocalTime();
        //    }

        //    if (reader["permissions_updated_date"] != DBNull.Value)
        //    {
        //        myRole.PermissionsUpdatedDate = ((DateTime)reader["permissions_updated_date"]).GetLocalTime();
        //    }

        //    return myRole;
        //}
    }
}
