using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using Centrix.UM.Business.Entity;
using Centrix.UM.DataAccess.IRepository;

namespace Centrix.UM.DataAccess.Repository
{
    public partial class PermissionExceptionsRepository : IPermissionExceptionsRepository
    {

        #region IRepository<PermissionExceptions,int> Members

        public void Add(ref PermissionExceptions myEntity)
        {
            throw new NotImplementedException();
        }

        public void Delete(ref PermissionExceptions myEntity)
        {
            throw new NotImplementedException();
        }

        public void DeleteLogical(ref PermissionExceptions myEntity)
        {
            throw new NotImplementedException();
        }

        public List<PermissionExceptions> FindAll(CriteriaBase myCriteria)
        {
            throw new NotImplementedException();
        }

        public PermissionExceptions FindById(int Id, CriteriaBase myCriteria)
        {
            throw new NotImplementedException();
        }

        public List<PermissionExceptions> FindByParentId<TParentId>(TParentId ParentId, Type ParentType, CriteriaBase myCriteria)
        {
            throw new NotImplementedException();
        }

        public void Update(ref PermissionExceptions myEntity)
        {
            throw new NotImplementedException();
        }


        public string CustomConnectionString
        {
            get;
            set;
        }

        public bool UseCustomConnectionString
        {
            get;
            set;
        }
        public Object BeginTransaction()
        {
            return new object();
        }


        public void CommitTransaction(Object object1)
        {
        }

        public void Rollback(Object object1)
        {

        }

        public void KillConnection()
        {

        }


        public bool UseSharedSession { get; set; }

        #endregion
    }
}
