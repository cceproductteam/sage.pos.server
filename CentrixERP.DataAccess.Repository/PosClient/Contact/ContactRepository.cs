
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using SagePOS.Server.Business.Entity;
using SagePOS.Server.DataAcces.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace SagePOS.Server.DataAcces.Repository
{
    public partial class ContactRepository : RepositoryBaseClass<Contact, ContactLite, int>, IContactRepository
    {
        #region IRepository< Contact, int > Members

        public ContactRepository()
        {
            this.AddSPName = "SP_Contact_Add";
            this.UpdateSPName = "SP_Contact_Update";
            this.DeleteSPName = "SP_Contact_Delete";
            this.DeleteLogicalSPName = "SP_Contact_DeleteLogical";
            this.FindAllSPName = "SP_Contact_FindAll";
            this.FindByIdSPName = "SP_Contact_FindById";
            this.FindByParentSPName = "SP_Contact_FindByParentId";
            this.FindAllLiteSPName = "SP_Contact_FindAll_Lite";
            this.FindByIdLiteSPName = "SP_Contact_FindById_Lite";
            this.AdvancedSearchSPName = "SP_Contact_AdvancedSearch_Lite";
        }

		#endregion
    }
}