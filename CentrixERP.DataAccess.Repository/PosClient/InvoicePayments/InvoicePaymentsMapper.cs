using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using Centrix.POS.Standalone.Client.Business.Entity;
using SagePOS.Server.Business.Entity;
using SF.Framework.Data;
using System.Data.SqlClient;

namespace SagePOS.Server.DataAcces.Repository
{
    public partial class InvoicePaymentsRepository
    {
        //public InvoicePayments GetMapper(SqlDataReader reader)
        //{
        //    InvoicePayments obj = new InvoicePayments();
        //    //Redaer//Values
        //    return obj;
        //}

        // public InvoicePaymentsLite GetMapperAllLite(SqlDataReader reader)
        //{
        //    InvoicePaymentsLite obj = new InvoicePaymentsLite();
        //    //Redaer//Values
        //    obj.Id = obj.InvoicePaymentsId;
        //    return obj;
        //}

        // public InvoicePaymentsLite GetLiteMapper(SqlDataReader reader)
        //{
        //    InvoicePaymentsLite obj = new InvoicePaymentsLite();
        //    //Redaer//Values
        //         if (reader["created_by_name"] != DBNull.Value)
        //    {
        //        obj.CreatedByName = (string)reader["created_by_name"];
        //    }
        //    if (reader["updated_by_name"] != DBNull.Value)
        //    {
        //        obj.UpdatedByName = (string)reader["updated_by_name"];
        //    }
        //    return obj;
        //}

        public InvoicePaymentIntegration GetInvoicePaymentMapper(SqlDataReader reader) {
            InvoicePaymentIntegration obj = new InvoicePaymentIntegration();
            if (reader["invoice_unique_number"] != DBNull.Value)
                obj.InvoiceUnqiueNumber = reader["invoice_unique_number"].ToString();
            if (reader["amount_default_currency"] != DBNull.Value)
                obj.AmountDefaultCurrency = Convert.ToDouble(reader["amount_default_currency"]);
            if (reader["available_amount"] != DBNull.Value)
                obj.AvailableAmount = Convert.ToDouble(reader["available_amount"]);
            if (reader["currency"] != DBNull.Value)
                obj.Currency = reader["currency"].ToString();
            if (reader["currency_rate"] != DBNull.Value)
                obj.CurrencyRate = Convert.ToDouble(reader["currency_rate"]);
            if (reader["cash_bank"] != DBNull.Value)
                obj.CashBank = reader["cash_bank"].ToString();
            if (reader["visa_bank"] != DBNull.Value)
                obj.VisaBank = reader["visa_bank"].ToString();
            if (reader["master_bank"] != DBNull.Value)
                obj.MasterBank = reader["master_bank"].ToString();
            if (reader["amex_bank"] != DBNull.Value)
                obj.AmexBank = reader["amex_bank"].ToString();
            if (reader["national_bank"] != DBNull.Value)
                obj.NationalBank = reader["national_bank"].ToString();
            if (reader["cheque_bank"] != DBNull.Value)
                obj.ChequeBank = reader["cheque_bank"].ToString();
            if (reader["cash_customer"] != DBNull.Value)
                obj.CashCustomer = reader["cash_customer"].ToString();
            if (reader["accpac_orderentry_no"] != DBNull.Value)
                obj.AccpacOrderEntryNumber = reader["accpac_orderentry_no"].ToString();
            if (reader["sync_number"] != DBNull.Value)
                obj.SyncNumber = reader["sync_number"].ToString();
            if (reader["CreatedDate"] != DBNull.Value)
                obj.CreatedDate = (DateTime)reader["CreatedDate"];
            if (reader["PaymentType"] != DBNull.Value)
                obj.PaymentType = (int)reader["PaymentType"];
            if (reader["CreditCardType"] != DBNull.Value)
                obj.CreditCardType = reader["CreditCardType"].ToString();

            return obj;
        }
    }
}
