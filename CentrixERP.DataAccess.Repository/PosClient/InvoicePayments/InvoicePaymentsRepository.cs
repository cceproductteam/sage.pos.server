
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
//using Centrix.POS.Standalone.Client.Business.Entity;
//using Centrix.POS.Standalone.Client.DataAccess.IRepository;
using SagePOS.Server.Business.Entity;
using SagePOS.Server.DataAcces.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace SagePOS.Server.DataAcces.Repository
{
    public partial class InvoicePaymentsRepository : RepositoryBaseClass<InvoicePayments, InvoicePaymentsLite, int>, IInvoicePaymentsRepository
    {
        #region IRepository< InvoicePayments, int > Members

        public InvoicePaymentsRepository()
        {
            this.AddSPName = "SP_InvoicePayments_Add";
            this.UpdateSPName = "SP_InvoicePayments_Update";
            this.DeleteSPName = "SP_InvoicePayments_Delete";
            this.DeleteLogicalSPName = "SP_InvoicePayments_DeleteLogical";
            this.FindAllSPName = "SP_InvoicePayments_FindAll";
            this.FindByIdSPName = "SP_InvoicePayments_FindById";
            this.FindByParentSPName = "SP_InvoicePayments_FindByParentId";
            this.FindAllLiteSPName = "SP_InvoicePayments_FindAll_Lite";
            this.FindByIdLiteSPName = "SP_InvoicePayments_FindById_Lite";
            this.AdvancedSearchSPName = "SP_InvoicePayments_AdvancedSearch_Lite";
        }




        public int GetParentInvoiceId(int refundInvoiceId)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);

            Instance.AddInParameter("@refund_invoice_id", SqlDbType.Int, refundInvoiceId);
            Instance.AddOutParameter("@parent_invoice_id", SqlDbType.Int);

            Instance.ExcuteNonQuery("dbo.SP_ParentInvoiceId_FindByRefundInvoiceId", conn, false);

            return (int)Instance.GetOutParamValue("@parent_invoice_id");


        }

        public List<InvoicePaymentIntegration> InvoicePaymentIntgration(string InvoiceUniqueNumber)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@invoice_unique_number", SqlDbType.NVarChar, InvoiceUniqueNumber);

            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_InvoicePayment_Integration", conn);
            List<InvoicePaymentIntegration> mylist = new List<InvoicePaymentIntegration>();
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    mylist.Add(GetInvoicePaymentMapper(reader));

                }
                return mylist;
            }
            else
                return null;
        }

        #endregion
    }
}