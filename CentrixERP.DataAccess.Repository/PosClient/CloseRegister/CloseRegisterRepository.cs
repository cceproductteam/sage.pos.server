
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;


using SagePOS.Server.Business.Entity;
using SagePOS.Server.DataAcces.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace SagePOS.Server.DataAcces.Repository
{
    public partial class CloseRegisterRepository : RepositoryBaseClass<CloseRegister, CloseRegisterLite, int>, ICloseRegisterRepository
    {
        #region IRepository< CloseRegister, int > Members

        public CloseRegisterRepository()
        {
            this.AddSPName = "SP_CloseRegister_Add";
            this.UpdateSPName = "SP_CloseRegister_Update";
            this.DeleteSPName = "SP_CloseRegister_Delete";
            this.DeleteLogicalSPName = "SP_CloseRegister_DeleteLogical";
            this.FindAllSPName = "SP_CloseRegister_FindAll";
            this.FindByIdSPName = "SP_CloseRegister_FindById";
            this.FindByParentSPName = "SP_CloseRegister_FindByParentId";
            this.FindAllLiteSPName = "SP_CloseRegister_FindAll_Lite";
            this.FindByIdLiteSPName = "SP_CloseRegister_FindById_Lite";
            this.AdvancedSearchSPName = "SP_CloseRegister_AdvancedSearch_Lite";
        }

		#endregion


        public List<CloseRegiserDetails> GetCloseRegisterDetailsPaymentType(CriteriaBase myCriteria)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;

            List<CloseRegiserDetails> myList = new List<CloseRegiserDetails>();
            if (myCriteria != null)
            {
                CloseRegisterCriteria criteria = (CloseRegisterCriteria)myCriteria;
                Instance.AddInParameter("@register_id", SqlDbType.Int, criteria.RegisterId);
                Instance.AddInParameter("@user_id", SqlDbType.Int, criteria.UserId);
                Instance.AddInParameter("@close_register_id", SqlDbType.Int, criteria.CloseRegisterId);
                Instance.AddInParameter("@store_id", SqlDbType.Int, criteria.StoreId);

            }
            reader = Instance.ExcuteReader("dbo.SP_Close_Register_PaymentType", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    myList.Add(GetPaymentTypeMapper(reader));
                }
            }
            return myList;
        }

        public void UpdateSyncStatus(string trasactionNumber, int EntityId)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            if (UseCustomConnectionString)
                Instance.SetConnectionString(CustomConnectionString);
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@transaction_number", SqlDbType.NVarChar, trasactionNumber);
            Instance.AddInParameter("@entity_id", SqlDbType.Int, EntityId);

            Instance.ExcuteNonQuery("SP_UpdateSyncStatus", conn, false);
        }

        public List<CloseRegisterInquiryLite> CloseRegisterInquiryResults(string StoreIds, string RegisterIds, DateTime? OpenedDateFrom, DateTime? OpenedDateTo, DateTime? ClosedDateFrom, DateTime? ClosedDateTo)
        {
            List<CloseRegisterInquiryLite> resultsList = new List<CloseRegisterInquiryLite>();
            SQLDAHelper instance = new SQLDAHelper();
            SqlConnection conn = instance.GetConnection(false);
            instance.AddInParameter("@store_ids", SqlDbType.NVarChar, StoreIds);
            instance.AddInParameter("@register_ids", SqlDbType.NVarChar, RegisterIds);

            instance.AddInParameter("@open_date_from", SqlDbType.NVarChar, OpenedDateFrom);
            instance.AddInParameter("@open_date_to", SqlDbType.NVarChar, OpenedDateTo);

            instance.AddInParameter("@close_date_from", SqlDbType.NVarChar, ClosedDateFrom);
            instance.AddInParameter("@close_date_to", SqlDbType.NVarChar, ClosedDateTo);

            SqlDataReader reader = instance.ExcuteReader("SP_ERP_Integration_CloseRegister_Inquiry", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    resultsList.Add(GetCloseRegisterInquiryMapper(reader));
                }
                return resultsList;
            }
            else
            {
                return null;
            }
        }


        public List<CloseRegisterInquiryLiteNew> CloseRegisterInquiryResultsNew(string StoreId, string RegisterId, string fromdate, string todate)
        {
            List<CloseRegisterInquiryLiteNew> resultsList = new List<CloseRegisterInquiryLiteNew>();
            SQLDAHelper instance = new SQLDAHelper();
            SqlConnection conn = instance.GetConnection(false);
            instance.AddInParameter("@store_id", SqlDbType.NVarChar, StoreId);
            instance.AddInParameter("@register_id", SqlDbType.NVarChar, RegisterId);
            instance.AddInParameter("@date_from", SqlDbType.NVarChar, fromdate);
            instance.AddInParameter("@date_to", SqlDbType.NVarChar, todate);
            //instance.AddInParameter("@open_date_from", SqlDbType.NVarChar, OpenedDateFrom);
            //instance.AddInParameter("@open_date_to", SqlDbType.NVarChar, OpenedDateTo);

            //instance.AddInParameter("@close_date_from", SqlDbType.NVarChar, ClosedDateFrom);
            //instance.AddInParameter("@close_date_to", SqlDbType.NVarChar, ClosedDateTo);

            SqlDataReader reader = instance.ExcuteReader("SP_CloseRegister_Report_Inquiry", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    resultsList.Add(GetCloseRegisterInquiryMapperNew(reader));
                }
                return resultsList;
            }
            else
            {
                return null;
            }
        }

        public CloseRegisterTotals FindCloseRegisterSalesTotal(int CloseRegisterId)
        {
            CloseRegisterTotals resultsList = new CloseRegisterTotals();
            SQLDAHelper instance = new SQLDAHelper();
            SqlConnection conn = instance.GetConnection(false);
            instance.AddInParameter("@close_register_id", SqlDbType.Int, CloseRegisterId);
            SqlDataReader reader = instance.ExcuteReader("SP_CloseRegister_Sales_Totals", conn);
            if (reader != null && reader.HasRows)
            {
                reader.Read();
                resultsList = GetCloseRegisterTotalsMapper(reader);
                return resultsList;

            }
            else
                return null;
        
        }


    }
}