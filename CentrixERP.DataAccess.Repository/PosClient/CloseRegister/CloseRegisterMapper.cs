using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SagePOS.Server.Business.Entity;
using SF.Framework.Data;
using System.Data.SqlClient;

namespace SagePOS.Server.DataAcces.Repository
{
    public partial class CloseRegisterRepository
    {
        //public CloseRegister GetMapper(SqlDataReader reader)
        //{
        //    CloseRegister obj = new CloseRegister();
        //    //Redaer//Values
        //    return obj;
        //}

        // public CloseRegisterLite GetMapperAllLite(SqlDataReader reader)
        //{
        //    CloseRegisterLite obj = new CloseRegisterLite();
        //    //Redaer//Values
        //    obj.Id = obj.CloseRegisterId;
        //    return obj;
        //}

        // public CloseRegisterLite GetLiteMapper(SqlDataReader reader)
        //{
        //    CloseRegisterLite obj = new CloseRegisterLite();
        //    //Redaer//Values
        //         if (reader["created_by_name"] != DBNull.Value)
        //    {
        //        obj.CreatedByName = (string)reader["created_by_name"];
        //    }
        //    if (reader["updated_by_name"] != DBNull.Value)
        //    {
        //        obj.UpdatedByName = (string)reader["updated_by_name"];
        //    }
        //    return obj;
        //}

        public CloseRegiserDetails GetPaymentTypeMapper(SqlDataReader reader)
        {
            CloseRegiserDetails obj = new CloseRegiserDetails();
            if (reader["TotalPrice"] != DBNull.Value)
            {
                obj.TotalPrice = Convert.ToDecimal(reader["TotalPrice"]);
            }

            if (reader["payment_type"] != DBNull.Value)
            {
                obj.PaymentType = (int)reader["payment_type"];
            }
            //if (reader["tax_amount"]!=DBNull.Value)
            //{
            //    obj.DefaultTax = Convert.ToDouble(reader["tax_amount"]);
            //}
            if (reader["Currency"] != DBNull.Value)
            {
                obj.Currency = reader["Currency"].ToString();
            }
            if (reader["paymentType_name"] != DBNull.Value)
            {
                obj.PaymentTypeName = (string)reader["paymentType_name"];
            }
            if (reader["CurrencyDefaultPrice"] != DBNull.Value)
            {
                obj.CurrencyDefaultPrice = Convert.ToDouble(reader["CurrencyDefaultPrice"]);
            }
            if (reader["cash_loan"] != DBNull.Value)
            {
                obj.CashLoan = Convert.ToDouble(reader["cash_loan"]);
            }
            //if (reader["is_layby"] != DBNull.Value)
            //{
            //    obj.IsLayBy = (bool)reader["is_layby"];
            //}
            if (reader["invoice_status"] != DBNull.Value)
            {
                obj.InvoiceStatus = (int)reader["invoice_status"];
            }
            if (reader["credit_card_type"] != DBNull.Value)
            {
                obj.CreditCardType = reader["credit_card_type"].ToString();
            }
            if (reader["invoice_count"] != DBNull.Value)
            {
                obj.InvoiceCount = (int)reader["invoice_count"];
            }
            return obj;
        }


        public CloseRegisterInquiryLite GetCloseRegisterInquiryMapper(SqlDataReader reader)
        {
            CloseRegisterInquiryLite obj = new CloseRegisterInquiryLite();

            if (reader["close_register_id"] != DBNull.Value)
            {
                obj.CloseRegisterId = (int)reader["close_register_id"];
            }

            if (reader["store_id"] != DBNull.Value)
            {
                obj.StoreId = (int)reader["store_id"];
            }
            if (reader["row_num"] != DBNull.Value)
            {
                obj.RowNum = (int)reader["row_num"];
            }


            if (reader["store_name"] != DBNull.Value)
            {
                obj.StoreName = Convert.ToString(reader["store_name"]);
            }
            if (reader["opened_date"] != DBNull.Value)
            {
                obj.OpenedDate = Convert.ToDateTime(reader["opened_date"]);
            }

            if (reader["closed_date"] != DBNull.Value)
            {
                obj.ClosedDate = Convert.ToDateTime(reader["closed_date"]);
            }

            if (reader["opened_cash_loan"] != DBNull.Value)
            {
                obj.OpenedCashLoan = Convert.ToDouble(reader["opened_cash_loan"]);
            }
            if (reader["total_price"] != DBNull.Value)
            {
                obj.TotalPrice = Convert.ToDecimal(reader["total_price"]);
            }
            if (reader["identical"] != DBNull.Value)
            {
                obj.IsIdentical = Convert.ToString(reader["identical"]);
            }
            if (reader["diff_amount"] != DBNull.Value)
            {
                obj.DifferenceAmount = Convert.ToDouble(reader["diff_amount"]);
            }
            return obj;
        }



        public CloseRegisterInquiryLiteNew GetCloseRegisterInquiryMapperNew(SqlDataReader reader)
        {
            CloseRegisterInquiryLiteNew obj = new CloseRegisterInquiryLiteNew();

            if (reader["close_register_id"] != DBNull.Value)
            {
                obj.CloseRegisterId = (int)reader["close_register_id"];
            }

            if (reader["store_id"] != DBNull.Value)
            {
                obj.StoreId = (int)reader["store_id"];
            }


            if (reader["register_id"] != DBNull.Value)
            {
                obj.RegisterId = (int)reader["register_id"];
            }

            

                            if (reader["session_number"] != DBNull.Value)
            {
                obj.SessionNumber = (string)reader["session_number"];
            }

            if (reader["register_name"] != DBNull.Value)
            {
                obj.RegisterName = (string)reader["register_name"];
            }
         
                           if (reader["amountSum"] != DBNull.Value)
            {
                obj.amountSum = (decimal)reader["amountSum"];
            }
            
   

                                           if (reader["users_username"] != DBNull.Value)
            {
                obj.UserName = (string)reader["users_username"];
            }
            
            if (reader["row_num"] != DBNull.Value)
            {
                obj.RowNum = (int)reader["row_num"];
            }

        
            if (reader["store_name"] != DBNull.Value)
            {
                obj.StoreName = Convert.ToString(reader["store_name"]);
            }
            if (reader["opened_date"] != DBNull.Value)
            {
                obj.OpenedDate = Convert.ToString(reader["opened_date"]);

            }
            if (reader["default_curr_symbol"] != DBNull.Value)
            {
                obj.defaultcurrsymbol = (string)reader["default_curr_symbol"];
            }

            if (reader["closed_date"] != DBNull.Value)
            {
                obj.ClosedDate = Convert.ToString(reader["closed_date"]);
            }

            if (reader["opened_cash_loan"] != DBNull.Value)
            {
                obj.OpenedCashLoan = Convert.ToDecimal(reader["opened_cash_loan"]);
            }
            if (reader["total_price"] != DBNull.Value)
            {
                obj.TotalPrice = Convert.ToDecimal(reader["total_price"]);
            }
            if (reader["identical"] != DBNull.Value)
            {
                obj.IsIdentical = Convert.ToString(reader["identical"]);
            }
            if (reader["diff_amount"] != DBNull.Value)
            {
                obj.DifferenceAmount = Convert.ToDouble(reader["diff_amount"]);
            }
            return obj;
        }
        public CloseRegisterTotals GetCloseRegisterTotalsMapper(SqlDataReader reader)
        {
            CloseRegisterTotals Obj = new CloseRegisterTotals();

            if (reader["sales"] != DBNull.Value)
            {
                Obj.SalesAmount = Convert.ToDouble(reader["sales"]);

            }
            if (reader["refund"] != DBNull.Value)
            {
                Obj.RefundAmount = Convert.ToDouble(reader["refund"]);

            }
            if (reader["exchange"] != DBNull.Value)
            {
                Obj.ExchangeAmount = Convert.ToDouble(reader["exchange"]);

            }
            if (reader["depoist"] != DBNull.Value)
            {
                Obj.Deposit = Convert.ToDouble(reader["depoist"]);

            }
            if (reader["withdrawal"] != DBNull.Value)
            {
                Obj.Withdrawal = Convert.ToDouble(reader["withdrawal"]);

            }

            return Obj;

        }

    }
}
