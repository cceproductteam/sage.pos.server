using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SagePOS.Server.Business.Entity;
using SF.Framework.Data;
using System.Data.SqlClient;

namespace SagePOS.Server.DataAcces.Repository
{


    public partial class InvoiceRepository
    {
        //public Invoice GetMapper(SqlDataReader reader)
        //{
        //    Invoice obj = new Invoice();
        //    //Redaer//Values
        //    return obj;s
        //}



        public InvoiceIntegration GetIntegrationMapper(SqlDataReader reader)
        {
            InvoiceIntegration obj = new InvoiceIntegration();
            if (reader["parent_invoice_id"] != DBNull.Value)
                obj.ParentInvoiceId = (int)reader["parent_invoice_id"];

            if (reader["invoice_id"] != DBNull.Value)
                obj.InvoiceId = (int)reader["invoice_id"];

            if (reader["invoice_date"] != DBNull.Value)
                obj.InvoiceDate = (DateTime)reader["invoice_date"];

            if (reader["invoice_unique_number"] != DBNull.Value)
                obj.InvoiceUniqueNumber = reader["invoice_unique_number"].ToString();

            if (reader["grand_total"] != DBNull.Value)
                obj.GrandTotal = Convert.ToDouble(reader["grand_total"]);

            if (reader["amount_discount"] != DBNull.Value)
                obj.AmountDiscount = Convert.ToDouble(reader["amount_discount"]);

            if (reader["discount_percent"] != DBNull.Value)
                obj.PercentageDiscount = Convert.ToDouble(reader["discount_percent"]);

            if (reader["invoice_number"] != DBNull.Value)
                obj.InvoiceNumber = reader["invoice_number"].ToString();








            if (reader["location_id"] != DBNull.Value)
                obj.LocationId = (int)reader["location_id"];




            if (reader["tax_class_id"] != DBNull.Value)
                obj.TaxClassId = Convert.ToInt32(reader["tax_class_id"]);
            if (reader["total_discount"] != DBNull.Value)
                obj.TotalDiscountAmount = Convert.ToDouble(reader["total_discount"]);
            if (reader["tax"] != DBNull.Value)
                obj.TotalTaxAmount = Convert.ToDouble(reader["tax"]);
            if (reader["default_currency"] != DBNull.Value)
                obj.CurrencyId = Convert.ToInt32(reader["default_currency"]);

            if (reader["grand_total"] != DBNull.Value)
                obj.TotalAmount = Convert.ToDouble(reader["grand_total"]);

            if (reader["non_taxable_class_id"] != DBNull.Value)
                obj.NonTaxableClassId = Convert.ToInt32(reader["non_taxable_class_id"]);

            if (reader["credit_note_account_ids"] != DBNull.Value)
                obj.CreditNoteAccountIds = reader["credit_note_account_ids"].ToString();

            if (reader["change_amount"] != DBNull.Value)
                obj.ChangeAmount = Convert.ToDouble(reader["change_amount"]);

            if (reader["invoice_status"] != DBNull.Value)
                obj.InvoiceStatus = (int)reader["invoice_status"];
            return obj;
        }

        public InvoiceInquiryLite GetInvoiceInquiryMapper(SqlDataReader reader)
        {
            InvoiceInquiryLite obj = new InvoiceInquiryLite();

            if (reader["invoice_id"] != DBNull.Value)
            {
                obj.InvoiceId = (int)reader["invoice_id"];
            }



            if (reader["invoice_number"] != DBNull.Value)
            {
                obj.InvoiceNumber = Convert.ToString(reader["invoice_number"]);
            }

            if (reader["status"] != DBNull.Value)
            {
                obj.Type = Convert.ToString(reader["status"]);
            }

            if (reader["invoice_date"] != DBNull.Value)
            {
                obj.InvoiceDate = Convert.ToDateTime(reader["invoice_date"]);
            }

            if (reader["shipment_number"] != DBNull.Value)
            {
                obj.ShipmentNumber = Convert.ToString(reader["shipment_number"]);
            }

            if (reader["grand_total"] != DBNull.Value)
            {
                obj.Total = Convert.ToDouble(reader["grand_total"]);
            }

            if (reader["store_name"] != DBNull.Value)
            {
                obj.Store = Convert.ToString(reader["store_name"]);
            }


            if (reader["register_name"] != DBNull.Value)
            {
                obj.Register = Convert.ToString(reader["register_name"]);
            }

            if (reader["row_num"] != DBNull.Value)
            {
                obj.RowNum = (int)reader["row_num"];
            }


            if (reader["shipment_id"] != DBNull.Value)
            {
                obj.ShipmentId = (int)reader["shipment_id"];
            }
            if (reader["cash_amount"] != DBNull.Value)
            {
                obj.CashAmount = Convert.ToDouble(reader["cash_amount"]);
            }
            if (reader["credit_card_amount"] != DBNull.Value)
            {
                obj.CreditCardAmount = Convert.ToDouble(reader["credit_card_amount"]);
            }
            if (reader["cheque_amount"] != DBNull.Value)
            {
                obj.ChequeAmount = Convert.ToDouble(reader["cheque_amount"]);
            }
            if (reader["on_account_amount"] != DBNull.Value)
            {
                obj.OnAccountAmount = Convert.ToDouble(reader["on_account_amount"]);
            }
            if (reader["total_payment"] != DBNull.Value)
            {
                obj.TotalPayment = Convert.ToDouble(reader["total_payment"]);
            }
            if (reader["total_price"] != DBNull.Value)
            {
                obj.TotalPrice = Convert.ToDouble(reader["total_price"]);
            }
            if (reader["tax"] != DBNull.Value)
            {
                obj.Tax = Convert.ToDouble(reader["tax"]);
            }
            if (reader["amount_discount"] != DBNull.Value)
            {
                obj.AmountDiscount = Convert.ToDouble(reader["amount_discount"]);
            }
            if (reader["items_discount"] != DBNull.Value)
            {
                obj.ItemsDiscount = Convert.ToDouble(reader["items_discount"]);
            }
            if (reader["Currency"] != DBNull.Value)
            {
                obj.Currency = reader["Currency"].ToString();
            }
            return obj;

        }


        public POSQuantityInQuiryLite GetQuantityInquiryMapper(SqlDataReader reader)
        {
            POSQuantityInQuiryLite obj = new POSQuantityInQuiryLite();

            if (reader["item_number"] != DBNull.Value)
            {
                obj.ItemNumber = reader["item_number"].ToString();
            }
            if (reader["location_name"] != DBNull.Value)
            {
                obj.LocationName = reader["location_name"].ToString();
            }
            if (reader["item_name"] != DBNull.Value)
            {
                obj.ItemName = reader["item_name"].ToString();
            }
            if (reader["erp_quantity"] != DBNull.Value)
            {
                obj.ERPQuantity = (int)reader["erp_quantity"];
            }
            if (reader["staging_quantity"] != DBNull.Value)
            {
                obj.StagingQuantity = (int)reader["staging_quantity"];
            }
            if (reader["pos_quantity"] != DBNull.Value)
            {
                obj.POSQuantity = (int)reader["pos_quantity"];
            }
            if (reader["row_num"] != DBNull.Value)
            {
                obj.RowNum = (int)reader["row_num"];
            }

            if (reader["StoreName"] != DBNull.Value)
            {
                obj.StoreName = reader["StoreName"].ToString();
            }
            if (reader["pendingQTY"] != DBNull.Value)
            {
                obj.PendingQTY = (int)reader["pendingQTY"];
            }
            if (reader["summationQTY"] != DBNull.Value)
            {
                obj.SummationQTY = (int)reader["summationQTY"];
            }
            if (reader["staging_out_quantity"] != DBNull.Value)
            {
                obj.StagingOutQuantity = (int)reader["staging_out_quantity"];
            }
            if (reader["pendingOutQTY"] != DBNull.Value)
            {
                obj.PendingOutQuantity = (int)reader["pendingOutQTY"];
            }

            return obj;

        }

        public ICQuantityInQuiryLite GetICQuantityInquiryMapper(SqlDataReader reader)
        {
            ICQuantityInQuiryLite obj = new ICQuantityInQuiryLite();

            if (reader["item_number"] != DBNull.Value)
            {
                obj.ItemNumber = reader["item_number"].ToString();
            }
            if (reader["location_name"] != DBNull.Value)
            {
                obj.LocationName = reader["location_name"].ToString();
            }
            if (reader["item_name"] != DBNull.Value)
            {
                obj.ItemName = reader["item_name"].ToString();
            }
            if (reader["erp_quantity"] != DBNull.Value)
            {
                obj.ERPQuantity = (int)reader["erp_quantity"];
            }
            if (reader["staging_quantity"] != DBNull.Value)
            {
                obj.StagingQuantity = (int)reader["staging_quantity"];
            }
            if (reader["pos_quantity"] != DBNull.Value)
            {
                obj.POSQuantity = (int)reader["pos_quantity"];
            }
            if (reader["row_num"] != DBNull.Value)
            {
                obj.RowNum = (int)reader["row_num"];
            }

            if (reader["StoreName"] != DBNull.Value)
            {
                obj.StoreName = reader["StoreName"].ToString();
            }
            if (reader["pendingQTY"] != DBNull.Value)
            {
                obj.PendingQTY = (int)reader["pendingQTY"];
            }
            if (reader["summationQTY"] != DBNull.Value)
            {
                obj.SummationQTY = (int)reader["summationQTY"];
            }
            if (reader["staging_out_quantity"] != DBNull.Value)
            {
                obj.StagingOutQuantity = (int)reader["staging_out_quantity"];
            }
            if (reader["pendingOutQTY"] != DBNull.Value)
            {
                obj.PendingOutQuantity = (int)reader["pendingOutQTY"];
            }
            if (reader["avg_cost"] != DBNull.Value)
            {
                obj.AverageCost = Convert.ToDouble(reader["avg_cost"]);
            }

            return obj;

        }

        public POSDailyRecieptsReport GetTotalDaiySalesMapper(SqlDataReader reader)
        {
            POSDailyRecieptsReport obj = new POSDailyRecieptsReport();

            if (reader["invoice_date"] != DBNull.Value)
            {
                obj.Date = reader["invoice_date"].ToString();
            }

            if (reader["store_name"] != DBNull.Value)
            {
                obj.StoreName = reader["store_name"].ToString();
            }

            if (reader["register_name"] != DBNull.Value)
            {
                obj.RegisterName = reader["register_name"].ToString();
            }
            if (reader["paymentType_name"] != DBNull.Value)
            {
                obj.PaymentTypeName = reader["paymentType_name"].ToString();
            }
            if (reader["amount"] != DBNull.Value)
            {
                obj.Amount = reader["amount"].ToString();
            }

            if (reader["total"] != DBNull.Value)
            {
                obj.Total = reader["total"].ToString();
            }
            if (reader["currency"] != DBNull.Value)
            {
                obj.Currency = reader["currency"].ToString();
            }
            if (reader["amount_default_currency"] != DBNull.Value)
            {
                obj.AmountStoreCurrency = reader["amount_default_currency"].ToString();
            }
            if (reader["store_currency"] != DBNull.Value)
            {
                obj.StoreCurrency = reader["store_currency"].ToString();
            }
            if (reader["transaction_number"] != DBNull.Value)
            {
                obj.TransactionNumber = reader["transaction_number"].ToString();
            }

            return obj;
        }


        public POSDailySalesReport GetPOSDaiySalesMapper(SqlDataReader reader)
        {

            POSDailySalesReport obj = new POSDailySalesReport();

            if (reader["invoice_number"] != DBNull.Value)
            {
                obj.InvoiceNumber = reader["invoice_number"].ToString();
            }
            if (reader["invoice_date"] != DBNull.Value)
            {
                obj.InvoiceDate = reader["invoice_date"].ToString();
            }

            if (reader["store_name"] != DBNull.Value)
            {
                obj.StoreName = reader["store_name"].ToString();
            }

            if (reader["transaction_type"] != DBNull.Value)
            {
                obj.TransactionType = reader["transaction_type"].ToString();
            }
            if (reader["item_no"] != DBNull.Value)
            {
                obj.ItemNumber = reader["item_no"].ToString();
            }

            if (reader["product_name"] != DBNull.Value)
            {
                obj.ProductName = reader["product_name"].ToString();
            }

            if (reader["quantity"] != DBNull.Value)
            {
                obj.Quantity = reader["quantity"].ToString();
            }
            if (reader["orginal_price"] != DBNull.Value)
            {
                obj.OriginalPrice = reader["orginal_price"].ToString();
            }
            if (reader["unit_price"] != DBNull.Value)
            {
                obj.UnitPrice = reader["unit_price"].ToString();
            }
            if (reader["amount_discount"] != DBNull.Value)
            {
                obj.ItemDiscount = reader["amount_discount"].ToString();
            }

            if (reader["invoice_discount_per_item"] != DBNull.Value)
            {
                obj.InvoiceDiscountPerItem = reader["invoice_discount_per_item"].ToString();
            }

            if (reader["tax_amount"] != DBNull.Value)
            {
                obj.TaxAmount = reader["tax_amount"].ToString();
            }
            if (reader["total_price"] != DBNull.Value)
            {
                obj.TotalPrice = reader["total_price"].ToString();
            }

            if (reader["person_name"] != DBNull.Value)
            {
                obj.ContactName = reader["person_name"].ToString();
            }

            if (reader["register_name"] != DBNull.Value)
            {
                obj.RegisterName = reader["register_name"].ToString();
            }

            //if (reader["sync_number"] != DBNull.Value)
            //{
            //    obj.SyncNumber = reader["sync_number"].ToString();
            //}

            //if (reader["sync_status"] != DBNull.Value)
            //{
            //    obj.SyncStatus = Convert.ToInt16(reader["sync_number"]);


            //} 



            if (reader["sync_number"] != DBNull.Value)
            {
                obj.SyncNumber = reader["sync_number"].ToString();
            }



            if (reader["sync_status"] != DBNull.Value)
            {
                obj.SyncStatus = reader["sync_status"].ToString();
            }

            if (reader["accpac_invoice_no"] != DBNull.Value)
            {
                obj.AccpacInvoiceNo = reader["accpac_invoice_no"].ToString();
            }
            if (reader["accpac_orderentry_no"] != DBNull.Value)
            {
                obj.AccpacOrderentryNo = reader["accpac_orderentry_no"].ToString();
            }
            if (reader["accpac_shipment_no"] != DBNull.Value)
            {
                obj.AccpacShipmentNo = reader["accpac_shipment_no"].ToString();
            }
            if (reader["accpac_prepyament_no"] != DBNull.Value)
            {
                obj.AccpacPrepyamentNo = reader["accpac_prepyament_no"].ToString();
            }
            if (reader["accpac_refund_no"] != DBNull.Value)
            {
                obj.AccpacRefundNo = reader["accpac_refund_no"].ToString();
            }






            return obj;


        }


        public POSDailySalesSummary GetPOSDaiySalesSummaryMapper(SqlDataReader reader)
        {

            POSDailySalesSummary obj = new POSDailySalesSummary();

            if (reader["invoice_number"] != DBNull.Value)
            {
                obj.InvoiceNumber = reader["invoice_number"].ToString();
            }
            if (reader["invoice_date"] != DBNull.Value)
            {
                obj.InvoiceDate = reader["invoice_date"].ToString();
            }

            if (reader["store_name"] != DBNull.Value)
            {
                obj.StoreName = reader["store_name"].ToString();
            }

            if (reader["transaction_type"] != DBNull.Value)
            {
                obj.TransactionType = reader["transaction_type"].ToString();
            }
            if (reader["sales_man_name"] != DBNull.Value)
            {
                obj.SalesManName = reader["sales_man_name"].ToString();

            }

            if (reader["total_unit_price"] != DBNull.Value)
            {
                obj.TotalUnitPrice = reader["total_unit_price"].ToString();
            }

            if (reader["grand_total"] != DBNull.Value)
            {
                obj.GrandTotal = reader["grand_total"].ToString();
            }


            if (reader["items_discount"] != DBNull.Value)
            {
                obj.ItemDiscount = reader["items_discount"].ToString();
            }



            if (reader["tax"] != DBNull.Value)
            {
                obj.TaxAmount = reader["tax"].ToString();
            }


            if (reader["person_name"] != DBNull.Value)
            {
                obj.ContactName = reader["person_name"].ToString();
            }

            if (reader["payments"] != DBNull.Value)
            {
                obj.Payments = reader["payments"].ToString();
            }









            return obj;


        }


        public POSDaileSalesSummation GetTotalDaiySalesSummationMapper(SqlDataReader reader)
        {
            POSDaileSalesSummation obj = new POSDaileSalesSummation();

            if (reader["store_name"] != DBNull.Value)
            {
                obj.StoreName = reader["store_name"].ToString();
            }
            if (reader["register_name"] != DBNull.Value)
            {
                obj.RegisterName = reader["register_name"].ToString();
            }

            if (reader["invoice_date"] != DBNull.Value)
            {
                obj.InvoiceDate = reader["invoice_date"].ToString();
            }
            if (reader["total_sales"] != DBNull.Value)
            {
                obj.TotalSales = reader["total_sales"].ToString();
            }
            if (reader["total_refund"] != DBNull.Value)
            {
                obj.TotalRefund = reader["total_refund"].ToString();
            }

            if (reader["total_exchange"] != DBNull.Value)
            {
                obj.TotalExchange = reader["total_exchange"].ToString();
            }
            if (reader["Total"] != DBNull.Value)
            {
                obj.Total = reader["Total"].ToString();
            }
            return obj;
        }


        public POSPettyCash GetPettyCashMapper(SqlDataReader reader)
        {
            POSPettyCash obj = new POSPettyCash();

            if (reader["store_name"] != DBNull.Value)
            {
                obj.StoreName = reader["store_name"].ToString();
            }
            if (reader["register_name"] != DBNull.Value)
            {
                obj.RegisterName = reader["register_name"].ToString();
            }

            if (reader["invoice_number"] != DBNull.Value)
            {
                obj.InvoiceNumber = reader["invoice_number"].ToString();
            }
            if (reader["grand_total"] != DBNull.Value)
            {
                obj.GrandTotal = reader["grand_total"].ToString();
            }

            if (reader["session_number"] != DBNull.Value)
            {
                obj.SessionNumber = reader["session_number"].ToString();
            }
            
            if (reader["type"] != DBNull.Value)
            {
                obj.Type = reader["type"].ToString();
            }
            if (reader["invoice_date"] != DBNull.Value)
            {
                obj.InvoiceDate = reader["invoice_date"].ToString();
            }

            if (reader["sale_man_id"] != DBNull.Value)
            {
                obj.SaleManId = (int)reader["sale_man_id"];
            }

            if (reader["default_curr_symbol"] != DBNull.Value)
            {
                obj.defaultcurrsymbol = (string)reader["default_curr_symbol"];
            }

            if (reader["sales_man_name"] != DBNull.Value)
            {
                obj.SaleManName = (string)reader["sales_man_name"];
            }


            return obj;
        }
        public DashBoarBoxes GetDashBoardBoxesMapper(SqlDataReader reader)
        {
            DashBoarBoxes obj = new DashBoarBoxes();

            if (reader["stores"] != DBNull.Value)
            {
                obj.Stores = reader["stores"].ToString();
            }
            if (reader["registers"] != DBNull.Value)
            {
                obj.Registers = reader["registers"].ToString();
            }
            if (reader["users"] != DBNull.Value)
            {
                obj.Users = reader["users"].ToString();
            }
            if (reader["quickkeys"] != DBNull.Value)
            {
                obj.QuickKeys = reader["quickkeys"].ToString();
            }

            return obj;
        }

        public RegistersSales GetRegisterSalesMapper(SqlDataReader reader)
        {
            RegistersSales obj = new RegistersSales();

            if (reader["register_name"] != DBNull.Value)
            {
                obj.RegisterName = reader["register_name"].ToString();
            }
            if (reader["total"] != DBNull.Value)
            {
                obj.Total = reader["total"].ToString();
            }
            if (reader["date"] != DBNull.Value)
            {
                obj.Date = reader["date"].ToString();
            }


            return obj;
        }

      

        public StoreSales GetStoreSalesMapper(SqlDataReader reader)
        {
            StoreSales obj = new StoreSales();

            if (reader["store_name"] != DBNull.Value)
            {
                obj.StoreName = reader["store_name"].ToString();
            }
            if (reader["total"] != DBNull.Value)
            {
                obj.Total = reader["total"].ToString();
            }


            return obj;
        }
    }
}


      



        // public InvoiceLite GetMapperAllLite(SqlDataReader reader)
        //{
        //    InvoiceLite obj = new InvoiceLite();
        //    //Redaer//Values
        //    obj.Id = obj.InvoiceId;
        //    return obj;
        //}

        // public InvoiceLite GetLiteMapper(SqlDataReader reader)
        //{
        //    InvoiceLite obj = new InvoiceLite();
        //    //Redaer//Values
        //         if (reader["created_by_name"] != DBNull.Value)
        //    {
        //        obj.CreatedByName = (string)reader["created_by_name"];
        //    }
        //    if (reader["updated_by_name"] != DBNull.Value)
        //    {
        //        obj.UpdatedByName = (string)reader["updated_by_name"];
        //    }
        //    return obj;
        //}


