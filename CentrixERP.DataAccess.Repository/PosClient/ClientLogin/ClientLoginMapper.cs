using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.Framework;
using SF.Framework.Data;
using Centrix.UM.Business.Entity;
using System.Data.SqlClient;
using SagePOS.Server.DataAcces.IRepository;


namespace SagePOS.Server.DataAcces.Repository
{
    public partial class ClientLoginRepository
    {
         public User GetMapper(SqlDataReader reader)
         {
             User myUser = new User();
             if (reader["users_id"] != DBNull.Value)
             {
                 myUser.UserId = (int)reader["users_id"];

             }
             if (reader["role_id"] != DBNull.Value)
             {
                 myUser.RoleId = (int)reader["role_id"];
             }
             if (reader["team_id"] != DBNull.Value)
             {
                 myUser.TeamId = (int)reader["team_id"];
             }
             if (reader["updated_by"] != DBNull.Value)
             {
                 myUser.UpdatedBy = (int)reader["updated_by"];
             }
             if (reader["users_dob"] != DBNull.Value)
             {
                 myUser.Dob = Convert.ToDateTime(reader["users_dob"]).GetLocalTime();
             }
             if (reader["users_firstname"] != DBNull.Value)
             {
                 myUser.FirstnameEnglish = reader["users_firstname"].ToString();
             }
             if (reader["users_firstname_ar"] != DBNull.Value)
             {
                 myUser.FirstNameArabic = reader["users_firstname_ar"].ToString();
             }
             if (reader["users_lastname_ar"] != DBNull.Value)
             {
                 myUser.LastNameArabic = reader["users_lastname_ar"].ToString();
             }
             if (reader["users_lastname"] != DBNull.Value)
             {
                 myUser.LastNameEnglish = reader["users_lastname"].ToString();
             }
             if (reader["users_middlename"] != DBNull.Value)
             {
                 myUser.MiddleNameEnglish = reader["users_middlename"].ToString();
             }
             if (reader["users_middlename_ar"] != DBNull.Value)
             {
                 myUser.MiddleNameArabic = reader["users_middlename_ar"].ToString();
             }
             if (reader["users_notes"] != DBNull.Value)
             {
                 myUser.Notes = reader["users_notes"].ToString();
             }
             if (reader["users_password"] != DBNull.Value)
             {
                 // myUser.Password = reader["users_password"].ToString();
                 try
                 {
                     myUser.Password = Cryption.Decrypt.String(reader["users_password"].ToString(), Cryption.Algorithm.AES);
                 }
                 catch (Exception)
                 {
                 }

             }
             if (reader["users_title"] != DBNull.Value)
             {
                 myUser.Title = reader["users_title"].ToString();
             }
             if (reader["users_username"] != DBNull.Value)
             {
                 myUser.UserName = reader["users_username"].ToString();
             }
             if (reader["created_by"] != DBNull.Value)
             {
                 myUser.CreatedBy = (int)reader["created_by"];
             }
             if (reader["is_default"] != DBNull.Value)
             {
                 myUser.IsDefault = Convert.ToBoolean(reader["is_default"]);
             }
             if (reader["receive_notifications"] != DBNull.Value)
             {
                 myUser.ReceiveNoyifications = Convert.ToBoolean(reader["receive_notifications"]);
             }
             if (reader["full_name"] != DBNull.Value)
             {
                 myUser.Fullname = (string)reader["full_name"];
             }
             if (reader["is_manager"] != DBNull.Value)
             {
                 myUser.IsManager = (bool)reader["is_manager"];
             }

             if (reader["manager_id"] != DBNull.Value)
             {
                 myUser.ManagerId = SQLDAHelper.Convert_DBTOInt(reader["manager_id"]);
             }

             if (reader["is_super_user"] != DBNull.Value)
             {
                 myUser.IsSupperUser = (bool)(reader["is_super_user"]);
             }

            
             return myUser;
         }
    }
}
