
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;


using SagePOS.Server.Business.Entity;
using SagePOS.Server.DataAcces.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace SagePOS.Server.DataAcces.Repository
{
    public partial class RegisterAmountsRepository : RepositoryBaseClass<RegisterAmounts, RegisterAmountsLite, int>, IRegisterAmountsRepository
    {
        #region IRepository< RegisterAmounts, int > Members

        public RegisterAmountsRepository()
        {
            this.AddSPName = "SP_RegisterAmounts_Add";
            this.UpdateSPName = "SP_RegisterAmounts_Update";
            this.DeleteSPName = "SP_RegisterAmounts_Delete";
            this.DeleteLogicalSPName = "SP_RegisterAmounts_DeleteLogical";
            this.FindAllSPName = "SP_RegisterAmounts_FindAll";
            this.FindByIdSPName = "SP_RegisterAmounts_FindById";
            this.FindByParentSPName = "SP_RegisterAmounts_FindByParentId";
            this.FindAllLiteSPName = "SP_RegisterAmounts_FindAll_Lite";
            this.FindByIdLiteSPName = "SP_RegisterAmounts_FindById_Lite";
            this.AdvancedSearchSPName = "SP_RegisterAmounts_AdvancedSearch_Lite";
        }

		#endregion
    }
}