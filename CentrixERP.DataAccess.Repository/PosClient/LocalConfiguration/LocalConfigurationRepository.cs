
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
//using Centrix.POS.Standalone.Client.Business.Entity;
//using Centrix.POS.Standalone.Client.DataAccess.IRepository;
using SagePOS.Server.Business.Entity;
using SagePOS.Server.DataAcces.IRepository;

using System.Data.SqlClient;
using System.Data;

namespace SagePOS.Server.DataAcces.Repository
{
    public partial class LocalConfigurationRepository : RepositoryBaseClass<LocalConfiguration, LocalConfigurationLite, int>, ILocalConfigurationRepository
    {
        #region IRepository< LocalConfiguration, int > Members

        public LocalConfigurationRepository()
        {
            this.AddSPName = "SP_LocalConfiguration_Add";
            this.UpdateSPName = "SP_LocalConfiguration_Update";
            this.DeleteSPName = "SP_LocalConfiguration_Delete";
            this.DeleteLogicalSPName = "SP_LocalConfiguration_DeleteLogical";
            this.FindAllSPName = "SP_LocalConfiguration_FindAll";
            this.FindByIdSPName = "SP_LocalConfiguration_FindById";
            this.FindByParentSPName = "SP_LocalConfiguration_FindByParentId";
            this.FindAllLiteSPName = "SP_LocalConfiguration_FindAll_Lite";
            this.FindByIdLiteSPName = "SP_LocalConfiguration_FindById_Lite";
            this.AdvancedSearchSPName = "SP_LocalConfiguration_AdvancedSearch_Lite";
        }

		#endregion
      
      

      
    }
}