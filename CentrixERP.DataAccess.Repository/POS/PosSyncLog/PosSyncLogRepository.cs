
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using SagePOS.Server.Business.Entity;
using SagePOS.Server.DataAcces.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace SagePOS.Server.DataAcces.Repository
{
    public partial class PosSyncLogRepository : RepositoryBaseClass<PosSyncLog, PosSyncLogLite, int>, IPosSyncLogRepository
    {
        #region IRepository< PosSyncLog, int > Members

        public PosSyncLogRepository()
        {
            this.AddSPName = "SP_PosSyncLog_Add";
            this.UpdateSPName = "SP_PosSyncLog_Update";
            this.DeleteSPName = "SP_PosSyncLog_Delete";
            this.DeleteLogicalSPName = "SP_PosSyncLog_DeleteLogical";
            this.FindAllSPName = "SP_PosSyncLog_FindAll";
            this.FindByIdSPName = "SP_PosSyncLog_FindById";
            this.FindByParentSPName = "SP_PosSyncLog_FindByParentId";
            this.FindAllLiteSPName = "SP_PosSyncLog_FindAll_Lite";
            this.FindByIdLiteSPName = "SP_PosSyncLog_FindById_Lite";
            this.AdvancedSearchSPName = "SP_PosSyncLog_AdvancedSearch_Lite";
        }


		#endregion
    }
}