
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using SagePOS.Server.Business.Entity;
using SagePOS.Server.DataAcces.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace SagePOS.Server.DataAcces.Repository
{
    public partial class PosSyncConfigurationRepository : RepositoryBaseClass<PosSyncConfiguration, PosSyncConfigurationLite, int>, IPosSyncConfigurationRepository
    {
        #region IRepository< PosSyncConfiguration, int > Members

        public PosSyncConfigurationRepository()
        {
            this.AddSPName = "SP_PosSyncConfiguration_Add";
            this.UpdateSPName = "SP_PosSyncConfiguration_Update";
            this.DeleteSPName = "SP_PosSyncConfiguration_Delete";
            this.DeleteLogicalSPName = "SP_PosSyncConfiguration_DeleteLogical";
            this.FindAllSPName = "SP_PosSyncConfiguration_FindAll";
            this.FindByIdSPName = "SP_PosSyncConfiguration_FindById";
            this.FindByParentSPName = "SP_PosSyncConfiguration_FindByParentId";
            this.FindAllLiteSPName = "SP_PosSyncConfiguration_FindAll_Lite";
            this.FindByIdLiteSPName = "SP_PosSyncConfiguration_FindById_Lite";
            this.AdvancedSearchSPName = "SP_PosSyncConfiguration_AdvancedSearch_Lite";
        }

		#endregion
    }
}