
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using SagePOS.Server.Business.Entity;
using SagePOS.Server.DataAcces.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace SagePOS.Server.DataAcces.Repository
{
    public partial class SyncScheduleRepository : RepositoryBaseClass<SyncSchedule, SyncScheduleLite, int>, ISyncScheduleRepository
    {
        #region IRepository< SyncSchedule, int > Members

        public SyncScheduleRepository()
        {
            this.AddSPName = "SP_SyncSchedule_Add";
            this.UpdateSPName = "SP_SyncSchedule_Update";
            this.DeleteSPName = "SP_SyncSchedule_Delete";
            this.DeleteLogicalSPName = "SP_SyncSchedule_DeleteLogical";
            this.FindAllSPName = "SP_SyncSchedule_FindAll";
            this.FindByIdSPName = "SP_SyncSchedule_FindById";
            this.FindByParentSPName = "SP_SyncSchedule_FindByParentId";
            this.FindAllLiteSPName = "SP_SyncSchedule_FindAll_Lite";
            this.FindByIdLiteSPName = "SP_SyncSchedule_FindById_Lite";
            this.AdvancedSearchSPName = "SP_SyncSchedule_AdvancedSearch_Lite";
        }
        public List<SyncSchedule> FindAllToSync(int status)
        {
            List<SyncSchedule> syncEntity = new List<SyncSchedule>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;

            Instance.AddInParameter("@status", SqlDbType.Int, status);

            reader = Instance.ExcuteReader("dbo.SP_SyncSchedule_FindAll_to_sync", conn);

            if (reader != null && reader.HasRows)
            {

                while (reader.Read())
                    syncEntity.Add(GetMapper(reader));
                reader.Close();
                return syncEntity;
            }
            else
                return null;
        }


        public void addNewSchedule(int type, int userId)
        {

            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;

            Instance.AddInParameter("@type", SqlDbType.Int, type);

            Instance.AddInParameter("@user_id", SqlDbType.Int, userId);
            try
            {
                Instance.ExcuteReader("dbo.SP_SyncSchedule_AddNew", conn);
            }
            catch (Exception ex)
            {

                throw ex;
            }


        }


        public NewSyncSchedule checkSynchronizationStatus()
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;

            DateTime serverTime = DateTime.Now; // gives you current Time in server timeZone
            DateTime utcTime = DateTime.UtcNow; // convert it to Utc using timezone setting of server computer

            TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
            DateTime localTime = TimeZoneInfo.ConvertTimeFromUtc(utcTime, tzi);
            string currencTime = serverTime.ToString();
            try
            {
                NewSyncSchedule obj = new NewSyncSchedule();
                reader = Instance.ExcuteReader("dbo.checkSyncStatus", conn);
                if (reader != null && reader.HasRows)
                {
                    reader.Read();
                    obj.action = (int)reader["sync_action"];
                    obj.startTime = (string)reader["start_time"];
                    obj.SyncNumber = (string)reader["sync_number"];
                    obj.SyncStatus = (int)reader["sync_status"];
                    obj.isChecked = (int)reader["is_checked"];
                    obj.hasData = (int)reader["has_data"];
                    obj.createdby = (int)reader["created_by"];
                    obj.CurrentDate = (string)reader["CurrentDate"];

                    return obj;
                }
                else
                    return null;

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public string  getLastSyncNumber(int action)
        {

            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;
            try
            {
                Instance.AddInParameter("@action", SqlDbType.Int, action);
                reader = Instance.ExcuteReader("dbo.get_last_sync_number", conn);
                if (reader != null && reader.HasRows)
                {
                    reader.Read();
                    string syncNumber = (string)reader["sync_number"];
                    return syncNumber;

                }
                else
                    return null;

            }
            catch (Exception)
            {

                throw;
            }
        }

        public SyncScheduleNew FindsyncSchedule(int status)
        {
            SyncScheduleNew syncEntity = new SyncScheduleNew();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;

            Instance.AddInParameter("@status", SqlDbType.Int, status);

            reader = Instance.ExcuteReader("dbo.SP_syncscheduleNew_find", conn);

            if (reader != null && reader.HasRows)
            {

                while (reader.Read())
                    syncEntity= (GetSyncScheduleMapper(reader));
                reader.Close();
                return syncEntity;
            }
            else
                return null;

        }

        public void UpdateSyncScheduleStatus(int id)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;
            Instance.AddInParameter("@sync_id", SqlDbType.Int, id);
            try
            {
                Instance.ExcuteReader("dbo.sp_sync_schedule_update", conn);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        public List<SyncScheduleDetails> getSyncScheduleDetails(string SyncLogNumber)
        {
            List<SyncScheduleDetails> syncEntity = new List<SyncScheduleDetails>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;

            Instance.AddInParameter("@accpac_unique_sync_log", SqlDbType.NVarChar, SyncLogNumber);

            reader = Instance.ExcuteReader("dbo.SP_Accpac_SyncLog_entities", conn);

            if (reader != null && reader.HasRows)
            {

                while (reader.Read())
                    syncEntity.Add(GetSyncDetailsMapper(reader));
                reader.Close();
                return syncEntity;
            }
            else
                return null;
        }

        public List<SyncLogEntityDetails> getSyncScheduleentityDetails(string entity, int action, string logNumber)
        {
            List<SyncLogEntityDetails> syncEntity = new List<SyncLogEntityDetails>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;

            Instance.AddInParameter("@action", SqlDbType.Int, action);
            Instance.AddInParameter("@log_number", SqlDbType.NVarChar, logNumber);
            Instance.AddInParameter("@entity", SqlDbType.NVarChar, entity);
            reader = Instance.ExcuteReader("dbo.SP_Accpac_SyncLog_entitieyDetails", conn);

            if (reader != null && reader.HasRows)
            {

                while (reader.Read())
                    syncEntity.Add(getSyncScheduleentityDetails(reader));
                reader.Close();
                return syncEntity;
            }
            else
                return null;

        }


        #endregion
    }
}