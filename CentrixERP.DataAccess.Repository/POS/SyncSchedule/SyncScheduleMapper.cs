using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SagePOS.Server.Business.Entity;
using SF.Framework.Data;
using System.Data.SqlClient;

namespace SagePOS.Server.DataAcces.Repository
{
    public partial class SyncScheduleRepository
    {
        public SyncScheduleNew GetSyncScheduleMapper(SqlDataReader reader)
        {
            SyncScheduleNew obj = new SyncScheduleNew();

            if (reader["sync_schedule_id"] != DBNull.Value)
            {
                obj.SyncScheduleId = (int)reader["sync_schedule_id"];
            }
            if (reader["sync_action"] != DBNull.Value)
            {
                obj.SyncAction = (int)reader["sync_action"];
            }
            if (reader["sync_status"] != DBNull.Value)
            {

                obj.SyncStatus = (int)reader["sync_status"];
            }

            return obj;
        }


        public SyncScheduleDetails GetSyncDetailsMapper(SqlDataReader reader)
        {
            SyncScheduleDetails obj = new SyncScheduleDetails();

            if (reader["accpac_sync_unique_number"] != DBNull.Value)
            {
                obj.AccpacSyncUniqueNumner = reader["accpac_sync_unique_number"].ToString();
            }
            if (reader["sync_data"] != DBNull.Value)
            {
                obj.SyncData = reader["sync_data"].ToString();
            }
            if (reader["sync_time"] != DBNull.Value)
            {

                obj.SyncTime = reader["sync_time"].ToString();
            }
            if (reader["added_counts"] != DBNull.Value)
            {

                obj.AddedCounts = (int)reader["added_counts"];
            }
            if (reader["updated_counts"] != DBNull.Value)
            {

                obj.Updatecounts = (int)reader["updated_counts"];
            }
            if (reader["removed_counts"] != DBNull.Value)
            {

                obj.RemoveCounts = (int)reader["removed_counts"];
            }

            return obj;
        }

        public SyncLogEntityDetails getSyncScheduleentityDetails(SqlDataReader reader)
        {
            SyncLogEntityDetails obj = new SyncLogEntityDetails();

            if (reader["sync_data"] != DBNull.Value)
            {
                obj.syncData = reader["sync_data"].ToString();
            }
            if (reader["sync_data_id"] != DBNull.Value)
            {
                obj.SyncDataObject = reader["sync_data_id"].ToString();
            }
            return obj;
        }
    }
}
