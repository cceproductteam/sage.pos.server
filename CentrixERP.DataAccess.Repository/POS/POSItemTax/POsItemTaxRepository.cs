
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using SagePOS.Server.Business.Entity;
using SagePOS.Server.DataAcces.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace SagePOS.Server.DataAcces.Repository
{
    public partial class POSItemTaxRepository : RepositoryBaseClass<POSItemTax, POSItemTaxLite, int>, IPOSItemTaxRepository
    {
        #region IRepository< IcItemTax, int > Members

        public POSItemTaxRepository()
        {
            this.AddSPName = "SP_IcItemTax_Add";
            this.UpdateSPName = "SP_IcItemTax_Update";
            this.DeleteSPName = "SP_IcItemTax_Delete";
            this.DeleteLogicalSPName = "SP_IcItemTax_DeleteLogical";
            this.FindAllSPName = "SP_IcItemTax_FindAll";
            this.FindByIdSPName = "SP_IcItemTax_FindById";
            this.FindByParentSPName = "SP_IcItemTax_FindByParentId";
            this.FindAllLiteSPName = "SP_IcItemTax_FindAll_Lite";
            this.FindByIdLiteSPName = "SP_IcItemTax_FindById_Lite";
            this.AdvancedSearchSPName = "SP_IcItemTax_AdvancedSearch_Lite";
        }

        public string SaveIntegration(List<POSItemTax> itemTaxList)
        {
          
            try
            {
               
                foreach (POSItemTax item in itemTaxList)
                {
                    SQLDAHelper Instance = new SQLDAHelper();
                    if (UseCustomConnectionString)
                        Instance.SetConnectionString(CustomConnectionString);
                    SqlConnection conn = Instance.GetConnection(false);
                    Instance.AddInParameter("@item_tax_id", SqlDbType.Int, item.ItemTaxId);
                    Instance.AddInParameter("@item_card_id", SqlDbType.Int, item.ItemCardId);
                    Instance.AddInParameter("@item_bar_code", SqlDbType.NVarChar, item.ItemBarCode);
                    Instance.AddInParameter("@tax_authority_id", SqlDbType.Int, item.TaxAuthorityId);
                    Instance.AddInParameter("@tax_authority", SqlDbType.NVarChar, item.TaxAuthority);
                    Instance.AddInParameter("@item_tax_class", SqlDbType.Int, item.ItemTaxClass);
                    Instance.AddInParameter("@rate", SqlDbType.Money,item.Rate);
                    Instance.AddInParameter("@cust_tax_class", SqlDbType.Int, item.CustTaxClass);
                    Instance.AddInParameter("@created_by", SqlDbType.Int, item.CreatedBy);
                    Instance.AddInParameter("@flag_deleted", SqlDbType.Bit, item.FlagDeleted);
                    Instance.AddOutParameter("@pos_item_tax_id", SqlDbType.Int);
                    Instance.ExcuteNonQuery("SP_IcItemTax_Add", conn, false);
                    item.PosItemTaxId = (int)Instance.GetOutParamValue("@pos_item_tax_id");
                }
                return "success";

            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }

		#endregion
    }
}