
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;

using System.Data.SqlClient;
using System.Data;
using SagePOS.Server.Business.Entity;
using SagePOS.Server.DataAcces.IRepository;

namespace SagePOS.Server.DataAcces.Repository
{
    public partial class POSItemCardSerialRepository : RepositoryBaseClass<POSItemCardSerial, POSItemCardSerialLite, int>, IPOSItemCardSerialRepository
    {
        #region IRepository< IcItemCardSerial, int > Members

        public POSItemCardSerialRepository()
        {
            this.AddSPName = "SP_IcItemCardSerial_Add";
            this.UpdateSPName = "SP_IcItemCardSerial_Update";
            this.DeleteSPName = "SP_IcItemCardSerial_Delete";
            this.DeleteLogicalSPName = "SP_IcItemCardSerial_DeleteLogical";
            this.FindAllSPName = "SP_IcItemCardSerial_FindAll";
            this.FindByIdSPName = "SP_IcItemCardSerial_FindById";
            this.FindByParentSPName = "SP_IcItemCardSerial_FindByParentId";
            this.FindAllLiteSPName = "SP_IcItemCardSerial_FindAll_Lite";
            this.FindByIdLiteSPName = "SP_IcItemCardSerial_FindById_Lite";
            this.AdvancedSearchSPName = "SP_IcItemCardSerial_AdvancedSearch_Lite";
        }




        public string SaveIntegration(List<POSItemCardSerial> SerialList)
        {

            try
            {
                foreach (POSItemCardSerial item in SerialList)
                {
                    SQLDAHelper Instance = new SQLDAHelper();
                    if (UseCustomConnectionString)
                        Instance.SetConnectionString(CustomConnectionString);
                    SqlConnection conn = Instance.GetConnection(false);
                    Instance.AddInParameter("@item_card_serial_id", SqlDbType.Int, item.ItemCardSerialId);
                    Instance.AddInParameter("@item_serial_number", SqlDbType.NVarChar, item.ItemSerialNumber);
                    Instance.AddInParameter("@item_card_id", SqlDbType.Int, item.ItemCardId);
                    Instance.AddInParameter("@location_id", SqlDbType.Int, item.LocationId);
                    Instance.AddInParameter("@status", SqlDbType.Int, item.Status);
                    Instance.AddInParameter("@receipt_detail_id", SqlDbType.Int, item.ReceiptDetailId);
                    Instance.AddInParameter("@transfer_detail_id", SqlDbType.Int, item.TransferDetailId);
                    Instance.AddInParameter("@created_by", SqlDbType.Int, item.CreatedBy);
                    Instance.AddInParameter("@flag_deleted", SqlDbType.Bit, item.FlagDeleted);
                    Instance.AddInParameter("@is_system", SqlDbType.Bit, item.IsSystem);
                    Instance.AddInParameter("@stock_date", SqlDbType.DateTime, item.StockDate);
                    Instance.AddOutParameter("@pos_item_card_serial_id", SqlDbType.Int);
                    Instance.ExcuteNonQuery("sp_Icitemcardserial_Add", conn, false);
                    item.ItemCardSerialId = (int)Instance.GetOutParamValue("@pos_item_card_serial_id");
                }
                return "success";

            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }
        #endregion
    }
}