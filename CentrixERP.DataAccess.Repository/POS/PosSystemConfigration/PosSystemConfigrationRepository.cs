
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using SagePOS.Server.Business.Entity;
using SagePOS.Server.DataAcces.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace SagePOS.Server.DataAcces.Repository
{
    public partial class PosSystemConfigrationRepository : RepositoryBaseClass<PosSystemConfigration, PosSystemConfigrationLite, int>, IPosSystemConfigrationRepository
    {
        #region IRepository< PosSystemConfigration, int > Members

        public PosSystemConfigrationRepository()
        {
            this.AddSPName = "SP_PosSystemConfigration_Add";
            this.UpdateSPName = "SP_PosSystemConfigration_Update";
            this.FindAllSPName = "SP_PosSystemConfigration_FindAll";
            this.FindAllLiteSPName = "SP_PosSystemConfigration_FindAll_Lite";

            this.AdvancedSearchSPName = "SP_PosSystemConfigration_AdvancedSearch_Lite";
            this.DeleteSPName = "SP_PosSystemConfigration_Delete";
            this.DeleteLogicalSPName = "SP_PosSystemConfigration_DeleteLogical";
            this.FindByIdSPName = "SP_PosSystemConfigration_FindById";
            this.FindByParentSPName = "SP_PosSystemConfigration_FindByParentId";
            this.FindByIdLiteSPName = "SP_PosSystemConfigration_FindById_Lite";

        }

		#endregion
    }
}