using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SagePOS.Server.Business.Entity;
using SF.Framework.Data;
using System.Data.SqlClient;

namespace SagePOS.Server.DataAcces.Repository
{
    public partial class PersonRepository
    {

        public PersonLite GetPersonNotificationMapper(SqlDataReader reader)
        {
            PersonLite person = new PersonLite();

            if (reader["person_id"] != DBNull.Value)
            {
                person.PersonId = Convert.ToInt32(reader["person_id"]);
                person.Id = Convert.ToInt32(reader["person_id"]);
            }

            if (reader["first_name_en"] != DBNull.Value)
            {
                person.FirstNameEn = (string)reader["first_name_en"];
            }
            if (reader["last_name_en"] != DBNull.Value)
            {
                person.LastNameEn = (string)reader["last_name_en"];
            }

            if (reader["full_name_en"] != DBNull.Value)
            {
                person.FullNameEn = (string)reader["full_name_en"];
            }
            return person;
        }
    }
}
