
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using SagePOS.Server.Business.Entity;
using SagePOS.Server.DataAcces.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace SagePOS.Server.DataAcces.Repository
{
    public partial class CompanyRepository : RepositoryBaseClass<Company, CompanyLite, int>, ICompanyRepository
    {
        #region IRepository< Company, int > Members

        public CompanyRepository()
        {
            this.AddSPName = "SP_Company_Add";
            this.UpdateSPName = "SP_Company_Update";
            this.DeleteSPName = "SP_Company_Delete";
            this.DeleteLogicalSPName = "SP_Company_DeleteLogical";
            this.FindAllSPName = "SP_Company_FindAll";
            this.FindByIdSPName = "SP_Company_FindById";
            this.FindByParentSPName = "SP_Company_FindByParentId";
            this.FindAllLiteSPName = "SP_Company_FindAll_Lite";
            this.FindByIdLiteSPName = "SP_Company_FindById_Lite";
            this.AdvancedSearchSPName = "SP_Company_AdvancedSearch_Lite";
        }


        #endregion

        public void UpdateSyncStatus(string trasactionNumber, int EntityId)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            if (UseCustomConnectionString)
                Instance.SetConnectionString(CustomConnectionString);
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@transaction_number", SqlDbType.NVarChar, trasactionNumber);
            Instance.AddInParameter("@entity_id", SqlDbType.Int, EntityId);

            Instance.ExcuteNonQuery("SP_UpdateSyncStatus", conn, false);
        }
    }
}