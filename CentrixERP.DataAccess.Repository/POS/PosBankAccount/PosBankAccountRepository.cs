
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using SagePOS.Server.Business.Entity;
using SagePOS.Server.DataAcces.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace SagePOS.Server.DataAcces.Repository
{
    public partial class PosBankAccountRepository : RepositoryBaseClass<PosBankAccount, PosBankAccountLite, int>, IPosBankAccountRepository
    {
        #region IRepository< PosBankAccount, int > Members

        public PosBankAccountRepository()
        {
            this.AddSPName = "SP_PosBankAccount_Add";
            this.UpdateSPName = "SP_PosBankAccount_Update";
            this.DeleteSPName = "SP_PosBankAccount_Delete";
            this.DeleteLogicalSPName = "SP_PosBankAccount_DeleteLogical";
            this.FindAllSPName = "SP_PosBankAccount_FindAll";
            this.FindByIdSPName = "SP_PosBankAccount_FindById";
            this.FindByParentSPName = "SP_PosBankAccount_FindByParentId";
            this.FindAllLiteSPName = "SP_PosBankAccount_FindAll_Lite";
            this.FindByIdLiteSPName = "SP_PosBankAccount_FindById_Lite";
            this.AdvancedSearchSPName = "SP_PosBankAccount_AdvancedSearch_Lite";
        }

		#endregion
    }
}