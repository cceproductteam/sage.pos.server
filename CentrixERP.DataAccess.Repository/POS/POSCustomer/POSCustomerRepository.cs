
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using SagePOS.Server.Business.Entity;
using SagePOS.Server.DataAcces.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace SagePOS.Server.DataAcces.Repository
{
    public partial class POSCustomerRepository : RepositoryBaseClass<POSCustomer, POSCustomerLite, int>, IPOSCustomerRepository
    {
        #region IRepository< IcCustomer, int > Members

        public POSCustomerRepository()
        {
            this.AddSPName = "SP_IcCustomer_Add";
            this.UpdateSPName = "SP_IcCustomer_Update";
            this.DeleteSPName = "SP_IcCustomer_Delete";
            this.DeleteLogicalSPName = "SP_IcCustomer_DeleteLogical";
            this.FindAllSPName = "SP_IcCustomer_FindAll";
            this.FindByIdSPName = "SP_IcCustomer_FindById";
            this.FindByParentSPName = "SP_IcCustomer_FindByParentId";
            this.FindAllLiteSPName = "SP_IcCustomer_FindAll_Lite";
            this.FindByIdLiteSPName = "SP_IcCustomer_FindById_Lite";
            this.AdvancedSearchSPName = "SP_IcCustomer_AdvancedSearch_Lite";
        }

        public string SaveIntegration(List<POSCustomer> CustomerList)
        {
            
            try
            {
                foreach (POSCustomer item in CustomerList)
                {
                    SQLDAHelper Instance = new SQLDAHelper();
                    if (UseCustomConnectionString)
                        Instance.SetConnectionString(CustomConnectionString);
                    SqlConnection conn = Instance.GetConnection(false);
                    Instance.AddInParameter("@customer_id", SqlDbType.Int, item.CustomerId);
                    Instance.AddInParameter("@customer_number", SqlDbType.NVarChar, item.CustomerNumber);
                    Instance.AddInParameter("@customer_name", SqlDbType.NVarChar, item.CustomerName);
                    Instance.AddInParameter("@tax_class_id", SqlDbType.Int, item.TaxClassId);
                    Instance.AddInParameter("@credit_limit_amount", SqlDbType.Float, item.CreditLimitAmount);
                    Instance.AddInParameter("@contact_email", SqlDbType.NVarChar, item.ContactEmail);
                    Instance.AddInParameter("@contact_telephone", SqlDbType.NVarChar, item.ContactTelephone);
                    Instance.AddInParameter("@created_by", SqlDbType.Bit, item.CreatedBy);
                    Instance.AddInParameter("@flag_deleted", SqlDbType.Bit, item.FlagDeleted);
                    Instance.AddOutParameter("@pos_customer_id", SqlDbType.Int);
                    Instance.AddInParameter("@tax_authority_id", SqlDbType.Int, item.TaxAuthorityId);
                    Instance.AddInParameter("@currency_id", SqlDbType.Int, item.CurrencyId);
                    Instance.AddInParameter("@taxable", SqlDbType.Bit, item.Taxable);

                   

                    Instance.ExcuteNonQuery("SP_IcCustomer_Add", conn, false);
                    item.PosCustomerId = (int)Instance.GetOutParamValue("@pos_customer_id");
                }
                return "success";

            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }

		#endregion
    }
}