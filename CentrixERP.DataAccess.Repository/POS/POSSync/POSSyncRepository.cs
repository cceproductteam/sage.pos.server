﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using SagePOS.Server.Business.Entity;
using SagePOS.Server.DataAcces.IRepository;
using System.Data.SqlClient;
using System.Data;
using System.Net;
using System.Net.Sockets;

namespace SagePOS.Server.DataAcces.Repository
{

    public partial class POSSyncRepository : RepositoryBaseClass<POSSync, POSSyncLite, int>, IPOSSyncRepository
    {
        #region IRepository<POSSync, int > Members

        public POSSyncRepository()
        {
            this.AdvancedSearchSPName = "SP_POSSync_AdvancedSearch_Lite";
        }

        public void POSSyncData(Object[] data)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;
            // Instance.AddInParameter("@", SqlDbType.Int, );
            reader = Instance.ExcuteReader("dbo.SP_POS_Sync", conn);

        }

        public void SyncPOSData(POSSync myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@get_transactions", SqlDbType.Bit, myEntity.GetTransactions);
            Instance.AddInParameter("@registers_ip", SqlDbType.NVarChar, myEntity.IpAddress);
            Instance.AddInParameter("@get_close_Register", SqlDbType.Bit, myEntity.GetCloseRegister);
            Instance.AddInParameter("@get_user_data", SqlDbType.Bit, myEntity.GetUserData);
            Instance.AddInParameter("@get_role_data", SqlDbType.Bit, myEntity.GetRoles);
            Instance.AddInParameter("@get_team_data", SqlDbType.Bit, myEntity.GetTeams);
            Instance.AddInParameter("@get_permission_data", SqlDbType.Bit, myEntity.GetPermissions);
            Instance.AddInParameter("@get_shift_data", SqlDbType.Bit, myEntity.GetShifts);
            Instance.AddInParameter("@get_user_store_shift_data", SqlDbType.Bit, myEntity.GetUserShifts);
            Instance.AddInParameter("@get_store_data", SqlDbType.Bit, myEntity.GetStores);
            Instance.AddInParameter("@get_register_data", SqlDbType.Bit, myEntity.GetRegisters);
            Instance.AddInParameter("@get_item_data", SqlDbType.Bit, myEntity.GetItems);
            Instance.AddInParameter("@get_price_list_data", SqlDbType.Bit, myEntity.GetPriceList);
            Instance.AddInParameter("@get_item_tax_data", SqlDbType.Bit, myEntity.GetItemTax);
            Instance.AddInParameter("@get_item_tax_mtrix_data", SqlDbType.Bit, myEntity.GetTaxMatrix);
            Instance.AddInParameter("@get_item_tax_auth_data", SqlDbType.Bit, myEntity.GetTaxAuth);
            Instance.AddInParameter("@get_item_price_list_data", SqlDbType.Bit, myEntity.GetItemPriceList);
            Instance.AddInParameter("@get_currency_data", SqlDbType.Bit, myEntity.GetCurrencyData);
            Instance.AddInParameter("@get_currency_rate", SqlDbType.Bit, myEntity.GetCurrencyRates);
            Instance.AddInParameter("@get_quick_keys_data", SqlDbType.Bit, myEntity.GetQuickKeys);
            Instance.AddInParameter("@get_person_data", SqlDbType.Bit, myEntity.GetPersonData);
            Instance.AddInParameter("@get_configuration_data", SqlDbType.Bit, myEntity.GetConfigData);
            Instance.AddInParameter("@get_customer_data", SqlDbType.Bit, myEntity.GetAccpacCustomerData);
            Instance.AddInParameter("@get_quick_add_customer", SqlDbType.Bit, myEntity.GetCustomers);
            Instance.AddInParameter("@get_paymentData", SqlDbType.Bit, myEntity.GetPaymentData);
            Instance.AddInParameter("@get_store_transaction", SqlDbType.Bit, 0);
            Instance.AddInParameter("@get_lovs", SqlDbType.Bit, myEntity.GetLovs);
            Instance.AddInParameter("@get_categories", SqlDbType.Bit, 0);

            Instance.AddInParameter("@get_transfers", SqlDbType.Bit, 0);
            Instance.AddInParameter("@get_receipts", SqlDbType.Bit, 0);
            Instance.AddInParameter("@get_banks", SqlDbType.Bit, 0);
            Instance.AddInParameter("@get_glaccounts", SqlDbType.Bit, 0);

            Instance.AddInParameter("@machine_ip_address", SqlDbType.NVarChar, GetIPAddress());
            Instance.AddInParameter("@user_name", SqlDbType.NVarChar, myEntity.DataBaseUserName);
            Instance.AddInParameter("@password", SqlDbType.NVarChar, myEntity.DataBasePassword);
            Instance.AddInParameter("@data_base_name", SqlDbType.NVarChar, myEntity.DataBaseName);
            Instance.AddInParameter("@pos_server_ip", SqlDbType.NVarChar, myEntity.ServerIPAddress);
            Instance.AddInParameter("@pos_server_data_base_name", SqlDbType.NVarChar, myEntity.ServerDataBaseName);
            Instance.AddInParameter("@location_code", SqlDbType.NVarChar, myEntity.LocationCode);
            Instance.ExcuteNonQuery("SP_Sync_POSData", conn, false);
        }

        public string GetIPAddress()
        {
            IPHostEntry host;
            string localIP = "";
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    localIP = ip.ToString();
                }
            }
            return localIP;
        }

        #endregion
    }
}
