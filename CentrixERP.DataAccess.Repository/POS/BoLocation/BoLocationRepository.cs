
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;

using System.Data.SqlClient;
using System.Data;
using SagePOS.Server.Business.Entity;
using SagePOS.Server.DataAcces.IRepository;


namespace SagePOS.Server.DataAcces.Repository
{
    public partial class BoLocationRepository : RepositoryBaseClass<BoLocation, BoLocationLite, int>, IBoLocationRepository
    {
        #region IRepository< BoLocation, int > Members

        public BoLocationRepository()
        {
            this.AddSPName = "SP_BoLocation_Add";
            this.UpdateSPName = "SP_BoLocation_Update";
            this.DeleteSPName = "SP_BoLocation_Delete";
            this.DeleteLogicalSPName = "SP_BoLocation_DeleteLogical";
            this.FindAllSPName = "SP_BoLocation_FindAll";
            this.FindByIdSPName = "SP_BoLocation_FindById";
            this.FindByParentSPName = "SP_BoLocation_FindByParentId";
            this.FindAllLiteSPName = "SP_BoLocation_FindAll_Lite";
            this.FindByIdLiteSPName = "SP_BoLocation_FindById_Lite";
            this.AdvancedSearchSPName = "SP_BoLocation_AdvancedSearch_Lite";
        }

		#endregion
    }
}