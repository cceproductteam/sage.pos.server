
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;

using SagePOS.Server.Business.Entity;
using SagePOS.Server.DataAcces.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace SagePOS.Server.DataAcces.Repository
{
    public partial class AccpacSyncScheduleRepository : RepositoryBaseClass<AccpacSyncSchedule, AccpacSyncScheduleLite, int>, IAccpacSyncScheduleRepository
    {
        #region IRepository< AccpacSyncSchedule, int > Members

        public AccpacSyncScheduleRepository()
        {
            this.AddSPName = "SP_AccpacSyncSchedule_Add";
            this.UpdateSPName = "SP_AccpacSyncSchedule_Update";
            this.DeleteSPName = "SP_AccpacSyncSchedule_Delete";
            this.DeleteLogicalSPName = "SP_AccpacSyncSchedule_DeleteLogical";
            this.FindAllSPName = "SP_AccpacSyncSchedule_FindAll";
            this.FindByIdSPName = "SP_AccpacSyncSchedule_FindById";
            this.FindByParentSPName = "SP_AccpacSyncSchedule_FindByParentId";
            this.FindAllLiteSPName = "SP_AccpacSyncSchedule_FindAll_Lite";
            this.FindByIdLiteSPName = "SP_AccpacSyncSchedule_FindById_Lite";
            this.AdvancedSearchSPName = "SP_AccpacSyncSchedule_AdvancedSearch_Lite";
        }


        public List<AccpacSyncLogLite> FindAllLiteLog(string fromdate, string todate, string AccpacSyncUniqueNumber)

        {

            List<AccpacSyncLogLite> resultsList = new List<AccpacSyncLogLite>();
            SQLDAHelper instance = new SQLDAHelper();
            SqlConnection conn = instance.GetConnection(false);
            instance.AddInParameter("@from_date", SqlDbType.NVarChar, fromdate);
            instance.AddInParameter("@to_date", SqlDbType.NVarChar, todate);
            instance.AddInParameter("@accpac_sync_unique_number", SqlDbType.NVarChar, AccpacSyncUniqueNumber);

            SqlDataReader reader = instance.ExcuteReader("SP_AccpacSyncLog_FindAll", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    resultsList.Add(AccpacSyncLogLite(reader));
                }
                return resultsList;
            }
            else
                return null;
        }

        private AccpacSyncLogLite AccpacSyncLogLite(SqlDataReader reader)
        {
            AccpacSyncLogLite obj = new AccpacSyncLogLite();


            
                
             if (reader["sync_data"] != DBNull.Value)
            {
                obj.SyncData = (string)reader["sync_data"];

            }
             if (reader["sync_data_id"] != DBNull.Value)
             {
                 obj.SyncDataId = (string)reader["sync_data_id"];

             }

             if (reader["action"] != DBNull.Value)
             {
                 obj.Action = (string)(reader["action"]);

             }

             if (reader["created_date"] != DBNull.Value)
             {
                 obj.CreatedDate = (string)(reader["created_date"]);

             }


            

             if (reader["accpac_sync_unique_number"] != DBNull.Value)
             {
                 obj.AccpacSyncUniqueNumber = (string)(reader["accpac_sync_unique_number"]);

             }


            




           
            return obj;
        }

		#endregion
    }
}