
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using SagePOS.Server.Business.Entity;
using SagePOS.Server.DataAcces.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace SagePOS.Server.DataAcces.Repository
{
    public partial class CustomFildRepository : RepositoryBaseClass<CustomFild, CustomFildLite, int>, ICustomFildRepository
    {
        #region IRepository< CustomFild, int > Members

        public CustomFildRepository()
        {
            this.AddSPName = "SP_CustomFild_Add";
            this.UpdateSPName = "SP_CustomFild_Update";
            this.DeleteSPName = "SP_CustomFild_Delete";
            this.DeleteLogicalSPName = "SP_CustomFild_DeleteLogical";
            this.FindAllSPName = "SP_CustomFild_FindAll";
            this.FindByIdSPName = "SP_CustomFild_FindById";
            this.FindByParentSPName = "SP_CustomFild_FindByParentId";
            this.FindAllLiteSPName = "SP_CustomFild_FindAll_Lite";
            this.FindByIdLiteSPName = "SP_CustomFild_FindById_Lite";
            this.AdvancedSearchSPName = "SP_CustomFild_AdvancedSearch_Lite";
        }


        #endregion




        public void UpdateSyncStatus(string trasactionNumber, int EntityId)
        {
            throw new NotImplementedException();
        }



        
    }
}