
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;

using System.Data.SqlClient;
using System.Data;
using SagePOS.Server.Business.Entity;
using SagePOS.Server.DataAcces.IRepository;

namespace SagePOS.Server.DataAcces.Repository
{
    public partial class BoCurrencyRepository : RepositoryBaseClass<BoCurrency, BoCurrencyLite, int>, IBoCurrencyRepository
    {
        #region IRepository< BoCurrency, int > Members

        public BoCurrencyRepository()
        {
            this.AddSPName = "SP_BoCurrency_Add";
            this.UpdateSPName = "SP_BoCurrency_Update";
            this.DeleteSPName = "SP_BoCurrency_Delete";
            this.DeleteLogicalSPName = "SP_BoCurrency_DeleteLogical";
            this.FindAllSPName = "SP_BoCurrency_FindAll";
            this.FindByIdSPName = "SP_BoCurrency_FindById";
            this.FindByParentSPName = "SP_BoCurrency_FindByParentId";
            this.FindAllLiteSPName = "SP_BoCurrency_FindAll_Lite";
            this.FindByIdLiteSPName = "SP_BoCurrency_FindById_Lite";
            this.AdvancedSearchSPName = "SP_BoCurrency_AdvancedSearch_Lite";
        }

		#endregion
    }
}