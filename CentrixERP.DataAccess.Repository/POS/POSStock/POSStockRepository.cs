
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using SagePOS.Server.Business.Entity;
using SagePOS.Server.DataAcces.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace SagePOS.Server.DataAcces.Repository
{
    public partial class POSStockRepository : RepositoryBaseClass<POSStock, POSStockLite, int>, IPOSStockRepository
    {
        #region IRepository< IcStock, int > Members

        public POSStockRepository()
        {
            this.AddSPName = "SP_IcStock_Add";
            this.UpdateSPName = "SP_IcStock_Update";
            this.DeleteSPName = "SP_IcStock_Delete";
            this.DeleteLogicalSPName = "SP_IcStock_DeleteLogical";
            this.FindAllSPName = "SP_IcStock_FindAll";
            this.FindByIdSPName = "SP_IcStock_FindById";
            this.FindByParentSPName = "SP_IcStock_FindByParentId";
            this.FindAllLiteSPName = "SP_IcStock_FindAll_Lite";
            this.FindByIdLiteSPName = "SP_IcStock_FindById_Lite";
            this.AdvancedSearchSPName = "SP_IcStock_AdvancedSearch_Lite";
        }


        public string SaveIntegration(List<POSStock> stockList)
        {
           
            try
            {
                foreach (POSStock item in stockList)
                {
                    SQLDAHelper Instance = new SQLDAHelper();
                    if (UseCustomConnectionString)
                        Instance.SetConnectionString(CustomConnectionString);
                    SqlConnection conn = Instance.GetConnection(false);
                    Instance.AddInParameter("@stock_id", SqlDbType.Int, item.StockId);
                    Instance.AddInParameter("@item_card_id", SqlDbType.Int, item.ItemCardId);
                    Instance.AddInParameter("@item_card", SqlDbType.NVarChar, item.ItemCard);
                    Instance.AddInParameter("@location_id", SqlDbType.Int, item.LocationId);
                    Instance.AddInParameter("@location_name", SqlDbType.NVarChar, item.LocationName);
                    Instance.AddInParameter("@quantity_on_hand", SqlDbType.Money, item.QuantityOnHand);
                    Instance.AddInParameter("@cost_unit_of_measure", SqlDbType.Int, item.CostUnitOfMeasure);
                    Instance.AddInParameter("@created_by", SqlDbType.Int, item.CreatedBy);
                    Instance.AddInParameter("@flag_deleted", SqlDbType.Bit, item.FlagDeleted);
                    Instance.AddOutParameter("@pos_stock_id", SqlDbType.Int);
                    Instance.ExcuteNonQuery("SP_IcStock_Add", conn, false);
                    item.PosStockId = (int)Instance.GetOutParamValue("@pos_stock_id");
                }
                return "success";

            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }

		#endregion
    }
}