
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using SagePOS.Server.Business.Entity;
using SagePOS.Server.DataAcces.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace SagePOS.Server.DataAcces.Repository
{
    public partial class IcSyncRepository : RepositoryBaseClass<IcSync, IcSyncLite, int>, IIcSyncRepository
    {
        #region IRepository< IcSync, int > Members

        public IcSyncRepository()
        {
            this.AddSPName = "SP_IcSync_Add";
            this.UpdateSPName = "SP_IcSync_Update";
            this.DeleteSPName = "SP_IcSync_Delete";
            this.DeleteLogicalSPName = "SP_IcSync_DeleteLogical";
            this.FindAllSPName = "SP_IcSync_FindAll";
            this.FindByIdSPName = "SP_IcSync_FindById";
            this.FindByParentSPName = "SP_IcSync_FindByParentId";
            this.FindAllLiteSPName = "SP_IcSync_FindAll_Lite";
            this.FindByIdLiteSPName = "SP_IcSync_FindById_Lite";
            this.AdvancedSearchSPName = "SP_IcSync_AdvancedSearch_Lite";
        }

		#endregion
    }
}