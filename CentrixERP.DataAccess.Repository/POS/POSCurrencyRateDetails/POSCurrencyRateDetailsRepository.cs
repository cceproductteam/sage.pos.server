
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using SagePOS.Server.Business.Entity;
using SagePOS.Server.DataAcces.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace SagePOS.Server.DataAcces.Repository
{
    public partial class POSCurrencyRateDetailsRepository : RepositoryBaseClass<POSCurrencyRateDetails, POSCurrencyRateDetailsLite, int>, IPOSCurrencyRateDetailsRepository
    {
        #region IRepository< IcCurrencyRateDetails, int > Members

        public POSCurrencyRateDetailsRepository()
        {
            this.AddSPName = "SP_IcCurrencyRateDetails_Add";
            this.UpdateSPName = "SP_IcCurrencyRateDetails_Update";
            this.DeleteSPName = "SP_IcCurrencyRateDetails_Delete";
            this.DeleteLogicalSPName = "SP_IcCurrencyRateDetails_DeleteLogical";
            this.FindAllSPName = "SP_IcCurrencyRateDetails_FindAll";
            this.FindByIdSPName = "SP_IcCurrencyRateDetails_FindById";
            this.FindByParentSPName = "SP_IcCurrencyRateDetails_FindByParentId";
            this.FindAllLiteSPName = "SP_IcCurrencyRateDetails_FindAll_Lite";
            this.FindByIdLiteSPName = "SP_IcCurrencyRateDetails_FindById_Lite";
            this.AdvancedSearchSPName = "SP_IcCurrencyRateDetails_AdvancedSearch_Lite";
        }

        public string SaveIntegration(List<POSCurrencyRateDetails> currenyRateList)
        {
          
            try
            {
                foreach (POSCurrencyRateDetails item in currenyRateList)
                {
                    SQLDAHelper Instance = new SQLDAHelper();
                    if (UseCustomConnectionString)
                        Instance.SetConnectionString(CustomConnectionString);
                    SqlConnection conn = Instance.GetConnection(false);
                    Instance.AddInParameter("@currency_rate_details_id", SqlDbType.Int, item.CurrencyRateDetailsId);
                    Instance.AddInParameter("@source_currency", SqlDbType.Int, item.SourceCurrency);
                    Instance.AddInParameter("@from_currency_id", SqlDbType.Int, item.FromCurrencyId);
                    Instance.AddInParameter("@rate", SqlDbType.Money, item.Rate);
                    Instance.AddInParameter("@created_by", SqlDbType.Int, item.CreatedBy);
                    Instance.AddInParameter("@flag_deleted", SqlDbType.Bit, item.FlagDeleted);
                    Instance.AddOutParameter("@pos_currency_rate_details_id", SqlDbType.Int); 
                    Instance.ExcuteNonQuery("SP_IcCurrencyRateDetails_Add", conn, false);
                    item.PosCurrencyRateDetailsId = (int)Instance.GetOutParamValue("@pos_currency_rate_details_id");
                }
                return "success";

            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }

		#endregion
    }
}