
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using SagePOS.Server.Business.Entity;
using SagePOS.Server.DataAcces.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace SagePOS.Server.DataAcces.Repository
{
    public partial class POSPriceListRepository : RepositoryBaseClass<POSPriceList, POSPriceListLite, int>, IPOSPriceListRepository
    {
        #region IRepository< IcPriceList, int > Members

        public POSPriceListRepository()
        {
            this.AddSPName = "SP_IcPriceList_Add";
            this.UpdateSPName = "SP_IcPriceList_Update";
            this.DeleteSPName = "SP_IcPriceList_Delete";
            this.DeleteLogicalSPName = "SP_IcPriceList_DeleteLogical";
            this.FindAllSPName = "SP_IcPriceList_FindAll";
            this.FindByIdSPName = "SP_IcPriceList_FindById";
            this.FindByParentSPName = "SP_IcPriceList_FindByParentId";
            this.FindAllLiteSPName = "SP_IcPriceList_FindAll_Lite";
            this.FindByIdLiteSPName = "SP_IcPriceList_FindById_Lite";
            this.AdvancedSearchSPName = "SP_IcPriceList_AdvancedSearch_Lite";
        }


        public string SaveIntegration(List<POSPriceList> IcPriceList)
        {
            
            try
            {
                foreach (POSPriceList item in IcPriceList)
                {
                    SQLDAHelper Instance = new SQLDAHelper();
                    if (UseCustomConnectionString)
                        Instance.SetConnectionString(CustomConnectionString);
                    SqlConnection conn = Instance.GetConnection(false);
                    Instance.AddInParameter("@price_list_id", SqlDbType.Int, item.PriceListId);
                    Instance.AddInParameter("@price_list_code", SqlDbType.NVarChar, item.PriceListCode);
                    Instance.AddInParameter("@price_list_description", SqlDbType.NVarChar, item.PriceListDescription);
                    Instance.AddInParameter("@currency_id", SqlDbType.Int, item.CurrencyId);
                    Instance.AddInParameter("@currency", SqlDbType.NVarChar, item.Currency);
                    Instance.AddInParameter("@base_price", SqlDbType.Money, item.BasePrice);
                    Instance.AddInParameter("@unit_of_measure", SqlDbType.NVarChar, item.UnitOfMeasure);
                    Instance.AddInParameter("@created_by", SqlDbType.Int, item.CreatedBy);
                    Instance.AddInParameter("@flag_deleted", SqlDbType.Bit, item.FlagDeleted);
                    Instance.AddInParameter("@price_list_group_id", SqlDbType.Int, item.PriceListGroupId);
                    Instance.AddOutParameter("@pos_price_list_id", SqlDbType.Int);
                    Instance.ExcuteNonQuery("dbo.SP_IcPriceList_Add", conn, false);
                    item.PosPriceListId = (int)Instance.GetOutParamValue("@pos_price_list_id");
                }
                return "success";

            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }

		#endregion
    }
}