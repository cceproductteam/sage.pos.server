using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;

using System.Data.SqlClient;
using System.Data;
using SagePOS.Server.Business.Entity;
using SagePOS.Server.DataAcces.IRepository;

namespace SagePOS.Server.DataAcces.Repository
{
    public partial class BoBankRepository : RepositoryBaseClass<BoBank, BoBankLite, int>, IBoBankRepository
    {
        #region IRepository< BoBank, int > Members

        public BoBankRepository()
        {
            this.AddSPName = "SP_BoBank_Add";
            this.UpdateSPName = "SP_BoBank_Update";
            this.DeleteSPName = "SP_BoBank_Delete";
            this.DeleteLogicalSPName = "SP_BoBank_DeleteLogical";
            this.FindAllSPName = "SP_BoBank_FindAll";
            this.FindByIdSPName = "SP_BoBank_FindById";
            this.FindByParentSPName = "SP_BoBank_FindByParentId";
            this.FindAllLiteSPName = "SP_BoBank_FindAll_Lite";
            this.FindByIdLiteSPName = "SP_BoBank_FindById_Lite";
            this.AdvancedSearchSPName = "SP_BoBank_AdvancedSearch_Lite";
        }

		#endregion
    }
}