
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using SagePOS.Server.Business.Entity;
using SagePOS.Server.DataAcces.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace SagePOS.Server.DataAcces.Repository
{
    public partial class QuickKeysProductRepository : RepositoryBaseClass<QuickKeysProduct, QuickKeysProductLite, int>, IQuickKeysProductRepository
    {
        #region IRepository< QuickKeysProduct, int > Members

        public QuickKeysProductRepository()
        {
            this.AddSPName = "SP_QuickKeysProduct_Add";
            this.UpdateSPName = "SP_QuickKeysProduct_Update";
            this.DeleteSPName = "SP_QuickKeysProduct_Delete";
            this.DeleteLogicalSPName = "SP_QuickKeysProduct_DeleteLogical";
            this.FindAllSPName = "SP_QuickKeysProduct_FindAll";
            this.FindByIdSPName = "SP_QuickKeysProduct_FindById";
            this.FindByParentSPName = "SP_QuickKeysProduct_FindByParentId";
            this.FindAllLiteSPName = "SP_QuickKeysProduct_FindAll_Lite";
            this.FindByIdLiteSPName = "SP_QuickKeysProduct_FindById_Lite";
            this.AdvancedSearchSPName = "SP_QuickKeysProduct_AdvancedSearch_Lite";
        }

		#endregion
    }
}