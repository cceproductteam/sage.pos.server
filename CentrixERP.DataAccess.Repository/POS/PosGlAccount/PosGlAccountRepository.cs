
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using SagePOS.Server.Business.Entity;
using SagePOS.Server.DataAcces.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace SagePOS.Server.DataAcces.Repository
{
    public partial class PosGlAccountRepository : RepositoryBaseClass<PosGlAccount, PosGlAccountLite, int>, IPosGlAccountRepository
    {
        #region IRepository< PosGlAccount, int > Members

        public PosGlAccountRepository()
        {
            this.AddSPName = "SP_PosGlAccount_Add";
            this.UpdateSPName = "SP_PosGlAccount_Update";
            this.DeleteSPName = "SP_PosGlAccount_Delete";
            this.DeleteLogicalSPName = "SP_PosGlAccount_DeleteLogical";
            this.FindAllSPName = "SP_PosGlAccount_FindAll";
            this.FindByIdSPName = "SP_PosGlAccount_FindById";
            this.FindByParentSPName = "SP_PosGlAccount_FindByParentId";
            this.FindAllLiteSPName = "SP_PosGlAccount_FindAll_Lite";
            this.FindByIdLiteSPName = "SP_PosGlAccount_FindById_Lite";
            this.AdvancedSearchSPName = "SP_PosGlAccount_AdvancedSearch_Lite";
        }

		#endregion
    }
}