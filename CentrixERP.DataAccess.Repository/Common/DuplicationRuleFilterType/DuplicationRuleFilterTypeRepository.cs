using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SagePOS.Common.DataAccess.IRepository;
using SagePOS.Manger.Common.Business.Entity;
using SF.FrameworkEntity;
using SF.Framework.Data;
using System.Data.SqlClient;
using System.Data;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class DuplicationRuleFilterTypeRepository : IDuplicationRuleFilterTypeRepository
    {
       
        #region IRepository<DuplicationRuleFilterType,int> Members

        public void Add(ref SagePOS.Manger.Common.Business.Entity.DuplicationRuleFilterType myEntity)
        {
            throw new NotImplementedException();
        }

        public void Delete(ref SagePOS.Manger.Common.Business.Entity.DuplicationRuleFilterType myEntity)
        {
            throw new NotImplementedException();
        }

        public void DeleteLogical(ref SagePOS.Manger.Common.Business.Entity.DuplicationRuleFilterType myEntity)
        {
            throw new NotImplementedException();
        }

        public List<SagePOS.Manger.Common.Business.Entity.DuplicationRuleFilterType> FindAll(SF.FrameworkEntity.CriteriaBase myCriteria)
        {
            throw new NotImplementedException();
        }

        public SagePOS.Manger.Common.Business.Entity.DuplicationRuleFilterType FindById(int Id, SF.FrameworkEntity.CriteriaBase myCriteria)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@id", SqlDbType.Int, Id);
            SqlDataReader reader;
            reader = Instance.ExcuteReader("dbo.SP_DuplicationRuleFilterType_FindByID", conn);

            if (reader != null && reader.HasRows)
            {
                reader.Read();
                DuplicationRuleFilterType myDuplicationrule = GetDuplicationRuleFilterMapper(reader);
                reader.Close();
                return myDuplicationrule;
            }
            else
                return null;
        }

        public List<SagePOS.Manger.Common.Business.Entity.DuplicationRuleFilterType> FindByParentId<TParentId>(TParentId ParentId, Type ParentType, SF.FrameworkEntity.CriteriaBase myCriteria)
        {
            throw new NotImplementedException();
        }

        public void Update(ref SagePOS.Manger.Common.Business.Entity.DuplicationRuleFilterType myEntity)
        {
            throw new NotImplementedException();
        }
        public List<DuplicationRuleFilterType> DuplicationFilter_FindAll(CriteriaBase myCriteria)
        {
            List<DuplicationRuleFilterType> DuplicationRuleFilterList = new List<DuplicationRuleFilterType>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;
            DuplicationRuleCriteria Criteria = (DuplicationRuleCriteria)myCriteria;

            reader = Instance.ExcuteReader("dbo.SP_DuplicationRuleFilter_FindAll", conn);

            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                    DuplicationRuleFilterList.Add(GetDuplicationRuleFilterMapper(reader));
                reader.Close();
                return DuplicationRuleFilterList;
            }
            else
                return null;
        }


        public string CustomConnectionString
        {
            get;
            set;
        }

        public bool UseCustomConnectionString
        {
            get;
            set;
        }
        public Object BeginTransaction()
        {
            return new object();
        }


        public void CommitTransaction(Object object1)
        {
        }

        public void Rollback(Object object1)
        {

        }

        public void KillConnection()
        {

        }


        public bool UseSharedSession { get; set; }

        #endregion
    }
}
