using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using SagePOS.Manger.Common.Business.Entity;
using SagePOS.Common.DataAccess.IRepository;
using System.Data.SqlClient;


namespace CentrixERP.Common.DataAccess.Repository
{

    public partial class CityRepository : ICityRepository
    {
        #region IRepository<City,int> Members
        public void Add(ref SagePOS.Manger.Common.Business.Entity.City myEntity)
        {
             SQLDAHelper Instance = new SQLDAHelper(); 
 SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@city_name_en", System.Data.SqlDbType.NVarChar, myEntity.CityNameEn);
            Instance.AddInParameter("@city_name_ar", System.Data.SqlDbType.NVarChar, myEntity.CityNameAr);
            Instance.AddInParameter("@country_id", System.Data.SqlDbType.Int, myEntity.CountryId);

            Instance.AddOutParameter("@Id", System.Data.SqlDbType.Int);


            Instance.ExcuteNonQuery("dbo.SP_City_Add", conn, false);

            myEntity.CityId = (int)Instance.GetOutParamValue("@Id");
        }
        public void Delete(ref SagePOS.Manger.Common.Business.Entity.City myEntity)
        {
             SQLDAHelper Instance = new SQLDAHelper(); 
 SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@Id", System.Data.SqlDbType.Int, myEntity.CityId);

            Instance.ExcuteNonQuery("dbo.SP_City_Delete", conn, false);


        }
        public void DeleteLogical(ref SagePOS.Manger.Common.Business.Entity.City myEntity)
        {
             SQLDAHelper Instance = new SQLDAHelper(); 
 SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@Id", System.Data.SqlDbType.Int, myEntity.CityId);
            try
            {
                Instance.ExcuteNonQuery("dbo.SP_City_DeleteLogical", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<SagePOS.Manger.Common.Business.Entity.City> FindAll(CriteriaBase myCriteria)
        {
            List<City> myCity = new List<City>();
             SQLDAHelper Instance = new SQLDAHelper(); 
            SqlConnection conn = Instance.GetConnection(false);
            if (myCriteria != null)
            {
                CityCriteria myCtr = (CityCriteria)myCriteria;
                if (!string.IsNullOrEmpty(myCtr.Name))
                {
                    Instance.AddInParameter("@Keyword", System.Data.SqlDbType.NVarChar, myCtr.Name);
                }

            }
            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_City_FindAll", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    myCity.Add(GetMapper(reader));
                }
                reader.Close();
                return myCity;
            }
            else
            {
                return null;
            }


        }
        public SagePOS.Manger.Common.Business.Entity.City FindById(int Id, CriteriaBase myCriteria)
        {
             SQLDAHelper Instance = new SQLDAHelper(); 
 SqlConnection conn = Instance.GetConnection(false);

            Instance.AddInParameter("@Id", System.Data.SqlDbType.Int, Id);

            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_City_FindById", conn);
            if (reader != null && reader.HasRows)
            {
                reader.Read();
                City city = GetMapper(reader);
                reader.Close();
                return city;

            }
            else
            {
                return null;
            }
        }
        public List<SagePOS.Manger.Common.Business.Entity.City> FindByParentId<TParentId>(TParentId ParentId, Type ParentType, CriteriaBase myCriteria)
        {
             SQLDAHelper Instance = new SQLDAHelper(); 
             SqlConnection conn = Instance.GetConnection(false);
             if (myCriteria != null)
             {
                 CityCriteria myCtr = (CityCriteria)myCriteria;
                 if (!string.IsNullOrEmpty(myCtr.Name))
                 {
                     Instance.AddInParameter("@keyword", System.Data.SqlDbType.NVarChar, myCtr.Name);
                 }

             }

            Instance.AddInParameter("@CountryId", System.Data.SqlDbType.Int, ParentId);

            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_City_FindByParentId", conn);
            List<City> myCity = new List<City>();
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    myCity.Add(GetMapper(reader));
                }
                reader.Close();
                return myCity;

            }
            else
            {
                return null;
            }
        }
        public void Update(ref SagePOS.Manger.Common.Business.Entity.City myEntity)
        {
             SQLDAHelper Instance = new SQLDAHelper(); 
 SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@city_name_en", System.Data.SqlDbType.NVarChar, myEntity.CityNameEn);
            Instance.AddInParameter("@city_name_ar", System.Data.SqlDbType.NVarChar, myEntity.CityNameAr);
            Instance.AddInParameter("@country_id", System.Data.SqlDbType.Int, myEntity.CountryId);
            Instance.AddInParameter("@Id", System.Data.SqlDbType.Int, myEntity.CityId);


            Instance.ExcuteNonQuery("dbo.SP_City_Update", conn, false);


        }

        public string CustomConnectionString
        {
            get;
            set;
        }

        public bool UseCustomConnectionString
        {
            get;
            set;
        }
        public Object BeginTransaction()
        {
            return new object();
        }


        public void CommitTransaction(Object object1)
        {
        }

        public void Rollback(Object object1)
        {

        }

        public void KillConnection()
        {

        }


        public bool UseSharedSession { get; set; }

        #endregion
    }
}
