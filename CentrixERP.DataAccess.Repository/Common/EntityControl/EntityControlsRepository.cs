
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using SagePOS.Manger.Common.Business.Entity;
using SagePOS.Common.DataAccess.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class EntityControlsRepository : IEntityControlsRepository
    {
        #region IRepository< EntityControls, int > Members

        public void Add(ref EntityControls myEntity)
        {
            throw new NotImplementedException();
        }

        public void Update(ref EntityControls myEntity)
        {
            throw new NotImplementedException();
        }

        public EntityControls FindById(int Id, CriteriaBase myCriteria)
        {
            throw new NotImplementedException();
        }
        public List<EntityControls> FindAll(CriteriaBase myCriteria)
        {
            List<EntityControls> EntityControlsList = new List<EntityControls>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;


            if (myCriteria != null)
            {
                EntityControlsCriteria criteria = (EntityControlsCriteria)myCriteria;
                if (criteria.EntityId.HasValue)
                    Instance.AddInParameter("@entity_id", SqlDbType.Int, criteria.EntityId.Value);

            }	
            reader = Instance.ExcuteReader("dbo.SP_EntityControls_FindAll", conn);

            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                    EntityControlsList.Add(GetMapper(reader));
                reader.Close();
                return EntityControlsList;
            }
            else
                return null;

        }

        public List<EntityControls> FindByParentId<TParentId>(TParentId ParentId, Type ParentType, CriteriaBase myCriteria)
        {
            List<EntityControls> EntityControlsList = new List<EntityControls>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);

			if (ParentType == typeof(SystemEntity))
				Instance.AddInParameter("@entity_id", SqlDbType.Int, ParentId);
	

            if (myCriteria != null)
            {
                EntityControlsCriteria criteria = (EntityControlsCriteria)myCriteria;
            }			

            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_EntityControls_FindByParentId", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    EntityControlsList.Add(GetMapper(reader));
                }
                reader.Close();
                return EntityControlsList;
            }
            else
            {
                return null;
            }
        }

        public void Delete(ref EntityControls myEntity)
        {
            throw new NotImplementedException();
        }

        public void DeleteLogical(ref EntityControls myEntity)
        {
            throw new NotImplementedException();
        }

        public string CustomConnectionString
        {
            get;
            set;
        }

        public bool UseCustomConnectionString
        {
            get;
            set;
        }
        public Object BeginTransaction()
        {
            return new object();
        }


        public void CommitTransaction(Object object1)
        {
        }

        public void Rollback(Object object1)
        {

        }

        public void KillConnection()
        {

        }


        public bool UseSharedSession { get; set; }
		#endregion
    }
}