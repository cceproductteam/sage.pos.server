using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SagePOS.Server.Business.Entity;
using SF.Framework.Data;
using System.Data.SqlClient;

namespace SagePOS.Server.DataAcces.Repository
{
    public partial class CustomerRepository
    {
        public Customer GetMapper(SqlDataReader reader)
        {
            Customer obj = new Customer();
			if (reader["customer_id"] != DBNull.Value)
			{
				obj.CustomerId = (int)reader["customer_id"];
			}
			if (reader["customer_number"] != DBNull.Value)
			{
				obj.CustomerNumber = (string)reader["customer_number"];
			}
			if (reader["customer_name"] != DBNull.Value)
			{
				obj.CustomerName = (string)reader["customer_name"];
			}
			if (reader["customer_group_id"] != DBNull.Value)
			{
				obj.CustomerGroupId = (int)reader["customer_group_id"];
			}
			if (reader["on_hold"] != DBNull.Value)
			{
				obj.OnHold = (bool)reader["on_hold"];
			}
			if (reader["status"] != DBNull.Value)
			{
				obj.Status = (bool)reader["status"];
			}
			if (reader["short_name"] != DBNull.Value)
			{
				obj.ShortName = (string)reader["short_name"];
			}
			if (reader["start_date"] != DBNull.Value)
			{
				obj.StartDate = (DateTime)reader["start_date"];
			}
			if (reader["address_line1"] != DBNull.Value)
			{
				obj.AddressLine1 = (string)reader["address_line1"];
			}
			if (reader["address_line2"] != DBNull.Value)
			{
				obj.AddressLine2 = (string)reader["address_line2"];
			}
			if (reader["address_line3"] != DBNull.Value)
			{
				obj.AddressLine3 = (string)reader["address_line3"];
			}
			if (reader["address_line4"] != DBNull.Value)
			{
				obj.AddressLine4 = (string)reader["address_line4"];
			}
			if (reader["city"] != DBNull.Value)
			{
				obj.City = (string)reader["city"];
			}
			if (reader["country"] != DBNull.Value)
			{
				obj.Country = (string)reader["country"];
			}
			if (reader["state_prov"] != DBNull.Value)
			{
				obj.StateProv = (string)reader["state_prov"];
			}
			if (reader["postal_code"] != DBNull.Value)
			{
				obj.PostalCode = (string)reader["postal_code"];
			}
			if (reader["telephone"] != DBNull.Value)
			{
				obj.Telephone = (string)reader["telephone"];
			}
			if (reader["email"] != DBNull.Value)
			{
				obj.Email = (string)reader["email"];
			}
			if (reader["website"] != DBNull.Value)
			{
				obj.Website = (string)reader["website"];
			}
			if (reader["contact"] != DBNull.Value)
			{
				obj.Contact = (string)reader["contact"];
			}
			if (reader["contact_telephone"] != DBNull.Value)
			{
				obj.ContactTelephone = (string)reader["contact_telephone"];
			}
			if (reader["contact_fax"] != DBNull.Value)
			{
				obj.ContactFax = (string)reader["contact_fax"];
			}
			if (reader["contact_email"] != DBNull.Value)
			{
				obj.ContactEmail = (string)reader["contact_email"];
			}
			if (reader["account_type_id"] != DBNull.Value)
			{
				obj.AccountTypeId = (int)reader["account_type_id"];
			}
			if (reader["print_statements"] != DBNull.Value)
			{
				obj.PrintStatements = (bool)reader["print_statements"];
			}
			if (reader["account_set_id"] != DBNull.Value)
			{
				obj.AccountSetId = (int)reader["account_set_id"];
			}
			if (reader["payment_term_code_id"] != DBNull.Value)
			{
				obj.PaymentTermCodeId = (int)reader["payment_term_code_id"];
			}
			if (reader["payment_code_id"] != DBNull.Value)
			{
				obj.PaymentCodeId = (int)reader["payment_code_id"];
			}
			if (reader["delivery_method_id"] != DBNull.Value)
			{
				obj.DeliveryMethodId = (int)reader["delivery_method_id"];
			}
			if (reader["outstanding_balance_exceeds_limit"] != DBNull.Value)
			{
				obj.OutstandingBalanceExceedsLimit = (bool)reader["outstanding_balance_exceeds_limit"];
			}
			if (reader["credit_limit_amount"] != DBNull.Value)
			{
				obj.CreditLimitAmount = (double)reader["credit_limit_amount"];
			}
			if (reader["transactions_overdue_ndays_exceed_overdue_limit"] != DBNull.Value)
			{
				obj.TransactionsOverdueNdaysExceedOverdueLimit = (bool)reader["transactions_overdue_ndays_exceed_overdue_limit"];
			}
			if (reader["n_days"] != DBNull.Value)
			{
				obj.NDays = (int)reader["n_days"];
			}
			if (reader["overdue_limit"] != DBNull.Value)
			{
                obj.OverDueLimit = (double)reader["overdue_limit"];
			}            
			if (reader["customer_type_id"] != DBNull.Value)
			{
				obj.CustomerTypeId = (int)reader["customer_type_id"];
			}
			if (reader["check_duplicat_pos_id"] != DBNull.Value)
			{
				obj.CheckDuplicatPosId = (int)reader["check_duplicat_pos_id"];
			}
			if (reader["tax_group_id"] != DBNull.Value)
			{
				obj.TaxGroupId = (int)reader["tax_group_id"];
			}
			if (reader["tax_authority_id"] != DBNull.Value)
			{
				obj.TaxAuthorityId = (int)reader["tax_authority_id"];
			}
			if (reader["tax_class_id"] != DBNull.Value)
			{
				obj.TaxClassId = (int)reader["tax_class_id"];
			}
			if (reader["sales_person_id"] != DBNull.Value)
			{
				obj.SalesPersonId = (int)reader["sales_person_id"];
			}
			if (reader["percentage"] != DBNull.Value)
			{
                obj.Percentage = (double)reader["percentage"];
			}
			if (reader["created_date"] != DBNull.Value)
			{
				obj.CreatedDate = (DateTime)reader["created_date"];
			}
			if (reader["created_by"] != DBNull.Value)
			{
				obj.CreatedBy = (int)reader["created_by"];
			}
			if (reader["updated_date"] != DBNull.Value)
			{
				obj.UpdatedDate = (DateTime)reader["updated_date"];
			}
			if (reader["updated_by"] != DBNull.Value)
			{
				obj.UpdatedBy = (int)reader["updated_by"];
            }
            if (reader["cr_company_id"] != DBNull.Value)
            {
                obj.CrCompanyId = Convert.ToInt32(reader["cr_company_id"]);
            }
            if (reader["cr_company_name"] != DBNull.Value)
            {
                obj.CrCompanyName = Convert.ToString(reader["cr_company_name"]);
            }

            return obj;
        }

        public CustomerLite GetFindByIdLiteMapper(SqlDataReader reader)
        {
            CustomerLite Obj = new CustomerLite();

            if (reader["customer_id"] != DBNull.Value)
            {
                Obj.CustomerId = (int)reader["customer_id"];
                Obj.Id = (int)reader["customer_id"];
            }
            if (reader["customer_number"] != DBNull.Value)
            {
                Obj.CustomerNumber = (string)reader["customer_number"];
            }
            if (reader["customer_name"] != DBNull.Value)
            {
                Obj.CustomerName = (string)reader["customer_name"];
            }
            if (reader["customer_group_id"] != DBNull.Value)
            {
                Obj.CustomerGroupId = (int)reader["customer_group_id"];
            }
            if (reader["customer_group"] != DBNull.Value)
            {
                Obj.CustomerGroup = (string)reader["customer_group"];
            }

            if (reader["on_hold"] != DBNull.Value)
            {
                Obj.OnHoldValue = (bool)reader["on_hold"];
            }
            if (reader["on_hold_value"] != DBNull.Value)
            {
                Obj.OnHold = (string)reader["on_hold_value"];
            }
            if (reader["status_value"] != DBNull.Value)
            {
                Obj.Status = (string)reader["status_value"];
            }
            if (reader["status"] != DBNull.Value)
            {
                Obj.StatusValue = (bool)reader["status"];
            }


            if (reader["short_name"] != DBNull.Value)
            {
                Obj.ShortName = (string)reader["short_name"];
            }
            if (reader["start_date"] != DBNull.Value)
            {
                Obj.StartDate = (System.DateTime)reader["start_date"];
            }
            if (reader["address_line1"] != DBNull.Value)
            {
                Obj.AddressLine1 = (string)reader["address_line1"];
            }
            if (reader["address_line2"] != DBNull.Value)
            {
                Obj.AddressLine2 = (string)reader["address_line2"];
            }
            if (reader["address_line3"] != DBNull.Value)
            {
                Obj.AddressLine3 = (string)reader["address_line3"];
            }
            if (reader["address_line4"] != DBNull.Value)
            {
                Obj.AddressLine4 = (string)reader["address_line4"];
            }
            if (reader["city"] != DBNull.Value)
            {
                Obj.City = (string)reader["city"];
            }
            if (reader["country"] != DBNull.Value)
            {
                Obj.Country = (string)reader["country"];
            }
            if (reader["state_prov"] != DBNull.Value)
            {
                Obj.StateProv = (string)reader["state_prov"];
            }
            if (reader["postal_code"] != DBNull.Value)
            {
                Obj.PostalCode = (string)reader["postal_code"];
            }
            if (reader["telephone"] != DBNull.Value)
            {
                Obj.Telephone = (string)reader["telephone"];
            }
            if (reader["email"] != DBNull.Value)
            {
                Obj.Email = (string)reader["email"];
            }
            if (reader["website"] != DBNull.Value)
            {
                Obj.Website = (string)reader["website"];
            }
            if (reader["contact"] != DBNull.Value)
            {
                Obj.Contact = (string)reader["contact"];
            }
            if (reader["contact_telephone"] != DBNull.Value)
            {
                Obj.ContactTelephone = (string)reader["contact_telephone"];
            }
            if (reader["contact_fax"] != DBNull.Value)
            {
                Obj.ContactFax = (string)reader["contact_fax"];
            }
            if (reader["contact_email"] != DBNull.Value)
            {
                Obj.ContactEmail = (string)reader["contact_email"];
            }
            if (reader["account_type_id"] != DBNull.Value)
            {
                Obj.AccountTypeId = (int)reader["account_type_id"];
            }
            if (reader["account_type_en"] != DBNull.Value)
            {
                Obj.AccountTypeEn = (string)reader["account_type_en"];
            }
            if (reader["account_type_ar"] != DBNull.Value)
            {
                Obj.AccountTypeAr = (string)reader["account_type_ar"];
            }
            if (reader["print_statements_value"] != DBNull.Value)
            {
                Obj.PrintStatements = (string)reader["print_statements_value"];
            }
            if (reader["print_statements"] != DBNull.Value)
            {
                Obj.PrintStatementsValue = (bool)reader["print_statements"];
            }

            if (reader["account_set_id"] != DBNull.Value)
            {
                Obj.AccountSetId = (int)reader["account_set_id"];
            }
            if (reader["account_set"] != DBNull.Value)
            {
                Obj.AccountSet = (string)reader["account_set"];
            }
            if (reader["payment_term_code_id"] != DBNull.Value)
            {
                Obj.PaymentTermCodeId = (int)reader["payment_term_code_id"];
            }
            if (reader["payment_term_code"] != DBNull.Value)
            {
                Obj.PaymentTermCode = (string)reader["payment_term_code"];
            }
            if (reader["payment_code_id"] != DBNull.Value)
            {
                Obj.PaymentCodeId = (int)reader["payment_code_id"];
            }
            if (reader["payment_code"] != DBNull.Value)
            {
                Obj.PaymentCode = (string)reader["payment_code"];
            }
            if (reader["delivery_method_id"] != DBNull.Value)
            {
                Obj.DeliveryMethodId = (int)reader["delivery_method_id"];
            }
            if (reader["delivery_method_en"] != DBNull.Value)
            {
                Obj.DeliveryMethodEn = (string)reader["delivery_method_en"];
            }
            if (reader["delivery_method_ar"] != DBNull.Value)
            {
                Obj.DeliveryMethodAr = (string)reader["delivery_method_ar"];
            }
            if (reader["outstanding_balance_exceeds_limit_value"] != DBNull.Value)
            {
                Obj.OutstandingBalanceExceedsLimit = (string)reader["outstanding_balance_exceeds_limit_value"];
            }
            if (reader["outstanding_balance_exceeds_limit"] != DBNull.Value)
            {
                Obj.OutstandingBalanceExceedsLimitValue = (bool)reader["outstanding_balance_exceeds_limit"];
            }


            if (reader["credit_limit_amount"] != DBNull.Value)
            {
                Obj.CreditLimitAmount = (double)reader["credit_limit_amount"];
            }
            if (reader["transactions_overdue_ndays_exceed_overdue_limit_value"] != DBNull.Value)
            {
                Obj.TransactionsOverdueNdaysExceedOverdueLimit = (string)reader["transactions_overdue_ndays_exceed_overdue_limit_value"];
            }
            if (reader["transactions_overdue_ndays_exceed_overdue_limit"] != DBNull.Value)
            {
                Obj.TransactionsOverdueNdaysExceedOverdueLimitValue = (bool)reader["transactions_overdue_ndays_exceed_overdue_limit"];
            }


            if (reader["n_days"] != DBNull.Value)
            {
                Obj.NDays = (int)reader["n_days"];
            }
            if (reader["overdue_limit"] != DBNull.Value)
            {
                Obj.OverDueLimit = (double)reader["overdue_limit"];
            }  
            if (reader["customer_type_id"] != DBNull.Value)
            {
                Obj.CustomerTypeId = (int)reader["customer_type_id"];
            }
            if (reader["customer_type_en"] != DBNull.Value)
            {
                Obj.CustomerTypeEn = (string)reader["customer_type_en"];
            }
            if (reader["customer_type_ar"] != DBNull.Value)
            {
                Obj.CustomerTypeAr = (string)reader["customer_type_ar"];
            }
            if (reader["check_duplicat_pos_id"] != DBNull.Value)
            {
                Obj.CheckDuplicatPosId = (int)reader["check_duplicat_pos_id"];
            }
            if (reader["check_duplicat_pos_en"] != DBNull.Value)
            {
                Obj.CheckDuplicatPosEn = (string)reader["check_duplicat_pos_en"];
            }

            if (reader["check_duplicat_pos_ar"] != DBNull.Value)
            {
                Obj.CheckDuplicatPosAr = (string)reader["check_duplicat_pos_ar"];
            }
            if (reader["tax_group_id"] != DBNull.Value)
            {
                Obj.TaxGroupId = (int)reader["tax_group_id"];
            }
            if (reader["tax_group"] != DBNull.Value)
            {
                Obj.TaxGroup = (string)reader["tax_group"];
            }
            if (reader["tax_authority_id"] != DBNull.Value)
            {
                Obj.TaxAuthorityId = (int)reader["tax_authority_id"];
            }
            if (reader["tax_authority"] != DBNull.Value)
            {
                Obj.TaxAuthority = (string)reader["tax_authority"];
            }
            if (reader["tax_class_id"] != DBNull.Value)
            {
                Obj.TaxClassId = (int)reader["tax_class_id"];
            }
            if (reader["tax_class"] != DBNull.Value)
            {
                Obj.TaxClass = (string)reader["tax_class"];
            }
            if (reader["sales_person_id"] != DBNull.Value)
            {
                Obj.SalesPersonId = (int)reader["sales_person_id"];
            }
            if (reader["sales_person"] != DBNull.Value)
            {
                Obj.SalesPerson = (string)reader["sales_person"];
            }
            if (reader["percentage"] != DBNull.Value)
            {
                Obj.Percentage = (double)reader["percentage"];
            }
            if (reader["created_by_name"] != DBNull.Value)
            {
                Obj.CreatedBy = (string)reader["created_by_name"];
            }
            if (reader["updated_by_name"] != DBNull.Value)
            {
                Obj.UpdatedBy = (string)reader["updated_by_name"];
            }
            if (reader["created_by"] != DBNull.Value)
            {
                Obj.CreatedById = (int)reader["created_by"];
            }
            if (reader["created_date"] != DBNull.Value)
            {
                Obj.CreatedDate = (System.DateTime)reader["created_date"];
            }
            if (reader["updated_by"] != DBNull.Value)
            {
                Obj.UpdatedById = (int)reader["updated_by"];
            }
            if (reader["updated_date"] != DBNull.Value)
            {
                Obj.UpdatedDate = (System.DateTime)reader["updated_date"];
            }
            if (reader["cr_company_id"] != DBNull.Value)
            {
                Obj.CrCompanyId = Convert.ToInt32(reader["cr_company_id"]);
            }
            if (reader["cr_company_name"] != DBNull.Value)
            {
                Obj.CrCompanyName = Convert.ToString(reader["cr_company_name"]);
            }


            if (reader["total_invoices"] != DBNull.Value)
            {
                Obj.TotalInvoices = Convert.ToDouble(reader["total_invoices"]);
            }
            if (reader["total_receipts"] != DBNull.Value)
            {
                Obj.TotalReceipts = Convert.ToDouble(reader["total_receipts"]);
            }
            if (reader["balance"] != DBNull.Value)
            {
                Obj.CustomerBalance = Convert.ToDouble(reader["balance"]);
            }
            return Obj;
        }

        public CustomerLite GetFindAllLiteMapper(SqlDataReader reader)
        {
            CustomerLite Obj = new CustomerLite();

            if (reader["customer_id"] != DBNull.Value)
            {
                Obj.CustomerId = (int)reader["customer_id"];
                Obj.Id = Obj.value = Obj.CustomerId;
            }
            if (reader["customer_number"] != DBNull.Value)
            {
                Obj.CustomerNumber = (string)reader["customer_number"];
                Obj.label = Obj.CustomerNumber;
            }
            if (reader["customer_name"] != DBNull.Value)
            {
                Obj.CustomerName = (string)reader["customer_name"];
                Obj.label += " - " + Obj.CustomerName;
            }
            if (reader["customer_group_id"] != DBNull.Value)
            {
                Obj.CustomerGroupId = (int)reader["customer_group_id"];
            }
            if (reader["customer_group"] != DBNull.Value)
            {
                Obj.CustomerGroup = (string)reader["customer_group"];
            }

            if (reader["on_hold"] != DBNull.Value)
            {
                Obj.OnHoldValue = (bool)reader["on_hold"];
            }

            if (reader["last_rows"] != DBNull.Value)
            {
                Obj.LastRows = (bool)reader["last_rows"];
            }
            if (reader["total_count"] != DBNull.Value)
            {
                Obj.TotalRecords = (int)reader["total_count"];
            }

            if (reader["tax_authority_id"] != DBNull.Value)
            {
                Obj.TaxAuthorityId = (int)reader["tax_authority_id"];
            }
            if (reader["current_exchange_rate"] != DBNull.Value)
            {
                Obj.CurrentExchangeRate = Convert.ToDouble(reader["current_exchange_rate"]);
            }

            if (reader["payment_term_code_id"] != DBNull.Value)
            {
                Obj.PaymentTermCodeId = Convert.ToInt32(reader["payment_term_code_id"]);
            }
            if (reader["payment_term_code"] != DBNull.Value)
            {
                Obj.PaymentTermCode = Convert.ToString(reader["payment_term_code"]);
            }
            if (reader["tax_class_id"] != DBNull.Value)
            {
                Obj.TaxClassId = Convert.ToInt32(reader["tax_class_id"]);
            }
            if (reader["tax_class"] != DBNull.Value)
            {
                Obj.TaxClass = Convert.ToString(reader["tax_class"]);
            }
            if (reader["currency_id"] != DBNull.Value)
            {
                Obj.CustomerCurrencyId = Convert.ToInt32(reader["currency_id"]);
            }

            if (reader["currency"] != DBNull.Value)
            {
                Obj.CustomerCurrency = Convert.ToString(reader["currency"]);

            }
            if (reader["currency_symbol"] != DBNull.Value)
            {
                Obj.CustomerCurrencySymbol = Convert.ToString(reader["currency_symbol"]);

            }
            return Obj;
        }

        public POSIntegrationCustomer GetPOSIntegrationCustomerMapper(SqlDataReader reader)
        {
            POSIntegrationCustomer obj = new POSIntegrationCustomer();

            if (reader["customer_id"] != DBNull.Value)
            {
                obj.CustomerId = (int)reader["customer_id"];
            }
            if (reader["taxable"] != DBNull.Value)
            {
                obj.taxable = (bool)reader["taxable"];
            }
            if (reader["customer_number"] != DBNull.Value)
            {
                obj.CustomerNumber = (string)reader["customer_number"];
            }
            if (reader["customer_name"] != DBNull.Value)
            {
                obj.CustomerName = (string)reader["customer_name"];
            }
            if (reader["tax_class_id"] != DBNull.Value)
            {
                obj.TaxClassId = (int)reader["tax_class_id"];
            }
            if (reader["credit_limit_amount"] != DBNull.Value)
            {
                obj.CreditLimitAmount = (double)reader["credit_limit_amount"];
            }

            if (reader["contact_telephone"] != DBNull.Value)
            {
                obj.ContactPhone = (string)reader["contact_telephone"];
            }
            if (reader["contact_email"] != DBNull.Value)
            {
                obj.ContactEmail = (string)reader["contact_email"];
            }
            if (reader["flag_deleted"] != DBNull.Value)
            {
                obj.FlagDeleted = (bool)reader["flag_deleted"];
            }
            if (reader["tax_authority_id"] != DBNull.Value)
            {
                obj.TaxAuthorityId = (int)reader["tax_authority_id"];
            }

            if (reader["currency_id"] != DBNull.Value)
            {
                obj.CurrencyId = (int)reader["currency_id"];
            }
            return obj;
        }
    }
}
