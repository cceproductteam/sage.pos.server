
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using SagePOS.Server.Business.Entity;
using SagePOS.Server.DataAcces.IRepository;
using System.Data.SqlClient;
using System.Data;


namespace SagePOS.Server.DataAcces.Repository
{
    public partial class CustomerRepository : ICustomerRepository
    {
        #region IRepository< Customer, int > Members

        public void Add(ref Customer myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);

            Instance.AddInParameter("@customer_number", SqlDbType.NVarChar, myEntity.CustomerNumber);
            Instance.AddInParameter("@customer_name", SqlDbType.NVarChar, myEntity.CustomerName);
            Instance.AddInParameter("@customer_group_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.CustomerGroupId));
            Instance.AddInParameter("@on_hold", SqlDbType.Bit, myEntity.OnHold);
            Instance.AddInParameter("@status", SqlDbType.Bit, myEntity.Status);
            Instance.AddInParameter("@short_name", SqlDbType.NVarChar, myEntity.ShortName);
            Instance.AddInParameter("@start_date", SqlDbType.DateTime, myEntity.StartDate);
            Instance.AddInParameter("@address_line1", SqlDbType.NVarChar, myEntity.AddressLine1);
            Instance.AddInParameter("@address_line2", SqlDbType.NVarChar, myEntity.AddressLine2);
            Instance.AddInParameter("@address_line3", SqlDbType.NVarChar, myEntity.AddressLine3);
            Instance.AddInParameter("@address_line4", SqlDbType.NVarChar, myEntity.AddressLine4);
            Instance.AddInParameter("@city", SqlDbType.NVarChar, myEntity.City);
            Instance.AddInParameter("@country", SqlDbType.NVarChar, myEntity.Country);
            Instance.AddInParameter("@state_prov", SqlDbType.NVarChar, myEntity.StateProv);
            Instance.AddInParameter("@postal_code", SqlDbType.NVarChar, myEntity.PostalCode);
            Instance.AddInParameter("@telephone", SqlDbType.NVarChar, myEntity.Telephone);
            Instance.AddInParameter("@email", SqlDbType.NVarChar, myEntity.Email);
            Instance.AddInParameter("@website", SqlDbType.NVarChar, myEntity.Website);
            Instance.AddInParameter("@contact", SqlDbType.NVarChar, myEntity.Contact);
            Instance.AddInParameter("@contact_telephone", SqlDbType.NVarChar, myEntity.ContactTelephone);
            Instance.AddInParameter("@contact_fax", SqlDbType.NVarChar, myEntity.ContactFax);
            Instance.AddInParameter("@contact_email", SqlDbType.NVarChar, myEntity.ContactEmail);
            Instance.AddInParameter("@account_type_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.AccountTypeId));
            Instance.AddInParameter("@print_statements", SqlDbType.Bit, myEntity.PrintStatements);
            Instance.AddInParameter("@account_set_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.AccountSetId));
            Instance.AddInParameter("@payment_term_code_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.PaymentTermCodeId));
            Instance.AddInParameter("@payment_code_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.PaymentCodeId));
            Instance.AddInParameter("@delivery_method_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.DeliveryMethodId));
            Instance.AddInParameter("@outstanding_balance_exceeds_limit", SqlDbType.Bit, myEntity.OutstandingBalanceExceedsLimit);
            Instance.AddInParameter("@credit_limit_amount", SqlDbType.Float, myEntity.CreditLimitAmount);
            Instance.AddInParameter("@transactions_overdue_ndays_exceed_overdue_limit", SqlDbType.Bit, myEntity.TransactionsOverdueNdaysExceedOverdueLimit);
            Instance.AddInParameter("@n_days", SqlDbType.Int, myEntity.NDays);
            Instance.AddInParameter("@customer_type_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.CustomerTypeId));
            Instance.AddInParameter("@check_duplicat_pos_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.CheckDuplicatPosId));
            Instance.AddInParameter("@tax_group_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.TaxGroupId));
            Instance.AddInParameter("@tax_authority_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.TaxAuthorityId));
            Instance.AddInParameter("@tax_class_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.TaxClassId));
            Instance.AddInParameter("@sales_person_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.SalesPersonId));
            Instance.AddInParameter("@percentage", SqlDbType.Float, myEntity.Percentage);
            Instance.AddInParameter("@created_by", SqlDbType.Int, myEntity.CreatedBy);
            Instance.AddInParameter("@overdue_limit", SqlDbType.Float, myEntity.OverDueLimit);
            Instance.AddInParameter("@cr_company_name", SqlDbType.NVarChar, myEntity.CrCompanyName);
            Instance.AddInParameter("@cr_company_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.CrCompanyId));
            Instance.AddOutParameter("@customer_id", SqlDbType.Int);

            Instance.ExcuteNonQuery("dbo.SP_Customer_Add", conn, false);

            myEntity.CustomerId = (int)Instance.GetOutParamValue("@customer_id");
        }

        public void Update(ref Customer myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@customer_id", SqlDbType.Int, myEntity.CustomerId);
            Instance.AddInParameter("@customer_number", SqlDbType.NVarChar, myEntity.CustomerNumber);
            Instance.AddInParameter("@customer_name", SqlDbType.NVarChar, myEntity.CustomerName);
            Instance.AddInParameter("@customer_group_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.CustomerGroupId));
            Instance.AddInParameter("@on_hold", SqlDbType.Bit, myEntity.OnHold);
            Instance.AddInParameter("@status", SqlDbType.Bit, myEntity.Status);
            Instance.AddInParameter("@short_name", SqlDbType.NVarChar, myEntity.ShortName);
            Instance.AddInParameter("@start_date", SqlDbType.DateTime, myEntity.StartDate);
            Instance.AddInParameter("@address_line1", SqlDbType.NVarChar, myEntity.AddressLine1);
            Instance.AddInParameter("@address_line2", SqlDbType.NVarChar, myEntity.AddressLine2);
            Instance.AddInParameter("@address_line3", SqlDbType.NVarChar, myEntity.AddressLine3);
            Instance.AddInParameter("@address_line4", SqlDbType.NVarChar, myEntity.AddressLine4);
            Instance.AddInParameter("@city", SqlDbType.NVarChar, myEntity.City);
            Instance.AddInParameter("@country", SqlDbType.NVarChar, myEntity.Country);
            Instance.AddInParameter("@state_prov", SqlDbType.NVarChar, myEntity.StateProv);
            Instance.AddInParameter("@postal_code", SqlDbType.NVarChar, myEntity.PostalCode);
            Instance.AddInParameter("@telephone", SqlDbType.NVarChar, myEntity.Telephone);
            Instance.AddInParameter("@email", SqlDbType.NVarChar, myEntity.Email);
            Instance.AddInParameter("@website", SqlDbType.NVarChar, myEntity.Website);
            Instance.AddInParameter("@contact", SqlDbType.NVarChar, myEntity.Contact);
            Instance.AddInParameter("@contact_telephone", SqlDbType.NVarChar, myEntity.ContactTelephone);
            Instance.AddInParameter("@contact_fax", SqlDbType.NVarChar, myEntity.ContactFax);
            Instance.AddInParameter("@contact_email", SqlDbType.NVarChar, myEntity.ContactEmail);
            Instance.AddInParameter("@account_type_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.AccountTypeId));
            Instance.AddInParameter("@print_statements", SqlDbType.Bit, myEntity.PrintStatements);
            Instance.AddInParameter("@account_set_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.AccountSetId));
            Instance.AddInParameter("@payment_term_code_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.PaymentTermCodeId));
            Instance.AddInParameter("@payment_code_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.PaymentCodeId));
            Instance.AddInParameter("@delivery_method_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.DeliveryMethodId));
            Instance.AddInParameter("@outstanding_balance_exceeds_limit", SqlDbType.Bit, myEntity.OutstandingBalanceExceedsLimit);
            Instance.AddInParameter("@credit_limit_amount", SqlDbType.Float, myEntity.CreditLimitAmount);
            Instance.AddInParameter("@transactions_overdue_ndays_exceed_overdue_limit", SqlDbType.Bit, myEntity.TransactionsOverdueNdaysExceedOverdueLimit);
            Instance.AddInParameter("@n_days", SqlDbType.Int, myEntity.NDays);
            Instance.AddInParameter("@customer_type_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.CustomerTypeId));
            Instance.AddInParameter("@check_duplicat_pos_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.CheckDuplicatPosId));
            Instance.AddInParameter("@tax_group_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.TaxGroupId));
            Instance.AddInParameter("@tax_authority_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.TaxAuthorityId));
            Instance.AddInParameter("@tax_class_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.TaxClassId));
            Instance.AddInParameter("@sales_person_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.SalesPersonId));
            Instance.AddInParameter("@percentage", SqlDbType.Float, myEntity.Percentage);
            Instance.AddInParameter("@updated_by", SqlDbType.Int, myEntity.UpdatedBy);
            Instance.AddInParameter("@overdue_limit", SqlDbType.Float, myEntity.OverDueLimit);
            Instance.AddOutParameter("@updated_date", SqlDbType.DateTime);

            Instance.ExcuteNonQuery("dbo.SP_Customer_Update", conn, false);
            myEntity.UpdatedDate = (DateTime)Instance.GetOutParamValue("@updated_date");
        }

        public Customer FindById(int Id, CriteriaBase myCriteria)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@customer_id", SqlDbType.Int, Id);
            SqlDataReader reader;

            reader = Instance.ExcuteReader("dbo.SP_Customer_FindById", conn);

            if (reader != null && reader.HasRows)
            {
                reader.Read();
                Customer myCustomer = GetMapper(reader);
                reader.Close();
                return myCustomer;
            }
            else
                return null;
        }

        public List<Customer> FindAll(CriteriaBase myCriteria)
        {
            List<Customer> CustomerList = new List<Customer>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;
            if (myCriteria != null)
            {
                CustomerCriteria criteria = (CustomerCriteria)myCriteria;
            }
            reader = Instance.ExcuteReader("dbo.SP_Customer_FindAll", conn);

            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                    CustomerList.Add(GetMapper(reader));
                reader.Close();
                return CustomerList;
            }
            else
                return null;

        }

        public List<Customer> FindByParentId<TParentId>(TParentId ParentId, Type ParentType, CriteriaBase myCriteria)
        {
            List<Customer> CustomerList = new List<Customer>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);

            //if (ParentType == typeof(CustomerGroup))
            //    Instance.AddInParameter("@customer_group_id", SqlDbType.Int, ParentId);
            //else if (ParentType == typeof(AccountSet))
            //    Instance.AddInParameter("@account_set_id", SqlDbType.Int, ParentId);
            //else if (ParentType == typeof(PaymentTerm))
            //    Instance.AddInParameter("@payment_term_code_id", SqlDbType.Int, ParentId);
            //else if (ParentType == typeof(PaymentCode))
            //    Instance.AddInParameter("@payment_code_id", SqlDbType.Int, ParentId);
            //else if (ParentType == typeof(TaxGroupHeader))
            //    Instance.AddInParameter("@tax_group_id", SqlDbType.Int, ParentId);
            //else if (ParentType == typeof(TaxAuthority))
            //    Instance.AddInParameter("@tax_authority_id", SqlDbType.Int, ParentId);
            //else if (ParentType == typeof(TaxClassHeader))
            //    Instance.AddInParameter("@tax_class_id", SqlDbType.Int, ParentId);
            //else if (ParentType == typeof(SalesPerson))
            //    Instance.AddInParameter("@sales_person_id", SqlDbType.Int, ParentId);


            if (myCriteria != null)
            {
                CustomerCriteria criteria = (CustomerCriteria)myCriteria;
            }

            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_Customer_FindByParentId", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    CustomerList.Add(GetMapper(reader));
                }
                reader.Close();
                return CustomerList;
            }
            else
            {
                return null;
            }
        }

        public void Delete(ref Customer myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@customer_id", SqlDbType.Int, myEntity.CustomerId);

            Instance.ExcuteNonQuery("dbo.SP_Customer_Delete", conn, false);
        }

        public void DeleteLogical(ref Customer myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@customer_id", SqlDbType.Int, myEntity.CustomerId);

            Instance.ExcuteNonQuery("dbo.SP_Customer_DeleteLogical", conn, false);
        }

        public CustomerLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@customer_id", SqlDbType.Int, Id);
            SqlDataReader reader;

            reader = Instance.ExcuteReader("dbo.SP_Customer_FindById_Lite", conn);

            if (reader != null && reader.HasRows)
            {
                reader.Read();
                CustomerLite myCustomer = GetFindByIdLiteMapper(reader);
                reader.Close();
                return myCustomer;
            }
            else
                return null;
        }

        public List<CustomerLite> FindAllLite(CriteriaBase myCriteria)
        {
            List<CustomerLite> CustomerList = new List<CustomerLite>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;
            if (myCriteria != null)
            {
                CustomerCriteria criteria = (CustomerCriteria)myCriteria;
                if (!string.IsNullOrEmpty(criteria.KeyWord))
                    Instance.AddInParameter("@keyword", SqlDbType.NVarChar, criteria.KeyWord);
                if (!criteria.pageNumber.Equals(-1))
                    Instance.AddInParameter("@page_number", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(criteria.pageNumber));
                if (!criteria.resultCount.Equals(-1))
                    Instance.AddInParameter("@result_count", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(criteria.resultCount));
                if (criteria.CustomerId.HasValue)
                    Instance.AddInParameter("@customer_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(criteria.CustomerId.Value));
                if (criteria.Status.HasValue)
                    Instance.AddInParameter("@status", SqlDbType.Bit, criteria.Status.Value);
                if (criteria.OnHold.HasValue)
                    Instance.AddInParameter("@on_hold", SqlDbType.Bit, criteria.OnHold.Value);
                if (criteria.CustomerGroupId > 0)
                    Instance.AddInParameter("@customer_group_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(criteria.CustomerGroupId));

                if (criteria.CurrencyId.HasValue)
                    Instance.AddInParameter("@currency_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(criteria.CurrencyId.Value));
            }
            reader = Instance.ExcuteReader("dbo.SP_Customer_FindAll_Lite", conn);

            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                    CustomerList.Add(GetFindAllLiteMapper(reader));
                reader.Close();
                return CustomerList;
            }
            else
                return null;

        }

        #endregion

        public List<object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;

            List<object> ArInvoiceLiteLite;

            if (myCriteria != null)
            {
                CustomerCriteria criteria = (CustomerCriteria)myCriteria;

                Instance.AddInParameter("@sort_type", SqlDbType.NVarChar, criteria.SortType);
                if (!string.IsNullOrEmpty(criteria.SearchCriteria))
                    Instance.AddInParameter("@search_criteria", SqlDbType.NVarChar, criteria.SearchCriteria);
                if (!criteria.pageNumber.Equals(-1))
                    Instance.AddInParameter("@page_number", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(criteria.pageNumber));
                if (!criteria.resultCount.Equals(-1))
                    Instance.AddInParameter("@result_count", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(criteria.resultCount));

                Instance.AddInParameter("@sort_fields", SqlDbType.NVarChar, criteria.SortFields);
            }

            reader = Instance.ExcuteReader("dbo.SP_Customer_AdvancedSearch_Lite", conn);

            if (reader != null && reader.HasRows)
            {
                ArInvoiceLiteLite = new List<object>();
                while (reader.Read())
                    ArInvoiceLiteLite.Add(GetFindAllLiteMapper(reader));


                reader.Close();
                return ArInvoiceLiteLite;
            }
            else
                return null;
        }

        public double FindPendingAmount(int Id, CriteriaBase myCriteria)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            
            Instance.AddInParameter("@customer_id", SqlDbType.Int, Id);

            if (myCriteria != null)
            {
                CustomerCriteria criteria = (CustomerCriteria)myCriteria;
                if (criteria.ExcludedInvoice > 0)
                    Instance.AddInParameter("@excluded_invoice", SqlDbType.Int, criteria.ExcludedInvoice);
            }
            
            SqlDataReader reader;

            reader = Instance.ExcuteReader("dbo.SP_Customer_Find_PendingAmount", conn);

            reader.Read();
            double amount = (double)reader["pending_amount"];
            reader.Close();
            return amount;
        }

        public List<POSIntegrationCustomer> POSIntegrationCustomerFindAll(DateTime? LastSyncDate)
        {
            List<POSIntegrationCustomer> CustomerList = new List<POSIntegrationCustomer>();
            SQLDAHelper instance = new SQLDAHelper();
            SqlConnection conn = instance.GetConnection(false);
            instance.AddInParameter("@last_sync_date", SqlDbType.DateTime, LastSyncDate);
            SqlDataReader reader = instance.ExcuteReader("SP_POS_Integration_Customer_FindAll", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    CustomerList.Add(GetPOSIntegrationCustomerMapper(reader));
                }
                return CustomerList;
            }
            else
            {
                return null;
            }

        }

        public string CustomConnectionString
        {
            get;
            set;
        }

        public bool UseCustomConnectionString
        {
            get;
            set;
        }
        public Object BeginTransaction()
        {
            return new object();
        }


        public void CommitTransaction(Object object1)
        {
        }

        public void Rollback(Object object1)
        {

        }

        public void KillConnection()
        {

        }


        public bool UseSharedSession { get; set; }
    }
}