
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using SagePOS.Manger.Common.Business.Entity;
using SagePOS.Common.DataAccess.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class NoteEntityRepository : RepositoryBaseClass<NoteEntity, NoteEntityLite, int>, INoteEntityRepository
    {
        #region IRepository< NoteEntity, int > Members

        public NoteEntityRepository()
        {
            this.AddSPName = "SP_NoteEntity_Add";
            this.UpdateSPName = "SP_NoteEntity_Update";
            this.DeleteSPName = "SP_NoteEntity_Delete";
            this.DeleteLogicalSPName = "SP_NoteEntity_DeleteLogical";
            this.FindAllSPName = "SP_NoteEntity_FindAll";
            this.FindByIdSPName = "SP_NoteEntity_FindById";
            this.FindByParentSPName = "SP_NoteEntity_FindByParentId";
            this.FindAllLiteSPName = "SP_NoteEntity_FindAll_Lite";
            this.FindByIdLiteSPName = "SP_NoteEntity_FindById_Lite";
            this.AdvancedSearchSPName = "SP_NoteEntity_AdvancedSearch_Lite";
        }

        public override List<NoteEntity> FindByParentId<TParentId>(TParentId ParentId, Type ParentType, CriteriaBase myCriteria)
        {
            List<NoteEntity> NoteList = new List<NoteEntity>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;
            NoteEntityCriteria Criteria = new NoteEntityCriteria();
            Criteria.EntityValueId = Convert.ToInt32(ParentId);
            Criteria.EntityId = SagePOS.Server.Configuration.Configuration.GetEntityId(ParentType);

            if (Criteria != null)
            {

                Instance.AddInParameter("@entity_id", SqlDbType.Int, Criteria.EntityId);
                Instance.AddInParameter("@entity_value_id", SqlDbType.Int, Criteria.EntityValueId);//ParentId
            }
            reader = Instance.ExcuteReader("dbo.SP_NoteEntity_FindByParentId", conn);

            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                    NoteList.Add(GetNoteEntityData(reader));
                reader.Close();
                return NoteList;
            }
            else
                return null;
        }


		#endregion
    }
}