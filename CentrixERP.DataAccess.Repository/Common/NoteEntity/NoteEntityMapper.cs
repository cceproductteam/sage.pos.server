using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SagePOS.Manger.Common.Business.Entity;
using SF.Framework.Data;
using System.Data.SqlClient;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class NoteEntityRepository
    {
        public NoteEntity GetNoteEntityData(SqlDataReader reader)
        {
            NoteEntity obj = new NoteEntity();

            if (reader["note_entity_id"] != DBNull.Value)
            {
                obj.NoteEntityId = (int)reader["note_entity_id"];
            }
            if (reader["note_id"] != DBNull.Value)
            {
                obj.NoteId = (int)reader["note_id"];
            }
            if (reader["entity_id"] != DBNull.Value)
            {
                obj.EntityId = (int)reader["entity_id"];
            }
            if (reader["entity_value_id"] != DBNull.Value)
            {
                obj.EntityValueId = (int)reader["entity_value_id"];
            }
            return obj;
        }

    }
}
