using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SagePOS.Manger.Common.Business.Entity;
using SF.Framework.Data;
using System.Data.SqlClient;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class DocumentFolderRepository
    {
        public DocumentFolder GetMapper(SqlDataReader reader)
        {
            DocumentFolder obj = new DocumentFolder();
			if (reader["folder_id"] != DBNull.Value)
			{
				obj.FolderId = (int)reader["folder_id"];
			}
			if (reader["folder_name"] != DBNull.Value)
			{
				obj.FolderName = (string)reader["folder_name"];
			}
			if (reader["parent_folder"] != DBNull.Value)
			{
				obj.ParentFolder = (int)reader["parent_folder"];
			}
			if (reader["entity_id"] != DBNull.Value)
			{
				obj.EntityId = (int)reader["entity_id"];
			}
			if (reader["updated_by"] != DBNull.Value)
			{
				obj.UpdatedBy = (int)reader["updated_by"];
			}
			if (reader["updated_date"] != DBNull.Value)
			{
				obj.UpdatedDate = (DateTime)reader["updated_date"];
			}
			if (reader["created_by"] != DBNull.Value)
			{
				obj.CreatedBy = (int)reader["created_by"];
			}
			if (reader["created_date"] != DBNull.Value)
			{
				obj.CreatedDate = (DateTime)reader["created_date"];
			}

            return obj;
        }
    }
}
