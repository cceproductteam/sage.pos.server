
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using SagePOS.Manger.Common.Business.Entity;
using SagePOS.Common.DataAccess.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class EntityControlConfigEventsRepository : IEntityControlConfigEventsRepository
    {
        #region IRepository< EntityControlConfigEvents, int > Members

        public void Add(ref EntityControlConfigEvents myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);

			Instance.AddInParameter("@entity_Id", SqlDbType.Int, myEntity.EntityId);
			Instance.AddInParameter("@entity_control_config_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.EntityControlConfigId));
			Instance.AddInParameter("@control_event_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.ControlEventId));
			Instance.AddInParameter("@js_function_name", SqlDbType.NVarChar, myEntity.JsFunctionName);
			Instance.AddInParameter("@js_function_body", SqlDbType.NVarChar, myEntity.JsFunctionBody);
            Instance.AddInParameter("@created_by", SqlDbType.Int, myEntity.CreatedBy);
            Instance.AddOutParameter("@entity_control_config_event_id", SqlDbType.Int);

            Instance.ExcuteNonQuery("dbo.SP_EntityControlConfigEvents_Add", conn, false);

            myEntity.EntityControlConfigEventId = (int)Instance.GetOutParamValue("@entity_control_config_event_id");
        }

        public void Update(ref EntityControlConfigEvents myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@entity_control_config_event_id", SqlDbType.Int, myEntity.EntityControlConfigEventId);
			Instance.AddInParameter("@entity_Id", SqlDbType.Int, myEntity.EntityId);
			Instance.AddInParameter("@entity_control_config_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.EntityControlConfigId));
			Instance.AddInParameter("@control_event_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.ControlEventId));
			Instance.AddInParameter("@js_function_name", SqlDbType.NVarChar, myEntity.JsFunctionName);
			Instance.AddInParameter("@js_function_body", SqlDbType.NVarChar, myEntity.JsFunctionBody);
            Instance.AddInParameter("@updated_by", SqlDbType.Int,myEntity.UpdatedBy);
            
            Instance.ExcuteNonQuery("dbo.SP_EntityControlConfigEvents_Update", conn, false);
        }

        public EntityControlConfigEvents FindById(int Id, CriteriaBase myCriteria)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@entity_control_config_event_id", SqlDbType.Int, Id);
            SqlDataReader reader;

            reader = Instance.ExcuteReader("dbo.SP_EntityControlConfigEvents_FindById", conn);

            if (reader != null && reader.HasRows)
            {
                reader.Read();
                EntityControlConfigEvents myEntityControlConfigEvents = GetMapper(reader);
                reader.Close();
                return myEntityControlConfigEvents;
            }
            else
                return null;
        }
        public List<EntityControlConfigEvents> FindAll(CriteriaBase myCriteria)
        {
            List<EntityControlConfigEvents> EntityControlConfigEventsList = new List<EntityControlConfigEvents>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;
            if (myCriteria != null)
            {
                EntityControlConfigEventsCriteria criteria = (EntityControlConfigEventsCriteria)myCriteria;
            }
            reader = Instance.ExcuteReader("dbo.SP_EntityControlConfigEvents_FindAll", conn);

            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                    EntityControlConfigEventsList.Add(GetMapper(reader));
                reader.Close();
                return EntityControlConfigEventsList;
            }
            else
                return null;

        }

        public List<EntityControlConfigEvents> FindByParentId<TParentId>(TParentId ParentId, Type ParentType, CriteriaBase myCriteria)
        {
            List<EntityControlConfigEvents> EntityControlConfigEventsList = new List<EntityControlConfigEvents>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);

			if (ParentType == typeof(EntityControlConfig))
				Instance.AddInParameter("@entity_control_config_id", SqlDbType.Int, ParentId);
			//else if (ParentType == typeof(ControlsEvents))
			//	Instance.AddInParameter("@control_event_id", SqlDbType.Int, ParentId);
	

            if (myCriteria != null)
            {
                EntityControlConfigEventsCriteria criteria = (EntityControlConfigEventsCriteria)myCriteria;
            }			

            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_EntityControlConfigEvents_FindByParentId", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    EntityControlConfigEventsList.Add(GetMapper(reader));
                }
                reader.Close();
                return EntityControlConfigEventsList;
            }
            else
            {
                return null;
            }
        }

        public void Delete(ref EntityControlConfigEvents myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@entity_control_config_event_id", SqlDbType.Int, myEntity.EntityControlConfigEventId);

            Instance.ExcuteNonQuery("dbo.SP_EntityControlConfigEvents_Delete", conn, false);
        }

        public void DeleteLogical(ref EntityControlConfigEvents myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@entity_control_config_event_id", SqlDbType.Int, myEntity.EntityControlConfigEventId);

            Instance.ExcuteNonQuery("dbo.SP_EntityControlConfigEvents_DeleteLogical", conn, false);
        }

        public List<EntityControlConfigEvents_Lite> FindAllLite(CriteriaBase myCriteria)
        {
            List<EntityControlConfigEvents_Lite> EntityControlConfigEventsList = new List<EntityControlConfigEvents_Lite>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;
            if (myCriteria != null)
            {
                EntityControlConfigEventsCriteria criteria = (EntityControlConfigEventsCriteria)myCriteria;
                if (criteria.EntityId > -1)
                    Instance.AddInParameter("@entity_id", SqlDbType.Int, criteria.EntityId);

                if (criteria.ConfigId > -1)
                    Instance.AddInParameter("@config_id", SqlDbType.Int, criteria.ConfigId);

            }
            reader = Instance.ExcuteReader("dbo.SP_EntityControlConfigEvents_FindAll_Lite", conn);

            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                    EntityControlConfigEventsList.Add(GetLiteMapper(reader));
                reader.Close();
                return EntityControlConfigEventsList;
            }
            else
                return null;

        }

        public string CustomConnectionString
        {
            get;
            set;
        }

        public bool UseCustomConnectionString
        {
            get;
            set;
        }
        public Object BeginTransaction()
        {
            return new object();
        }


        public void CommitTransaction(Object object1)
        {
        }

        public void Rollback(Object object1)
        {

        }

        public void KillConnection()
        {

        }


        public bool UseSharedSession { get; set; }
		#endregion
    }
}