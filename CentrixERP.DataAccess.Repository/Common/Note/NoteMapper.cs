using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SagePOS.Manger.Common.Business.Entity;
using System.Data.SqlClient;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class NoteRepository
    {
        public Note GetNoteMapper(SqlDataReader reader)
        {
            Note note = new Note();
            if (reader["note_id"] != DBNull.Value)
            {
                note.NoteID = (int)reader["note_id"];
            }

            if (reader["note_content"] != DBNull.Value)
            {
                note.NoteContent = (string)reader["note_content"];
            }
            if (reader["created_by"] != DBNull.Value)
            {
                note.CreatedByName = (string)reader["created_by"];
            }
            if (reader["updated_by"] != DBNull.Value)
            {
                note.UpdatedByName = (string)reader["updated_by"];
            }

            if (reader["created_date"] != DBNull.Value)
            {
                note.Crearted_Date = (DateTime)reader["created_date"];
            }

            if (reader["updated_date"] != DBNull.Value)
            {
                note.Updated_Date = (DateTime)reader["updated_date"];
            }
            return note;
        }
    }
}
