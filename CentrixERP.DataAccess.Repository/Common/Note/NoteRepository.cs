using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SagePOS.Common.DataAccess.IRepository;
using SF.Framework;
using SF.Framework.Data;
using SF.FrameworkEntity;
using System.Data.SqlClient;
using SagePOS.Manger.Common.Business.Entity;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class NoteRepository : INoteRepository
    {
        #region IRepository<Note,int> Members

        public void Add(ref SagePOS.Manger.Common.Business.Entity.Note myEntity)
        {
             SQLDAHelper Instance = new SQLDAHelper(); 
 SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@created_by", System.Data.SqlDbType.Int, myEntity.Createdby);
            Instance.AddInParameter("@note_content", System.Data.SqlDbType.NVarChar, myEntity.NoteContent);
            Instance.AddOutParameter("@noteID", System.Data.SqlDbType.Int);
            try
            {
                Instance.ExcuteNonQuery("dbo.sp_Note_Add", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            var outPutParamValue = Instance.GetOutParamValue("@noteID");
            if (outPutParamValue != DBNull.Value)
                myEntity.NoteID = (int)outPutParamValue;
        }

        public void Delete(ref SagePOS.Manger.Common.Business.Entity.Note myEntity)
        {
             SQLDAHelper Instance = new SQLDAHelper(); 
 SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@noteID", System.Data.SqlDbType.Int, myEntity.NoteID);
            try
            {
                Instance.ExcuteNonQuery("dbo.sp_Note_Delete", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteLogical(ref SagePOS.Manger.Common.Business.Entity.Note myEntity)
        {
             SQLDAHelper Instance = new SQLDAHelper(); 
 SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@noteID", System.Data.SqlDbType.Int, myEntity.NoteID);
            try
            {
                Instance.ExcuteNonQuery("dbo.sp_Note_DeleteLogical", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SagePOS.Manger.Common.Business.Entity.Note> FindAll(CriteriaBase myCriteria)
        {
             SQLDAHelper Instance = new SQLDAHelper(); 
 SqlConnection conn = Instance.GetConnection(false);
            List<Note> noteList = new List<Note>();
            SqlDataReader reader;
            try
            {
                reader = Instance.ExcuteReader("dbo.sp_Note_FindAll", conn);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                    noteList.Add(GetNoteMapper(reader));
                reader.Close();
                return noteList;
            }
            else
                return null;
        }

        public SagePOS.Manger.Common.Business.Entity.Note FindById(int Id, CriteriaBase myCriteria)
        {
             SQLDAHelper Instance = new SQLDAHelper(); 
 SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@noteID", System.Data.SqlDbType.Int, Id);
            SqlDataReader reader;
            try
            {
                reader = Instance.ExcuteReader("sp_Note_FindByID", conn);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (reader != null && reader.HasRows)
            {
                reader.Read();
                Note note= GetNoteMapper(reader);
                reader.Close();
                return note;
            }
            else
                return null;
        }

        public List<SagePOS.Manger.Common.Business.Entity.Note> FindByParentId<TParentId>(TParentId ParentId, Type ParentType, CriteriaBase myCriteria)
        {
            throw new NotImplementedException();
        }

        public void Update(ref SagePOS.Manger.Common.Business.Entity.Note myEntity)
        {
             SQLDAHelper Instance = new SQLDAHelper(); 
 SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@updated_by", System.Data.SqlDbType.Int, myEntity.UpdatedBy);
            Instance.AddInParameter("@note_content", System.Data.SqlDbType.NVarChar, myEntity.NoteContent);
            Instance.AddInParameter("@note_id", System.Data.SqlDbType.Int, myEntity.NoteID);
            try
            {
                Instance.ExcuteNonQuery("dbo.sp_Note_Update", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public string CustomConnectionString
        {
            get;
            set;
        }

        public bool UseCustomConnectionString
        {
            get;
            set;
        }
        public Object BeginTransaction()
        {
            return new object();
        }


        public void CommitTransaction(Object object1)
        {
        }

        public void Rollback(Object object1)
        {

        }

        public void KillConnection()
        {

        }


        public bool UseSharedSession { get; set; }
        #endregion
    }
}
