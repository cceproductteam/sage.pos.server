using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SagePOS.Manger.Common.Business.Entity;
using SF.Framework.Data;
using System.Data.SqlClient;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class EmailEntityRepository
    {

        public EmailEntity GetEmailEntityData(SqlDataReader reader)
        {
            EmailEntity obj = new EmailEntity();

            if (reader["email_entity_id"] != DBNull.Value)
            {
                obj.EmailEntityId = (int)reader["email_entity_id"];
            }
            if (reader["email_id"] != DBNull.Value)
            {
                obj.EmailId = (int)reader["email_id"];
            }
            if (reader["entity_id"] != DBNull.Value)
            {
                obj.EntityId = (int)reader["entity_id"];
            }
            if (reader["entity_value_id"] != DBNull.Value)
            {
                obj.EntityValueId = (int)reader["entity_value_id"];
            }

         
            return obj;
        }

    }
}
