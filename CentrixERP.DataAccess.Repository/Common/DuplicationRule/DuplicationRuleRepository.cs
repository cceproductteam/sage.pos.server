
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using SagePOS.Manger.Common.Business.Entity;
using SagePOS.Common.DataAccess.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace CentrixERP.Common.DataAccess.Repository
{    
    public partial class DuplicationRuleRepository :IDuplicationRuleRepository
    {
       #region IRepository< DuplicationRule, int > Members
    
        public void Add(ref DuplicationRule myEntity)
            {
                SQLDAHelper Instance = new SQLDAHelper();
                SqlConnection conn = Instance.GetConnection(false);
				Instance.AddInParameter("@module_id", SqlDbType.Int, myEntity.ModuleId);
				Instance.AddInParameter("@ui_name_en", SqlDbType.NVarChar, myEntity.UiNameEn);
				Instance.AddInParameter("@ui_name_ar", SqlDbType.NVarChar, myEntity.UiNameAr);
				Instance.AddInParameter("@filter_type_id", SqlDbType.Int, myEntity.FilterTypeId);
				Instance.AddInParameter("@property_name_en", SqlDbType.NVarChar, myEntity.PropertyNameEn);
				Instance.AddInParameter("@property_name_ar", SqlDbType.NVarChar, myEntity.PropertyNameAr);
				Instance.AddInParameter("@property_parent_id", SqlDbType.Int, myEntity.PropertyParentId);
				Instance.AddInParameter("@created_by", SqlDbType.Int, myEntity.CreatedBy);
                Instance.AddOutParameter("@id",SqlDbType.Int);
                
                Instance.ExcuteNonQuery("dbo.SP_DuplicationRule_Add", conn, false);
            
                myEntity.Id = (int)Instance.GetOutParamValue("@id");
            }
        
        public void Update(ref DuplicationRule myEntity)
            {
                SQLDAHelper Instance = new SQLDAHelper();
                SqlConnection conn = Instance.GetConnection(false);
				Instance.AddInParameter("@id", SqlDbType.Int, myEntity.Id);
				Instance.AddInParameter("@module_id", SqlDbType.Int, myEntity.ModuleId);
				Instance.AddInParameter("@ui_name_en", SqlDbType.NVarChar, myEntity.UiNameEn);
				Instance.AddInParameter("@ui_name_ar", SqlDbType.NVarChar, myEntity.UiNameAr);
				Instance.AddInParameter("@filter_type_id", SqlDbType.Int, myEntity.FilterTypeId);
				Instance.AddInParameter("@property_name_en", SqlDbType.NVarChar, myEntity.PropertyNameEn);
				Instance.AddInParameter("@property_name_ar", SqlDbType.NVarChar, myEntity.PropertyNameAr);
				Instance.AddInParameter("@property_parent_id", SqlDbType.Int, myEntity.PropertyParentId);
				Instance.AddInParameter("@updated_by", SqlDbType.Int, myEntity.UpdatedBy);
                
                Instance.ExcuteNonQuery("dbo.SP_DuplicationRule_Update", conn, false);         
                
            }
        
        public DuplicationRule FindById(int Id, CriteriaBase myCriteria)
            {
                SQLDAHelper Instance = new SQLDAHelper();
                SqlConnection conn = Instance.GetConnection(false);
                Instance.AddInParameter("@id", SqlDbType.Int, Id);
                SqlDataReader reader;
                reader = Instance.ExcuteReader("dbo.sp_DuplicationRule_FindByID", conn);
            
                if (reader != null && reader.HasRows)
                {
                    reader.Read();
                    DuplicationRule myDuplicationrule = GetMapper(reader);
                    reader.Close();
                    return myDuplicationrule;
                }
                else
                    return null;
            }
        
        public List< DuplicationRule > FindAll(CriteriaBase myCriteria)
            {
                List< DuplicationRule > DuplicationRuleList = new List< DuplicationRule>();
                SQLDAHelper Instance = new SQLDAHelper();
                SqlConnection conn = Instance.GetConnection(false);
                SqlDataReader reader;
                DuplicationRuleCriteria Criteria = (DuplicationRuleCriteria)myCriteria;
                if (myCriteria != null)
                    Instance.AddInParameter("@module_id", SqlDbType.Int, Criteria.ModuleID);

                reader = Instance.ExcuteReader("dbo.sp_DuplicationRule_FindAll", conn);
            
                if (reader != null && reader.HasRows)
                {
                    while (reader.Read())
                        DuplicationRuleList.Add(GetMapper(reader));
                    reader.Close();
                    return DuplicationRuleList;
                }
                else
                    return null;
    
            }
        
        public List< DuplicationRule> FindByParentId<TParentId>(TParentId ParentId, Type ParentType, CriteriaBase myCriteria)
            {
                throw new NotImplementedException();
            }
        
        public void Delete(ref DuplicationRule myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@id", SqlDbType.Int, myEntity.Id);

            
            Instance.ExcuteNonQuery("dbo.SP_DuplicationRule_Delete", conn, false);
            
        }
        
        public void DeleteLogical(ref DuplicationRule myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@id", SqlDbType.Int, myEntity.Id);

            
            Instance.ExcuteNonQuery("dbo.SP_DuplicationRule_DeleteLogical", conn, false);
            
        }

        public string CustomConnectionString
        {
            get;
            set;
        }

        public bool UseCustomConnectionString
        {
            get;
            set;
        }
        public Object BeginTransaction()
        {
            return new object();
        }


        public void CommitTransaction(Object object1)
        {
        }

        public void Rollback(Object object1)
        {

        }

        public void KillConnection()
        {

        }


        public bool UseSharedSession { get; set; }

     
       #endregion
    }
}


