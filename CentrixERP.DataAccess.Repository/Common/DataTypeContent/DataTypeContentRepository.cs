using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SagePOS.Common.DataAccess.IRepository;
using System.Data.SqlClient;
using SF.Framework.Data;
using SagePOS.Manger.Common.Business.Entity;
using System.Data;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class DataTypeContentRepository : IDataTypeContentRepository
    {
        #region IRepository<DataTypeContent,int> Members

        public void Add(ref SagePOS.Manger.Common.Business.Entity.DataTypeContent myEntity)
        {
             SQLDAHelper Instance = new SQLDAHelper(); 
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@DataTypeContent", System.Data.SqlDbType.NVarChar, myEntity.DataTypeContentEN);
            Instance.AddInParameter("@ArabicDataTypeContent", System.Data.SqlDbType.NVarChar, myEntity.DataTypeContentAR);
            Instance.AddInParameter("@DataTypeID", System.Data.SqlDbType.Int, myEntity.DataTypeID);
            Instance.AddInParameter("@DataTypeContentOrder", System.Data.SqlDbType.Int, myEntity.DataTypeOrder);
            Instance.AddInParameter("@CreatedBy", System.Data.SqlDbType.Int, myEntity.CreatedBy);
            Instance.AddOutParameter("@DataTypeContentID", System.Data.SqlDbType.Int);
            try
            {
                Instance.ExcuteNonQuery("dbo.sp_DataTypeContent_Add", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            var outPutParam = Instance.GetOutParamValue("@DataTypeContentID");
            if (outPutParam != DBNull.Value)
                myEntity.DataTypeContentID = (int)outPutParam;
        }

        public void Delete(ref SagePOS.Manger.Common.Business.Entity.DataTypeContent myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@DataTypeContentID", System.Data.SqlDbType.Int, myEntity.DataTypeContentID);
            try
            {
                Instance.ExcuteNonQuery("dbo.sp_DataTypeContent_Delete", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
         
        }

        public void DeleteLogical(ref SagePOS.Manger.Common.Business.Entity.DataTypeContent myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@DataTypeContentID", System.Data.SqlDbType.Int, myEntity.DataTypeContentID);
            try
            {
                Instance.ExcuteNonQuery("dbo.sp_DataTypeContent_DeleteLogical", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SagePOS.Manger.Common.Business.Entity.DataTypeContent> FindAll(CriteriaBase myCriteria)
        {
            List<DataTypeContent> entitiesList = new List<DataTypeContent>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);

            SqlDataReader reader = Instance.ExcuteReader("dbo.sp_DataTypeContent_FindAll", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    entitiesList.Add(GetDataTypeContentMapper(reader));
                }
                reader.Close();
                return entitiesList;
            }
            else
            {
                return null;
            }


           
        }

        public SagePOS.Manger.Common.Business.Entity.DataTypeContent FindById(int Id, CriteriaBase myCriteria)
        {
             SQLDAHelper Instance = new SQLDAHelper(); 
              SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@DataTypeContentID", System.Data.SqlDbType.Int, Id);
            SqlDataReader reader;
            try
            {
                reader = Instance.ExcuteReader("dbo.sp_DataTypeContent_FindByID", conn);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            if (reader != null && reader.HasRows)
            {
                reader.Read();
                DataTypeContent dtc= GetDataTypeContentMapper(reader);
                reader.Close();
                return dtc;
            }
            else
                return null;
        }

        public List<SagePOS.Manger.Common.Business.Entity.DataTypeContent> FindByParentId<TParentId>(TParentId ParentId, Type ParentType, CriteriaBase myCriteria)
        {
            List<DataTypeContent> dataTypeContentList = new List<DataTypeContent>();
             SQLDAHelper Instance = new SQLDAHelper(); 
             SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@DataTypeID", System.Data.SqlDbType.Int, ParentId);

            if (myCriteria != null)
            {
                DataTypeContentCriteria criteria = (DataTypeContentCriteria)myCriteria;
                if (criteria.UseSystemDataType.HasValue)
                    Instance.AddInParameter("@use_system_data_type", System.Data.SqlDbType.Bit, criteria.UseSystemDataType);
            }
            SqlDataReader reader;
            try
            {
                reader = Instance.ExcuteReader("dbo.sp_DataTypeContent_FindByParentID", conn);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                    dataTypeContentList.Add(GetDataTypeContentMapper(reader));
                reader.Close();
                return dataTypeContentList;
            }
            else
                return null;
        }

        public void Update(ref SagePOS.Manger.Common.Business.Entity.DataTypeContent myEntity)
        {
             SQLDAHelper Instance = new SQLDAHelper(); 
             SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@DataTypeContent", System.Data.SqlDbType.NVarChar, myEntity.DataTypeContentEN);
            Instance.AddInParameter("@ArabicDataTypeContent", System.Data.SqlDbType.NVarChar, myEntity.DataTypeContentAR);
            Instance.AddInParameter("@DataTypeContentOrder", System.Data.SqlDbType.Int, myEntity.DataTypeOrder);
            Instance.AddInParameter("@UpdatedBy", System.Data.SqlDbType.Int, myEntity.UpdatedBy);
            Instance.AddInParameter("@DataTypeContentID", System.Data.SqlDbType.Int, myEntity.DataTypeContentID);
            try
            {
                Instance.ExcuteNonQuery("dbo.sp_DataTypeContent_Update", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ReorderDataTypeContent(string dataTypeContentIDs)
        {
             SQLDAHelper Instance = new SQLDAHelper(); 
             SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@DataTypeContentIDs", System.Data.SqlDbType.NVarChar, dataTypeContentIDs);
            try
            {
                Instance.ExcuteNonQuery("dbo.sp_DataTypeContent_Order", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string CustomConnectionString
        {
            get;
            set;
        }

        public bool UseCustomConnectionString
        {
            get;
            set;
        }
        public Object BeginTransaction()
        {
            return new object();
        }


        public void CommitTransaction(Object object1)
        {
        }

        public void Rollback(Object object1)
        {

        }

        public void KillConnection()
        {

        }


        public bool UseSharedSession { get; set; }

        #endregion
        public List<SagePOS.Manger.Common.Business.Entity.DataTypeContentLite> FindAllByIds(string Ids)
        {
            List<DataTypeContentLite> entitiesList = new List<DataTypeContentLite>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);

            if (!string.IsNullOrEmpty(Ids))
                Instance.AddInParameter("@type_ids", SqlDbType.NVarChar, Ids);

            SqlDataReader reader = Instance.ExcuteReader("dbo.sp_get_DataTypeContent_ByIds", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    entitiesList.Add(GetLiteMapper(reader));
                }
                reader.Close();
                return entitiesList;
            }
            else
            {
                return null;
            }
        }
    }
}
