using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.Framework;
using SF.FrameworkEntity;
using SagePOS.Manger.Common.Business.Entity;
using System.Data.SqlClient;
using SF.Framework.Data;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class DataTypeContentRepository
    {
        public DataTypeContent GetDataTypeContentMapper(SqlDataReader reader)
        {
            DataTypeContent dataTypeContent = new DataTypeContent();
            if (reader["data_type_content_ar"] != DBNull.Value)
            {
                dataTypeContent.DataTypeContentAR = (string)reader["data_type_content_ar"];
            }
            if (reader["data_type_id"] != DBNull.Value)
            {
                dataTypeContent.DataTypeID = (int)reader["data_type_id"];
            }
            if (reader["data_type_content"] != DBNull.Value)
            {
                dataTypeContent.DataTypeContentEN = (string)reader["data_type_content"];
            }
            if (reader["data_type_content_id"] != DBNull.Value)
            {
                dataTypeContent.DataTypeContentID = (int)reader["data_type_content_id"];
                dataTypeContent.DataTypeContentValue = SQLDAHelper.Convert_DBTOInt(reader["data_type_content_value"]);
            }

            if (reader["created_by"] != DBNull.Value)
            {
                dataTypeContent.CreatedBy = (int)reader["created_by"];
            }
            if (reader["full_name"] != DBNull.Value)
            {
                dataTypeContent.CreatedByName = (string)reader["full_name"];
            }

            if (reader["created_date"] != DBNull.Value)
            {
                dataTypeContent.CreatedDate = ((DateTime)reader["created_date"]).GetLocalTime();
            }

            if (reader["updated_by"] != DBNull.Value)
            {
                dataTypeContent.UpdatedBy = (int)reader["updated_by"];
            }


            if (reader["updated_date"] != DBNull.Value)
            {
                dataTypeContent.UpdateDate = ((DateTime)reader["updated_date"]).GetLocalTime();
            }


            if (reader["is_system_value"] != DBNull.Value)
            {
                dataTypeContent.IsSystem = (bool)reader["is_system_value"];
            }



            if (reader["set_as_default"] != DBNull.Value)
            {
                dataTypeContent.SetAsDefault = (bool)reader["set_as_default"];
            }


            return dataTypeContent;
        }
        public DataTypeContentLite GetLiteMapper(SqlDataReader reader)
        {
            DataTypeContentLite Obj = new DataTypeContentLite();

            if (reader["data_type_content_id"] != DBNull.Value)
            {
                Obj.DataTypeContentId = (int)reader["data_type_content_id"];

            }
            if (reader["data_type_content"] != DBNull.Value)
            {
                Obj.DataTypeContent = (string)reader["data_type_content"];
            }
            if (reader["data_type_content_ar"] != DBNull.Value)
            {
                Obj.DataTypeContentAr = (string)reader["data_type_content_ar"];
            }
            if (reader["data_type_id"] != DBNull.Value)
            {
                Obj.DataTypeId = (int)reader["data_type_id"];
            }
            if (reader["data_type_content_order"] != DBNull.Value)
            {
                Obj.DataTypeContentOrder = (int)reader["data_type_content_order"];
            }
            if (reader["flag_deleted"] != DBNull.Value)
            {
                Obj.FlagDeleted = (bool)reader["flag_deleted"];
            }
            if (reader["is_system"] != DBNull.Value)
            {
                Obj.IsSystem = (bool)reader["is_system"];
            }
            if (reader["created_by"] != DBNull.Value)
            {
                Obj.CreatedBy = (int)reader["created_by"];
            }
            if (reader["updated_by"] != DBNull.Value)
            {
                Obj.UpdatedBy = (int)reader["updated_by"];
            }
            if (reader["created_date"] != DBNull.Value)
            {
                Obj.CreatedDate = (System.DateTime)reader["created_date"];
            }
            if (reader["updated_date"] != DBNull.Value)
            {
                Obj.UpdatedDate = (System.DateTime)reader["updated_date"];
            }
            if (reader["data_type_content_value"] != DBNull.Value)
            {
                Obj.DataTypeContentValue = (int)reader["data_type_content_value"];
            }
            if (reader["set_as_default"] != DBNull.Value)
            {
                Obj.SetAsDefault = (bool)reader["set_as_default"];
            }




            return Obj;
        }
    }
}