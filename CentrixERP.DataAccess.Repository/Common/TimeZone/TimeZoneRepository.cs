
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using SagePOS.Manger.Common.Business.Entity;
using SagePOS.Common.DataAccess.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class TimeZoneRepository : ITimeZoneRepository
    {
        #region IRepository< TimeZone, int > Members

        public void Add(ref SagePOS.Manger.Common.Business.Entity.TimeZone myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);

			Instance.AddInParameter("@time_zone", SqlDbType.NVarChar, myEntity.TimeZoneDescription);
            Instance.AddInParameter("@created_by", SqlDbType.Int, myEntity.CreatedBy);
            Instance.AddOutParameter("@time_zone_id", SqlDbType.Int);

            Instance.ExcuteNonQuery("dbo.SP_TimeZone_Add", conn, false);

            myEntity.TimeZoneId = (int)Instance.GetOutParamValue("@time_zone_id");
        }

        public void Update(ref SagePOS.Manger.Common.Business.Entity.TimeZone myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@time_zone_id", SqlDbType.Int, myEntity.TimeZoneId);
			Instance.AddInParameter("@time_zone", SqlDbType.NVarChar, myEntity.TimeZoneDescription);
            Instance.AddInParameter("@updated_by", SqlDbType.Int,myEntity.UpdatedBy);
            
            Instance.ExcuteNonQuery("dbo.SP_TimeZone_Update", conn, false);
        }

        public SagePOS.Manger.Common.Business.Entity.TimeZone FindById(int Id, CriteriaBase myCriteria)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@time_zone_id", SqlDbType.Int, Id);
            SqlDataReader reader;

            reader = Instance.ExcuteReader("dbo.SP_TimeZone_FindById", conn);

            if (reader != null && reader.HasRows)
            {
                reader.Read();
                SagePOS.Manger.Common.Business.Entity.TimeZone myTimeZone = GetMapper(reader);
                reader.Close();
                return myTimeZone;
            }
            else
                return null;
        }
        public List<SagePOS.Manger.Common.Business.Entity.TimeZone> FindAll(CriteriaBase myCriteria)
        {
            List<SagePOS.Manger.Common.Business.Entity.TimeZone> TimeZoneList = new List<SagePOS.Manger.Common.Business.Entity.TimeZone>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;
            if (myCriteria != null)
            {
                TimeZoneCriteria criteria = (TimeZoneCriteria)myCriteria;
                Instance.AddInParameter("@keyword", SqlDbType.NVarChar, criteria.Keyword);
            }
            reader = Instance.ExcuteReader("dbo.SP_TimeZone_FindAll", conn);

            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                    TimeZoneList.Add(GetMapper(reader));
                reader.Close();
                return TimeZoneList;
            }
            else
                return null;

        }

        public List<SagePOS.Manger.Common.Business.Entity.TimeZone> FindByParentId<TParentId>(TParentId ParentId, Type ParentType, CriteriaBase myCriteria)
        {
            List<SagePOS.Manger.Common.Business.Entity.TimeZone> TimeZoneList = new List<SagePOS.Manger.Common.Business.Entity.TimeZone>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);

	

            if (myCriteria != null)
            {
                TimeZoneCriteria criteria = (TimeZoneCriteria)myCriteria;
            }			

            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_TimeZone_FindByParentId", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    TimeZoneList.Add(GetMapper(reader));
                }
                reader.Close();
                return TimeZoneList;
            }
            else
            {
                return null;
            }
        }

        public void Delete(ref SagePOS.Manger.Common.Business.Entity.TimeZone myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@time_zone_id", SqlDbType.Int, myEntity.TimeZoneId);

            Instance.ExcuteNonQuery("dbo.SP_TimeZone_Delete", conn, false);
        }

        public void DeleteLogical(ref SagePOS.Manger.Common.Business.Entity.TimeZone myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@time_zone_id", SqlDbType.Int, myEntity.TimeZoneId);

            Instance.ExcuteNonQuery("dbo.SP_TimeZone_DeleteLogical", conn, false);
        }

        public string CustomConnectionString
        {
            get;
            set;
        }

        public bool UseCustomConnectionString
        {
            get;
            set;
        }
        public Object BeginTransaction()
        {
            return new object();
        }


        public void CommitTransaction(Object object1)
        {
        }

        public void Rollback(Object object1)
        {

        }

        public void KillConnection()
        {

        }


        public bool UseSharedSession { get; set; }
		#endregion
    }
}