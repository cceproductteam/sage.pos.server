﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;
using SagePOS.Server.DataAcces.IRepository;

namespace SagePOS.Server.DataAcces.Repository
{
    public partial class IcPriceListDiscountRepository : RepositoryBaseClass<PriceListDiscount, PriceListDiscountLite, int>, IIcPriceListDiscountRepository
    {
        #region IRepository< PriceListDiscount, int > Members

        public IcPriceListDiscountRepository()
        {
            this.AddSPName = "SP_IcPriceListDiscount_Add";
            this.UpdateSPName = "SP_IcPriceListDiscount_Update";
            this.DeleteSPName = "SP_IcPriceListDiscount_Delete";
            this.DeleteLogicalSPName = "SP_IcPriceListDiscount_DeleteLogical";
            this.FindAllSPName = "SP_IcPriceListDiscount_FindAll";
            this.FindByIdSPName = "SP_IcPriceListDiscount_FindById";
            this.FindByParentSPName = "SP_IcPriceListDiscount_FindByParentId";
            this.FindAllLiteSPName = "SP_IcPriceListDiscount_FindAll_Lite";
            this.FindByIdLiteSPName = "SP_IcPriceListDiscount_FindById_Lite";
            this.AdvancedSearchSPName = "SP_IcPriceListDiscount_AdvancedSearch_Lite";
        }

        #endregion
    }
}
