
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using SagePOS.Server.Business.Entity;
using SagePOS.Server.DataAcces.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace SagePOS.Server.DataAcces.Repository
{
    public partial class IcPriceListRepository : RepositoryBaseClass<IcPriceList, IcPriceListLite, int>, IIcPriceListRepository
    {
        #region IRepository< IcPriceList, int > Members

        public IcPriceListRepository()
        {
            this.AddSPName = "SP_IcPriceList_Add";
            this.UpdateSPName = "SP_IcPriceList_Update";
            this.DeleteSPName = "SP_IcPriceList_Delete";
            this.DeleteLogicalSPName = "SP_IcPriceList_DeleteLogical";
            this.FindAllSPName = "SP_IcPriceList_FindAll";
            this.FindByIdSPName = "SP_IcPriceList_FindById";
            this.FindByParentSPName = "SP_IcPriceList_FindByParentId";
            this.FindAllLiteSPName = "SP_IcPriceList_FindAll_Lite";
            this.FindByIdLiteSPName = "SP_IcPriceList_FindById_Lite";
            this.AdvancedSearchSPName = "SP_IcPriceList_AdvancedSearch_Lite";
        }

		#endregion
    }
}