using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SagePOS.Manger.Common.Business.Entity;
using SF.Framework;
using SF.Framework.Data;
using SF.FrameworkEntity;
using SagePOS.Common.DataAccess.IRepository;
using System.Data.SqlClient;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class PhoneRepository : IPhoneRepository
    {

        #region IRepository<Phone,int> Members

        public void Add(ref Phone myEntity)
        {
             SQLDAHelper Instance = new SQLDAHelper(); 
 SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@number", System.Data.SqlDbType.NVarChar, myEntity.PhoneNumber);
            Instance.AddInParameter("@countryCode", System.Data.SqlDbType.NVarChar, myEntity.CountryCode);
            Instance.AddInParameter("@areaCode", System.Data.SqlDbType.NVarChar, myEntity.AreaCode);
            Instance.AddInParameter("@type", System.Data.SqlDbType.Int, myEntity.Type);
            Instance.AddInParameter("@createdBy", System.Data.SqlDbType.Int, myEntity.CreatedBy);
            Instance.AddOutParameter("@phoneID", System.Data.SqlDbType.Int);
            Instance.AddInParameter("@Description", System.Data.SqlDbType.NVarChar, myEntity.Description);
            try
            {
                Instance.ExcuteNonQuery("dbo.sp_Phone_Add", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            var outPutParamValue = Instance.GetOutParamValue("@phoneID");
            if (outPutParamValue != DBNull.Value)
                myEntity.PhoneID = (int)outPutParamValue;
        }

        public void Delete(ref Phone myEntity)
        {
             SQLDAHelper Instance = new SQLDAHelper(); 
 SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@phoneID", System.Data.SqlDbType.Int, myEntity.PhoneID);
            try
            {
                Instance.ExcuteNonQuery("dbo.sp_Phone_Delete", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteLogical(ref Phone myEntity)
        {
             SQLDAHelper Instance = new SQLDAHelper(); 
 SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@phoneID", System.Data.SqlDbType.Int, myEntity.PhoneID);
            try
            {
                Instance.ExcuteNonQuery("dbo.sp_Phone_DeleteLogical", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Phone> FindAll(SF.FrameworkEntity.CriteriaBase myCriteria)
        {
             SQLDAHelper Instance = new SQLDAHelper(); 
 SqlConnection conn = Instance.GetConnection(false);
            List<Phone> phoneList = new List<Phone>();
            SqlDataReader reader;
            try
            {
                reader = Instance.ExcuteReader("dbo.sp_Phone_FindAll", conn);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                    phoneList.Add(GetPhoneMapper(reader));
                reader.Close();
                return phoneList;
            }
            else
                return null;
        }

        public Phone FindById(int Id, SF.FrameworkEntity.CriteriaBase myCriteria)
        {
             SQLDAHelper Instance = new SQLDAHelper(); 
 SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@phoneID", System.Data.SqlDbType.Int, Id);
            SqlDataReader reader;
            try
            {
                reader = Instance.ExcuteReader("dbo.sp_Phone_FindByID", conn);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (reader != null && reader.HasRows)
            {
                reader.Read();
                Phone phone= GetPhoneMapper(reader);
                reader.Close();
                return phone;
            }
            else
                return null;

        }

        public List<Phone> FindByParentId<TParentId>(TParentId ParentId, Type ParentType, SF.FrameworkEntity.CriteriaBase myCriteria)
        {
            throw new NotImplementedException();
        }

        public void Update(ref Phone myEntity)
        {
             SQLDAHelper Instance = new SQLDAHelper(); 
 SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@number", System.Data.SqlDbType.NVarChar, myEntity.PhoneNumber);
            Instance.AddInParameter("@countryCode", System.Data.SqlDbType.NVarChar, myEntity.CountryCode);
            Instance.AddInParameter("@areaCode", System.Data.SqlDbType.NVarChar, myEntity.AreaCode);
            Instance.AddInParameter("@type", System.Data.SqlDbType.Int, myEntity.Type);
            Instance.AddInParameter("@updatedBy", System.Data.SqlDbType.Int, myEntity.UpdatedBy);
            Instance.AddInParameter("@phoneID", System.Data.SqlDbType.Int, myEntity.PhoneID);
            Instance.AddInParameter("@Description", System.Data.SqlDbType.NVarChar, myEntity.Description);
            try
            {
                Instance.ExcuteNonQuery("dbo.sp_Phone_Update", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public List<Phone> FindMobileList(string ids, bool forPerson)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            List<Phone> phoneList = new List<Phone>();
            SqlDataReader reader;

            Instance.AddInParameter("@ids", System.Data.SqlDbType.NVarChar, ids);
            Instance.AddInParameter("@for_person", System.Data.SqlDbType.Bit, forPerson);

            reader = Instance.ExcuteReader("dbo.SP_QuickMobile_FindAll", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                    phoneList.Add(GetFindMobileMapper(reader));
                reader.Close();
                return phoneList;
            }
            else
                return null;
        }

        public string CustomConnectionString
        {
            get;
            set;
        }

        public bool UseCustomConnectionString
        {
            get;
            set;
        }
        public Object BeginTransaction()
        {
            return new object();
        }


        public void CommitTransaction(Object object1)
        {
        }

        public void Rollback(Object object1)
        {

        }

        public void KillConnection()
        {

        }


        public bool UseSharedSession { get; set; }

        #endregion


    }
}
