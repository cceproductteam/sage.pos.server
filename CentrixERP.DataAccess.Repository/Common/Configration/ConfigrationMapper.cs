using System;
using System.Collections.Generic;
using System.Linq;
using SF.FrameworkEntity;
using SagePOS.Manger.Common.Business.Entity;
using System.Data.SqlClient;
using SagePOS.Server.Configuration;
using System.Text;

namespace CentrixERP.Common.DataAccess.Repository
{
   public partial class ConfigrationRepository
    {
       public Configration GetMapper(SqlDataReader reader)
       {
           Configration myConfigration = new Configration();
           if (reader["sys_config_id"] != DBNull.Value)
           {
               myConfigration.ConfigId = (int)reader["sys_config_id"];
           }
           if (reader["defaultLanguage"] != DBNull.Value)
           {
               myConfigration.DefaultLanguage = (Enums_S3.Configuration.Language)reader["defaultLanguage"];
           }
           if (reader["lockOnUncompletedTasks"] != DBNull.Value)
           {
               myConfigration.LockUncompleteTasks = Convert.ToBoolean( reader["lockOnUncompletedTasks"]);
           }
           if (reader["pageSize"] != DBNull.Value)
           {
               myConfigration.PageSize = (int)reader["pageSize"];
           }
           if (reader["tags_maxFontSize"] != DBNull.Value)
           {
               myConfigration.MaxFontSize = (int)reader["tags_maxFontSize"];
           }
           if (reader["tags_maxTagstoAppear"] != DBNull.Value)
           {
               myConfigration.MaxTagsToAppear = (int)reader["tags_maxTagstoAppear"];
           }
           if (reader["tags_minFontSize"] != DBNull.Value)
           {
               myConfigration.MinFontSize = (int)reader["tags_minFontSize"];
           }
           if (reader["firstDay"] != DBNull.Value)
           {
               myConfigration.FirstDay = (int)reader["firstDay"];
           }
           if (reader["firstHour"] != DBNull.Value)
           {
               myConfigration.FirstHour = (int)reader["firstHour"];
           }
           if (reader["minTime"] != DBNull.Value)
           {
               myConfigration.MinTime = (int)reader["minTime"];
           }
           if (reader["maxTime"] != DBNull.Value)
           {
               myConfigration.MaxTime = (int)reader["maxTime"];
           }
           if (reader["defaultEventMinutes"] != DBNull.Value)
           {
               myConfigration.DefaultEventMinutes = (int)reader["defaultEventMinutes"];
           }
           if (reader["weekends"] != DBNull.Value)
           {
               myConfigration.Weekends = Convert.ToBoolean ( reader["weekends"]);
           }
           if (reader["companyText"] != DBNull.Value)
           {
               myConfigration.CompanyTextEnglish = reader["companyText"].ToString();
           }

           if (reader["companyTextAr"] != DBNull.Value)
           {
               myConfigration.CompanyTextArabic = reader["companyTextAr"].ToString();
           }
           if (reader["PersonText"] != DBNull.Value)
           {
               myConfigration.PersonTextEnglish = reader["PersonText"].ToString();
           }
           if (reader["PersonTextAr"] != DBNull.Value)
           {
               myConfigration.PersonTextArabic = reader["PersonTextAr"].ToString();
           }
           if (reader["NumberofUsers"] != DBNull.Value)
           {
               myConfigration.NumberOfUsers = (int)reader["NumberofUsers"];
           }
           if (reader["tasksNumberToShow"] != DBNull.Value)
           {
               myConfigration.TasksNumberToShow = (int)reader["tasksNumberToShow"];
           }
           if (reader["pluralCompanyText"] != DBNull.Value)
           {
               myConfigration.PluralCompanyTextEnglish = reader["pluralCompanyText"].ToString();
           }
           if (reader["pluralCompanyTextAr"] != DBNull.Value)
           {
               myConfigration.PluralCompanyTextArabic = reader["pluralCompanyTextAr"].ToString();
           }
           if (reader["pluralPersonText"] != DBNull.Value)
           {
               myConfigration.PluralPersonTextEnglish = reader["pluralPersonText"].ToString();
           }
           if (reader["pluralPersonTextAr"] != DBNull.Value)
           {
               myConfigration.PluralPersonTextArabic = reader["pluralPersonTextAr"].ToString();
           }
           if (reader["personNameFormat"] != DBNull.Value)
           {
               myConfigration.PersonNameFormat = reader["personNameFormat"].ToString();
           }
           if (reader["AddressFormat"] != DBNull.Value)
           {
               myConfigration.AddressFormat = reader["AddressFormat"].ToString();
           }
           if (reader["notifications_count_send_form_windows_service"] != DBNull.Value)
           {
               myConfigration.NotificationCount = (int)reader["notifications_count_send_form_windows_service"];
           }

           if (reader["notifications_top_values"] != DBNull.Value)
           {
               myConfigration.NotificationsTopValues = (int)reader["notifications_top_values"];
           }
           if (reader["force_receive_notifications"] != DBNull.Value)
           {
               myConfigration.ForceRecivingNotifications = (bool)reader["force_receive_notifications"];
           }

           if (reader["default_email_configration_id"] != DBNull.Value)
           {
               myConfigration.DefaultEmailConfigrationId = (int)reader["default_email_configration_id"];
           }

           return myConfigration;
       }

     
    }
}
