using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Common.DataAccess.IRepository;
using System.Data.SqlClient;
using SagePOS.Manger.Common.Business.Entity;
using SF.Framework.Data;
using System.Data;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class ConfigrationRepository:IConfigrationRepository 
    {

        #region IRepository<Configration,int> Members

        public void Add(ref Configration myEntity)
        {
            throw new NotImplementedException();
        }

        public void Delete(ref Configration myEntity)
        {
            throw new NotImplementedException();
        }

        public void DeleteLogical(ref Configration myEntity)
        {
            throw new NotImplementedException();
        }

        public List<Configration> FindAll(CriteriaBase myCriteria)
        {
            List<Configration> Configs = new List<Configration>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            if (myCriteria != null)
            {
                //Instance.AddInParameter("@RoleIDToExclude", System.Data.SqlDbType.Int, ((PermissionCriteria)myCriteria).RoleToExclude);
            }

            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_Configuration_FindAll", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    Configs.Add(GetMapper(reader));
                }
                reader.Close();
                return Configs;
            }
            else
            {
                return null;
            }
        }

        public Configration FindById(int Id, CriteriaBase myCriteria)
        {
           
             SQLDAHelper Instance = new SQLDAHelper();
             SqlConnection conn = Instance.GetConnection(false);
            if (myCriteria != null)
            {
                //Instance.AddInParameter("@RoleIDToExclude", System.Data.SqlDbType.Int, ((PermissionCriteria)myCriteria).RoleToExclude);
            }

            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_Configuration_FindById", conn);
            if (reader != null && reader.HasRows)
            {
                reader.Read();                
                Configration Config= GetMapper(reader);
                reader.Close();
                return Config;
            }
            else
            {
                return null;
            }
        }

        public List<Configration> FindByParentId<TParentId>(TParentId ParentId, Type ParentType, CriteriaBase myCriteria)
        {
            throw new NotImplementedException();
        }

        public void Update(ref Configration myEntity)
        {
             SQLDAHelper Instance = new SQLDAHelper();
             SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@DefaultLanguage", System.Data.SqlDbType.Int, (int)myEntity.DefaultLanguage);
            Instance.AddInParameter("@LockOnUncompletedTasks", System.Data.SqlDbType.Bit, myEntity.LockUncompleteTasks);
            Instance.AddInParameter("@PageSize", System.Data.SqlDbType.NVarChar, myEntity.PageSize);
            Instance.AddInParameter("@TagsMaxFontSize", System.Data.SqlDbType.NVarChar, myEntity.MaxFontSize);
            Instance.AddInParameter("@TagsMaxTagstoAppear", System.Data.SqlDbType.Int, myEntity.MaxTagsToAppear);
            Instance.AddInParameter("@TagsMinFontSize", System.Data.SqlDbType.Int, myEntity.MinFontSize);
            Instance.AddInParameter("@firstDay", System.Data.SqlDbType.Int, myEntity.FirstDay);
            Instance.AddInParameter("@firstHour", System.Data.SqlDbType.Int, myEntity.FirstHour);
            Instance.AddInParameter("@minTime", System.Data.SqlDbType.Int, myEntity.MinTime);
            Instance.AddInParameter("@maxTime", System.Data.SqlDbType.Int, myEntity.MaxTime);
            Instance.AddInParameter("@defaultEventMinutes", System.Data.SqlDbType.Int, myEntity.DefaultEventMinutes);
            Instance.AddInParameter("@weekends", System.Data.SqlDbType.Bit, myEntity.Weekends);
            Instance.AddInParameter("@dragOpacity", System.Data.SqlDbType.Float, myEntity.DragOpacity);
            Instance.AddInParameter("@companyText", System.Data.SqlDbType.NVarChar, myEntity.CompanyTextEnglish);
            Instance.AddInParameter("@companyTextAr", System.Data.SqlDbType.NVarChar, myEntity.CompanyTextArabic);
            Instance.AddInParameter("@personText", System.Data.SqlDbType.NVarChar, myEntity.PersonTextEnglish);
            Instance.AddInParameter("@personTextAr", System.Data.SqlDbType.NVarChar, myEntity.PersonTextArabic);
            Instance.AddInParameter("@tasksNumberToShow", System.Data.SqlDbType.Int, myEntity.TasksNumberToShow);
            Instance.AddInParameter("@pluralCompanyText", System.Data.SqlDbType.NVarChar, myEntity.PluralCompanyTextEnglish);
            Instance.AddInParameter("@pluralCompanyTextAr", System.Data.SqlDbType.NVarChar, myEntity.PluralCompanyTextArabic);
            Instance.AddInParameter("@pluralPersonText", System.Data.SqlDbType.NVarChar, myEntity.PluralPersonTextEnglish);
            Instance.AddInParameter("@pluralPersonTextAr", System.Data.SqlDbType.NVarChar, myEntity.PluralPersonTextArabic);
            Instance.AddInParameter("@personNameFormat", System.Data.SqlDbType.NVarChar, myEntity.PersonNameFormat);
            Instance.AddInParameter("@addressFormat", System.Data.SqlDbType.NVarChar, myEntity.AddressFormat);
            Instance.AddInParameter("@notifications_count_send_form_windows_service", System.Data.SqlDbType.Int, myEntity.NotificationCount);
            Instance.AddInParameter("@notifications_top_values", System.Data.SqlDbType.Int, myEntity.NotificationsTopValues);
            Instance.AddInParameter("@force_receive_notifications", System.Data.SqlDbType.Bit, myEntity.ForceRecivingNotifications);
            Instance.AddInParameter("@default_email_configration_id", System.Data.SqlDbType.Int, myEntity.DefaultEmailConfigrationId);
            


            try
            {
                Instance.ExcuteNonQuery("dbo.SP_Configuration_Update", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string CustomConnectionString
        {
            get;
            set;
        }

        public bool UseCustomConnectionString
        {
            get;
            set;
        }
        public Object BeginTransaction()
        {
            return new object();
        }


        public void CommitTransaction(Object object1)
        {
        }

        public void Rollback(Object object1)
        {

        }

        public void KillConnection()
        {

        }


        public bool UseSharedSession { get; set; }


    
        #endregion
    }
}
