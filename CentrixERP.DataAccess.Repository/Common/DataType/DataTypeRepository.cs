using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.Framework;
using SF.FrameworkEntity;
using SagePOS.Common.DataAccess.IRepository;
using SagePOS.Manger.Common.Business.Entity;
using System.Data.SqlClient;
using SF.Framework.Data;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class DataTypeRepository : IDataTypeRepository
    {
        #region IRepository<DataType,int> Members

        public void Add(ref SagePOS.Manger.Common.Business.Entity.DataType myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            if (UseCustomConnectionString)
                Instance.SetConnectionString(CustomConnectionString);
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@data_type_name", System.Data.SqlDbType.NVarChar, myEntity.DataTypeName);
            Instance.AddInParameter("@data_type_name_ar", System.Data.SqlDbType.NVarChar, myEntity.DataTypeName);
            Instance.AddInParameter("@CreatedBy", System.Data.SqlDbType.Int, myEntity.CreatedBy);
            Instance.AddOutParameter("@DataTypeID", System.Data.SqlDbType.Int, (object)myEntity.DataTypeID);
            try
            {
                Instance.ExcuteNonQuery("dbo.sp_DataType_Add", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            var outPutParam = Instance.GetOutParamValue("@DataTypeID");
            if (outPutParam != DBNull.Value)
                myEntity.DataTypeID = (int)outPutParam;
        }

        public void Delete(ref SagePOS.Manger.Common.Business.Entity.DataType myEntity)
        {
            throw new NotImplementedException();
        }

        public void DeleteLogical(ref SagePOS.Manger.Common.Business.Entity.DataType myEntity)
        {
            throw new NotImplementedException();
        }

        public List<SagePOS.Manger.Common.Business.Entity.DataType> FindAll(CriteriaBase myCriteria)
        {
            List<DataType> dataTypeList = new List<DataType>();
             SQLDAHelper Instance = new SQLDAHelper(); 
             SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;
            if (myCriteria != null)
            {
                DataTypeCriteria myCtr = (DataTypeCriteria)myCriteria;
                if (!string.IsNullOrEmpty(myCtr.Name))
                {
                    Instance.AddInParameter("@Name", System.Data.SqlDbType.NVarChar, myCtr.Name);
                }

            }

            try
            {
                reader = Instance.ExcuteReader("dbo.sp_DataTypes_FindAll", conn);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                    dataTypeList.Add(GetDataTypeMapper(reader));
                reader.Close();
                return dataTypeList;
            }
            else
                return null;
        }

        public SagePOS.Manger.Common.Business.Entity.DataType FindById(int Id, CriteriaBase myCriteria)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@data_type_id", System.Data.SqlDbType.Int,Id);
            SqlDataReader reader;
            try
            {
                reader = Instance.ExcuteReader("dbo.sp_DataTypes_FindById", conn);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            if (reader != null && reader.HasRows)
            {
                reader.Read();
                DataType dtc = GetDataTypeMapper(reader);
                reader.Close();
                return dtc;
            }
            else
                return null;
        }

        public List<SagePOS.Manger.Common.Business.Entity.DataType> FindByParentId<TParentId>(TParentId ParentId, Type ParentType, CriteriaBase myCriteria)
        {
            throw new NotImplementedException();
        }

        public void Update(ref SagePOS.Manger.Common.Business.Entity.DataType myEntity)
        {
            throw new NotImplementedException();
        }

        public string CustomConnectionString
        {
            get;
            set;
        }

        public bool UseCustomConnectionString
        {
            get;
            set;
        }
        public Object BeginTransaction()
        {
            return new object();
        }


        public void CommitTransaction(Object object1)
        {
        }

        public void Rollback(Object object1)
        {

        }

        public void KillConnection()
        {

        }


        public bool UseSharedSession { get; set; }

        #endregion
    }
}
