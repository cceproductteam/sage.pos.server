using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.Framework;
using SF.FrameworkEntity;
using SagePOS.Manger.Common.Business.Entity;
using System.Data.SqlClient;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class DataTypeRepository
    {
        public DataType GetDataTypeMapper(SqlDataReader reader)
        {
            DataType datatype = new DataType();
            if (reader["data_type_name"] != DBNull.Value)
            {
                datatype.DataTypeName = (string)reader["data_type_name"];
            }
            if (reader["data_type_id"] != DBNull.Value)
            {
                datatype.DataTypeID = (int)reader["data_type_id"];
            }

            if (reader["created_by"] != DBNull.Value)
            {
                datatype.CreatedBy = (int)reader["created_by"];
            }

               if (reader["updated_by"] != DBNull.Value)
            {
                datatype.UpdatedBy = (int)reader["updated_by"];
            }

               if (reader["created_date"] != DBNull.Value)
            {
                datatype.CreatedDate = ((DateTime)reader["created_date"]).GetLocalTime();
            }
               if (reader["updated_date"] != DBNull.Value)
            {
                datatype.UpdateDate = ((DateTime)reader["updated_date"]).GetLocalTime();
            }


            return datatype;
        }
    }
}
