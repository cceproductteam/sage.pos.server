
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using SagePOS.Manger.Common.Business.Entity;
using SagePOS.Common.DataAccess.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class SystemConfigurationWebKeyRepository : ISystemConfigurationWebKeyRepository
    {
        #region IRepository< SystemConfigurationWebKey, int > Members

        public void Add(ref SystemConfigurationWebKey myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);

			Instance.AddInParameter("@key", SqlDbType.NVarChar, myEntity.Key);
			Instance.AddInParameter("@value", SqlDbType.NVarChar, myEntity.Value);
			Instance.AddInParameter("@type", SqlDbType.Int, myEntity.Type);
            Instance.AddInParameter("@created_by", SqlDbType.Int, myEntity.CreatedBy);
            Instance.AddOutParameter("@system_configuration_web_key_id", SqlDbType.Int);

            Instance.ExcuteNonQuery("dbo.SP_SystemConfigurationWebKey_Add", conn, false);

            myEntity.SystemConfigurationWebKeyId = (int)Instance.GetOutParamValue("@system_configuration_web_key_id");
        }

        public void Update(ref SystemConfigurationWebKey myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@system_configuration_web_key_id", SqlDbType.Int, myEntity.SystemConfigurationWebKeyId);
			Instance.AddInParameter("@key", SqlDbType.NVarChar, myEntity.Key);
			Instance.AddInParameter("@value", SqlDbType.NVarChar, myEntity.Value);
			Instance.AddInParameter("@type", SqlDbType.Int, myEntity.Type);
            Instance.AddInParameter("@updated_by", SqlDbType.Int,myEntity.UpdatedBy);
            
            Instance.ExcuteNonQuery("dbo.SP_SystemConfigurationWebKey_Update", conn, false);
        }

        public SystemConfigurationWebKey FindById(int Id, CriteriaBase myCriteria)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@system_configuration_web_key_id", SqlDbType.Int, Id);
            SqlDataReader reader;

            reader = Instance.ExcuteReader("dbo.SP_SystemConfigurationWebKey_FindById", conn);

            if (reader != null && reader.HasRows)
            {
                reader.Read();
                SystemConfigurationWebKey mySystemConfigurationWebKey = GetMapper(reader);
                reader.Close();
                return mySystemConfigurationWebKey;
            }
            else
                return null;
        }
        public List<SystemConfigurationWebKey> FindAll(CriteriaBase myCriteria)
        {
            List<SystemConfigurationWebKey> SystemConfigurationWebKeyList = new List<SystemConfigurationWebKey>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;
            if (myCriteria != null)
            {
                SystemConfigurationWebKeyCriteria criteria = (SystemConfigurationWebKeyCriteria)myCriteria;
            }
            reader = Instance.ExcuteReader("dbo.SP_SystemConfigurationWebKey_FindAll", conn);

            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                    SystemConfigurationWebKeyList.Add(GetMapper(reader));
                reader.Close();
                return SystemConfigurationWebKeyList;
            }
            else
                return null;

        }

        public List<SystemConfigurationWebKey> FindByParentId<TParentId>(TParentId ParentId, Type ParentType, CriteriaBase myCriteria)
        {
            List<SystemConfigurationWebKey> SystemConfigurationWebKeyList = new List<SystemConfigurationWebKey>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);

	

            if (myCriteria != null)
            {
                SystemConfigurationWebKeyCriteria criteria = (SystemConfigurationWebKeyCriteria)myCriteria;
            }			

            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_SystemConfigurationWebKey_FindByParentId", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    SystemConfigurationWebKeyList.Add(GetMapper(reader));
                }
                reader.Close();
                return SystemConfigurationWebKeyList;
            }
            else
            {
                return null;
            }
        }

        public void Delete(ref SystemConfigurationWebKey myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@system_configuration_web_key_id", SqlDbType.Int, myEntity.SystemConfigurationWebKeyId);

            Instance.ExcuteNonQuery("dbo.SP_SystemConfigurationWebKey_Delete", conn, false);
        }

        public void DeleteLogical(ref SystemConfigurationWebKey myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@system_configuration_web_key_id", SqlDbType.Int, myEntity.SystemConfigurationWebKeyId);

            Instance.ExcuteNonQuery("dbo.SP_SystemConfigurationWebKey_DeleteLogical", conn, false);
        }

        public string CustomConnectionString
        {
            get;
            set;
        }

        public bool UseCustomConnectionString
        {
            get;
            set;
        }
        public Object BeginTransaction()
        {
            return new object();
        }


        public void CommitTransaction(Object object1)
        {
        }

        public void Rollback(Object object1)
        {

        }

        public void KillConnection()
        {

        }


        public bool UseSharedSession { get; set; }
		#endregion
    }
}