using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using SagePOS.Manger.Common.Business.Entity;
using SagePOS.Common.DataAccess.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class DocumentEntityRepository : RepositoryBaseClass<DocumentEntity, DocumentEntityLite, int>, IDocumentEntityRepository
    {
        #region IRepository< DocumentEntity, int > Members

        public DocumentEntityRepository()
        {
            this.AddSPName = "SP_DocumentEntity_Add";
            this.UpdateSPName = "SP_DocumentEntity_Update";
            this.DeleteSPName = "SP_DocumentEntity_Delete";
            this.DeleteLogicalSPName = "SP_DocumentEntity_DeleteLogical";
            this.FindAllSPName = "SP_DocumentEntity_FindAll";
            this.FindByIdSPName = "SP_DocumentEntity_FindById";
            this.FindByParentSPName = "SP_DocumentEntity_FindByParentId";
            this.FindAllLiteSPName = "SP_DocumentEntity_FindAll_Lite";
            this.FindByIdLiteSPName = "SP_DocumentEntity_FindById_Lite";
            this.AdvancedSearchSPName = "SP_DocumentEntity_AdvancedSearch_Lite";
        }


        public List<DocumentEntity> FindByEntityId(DocumentEntityCriteria myCriteria)
        {
            List<DocumentEntity> DocumentList = new List<DocumentEntity>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;

            if (myCriteria != null)
            {

                Instance.AddInParameter("@entity_id", SqlDbType.Int, myCriteria.EntityId);
                Instance.AddInParameter("@entity_value_id", SqlDbType.Int, myCriteria.EntityValueId);//ParentId
                Instance.AddInParameter("@parent_folder_id", SqlDbType.Int, myCriteria.ParentFolderId);
            }
            reader = Instance.ExcuteReader("dbo.SP_DocumentEntity_FindByEntityId", conn);


            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                    DocumentList.Add(GetDocumentEntityData(reader));
                reader.Close();
                return DocumentList;
            }
            else
                return null;
        }


        public override List<DocumentEntity> FindByParentId<TParentId>(TParentId ParentId, Type ParentType, CriteriaBase myCriteria)
        {
            List<DocumentEntity> DocumentList = new List<DocumentEntity>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;
            DocumentEntityCriteria Criteria = (myCriteria == null) ? new DocumentEntityCriteria() : (DocumentEntityCriteria)myCriteria;
            Criteria.EntityValueId = Convert.ToInt32(ParentId);
            Criteria.EntityId = SagePOS.Server.Configuration.Configuration.GetEntityId(ParentType);
          

            if (Criteria != null)
            {

                Instance.AddInParameter("@entity_id", SqlDbType.Int, Criteria.EntityId);
                Instance.AddInParameter("@entity_value_id", SqlDbType.Int, Criteria.EntityValueId);//ParentId
                Instance.AddInParameter("@parent_folder_id", SqlDbType.Int, Criteria.ParentFolderId);
            }
            reader = Instance.ExcuteReader("dbo.SP_DocumentEntity_FindByParentId", conn);


            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                    DocumentList.Add(GetDocumentEntityData(reader));
                reader.Close();
                return DocumentList;
            }
            else
                return null;
        }

        
		#endregion
    }
}