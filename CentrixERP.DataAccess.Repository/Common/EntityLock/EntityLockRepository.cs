
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using SagePOS.Manger.Common.Business.Entity;
using SagePOS.Common.DataAccess.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class EntityLockRepository : IEntityLockRepository
    {
        #region IRepository< EntityLock, int > Members

        public void Add(ref EntityLock myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);

			Instance.AddInParameter("@entity_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.EntityId));
            Instance.AddInParameter("@entity_value_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.EntityValueId));
			//Instance.AddInParameter("@lock_all_entity", SqlDbType.Bit, myEntity.LockAllEntity);
            Instance.AddInParameter("@created_by", SqlDbType.Int, myEntity.CreatedBy);

            if (myEntity.EntityLastUpdatedDate.HasValue)
                Instance.AddInParameter("@entity_last_updated_date", SqlDbType.DateTime, myEntity.EntityLastUpdatedDate.Value.ToLocalTime());

            Instance.AddInParameter("@lock_without_check", SqlDbType.Bit, myEntity.LockWithoutCheck);
            Instance.AddInParameter("@lock_for_delete", SqlDbType.Bit, myEntity.LockForDelete);
            

            Instance.AddOutParameter("@entity_lock_id", SqlDbType.Int);
            Instance.AddOutParameter("@status", SqlDbType.Int);

            Instance.ExcuteNonQuery("dbo.SP_EntityLock_Add", conn, false);

            myEntity.EntityLockId = (int)Instance.GetOutParamValue("@entity_lock_id");
            myEntity.Status = (int)Instance.GetOutParamValue("@status");
        }

        public void Update(ref EntityLock myEntity)
        {
            throw new NotImplementedException();
        }

        public EntityLock FindById(int Id, CriteriaBase myCriteria)
        {
            throw new NotImplementedException();
        }
        public List<EntityLock> FindAll(CriteriaBase myCriteria)
        {
            List<EntityLock> EntityLockList = new List<EntityLock>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;
            
            if (myCriteria != null)
            {
                EntityLockCriteria criteria = (EntityLockCriteria)myCriteria;
                if (criteria.EntityId.HasValue)
                    Instance.AddInParameter("@entity_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(criteria.EntityId.Value));
                if (criteria.UserId.HasValue)
                    Instance.AddInParameter("@user_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(criteria.UserId.Value));
                if (criteria.EntityValueId.HasValue)
                    Instance.AddInParameter("@entity_value_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(criteria.EntityValueId.Value));
                if (criteria.LockedBy.HasValue)
                    Instance.AddInParameter("@locked_by", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(criteria.LockedBy.Value));
            }

            reader = Instance.ExcuteReader("dbo.SP_EntityLock_FindAll", conn);

            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                    EntityLockList.Add(GetMapper(reader));
                reader.Close();
                return EntityLockList;
            }
            else
                return null;

        }

        public List<EntityLock> FindByParentId<TParentId>(TParentId ParentId, Type ParentType, CriteriaBase myCriteria)
        {
            throw new NotImplementedException();
        }

        public void Delete(ref EntityLock myEntity)
        {
            throw new NotImplementedException();
        }

        public void DeleteLogical(ref EntityLock myEntity)
        {
            throw new NotImplementedException();
        }
		#endregion

        public void UnLock(int entityId, int? entityValueId)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);

            Instance.AddInParameter("@entity_id", SqlDbType.Int, entityId);
            if (entityValueId.HasValue)
                Instance.AddInParameter("@entity_value_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(entityValueId.Value));

            Instance.ExcuteNonQuery("dbo.SP_EntityLock_UnLock", conn, false);
        }

        public void UnLock(int? userId)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);

            if (userId.HasValue)
                Instance.AddInParameter("@user_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(userId.Value));

            Instance.ExcuteNonQuery("dbo.SP_EntityLock_UnLockAll", conn, false);
        }

        public List<EntityLockLite> FindAllLite(CriteriaBase myCriteria)
        {
            List<EntityLockLite> EntityLockList = new List<EntityLockLite>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;

            if (myCriteria != null)
            {
                EntityLockCriteria criteria = (EntityLockCriteria)myCriteria;
                Instance.AddInParameter("@user_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(criteria.LockedBy.Value));
            }

            reader = Instance.ExcuteReader("dbo.SP_EntityLock_FindAllLite", conn);

            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                    EntityLockList.Add(GetMapperLite(reader));
                reader.Close();
                return EntityLockList;
            }
            else
                return null;
        }


        public void LockWithOutCheck(EntityLock myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);

            Instance.AddInParameter("@entity_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.EntityId));
            Instance.AddInParameter("@entity_value_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.EntityValueId));
            Instance.AddInParameter("@created_by", SqlDbType.Int, myEntity.CreatedBy);

            if (myEntity.EntityLastUpdatedDate.HasValue)
                Instance.AddInParameter("@entity_last_updated_date", SqlDbType.DateTime, myEntity.EntityLastUpdatedDate.Value.ToLocalTime());


            Instance.AddOutParameter("@entity_lock_id", SqlDbType.Int);

            Instance.ExcuteNonQuery("dbo.SP_EntityLock_AddWithoutCheck", conn, false);

            myEntity.EntityLockId = (int)Instance.GetOutParamValue("@entity_lock_id");
        }

        public string CustomConnectionString
        {
            get;
            set;
        }

        public bool UseCustomConnectionString
        {
            get;
            set;
        }
        public Object BeginTransaction()
        {
            return new object();
        }


        public void CommitTransaction(Object object1)
        {
        }

        public void Rollback(Object object1)
        {

        }

        public void KillConnection()
        {

        }


        public bool UseSharedSession { get; set; }
    }
}