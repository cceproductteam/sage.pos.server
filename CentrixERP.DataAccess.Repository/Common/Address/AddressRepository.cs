using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.Framework;
using SF.FrameworkEntity;
using SagePOS.Common.DataAccess.IRepository;
using System.Data.SqlClient;
using SF.Framework.Data;
using SagePOS.Manger.Common.Business.Entity;
using SagePOS.Common.DataAccess.IRepository;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class AddressRepository : IAddressRepository
    {
        #region IRepository<Address,int> Members

        public void Add(ref SagePOS.Manger.Common.Business.Entity.Address myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@Place", System.Data.SqlDbType.NVarChar, myEntity.Place);
            Instance.AddInParameter("@Nearby", System.Data.SqlDbType.NVarChar, myEntity.NearBy);
            Instance.AddInParameter("@BuildingNumber", System.Data.SqlDbType.NVarChar, myEntity.BuildingNumber);
            Instance.AddInParameter("@Street", System.Data.SqlDbType.NVarChar, myEntity.StreetAddress);
            Instance.AddInParameter("@Pobox", System.Data.SqlDbType.NVarChar, myEntity.POBox);
            Instance.AddInParameter("@Country", System.Data.SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.CountryId));
            Instance.AddInParameter("@City", System.Data.SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.CityId));
            Instance.AddInParameter("@ZipCode", System.Data.SqlDbType.NVarChar, myEntity.ZipCode);
            Instance.AddInParameter("@Region", System.Data.SqlDbType.NVarChar, myEntity.Region);
            Instance.AddInParameter("@officeNumber", System.Data.SqlDbType.NVarChar, myEntity.OfficeNumber);
            Instance.AddInParameter("@floor", System.Data.SqlDbType.NVarChar, myEntity.Floor);
            Instance.AddInParameter("@county", System.Data.SqlDbType.NVarChar, myEntity.County);
            Instance.AddInParameter("@citytext", System.Data.SqlDbType.NVarChar, myEntity.CityText);
            Instance.AddInParameter("@Area", System.Data.SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.AreaId));
            Instance.AddInParameter("@CreatedBy", System.Data.SqlDbType.Int, myEntity.CreatedBy);
            Instance.AddOutParameter("@AddressID", System.Data.SqlDbType.Int);
            Instance.AddInParameter("@Type", System.Data.SqlDbType.NVarChar, myEntity.Type);

            try
            {
                Instance.ExcuteNonQuery("dbo.sp_Address_Add", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            var outPutParamValue = Instance.GetOutParamValue("@AddressID");
            if (outPutParamValue != DBNull.Value)
                myEntity.AddressID = (int)outPutParamValue;
        }

        public void Delete(ref SagePOS.Manger.Common.Business.Entity.Address myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@AddressID", System.Data.SqlDbType.Int, myEntity.AddressID);
            try
            {
                Instance.ExcuteNonQuery("dbo.sp_Address_Delete", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteLogical(ref SagePOS.Manger.Common.Business.Entity.Address myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@AddressID", System.Data.SqlDbType.Int, myEntity.AddressID);
            try
            {
                Instance.ExcuteNonQuery("dbo.sp_Address_DeleteLogical", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SagePOS.Manger.Common.Business.Entity.Address> FindAll(CriteriaBase myCriteria)
        {
            List<Address> addressList = new List<Address>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;
            if (myCriteria != null)
            {               
                Instance.AddInParameter("@CityId", System.Data.SqlDbType.Int, SQLDAHelper .Convert_IntTODB (((AddressCriteria)myCriteria).CityId));
                Instance.AddInParameter("@AreaId", System.Data.SqlDbType.Int, SQLDAHelper.Convert_IntTODB(((AddressCriteria)myCriteria).AreaId));
            }
            reader = Instance.ExcuteReader("dbo.sp_Address_FindAll", conn);


            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                    addressList.Add(GetAddressMapper(reader));
                reader.Close();
                return addressList;
            }
            else
                return null;

        }

        public SagePOS.Manger.Common.Business.Entity.Address FindById(int Id, CriteriaBase myCriteria)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@AddressID", System.Data.SqlDbType.Int, Id);
            SqlDataReader reader;

            try
            {
                reader = Instance.ExcuteReader("dbo.sp_Address_FindByID", conn);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (reader != null && reader.HasRows)
            {
                reader.Read();
                Address address= GetAddressMapper(reader);
                reader.Close();
                return address;
            }
            else
                return null;
        }

        public List<SagePOS.Manger.Common.Business.Entity.Address> FindByParentId<TParentId>(TParentId ParentId, Type ParentType, CriteriaBase myCriteria)
        {
            throw new NotImplementedException();
        }

        public void Update(ref SagePOS.Manger.Common.Business.Entity.Address myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@Place", System.Data.SqlDbType.NVarChar, myEntity.Place);
            Instance.AddInParameter("@Nearby", System.Data.SqlDbType.NVarChar, myEntity.NearBy);
            Instance.AddInParameter("@BuildingNumber", System.Data.SqlDbType.NVarChar, myEntity.BuildingNumber);
            Instance.AddInParameter("@Street", System.Data.SqlDbType.NVarChar, myEntity.StreetAddress);
            Instance.AddInParameter("@Pobox", System.Data.SqlDbType.NVarChar, myEntity.POBox);
            Instance.AddInParameter("@Country", System.Data.SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.CountryId));
            Instance.AddInParameter("@City", System.Data.SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.CityId));
            Instance.AddInParameter("@ZipCode", System.Data.SqlDbType.NVarChar, myEntity.ZipCode);
            Instance.AddInParameter("@Region", System.Data.SqlDbType.NVarChar, myEntity.Region);
            Instance.AddInParameter("@Area", System.Data.SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.AreaId));
            Instance.AddInParameter("@UpdatedBy", System.Data.SqlDbType.Int, myEntity.UpdatedBy);

            Instance.AddInParameter("@officeNumber", System.Data.SqlDbType.NVarChar, myEntity.OfficeNumber);
            Instance.AddInParameter("@floor", System.Data.SqlDbType.NVarChar, myEntity.Floor);
            Instance.AddInParameter("@county", System.Data.SqlDbType.NVarChar, myEntity.County);
            Instance.AddInParameter("@citytext", System.Data.SqlDbType.NVarChar, myEntity.CityText);
            Instance.AddInParameter("@Type", System.Data.SqlDbType.NVarChar, myEntity.Type);

            Instance.AddInParameter("@AddressID", System.Data.SqlDbType.Int, myEntity.AddressID);
            try
            {
                Instance.ExcuteNonQuery("dbo.sp_Address_Update", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string CustomConnectionString
        {
            get;
            set;
        }

        public bool UseCustomConnectionString
        {
            get;
            set;
        }
        public Object BeginTransaction()
        {
            return new object();
        }


        public void CommitTransaction(Object object1)
        {
        }

        public void Rollback(Object object1)
        {

        }

        public void KillConnection()
        {

        }


        public bool UseSharedSession { get; set; }

        #endregion
    }
}
