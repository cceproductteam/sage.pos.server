
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using SagePOS.Manger.Common.Business.Entity;
using System.Data.SqlClient;
using System.Data;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class ModuleFieldRepository
    {
        private ModuleField GetMapper(SqlDataReader reader)
        {
            ModuleField myEntity = new ModuleField();
            if (reader["module_field_id"] != DBNull.Value)
            {
                myEntity.ModuleFieldId = (int)reader["module_field_id"];
            }
            if (reader["module_id"] != DBNull.Value)
            {
                myEntity.ModuleId = (int)reader["module_id"];
            }
            if (reader["ui_name_en"] != DBNull.Value)
            {
                myEntity.UiNameEn = reader["ui_name_en"].ToString();
            }
            if (reader["ui_name_ar"] != DBNull.Value)
            {
                myEntity.UiNameAr = reader["ui_name_ar"].ToString();
            }
            if (reader["db_column_name_en"] != DBNull.Value)
            {
                myEntity.DbColumnNameEn = reader["db_column_name_en"].ToString();
            }
            if (reader["db_column_name_ar"] != DBNull.Value)
            {
                myEntity.DbColumnNameAr = reader["db_column_name_ar"].ToString();
            }
            if (reader["service_name"] != DBNull.Value)
            {
                myEntity.ServiceName = reader["service_name"].ToString();
            }
            if (reader["service_method"] != DBNull.Value)
            {
                myEntity.ServiceMethod = reader["service_method"].ToString();
            }
            if (reader["control_type"] != DBNull.Value)
            {
                myEntity.ControlType = (int)reader["control_type"];
            }
            if (reader["property_parent_id"] != DBNull.Value)
            {
                myEntity.PropertyParentId = (int)reader["property_parent_id"];
            }
            if (reader["property_name_en"] != DBNull.Value)
            {
                myEntity.PropertyNameEn = reader["property_name_en"].ToString();
            }
            if (reader["property_name_ar"] != DBNull.Value)
            {
                myEntity.PropertyNameAr = reader["property_name_ar"].ToString();
            }
            if (reader["data_type_id"] != DBNull.Value)
            {
                myEntity.DataTypeId = (int)reader["data_type_id"];
            }
            if (reader["created_by"] != DBNull.Value)
            {
                myEntity.CreatedBy = (int)reader["created_by"];
            }
            if (reader["updated_by"] != DBNull.Value)
            {
                myEntity.UpdatedBy = (int)reader["updated_by"];
            }
            if (reader["created_date"] != DBNull.Value)
            {
                myEntity.CreatedDate = (DateTime)reader["created_date"];
            }
            if (reader["updated_date"] != DBNull.Value)
            {
                myEntity.UpdatedDate = (DateTime)reader["updated_date"];
            }
            return myEntity;
        }

        private SearchFieldCriteria GetSearchCriteriaMapper(SqlDataReader reader)
        {
            SearchFieldCriteria search = new SearchFieldCriteria();
            if (reader["search_criteria_id"] != DBNull.Value)
            {
                search.SearchCriteriaId = Convert.ToInt32(reader["search_criteria_id"]);
            }
            if (reader["filter_type"] != DBNull.Value)
            {
                search.FilterTypeNameEn = reader["filter_type"].ToString();
            }
            if (reader["filter_type_ar"] != DBNull.Value)
            {
                search.FilterTypeNameAr = reader["filter_type_ar"].ToString();
            }
            return search;
        }

    }
}