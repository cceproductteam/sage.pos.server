
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using SagePOS.Manger.Common.Business.Entity;
using SagePOS.Common.DataAccess.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class ModuleFieldRepository : IModuleFieldRepository
    {
        #region IRepository< ModuleField, int > Members

        public void Add(ref ModuleField myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@module_id", SqlDbType.Int, myEntity.ModuleId);
            Instance.AddInParameter("@ui_name_en", SqlDbType.NVarChar, myEntity.UiNameEn);
            Instance.AddInParameter("@ui_name_ar", SqlDbType.NVarChar, myEntity.UiNameAr);
            Instance.AddInParameter("@db_column_name_en", SqlDbType.NVarChar, myEntity.DbColumnNameEn);
            Instance.AddInParameter("@db_column_name_ar", SqlDbType.NVarChar, myEntity.DbColumnNameAr);
            Instance.AddInParameter("@service_name", SqlDbType.NVarChar, myEntity.ServiceName);
            Instance.AddInParameter("@service_method", SqlDbType.NVarChar, myEntity.ServiceMethod);
            Instance.AddInParameter("@control_type", SqlDbType.Int, myEntity.ControlType);
            Instance.AddInParameter("@property_parent_id", SqlDbType.Int, myEntity.PropertyParentId);
            Instance.AddInParameter("@property_name_en", SqlDbType.NVarChar, myEntity.PropertyNameEn);
            Instance.AddInParameter("@property_name_ar", SqlDbType.NVarChar, myEntity.PropertyNameAr);
            Instance.AddInParameter("@created_by", SqlDbType.Int, myEntity.CreatedBy);
            Instance.AddOutParameter("@module_field_id", SqlDbType.Int);

            Instance.ExcuteNonQuery("dbo.SP_ModuleField_Add", conn, false);

            myEntity.ModuleFieldId = (int)Instance.GetOutParamValue("@module_field_id");
        }

        public void Update(ref ModuleField myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@module_field_id", SqlDbType.Int, myEntity.ModuleFieldId);
            Instance.AddInParameter("@module_id", SqlDbType.Int, myEntity.ModuleId);
            Instance.AddInParameter("@ui_name_en", SqlDbType.NVarChar, myEntity.UiNameEn);
            Instance.AddInParameter("@ui_name_ar", SqlDbType.NVarChar, myEntity.UiNameAr);
            Instance.AddInParameter("@db_column_name_en", SqlDbType.NVarChar, myEntity.DbColumnNameEn);
            Instance.AddInParameter("@db_column_name_ar", SqlDbType.NVarChar, myEntity.DbColumnNameAr);
            Instance.AddInParameter("@service_name", SqlDbType.NVarChar, myEntity.ServiceName);
            Instance.AddInParameter("@service_method", SqlDbType.NVarChar, myEntity.ServiceMethod);
            Instance.AddInParameter("@control_type", SqlDbType.Int, myEntity.ControlType);
            Instance.AddInParameter("@property_parent_id", SqlDbType.Int, myEntity.PropertyParentId);
            Instance.AddInParameter("@property_name_en", SqlDbType.NVarChar, myEntity.PropertyNameEn);
            Instance.AddInParameter("@property_name_ar", SqlDbType.NVarChar, myEntity.PropertyNameAr);
            Instance.AddInParameter("@updated_by", SqlDbType.Int, myEntity.UpdatedBy);

            Instance.ExcuteNonQuery("dbo.SP_ModuleField_Update", conn, false);

        }

        public ModuleField FindById(int Id, CriteriaBase myCriteria)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@module_field_id", SqlDbType.Int, Id);
            SqlDataReader reader;
            reader = Instance.ExcuteReader("dbo.sp_ModuleField_FindByID", conn);

            if (reader != null && reader.HasRows)
            {
                reader.Read();
                ModuleField myModulefield = GetMapper(reader);
                reader.Close();
                return myModulefield;
            }
            else
                return null;
        }

        public List<ModuleField> FindAll(CriteriaBase myCriteria)
        {
            List<ModuleField> ModuleFieldList = new List<ModuleField>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;
            reader = Instance.ExcuteReader("dbo.sp_ModuleField_FindAll", conn);

            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                    ModuleFieldList.Add(GetMapper(reader));
                reader.Close();
                return ModuleFieldList;
            }
            else
                return null;

        }

        public List<ModuleField> FindByParentId<TParentId>(TParentId ParentId, Type ParentType, CriteriaBase myCriteria)
        {
            List<ModuleField> ModuleFieldList = new List<ModuleField>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);

            if (ParentType == typeof(Module))
            {
                Instance.AddInParameter("@module_id", SqlDbType.Int, ParentId);
            }

            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_ModuleField_FindByParentId", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    ModuleFieldList.Add(GetMapper(reader));
                }
                reader.Close();
                return ModuleFieldList;
            }
            else
            {
                return null;
            }
        }

        public void Delete(ref ModuleField myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@module_field_id", SqlDbType.Int, myEntity.ModuleFieldId);


            Instance.ExcuteNonQuery("dbo.SP_ModuleField_Delete", conn, false);

        }

        public void DeleteLogical(ref ModuleField myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@module_field_id", SqlDbType.Int, myEntity.ModuleFieldId);


            Instance.ExcuteNonQuery("dbo.SP_ModuleField_DeleteLogical", conn, false);

        }
        public string CustomConnectionString
        {
            get;
            set;
        }

        public bool UseCustomConnectionString
        {
            get;
            set;
        }
        public Object BeginTransaction()
        {
            return new object();
        }


        public void CommitTransaction(Object object1)
        {
        }

        public void Rollback(Object object1)
        {

        }

        public void KillConnection()
        {

        }


        public bool UseSharedSession { get; set; }
        #endregion

        #region IModuleFieldRepository Members

        public List<SearchFieldCriteria> FindAllSearchCriteria()
        {
            List<SearchFieldCriteria> ModuleFieldList = new List<SearchFieldCriteria>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;
            reader = Instance.ExcuteReader("dbo.SP_SearchCriteria_FindAll", conn);

            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                    ModuleFieldList.Add(GetSearchCriteriaMapper(reader));
                reader.Close();
                return ModuleFieldList;
            }
            else
                return null;
        }

        public SearchFieldCriteria FindSearchFieldCriteriaById(int SearchFieldCriteriaId)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@search_criteria_id", SqlDbType.Int, SearchFieldCriteriaId);
            SqlDataReader reader;
            reader = Instance.ExcuteReader("dbo.SP_FieldSearchCriteria_FindById", conn);

            if (reader != null && reader.HasRows)
            {
                reader.Read();
                SearchFieldCriteria SearchFieldCriteria = GetSearchCriteriaMapper(reader);
                reader.Close();
                return SearchFieldCriteria;
            }
            else
                return null;
        }

        #endregion
    }
}