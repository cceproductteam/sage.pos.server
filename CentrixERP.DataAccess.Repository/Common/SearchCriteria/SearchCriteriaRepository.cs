using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.Framework.Data;
using System.Data.SqlClient;
using SagePOS.Common.DataAccess.IRepository;
using Entity=SagePOS.Manger.Common.Business.Entity;
using SagePOS.Manger.Common.Business.Entity;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class SearchCriteriaRepository : ISearchCriteriaRepository
    {
              #region IRepository<SerchCriteria,int> Members

        public void Add(ref Entity.SearchCriteria myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@user_id", System.Data.SqlDbType.Int, myEntity.UserId);
            Instance.AddInParameter("@enitity_id", System.Data.SqlDbType.Int, myEntity.EntityId);
            Instance.AddInParameter("@search_name", System.Data.SqlDbType.NVarChar, myEntity.SearchName);
            Instance.AddInParameter("@search_criteria", System.Data.SqlDbType.NVarChar, myEntity.SerachCriteria);
            Instance.AddOutParameter("@saved_search_id", System.Data.SqlDbType.Int);
            Instance.ExcuteNonQuery("SP_Search_Criteria_Add", conn, false);
        }

        public void Delete(ref Entity.SearchCriteria myEntity)
        {
            throw new NotImplementedException();
        }

        public void DeleteLogical(ref Entity.SearchCriteria myEntity)
        {
            throw new NotImplementedException();
        }

        public List<Entity.SearchCriteria> FindAll(SF.FrameworkEntity.CriteriaBase myCriteria)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            List<SearchCriteria> searchCriteriaList = new List<SearchCriteria>();
            if (myCriteria != null)
            {
                SearchCriteriasCriteria criteria = (SearchCriteriasCriteria)myCriteria;
                Instance.AddInParameter("@user_id", System.Data.SqlDbType.Int, criteria.UserID);
                Instance.AddInParameter("@enitity_id", System.Data.SqlDbType.Int, criteria.EntityID);
 
            }
            SqlDataReader reader;
            try
            {
                reader = Instance.ExcuteReader("SP_Search_Criteria_FindAll", conn);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                    searchCriteriaList.Add(GetSearchCriteriaMapper(reader));
                reader.Close();
                return searchCriteriaList;
            }
            else
                return null;
        }

        public Entity.SearchCriteria FindById(int Id, SF.FrameworkEntity.CriteriaBase myCriteria)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SearchCriteria searchCriteria = new SearchCriteria();
            Instance.AddInParameter("@saved_search_id", System.Data.SqlDbType.Int, Id);
            SqlDataReader reader;
            try
            {
                reader = Instance.ExcuteReader("SP_Search_Criteria_FindById", conn);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                    searchCriteria=GetSearchCriteriaMapper(reader);
                reader.Close();
                return searchCriteria;
            }
            else
                return null;
        }

        public List<Entity.SearchCriteria> FindByParentId<TParentId>(TParentId ParentId, Type ParentType, SF.FrameworkEntity.CriteriaBase myCriteria)
        {
            throw new NotImplementedException();
        }

        public void Update(ref Entity.SearchCriteria myEntity)
        {
            throw new NotImplementedException();
        }

        public string CustomConnectionString
        {
            get;
            set;
        }

        public bool UseCustomConnectionString
        {
            get;
            set;
        }
        public Object BeginTransaction()
        {
            return new object();
        }


        public void CommitTransaction(Object object1)
        {
        }

        public void Rollback(Object object1)
        {

        }

        public void KillConnection()
        {

        }


        public bool UseSharedSession { get; set; }

        #endregion
    }
}
