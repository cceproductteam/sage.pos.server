using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using SagePOS.Manger.Common.Business.Entity;
using SagePOS.Common.DataAccess.IRepository;
using System.Data.SqlClient;


namespace CentrixERP.Common.DataAccess.Repository
{

    public partial class CountryRepository : ICountryRepository
    {
        #region IRepository<Country,int> Members
        public void Add(ref SagePOS.Manger.Common.Business.Entity.Country myEntity)
        {
             SQLDAHelper Instance = new SQLDAHelper(); 
 SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@country_name_en", System.Data.SqlDbType.NVarChar, myEntity.CountryNameEn);
            Instance.AddInParameter("@country_name_ar", System.Data.SqlDbType.NVarChar, myEntity.CountryNameAr);
            Instance.AddInParameter("@country_name_code", System.Data.SqlDbType.VarChar, myEntity.CountryNameCode);
            Instance.AddInParameter("@country_code", System.Data.SqlDbType.NVarChar, myEntity.CountryCode);

            Instance.AddOutParameter("@Id", System.Data.SqlDbType.Int);

            try
            {
                Instance.ExcuteNonQuery("dbo.SP_Country_Add", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            myEntity.CountryId = (int)Instance.GetOutParamValue("@Id");

        }
        public void Delete(ref SagePOS.Manger.Common.Business.Entity.Country myEntity)
        {
             SQLDAHelper Instance = new SQLDAHelper(); 
 SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@Id", System.Data.SqlDbType.Int, myEntity.CountryId);
            try
            {
                Instance.ExcuteNonQuery("dbo.SP_Country_Delete", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void DeleteLogical(ref SagePOS.Manger.Common.Business.Entity.Country myEntity)
        {
             SQLDAHelper Instance = new SQLDAHelper(); 
 SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@Id", System.Data.SqlDbType.Int, myEntity.CountryId);
            try
            {
                Instance.ExcuteNonQuery("dbo.SP_Country_DeleteLogical", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<SagePOS.Manger.Common.Business.Entity.Country> FindAll(CriteriaBase myCriteria)
        {
            List<Country> myCountry = new List<Country>();
             SQLDAHelper Instance = new SQLDAHelper(); 
             SqlConnection conn = Instance.GetConnection(false);
            if (myCriteria != null)
            {
                CountryCriteria myCtr = (CountryCriteria)myCriteria;
                if (!string.IsNullOrEmpty(myCtr.Name))
                {
                    Instance.AddInParameter("@keyword", System.Data.SqlDbType.NVarChar, myCtr.Name);
                }
                if (myCtr.pageNumber>0)
                {
                    Instance.AddInParameter("@page_number", System.Data.SqlDbType.Int, myCtr.pageNumber);
                }
                if (myCtr.resultCount > 0)
                {
                    Instance.AddInParameter("@result_count", System.Data.SqlDbType.Int, myCtr.resultCount);
                }
                if (myCtr.from > 0)
                {
                    Instance.AddInParameter("@from", System.Data.SqlDbType.Int, myCtr.from);
                }
                if (myCtr.to > 0)
                {
                    Instance.AddInParameter("@to", System.Data.SqlDbType.Int, myCtr.to);
                }

            }
            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_Country_FindAll", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    myCountry.Add(GetMapper(reader));
                }
                reader.Close();
                return myCountry;
            }
            else
            {
                return null;
            }
        }
        public SagePOS.Manger.Common.Business.Entity.Country FindById(int Id, CriteriaBase myCriteria)
        {
             SQLDAHelper Instance = new SQLDAHelper(); 
 SqlConnection conn = Instance.GetConnection(false);

            Instance.AddInParameter("@Id", System.Data.SqlDbType.Int, Id);

            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_Country_FindById", conn);
            if (reader != null && reader.HasRows)
            {
                reader.Read();
                Country country = GetMapper(reader);
                reader.Close();
                return country;

            }
            else
            {
                return null;
            }
        }
        public List<SagePOS.Manger.Common.Business.Entity.Country> FindByParentId<TParentId>(TParentId ParentId, Type ParentType, CriteriaBase myCriteria) { throw new NotImplementedException(); }


        public void Update(ref SagePOS.Manger.Common.Business.Entity.Country myEntity)
        {
             SQLDAHelper Instance = new SQLDAHelper(); 
 SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@country_name_en", System.Data.SqlDbType.NVarChar, myEntity.CountryNameEn);
            Instance.AddInParameter("@country_name_ar", System.Data.SqlDbType.NVarChar, myEntity.CountryNameAr);
            Instance.AddInParameter("@country_name_code", System.Data.SqlDbType.VarChar, myEntity.CountryNameCode);
            Instance.AddInParameter("@country_code", System.Data.SqlDbType.NVarChar, myEntity.CountryCode);
            Instance.AddInParameter("@Id", System.Data.SqlDbType.Int, myEntity.CountryId);

            try
            {
                Instance.ExcuteNonQuery("dbo.SP_Country_Update", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string CustomConnectionString
        {
            get;
            set;
        }

        public bool UseCustomConnectionString
        {
            get;
            set;
        }
        public Object BeginTransaction()
        {
            return new object();
        }


        public void CommitTransaction(Object object1)
        {
        }

        public void Rollback(Object object1)
        {

        }

        public void KillConnection()
        {

        }


        public bool UseSharedSession { get; set; }

        #endregion
    }
}
