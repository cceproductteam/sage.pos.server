using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SagePOS.Manger.Common.Business.Entity ;

namespace SagePOS.Server.Business.Factory
{
    public interface INote:IBaseClass
    {
        int NoteId { get; set; }
        Note NoteObj { get; set; }        
    }
}
