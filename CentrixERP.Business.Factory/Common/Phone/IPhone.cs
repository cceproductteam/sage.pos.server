using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SagePOS.Manger.Common.Business.Entity ; 



namespace SagePOS.Server.Business.Factory
{
    public interface IPhone : IBaseClass
    {
        int PhoneId { get; set; }
        Phone PhoneObj { get; set; }
        bool Isdefault { get; set; }
    }
}
