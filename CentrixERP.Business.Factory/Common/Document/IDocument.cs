using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SagePOS.Manger.Common.Business.Entity ;

namespace SagePOS.Server.Business.Factory
{
    public interface IDocument:IBaseClass
    {
        int DocumentId { get; set; }
        Document DocumentObj { get; set; }
        string DocumentPath { get; }
        string DocumentUrl { get; }
        string ModuleName { get; }
    }
}
