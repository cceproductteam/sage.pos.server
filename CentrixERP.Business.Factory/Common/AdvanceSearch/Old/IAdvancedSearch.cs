using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;

namespace CentrixERP.Common.Business.Factory
{
    public interface IAdvancedSearch
    {
        List<Object> AdvancedSearchLite(CriteriaBase myCriteria);
    }
}
