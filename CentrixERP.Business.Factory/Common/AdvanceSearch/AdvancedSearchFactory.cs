using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Reflection;
using System.Configuration;
using System.IO;
using System.Xml.Linq;
using SF.Framework;

namespace SagePOS.Server.Business.Factory
{
    public class AdvancedSearchFactory
    {
        private string xmlPath;
        public IAdvancedSearch IManager;
        public IAdvancedSearchCriteria EntityCriteria;
        public int EntityId;

        public AdvancedSearchFactory(string moduleKey)
        {
            xmlPath = ConfigurationManager.AppSettings["TypeMapperPath"].ToString();
            CheckFileExists();
            GetType(moduleKey);
        }

        private void CheckFileExists()
        {
            if (!File.Exists(this.xmlPath))
                throw new Exception("The TypeMapper.xml file is not exists!!!!!");
        }

        private void GetType(string moduleKey)
        {
            try
            {
                XDocument doc = XDocument.Load(this.xmlPath);
                var tmp = from XElement element in doc.Elements("TypeMapper").Elements("Type")
                          where element.Attribute("Key").Value == moduleKey
                          select element;

                   
                if (tmp == null && tmp.Count() == 0)
                    return;

                XElement type = (XElement)tmp.First();
                this.IManager = (IAdvancedSearch)IoC.Instance.Resolve(Type.GetType(type.Element("IManager").Value));
                this.EntityId = type.Attribute("EntityId").Value.ToNumber();
                if (type.Element("Criteria").Value != null && type.Element("Criteria").Value != "")
                    this.EntityCriteria = (IAdvancedSearchCriteria)Activator.CreateInstance(Type.GetType(type.Element("Criteria").Value));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        Object AdvancedSearchLite(CriteriaBase myCriteria)
        {
            //MethodInfo method = this.IManager.GetType().GetMethod("AdvancedSearch");
            //return method.Invoke(this, new Object[] { myCriteria });
            return this.IManager.AdvancedSearchLite(myCriteria);
        }
    }
}