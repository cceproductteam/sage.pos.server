using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SagePOS.Server.Business.Factory
{
  public interface IAdvancedSearchCriteria
    {
       string SearchCriteria { get; set; }
       bool SortType { get; set; }
       int EntityId { get; set; }
       string SortFields { get; set; }
       int pageNumber { get; set; }
       int resultCount { get; set; }
    }
}
