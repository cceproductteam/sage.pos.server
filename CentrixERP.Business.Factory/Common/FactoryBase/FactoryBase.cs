using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Collections;

namespace SagePOS.Server.Business.Factory
{
    public class FactoryBase<T>
    {
        private Object iManager;
        private Type entityParent;
        public Object IManager
        {
            get { return this.iManager; }
        }

        private T iEntity;
        public T IEntity
        {
            get { return this.iEntity; }
            //set { this.iModuleObject = value; }
        }

        private Object iEntityCriteria;
        public Object IEntityCriteria
        {
            get { return this.iEntityCriteria; }
        }
        public Type EntityParent
        {
            get { return this.entityParent; }
        }

        public FactoryBase(string moduleKey)
        {
            TypeMapper<T> mapper = new TypeMapper<T>(moduleKey);
            this.iEntity = mapper.EntityObject;
            this.iManager = mapper.IManager;
            this.iEntityCriteria = mapper.EntityCriteria;
            this.entityParent = mapper.EntityParent;
        }

        public void Save(T myEntity)
        {
            MethodInfo save = this.iManager.GetType().GetMethod("Save");
            save.Invoke(this.iManager, new Object[] { myEntity });
        }

        //public void Delete(T myEntity)
        //{
        //    MethodInfo delete = this.iManager.GetType().GetMethod("Delete");
        //    delete.Invoke(this.iManager, new Object[] { myEntity });
        //}

        //public void DeleteLogical(T myEntity)
        //{
        //    MethodInfo deleteLogical = this.iManager.GetType().GetMethod("DeleteLogical");
        //    deleteLogical.Invoke(this.iManager, new Object[] { myEntity });
        //}

        //public void Update(T myEntity)
        //{
        //    MethodInfo update = this.iManager.GetType().GetMethod("Update");
        //    update.Invoke(this.iManager, new Object[] { myEntity });
        //}

        public List<T> FindAll(T myCriteria)
        {
            MethodInfo findAll = this.iManager.GetType().GetMethod("FindAll");
            Object list = findAll.Invoke(this.iManager, new Object[] { myCriteria });

            if (list != null)
            {
                List<T> newList = new List<T>();
                foreach (T entity in ((IList)list))
                    newList.Add(entity);

                return newList;
            }
            else
                return null;
        }

        public T FindById(int Id, Object myCriteria)
        {
            MethodInfo findById = this.iManager.GetType().GetMethod("FindById");
            T entity = (T)findById.Invoke(this.iManager, new Object[] { Id, myCriteria });
            return entity;
        }

        public List<T> FindByParentId(int ParentId, Object myCriteria)
        {
            MethodInfo findByParentId = this.iManager.GetType().GetMethod("FindByParentId").MakeGenericMethod(new Type[] { typeof(int) });
            IList temp = (IList)findByParentId.Invoke(this.iManager, new Object[] { ParentId, this.entityParent, myCriteria });
            if (temp != null)
            {
                List<T> list = new List<T>();
                foreach (T entity in (IList)temp)
                    list.Add(entity);

                return list;
            }
            else
                return null;
        }
    }
}
