using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SagePOS.Manger.Common.Business.Entity ; 

namespace SagePOS.Server.Business.Factory
{
    public interface IEmail:IBaseClass
    {
        int EmailId { get; set; }
        Email EmailObj { get; set; }
        bool Isdefault { get; set; }
    }
}
