using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SagePOS.Manger.Common.Business.Entity;

namespace SagePOS.Server.Business.Factory
{
    public interface IAddress :IBaseClass
    {
        int AddressId { get; set; }
        Address AddressObj { get; set; }
        bool Isdefault { get; set; }
    }
}
