using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Manger.Common.Business.Entity;

namespace SagePOS.Common.DataAccess.IRepository
{
    public interface IEntityLockRepository : IRepository<EntityLock, int>
    {
        void UnLock(int entityId, int? entityValueId);
        void UnLock(int? userId);
        List<EntityLockLite> FindAllLite(CriteriaBase myCriteria);
        void LockWithOutCheck(EntityLock myEntity);
    }
}
