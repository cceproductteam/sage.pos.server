using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.Framework;
using SF.FrameworkEntity;
using SagePOS.Manger.Common.Business.Entity;

namespace SagePOS.Common.DataAccess.IRepository
{
    public interface IDataTypeRepository : IRepository<DataType, int>
    {
    }
}