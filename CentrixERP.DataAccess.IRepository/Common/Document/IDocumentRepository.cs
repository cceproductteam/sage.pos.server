using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SagePOS.Manger.Common.Business.Entity;

namespace SagePOS.Common.DataAccess.IRepository
{
    public interface IDocumentRepository : IRepository<Document, int>
    {
        void UpdateOrder(int DocumentID, int EntityId, int EntityValueId, int OrderID);
    }
}
