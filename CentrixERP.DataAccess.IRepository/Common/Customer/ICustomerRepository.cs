using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;

namespace SagePOS.Server.DataAcces.IRepository
{
    public interface ICustomerRepository : IRepository<Customer, int>
    {
        List<CustomerLite> FindAllLite(CriteriaBase myCriteria);
        CustomerLite FindByIdLite(int Id, CriteriaBase myCriteria);

        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
        double FindPendingAmount(int Id, CriteriaBase myCriteria);
        List<POSIntegrationCustomer> POSIntegrationCustomerFindAll(DateTime? LastSyncDate);
    }
}
