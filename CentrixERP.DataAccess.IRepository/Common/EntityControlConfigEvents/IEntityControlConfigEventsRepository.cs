using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Manger.Common.Business.Entity;

namespace SagePOS.Common.DataAccess.IRepository
{
    public interface IEntityControlConfigEventsRepository : IRepository<EntityControlConfigEvents, int>
    {
        List<EntityControlConfigEvents_Lite> FindAllLite(CriteriaBase myCriteria);
    }
}
