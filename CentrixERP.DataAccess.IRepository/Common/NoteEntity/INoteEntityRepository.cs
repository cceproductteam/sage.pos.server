using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Manger.Common.Business.Entity;

namespace SagePOS.Common.DataAccess.IRepository
{
    public interface INoteEntityRepository : IRepository<NoteEntity, int>
    {
        NoteEntityLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<NoteEntityLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
    }
}
