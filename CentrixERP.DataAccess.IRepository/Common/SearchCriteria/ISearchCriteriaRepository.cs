using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SagePOS.Manger.Common.Business.Entity;
using SF.FrameworkEntity;

namespace SagePOS.Common.DataAccess.IRepository
{
    public interface ISearchCriteriaRepository : IRepository<SearchCriteria, int>
    {
        
    }
}
