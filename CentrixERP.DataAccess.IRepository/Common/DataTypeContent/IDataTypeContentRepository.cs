using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SagePOS.Manger.Common.Business.Entity;

namespace SagePOS.Common.DataAccess.IRepository
{
    public interface IDataTypeContentRepository : IRepository<DataTypeContent, int>
    {
        void ReorderDataTypeContent(string dataTypeContentIDs);
        List<SagePOS.Manger.Common.Business.Entity.DataTypeContentLite> FindAllByIds(string Ids);
    }
}
