using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using Centrix.UM.Business.Entity;
using SagePOS.Manger.Common.Business.Entity;

namespace Centrix.UM.DataAccess.IRepository
{
    public interface ITeamRepository : IRepository<Team, int>
    {
        List<Team> FindAll_RecentlyViewed(int userId, int entityId, TeamCriteria myCriteria);
        List<Team> FindAllTaskTeams(TeamCriteria myCriteria);
        List<Team_Lite> FindAllLite(TeamCriteria myCriteria);
        Team_Lite FindByIdLite(int Id);
        Email FindTeamEmail(int id);
    }
}
