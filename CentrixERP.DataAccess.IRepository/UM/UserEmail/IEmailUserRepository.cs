
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.Framework;
using SF.FrameworkEntity;
using Centrix.UM.Business.Entity;


namespace Centrix.UM.DataAccess.IRepository
{
    public interface IEmailUserRepository : IRepository< EmailUser,int>
    {
    }
}


