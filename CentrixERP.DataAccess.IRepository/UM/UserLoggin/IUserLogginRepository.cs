using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using Centrix.UM.Business.Entity;

namespace Centrix.UM.DataAccess.IRepository
{
    public interface IUserLogginRepository : IRepository<UserLoggin, int>
    {
        void KillUserLogging(UserLogginCriteria userLogginCriteria);
        void KillUserLogging(int userId);
    }
}
