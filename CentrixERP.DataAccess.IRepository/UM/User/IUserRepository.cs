using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using Centrix.UM.Business.Entity;

namespace Centrix.UM.DataAccess.IRepository
{
    public interface IUserRepository : IRepository<User, int>
    {
        User Login(string UserName,string Password);
        List<User> FindAll_RecentlyViewed(int userId, int entityId, UserCriteria myCriteria);
        List<User> FindAllTaskUsers(UserCriteria myCriteria);

        UserLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<UserLite> FindAllLite(CriteriaBase myCriteria);
        UserLite FindUserManager(int Id, CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
        List<RoleUsersLite> FindRoleUsersLite(int RoleId);
        void AddUserRoles(int roleId, string RoleUserIds);
        void AddUserTeams(int teamId, string TeamUserIds);
        List<TeamUsersLite> FindTeamUsersLite(int TeamId);
        bool CheckUserCount();
        User ClientLogin(string UserName, string Password, string AccessCode);
    }
}
