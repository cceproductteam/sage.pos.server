using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;

namespace SagePOS.Server.DataAcces.IRepository
{
    public interface ICloseRegisterRepository : IRepository<CloseRegister, int>
    {
        CloseRegisterLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<CloseRegisterLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
        List<CloseRegiserDetails> GetCloseRegisterDetailsPaymentType(CriteriaBase myCriteria);
        void UpdateSyncStatus(string trasactionNumber, int EntityId);

        List<CloseRegisterInquiryLite> CloseRegisterInquiryResults(string StoreIds, string RegisterIds, DateTime? OpenedDateFrom, DateTime? OpenedDateTo, DateTime? ClosedDateFrom, DateTime? ClosedDateTo);
        List<CloseRegisterInquiryLiteNew> CloseRegisterInquiryResultsNew(string StoreId, string RegisterId, string fromdate, string todate);
        CloseRegisterTotals FindCloseRegisterSalesTotal(int CloseRegisterId);
    }
}
