using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
//using Centrix.POS.Standalone.Client.Business.Entity;
using SagePOS.Server.Business.Entity;

namespace SagePOS.Server.DataAcces.IRepository
{
    public interface ILocalConfigurationRepository : IRepository<LocalConfiguration, int>
    {
        LocalConfigurationLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<LocalConfigurationLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
       
    }
}
