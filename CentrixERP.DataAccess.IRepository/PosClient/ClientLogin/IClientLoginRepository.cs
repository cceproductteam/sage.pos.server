using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SF.FrameworkEntity;
using Centrix.UM.Business.Entity;



namespace SagePOS.Server.DataAcces.IRepository
{
    public interface IClientLoginRepository : IRepository<User, int>
    {
        User ClientLogin(string UserName, string Password, string AccessCode);
    }
}
