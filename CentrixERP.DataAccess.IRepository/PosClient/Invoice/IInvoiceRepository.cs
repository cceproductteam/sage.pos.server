using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;

using SagePOS.Server.Business.Entity;

namespace SagePOS.Server.DataAcces.IRepository
{
    public interface IInvoiceRepository : IRepository<Invoice, int>
    {
        InvoiceLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<InvoiceLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
        Invoice FindLastInvoice(CriteriaBase myCriteria);
        Invoice FindInvoiceByNumber(CriteriaBase myCriteria);
        List<Invoice> FindAllQl(CriteriaBase myCriteria, string phoneNumber);
        List<Invoice> FindByCustomer(CriteriaBase myCriteria);
        List<InvoiceIntegration> InvoiceIntegrationFindAll(CriteriaBase myCriteria);
        void UpdateTransactionStatus(int transactionType, string transactionRef, string transactionNumber, string status, bool IsConvertedToShipment);
        void UpdateSyncStatus(string trasactionNumber, int EntityId);

        List<InvoiceLite> searchRefund(CriteriaBase myCriteria);
        void UpdateInvoiceTransNumber(int invoiceId, int shipmentId, string shipmentNumber);

        List<InvoiceInquiryLite> InvoiceInquiryResults(string StoreIds, DateTime? InvoiceDateFrom, DateTime? InvoiceDateTo);
        List<POSQuantityInQuiryLite> FindInquiryResults(int LocationId, int StoreId, int ItemId);
        List<ICQuantityInQuiryLite> FindICInquiryResults(int LocationId, int StoreId, int ItemId);
        bool CheckParentShipmentValidity();
        void UpdateAccpacInvoiceNumber(string invoiceUniqueNumber, string AccpacInvoiceNum, string AccpacOrderentryNum, string AccpacShipmentNum, string AccpacPrePaymnentNumber, string accpacRefundNumber);

        List<POSDailyRecieptsReport> FindPOSDailySalesReport(string fromdate, string todate, int storeId);
        List<POSDailySalesReport> FindPOSDailySalesPerItemReport(int StoreId, string itemNumber, string InvoiceNumberFrom, string InvoiceNumberTo, string datefrom, string dateTo);
        List<POSDailySalesSummary> FindPOSDailySalesSummaryReport(int StoreId, string itemNumber, string InvoiceNumberFrom, string InvoiceNumberTo, string datefrom, string dateTo);
        List<POSDailySalesReport> InvoiceInquirSalesReport(string fromdate, string todate, string AccpacSyncUniqueNumber, int Posted, string InvoiceNumber);
        List<POSDailyRecieptsReport> FindPOSDailySalesReportSummation(string fromdate, string todate, int storeId);

        List<POSPettyCash> FindPOSPettyCash(string fromdate, string todate, int storeId, int registerId);
        List<POSDaileSalesSummation> FindPOSDaileSalessummation(string fromdate, string todate, int storeId);
        DashBoarBoxes FindDashBoardBoxed();
        List<RegistersSales> FindRegistersSales(int storeId);
        List<StoreSales> FindStoresSales();
    }
}
