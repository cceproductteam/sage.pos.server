using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;

namespace SagePOS.Server.DataAcces.IRepository
{
    public interface IPOSItemCardRepository : IRepository<POSItemCard, int>
    {
        POSItemCardLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<POSItemCardLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
        BOItemCardQl FindItemQl(CriteriaBase myCriteria);
        List<BOItemCardQl> SearchItemQl(CriteriaBase myCriteria);
        BOItemCardQl SearchItemSerial(CriteriaBase myCriteria,bool status);
        string SaveIntegration(List<POSItemCard> itemCardList);
        List<POSItemCard> FindAll();
        
    }
}
