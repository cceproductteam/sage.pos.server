using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;

namespace SagePOS.Server.DataAcces.IRepository
{
    public interface IQuickKeysRepository : IRepository<QuickKeys, int>
    {
        QuickKeysLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<QuickKeysLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
    }
}
