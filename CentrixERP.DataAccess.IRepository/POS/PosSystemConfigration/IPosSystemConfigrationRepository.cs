using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;

namespace SagePOS.Server.DataAcces.IRepository
{
    public interface IPosSystemConfigrationRepository : IRepository<PosSystemConfigration, int>
    {
        List<PosSystemConfigrationLite> FindAllLite(CriteriaBase myCriteria);
    }
}
