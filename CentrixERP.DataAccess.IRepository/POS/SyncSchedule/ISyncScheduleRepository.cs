using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;

namespace SagePOS.Server.DataAcces.IRepository
{
    public interface ISyncScheduleRepository : IRepository<SyncSchedule, int>
    {
        SyncScheduleLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<SyncScheduleLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
        List<SyncSchedule> FindAllToSync(int status);
        void addNewSchedule(int type, int userId);
        NewSyncSchedule checkSynchronizationStatus();
        SyncScheduleNew FindsyncSchedule(int status);
        void UpdateSyncScheduleStatus(int Id);
        List<SyncScheduleDetails> getSyncScheduleDetails(string SyncLogNumber);
        List<SyncLogEntityDetails> getSyncScheduleentityDetails(string entity, int action, string logNumber);
        string getLastSyncNumber(int action);


    }
}
