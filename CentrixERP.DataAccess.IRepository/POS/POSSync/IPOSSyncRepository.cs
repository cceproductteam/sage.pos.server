﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;

namespace SagePOS.Server.DataAcces.IRepository
{

    public interface IPOSSyncRepository : IRepository<POSSync, int>
    {
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
        void POSSyncData(Object[] data);
        void SyncPOSData(POSSync POSData);
    }

}
