using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;

namespace SagePOS.Server.DataAcces.IRepository
{
    public interface ICustomFildRepository : IRepository<CustomFild, int>
    {
        CustomFildLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<CustomFildLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
        void UpdateSyncStatus(string trasactionNumber, int EntityId);
    }
}
