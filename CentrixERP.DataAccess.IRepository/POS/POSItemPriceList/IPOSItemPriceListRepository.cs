using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;

namespace SagePOS.Server.DataAcces.IRepository
{
    public interface IPOSItemPriceListRepository : IRepository<POSItemPriceList, int>
    {
        POSItemPriceListLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<POSItemPriceListLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);

        string SaveIntegration(List<POSItemPriceList> ItemPriceList); 
    }
}
