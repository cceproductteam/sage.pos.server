using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;

using SagePOS.Server.Business.Entity;
namespace SagePOS.Server.DataAcces.IRepository
{
    public interface IAccpacSyncScheduleRepository : IRepository<AccpacSyncSchedule, int>
    {
        AccpacSyncScheduleLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<AccpacSyncScheduleLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);

        List<AccpacSyncLogLite> FindAllLiteLog(string datefrom, string dateto, string AccpacSyncUniqueNumber);

    }
}
