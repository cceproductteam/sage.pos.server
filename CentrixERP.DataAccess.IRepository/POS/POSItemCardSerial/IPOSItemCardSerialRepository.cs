using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;


namespace SagePOS.Server.DataAcces.IRepository
{
    public interface IPOSItemCardSerialRepository : IRepository<POSItemCardSerial, int>
    {
        POSItemCardSerialLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<POSItemCardSerialLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
        string SaveIntegration(List<POSItemCardSerial> SerialList);
    }
}
