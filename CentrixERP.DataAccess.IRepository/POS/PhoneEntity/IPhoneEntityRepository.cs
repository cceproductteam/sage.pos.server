using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;
using SagePOS.Manger.Common.Business.Entity;

namespace SagePOS.Server.DataAcces.IRepository
{
    public interface IPhoneEntityRepository : IRepository<PhoneEntity, int>
    {
        PhoneEntityLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<PhoneEntityLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
    }
}
