using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;

namespace SagePOS.Server.DataAcces.IRepository
{
    public interface IPOSPaymentCodeRepository : IRepository<POSPaymentCode, int>
    {
        POSPaymentCodeLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<POSPaymentCodeLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
        
    }
}
