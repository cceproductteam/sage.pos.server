using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;
using SagePOS.Server.Business.Entity;

namespace SagePOS.Server.DataAcces.IRepository
{
    public interface IPersonRepository : IRepository<Person, int>
    {
        PersonLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<PersonLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
        List<PersonLite> GetAllPersonBirthOfDateFindAll(PersonCriteria myCriteria);
        List<PersonLite> GetAllPersonAnniversaryFindAll(PersonCriteria myCriteria);
        void UpdateSyncStatus(string trasactionNumber, int EntityId);
    }
}
