using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SagePOS.Server.Configuration
{
    public class URLEnums
    {
        public enum Entity
        {
            Company = 1,
            Person = 2,
            Project = 3,
            Role = 4,
            User = 5,
            Team =6,
            DataTypeContent = 7,
            ActivityLookup = 8,
            Configration= 9,
            Template= 10,
            TemplateType = 11,
            SentNotifications = 12,
            SendNotifications = 13,
            EmailConfigration = 14,
            DuplicationRule =15,
            Opportunity =16 ,
            Lead =17,
            UserAccount = 18,
            Credits=19,
            CreditsHistory=20,
            PriceList=21,
            Currency=22,
            Category = 23,
            UserHelp =24,
            CategoryContent=25,
            DocumentFolder=26,
            Product=27,
            Quotation=28,
            Group=29,
            CompanyRelationShip=30,
            CompanyContracts=31,
            CompanySites = 32,
            Common = 33,
            UserPermissions = 34,
            QuotationItem = 35,
            CompanyPersons =36,
            Phone = 37,
            Document = 38,
            Note = 39,
            Address=40,
            Email= 41,
            Communication=42,
           CompanyOpportunity = 43,
            ListContactCredit = 44,
            ListClearanceLevel = 45,
            GroupSearchCriteriaList = 46,
            PersonOpportunity = 47,
            Contracts = 48,
            Sites = 49,
            ProductPriceList=50,
            RealtedOpportunity = 51,
            CompanyFinancialInformation=52,
            SiteContacts=53,
            ClientCareCase = 54,
            ClientCareProgress = 55,
            PersonClientcareCaseType = 56,
            SelfServiceUser = 57,
            SelfServiceSecurityGroup = 58
         
           

        }
        
        public class Action
        {
            public enum Default
            {
                Add = 1,
                Edit = 2,
                List = 3,
                View = 4,
                Delete = 5
            }

            public enum Extended
            {
                SendNotifications = 1,
                NavigationMenu = 2,
                Calender = 3,
                Configration = 4,
                Logout = 5,
                Login = 6,
                ChangeLang = 7,
                Delete = 8,
                Search = 9,
                MainStyle = 10,
                IncStyle = 11,
                LoginStyle = 12,
                scheduleStyle = 13,
                JqureyJscrollpane = 14,
                Default = 15,
                Style = 16,
                UserPersonPermissions = 17,
                UserCompanyPermissions = 18,
                NotificationStyle = 19,
                TreeStyle = 20,
                SalesStyle = 21,
                JQueryUIAllCSS = 22,
                JQueryUIDemo = 23,
                SpryTabbedPanels = 24,
                ERPStyle=25,
                Autocomplete=26
            }
        }
    }
}