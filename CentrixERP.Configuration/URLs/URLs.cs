using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using System.Configuration;
using System.Xml.Linq;
using SF.Framework;

using System.Web;

namespace SagePOS.Server.Configuration
{
    public static class URLs
    {
        public static string GetStyleURL(URLEnums.Action.Extended action, string lang)
        {
            URLPermission url = GetURL(Enums_S3.Configuration.Module.Common.ToString(), action.ToString());
            if (!string.IsNullOrEmpty(lang))
                return url.URL.Replace(ConfigurationManager.AppSettings["LangDelimiter"], lang);
            else
                return url.URL;
        }

        public static string GetURL(Enums_S3.Configuration.Module module, URLEnums.Action.Default action, string keys)
        {
            URLPermission url = GetURL(module.ToString(), action.ToString());
            if (!string.IsNullOrEmpty(keys))
            {
                url.URL += keys.StartsWith("?") ? keys : "?" + keys;
            }
            return url.URL;
        }

        public static string GetURL(Enums_S3.Configuration.Module module, URLEnums.Action.Extended action, string keys)
        {
            URLPermission url = GetURL(module.ToString(), action.ToString());

            if (!string.IsNullOrEmpty(keys))
            {
                url.URL += keys.StartsWith("?") ? keys : "?" + keys;
            }

            return url.URL;
        }

        public static string GetURL(URLEnums.Entity entity, URLEnums.Action.Default action, Dictionary<string, string> dictionary, ref int? permissionId)
        {
            URLPermission url = GetURL(entity.ToString(), action.ToString());

            if (dictionary != null && dictionary.Count != 0)
            {
                url.URL += "?";
                foreach (KeyValuePair<string, string> item in dictionary)
                {
                    url.URL += string.Format("{0}={1}&", item.Key, item.Value);
                }
                url.URL = url.URL.TrimEnd('&');
            }
            if (permissionId != null)
                permissionId = url.PermissionId;
            return url.URL;
        }

        public static string GetURL(URLEnums.Entity entity, URLEnums.Action.Extended action, Dictionary<string, string> dictionary, ref int? permissionId)
        {
            URLPermission url = GetURL(entity.ToString(), action.ToString());

            if (dictionary != null && dictionary.Count != 0)
            {
                url.URL += "?";
                foreach (KeyValuePair<string, string> item in dictionary)
                {
                    url.URL += string.Format("{0}={1}&", item.Key, item.Value);
                }
                url.URL = url.URL.TrimEnd('&');
            }
            if (permissionId != null)
                permissionId = url.PermissionId;
            return url.URL;
        }

        public static string GetURL(URLEnums.Entity entity, URLEnums.Action.Default action, string key, string value, ref int? permissionId)
        {
            URLPermission url = GetURL(entity.ToString(), action.ToString());
            url.URL += string.Format("?{0}={1}", key, value);
            if (permissionId != null)
                permissionId = url.PermissionId;
            return url.URL;
        }

        public static string GetURL(URLEnums.Entity entity, URLEnums.Action.Extended action, string key, string value, ref int? permissionId)
        {
            URLPermission url = GetURL(entity.ToString(), action.ToString());
            url.URL += string.Format("?{0}={1}", key, value);
            if (permissionId != null)
                permissionId = url.PermissionId;
            return url.URL;
        }

        public static string GetURL(URLEnums.Entity entity, URLEnums.Action.Default action, ref int? permissionId)
        {
            URLPermission url = GetURL(entity.ToString(), action.ToString());
            if (permissionId != null)
                permissionId = url.PermissionId;
            return url.URL;
        }

        public static string GetURL(URLEnums.Entity entity, URLEnums.Action.Extended action, ref int? permissionId)
        {
            URLPermission url = GetURL(entity.ToString(), action.ToString());
            if (permissionId != null)
                permissionId = url.PermissionId;
            return url.URL;
        }

        private static URLPermission GetURL(string entity, string mode)
        {
            URLPermission url = new URLPermission();
            string xmlPath = ConfigurationManager.AppSettings["EntityURLs"].ToString();

            if (!System.IO.File.Exists(xmlPath))
                throw new Exception("The Entity URLs XML File is Not Exists!!!!!!!!");

            XDocument doc = XDocument.Load(xmlPath);
            var link = from XElement element in doc.Elements("Configuration").Elements("URLs").Elements("Entity")
                       where element.Attribute("Name").Value == entity.ToString()
                       select element;

            if (link.Count() != 0)
            {
                foreach (XElement element in link.Elements("URL"))
                {
                    if (element.Attribute("Mode").Value == mode.ToString())
                    {
                        url = new URLPermission();
                        url.URL = element.Value;
                        url.PermissionId = Convert.ToInt32(element.Attribute("PermissionId").Value.ToString());
                        break;
                    }
                }
            }
            return url;
        }

        public static string Phones(int parentId, string moduleKey)
            {
                const string parentIDKey = "id";
                const string moduleIDKey = "key";
                Dictionary<string, string> dictionary = new Dictionary<string, string>();
                dictionary.Add(parentIDKey, parentId.ToString());
                dictionary.Add(moduleIDKey, moduleKey);

                //return GetPageURL(Enums_S3.Configuration.Page.Phones, dictionary);
                return URLs.GetURL(Enums_S3.Configuration.Module.Phone, URLEnums.Action.Default.List, "id=" + parentId.ToString() + "&key=" + moduleKey);
            }
        public static string Addresses(int parentId, string moduleKey)
            {
                const string parentIDKey = "id";
                const string moduleIDKey = "key";
                Dictionary<string, string> dictionary = new Dictionary<string, string>();
                dictionary.Add(parentIDKey, parentId.ToString());
                dictionary.Add(moduleIDKey, moduleKey);
               // return GetPageURL(Enums_S3.Configuration.Page.Address, dictionary);
                return URLs.GetURL(Enums_S3.Configuration.Module.Address, URLEnums.Action.Default.List, "id=" + parentId.ToString() + "&key=" + moduleKey);
            }

       public static string Email(int parentId, string moduleKey)
            {
                const string parentIDKey = "id";
                const string moduleIDKey = "key";
                Dictionary<string, string> dictionary = new Dictionary<string, string>();
                dictionary.Add(parentIDKey, parentId.ToString());
                dictionary.Add(moduleIDKey, moduleKey);

               // return GetPageURL(Enums_S3.Configuration.Page.Email, dictionary);
                return URLs.GetURL(Enums_S3.Configuration.Module.Email, URLEnums.Action.Default.List, "id=" + parentId.ToString() + "&key=" + moduleKey);
            }
       public static string Notes(int parentId, string moduleKey)
            {
                const string parentIDKey = "id";
                const string ModuleKey = "key";

                Dictionary<string, string> dictionary = new Dictionary<string, string>();
                dictionary.Add(parentIDKey, parentId.ToString());
                dictionary.Add(ModuleKey, moduleKey);
                //return GetPageURL(Enums_S3.Configuration.Page.Notes, dictionary);
                return URLs.GetURL(Enums_S3.Configuration.Module.Note, URLEnums.Action.Default.List, "id=" + parentId.ToString() + "&key=" + moduleKey);
            }

       public static string GetEntityNavigationURLs(Enums_S3.Configuration.Module module, URLEnums.Action.Default action, string ModuleKey, int EntityID, string sortField, int sortDirection, int flag, string nextIds, string previousIds, string searchCriteria, int filter)
       {
           URLPermission url = GetURL(module.ToString(), action.ToString());
           string queryString = string.Format("?ids={0}&PreviousIds={1}&flag={2}&SearchCriteria={3}&filter={4}&sortDirection={5}&field={6}&{7}={8}", nextIds, previousIds, flag, searchCriteria, filter, sortDirection, sortField, ModuleKey, EntityID);
           return url.URL + queryString;
       }

       public static string GetEntityNavigationURLs(Enums_S3.Configuration.Module module, URLEnums.Action.Default action, string ModuleKey, int EntityID, int flag, string nextIds, string previousIds, string searchCriteria)
       {
           URLPermission url = GetURL(module.ToString(), action.ToString());
           string queryString = string.Format("?ids={0}&PreviousIds={1}&flag={2}&SearchCriteria={3}&{4}={5}", nextIds, previousIds, flag, searchCriteria, ModuleKey, EntityID);
           return url.URL + queryString;



       }

       public static string ConstructPageOppositeLangURL(Enums_S3.Configuration.Language lang, Enums_S3.Configuration.Language oppLang)
       {
           /// TODO : REMOVE FROM HERE
           string URL = HttpContext.Current.Request.Url.AbsoluteUri;
           if (URL.ToLower().IndexOf("lang=") != -1)
           {
               URL = URL.Replace(lang.ToString(), oppLang.ToString());
           }
           else
           {
               if (URL.IndexOf("?") != -1)
                   URL = URL + "&lang=" + oppLang;
               else
                   URL = URL + "?lang=" + oppLang;
           }
           return URL;
       }

       public static void MarkViewed(int id, Enums_S3.Configuration.Module entityId, int loggedInId)
       {
           //SagePOS.Manger.Common.Business.Entity.RecentView recentView = new SagePOS.Manger.Common.Business.Entity.RecentView();
           //IRecentViewManager recentViewManager = IoC.Instance.Resolve<IRecentViewManager>();
           //recentView.UserId = loggedInId;
           //recentView.Id = id;
           //recentView.EntityId = (int)entityId;
           //recentViewManager.Save(recentView);
       }

       public static List<TEntity> SortEntity<TEntity>(List<TEntity> EntityList, string field, Enums_S3.SortDirection direction)//new
       {
           List<TEntity> result = EntityList;

           if (result != null)
           {
               if (direction == Enums_S3.SortDirection.desc)
               {
                   result = (from TEntity el in EntityList
                             orderby
                                 (el.GetType().GetProperty(field).GetValue(el, null))
                                       descending
                             select el).ToList<TEntity>();

               }
               else
               {
                   result = (from TEntity el in EntityList
                             orderby
                                 (el.GetType().GetProperty(field).GetValue(el, null))
                                 ascending
                             select el).ToList<TEntity>();
               }
           }

           return result;
       }

    }


    public struct URLPermission
    {
        public string URL { get; set; }
        public int PermissionId { get; set; }
    }
}