using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SagePOS.Server.Configuration
{
    public class DataTypeContectEnumerations
    {

        public enum DataType
        {
            Type = 1,
            Segment = 2,
            Source = 3,
            Salutation = 4,
            Gender = 5,
            MaritalStatus = 6,
            Religion = 7,
            Department = 8,
            Position = 9,
            CompanyCategory = 10,
            MainProductInterest = 11,
            DecisionTimeframe = 12,
            LeadStage = 13,
            LeadStatus = 14,
            Priority = 15,
            Rating = 16,
            AnnualRevenues = 17,
            NoOfEmployee = 18,
            Industry = 19,
            OpportunityStage = 20,
            OpportunityStatus = 21,
            OpportunityType = 22,
            Currency = 23,
            RealtedOpportunityType = 24,
            ProjectProduct = 28,
            ProjectStatus = 29,
            TransactionType = 30,
            ClassType = 31,
            DateMatched = 32,
            RateOperation = 33,
            PrizesTypes = 34,
            Module = 35,
            FilterType = 36,
            PersonRelations = 37,
            scheduleTaskType = 38,
            SegmentDelimeter = 39,
            ProductAttribute = 42,
            NormalBalance = 40,
            AccountType = 41,
            ProhibitedStatus = 42,
            BatchListStatus = 43,
            SourceLedger = 44,
            DefaultPostingDate = 45,
            DefaultTransactionType = 46,
            DefaultOrderofOpenDocuments = 47,
            IncludePendingTransactionsType = 48,
            CheckForDuplicatedChecks = 49,
            SortChecksBy = 50,
            NumberingDocumentType = 51,
            BatchSequenceDocumentType = 52,
            PaymentType = 53,
            DueDateType = 54,
            CalculateBaseforDiscountWithTax = 55,
            TaxBaseType = 56,
            ARAccountType = 57,
            DeliveryMethod = 58,
            CustomerType = 59,
            CheckforDuplicatePOs = 60,
            InvoiceDocumentType = 61
        }

        public enum Stage
        {
            Assigned = 90
        }

        public enum Status
        {
            StatusInProgress = 95
        }

        public enum DefualtOpportunityStage
        {
            Lead = 118

        }

        public enum DefualtOpportunityStatus
        {
            InProgress = 126
        }

        public enum DefualtRating
        {
            Rating = 101
        }

        public enum Gender
        {
            Male = 31,
            Female = 32
        }

        public enum TransactionType
        {
            Purchase = 161,
            Sale = 162
        }

        public enum ClassType
        {
            Vendor = 163,
            Customer = 164,
            Item = 165
        }

        public enum OpportunityStage
        {
            Lead = 118,
            Suspended = 119,
            Prospect = 120,
            Quoted = 121,
            QuoteSigned = 122,
            OrderRecived = 123,
            Closed = 125
        }

        public enum Frequency
        {
            OnOff = 273,
            Recurring = 276
        }

        public enum ProductStatus
        {
            Active = 280,
            Inactive = 281
        }

        public enum QuotationStatus
        {
            Generated = 297
        }

        public enum QuotataionCategory
        {
            NewSale = 292,
            Renewal = 293
        }

        public enum CompanyType
        {
            ResellerCompany = 237,
            CustomerCompany = 3,
            VendorCompany = 6
        }

        public enum AccessDevice
        {
            PC = 336,
            Mobile = 337
        }

        public enum AccessDeviceType
        {
            Windows = 345,
            MAC = 346
        }

        public enum ClientCareStage
        {
            CaseClosed = 328,
            CaseInvestigating = 329,
            CaseSolved = 330,
            CaseOnhold = 331,
            CaseEscalated = 332

        }

        public enum Priorityvalues
        {
            Extreme = 347
        }

        public enum SystemConfigurationWebKeyType
        {
            WebConfigKey = 1,
            ServiceURL = 2,
            PageURL = 3,
            HTMLTemplateURL = 4
        }

        public enum BatchListType
        {
            InvoiceBatchList = 1,
            ReceiptBatchList = 2,
            AdjustmentBatchList = 3,
            RefundBatchList = 4
        }
        public enum DateMatched
        {
            earlier = 32,
            late = 34,
            current = 33
        }
        public enum BatchListStatus { Open = 51, Posted = 50, Deleted = 49 }
        public enum BatchPostingErrors
        {
            EmptyBatch = 10,
            NotBalanceBatch = 11,
            PostedBatch = 12,
            ReadyToPost = 13,
            DeletedBatch = 14,
            EntryInLockedPeriod = 15,
            EntryBeforOldesetYear = 16,
            EntryAccountInActive = 17,
            AllowPostingToPreviousYear = 18,
            EntryInFutureYear = 19,
            NoError = 20,
            EntryHasNoPeriod = 21,
            AdjustmentPeriodLocked=22
        }


        public enum BatchTransactionsErrors
        {
            EmptyBatch = 910,
            NotBalanceBatch = 911,
            PostedBatch = 912,
            ReadyToPost = 913,
            DeletedBatch = 914,
            EntryInLockedPeriod = 915,
            EntryBeforOldesetYear = 916,
            EntryAccountInActive = 917,
            AllowPostingToPreviousYear = 918,
            EntryInFutureYear = 919,
            NoError = 920,
            EntryHasNoPeriod = 921,
            AdjustmentPeriodLocked = 922
        }

        public enum SourceLedger
        {

            CM = 99,
            GL=48
        }

        public enum BankEntryTransactionType { Withdrawel = 101, Deposit = 100 }

        public enum ReconciliationStatus { OutStanding = 105, Cleared = 106 }
        public enum ReconciliationReason { CreditCardCharges = 107, WriteOff = 108 }
        public enum AccountNormalBalance
        {
            Debit = 40,
            Credit = 41
        }
        public enum AccountType { Allow = 45, prohibted = 46 }
        public enum AccountTypeId { IncomeStatment = 43 }

        public enum SourceCode
        {
            CMBE = 1,
            GLJE = 11,
            CMTF = 12,
            CMBR = 14,
            CRCN = 17,
            CRCT = 18,
            CRPY = 19,
            CRRR = 20,
            GLCL = 21
        }

    }
}