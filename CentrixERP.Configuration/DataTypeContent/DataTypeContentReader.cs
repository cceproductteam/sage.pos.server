using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Configuration;

namespace SagePOS.Server.Configuration
{
    public class DataTypeContentReader
    {
        private Dictionary<string, int> dataTypeContent;
        private Dictionary<string, int> dataType;

        private static DataTypeContentReader instance;

        public static DataTypeContentReader Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new DataTypeContentReader();
                }
                return instance;
            }
        }

        private DataTypeContentReader()
        {
            GetURL();
        }

        public int GetDataTypeContentValue(Enum contentEnum)
        {
            return Convert.ToInt32(dataTypeContent[contentEnum.ToString()]);
        }

        public int GetDataTypeValue(DataTypeContectEnumerations.DataType contentEnum)
        {
            return Convert.ToInt32(dataType[contentEnum.ToString()]);
        }

        private void GetURL()
        {
            string xmlPath = ConfigurationManager.AppSettings["DataTypeContentValues"].ToString();

            if (!System.IO.File.Exists(xmlPath))
                throw new Exception("The Data Type Content Values XML File is Not Exists!!!!!!!!");

            XDocument doc = XDocument.Load(xmlPath);
            var DataType = from XElement element in doc.Elements("DataType").Elements("DataTypes")
                           select element;

            if (DataType.Count() > 0)
            {
                dataType = new Dictionary<string, int>();
                foreach (XElement element in DataType.Elements())
                {
                    dataType.Add(element.Attribute("Name").Value, Convert.ToInt32(element.Attribute("Value").Value));
                }
            }

            var DataTypeContent = from XElement element in doc.Elements("DataType").Elements("DataTypeContects")
                                  select element;

            if (DataTypeContent.Count() > 0)
            {
                dataTypeContent = new Dictionary<string, int>();
                foreach (XElement element in DataTypeContent.Elements())
                {
                    dataTypeContent.Add(element.Attribute("Name").Value, Convert.ToInt32(element.Attribute("Value").Value));
                }
            }
        }
    }
}