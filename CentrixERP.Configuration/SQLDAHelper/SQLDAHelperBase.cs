﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



namespace Centrix.POS.Standalone.Client.Configuration
{
    public abstract class SQLDAHelperBase
    {
        protected int commandTimeOut;
        private string connectionString;
        protected string unPooledConnectionString;
        protected string originalConnectionString;
        protected bool IsDbDebugEnabled;

        protected string ConnectionString
        {
            get { return connectionString; }
            set { connectionString = value; }
        }

        public SQLDAHelperBase()
        {
            try
            {
                commandTimeOut = GetCommandTimeOut();
                // connectionString = Cryption.Decrypt.String(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"].ToString(), Cryption.Algorithm.AES);
                connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DBConn"].ConnectionString;
                IsDbDebugEnabled = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["DBDebug"]);

                originalConnectionString = connectionString;

                if (connectionString.IndexOf("Connect Timeout") == 0)
                {
                    connectionString += ";Connect Timeout=10";
                }

                if (connectionString.IndexOf("Application Name") == 0)
                {
                    connectionString += ";Application Name=" + Environment.MachineName + "\\" + System.Configuration.ConfigurationManager.AppSettings["AppName"].ToString();
                }

                if (connectionString.IndexOf("Pooling") > -1)
                {
                    string[] connectionOption = connectionString.Split(';');
                    string sOption = "";
                    foreach (string option in connectionOption)
                    {
                        sOption = option;
                        if (option.IndexOf("Pooling=") > -1)
                        {
                            sOption = ";Pooling=false";
                        }
                        unPooledConnectionString += sOption + ";";
                    }
                    connectionString += ";Application Name=" + Environment.MachineName + "\\" + System.Configuration.ConfigurationManager.AppSettings["AppName"].ToString();
                }
            }
            catch (Exception ex)
            {
                //Logging.Logger.Instance.LogExecption(ex);
            }
        }


        public int GetCommandTimeOut()
        {
            return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["DatabaseCommandTimeout"]);
        }

    }
}
