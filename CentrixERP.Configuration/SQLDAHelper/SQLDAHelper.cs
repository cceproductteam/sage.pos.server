﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;


namespace Centrix.POS.Standalone.Client.Configuration
{
    public class SQLDAHelper : SQLDAHelperBase
    {
        private static int PoolHitCount = 0;

        public SQLDAHelper()
        {
            cmd = new SqlCommand();
        }

        static bool isProcessing;
        //public static SQLDAHelper Instance = new SQLDAHelper();

        //to make the Instance specific for each request
        //public static SQLDAHelper Instance
        //{
        //    get
        //    {
        //        IDictionary items = HttpContext.Current.Items;
        //        if (!items.Contains("TheInstance"))
        //        {
        //            items["TheInstance"] = new SQLDAHelper();
        //        }
        //        return items["TheInstance"] as SQLDAHelper;
        //        //return new SQLDAHelper();
        //    }
        //}

        private SqlCommand cmd;

        private void tryToFreeResources()
        {

            if (isProcessing)
                return;
            isProcessing = true;
            try
            {
                freeSQLResources();
            }
            catch (Exception ex)
            {
                //Logging.Logger.Instance.LogExecption(ex);
            }
            finally
            {
                isProcessing = false;
            }
        }

        private void freeSQLResources()
        {
            System.Data.SqlClient.SqlConnection.ClearAllPools();
            GC.Collect();
        }

        public SqlConnection GetConnection(bool open)
        {
            //cmd.Parameters.Clear();
            //tryToFreeResources();
            //cmd = new SqlCommand();
            if (open)
            {
                SqlConnection mySQLConnect = GetConnection(false);
                try
                {
                    mySQLConnect.Open();
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("login failed"))
                    {
                        //Logging.Logger.Instance.LogExecption(ex);
                    }
                    tryToFreeResources();


                    if (mySQLConnect.ConnectionString.Contains("Pooling"))
                    {
                        //Logging.Logger.Instance.LogInformation("The Pooled Connection fail to Connect we will try the unpooled connection.");
                        mySQLConnect.ConnectionString = unPooledConnectionString;
                        try
                        {
                            mySQLConnect.Open();
                        }
                        catch (Exception ex1)
                        {
                            //Logging.Logger.Instance.LogInformation("The UnPooled Connection fail to Connect.");
                            //Logging.Logger.Instance.LogExecption(ex1);
                        }
                    }

                }
                return mySQLConnect;
            }
            else
            {
                return new SqlConnection(ConnectionString);
            }
        }

        public int ExcuteNonQuery(SqlCommand cmd, bool useTransaction)
        {

            if (useTransaction)
                cmd.Transaction = cmd.Connection.BeginTransaction();

            //if (IsDbDebugEnabled)
                //Logging.Logger.Instance.LogInformation(cmd.CommandText);
            try
            {
                int effectedRows = cmd.ExecuteNonQuery();
                if (cmd.Transaction != null)
                {
                    cmd.Transaction.Commit();
                    //if (IsDbDebugEnabled)
                        //Logging.Logger.Instance.LogInformation(cmd.CommandText + "IS COMMITED.");
                }
                return effectedRows;
            }
            catch (Exception ex)
            {
                if (cmd.Transaction != null)
                {
                    cmd.Transaction.Rollback();
                    //if (IsDbDebugEnabled)
                        //Logging.Logger.Instance.LogInformation(cmd.CommandText + "IS RollBack.");
                }
                //Logging.Logger.Instance.LogExecption(ex);
                throw ex;
                
            }
            finally
            {
                cmd.Connection.Close();
            }

        }

        public int ExcuteNonQuery(string spName, SqlConnection conn, bool useTransaction)
        {



            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = spName;
            cmd.CommandTimeout = this.GetCommandTimeOut();
            cmd.Connection = conn;

            cmd.Connection.Open();

            SqlCommand cmd1 = new SqlCommand("SET NOCOUNT OFF", conn);
            cmd1.ExecuteNonQuery();


            if (useTransaction)
                cmd.Transaction = cmd.Connection.BeginTransaction();

          //  if (IsDbDebugEnabled)
                //Logging.Logger.Instance.LogInformation(cmd.CommandText);
            try
            {
                int effectedRows = cmd.ExecuteNonQuery();
                if (effectedRows == -1)
                    //throw new Exceptions.RecordExsitsException("");
                if (effectedRows == 0)
                    //throw new Exceptions.RecordNotAffected("");

                if (cmd.Transaction != null)
                {
                    cmd.Transaction.Commit();
                    //if (IsDbDebugEnabled)
                        //Logging.Logger.Instance.LogInformation(cmd.CommandText + "IS COMMITED.");
                }
                return effectedRows;
            }
            catch (Exception ex)
            {
                if (cmd.Transaction != null)
                {
                    cmd.Transaction.Rollback();
                    //if (IsDbDebugEnabled)
                        //Logging.Logger.Instance.LogInformation(cmd.CommandText + "IS RollBack.");
                }
                //Logging.Logger.Instance.LogExecption(ex);
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection.Dispose();
                cmd.Dispose();
            }

        }

        public SqlDataReader ExcuteReader(string spName, SqlConnection conn)
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = spName;
            cmd.CommandTimeout = this.GetCommandTimeOut();
            cmd.Connection = conn;

            conn.Open();
            //if (IsDbDebugEnabled)
                //Logging.Logger.Instance.LogInformation(cmd.CommandText);
            try
            {
                //return cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
                PoolHitCount += 1;
                if (PoolHitCount == Convert.ToInt16(ConfigurationManager.AppSettings["PoolCountToReset"].ToString()))
                {

                    PoolHitCount = 0;
                    SqlConnection.ClearPool(conn);
                }
                return cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);

            }
            catch (Exception ex)
            {
                //Logging.Logger.Instance.LogExecption(ex);
                cmd.Connection.Close();
                cmd.Connection.Dispose();
                cmd.Dispose();
                throw ex;
            }
            finally
            {
                //cmd.Connection.Close();
            }

        }

        public SqlDataReader ExcuteReader(SqlCommand cmd)
        {

            //if (IsDbDebugEnabled)
                //Logging.Logger.Instance.LogInformation(cmd.CommandText);
            try
            {
                return cmd.ExecuteReader();

            }
            catch (Exception ex)
            {
                //Logging.Logger.Instance.LogExecption(ex);
                cmd.Connection.Close();
                cmd.Connection.Dispose();
                cmd.Dispose();
                throw ex;
            }
            finally
            {

            }

        }

        public void AddInParameter(string name, SqlDbType dbType, object value)
        {
            SqlParameter param;
            param = cmd.Parameters.AddWithValue(name, value);
            param.SqlDbType = dbType;
        }

        public void AddOutParameter(string name, SqlDbType dbType)
        {
            SqlParameter param;
            param = new SqlParameter(name, dbType);
            param.Direction = System.Data.ParameterDirection.Output;
            cmd.Parameters.Add(param);
        }

        public void AddOutParameter(string name, SqlDbType dbType, int size)
        {
            SqlParameter param;
            param = new SqlParameter(name, dbType, size);

            param.Direction = System.Data.ParameterDirection.Output;
            cmd.Parameters.Add(param);
        }

        public object GetOutParamValue(string Name)
        {
            return cmd.Parameters[Name].Value;
        }

        public static object Convert_IntTODB(int value)
        {
            if (value == -1 || value == 0)
                return DBNull.Value;
            else
                return value;
        }

        public static int Convert_DBTOInt(object value)
        {
            if (value == DBNull.Value)
                return -1;
            else
                return Convert.ToInt32(value);
        }

        public static object Convert_DoubleTODB(double value)
        {
            if (value == -1 || value == 0)
                return DBNull.Value;
            else
                return value;
        }

        public static double Convert_DBTODouble(object value)
        {
            if (value == DBNull.Value)
                return -1;
            else
                return Convert.ToDouble(value);
        }

        public static object Convert_DateTiemTODB(DateTime? value)
        {
            if (!value.HasValue)
                return DBNull.Value;
            else
                return value;
        }

        public static bool HasColumn(SqlDataReader reader, string columnName)
        {
            try
            {
                return reader.GetOrdinal(columnName) >= 0;
            }
            catch (IndexOutOfRangeException)
            {
                return false;
            }
        }



        public SqlConnection SetConnection(string connectionString, bool open)
        {
            //cmd.Parameters.Clear();
            //tryToFreeResources();
            //cmd = new SqlCommand();

            SqlConnection conn = GetConnection(true);
            conn.Dispose();
            conn.Close();

            if (open)
            {
                SqlConnection mySQLConnect = SetConnection(connectionString, false);
                try
                {
                    mySQLConnect.Open();
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("login failed"))
                    {
                        //Logging.Logger.Instance.LogExecption(ex);
                    }
                    tryToFreeResources();


                    if (mySQLConnect.ConnectionString.Contains("Pooling"))
                    {
                        //Logging.Logger.Instance.LogInformation("The Pooled Connection fail to Connect we will try the unpooled connection.");
                        mySQLConnect.ConnectionString = connectionString;
                        try
                        {
                            mySQLConnect.Open();
                        }
                        catch (Exception ex1)
                        {
                            //Logging.Logger.Instance.LogInformation("The UnPooled Connection fail to Connect.");
                            //Logging.Logger.Instance.LogExecption(ex1);
                        }
                    }

                }
                return mySQLConnect;
            }
            else
            {
                return new SqlConnection(connectionString);
            }
        }

    }
}
