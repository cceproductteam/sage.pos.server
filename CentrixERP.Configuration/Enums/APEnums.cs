﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SagePOS.Server.Configuration
{
    public class APEnums
    {
        public enum InvoiceType
        {
            None = 0,
            Invoice = 79,
            DebitNote = 80,
            CreditNote = 81
        }

        public enum BatchListStatus
        {
            None = 0,
            Deleted = 49,
            Posted = 50,
            Open = 51
        }

        public enum EntryEditStatus
        {
            Editable = 1,
            Posted = 2,
            Deleted = 3,
            ReadyToPost = 4
        }

        public enum PaymentType
        {
            None = 0,
            Prepayment = 113,
            Payment = 112,
            ApplyDocument = 114,
            Miscellaneous = 115

        }

        public enum ApDocumentType
        {
            None = 0,
            Invoice = 1,
            DebitNote = 2,
            CreditNote = 3,
            Payment = 4,
            Prepayment = 5,
            UnappliedCash = 6,
            Adjustment = 7
        }

        public enum SourceLedger
        {
            CR = 111
        }

        public enum SourceCode
        {
            CMBE = 1,
            ARIN = 2,
            ARDN = 3,
            ARCN = 4,
            ARRC = 5,
            ARMC = 6,
            ARAP = 7,
            ARPR = 8,
            ARRF = 9,
            ARAD = 10,
            GLJE = 11,
            CMTF = 12,
            ARRV = 13,
            CMBR = 14,
            tete = 16,
            CRCN = 17,
            CRCT = 18,
            CRPY = 19,
            CRRR = 20,
        }
    }
}