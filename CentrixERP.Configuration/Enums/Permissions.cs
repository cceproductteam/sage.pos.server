//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//namespace CentrixERPV1.0.Configuration
//{
//    public class Permissions : EnumsReader<Permissions>
//    {
//        public Permissions()
//            : base("Permissions") { }

//        private SystemModules systemModules;
//        private TeamPermission teamPermission;

//        public SystemModules SystemModules { get { return systemModules; } }
//        public TeamPermission TeamPermission { get { return teamPermission; } }
//    }

//    public class SystemModules
//    {

//        private int none;
//        private int viewUser;
//        private int addUser;
//        private int editUser;
//        private int deleteUser;
//        private int viewRole;
//        private int addRole;
//        private int editRole;
//        private int deleteRole;
//        private int viewCompany;
//        private int addCompany;
//        private int editCompany;
//        private int deleteCompany;
//        private int viewPerson;
//        private int addPerson;
//        private int editPerson;
//        private int deletePerson;
//        private int viewPhone;
//        private int addPhone;
//        private int editPhone;
//        private int deletePhone;
//        private int viewAddress;
//        private int addAddress;
//        private int editAddress;
//        private int deleteAddress;
//        private int viewNote;
//        private int addNote;
//        private int editNote;
//        private int deleteNote;
//        private int viewPermissions;
//        private int addPermissions;
//        private int editPermissions;
//        private int deletePermissions;
//        private int viewDefaultPage;
//        private int logout;
//        private int schedualTask;
//        private int userCompanyPermissions;
//        private int userPersonPermissions;
//        private int assignTaskToUser;
//        private int configuration;
//        private int deleteTask;
//        private int viewLead;
//        private int addLead;
//        private int editLead;
//        private int deleteLead;
//        private int viewOpportunity;
//        private int addOpportunity;
//        private int editOpportunity;
//        private int deleteOpportunity;
//        private int reports;
//        private int addTeam;
//        private int editTeam;
//        private int viewTeam;
//        private int deleteTeam;
//        private int addDocument;
//        private int editDocument;
//        private int viewDocument;
//        private int deleteDocument;
//        private int addOpportunityDocument;
//        private int editOpportunityDocument;
//        private int viewOpportunityDocument;
//        private int deleteOpportunityDocument;
//        private int viewOpportunityTab;
//        private int addOpportunityTab;
//        private int editOpportunityTab;
//        private int addRealtedOpportunity;
//        private int editRealtedOpportunity;
//        private int viewRealtedOpportunity;
//        private int listRealtedOpportunity;
//        private int viewEmail;
//        private int addEmail;
//        private int editEmail;
//        private int deleteEmail;
//        private int viewOpportunityNote;
//        private int addOpportunityNote;
//        private int editOpportunityNote;
//        private int deleteOpportunityNote;
//        private int companyCommunication;
//        private int personCommunication;
//        private int opportunityCommunication;
//        private int viewProject;
//        private int addProject;
//        private int editProject;
//        private int deleteProject;
//        private int projectCommunication;
//        private int salesEmail;
//        private int addForm;
//        private int editFormValue;
//        private int addFormValue;
//        private int viewStakeHolder;
//        private int addStakeHolder;
//        private int editStakeHolder;
//        private int deleteStakeHolder;
//        private int viewResource;
//        private int addResource;
//        private int editResource;
//        private int deleteResource;
//        private int viewMileStone;
//        private int addMileStone;
//        private int editMileStone;
//        private int deleteMileStone;
//        private int addEditTaxAuthority;
//        private int viewTaxAuthority;
//        private int deleteTaxAuthority;
//        private int addTaxClassHeader;
//        private int viewTaxClassHeader;
//        private int listTaxClassHeader;
//        private int deleteTaxClassHeader;
//        private int addEditTaxClassDetails;
//        private int viewTaxClassDetails;
//        private int listTaxClassDetails;
//        private int deleteTaxClassDetails;
//        private int addEditCurrency;
//        private int viewCurrency;
//        private int deleteCurrency;
//        private int viewCurrencyRateHeader;
//        private int addEditCurrencyRateHeader;
//        private int deleteCurrencyRateHeader;
//        private int viewCurrencyRateDetails;
//        private int addEditCurrencyRateDetails;
//        private int deleteCurrencyRateDetails;
//        private int addEditTaxRate;
//        private int viewTaxRate;
//        private int listTaxRate;
//        private int addTaxGroupHeader;
//        private int editTaxGroupHeader;
//        private int viewTaxGroupHeader;
//        private int listTaxGroupHeader;
//        private int addEditTaxGroupDetails;
//        private int listTaxGroupDetails;
//        private int viewTaxGroupDetails;
//        private int listFiscalCalender;
//        private int addEditFiscalCalender;
//        private int viewFiscalCalender;
//        private int deleteFiscalCalender;
//        private int leadReport;
//        private int companyReport;
//        private int personReport;
//        private int opportunityReport;
//        private int systemReports;
//        private int viewPrizes;
//        private int addEditPrizes;
//        private int deletePrizes;
//        private int viewRedemptions;
//        private int addEditRedemptions;
//        private int deleteRedemptions;
//        private int addEditActivityLookup;
//        private int viewActivityLookup;
//        private int listDataTypeContent;
//        private int addDataTypeContent;
//        private int editDataTypeContent;
//        private int viewTemplate;
//        private int addTemplate;
//        private int editTemplate;
//        private int listTemplate;
//        private int viewTemplateType;
//        private int addTemplateType;
//        private int editTemplateType;
//        private int listTemplateType;
//        private int sendNotifications;
//        private int sentNotificationList;
//        private int sentNotificationView;
//        private int schedualTaskReport;
//        private int userAccount;
//        private int newDesignCalenderDefault;
//        private int addCommunication;
//        private int duplicationRoleConfiguration;
//        private int companyLead;
//        private int personLead;
//        private int viewLeadTab;
//        private int savedSearches;
//        private int addEditDocumentFolder;
//        private int addEditCategory;
//        private int viewCategory;
//        private int listCategory;
//        private int deleteCategory;
//        private int viewHelp;
//        private int viewCategoryContent;
//        private int addEditCategoryContent;
//        private int listCategoryContent;
//        private int deleteCategoryContent;
//        private int viewDocumentFolder;




//        public int None { get { return none; } }
//        public int ViewUser { get { return viewUser; } }
//        public int AddUser { get { return addUser; } }
//        public int EditUser { get { return editUser; } }
//        public int DeleteUser { get { return deleteUser; } }
//        public int ViewRole { get { return viewRole; } }
//        public int AddRole { get { return addRole; } }
//        public int EditRole { get { return editRole; } }
//        public int DeleteRole { get { return deleteRole; } }
//        public int ViewCompany { get { return viewCompany; } }
//        public int AddCompany { get { return addCompany; } }
//        public int EditCompany { get { return editCompany; } }
//        public int DeleteCompany { get { return deleteCompany; } }
//        public int ViewPerson { get { return viewPerson; } }
//        public int AddPerson { get { return addPerson; } }
//        public int EditPerson { get { return editPerson; } }
//        public int DeletePerson { get { return deletePerson; } }
//        public int ViewPhone { get { return viewPhone; } }
//        public int AddPhone { get { return addPhone; } }
//        public int EditPhone { get { return editPhone; } }
//        public int DeletePhone { get { return deletePhone; } }
//        public int ViewAddress { get { return viewAddress; } }
//        public int AddAddress { get { return addAddress; } }
//        public int EditAddress { get { return editAddress; } }
//        public int DeleteAddress { get { return deleteAddress; } }
//        public int ViewNote { get { return viewNote; } }
//        public int AddNote { get { return addNote; } }
//        public int EditNote { get { return editNote; } }
//        public int DeleteNote { get { return deleteNote; } }
//        public int ViewPermissions { get { return viewPermissions; } }
//        public int AddPermissions { get { return addPermissions; } }
//        public int EditPermissions { get { return editPermissions; } }
//        public int DeletePermissions { get { return deletePermissions; } }
//        public int ViewDefaultPage { get { return viewDefaultPage; } }
//        public int Logout { get { return logout; } }
//        public int SchedualTask { get { return schedualTask; } }
//        public int UserCompanyPermissions { get { return userCompanyPermissions; } }
//        public int UserPersonPermissions { get { return userPersonPermissions; } }
//        public int AssignTaskToUser { get { return assignTaskToUser; } }
//        public int Configuration { get { return configuration; } }
//        public int DeleteTask { get { return deleteTask; } }
//        public int ViewLead { get { return viewLead; } }
//        public int AddLead { get { return addLead; } }
//        public int EditLead { get { return editLead; } }
//        public int DeleteLead { get { return deleteLead; } }
//        public int ViewOpportunity { get { return viewOpportunity; } }
//        public int AddOpportunity { get { return addOpportunity; } }
//        public int EditOpportunity { get { return editOpportunity; } }
//        public int DeleteOpportunity { get { return deleteOpportunity; } }
//        public int Reports { get { return reports; } }
//        public int AddTeam { get { return addTeam; } }
//        public int EditTeam { get { return editTeam; } }
//        public int ViewTeam { get { return viewTeam; } }
//        public int DeleteTeam { get { return deleteTeam; } }
//        public int AddDocument { get { return addDocument; } }
//        public int EditDocument { get { return editDocument; } }
//        public int ViewDocument { get { return viewDocument; } }
//        public int DeleteDocument { get { return deleteDocument; } }
//        public int AddOpportunityDocument { get { return addOpportunityDocument; } }
//        public int EditOpportunityDocument { get { return editOpportunityDocument; } }
//        public int ViewOpportunityDocument { get { return viewOpportunityDocument; } }
//        public int DeleteOpportunityDocument { get { return deleteOpportunityDocument; } }
//        public int ViewOpportunityTab { get { return viewOpportunityTab; } }
//        public int AddOpportunityTab { get { return addOpportunityTab; } }
//        public int EditOpportunityTab { get { return editOpportunityTab; } }
//        public int AddRealtedOpportunity { get { return addRealtedOpportunity; } }
//        public int EditRealtedOpportunity { get { return editRealtedOpportunity; } }
//        public int ViewRealtedOpportunity { get { return viewRealtedOpportunity; } }
//        public int ListRealtedOpportunity { get { return listRealtedOpportunity; } }
//        public int ViewEmail { get { return viewEmail; } }
//        public int AddEmail { get { return addEmail; } }
//        public int EditEmail { get { return editEmail; } }
//        public int DeleteEmail { get { return deleteEmail; } }
//        public int ViewOpportunityNote { get { return viewOpportunityNote; } }
//        public int AddOpportunityNote { get { return addOpportunityNote; } }
//        public int EditOpportunityNote { get { return editOpportunityNote; } }
//        public int DeleteOpportunityNote { get { return deleteOpportunityNote; } }
//        public int CompanyCommunication { get { return companyCommunication; } }
//        public int PersonCommunication { get { return personCommunication; } }
//        public int OpportunityCommunication { get { return opportunityCommunication; } }
//        public int ViewProject { get { return viewProject; } }
//        public int AddProject { get { return addProject; } }
//        public int EditProject { get { return editProject; } }
//        public int DeleteProject { get { return deleteProject; } }
//        public int ProjectCommunication { get { return projectCommunication; } }
//        public int SalesEmail { get { return salesEmail; } }
//        public int AddForm { get { return addForm; } }
//        public int EditFormValue { get { return editFormValue; } }
//        public int AddFormValue { get { return addFormValue; } }
//        public int ViewStakeHolder { get { return viewStakeHolder; } }
//        public int AddStakeHolder { get { return addStakeHolder; } }
//        public int EditStakeHolder { get { return editStakeHolder; } }
//        public int DeleteStakeHolder { get { return deleteStakeHolder; } }
//        public int ViewResource { get { return viewResource; } }
//        public int AddResource { get { return addResource; } }
//        public int EditResource { get { return editResource; } }
//        public int DeleteResource { get { return deleteResource; } }
//        public int ViewMileStone { get { return viewMileStone; } }
//        public int AddMileStone { get { return addMileStone; } }
//        public int EditMileStone { get { return editMileStone; } }
//        public int DeleteMileStone { get { return deleteMileStone; } }
//        public int AddEditTaxAuthority { get { return addEditTaxAuthority; } }
//        public int ViewTaxAuthority { get { return viewTaxAuthority; } }
//        public int DeleteTaxAuthority { get { return deleteTaxAuthority; } }
//        public int AddTaxClassHeader { get { return addTaxClassHeader; } }
//        public int ViewTaxClassHeader { get { return viewTaxClassHeader; } }
//        public int ListTaxClassHeader { get { return listTaxClassHeader; } }
//        public int DeleteTaxClassHeader { get { return deleteTaxClassHeader; } }
//        public int AddEditTaxClassDetails { get { return addEditTaxClassDetails; } }
//        public int ViewTaxClassDetails { get { return viewTaxClassDetails; } }
//        public int ListTaxClassDetails { get { return listTaxClassDetails; } }
//        public int DeleteTaxClassDetails { get { return deleteTaxClassDetails; } }
//        public int AddEditCurrency { get { return addEditCurrency; } }
//        public int ViewCurrency { get { return viewCurrency; } }
//        public int DeleteCurrency { get { return deleteCurrency; } }
//        public int ViewCurrencyRateHeader { get { return viewCurrencyRateHeader; } }
//        public int AddEditCurrencyRateHeader { get { return addEditCurrencyRateHeader; } }
//        public int DeleteCurrencyRateHeader { get { return deleteCurrencyRateHeader; } }
//        public int ViewCurrencyRateDetails { get { return viewCurrencyRateDetails; } }
//        public int AddEditCurrencyRateDetails { get { return addEditCurrencyRateDetails; } }
//        public int DeleteCurrencyRateDetails { get { return deleteCurrencyRateDetails; } }
//        public int AddEditTaxRate { get { return addEditTaxRate; } }
//        public int ViewTaxRate { get { return viewTaxRate; } }
//        public int ListTaxRate { get { return listTaxRate; } }
//        public int AddTaxGroupHeader { get { return addTaxGroupHeader; } }
//        public int EditTaxGroupHeader { get { return editTaxGroupHeader; } }
//        public int ViewTaxGroupHeader { get { return viewTaxGroupHeader; } }
//        public int ListTaxGroupHeader { get { return listTaxGroupHeader; } }
//        public int AddEditTaxGroupDetails { get { return addEditTaxGroupDetails; } }
//        public int ListTaxGroupDetails { get { return listTaxGroupDetails; } }
//        public int ViewTaxGroupDetails { get { return viewTaxGroupDetails; } }
//        public int ListFiscalCalender { get { return listFiscalCalender; } }
//        public int AddEditFiscalCalender { get { return addEditFiscalCalender; } }
//        public int ViewFiscalCalender { get { return viewFiscalCalender; } }
//        public int DeleteFiscalCalender { get { return deleteFiscalCalender; } }
//        public int LeadReport { get { return leadReport; } }
//        public int CompanyReport { get { return companyReport; } }
//        public int PersonReport { get { return personReport; } }
//        public int OpportunityReport { get { return opportunityReport; } }
//        public int SystemReports { get { return systemReports; } }
//        public int ViewPrizes { get { return viewPrizes; } }
//        public int AddEditPrizes { get { return addEditPrizes; } }
//        public int DeletePrizes { get { return deletePrizes; } }
//        public int ViewRedemptions { get { return viewRedemptions; } }
//        public int AddEditRedemptions { get { return addEditRedemptions; } }
//        public int DeleteRedemptions { get { return deleteRedemptions; } }
//        public int AddEditActivityLookup { get { return addEditActivityLookup; } }
//        public int ViewActivityLookup { get { return viewActivityLookup; } }
//        public int ListDataTypeContent { get { return listDataTypeContent; } }
//        public int AddDataTypeContent { get { return addDataTypeContent; } }
//        public int EditDataTypeContent { get { return editDataTypeContent; } }
//        public int ViewTemplate { get { return viewTemplate; } }
//        public int AddTemplate { get { return addTemplate; } }
//        public int EditTemplate { get { return editTemplate; } }
//        public int ListTemplate { get { return listTemplate; } }
//        public int ViewTemplateType { get { return viewTemplateType; } }
//        public int AddTemplateType { get { return addTemplateType; } }
//        public int EditTemplateType { get { return editTemplateType; } }
//        public int ListTemplateType { get { return listTemplateType; } }
//        public int SendNotifications { get { return sendNotifications; } }
//        public int SentNotificationList { get { return sentNotificationList; } }
//        public int SentNotificationView { get { return sentNotificationView; } }
//        public int SchedualTaskReport { get { return schedualTaskReport; } }
//        public int UserAccount { get { return userAccount; } }
//        public int NewDesignCalenderDefault { get { return newDesignCalenderDefault; } }
//        public int AddCommunication { get { return addCommunication; } }
//        public int DuplicationRoleConfiguration { get { return duplicationRoleConfiguration; } }
//        public int CompanyLead { get { return companyLead; } }
//        public int PersonLead { get { return personLead; } }
//        public int ViewLeadTab { get { return viewLeadTab; } }
//        public int SavedSearches { get { return savedSearches; } }
//        public int AddEditDocumentFolder { get { return addEditDocumentFolder; } }
//        public int AddEditCategory { get { return addEditCategory; } }
//        public int ViewCategory { get { return viewCategory; } }
//        public int ListCategory { get { return listCategory; } }
//        public int DeleteCategory { get { return deleteCategory; } }
//        public int ViewHelp { get { return viewHelp; } }
//        public int ViewCategoryContent { get { return viewCategoryContent; } }
//        public int AddEditCategoryContent { get { return addEditCategoryContent; } }
//        public int ListCategoryContent { get { return listCategoryContent; } }
//        public int DeleteCategoryContent { get { return deleteCategoryContent; } }
//        public int ViewDocumentFolder { get { return viewDocumentFolder; } }

//    }

//    public class TeamPermission
//    {

//        private int user;
//        private int team;
//        private int role;

//        public int User { get { return user; } }
//        public int Team { get { return team; } }
//        public int Role { get { return role; } }

//    }

//}