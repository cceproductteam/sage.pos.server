﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Configuration;
//using Centrix.InventoryModule.CRM.Business.Entity;
//using Centrix.UM.Business.Entity;

namespace SagePOS.Server.Configuration
{
    public class Configuration
    {
        private static Enums_S3.Configuration.Language lang;       
        public static Enums_S3.Configuration.Language Lang
        {
            get { return lang; }
        }

     

        public static string GetConfigKeyURL(string configKey)
        {
            if (!string.IsNullOrEmpty(configKey))
                return GetConfigKeyValue("Domain") + GetConfigKeyValue("RootDirectory") + GetConfigKeyValue(configKey);
            else
                return GetConfigKeyValue("Domain") + GetConfigKeyValue("RootDirectory");
        }
        public static string GetConfigKeyPath(string configKey)
        {
            return GetConfigKeyValue("PathDirectory") + GetConfigKeyValue(configKey);
        }

        public static string GetConfigKeyValue(string configKey)
        {
            return ConfigurationManager.AppSettings[configKey].ToString();
        }

        public static void setLanguage(bool IsChangeLang)
        {

            if (HttpContext.Current.Request.Cookies["Lang"] != null && HttpContext.Current.Request.Cookies["Lang"].Value.ToLower() == "ar")
            {
                //SF.Framework.Cookies.Add("Lang", Enums_S3.Configuration.Language.en.ToString());
                lang = Enums_S3.Configuration.Language.ar;
            }
            else
            {
                //SF.Framework.Cookies.Add("Lang", Enums_S3.Configuration.Language.ar.ToString());
                lang = Enums_S3.Configuration.Language.en;
            }
            if (IsChangeLang)
                HttpContext.Current.Response.Redirect(HttpContext.Current.Request.RawUrl);



            //if (HttpContext.Current.Request.Cookies["Lang"] != null)
            //{
            //    string currentLang = HttpContext.Current.Request.Cookies["Lang"].Value;
            //    if (currentLang == Enums_S3.Configuration.Language.ar.ToString())
            //    {
            //        SF.Framework.Cookies.Add("Lang", Enums_S3.Configuration.Language.en.ToString());
            //        lang = Enums_S3.Configuration.Language.en;
            //        //SF.Framework.Cookies.Add("LangFile", Enums_S3.Configuration.Language.ar.ToString());

            //        //HttpContext.Current.Response.Cookies[
            //    }
            //    else if (currentLang == Enums_S3.Configuration.Language.en.ToString())
            //    {
            //        SF.Framework.Cookies.Add("Lang", Enums_S3.Configuration.Language.ar.ToString());
            //        lang = Enums_S3.Configuration.Language.ar;
            //        //SF.Framework.Cookies.Add("LangFile", "");
            //    }
            //    else
            //    {
            //        SF.Framework.Cookies.Add("Lang", Enums_S3.Configuration.Language.en.ToString());
            //        lang = Enums_S3.Configuration.Language.en;
            //    }
            //}
            //else
            //{
            //    SF.Framework.Cookies.Add("Lang", Enums_S3.Configuration.Language.en.ToString());
            //    lang = Enums_S3.Configuration.Language.en;
            //}
            //if (IsChangeLang)
            //    HttpContext.Current.Response.Redirect(HttpContext.Current.Request.RawUrl);
        }

        //public static void setCalendarMode() {
        //    if (HttpContext.Current.Request.Cookies["CalendarMode"] != null && HttpContext.Current.Request.Cookies["CalendarMode"].Value.ToLower() == "list")
        //    {
        //        calendarMode = Enums_S3.Configuration.CalendarMode.list;
        //    }
        //    else calendarMode = Enums_S3.Configuration.CalendarMode.calendarView;
        //}

        public static int GetEntityId(Type Type)
        {
            //if (Type == typeof(Company))
            //    return (int)Enums_S3.Entity.Company;
            //else if (Type == typeof(Person))
            //    return (int)Enums_S3.Entity.Person;
            //else if (Type == typeof(User))
            //    return (int)Enums_S3.Entity.User;
            //else if (Type == typeof(FixedAssets))
            //    return (int)Enums_S3.Entity.FixedAssets;

            //else
            return -1;
        }
       
    }
}
