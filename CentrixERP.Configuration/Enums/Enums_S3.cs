using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SagePOS.Server.Configuration
{
    public class Enums_S3
    {
        public class InventoryControl
        {
            public enum ReceiptPostType
            {
                NotPosted = 0,
                Posted = 1,
                Return = 187,
                Adjustment = 188,
                Complete = 189
            }
            public enum DocumentNumbers
            {
                AssemblyNumber = 1,
                DisassemblyNumber = 2,
                TransferNumber = 3,
                TransferReceiptNumber = 4,
                AdjustmentNumber = 5,
                ShipmentNumber = 6,
                ShipmentReturnNumber = 7,
                ReceiptNumber = 8,
                InternalUsageNumber = 9,
                RecallNumber = 10,
                ReleaseNumber = 11,
                CombineNumber = 12,
                SplitNumber = 13,
                ReconciliationNumber = 14,
                ReceiptReturnNumber = 15
            }
            public enum ShipmentType
            {
                Shipment = 190,
                Return = 191
            }
            public enum AdjustmentType
            {
                QuantityIncrease = 200,
                QuantityDecrease = 201,
                CostIncrease = 202,
                CostDecrease = 203,
                BothIncrease = 204,
                BothDecrease = 205
            }
            public enum TransferType
            {
                Transfer = 192,
                TransitTransfer = 193,
                TransitReceipt = 194
            }
            public enum TransferProrationMethod
            {
                ByQuantity = 195,
                ByCost = 197,
                ByWeight = 196,
                Equally = 198,
                Manually = 199
            }
            public enum SerialItemStatus
            {
                Available = 1,
                NotAvailable = 2,
                ReservedForReceiptReturn = 3,
                ReservedForShipment = 4,
                ReservedForTransfer = 5
            }
            public enum LotItemStatus
            {
                Available = 1,
                NotAvailable = 2,
                ReservedForReceiptReturn = 3,
                ReservedForShipment = 4,
                ReservedForTransfer = 5
            }
            public enum TransactionType
            {
                Receipt = 1,
                ReceiptReturn = 2,
                ReceiptAdjustment = 3,
                Shipment = 4,
                ShipmentReturn = 5,
                Transfer = 6,
                TransitTransfer = 7,
                TransitReceipt = 8,
                AdjustmentQuantityIncrease = 9,
                AdjustmentQuantityDecrease = 10,
                AdjustmentCostIncrease = 11,
                AdjustmentCostDecrease = 12,
                AdjustmentBothIncrease = 13,
                AdjustmentBothDecrease = 14,
                Adjustment = 15
            }
            public enum InventoryErrors
            {
                GLAccountsDoesNotExist = 600,
                RecordExsits = 501,
                AutomaticInvoiceNotCreated = 601,
                LotPostingErrors = 602
            }
            public enum AdditionalCostAction
            {
                Leave = 168,
                Prorate = 169
            }
        }

        public class Reports
        {
            public enum schema
            {
                Company = 1,
                Lead = 2,
                Person = 4,
                Opportunity = 5,
                SchedualTask = 6,
                CompanyDetailesReport = 7,
                LeadDetailsReport = 8,
                PersonDetailsReport = 9,
                OpportunityDetailsReport = 10,
                SchedualTaskDetailesReport = 11,
                CustomerStatement = 12,
                CustomerTransaction = 13,
                CustomerAging = 14,
                CMTransactionListing = 15,
                BankReconciliation = 16,
                InvoiceBatchList = 17,
                ReceiptbatchList = 18,
                AdjustmentBatchList = 19,
                RefundBatchList = 20,
                InvoiceDetailsReport = 21,
                ReceiptDetailsReport = 22,
                ReceiptApplyDocReport = 23,
                ReceiptMiscReport = 24,
                ReceiptPrePaymentReport = 25,
                AdjustmentDetailsReport = 26,
                RefundDetailsReport = 27,
                InvoiceAppliedDocumentDetails = 28,
                ReceiptAppliedDocumentDetails = 29,
                RevenueRecognitionReport = 30,
                CurrencyExchangeRateReport = 31,
                ApInvoiceBatchListReport = 32,
                ApInvoiceDetailsReport = 33,
                VendorTransaction = 34,
                VendorStatment = 35,
                ApPaymentBatchListReport = 36,
                ApPaymentDetailsReport = 37,
                VendorAging = 38,
                TrialBalanceReport = 39,
                IcQuantityOnHandReport = 40,
                IcSalesReport = 41,
                IcItemDeliveryReport = 42,
                IcSellingPriceListReport = 43,
                GLIncomeStatementsReport = 44,
                GLAccountTransactionsReport = 45,
                GLCashFlowStatementReport = 46,
                IcItemSellingPriceReport = 47,
                ICReceiptReport = 48,
                ICShipmentReport = 49,
                GLTransactionsListReport = 50,
                PDCCheckReport = 51,
                IcAdjustmentReport = 52,
                IcTransferReport = 53,

                POSInvoiceReport = 54,
                POSTotalDailySales = 55,
                POSDiscountReport = 56,
                POSDailySalesPerItem = 57,
                POSDailysales = 58,
                POSChequeDetails = 59,
                GLBatchListReport = 60,
                POSGenerateBarcode = 61,
                
                OeOrderEntryReport = 62,
                OeShipmentReport = 63,
                OeInvoiceReport = 64,
                OeShipmentReturnReport = 65,
                POReport = 66,
                //POReport = 85,
                PoInvoiceReport = 67,
                POReceiptReport = 68,
                POReceiptReturnReport = 69,

                POSCloseRegisterReport = 70,
                ItemTransactionsReport = 71,
                APPaymentVoucher = 72,
                ARReceiptVoucherReport = 73,
                ChartOfAccounts = 74,
                CustomerReport = 75,
                VendorReport = 76,
                ItemReport = 77,
                SummarySalesPerItem = 78,
                SalesReport = 79,
        

                //PO = 972,
                //POInvoice = 973,
                //POReceipt = 974,
                //POReceiptReturn = 975,
                RollupAccountsReport = 976,
                CustomerBalanceReport = 977,
                VendorBalanceReport = 978,
                IcAdjustmenteReport = 979,
                POSCustomField = 980,

                //POSCloseRegisterReport = 962,
                //ItemTransactionsReport = 963,
                //APPaymentVoucher = 964,
                //ARReceiptVoucherReport = 965,
                //ChartOfAccounts = 966,
                //CustomerReport = 967,
                //VendorReport = 968,
                //ItemReport = 969,
                //SummarySalesPerItem = 970,
                //SalesReport = 971,
                //PO = 972,
                //POInvoice = 973,
                //POReceipt = 974,
                //POReceiptReturn = 975,
                //RollupAccountsReport = 976,
                //CustomerBalanceReport = 977,
                //VendorBalanceReport = 978,
                //IcAdjustmenteReport = 979,
                //POSCustomField = 980,



            }
            //public enum schema
            //{
            //    Company = 1,
            //    Lead = 2,
            //    Person = 4,
            //    Opportunity = 5,
            //    SchedualTask = 6,
            //    CompanyDetailesReport = 7,
            //    LeadDetailsReport = 8,
            //    PersonDetailsReport = 9,
            //    OpportunityDetailsReport = 10,
            //    SchedualTaskDetailesReport = 11,
            //    CustomerStatement = 12,
            //    CustomerTransaction = 13,
            //    CustomerAging = 14,
            //    CMTransactionListing = 15,
            //    BankReconciliation = 16,
            //    InvoiceBatchList = 17,
            //    ReceiptbatchList = 18,
            //    AdjustmentBatchList = 19,
            //    RefundBatchList = 20,
            //    InvoiceDetailsReport = 21,
            //    ReceiptDetailsReport = 22,
            //    ReceiptApplyDocReport = 23,
            //    ReceiptMiscReport = 24,
            //    ReceiptPrePaymentReport = 25,
            //    AdjustmentDetailsReport = 26,
            //    RefundDetailsReport = 27,
            //    InvoiceAppliedDocumentDetails = 28,
            //    ReceiptAppliedDocumentDetails = 29,
            //    RevenueRecognitionReport = 30,
            //    CurrencyExchangeRateReport = 31,
            //    ApInvoiceBatchListReport = 32,
            //    ApInvoiceDetailsReport = 33,
            //    VendorTransaction = 34,
            //    VendorStatment = 35,
            //    ApPaymentBatchListReport = 36,
            //    ApPaymentDetailsReport = 37,
            //    VendorAging = 38,
            //    TrialBalanceReport = 39,
            //    IcQuantityOnHandReport = 40,
            //    IcSalesReport = 41,
            //    IcItemDeliveryReport = 42,
            //    IcSellingPriceListReport = 43,
            //    GLIncomeStatementsReport = 44,
            //    GLAccountTransactionsReport = 45,
            //    GLCashFlowStatementReport = 46,
            //    IcItemSellingPriceReport = 47,
            //    ICReceiptReport = 48,
            //    ICShipmentReport = 49,
            //    GLTransactionsListReport = 50,
            //    PDCCheckReport = 51,
            //    IcAdjustmentReport = 52,
            //    IcTransferReport = 53,

            //    POSInvoiceReport = 54,
            //    POSTotalDailySales = 55,
            //    POSDiscountReport = 56,
            //    POSDailySalesPerItem = 57,
            //    POSDailysales = 58,
            //    POSChequeDetails = 59,
            //    POSGenerateBarcode=61,

            //    GLBatchListReport = 60,
            //    POSCloseRegisterReport=62,
            //    ItemTransactionsReport = 63,
            //    APPaymentVoucher=64,
            //    ARReceiptVoucherReport=65,

            //    ChartOfAccounts=66,
            //    CustomerReport=67,
            //    VendorReport=68,
            //    ItemReport=69,
            //    SummarySalesPerItem=70,

            //    SalesReport = 71,

            //    PO = 72,
            //    POInvoice = 73,
            //    POReceipt = 74,
            //    POReceiptReturn = 75,

            //    RollupAccountsReport = 76,
            //    CustomerBalanceReport = 77,
            //    VendorBalanceReport = 78,
            //    IcAdjustmenteReport=79,
            //    POSCustomField=80,

 
            //    OeOrderEntryReport = 81,
            //    OeShipmentReport = 82,
            //    OeInvoiceReport = 83,
            //    OeShipmentReturnReport = 84,
            //    POReport = 85,
            //    PoInvoiceReport = 86,
            //    POReceiptReport = 87,
            //    POReceiptReturnReport = 88

            //}
        }

        public class ReportQueue
        {
            public enum ReportStatus
            {
                Pending = 1,
                InProgress = 2,
                Succeed = 3,
                Failed = 4
            }
        }

        public enum Priority
        {
            High = 1,
            Normal = 2,
            Low = 3
        }

      

        public class Permissions
        {
            public enum SystemModules
            {
                None = 0,
                ViewUser = 1,
                AddUser = 2,
                EditUser = 3,
                DeleteUser = 4,
                ViewRole = 5,
                AddRole = 6,
                EditRole = 7,
                DeleteRole = 8,
                ViewCompany = 9,
                AddCompany = 10,
                EditCompany = 11,
                DeleteCompany = 12,
                ViewPerson = 13,
                AddPerson = 14,
                EditPerson = 15,
                DeletePerson = 16,
                ViewPhone = 17,
                AddPhone = 18,
                EditPhone = 19,
                DeletePhone = 20,
                ViewAddress = 21,
                AddAddress = 22,
                EditAddress = 23,
                DeleteAddress = 24,
                ViewNote = 25,
                AddNote = 26,
                EditNote = 27,
                DeleteNote = 28,
                ViewPermissions = 29,
                AddPermissions = 30,
                EditPermissions = 31,
                DeletePermissions = 32,
                ViewDefaultPage = 33,
                Logout = 34,
                SchedualTask = 35,
                UserCompanyPermissions = 36,
                UserPersonPermissions = 37,
                AssignTaskToUser = 38,
                Configuration = 39,
                DeleteTask = 40,
                ViewLead = 41,
                AddLead = 42,
                EditLead = 43,
                DeleteLead = 44,
                ViewOpportunity = 45,
                AddOpportunity = 46,
                EditOpportunity = 47,
                DeleteOpportunity = 48,
                Reports = 49,
                AddTeam = 50,
                EditTeam = 51,
                ViewTeam = 52,
                DeleteTeam = 53,
                AddDocument = 54,
                EditDocument = 55,
                ViewDocument = 56,
                DeleteDocument = 57,
                AddOpportunityDocument = 58,
                EditOpportunityDocument = 59,
                ViewOpportunityDocument = 60,
                DeleteOpportunityDocument = 61,
                ViewOpportunityTab = 67,
                AddOpportunityTab = 68,
                EditOpportunityTab = 69,
                AddRealtedOpportunity = 70,
                EditRealtedOpportunity = 71,
                ViewRealtedOpportunity = 72,
                ListRealtedOpportunity = 73,
                ViewEmail = 74,
                AddEmail = 75,
                EditEmail = 76,
                DeleteEmail = 77,
                ViewOpportunityNote = 78,
                AddOpportunityNote = 79,
                EditOpportunityNote = 80,
                DeleteOpportunityNote = 81,
                CompanyCommunication = 82,
                PersonCommunication = 83,
                OpportunityCommunication = 84,
                ViewProject = 85,
                AddProject = 86,
                EditProject = 87,
                DeleteProject = 88,
                ProjectCommunication = 89,
                SalesEmail = 90,
                AddForm = 91,
                EditFormValue = 92,
                AddFormValue = 93,
                ViewStakeHolder = 94,
                AddStakeHolder = 95,
                EditStakeHolder = 96,
                DeleteStakeHolder = 97,
                ViewResource = 98,
                AddResource = 99,
                EditResource = 100,
                DeleteResource = 101,
                ViewMileStone = 102,
                AddMileStone = 103,
                EditMileStone = 104,
                DeleteMileStone = 105,
                AddEditTaxAuthority = 106,
                ViewTaxAuthority = 107,
                DeleteTaxAuthority = 108,
                AddTaxClassHeader = 109,
                ViewTaxClassHeader = 110,
                ListTaxClassHeader = 111,
                DeleteTaxClassHeader = 112,
                AddEditTaxClassDetails = 113,
                ViewTaxClassDetails = 114,
                ListTaxClassDetails = 115,
                DeleteTaxClassDetails = 116,
                //AddEditCurrency = 117,
                //ViewCurrency = 118,
                //DeleteCurrency = 119,
                ViewCurrencyRateHeader = 120,
                AddEditCurrencyRateHeader = 121,
                DeleteCurrencyRateHeader = 122,
                ViewCurrencyRateDetails = 123,
                AddEditCurrencyRateDetails = 124,
                DeleteCurrencyRateDetails = 125,
                AddEditTaxRate = 126,
                ViewTaxRate = 127,
                ListTaxRate = 128,
                AddTaxGroupHeader = 129,
                EditTaxGroupHeader = 130,
                ViewTaxGroupHeader = 131,
                ListTaxGroupHeader = 132,
                AddEditTaxGroupDetails = 133,
                ListTaxGroupDetails = 134,
                ViewTaxGroupDetails = 135,
                ListFiscalCalender = 136,
                AddEditFiscalCalender = 137,
                ViewFiscalCalender = 138,
                DeleteFiscalCalender = 139,
                LeadReport = 148,
                CompanyReport = 149,
                PersonReport = 150,
                OpportunityReport = 151,
                SystemReports = 152,
                ViewPrizes = 153,
                AddEditPrizes = 154,
                DeletePrizes = 155,
                ViewRedemptions = 156,
                AddEditRedemptions = 157,
                DeleteRedemptions = 158,
                AddEditActivityLookup = 159,
                ViewActivityLookup = 160,
                ListDataTypeContent = 161,
                AddDataTypeContent = 162,
                EditDataTypeContent = 163,
                ViewTemplate = 164,
                AddTemplate = 165,
                EditTemplate = 166,
                ListTemplate = 167,
                ViewTemplateType = 168,
                AddTemplateType = 169,
                EditTemplateType = 171,
                ListTemplateType = 172,
                SendNotifications = 173,
                SentNotificationList = 174,
                SentNotificationView = 175,
                SchedualTaskReport = 176,
                UserAccount = 177,
                NewDesignCalenderDefault = 178,
                AddCommunication = 179,
                DuplicationRoleConfiguration = 180,
                CompanyLead = 183,
                PersonLead = 184,
                ViewLeadTab = 185,
                SavedSearches = 186,
                AddEditDocumentFolder = 187,
                AddEditCategory = 189,
                ViewCategory = 190,
                ListCategory = 191,
                DeleteCategory = 192,
                ViewHelp = 193,
                ViewCategoryContent = 194,
                AddEditCategoryContent = 195,
                ListCategoryContent = 196,
                DeleteCategoryContent = 197,
                ListDocumentFolder = 197,
                ViewDocumentFolder = 198,
                ViewCompanyRelationShip = 199,
                AddEditCompanyRelationShip = 200,
                DeleteCompanyRelationShip = 202,
                viewCompanyContracts = 204,
                AddEditCredit = 205,
                ListCredit = 206,
                ViewCredit = 207,
                ListContactCredit = 208,
                ListCreditHistory = 209,
                ListClearanceLevel = 210,
                ListProduct = 211,
                ViewProduct = 212,
                AddEditProduct = 213,
                ListPriceList = 214,
                AddEditPriceList = 215,
                ViewPriceList = 216,
                ListCurrency = 217,
                AddEditCurrency = 218,
                ViewCurrency = 219,
                AddEditQuotation = 220,
                ViewQuotation = 221,
                ListQoutation = 222,
                AddEditGroup = 223,
                ViewGroup = 224,
                DeleteGroup = 225,
                AddEditGroupSearchCriteria = 226,
                ViewGroupSearchCriteria = 227,
                DeleteGroupSearchCriteria = 228,
                ViewEmailConfigration = 229,
                AddEditEmailConfigration = 230,
                DeleteEmailConfigration = 231,
                AddCompanySites = 233,
                EditCompanySites = 234,
                ViewCompanySites = 235,
                DeleteCompanySites = 236,
                AddCompanyFinancialInformation = 237,
                EditCompanyFinancialInformation = 238,
                ViewCompanyFinancialInformation = 239,
                DeleteCompanyFinancialInformation = 240,
                AddSiteContact = 241,
                EditSiteContact = 242,
                ViewSiteContact = 243,
                DeleteSiteContact = 244,
                AddEditClientCare = 245,
                ViewClientCare = 246,
                DeleteClientCare = 247,

                AddEditClientCareProgress = 251,
                ViewClientCareProgress = 252,
                DeleteClientCareProgress = 253,
                AddEditPersonClientcareCaseType = 254,
                ViewPersonClientcareCaseType = 255,
                AddEditPersonSelfServiceUser = 256,
                ViewPersonSelfServiceUser = 257,
                AddEditSelfServiceSecurityGroup = 258,
                ViewSelfServiceSecurityGroup = 259,
                DeleteSelfServiceSecurityGroup = 260


            }

            public enum TeamPermission
            {
                User = 1,
                Team = 2,
                Role = 3
            }
        }

        public class Phone
        {
            public enum PhoneType
            {
                LandLine = 1,
                Skype = 2,
                Mobile = 3,
                Fax = 000,
                Home = 000
            }
        }

        public class Email
        {
            public enum EmailType
            {
                //Business = 758,
                //Personal = 757
                Business = 1,
                Personal = 2
            }
        }
        public class DataTypes
        {
            //public enum DataType
            //{
            //    Type = 1,
            //    Segment = 2,
            //    Source = 3,
            //    Salutation = 4,
            //    Gender = 5,
            //    MaritalStatus = 6,
            //    Religion = 7,
            //    Department = 8,
            //    Position = 9,
            //    CompanyCategory = 10,
            //    MainProductInterest = 11,
            //    DecisionTimeframe = 12,
            //    LeadStage = 13,
            //    LeadStatus = 14,
            //    Priority = 15,
            //    Rating = 16,
            //    AnnualRevenues = 17,
            //    NoOfEmployee = 18,
            //    Industry = 19,
            //    OpportunityStage = 20,
            //    OpportunityStatus = 21,
            //    OpportunityType = 22,
            //    Currency = 23,
            //    RealtedOpportunityType = 24,
            //    ProjectProduct = 28,
            //    ProjectStatus = 29,
            //    TransactionType = 30,
            //    ClassType = 31,
            //    DateMatched = 32,
            //    RateOperation = 33,
            //    PrizesTypes = 34,
            //    Module = 35,
            //    FilterType = 36,
            //    PersonRelations = 37,
            //    scheduleTaskType = 38,
            //    CompanyOwner = 39,
            //    ClearanceLevel = 40,
            //    CommunicationMedium = 41,
            //    Frequency = 42,
            //    ProductStatus = 43,
            //    ProductItemType = 44,
            //    CurrencySymbol = 45,
            //    CloseReason = 46,
            //    SuspendReason = 47,
            //    QuotationType = 48,
            //    QuotationCategory = 49,
            //    QuotationStatus=50

            //}

            //public enum Stage
            //{
            //    Assigned = 90
            //}

            //public enum Status
            //{
            //    StatusInProgress = 95
            //}

            //public enum DefualtOpportunityStage
            //{
            //    Lead = 118

            //}

            //public enum DefualtOpportunityStatus
            //{
            //    InProgress = 126
            //}

            //public enum DefualtRating
            //{
            //    Rating = 101
            //}

            //public enum Gender
            //{
            //    Male = 31,
            //    Female = 32
            //}

            public enum TransactionType
            {
                Purchase = 24,
                Sale = 25
            }

            public enum ClassType
            {
                Vendor = 26,
                Customer = 27,
                Item = 28
            }

            //public enum OpportunityStage
            //{
            //    Lead = 118,
            //    Suspended = 119,
            //    Prospect = 120,
            //    Quoted = 121,
            //    QuoteSigned = 122,
            //    OrderRecived = 123,
            //    Closed = 125

            //}

            //public enum Frequency
            //{
            //    OnOff = 273,
            //    Recurring = 276

            //}
            //public enum ProductStatus
            //{
            //    Active = 280,
            //    Inactive = 281
            //}

            //public enum QuotationStatus
            //{
            //    Generated=297
            //}

            //public enum QuotataionCategory
            //{
            //    NewSale=292,
            //    Renewal=293
            //}
        }

        public class Customers
        {
            public enum CustomerType
            {
                Company = 0,
                Person = 1
            }
        }

        public class Schedule
        {
            public enum TaskStatus
            {
                Active = 1,
                Completed = 2,
                Incomplete = 3,
                Refused = 4
            }
        }

        public class Configuration
        {
            public enum Language
            {
                ar = 1,
                en = 2
            }

            public enum Mode
            {
                Add,
                Edit,
                Delete,
                List,
                View
            }

            public enum Module
            {
                Company = 1,
                Person = 2,
                User,
                Lead,
                Opportunity,
                Team,
                Role,
                RealtedOpportunity,
                Project,
                FormBuilder,
                TaxAuthority,
                TaxClassHeader,
                TaxClassDetails,
                Currency,
                CurrencyRateHeader,
                CurrencyRateDetails,
                TaxRate,
                TaxGroupHeader,
                TaxGroupDetails,
                FiscalCalender,
                Report,
                Prizes,
                Redemptions,
                Template,
                TemplateType,
                ActivityLookup,
                DataTypeContent,
                Address,
                Phone,
                Email,
                Note,
                Category,
                CategoryContent,
                DocumentFolder,
                Credits,
                Product,
                PriceList,
                Quotation,
                QuotationItem,
                Group,
                UserHelp,
                Common,
                UserPermissions,
                StakeHolders,
                Resources,
                Milestones,
                CompanyPersons,
                Document,
                Communication,
                CompanyRelationShip,
                ListContactCredit,
                ListClearanceLevel,
                GroupSearchCriteriaList,
                PersonOpportunity,
                CompanyOpportunity,
                Contracts,
                Sites,
                CompanyContracts,
                CompanySites,
                ProductPriceList,
                CompanyFinancialInformation,
                SiteContacts,
                ClientCareCase,
                ClientCareProgress,
                PersonClientcareCaseType,
                SelfServiceUser,
                SelfServiceSecurityGroup

            }

            public enum Page
            {
                EditCompany,
                AddCompany,
                ViewCompany,
                ListCompany,
                EditPerson,
                AddPerson,
                ViewPerson,
                ListPerson,
                EditUser,
                AddUser,
                ViewUser,
                ListUser,
                EditTeam,
                AddTeam,
                ViewTeam,
                ListTeam,
                EditRole,
                AddRole,
                ViewRole,
                ListRole,
                EditLead,
                AddLead,
                ViewLead,
                ListLead,
                EditOpportunity,
                AddOpportunity,
                ViewOpportunity,
                ListOpportunity,
                EditDocument,
                AddDocument,
                ViewDocument,
                ListDocument,
                Notes,
                Phones,
                Address,
                Email,
                CompanyPersons,
                CompanyOpportunity,
                LeadAddCompany,
                LeadViewCompany,
                LeadAddPerson,
                LeadViewPerson,
                CompanyPermission,
                PersonPermission,
                EditDataTypeContent,
                AddDataTypeContent,
                SchedualTasksReport,
                UserCompaniesReport,
                UserPersonsReport,
                AreaCompaniesReport,
                ReportDefault,

                ViewPersonCompany,
                CompanyCommunication,
                PersonCommunication,
                EditOpportunityTab,
                AddOpportunityTab,
                ViewOpportunityTab,
                EditRealtedOpportunity,
                AddRealtedOpportunity,
                ViewRealtedOpportunity,
                ListRealtedOpportunity,
                OpportunityNotes,
                DocumentOpportunityAdd,
                DocumentOpportunityEdit,
                DocumentOpportunityView,
                DocumentOpportunityList,
                OpportunityCommunication,
                EditProject,
                AddProject,
                ViewProject,
                ListProject,
                ProjectCommunication,
                SalesEmail,
                AdminTool,
                AddEditTaxAuthority,
                ViewTaxAuthority,
                ListTaxAuthority,
                AddTaxClassHeader,
                ViewTaxClassHeader,
                ListTaxClassHeader,
                AddEditTaxClassDetails,
                ViewTaxClassDetails,
                ListTaxClassDetails,
                //AddEditCurrency,
                //ViewCurrency,
                //ListCurrency,
                AddEditCurrencyRateHeader,
                ViewCurrencyRateHeader,
                ListCurrencyRateHeader,
                AddEditCurrencyRateDetails,
                ViewCurrencyRateDetails,
                ListCurrencyRateDetails,
                TaxRate,
                ViewTaxRate,
                ListTaxRate,
                AddTaxGroupHeader,
                EditTaxGroupHeader,
                ViewTaxGroupHeader,
                ListTaxGroupHeader,
                AddEditTaxGroupDetails,
                ListTaxGroupDetails,
                ViewTaxGroupDetails,
                ListFiscalCalender,
                AddEditFiscalCalender,
                ViewFiscalCalender,
                LeadReport,
                PurchasedItemsList,
                ListPrizes,
                ViewPrizes,
                AddEditPrizes,
                ListRedemptions,
                ViewRedemptions,
                AddEditRedemptions,

                TemplateList,
                TemplateView,
                TemplateAddEdit,
                TemplateTypeList,
                TemplateTypeView,
                TemplateTypeAddEdit,
                NotificationStyle,
                EmailConfiguration,
                ViewActivityLookup,
                AddEditActivityLookup,
                ListActivityLookup,
                ListDTC,
                AddDTC,
                EditDTC,
                TreeStyle,
                SentNotificationList,
                SentNotificationView,
                NewDesignCalenderDefault,
                AddCommunication,
                CompanyLead,
                PersonLead,
                Communication,
                ViewLeadTab,
                SavedSearches,
                RelatedPerson,
                AddEditCategory,
                ListCategory,
                ViewCategory,
                AddEditCategoryContent,
                ListCategoryContent,
                ViewCategoryContent,
                AddEditDocumentFolder,
                ListDocumentFolder,
                ViewDocumentFolder,
                CompanyRelationShips,
                CompanyContracts,
                CompanySits,
                AddEditCredit,
                ListCredit,
                ViewCredit,
                ListContactCredit,
                ViewContactCredit,
                ListCreditHistory,
                ListClearanceLevel,
                ListProduct,
                ViewProduct,
                AddEditProduct,
                ListPriceList,
                AddEditPriceList,
                ViewPriceList,
                ListCurrency,
                AddEditCurrency,
                ViewCurrency,
                AddEditProductPriceList,
                ListProductPriceList,
                ViewProductPriceList,
                AddEditQuotation,
                ViewQuotation,
                ListQoutation,
                AddEditQuoteItem,
                AddEditGroup,
                ViewGroup,
                ListGroup,
                GroupSearchCriteriaList,
                EmailConfigration,
                UserHelp,
                Reports,
                Delete,
                Search,
                MainStyle,
                IncStyle,
                LoginStyle,
                scheduleStyle,
                Jscrollpane,
                Default,
                Style,
                ChangeLang,
                Calender,
                Configration,
                Logout,
                Login,
                AddNavigationMenu,
                EditNavigationMenu,
                AddEditCompanySites,
                ViewCompanySites,
                ListCompanySites,
                AddEditCompanyFinancialInformation,
                ViewCompanyFinancialInformation,
                ListCompanyFinancialInformation,
                AddEditSiteContact,
                ViewSiteContact,
                ListSiteContact

            }

            public enum CalendarMode
            {
                list = 1,
                calendarView = 2
            }
        }

        public class Statistics
        {
            public enum Type
            {
                Number = 1,
                Value = 2
            }
        }

        public class Geo
        {
            public enum Country
            {
                Jordan = 1
            }
        }

        public class ReturnedValue
        {
            public enum Status
            {
                Failed = 0,
                Added = 1,
                Edited = 2,
                Deleted = 3
            }
        }

        public enum PersonFormatKeyword
        {
            FirstName,
            LastName,
            MiddleName,
            Salutation,
            JobTitle,
            PhoneNumber
        }

        public enum AddressFormatKeyword
        {
            Place,
            NearBy,
            BuildingNumber,
            StreetAddress,
            POBox,
            Country,
            City,
            Area,
            ZipCode,
            Region
        }

        public class ModulesKeys
        {
            public enum ModuleKeys
            {
                NPers,
                NComp,
                NProj,
                EPers,
                EComp,
                PPers,
                PComp,
                APers,
                AComp,
                DPers,
                DComp,
                DProj,
                EUser,
                NOpp,
                DOpp,
                NTask,
            }
        }

        public enum SortDirection
        {
            asc = 0,
            desc = 1,

        }

        public enum Filter
        {
            all = 0,
            recentlyViewed = 1,
        }

        public enum ControlType
        {
            Text = 1,
            DatePicker = 2,
            DDL = 3,
            AC = 4,
            RadioButton = 5,
            Label = 6,

        }

        public enum Module
        {
            Company = 193,
            Person = 194,

        }

        public enum Entity
        {
            None = 0,
            ArOptions = 1,
            ARDocumentNumber = 2,
            PhysicalCalenderHeader = 3,
            TaxAuthority = 4,
            TaxClassHeader = 5,
            TaxClassDetails = 6,
            User = 7,
            Role = 8,
            PhysicalCalenderDetails = 9,
            TaxRate = 10,
            TaxGroupHeader = 11,
            Team = 12,
            TaxGroupDetails = 13,
            Currency = 14,
            CurrencyRateHeader = 15,
            CurrencyRateDetails = 16,
            CompanyProfile = 17,
            AccountSet = 18,
            PaymentCode = 19,
            PaymentTerm = 20,
            CustomerGroup = 21,
            Customer = 22,
            SalesPerson = 23,
            SalesPersonCommission = 24,
            InvoiceBatchList = 25,
            ArInvoice = 26,
            ArInvoiceDetails = 27,
            ReceiptBatchList = 28,
            ArReceipt = 29,
            CMOptions = 30,
            CreditCardType = 31,
            CMDocuments = 32,
            GLIntegration = 33,
            Bank = 34,
            BankCheckStock = 35,
            BankCurrency = 36,
            TransferBatchList = 37,
            TransferEntryHeader = 38,
            TransferEntryDetails = 39,
            ArReceiptDetails = 40,
            BankEntryBatchList = 41,
            BankEntryHeader = 42,
            BankEntryDetails = 43,
            RefundBatchList = 44,
            AdjustmentBatchList = 45,
            ArRefund = 46,
            ArAdjustment = 47,
            ArAdjustmentDetails = 48,
            ArRefundDetails = 49,
            ReverseTrasaction = 50,
            BankReconciliation = 51,
            CheckReconcile = 52,
            Option = 53,
            SegmentCode = 54,
            ClassCode = 55,
            RolePermission = 56,
            PeriodicProcessing = 57,
            Segment = 58,
            AccountClass = 59,
            AccountStructure = 60,
            AccountGroup = 61,
            SourceCode = 62,
            Account = 63,
            ProhibitedStatusTracking = 64,
            BatchList = 65,
            AllocatedAccount = 66,
            JournalEntryHeader = 67,
            JournalEntryDetails = 68,
            CreateNewYear = 69,
            Report = 70,
            CMReport = 71,
            ARReport = 72,
            LockEntity = 73,
            CustomerGroupSalesPerson = 74,
            Note = 75,
            APOptions = 76,
            APDocumentNumber = 77,
            ApPaymentCode = 78,
            ApPaymentTerm = 79,
            ApAccountSet = 80,
            VendorGroup = 81,
            Attachments = 2139,//59,
            Vendor = 82,
            ApInvoiceBatchList = 83,
            ApInvoice = 84,
            PaymentBatchList = 86,
            ApPayment = 87,
            ApPaymentDetails = 88,
            ApPeriodicProcessing = 89,
            Email = 1135,
            ApInvoiceDetails = 85,
            IcUnitOfMeasures = 1095,
            IcWieghtUnitOfMeasures = 1096,
            IcLocation = 1100,
            IcWarrantyInfo = 1101,
            ICSegment = 1105,
            ICSegmentCode = 1106,
            IcItemStructure = 1107,
            IcItemStructureSegment = 1108,
            IcPriceList = 1109,
            IcPriceListTax = 1111,
            IcPriceListDiscount = 1110,
            IcAccountSet = 1112,
            IcCategory = 1113,
            IcOptions = 1094,
            IcDocumentNumbers = 1114,
            IcItemCard = 1116,
            IcItemUnitOfMeasures = 1117,
            IcItemTax = 1118,

            IcItemLocationCosting = 1119,
            IcPriceListGroup = 1120,
            IcPriceListGroupTax = 1123,
            IcPriceListGroupDiscount = 1121,
            IcPriceListGroupDiscountValue = 1122,
            IcReceipt = 1124,
            IcShipment = 1126,
            IcAdjustment = 1130,
            IcTransfer = 1128,
            OptionalFieldEntity = 1134,
            IcReceiptDetail = 1125,
            IcShipmentDetail = 1127,
            IcAdjustmentDetail = 1131,
            IcTransferDetail = 1129,
            IcDayEndProcessing = 1136,
            IcPriceListGroupItem = 1137,
            IcLocationIntegration = 2137,
            IcCommonQueries = 2140,
            IcAsset = 2141,
            IcUploadItems = 2142,
            IcAdditionalCostType = 2143,
            IcAdditionalCostTypeReceipt = 2144,
            //TaxRate = 113,
            //TaxClassHeader = 114,
            //TaxClassDetails = 115,
            //TaxAuthority = 116,
            IcItemCardLot = 2145,

            POSSync = 2153,
            RegisterStatus = 2174,
            SystemConfigration = 2155,
            IcCurrency = 2156,
            IcCurrencyRateDetails = 2157,
            IcCustomer = 2158,

            IcItemPriceList = 2159,


            IcStock = 2160,
            BackEndSync = 2162,
            IcSync = 2163,
            CloseRegister = 2164,
            Invoice = 2178,
            Contact = 3188,
            PosSyncConfiguration = 2166,
            SyncSchedule = 2167,
            GLAccount = 2168,
            BankAccount = 2169,
            Permission = 2170,

            Store = 2171,
            QuickKeys = 2172,
            QuickKeysProduct = 2173,
            Register = 2174,
            GiftVoucher = 2176,
            Shifts = 2177,
            ItemCard = 2178,

            UserStoreShifs = 2179,
            Company = 2183,
            Person = 2184,
            Phone = 2186,
            ItemCardSerial = 1132,
            POSPerson=11111,
            POSCompany=11112,
            CustomFild = 3187,
             PurchaseOrderHeader = 3190,
            PoInvoice = 3191,
            PurchaseOrderDetails = 3192,
            PoReceipt = 3193,
            PoAdditionalCostTypeReceipt = 3194,
            PoReceiptDetail = 41870,
            PoInvoiceDetails = 4189,
            PoReceiptReturn = 4190,
            PoReceiptReturnDetails = 4191,
            PoReturnReceipt = 4192,

            OeOrderEntry = 5193,
            OeInvoice = 6194,
            OeShipment = 6193,
            OeShipmentReturn = 6195,
            POUploadLog = 4193,
            OEUploadLog = 6196,
            BoLocation = 6197,
            BoBank=6198


    }



        public enum DefaultPermissions
        {
            None = 0,
            View = 1,
            List = 2,
            Add = 3,
            Edit = 4,
            Delete = 5,


        }

        public enum ExtendedPermissions
        {
            None = 0,
            PrintBatchList = 6,
            PostBatchList = 7,
            YearEnd = 8,
            YearEndProcessing = 10,
            ReverseBatch = 11,
            CloneBatch = 12,
            ProvisionalPosting = 13,
            Unlock = 14,
            Add_Edit = 16,
            ListItemSerials = 24,
            ListSerialTransactions = 25,
            ListItemLots = 26,
            ListLotTransactions = 27,
            ViewTaxGroupHeaderAndDetails = 28,
            ListTaxGroupHeaderAndDetails = 30,
            Sync = 25,

            POUpload = 2033,
            OEUoplad = 2032
        }

        public enum DocumentModule
        {
            Company = 1,
            Person = 2,
            Project = 3,
            Opportunity = 4
        }

        public class Notification
        {
            public enum NotificationStatus
            {
                Pending = 1,
                InProgress = 2,
                Succeed = 3,
                Failed = 4
            }

            public enum NotificationType
            {
                OnScreen = 1,
                Email = 2,
                SMS = 3
            }

            public enum NotificationTemplate
            {
                CaseReasign = 1,
                UnresolvedCaseDays = 2,
                ExtremeCaseClosed = 3,
                ExtremeCaseCreated = 4,
                QuotationExpiryDate = 5,
                CaseReasignForCreater = 6,
                SelfServiceClientCareCase = 7,
                NewTaskAssignment = 8,
                CreatorChangeStatus = 9,
                AssignedUserChangeStatus = 10,
                AddDiscussion = 11,
                AddAttachment = 12,
                CaseReasignToAnotherUser = 13,
                CustomerInvoice = 15
            }
        }

        public class UserLoggin
        {
            public enum UserLogginStatus
            {
                NotLoggedIn = 1,
                LoggedIn = 2,
                SessionExpired = 3,
                LoggedFromAnotherPlace = 4,
            }
        }

        public class StatusCode
        {
            public enum Codes
            {
                Failed = 0,
                Success = 1,
                InternalServerError = 500,
                EntityLocked = 501,
                EntityLockedDeleted = 501,
                EntityLockedChanged = 502,
                SessionNotAuthorizedCode = 403,
                NotAuthorizedErrorCode = 401,
                FailedToDelete = 503,
                FailedToDeleteActiveItem = 504,
                FailedToInactiveItem = 505,
                FailedToDeactivate = 504,
                OptioanlFieldDetailsError = 510


            }
        }

        public enum PriceListPricingDeterminedBy
        {
            CustomerType = 152,
            VolumeDiscounts = 153
        }

        public class MigrationToolKeys
        {
            public enum UploadLogStatus
            {
                Pending = 1,
                Completed = 2,
                Failed = 3
            }

            public enum UploadTypes
            {
                Items = 1,
                Adjustment = 2
            }
        }

        public enum SyncType
        {
            Invoice = 1,
            Refund = 2,
            PettyCash = 3,
        }
        public enum BOSyncStatus
        {
            Pending = 1,
            InProgress = 2,
            Succeed = 3,
            Failed = 4,
        }
        public enum OEInvoiceStatus
        {
            Saved = 1466,
            Posted = 1467
        }
        public enum OEOrderEntityStatus
        {
            ReadyToDeliver = 463,
            PartiallyDelivered = 464,
            FullyDelivered = 465,
            Saved = 466
        }
        public enum OeShipmentReturnStatus
        {
            Saved = 1469,
            Posted = 1470
        }
        public enum PoInvoiceType
        {
            ReciptInvoice = 1479,
            AdditionalCostInvoice = 1480
        }
        public enum PoInvoiceStatus
        {
            Saved = 457,
            Posted = 458
        }
        public enum PoReciptStatus
        {
            Saved = 455,
            Posted = 456,
            Invoiced = 1472
        }

        public enum PurchaseOrderStatus
        {
            Saved = 452,
            PartiallyReceived = 453,
            FullyReceived = 454,
            ReadyToReceived = 1476
        }

    }

}