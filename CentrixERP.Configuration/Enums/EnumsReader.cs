using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Xml.Linq;
using System.Reflection;

namespace SagePOS.Server.Configuration
{
    public abstract class EnumsReader<TEnums>
        where TEnums : EnumsReader<TEnums>
    {
        private string enums;
        //private string child;

        public EnumsReader(string enums)
        {
            this.enums = enums;
            GetEnums();
            //this.child = child;
        }

        private void GetEnums()
        {
            string xmlPath = ConfigurationManager.AppSettings["EnumsURL"].ToString();

            if (!System.IO.File.Exists(xmlPath))
                throw new Exception("The Entity Enums XML File is Not Exists!!!!!!!!");

            XDocument doc = XDocument.Load(xmlPath);
            var EnumProperties = from XElement element in doc.Elements("Enums").Elements(this.enums).Elements("EnumProperty")
                       //where element.Attribute("Name").Value == entity.ToString()
                       select element;

            Type tEnums = typeof(TEnums);

            if (EnumProperties.Count() > 0)
            {
                foreach (XElement xel in EnumProperties.Elements("Property"))
                {
                    FieldInfo field = this.GetType().GetField(xel.Attribute("Name").Value, BindingFlags.NonPublic | BindingFlags.Instance);
                    field.SetValue(this, Convert.ToInt32(xel.Attribute("Value").Value));
                }
            }

            var EnumType = from XElement element in doc.Elements("Enums").Elements(this.enums).Elements("EnumType")
                           //where element.Attribute("Name").Value == entity.ToString()
                           select element;

            if (EnumType.Count() > 0)
            {
                foreach (XElement xel in EnumType.Elements())
                {
                    Object instance = Activator.CreateInstance(Type.GetType(xel.Attribute("Type").Value));
                    FieldInfo fieldType = this.GetType().GetField(xel.Attribute("Name").Value, BindingFlags.NonPublic | BindingFlags.Instance);

                    var typeFields = from XElement pro in xel.Elements("Property")
                                         select pro;

                    foreach (XElement property in typeFields)
                    {
                        FieldInfo field = fieldType.FieldType.GetField(property.Attribute("Name").Value, BindingFlags.NonPublic | BindingFlags.Instance);
                        field.SetValue(instance, Convert.ToInt32(property.Attribute("Value").Value));
                    }
                    fieldType.SetValue(this, instance);
                    //field.SetValue(this, Convert.ToInt32(xel.Attribute("Value").Value));
                }
            }

            //if (EnumProperties != null && EnumProperties.Count() != 0)
            //{
            //    



            //    foreach (XElement xel in Enum)
            //    {
 
            //    }

            //    foreach (PropertyInfo pro in tEnums.GetProperties())
            //    {
            //        EnumProperty[] enumPro=(EnumProperty[])pro.GetCustomAttributes(typeof(EnumProperty),false);
            //        EnumType[] enumType = (EnumType[])pro.GetCustomAttributes(typeof(EnumType), false);

            //        if (enumPro != null && enumPro.Count() > 0)
            //        {
            //            string propertyName = string.Format("{0}.{1}", tEnums, pro.Name);
            //            var property = from XElement xel in Enum.Elements("Property")
            //                           where xel.Attribute("Name").Value == pro.Name
            //                           select xel;
            //        }
            //        else if (enumType != null && enumType.Count() > 0)
            //        {
            //            string elemntName = string.Format("{0}.{1}", tEnums, pro.Name);
            //        }

                    
            //        //var child = Enum.Elements(elemntName);
            //        //if (child != null && child.Count() > 0)
            //        //{
 
            //        //}
            //    }

            //    foreach (XElement element in Enum.Elements("URL"))
            //    {
            //        //if (element.Attribute("Mode").Value == mode.ToString())
            //        //{
            //        //    url = new URLPermission();
            //        //    url.URL = element.Value;
            //        //    url.PermissionId = Convert.ToInt32(element.Attribute("PermissionId").Value.ToString());
            //        //    break;
            //        //}
            //    }
            //}
            //return url;
        }
    }
}