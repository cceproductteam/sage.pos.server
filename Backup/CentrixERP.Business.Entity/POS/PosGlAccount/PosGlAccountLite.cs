using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace CentrixERP.Business.Entity
{
    public class PosGlAccountLite : EntityBaseLite
    {

        #region Public Properties

        [LiteProperty("pos_gl_account_id", SqlDbType.Int)]
        public int PosGlAccountId
        {
            get;
            set;
        }

        [LiteProperty("account_id", SqlDbType.Int)]
        public int AccountId
        {
            get;
            set;
        }

        [LiteProperty("account_description", SqlDbType.NVarChar)]
        public string AccountDescription
        {
            get;
            set;
        }

        [LiteProperty("account_description_ar", SqlDbType.NVarChar)]
        public string AccountDescriptionAr
        {
            get;
            set;
        }

        [LiteProperty("rollup", SqlDbType.Bit)]
        public bool? Rollup
        {
            get;
            set;
        }



        //[LiteProperty("pos_gl_account_id", SqlDbType.Int)]
        public int value
        {
            get
            {
                return PosGlAccountId;
            }
        }

        //[LiteProperty("account_description", SqlDbType.NVarChar)]
        public string label
        {
            get
            {
                return AccountDescription;
            }
        }

        [LiteProperty("flag_deleted", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? FlagDeleted
        {
            get;
            set;
        }

        [LiteProperty("is_system", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? IsSystem
        {
            get;
            set;
        }

        [LiteProperty("created_date", SqlDbType.DateTime)]
        public DateTime? CreatedDate
        {
            get;
            set;
        }

        [LiteProperty("updated_date", SqlDbType.DateTime)]
        public DateTime? UpdatedDate
        {
            get;
            set;
        }

        [LiteProperty("created_by", SqlDbType.Int, FindByIdLiteParameter = false)]
        public int CreatedBy
        {
            get;
            set;
        }

        [LiteProperty("updated_by", SqlDbType.Int, FindByIdLiteParameter = false)]
        public int UpdatedBy
        {
            get;
            set;
        }

        [LiteProperty("created_by_name", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string CreatedByName { get; set; }

        [LiteProperty("updated_by_name", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string UpdatedByName { get; set; }

        [LiteProperty("pos_gl_account_id", SqlDbType.NVarChar)]
        public int Id
        {
            get { return this.PosGlAccountId; }
            set { this.PosGlAccountId = value; }
        }

        [LiteProperty("last_rows", SqlDbType.NVarChar)]
        public bool LastRows { get; set; }

        [LiteProperty("total_count", SqlDbType.NVarChar)]
        public int TotalRecords { get; set; }
        #endregion


    }
}