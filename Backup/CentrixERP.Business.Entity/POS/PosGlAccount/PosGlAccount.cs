using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
using CentrixERP.Common.Business.Entity;

namespace CentrixERP.Business.Entity
{
    [EntityIManager("CentrixERP.Business.IManager.IPosGlAccountManager,CentrixERP.Business.IManager", isLogicalDelete = true)]
    public class PosGlAccount : EntityBase<PosGlAccount, int>
    {
        #region Constructors

        public PosGlAccount() { }

        public PosGlAccount(int posGlAccountId)
        {
            this.posGlAccountId = posGlAccountId;
        }

        #endregion

        #region Private Properties

		private int posGlAccountId;

		private int accountId;

		private string accountDescription;

		private string accountDescriptionAr;

		private bool? rollup;

		private DateTime? createdDate;

		private DateTime? updatedDate;

		private int createdBy;

		private int updatedBy;

		private bool? flagDeleted;

		private bool? isSystem;



        //private bool? flagDeleted;

        //private bool? isSystem;

        //private int createdBy;

        //private int updatedBy;

        //private DateTime? createdDate;

        //private DateTime? updatedDate;
        #endregion

        #region Public Properties

        [PrimaryKey("@pos_gl_account_id", SqlDbType.Int)]
[Property("pos_gl_account_id", SqlDbType.Int, AddParameter = false)]
		public int PosGlAccountId
		{
			get
			{
				return posGlAccountId;
			}
			set
			{
				posGlAccountId = value;
			}
		}

[Property("account_id", SqlDbType.Int)]
[PropertyConstraint(false, "Account")]
		public int AccountId
		{
			get
			{
				return accountId;
			}
			set
			{
				accountId = value;
			}
		}

[Property("account_description", SqlDbType.NVarChar)]
[PropertyConstraint(false, "AccountDescription", MaxLength = 500, MinLength = 3)]
		public string AccountDescription
		{
			get
			{
				return accountDescription;
			}
			set
			{
				accountDescription = value;
			}
		}

[Property("account_description_ar", SqlDbType.NVarChar)]
[PropertyConstraint(false, "AccountDescriptionAr", MaxLength = 500, MinLength = 3)]
		public string AccountDescriptionAr
		{
			get
			{
				return accountDescriptionAr;
			}
			set
			{
				accountDescriptionAr = value;
			}
		}

[Property("rollup", SqlDbType.Bit)]
[PropertyConstraint(false, "Rollup")]
		public bool? Rollup
		{
			get
			{
				return rollup;
			}
			set
			{
				rollup = value;
			}
		}



        [Property("flag_deleted", SqlDbType.Bit, AddParameter = true, UpdateParameter = true, FindParameter = true)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [Property("is_system", SqlDbType.Bit, AddParameter = false, UpdateParameter = false,FindParameter=false)]
        public bool? IsSystem
        {
            get
            {
                return isSystem;
            }
            set
            {
                isSystem = value;
            }
        }

		[Property("created_date",SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
		public DateTime? CreatedDate
		{
			get
			{
				return createdDate;
			}
			set
			{
				createdDate = value;
			}
		}

        [Property("updated_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
		public DateTime? UpdatedDate
		{
			get
			{
				return updatedDate;
			}
			set
			{
				updatedDate = value;
			}
		}

		[Property("created_by",SqlDbType.Int, AddParameter = true, UpdateParameter = false)]
		public int CreatedBy
		{
			get
			{
				return createdBy;
			}
			set
			{
				createdBy = value;
			}
		}

		[Property("updated_by",SqlDbType.Int, AddParameter = false, UpdateParameter = true)]
		public int UpdatedBy
		{
			get
			{
				return updatedBy;
			}
			set
			{
				updatedBy = value;
			}
		}
        #endregion

             #region Composite Objects
              

                 #endregion
   

        public override int GetIdentity()
        {
            return posGlAccountId;
        }
        public override void SetIdentity(int value)
        {
            posGlAccountId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            			throw new NotImplementedException();
        }
    }
}