using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace CentrixERP.Business.Entity
{
    public class CustomFildLite : EntityBaseLite
    {

        #region Public Properties
        [LiteProperty("autocomplete_values", SqlDbType.Int)]
        public string AutocompleteValues
        {
            get;
            set;
        }
        [LiteProperty("custom_fild_id", SqlDbType.Int)]
        public int CustomFildId
        {
            get;
            set;
        }

        [LiteProperty("fild_name", SqlDbType.NVarChar)]
        public string FildName
        {
            get;
            set;
        }

        [LiteProperty("fild_name", SqlDbType.NVarChar)]
        public string label
        {
            get;
            set;
        }

        [LiteProperty("fild_type_id", SqlDbType.Int)]
        public int value
        {
            get;
            set;
        }


        [LiteProperty("fild_type_id", SqlDbType.Int)]
        public int FildTypeId
        {
            get;
            set;
        }

        [LiteProperty("fild_type", SqlDbType.NVarChar)]
        public string FildType
        {
            get;
            set;
        }

        [LiteProperty("is_required", SqlDbType.Bit)]
        public bool IsRequired
        {
            get;
            set;
        }

        [LiteProperty("validation_id", SqlDbType.Int)]
        public int ValidationId
        {
            get;
            set;
        }



        [LiteProperty("validation", SqlDbType.NVarChar)]
        public string Validation
        {
            get;
            set;
        }

        [LiteProperty("action_id", SqlDbType.NVarChar)]
        public string ActionId
        {
            get;
            set;
        }
        
        [LiteProperty("action", SqlDbType.NVarChar)]
        public string Action
        {
            get;
            set;
        }

        [LiteProperty("flag_deleted", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? FlagDeleted
        {
            get;
            set;
        }

        [LiteProperty("is_system", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? IsSystem
        {
            get;
            set;
        }

        [LiteProperty("created_date", SqlDbType.DateTime)]
        public DateTime? CreatedDate
        {
            get;
            set;
        }

        [LiteProperty("updated_date", SqlDbType.DateTime)]
        public DateTime? UpdatedDate
        {
            get;
            set;
        }

        [LiteProperty("created_by", SqlDbType.Int, FindByIdLiteParameter = false)]
        public int CreatedBy
        {
            get;
            set;
        }

        [LiteProperty("updated_by", SqlDbType.Int, FindByIdLiteParameter = false)]
        public int UpdatedBy
        {
            get;
            set;
        }

        [LiteProperty("created_by_name", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string CreatedByName { get; set; }

        [LiteProperty("updated_by_name", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string UpdatedByName { get; set; }

        [LiteProperty("custom_fild_id", SqlDbType.Int)]
        public int Id
        {
            get { return this.CustomFildId; }
            set { this.CustomFildId = value; }
        }

        [LiteProperty("last_rows", SqlDbType.NVarChar)]
        public bool LastRows { get; set; }

        [LiteProperty("total_count", SqlDbType.NVarChar)]
        public int TotalRecords { get; set; }
        #endregion


    }
}