using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
using CentrixERP.Common.Business.Entity;

namespace CentrixERP.Business.Entity
{
    [EntityIManager("CentrixERP.Business.IManager.ICustomFildManager,CentrixERP.Business.IManager", isLogicalDelete = true)]
    public class CustomFild : EntityBase<CustomFild, int>
    {
        #region Constructors

        public CustomFild() { }

        public CustomFild(int CustomFildId)
        {
            this.customFildId = CustomFildId;
        }

        #endregion

        #region Private Properties

        private int customFildId;

        private string fildName;

        private int fildTypeId;
        private string fildType;

        private bool isRequired;

        private int validationId;
        private string validation;

        private string actionId;

        private string action;
        private bool? flagDeleted;

        private bool? isSystem;

        private int createdBy;

        private int updatedBy;
        private DateTime? createdDate;

        private DateTime? updatedDate;
        private string autocompleteValues;



        //private bool? flagDeleted;

        //private bool? isSystem;

        //private int createdBy;

        //private int updatedBy;

        //private DateTime? createdDate;

        //private DateTime? updatedDate;
        #endregion

        #region Public Properties

        [PrimaryKey("@custom_fild_id", SqlDbType.Int)]
        [Property("custom_fild_id", SqlDbType.Int, AddParameter = false)]
        public int CustomFildId
        {
            get
            {
                return customFildId;
            }
            set
            {
                customFildId = value;
            }
        }

        [Property("fild_name", SqlDbType.NVarChar)]
        [PropertyConstraint(true, "FildName", MaxLength = 100, MinLength = 3)]
        public string FildName
        {
            get
            {
                return fildName;
            }
            set
            {
                fildName = value;
            }
        }

        [Property("fild_type_id", SqlDbType.Int)]
        
        public int FildTypeId
        {
            get
            {
                return fildTypeId;
            }
            set
            {
                fildTypeId = value;
            }
        }
        [Property("autocomplete_values", SqlDbType.NVarChar)]
        public string AutocompleteValues
        {
            get
            {
                return autocompleteValues;
            }
            set
            {
                autocompleteValues = value;
            }
        }

        //[Property("fild_type", SqlDbType.NVarChar)]
        ////[PropertyConstraint(true, "FildType")]
        //public string FildType
        //{
        //    get
        //    {
        //        return fildType;
        //    }
        //    set
        //    {
        //        fildType = value;
        //    }
        //}

        [Property("is_required", SqlDbType.Bit)]
        [PropertyConstraint(false, "IsRequired")]
        public bool IsRequired
        {
            get
            {
                return isRequired;
            }
            set
            {
                isRequired = value;
            }
        }

        [Property("validation_id", SqlDbType.Int)]
        
        public int ValidationId
        {
            get
            {
                return validationId;
            }
            set
            {
                validationId = value;
            }
        }

        //[Property("validation", SqlDbType.NVarChar)]

        //public string Validation
        //{
        //    get
        //    {
        //        return validation;
        //    }
        //    set
        //    {
        //        validation = value;
        //    }
        //}

        [Property("action_id", SqlDbType.NVarChar)]

        public string ActionId
        {
            get
            {
                return actionId;
            }
            set
            {
                actionId = value;
            }
        }

        //[Property("action", SqlDbType.NVarChar)]

        //public string Action
        //{
        //    get
        //    {
        //        return action;
        //    }
        //    set
        //    {
        //        action = value;
        //    }
        //}



        [Property("flag_deleted", SqlDbType.Bit, AddParameter = true, UpdateParameter = true, FindParameter = true)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [Property("is_system", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool? IsSystem
        {
            get
            {
                return isSystem;
            }
            set
            {
                isSystem = value;
            }
        }

        [Property("created_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? CreatedDate
        {
            get
            {
                return createdDate;
            }
            set
            {
                createdDate = value;
            }
        }

        [Property("updated_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? UpdatedDate
        {
            get
            {
                return updatedDate;
            }
            set
            {
                updatedDate = value;
            }
        }

        [Property("created_by", SqlDbType.Int, AddParameter = true, UpdateParameter = false)]
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        [Property("updated_by", SqlDbType.Int, AddParameter = false, UpdateParameter = true)]
        public int UpdatedBy
        {
            get
            {
                return updatedBy;
            }
            set
            {
                updatedBy = value;
            }
        }
        #endregion

       


        public override int GetIdentity()
        {
            return customFildId;
        }
        public override void SetIdentity(int value)
        {
            customFildId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            throw new NotImplementedException();
        }
    }
}