using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
using CentrixERP.Common.Business.Entity;

namespace CentrixERP.Business.Entity
{
    [EntityIManager("CentrixERP.Business.IManager.IRegisterManager,CentrixERP.Business.IManager", isLogicalDelete = true)]
    public class Register : EntityBase<Register, int>
    {
        #region Constructors

        public Register() { }

        public Register(int registerId)
        {
            this.registerId = registerId;
        }

        #endregion

        #region Private Properties

        private int registerId;

        private string registerName;

        private string registerNameAr;

        private string regiserCode;

        private int storeId = -1;

        private decimal? cashLoan;

        private int quickKeysId = -1;

        private string ipAddress;

        private string macAddress;

        private string dataBaseInstanceName;

        private string dataBaseName;

        private string dataBaseUserName;

        private string dataBasePassword;

        private int createdBy;

        private int updatedBy;

        private DateTime? createdDate;

        private DateTime? updatedDate;

        private bool? flagDeleted;

        private bool? isSystem;

        private int branchId;

        private string invoiceNumberFormat;

        private string laybyNumberFormat;

        private bool? printRecipt;

        private bool? emailRecipt;

        private bool? isUsed;

        private bool? isNew;
        private int invoiceCount;


        //private bool? flagDeleted;

        //private bool? isSystem;

        //private int createdBy;

        //private int updatedBy;

        //private DateTime? createdDate;

        //private DateTime? updatedDate;
        #endregion

        #region Public Properties

        [PrimaryKey("@register_id", SqlDbType.Int)]
        [Property("register_id", SqlDbType.Int, AddParameter = false)]
        public int RegisterId
        {
            get
            {
                return registerId;
            }
            set
            {
                registerId = value;
            }
        }

        [Property("register_name", SqlDbType.NVarChar)]
        [PropertyConstraint(true, "RegisterName", MaxLength = 100, MinLength = 3)]
        public string RegisterName
        {
            get
            {
                return registerName;
            }
            set
            {
                registerName = value;
            }
        }


        [Property("invoice_count", SqlDbType.Int)]
        //[PropertyConstraint(true, "RegisterName", MaxLength = 100, MinLength = 3)]
        public int InvoiceCount
        {
            get
            {
                return invoiceCount;
            }
            set
            {
                invoiceCount = value;
            }
        }

        [Property("register_name_ar", SqlDbType.NVarChar)]
        [PropertyConstraint(true, "RegisterNameAr", MaxLength = 100, MinLength = 3)]
        public string RegisterNameAr
        {
            get
            {
                return registerNameAr;
            }
            set
            {
                registerNameAr = value;
            }
        }

        [Property("regiser_code", SqlDbType.NVarChar)]
        [PropertyConstraint(true, "RegiserCode", MaxLength = 4, MinLength = 1)]
        public string RegiserCode
        {
            get
            {
                return regiserCode;
            }
            set
            {
                regiserCode = value;
            }
        }

        [Property("store_id", SqlDbType.Int, ReferenceParameter = true)]
        [PropertyConstraint(true, "Store", RefEntity = false, RefPropertyEntity = "StoreObj")]
        public int StoreId
        {
            get
            {
                return storeId;
            }
            set
            {
                storeId = value;
            }
        }

        [Property("cash_loan", SqlDbType.Money)]
        [PropertyConstraint(true, "CashLoan")]
        public decimal? CashLoan
        {
            get
            {
                return cashLoan;
            }
            set
            {
                cashLoan = value;
            }
        }

        [Property("quick_keys_id", SqlDbType.Int, ReferenceParameter = true)] // ReferenceParameter = true
        [PropertyConstraint(false, "QuickKeys", RefEntity = true)] //, RefPropertyEntity = "QuickKeysObj"
        public int QuickKeysId
        {
            get
            {
                return quickKeysId;
            }
            set
            {
                quickKeysId = value;
            }
        }

        [Property("ip_address", SqlDbType.NVarChar)]
        [PropertyConstraint(true, "IpAddress", MaxLength = 4000, MinLength = 3)]
        public string IpAddress
        {
            get
            {
                return ipAddress;
            }
            set
            {
                ipAddress = value;
            }
        }

        [Property("mac_address", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "MacAddress", MaxLength = 4000, MinLength = 3)]
        public string MacAddress
        {
            get
            {
                return macAddress;
            }
            set
            {
                macAddress = value;
            }
        }

        [Property("data_base_instance_name", SqlDbType.NVarChar)]
        [PropertyConstraint(true, "DataBaseInstanceName", MaxLength = 100, MinLength = 3)]
        public string DataBaseInstanceName
        {
            get
            {
                return dataBaseInstanceName;
            }
            set
            {
                dataBaseInstanceName = value;
            }
        }

        [Property("data_base_name", SqlDbType.NVarChar)]
        [PropertyConstraint(true, "DataBaseName", MaxLength = 100, MinLength = 1)]
        public string DataBaseName
        {
            get
            {
                return dataBaseName;
            }
            set
            {
                dataBaseName = value;
            }
        }

        [Property("data_base_user_name", SqlDbType.NVarChar)]
        [PropertyConstraint(true, "DataBaseUserName", MaxLength = 100, MinLength = 1)]
        public string DataBaseUserName
        {
            get
            {
                return dataBaseUserName;
            }
            set
            {
                dataBaseUserName = value;
            }
        }

        [Property("data_base_password", SqlDbType.NVarChar)]
        [PropertyConstraint(true, "DataBasePassword", MaxLength = 100, MinLength = 1)]
        public string DataBasePassword
        {
            get
            {
                return dataBasePassword;
            }
            set
            {
                dataBasePassword = value;
            }
        }

        [Property("branch_id", SqlDbType.Int)]
        [PropertyConstraint(false, "Branch")]
        public int BranchId
        {
            get
            {
                return branchId;
            }
            set
            {
                branchId = value;
            }
        }

        [Property("invoice_number_format", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "InvoiceNumberFormat", MaxLength = 100, MinLength = 3)]
        public string InvoiceNumberFormat
        {
            get
            {
                return invoiceNumberFormat;
            }
            set
            {
                invoiceNumberFormat = value;
            }
        }

        [Property("layby_number_format", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "LaybyNumberFormat", MaxLength = 100, MinLength = 3)]
        public string LaybyNumberFormat
        {
            get
            {
                return laybyNumberFormat;
            }
            set
            {
                laybyNumberFormat = value;
            }
        }

        [Property("print_recipt", SqlDbType.Bit)]
        [PropertyConstraint(false, "PrintRecipt")]
        public bool? PrintRecipt
        {
            get
            {
                return printRecipt;
            }
            set
            {
                printRecipt = value;
            }
        }

        [Property("email_recipt", SqlDbType.Bit)]
        [PropertyConstraint(false, "EmailRecipt")]
        public bool? EmailRecipt
        {
            get
            {
                return emailRecipt;
            }
            set
            {
                emailRecipt = value;
            }
        }

        [Property("is_used", SqlDbType.Bit)]
        [PropertyConstraint(false, "IsUsed")]
        public bool? IsUsed
        {
            get
            {
                return isUsed;
            }
            set
            {
                isUsed = value;
            }
        }

        //[Property("is_new", SqlDbType.Bit)]
        //[PropertyConstraint(false, "IsNew")]
        //public bool? IsNew
        //{
        //    get
        //    {
        //        return isNew;
        //    }
        //    set
        //    {
        //        isNew = value;
        //    }
        //}



        [Property("flag_deleted", SqlDbType.Bit, AddParameter = true, UpdateParameter = true, FindParameter = true)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [Property("is_system", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool? IsSystem
        {
            get
            {
                return isSystem;
            }
            set
            {
                isSystem = value;
            }
        }

        [Property("created_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? CreatedDate
        {
            get
            {
                return createdDate;
            }
            set
            {
                createdDate = value;
            }
        }

        [Property("updated_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? UpdatedDate
        {
            get
            {
                return updatedDate;
            }
            set
            {
                updatedDate = value;
            }
        }

        [Property("created_by", SqlDbType.Int, AddParameter = true, UpdateParameter = false)]
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        [Property("updated_by", SqlDbType.Int, AddParameter = false, UpdateParameter = true)]
        public int UpdatedBy
        {
            get
            {
                return updatedBy;
            }
            set
            {
                updatedBy = value;
            }
        }
        #endregion

        #region Composite Objects
        private Store storeObj;
        [CompositeObject(CompositObjectTypeEnum.Single, "StoreId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable = false)]
        public Store StoreObj
        {
            get
            {
                if (storeObj == null)
                    loadCompositObject_Lazy("StoreObj");
                return storeObj;
            }
            set
            {
                storeObj = value; ;
            }
        }

        //private QuickKeys quickKeysObj;
        //[CompositeObject(CompositObjectTypeEnum.Single, "QuickKeysId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable = false)]
        //public QuickKeys QuickKeysObj
        //{
        //    get
        //    {
        //        if (quickKeysObj == null)
        //            loadCompositObject_Lazy("QuickKeysObj");
        //        return quickKeysObj;
        //    }
        //    set
        //    {
        //        quickKeysObj = value; ;
        //    }
        //}



        #endregion


        public override int GetIdentity()
        {
            return registerId;
        }
        public override void SetIdentity(int value)
        {
            registerId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            if (ParentType == typeof(Store))
                this.storeId = (int)value;
            //else if (ParentType == typeof(QuickKeys))
            //    this.quickKeysId = (int)value;

        }
    }
}