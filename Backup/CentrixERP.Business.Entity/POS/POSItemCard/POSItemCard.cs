using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
using CentrixERP.Common.Business.Entity;

namespace CentrixERP.Business.Entity
{
    [EntityIManager("CentrixERP.Business.IManager.IPOSItemCardManager,CentrixERP.Business.IManager", isLogicalDelete = true)]
    public class POSItemCard : EntityBase<POSItemCard, int>
    {
        #region Constructors

        public POSItemCard() { }

        public POSItemCard(int posItemCardId)
        {
            this.posItemCardId = posItemCardId;
        }

        #endregion

        #region Private Properties

        private int posItemCardId;

        private int itemCardId;

        private string itemNumber;

        private string itemDescription;

        private string itemBarCode;

        private int defaultPriceListId;

        private string defaultPriceList;

        private decimal? defaultPrice;

        private int salesTaxClassId;

        private int salesTaxClassNumber;

        private string salesTaxClassDescription;

        private double? quantityOnHand;

        private bool? serialNumber;

        private int unitOfMeasureId;

        private bool? flagDeleted;

        private bool? isSystem;

        private DateTime? createdDate;

        private DateTime? updatedDate;

        private int updatedBy;

        private int createdBy;



        //private bool? flagDeleted;

        //private bool? isSystem;

        //private int createdBy;

        //private int updatedBy;

        //private DateTime? createdDate;

        //private DateTime? updatedDate;
        #endregion

        #region Public Properties
        [Property("pos_item_card_id", SqlDbType.Int)]
        public int value
        {
            get;
            set;
        }

        [Property("item_description", SqlDbType.NVarChar)]
        public string label
        {
            get;
            set;
        }
        [PrimaryKey("@pos_item_card_id", SqlDbType.Int)]
        [Property("pos_item_card_id", SqlDbType.Int, AddParameter = false)]
        public int PosItemCardId
        {
            get
            {
                return posItemCardId;
            }
            set
            {
                posItemCardId = value;
            }
        }

        [Property("item_card_id", SqlDbType.Int)]
        [PropertyConstraint(true, "ItemCard")]
        public int ItemCardId
        {
            get
            {
                return itemCardId;
            }
            set
            {
                itemCardId = value;
            }
        }

        [Property("item_number", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "ItemNumber", MaxLength = 100, MinLength = 3)]
        public string ItemNumber
        {
            get
            {
                return itemNumber;
            }
            set
            {
                itemNumber = value;
            }
        }

        [Property("item_description", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "ItemDescription", MaxLength = 100, MinLength = 3)]
        public string ItemDescription
        {
            get
            {
                return itemDescription;
            }
            set
            {
                itemDescription = value;
            }
        }

        [Property("item_bar_code", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "ItemBarCode", MaxLength = 100, MinLength = 3)]
        public string ItemBarCode
        {
            get
            {
                return itemBarCode;
            }
            set
            {
                itemBarCode = value;
            }
        }

        [Property("default_price_list_id", SqlDbType.Int)]
        [PropertyConstraint(false, "DefaultPriceList")]
        public int DefaultPriceListId
        {
            get
            {
                return defaultPriceListId;
            }
            set
            {
                defaultPriceListId = value;
            }
        }

        [Property("default_price_list", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "DefaultPriceList", MaxLength = 50, MinLength = 3)]
        public string DefaultPriceList
        {
            get
            {
                return defaultPriceList;
            }
            set
            {
                defaultPriceList = value;
            }
        }

        [Property("default_price", SqlDbType.Money)]
        [PropertyConstraint(false, "DefaultPrice")]
        public decimal? DefaultPrice
        {
            get
            {
                return defaultPrice;
            }
            set
            {
                defaultPrice = value;
            }
        }

        [Property("sales_tax_class_id", SqlDbType.Int)]
        [PropertyConstraint(false, "SalesTaxClass")]
        public int SalesTaxClassId
        {
            get
            {
                return salesTaxClassId;
            }
            set
            {
                salesTaxClassId = value;
            }
        }

        [Property("sales_tax_class_number", SqlDbType.Int)]
        [PropertyConstraint(false, "SalesTaxClassNumber")]
        public int SalesTaxClassNumber
        {
            get
            {
                return salesTaxClassNumber;
            }
            set
            {
                salesTaxClassNumber = value;
            }
        }

        [Property("sales_tax_class_description", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "SalesTaxClassDescription", MaxLength = 100, MinLength = 3)]
        public string SalesTaxClassDescription
        {
            get
            {
                return salesTaxClassDescription;
            }
            set
            {
                salesTaxClassDescription = value;
            }
        }

        [Property("quantity_on_hand", SqlDbType.Float)]
        [PropertyConstraint(false, "QuantityOnHand")]
        public double? QuantityOnHand
        {
            get
            {
                return quantityOnHand;
            }
            set
            {
                quantityOnHand = value;
            }
        }

        [Property("serial_number", SqlDbType.Bit)]
        [PropertyConstraint(false, "SerialNumber")]
        public bool? SerialNumber
        {
            get
            {
                return serialNumber;
            }
            set
            {
                serialNumber = value;
            }
        }

        [Property("unit_of_measure_id", SqlDbType.Int)]
        [PropertyConstraint(false, "UnitOfMeasure")]
        public int UnitOfMeasureId
        {
            get
            {
                return unitOfMeasureId;
            }
            set
            {
                unitOfMeasureId = value;
            }
        }



        [Property("flag_deleted", SqlDbType.Bit, AddParameter = true, UpdateParameter = true, FindParameter = true)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [Property("is_system", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool? IsSystem
        {
            get
            {
                return isSystem;
            }
            set
            {
                isSystem = value;
            }
        }

        [Property("created_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? CreatedDate
        {
            get
            {
                return createdDate;
            }
            set
            {
                createdDate = value;
            }
        }

        [Property("updated_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? UpdatedDate
        {
            get
            {
                return updatedDate;
            }
            set
            {
                updatedDate = value;
            }
        }

        [Property("created_by", SqlDbType.Int, AddParameter = true, UpdateParameter = false)]
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        [Property("updated_by", SqlDbType.Int, AddParameter = false, UpdateParameter = true)]
        public int UpdatedBy
        {
            get
            {
                return updatedBy;
            }
            set
            {
                updatedBy = value;
            }
        }
        #endregion

        #region Composite Objects


        #endregion


        public override int GetIdentity()
        {
            return posItemCardId;
        }
        public override void SetIdentity(int value)
        {
            posItemCardId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            throw new NotImplementedException();
        }
    }
}