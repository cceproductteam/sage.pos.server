using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace CentrixERP.Business.Entity
{
     public class POSItemCardLite : EntityBaseLite
    {      
        
        #region Public Properties

         		[LiteProperty("pos_item_card_id",SqlDbType.Int)]
		public int PosItemCardId
		{
			get;
			set;
		}

		[LiteProperty("item_card_id",SqlDbType.Int)]
		public int ItemCardId
		{
			get;
			set;
		}

		[LiteProperty("item_number",SqlDbType.NVarChar)]
		public string ItemNumber
		{
			get;
			set;
		}

		[LiteProperty("item_description",SqlDbType.NVarChar)]
		public string ItemDescription
		{
			get;
			set;
		}

		[LiteProperty("item_bar_code",SqlDbType.NVarChar)]
		public string ItemBarCode
		{
			get;
			set;
		}

		[LiteProperty("default_price_list_id",SqlDbType.Int)]
		public int DefaultPriceListId
		{
			get;
			set;
		}

		[LiteProperty("default_price_list",SqlDbType.NVarChar)]
		public string DefaultPriceList
		{
			get;
			set;
		}

		[LiteProperty("default_price",SqlDbType.Money)]
		public decimal? DefaultPrice
		{
			get;
			set;
		}

		[LiteProperty("sales_tax_class_id",SqlDbType.Int)]
		public int SalesTaxClassId
		{
			get;
			set;
		}

		[LiteProperty("sales_tax_class_number",SqlDbType.Int)]
		public int SalesTaxClassNumber
		{
			get;
			set;
		}

		[LiteProperty("sales_tax_class_description",SqlDbType.NVarChar)]
		public string SalesTaxClassDescription
		{
			get;
			set;
		}

		[LiteProperty("quantity_on_hand",SqlDbType.Float)]
		public double? QuantityOnHand
		{
			get;
			set;
		}

		[LiteProperty("serial_number",SqlDbType.Bit)]
		public bool? SerialNumber
		{
			get;
			set;
		}

		[LiteProperty("unit_of_measure_id",SqlDbType.Int)]
		public int UnitOfMeasureId
		{
			get;
			set;
		}

        [LiteProperty("rate", SqlDbType.Float)]
        public double? Rate
        {
            get;
            set;
        }

        [LiteProperty("pos_item_card_id", SqlDbType.Int)]
        public int value
        {
            get;
            set;
        }

        [LiteProperty("item_description", SqlDbType.NVarChar)]
        public string label
        {
            get;
            set;
        }

         [LiteProperty("flag_deleted", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? FlagDeleted
        {
            get;
            set;
        }

        [LiteProperty("is_system",SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
		public bool? IsSystem
		{
			get;
			set;
		}

		[LiteProperty("created_date",SqlDbType.DateTime)]
		public DateTime? CreatedDate
		{
			get;
			set;
		}

		[LiteProperty("updated_date",SqlDbType.DateTime)]
		public DateTime? UpdatedDate
		{
			get;
			set;
		}

		[LiteProperty("created_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int CreatedBy
		{
			get;
			set;
		}

		[LiteProperty("updated_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int UpdatedBy
		{
			get;
			set;
		}

         [LiteProperty("created_by_name",SqlDbType.NVarChar, FindAllLiteParameter = false)]
         public string CreatedByName{get;set;}

         [LiteProperty("updated_by_name",SqlDbType.NVarChar,FindAllLiteParameter = false)]
         public string UpdatedByName{get;set;}

         [LiteProperty("pos_item_card_id",SqlDbType.Int)]
          public int Id
        {
            get{return this.PosItemCardId; }
            set{this.PosItemCardId=value;}
        }

         [LiteProperty("last_rows",SqlDbType.NVarChar)]
         public bool LastRows { get; set; }

         [LiteProperty("total_count",SqlDbType.NVarChar)]
         public int TotalRecords { get; set; }
        #endregion
      
     }
     public class BOItemCardQl
     {
         private string itemNumber;
         private string itemBarCode;
         private string itemDesc;
         private decimal itemPrice;
         private string itemPriceList;
         private string itemUnit;
         private string itemTaxAuth;
         private decimal itemtaxRate;
         private bool itemSerial;
         private string serialNumber;
         private int availableQuantity;

         public int AvailableQuantity
         {
             get { return availableQuantity; }
             set { availableQuantity = value; }
         }

         public string ItemNumber
         {
             get { return itemNumber; }
             set { itemNumber = value; }
         }

         public string ItemBarCode
         {
             get { return itemBarCode; }
             set { itemBarCode = value; }
         }

         public string ItemDesc
         {
             get { return itemDesc; }
             set { itemDesc = value; }
         }

         public decimal ItemPrice
         {
             get { return itemPrice; }
             set { itemPrice = value; }
         }

         public string ItemPriceList
         {
             get { return itemPriceList; }
             set { itemPriceList = value; }
         }

         public string ItemUnit
         {
             get { return itemUnit; }
             set { itemUnit = value; }
         }

         public string ItemTaxAuth
         {
             get { return itemTaxAuth; }
             set { itemTaxAuth = value; }
         }

         public decimal ItemTaxRate
         {
             get { return itemtaxRate; }
             set { itemtaxRate = value; }
         }

         public bool ItemSerial
         {
             get { return itemSerial; }
             set { itemSerial = value; }
         }

         public string SerialNo
         {
             get { return serialNumber; }
             set { serialNumber = value; }
         }
     }
}