using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
using CentrixERP.Common.Business.Entity;

namespace CentrixERP.Business.Entity
{
    [EntityIManager("CentrixERP.Business.IManager.IGiftVoucherManager,CentrixERP.Business.IManager", isLogicalDelete = true)]
    public class GiftVoucher : EntityBase<GiftVoucher, int>
    {
        #region Constructors

        public GiftVoucher() { }

        public GiftVoucher(int giftVoucherId)
        {
            this.giftVoucherId = giftVoucherId;
        }

        #endregion

        #region Private Properties

        private int giftVoucherId;

        private string giftNumber;

        private string giftName;

        private decimal? giftAmount;

        private DateTime? expireDate;

        private bool? flagDeleted;

        private bool? isSystem;

        private int createdBy;

        private int updatedBy;

        private DateTime? createdDate;

        private DateTime? updatedDate;

        private bool? isUsed;



        //private bool? flagDeleted;

        //private bool? isSystem;

        //private int createdBy;

        //private int updatedBy;

        //private DateTime? createdDate;

        //private DateTime? updatedDate;
        #endregion

        #region Public Properties

        [PrimaryKey("@gift_voucher_id", SqlDbType.Int)]
        [Property("gift_voucher_id", SqlDbType.Int, AddParameter = false)]
        public int GiftVoucherId
        {
            get
            {
                return giftVoucherId;
            }
            set
            {
                giftVoucherId = value;
            }
        }

        [Property("gift_number", SqlDbType.NVarChar)]
        [PropertyConstraint(true, "GiftNumber", MaxLength = 100, MinLength = 3)]
        public string GiftNumber
        {
            get
            {
                return giftNumber;
            }
            set
            {
                giftNumber = value;
            }
        }

        [Property("gift_name", SqlDbType.NVarChar)]
        [PropertyConstraint(true, "GiftName", MaxLength = 100, MinLength = 3)]
        public string GiftName
        {
            get
            {
                return giftName;
            }
            set
            {
                giftName = value;
            }
        }

        [Property("gift_amount", SqlDbType.Money)]
        [PropertyConstraint(true, "GiftAmount")]
        public decimal? GiftAmount
        {
            get
            {
                return giftAmount;
            }
            set
            {
                giftAmount = value;
            }
        }

        [Property("expire_date", SqlDbType.DateTime)]
        [PropertyConstraint(true, "ExpireDate")]
        public DateTime? ExpireDate
        {
            get
            {
                return expireDate;
            }
            set
            {
                expireDate = value;
            }
        }

        [Property("is_used", SqlDbType.Bit)]
        [PropertyConstraint(false, "IsUsed")]
        public bool? IsUsed
        {
            get
            {
                return isUsed;
            }
            set
            {
                isUsed = value;
            }
        }



        [Property("flag_deleted", SqlDbType.Bit, AddParameter = true, UpdateParameter = true, FindParameter = true)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [Property("is_system", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool? IsSystem
        {
            get
            {
                return isSystem;
            }
            set
            {
                isSystem = value;
            }
        }

        [Property("created_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? CreatedDate
        {
            get
            {
                return createdDate;
            }
            set
            {
                createdDate = value;
            }
        }

        [Property("updated_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? UpdatedDate
        {
            get
            {
                return updatedDate;
            }
            set
            {
                updatedDate = value;
            }
        }

        [Property("created_by", SqlDbType.Int, AddParameter = true, UpdateParameter = false)]
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        [Property("updated_by", SqlDbType.Int, AddParameter = false, UpdateParameter = true)]
        public int UpdatedBy
        {
            get
            {
                return updatedBy;
            }
            set
            {
                updatedBy = value;
            }
        }
        #endregion

        #region Composite Objects


        #endregion


        public override int GetIdentity()
        {
            return giftVoucherId;
        }
        public override void SetIdentity(int value)
        {
            giftVoucherId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            throw new NotImplementedException();
        }
    }
}