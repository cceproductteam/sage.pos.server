using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace CentrixERP.Business.Entity
{
    public class ItemCardLite : EntityBaseLite
    {

        #region Public Properties

        [LiteProperty("item_card_id", SqlDbType.Int)]
        public int ItemCardId
        {
            get;
            set;
        }

        [LiteProperty("structure_code_id", SqlDbType.Int)]
        public int StructureCode
        {
            get;
            set;
        }

        [LiteProperty("item_number", SqlDbType.NVarChar)]
        public string ItemNumber
        {
            get;
            set;
        }

        [LiteProperty("item_description", SqlDbType.NVarChar)]
        public string ItemDescription
        {
            get;
            set;
        }

        [LiteProperty("coasting_method_id", SqlDbType.Int)]
        public int CoastingMethod
        {
            get;
            set;
        }


        //private string coastingMethod;

        //private string coastingMethodAr;

        //[LiteProperty("coasting_method_ar",SqlDbType.NVarChar)]
        //public string CoastingMethodAr
        //{
        //    get{return coastingMethodAr;}

        //    set{ coastingMethodAr= value; }
        //}

        //[LiteProperty("coasting_method", SqlDbType.NVarChar)]
        //public string CoastingMethodEn
        //{
        //    get { return coastingMethod; }

        //    set { coastingMethod = value; }
        //}

        //public string CoastingMethod
        //{
        //    get
        //    {
        //        if (Configuration.Configuration.Lang.ToString() == "ar")
        //            return CoastingMethodAr;
        //        else
        //            return CoastingMethodEn;
        //    }
        //}



        [LiteProperty("default_price_list_id", SqlDbType.Int)]
        public int DefaultPriceListId
        {
            get;
            set;
        }


        //[LiteProperty("default_price_list",SqlDbType.NVarChar)]
        //public string DefaultPriceList
        //{
        //    get;
        //    set;
        //}

        [LiteProperty("default_picking_sequence", SqlDbType.NVarChar)]
        public string DefaultPickingSequence
        {
            get;
            set;
        }

        [LiteProperty("unit_weight", SqlDbType.NVarChar)]
        public string UnitWeight
        {
            get;
            set;
        }

        [LiteProperty("weight_unit_of_measure_id", SqlDbType.Int)]
        public int WeightUnitOfMeasureId
        {
            get;
            set;
        }

        //[LiteProperty("weight_unit_of_measure",SqlDbType.NVarChar)]
        //public string WeightUnitOfMeasure
        //{
        //    get;
        //    set;
        //}

        [LiteProperty("alternate_item_id", SqlDbType.Int)]
        public int AlternateItem
        {
            get;
            set;
        }

        [LiteProperty("additional_item_information", SqlDbType.NVarChar)]
        public string AdditionalItemInformation
        {
            get;
            set;
        }

        [LiteProperty("is_active", SqlDbType.Bit)]
        public bool? IsActive
        {
            get;
            set;
        }

        [LiteProperty("stock_item", SqlDbType.Bit)]
        public bool? StockItem
        {
            get;
            set;
        }

        [LiteProperty("serial_number", SqlDbType.Bit)]
        public bool? SerialNumber
        {
            get;
            set;
        }

        [LiteProperty("lot_number", SqlDbType.Bit)]
        public bool? LotNumber
        {
            get;
            set;
        }

        [LiteProperty("sellable", SqlDbType.Bit)]
        public bool? Sellable
        {
            get;
            set;
        }

        [LiteProperty("kitting_item", SqlDbType.Bit)]
        public bool? KittingItem
        {
            get;
            set;
        }

        [LiteProperty("item_image_id", SqlDbType.Int)]
        public int ItemImageId
        {
            get;
            set;
        }

        //[LiteProperty("item_image",SqlDbType.NVarChar)]
        //public string ItemImage
        //{
        //    get;
        //    set;
        //}

        [LiteProperty("validate_quantity_on_boq", SqlDbType.Bit)]
        public bool? ValidateQuantityOnBoq
        {
            get;
            set;
        }

        [LiteProperty("item_bar_code", SqlDbType.NVarChar)]
        public string ItemBarCode
        {
            get;
            set;
        }

        [LiteProperty("item_sku", SqlDbType.NVarChar)]
        public string ItemSku
        {
            get;
            set;
        }

        [LiteProperty("category_id", SqlDbType.Int)]
        public int CategoryId
        {
            get;
            set;
        }

        //[LiteProperty("category",SqlDbType.NVarChar)]
        //public string Category
        //{
        //    get;
        //    set;
        //}

        [LiteProperty("account_set_code_id", SqlDbType.Int)]
        public int AccountSetCodeId
        {
            get;
            set;
        }

        //[LiteProperty("account_set_code",SqlDbType.NVarChar)]
        //public string AccountSetCode
        //{
        //    get;
        //    set;
        //}

        [LiteProperty("manufacturing_company", SqlDbType.NVarChar)]
        public string ManufacturingCompany
        {
            get;
            set;
        }

        [LiteProperty("capacity", SqlDbType.Float)]
        public double? Capacity
        {
            get;
            set;
        }

        [LiteProperty("price", SqlDbType.Float)]
        public double? Price
        {
            get;
            set;
        }
        [LiteProperty("tax", SqlDbType.Float)]
        public double? Tax
        {
            get;
            set;
        }


        [LiteProperty("item_card_id", SqlDbType.Int)]
        public int value
        {
            get;
            set;
        }

        [LiteProperty("item_number", SqlDbType.NVarChar)]
        public string label
        {
            get;
            set;
        }

        [LiteProperty("flag_deleted", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? FlagDeleted
        {
            get;
            set;
        }

        [LiteProperty("is_system", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? IsSystem
        {
            get;
            set;
        }

        [LiteProperty("created_date", SqlDbType.DateTime)]
        public DateTime? CreatedDate
        {
            get;
            set;
        }

        [LiteProperty("updated_date", SqlDbType.DateTime)]
        public DateTime? UpdatedDate
        {
            get;
            set;
        }

        [LiteProperty("created_by", SqlDbType.Int, FindByIdLiteParameter = false)]
        public int CreatedBy
        {
            get;
            set;
        }

        [LiteProperty("updated_by", SqlDbType.Int, FindByIdLiteParameter = false)]
        public int UpdatedBy
        {
            get;
            set;
        }

        [LiteProperty("created_by_name", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string CreatedByName { get; set; }

        [LiteProperty("updated_by_name", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string UpdatedByName { get; set; }

        [LiteProperty("item_card_id", SqlDbType.NVarChar)]
        public int Id
        {
            get { return this.ItemCardId; }
            set { this.ItemCardId = value; }
        }

        [LiteProperty("last_rows", SqlDbType.NVarChar)]
        public bool LastRows { get; set; }

        [LiteProperty("total_count", SqlDbType.NVarChar)]
        public int TotalRecords { get; set; }
        #endregion


    }
}