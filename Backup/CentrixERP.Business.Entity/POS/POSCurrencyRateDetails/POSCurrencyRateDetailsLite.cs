using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace CentrixERP.Business.Entity
{
    public class POSCurrencyRateDetailsLite : EntityBaseLite
    {

        #region Public Properties

        [LiteProperty("pos_currency_rate_details_id", SqlDbType.Int)]
        public int PosCurrencyRateDetailsId
        {
            get;
            set;
        }

        [LiteProperty("currency_rate_details_id", SqlDbType.Int)]
        public int CurrencyRateDetailsId
        {
            get;
            set;
        }

        [LiteProperty("source_currency", SqlDbType.Int)]
        public int SourceCurrency
        {
            get;
            set;
        }

        [LiteProperty("from_currency_id", SqlDbType.Int)]
        public int FromCurrencyId
        {
            get;
            set;
        }

        [LiteProperty("rate", SqlDbType.Float)]
        public double? Rate
        {
            get;
            set;
        }



        [LiteProperty("pos_currency_rate_details_id", SqlDbType.Int)]
        public int value
        {
            get;
            set;
        }

        [LiteProperty("code", SqlDbType.NVarChar)]
        public string label
        {
            get;
            set;
        }

        [LiteProperty("flag_deleted", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? FlagDeleted
        {
            get;
            set;
        }

        [LiteProperty("is_system", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? IsSystem
        {
            get;
            set;
        }

        [LiteProperty("created_date", SqlDbType.DateTime)]
        public DateTime? CreatedDate
        {
            get;
            set;
        }

        [LiteProperty("updated_date", SqlDbType.DateTime)]
        public DateTime? UpdatedDate
        {
            get;
            set;
        }

        [LiteProperty("created_by", SqlDbType.Int, FindByIdLiteParameter = false)]
        public int CreatedBy
        {
            get;
            set;
        }

        [LiteProperty("updated_by", SqlDbType.Int, FindByIdLiteParameter = false)]
        public int UpdatedBy
        {
            get;
            set;
        }

        [LiteProperty("created_by_name", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string CreatedByName { get; set; }

        [LiteProperty("updated_by_name", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string UpdatedByName { get; set; }

        [LiteProperty("pos_currency_rate_details_id", SqlDbType.Int)]
        public int Id
        {
            get { return this.PosCurrencyRateDetailsId; }
            set { this.PosCurrencyRateDetailsId = value; }
        }

        [LiteProperty("last_rows", SqlDbType.NVarChar)]
        public bool LastRows { get; set; }

        [LiteProperty("total_count", SqlDbType.NVarChar)]
        public int TotalRecords { get; set; }
        #endregion


    }
}