using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
using CentrixERP.Common.Business.Entity;

namespace CentrixERP.Business.Entity
{
    [EntityIManager("CentrixERP.Business.IManager.IPOSCurrencyRateDetailsManager,CentrixERP.Business.IManager", isLogicalDelete = true)]
    public class POSCurrencyRateDetails : EntityBase<POSCurrencyRateDetails, int>
    {
        #region Constructors

        public POSCurrencyRateDetails() { }

        public POSCurrencyRateDetails(int posCurrencyRateDetailsId)
        {
            this.posCurrencyRateDetailsId = posCurrencyRateDetailsId;
        }

        #endregion

        #region Private Properties

        private int posCurrencyRateDetailsId;

        private int currencyRateDetailsId;

        private int sourceCurrency;

        private int fromCurrencyId;

        private double? rate;

        private bool? flagDeleted;

        private bool? isSystem;

        private DateTime? createdDate;

        private DateTime? updatedDate;

        private int updatedBy;

        private int createdBy;



        //private bool? flagDeleted;

        //private bool? isSystem;

        //private int createdBy;

        //private int updatedBy;

        //private DateTime? createdDate;

        //private DateTime? updatedDate;
        #endregion

        #region Public Properties

        [PrimaryKey("@pos_currency_rate_details_id", SqlDbType.Int)]
        [Property("pos_currency_rate_details_id", SqlDbType.Int, AddParameter = false)]
        public int PosCurrencyRateDetailsId
        {
            get
            {
                return posCurrencyRateDetailsId;
            }
            set
            {
                posCurrencyRateDetailsId = value;
            }
        }

        [Property("currency_rate_details_id", SqlDbType.Int)]
        [PropertyConstraint(true, "CurrencyRateDetails")]
        public int CurrencyRateDetailsId
        {
            get
            {
                return currencyRateDetailsId;
            }
            set
            {
                currencyRateDetailsId = value;
            }
        }

        [Property("source_currency", SqlDbType.Int)]
        [PropertyConstraint(false, "SourceCurrency")]
        public int SourceCurrency
        {
            get
            {
                return sourceCurrency;
            }
            set
            {
                sourceCurrency = value;
            }
        }

        [Property("from_currency_id", SqlDbType.Int)]
        [PropertyConstraint(false, "FromCurrency")]
        public int FromCurrencyId
        {
            get
            {
                return fromCurrencyId;
            }
            set
            {
                fromCurrencyId = value;
            }
        }

        [Property("rate", SqlDbType.Float)]
        [PropertyConstraint(false, "Rate")]
        public double? Rate
        {
            get
            {
                return rate;
            }
            set
            {
                rate = value;
            }
        }



        [Property("flag_deleted", SqlDbType.Bit, AddParameter = true, UpdateParameter = true, FindParameter = true)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [Property("is_system", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool? IsSystem
        {
            get
            {
                return isSystem;
            }
            set
            {
                isSystem = value;
            }
        }

        [Property("created_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? CreatedDate
        {
            get
            {
                return createdDate;
            }
            set
            {
                createdDate = value;
            }
        }

        [Property("updated_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? UpdatedDate
        {
            get
            {
                return updatedDate;
            }
            set
            {
                updatedDate = value;
            }
        }

        [Property("created_by", SqlDbType.Int, AddParameter = true, UpdateParameter = false)]
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        [Property("updated_by", SqlDbType.Int, AddParameter = false, UpdateParameter = true)]
        public int UpdatedBy
        {
            get
            {
                return updatedBy;
            }
            set
            {
                updatedBy = value;
            }
        }
        #endregion

        #region Composite Objects


        #endregion


        public override int GetIdentity()
        {
            return posCurrencyRateDetailsId;
        }
        public override void SetIdentity(int value)
        {
            posCurrencyRateDetailsId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            throw new NotImplementedException();
        }
    }
}