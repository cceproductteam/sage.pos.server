using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
using CentrixERP.Common.Business.Entity;

namespace CentrixERP.Business.Entity
{
    [EntityIManager("CentrixERP.Business.IManager.IPOSCurrencyManager,CentrixERP.Business.IManager", isLogicalDelete = true)]
    public class POSCurrency : EntityBase<POSCurrency, int>
    {
        #region Constructors

        public POSCurrency() { }

        public POSCurrency(int posCurrencyId)
        {
            this.posCurrencyId = posCurrencyId;
        }

        #endregion

        #region Private Properties

		private int posCurrencyId;

		private int currencyId;

		private string code;

		private string description;

		private string symbol;

		private bool? flagDeleted;

		private bool? isSystem;

		private DateTime? createdDate;

		private DateTime? updatedDate;

		private int updatedBy;

		private int createdBy;



        //private bool? flagDeleted;

        //private bool? isSystem;

        //private int createdBy;

        //private int updatedBy;

        //private DateTime? createdDate;

        //private DateTime? updatedDate;
        #endregion

        #region Public Properties

        [PrimaryKey("@pos_currency_id", SqlDbType.Int)]
[Property("pos_currency_id", SqlDbType.Int, AddParameter = false)]
		public int PosCurrencyId
		{
			get
			{
				return posCurrencyId;
			}
			set
			{
				posCurrencyId = value;
			}
		}

[Property("currency_id", SqlDbType.Int)]
[PropertyConstraint(false, "Currency")]
		public int CurrencyId
		{
			get
			{
				return currencyId;
			}
			set
			{
				currencyId = value;
			}
		}

[Property("code", SqlDbType.NVarChar)]
[PropertyConstraint(false, "Code", MaxLength = 100, MinLength = 3)]
		public string Code
		{
			get
			{
				return code;
			}
			set
			{
				code = value;
			}
		}

[Property("description", SqlDbType.NVarChar)]
[PropertyConstraint(false, "Description", MaxLength = 200, MinLength = 3)]
		public string Description
		{
			get
			{
				return description;
			}
			set
			{
				description = value;
			}
		}

[Property("symbol", SqlDbType.NVarChar)]
[PropertyConstraint(false, "Symbol", MaxLength = 50, MinLength = 3)]
		public string Symbol
		{
			get
			{
				return symbol;
			}
			set
			{
				symbol = value;
			}
		}



        [Property("flag_deleted", SqlDbType.Bit, AddParameter = true, UpdateParameter = true, FindParameter = true)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [Property("is_system", SqlDbType.Bit, AddParameter = false, UpdateParameter = false,FindParameter=false)]
        public bool? IsSystem
        {
            get
            {
                return isSystem;
            }
            set
            {
                isSystem = value;
            }
        }

		[Property("created_date",SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
		public DateTime? CreatedDate
		{
			get
			{
				return createdDate;
			}
			set
			{
				createdDate = value;
			}
		}

        [Property("updated_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
		public DateTime? UpdatedDate
		{
			get
			{
				return updatedDate;
			}
			set
			{
				updatedDate = value;
			}
		}

		[Property("created_by",SqlDbType.Int, AddParameter = true, UpdateParameter = false)]
		public int CreatedBy
		{
			get
			{
				return createdBy;
			}
			set
			{
				createdBy = value;
			}
		}

		[Property("updated_by",SqlDbType.Int, AddParameter = false, UpdateParameter = true)]
		public int UpdatedBy
		{
			get
			{
				return updatedBy;
			}
			set
			{
				updatedBy = value;
			}
		}
        #endregion

             #region Composite Objects
              

                 #endregion
   

        public override int GetIdentity()
        {
            return posCurrencyId;
        }
        public override void SetIdentity(int value)
        {
            posCurrencyId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            			throw new NotImplementedException();
        }
    }
}