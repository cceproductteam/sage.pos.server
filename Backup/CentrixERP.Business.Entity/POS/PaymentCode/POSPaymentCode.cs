using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
using CentrixERP.Common.Business.Entity;

namespace CentrixERP.Business.Entity
{
    [EntityIManager("CentrixERP.Business.IManager.IPaymentCodeManager,CentrixERP.Business.IManager", isLogicalDelete = true)]
    public class POSPaymentCode : EntityBase<POSPaymentCode, int>
    {
        #region Constructors

        public POSPaymentCode() { }

        public POSPaymentCode(int paymentCodeId)
        {
            this.paymentCodeId = paymentCodeId;
        }

        #endregion

        #region Private Properties

        private int paymentCodeId;

        private int posId;

        private string paymentCode;

        private string description;

        private bool? inactive;

        private int paymentTypeId = -1;

        private int createdBy;

        private DateTime? createdDate;

        private int updatedBy;

        private DateTime? updatedDate;

        private bool? flagDeleted;

        private bool? isSystem;



        //private bool? flagDeleted;

        //private bool? isSystem;

        //private int createdBy;

        //private int updatedBy;

        //private DateTime? createdDate;

        //private DateTime? updatedDate;
        #endregion

        #region Public Properties

        [PrimaryKey("@payment_code_id", SqlDbType.Int)]
        [Property("payment_code_id", SqlDbType.Int, AddParameter = false)]
        public int PaymentCodeId
        {
            get
            {
                return paymentCodeId;
            }
            set
            {
                paymentCodeId = value;
            }
        }

        [Property("payment_code", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "PaymentCode", MaxLength = 12, MinLength = 3)]
        public string PaymentCodeDesc
        {
            get
            {
                return paymentCode;
            }
            set
            {
                paymentCode = value;
            }
        }

        [Property("pos_id", SqlDbType.Int)]
        //[PropertyConstraint(false, "PaymentCode", MaxLength = 12, MinLength = 3)]
        public int PosId
        {
            get
            {
                return posId;
            }
            set
            {
                posId = value;
            }
        }

        [Property("description", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "Description", MaxLength = 60, MinLength = 3)]
        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }

        [Property("inactive", SqlDbType.Bit)]
        [PropertyConstraint(false, "Inactive")]
        public bool? Inactive
        {
            get
            {
                return inactive;
            }
            set
            {
                inactive = value;
            }
        }

        [Property("payment_type_id", SqlDbType.Int, ReferenceParameter = true)]
        [PropertyConstraint(false, "PaymentType", RefEntity = true, RefPropertyEntity = "PaymentTypeObj")]
        public int PaymentTypeId
        {
            get
            {
                return paymentTypeId;
            }
            set
            {
                paymentTypeId = value;
            }
        }



        [Property("flag_deleted", SqlDbType.Bit, AddParameter = true, UpdateParameter = true, FindParameter = true)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [Property("is_system", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool? IsSystem
        {
            get
            {
                return isSystem;
            }
            set
            {
                isSystem = value;
            }
        }

        [Property("created_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? CreatedDate
        {
            get
            {
                return createdDate;
            }
            set
            {
                createdDate = value;
            }
        }

        [Property("updated_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? UpdatedDate
        {
            get
            {
                return updatedDate;
            }
            set
            {
                updatedDate = value;
            }
        }

        [Property("created_by", SqlDbType.Int, AddParameter = true, UpdateParameter = false)]
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        [Property("updated_by", SqlDbType.Int, AddParameter = false, UpdateParameter = true)]
        public int UpdatedBy
        {
            get
            {
                return updatedBy;
            }
            set
            {
                updatedBy = value;
            }
        }
        #endregion

        #region Composite Objects
        private DataTypeContent paymentTypeObj;
        [CompositeObject(CompositObjectTypeEnum.Single, "PaymentTypeId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable = false)]
        public DataTypeContent PaymentTypeObj
        {
            get
            {
                if (paymentTypeObj == null)
                    loadCompositObject_Lazy("PaymentTypeObj");
                return paymentTypeObj;
            }
            set
            {
                paymentTypeObj = value; ;
            }
        }



        #endregion


        public override int GetIdentity()
        {
            return paymentCodeId;
        }
        public override void SetIdentity(int value)
        {
            paymentCodeId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            throw new NotImplementedException();
        }
    }
}