using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
using CentrixERP.Configuration;

namespace CentrixERP.Business.Entity
{
     public class POSPaymentCodeLite : EntityBaseLite
    {      
        
        #region Public Properties

         		[LiteProperty("payment_code_id",SqlDbType.Int)]
		public int PaymentCodeId
		{
			get;
			set;
		}

		[LiteProperty("payment_code",SqlDbType.NVarChar)]
		public string PaymentCode
		{
			get;
			set;
		}

		[LiteProperty("description",SqlDbType.NVarChar)]
		public string Description
		{
			get;
			set;
		}

		[LiteProperty("inactive",SqlDbType.Bit)]
		public bool? Inactive
		{
			get;
			set;
		}

		[LiteProperty("payment_type_id",SqlDbType.Int)]
		public int PaymentTypeId
		{
			get;
			set;
		}

		private string paymentType;

		private string paymentTypeAr;

		[LiteProperty("payment_type",SqlDbType.NVarChar)]
		public string PaymentType
		{
			get{return  paymentType;}
            
			set{ paymentType= value; }
		}

		[LiteProperty("payment_type_ar",SqlDbType.NVarChar)]
		public string PaymentTypeAr
		{
			get{return paymentTypeAr;}
            
			set{ paymentTypeAr= value; }
		}



        [LiteProperty("payment_code_id", SqlDbType.Int)]
        public int value
        {
            get;
            set;
        }

          [LiteProperty("payment_code_id", SqlDbType.Int)]
        public int label
        {
            get;
            set;
        }

         [LiteProperty("flag_deleted", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? FlagDeleted
        {
            get;
            set;
        }

        [LiteProperty("is_system",SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
		public bool? IsSystem
		{
			get;
			set;
		}

		[LiteProperty("created_date",SqlDbType.DateTime)]
		public DateTime? CreatedDate
		{
			get;
			set;
		}

		[LiteProperty("updated_date",SqlDbType.DateTime)]
		public DateTime? UpdatedDate
		{
			get;
			set;
		}

		[LiteProperty("created_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int CreatedBy
		{
			get;
			set;
		}

		[LiteProperty("updated_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int UpdatedBy
		{
			get;
			set;
		}

         [LiteProperty("created_by_name",SqlDbType.NVarChar, FindAllLiteParameter = false)]
         public string CreatedByName{get;set;}

         [LiteProperty("updated_by_name",SqlDbType.NVarChar,FindAllLiteParameter = false)]
         public string UpdatedByName{get;set;}

         [LiteProperty("payment_code_id",SqlDbType.NVarChar)]
          public int Id
        {
            get{return this.PaymentCodeId; }
            set{this.PaymentCodeId=value;}
        }

         [LiteProperty("last_rows",SqlDbType.NVarChar)]
         public bool LastRows { get; set; }

         [LiteProperty("total_count",SqlDbType.NVarChar)]
         public int TotalRecords { get; set; }

         [LiteProperty("pos_id", SqlDbType.NVarChar)]
         public int PosId
         {
             get;
             set;
         }
        #endregion
       
          
     }
}