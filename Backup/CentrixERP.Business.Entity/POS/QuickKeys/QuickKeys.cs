using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
using CentrixERP.Common.Business.Entity;

namespace CentrixERP.Business.Entity
{
    [EntityIManager("CentrixERP.Business.IManager.IQuickKeysManager,CentrixERP.Business.IManager", isLogicalDelete = true)]
    public class QuickKeys : EntityBase<QuickKeys, int>
    {
        #region Constructors

        public QuickKeys() { }

        public QuickKeys(int quickKeyId)
        {
            this.quickKeyId = quickKeyId;
        }

        #endregion

        #region Private Properties

        private int quickKeyId;

        private string name;

        private int createdBy;

        private int updatedBy;

        private DateTime? createdDate;

        private DateTime? updatedDate;

        private bool? flagDeleted;

        private bool? isSystem;



        //private bool? flagDeleted;

        //private bool? isSystem;

        //private int createdBy;

        //private int updatedBy;

        //private DateTime? createdDate;

        //private DateTime? updatedDate;
        #endregion

        #region Public Properties

        [PrimaryKey("@quick_key_id", SqlDbType.Int)]
        [Property("quick_key_id", SqlDbType.Int, AddParameter = false)]
        public int QuickKeyId
        {
            get
            {
                return quickKeyId;
            }
            set
            {
                quickKeyId = value;
            }
        }

        [Property("name", SqlDbType.NVarChar)]
        [PropertyConstraint(true, "Name", MaxLength = 150, MinLength = 3)]
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }



        [Property("flag_deleted", SqlDbType.Bit, AddParameter = true, UpdateParameter = true, FindParameter = true)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [Property("is_system", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool? IsSystem
        {
            get
            {
                return isSystem;
            }
            set
            {
                isSystem = value;
            }
        }

        [Property("created_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? CreatedDate
        {
            get
            {
                return createdDate;
            }
            set
            {
                createdDate = value;
            }
        }

        [Property("updated_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? UpdatedDate
        {
            get
            {
                return updatedDate;
            }
            set
            {
                updatedDate = value;
            }
        }

        [Property("created_by", SqlDbType.Int, AddParameter = true, UpdateParameter = false)]
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        [Property("updated_by", SqlDbType.Int, AddParameter = false, UpdateParameter = true)]
        public int UpdatedBy
        {
            get
            {
                return updatedBy;
            }
            set
            {
                updatedBy = value;
            }
        }
        #endregion

        #region Composite Objects
        //private List<QuickKeyProduct> quickKeyProductList;
        //[CompositeObject(CompositObjectTypeEnum.List, "QuickKeyId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true)]
        //public List<QuickKeyProduct> QuickKeyProductList
        //{
        //    get
        //    {
        //        if (quickKeyProductList == null)
        //            loadCompositObject_Lazy("QuickKeyProductList");
        //        return quickKeyProductList;
        //    }
        //    set
        //    {
        //        quickKeyProductList = value; ;
        //    }
        //}



        #endregion


        public override int GetIdentity()
        {
            return quickKeyId;
        }
        public override void SetIdentity(int value)
        {
            quickKeyId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            throw new NotImplementedException();
        }
    }
}