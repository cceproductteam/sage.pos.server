using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
using CentrixERP.Common.Business.Entity;
using Centrix.UM.Business.Entity;
using System.Web.Script.Serialization;

namespace CentrixERP.Business.Entity
{
    [EntityIManager("CentrixERP.Business.IManager.ICompanyManager,CentrixERP.Business.IManager", isLogicalDelete = true)]
    public class Company : EntityBase<Company, int>
    {
        #region Constructors

        public Company() { }

        public Company(int companyId)
        {
            this.companyId = companyId;
        }

        #endregion

        #region Private Properties

		private int companyId;

		private string companyNameEnglish;

		private string companyNameArabic;

		private string website;

		private string companyType;

		private int decisionMakerId = -1;

		private int salesManAccountManagerId = -1;

		private string abbreviation;

		private string registrationNumber;

		private DateTime? registrationDate;

		private int segmentId = -1;

		private int sourceId = -1;

		private int yearlyRevenueId = -1;

		private int employeeNumber = -1;

		private int statusId = -1;

        //private bool? isBranch;

        //private int parentCompanyId = -1;

        //private int paymentTerms = -1;

        //private int paymentType = -1;

		private bool? isSystem;

		private bool? flagDeleted;

		private int createdBy;

		private DateTime? createdDate;

		private int updatedBy;

		private DateTime? updatedDate;

        private string interestedIn;

        private int countryOfOrigion;
        private string facebookPage;
        private string linkedInPage;
        private string twitterAccount;


        private string mobileNumber;
        private string email;
        private bool isQuickAdd;
        private bool? isSync;
        private string uniqueNumber;

        //private int customerReferralCompanyId = -1;
        //private int customerReferralPersonId = -1;
        //private int employeeUserId = -1;
        //private string scoutingComments;

        #endregion

        #region Public Properties




        //[Property("unique_number", SqlDbType.NVarChar)]
        

        [PrimaryKey("@company_id", SqlDbType.Int)]
		[Property("company_id",SqlDbType.Int,AddParameter=false)]
		public int CompanyId
		{
			get
			{
				return companyId;
			}
			set
			{
				companyId = value;
			}
		}

        [Property("facebook_page", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "facebook_page", UiName = "Facebook Page", MaxLength = 60, MinLength = 3)]
        public string FacebookPage
        {
            get
            {
                return facebookPage;
            }
            set
            {
                facebookPage = value;
            }
        }

        [Property("linkedIn_page", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "linkedIn_page", UiName = "LinkedIn Page", MaxLength = 60, MinLength = 3)]
        public string LinkedInPage
        {
            get
            {
                return linkedInPage;
            }
            set
            {
                linkedInPage = value;
            }
        }

        [Property("twitter_account", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "twitter_account", UiName = "Twitter Account", MaxLength = 60, MinLength = 3)]
        public string TwitterAccount
        {
            get
            {
                return twitterAccount;
            }
            set
            {
                twitterAccount = value;
            }
        }

		
		[Property("company_name_english",SqlDbType.NVarChar)]
		[PropertyConstraint(true,"Company Name English",MaxLength=60)]
		public string CompanyNameEnglish
		{
			get
			{
				return companyNameEnglish;
			}
			set
			{
				companyNameEnglish = value;
			}
		}

		
		[Property("company_name_arabic",SqlDbType.NVarChar)]
		[PropertyConstraint(false,"Company Name Arabic",MaxLength=100)]
		public string CompanyNameArabic
		{
			get
			{
				return companyNameArabic;
			}
			set
			{
				companyNameArabic = value;
			}
		}

		
		[Property("website",SqlDbType.NVarChar)]
		[PropertyConstraint(false,"Website",MaxLength=100)]
		public string Website
		{
			get
			{
				return website;
			}
			set
			{
				website = value;
			}
		}

		
		[Property("company_type",SqlDbType.NVarChar)]
		[PropertyConstraint(true,"Company Type",MaxLength=100,MinLength=0)]
		public string CompanyType
		{
			get
			{
				return companyType;
			}
			set
			{
				companyType = value;
			}
		}

		
		[Property("decision_maker_id",SqlDbType.Int,ReferenceParameter=true)]
		[PropertyConstraint(false,"Decision Maker")]
		public int DecisionMaker
		{
			get
			{
				return decisionMakerId;
			}
			set
			{
				decisionMakerId = value;
			}
		}

        [Property("country_of_origion_id", SqlDbType.Int, ReferenceParameter = true)]
        [PropertyConstraint(false, "Country Of Origion")]
        public int CountryOfOrigion
        {
            get
            {
                return countryOfOrigion;
            }
            set
            {
                countryOfOrigion = value;
            }
        }


        [Property("sales_man_account_manager_id", SqlDbType.Int, ReferenceParameter = true)]
		[PropertyConstraint(false,"SalesMan Account Manager")]
		public int SalesManAccountManager
		{
			get
			{
				return salesManAccountManagerId;
			}
			set
			{
				salesManAccountManagerId = value;
			}
		}

		
		[Property("abbreviation",SqlDbType.NVarChar)]
		[PropertyConstraint(false,"Abbreviation",MaxLength=60)]
		public string Abbreviation
		{
			get
			{
				return abbreviation;
			}
			set
			{
				abbreviation = value;
			}
		}

		
		[Property("registration_number",SqlDbType.NVarChar)]
		[PropertyConstraint(false,"Registration Number",MaxLength=60)]
		public string RegistrationNumber
		{
			get
			{
				return registrationNumber;
			}
			set
			{
				registrationNumber = value;
			}
		}

		
		[Property("registration_date",SqlDbType.DateTime)]
		[PropertyConstraint(false,"Registration Date")]
		public DateTime? RegistrationDate
		{
			get
			{
				return registrationDate;
			}
			set
			{
				registrationDate = value;
			}
		}


        [Property("segment_id", SqlDbType.Int, ReferenceParameter = true)]
        [PropertyConstraint(false, "Segment", RefEntity = true, RefPropertyEntity = "SegmentObj")]
        public int Segment
        {
            get
            {
                return segmentId;
            }
            set
            {
                segmentId = value;
            }
        }


        [Property("source_id", SqlDbType.Int, ReferenceParameter = true)]
		[PropertyConstraint(false,"Source")]
		public int Source
		{
			get
			{
				return sourceId;
			}
			set
			{
				sourceId = value;
			}
		}


        [Property("yearly_revenue_id", SqlDbType.Int, ReferenceParameter = true)]
		[PropertyConstraint(false,"Yearly Revenue")]
		public int YearlyRevenue
		{
			get
			{
				return yearlyRevenueId;
			}
			set
			{
				yearlyRevenueId = value;
			}
		}


        [Property("employee_number", SqlDbType.Int, ReferenceParameter = true)]
		[PropertyConstraint(false,"Employee Number")]
		public int EmployeeNumber
		{
			get
			{
				return employeeNumber;
			}
			set
			{
				employeeNumber = value;
			}
		}


        [Property("status_id", SqlDbType.Int, ReferenceParameter = true)]
        [PropertyConstraint(false, "Status", RefEntity = true, RefPropertyEntity = "StatusObj")]
		public int Status
		{
			get
			{
				return statusId;
			}
			set
			{
				statusId = value;
			}
		}



        [Property("interested_in", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "InterestedIn", MaxLength = 100)]
        public string InterestedIn
        {
            get
            {
                return interestedIn;
            }
            set
            {
                interestedIn = value;
            }
        }
		
        //[Property("is_branch",SqlDbType.Bit)]
        //[PropertyConstraint(false,"Is Branch?")]
        //public bool? IsBranch
        //{
        //    get
        //    {
        //        return isBranch;
        //    }
        //    set
        //    {
        //        isBranch = value;
        //    }
        //}


        //[Property("parent_company_id", SqlDbType.Int, ReferenceParameter = true)]
        //[PropertyConstraint(false,"Parent Company")]
        //public int ParentCompany
        //{
        //    get
        //    {
        //        return parentCompanyId;
        //    }
        //    set
        //    {
        //        parentCompanyId = value;
        //    }
        //}


        //[Property("payment_terms", SqlDbType.Int, ReferenceParameter = true)]
        //[PropertyConstraint(true, "Payment Terms", RefEntity = true, RefPropertyEntity = "PaymentTermsObj")]
        //public int PaymentTerms
        //{
        //    get
        //    {
        //        return paymentTerms;
        //    }
        //    set
        //    {
        //        paymentTerms = value;
        //    }
        //}


        //[Property("payment_type", SqlDbType.Int, ReferenceParameter = true)]
        //[PropertyConstraint(true, "Payment Type", RefEntity = true, RefPropertyEntity = "PaymentTypeObj")]
        //public int PaymentType
        //{
        //    get
        //    {
        //        return paymentType;
        //    }
        //    set
        //    {
        //        paymentType = value;
        //    }
        //}



        [Property("flag_deleted", SqlDbType.Bit, AddParameter = false, UpdateParameter = false,FindParameter=false)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [Property("is_system", SqlDbType.Bit, AddParameter = false, UpdateParameter = false,FindParameter=false)]
        public bool? IsSystem
        {
            get
            {
                return isSystem;
            }
            set
            {
                isSystem = value;
            }
        }

		[Property("created_date",SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
		public DateTime? CreatedDate
		{
			get
			{
				return createdDate;
			}
			set
			{
				createdDate = value;
			}
		}

        [Property("updated_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
		public DateTime? UpdatedDate
		{
			get
			{
				return updatedDate;
			}
			set
			{
				updatedDate = value;
			}
		}

		[Property("created_by",SqlDbType.Int, AddParameter = true, UpdateParameter = false)]
		public int CreatedBy
		{
			get
			{
				return createdBy;
			}
			set
			{
				createdBy = value;
			}
		}

		[Property("updated_by",SqlDbType.Int, AddParameter = false, UpdateParameter = true)]
		public int UpdatedBy
		{
			get
			{
				return updatedBy;
			}
			set
			{
				updatedBy = value;
			}
		}


        //[Property("customer_referral_company_id", SqlDbType.Int, ReferenceParameter = true)]
        //[PropertyConstraint(false, "customer_referral_company_id", UiName = "Customer Referral Company", RefEntity = true, RefPropertyEntity = "CustomerReferralCompanyObj")]
        //public int CustomerReferralCompanyId
        //{
        //    get { return customerReferralCompanyId; }
        //    set { customerReferralCompanyId = value; }
        //}

        //[Property("customer_referral_person_id", SqlDbType.Int, ReferenceParameter = true)]
        //[PropertyConstraint(false, "customer_referral_person_id", UiName = "Customer Referral Person", RefEntity = true, RefPropertyEntity = "CustomerReferralPersonObj")]
        //public int CustomerReferralPersonId
        //{
        //    get { return customerReferralPersonId; }
        //    set { customerReferralPersonId = value; }
        //}

        //[Property("employee_user_id", SqlDbType.Int, ReferenceParameter = true)]
        //[PropertyConstraint(false, "employee_user_id", UiName = "Employee User", RefEntity = true, RefPropertyEntity = "EmployeeUserObj")]
        //public int EmployeeUserId
        //{
        //    get { return employeeUserId; }
        //    set { employeeUserId = value; }
        //}

        //[Property("scouting_comments", SqlDbType.NVarChar)]
        //[PropertyConstraint(false, "scouting_comments", UiName = "Scouting Comments", MaxLength = 60, MinLength = 3)]
        //public string ScoutingComments
        //{
        //    get { return scoutingComments; }
        //    set { scoutingComments = value; }
        //}

        [Property("unique_number", SqlDbType.NVarChar)]
        public string UniqueNumber
        {
            get{return uniqueNumber;}
            set { uniqueNumber = value; }
        }

        [Property("is_quick_add", SqlDbType.Bit)]
        public bool IsQuickAdd
        {
            get
            {
                return isQuickAdd;
            }
            set
            {
                isQuickAdd = value;
            }
        }

        [Property("email", SqlDbType.NVarChar)]
        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                email = value;
            }
        }

        [Property("mobile_number", SqlDbType.NVarChar)]
        public string MobileNumber
        {
            get
            {
                return mobileNumber;
            }
            set
            {
                mobileNumber = value;
            }
        }

        [Property("is_sync", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool? IsSync
        {
            get
            {
                return isSync;
            }
            set
            {
                isSync = value;
            }
        }

        #endregion

       #region Composite Objects
              		private Person personObj;
        //[CompositeObject(CompositObjectTypeEnum.Single, "DecisionMaker", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable= false)]
        //public Person PersonObj
        //{
        //    get
        //    {
        //        if (personObj == null)
        //            loadCompositObject_Lazy("PersonObj");
        //        return personObj;
        //    }
        //    set
        //    {
        //        personObj = value;;
        //    }
        //}
		
        //private User userObj;
        //[CompositeObject(CompositObjectTypeEnum.Single, "SalesManAccountManager", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable= false)]
        //public User UserObj
        //{
        //    get
        //    {
        //        if (userObj == null)
        //            loadCompositObject_Lazy("UserObj");
        //        return userObj;
        //    }
        //    set
        //    {
        //        userObj = value;;
        //    }
        //}
		
        //private DataTypeContent segmentObj;
        //[CompositeObject(CompositObjectTypeEnum.Single, "Segment", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable= false)]
        //public DataTypeContent SegmentObj
        //{
        //    get
        //    {
        //        if (segmentObj == null)
        //            loadCompositObject_Lazy("SegmentObj");
        //        return segmentObj;
        //    }
        //    set
        //    {
        //        segmentObj = value;;
        //    }
        //}
		
        //private DataTypeContent sourceObj;
        //[CompositeObject(CompositObjectTypeEnum.Single, "Source", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable= false)]
        //public DataTypeContent SourceObj
        //{
        //    get
        //    {
        //        if (sourceObj == null)
        //            loadCompositObject_Lazy("SourceObj");
        //        return sourceObj;
        //    }
        //    set
        //    {
        //        sourceObj = value;;
        //    }
        //}
		
        //private DataTypeContent yearlyRevenueObj;
        //[CompositeObject(CompositObjectTypeEnum.Single, "YearlyRevenue", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable= false)]
        //public DataTypeContent YearlyRevenueObj
        //{
        //    get
        //    {
        //        if (yearlyRevenueObj == null)
        //            loadCompositObject_Lazy("YearlyRevenueObj");
        //        return yearlyRevenueObj;
        //    }
        //    set
        //    {
        //        yearlyRevenueObj = value;;
        //    }
        //}
		
        //private DataTypeContent employeeNumberObj;
        //[CompositeObject(CompositObjectTypeEnum.Single, "EmployeeNumber", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable= false)]
        //public DataTypeContent EmployeeNumberObj
        //{
        //    get
        //    {
        //        if (employeeNumberObj == null)
        //            loadCompositObject_Lazy("EmployeeNumberObj");
        //        return employeeNumberObj;
        //    }
        //    set
        //    {
        //        employeeNumberObj = value;;
        //    }
        //}
		
        //private DataTypeContent statusObj;
        //[CompositeObject(CompositObjectTypeEnum.Single, "Status", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable= false)]
        //public DataTypeContent StatusObj
        //{
        //    get
        //    {
        //        if (statusObj == null)
        //            loadCompositObject_Lazy("StatusObj");
        //        return statusObj;
        //    }
        //    set
        //    {
        //        statusObj = value;;
        //    }
        //}
		
        //private Company companyObj;
        //[CompositeObject(CompositObjectTypeEnum.Single, "ParentCompany", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable= false)]
        //public Company CompanyObj
        //{
        //    get
        //    {
        //        if (companyObj == null)
        //            loadCompositObject_Lazy("CompanyObj");
        //        return companyObj;
        //    }
        //    set
        //    {
        //        companyObj = value;;
        //    }
        //}
		
        //private DataTypeContent paymentTermsObj;
        //[CompositeObject(CompositObjectTypeEnum.Single, "PaymentTerms", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable= false)]
        //public DataTypeContent PaymentTermsObj
        //{
        //    get
        //    {
        //        if (paymentTermsObj == null)
        //            loadCompositObject_Lazy("PaymentTermsObj");
        //        return paymentTermsObj;
        //    }
        //    set
        //    {
        //        paymentTermsObj = value;;
        //    }
        //}
		
        //private DataTypeContent paymentTypeObj;
        //[CompositeObject(CompositObjectTypeEnum.Single, "PaymentType", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable= false)]
        //public DataTypeContent PaymentTypeObj
        //{
        //    get
        //    {
        //        if (paymentTypeObj == null)
        //            loadCompositObject_Lazy("PaymentTypeObj");
        //        return paymentTypeObj;
        //    }
        //    set
        //    {
        //        paymentTypeObj = value;;
        //    }
        //}


        //private User employeeUserObj;
        //[CompositeObject(CompositObjectTypeEnum.Single, "EmployeeUserId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable = false)]
        //public User EmployeeUserObj
        //{
        //    get
        //    {
        //        if (employeeUserObj == null)
        //            loadCompositObject_Lazy("EmployeeUserObj");
        //        return employeeUserObj;
        //    }
        //    set
        //    {
        //        employeeUserObj = value; ;
        //    }
        //}


        //private Company customerReferralCompanyObj;
        //[CompositeObject(CompositObjectTypeEnum.Single, "CustomerReferralCompanyId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable = false)]
        //public Company CustomerReferralCompanyObj
        //{
        //    get
        //    {
        //        if (customerReferralCompanyObj == null)
        //            loadCompositObject_Lazy("CustomerReferralCompanyObj");
        //        return customerReferralCompanyObj;
        //    }
        //    set
        //    {
        //        customerReferralCompanyObj = value; ;
        //    }
        //}



        //private Person customerReferralPersonObj;
        //[CompositeObject(CompositObjectTypeEnum.Single, "CustomerReferralPersonId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable = false)]
        //public Person CustomerReferralPersonObj
        //{
        //    get
        //    {
        //        if (customerReferralPersonObj == null)
        //            loadCompositObject_Lazy("CustomerReferralPersonObj");
        //        return customerReferralPersonObj;
        //    }
        //    set
        //    {
        //        customerReferralPersonObj = value; ;
        //    }
        //}


        #endregion
                    //[ScriptIgnore]
                    //private List<EmailEntity> emailCompanyList;
                    //[CompositeObject(CompositObjectTypeEnum.List, "CompanyId", LazyLoad = true, CascadeDelete = true)]
                    //public List<EmailEntity> EmailCompanyList
                    //{
                    //    get
                    //    {
                    //        if (emailCompanyList == null)
                    //            loadCompositObject_Lazy("EmailCompanyList");
                    //        return emailCompanyList;
                    //    }
                    //    set { emailCompanyList = value; }
                    //}

        //[ScriptIgnore]
        //            private List<PhoneEntity> phoneCompanyList;
        //            [CompositeObject(CompositObjectTypeEnum.List, "CompanyId", LazyLoad = true, CascadeDelete = true)]
        //            public List<PhoneEntity> PhoneCompanyList
        //            {
        //                get
        //                {
        //                    if (phoneCompanyList == null)
        //                        loadCompositObject_Lazy("PhoneCompanyList");
        //                    return phoneCompanyList;
        //                }
        //                set { phoneCompanyList = value; }
        //            }

        
       

        
		

        public override int GetIdentity()
        {
            return companyId;
        }
        public override void SetIdentity(int value)
        {
            companyId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            			if (ParentType == typeof(Person))
				this.decisionMakerId = (int)value;
			else if (ParentType == typeof(User))
				this.salesManAccountManagerId = (int)value;
            //else if (ParentType == typeof(Company))
            //    this.parentCompanyId = (int)value;

        }
    }
}