using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
using CentrixERP.Configuration;


namespace CentrixERP.Business.Entity
{
    public class CompanyLite : EntityBaseLite
    {

        #region Public Properties


        [LiteProperty("company_id", SqlDbType.Int)]
        public int CompanyId
        {
            get;
            set;
        }

        [LiteProperty("company_name_english", SqlDbType.NVarChar)]
        public string CompanyNameEnglish
        {
            get;
            set;
        }

        [LiteProperty("company_name_arabic", SqlDbType.NVarChar)]
        public string CompanyNameArabic
        {
            get;
            set;
        }

        [LiteProperty("website", SqlDbType.NVarChar)]
        public string Website
        {
            get;
            set;
        }

        [LiteProperty("company_type", SqlDbType.NVarChar)]
        public string CompanyType
        {
            get;
            set;
        }

        [LiteProperty("company_type_en", SqlDbType.NVarChar)]
        public string CompanyTypeEn
        {
            get;
            set;
        }
        [LiteProperty("company_type_ar", SqlDbType.NVarChar)]
        public string CompanyTypeAr
        {
            get;
            set;
        }

        [LiteProperty("decision_maker_id", SqlDbType.Int)]
        public int DecisionMakerId
        {
            get;
            set;
        }

        [LiteProperty("decision_maker_name_en", SqlDbType.NVarChar)]
        public string DecisionMakerEN
        {
            get;
            set;
        }

        [LiteProperty("decision_maker_name_ar", SqlDbType.NVarChar)]
        public string DecisionMakerAR
        {
            get;
            set;
        }

        public string DecisionMaker
        {
            get
            {
                if (Configuration.Configuration.Lang.ToString() == "ar")
                    return DecisionMakerAR;
                else
                    return DecisionMakerEN;
            }
        }


        [LiteProperty("facebook_page", SqlDbType.Bit)]
        public string FacebookPage
        {
            get;
            set;
        }
        [LiteProperty("linkedIn_page", SqlDbType.Bit)]
        public string LinkedInPage
        {
            get;
            set;
        }
        [LiteProperty("twitter_account", SqlDbType.Bit)]
        public string TwitterAccount
        {
            get;
            set;
        }

        [LiteProperty("sales_man_account_manager_id", SqlDbType.Int)]
        public int SalesManAccountManagerId
        {
            get;
            set;
        }

        [LiteProperty("sales_man_name", SqlDbType.NVarChar)]
        public string SalesManAccountManager
        {
            get;
            set;
        }




        [LiteProperty("abbreviation", SqlDbType.NVarChar)]
        public string Abbreviation
        {
            get;
            set;
        }

        [LiteProperty("registration_number", SqlDbType.NVarChar)]
        public string RegistrationNumber
        {
            get;
            set;
        }

        [LiteProperty("registration_date", SqlDbType.DateTime)]
        public DateTime? RegistrationDate
        {
            get;
            set;
        }

        [LiteProperty("segment_id", SqlDbType.Int)]
        public int SegmentId
        {
            get;
            set;
        }

        [LiteProperty("segment_en", SqlDbType.NVarChar)]
        public string SegmentEN
        {
            get;
            set;
        }

        [LiteProperty("segment_ar", SqlDbType.NVarChar)]
        public string SegmentAR
        {
            get;
            set;
        }

        public string Segment
        {
            get
            {
                if (Configuration.Configuration.Lang.ToString() == "ar")
                    return SegmentAR;
                else
                    return SegmentEN;
            }
        }

        [LiteProperty("source_id", SqlDbType.Int)]
        public int SourceId
        {
            get;
            set;
        }

        [LiteProperty("source_en", SqlDbType.NVarChar)]
        public string SourceEN
        {
            get;
            set;
        }

        [LiteProperty("source_ar", SqlDbType.NVarChar)]
        public string SourceAR
        {
            get;
            set;
        }
        public string Source
        {
            get
            {
                if (Configuration.Configuration.Lang.ToString() == "ar")
                    return SourceAR;
                else
                    return SourceEN;
            }
        }

        [LiteProperty("yearly_revenue_id", SqlDbType.Int)]
        public int YearlyRevenueId
        {
            get;
            set;
        }


        [LiteProperty("yearly_revenue_en", SqlDbType.NVarChar)]
        public string YearlyRevenueEN
        {
            get;
            set;
        }
        [LiteProperty("yearly_revenue_ar", SqlDbType.NVarChar)]
        public string YearlyRevenueAR
        {
            get;
            set;
        }
        public string YearlyRevenue
        {
            get
            {
                if (Configuration.Configuration.Lang.ToString() == "ar")
                    return YearlyRevenueAR;
                else
                    return YearlyRevenueEN;
            }
        }


        [LiteProperty("employee_number", SqlDbType.Int)]
        public int EmployeeNumberId
        {
            get;
            set;
        }


        [LiteProperty("employee_number_en", SqlDbType.NVarChar)]
        public string EmployeeNumberEN
        {
            get;
            set;
        }

        [LiteProperty("employee_number_ar", SqlDbType.NVarChar)]
        public string EmployeeNumberAR
        {
            get;
            set;
        }

        public string EmployeeNumber
        {
            get
            {
                if (Configuration.Configuration.Lang.ToString() == "ar")
                    return EmployeeNumberAR;
                else
                    return EmployeeNumberEN;
            }
        }



        [LiteProperty("status_id", SqlDbType.Int)]
        public int StatusId
        {
            get;
            set;
        }
        [LiteProperty("status_en", SqlDbType.NVarChar)]
        public string StatusEN
        {
            get;
            set;
        }
        [LiteProperty("status_ar", SqlDbType.NVarChar)]
        public string StatusAR
        {
            get;
            set;
        }

        public string Status
        {
            get
            {
                if (Configuration.Configuration.Lang.ToString() == "ar")
                    return StatusAR;
                else
                    return StatusEN;
            }
        }

        [LiteProperty("interested_in_id", SqlDbType.NVarChar)]
        public string InterestedInId
        {
            get;
            set;
        }
          [LiteProperty("interestedInd_en", SqlDbType.NVarChar)]
        public string InterestedInEn
        {
            get;
            set;
        }

           [LiteProperty("interestedInd_ar", SqlDbType.NVarChar)]
        public string InterestedInAr
        {
            get;
            set;
        }


           public string InterestedIn
           {
               get {  if (Configuration.Configuration.Lang.ToString() == "ar")
                   return InterestedInAr;
                else
                   return InterestedInEn;
               }
               set { }
           }


           private List<CentrixERP.Common.Business.Entity.DataTypeContentLite> intresetedInList = null;
           public List<CentrixERP.Common.Business.Entity.DataTypeContentLite> IntresetedInList
           {
               get
               {
                   return intresetedInList;
               }
               set
               {
                   intresetedInList = value;
               }
           }






           [LiteProperty("country_of_origion_id", SqlDbType.Int)]
           public int CountryOfOrigionId
           {
               get;
               set;
           }

           [LiteProperty("country_of_origion_en", SqlDbType.NVarChar)]
           public string CountryOfOrigionEN
           {
               get;
               set;
           }

           [LiteProperty("country_of_origion_ar", SqlDbType.NVarChar)]
           public string CountryOfOrigionAR
           {
               get;
               set;
           }

           public string CountryOfOrigion
           {
               get
               {
                   if (Configuration.Configuration.Lang.ToString() == "ar")
                       return CountryOfOrigionAR;
                   else
                       return CountryOfOrigionEN;
               }
           }






        //[LiteProperty("is_branch_value", SqlDbType.Bit)]
        //public bool? IsBranchValue
        //{
        //    get;
        //    set;
        //}

        //[LiteProperty("is_branch_en", SqlDbType.NVarChar)]
        //public string IsBranchEN
        //{
        //    get;
        //    set;
        //}

        //[LiteProperty("is_branch_ar", SqlDbType.NVarChar)]
        //public string IsBranchAR
        //{
        //    get;
        //    set;
        //}


        //public string IsBranch
        //{
        //    get
        //    {
        //        if (Configuration.Configuration.Lang.ToString() == "ar")
        //            return IsBranchAR;
        //        else
        //            return IsBranchEN;
        //    }
        //}



        //[LiteProperty("parent_company_id", SqlDbType.Int)]
        //public int ParentCompanyId
        //{
        //    get;
        //    set;
        //}

        //[LiteProperty("parent_company_en", SqlDbType.NVarChar)]
        //public string ParentCompanyEN
        //{
        //    get;
        //    set;
        //}

        //[LiteProperty("parent_company_ar", SqlDbType.NVarChar)]
        //public string ParentCompanyAR
        //{
        //    get;
        //    set;
        //}

        //public string ParentCompany
        //{
        //    get
        //    {
        //        if (Configuration.Configuration.Lang.ToString() == "ar")
        //            return ParentCompanyAR;
        //        else
        //            return ParentCompanyEN;
        //    }
        //}


        //[LiteProperty("payment_terms", SqlDbType.Int)]
        //public int PaymentTermsId
        //{
        //    get;
        //    set;
        //}

        //[LiteProperty("payment_terms_en", SqlDbType.NVarChar)]
        //public string PaymentTermsEN
        //{
        //    get;
        //    set;
        //}

        //[LiteProperty("payment_terms_ar", SqlDbType.NVarChar)]
        //public string PaymentTermsAR
        //{
        //    get;
        //    set;
        //}

        //public string PaymentTerms
        //{
        //    get
        //    {
        //        if (Configuration.Configuration.Lang.ToString() == "ar")
        //            return PaymentTermsAR;
        //        else
        //            return PaymentTermsEN;
        //    }
        //}





        //[LiteProperty("payment_type", SqlDbType.Int)]
        //public int PaymentTypeId
        //{
        //    get;
        //    set;
        //}

        //[LiteProperty("payment_type_en", SqlDbType.NVarChar)]
        //public string PaymentTypeEN
        //{
        //    get;
        //    set;
        //}

        //[LiteProperty("payment_type_ar", SqlDbType.NVarChar)]
        //public string PaymentTypeAR
        //{
        //    get;
        //    set;
        //}

        //public string PaymentType
        //{
        //    get
        //    {
        //        if (Configuration.Configuration.Lang.ToString() == "ar")
        //            return PaymentTypeAR;
        //        else
        //            return PaymentTypeEN;
        //    }
        //}

        [LiteProperty("flag_deleted", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? FlagDeleted
        {
            get;
            set;
        }

        [LiteProperty("is_system", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? IsSystem
        {
            get;
            set;
        }

        [LiteProperty("created_date", SqlDbType.DateTime)]
        public DateTime? CreatedDate
        {
            get;
            set;
        }

        [LiteProperty("updated_date", SqlDbType.DateTime)]
        public DateTime? UpdatedDate
        {
            get;
            set;
        }

        [LiteProperty("created_by", SqlDbType.Int, FindByIdLiteParameter = false)]
        public int CreatedBy
        {
            get;
            set;
        }

        [LiteProperty("updated_by", SqlDbType.Int, FindByIdLiteParameter = false)]
        public int? UpdatedBy
        {
            get;
            set;
        }

        [LiteProperty("created_by_name", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string CreatedByName { get; set; }

        [LiteProperty("updated_by_name", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string  UpdatedByName { get; set; }

        [LiteProperty("company_id", SqlDbType.NVarChar)]
        public int Id
        {
            get { return this.CompanyId; }
            set { this.CompanyId = value; }
        }

        [LiteProperty("last_rows", SqlDbType.NVarChar)]
        public bool LastRows { get; set; }

        [LiteProperty("total_count", SqlDbType.NVarChar)]
        public int TotalRecords { get; set; }
        /// <summary>
        /// ///////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// </summary>
        public string label
        {
            get
            {
              
                    return CompanyNameEnglish;
            }
        }
        public int value
        {
            get { return CompanyId; }

        }


        [LiteProperty("business_email", SqlDbType.NVarChar, FindAllLiteParameter = true)]
        public string BusinessEmail
        {
            get;
            set;
        }

        [LiteProperty("personal_email", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string PersonalEmail
        {
            get;
            set;
        }

        [LiteProperty("mobile_number", SqlDbType.NVarChar, FindAllLiteParameter = true)]
        public string MobileNumber
        {
            get;
            set;
        }
        [LiteProperty("country_mobile_number", SqlDbType.NVarChar, FindAllLiteParameter = true)]
        public string CountryMobileNumber
        {
            get;
            set;
        }
        [LiteProperty("area_mobile_number", SqlDbType.NVarChar, FindAllLiteParameter = true)]
        public string AreaMobileNumber
        {
            get;
            set;
        }
        [LiteProperty("phone_number", SqlDbType.NVarChar, FindAllLiteParameter = true)]
        public string HomeNumber
        {
            get;
            set;
        }
        [LiteProperty("country_phone_number", SqlDbType.NVarChar, FindAllLiteParameter = true, FindByIdLiteParameter = false)]
        public string CountryHomeNumber
        {
            get;
            set;
        }
        [LiteProperty("area_phone_number", SqlDbType.NVarChar, FindAllLiteParameter = true)]
        public string AreaHomeNumber
        {
            get;
            set;
        }
        [LiteProperty("fax_number", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string FaxNumber
        {
            get;
            set;
        }
        [LiteProperty("country_fax_number", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string CountryFaxNumber
        {
            get;
            set;
        }
        [LiteProperty("area_fax_number", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string AreaFaxNumber
        {
            get;
            set;
        }

        [LiteProperty("address_country", SqlDbType.Int, FindAllLiteParameter = false)]
        public int CountryId
        {
            get;
            set;
        }
        [LiteProperty("address_country_en", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string CountryEn { set; get; }


        [LiteProperty("address_country_ar", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string CountryAr { set; get; }
        public string Country
        {
            get
            {
                if (Configuration.Configuration.Lang.ToString() == "ar")
                    return CountryAr;
                else
                    return CountryEn;
            }
        }


        [LiteProperty("address_city_text", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string AddressCityText
        {
            get;
            set;
        }

        [LiteProperty("address_street_address", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string AddressStreetAddress
        {
            get;
            set;
        }

        [LiteProperty("address_nearby", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string NearBy
        {
            get;
            set;
        }

        [LiteProperty("address_place", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string Place
        {
            get;
            set;
        }


        [LiteProperty("address_building_number", SqlDbType.Int, FindAllLiteParameter = false)]
        public int? BuildingNumber
        {
            get;
            set;
        }

        [LiteProperty("address_pobox", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string POBox
        {
            get;
            set;
        }

        [LiteProperty("address_zip_code", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string ZipCode
        {
            get;
            set;
        }

        [LiteProperty("address_region", SqlDbType.NVarChar,FindAllLiteParameter = false)]
        public string Region
        {
            get;
            set;
        }

        [LiteProperty("address_area", SqlDbType.NVarChar,FindAllLiteParameter=false)]
        public string Area
        {
            get;
            set;
        }

        //[LiteProperty("customer_referral_company_id", SqlDbType.Int)]
        //public int CustomerReferralCompanyId { get; set; }

        //[LiteProperty("customer_referral_person_id", SqlDbType.Int)]
        //public int CustomerReferralPersonId { get; set; }

        //[LiteProperty("employee_user_id", SqlDbType.Int)]
        //public int EmployeeUserId { get; set; }


        //[LiteProperty("scouting_comments", SqlDbType.NVarChar)]
        //public string ScoutingComments { get; set; }


        //[LiteProperty("customer_referral_company_ar", SqlDbType.NVarChar)]
        //public string CustomerReferralCompanyAR { get; set; }

        //[LiteProperty("customer_referral_company_en", SqlDbType.NVarChar)]
        //public string CustomerReferralCompanyEN { get; set; }

        //public string CustomerReferralCompany
        //{
        //    get
        //    {
        //        if (Configuration.Configuration.Lang.ToString() == "ar")
        //            return CustomerReferralCompanyAR;
        //        else
        //            return CustomerReferralCompanyEN;
        //    }
        //}

        //[LiteProperty("customer_referral_person_en", SqlDbType.NVarChar)]
        //public string CustomerReferralPersonEN { get; set; }

        //[LiteProperty("customer_referral_person_ar", SqlDbType.NVarChar)]
        //public string CustomerReferralPersonAR { get; set; }

        //public string CustomerReferralPerson
        //{
        //    get
        //    {
        //        if (Configuration.Configuration.Lang.ToString() == "ar")
        //            return CustomerReferralPersonAR;
        //        else
        //            return CustomerReferralPersonEN;
        //    }
        //}

        //[LiteProperty("employee_user_en", SqlDbType.NVarChar)]
        //public string EmployeeUser { get; set; }

        [LiteProperty("is_quick_add", SqlDbType.Bit)]
        public bool IsQuickAdd
        {
            get;
            set;

        }
        [LiteProperty("email", SqlDbType.NVarChar)]
        public string Email
        {
            get;
            set;

        }

        



        #endregion
        private List<CentrixERP.Common.Business.Entity.DataTypeContentLite> companytypesList = null;
        public List<CentrixERP.Common.Business.Entity.DataTypeContentLite> CompanytypesList
        {
            get
            {
                return companytypesList;
            }
            set
            {
                companytypesList = value;
            }
        }

    }
}