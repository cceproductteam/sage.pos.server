using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
using CentrixERP.Common.Business.Factory;

namespace CentrixERP.Business.Entity
{
    public class CompanyCriteria : CriteriaBase, IAdvancedSearchCriteria
    {
        [CrtiteriaField("@page_number", SqlDbType.Int)]
        public int pageNumber { get; set; }

        [CrtiteriaField("@result_count", SqlDbType.Int)]
        public int resultCount { get; set; }

        [CrtiteriaField("@person_name", SqlDbType.NVarChar)]
        public string CompanyName { get; set; }

        [CrtiteriaField("@mobile_number", SqlDbType.NVarChar)]
        public string MobileNumber { get; set; }

        [CrtiteriaField("@keyword", SqlDbType.NVarChar)]
        public string Keyword { get; set; }

        [CrtiteriaField("@search_criteria", SqlDbType.NVarChar)]
        public string SearchCriteria { get; set; }

        [CrtiteriaField("@sort_type", SqlDbType.Bit)]
        public bool? SortType { get; set; }

        [CrtiteriaField("@sort_fields", SqlDbType.NVarChar)]
        public string SortFields { get; set; }

        [CrtiteriaField("@company_id", SqlDbType.Int, IsReferance = true)]
        public int? CompanyId { get; set; }

        [CrtiteriaField("@company_id", SqlDbType.Int, IsReferance = true)]
        public int? EntityId { get; set; }


        [CrtiteriaField("@excipt_id", SqlDbType.Int)]
        public int? ExciptId { get; set; }
        [CrtiteriaField("@is_quick_add", SqlDbType.Bit)]
        public bool? IsQuickAdd { get; set; }

        [CrtiteriaField("@is_synced", SqlDbType.Bit)]
        public bool? IsSynced { get; set; }

        [CrtiteriaField("@last_sync_date", SqlDbType.DateTime)]
        public DateTime? LastSyncDate { get; set; }


        public int NotificationOccasion { get; set; }


        int IAdvancedSearchCriteria.EntityId
        {
            get;
            set;
        }

        bool IAdvancedSearchCriteria.SortType
        {
            get;
            set;
        }

        //int IAdvancedSearchCriteria.pageNumber
        //{
        //    get;
        //    set;
        //}

        //int IAdvancedSearchCriteria.resultCount
        //{
        //    get;
        //    set;
        //}
    }
}
