using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace CentrixERP.Business.Entity
{
    public class POSPriceListLite : EntityBaseLite
    {

        #region Public Properties

        [LiteProperty("pos_price_list_id", SqlDbType.Int)]
        public int PosPriceListId
        {
            get;
            set;
        }

        [LiteProperty("price_list_group_id", SqlDbType.Int)]
        public int PriceListGroupId
        {
            get;
            set;
        }

        [LiteProperty("price_list_id", SqlDbType.Int)]
        public int PriceListId
        {
            get;
            set;
        }

        [LiteProperty("price_list_code", SqlDbType.NVarChar)]
        public string PriceListCode
        {
            get;
            set;
        }

        [LiteProperty("price_list_description", SqlDbType.NVarChar)]
        public string PriceListDescription
        {
            get;
            set;
        }

        [LiteProperty("currency_id", SqlDbType.Int)]
        public int CurrencyId
        {
            get;
            set;
        }

        [LiteProperty("currency", SqlDbType.NVarChar)]
        public string Currency
        {
            get;
            set;
        }

        [LiteProperty("base_price", SqlDbType.Money)]
        public decimal? BasePrice
        {
            get;
            set;
        }

        [LiteProperty("unit_of_measure", SqlDbType.NVarChar)]
        public string UnitOfMeasure
        {
            get;
            set;
        }



        [LiteProperty("pos_price_list_id", SqlDbType.Int)]
        public int value
        {
            get;
            set;
        }

        [LiteProperty("price_list_code", SqlDbType.NVarChar)]
        public string label
        {
            get;
            set;
        }

        [LiteProperty("flag_deleted", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? FlagDeleted
        {
            get;
            set;
        }

        [LiteProperty("is_system", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? IsSystem
        {
            get;
            set;
        }

        [LiteProperty("created_date", SqlDbType.DateTime)]
        public DateTime? CreatedDate
        {
            get;
            set;
        }

        [LiteProperty("updated_date", SqlDbType.DateTime)]
        public DateTime? UpdatedDate
        {
            get;
            set;
        }

        [LiteProperty("created_by", SqlDbType.Int, FindByIdLiteParameter = false)]
        public int CreatedBy
        {
            get;
            set;
        }

        [LiteProperty("updated_by", SqlDbType.Int, FindByIdLiteParameter = false)]
        public int UpdatedBy
        {
            get;
            set;
        }

        [LiteProperty("created_by_name", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string CreatedByName { get; set; }

        [LiteProperty("updated_by_name", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string UpdatedByName { get; set; }

        [LiteProperty("pos_price_list_id", SqlDbType.Int)]
        public int Id
        {
            get { return this.PosPriceListId; }
            set { this.PosPriceListId = value; }
        }

        [LiteProperty("last_rows", SqlDbType.NVarChar)]
        public bool LastRows { get; set; }

        [LiteProperty("total_count", SqlDbType.NVarChar)]
        public int TotalRecords { get; set; }
        #endregion


    }
}