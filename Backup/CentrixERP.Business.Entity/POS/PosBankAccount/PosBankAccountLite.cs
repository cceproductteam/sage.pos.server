using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace CentrixERP.Business.Entity
{
     public class PosBankAccountLite : EntityBaseLite
    {      
        
        #region Public Properties

         		[LiteProperty("pos_bank_account_id",SqlDbType.Int)]
		public int PosBankAccountId
		{
			get;
			set;
		}

		[LiteProperty("bank_id",SqlDbType.Int)]
		public int BankId
		{
			get;
			set;
		}

		[LiteProperty("bank_code",SqlDbType.NVarChar)]
		public string BankCode
		{
			get;
			set;
		}

		[LiteProperty("description",SqlDbType.NVarChar)]
		public string Description
		{
			get;
			set;
		}



        //[LiteProperty("pos_bank_account_id", SqlDbType.Int)]
        public int value
        {
            get { return BankId; }
        }

          //[LiteProperty("bank_code", SqlDbType.NVarChar)]
        public string label
        {
            get { return BankCode; }
        }

         [LiteProperty("flag_deleted", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? FlagDeleted
        {
            get;
            set;
        }

        [LiteProperty("is_system",SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
		public bool? IsSystem
		{
			get;
			set;
		}

		[LiteProperty("created_date",SqlDbType.DateTime)]
		public DateTime? CreatedDate
		{
			get;
			set;
		}

		[LiteProperty("updated_date",SqlDbType.DateTime)]
		public DateTime? UpdatedDate
		{
			get;
			set;
		}

		[LiteProperty("created_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int CreatedBy
		{
			get;
			set;
		}

		[LiteProperty("updated_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int UpdatedBy
		{
			get;
			set;
		}

         [LiteProperty("created_by_name",SqlDbType.NVarChar, FindAllLiteParameter = false)]
         public string CreatedByName{get;set;}

         [LiteProperty("updated_by_name",SqlDbType.NVarChar,FindAllLiteParameter = false)]
         public string UpdatedByName{get;set;}

         [LiteProperty("pos_bank_account_id",SqlDbType.NVarChar)]
          public int Id
        {
            get{return this.PosBankAccountId; }
            set{this.PosBankAccountId=value;}
        }

         [LiteProperty("last_rows",SqlDbType.NVarChar)]
         public bool LastRows { get; set; }

         [LiteProperty("total_count",SqlDbType.NVarChar)]
         public int TotalRecords { get; set; }
        #endregion
       
          
     }
}