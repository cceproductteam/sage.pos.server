using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace CentrixERP.Business.Entity
{
     public class PosSyncConfigurationLite : EntityBaseLite
    {      
        
        #region Public Properties

         		[LiteProperty("pos_sync_configuration_id",SqlDbType.Int)]
		public int PosSyncConfigurationId
		{
			get;
			set;
		}

		[LiteProperty("pos_sync_time1_id",SqlDbType.Int)]
		public int PosSyncTime1Id
		{
			get;
			set;
		}

		[LiteProperty("pos_sync_time1",SqlDbType.NVarChar)]
		public string PosSyncTime1
		{
			get;
			set;
		}

         [LiteProperty("last_Pos_sync_date", SqlDbType.NVarChar)]
        public string LastPosSyncDate
		{
			get;
			set;
		}

         [LiteProperty("last_Erp_sync_date", SqlDbType.NVarChar)]
         public string LastErpSyncDate
         {
             get;
             set;
         }

		[LiteProperty("pos_sync_time2_id",SqlDbType.Int)]
		public int PosSyncTime2Id
		{
			get;
			set;
		}

		[LiteProperty("pos_sync_time2",SqlDbType.NVarChar)]
		public string PosSyncTime2
		{
			get;
			set;
		}

		[LiteProperty("pos_sync_time3_id",SqlDbType.Int)]
		public int PosSyncTime3Id
		{
			get;
			set;
		}

		[LiteProperty("pos_sync_time3",SqlDbType.NVarChar)]
		public string PosSyncTime3
		{
			get;
			set;
		}

		[LiteProperty("pos_sync_time4_id",SqlDbType.Int)]
		public int PosSyncTime4Id
		{
			get;
			set;
		}

		[LiteProperty("pos_sync_time4",SqlDbType.NVarChar)]
		public string PosSyncTime4
		{
			get;
			set;
		}

		[LiteProperty("pos_sync_time5_id",SqlDbType.Int)]
		public int PosSyncTime5Id
		{
			get;
			set;
		}

		[LiteProperty("pos_sync_time5",SqlDbType.NVarChar)]
		public string PosSyncTime5
		{
			get;
			set;
		}

		[LiteProperty("pos_data_to_sync",SqlDbType.Int)]
		public int PosDataToSync
		{
			get;
			set;
		}

		[LiteProperty("sync_data_from_accpac",SqlDbType.Bit)]
		public bool? SyncDataFromAccpac
		{
			get;
			set;
		}

		[LiteProperty("accpac_sync_time1_id",SqlDbType.Int)]
		public int AccpacSyncTime1Id
		{
			get;
			set;
		}

		[LiteProperty("accpac_sync_time1",SqlDbType.NVarChar)]
		public string AccpacSyncTime1
		{
			get;
			set;
		}

		[LiteProperty("accpac_sync_time2_id",SqlDbType.Int)]
		public int AccpacSyncTime2Id
		{
			get;
			set;
		}

		[LiteProperty("accpac_sync_time2",SqlDbType.NVarChar)]
		public string AccpacSyncTime2
		{
			get;
			set;
		}

		[LiteProperty("accpac_sync_time3_id",SqlDbType.Int)]
		public int AccpacSyncTime3Id
		{
			get;
			set;
		}

		[LiteProperty("accpac_sync_time3",SqlDbType.NVarChar)]
		public string AccpacSyncTime3
		{
			get;
			set;
		}

		[LiteProperty("accpac_sync_time4_id",SqlDbType.Int)]
		public int AccpacSyncTime4Id
		{
			get;
			set;
		}

		[LiteProperty("accpac_sync_time4",SqlDbType.NVarChar)]
		public string AccpacSyncTime4
		{
			get;
			set;
		}

		[LiteProperty("accpac_sync_time5_id",SqlDbType.Int)]
		public int AccpacSyncTime5Id
		{
			get;
			set;
		}

		[LiteProperty("accpac_sync_time5",SqlDbType.NVarChar)]
		public string AccpacSyncTime5
		{
			get;
			set;
		}

		[LiteProperty("accpac_data_to_sync_ids",SqlDbType.NVarChar)]
		public string AccpacDataToSyncIds
		{
			get;
			set;
		}

		[LiteProperty("accpac_data_to_sync",SqlDbType.NVarChar)]
		public string AccpacDataToSync
		{
			get;
			set;
		}

		[LiteProperty("status",SqlDbType.Int)]
		public int Status
		{
			get;
			set;
		}



        [LiteProperty("pos_sync_configuration_id", SqlDbType.Int)]
        public int value
        {
            get;
            set;
        }

          [LiteProperty("pos_sync_configuration_id", SqlDbType.Int)]
        public int label
        {
            get;
            set;
        }

         [LiteProperty("flag_deleted", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? FlagDeleted
        {
            get;
            set;
        }

        [LiteProperty("is_system",SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
		public bool? IsSystem
		{
			get;
			set;
		}

		[LiteProperty("created_date",SqlDbType.DateTime)]
		public DateTime? CreatedDate
		{
			get;
			set;
		}

		[LiteProperty("updated_date",SqlDbType.DateTime)]
		public DateTime? UpdatedDate
		{
			get;
			set;
		}

		[LiteProperty("created_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int CreatedBy
		{
			get;
			set;
		}

		[LiteProperty("updated_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int UpdatedBy
		{
			get;
			set;
		}

         [LiteProperty("created_by_name",SqlDbType.NVarChar, FindAllLiteParameter = false)]
         public string CreatedByName{get;set;}

         [LiteProperty("updated_by_name",SqlDbType.NVarChar,FindAllLiteParameter = false)]
         public string UpdatedByName{get;set;}

         [LiteProperty("pos_sync_configuration_id",SqlDbType.NVarChar)]
          public int Id
        {
            get{return this.PosSyncConfigurationId; }
            set{this.PosSyncConfigurationId=value;}
        }

         [LiteProperty("last_rows",SqlDbType.NVarChar)]
         public bool LastRows { get; set; }

         [LiteProperty("total_count",SqlDbType.NVarChar)]
         public int TotalRecords { get; set; }
        #endregion
       
          
     }
}