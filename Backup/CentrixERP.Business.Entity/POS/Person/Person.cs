using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
using CentrixERP.Common.Business.Entity;
using Centrix.UM.Business.Entity;
using System.Web.Script.Serialization;

namespace CentrixERP.Business.Entity
{
    [EntityIManager("CentrixERP.Business.IManager.IPersonManager,CentrixERP.Business.IManager", isLogicalDelete = true)]
    public class Person : EntityBase<Person, int>
    {
        #region Constructors

        public Person() { }

        public Person(int personId)
        {
            this.personId = personId;
        }

        #endregion

        #region Private Properties

        private int personId;

        private int companyId = -1;

        private string firstNameEn;

        private string middleNameEn;

        private string lastNameEn;

        private string firstNameAr;

        private string middleNameAr;

        private string lastNameAr;

        private string fullNameEn;

        private string fullNameAr;





        private int? primaryContactEmployee;

        private int salutationId = -1;

        private int genderId = -1;

        private int sourceId = -1;

        private DateTime? dateOfBirth;

        private int religionId = -1;

        private int maritalStatusId = -1;

        private int departmentId = -1;

        private int positionId = -1;

        private int contactImageId = -1;

        private bool? flagDeleted;
        private bool? isSync;

        private bool? isSystem;

        private int createdBy;

        private int updatedBy;

        private DateTime? createdDate;

        private DateTime? updatedDate;

        private string interestedIn;
        private DateTime? anniversary;
        private int? nationalityId;
        private int? contactType;
        private bool notifyForAnniversary;
        private bool notifyForBirthday;
        private int hearAboutUsId;
        private int? bestTimeToContactId;
        private string facebookPage;
        private string linkedInPage;
        private string twitterAccount;
        private string website;
        private int accountManagerId;
        //private bool? isVip;
        private int vipClassId;

        private string mobileNumber;
        private string email;
        private bool isQuickAdd;
     


        #endregion

        #region Public Properties




        [Property("is_quick_add", SqlDbType.Bit)]
        public bool IsQuickAdd
        {
            get
            {
                return isQuickAdd;
            }
            set
            {
                isQuickAdd = value;
            }
        }

        [Property("unique_number", SqlDbType.NVarChar)]
        public string UniqueNumber
        {
            get;
            set;
        }

        [Property("email", SqlDbType.NVarChar)]
        public string Email
        {
            get
            {
                return email ;
            }
            set
            {
                email = value;
            }
        }

        [Property("mobile_number", SqlDbType.NVarChar)]
        public string MobileNumber
        {
            get
            {
                return mobileNumber;
            }
            set
            {
                mobileNumber = value;
            }
        }


        //[Property("interested_in", SqlDbType.Int, ReferenceParameter = true)]
        //[PropertyConstraint(false, "InterestedIn", RefEntity = true)]



        [PrimaryKey("@person_id", SqlDbType.Int)]
        [Property("person_id", SqlDbType.Int, AddParameter = false)]
        public int PersonId
        {
            get
            {
                return personId;
            }
            set
            {
                personId = value;
            }
        }
        [Property("interested_in", SqlDbType.NVarChar)]
        public string InterstedIn
        {
            get
            {
                return interestedIn;
            }
            set
            {
                interestedIn = value;
            }
        }

        [Property("anniversary", SqlDbType.DateTime)]
        public DateTime? Anniversary
        {
            get
            {
                return anniversary;
            }
            set
            {
                anniversary = value;
            }
        }

        [Property("nationality", SqlDbType.Int, ReferenceParameter = true)]
        [PropertyConstraint(false, "NationalityId", RefEntity = true)]
        public int? NationalityId
        {
            get
            {
                return nationalityId;
            }
            set
            {
                nationalityId = value;
            }
        }


        [Property("contact_type", SqlDbType.Int, ReferenceParameter=true)]
        [PropertyConstraint(false, "ContactTypeId", RefEntity = true)]
        public int? ContactTypeId
        {
            get
            {
                return contactType;
            }
            set
            {
                contactType = value;
            }
        }

        [Property("vip_class_id", SqlDbType.Int, ReferenceParameter = false)]
        [PropertyConstraint(false, "VipClassId", RefEntity = false)]
        public int VipClassId
        {
            get
            {
                return vipClassId;
            }
            set
            {
                vipClassId = value;
            }
        }


        [Property("notify_for_birthday", SqlDbType.Bit)]
        public bool NotifyForBirthday
        {
            get
            {
                return notifyForBirthday;
            }
            set
            {
                notifyForBirthday = value;
            }
        }



        [Property("notify_for_anniversary", SqlDbType.Bit)]
        public bool NotifyForAnniversary
        {
            get
            {
                return notifyForAnniversary;
            }
            set
            {
                notifyForAnniversary = value;
            }
        }



       

        [Property("company_id", SqlDbType.Int, ReferenceParameter = true)]
        [PropertyConstraint(false, "company_id", UiName = "Company", RefEntity = true, RefPropertyEntity = "CompanyObj")]
        public int CompanyId
        {
            get
            {
                return companyId;
            }
            set
            {
                companyId = value;
            }
        }

        [Property("first_name_en", SqlDbType.NVarChar)]
        [PropertyConstraint(true, "first_name_en", UiName = "First Name English", MaxLength = 60, MinLength = 3)]
        public string FirstNameEn
        {
            get
            {
                return firstNameEn;
            }
            set
            {
                firstNameEn = value;
            }
        }

        [Property("middle_name_en", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "middle_name_en", UiName = "Middle Name English", MaxLength = 60, MinLength = 3)]
        public string MiddleNameEn
        {
            get
            {
                return middleNameEn;
            }
            set
            {
                middleNameEn = value;
            }
        }

        [Property("last_name_en", SqlDbType.NVarChar)]
        [PropertyConstraint(true, "last_name_en", UiName = "Last Name English", MaxLength = 60, MinLength = 3)]
        public string LastNameEn
        {
            get
            {
                return lastNameEn;
            }
            set
            {
                lastNameEn = value;
            }
        }

        [Property("first_name_ar", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "first_name_ar", UiName = "First Name Arabic", MaxLength = 60, MinLength = 3)]
        public string FirstNameAr
        {
            get
            {
                return firstNameAr;
            }
            set
            {
                firstNameAr = value;
            }
        }

        [Property("middle_name_ar", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "middle_name_ar", UiName = "Middle Name Arabic", MaxLength = 60, MinLength = 3)]
        public string MiddleNameAr
        {
            get
            {
                return middleNameAr;
            }
            set
            {
                middleNameAr = value;
            }
        }

        [Property("last_name_ar", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "last_name_ar", UiName = "Last Name Arabic", MaxLength = 60, MinLength = 3)]
        public string LastNameAr
        {
            get
            {
                return lastNameAr;
            }
            set
            {
                lastNameAr = value;
            }
        }

        [Property("full_name_en", SqlDbType.NVarChar)]
        public string FullNameEn
        {
            get
            {
                return fullNameEn;
            }
            set
            {
                fullNameEn = value;
            }
        }

        [Property("full_name_ar", SqlDbType.NVarChar)]
        public string FullNameAr
        {
            get
            {
                return fullNameAr;
            }
            set
            {
                fullNameAr = value;
            }
        }


        [Property("primary_contact_employee", SqlDbType.Int)]
        [PropertyConstraint(false, "primary_contact_employee", UiName = "Primary Contact Employee")]
        public int? PrimaryContactEmployee
        {
            get
            {
                return primaryContactEmployee;
            }
            set
            {
                primaryContactEmployee = value;
            }
        }

        [Property("salutation_id", SqlDbType.Int, ReferenceParameter = true)]
        [PropertyConstraint(false, "salutation_id", UiName = "Salutation", RefEntity = true, RefPropertyEntity = "SalutationObj")]
        public int SalutationId
        {
            get
            {
                return salutationId;
            }
            set
            {
                salutationId = value;
            }
        }

        [Property("gender_id", SqlDbType.Int, ReferenceParameter = true)]
        [PropertyConstraint(false, "gender_id", UiName = "Gender", RefEntity = true, RefPropertyEntity = "GenderObj")]
        public int GenderId
        {
            get
            {
                return genderId;
            }
            set
            {
                genderId = value;
            }
        }

        [Property("source_id", SqlDbType.Int, ReferenceParameter = true)]
        [PropertyConstraint(false, "source_id", UiName = "Source", RefEntity = true, RefPropertyEntity = "SourceObj")]
        public int SourceId
        {
            get
            {
                return sourceId;
            }
            set
            {
                sourceId = value;
            }
        }

        [Property("date_of_birth", SqlDbType.DateTime)]
        [PropertyConstraint(false, "date_of_birth", UiName = "Date Of Birth")]
        public DateTime? DateOfBirth
        {
            get
            {
                return dateOfBirth;
            }
            set
            {
                dateOfBirth = value;
            }
        }

        [Property("religion_id", SqlDbType.Int, ReferenceParameter = true)]
        [PropertyConstraint(false, "religion_id", UiName = "Religion", RefEntity = true, RefPropertyEntity = "ReligionObj")]
        public int ReligionId
        {
            get
            {
                return religionId;
            }
            set
            {
                religionId = value;
            }
        }

        [Property("marital_status_id", SqlDbType.Int, ReferenceParameter = true)]
        [PropertyConstraint(false, "marital_status_id", UiName = "Marital Status", RefEntity = true, RefPropertyEntity = "MaritalStatusObj")]
        public int MaritalStatusId
        {
            get
            {
                return maritalStatusId;
            }
            set
            {
                maritalStatusId = value;
            }
        }

        [Property("department_id", SqlDbType.Int, ReferenceParameter = true)]
        [PropertyConstraint(false, "department_id", UiName = "Department", RefEntity = true, RefPropertyEntity = "DepartmentObj")]
        public int DepartmentId
        {
            get
            {
                return departmentId;
            }
            set
            {
                departmentId = value;
            }
        }

        [Property("position_id", SqlDbType.Int, ReferenceParameter = true)]
        [PropertyConstraint(false, "position_id", UiName = "Position", RefEntity = true, RefPropertyEntity = "PositionObj")]
        public int PositionId
        {
            get
            {
                return positionId;
            }
            set
            {
                positionId = value;
            }
        }

        [Property("contact_image_id", SqlDbType.Int, ReferenceParameter = true)]
        [PropertyConstraint(false, "contact_image_id", UiName = "Contact Image", RefEntity = true, RefPropertyEntity = "DocumentObj")]
        public int ContactImageId
        {
            get
            {
                return contactImageId;
            }
            set
            {
                contactImageId = value;
            }
        }



        [Property("flag_deleted", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [Property("is_sync", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool? IsSync
        {
            get
            {
                return isSync;
            }
            set
            {
                isSync = value;
            }
        }

        [Property("is_system", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool? IsSystem
        {
            get
            {
                return isSystem;
            }
            set
            {
                isSystem = value;
            }
        }

        [Property("created_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? CreatedDate
        {
            get
            {
                return createdDate;
            }
            set
            {
                createdDate = value;
            }
        }

        [Property("updated_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? UpdatedDate
        {
            get
            {
                return updatedDate;
            }
            set
            {
                updatedDate = value;
            }
        }

        [Property("created_by", SqlDbType.Int, AddParameter = true, UpdateParameter = false)]
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        [Property("updated_by", SqlDbType.Int, AddParameter = false, UpdateParameter = true)]
        public int UpdatedBy
        {
            get
            {
                return updatedBy;
            }
            set
            {
                updatedBy = value;
            }
        }

        [Property("hear_about_us_id", SqlDbType.Int, ReferenceParameter = true)]
        [PropertyConstraint(false, "hear_about_us_id", UiName = "HearAboutUs", RefEntity = true, RefPropertyEntity = "HearAboutUsObj")]
        public int HearAboutUsId
        {
            get
            {
                return hearAboutUsId;
            }
            set
            {
                hearAboutUsId = value;
            }
        }
        [Property("best_time_to_contact_id", SqlDbType.Int, ReferenceParameter = true)]
        [PropertyConstraint(false, "best_time_to_contact_id", UiName = "BestTimeToContact", RefEntity = true, RefPropertyEntity = "BestTimeToContactObj")]
        public int? BestTimeToContactId
        {
            get
            {
                return bestTimeToContactId;
            }
            set
            {
                bestTimeToContactId = value;
            }
        }

        [Property("account_manager_id", SqlDbType.Int, ReferenceParameter = true)]
        [PropertyConstraint(false, "account_manager_id", UiName = "AccountManager", RefEntity = true, RefPropertyEntity = "AccountManagerObj")]
        public int AccountManagerId
        {
            get
            {//xxxxx
                return accountManagerId;
            }
            set
            {
                accountManagerId = value;
            }
        }

        //[Property("is_vip", SqlDbType.Bit)]
        //public bool? IsVip
        //{
        //    get
        //    {
        //        return isVip;
        //    }
        //    set
        //    {
        //        isVip = value;
        //    }
        //}
       


        [Property("facebook_page", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "facebook_page", UiName = "Facebook Page", MaxLength = 60, MinLength = 3)]
        public string FacebookPage
        {
            get
            {
                return facebookPage;
            }
            set
            {
                facebookPage = value;
            }
        }

        [Property("linkedIn_page", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "linkedIn_page", UiName = "LinkedIn Page", MaxLength = 60, MinLength = 3)]
        public string LinkedInPage
        {
            get
            {
                return linkedInPage;
            }
            set
            {
                linkedInPage = value;
            }
        }

        [Property("twitter_account", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "twitter_account", UiName = "Twitter Account", MaxLength = 60, MinLength = 3)]
        public string TwitterAccount
        {
            get
            {
                return twitterAccount;
            }
            set
            {
                twitterAccount = value;
            }
        }

        [Property("website", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "website", UiName = "Website", MaxLength = 60, MinLength = 3)]
        public string Website
        {
            get
            {
                return website;
            }
            set
            {
                website = value;
            }
        }


        #endregion

        //#region Composite Objects

        //private User accountManagerObj;
        //[CompositeObject(CompositObjectTypeEnum.Single, "AccountManagerId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable = false)]
        //public User AccountManagerObj
        //{
        //    get
        //    {
        //        if (accountManagerObj == null)
        //            loadCompositObject_Lazy("AccountManagerObj");
        //        return accountManagerObj;
        //    }
        //    set
        //    {
        //        accountManagerObj = value; ;
        //    }
        //}


        ////private DataTypeContent bestTimeToContactObj;
        ////[CompositeObject(CompositObjectTypeEnum.Single, "BestTimeToContactId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable = false)]
        ////public DataTypeContent BestTimeToContactObj
        ////{
        ////    get
        ////    {
        ////        if (bestTimeToContactObj == null)
        ////            loadCompositObject_Lazy("BestTimeToContactObj");
        ////        return bestTimeToContactObj;
        ////    }
        ////    set
        ////    {
        ////        bestTimeToContactObj = value; ;
        ////    }
        ////}

        //private DataTypeContent haearAboutUsObj;
        //[CompositeObject(CompositObjectTypeEnum.Single, "HearAboutUsId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable = false)]
        //public DataTypeContent HearAboutUsObj
        //{
        //    get
        //    {
        //        if (haearAboutUsObj == null)
        //            loadCompositObject_Lazy("HearAboutUsObj");
        //        return haearAboutUsObj;
        //    }
        //    set
        //    {
        //        haearAboutUsObj = value; ;
        //    }
        //}

        //private Company companyObj;
        //[CompositeObject(CompositObjectTypeEnum.Single, "CompanyId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable = false)]
        //public Company CompanyObj
        //{
        //    get
        //    {
        //        if (companyObj == null)
        //            loadCompositObject_Lazy("CompanyObj");
        //        return companyObj;
        //    }
        //    set
        //    {
        //        companyObj = value; ;
        //    }
        //}


        ////private User primaryContactEmployeeObj;
        ////[CompositeObject(CompositObjectTypeEnum.Single, "PrimaryContactEmployee", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable = false)]
        ////public User PrimaryContactEmployeeObj
        ////{
        ////    get
        ////    {
        ////        if (primaryContactEmployeeObj == null)
        ////            loadCompositObject_Lazy("PrimaryContactEmployeeObj");
        ////        return primaryContactEmployeeObj;
        ////    }
        ////    set
        ////    {
        ////        primaryContactEmployeeObj = value; ;
        ////    }
        ////}


        //private DataTypeContent salutationObj;
        //[CompositeObject(CompositObjectTypeEnum.Single, "SalutationId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable = false)]
        //public DataTypeContent SalutationObj
        //{
        //    get
        //    {
        //        if (salutationObj == null)
        //            loadCompositObject_Lazy("SalutationObj");
        //        return salutationObj;
        //    }
        //    set
        //    {
        //        salutationObj = value; ;
        //    }
        //}

        //private DataTypeContent genderObj;
        //[CompositeObject(CompositObjectTypeEnum.Single, "GenderId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable = false)]
        //public DataTypeContent GenderObj
        //{
        //    get
        //    {
        //        if (genderObj == null)
        //            loadCompositObject_Lazy("GenderObj");
        //        return genderObj;
        //    }
        //    set
        //    {
        //        genderObj = value; ;
        //    }
        //}

        //private DataTypeContent sourceObj;
        //[CompositeObject(CompositObjectTypeEnum.Single, "SourceId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable = false)]
        //public DataTypeContent SourceObj
        //{
        //    get
        //    {
        //        if (sourceObj == null)
        //            loadCompositObject_Lazy("SourceObj");
        //        return sourceObj;
        //    }
        //    set
        //    {
        //        sourceObj = value; ;
        //    }
        //}

        //private DataTypeContent religionObj;
        //[CompositeObject(CompositObjectTypeEnum.Single, "ReligionId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable = false)]
        //public DataTypeContent ReligionObj
        //{
        //    get
        //    {
        //        if (religionObj == null)
        //            loadCompositObject_Lazy("ReligionObj");
        //        return religionObj;
        //    }
        //    set
        //    {
        //        religionObj = value; ;
        //    }
        //}

        //private DataTypeContent maritalStatusObj;
        //[CompositeObject(CompositObjectTypeEnum.Single, "MaritalStatusId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable = false)]
        //public DataTypeContent MaritalStatusObj
        //{
        //    get
        //    {
        //        if (maritalStatusObj == null)
        //            loadCompositObject_Lazy("MaritalStatusObj");
        //        return maritalStatusObj;
        //    }
        //    set
        //    {
        //        maritalStatusObj = value; ;
        //    }
        //}

        ////private DataTypeContent departmentObj;
        ////[CompositeObject(CompositObjectTypeEnum.Single, "DepartmentId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable = false)]
        ////public DataTypeContent DepartmentObj
        ////{
        ////    get
        ////    {
        ////        if (departmentObj == null)
        ////            loadCompositObject_Lazy("DepartmentObj");
        ////        return departmentObj;
        ////    }
        ////    set
        ////    {
        ////        departmentObj = value; ;
        ////    }
        ////}

        //private DataTypeContent positionObj;
        //[CompositeObject(CompositObjectTypeEnum.Single, "PositionId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable = false)]
        //public DataTypeContent PositionObj
        //{
        //    get
        //    {
        //        if (positionObj == null)
        //            loadCompositObject_Lazy("PositionObj");
        //        return positionObj;
        //    }
        //    set
        //    {
        //        positionObj = value; ;
        //    }
        //}

        //private Document documentObj;
        //[CompositeObject(CompositObjectTypeEnum.Single, "ContactImageId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable = false)]
        //public Document DocumentObj
        //{
        //    get
        //    {
        //        if (documentObj == null)
        //            loadCompositObject_Lazy("DocumentObj");
        //        return documentObj;
        //    }
        //    set
        //    {
        //        documentObj = value; ;
        //    }
        //}



        [ScriptIgnore]
        private List<EmailEntity> emailPersonList;
        [CompositeObject(CompositObjectTypeEnum.List, "PersonId", LazyLoad = true, CascadeDelete = true,Savable=false)]
        public List<EmailEntity> EmailPersonList
        {
            get
            {
                if (emailPersonList == null)
                    loadCompositObject_Lazy("EmailPersonList");
                return emailPersonList;
            }
            set { emailPersonList = value; }
        }



        [ScriptIgnore]
        private List<PhoneEntity> phonePersonList;
        [CompositeObject(CompositObjectTypeEnum.List, "PersonId", LazyLoad = true, CascadeDelete = true, Savable = false)]
        public List<PhoneEntity> PhonePersonList
        {
            get
            {
                if (phonePersonList == null)
                    loadCompositObject_Lazy("PhonePersonList");
                return phonePersonList;
            }
            set { phonePersonList = value; }
        }

       




        //#endregion


        public override int GetIdentity()
        {
            return personId;
        }
        public override void SetIdentity(int value)
        {
            personId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            if (ParentType == typeof(Company))
                this.companyId = (int)value;

            else if (ParentType == typeof(Document))
                this.contactImageId = (int)value;

        }
    }
}