using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace CentrixERP.Business.Entity
{
     public class POSItemCardSerialLite : EntityBaseLite
    {      
        
        #region Public Properties

         		[LiteProperty("item_card_serial_id",SqlDbType.Int)]
		public int ItemCardSerialId
		{
			get;
			set;
		}

		[LiteProperty("item_serial_number",SqlDbType.NVarChar)]
		public string ItemSerialNumber
		{
			get;
			set;
		}

		[LiteProperty("item_card_id",SqlDbType.Int)]
		public int ItemCardId
		{
			get;
			set;
		}

		[LiteProperty("location_id",SqlDbType.Int)]
		public int LocationId
		{
			get;
			set;
		}

		[LiteProperty("status",SqlDbType.Int)]
		public int Status
		{
			get;
			set;
		}

		[LiteProperty("receipt_detail_id",SqlDbType.Int)]
		public int ReceiptDetailId
		{
			get;
			set;
		}

		[LiteProperty("transfer_detail_id",SqlDbType.Int)]
		public int TransferDetailId
		{
			get;
			set;
		}

		[LiteProperty("stock_date",SqlDbType.DateTime)]
		public DateTime? StockDate
		{
			get;
			set;
		}



       
        public int value
        {
            get{ return Id;}
        }

        
        public string label
        {
            get { return ItemSerialNumber; }
        }

         [LiteProperty("flag_deleted", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? FlagDeleted
        {
            get;
            set;
        }

        [LiteProperty("is_system",SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
		public bool? IsSystem
		{
			get;
			set;
		}

		[LiteProperty("created_date",SqlDbType.DateTime)]
		public DateTime? CreatedDate
		{
			get;
			set;
		}

		[LiteProperty("updated_date",SqlDbType.DateTime)]
		public DateTime? UpdatedDate
		{
			get;
			set;
		}

		[LiteProperty("created_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int CreatedBy
		{
			get;
			set;
		}

		[LiteProperty("updated_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int UpdatedBy
		{
			get;
			set;
		}

         [LiteProperty("created_by_name",SqlDbType.NVarChar, FindAllLiteParameter = false)]
         public string CreatedByName{get;set;}

         [LiteProperty("updated_by_name",SqlDbType.NVarChar,FindAllLiteParameter = false)]
         public string UpdatedByName{get;set;}

        
          public int Id
        {
            get{return this.ItemCardSerialId; }
        }

         [LiteProperty("last_rows",SqlDbType.Bit)]
         public bool LastRows { get; set; }

         [LiteProperty("total_count",SqlDbType.Int)]
         public int TotalRecords { get; set; }
        #endregion
       
          
     }
}