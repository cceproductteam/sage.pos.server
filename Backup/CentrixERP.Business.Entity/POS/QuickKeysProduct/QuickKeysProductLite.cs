using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace CentrixERP.Business.Entity
{
     public class QuickKeysProductLite : EntityBaseLite
    {      
        
        #region Public Properties

         		[LiteProperty("quick_key_product_id",SqlDbType.Int)]
		public int QuickKeyProductId
		{
			get;
			set;
		}

		[LiteProperty("quick_key_id",SqlDbType.Int)]
		public int QuickKeyId
		{
			get;
			set;
		}

		[LiteProperty("quick_key",SqlDbType.NVarChar)]
		public string QuickKey
		{
			get;
			set;
		}

		[LiteProperty("tab_id",SqlDbType.Int)]
		public int TabId
		{
			get;
			set;
		}

		[LiteProperty("css_class",SqlDbType.NVarChar)]
		public string CssClass
		{
			get;
			set;
		}

		[LiteProperty("product_id",SqlDbType.Int)]
		public int ProductId
		{
			get;
			set;
		}

          public string Product
        {
            get
            {
                return ItemBarCode;
            }
        }

		[LiteProperty("tab_name",SqlDbType.NVarChar)]
		public string TabName
		{
			get;
			set;
		}

		[LiteProperty("item_number",SqlDbType.NVarChar)]
		public string ItemNumber
		{
			get;
			set;
		}

		[LiteProperty("item_desc",SqlDbType.NVarChar)]
        public string ItemDescription
		{
			get;
			set;
		}

		[LiteProperty("item_barCode",SqlDbType.NVarChar)]
        public string ItemBarCode
		{
			get;
			set;
		}



        [LiteProperty("quick_key_product_id", SqlDbType.Int)]
        public int value
        {
            get;
            set;
        }

          [LiteProperty("quick_key_product_id", SqlDbType.Int)]
        public int label
        {
            get;
            set;
        }

         [LiteProperty("flag_deleted", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? FlagDeleted
        {
            get;
            set;
        }

        [LiteProperty("is_system",SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
		public bool? IsSystem
		{
			get;
			set;
		}

		[LiteProperty("created_date",SqlDbType.DateTime)]
		public DateTime? CreatedDate
		{
			get;
			set;
		}

		[LiteProperty("updated_date",SqlDbType.DateTime)]
		public DateTime? UpdatedDate
		{
			get;
			set;
		}

		[LiteProperty("created_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int CreatedBy
		{
			get;
			set;
		}

		[LiteProperty("updated_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int UpdatedBy
		{
			get;
			set;
		}

         [LiteProperty("created_by_name",SqlDbType.NVarChar, FindAllLiteParameter = false)]
         public string CreatedByName{get;set;}

         [LiteProperty("updated_by_name",SqlDbType.NVarChar,FindAllLiteParameter = false)]
         public string UpdatedByName{get;set;}

         [LiteProperty("quick_key_product_id",SqlDbType.Int)]
          public int Id
        {
            get{return this.QuickKeyProductId; }
            set{this.QuickKeyProductId=value;}
        }

         [LiteProperty("last_rows",SqlDbType.NVarChar)]
         public bool LastRows { get; set; }

         [LiteProperty("total_count",SqlDbType.NVarChar)]
         public int TotalRecords { get; set; }
        #endregion
       
          
     }
}