using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
using CentrixERP.Common.Business.Entity;

namespace CentrixERP.Business.Entity
{
    [EntityIManager("CentrixERP.Business.IManager.IQuickKeysProductManager,CentrixERP.Business.IManager", isLogicalDelete = true)]
    public class QuickKeysProduct : EntityBase<QuickKeysProduct, int>
    {
        #region Constructors

        public QuickKeysProduct() { }

        public QuickKeysProduct(int quickKeyProductId)
        {
            this.quickKeyProductId = quickKeyProductId;
        }

        #endregion

        #region Private Properties

        private int quickKeyProductId;

        private int quickKeyId = -1;

        private int tabId;

        private string cssClass;

        private int productId;

        private string tabName;

        private string itemNumber;

        private string itemDesc;

        private string itemBarcode;

        private int createdBy;

        private int updatedBy;

        private DateTime? createdDate;

        private DateTime? updatedDate;

        private bool? flagDeleted;

        private bool? isSystem;



        //private bool? flagDeleted;

        //private bool? isSystem;

        //private int createdBy;

        //private int updatedBy;

        //private DateTime? createdDate;

        //private DateTime? updatedDate;
        #endregion

        #region Public Properties

        [PrimaryKey("@quick_key_product_id", SqlDbType.Int)]
        [Property("quick_key_product_id", SqlDbType.Int, AddParameter = false)]
        public int QuickKeyProductId
        {
            get
            {
                return quickKeyProductId;
            }
            set
            {
                quickKeyProductId = value;
            }
        }

        [Property("quick_key_id", SqlDbType.Int, ReferenceParameter = true)]
        [PropertyConstraint(true, "QuickKey", RefEntity = true, RefPropertyEntity = "QuickKeysObj")]
        public int QuickKeyId
        {
            get
            {
                return quickKeyId;
            }
            set
            {
                quickKeyId = value;
            }
        }

        [Property("tab_id", SqlDbType.Int)]
        [PropertyConstraint(true, "Tab")]
        public int TabId
        {
            get
            {
                return tabId;
            }
            set
            {
                tabId = value;
            }
        }

        [Property("css_class", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "CssClass", MaxLength = 100, MinLength = 3)]
        public string CssClass
        {
            get
            {
                return cssClass;
            }
            set
            {
                cssClass = value;
            }
        }

        [Property("product_id", SqlDbType.Int)]
        [PropertyConstraint(true, "Product")]
        public int ProductId
        {
            get
            {
                return productId;
            }
            set
            {
                productId = value;
            }
        }




        [Property("tab_name", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "TabName", MaxLength = 100, MinLength = 3)]
        public string TabName
        {
            get
            {
                return tabName;
            }
            set
            {
                tabName = value;
            }
        }

        [Property("item_number", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "ItemNumber", MaxLength = 100, MinLength = 3)]
        public string ItemNumber
        {
            get
            {
                return itemNumber;
            }
            set
            {
                itemNumber = value;
            }
        }

        [Property("item_desc", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "ItemDescription", MaxLength = 100, MinLength = 3)]
        public string ItemDescription
        {
            get
            {
                return itemDesc;
            }
            set
            {
                itemDesc = value;
            }
        }

        [Property("item_barCode", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "ItemBarCode", MaxLength = 100, MinLength = 3)]
        public string ItemBarCode
        {
            get
            {
                return itemBarcode;
            }
            set
            {
                itemBarcode = value;
            }
        }



        [Property("flag_deleted", SqlDbType.Bit, AddParameter = true, UpdateParameter = true, FindParameter = true)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [Property("is_system", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool? IsSystem
        {
            get
            {
                return isSystem;
            }
            set
            {
                isSystem = value;
            }
        }

        [Property("created_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? CreatedDate
        {
            get
            {
                return createdDate;
            }
            set
            {
                createdDate = value;
            }
        }

        [Property("updated_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? UpdatedDate
        {
            get
            {
                return updatedDate;
            }
            set
            {
                updatedDate = value;
            }
        }

        [Property("created_by", SqlDbType.Int, AddParameter = true, UpdateParameter = false)]
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        [Property("updated_by", SqlDbType.Int, AddParameter = false, UpdateParameter = true)]
        public int UpdatedBy
        {
            get
            {
                return updatedBy;
            }
            set
            {
                updatedBy = value;
            }
        }

       
        #endregion

        #region Composite Objects
        private QuickKeys quickKeysObj;
        [CompositeObject(CompositObjectTypeEnum.Single, "QuickKeyId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable = false)]
        public QuickKeys QuickKeysObj
        {
            get
            {
                if (quickKeysObj == null)
                    loadCompositObject_Lazy("QuickKeysObj");
                return quickKeysObj;
            }
            set
            {
                quickKeysObj = value; ;
            }
        }



        #endregion


        public override int GetIdentity()
        {
            return quickKeyProductId;
        }
        public override void SetIdentity(int value)
        {
            quickKeyProductId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            if (ParentType == typeof(QuickKeys))
                this.quickKeyId = (int)value;

        }
    }
}