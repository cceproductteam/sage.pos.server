using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace CentrixERP.Business.Entity
{
     public class SyncScheduleLite : EntityBaseLite
    {      
        
        #region Public Properties

         		[LiteProperty("sync_schedule_id",SqlDbType.Int)]
		public int SyncScheduleId
		{
			get;
			set;
		}

		[LiteProperty("sync_entities",SqlDbType.NVarChar)]
		public string SyncEntities
		{
			get;
			set;
		}

		[LiteProperty("sync_entities_ids",SqlDbType.NVarChar)]
		public string SyncEntitiesIds
		{
			get;
			set;
		}

		[LiteProperty("status",SqlDbType.Int)]
		public int Status
		{
			get;
			set;
		}



        [LiteProperty("sync_schedule_id", SqlDbType.Int)]
        public int value
        {
            get;
            set;
        }

          [LiteProperty("sync_schedule_id", SqlDbType.Int)]
        public int label
        {
            get;
            set;
        }

         [LiteProperty("flag_deleted", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? FlagDeleted
        {
            get;
            set;
        }

        [LiteProperty("is_system",SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
		public bool? IsSystem
		{
			get;
			set;
		}

		[LiteProperty("created_date",SqlDbType.DateTime)]
		public DateTime? CreatedDate
		{
			get;
			set;
		}

		[LiteProperty("updated_date",SqlDbType.DateTime)]
		public DateTime? UpdatedDate
		{
			get;
			set;
		}

		[LiteProperty("created_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int CreatedBy
		{
			get;
			set;
		}

		[LiteProperty("updated_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int UpdatedBy
		{
			get;
			set;
		}

         [LiteProperty("created_by_name",SqlDbType.NVarChar, FindAllLiteParameter = true)]
         public string CreatedByName{get;set;}

         [LiteProperty("updated_by_name",SqlDbType.NVarChar,FindAllLiteParameter = true)]
         public string UpdatedByName{get;set;}

         [LiteProperty("sync_schedule_id",SqlDbType.NVarChar)]
          public int Id
        {
            get{return this.SyncScheduleId; }
            set{this.SyncScheduleId=value;}
        }

         [LiteProperty("last_rows",SqlDbType.NVarChar)]
         public bool LastRows { get; set; }

         [LiteProperty("total_count",SqlDbType.NVarChar)]
         public int TotalRecords { get; set; }
        #endregion
       
          
     }
}