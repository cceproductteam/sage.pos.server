using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
using CentrixERP.Common.Business.Entity;

namespace CentrixERP.Business.Entity
{
    [EntityIManager("CentrixERP.Business.IManager.ISyncScheduleManager,CentrixERP.Business.IManager", isLogicalDelete = true)]
    public class SyncSchedule : EntityBase<SyncSchedule, int>
    {
        #region Constructors

        public SyncSchedule() { }

        public SyncSchedule(int syncScheduleId)
        {
            this.syncScheduleId = syncScheduleId;
        }

        #endregion

        #region Private Properties

		private int syncScheduleId;

		private string syncEntities;

		private string syncEntitiesIds;

		private int status;

		private int createdBy;

		private DateTime? createdDate;

		private int updatedBy;

		private DateTime? updatedDate;

		private bool? flagDeleted;

		private bool? isSystem;



        //private bool? flagDeleted;

        //private bool? isSystem;

        //private int createdBy;

        //private int updatedBy;

        //private DateTime? createdDate;

        //private DateTime? updatedDate;
        #endregion

        #region Public Properties

        [PrimaryKey("@sync_schedule_id", SqlDbType.Int)]
[Property("sync_schedule_id", SqlDbType.Int, AddParameter = false)]
		public int SyncScheduleId
		{
			get
			{
				return syncScheduleId;
			}
			set
			{
				syncScheduleId = value;
			}
		}

[Property("sync_entities", SqlDbType.NVarChar)]
[PropertyConstraint(false, "SyncEntities", MaxLength = 500, MinLength = 3)]
		public string SyncEntities
		{
			get
			{
				return syncEntities;
			}
			set
			{
				syncEntities = value;
			}
		}

[Property("sync_entities_ids", SqlDbType.NVarChar)]
[PropertyConstraint(false, "SyncEntitiesIds", MaxLength = 500, MinLength = 3)]
		public string SyncEntitiesIds
		{
			get
			{
				return syncEntitiesIds;
			}
			set
			{
				syncEntitiesIds = value;
			}
		}

[Property("status", SqlDbType.Int)]
[PropertyConstraint(false, "Status")]
		public int Status
		{
			get
			{
				return status;
			}
			set
			{
				status = value;
			}
		}



        [Property("flag_deleted", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [Property("is_system", SqlDbType.Bit, AddParameter = false, UpdateParameter = false,FindParameter=false)]
        public bool? IsSystem
        {
            get
            {
                return isSystem;
            }
            set
            {
                isSystem = value;
            }
        }

		[Property("created_date",SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
		public DateTime? CreatedDate
		{
			get
			{
				return createdDate;
			}
			set
			{
				createdDate = value;
			}
		}

        [Property("updated_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
		public DateTime? UpdatedDate
		{
			get
			{
				return updatedDate;
			}
			set
			{
				updatedDate = value;
			}
		}

		[Property("created_by",SqlDbType.Int, AddParameter = true, UpdateParameter = false)]
		public int CreatedBy
		{
			get
			{
				return createdBy;
			}
			set
			{
				createdBy = value;
			}
		}

		[Property("updated_by",SqlDbType.Int, AddParameter = false, UpdateParameter = true)]
		public int UpdatedBy
		{
			get
			{
				return updatedBy;
			}
			set
			{
				updatedBy = value;
			}
		}
        #endregion

             #region Composite Objects
              

                 #endregion
   

        public override int GetIdentity()
        {
            return syncScheduleId;
        }
        public override void SetIdentity(int value)
        {
            syncScheduleId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            			throw new NotImplementedException();
        }
    }
}