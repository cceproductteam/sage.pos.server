using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace CentrixERP.Business.Entity
{
    public class StoreLite : EntityBaseLite
    {

        #region Public Properties

        [LiteProperty("store_ID", SqlDbType.Int)]
        public int StoreId
        {
            get;
            set;
        }

        [LiteProperty("store_name", SqlDbType.NVarChar)]
        public string StoreName
        {
            get;
            set;
        }

        [LiteProperty("store_name_ar", SqlDbType.NVarChar)]
        public string StoreNameAr
        {
            get;
            set;
        }

        [LiteProperty("store_code", SqlDbType.NVarChar)]
        public string StoreCode
        {
            get;
            set;
        }

        [LiteProperty("default_currency_id", SqlDbType.Int)]
        public int DefaultCurrencyId
        {
            get;
            set;
        }

        [LiteProperty("default_currency", SqlDbType.NVarChar)]
        public string DefaultCurrency
        {
            get;
            set;
        }

        [LiteProperty("default_curr_code", SqlDbType.NVarChar)]
        public string DefaultCurrCode
        {
            get;
            set;
        }

        [LiteProperty("default_curr_symbol", SqlDbType.NVarChar)]
        public string DefaultCurrSymbol
        {
            get;
            set;
        }

        [LiteProperty("location_id", SqlDbType.Int)]
        public int LocationId
        {
            get;
            set;
        }

        [LiteProperty("location", SqlDbType.NVarChar)]
        public string Location
        {
            get;
            set;
        }

        [LiteProperty("location_code", SqlDbType.NVarChar)]
        public string LocationCode
        {
            get;
            set;
        }

        [LiteProperty("location_desc", SqlDbType.NVarChar)]
        public string LocationDesc
        {
            get;
            set;
        }

        [LiteProperty("ip_address", SqlDbType.NVarChar)]
        public string IpAddress
        {
            get;
            set;
        }

        [LiteProperty("mac_address", SqlDbType.NVarChar)]
        public string MacAddress
        {
            get;
            set;
        }

        [LiteProperty("data_base_instance_name", SqlDbType.NVarChar)]
        public string DataBaseInstanceName
        {
            get;
            set;
        }

        [LiteProperty("data_base_name", SqlDbType.NVarChar)]
        public string DataBaseName
        {
            get;
            set;
        }

        [LiteProperty("data_base_user_name", SqlDbType.NVarChar)]
        public string DataBaseUserName
        {
            get;
            set;
        }

        [LiteProperty("data_base_password", SqlDbType.NVarChar)]
        public string DataBasePassword
        {
            get;
            set;
        }

        [LiteProperty("default_tax", SqlDbType.Int)]
        public int DefaultTax
        {
            get;
            set;
        }

        [LiteProperty("display_pricses", SqlDbType.Int)]
        public int DisplayPricses
        {
            get;
            set;
        }

        [LiteProperty("cashier_discounts", SqlDbType.Bit)]
        public bool? CashierDiscounts
        {
            get;
            set;
        }

        [LiteProperty("legal_entity_id", SqlDbType.Int)]
        public int LegalEntityId
        {
            get;
            set;
        }

        [LiteProperty("weight_scale_formula_id", SqlDbType.Int)]
        public int WeightScaleFormulaId
        {
            get;
            set;
        }

        [LiteProperty("is_used", SqlDbType.Bit)]
        public bool? IsUsed
        {
            get;
            set;
        }


        [LiteProperty("ar_cash_customer_id", SqlDbType.Int)]
        public int ArCashCustomerId
        {
            get;
            set;
        }

        [LiteProperty("ar_visa_customer_id", SqlDbType.Int)]
        public int ArVisaCustomerId
        {
            get;
            set;
        }

        [LiteProperty("ar_mixed_customer_id", SqlDbType.Int)]
        public int ArMixedCustomerId
        {
            get;
            set;
        }

        [LiteProperty("cash_customer_name", SqlDbType.NVarChar)]
        public string ArCashCustomer
        {
            get;
            set;
        }

        [LiteProperty("visa_customer_name", SqlDbType.NVarChar)]
        public string ArVisaCustomer
        {
            get;
            set;
        }

        [LiteProperty("mixed_customer_name", SqlDbType.NVarChar)]
        public string ArMixedCustomer
        {
            get;
            set;
        }   
        

        [LiteProperty("store_ID", SqlDbType.Int)]
        public int value
        {
            get;
            set;
        }

        [LiteProperty("store_name", SqlDbType.NVarChar)]
        public string label
        {
            get;
            set;
        }

        [LiteProperty("flag_deleted", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? FlagDeleted
        {
            get;
            set;
        }

        [LiteProperty("is_system", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? IsSystem
        {
            get;
            set;
        }

        [LiteProperty("created_date", SqlDbType.DateTime)]
        public DateTime? CreatedDate
        {
            get;
            set;
        }

        [LiteProperty("updated_date", SqlDbType.DateTime)]
        public DateTime? UpdatedDate
        {
            get;
            set;
        }

        [LiteProperty("created_by", SqlDbType.Int, FindByIdLiteParameter = false)]
        public int CreatedBy
        {
            get;
            set;
        }




        [LiteProperty("updated_by", SqlDbType.Int, FindByIdLiteParameter = false)]
        public int UpdatedBy
        {
            get;
            set;
        }

        [LiteProperty("created_by_name", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string CreatedByName { get; set; }

        [LiteProperty("updated_by_name", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string UpdatedByName { get; set; }

        [LiteProperty("store_ID", SqlDbType.Int)]
        public int Id
        {
            get { return this.StoreId; }
            set { this.StoreId = value; }
        }

        [LiteProperty("last_rows", SqlDbType.NVarChar)]
        public bool LastRows { get; set; }

        [LiteProperty("total_count", SqlDbType.NVarChar)]
        public int TotalRecords { get; set; }


        [LiteProperty("gl_account_id", SqlDbType.Int)]
        public int GlAccountId
        {
            get;
            set;
        }

        [LiteProperty("gl_account", SqlDbType.NVarChar)]
        public string GlAccount
        {
            get;
            set;
        }

        [LiteProperty("gl_national_id", SqlDbType.Int)]
        public int GlNationalId
        {
            get;
            set;
        }

        [LiteProperty("gl_check_id", SqlDbType.Int)]
        public int GlCheckId
        {
            get;
            set;
        }

        [LiteProperty("gl_national", SqlDbType.NVarChar)]
        public string GlNational
        {
            get;
            set;
        }

        [LiteProperty("gl_check", SqlDbType.NVarChar)]
        public string GlCheck
        {
            get;
            set;
        }
       

       


        #endregion


    }
}