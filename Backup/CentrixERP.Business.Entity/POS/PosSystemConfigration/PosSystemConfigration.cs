using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
using CentrixERP.Common.Business.Entity;

namespace CentrixERP.Business.Entity
{
    [EntityIManager("CentrixERP.Business.IManager.IPosSystemConfigrationManager,CentrixERP.Business.IManager", isLogicalDelete = true)]
    public class PosSystemConfigration : EntityBase<PosSystemConfigration, int>
    {
        #region Constructors

        public PosSystemConfigration() { }

        public PosSystemConfigration(int posConfigId)
        {
            this.posConfigId = posConfigId;
        }

        #endregion

        #region Private Properties
        private int taxAuthorityId;
        private string taxAuthority;
        private int addingItemStructureId;
        private int posConfigId;

        private int giftVoucherOverAmount;

        private decimal? laybyMinPercentage;

        private int loginMethodId = -1;

        private int afterCloseRegisterValueId = -1;

        private string invoiceNumberFormat;

        private string laybyNumberFormat;

        private int cashCustomerId;

        private string accpacStagingIp;

        private string posServerIp;

        private string accpacDataBaseUsername;

        private string accpacDataBasePassword;

        private string posServerDataBaseUsername;

        private string posServerDataBasePassword;

        private string posServerDataBaseName;

        private string accpacDataBaseName;

        private bool? flagDeleted;

        private int createdBy;

        private int updatedBy;

        private DateTime? createdDate;

        private DateTime? updatedDate;

        private bool? isSysetm;

        private int taxableClass;
        private int nonTaxableClass;

        private int discountAccountId;

        private int exchangeAccountId;

        //private bool? flagDeleted;

        //private bool? isSystem;

        //private int createdBy;

        //private int updatedBy;

        //private DateTime? createdDate;

        //private DateTime? updatedDate;
        #endregion

        #region Public Properties
        [Property("tax_Authority_id", SqlDbType.Int)]
        [PropertyConstraint(false, "TaxAuthorityId")]
        public int TaxAuthorityId
        {
            get
            {
                return taxAuthorityId;
            }
            set
            {
                taxAuthorityId = value;
            }
        }

        [Property("tax_Authority", SqlDbType.NVarChar, AddParameter=false,UpdateParameter=false)]
        [PropertyConstraint(false, "TaxAuthority")]
        public string TaxAuthority
        {
            get
            {
                return taxAuthority;
            }
            set
            {
                taxAuthority = value;
            }
        }



        [PrimaryKey("@pos_config_id", SqlDbType.Int)]
        [Property("pos_config_id", SqlDbType.Int, AddParameter = false)]
        public int PosConfigId
        {
            get
            {
                return posConfigId;
            }
            set
            {
                posConfigId = value;
            }
        }

        [Property("gift_voucher_over_amount", SqlDbType.Int)]
        [PropertyConstraint(false, "GiftVoucherOverAmount")]
        public int GiftVoucherOverAmount
        {
            get
            {
                return giftVoucherOverAmount;
            }
            set
            {
                giftVoucherOverAmount = value;
            }
        }

        [Property("discount_account_id", SqlDbType.Int)]
        [PropertyConstraint(true, "DiscountAccountId")]
        public int DiscountAccountId
        {
            get
            {
                return discountAccountId;
            }
            set
            {
                discountAccountId = value;
            }
        }


        [Property("exchange_account_id", SqlDbType.Int , FindParameter = true)]
        [PropertyConstraint(true, "ExchangeAccountId")]
        public int ExchangeAccountId
        {
            get
            {
                return exchangeAccountId;
            }
            set
            {
                exchangeAccountId = value;
            }
        }

        [Property("adding_item_structure_id", SqlDbType.Int)]

        public int AddingItemStructureId
        {
            get
            {
                return addingItemStructureId;
            }
            set
            {
                addingItemStructureId = value;
            }
        }

        [Property("layby_min_percentage", SqlDbType.Money)]
        [PropertyConstraint(false, "LaybyMinPercentage")]
        public decimal? LaybyMinPercentage
        {
            get
            {
                return laybyMinPercentage;
            }
            set
            {
                laybyMinPercentage = value;
            }
        }

        [Property("login_method_id", SqlDbType.Int, ReferenceParameter = true)]
        [PropertyConstraint(false, "LoginMethod", RefEntity = true, RefPropertyEntity = "LoginMethodObj")]
        public int LoginMethodId
        {
            get
            {
                return loginMethodId;
            }
            set
            {
                loginMethodId = value;
            }
        }

        [Property("after_close_register_value_id", SqlDbType.Int, ReferenceParameter = true)]
        [PropertyConstraint(false, "AfterCloseRegisterValue", RefEntity = true, RefPropertyEntity = "AfterCloseRegisterValueObj")]
        public int AfterCloseRegisterValueId
        {
            get
            {
                return afterCloseRegisterValueId;
            }
            set
            {
                afterCloseRegisterValueId = value;
            }
        }

        [Property("invoice_number_format", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "InvoiceNumberFormat", MaxLength = 4000, MinLength = 3)]
        public string InvoiceNumberFormat
        {
            get
            {
                return invoiceNumberFormat;
            }
            set
            {
                invoiceNumberFormat = value;
            }
        }

        [Property("layby_number_format", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "LaybyNumberFormat", MaxLength = 4000, MinLength = 3)]
        public string LaybyNumberFormat
        {
            get
            {
                return laybyNumberFormat;
            }
            set
            {
                laybyNumberFormat = value;
            }
        }

        [Property("cash_customer_id", SqlDbType.Int)]
        [PropertyConstraint(false, "CashCustomer")]
        public int CashCustomerId
        {
            get
            {
                return cashCustomerId;
            }
            set
            {
                cashCustomerId = value;
            }
        }

       

        [Property("accpac_staging_ip", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "AccpacStagingIp", MaxLength = 4000, MinLength = 3)]
        public string AccpacStagingIp
        {
            get
            {
                return accpacStagingIp;
            }
            set
            {
                accpacStagingIp = value;
            }
        }

        [Property("pos_server_ip", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "PosServerIp", MaxLength = 4000, MinLength = 3)]
        public string PosServerIp
        {
            get
            {
                return posServerIp;
            }
            set
            {
                posServerIp = value;
            }
        }

        [Property("accpac_data_base_username", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "AccpacDataBaseUsername", MaxLength = 4000, MinLength = 3)]
        public string AccpacDataBaseUsername
        {
            get
            {
                return accpacDataBaseUsername;
            }
            set
            {
                accpacDataBaseUsername = value;
            }
        }

        [Property("accpac_data_base_password", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "AccpacDataBasePassword", MaxLength = 4000, MinLength = 3)]
        public string AccpacDataBasePassword
        {
            get
            {
                return accpacDataBasePassword;
            }
            set
            {
                accpacDataBasePassword = value;
            }
        }

        [Property("pos_server_data_base_username", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "PosServerDataBaseUsername", MaxLength = 4000, MinLength = 3)]
        public string PosServerDataBaseUsername
        {
            get
            {
                return posServerDataBaseUsername;
            }
            set
            {
                posServerDataBaseUsername = value;
            }
        }

        [Property("pos_server_data_base_password", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "PosServerDataBasePassword", MaxLength = 4000, MinLength = 3)]
        public string PosServerDataBasePassword
        {
            get
            {
                return posServerDataBasePassword;
            }
            set
            {
                posServerDataBasePassword = value;
            }
        }

        [Property("pos_server_data_base_name", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "PosServerDataBaseName", MaxLength = 4000, MinLength = 3)]
        public string PosServerDataBaseName
        {
            get
            {
                return posServerDataBaseName;
            }
            set
            {
                posServerDataBaseName = value;
            }
        }

        [Property("accpac_data_base_name", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "AccpacDataBaseName", MaxLength = 4000, MinLength = 3)]
        public string AccpacDataBaseName
        {
            get
            {
                return accpacDataBaseName;
            }
            set
            {
                accpacDataBaseName = value;
            }
        }



        [Property("flag_deleted", SqlDbType.Bit, AddParameter = true, UpdateParameter = true, FindParameter = true)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [Property("is_sysetm", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool? IsSystem
        {
            get
            {
                return isSysetm;
            }
            set
            {
                isSysetm = value;
            }
        }

        [Property("created_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? CreatedDate
        {
            get
            {
                return createdDate;
            }
            set
            {
                createdDate = value;
            }
        }

        [Property("updated_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? UpdatedDate
        {
            get
            {
                return updatedDate;
            }
            set
            {
                updatedDate = value;
            }
        }

        [Property("created_by", SqlDbType.Int, AddParameter = true, UpdateParameter = false)]
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        [Property("updated_by", SqlDbType.Int, AddParameter = false, UpdateParameter = true)]
        public int UpdatedBy
        {
            get
            {
                return updatedBy;
            }
            set
            {
                updatedBy = value;
            }
        }



        [Property("taxable_class_id", SqlDbType.Int)]
        [PropertyConstraint(false, "TaxableClass", MaxLength = 4000, MinLength = 3)]
        public int TaxableClass
        {
            get
            {
                return taxableClass;
            }
            set
            {
                taxableClass = value;
            }
        }

        [Property("non_taxable_class_id", SqlDbType.Int)]
        [PropertyConstraint(false, "NonTaxableClass", MaxLength = 4000, MinLength = 3)]
        public int NonTaxableClass
        {
            get
            {
                return nonTaxableClass;
            }
            set
            {
                nonTaxableClass = value;
            }
        }
        #endregion

        #region Composite Objects
        private DataTypeContent loginMethodObj;
        [CompositeObject(CompositObjectTypeEnum.Single, "LoginMethodId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable = false)]
        public DataTypeContent LoginMethodObj
        {
            get
            {
                if (loginMethodObj == null)
                    loadCompositObject_Lazy("LoginMethodObj");
                return loginMethodObj;
            }
            set
            {
                loginMethodObj = value; ;
            }
        }

        private DataTypeContent afterCloseRegisterValueObj;
        [CompositeObject(CompositObjectTypeEnum.Single, "AfterCloseRegisterValueId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable = false)]
        public DataTypeContent AfterCloseRegisterValueObj
        {
            get
            {
                if (afterCloseRegisterValueObj == null)
                    loadCompositObject_Lazy("AfterCloseRegisterValueObj");
                return afterCloseRegisterValueObj;
            }
            set
            {
                afterCloseRegisterValueObj = value; ;
            }
        }



        #endregion


        public override int GetIdentity()
        {
            return posConfigId;
        }
        public override void SetIdentity(int value)
        {
            posConfigId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            throw new NotImplementedException();
        }
    }
}