using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Centrix.UM.Business.Entity
{
    public class Team_Lite
    {

        public int Id { get; set; }

        public string Name { get; set; }

        public string NameAr { get; set; }

        public string Description { get; set; }

        public int DepartmentId { get; set; }

        public string Department { get; set; }

        public int CreatedBy { get; set; }

        public string CreatedByName { get; set; }

        public int UpdatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }

        public bool LastRows { get; set; }
        public int TotalRecords { get; set; }
        public string label { set; get; }
        public int value { set; get; }
        public int ManagerId { set; get; }
        public string Manager { set; get; }
        public bool IsManagement { get; set; }
        public int UsersCount { set; get; }
    }
}
