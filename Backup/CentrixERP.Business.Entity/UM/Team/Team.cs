using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;

namespace Centrix.UM.Business.Entity
{
    [EntityIManager("Centrix.UM.Business.IManager.ITeamManager,CentrixERP.Business.IManager", isLogicalDelete = true)]
    public class Team : EntityBase<Team, int>
    {
        private int teamId = -1;
        private string name;
        private string namear;
        private string description;
        private int departmentId = -1;
        private int createdBy = -1;
        private int updatedBy = -1;
        private int managerId = -1;
        public int TeamId
        {
            get { return teamId; }
            set { teamId = value; }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public string NameAr
        {
            get { return namear; }
            set { namear = value; }
        }
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
        public int DepartmentId
        {
            get { return departmentId; }
            set { departmentId = value; }
        }
        public int ManagerId 
        {
            get { return managerId; }
            set { managerId = value; }
        }
        public int CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }
        public int UpdatedBy
        {
            get { return updatedBy; }
            set { updatedBy = value; }
        }

        private CentrixERP.Common.Business.Entity.DataTypeContent department;
        [CompositeObject(CompositObjectTypeEnum.Single, "DepartmentId", LazyLoad = true, SaveBeforeParent = true, CascadeDelete = false,Savable=false)]
        public CentrixERP.Common.Business.Entity.DataTypeContent Department
        {
            get
            {
                if (department == null)
                    loadCompositObject_Lazy("Department");
                return department;
            }
            set { department = value; }
        }


        public override int GetIdentity()
        {
            return teamId;
        }

        public override void SetIdentity(int value)
        {
            teamId = value;
        }

        public override void SetParentId(object value, Type ParentType)
        {
            throw new NotImplementedException();
        }
    }
}
