using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Centrix.UM.Business.Entity
{
    public enum PermissionLevel
    {
        a=1,
        b=2,
        c=3,
        d=4
    }

    public enum PermissionType
    {
        User=1,
        Team=2,
        Role=3
    }
}
