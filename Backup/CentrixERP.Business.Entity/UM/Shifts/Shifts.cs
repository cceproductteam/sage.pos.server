using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace Centrix.UM.Business.Entity
{
    [EntityIManager("Centrix.UM.Business.IManager.IShiftsManager,CentrixERP.Business.IManager", isLogicalDelete = true)]
    public class Shifts : EntityBase<Shifts, int>
    {
        #region Constructors

        public Shifts() { }

        public Shifts(int shiftId)
        {
            this.shiftId = shiftId;
        }

        #endregion

        #region Private Properties

        private int shiftId;

        private string shiftName;

        private string shiftDescription;

        private string startTime;

        private string endTime;

        private int createdBy;

        private int updatedBy;

        private DateTime? createdDate;

        private DateTime? updatedDate;

        private bool? flagDeleted;

        private bool? isSystem;



        //private bool? flagDeleted;

        //private bool? isSystem;

        //private int createdBy;

        //private int updatedBy;

        //private DateTime? createdDate;

        //private DateTime? updatedDate;
        #endregion

        #region Public Properties

        [PrimaryKey("@shift_id", SqlDbType.Int)]
        [Property("shift_id", SqlDbType.Int, AddParameter = false)]
        public int ShiftId
        {
            get
            {
                return shiftId;
            }
            set
            {
                shiftId = value;
            }
        }

        [Property("shift_name", SqlDbType.NVarChar)]
        [PropertyConstraint(true, "ShiftName", MaxLength = 100, MinLength = 3)]
        public string ShiftName
        {
            get
            {
                return shiftName;
            }
            set
            {
                shiftName = value;
            }
        }

        [Property("shift_description", SqlDbType.NVarChar)]
        [PropertyConstraint(true, "ShiftDescription", MaxLength = 100, MinLength = 3)]
        public string ShiftDescription
        {
            get
            {
                return shiftDescription;
            }
            set
            {
                shiftDescription = value;
            }
        }

        [Property("start_time", SqlDbType.NVarChar)]
        [PropertyConstraint(true, "StartTime", MaxLength = 100, MinLength = 3)]
        public string StartTime
        {
            get
            {
                return startTime;
            }
            set
            {
                startTime = value;
            }
        }

        [Property("end_time", SqlDbType.NVarChar)]
        [PropertyConstraint(true, "EndTime", MaxLength = 100, MinLength = 3)]
        public string EndTime
        {
            get
            {
                return endTime;
            }
            set
            {
                endTime = value;
            }
        }



        [Property("flag_deleted", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [Property("is_system", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool? IsSystem
        {
            get
            {
                return isSystem;
            }
            set
            {
                isSystem = value;
            }
        }

        [Property("created_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? CreatedDate
        {
            get
            {
                return createdDate;
            }
            set
            {
                createdDate = value;
            }
        }

        [Property("updated_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? UpdatedDate
        {
            get
            {
                return updatedDate;
            }
            set
            {
                updatedDate = value;
            }
        }

        [Property("created_by", SqlDbType.Int, AddParameter = true, UpdateParameter = false)]
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        [Property("updated_by", SqlDbType.Int, AddParameter = false, UpdateParameter = true)]
        public int UpdatedBy
        {
            get
            {
                return updatedBy;
            }
            set
            {
                updatedBy = value;
            }
        }
        #endregion

        #region Composite Objects


        #endregion


        public override int GetIdentity()
        {
            return shiftId;
        }
        public override void SetIdentity(int value)
        {
            shiftId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            throw new NotImplementedException();
        }
    }
}