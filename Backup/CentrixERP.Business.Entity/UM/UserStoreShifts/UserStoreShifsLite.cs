using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace Centrix.UM.Business.Entity
{
    public class UserStoreShifsLite : EntityBaseLite
    {

        #region Public Properties

        [LiteProperty("user_store_shift_id", SqlDbType.Int)]
        public int UserStoreShiftId
        {
            get;
            set;
        }

        [LiteProperty("user_id", SqlDbType.Int)]
        public int UserId
        {
            get;
            set;
        }

        [LiteProperty("user", SqlDbType.NVarChar)]
        public string User
        {
            get;
            set;
        }

        [LiteProperty("store_id", SqlDbType.Int)]
        public int StoreId
        {
            get;
            set;
        }

        [LiteProperty("store", SqlDbType.NVarChar)]
        public string Store
        {
            get;
            set;
        }

        [LiteProperty("shift_id", SqlDbType.Int)]
        public int ShiftId
        {
            get;
            set;
        }

        [LiteProperty("shift", SqlDbType.NVarChar)]
        public string Shift
        {
            get;
            set;
        }

        [LiteProperty("validate_shift", SqlDbType.Bit)]
        public bool? ValidateShift
        {
            get;
            set;
        }



        [LiteProperty("user_store_shift_id", SqlDbType.Int)]
        public int value
        {
            get;
            set;
        }

        [LiteProperty("shift", SqlDbType.NVarChar)]
        public string label
        {
            get;
            set;
        }

        [LiteProperty("flag_deleted", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? FlagDeleted
        {
            get;
            set;
        }

        [LiteProperty("is_system", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? IsSystem
        {
            get;
            set;
        }

        [LiteProperty("created_date", SqlDbType.DateTime)]
        public DateTime? CreatedDate
        {
            get;
            set;
        }

        [LiteProperty("updated_date", SqlDbType.DateTime)]
        public DateTime? UpdatedDate
        {
            get;
            set;
        }

        [LiteProperty("created_by", SqlDbType.Int, FindByIdLiteParameter = false)]
        public int CreatedBy
        {
            get;
            set;
        }

        [LiteProperty("updated_by", SqlDbType.Int, FindByIdLiteParameter = false)]
        public int UpdatedBy
        {
            get;
            set;
        }

        [LiteProperty("created_by_name", SqlDbType.NVarChar, FindAllLiteParameter = true)]
        public string CreatedByName { get; set; }

        [LiteProperty("updated_by_name", SqlDbType.NVarChar, FindAllLiteParameter = true)]
        public string UpdatedByName { get; set; }

        [LiteProperty("user_store_shift_id", SqlDbType.Int)]
        public int Id
        {
            get { return this.UserStoreShiftId; }
            set { this.UserStoreShiftId = value; }
        }

        [LiteProperty("last_rows", SqlDbType.NVarChar)]
        public bool LastRows { get; set; }

        [LiteProperty("total_count", SqlDbType.NVarChar)]
        public int TotalRecords { get; set; }
        #endregion


    }
}