using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
using CentrixERP.Business.Entity;

namespace Centrix.UM.Business.Entity
{
    [EntityIManager("Centrix.UM.Business.IManager.IUserStoreShifsManager,CentrixERP.Business.IManager", isLogicalDelete = true)]
    public class UserStoreShifs : EntityBase<UserStoreShifs, int>
    {
        #region Constructors

        public UserStoreShifs() { }

        public UserStoreShifs(int userStoreShiftId)
        {
            this.userStoreShiftId = userStoreShiftId;
        }

        #endregion

        #region Private Properties

        private int userStoreShiftId;

        private int userId = -1;

        private int storeId = -1;

        private int shiftId = -1;

        private int createdBy;

        private DateTime? createdDate;

        private int updatedBy;

        private DateTime? updatedDate;

        private bool? flagDeleted;

        private bool? isSystem;

        private bool? validateShift;



        //private bool? flagDeleted;

        //private bool? isSystem;

        //private int createdBy;

        //private int updatedBy;

        //private DateTime? createdDate;

        //private DateTime? updatedDate;
        #endregion

        #region Public Properties

        [PrimaryKey("@user_store_shift_id", SqlDbType.Int)]
        [Property("user_store_shift_id", SqlDbType.Int, AddParameter = false)]
        public int UserStoreShiftId
        {
            get
            {
                return userStoreShiftId;
            }
            set
            {
                userStoreShiftId = value;
            }
        }

        [Property("user_id", SqlDbType.Int, ReferenceParameter = true)]
        [PropertyConstraint(true, "User", RefEntity = true, RefPropertyEntity = "UserObj")]
        public int UserId
        {
            get
            {
                return userId;
            }
            set
            {
                userId = value;
            }
        }

        [Property("store_id", SqlDbType.Int, ReferenceParameter = true)]
        [PropertyConstraint(true, "Store", RefEntity = true, RefPropertyEntity = "StoreObj")]
        public int StoreId
        {
            get
            {
                return storeId;
            }
            set
            {
                storeId = value;
            }
        }

        [Property("shift_id", SqlDbType.Int, ReferenceParameter = true)]
        [PropertyConstraint(false, "Shift", RefEntity = true, RefPropertyEntity = "ShiftsObj")]
        public int ShiftId
        {
            get
            {
                return shiftId;
            }
            set
            {
                shiftId = value;
            }
        }

        [Property("validate_shift", SqlDbType.Bit)]
        [PropertyConstraint(false, "ValidateShift")]
        public bool? ValidateShift
        {
            get
            {
                return validateShift;
            }
            set
            {
                validateShift = value;
            }
        }



        [Property("flag_deleted", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [Property("is_system", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool? IsSystem
        {
            get
            {
                return isSystem;
            }
            set
            {
                isSystem = value;
            }
        }

        [Property("created_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? CreatedDate
        {
            get
            {
                return createdDate;
            }
            set
            {
                createdDate = value;
            }
        }

        [Property("updated_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? UpdatedDate
        {
            get
            {
                return updatedDate;
            }
            set
            {
                updatedDate = value;
            }
        }

        [Property("created_by", SqlDbType.Int, AddParameter = true, UpdateParameter = false)]
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        [Property("updated_by", SqlDbType.Int, AddParameter = false, UpdateParameter = true)]
        public int UpdatedBy
        {
            get
            {
                return updatedBy;
            }
            set
            {
                updatedBy = value;
            }
        }
        #endregion

        #region Composite Objects
        private User userObj;
        [CompositeObject(CompositObjectTypeEnum.Single, "UserId", SaveBeforeParent = false, LazyLoad = false, CascadeDelete = true, Savable = false)]
        public User UserObj
        {
            get
            {
                if (userObj == null)
                    loadCompositObject_Lazy("UserObj");
                return userObj;
            }
            set
            {
                userObj = value; ;
            }
        }

        private Store storeObj;
        [CompositeObject(CompositObjectTypeEnum.Single, "StoreId", SaveBeforeParent = false, LazyLoad = false, CascadeDelete = true, Savable = false)]
        public Store StoreObj
        {
            get
            {
                if (storeObj == null)
                    loadCompositObject_Lazy("StoreObj");
                return storeObj;
            }
            set
            {
                storeObj = value; ;
            }
        }

        private Shifts shiftsObj;
        [CompositeObject(CompositObjectTypeEnum.Single, "ShiftId", SaveBeforeParent = false, LazyLoad = false, CascadeDelete = true, Savable = false)]
        public Shifts ShiftsObj
        {
            get
            {
                if (shiftsObj == null)
                    loadCompositObject_Lazy("ShiftsObj");
                return shiftsObj;
            }
            set
            {
                shiftsObj = value; ;
            }
        }



        #endregion


        public override int GetIdentity()
        {
            return userStoreShiftId;
        }
        public override void SetIdentity(int value)
        {
            userStoreShiftId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            if (ParentType == typeof(User))
                this.userId = (int)value;
            else if (ParentType == typeof(Store))
                this.storeId = (int)value;
            else if (ParentType == typeof(Shifts))
                this.shiftId = (int)value;

        }
    }
}