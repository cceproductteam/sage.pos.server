using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;

namespace Centrix.UM.Business.Entity
{
    public class UserLogginCriteria : CriteriaBase
    {
        public int? UserId { get; set; }
        public string RandomString { get; set; }
        public string LocalIP { get; set; }
        public string PublicIP { get; set; }
        public string UserAgent { get; set; }
    }
}
