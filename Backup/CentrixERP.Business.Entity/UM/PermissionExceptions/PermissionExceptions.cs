using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;

namespace Centrix.UM.Business.Entity
{
    [EntityIManager("Centrix.UM.Business.IManager.IPermissionExceptionsManager,CentrixERP.Business.IManager", isLogicalDelete = false)]
    public class PermissionExceptions : EntityBase<PermissionExceptions, int>
    {
        private int permissionExceptionsId = -1;
        private int id = -1;
        private PermissionType type;
        private bool dos;
        private int userId = -1;
        private int teamId = -1;
        private int roleId = -1;
        private int createdBy = -1;
        private int updatedBy = -1;

        private User myUser;    
        private Role myRole;
        private Team myTeam;

        public int PermissionExceptionsId
        {
            get { return permissionExceptionsId; }
            set { permissionExceptionsId = value; }
        }
        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        internal PermissionType Type
        {
            get { return type; }
            set { type = value; }
        }
        public bool Dos
        {
            get { return dos; }
            set { dos = value; }
        }
        public int UserId
        {
            get { return userId; }
            set { userId = value; }
        }
        public int TeamId
        {
            get { return teamId; }
            set { teamId = value; }
        }
        public int RoleId
        {
            get { return roleId; }
            set { roleId = value; }
        }
        public int CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }
        public int UpdatedBy
        {
            get { return updatedBy; }
            set { updatedBy = value; }
        }


        [CompositeObject(CompositObjectTypeEnum.Single, "RoleId", LazyLoad = true, SaveBeforeParent = true, CascadeDelete = true)]
        public Role MyRole
        {
            get
            {
                if (myRole == null)
                    loadCompositObject_Lazy("MyRole");
                return myRole;
            }
            set { myRole = value; myRole.MarkModified(); }
        }

        [CompositeObject(CompositObjectTypeEnum.Single, "TeamId", LazyLoad = true, SaveBeforeParent = true, CascadeDelete = true)]
        public Team MyTeam
        {
            get
            {
                if (myTeam == null)
                    loadCompositObject_Lazy("MyTeam");
                return myTeam;
            }
            set { myTeam = value; myTeam.MarkModified(); }
        }


        [CompositeObject(CompositObjectTypeEnum.Single, "UserId", LazyLoad = true, SaveBeforeParent = true, CascadeDelete = true)]
        public User MyUser
        {
            get
            {
                if (myUser == null)
                    loadCompositObject_Lazy("MyUser");
                return myUser;
            }
            set { myUser = value; myUser.MarkModified(); }
        }


        public override int GetIdentity()
        {
            return permissionExceptionsId;
        }

        public override void SetIdentity(int value)
        {
            permissionExceptionsId = value;
        }

        public override void SetParentId(object value, Type ParentType)
        {
            throw new NotImplementedException();
        }
    }
}
