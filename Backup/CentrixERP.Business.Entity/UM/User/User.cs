using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using CentrixERP.Common.Business.Entity;
using Centrix.UM.Business.Entity;

namespace Centrix.UM.Business.Entity
{
    [Serializable]
    [EntityIManager("Centrix.UM.Business.IManager.IUserManager,CentrixERP.Business.IManager", isLogicalDelete = true)]
    public class User : EntityBase<User, int>
    {
        private int userId = -1;
        private string userName;
        private string password;
        private string firstnameEnglish;
        private string middleNameEnglish;
        private string lastNameEnglish;
        private string firstNameArabic;
        private string middleNameArabic;
        private string lastNameArabic;
        private DateTime? dob;
        private string title;
        private string notes;
        private int createdBy = -1;
        private int updatedBy = -1;
        private int roleId = -1;
        private int teamId = -1;
        private Boolean isDefault;
        private bool receiveNoyifications = true;
        private string fullName;
        private bool isManager;
        private int managerId;
        private bool isSupperUser;
        private bool isPosUser;

        private Role myRole;
        private Team myTeam;
        private bool flagDeleted;
        private string accessCode;

        public string AccessCode
        {
            get { return accessCode; }
            set { accessCode = value; }
        }
        
        private List<UserArea> myUserArea;
        //private List<UserCompany> myUserCompany;
        //private List<UserPerson> myUserPerson;
        public bool FlagDeleted
        {
            get { return flagDeleted; }
            set { flagDeleted = value; }
        }
        public bool IsSupperUser
        {
            get { return isSupperUser; }
            set { isSupperUser = value; }
        }

        public bool IsPosUser
        {
            get { return isPosUser; }
            set { isPosUser = value; }
        }
        public Boolean IsDefault
        {
            get { return isDefault; }
            set { isDefault = value; }
        }

        public Boolean IsManager
        {
            get { return isManager; }
            set { isManager = value; }
        }

        public int ManagerId
        {
            get { return managerId; }
            set { managerId = value; }
        }

        public int UserId
        {
            get { return userId; }
            set { userId = value; }
        }
        public string UserName
        {
            get { return userName; }
            set { userName = value; }
        }
        public string Password
        {
            get { return password; }
            set { password = value; }
        }
        public string FirstnameEnglish
        {
            get { return firstnameEnglish; }
            set { firstnameEnglish = value; }
        }
        public string MiddleNameEnglish
        {
            get { return middleNameEnglish; }
            set { middleNameEnglish = value; }
        }
        public string LastNameEnglish
        {
            get { return lastNameEnglish; }
            set { lastNameEnglish = value; }
        }
        public string FirstNameArabic
        {
            get { return firstNameArabic; }
            set { firstNameArabic = value; }
        }
        public string MiddleNameArabic
        {
            get { return middleNameArabic; }
            set { middleNameArabic = value; }
        }
        public string LastNameArabic
        {
            get { return lastNameArabic; }
            set { lastNameArabic = value; }
        }
        public string FullNameEn
        {
            get { return firstnameEnglish + " " + middleNameEnglish + " " + lastNameEnglish; }
        }
        public string FullNameAr
        {
            get { return firstNameArabic + " " + middleNameArabic + " " + lastNameArabic; }
        }
        public DateTime? Dob
        {
            get { return dob; }
            set { dob = value; }
        }
        public string Title
        {
            get { return title; }
            set { title = value; }
        }
        public string Notes
        {
            get { return notes; }
            set { notes = value; }
        }
        public int CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }
        public int UpdatedBy
        {
            get { return updatedBy; }
            set { updatedBy = value; }
        }
        public int RoleId
        {
            get { return roleId; }
            set { roleId = value; }
        }
        public int TeamId
        {
            get { return teamId; }
            set { teamId = value; }
        }

        public bool ReceiveNoyifications
        {
            get { return receiveNoyifications; }
            set { receiveNoyifications = value; }
        }

        public string Fullname
        {
            get
            {
                return this.fullName;
            }
            set
            {
                this.fullName = value;
            }
        }


        [CompositeObject(CompositObjectTypeEnum.Single, "RoleId", LazyLoad = true, SaveBeforeParent = true, CascadeDelete = false)]
        public Role MyRole
        {
            get
            {
                if (myRole == null)
                    loadCompositObject_Lazy("MyRole");
                return myRole;
            }
            set { myRole = value; myRole.MarkModified(); }
        }

        [CompositeObject(CompositObjectTypeEnum.Single, "TeamId", LazyLoad = true, SaveBeforeParent = true, CascadeDelete = false)]
        public Team MyTeam
        {
            get
            {
                if (myTeam == null)
                    loadCompositObject_Lazy("MyTeam");
                return myTeam;
            }
            set { myTeam = value; myTeam.MarkModified(); }
        }

        //[CompositeObject(CompositObjectTypeEnum.List, "UserId", LazyLoad = true, SaveBeforeParent = false, CascadeDelete = true)]
        //public List<UserArea> MyUserArea
        //{
        //    get
        //    {
        //        if (myUserArea == null)
        //            loadCompositObject_Lazy("MyUserArea");
        //        return myUserArea;
        //    }
        //    set { myUserArea = value; }
        //}

        //[CompositeObject(CompositObjectTypeEnum.List, "UserId", LazyLoad = true, SaveBeforeParent = false, CascadeDelete = true)]
        //public List<UserCompany> MyUserCompany
        //{
        //    get
        //    {
        //        if (myUserCompany == null)
        //            loadCompositObject_Lazy("MyUserCompany");
        //        return myUserCompany;
        //    }
        //    set { myUserCompany = value; }
        //}

        //[CompositeObject(CompositObjectTypeEnum.List, "UserId", LazyLoad = true, SaveBeforeParent = false, CascadeDelete = true)]
        //public List<UserPerson> MyUserPerson
        //{
        //    get
        //    {
        //        if (myUserPerson == null)
        //            loadCompositObject_Lazy("MyUserPerson");
        //        return myUserPerson;
        //    }
        //    set { myUserPerson = value; }
        //}


      
        //private List<EmailEntity> myEmailList;
        //[CompositeObject(CompositObjectTypeEnum.List, "UserId", LazyLoad = true, CascadeDelete = false)]
        //public List<EmailEntity> MyEmailList
        //{
        //    get
        //    {
        //        if (myEmailList == null)
        //            loadCompositObject_Lazy("MyEmailList");
        //        return myEmailList;
        //    }
        //    set { myEmailList = value; }
        //}


        public bool IsAdmin { get { return roleId.Equals(1) ? true : false; } }

        public override int GetIdentity()
        {
            return userId;
        }

        public override void SetIdentity(int value)
        {
            userId = value;
        }

        public override void SetParentId(object value, Type ParentType)
        {
            throw new NotImplementedException();
        }
    }
}
