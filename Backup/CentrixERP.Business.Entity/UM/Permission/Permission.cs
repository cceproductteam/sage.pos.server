using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;

namespace Centrix.UM.Business.Entity
{
    [EntityIManager("Centrix.UM.Business.IManager.IPermissionManager,CentrixERP.Business.IManager", isLogicalDelete = false)]
    public class Permission : EntityBase<Permission, int>
    {
        private int permissionId = -1;
        private string nameArabic;
        private string nameEnglish;
        private string description;
        private bool defaultPermission;
        private int createdBy = -1;
        private int updatedBy = -1;

        #region Constructor

        public Permission()
        {
        }
        public Permission(int id)
        {
            permissionId = id;
        }

        #endregion
        public int PermissionId
        {
            get { return permissionId; }
            set { permissionId = value; }
        }
        public string NameArabic
        {
            get { return nameArabic; }
            set { nameArabic = value; }
        }
        public string NameEnglish
        {
            get { return nameEnglish; }
            set { nameEnglish = value; }
        }
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
        public bool DefaultPermission
        {
            get { return defaultPermission; }
            set { defaultPermission = value; }
        }
        public int CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }
        public int UpdatedBy
        {
            get { return updatedBy; }
            set { updatedBy = value; }
        }

        public override int GetIdentity()
        {
            return permissionId;
        }

        public override void SetIdentity(int value)
        {
            permissionId = value;
        }

        public override void SetParentId(object value, Type ParentType)
        {
            throw new NotImplementedException();
        }
    }
}
