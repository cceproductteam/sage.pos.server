using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace CentrixERP.Common.Business.Entity
{
    [EntityIManager("CentrixERP.Common.Business.IManager.INoteEntityManager,CentrixERP.Business.IManager", isLogicalDelete = true)]
    public class NoteEntity : EntityBase<NoteEntity, int>
    {
        #region Constructors

        public NoteEntity() { }

        public NoteEntity(int noteEntityId)
        {
            this.noteEntityId = noteEntityId;
        }

        #endregion

        #region Private Properties

        private int noteEntityId;

        private int noteId = -1;

        private int entityId = -1;

        private int entityValueId;

        private int createdBy;

        private DateTime? createdDate;

        private int updatedBy;

        private DateTime? updatedDate;

        private bool? flagDeleted;

        private bool? isSystem;


        #endregion

        #region Public Properties

        [PrimaryKey("@note_entity_id", SqlDbType.Int)]
        [Property("note_entity_id", SqlDbType.Int, AddParameter = false)]
        public int NoteEntityId
        {
            get
            {
                return noteEntityId;
            }
            set
            {
                noteEntityId = value;
            }
        }

        [Property("note_id", SqlDbType.Int)]
        [PropertyConstraint(true, "NoteId")]
        public int NoteId
        {
            get
            {
                return noteId;
            }
            set
            {
                noteId = value;
            }
        }

        [Property("entity_id", SqlDbType.Int)]
        [PropertyConstraint(true, "EntityId")]
        public int EntityId
        {
            get
            {
                return entityId;
            }
            set
            {
                entityId = value;
            }
        }

        [Property("entity_value_id", SqlDbType.Int)]
        [PropertyConstraint(true, "EntityValueId")]
        public int EntityValueId
        {
            get
            {
                return entityValueId;
            }
            set
            {
                entityValueId = value;
            }
        }



        [Property("flag_deleted", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [Property("is_system", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool? IsSystem
        {
            get
            {
                return isSystem;
            }
            set
            {
                isSystem = value;
            }
        }

        [Property("created_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? CreatedDate
        {
            get
            {
                return createdDate;
            }
            set
            {
                createdDate = value;
            }
        }

        [Property("updated_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? UpdatedDate
        {
            get
            {
                return updatedDate;
            }
            set
            {
                updatedDate = value;
            }
        }

        [Property("created_by", SqlDbType.Int, AddParameter = true, UpdateParameter = false)]
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        [Property("updated_by", SqlDbType.Int, AddParameter = false, UpdateParameter = true)]
        public int UpdatedBy
        {
            get
            {
                return updatedBy;
            }
            set
            {
                updatedBy = value;
            }
        }
        #endregion

        private Note noteObj;
        [CompositeObject(CompositObjectTypeEnum.Single, "NoteId", Savable = true, CascadeDelete = true, SaveBeforeParent = true)]
        public Note NoteObj
        {
            get
            {
                if (this.noteObj == null)
                    loadCompositObject_Lazy("NoteObj");
                return this.noteObj;
            }
            set
            {
                this.noteObj = value;
            }
        }

        public override int GetIdentity()
        {
            return noteEntityId;
        }
        public override void SetIdentity(int value)
        {
            noteEntityId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            if (ParentType == typeof(Note))
                this.noteId = (int)value;
            else if (ParentType == typeof(SystemEntity))
                this.entityId = (int)value;

        }
    }
}