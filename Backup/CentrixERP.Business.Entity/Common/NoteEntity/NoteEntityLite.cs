using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace CentrixERP.Common.Business.Entity
{
    public class NoteEntityLite : EntityBaseLite
    {
        #region Private Properties

        private int noteEntityId;

        private int noteId = -1;

        private int entityId = -1;

        private int entityValueId;

        private int createdBy;

        private DateTime? createdDate;

        private int updatedBy;

        private DateTime? updatedDate;

        private bool? flagDeleted;

        private bool? isSystem;



        #endregion
        #region Public Properties

        [LiteProperty("note_entity_id", SqlDbType.Int)]
        public int NoteEntityId
        {
            get
            {
                return noteEntityId;
            }
            set
            {
                noteEntityId = value;
            }
        }

        [LiteProperty("note_id", SqlDbType.Int)]
        public int NoteId
        {
            get
            {
                return noteId;
            }
            set
            {
                noteId = value;
            }
        }

        [LiteProperty("entity_id", SqlDbType.Int)]
        public int EntityId
        {
            get
            {
                return entityId;
            }
            set
            {
                entityId = value;
            }
        }

        [LiteProperty("entity_value_id", SqlDbType.Int)]
        public int EntityValueId
        {
            get
            {
                return entityValueId;
            }
            set
            {
                entityValueId = value;
            }
        }

        [LiteProperty("note_content", SqlDbType.NVarChar, FindAllLiteParameter = true)]
        public string NoteContent { get; set; }

        [LiteProperty("flag_deleted", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [LiteProperty("is_system", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? IsSystem
        {
            get
            {
                return isSystem;
            }
            set
            {
                isSystem = value;
            }
        }

        [LiteProperty("created_date", SqlDbType.DateTime)]
        public DateTime? CreatedDate
        {
            get
            {
                return createdDate;
            }
            set
            {
                createdDate = value;
            }
        }

        [LiteProperty("updated_date", SqlDbType.DateTime)]
        public DateTime? UpdatedDate
        {
            get
            {
                return updatedDate;
            }
            set
            {
                updatedDate = value;
            }
        }

        [LiteProperty("created_by", SqlDbType.Int, FindByIdLiteParameter = false)]
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        [LiteProperty("updated_by", SqlDbType.Int, FindByIdLiteParameter = false)]
        public int UpdatedBy
        {
            get
            {
                return updatedBy;
            }
            set
            {
                updatedBy = value;
            }
        }

        [LiteProperty("created_by_name", SqlDbType.NVarChar, FindAllLiteParameter = true)]
        public string CreatedByName { get; set; }

        [LiteProperty("updated_by_name", SqlDbType.NVarChar, FindAllLiteParameter = true)]
        public string UpdatedByName { get; set; }

        [LiteProperty("note_entity_id", SqlDbType.NVarChar)]
        public int Id
        {
            get { return this.NoteEntityId; }
            set { this.noteEntityId = value; }
        }

        [LiteProperty("last_rows", SqlDbType.NVarChar)]
        public bool LastRows { get; set; }

        [LiteProperty("total_count", SqlDbType.NVarChar)]
        public int TotalRecords { get; set; }
        #endregion


    }
}