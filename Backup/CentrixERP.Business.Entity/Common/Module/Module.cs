using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;

namespace CentrixERP.Common.Business.Entity
{
    [EntityIManager("CentrixERP.Common.Business.IManager.IModuleManager,CentrixERP.Business.IManager", isLogicalDelete = true)]
    public class Module : EntityBase<Module, int>
    {
        #region Constructors

        public Module() { }

        public Module(int moduleId)
        {
            this.moduleId = moduleId;
        }

        #endregion

        #region Private Properties
		private int moduleId;
		private string moduleNameEn;
		private string moduleNameAr;
		private bool? hasDocs;
		private int? createdBy;
		private DateTime? createdDate;
		private int? updatedBy;
		private DateTime? updatedDate;
		private bool? isSystem;

        #endregion

        #region Public Properties
		public int ModuleId
		{
			get
			{
				return moduleId;
			}
			set
			{
				moduleId = value;
			}
		}
		public string ModuleNameEn
		{
			get
			{
				return moduleNameEn;
			}
			set
			{
				moduleNameEn = value;
			}
		}
		public string ModuleNameAr
		{
			get
			{
				return moduleNameAr;
			}
			set
			{
				moduleNameAr = value;
			}
		}
		public bool? HasDocs
		{
			get
			{
				return hasDocs;
			}
			set
			{
				hasDocs = value;
			}
		}
		public int? CreatedBy
		{
			get
			{
				return createdBy;
			}
			set
			{
				createdBy = value;
			}
		}
		public DateTime? CreatedDate
		{
			get
			{
				return createdDate;
			}
			set
			{
				createdDate = value;
			}
		}
		public int? UpdatedBy
		{
			get
			{
				return updatedBy;
			}
			set
			{
				updatedBy = value;
			}
		}
		public DateTime? UpdatedDate
		{
			get
			{
				return updatedDate;
			}
			set
			{
				updatedDate = value;
			}
		}
		public bool? IsSystem
		{
			get
			{
				return isSystem;
			}
			set
			{
				isSystem = value;
			}
		}

        #endregion

        #region Composite Objects

        #endregion

        public override int GetIdentity()
        {
            return moduleId;
        }
        public override void SetIdentity(int value)
        {
            moduleId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
			throw new NotImplementedException();
        }
    }
}