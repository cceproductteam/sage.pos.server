using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;

namespace CentrixERP.Common.Business.Entity
{
    [EntityIManager("CentrixERP.Common.Business.IManager.IEntityControlConfigManager,CentrixERP.Business.IManager", isLogicalDelete = true)]
    public class EntityControlConfig : EntityBase<EntityControlConfig, int>
    {
        #region Constructors

        public EntityControlConfig() { }

        public EntityControlConfig(int entityControlConfigId)
        {
            this.entityControlConfigId = entityControlConfigId;
        }

        #endregion

        #region Private Properties

		private int entityControlConfigId;

		private int entityControlId = -1;

		private int userId;

		private int searchCriteriaId = -1;

		private bool? searchable;

		private bool? quickSearch;

		private int createdBy;

		private int updatedBy;

		private DateTime? createdDate;

		private DateTime? updatedDate;

		private bool? isSystem;

		private bool? flagDeleted;


        #endregion

        #region Public Properties

		public int EntityControlConfigId
		{
			get
			{
				return entityControlConfigId;
			}
			set
			{
				entityControlConfigId = value;
			}
		}
		public int EntityControlId
		{
			get
			{
				return entityControlId;
			}
			set
			{
				entityControlId = value;
			}
		}
		public int UserId
		{
			get
			{
				return userId;
			}
			set
			{
				userId = value;
			}
		}
		public int SearchCriteriaId
		{
			get
			{
				return searchCriteriaId;
			}
			set
			{
				searchCriteriaId = value;
			}
		}
		public bool? Searchable
		{
			get
			{
				return searchable;
			}
			set
			{
				searchable = value;
			}
		}
		public bool? QuickSearch
		{
			get
			{
				return quickSearch;
			}
			set
			{
				quickSearch = value;
			}
		}
		public int CreatedBy
		{
			get
			{
				return createdBy;
			}
			set
			{
				createdBy = value;
			}
		}
		public int UpdatedBy
		{
			get
			{
				return updatedBy;
			}
			set
			{
				updatedBy = value;
			}
		}
		public DateTime? CreatedDate
		{
			get
			{
				return createdDate;
			}
			set
			{
				createdDate = value;
			}
		}
		public DateTime? UpdatedDate
		{
			get
			{
				return updatedDate;
			}
			set
			{
				updatedDate = value;
			}
		}
		public bool? IsSystem
		{
			get
			{
				return isSystem;
			}
			set
			{
				isSystem = value;
			}
		}
		public bool? FlagDeleted
		{
			get
			{
				return flagDeleted;
			}
			set
			{
				flagDeleted = value;
			}
		}
        public int EntityReferanceId { get; set; }

        #endregion

        #region Composite Objects
        //private TblEntityControls tblEntityControlsObj;
        //[CompositeObject(CompositObjectTypeEnum.Single, "EntityControlId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable= false)]
        //public TblEntityControls TblEntityControlsObj
        //{
        //    get
        //    {
        //        if (tblEntityControlsObj == null)
        //            loadCompositObject_Lazy("TblEntityControlsObj");
        //        return tblEntityControlsObj;
        //    }
        //    set
        //    {
        //        tblEntityControlsObj = value;;
        //    }
        //}
		
		private SearchCriteria searchCriteriaObj;
		[CompositeObject(CompositObjectTypeEnum.Single, "SearchCriteriaId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable= false)]
		public SearchCriteria SearchCriteriaObj
		{
			get
			{
                if (searchCriteriaObj == null)
                    loadCompositObject_Lazy("SearchCriteriaObj");
                return searchCriteriaObj;
			}
			set
			{
                searchCriteriaObj = value; ;
			}
		}
		


        #endregion

        public override int GetIdentity()
        {
            return entityControlConfigId;
        }
        public override void SetIdentity(int value)
        {
            entityControlConfigId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            //if (ParentType == typeof(EntityControls))
            //    this.entityControlId = (int)value;
			 if (ParentType == typeof(SearchCriteria))
				this.searchCriteriaId = (int)value;

        }
    }
}