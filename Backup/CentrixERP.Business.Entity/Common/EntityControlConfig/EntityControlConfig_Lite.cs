using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CentrixERP.Common.Business.Entity
{
   public class EntityControlConfig_Lite
    {
        public int EntityControlConfigId {set;get;}
       
        public int EntityControlId {set;get;}
       
        public int UserId {set;get;}
       
        public int SearchCriteriaId {set;get;}
        
        public bool? Searchable {set;get;}
       
        public bool? QuickSearch {set;get;}
       
        public int CreatedBy {set;get;}
       
        public int UpdatedBy {set;get;}
    
        public DateTime? CreatedDate {set;get;}

        public DateTime? UpdatedDate { set; get; }

       public string UINameAr {set;get;}
       public string UIName { set; get; }
       public string UIValue { set; get; }
       public string UIValueAr { set; get; }
       public int ControlId { set; get; }
       public int ControlTypeId { set; get; }
       public string ControlName { get; set; }
       public int EntityId { set; get; }
       public string EntityName { set; get; }
       public string EntityNameAr { set; get; }
       public int DataTypeContentId { set; get; }
       public string PropertyNameAr { set; get; }
       public string PropertyName { set; get; }
       public int ProprtyParentId { set; get; }
       public string ServiceMethod { get; set; }
       public string ServiceName { get; set; }
       public string ControlValue { set; get; }
       public string label
       {
           get
           {
               if (CentrixERP.Configuration.Configuration.Lang == CentrixERP.Configuration.Enums_S3.Configuration.Language.ar)
                   return UINameAr;
               else return UIName;
           }
       }
       public int value { get; set; }
       public string ColumnNameAr { set; get; }
       public string ColumnName { set; get; }
       public bool ShowInSearchResult { get; set; }
       public string ChildAC { get; set; }
       public string ChildACValue { get; set; }
       public string MethodArgs { set; get; }
       public string MethodArgsValues { set; get; }
       public List<EntityControlConfigEvents_Lite> EventsList { set; get; }
       public int EntityReferanceId { get; set; }
       public string EntityReferanceUrl { get; set; }
    }
}
