﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CentrixERP.AdministrativeSettings.Business.Entity
{
    public class POSIntegrationCurrency
    {
        public int CurrencyId
        {
            get;
            set;
        }

        public string Code
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public string Symbol
        {
            get;
            set;
        }


        public bool FlagDeleted
        {
            get;
            set;
        }

    }
}
