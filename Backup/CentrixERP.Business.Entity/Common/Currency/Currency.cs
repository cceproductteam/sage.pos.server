using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;


namespace CentrixERP.AdministrativeSettings.Business.Entity
{
    [EntityIManager("CentrixERP.AdministrativeSettings.Business.IManager.ICurrencyManager,CentrixERP.Business.IManager", isLogicalDelete = true)]
    public class Currency: EntityBase< Currency, int>
    {
        #region "Constructors"
        public Currency()
        {
            }
        
        public Currency(int currency_id)
        {
            this.currencyid=currency_id;
            }
        #endregion
        
        #region "Private Property"
		private int currencyid=-1;
		private string code;
		private string description;
		private string symbol;
		private int decimalplaces=-1;
		private bool issystem;
		private bool flagdeleted;
		private int createdby=-1;
		private int updatedby=-1;
		private DateTime? createddate;
		private DateTime? updateddate;
        private bool isFunctional = false;
        #endregion
        
        #region "Public Property"
		public int CurrencyId
		{
			get {return currencyid;}
			set {currencyid=value;}
		}

		public string Code
		{
			get {return code;}
			set {code=value;}
		}

		public string Description
		{
			get {return description;}
			set {description=value;}
		}

		public string Symbol
		{
			get {return symbol;}
			set {symbol=value;}
		}

		public int DecimalPlaces
		{
			get {return decimalplaces;}
			set {decimalplaces=value;}
		}

		public bool IsSystem
		{
			get {return issystem;}
			set {issystem=value;}
		}

		public bool FlagDeleted
		{
			get {return flagdeleted;}
			set {flagdeleted=value;}
		}

		public int CreatedBy
		{
			get {return createdby;}
			set {createdby=value;}
		}

		public int UpdatedBy
		{
			get {return updatedby;}
			set {updatedby=value;}
		}

		public DateTime? CreatedDate
		{
			get {return createddate;}
			set {createddate=value;}
		}

		public DateTime? UpdatedDate
		{
			get {return updateddate;}
			set {updateddate=value;}
		}
        public bool ConnectedItems { get; set; }

        public bool IsFunctional {
            get { return isFunctional; }
            set { isFunctional = value; }
        }

        public bool LastRows { get; set; }
        public int TotalCount { get; set; }
        public int Id { get; set; }
        public int TotalRecords { get; set; }
        public string CreatedByName { get; set; }
        public string UpdatedByName { get; set; }
        #endregion
        
        #region "Composite Objects"

        //private List<TaxAuthority> taxauthoritylist;
        //[CompositeObject(CompositObjectTypeEnum.List, "ReportingCurrencyId", LazyLoad = true, SaveBeforeParent = false, CascadeDelete = true)]
        //public List<TaxAuthority> TaxAuthorityList
        //{
        //    get{
        //        if (taxauthoritylist == null)
        //            loadCompositObject_Lazy("TaxAuthorityList");
        //        return taxauthoritylist; }
        //    set { taxauthoritylist = value; }
        //}
        #endregion
        
        #region "Entity Base Methods"
         public override int GetIdentity()
        {
            return currencyid;
        }

        public override void SetIdentity(int value)
        {
             currencyid = value;
        }

        public override void SetParentId(object value, Type ParentType)
        {
            throw new NotImplementedException();
        }
        
        #endregion 
    }
}


