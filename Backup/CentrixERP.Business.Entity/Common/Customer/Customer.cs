using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Common.Business.Entity;
using CentrixERP.AdministrativeSettings.Business.Entity;

namespace CentrixERP.AccountsReceivable.Business.Entity
{
    [EntityIManager("CentrixERP.AccountsReceivable.Business.IManager.ICustomerManager,CentrixERP.Business.IManager", isLogicalDelete = true)]
    public class Customer : EntityBase<Customer, int>
    {
        #region Constructors

        public Customer() { }

        public Customer(int customerId)
        {
            this.customerId = customerId;
        }

        #endregion

        #region Private Properties

		private int customerId;

		private string customerNumber;

		private string customerName;

		private int customerGroupId = -1;

		private bool onHold;

		private bool status;

		private string shortName;

		private DateTime? startDate;

		private string addressLine1;

		private string addressLine2;

		private string addressLine3;

		private string addressLine4;

		private string city;

		private string country;

		private string stateProv;

		private string postalCode;

		private string telephone;

		private string email;

		private string website;

		private string contact;

		private string contactTelephone;

		private string contactFax;

		private string contactEmail;

		private int accountTypeId = -1;

		private bool printStatements;

		private int accountSetId = -1;

		private int paymentTermCodeId = -1;

		private int paymentCodeId = -1;

		private int deliveryMethodId = -1;

		private bool outstandingBalanceExceedsLimit;

		private double creditLimitAmount;

		private bool transactionsOverdueNdaysExceedOverdueLimit;

		private int nDays;

        private double overdueLimit;

		private int customerTypeId = -1;

		private int checkDuplicatPosId = -1;

		private int taxGroupId = -1;

		private int taxAuthorityId = -1;

		private int taxClassId = -1;

		private int salesPersonId = -1;

        private double percentage;

		private DateTime? createdDate;

		private int createdBy;

		private DateTime? updatedDate;

		private int updatedBy;

		private bool flagDeleted;

		private bool isSytem;

        private int crCompanyId;
        private string crCompanyName;

        #endregion

        #region Public Properties

		public int CustomerId
		{
			get
			{
				return customerId;
			}
			set
			{
				customerId = value;
			}
		}
		public string CustomerNumber
		{
			get
			{
				return customerNumber;
			}
			set
			{
				customerNumber = value;
			}
		}
		public string CustomerName
		{
			get
			{
				return customerName;
			}
			set
			{
				customerName = value;
			}
		}
		public int CustomerGroupId
		{
			get
			{
				return customerGroupId;
			}
			set
			{
				customerGroupId = value;
			}
		}
		public bool OnHold
		{
			get
			{
				return onHold;
			}
			set
			{
				onHold = value;
			}
		}
		public bool Status
		{
			get
			{
				return status;
			}
			set
			{
				status = value;
			}
		}
		public string ShortName
		{
			get
			{
				return shortName;
			}
			set
			{
				shortName = value;
			}
		}
		public DateTime? StartDate
		{
			get
			{
				return startDate;
			}
			set
			{
				startDate = value;
			}
		}
		public string AddressLine1
		{
			get
			{
				return addressLine1;
			}
			set
			{
				addressLine1 = value;
			}
		}
		public string AddressLine2
		{
			get
			{
				return addressLine2;
			}
			set
			{
				addressLine2 = value;
			}
		}
		public string AddressLine3
		{
			get
			{
				return addressLine3;
			}
			set
			{
				addressLine3 = value;
			}
		}
		public string AddressLine4
		{
			get
			{
				return addressLine4;
			}
			set
			{
				addressLine4 = value;
			}
		}
		public string City
		{
			get
			{
				return city;
			}
			set
			{
				city = value;
			}
		}
		public string Country
		{
			get
			{
				return country;
			}
			set
			{
				country = value;
			}
		}
		public string StateProv
		{
			get
			{
				return stateProv;
			}
			set
			{
				stateProv = value;
			}
		}
		public string PostalCode
		{
			get
			{
				return postalCode;
			}
			set
			{
				postalCode = value;
			}
		}
		public string Telephone
		{
			get
			{
				return telephone;
			}
			set
			{
				telephone = value;
			}
		}
		public string Email
		{
			get
			{
				return email;
			}
			set
			{
				email = value;
			}
		}
		public string Website
		{
			get
			{
				return website;
			}
			set
			{
				website = value;
			}
		}
		public string Contact
		{
			get
			{
				return contact;
			}
			set
			{
				contact = value;
			}
		}
		public string ContactTelephone
		{
			get
			{
				return contactTelephone;
			}
			set
			{
				contactTelephone = value;
			}
		}
		public string ContactFax
		{
			get
			{
				return contactFax;
			}
			set
			{
				contactFax = value;
			}
		}
		public string ContactEmail
		{
			get
			{
				return contactEmail;
			}
			set
			{
				contactEmail = value;
			}
		}
		public int AccountTypeId
		{
			get
			{
				return accountTypeId;
			}
			set
			{
				accountTypeId = value;
			}
		}
		public bool PrintStatements
		{
			get
			{
				return printStatements;
			}
			set
			{
				printStatements = value;
			}
		}
		public int AccountSetId
		{
			get
			{
				return accountSetId;
			}
			set
			{
				accountSetId = value;
			}
		}
		public int PaymentTermCodeId
		{
			get
			{
				return paymentTermCodeId;
			}
			set
			{
				paymentTermCodeId = value;
			}
		}
		public int PaymentCodeId
		{
			get
			{
				return paymentCodeId;
			}
			set
			{
				paymentCodeId = value;
			}
		}
		public int DeliveryMethodId
		{
			get
			{
				return deliveryMethodId;
			}
			set
			{
				deliveryMethodId = value;
			}
		}
		public bool OutstandingBalanceExceedsLimit
		{
			get
			{
				return outstandingBalanceExceedsLimit;
			}
			set
			{
				outstandingBalanceExceedsLimit = value;
			}
		}
        public double CreditLimitAmount
		{
			get
			{
				return creditLimitAmount;
			}
			set
			{
				creditLimitAmount = value;
			}
		}
		public bool TransactionsOverdueNdaysExceedOverdueLimit
		{
			get
			{
				return transactionsOverdueNdaysExceedOverdueLimit;
			}
			set
			{
				transactionsOverdueNdaysExceedOverdueLimit = value;
			}
		}
		public int NDays
		{
			get
			{
				return nDays;
			}
			set
			{
				nDays = value;
			}
		}

        public double OverDueLimit
        {
            get {
                return this.overdueLimit;
            }
            set {
                this.overdueLimit = value;
            }

        }

		public int CustomerTypeId
		{
			get
			{
				return customerTypeId;
			}
			set
			{
				customerTypeId = value;
			}
		}
		public int CheckDuplicatPosId
		{
			get
			{
				return checkDuplicatPosId;
			}
			set
			{
				checkDuplicatPosId = value;
			}
		}
		public int TaxGroupId
		{
			get
			{
				return taxGroupId;
			}
			set
			{
				taxGroupId = value;
			}
		}
		public int TaxAuthorityId
		{
			get
			{
				return taxAuthorityId;
			}
			set
			{
				taxAuthorityId = value;
			}
		}
		public int TaxClassId
		{
			get
			{
				return taxClassId;
			}
			set
			{
				taxClassId = value;
			}
		}
		public int SalesPersonId
		{
			get
			{
				return salesPersonId;
			}
			set
			{
				salesPersonId = value;
			}
		}
        public double Percentage
		{
			get
			{
				return percentage;
			}
			set
			{
				percentage = value;
			}
		}
		public DateTime? CreatedDate
		{
			get
			{
				return createdDate;
			}
			set
			{
				createdDate = value;
			}
		}
		public int CreatedBy
		{
			get
			{
				return createdBy;
			}
			set
			{
				createdBy = value;
			}
		}
		public DateTime? UpdatedDate
		{
			get
			{
				return updatedDate;
			}
			set
			{
				updatedDate = value;
			}
		}
		public int UpdatedBy
		{
			get
			{
				return updatedBy;
			}
			set
			{
				updatedBy = value;
			}
		}
		public bool FlagDeleted
		{
			get
			{
				return flagDeleted;
			}
			set
			{
				flagDeleted = value;
			}
		}
		public bool IsSytem
		{
			get
			{
				return isSytem;
			}
			set
			{
				isSytem = value;
			}
		}

        public string CrCompanyName
        {
            get
            {
                return crCompanyName;
            }
            set
            {
                crCompanyName = value;
            }
        }

        public int CrCompanyId
        {
            get
            {
                return crCompanyId;
            }
            set
            {
                crCompanyId = value;
            }
        }

        #endregion

        #region Composite Objects
        //private CustomerGroup customerGroupObj;
        //[CompositeObject(CompositObjectTypeEnum.Single, "CustomerGroupId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable= false)]
        //public CustomerGroup CustomerGroupObj
        //{
        //    get
        //    {
        //        if (customerGroupObj == null)
        //            loadCompositObject_Lazy("CustomerGroupObj");
        //        return customerGroupObj;
        //    }
        //    set
        //    {
        //        customerGroupObj = value;;
        //    }
        //}
		
		private DataTypeContent accountTypeObj;
		[CompositeObject(CompositObjectTypeEnum.Single, "AccountTypeId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable= false)]
		public DataTypeContent AccountTypeObj
		{
			get
			{
				if (accountTypeObj == null)
					loadCompositObject_Lazy("AccountTypeObj");
				return accountTypeObj;
			}
			set
			{
				accountTypeObj = value;;
			}
		}
		
        //private AccountSet accountSetObj;
        //[CompositeObject(CompositObjectTypeEnum.Single, "AccountSetId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable= false)]
        //public AccountSet AccountSetObj
        //{
        //    get
        //    {
        //        if (accountSetObj == null)
        //            loadCompositObject_Lazy("AccountSetObj");
        //        return accountSetObj;
        //    }
        //    set
        //    {
        //        accountSetObj = value;;
        //    }
        //}
		
        //private PaymentTerm paymentTermObj;
        //[CompositeObject(CompositObjectTypeEnum.Single, "PaymentTermCodeId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable= false)]
        //public PaymentTerm PaymentTermObj
        //{
        //    get
        //    {
        //        if (paymentTermObj == null)
        //            loadCompositObject_Lazy("PaymentTermObj");
        //        return paymentTermObj;
        //    }
        //    set
        //    {
        //        paymentTermObj = value;;
        //    }
        //}
		
        //private PaymentCode paymentCodeObj;
        //[CompositeObject(CompositObjectTypeEnum.Single, "PaymentCodeId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable= false)]
        //public PaymentCode PaymentCodeObj
        //{
        //    get
        //    {
        //        if (paymentCodeObj == null)
        //            loadCompositObject_Lazy("PaymentCodeObj");
        //        return paymentCodeObj;
        //    }
        //    set
        //    {
        //        paymentCodeObj = value;;
        //    }
        //}
		
		private DataTypeContent deliveryMethodObj;
		[CompositeObject(CompositObjectTypeEnum.Single, "DeliveryMethodId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable= false)]
		public DataTypeContent DeliveryMethodObj
		{
			get
			{
				if (deliveryMethodObj == null)
					loadCompositObject_Lazy("DeliveryMethodObj");
				return deliveryMethodObj;
			}
			set
			{
				deliveryMethodObj = value;;
			}
		}
		
		private DataTypeContent customerTypeObj;
		[CompositeObject(CompositObjectTypeEnum.Single, "CustomerTypeId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable= false)]
		public DataTypeContent CustomerTypeObj
		{
			get
			{
				if (customerTypeObj == null)
					loadCompositObject_Lazy("CustomerTypeObj");
				return customerTypeObj;
			}
			set
			{
				customerTypeObj = value;;
			}
		}
		
		private DataTypeContent checkDuplicatPosObj;
		[CompositeObject(CompositObjectTypeEnum.Single, "CheckDuplicatPosId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable= false)]
		public DataTypeContent CheckDuplicatPosObj
		{
			get
			{
				if (checkDuplicatPosObj == null)
					loadCompositObject_Lazy("CheckDuplicatPosObj");
				return checkDuplicatPosObj;
			}
			set
			{
				checkDuplicatPosObj = value;;
			}
		}
		
        //private TaxGroupHeader taxGroupHeaderObj;
        //[CompositeObject(CompositObjectTypeEnum.Single, "TaxGroupId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable= false)]
        //public TaxGroupHeader TaxGroupHeaderObj
        //{
        //    get
        //    {
        //        if (taxGroupHeaderObj == null)
        //            loadCompositObject_Lazy("TaxGroupHeaderObj");
        //        return taxGroupHeaderObj;
        //    }
        //    set
        //    {
        //        taxGroupHeaderObj = value;;
        //    }
        //}
		
        //private TaxAuthority taxAuthorityObj;
        //[CompositeObject(CompositObjectTypeEnum.Single, "TaxAuthorityId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable= false)]
        //public TaxAuthority TaxAuthorityObj
        //{
        //    get
        //    {
        //        if (taxAuthorityObj == null)
        //            loadCompositObject_Lazy("TaxAuthorityObj");
        //        return taxAuthorityObj;
        //    }
        //    set
        //    {
        //        taxAuthorityObj = value;;
        //    }
        //}
		
        //private TaxClassHeader taxClassHeaderObj;
        //[CompositeObject(CompositObjectTypeEnum.Single, "TaxClassId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable= false)]
        //public TaxClassHeader TaxClassHeaderObj
        //{
        //    get
        //    {
        //        if (taxClassHeaderObj == null)
        //            loadCompositObject_Lazy("TaxClassHeaderObj");
        //        return taxClassHeaderObj;
        //    }
        //    set
        //    {
        //        taxClassHeaderObj = value;;
        //    }
        //}
		
        //private SalesPerson salesPersonObj;
        //[CompositeObject(CompositObjectTypeEnum.Single, "SalesPersonId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable= false)]
        //public SalesPerson SalesPersonObj
        //{
        //    get
        //    {
        //        if (salesPersonObj == null)
        //            loadCompositObject_Lazy("SalesPersonObj");
        //        return salesPersonObj;
        //    }
        //    set
        //    {
        //        salesPersonObj = value;;
        //    }
        //}

        //private List<NoteCustomer> noteCustomerList;
        //[CompositeObject(CompositObjectTypeEnum.List, "CustomerId", LazyLoad = true, CascadeDelete = true)]
        //public List<NoteCustomer> NoteCustomerList
        //{
        //    get
        //    {
        //        if (noteCustomerList == null)
        //            loadCompositObject_Lazy("NoteCustomerList");
        //        return noteCustomerList;
        //    }
        //    set { noteCustomerList = value; }
        //}


        #endregion

        public override int GetIdentity()
        {
            return customerId;
        }
        public override void SetIdentity(int value)
        {
            customerId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            //if (ParentType == typeof(CustomerGroup))
            //    this.customerGroupId = (int)value;
            //else if (ParentType == typeof(AccountSet))
            //    this.accountSetId = (int)value;
            //else if (ParentType == typeof(PaymentTerm))
            //    this.paymentTermCodeId = (int)value;
            //else if (ParentType == typeof(PaymentCode))
            //    this.paymentCodeId = (int)value;
            //else if (ParentType == typeof(TaxGroupHeader))
            //    this.taxGroupId = (int)value;
            //else if (ParentType == typeof(TaxAuthority))
            //    this.taxAuthorityId = (int)value;
            //else if (ParentType == typeof(TaxClassHeader))
            //    this.taxClassId = (int)value;
            //else if (ParentType == typeof(SalesPerson))
            //    this.salesPersonId = (int)value;

        }
    }
}