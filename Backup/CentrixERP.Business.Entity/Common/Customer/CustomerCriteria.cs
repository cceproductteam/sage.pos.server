using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Common.Business.Factory;

namespace CentrixERP.AccountsReceivable.Business.Entity
{
    public class CustomerCriteria : CriteriaBase, IAdvancedSearchCriteria
    {
        public string KeyWord;
        public int pageNumber { get; set; }
        public int resultCount { get; set; }
        public int? CustomerId { get; set; }
        public bool? Status { get; set; }
        public bool? OnHold { get; set; }
        public int CustomerGroupId { get; set; }

        public string SearchCriteria { get; set; }
        public bool SortType { get; set; }
        public string SortFields { get; set; }
        public int EntityId { get; set; }

        public int ExcludedInvoice { get; set; }

        public int? CurrencyId { get; set; }
    }
}
