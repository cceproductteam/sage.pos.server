using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace CentrixERP.Common.Business.Entity
{
    [EntityIManager("CentrixERP.Common.Business.IManager.IEmailEntityManager,CentrixERP.Business.IManager", isLogicalDelete = true)]
    public class EmailEntity : EntityBase<EmailEntity, int>
    {
        #region Constructors

        public EmailEntity() { }

        public EmailEntity(int emailEntityId)
        {
            this.emailEntityId = emailEntityId;
        }

        #endregion

        #region Private Properties

		private int emailEntityId;

		private int emailId = -1;

		private int entityId = -1;

		private int entityValueId;

		private bool? isDefault;

        private int emailType;

		private int createdBy;

		private DateTime? createdDate;

		private int updatedBy;

		private DateTime? updatedDate;

		private bool? flagDeleted;

		private bool? isSystem;


        #endregion

        #region Public Properties

        [PrimaryKey("@email_entity_id", SqlDbType.Int)]
		[Property("email_entity_id",SqlDbType.Int,AddParameter=false)]
	
		public int EmailEntityId
		{
			get
			{
				return emailEntityId;
			}
			set
			{
				emailEntityId = value;
			}
		}

		
		[Property("email_id",SqlDbType.Int)]
		[PropertyConstraint(true,"EmailId")]
		public int EmailId
		{
			get
			{
				return emailId;
			}
			set
			{
				emailId = value;
			}
		}

		
		[Property("entity_id",SqlDbType.Int)]
		[PropertyConstraint(true,"EntityId")]
		public int EntityId
		{
			get
			{
				return entityId;
			}
			set
			{
				entityId = value;
			}
		}

		
		[Property("entity_value_id",SqlDbType.Int)]
		[PropertyConstraint(true,"EntityValueId")]
		public int EntityValueId
		{
			get
			{
				return entityValueId;
			}
			set
			{
				entityValueId = value;
			}
		}

		
		[Property("is_default",SqlDbType.Bit)]
		[PropertyConstraint(false,"IsDefault")]
		public bool? IsDefault
		{
			get
			{
				return isDefault;
			}
			set
			{
				isDefault = value;
			}
		}

		
		[Property("email_type",SqlDbType.Int)]
        [PropertyConstraint(false, "email_type")]
		public int EmailType
		{
			get
			{
				return emailType;
			}
			set
			{
                emailType = value;
			}
		}



        [Property("flag_deleted", SqlDbType.Bit, AddParameter = false, UpdateParameter = false,FindParameter=false)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [Property("is_system", SqlDbType.Bit, AddParameter = false, UpdateParameter = false,FindParameter=false)]
        public bool? IsSystem
        {
            get
            {
                return isSystem;
            }
            set
            {
                isSystem = value;
            }
        }

		[Property("created_date",SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
		public DateTime? CreatedDate
		{
			get
			{
				return createdDate;
			}
			set
			{
				createdDate = value;
			}
		}

        [Property("updated_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
		public DateTime? UpdatedDate
		{
			get
			{
				return updatedDate;
			}
			set
			{
				updatedDate = value;
			}
		}

		[Property("created_by",SqlDbType.Int, AddParameter = true, UpdateParameter = false)]
		public int CreatedBy
		{
			get
			{
				return createdBy;
			}
			set
			{
				createdBy = value;
			}
		}

		[Property("updated_by",SqlDbType.Int, AddParameter = false, UpdateParameter = true)]
		public int UpdatedBy
		{
			get
			{
				return updatedBy;
			}
			set
			{
				updatedBy = value;
			}
		}
        #endregion

        public string UseCustomConnectionString
        {
            get;
            set;
        }

        #region Composite Objects
              		private Email emailObj;
        [CompositeObject(CompositObjectTypeEnum.Single, "EmailId", Savable = true, CascadeDelete = true, SaveBeforeParent = true)]
		public Email EmailObj
		{
			get
			{
				if (emailObj == null)
					loadCompositObject_Lazy("EmailObj");
				return emailObj;
			}
			set
			{
				emailObj = value;;
			}
		}
		
	


                 #endregion
   

        public override int GetIdentity()
        {
            return emailEntityId;
        }
        public override void SetIdentity(int value)
        {
            emailEntityId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            			if (ParentType == typeof(Email))
				this.emailId = (int)value;
			else if (ParentType == typeof(SystemEntity))
				this.entityId = (int)value;

        }
    }
}