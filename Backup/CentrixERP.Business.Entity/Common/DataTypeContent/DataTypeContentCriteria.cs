using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;

namespace CentrixERP.Common.Business.Entity
{
    public class DataTypeContentCriteria : CriteriaBase
    {
        public int? DataTypeContentId;
        public bool UserSystemData = false;
        public string Keyword;
        public bool? UseSystemDataType = null;
        public DateTime? lastSyncDate;


        public DateTime? LastSyncDate { get; set; }
    }
}