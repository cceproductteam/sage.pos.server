﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace CentrixERP.Common.Business.Entity
{
    public class DocumentEntityLite : EntityBaseLite
    {

        #region Public Properties

        [LiteProperty("document_entity_id", SqlDbType.Int)]
        public int DocumentEntityId
        {
            get;
            set;
        }

        [LiteProperty("document_id", SqlDbType.Int)]
        public int DocumentId
        {
            get;
            set;
        }

        [LiteProperty("entity_id", SqlDbType.Int)]
        public int EntityId
        {
            get;
            set;
        }

        [LiteProperty("entity_value_id", SqlDbType.Int)]
        public int EntityValueId
        {
            get;
            set;
        }

        [LiteProperty("document_title", SqlDbType.NVarChar)]
        public string DocumentTitle
        {
            get;
            set;
        }

        [LiteProperty("document_description", SqlDbType.NVarChar)]
        public string DocumentDescription
        {
            get;
            set;
        }

        [LiteProperty("document_fileName", SqlDbType.NVarChar)]
        public string DocumentFileName
        {
            get;
            set;
        }

        [LiteProperty("document_fileExtension", SqlDbType.NVarChar)]
        public string DocumentFileExtension
        {
            get;
            set;
        }

        [LiteProperty("document_orginalFileName", SqlDbType.NVarChar)]
        public string DocumentOrginalFileName
        {
            get;
            set;
        }
        [LiteProperty("document_path", SqlDbType.NVarChar)]
        public string DocumentPath
        {
            get;
            set;
        }
        [LiteProperty("ParentFolder", SqlDbType.NVarChar)]
        public string ParentFolder
        {
            get;
            set;
        }
        [LiteProperty("old_parent_folder", SqlDbType.NVarChar)]
        public string OldParentFolder
        {
            get;
            set;
        }
        [LiteProperty("folder_id", SqlDbType.Int)]
        public int FolderId
        {
            get;
            set;
        }


        [LiteProperty("flag_deleted", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? FlagDeleted
        {
            get;
            set;
        }

        [LiteProperty("is_system", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? IsSystem
        {
            get;
            set;
        }

        [LiteProperty("created_date", SqlDbType.DateTime)]
        public DateTime? CreatedDate
        {
            get;
            set;
        }

        [LiteProperty("updated_date", SqlDbType.DateTime)]
        public DateTime? UpdatedDate
        {
            get;
            set;
        }

        [LiteProperty("created_by", SqlDbType.Int, FindByIdLiteParameter = false)]
        public int CreatedBy
        {
            get;
            set;
        }

        [LiteProperty("updated_by", SqlDbType.Int, FindByIdLiteParameter = false)]
        public int UpdatedBy
        {
            get;
            set;
        }

        [LiteProperty("created_by_name", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string CreatedByName { get; set; }

        [LiteProperty("updated_by_name", SqlDbType.NVarChar, FindAllLiteParameter = false)]
        public string UpdatedByName { get; set; }

        [LiteProperty("document_entity_id", SqlDbType.NVarChar)]
        public int Id
        {
            get { return this.DocumentEntityId; }
            set { this.DocumentEntityId = value; }
        }

        [LiteProperty("last_rows", SqlDbType.NVarChar)]
        public bool LastRows { get; set; }

        [LiteProperty("total_count", SqlDbType.NVarChar)]
        public int TotalRecords { get; set; }
        #endregion


    }
}