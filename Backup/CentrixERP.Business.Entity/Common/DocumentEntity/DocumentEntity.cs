﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace CentrixERP.Common.Business.Entity
{
    [EntityIManager("CentrixERP.Common.Business.IManager.IDocumentEntityManager,CentrixERP.Business.IManager", isLogicalDelete = true)]
    public class DocumentEntity : EntityBase<DocumentEntity, int>
    {
        #region Constructors

        public DocumentEntity() { }

        public DocumentEntity(int documentEntityId)
        {
            this.documentEntityId = documentEntityId;
        }

        #endregion

        #region Private Properties

        private int documentEntityId;

        private int documentId = -1;

        private int entityId = -1;

        private int entityValueId;

        private int createdBy;

        private DateTime? createdDate;

        private int updatedBy;

        private DateTime? updatedDate;

        private bool? flagDeleted;

        private bool? isSystem;

        private string documentPath;
        private string documentUrl;
        private string moduleName;


        #endregion

        #region Public Properties

        [PrimaryKey("@document_entity_id", SqlDbType.Int)]
        [Property("document_entity_id", SqlDbType.Int, OutParameter = true, AddParameter = false)]
        public int DocumentEntityId
        {
            get
            {
                return documentEntityId;
            }
            set
            {
                documentEntityId = value;
            }
        }


        [Property("document_id", SqlDbType.Int,UpdateParameter=false)]
        [PropertyConstraint(true, "DocumentId")]
        public int DocumentId
        {
            get
            {
                return documentId;
            }
            set
            {
                documentId = value;
            }
        }


        [Property("entity_id", SqlDbType.Int, UpdateParameter = false)]
        [PropertyConstraint(true, "EntityId")]
        public int EntityId
        {
            get
            {
                return entityId;
            }
            set
            {
                entityId = value;
            }
        }


        [Property("entity_value_id", SqlDbType.Int, UpdateParameter = false)]
        [PropertyConstraint(true, "EntityValueId")]
        public int EntityValueId
        {
            get
            {
                return entityValueId;
            }
            set
            {
                entityValueId = value;
            }
        }

        [Property("document_path", SqlDbType.NVarChar, AddParameter = false, UpdateParameter = false)]
        public string DocumentPath 
        {
            get
            {
                return documentPath;
            }
            set
            {
                documentPath = value;
            }
        }

        [Property("document_url", SqlDbType.NVarChar, AddParameter = false, UpdateParameter = false)]
        public string DocumentUrl
        {
            get
            {
                return documentUrl;
            }
            set
            {
                documentUrl = value;
            }
        }

        [Property("module_name", SqlDbType.NVarChar, AddParameter = false, UpdateParameter = false)]
        public string ModuleName
        {
            get
            {
                return moduleName;
            }
            set
            {
                moduleName = value;
            }
        }



        [Property("flag_deleted", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [Property("is_system", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool? IsSystem
        {
            get
            {
                return isSystem;
            }
            set
            {
                isSystem = value;
            }
        }

        [Property("created_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? CreatedDate
        {
            get
            {
                return createdDate;
            }
            set
            {
                createdDate = value;
            }
        }

        [Property("updated_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? UpdatedDate
        {
            get
            {
                return updatedDate;
            }
            set
            {
                updatedDate = value;
            }
        }

        [Property("created_by", SqlDbType.Int, AddParameter = true, UpdateParameter = false)]
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        [Property("updated_by", SqlDbType.Int, AddParameter = false, UpdateParameter = false)]
        public int UpdatedBy
        {
            get
            {
                return updatedBy;
            }
            set
            {
                updatedBy = value;
            }
        }
        #endregion

        #region Composite Objects
        private Document documentObj;
        [CompositeObject(CompositObjectTypeEnum.Single, "DocumentId", Savable = true, CascadeDelete = true, SaveBeforeParent = true)]
        public Document DocumentObj
        {
            get
            {
                if (documentObj == null)
                    loadCompositObject_Lazy("DocumentObj");
                return documentObj;
            }
            set
            {
                documentObj = value; ;
            }
        }





        #endregion


        public override int GetIdentity()
        {
            return documentEntityId;
        }
        public override void SetIdentity(int value)
        {
            documentEntityId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            if (ParentType == typeof(Document))
                this.documentId = (int)value;
            else if (ParentType == typeof(SystemEntity))
                this.entityId = (int)value;

        }
    }
}