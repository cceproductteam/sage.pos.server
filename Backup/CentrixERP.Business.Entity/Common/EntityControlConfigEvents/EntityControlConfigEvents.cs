using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;

namespace CentrixERP.Common.Business.Entity
{
    [EntityIManager("CentrixERP.Common.Business.IManager.IEntityControlConfigEventsManager,CentrixERP.Business.IManager", isLogicalDelete = true)]
    public class EntityControlConfigEvents : EntityBase<EntityControlConfigEvents, int>
    {
        #region Constructors

        public EntityControlConfigEvents() { }

        public EntityControlConfigEvents(int entityControlConfigEventId)
        {
            this.entityControlConfigEventId = entityControlConfigEventId;
        }

        #endregion

        #region Private Properties

		private int entityControlConfigEventId;

		private int entityId;

		private int entityControlConfigId = -1;

		private int controlEventId = -1;

		private string jsFunctionName;

		private string jsFunctionBody;

		private int createdBy;

		private int updatedBy;

		private DateTime? createdDate;

		private DateTime? updatedDate;

		private bool? isSystem;

		private bool? flagDeleted;


        #endregion

        #region Public Properties

		public int EntityControlConfigEventId
		{
			get
			{
				return entityControlConfigEventId;
			}
			set
			{
				entityControlConfigEventId = value;
			}
		}
		public int EntityId
		{
			get
			{
				return entityId;
			}
			set
			{
				entityId = value;
			}
		}
		public int EntityControlConfigId
		{
			get
			{
				return entityControlConfigId;
			}
			set
			{
				entityControlConfigId = value;
			}
		}
		public int ControlEventId
		{
			get
			{
				return controlEventId;
			}
			set
			{
				controlEventId = value;
			}
		}
		public string JsFunctionName
		{
			get
			{
				return jsFunctionName;
			}
			set
			{
				jsFunctionName = value;
			}
		}
		public string JsFunctionBody
		{
			get
			{
				return jsFunctionBody;
			}
			set
			{
				jsFunctionBody = value;
			}
		}
		public int CreatedBy
		{
			get
			{
				return createdBy;
			}
			set
			{
				createdBy = value;
			}
		}
		public int UpdatedBy
		{
			get
			{
				return updatedBy;
			}
			set
			{
				updatedBy = value;
			}
		}
		public DateTime? CreatedDate
		{
			get
			{
				return createdDate;
			}
			set
			{
				createdDate = value;
			}
		}
		public DateTime? UpdatedDate
		{
			get
			{
				return updatedDate;
			}
			set
			{
				updatedDate = value;
			}
		}
		public bool? IsSystem
		{
			get
			{
				return isSystem;
			}
			set
			{
				isSystem = value;
			}
		}
		public bool? FlagDeleted
		{
			get
			{
				return flagDeleted;
			}
			set
			{
				flagDeleted = value;
			}
		}

        #endregion

        #region Composite Objects
		private EntityControlConfig entityControlConfigObj;
		[CompositeObject(CompositObjectTypeEnum.Single, "EntityControlConfigId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable= false)]
		public EntityControlConfig EntityControlConfigObj
		{
			get
			{
                if (entityControlConfigObj == null)
                    loadCompositObject_Lazy("EntityControlConfigObj");
                return entityControlConfigObj;
			}
			set
			{
                entityControlConfigObj = value; ;
			}
		}
		
        //private ControlsEvents controlsEventsObj;
        //[CompositeObject(CompositObjectTypeEnum.Single, "ControlEventId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable= false)]
        //public ControlsEvents ControlsEventsObj
        //{
        //    get
        //    {
        //        if (controlsEventsObj == null)
        //            loadCompositObject_Lazy("ControlsEventsObj");
        //        return controlsEventsObj;
        //    }
        //    set
        //    {
        //        controlsEventsObj = value; ;
        //    }
        //}
		


        #endregion

        public override int GetIdentity()
        {
            return entityControlConfigEventId;
        }
        public override void SetIdentity(int value)
        {
            entityControlConfigEventId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
			if (ParentType == typeof(EntityControlConfig))
				this.entityControlConfigId = (int)value;
			//else if (ParentType == typeof(ControlsEvents))
			//	this.controlEventId = (int)value;

        }
    }
}