using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;

namespace CentrixERP.Common.Business.Entity
{
    public class EntityControlConfigEventsCriteria : CriteriaBase
    {
        public int EntityId = -1;
        public int ConfigId = -1;
    }
}
