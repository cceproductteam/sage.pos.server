using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;

namespace CentrixERP.Common.Business.Entity
{
    [EntityIManager("CentrixERP.Common.Business.IManager.IEntityLockManager,CentrixERP.Business.IManager", isLogicalDelete = true)]
    public class EntityLock : EntityBase<EntityLock, int>
    {
        #region Constructors

        public EntityLock() { }

        public EntityLock(int entityLockId)
        {
            this.entityLockId = entityLockId;
        }

        #endregion

        #region Private Properties

		private int entityLockId;

		private int entityId = -1;

		private int entityValueId;

		private bool? lockAllEntity;

		private string userName;

		private DateTime? createdDate;

		private int createdBy;

		private DateTime? updatedDate;

		private int updatedBy;

		private bool? flagDeleted;

		private bool? isSystem;


        #endregion

        #region Public Properties

		public int EntityLockId
		{
			get
			{
				return entityLockId;
			}
			set
			{
				entityLockId = value;
			}
		}
		public int EntityId
		{
			get
			{
				return entityId;
			}
			set
			{
				entityId = value;
			}
		}
		public int EntityValueId
		{
			get
			{
				return entityValueId;
			}
			set
			{
				entityValueId = value;
			}
		}
		public bool? LockAllEntity
		{
			get
			{
				return lockAllEntity;
			}
			set
			{
				lockAllEntity = value;
			}
		}
		public string UserName
		{
			get
			{
				return userName;
			}
			set
			{
				userName = value;
			}
		}
		public DateTime? CreatedDate
		{
			get
			{
				return createdDate;
			}
			set
			{
				createdDate = value;
			}
		}
		public int CreatedBy
		{
			get
			{
				return createdBy;
			}
			set
			{
				createdBy = value;
			}
		}
		public DateTime? UpdatedDate
		{
			get
			{
				return updatedDate;
			}
			set
			{
				updatedDate = value;
			}
		}
		public int UpdatedBy
		{
			get
			{
				return updatedBy;
			}
			set
			{
				updatedBy = value;
			}
		}
		public bool? FlagDeleted
		{
			get
			{
				return flagDeleted;
			}
			set
			{
				flagDeleted = value;
			}
		}
		public bool? IsSystem
		{
			get
			{
				return isSystem;
			}
			set
			{
				isSystem = value;
			}
		}

        public DateTime? EntityLastUpdatedDate { get; set; }

        public bool LockWithoutCheck { get; set; }

        public bool LockForDelete { get; set; }

        public int Status{ get; set; }

        #endregion

        #region Composite Objects
        private SystemEntity entityObj;
		[CompositeObject(CompositObjectTypeEnum.Single, "EntityId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable= false)]
        public SystemEntity EntityObj
		{
			get
			{
				if (entityObj == null)
					loadCompositObject_Lazy("EntityObj");
				return entityObj;
			}
			set
			{
				entityObj = value;;
			}
		}
		


        #endregion

        public override int GetIdentity()
        {
            return entityLockId;
        }
        public override void SetIdentity(int value)
        {
            entityLockId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
			if (ParentType == typeof(SystemEntity))
				this.entityId = (int)value;

        }
    }
}