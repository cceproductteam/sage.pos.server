using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;

namespace CentrixERP.Common.Business.Entity
{
    [EntityIManager("CentrixERP.Common.Business.IManager.IRecentViewManager,CentrixERP.Business.IManager", isLogicalDelete = false)]
    public class RecentView : EntityBase<RecentView, int>
    {
        private int id;
        private int entityId;
        private int userId;
        private int recentlyViewedId;
        private int order;
        private DateTime createdDate;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public int EntityId
        {
            get { return entityId; }
            set { entityId = value; }
        }

        public int UserId
        {
            get { return userId; }
            set { userId = value; }
        }

        public int RecentlyViewedId
        {
            get { return recentlyViewedId; }
            set { recentlyViewedId = value; }
        }
        public int Order
        {
            get { return order; }
            set { order = value; }
        }
        public DateTime CreatedDate
        {
            get { return createdDate; }
            set { createdDate = value; }
        }
        public override int GetIdentity()
        {
            return recentlyViewedId;
        }

        public override void SetIdentity(int value)
        {
            recentlyViewedId = value;
        }

        public override void SetParentId(object value, Type ParentType)
        {
            throw new NotImplementedException();
        }
        
    }
}
