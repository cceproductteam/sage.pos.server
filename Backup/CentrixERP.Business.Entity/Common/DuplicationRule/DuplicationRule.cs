using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;


namespace CentrixERP.Common.Business.Entity
{
    [EntityIManager("CentrixERP.Common.Business.IManager.IDuplicationRuleManager,CentrixERP.Business.IManager", isLogicalDelete = true)]
    public class DuplicationRule: EntityBase< DuplicationRule, int>
    {
        #region "Constructors"
        public DuplicationRule()
        {
            }
        
        public DuplicationRule(int id)
        {
            this.id=id;
            }
        #endregion
        
        #region "Private Property"
		private int id=-1;
		private int moduleid=-1;
		private string uinameen;
		private string uinamear;
		private int filtertypeid=-1;
		private string propertynameen;
		private string propertynamear;
		private int propertyparentid=-1;
		private bool flagdeleted;
		private int createdby=-1;
		private int updatedby=-1;
		private DateTime? createddate;
		private DateTime? updateddate;
		private bool issystem;
        private DuplicationRuleFilterType filterTypeDataTypeContent;
        private string dbColumnNameEn;
        private string dbColumnNameAr;
        private int controlType = -1;
        private string serviceMethod;
        private string serviceName;
        private string mapToControlId;
        #endregion
        
        #region "Public Property"
		public int Id
		{
			get {return id;}
			set {id=value;}
		}

		public int ModuleId
		{
			get {return moduleid;}
			set {moduleid=value;}
		}

		public string UiNameEn
		{
			get {return uinameen;}
			set {uinameen=value;}
		}

		public string UiNameAr
		{
			get {return uinamear;}
			set {uinamear=value;}
		}

		public int FilterTypeId
		{
			get {return filtertypeid;}
			set {filtertypeid=value;}
		}

		public string PropertyNameEn
		{
			get {return propertynameen;}
			set {propertynameen=value;}
		}

		public string PropertyNameAr
		{
			get {return propertynamear;}
			set {propertynamear=value;}
		}

		public int PropertyParentId
		{
			get {return propertyparentid;}
			set {propertyparentid=value;}
		}

		public bool FlagDeleted
		{
			get {return flagdeleted;}
			set {flagdeleted=value;}
		}

		public int CreatedBy
		{
			get {return createdby;}
			set {createdby=value;}
		}

		public int UpdatedBy
		{
			get {return updatedby;}
			set {updatedby=value;}
		}

		public DateTime? CreatedDate
		{
			get {return createddate;}
			set {createddate=value;}
		}

		public DateTime? UpdatedDate
		{
			get {return updateddate;}
			set {updateddate=value;}
		}

		public bool IsSystem
		{
			get {return issystem;}
			set {issystem=value;}
		}

        public string DBColumnNameEn
         {
            get { return dbColumnNameEn; }
             set { dbColumnNameEn = value; }
        }
        public string DBColumnNameAr
        {
            get { return dbColumnNameAr; }
            set { dbColumnNameAr = value; }
        }
        public int ControlType
        {
            get { return controlType; }
            set { controlType = value; }
        }
        public string ServiceName
        {
            get { return serviceName; }
            set { serviceName = value; }
        }
        public string ServiceMethod
        {
            get { return serviceMethod; }
            set { serviceMethod = value; }
        }

        public string MapToControlId
        {
            get { return mapToControlId; }
            set { mapToControlId = value; }
        }
        #endregion
        
        #region "Composite Objects"
        [CompositeObject(CompositObjectTypeEnum.Single, "FilterTypeId", LazyLoad = true, CascadeDelete = true, Savable = false)]
        public DuplicationRuleFilterType FilterTypeDataTypeContent
        {
            get
            {
                if (filterTypeDataTypeContent == null)
                    loadCompositObject_Lazy("FilterTypeDataTypeContent");
                return filterTypeDataTypeContent;
            }
            set
            {
                filterTypeDataTypeContent = value;
                filterTypeDataTypeContent.MarkModified();
            }
        }
        #endregion
        
        #region "Entity Base Methods"
         public override int GetIdentity()
        {
            return id;
        }

        public override void SetIdentity(int value)
        {
             id = value;
        }

        public override void SetParentId(object value, Type ParentType)
        {
            throw new NotImplementedException();
        }
        
        #endregion 
    }

  
}


