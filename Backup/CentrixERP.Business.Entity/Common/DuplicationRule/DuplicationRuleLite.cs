using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CentrixERP.Common.Business.Entity
{
    public class DuplicationRuleLite
    {

        private int duplicationId;
        private int controlType;
        private string controlId;
        private string mapToContolId;
        private int dDLParentId;

        public int DuplicationID
        {
            get { return duplicationId; }
            set { duplicationId = value; }
        }
        public int ControlType
        {
            get { return controlType; }
            set { controlType = value; }
        }
        public int DDLParentID
        {
            get { return dDLParentId; }
            set { dDLParentId = value; }
        }
        public string ControlID
        {
            get { return controlId; }
            set { controlId = value; }
        }
        public string MapToContolID
        {
            get { return mapToContolId; }
            set { mapToContolId = value; }
        }

    }
}
