using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;

namespace CentrixERP.Common.Business.Entity
{
    [EntityIManager("CentrixERP.Common.Business.IManager.IDocumentManager,CentrixERP.Business.IManager", isLogicalDelete = true)]
    public class Document : EntityBase<Document, int>
    {
        private int documentID = -1;
        private string title;
        private string description;
        private int createdBy = -1;
        private int updatedBy = -1;
        private string fileName;
        private string fileExtension;
        private string orginalFileName; 
        private string documentPath;
        private string parentFolder;
        private int folderId=-1;
        private int parentEntityId = -1;        

        public byte[] documentBytes;

        public string OldParentFolder { get; set; }
        #region constructor
        public Document() { }

        public Document(int documentID)
        {
            DocumentID = documentID;
        }
        #endregion

        public int FolderId
        {
            get { return folderId; }
            set { folderId = value; }
        }

        public string DocumentPath
        {
            get { return documentPath; }
            set { documentPath = value; }
        }

        public int UpdatedBy
        {
            get { return updatedBy; }
            set { updatedBy = value; }
        }

        public string OrginalFileName
        {
            get { return orginalFileName; }
            set { orginalFileName = value; }
        }

        public string FileExtension
        {
            get { return fileExtension; }
            set { fileExtension = value; }
        }

        public string FileName
        {
            get { return fileName; }
            set { fileName = value; }
        }

        public int CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public string ParentFolder
        {
            get { return parentFolder; }
            set { parentFolder = value; }
        }

        public string Title
        {
            get { return title; }
            set { title = value; }
        }

        public int DocumentID
        {
            get { return documentID; }
            set { documentID = value; }
        }

        public int ParentEntityId
        {
            get { return parentEntityId; }
            set { parentEntityId = value; }
        }

        private bool preventPhysicalSave = true;
        public bool PreventPhysicalSave { get; set; }

        private DocumentFolder containingFolder;
        [CompositeObject(CompositObjectTypeEnum.Single, "FolderId", LazyLoad = true, CascadeDelete = true, Savable = false)]
        public DocumentFolder ContainingFolder
        {
            get
            {
                if (containingFolder == null)
                    loadCompositObject_Lazy("ContainingFolder");
                return containingFolder;
            }
            set
            {
                containingFolder = value;
                containingFolder.MarkModified();
            }
 
        }

        public override int GetIdentity()
        {
            return DocumentID;
        }

        public override void SetIdentity(int value)
        {
            DocumentID = documentID;
        }

        public override void SetParentId(object value, Type ParentType)
        {
            throw new NotImplementedException();
        }



    }

    public struct DocumentIcons
    {
        public string Icon { get; set; }
        public string CssClass { get; set; }        
    }
}
