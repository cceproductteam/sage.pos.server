using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.Framework;
using SF.FrameworkEntity;

namespace CentrixERP.Common.Business.Entity
{
    [EntityIManager("CentrixERP.Common.Business.IManager.IDataTypeManager,CentrixERP.Business.IManager", isLogicalDelete = true)]
    public class DataType : EntityBase<DataType, int>
    {
        private int dataTypeID = -1;
        private string dataTypeName;
        private int createdBy;
        private int updatedBy;
        private DateTime createdDate;
        private DateTime updateDate;
        private List<DataTypeContent> dataTypeContentList;


        #region constructor
        public DataType() { }
        #endregion
        [CompositeObject(CompositObjectTypeEnum.List, "DataTypeID", LazyLoad = true, CascadeDelete = true)]
        public List<DataTypeContent> DataTypeContentList
        {
          
            get
            {
                if (dataTypeContentList == null)
                    loadCompositObject_Lazy("DataTypeContentList");
                return dataTypeContentList;
            }
            set { dataTypeContentList = value; }
        }

        public string DataTypeName
        {
            get { return dataTypeName; }
            set { dataTypeName = value; }
        }

        public int DataTypeID
        {
            get { return dataTypeID; }
            set { dataTypeID = value; }
        }

        public int CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }
        public int UpdatedBy
        {
            get { return updatedBy; }
            set { updatedBy = value; }
        }

        public DateTime CreatedDate
        {
            get { return createdDate; }
            set { createdDate = value; }
        }

        public DateTime UpdateDate
        {
            get { return updateDate; }
            set { updateDate = value; }
        }


        public override int GetIdentity()
        {
            return DataTypeID;
        }
       
        public override void SetIdentity(int value)
        {
            DataTypeID = value;
        }

        public override void SetParentId(object value, Type ParentType)
        {
            throw new NotImplementedException();
        }
    }
}
