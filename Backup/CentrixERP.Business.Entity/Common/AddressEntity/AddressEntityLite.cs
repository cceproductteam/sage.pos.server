using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace CentrixERP.Common.Business.Entity
{
    public class AddressEntityLite : EntityBaseLite
    {

        #region Public Properties

        [LiteProperty("address_entity_id", SqlDbType.Int)]
        public int AddressEntityId
        {
            get;
            set;
        }

        [LiteProperty("address_id", SqlDbType.Int)]
        public int AddressId
        {
            get;
            set;
        }

        [LiteProperty("entity_id", SqlDbType.Int)]
        public int EntityId
        {
            get;
            set;
        }

        [LiteProperty("entity_value_id", SqlDbType.Int)]
        public int EntityValueId
        {
            get;
            set;
        }


        [LiteProperty("address_building_number", SqlDbType.Int, FindAllLiteParameter = true)]
        public int? BuildingNumber { get; set; }

        [LiteProperty("address_street_address", SqlDbType.NVarChar, FindAllLiteParameter = true)]
        public string StreetAddress { get; set; }

        [LiteProperty("address_floor", SqlDbType.NVarChar, FindAllLiteParameter = true)]
        public string Floor { get; set; }

        [LiteProperty("address_office_number", SqlDbType.NVarChar, FindAllLiteParameter = true)]
        public string OfficeNumber { get; set; }

        [LiteProperty("address_zip_code", SqlDbType.NVarChar, FindAllLiteParameter = true)]
        public string ZipCode { get; set; }

        [LiteProperty("country_name_ar", SqlDbType.NVarChar, FindAllLiteParameter = true)]
        public string CountryNameAr { get; set; }

        [LiteProperty("country_name_en", SqlDbType.NVarChar, FindAllLiteParameter = true)]
        public string CountryNameEn { get; set; }

        [LiteProperty("country_id", SqlDbType.Int, FindAllLiteParameter = true)]
        public int CountryId { get; set; }

        [LiteProperty("address_county", SqlDbType.NVarChar, FindAllLiteParameter = true)]
        public string County { get; set; }

        [LiteProperty("address_city_text", SqlDbType.NVarChar, FindAllLiteParameter = true)]
        public string CityText { get; set; }

        [LiteProperty("address_type", SqlDbType.Int, FindAllLiteParameter = true)]
        public int Type { get; set; }



        [LiteProperty("address_region", SqlDbType.NVarChar, FindAllLiteParameter = true)]
        public string Region { get; set; }

        [LiteProperty("address_area", SqlDbType.NVarChar, FindAllLiteParameter = true)]
        public string Area { get; set; }

        [LiteProperty("address_pobox", SqlDbType.NVarChar, FindAllLiteParameter = true)]
        public string POBox { get; set; }

        [LiteProperty("address_place", SqlDbType.NVarChar, FindAllLiteParameter = true)]
        public string Place { get; set; }

        [LiteProperty("address_nearby", SqlDbType.NVarChar, FindAllLiteParameter = true)]
        public string NearBy { get; set; }



        [LiteProperty("is_default", SqlDbType.Bit)]
        public bool? IsDefault
        {
            get;
            set;
        }

        [LiteProperty("address_type", SqlDbType.Int)]
        public int AddressType
        {
            get;
            set;
        }


        [LiteProperty("flag_deleted", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? FlagDeleted
        {
            get;
            set;
        }

        [LiteProperty("is_system", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? IsSystem
        {
            get;
            set;
        }

        [LiteProperty("created_date", SqlDbType.DateTime)]
        public DateTime? CreatedDate
        {
            get;
            set;
        }

        [LiteProperty("updated_date", SqlDbType.DateTime)]
        public DateTime? UpdatedDate
        {
            get;
            set;
        }

        [LiteProperty("created_by", SqlDbType.Int, FindByIdLiteParameter = false)]
        public int CreatedBy
        {
            get;
            set;
        }

        [LiteProperty("updated_by", SqlDbType.Int, FindByIdLiteParameter = false)]
        public int UpdatedBy
        {
            get;
            set;
        }

        [LiteProperty("created_by_name", SqlDbType.NVarChar, FindAllLiteParameter = true)]
        public string CreatedByName { get; set; }

        [LiteProperty("updated_by_name", SqlDbType.NVarChar, FindAllLiteParameter = true)]
        public string UpdatedByName { get; set; }

        [LiteProperty("address_entity_id", SqlDbType.NVarChar)]
        public int Id
        {
            get { return this.AddressEntityId; }
            set { this.AddressEntityId = value; }
        }

        [LiteProperty("last_rows", SqlDbType.NVarChar)]
        public bool LastRows { get; set; }

        [LiteProperty("total_count", SqlDbType.NVarChar)]
        public int TotalRecords { get; set; }
        #endregion


    }
}