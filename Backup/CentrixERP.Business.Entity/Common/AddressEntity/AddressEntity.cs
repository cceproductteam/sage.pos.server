using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace CentrixERP.Common.Business.Entity
{
    [EntityIManager("CentrixERP.Common.Business.IManager.IAddressEntityManager,CentrixERP.Common.Business.IManager", isLogicalDelete = true)]
    public class AddressEntity : EntityBase<AddressEntity, int>
    {
        #region Constructors

        public AddressEntity() { }

        public AddressEntity(int addressEntityId)
        {
            this.addressEntityId = addressEntityId;
        }

        #endregion

        #region Private Properties

        private int addressEntityId;

        private int addressId = -1;

        private int entityId = -1;

        private int entityValueId;

        private bool? isDefault;

        private int addressType;

        private int createdBy;

        private DateTime? createdDate;

        private int updatedBy;

        private DateTime? updatedDate;

        private bool? flagDeleted;

        private bool? isSystem;


        #endregion

        #region Public Properties

        [PrimaryKey("@address_entity_id", SqlDbType.Int)]
        [Property("address_entity_id", SqlDbType.Int, AddParameter = false)]
        public int AddressEntityId
        {
            get
            {
                return addressEntityId;
            }
            set
            {
                addressEntityId = value;
            }
        }


        [Property("address_id", SqlDbType.Int)]
        [PropertyConstraint(true, "AddressId")]
        public int AddressId
        {
            get
            {
                return addressId;
            }
            set
            {
                addressId = value;
            }
        }


        [Property("entity_id", SqlDbType.Int)]
        [PropertyConstraint(true, "EntityId")]
        public int EntityId
        {
            get
            {
                return entityId;
            }
            set
            {
                entityId = value;
            }
        }


        [Property("entity_value_id", SqlDbType.Int)]
        [PropertyConstraint(true, "EntityValueId")]
        public int EntityValueId
        {
            get
            {
                return entityValueId;
            }
            set
            {
                entityValueId = value;
            }
        }


        [Property("is_default", SqlDbType.Bit)]
        [PropertyConstraint(false, "IsDefault")]
        public bool? IsDefault
        {
            get
            {
                return isDefault;
            }
            set
            {
                isDefault = value;
            }
        }


        [Property("address_type", SqlDbType.Int)]
        [PropertyConstraint(false, "AddressType")]
        public int AddressType
        {
            get
            {
                return addressType;
            }
            set
            {
                addressType = value;
            }
        }



        [Property("flag_deleted", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [Property("is_system", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool? IsSystem
        {
            get
            {
                return isSystem;
            }
            set
            {
                isSystem = value;
            }
        }

        [Property("created_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? CreatedDate
        {
            get
            {
                return createdDate;
            }
            set
            {
                createdDate = value;
            }
        }

        [Property("updated_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? UpdatedDate
        {
            get
            {
                return updatedDate;
            }
            set
            {
                updatedDate = value;
            }
        }

        [Property("created_by", SqlDbType.Int, AddParameter = true, UpdateParameter = false)]
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        [Property("updated_by", SqlDbType.Int, AddParameter = false, UpdateParameter = true)]
        public int UpdatedBy
        {
            get
            {
                return updatedBy;
            }
            set
            {
                updatedBy = value;
            }
        }
        #endregion

        #region Composite Objects
        private Address addressObj;
        [CompositeObject(CompositObjectTypeEnum.Single, "AddressId", Savable = true, CascadeDelete = true, SaveBeforeParent = true)]
        public Address AddressObj
        {
            get
            {
                if (addressObj == null)
                    loadCompositObject_Lazy("AddressObj");
                return addressObj;
            }
            set
            {
                addressObj = value; ;
            }
        }





        #endregion


        public override int GetIdentity()
        {
            return addressEntityId;
        }
        public override void SetIdentity(int value)
        {
            addressEntityId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            if (ParentType == typeof(Address))
                this.addressId = (int)value;
            else if (ParentType == typeof(SystemEntity))
                this.entityId = (int)value;

        }
    }
}