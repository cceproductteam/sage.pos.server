using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;


namespace CentrixERP.Common.Business.Entity
{
    [EntityIManager("CentrixERP.Common.Business.IManager.IModuleFieldManager,CentrixERP.Business.IManager", isLogicalDelete = true)]
    public class ModuleField: EntityBase< ModuleField, int>
    {
        #region "Constructors"
        public ModuleField()
        {
            }
        
        public ModuleField(int module_field_id)
        {
            this.modulefieldid=module_field_id;
            }
        #endregion
        
        #region "Private Property"
		private int modulefieldid=-1;
		private int moduleid=-1;
		private string uinameen;
		private string uinamear;
		private string dbcolumnnameen;
		private string dbcolumnnamear;
		private string servicename;
		private string servicemethod;
		private int controltype=-1;
		private int propertyparentid=-1;
		private string propertynameen;
		private string propertynamear;
        private int dataTypeId = -1;
		private bool flagdeleted;
		private int createdby=-1;
		private int updatedby=-1;
		private DateTime? createddate;
		private DateTime? updateddate;
		private bool issystem;
        #endregion
        
        #region "Public Property"
		public int ModuleFieldId
		{
			get {return modulefieldid;}
			set {modulefieldid=value;}
		}

		public int ModuleId
		{
			get {return moduleid;}
			set {moduleid=value;}
		}

		public string UiNameEn
		{
			get {return uinameen;}
			set {uinameen=value;}
		}

		public string UiNameAr
		{
			get {return uinamear;}
			set {uinamear=value;}
		}

		public string DbColumnNameEn
		{
			get {return dbcolumnnameen;}
			set {dbcolumnnameen=value;}
		}

		public string DbColumnNameAr
		{
			get {return dbcolumnnamear;}
			set {dbcolumnnamear=value;}
		}

		public string ServiceName
		{
			get {return servicename;}
			set {servicename=value;}
		}

		public string ServiceMethod
		{
			get {return servicemethod;}
			set {servicemethod=value;}
		}

		public int ControlType
		{
			get {return controltype;}
			set {controltype=value;}
		}

		public int PropertyParentId
		{
			get {return propertyparentid;}
			set {propertyparentid=value;}
		}

		public string PropertyNameEn
		{
			get {return propertynameen;}
			set {propertynameen=value;}
		}

		public string PropertyNameAr
		{
			get {return propertynamear;}
			set {propertynamear=value;}
		}

        public int DataTypeId
        {
            get { return dataTypeId; }
            set { this.dataTypeId = value; }
        }

		public bool FlagDeleted
		{
			get {return flagdeleted;}
			set {flagdeleted=value;}
		}

		public int CreatedBy
		{
			get {return createdby;}
			set {createdby=value;}
		}

		public int UpdatedBy
		{
			get {return updatedby;}
			set {updatedby=value;}
		}

		public DateTime? CreatedDate
		{
			get {return createddate;}
			set {createddate=value;}
		}

		public DateTime? UpdatedDate
		{
			get {return updateddate;}
			set {updateddate=value;}
		}

		public bool IsSystem
		{
			get {return issystem;}
			set {issystem=value;}
		}

        #endregion
        
        #region "Composite Objects"

		private Module moduleobj;
		[CompositeObject(CompositObjectTypeEnum.Single, "ModuleId", LazyLoad = true, SaveBeforeParent = true, CascadeDelete = true)]
		public Module ModuleObj
		{
			get{
				if (moduleobj == null)
					loadCompositObject_Lazy("ModuleObj");
				return moduleobj; }
			set { moduleobj = value; }
		}
        #endregion
        
        #region "Entity Base Methods"
         public override int GetIdentity()
        {
            return modulefieldid;
        }

        public override void SetIdentity(int value)
        {
             modulefieldid = value;
        }

        public override void SetParentId(object value, Type ParentType)
        {
            throw new NotImplementedException();
        }
        
        #endregion 
    }

    public class SearchFieldCriteria
    {
        public int SearchCriteriaId { get; set; }
        public string FilterTypeNameEn { get; set; }
        public string FilterTypeNameAr { get; set; }
    }
}


