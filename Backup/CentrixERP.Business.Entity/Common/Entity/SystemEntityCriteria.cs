using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;

namespace CentrixERP.Common.Business.Entity
{
    public class SystemEntityCriteria : CriteriaBase
    {
        public string keyword;
        public bool? mainEntity;
        public bool? HasOptionalFields;
    }
}
