using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;

namespace CentrixERP.Common.Business.Entity
{
    [EntityIManager("CentrixERP.Common.Business.IManager.ISystemEntityManager,CentrixERP.Business.IManager", isLogicalDelete = true)]
    public class SystemEntity : EntityBase<SystemEntity, int>
    {
        #region Constructors

        public SystemEntity() { }

        public SystemEntity(int entityId)
        {
            this.entityId = entityId;
        }

        #endregion

        #region Private Properties

		private int entityId;

		private string tableName;

		private string entityName;

		private string entityNameAr;

		private string moduleName;

		private string nameSpace;

		private string imanager;

		private int createdBy;

		private DateTime? createdDate;

		private int updatedBy;

		private DateTime? updatedDate;

		private bool? isSystem;

		private bool? flagDeleted;

		private string primaryKeyFieldName;

		private string viewLabelFiledName;

		private string serviceUrl;

		private string serviceFindallMethod;

		private string viewUrl;

        private bool mainEntity;


        #endregion

        #region Public Properties

		public int SystemEntityId
		{
			get
			{
				return entityId;
			}
			set
			{
				entityId = value;
			}
		}
		public string TableName
		{
			get
			{
				return tableName;
			}
			set
			{
				tableName = value;
			}
		}
		public string EntityName
		{
			get
			{
				return entityName;
			}
			set
			{
				entityName = value;
			}
		}
		public string EntityNameAr
		{
			get
			{
				return entityNameAr;
			}
			set
			{
				entityNameAr = value;
			}
		}
		public string ModuleName
		{
			get
			{
				return moduleName;
			}
			set
			{
				moduleName = value;
			}
		}
		public string NameSpace
		{
			get
			{
				return nameSpace;
			}
			set
			{
				nameSpace = value;
			}
		}
		public string Imanager
		{
			get
			{
				return imanager;
			}
			set
			{
				imanager = value;
			}
		}
		public int CreatedBy
		{
			get
			{
				return createdBy;
			}
			set
			{
				createdBy = value;
			}
		}
		public DateTime? CreatedDate
		{
			get
			{
				return createdDate;
			}
			set
			{
				createdDate = value;
			}
		}
		public int UpdatedBy
		{
			get
			{
				return updatedBy;
			}
			set
			{
				updatedBy = value;
			}
		}
		public DateTime? UpdatedDate
		{
			get
			{
				return updatedDate;
			}
			set
			{
				updatedDate = value;
			}
		}
		public bool? IsSystem
		{
			get
			{
				return isSystem;
			}
			set
			{
				isSystem = value;
			}
		}
		public bool? FlagDeleted
		{
			get
			{
				return flagDeleted;
			}
			set
			{
				flagDeleted = value;
			}
		}
		public string PrimaryKeyFieldName
		{
			get
			{
				return primaryKeyFieldName;
			}
			set
			{
				primaryKeyFieldName = value;
			}
		}
		public string ViewLabelFiledName
		{
			get
			{
				return viewLabelFiledName;
			}
			set
			{
				viewLabelFiledName = value;
			}
		}
		public string ServiceUrl
		{
			get
			{
				return serviceUrl;
			}
			set
			{
				serviceUrl = value;
			}
		}
		public string ServiceFindallMethod
		{
			get
			{
				return serviceFindallMethod;
			}
			set
			{
				serviceFindallMethod = value;
			}
		}
		public string ViewUrl
		{
			get
			{
				return viewUrl;
			}
			set
			{
				viewUrl = value;
			}
		}


        public bool MainEntity
        {
            get { return mainEntity; }
            set { mainEntity = value; }
        }

        public string label
        {
            get
            {
                return (CentrixERP.Configuration.Configuration.Lang == CentrixERP.Configuration.Enums_S3.Configuration.Language.ar) ? EntityNameAr : EntityName;
            }
        }
        public int value
        {
            get
            {
                return SystemEntityId;
            }
        }
        #endregion

        //#region Composite Objects
        //private List<EntityControls> entityControlsList;
        //[CompositeObject(CompositObjectTypeEnum.List, "EntityId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true)]
        //public List<EntityControls> EntityControlsList
        //{
        //    get
        //    {
        //        if (entityControlsList == null)
        //            loadCompositObject_Lazy("EntityControlsList");
        //        return entityControlsList;
        //    }
        //    set
        //    {
        //        entityControlsList = value;;
        //    }
        //}
		
        //private List<EntityPermission> entityPermissionList;
        //[CompositeObject(CompositObjectTypeEnum.List, "EntityId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true)]
        //public List<EntityPermission> EntityPermissionList
        //{
        //    get
        //    {
        //        if (entityPermissionList == null)
        //            loadCompositObject_Lazy("EntityPermissionList");
        //        return entityPermissionList;
        //    }
        //    set
        //    {
        //        entityPermissionList = value;;
        //    }
        //}
		
        //private List<MetaData> metaDataList;
        //[CompositeObject(CompositObjectTypeEnum.List, "EntityId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true)]
        //public List<MetaData> MetaDataList
        //{
        //    get
        //    {
        //        if (metaDataList == null)
        //            loadCompositObject_Lazy("MetaDataList");
        //        return metaDataList;
        //    }
        //    set
        //    {
        //        metaDataList = value;;
        //    }
        //}
		
        //private List<NavigationMenu> navigationMenuList;
        //[CompositeObject(CompositObjectTypeEnum.List, "EntityId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true)]
        //public List<NavigationMenu> NavigationMenuList
        //{
        //    get
        //    {
        //        if (navigationMenuList == null)
        //            loadCompositObject_Lazy("NavigationMenuList");
        //        return navigationMenuList;
        //    }
        //    set
        //    {
        //        navigationMenuList = value;;
        //    }
        //}
		
        //private List<RolePermission> rolePermissionList;
        //[CompositeObject(CompositObjectTypeEnum.List, "EntityId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true)]
        //public List<RolePermission> RolePermissionList
        //{
        //    get
        //    {
        //        if (rolePermissionList == null)
        //            loadCompositObject_Lazy("RolePermissionList");
        //        return rolePermissionList;
        //    }
        //    set
        //    {
        //        rolePermissionList = value;;
        //    }
        //}
		
        //private List<StoredProcedure> storedProcedureList;
        //[CompositeObject(CompositObjectTypeEnum.List, "EntityId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true)]
        //public List<StoredProcedure> StoredProcedureList
        //{
        //    get
        //    {
        //        if (storedProcedureList == null)
        //            loadCompositObject_Lazy("StoredProcedureList");
        //        return storedProcedureList;
        //    }
        //    set
        //    {
        //        storedProcedureList = value;;
        //    }
        //}
		
        //private List<TaskAssignment> taskAssignmentList;
        //[CompositeObject(CompositObjectTypeEnum.List, "EntityId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true)]
        //public List<TaskAssignment> TaskAssignmentList
        //{
        //    get
        //    {
        //        if (taskAssignmentList == null)
        //            loadCompositObject_Lazy("TaskAssignmentList");
        //        return taskAssignmentList;
        //    }
        //    set
        //    {
        //        taskAssignmentList = value;;
        //    }
        //}
		
        //private List<TaskConfig> taskConfigList;
        //[CompositeObject(CompositObjectTypeEnum.List, "EntityId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true)]
        //public List<TaskConfig> TaskConfigList
        //{
        //    get
        //    {
        //        if (taskConfigList == null)
        //            loadCompositObject_Lazy("TaskConfigList");
        //        return taskConfigList;
        //    }
        //    set
        //    {
        //        taskConfigList = value;;
        //    }
        //}
		
        //private List<TaskEntity> taskEntityList;
        //[CompositeObject(CompositObjectTypeEnum.List, "EntityId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true)]
        //public List<TaskEntity> TaskEntityList
        //{
        //    get
        //    {
        //        if (taskEntityList == null)
        //            loadCompositObject_Lazy("TaskEntityList");
        //        return taskEntityList;
        //    }
        //    set
        //    {
        //        taskEntityList = value;;
        //    }
        //}
		


       // #endregion

        public override int GetIdentity()
        {
            return entityId;
        }
        public override void SetIdentity(int value)
        {
            entityId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
			throw new NotImplementedException();
        }
    }

    public class EntityRelations
    {
        public string RelatedEntityNameEn { get; set; }
        public string RelatedEntityNameAr { get; set; }
        public string RelatedEntityViewUrl { get; set; }
        public int RelatedPrimaryKeyId { get; set; }
    }
}