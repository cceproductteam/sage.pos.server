using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;


namespace CentrixERP.Common.Business.Entity
{
    [EntityIManager("CentrixERP.Common.Business.IManager.ICountryManager,CentrixERP.Business.IManager", isLogicalDelete = false)]
    public class Country : EntityBase<Country, int>
    {
        private int countryId = -1;
        private string countryNameEn;
        private string countryNameAr;
        private string countryCode;
        private string countryNameCode;
        private List<City> cityList;
        //private List<Address> addressList;

        public int CountryId
        {
            get { return countryId; }
            set { countryId = value; }
        }
        public string CountryNameEn
        {
            get { return countryNameEn; }
            set { countryNameEn = value; }
        }
        public string CountryNameAr
        {
            get { return countryNameAr; }
            set { countryNameAr = value; }
        }

        public string CountryCode
        {
            get { return countryCode; }
            set { countryCode = value; }
        }

        public string CountryNameCode
        {
            get { return countryNameCode; }
            set { countryNameCode = value; }
        }

        //[CompositeObject(CompositObjectTypeEnum.List, "CountryID", LazyLoad = true, CascadeDelete = true)]
        //public List<Address> AddressList
        //{
        //    get
        //    {
        //        if (AddressList == null)
        //            loadCompositObject_Lazy("AddressList");
        //        return AddressList;
        //    }
        //    set { AddressList = value; }
        //}



        [CompositeObject(CompositObjectTypeEnum.List, "CountryId", LazyLoad = true, CascadeDelete = true)]
        public List<City> CityList
        {
            get
            {
                if (cityList == null)
                    loadCompositObject_Lazy("CityList");
                return cityList;
            }
            set { cityList = value; }
        }


        public override int GetIdentity()
        {
            return countryId;
        }

        public override void SetIdentity(int value)
        {
            countryId = value;
        }

        public override void SetParentId(object value, Type ParentType)
        {
            throw new NotImplementedException();
        }
    }
}
