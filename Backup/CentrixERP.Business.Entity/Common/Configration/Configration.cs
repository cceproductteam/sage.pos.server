using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.Framework;
using SF.FrameworkEntity;
using Centrix.Notifications.Business.Entity;
using CentrixERP.Configuration;

namespace CentrixERP.Common.Business.Entity
{
    [EntityIManager("CentrixERP.Common.Business.IManager.IConfigrationManager,CentrixERP.Business.IManager", isLogicalDelete = false)]
    public class Configration:EntityBase<Configration,int>
    {

        private int configId = -1;
        private int minFontSize = -1;
        private int maxFontSize = -1;
        private int maxTagsToAppear = -1;      
        private int pageSize = -1;
        private bool lockUncompleteTasks;
        private Enums_S3.Configuration.Language defaultLanguage;
        private int firstDay = -1;
        private int firstHour = -1;
        private int minTime = -1;
        private int maxTime = -1;
        private int defaultEventMinutes = -1;
        private bool weekends;
        private double dragOpacity = -1;
        private string companyTextArabic;
        private string companyTextEnglish;
        private string personTextArabic;
        private string personTextEnglish;
        private int numberOfUsers = -1;
        private int tasksNumberToShow = -1;
        private string pluralCompanyTextEnglish;
        private string pluralCompanyTextArabic;
        private string pluralPersonTextEnglish;
        private string pluralPersonTextArabic;
        private string personNameFormat;
        private string addressFormat;
        private int notificationCount;
        private int notificationsTopValues;
        private bool forceRecivingNotifications;
        private int defaultEmailConfigrationId = -1;

     

        public string AddressFormat
        {
            get { return addressFormat; }
            set { addressFormat = value; }
        }

        public string PersonNameFormat
        {
            get { return personNameFormat; }
            set { personNameFormat = value; }
        }
        public int ConfigId
        {
            get { return configId; }
            set { configId = value; }
        }
        public int MinFontSize
        {
            get { return minFontSize; }
            set { minFontSize = value; }
        }
        public int MaxFontSize
        {
            get { return maxFontSize; }
            set { maxFontSize = value; }
        }
        public int PageSize
        {
            get { return pageSize; }
            set { pageSize = value; }
        }
        public bool LockUncompleteTasks
        {
            get { return lockUncompleteTasks; }
            set { lockUncompleteTasks = value; }
        }
        public Enums_S3.Configuration.Language DefaultLanguage
        {
            get { return defaultLanguage; }
            set { defaultLanguage = value; }
        }
        public int FirstDay
        {
            get { return firstDay; }
            set { firstDay = value; }
        }
        public int FirstHour
        {
            get { return firstHour; }
            set { firstHour = value; }
        }
        public int MinTime
        {
            get { return minTime; }
            set { minTime = value; }
        }
        public int MaxTime
        {
            get { return maxTime; }
            set { maxTime = value; }
        }
        public int DefaultEventMinutes
        {
            get { return defaultEventMinutes; }
            set { defaultEventMinutes = value; }
        }
        public bool Weekends
        {
            get { return weekends; }
            set { weekends = value; }
        }
        public double DragOpacity
        {
            get { return dragOpacity; }
            set { dragOpacity = value; }
        }
        public string CompanyTextArabic
        {
            get { return companyTextArabic; }
            set { companyTextArabic = value; }
        }
        public string CompanyTextEnglish
        {
            get { return companyTextEnglish; }
            set { companyTextEnglish = value; }
        }
        public string PersonTextArabic
        {
            get { return personTextArabic; }
            set { personTextArabic = value; }
        }
        public string PersonTextEnglish
        {
            get { return personTextEnglish; }
            set { personTextEnglish = value; }
        }
        public int NumberOfUsers
        {
            get { return numberOfUsers; }
            set { numberOfUsers = value; }
        }
        public int TasksNumberToShow
        {
            get { return tasksNumberToShow; }
            set { tasksNumberToShow = value; }
        }
        public string PluralCompanyTextEnglish
        {
            get { return pluralCompanyTextEnglish; }
            set { pluralCompanyTextEnglish = value; }
        }
        public string PluralCompanyTextArabic
        {
            get { return pluralCompanyTextArabic; }
            set { pluralCompanyTextArabic = value; }
        }
        public string PluralPersonTextEnglish
        {
            get { return pluralPersonTextEnglish; }
            set { pluralPersonTextEnglish = value; }
        }
        public string PluralPersonTextArabic
        {
            get { return pluralPersonTextArabic; }
            set { pluralPersonTextArabic = value; }
        }

        public int MaxTagsToAppear
        {
            get { return maxTagsToAppear; }
            set { maxTagsToAppear = value; }
        }

        public int NotificationCount
        {
            get { return this.notificationCount; }
            set { this.notificationCount = value; }
        }


        
        public int NotificationsTopValues
        {
            get { return this.notificationsTopValues; }
            set { this.notificationsTopValues = value; }
        }

        public bool ForceRecivingNotifications
        {
            get { return this.forceRecivingNotifications; }
            set { this.forceRecivingNotifications = value; }
        }

        public int DefaultEmailConfigrationId
        {
            get { return defaultEmailConfigrationId; }
            set { defaultEmailConfigrationId = value; }
        }


    
        private EmailConfiguration defaultEmailConfigObj;
        [CompositeObject(CompositObjectTypeEnum.Single, "DefaultEmailConfigrationId", LazyLoad = true, CascadeDelete = true, Savable = false)]
        public EmailConfiguration DefaultEmailConfigObj
        {
            get
            {
                if (defaultEmailConfigObj == null)
                    loadCompositObject_Lazy("DefaultEmailConfigObj");
                return defaultEmailConfigObj;
            }
            set
            {
                defaultEmailConfigObj = value;
                defaultEmailConfigObj.MarkModified();
            }
        }



        public override int GetIdentity()
        {
            return configId;
        }

        public override void SetIdentity(int value)
        {
            configId = value;
        }



        public override void SetParentId(object value, Type ParentType)
        {
            throw new NotImplementedException();
        }
    }


}
