using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.Framework;
using SF.FrameworkEntity;

namespace CentrixERP.Common.Business.Entity
{
    [EntityIManager("CentrixERP.Common.Business.IManager.INavigationMenueManager,CentrixERP.Business.IManager", isLogicalDelete = false)]
    public class NavigationMenue : EntityBase<NavigationMenue,int>
    {
        private int menueId = -1;

        public int MenueId
        {
            get { return menueId; }
            set { menueId = value; }
        }
        private string nameEnglish;

        public string NameEnglish
        {
            get { return nameEnglish; }
            set { nameEnglish = value; }
        }
        private string nameArabic;

        public string NameArabic
        {
            get { return nameArabic; }
            set { nameArabic = value; }
        }
        private string link;

        public string Link
        {
            get { return link; }
            set { link = value; }
        }
        private string image;

        public string Image
        {
            get { return image; }
            set { image = value; }
        }
        private string css;

        public string Css
        {
            get { return css; }
            set { css = value; }
        }
        private int order = -1;

        public int Order
        {
            get { return order; }
            set { order = value; }
        }



        private int category = -1;
        public int Category
        {
            get { return category; }
            set { category = value; }
        }


        private int permissionId = -1;

        public int PermissionId
        {
            get { return permissionId; }
            set { permissionId = value; }
        }

        public override int GetIdentity()
        {
            return menueId;
        }

        public override void SetIdentity(int value)
        {
            menueId = value;
        }

        public override void SetParentId(object value, Type ParentType)
        {
            throw new NotImplementedException();
        }
    }
}
