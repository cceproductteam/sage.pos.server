using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace CentrixERP.Business.Entity
{
     public class RegisterAmountsLite : EntityBaseLite
    {      
        
        #region Public Properties

         		[LiteProperty("register_amount_id",SqlDbType.Int)]
		public int RegisterAmountId
		{
			get;
			set;
		}

		[LiteProperty("register_id",SqlDbType.Int)]
		public int RegisterId
		{
			get;
			set;
		}

		[LiteProperty("currency_code",SqlDbType.NVarChar)]
		public string CurrencyCode
		{
			get;
			set;
		}

		[LiteProperty("currency_symbol",SqlDbType.NVarChar)]
		public string CurrencySymbol
		{
			get;
			set;
		}

		[LiteProperty("amount",SqlDbType.Money)]
		public decimal? Amount
		{
			get;
			set;
		}

		[LiteProperty("payment_type_id",SqlDbType.Int)]
		public int PaymentTypeId
		{
			get;
			set;
		}

		[LiteProperty("payment_type_name",SqlDbType.NVarChar)]
		public string PaymentTypeName
		{
			get;
			set;
		}

		[LiteProperty("close_register_id",SqlDbType.Int)]
		public int CloseRegisterId
		{
			get;
			set;
		}

		[LiteProperty("is_synced",SqlDbType.Bit)]
		public bool? IsSynced
		{
			get;
			set;
		}

		[LiteProperty("amount_default_currency",SqlDbType.Money)]
		public decimal? AmountDefaultCurrency
		{
			get;
			set;
		}



        [LiteProperty("register_amount_id", SqlDbType.Int)]
        public int value
        {
            get;
            set;
        }

          [LiteProperty("register_amount_id", SqlDbType.Int)]
        public int label
        {
            get;
            set;
        }

         [LiteProperty("flag_deleted", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? FlagDeleted
        {
            get;
            set;
        }

        [LiteProperty("is_system",SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
		public bool? IsSystem
		{
			get;
			set;
		}

		[LiteProperty("created_date",SqlDbType.DateTime)]
		public DateTime? CreatedDate
		{
			get;
			set;
		}

		[LiteProperty("updated_date",SqlDbType.DateTime)]
		public DateTime? UpdatedDate
		{
			get;
			set;
		}

		[LiteProperty("created_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int CreatedBy
		{
			get;
			set;
		}

		[LiteProperty("updated_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int UpdatedBy
		{
			get;
			set;
		}

         [LiteProperty("created_by_name",SqlDbType.NVarChar, FindAllLiteParameter = false)]
         public string CreatedByName{get;set;}

         [LiteProperty("updated_by_name",SqlDbType.NVarChar,FindAllLiteParameter = false)]
         public string UpdatedByName{get;set;}

         [LiteProperty("register_amount_id",SqlDbType.NVarChar)]
          public int Id
        {
            get{return this.RegisterAmountId; }
            set{this.RegisterAmountId=value;}
        }

         [LiteProperty("last_rows",SqlDbType.NVarChar)]
         public bool LastRows { get; set; }

         [LiteProperty("total_count",SqlDbType.NVarChar)]
         public int TotalRecords { get; set; }
        #endregion
       
          
     }
}