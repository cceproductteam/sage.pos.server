using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
using CentrixERP.Business.Entity;

namespace CentrixERP.Business.Entity
{
    [EntityIManager("Centrix.POS.Standalone.Client.Business.IManager.IRegisterAmountsManager,Centrix.POS.Standalone.Client.Business.IManager", isLogicalDelete = true)]
    public class RegisterAmounts : EntityBase<RegisterAmounts, int>
    {
        #region Constructors

        public RegisterAmounts() { }

        public RegisterAmounts(int registerAmountId)
        {
            this.registerAmountId = registerAmountId;
        }

        #endregion

        #region Private Properties

        private int registerAmountId;

        private int registerId;

        private string currencyCode;

        private string currencySymbol;

        private double amount;

        private int paymentTypeId;

        private string paymentTypeName;

        private bool? flagDeleted;

        private bool? isSystem;

        private DateTime? createdDate;

        private int createdBy;

        private int updatedBy;

        private DateTime? updatedDate;

        private int closeRegisterId;

        private bool? isSynced;

        private double amountDefaultCurrency;

        private string creditCardType;



        //private bool? flagDeleted;

        //private bool? isSystem;

        //private int createdBy;

        //private int updatedBy;

        //private DateTime? createdDate;

        //private DateTime? updatedDate;
        #endregion

        #region Public Properties

        [PrimaryKey("@register_amount_id", SqlDbType.Int)]
        [Property("register_amount_id", SqlDbType.Int, AddParameter = false)]
        public int RegisterAmountId
        {
            get
            {
                return registerAmountId;
            }
            set
            {
                registerAmountId = value;
            }
        }

        [Property("register_id", SqlDbType.Int)]
        [PropertyConstraint(false, "Register")]
        public int RegisterId
        {
            get
            {
                return registerId;
            }
            set
            {
                registerId = value;
            }
        }

        [Property("currency_code", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "CurrencyCode", MaxLength = 50, MinLength = 3)]
        public string CurrencyCode
        {
            get
            {
                return currencyCode;
            }
            set
            {
                currencyCode = value;
            }
        }

        [Property("currency_symbol", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "CurrencySymbol", MaxLength = 50, MinLength = 3)]
        public string CurrencySymbol
        {
            get
            {
                return currencySymbol;
            }
            set
            {
                currencySymbol = value;
            }
        }

        [Property("amount", SqlDbType.Money)]
        [PropertyConstraint(false, "Amount")]
        public double Amount
        {
            get
            {
                return amount;
            }
            set
            {
                amount = value;
            }
        }

        [Property("payment_type_id", SqlDbType.Int)]
        [PropertyConstraint(false, "PaymentType")]
        public int PaymentTypeId
        {
            get
            {
                return paymentTypeId;
            }
            set
            {
                paymentTypeId = value;
            }
        }

        [Property("payment_type_name", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "PaymentTypeName", MaxLength = 100, MinLength = 3)]
        public string PaymentTypeName
        {
            get
            {
                return paymentTypeName;
            }
            set
            {
                paymentTypeName = value;
            }
        }

        [Property("close_register_id", SqlDbType.Int)]
        [PropertyConstraint(false, "CloseRegister")]
        public int CloseRegisterId
        {
            get
            {
                return closeRegisterId;
            }
            set
            {
                closeRegisterId = value;
            }
        }

        [Property("is_synced", SqlDbType.Bit)]
        [PropertyConstraint(false, "IsSynced")]
        public bool? IsSynced
        {
            get
            {
                return isSynced;
            }
            set
            {
                isSynced = value;
            }
        }

        [Property("amount_default_currency", SqlDbType.Money)]
        [PropertyConstraint(false, "AmountDefaultCurrency")]
        public double AmountDefaultCurrency
        {
            get
            {
                return amountDefaultCurrency;
            }
            set
            {
                amountDefaultCurrency = value;
            }
        }
        [Property("credit_card_type", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "CreditCardType")]
        public string CreditCardType
        {
            get;
            set;
        }



        [Property("flag_deleted", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [Property("is_system", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool? IsSystem
        {
            get
            {
                return isSystem;
            }
            set
            {
                isSystem = value;
            }
        }

        [Property("created_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? CreatedDate
        {
            get
            {
                return createdDate;
            }
            set
            {
                createdDate = value;
            }
        }

        [Property("updated_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? UpdatedDate
        {
            get
            {
                return updatedDate;
            }
            set
            {
                updatedDate = value;
            }
        }

        [Property("created_by", SqlDbType.Int, AddParameter = true, UpdateParameter = false)]
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        [Property("updated_by", SqlDbType.Int, AddParameter = false, UpdateParameter = true)]
        public int UpdatedBy
        {
            get
            {
                return updatedBy;
            }
            set
            {
                updatedBy = value;
            }
        }
        #endregion

        #region Composite Objects


        #endregion


        public override int GetIdentity()
        {
            return registerAmountId;
        }
        public override void SetIdentity(int value)
        {
            registerAmountId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            if (ParentType == typeof(CloseRegister))
                CloseRegisterId = (int)value;
        }
    }
}