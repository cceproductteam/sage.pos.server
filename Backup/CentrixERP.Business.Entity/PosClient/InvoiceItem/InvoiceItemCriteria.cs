using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
using CentrixERP.Common.Business.Factory;

namespace CentrixERP.Business.Entity
{
    public class InvoiceItemCriteria : CriteriaBase, IAdvancedSearchCriteria
    {
        [CrtiteriaField("@page_number", SqlDbType.Int)]
        public int? pageNumber { get; set; }

        [CrtiteriaField("@result_count", SqlDbType.Int)]
        public int? resultCount { get; set; }

        [CrtiteriaField("@keyword", SqlDbType.NVarChar)]
        public string Keyword { get; set; }

        [CrtiteriaField("@search_criteria", SqlDbType.NVarChar)]
        public string SearchCriteria { get; set; }

        [CrtiteriaField("@sort_type", SqlDbType.Bit)]
        public bool? SortType { get; set; }

        [CrtiteriaField("@sort_fields", SqlDbType.NVarChar)]
        public string SortFields { get; set; }

        [CrtiteriaField("@invoice_item_id", SqlDbType.Int)]
        public int? InvoiceItemId { get; set; }

        [CrtiteriaField("@invoice_item_id", SqlDbType.Int)]
        public int? EntityId { get; set; }

        [CrtiteriaField("@invoice_id", SqlDbType.Int)]
        public int InvoiceId { get; set; }

        [CrtiteriaField("@invoice_unique_number", SqlDbType.NVarChar)]
        public string InvoiceUniqueNumber { get; set; }

        [CrtiteriaField("@is_exchanged", SqlDbType.Bit)]
        public bool IsExchanged { get; set; }

        [CrtiteriaField("@location_id", SqlDbType.Int)]
        public int LocationId { get; set; }

        int IAdvancedSearchCriteria.EntityId
        {
            get;
            set;
        }

        bool IAdvancedSearchCriteria.SortType
        {
            get;
            set;
        }

        int IAdvancedSearchCriteria.pageNumber
        {
            get;
            set;
        }

        int IAdvancedSearchCriteria.resultCount
        {
            get;
            set;
        }
    }
}
