using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
using CentrixERP.Common.Business.Factory;

namespace CentrixERP.Business.Entity
{
    public class CloseRegisterCriteria : CriteriaBase, IAdvancedSearchCriteria
    {
        [CrtiteriaField("@page_number", SqlDbType.Int)]
        public int? pageNumber { get; set; }

        [CrtiteriaField("@result_count", SqlDbType.Int)]
        public int? resultCount { get; set; }

        [CrtiteriaField("@keyword", SqlDbType.NVarChar)]
        public string Keyword { get; set; }

        [CrtiteriaField("@search_criteria", SqlDbType.NVarChar)]
        public string SearchCriteria { get; set; }

        [CrtiteriaField("@sort_type", SqlDbType.Bit)]
        public bool? SortType { get; set; }

        [CrtiteriaField("@sort_fields", SqlDbType.NVarChar)]
        public string SortFields { get; set; }

        [CrtiteriaField("@close_register_id", SqlDbType.Int)]
        public int? CloseRegisterId { get; set; }

        [CrtiteriaField("@etity_id", SqlDbType.Int)]
        public int? EntityId { get; set; }

        [CrtiteriaField("@register_id", SqlDbType.Int)]
        public int? RegisterId { get; set; }

        [CrtiteriaField("@user_id", SqlDbType.Int)]
        public int? UserId { get; set; }

        [CrtiteriaField("@store_id", SqlDbType.Int)]
        public int? StoreId { get; set; }

        [CrtiteriaField("@is_synced", SqlDbType.Bit)]
        public bool? isSynced { get; set; }

        [CrtiteriaField("@is_synce_proccess", SqlDbType.Bit)]
        public bool? isSynceProccess { get; set; }

        int IAdvancedSearchCriteria.EntityId
        {
            get;
            set;
        }

        bool IAdvancedSearchCriteria.SortType
        {
            get;
            set;
        }

        int IAdvancedSearchCriteria.pageNumber
        {
            get;
            set;
        }

        int IAdvancedSearchCriteria.resultCount
        {
            get;
            set;
        }
    }
}
