using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;


namespace CentrixERP.Business.Entity
{
    [EntityIManager("Centrix.POS.Standalone.Client.Business.IManager.ICloseRegisterManager,Centrix.POS.Standalone.Client.Business.IManager", isLogicalDelete = true)]
    public class CloseRegister : EntityBase<CloseRegister, int>
    {
        #region Constructors

        public CloseRegister() { }

        public CloseRegister(int closeRegisterId)
        {
            this.closeRegisterId = closeRegisterId;
        }

        #endregion

        #region Private Properties

        private int closeRegisterId;

        private int storeId;

        private string storeName;

        private DateTime? openedDate;

        private DateTime? closedDate;

        private bool? flagDeleted;

        private bool? isSystem;

        private int createdBy;

        private int updatedBy;

        private DateTime? createdDate;

        private DateTime? updatedDate;

        private int registerId;

        private string registerName;

        private double totalPrice;

        private bool? isSynced;

        private int managerCloseRegisterId;

        private double openedCashLoan;

        private bool? isIdentical;

        private double  diffAmount;
        private string remarks;
        private string sessionNumber;



        //private bool? flagDeleted;

        //private bool? isSystem;

        //private int createdBy;

        //private int updatedBy;

        //private DateTime? createdDate;

        //private DateTime? updatedDate;
        #endregion

        #region Public Properties

        [PrimaryKey("@close_register_id", SqlDbType.Int)]
        [Property("close_register_id", SqlDbType.Int, AddParameter = false)]
        public int CloseRegisterId
        {
            get
            {
                return closeRegisterId;
            }
            set
            {
                closeRegisterId = value;
            }
        }

        [Property("store_id", SqlDbType.Int)]
        [PropertyConstraint(false, "Store")]
        public int StoreId
        {
            get
            {
                return storeId;
            }
            set
            {
                storeId = value;
            }
        }

        [Property("store_name", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "StoreName", MaxLength = 100, MinLength = 3)]
        public string StoreName
        {
            get
            {
                return storeName;
            }
            set
            {
                storeName = value;
            }
        }

        [Property("opened_date", SqlDbType.DateTime)]
        [PropertyConstraint(false, "OpenedDate")]
        public DateTime? OpenedDate
        {
            get
            {
                return openedDate;
            }
            set
            {
                openedDate = value;
            }
        }

        [Property("closed_date", SqlDbType.DateTime)]
        [PropertyConstraint(false, "ClosedDate")]
        public DateTime? ClosedDate
        {
            get
            {
                return closedDate;
            }
            set
            {
                closedDate = value;
            }
        }

        [Property("register_id", SqlDbType.Int)]
        [PropertyConstraint(false, "Register")]
        public int RegisterId
        {
            get
            {
                return registerId;
            }
            set
            {
                registerId = value;
            }
        }

        [Property("register_name", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "RegisterName", MaxLength = 100, MinLength = 3)]
        public string RegisterName
        {
            get
            {
                return registerName;
            }
            set
            {
                registerName = value;
            }
        }

        [Property("unique_number", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "UniqueNumber", MaxLength = 100, MinLength = 0)]
        public string UniqueNumber
        {
            get;
            set;
        }

        [Property("total_price", SqlDbType.Money)]
        [PropertyConstraint(false, "TotalPrice")]
        public double TotalPrice
        {
            get
            {
                return totalPrice;
            }
            set
            {
                totalPrice = value;
            }
        }

        [Property("is_synced", SqlDbType.Bit,FindParameter=false)]
        [PropertyConstraint(false, "IsSynced")]
        public bool? IsSynced
        {
            get
            {
                return isSynced;
            }
            set
            {
                isSynced = value;
            }
        }

        [Property("manager_close_register_id", SqlDbType.Int)]
        [PropertyConstraint(false, "ManagerCloseRegister")]
        public int ManagerCloseRegisterId
        {
            get
            {
                return managerCloseRegisterId;
            }
            set
            {
                managerCloseRegisterId = value;
            }
        }

        [Property("opened_cash_loan", SqlDbType.Money)]
        [PropertyConstraint(false, "OpenedCashLoan")]
        public double OpenedCashLoan
        {
            get
            {
                return openedCashLoan;
            }
            set
            {
                openedCashLoan = value;
            }
        }

        [Property("is_identical", SqlDbType.Bit)]
        [PropertyConstraint(false, "IsIdentical")]
        public bool? IsIdentical
        {
            get
            {
                return isIdentical;
            }
            set
            {
                isIdentical = value;
            }
        }

        [Property("diff_amount", SqlDbType.Money)]
        [PropertyConstraint(false, "DiffAmount")]
        public double DiffAmount
        {
            get
            {
                return diffAmount;
            }
            set
            {
                diffAmount = value;
            }
        }
        [Property("remarks", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "Remarks")]
        public string Remarks
        {
            get { return remarks; }
            set { remarks = value; }
        }

        [Property("session_number", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "SessionNumber")]
        public string SessionNumber
        {
            get { return sessionNumber; }
            set { sessionNumber = value; }
        }



        [Property("flag_deleted", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [Property("is_system", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool? IsSystem
        {
            get
            {
                return isSystem;
            }
            set
            {
                isSystem = value;
            }
        }

        [Property("created_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? CreatedDate
        {
            get
            {
                return createdDate;
            }
            set
            {
                createdDate = value;
            }
        }

        [Property("updated_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? UpdatedDate
        {
            get
            {
                return updatedDate;
            }
            set
            {
                updatedDate = value;
            }
        }

        [Property("created_by", SqlDbType.Int, AddParameter = true, UpdateParameter = false)]
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        [Property("updated_by", SqlDbType.Int, AddParameter = false, UpdateParameter = true)]
        public int UpdatedBy
        {
            get
            {
                return updatedBy;
            }
            set
            {
                updatedBy = value;
            }
        }
        #endregion

        #region Composite Objects

        private List<RegisterAmounts> registerAmountsList;
        [CompositeObject(CompositObjectTypeEnum.List, "CloseRegisterId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true)]
        public List<RegisterAmounts> RegisterAmountsList
        {
            get
            {
                if (registerAmountsList == null)
                    loadCompositObject_Lazy("RegisterAmountsList");
                return registerAmountsList;
            }
            set
            {
                registerAmountsList = value;
            }
        }
        #endregion


        public override int GetIdentity()
        {
            return closeRegisterId;
        }
        public override void SetIdentity(int value)
        {
            closeRegisterId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            throw new NotImplementedException();
        }
    }




    public class CloseRegiserDetails
    {
        private double totalPrice;

        private int paymentType;

        private int paymentMethod;

        private double defatulTax;

        private string paymentTypeName;

        private string currency;

        private double currencyDefaultPrice;

        private double cashLoan;

        private bool isLayby;

        private int invoiceStatus;

        private string creditCardType;

        private int invoiceCount;


        public int InvoiceCount
        {
            get { return invoiceCount; }
            set { invoiceCount = value; }
        }
        public double TotalPrice
        {
            get { return totalPrice; }
            set { totalPrice = value; }
        }

        public int PaymentType
        {
            get { return paymentType; }
            set { paymentType = value; }
        }

        public int PaymentMethod
        {
            get { return paymentMethod; }
            set { paymentMethod = value; }
        }

        public double DefaultTax
        {
            get { return defatulTax; }
            set { defatulTax = value; }
        }

        public string PaymentTypeName
        {
            get { return paymentTypeName; }
            set { paymentTypeName = value; }
        }

        public string Currency
        {
            get { return currency; }
            set { currency = value; }
        }

        public double CurrencyDefaultPrice
        {
            get { return currencyDefaultPrice; }
            set { currencyDefaultPrice = value; }

        }

        public double CashLoan
        {
            get { return cashLoan; }
            set { cashLoan = value; }
        }

        public bool IsLayBy
        {
            get { return isLayby; }
            set { isLayby = value; }
        }

        public int InvoiceStatus
        {
            get { return invoiceStatus; }
            set { invoiceStatus = value; }
        }
        public string CreditCardType
        {
            get { return creditCardType; }
            set { creditCardType = value; }
        }

       

    }

    public class CloseRegisterTotals
    {
        private double salesAmount;
        private double refundAmount;
        private double exchangeAmount;
        private double depoist;
        private double withdrawal;

        public double SalesAmount
        {
            get { return salesAmount; }
            set { salesAmount = value; }
        }

        public double RefundAmount
        {
            get { return refundAmount; }
            set { refundAmount = value; }
        }

        public double ExchangeAmount
        {
            get { return exchangeAmount; }
            set { exchangeAmount = value; }
        }

        public double Deposit
        {
            get { return depoist; }
            set { depoist = value; }
        }

        public double Withdrawal
        {
            get { return withdrawal; }
            set { withdrawal = value; }
        }
    }

}