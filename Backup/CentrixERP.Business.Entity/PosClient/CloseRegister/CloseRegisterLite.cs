using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace CentrixERP.Business.Entity
{
     public class CloseRegisterLite : EntityBaseLite
    {      
        
        #region Public Properties

         		[LiteProperty("close_register_id",SqlDbType.Int)]
		public int CloseRegisterId
		{
			get;
			set;
		}

		[LiteProperty("store_id",SqlDbType.Int)]
		public int StoreId
		{
			get;
			set;
		}

		[LiteProperty("store_name",SqlDbType.NVarChar)]
		public string StoreName
		{
			get;
			set;
		}

		[LiteProperty("opened_date",SqlDbType.DateTime)]
		public DateTime? OpenedDate
		{
			get;
			set;
		}

		[LiteProperty("closed_date",SqlDbType.DateTime)]
		public DateTime? ClosedDate
		{
			get;
			set;
		}

		[LiteProperty("register_id",SqlDbType.Int)]
		public int RegisterId
		{
			get;
			set;
		}

		[LiteProperty("register_name",SqlDbType.NVarChar)]
		public string RegisterName
		{
			get;
			set;
		}

        [LiteProperty("unique_number", SqlDbType.NVarChar)]
        public string UniqueNumber
        {
            get;
            set;
        }

		[LiteProperty("total_price",SqlDbType.Money)]
		public decimal? TotalPrice
		{
			get;
			set;
		}

		[LiteProperty("is_synced",SqlDbType.Bit)]
		public bool? IsSynced
		{
			get;
			set;
		}

		[LiteProperty("manager_close_register_id",SqlDbType.Int)]
		public int ManagerCloseRegisterId
		{
			get;
			set;
		}

		[LiteProperty("opened_cash_loan",SqlDbType.Money)]
		public decimal? OpenedCashLoan
		{
			get;
			set;
		}

		[LiteProperty("is_identical",SqlDbType.Bit)]
		public bool? IsIdentical
		{
			get;
			set;
		}

		[LiteProperty("diff_amount",SqlDbType.Money)]
		public decimal? DiffAmount
		{
			get;
			set;
		}



        [LiteProperty("close_register_id", SqlDbType.Int)]
        public int value
        {
            get;
            set;
        }

          [LiteProperty("close_register_id", SqlDbType.Int)]
        public int label
        {
            get;
            set;
        }

         [LiteProperty("flag_deleted", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? FlagDeleted
        {
            get;
            set;
        }

        [LiteProperty("is_system",SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
		public bool? IsSystem
		{
			get;
			set;
		}

		[LiteProperty("created_date",SqlDbType.DateTime)]
		public DateTime? CreatedDate
		{
			get;
			set;
		}

		[LiteProperty("updated_date",SqlDbType.DateTime)]
		public DateTime? UpdatedDate
		{
			get;
			set;
		}

		[LiteProperty("created_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int CreatedBy
		{
			get;
			set;
		}

		[LiteProperty("updated_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int UpdatedBy
		{
			get;
			set;
		}

         [LiteProperty("created_by_name",SqlDbType.NVarChar, FindAllLiteParameter = false)]
         public string CreatedByName{get;set;}

         [LiteProperty("updated_by_name",SqlDbType.NVarChar,FindAllLiteParameter = false)]
         public string UpdatedByName{get;set;}

         [LiteProperty("close_register_id",SqlDbType.NVarChar)]
          public int Id
        {
            get{return this.CloseRegisterId; }
            set{this.CloseRegisterId=value;}
        }

         [LiteProperty("last_rows",SqlDbType.NVarChar)]
         public bool LastRows { get; set; }

         [LiteProperty("total_count",SqlDbType.NVarChar)]
         public int TotalRecords { get; set; }
        #endregion
       
          
     }


     public class CloseRegisterInquiryLite
     {
         public int StoreId
         {
             get;
             set;
         }

         public int CloseRegisterId
         {
             get;
             set;
         }

         public string StoreName
         {
             get;
             set;
         }



         public DateTime? OpenedDate
         {
             get;
             set;
         }

         public DateTime? ClosedDate
         {
             get;
             set;
         }


         public double OpenedCashLoan { get; set; }

         public double TotalPrice { get; set; }

         public string IsIdentical { get; set; }

         public double DifferenceAmount { get; set; }
        
         public int RowNum
         {
             get;
             set;
         }


     }

}