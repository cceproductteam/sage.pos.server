using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace CentrixERP.Business.Entity
{
     public class InvoiceLite : EntityBaseLite
    {      
        
        #region Public Properties

         		[LiteProperty("invoice_id",SqlDbType.Int)]
		public int InvoiceId
		{
			get;
			set;
		}

		[LiteProperty("invoice_date",SqlDbType.DateTime)]
		public DateTime? InvoiceDate
		{
			get;
			set;
		}

		[LiteProperty("company_id",SqlDbType.Int)]
		public int CompanyId
		{
			get;
			set;
		}

		[LiteProperty("person_id",SqlDbType.Int)]
		public int PersonId
		{
			get;
			set;
		}

		[LiteProperty("company_name",SqlDbType.NVarChar)]
		public string CompanyName
		{
			get;
			set;
		}

		[LiteProperty("person_name",SqlDbType.NVarChar)]
		public string PersonName
		{
			get;
			set;
		}

		[LiteProperty("total_price",SqlDbType.Float)]
		public Double TotalPrice
		{
			get;
			set;
		}

		[LiteProperty("grand_total",SqlDbType.Float)]
		public Double GrandTotal
		{
			get;
			set;
		}

        [LiteProperty("percentage_discount", SqlDbType.Float)]
		public Double PercentageDiscount
		{
			get;
			set;
		}

		[LiteProperty("tax",SqlDbType.Decimal)]
		public Object Tax
		{
			get;
			set;
		}

		[LiteProperty("note",SqlDbType.NVarChar)]
		public string Note
		{
			get;
			set;
		}

		[LiteProperty("amount_discount",SqlDbType.Float)]
		public Double AmountDiscount
		{
			get;
			set;
		}

		[LiteProperty("invoice_status",SqlDbType.Int)]
		public int InvoiceStatus
		{
			get;
			set;
		}

		[LiteProperty("payment_type",SqlDbType.Int)]
		public int PaymentType
		{
			get;
			set;
		}

		[LiteProperty("payment_method",SqlDbType.Int)]
		public int PaymentMethod
		{
			get;
			set;
		}

		[LiteProperty("store_id",SqlDbType.Int)]
		public int StoreId
		{
			get;
			set;
		}

		[LiteProperty("register_id",SqlDbType.Int)]
		public int RegisterId
		{
			get;
			set;
		}

		[LiteProperty("invoice_number",SqlDbType.NVarChar)]
		public string InvoiceNumber
		{
			get;
			set;
		}

		[LiteProperty("change_amount",SqlDbType.Float)]
		public Double ChangeAmount
		{
			get;
			set;
		}

		[LiteProperty("parent_invoice_id",SqlDbType.Int)]
		public int ParentInvoiceId
		{
			get;
			set;
		}

		[LiteProperty("sale_man_id",SqlDbType.Int)]
		public int SaleManId
		{
			get;
			set;
		}

		[LiteProperty("sales_man_name",SqlDbType.NVarChar)]
		public string SalesManName
		{
			get;
			set;
		}

		[LiteProperty("is_layby",SqlDbType.Bit)]
		public bool? IsLayby
		{
			get;
			set;
		}

		[LiteProperty("down_payment",SqlDbType.Float)]
		public Double DownPayment
		{
			get;
			set;
		}

		[LiteProperty("layby_number",SqlDbType.NVarChar)]
		public string LaybyNumber
		{
			get;
			set;
		}

		[LiteProperty("parent_layby_id",SqlDbType.Int)]
		public int ParentLaybyId
		{
			get;
			set;
		}

		[LiteProperty("is_copied",SqlDbType.Bit)]
		public bool? IsCopied
		{
			get;
			set;
		}

		[LiteProperty("items_discount",SqlDbType.Float)]
		public Double ItemsDiscount
		{
			get;
			set;
		}

		[LiteProperty("is_synced",SqlDbType.Bit)]
		public bool? IsSynced
		{
			get;
			set;
		}

		[LiteProperty("invoice_unique_number",SqlDbType.NVarChar)]
		public string InvoiceUniqueNumber
		{
			get;
			set;
		}

		[LiteProperty("price_list_code",SqlDbType.NVarChar)]
		public string PriceListCode
		{
			get;
			set;
		}

		[LiteProperty("is_new",SqlDbType.Bit)]
		public bool? IsNew
		{
			get;
			set;
		}


        [LiteProperty("taxable_invoice", SqlDbType.Bit)]
        public bool? IsTaxableInvoice
        {
            get;
            set;
        }


        [LiteProperty("canceled_layby", SqlDbType.Bit)]
        public bool? IsCanceledLayBy
        {
            get;
            set;
        }
        [LiteProperty("invoice_id", SqlDbType.Int)]
        public int value
        {
            get;
            set;
        }

          [LiteProperty("invoice_id", SqlDbType.Int)]
        public int label
        {
            get;
            set;
        }

         [LiteProperty("flag_deleted", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? FlagDeleted
        {
            get;
            set;
        }

        [LiteProperty("is_system",SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
		public bool? IsSystem
		{
			get;
			set;
		}

		[LiteProperty("created_date",SqlDbType.DateTime)]
		public DateTime? CreatedDate
		{
			get;
			set;
		}

		[LiteProperty("updated_date",SqlDbType.DateTime)]
		public DateTime? UpdatedDate
		{
			get;
			set;
		}

		[LiteProperty("created_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int CreatedBy
		{
			get;
			set;
		}

		[LiteProperty("updated_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int UpdatedBy
		{
			get;
			set;
		}
        [LiteProperty("accpac_shipment_no", SqlDbType.NVarChar)]
        public string AccpacShipmentNum
        {
            get;
            set;
        }
        [LiteProperty("accpac_orderentry_no", SqlDbType.NVarChar)]
        public string AccpacOrderentryNum
        {
            get;
            set;
        }
        [LiteProperty("accpac_invoice_no", SqlDbType.NVarChar)]
        public string AccpacInvoiceNum
        {
            get;
            set;
        }
        [LiteProperty("accpac_refund_no", SqlDbType.NVarChar)]
        public string AccpacRefundNo
        {
            get;
            set;
        }

         [LiteProperty("created_by_name",SqlDbType.NVarChar, FindAllLiteParameter = false)]
         public string CreatedByName{get;set;}

         [LiteProperty("updated_by_name",SqlDbType.NVarChar,FindAllLiteParameter = false)]
         public string UpdatedByName{get;set;}

         [LiteProperty("invoice_id",SqlDbType.NVarChar)]
          public int Id
        {
            get{return this.InvoiceId; }
            set{this.InvoiceId=value;}
        }

         [LiteProperty("last_rows",SqlDbType.NVarChar)]
         public bool LastRows { get; set; }

         [LiteProperty("total_count",SqlDbType.NVarChar)]
         public int TotalRecords { get; set; }

         [LiteProperty("is_converted_to_ap_misc", SqlDbType.Bit)]
         public bool? IsConvertedToApMisc { get; set; }

         [LiteProperty("ap_misc_number", SqlDbType.NVarChar)]
         public string ApMiscNumber { get; set; }

        #endregion
       
          
     }

     public class InvoiceIntegration
     {
         public int ParentInvoiceId { get; set; }
         public int InvoiceId
         {
             get;
             set;
         }

         public DateTime? InvoiceDate
         {
             get;
             set;
         }

         public string InvoiceUniqueNumber
         {
             get;
             set;
         }

         public double GrandTotal
         {
             get;
             set;
         }

         public double AmountDiscount
         {
             get;
             set;
         }
         public double PercentageDiscount
         {
             get;
             set;
         }

         public string InvoiceNumber
         {
             get;
             set;
         }

         public int ArCashCustomerId
         {
             get;
             set;
         }

         public int ArVisaCustomerId
         {
             get;
             set;
         }

         public int ArMixedCustomerId
         {
             get;
             set;
         }

         public int LocationId
         {
             get;
             set;
         }

         public string PersonName
         {
             get;
             set;
         }

         public double TotalTaxAmount
         {
             get;
             set;
         }
         public double TotalDiscountAmount
         {
             get;
             set;
         }
         public int TaxClassId
         {
             get;
             set;
         }

         public int CurrencyId
         {
             get;
             set;
         }
         public double TotalAmount
         {
             get;
             set;
         }
         public int CustomerId { get; set; }
         public int NonTaxableClassId { get; set; }

         public string CreditNoteAccountIds { get; set; }

         public double ChangeAmount { get; set; }

         public int InvoiceStatus { get; set; }

     }
     


     public class InvoiceInquiryLite
     {
         public int InvoiceId
         {
             get;
             set;
         }

         public string InvoiceNumber
         {
             get;
             set;
         }

         public string Type { get; set; }


         public DateTime? InvoiceDate
         {
             get;
             set;
         }

         public string ShipmentNumber { get; set; }


         public double Total
         {
             get;
             set;
         }

         public string Store
         {
             get;
             set;
         }


         public string Register
         {
             get;
             set;
         }

         public int ShipmentId
         {
             get;
             set;
         }

         public int RowNum
         {
             get;
             set;
         }
         public double CashAmount
         {
             get;
             set;
         }
         public double CreditCardAmount
         {
             get;
             set;
         }

         public double ChequeAmount
         {
             get;
             set;
         }
         public double OnAccountAmount
         {
             get;
             set;
         }
         public double TotalPrice
         {
             get;
             set;
         }
         public double Tax
         {
             get;
             set;
         }
         public double AmountDiscount
         {
             get;
             set;
         }
         public double ItemsDiscount
         {
             get;
             set;
         }

         public double TotalPayment
         {
             get;
             set;
         }

         public string Currency
         {
             get;
             set;
         }



     }

     public class POSQuantityInQuiryLite
     {
         public string LocationName
         {
             get;
             set;

         }
         public string ItemName
         {
             get;
             set;

         }
         public string ItemNumber
         {
             get;
             set;

         }

         public string StoreName
         {
             get;
             set;
         }

         public int ERPQuantity
         {
             get;
             set;

         }
         public int POSQuantity
         {
             get;
             set;

         }
         public int StagingQuantity
         {
             get;
             set;
         }

         public int PendingQTY
         {
             get;
             set;

         }
         public int SummationQTY
         {
             get;
             set;

         }

         public int StagingOutQuantity
         {
             get;
             set;
         }

         public int PendingOutQuantity
         {
             get;
             set;
         }
         
         public int RowNum
         {
             get;
             set;
         }
     }

     public class ICQuantityInQuiryLite
     {
         public string LocationName
         {
             get;
             set;

         }
         public string ItemName
         {
             get;
             set;

         }
         public string ItemNumber
         {
             get;
             set;

         }

         public string StoreName
         {
             get;
             set;
         }

         public int ERPQuantity
         {
             get;
             set;

         }
         public int POSQuantity
         {
             get;
             set;

         }
         public int StagingQuantity
         {
             get;
             set;
         }

         public int PendingQTY
         {
             get;
             set;

         }
         public int SummationQTY
         {
             get;
             set;

         }

         public int StagingOutQuantity
         {
             get;
             set;
         }

         public int PendingOutQuantity
         {
             get;
             set;
         }

         public int RowNum
         {
             get;
             set;
         }
         public Double AverageCost
         {
             get;
             set;
         }
     }
}