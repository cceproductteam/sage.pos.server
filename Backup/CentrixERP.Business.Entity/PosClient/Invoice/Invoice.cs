using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;



namespace CentrixERP.Business.Entity
{
    [EntityIManager("Centrix.POS.Standalone.Client.Business.IManager.IInvoiceManager,Centrix.POS.Standalone.Client.Business.IManager", isLogicalDelete = true)]
    public class Invoice : EntityBase<Invoice, int>
    {
        #region Constructors

        public Invoice() { }

        public Invoice(int invoiceId)
        {
            this.invoiceId = invoiceId;
        }

        #endregion

        #region Private Properties


        private string customActions;
        private int customerId;
        private int invoiceId;

        private DateTime? invoiceDate;

        private int companyId;

        private int personId;

        private string companyName;

        private string personName;

        private Double totalPrice;

        private Double grandTotal;

        private Double percentageDiscount;

        private int createdBy;

        private int updatedBy;

        private DateTime? updatedDate;

        private DateTime? createdDate;

        private bool? flagDeleted;

        private bool? isSystem;

        private Object tax;

        private string note;

        private Double amountDiscount;

        private int invoiceStatus;

        private int paymentType;

        private int paymentMethod;

        private int storeId;

        private int registerId;

        private string invoiceNumber;

        private Double changeAmount;

        private int parentInvoiceId;

        private int saleManId;

        private string salesManName;

        private bool isLayby;

        private Double downPayment;

        private string laybyNumber;

        private int parentLaybyId;

        private bool isCopied;

        private Double itemsDiscount;

        private bool isSynced;

        private string invoiceUniqueNumber;

        private string priceListCode;

        private bool? isNewInvoice;

        private string transactionTypeName;
        private bool isTaxableInvoice;
        private bool isCanceledLayby;

        private double subTotal;
        private int itemLineCount;
        private int paymentLineCount;
        private int closeRegisterId;
        private string accpacshipmentnum;
        private string accpacorderentrynum;
        private string accpacinvoicenum;
        private string accpacrefundno;

        //private int giftId;



        //private bool? flagDeleted;

        //private bool? isSystem;

        //private int createdBy;

        //private int updatedBy;

        //private DateTime? createdDate;

        //private DateTime? updatedDate;
        #endregion

        #region Public Properties


       
       
        [PrimaryKey("@invoice_id", SqlDbType.Int)]
        [Property("invoice_id", SqlDbType.Int, AddParameter = false)]
        public int InvoiceId
        {
            get
            {
                return invoiceId;
            }
            set
            {
                invoiceId = value;
            }
        }

        [Property("invoice_date", SqlDbType.DateTime,AddParameter=true)]
        //[PropertyConstraint(false, "InvoiceDate")]
        public DateTime? InvoiceDate
        {
            get
            {
                return invoiceDate;
            }
            set
            {
                invoiceDate = value;
            }
        }

       
        //public int GiftId
        //{
        //    get
        //    {
        //        return giftId;
        //    }
        //    set
        //    {
        //        giftId = value;
        //    }
        //}

        [Property("company_id", SqlDbType.Int)]
        [PropertyConstraint(false, "Company")]
        public int CompanyId
        {
            get
            {
                return companyId;
            }
            set
            {
                companyId = value;
            }
        }

        [Property("close_register_id", SqlDbType.Int, AddParameter = true)]
        [PropertyConstraint(false, "CloseRegisterId")]
        public int CloseRegisterId
        {
            get { return closeRegisterId; }
            set { closeRegisterId = value; }
        }


        [Property("customer_id", SqlDbType.Int)]
        //[PropertyConstraint(false, "Company")]
        public int CustomerId
        {
            get
            {
                return customerId;
            }
            set
            {
                customerId = value;
            }
        }


        [Property("custom_actions", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "CustomActions")]
        public string CustomActions
        {
            get
            {
                return customActions;
            }
            set
            {
                customActions = value;
            }
        }

        
        [Property("person_id", SqlDbType.Int)]
        [PropertyConstraint(false, "Person")]
        public int PersonId
        {
            get
            {
                return personId;
            }
            set
            {
                personId = value;
            }
        }

        [Property("company_name", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "CompanyName", MaxLength = 100, MinLength = 3)]
        public string CompanyName
        {
            get
            {
                return companyName;
            }
            set
            {
                companyName = value;
            }
        }

        [Property("person_name", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "PersonName", MaxLength = 100, MinLength = 3)]
        public string PersonName
        {
            get
            {
                return personName;
            }
            set
            {
                personName = value;
            }
        }

        [Property("total_price", SqlDbType.Money)]
        [PropertyConstraint(false, "TotalPrice")]
        public Double TotalPrice
        {
            get
            {
                return totalPrice;
            }
            set
            {
                totalPrice = value;
            }
        }

        [Property("grand_total", SqlDbType.Money)]
        [PropertyConstraint(false, "GrandTotal")]
        public Double GrandTotal
        {
            get
            {
                return grandTotal;
            }
            set
            {
                grandTotal = value;
            }
        }

        [Property("percentage_discount", SqlDbType.Money)]
        [PropertyConstraint(false, "PercentageDiscount")]
        public Double PercentageDiscount
        {
            get
            {
                return percentageDiscount;
            }
            set
            {
                percentageDiscount = value;
            }
        }

        [Property("tax", SqlDbType.Money)]
        [PropertyConstraint(false, "Tax")]
        public Object Tax
        {
            get
            {
                return tax;
            }
            set
            {
                tax = value;
            }
        }

        [Property("note", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "Note", MaxLength = 100, MinLength = 3)]
        public string Note
        {
            get
            {
                return note;
            }
            set
            {
                note = value;
            }
        }

        [Property("amount_discount", SqlDbType.Money)]
        [PropertyConstraint(false, "AmountDiscount")]
        public Double AmountDiscount
        {
            get
            {
                return amountDiscount;
            }
            set
            {
                amountDiscount = value;
            }
        }

        [Property("invoice_status", SqlDbType.Int)]
        [PropertyConstraint(false, "InvoiceStatus")]
        public int Status
        {
            get
            {
                return invoiceStatus;
            }
            set
            {
                invoiceStatus = value;
            }
        }

        [Property("payment_type", SqlDbType.Int)]
        [PropertyConstraint(false, "PaymentType")]
        public int PaymentType
        {
            get
            {
                return paymentType;
            }
            set
            {
                paymentType = value;
            }
        }

        [Property("payment_method", SqlDbType.Int)]
        [PropertyConstraint(false, "PaymentMethod")]
        public int PaymentMethod
        {
            get
            {
                return paymentMethod;
            }
            set
            {
                paymentMethod = value;
            }
        }

        [Property("store_id", SqlDbType.Int)]
        [PropertyConstraint(false, "Store")]
        public int StoreId
        {
            get
            {
                return storeId;
            }
            set
            {
                storeId = value;
            }
        }

        [Property("register_id", SqlDbType.Int)]
        [PropertyConstraint(false, "Register")]
        public int RegisterId
        {
            get
            {
                return registerId;
            }
            set
            {
                registerId = value;
            }
        }

        [Property("invoice_number", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "InvoiceNumber", MaxLength = 100, MinLength = 3)]
        public string InvoiceNumber
        {
            get
            {
                return invoiceNumber;
            }
            set
            {
                invoiceNumber = value;
            }
        }

        [Property("change_amount", SqlDbType.Money)]
        [PropertyConstraint(false, "ChangeAmount")]
        public Double ChangeAmount
        {
            get
            {
                return changeAmount;
            }
            set
            {
                changeAmount = value;
            }
        }

        [Property("parent_invoice_id", SqlDbType.Int)]
        [PropertyConstraint(false, "ParentInvoice")]
        public int ParentInvoiceId
        {
            get
            {
                return parentInvoiceId;
            }
            set
            {
                parentInvoiceId = value;
            }
        }

        [Property("sale_man_id", SqlDbType.Int)]
        [PropertyConstraint(false, "SalesManId")]
        public int SalesManId
        {
            get
            {
                return saleManId;
            }
            set
            {
                saleManId = value;
            }
        }

        [Property("sales_man_name", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "SalesManName", MaxLength = 100, MinLength = 3)]
        public string SalesManName
        {
            get
            {
                return salesManName;
            }
            set
            {
                salesManName = value;
            }
        }

        [Property("is_layby", SqlDbType.Bit)]
        [PropertyConstraint(false, "IsLayby")]
        public bool IsLayby
        {
            get
            {
                return isLayby;
            }
            set
            {
                isLayby = value;
            }
        }

        [Property("down_payment", SqlDbType.Money)]
        [PropertyConstraint(false, "DownPayment")]
        public Double DownPayment
        {
            get
            {
                return downPayment;
            }
            set
            {
                downPayment = value;
            }
        }


        [Property("accpac_shipment_no", SqlDbType.NVarChar)]

        public string AccpacShipmentNum
        {
            get
            {
                return accpacshipmentnum;
            }
            set
            {
                accpacshipmentnum = value;
            }
        }

        [Property("accpac_orderentry_no", SqlDbType.NVarChar)]

        public string AccpacOrderentryNum
        {
            get
            {
                return accpacorderentrynum;
            }
            set
            {
                accpacorderentrynum = value;
            }
        }


        [Property("accpac_invoice_no", SqlDbType.NVarChar)]

        public string AccpacInvoiceNum
        {
            get
            {
                return accpacinvoicenum;
            }
            set
            {
                accpacinvoicenum = value;
            }
        }


        [Property("accpac_refund_no", SqlDbType.NVarChar)]

        public string AccpacRefundNo
        {
            get
            {
                return accpacrefundno;
            }
            set
            {
                accpacrefundno = value;
            }
        }



        [Property("layby_number", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "LaybyNumber", MaxLength = 100, MinLength = 3)]
        public string LaybyNumber
        {
            get
            {
                return laybyNumber;
            }
            set
            {
                laybyNumber = value;
            }
        }

        [Property("parent_layby_id", SqlDbType.Int)]
        [PropertyConstraint(false, "ParentLayby")]
        public int ParentLaybyId
        {
            get
            {
                return parentLaybyId;
            }
            set
            {
                parentLaybyId = value;
            }
        }

        [Property("is_copied", SqlDbType.Bit,AddParameter=false,UpdateParameter=false)]
        [PropertyConstraint(false, "IsCopied")]
        public bool IsCopied
        {
            get
            {
                return isCopied;
            }
            set
            {
                isCopied = value;
            }
        }

        [Property("items_discount", SqlDbType.Money)]
        [PropertyConstraint(false, "ItemsDiscount")]
        public Double ItemsDiscount
        {
            get
            {
                return itemsDiscount;
            }
            set
            {
                itemsDiscount = value;
            }
        }



        [Property("sub_total", SqlDbType.Money, FindParameter = false, FindByIdLiteParameter = false, FindAllLiteParameter = false)]
        [PropertyConstraint(false, "SubTotal")]
        public double SubTotal
        {
            get
            {
                return subTotal;
            }
            set
            {
                subTotal = value;
            }
        }

        [Property("item_line_counts", SqlDbType.Int, FindParameter = false, FindByIdLiteParameter = false, FindAllLiteParameter = false)]
        [PropertyConstraint(false, "ItemLineCounts")]
        public int ItemLineCounts
        {
            get
            {
                return itemLineCount;
            }
            set
            {
                itemLineCount = value;
            }
        }

        [Property("payment_line_counts", SqlDbType.Int, FindParameter = false, FindByIdLiteParameter = false, FindAllLiteParameter = false)]
        [PropertyConstraint(false, "PaymentLineCounts")]
        public int PaymentLineCounts
        {
            get
            {
                return paymentLineCount;
            }
            set
            {
                paymentLineCount = value;
            }
        }

        [Property("is_synced", SqlDbType.Bit,AddParameter=true,UpdateParameter=true)]
        [PropertyConstraint(false, "IsSynced")]
        public bool IsSynced
        {
            get
            {
                return isSynced;
            }
            set
            {
                isSynced = value;
            }
        }

        [Property("invoice_unique_number", SqlDbType.NVarChar, AddParameter = true, UpdateParameter = true)]
        [PropertyConstraint(false, "InvoiceUniqueNumber", MaxLength = 4000, MinLength = 3)]
        public string InvoiceUniqueNumber
        {
            get
            {
                return invoiceUniqueNumber;
            }
            set
            {
                invoiceUniqueNumber = value;
            }
        }

        [Property("price_list_code", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "PriceListCode", MaxLength = 100, MinLength = 3)]
        public string PriceListCode
        {
            get
            {
                return priceListCode;
            }
            set
            {
                priceListCode = value;
            }
        }
        [Property("transaction_type_name", SqlDbType.NVarChar, AddParameter = false, UpdateParameter = false)]
        [PropertyConstraint(false, "TransactionTypeName", MaxLength = 100, MinLength = 3)]
        public string TransactionTypeName
        {
            get
            {
                return transactionTypeName;
            }
            set
            {
                transactionTypeName = value;
            }
        }

    
        [Property("is_new", SqlDbType.Bit,AddParameter=false,UpdateParameter=false)]
        [PropertyConstraint(false, "IsNew")]
        public bool? IsNewInvoice
        {
            get
            {
                return isNewInvoice;
            }
            set
            {
                isNewInvoice = value;
            }
        }



        [Property("flag_deleted", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool? FlagDeleted
        {
            get
            {
                return flagDeleted;
            }
            set
            {
                flagDeleted = value;
            }
        }

        [Property("is_system", SqlDbType.Bit, AddParameter = false, UpdateParameter = false, FindParameter = false)]
        public bool? IsSystem
        {
            get
            {
                return isSystem;
            }
            set
            {
                isSystem = value;
            }
        }

        [Property("created_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? CreatedDate
        {
            get
            {
                return createdDate;
            }
            set
            {
                createdDate = value;
            }
        }

        [Property("updated_date", SqlDbType.DateTime, AddParameter = false, UpdateParameter = false)]
        public DateTime? UpdatedDate
        {
            get
            {
                return updatedDate;
            }
            set
            {
                updatedDate = value;
            }
        }

        [Property("created_by", SqlDbType.Int, AddParameter = true, UpdateParameter = false)]
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        [Property("updated_by", SqlDbType.Int, AddParameter = false, UpdateParameter = true)]
        public int UpdatedBy
        {
            get
            {
                return updatedBy;
            }
            set
            {
                updatedBy = value;
            }
        }

        [Property("taxable_invoice", SqlDbType.Int, AddParameter = true, UpdateParameter = true)]
        public bool IsTaxableInvoice
        {
            get
            {
                return isTaxableInvoice;
            }
            set
            {
                isTaxableInvoice = value;
            }
        }


        [Property("canceled_layby", SqlDbType.Int, AddParameter = true, UpdateParameter = true)]
        public bool IsCanceledLayBy
        {
            get
            {
                return isCanceledLayby;
            }
            set
            {
                isCanceledLayby = value;
            }
        }
        #endregion

        #region Composite Objects

        //private Company companyObj;
        //[CompositeObject(CompositObjectTypeEnum.Single, "CompanyId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable = false)]
        //public Company CompanyObj
        //{
        //    get
        //    {
        //        if (companyObj == null)
        //            loadCompositObject_Lazy("CompanyObj");
        //        return companyObj;
        //    }
        //    set
        //    {
        //        companyObj = value; ;
        //    }
        //}

        //private Person personObj;
        //[CompositeObject(CompositObjectTypeEnum.Single, "PersonId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true, Savable = false)]
        //public Person PersonObj
        //{
        //    get
        //    {
        //        if (personObj == null)
        //            loadCompositObject_Lazy("PersonObj");
        //        return personObj;
        //    }
        //    set
        //    {
        //        personObj = value; ;
        //    }
        //}


        private List<InvoiceItem> invoiceItemList;
        [CompositeObject(CompositObjectTypeEnum.List, "InvoiceId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true)]
        public List<InvoiceItem> InvoiceItemList
        {
            get
            {
                if (invoiceItemList == null)
                    loadCompositObject_Lazy("InvoiceItemList");
                return invoiceItemList;
            }
            set
            {
                invoiceItemList = value;
            }
        }

        private List<InvoicePayments> invoicePaymentList;
        [CompositeObject(CompositObjectTypeEnum.List, "InvoiceId", SaveBeforeParent = false, LazyLoad = true, CascadeDelete = true)]
        public List<InvoicePayments> InvoicePaymentList
        {
            get
            {
                if (invoicePaymentList == null)
                    loadCompositObject_Lazy("InvoicePaymentList");
                return invoicePaymentList;
            }
            set { invoicePaymentList = value; }
        }
        #endregion


        public override int GetIdentity()
        {
            return invoiceId;
        }
        public override void SetIdentity(int value)
        {
            invoiceId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            throw new NotImplementedException();
        }

       
    }

}