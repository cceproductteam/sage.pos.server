using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;
using CentrixERP.Business.Entity;

namespace CentrixERP.Business.Entity
{
    [EntityIManager("Centrix.POS.POS.Business.IManager.IInvoiceCustomFeildsManager,Centrix.Business.IManager", isLogicalDelete = true)]
    public class InvoiceCustomFeilds : EntityBase<InvoiceCustomFeilds, int>
    {
        #region Constructors

        public InvoiceCustomFeilds() { }

        public InvoiceCustomFeilds(int customFieldId)
        {
            this.customFieldId = customFieldId;
        }

        #endregion

        #region Private Properties

        private int customFieldId;

        private string customFieldName;

        private string customFieldValue;

        private string invoiceUniqueNumber;

        private string itemNumber;


        #endregion

        #region Public Properties

        [PrimaryKey("@custom_field_id", SqlDbType.Int)]
        [Property("custom_field_id", SqlDbType.Int, AddParameter = false)]
        public int CustomFieldId
        {
            get
            {
                return customFieldId;
            }
            set
            {
                customFieldId = value;
            }
        }

        [Property("custom_field_name", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "CustomFieldName", MaxLength = 100, MinLength = 3)]
        public string CustomFieldName
        {
            get
            {
                return customFieldName;
            }
            set
            {
                customFieldName = value;
            }
        }

        [Property("custom_field_value", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "CustomFieldValue", MaxLength = 100, MinLength = 3)]
        public string CustomFieldValue
        {
            get
            {
                return customFieldValue;
            }
            set
            {
                customFieldValue = value;
            }
        }

        [Property("invoice_unique_number", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "InvoiceUniqueNumber", MaxLength = 100, MinLength = 3)]
        public string InvoiceUniqueNumber
        {
            get
            {
                return invoiceUniqueNumber;
            }
            set
            {
                invoiceUniqueNumber = value;
            }
        }

        [Property("item_number", SqlDbType.NVarChar)]
        [PropertyConstraint(false, "ItemNumber", MaxLength = 100, MinLength = 3)]
        public string ItemNumber
        {
            get
            {
                return itemNumber;
            }
            set
            {
                itemNumber = value;
            }
        }



       
    

       

      
      

       
        #endregion

        #region Composite Objects


        #endregion


        public override int GetIdentity()
        {
            return customFieldId;
        }
        public override void SetIdentity(int value)
        {
            customFieldId = value;
        }
        public override void SetParentId(object value, Type ParentType)
        {
            throw new NotImplementedException();
        }
    }
}