using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using System.Data;

namespace CentrixERP.Business.Entity
{
     public class InvoiceCustomFeildsLite : EntityBaseLite
    {      
        
        #region Public Properties

         		[LiteProperty("custom_field_id",SqlDbType.Int)]
		public int CustomPrimaryKeyId
		{
			get;
			set;
		}

		[LiteProperty("custom_field_name",SqlDbType.NVarChar)]
		public string CustomFieldName
		{
			get;
			set;
		}

		[LiteProperty("custom_field_value",SqlDbType.NVarChar)]
		public string CustomFieldValue
		{
			get;
			set;
		}

		[LiteProperty("invoice_unique_number",SqlDbType.NVarChar)]
		public string InvoiceUniqueNumber
		{
			get;
			set;
		}

		[LiteProperty("item_number",SqlDbType.NVarChar)]
		public string ItemNumber
		{
			get;
			set;
		}



       
        public int value
        {
            get{ return Id;}
        }

        
        public string label
        {
            get { return "" ;}
        }

         [LiteProperty("flag_deleted", SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
        public bool? FlagDeleted
        {
            get;
            set;
        }

        [LiteProperty("is_system",SqlDbType.Bit, FindAllLiteParameter = false, FindByIdLiteParameter = false)]
		public bool? IsSystem
		{
			get;
			set;
		}

		[LiteProperty("created_date",SqlDbType.DateTime)]
		public DateTime? CreatedDate
		{
			get;
			set;
		}

		[LiteProperty("updated_date",SqlDbType.DateTime)]
		public DateTime? UpdatedDate
		{
			get;
			set;
		}

		[LiteProperty("created_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int CreatedBy
		{
			get;
			set;
		}

		[LiteProperty("updated_by",SqlDbType.Int, FindByIdLiteParameter = false)]
		public int UpdatedBy
		{
			get;
			set;
		}

         [LiteProperty("created_by_name",SqlDbType.NVarChar, FindAllLiteParameter = false)]
         public string CreatedByName{get;set;}

         [LiteProperty("updated_by_name",SqlDbType.NVarChar,FindAllLiteParameter = false)]
         public string UpdatedByName{get;set;}

        
          public int Id
        {
            get{return this.CustomPrimaryKeyId; }
        }

         [LiteProperty("last_rows",SqlDbType.Bit)]
         public bool LastRows { get; set; }

         [LiteProperty("total_count",SqlDbType.Int)]
         public int TotalRecords { get; set; }
        #endregion
       
          
     }
}