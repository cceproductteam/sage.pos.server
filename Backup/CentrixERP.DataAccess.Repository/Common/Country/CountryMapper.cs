using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.Framework.Data;
using CentrixERP.Common.Business.Entity;
using CentrixERP.Common.DataAccess.IRepository;
using System.Data.SqlClient;

namespace CentrixERP.Common.DataAccess.Repository
{

    public partial class CountryRepository
    {
        public Country GetMapper(SqlDataReader reader)
        {
            Country myCountry = new Country();
            if (reader["country_id"] != DBNull.Value)
            {
                myCountry.CountryId = (int)reader["country_id"];
            }
            if (reader["country_name_en"] != DBNull.Value)
            {
                myCountry.CountryNameEn = reader["country_name_en"].ToString();
            }
            if (reader["country_name_ar"] != DBNull.Value)
            {
                myCountry.CountryNameAr = reader["country_name_ar"].ToString();
            }
            if (reader["country_code"] != DBNull.Value)
            {
                myCountry.CountryCode = reader["country_code"].ToString();
            }
            if (reader["country_name_code"] != DBNull.Value)
            {
                myCountry.CountryNameCode = reader["country_name_code"].ToString();
            }
            
            return myCountry;
        }
    }
}