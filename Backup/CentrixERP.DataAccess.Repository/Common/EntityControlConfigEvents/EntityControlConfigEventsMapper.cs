using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CentrixERP.Common.Business.Entity;
using SF.Framework.Data;
using System.Data.SqlClient;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class EntityControlConfigEventsRepository
    {
        public EntityControlConfigEvents GetMapper(SqlDataReader reader)
        {
            EntityControlConfigEvents obj = new EntityControlConfigEvents();
			if (reader["entity_control_config_event_id"] != DBNull.Value)
			{
				obj.EntityControlConfigEventId = (int)reader["entity_control_config_event_id"];
			}
			if (reader["entity_Id"] != DBNull.Value)
			{
				obj.EntityId = (int)reader["entity_Id"];
			}
			if (reader["entity_control_config_id"] != DBNull.Value)
			{
				obj.EntityControlConfigId = (int)reader["entity_control_config_id"];
			}
			if (reader["control_event_id"] != DBNull.Value)
			{
				obj.ControlEventId = (int)reader["control_event_id"];
			}
			if (reader["js_function_name"] != DBNull.Value)
			{
				obj.JsFunctionName = (string)reader["js_function_name"];
			}
			if (reader["js_function_body"] != DBNull.Value)
			{
				obj.JsFunctionBody = (string)reader["js_function_body"];
			}
			if (reader["created_by"] != DBNull.Value)
			{
				obj.CreatedBy = (int)reader["created_by"];
			}
			if (reader["updated_by"] != DBNull.Value)
			{
				obj.UpdatedBy = (int)reader["updated_by"];
			}
			if (reader["created_date"] != DBNull.Value)
			{
				obj.CreatedDate = (DateTime)reader["created_date"];
			}
			if (reader["updated_date"] != DBNull.Value)
			{
				obj.UpdatedDate = (DateTime)reader["updated_date"];
			}

            return obj;
        }

        public EntityControlConfigEvents_Lite GetLiteMapper(SqlDataReader reader)
        {
            EntityControlConfigEvents_Lite obj = new EntityControlConfigEvents_Lite();
            if (reader["entity_control_config_event_id"] != DBNull.Value)
            {
                obj.EntityControlConfigEventId = (int)reader["entity_control_config_event_id"];
            }
            if (reader["entity_Id"] != DBNull.Value)
            {
                obj.EntityId = (int)reader["entity_Id"];
            }
            if (reader["entity_control_config_id"] != DBNull.Value)
            {
                obj.EntityControlConfigId = (int)reader["entity_control_config_id"];
            }
            if (reader["control_event_id"] != DBNull.Value)
            {
                obj.ControlEventId = (int)reader["control_event_id"];
            }
            if (reader["js_function_name"] != DBNull.Value)
            {
                obj.JsFunctionName = (string)reader["js_function_name"];
            }
            if (reader["js_function_body"] != DBNull.Value)
            {
                obj.JsFunctionBody = (string)reader["js_function_body"];
            }
            if (reader["created_by"] != DBNull.Value)
            {
                obj.CreatedBy = (int)reader["created_by"];
            }
            if (reader["updated_by"] != DBNull.Value)
            {
                obj.UpdatedBy = (int)reader["updated_by"];
            }
            if (reader["created_date"] != DBNull.Value)
            {
                obj.CreatedDate = (DateTime)reader["created_date"];
            }
            if (reader["updated_date"] != DBNull.Value)
            {
                obj.UpdatedDate = (DateTime)reader["updated_date"];
            }
            if (reader["control_id"] != DBNull.Value)
            {
                obj.ControlId = (int)reader["control_id"];
            }
            if (reader["event_name"] != DBNull.Value)
            {
                obj.EventName = (string)reader["event_name"];
            }
            if (reader["entity_name"] != DBNull.Value)
            {
                obj.EntityName = (string)reader["entity_name"];
            }


            if (reader["property_name_en"] != DBNull.Value)
            {
                obj.ControlPropertyName = (string)reader["property_name_en"];
            }


            return obj;
        }
    }
}
