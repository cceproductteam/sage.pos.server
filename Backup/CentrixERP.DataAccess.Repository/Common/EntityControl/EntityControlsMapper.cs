using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CentrixERP.Common.Business.Entity;
using SF.Framework.Data;
using System.Data.SqlClient;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class EntityControlsRepository
    {
        public EntityControls GetMapper(SqlDataReader reader)
        {
            EntityControls obj = new EntityControls();
			if (reader["entity_control_id"] != DBNull.Value)
			{
				obj.EntityControlId = (int)reader["entity_control_id"];
			}
			if (reader["entity_id"] != DBNull.Value)
			{
				obj.EntityId = (int)reader["entity_id"];
			}
			if (reader["ui_name_en"] != DBNull.Value)
			{
				obj.UiNameEn = (string)reader["ui_name_en"];
			}
			if (reader["ui_name_ar"] != DBNull.Value)
			{
				obj.UiNameAr = (string)reader["ui_name_ar"];
			}
			if (reader["ui_value"] != DBNull.Value)
			{
				obj.UiValue = (string)reader["ui_value"];
			}
			if (reader["db_column_name_en"] != DBNull.Value)
			{
				obj.DbColumnNameEn = (string)reader["db_column_name_en"];
			}
			if (reader["db_column_name_ar"] != DBNull.Value)
			{
				obj.DbColumnNameAr = (string)reader["db_column_name_ar"];
			}
			if (reader["service_name"] != DBNull.Value)
			{
				obj.ServiceName = (string)reader["service_name"];
			}
			if (reader["service_method"] != DBNull.Value)
			{
				obj.ServiceMethod = (string)reader["service_method"];
			}
			if (reader["method_args"] != DBNull.Value)
			{
				obj.MethodArgs = (string)reader["method_args"];
			}
			if (reader["method_args_values"] != DBNull.Value)
			{
				obj.MethodArgsValues = (string)reader["method_args_values"];
			}
			if (reader["child_AC"] != DBNull.Value)
			{
				obj.ChildAc = (string)reader["child_AC"];
			}
			if (reader["child_AC_args"] != DBNull.Value)
			{
				obj.ChildAcArgs = (string)reader["child_AC_args"];
			}
			if (reader["control_id"] != DBNull.Value)
			{
				obj.ControlId = (int)reader["control_id"];
			}
			if (reader["property_parent_id"] != DBNull.Value)
			{
				obj.PropertyParentId = (int)reader["property_parent_id"];
			}
			if (reader["property_name_en"] != DBNull.Value)
			{
				obj.PropertyNameEn = (string)reader["property_name_en"];
			}
			if (reader["property_name_ar"] != DBNull.Value)
			{
				obj.PropertyNameAr = (string)reader["property_name_ar"];
			}
			if (reader["data_type_id"] != DBNull.Value)
			{
				obj.DataTypeId = (int)reader["data_type_id"];
			}
			if (reader["data_type_table_name"] != DBNull.Value)
			{
				obj.DataTypeTableName = (string)reader["data_type_table_name"];
			}
			if (reader["value_type_id"] != DBNull.Value)
			{
				obj.ValueTypeId = (int)reader["value_type_id"];
			}
			if (reader["value_type"] != DBNull.Value)
			{
				obj.ValueType = (string)reader["value_type"];
			}
			if (reader["created_by"] != DBNull.Value)
			{
				obj.CreatedBy = (int)reader["created_by"];
			}
			if (reader["updated_by"] != DBNull.Value)
			{
				obj.UpdatedBy = (int)reader["updated_by"];
			}
			if (reader["created_date"] != DBNull.Value)
			{
				obj.CreatedDate = (DateTime)reader["created_date"];
			}
			if (reader["updated_date"] != DBNull.Value)
			{
				obj.UpdatedDate = (DateTime)reader["updated_date"];
			}
			if (reader["entity_referance_id"] != DBNull.Value)
			{
				obj.EntityReferanceId = (int)reader["entity_referance_id"];
			}

            return obj;
        }
    }
}
