using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CentrixERP.Common.Business.Entity;
using System.Data.SqlClient;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class DuplicationRuleFilterTypeRepository
    {
        private DuplicationRuleFilterType GetDuplicationRuleFilterMapper(SqlDataReader reader)
        {
            DuplicationRuleFilterType myEntity = new DuplicationRuleFilterType();
            if (reader["duplication_filter_type_id"] != DBNull.Value)
            {
                myEntity.DuplicationFilterTypeId = (int)reader["duplication_filter_type_id"];
            }
            if (reader["filter_type"] != DBNull.Value)
            {
                myEntity.DuplicationFilterType = reader["filter_type"].ToString();
            }
            if (reader["filter_type_ar"] != DBNull.Value)
            {
                myEntity.DuplicationFilterTypeAr = reader["filter_type_ar"].ToString();
            }
            
            return myEntity;
        }
    }
}
