using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CentrixERP.Common.Business.Entity;
using SF.Framework.Data;
using System.Data.SqlClient;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class DocumentEntityRepository
    {
        public DocumentEntity GetDocumentEntityData(SqlDataReader reader)
        {
            DocumentEntity obj = new DocumentEntity();
            if (reader["document_entity_id"] != DBNull.Value)
            {
                obj.DocumentEntityId = (int)reader["document_entity_id"];
            }
            if (reader["document_id"] != DBNull.Value)
            {
                obj.DocumentId = (int)reader["document_id"];
            }
            if (reader["entity_id"] != DBNull.Value)
            {
                obj.EntityId = (int)reader["entity_id"];
            }
            if (reader["entity_value_id"] != DBNull.Value)
            {
                obj.EntityValueId = (int)reader["entity_value_id"];
            }
            return obj;
        }
    }
}
