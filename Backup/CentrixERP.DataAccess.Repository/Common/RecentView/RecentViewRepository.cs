using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CentrixERP.Common.DataAccess.IRepository;
using SF.Framework.Data;
using System.Data.SqlClient;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class RecentViewRepository : IRecentViewRepository
    {
        #region IRepository<RecentView,int> Members

        public void Add(ref CentrixERP.Common.Business.Entity.RecentView myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
             Instance.AddInParameter("@id", System.Data.SqlDbType.Int, myEntity.Id);
             Instance.AddInParameter("@user_id", System.Data.SqlDbType.Int, myEntity.UserId);
             Instance.AddInParameter("@entity_id", System.Data.SqlDbType.Int, myEntity.EntityId);
             Instance.AddOutParameter("@recently_viewedId", System.Data.SqlDbType.Int);
            try
            {
                Instance.ExcuteNonQuery("SP_EntityOrder_update", conn, false);
                myEntity.RecentlyViewedId = (int)Instance.GetOutParamValue("@recently_viewedId");
            }
            catch (Exception ex)
            {
                throw ex;
            }
          
        }

        public void Delete(ref CentrixERP.Common.Business.Entity.RecentView myEntity)
        {
            throw new NotImplementedException();
        }

        public void DeleteLogical(ref CentrixERP.Common.Business.Entity.RecentView myEntity)
        {
            throw new NotImplementedException();
        }

        public List<CentrixERP.Common.Business.Entity.RecentView> FindAll(SF.FrameworkEntity.CriteriaBase myCriteria)
        {
            throw new NotImplementedException();
        }

        public CentrixERP.Common.Business.Entity.RecentView FindById(int Id, SF.FrameworkEntity.CriteriaBase myCriteria)
        {
            throw new NotImplementedException();
        }

        public List<CentrixERP.Common.Business.Entity.RecentView> FindByParentId<TParentId>(TParentId ParentId, Type ParentType, SF.FrameworkEntity.CriteriaBase myCriteria)
        {
            throw new NotImplementedException();
        }

        public void Update(ref CentrixERP.Common.Business.Entity.RecentView myEntity)
        {
            throw new NotImplementedException();
        }

        public string CustomConnectionString
        {
            get;
            set;
        }

        public bool UseCustomConnectionString
        {
            get;
            set;
        }
        public Object BeginTransaction()
        {
            return new object();
        }


        public void CommitTransaction(Object object1)
        {
        }

        public void Rollback(Object object1)
        {

        }

        public void KillConnection()
        {

        }


        public bool UseSharedSession { get; set; }

        #endregion
    }
}
