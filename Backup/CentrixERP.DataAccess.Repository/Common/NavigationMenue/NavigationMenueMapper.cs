using System;
using System.Collections.Generic;
using System.Linq;
using SF.FrameworkEntity;
using CentrixERP.Common.Business.Entity;
using System.Data.SqlClient;

using System.Text;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class NavigationMenueRepository
    {
       public NavigationMenue GetMapper(SqlDataReader reader)
       {
           NavigationMenue myMenue = new NavigationMenue();
           if (reader["menu_id"] != DBNull.Value)
           {
               myMenue.MenueId = (int)reader["menu_id"];
           }
           if (reader["menu_link"] != DBNull.Value)
           {
               myMenue.Link = reader["menu_link"].ToString();
           }
           if (reader["menu_img"] != DBNull.Value)
           {
               myMenue.Image = reader["menu_img"].ToString();
           }
           if (reader["menu_name"] != DBNull.Value)
           {
               myMenue.NameEnglish = reader["menu_name"].ToString(); ;
           }
           if (reader["menu_name_ar"] != DBNull.Value)
           {
               myMenue.NameArabic = reader["menu_name_ar"].ToString();
           }
           if (reader["menu_order"] != DBNull.Value)
           {
               myMenue.Order = (int)reader["menu_order"];
           }

           if (reader["menu_category"] != DBNull.Value)
           {
               myMenue.Category = (int)reader["menu_category"];
           }


           if (reader["css_class"] != DBNull.Value)
           {
               myMenue.Css = reader["css_class"].ToString(); ;
           }
           if (reader["permission_id"] != DBNull.Value)
           {
               myMenue.PermissionId = (int)reader["permission_id"];
           }

           return myMenue;
       }
    }
}
