using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Common.DataAccess.IRepository;
using SF.Framework;
using SF.Framework.Data;
using CentrixERP.Common.Business.Entity;
using System.Data.SqlClient;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class NavigationMenueRepository:INavigationMenueRepository
    {
        #region IRepository<NavigationMenue,int> Members

        public void Add(ref NavigationMenue myEntity)
        {
             SQLDAHelper Instance = new SQLDAHelper(); 
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@MenuName", System.Data.SqlDbType.NVarChar, myEntity.NameEnglish);
            Instance.AddInParameter("@MenuNameAr", System.Data.SqlDbType.NVarChar, myEntity.NameArabic);
            Instance.AddInParameter("@MenuLink", System.Data.SqlDbType.NVarChar, myEntity.Link);
            Instance.AddInParameter("@MenuImg", System.Data.SqlDbType.NVarChar, myEntity.Image);
            Instance.AddInParameter("@CssClass", System.Data.SqlDbType.NVarChar, myEntity.Css);
            Instance.AddInParameter("@MenuOrder", System.Data.SqlDbType.Int, myEntity.Order);
            Instance.AddInParameter("@MenuCategory", System.Data.SqlDbType.Int, myEntity.Category);
            Instance.AddInParameter("@PermissionID", System.Data.SqlDbType.Int, myEntity.PermissionId);
          
            Instance.AddOutParameter("@Id", System.Data.SqlDbType.Int);

            try
            {
                Instance.ExcuteNonQuery("dbo.SP_NavigationMenue_Add", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            myEntity.MenueId = (int)Instance.GetOutParamValue("@Id");
        }

        public void Delete(ref NavigationMenue myEntity)
        {
             SQLDAHelper Instance = new SQLDAHelper(); 
             SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@Id", System.Data.SqlDbType.Int, myEntity.MenueId);
            try
            {
                Instance.ExcuteNonQuery("dbo.SP_NavigationMenue_Delete", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteLogical(ref NavigationMenue myEntity)
        {
             SQLDAHelper Instance = new SQLDAHelper(); 
 SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@Id", System.Data.SqlDbType.Int, myEntity.MenueId);
            try
            {
                Instance.ExcuteNonQuery("dbo.SP_NavigationMenue_DeleteLogical", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<NavigationMenue> FindAll(CriteriaBase myCriteria)
        {
            List<NavigationMenue> Menues = new List<NavigationMenue>();
             SQLDAHelper Instance = new SQLDAHelper(); 
             SqlConnection conn = Instance.GetConnection(false);
             if (myCriteria != null)
             {
                 NavigationMenueCriteria criteria = (NavigationMenueCriteria)myCriteria;
                 Instance.AddInParameter("@Name", System.Data.SqlDbType.NVarChar, criteria.Name);
                 Instance.AddInParameter("@UserId", System.Data.SqlDbType.NVarChar, SQLDAHelper.Convert_IntTODB(criteria.UserId));
                 Instance.AddInParameter("@MenuCategory", System.Data.SqlDbType.NVarChar, SQLDAHelper.Convert_IntTODB(criteria.Category));
                 if (criteria.RoleId.HasValue)
                     Instance.AddInParameter("@role_id", System.Data.SqlDbType.NVarChar, SQLDAHelper.Convert_IntTODB(criteria.RoleId.Value));
             }

            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_NavigationMenue_FindAll", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    Menues.Add(GetMapper(reader));
                }
                reader.Close();
                return Menues;
            }
            else
            {
                return null;
            }
        }

        public NavigationMenue FindById(int Id, CriteriaBase myCriteria)
        {
             SQLDAHelper Instance = new SQLDAHelper(); 
              SqlConnection conn = Instance.GetConnection(false);
            if (myCriteria != null)
            {
                //Instance.AddInParameter("@Name", System.Data.SqlDbType.Int, ((RoleCriteria)myCriteria).Name);
            }
            Instance.AddInParameter("@Id", System.Data.SqlDbType.Int, Id);
            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_NavigationMenu_FindById", conn);
            if (reader != null && reader.HasRows)
            {
                reader.Read();
                NavigationMenue myMenue = GetMapper(reader);
                reader.Close();
                return myMenue;
            }
            else
            {
                return null;
            }
        }

        public List<NavigationMenue> FindByParentId<TParentId>(TParentId ParentId, Type ParentType, CriteriaBase myCriteria)
        {
            List<NavigationMenue> Menues = new List<NavigationMenue>();
             SQLDAHelper Instance = new SQLDAHelper(); 
            SqlConnection conn = Instance.GetConnection(false);
            if (myCriteria != null)
            {
                //Instance.AddInParameter("@Name", System.Data.SqlDbType.NVarChar, ((RoleCriteria)myCriteria).Name);
            }
            Instance.AddInParameter("@PermissionId", System.Data.SqlDbType.Int, ParentId);
            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_NavigationMenue_FindByParentId", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    Menues.Add(GetMapper(reader));
                }
                reader.Close();
                return Menues;
            }
            else
            {
                return null;
            }
        }

        public void Update(ref NavigationMenue myEntity)
        {
             SQLDAHelper Instance = new SQLDAHelper(); 
 SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@MenuName", System.Data.SqlDbType.NVarChar, myEntity.NameEnglish);
            Instance.AddInParameter("@MenuNameAr", System.Data.SqlDbType.NVarChar, myEntity.NameArabic);
            Instance.AddInParameter("@MenuLink", System.Data.SqlDbType.NVarChar, myEntity.Link);
            Instance.AddInParameter("@MenuImg", System.Data.SqlDbType.NVarChar, myEntity.Image);
            Instance.AddInParameter("@CssClass", System.Data.SqlDbType.NVarChar, myEntity.Css);
            Instance.AddInParameter("@MenuOrder", System.Data.SqlDbType.Int, myEntity.Order);
            Instance.AddInParameter("@MenuCategory", System.Data.SqlDbType.Int, myEntity.Category);
            Instance.AddInParameter("@PermissionID", System.Data.SqlDbType.Int, myEntity.PermissionId);
            Instance.AddInParameter("@Id", System.Data.SqlDbType.Int, myEntity.MenueId);

            try
            {
                Instance.ExcuteNonQuery("dbo.SP_NavigationMenue_Update", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }           
        }


        public string CustomConnectionString
        {
            get;
            set;
        }

        public bool UseCustomConnectionString
        {
            get;
            set;
        }
        public Object BeginTransaction()
        {
            return new object();
        }


        public void CommitTransaction(Object object1)
        {
        }

        public void Rollback(Object object1)
        {

        }

        public void KillConnection()
        {

        }


        public bool UseSharedSession { get; set; }
        #endregion

        #region INavigationMenueRepository Members

        public void reOrder(string Ids)
        {
             SQLDAHelper Instance = new SQLDAHelper(); 
 SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@Ids", System.Data.SqlDbType.NVarChar, Ids);
            try
            {
                Instance.ExcuteNonQuery("dbo.SP_NavigationMenue_reOrder", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
