using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CentrixERP.Common.Business.Entity;
using SF.Framework.Data;
using System.Data.SqlClient;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class EntityLockRepository
    {
        public EntityLock GetMapper(SqlDataReader reader)
        {
            EntityLock obj = new EntityLock();
            if (reader["entity_lock_id"] != DBNull.Value)
            {
                obj.EntityLockId = (int)reader["entity_lock_id"];
            }
            if (reader["entity_id"] != DBNull.Value)
            {
                obj.EntityId = (int)reader["entity_id"];
            }
            if (reader["entity_value_id"] != DBNull.Value)
            {
                obj.EntityValueId = (int)reader["entity_value_id"];
            }
            if (reader["lock_all_entity"] != DBNull.Value)
            {
                obj.LockAllEntity = (bool)reader["lock_all_entity"];
            }
            if (reader["user_name"] != DBNull.Value)
            {
                obj.UserName = (string)reader["user_name"];
            }
            if (reader["created_date"] != DBNull.Value)
            {
                obj.CreatedDate = (DateTime)reader["created_date"];
            }
            if (reader["created_by"] != DBNull.Value)
            {
                obj.CreatedBy = (int)reader["created_by"];
            }
            if (reader["updated_date"] != DBNull.Value)
            {
                obj.UpdatedDate = (DateTime)reader["updated_date"];
            }
            if (reader["updated_by"] != DBNull.Value)
            {
                obj.UpdatedBy = (int)reader["updated_by"];
            }

            return obj;
        }
        public EntityLockLite GetMapperLite(SqlDataReader reader)
        {
            EntityLockLite obj = new EntityLockLite();
            if (reader["entity_lock_id"] != DBNull.Value)
            {
                obj.EntityLockId = (int)reader["entity_lock_id"];
            }
            if (reader["entity_id"] != DBNull.Value)
            {
                obj.EntityId = (int)reader["entity_id"];
            }
            if (reader["entity_value_id"] != DBNull.Value)
            {
                obj.EntityValueId = (int)reader["entity_value_id"];
            }

            if (reader["user_name"] != DBNull.Value)
            {
                obj.UserName = (string)reader["user_name"];
            }
            if (reader["created_date"] != DBNull.Value)
            {
                obj.CreatedDate = (DateTime)reader["created_date"];
            }
            if (reader["created_by"] != DBNull.Value)
            {
                obj.CreatedBy = (int)reader["created_by"];
            }

            if (reader["entity_name"] != DBNull.Value)
            {
                obj.EntityName = (string)reader["entity_name"];
            }

            if (reader["ui_name"] != DBNull.Value)
            {
                obj.UIName = (string)reader["ui_name"];
            }

            if (reader["view_url"] != DBNull.Value)
            {
                obj.EntityViewURL = (string)reader["view_url"];
            }

            return obj;
        }
    }
}
