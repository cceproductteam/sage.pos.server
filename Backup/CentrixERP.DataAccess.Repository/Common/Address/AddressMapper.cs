using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.Framework;
using SF.FrameworkEntity;
using CentrixERP.Common.Business.Entity;
using System.Data.SqlClient;
using SF.Framework.Data;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class AddressRepository
    {
        public Address GetAddressMapper(SqlDataReader reader)
        {
            Address address = new Address();
            if (reader["address_id"] != DBNull.Value)
            {
                address.AddressID = (int)reader["address_id"];
            }

            if (reader["address_area"] != DBNull.Value)
            {
                address.AreaId = SQLDAHelper.Convert_DBTOInt(reader["address_area"]);
            }

            if (reader["address_building_number"] != DBNull.Value)
            {
                address.BuildingNumber = (string)reader["address_building_number"];
            }
            if (reader["address_city"] != DBNull.Value)
            {
                address.CityId = SQLDAHelper.Convert_DBTOInt(reader["address_city"]);
            }
            if (reader["address_country"] != DBNull.Value)
            {
                address.CountryId = SQLDAHelper.Convert_DBTOInt(reader["address_country"]);
            }
            if (reader["address_nearby"] != DBNull.Value)
            {
                address.NearBy = (string)reader["address_nearby"];
            }
            if (reader["address_place"] != DBNull.Value)
            {
                address.Place = (string)reader["address_place"];
            }
            if (reader["address_pobox"] != DBNull.Value)
            {
                address.POBox = (string)reader["address_pobox"];
            }
            if (reader["address_region"] != DBNull.Value)
            {
                address.Region = (string)reader["address_region"];
            }
            if (reader["address_street_address"] != DBNull.Value)
            {
                address.StreetAddress = (string)reader["address_street_address"];
            }
            if (reader["address_zip_code"] != DBNull.Value)
            {
                address.ZipCode = (string)reader["address_zip_code"];
            }

            if (reader["address_office_number"] != DBNull.Value)
            {
                address.OfficeNumber = (string)reader["address_office_number"];
            }

            if (reader["address_floor"] != DBNull.Value)
            {
                address.Floor = (string)reader["address_floor"];
            }
            if (reader["address_county"] != DBNull.Value)
            {
                address.County = (string)reader["address_county"];
            }
            if (reader["address_city_text"] != DBNull.Value)
            {
                address.CityText = (string)reader["address_city_text"];
            }

            if (reader["created_by"] != DBNull.Value)
            {
                address.CreatedByName = (string)reader["created_by"];
            }
            if (reader["updated_by"] != DBNull.Value)
            {
                address.UpdatedByName = (string)reader["updated_by"];
            }

            if (reader["crearted_date"] != DBNull.Value)
            {
                address.Crearted_Date = (DateTime)reader["crearted_date"];
            }

            if (reader["updated_date"] != DBNull.Value)
            {
                address.Updated_Date = (DateTime)reader["updated_date"];
            }
            if (reader["Type"] != DBNull.Value)
            {
                address.Type = (string)reader["Type"];
            }

          
            return address;
        }
    }
}
