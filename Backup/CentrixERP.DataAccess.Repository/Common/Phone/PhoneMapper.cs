using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CentrixERP.Common.Business.Entity;
using System.Data.SqlClient;
using SF.Framework;
using SF.Framework.Data;
using SF.FrameworkEntity;
using CentrixERP.Configuration;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class PhoneRepository
    {
        public Phone GetPhoneMapper(SqlDataReader reader)
        {
            Phone phone = new Phone();

            if (reader["phone_id"] != DBNull.Value)
            {
                phone.PhoneID = (int)reader["phone_id"];
            }

            if (reader["phone_area_code"] != DBNull.Value)
            {
                phone.AreaCode = (string)reader["phone_area_code"];
            }

            if (reader["phone_country_code"] != DBNull.Value)
            {
                phone.CountryCode = (string)reader["phone_country_code"];
            }

            if (reader["phone_number"] != DBNull.Value)
            {
                phone.PhoneNumber = (string)reader["phone_number"];
            }

            if (reader["phone_type"] != DBNull.Value)
            {
                phone.Type = (Enums_S3.Phone.PhoneType)reader["phone_type"];
            }
            if (reader["created_by"] != DBNull.Value)
            {
                phone.CreatedByName = (string)reader["created_by"];
            }
            if (reader["updated_by"] != DBNull.Value)
            {
                phone.UpdatedByName = (string)reader["updated_by"];
            }

            if (reader["created_date"] != DBNull.Value)
            {
                phone.Crearted_Date = (DateTime)reader["created_date"];
            }

            if (reader["updated_date"] != DBNull.Value)
            {
                phone.Updated_Date = (DateTime)reader["updated_date"];
            }
            if (reader["Description"] != DBNull.Value)
            {
                phone.Description = (string)reader["Description"];
            }
           
            return phone;
        }

        public Phone GetFindMobileMapper(SqlDataReader reader)
        {
            Phone phone = new Phone();

            if (reader["mobilenumber"] != DBNull.Value)
            {
                phone.PhoneNumber = reader["mobilenumber"].ToString();
            }

            return phone;
        }
    }
}
