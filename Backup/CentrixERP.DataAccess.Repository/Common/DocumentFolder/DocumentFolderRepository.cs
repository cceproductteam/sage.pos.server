
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using CentrixERP.Common.Business.Entity;
using CentrixERP.Common.DataAccess.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class DocumentFolderRepository : IDocumentFolderRepository
    {
        #region IRepository< DocumentFolder, int > Members

        public void Add(ref DocumentFolder myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);

			Instance.AddInParameter("@folder_name", SqlDbType.NVarChar, myEntity.FolderName);
			Instance.AddInParameter("@parent_folder", SqlDbType.Int, myEntity.ParentFolder);
			Instance.AddInParameter("@entity_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.EntityId));
            Instance.AddInParameter("@parent_entity_id", SqlDbType.Int, myEntity.ParentEntityId);
            Instance.AddInParameter("@created_by", SqlDbType.Int,SQLDAHelper.Convert_IntTODB( myEntity.CreatedBy));
            Instance.AddOutParameter("@folder_id", SqlDbType.Int);

            Instance.ExcuteNonQuery("dbo.SP_DocumentFolder_Add", conn, false);

            myEntity.FolderId = (int)Instance.GetOutParamValue("@folder_id");
        }

        public void Update(ref DocumentFolder myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@folder_id", SqlDbType.Int, myEntity.FolderId);
			Instance.AddInParameter("@folder_name", SqlDbType.NVarChar, myEntity.FolderName);
			Instance.AddInParameter("@parent_folder", SqlDbType.Int,SQLDAHelper.Convert_IntTODB(myEntity.ParentFolder));
			Instance.AddInParameter("@entity_id", SqlDbType.Int,SQLDAHelper.Convert_IntTODB( myEntity.EntityId));
            Instance.AddInParameter("@parent_entity_id", SqlDbType.Int, myEntity.ParentEntityId);
            Instance.AddInParameter("@updated_by", SqlDbType.Int,SQLDAHelper.Convert_IntTODB(myEntity.UpdatedBy));
            
            Instance.ExcuteNonQuery("dbo.SP_DocumentFolder_Update", conn, false);
        }

        public DocumentFolder FindById(int Id, CriteriaBase myCriteria)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@folder_id", SqlDbType.Int, Id);
            SqlDataReader reader;

            reader = Instance.ExcuteReader("dbo.SP_DocumentFolder_FindById", conn);

            if (reader != null && reader.HasRows)
            {
                reader.Read();
                DocumentFolder myDocumentFolder = GetMapper(reader);
                reader.Close();
                return myDocumentFolder;
            }
            else
                return null;
        }

        public List<DocumentFolder> FindAll(CriteriaBase myCriteria)
        {
            List<DocumentFolder> DocumentFolderList = new List<DocumentFolder>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;
            if (myCriteria != null)
            {
                DocumentFolderCriteria criteria = (DocumentFolderCriteria)myCriteria;
                Instance.AddInParameter("@parent_id", SqlDbType.Int,criteria.parentFolder);
                Instance.AddInParameter("@entity_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(criteria.EntityID));
                Instance.AddInParameter("@created_by", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(criteria.CreatedBy));
                Instance.AddInParameter("@parent_entity_id", SqlDbType.Int, criteria.ParentEntityId);
            }
            reader = Instance.ExcuteReader("dbo.SP_DocumentFolder_FindAll", conn);

            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                    DocumentFolderList.Add(GetMapper(reader));
                reader.Close();
                return DocumentFolderList;
            }
            else
                return null;

        }

        public List<DocumentFolder> FindByParentId<TParentId>(TParentId ParentId, Type ParentType, CriteriaBase myCriteria)
        {
            List<DocumentFolder> DocumentFolderList = new List<DocumentFolder>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);

	

            if (myCriteria != null)
            {
                DocumentFolderCriteria criteria = (DocumentFolderCriteria)myCriteria;
            }			

            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_DocumentFolder_FindByParentId", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    DocumentFolderList.Add(GetMapper(reader));
                }
                reader.Close();
                return DocumentFolderList;
            }
            else
            {
                return null;
            }
        }

        public void Delete(ref DocumentFolder myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@folder_id", SqlDbType.Int, myEntity.FolderId);

            Instance.ExcuteNonQuery("dbo.SP_DocumentFolder_Delete", conn, false);
        }

        public void DeleteLogical(ref DocumentFolder myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@folder_id", SqlDbType.Int, myEntity.FolderId);

            Instance.ExcuteNonQuery("dbo.SP_DocumentFolder_DeleteLogical", conn, false);
        }

        public void DeleteFolder(DocumentFolder myEntity,int newFolderId)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@folder_id", SqlDbType.Int, myEntity.FolderId);
            Instance.AddInParameter("@new_folder_id", SqlDbType.Int,SQLDAHelper.Convert_IntTODB(newFolderId));
            Instance.ExcuteNonQuery("dbo.SP_DocumentFolder_DeleteLogical", conn, false);
        }

        public string CustomConnectionString
        {
            get;
            set;
        }

        public bool UseCustomConnectionString
        {
            get;
            set;
        }
        public Object BeginTransaction()
        {
            return new object();
        }


        public void CommitTransaction(Object object1)
        {
        }

        public void Rollback(Object object1)
        {

        }

        public void KillConnection()
        {

        }


        public bool UseSharedSession { get; set; }
		#endregion
    }
}