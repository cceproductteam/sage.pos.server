
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using CentrixERP.Common.Business.Entity;
using CentrixERP.Common.DataAccess.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class SystemEntityRepository : ISystemEntityRepository
    {
        #region IRepository< SystemEntity, int > Members

        public void Add(ref SystemEntity myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);

			Instance.AddInParameter("@table_name", SqlDbType.NVarChar, myEntity.TableName);
			Instance.AddInParameter("@entity_name", SqlDbType.NVarChar, myEntity.EntityName);
			Instance.AddInParameter("@entity_name_ar", SqlDbType.NVarChar, myEntity.EntityNameAr);
			Instance.AddInParameter("@module_name", SqlDbType.NVarChar, myEntity.ModuleName);
			Instance.AddInParameter("@name_space", SqlDbType.NVarChar, myEntity.NameSpace);
			Instance.AddInParameter("@imanager", SqlDbType.NVarChar, myEntity.Imanager);
			Instance.AddInParameter("@primary_key_field_name", SqlDbType.VarChar, myEntity.PrimaryKeyFieldName);
			Instance.AddInParameter("@view_label_filed_name", SqlDbType.VarChar, myEntity.ViewLabelFiledName);
			Instance.AddInParameter("@service_url", SqlDbType.VarChar, myEntity.ServiceUrl);
			Instance.AddInParameter("@service_findall_method", SqlDbType.VarChar, myEntity.ServiceFindallMethod);
			Instance.AddInParameter("@view_url", SqlDbType.VarChar, myEntity.ViewUrl);
            Instance.AddInParameter("@created_by", SqlDbType.Int, myEntity.CreatedBy);
            Instance.AddOutParameter("@entity_id", SqlDbType.Int);

            Instance.ExcuteNonQuery("dbo.SP_SystemEntity_Add", conn, false);

            myEntity.SystemEntityId = (int)Instance.GetOutParamValue("@entity_id");
        }

        public void Update(ref SystemEntity myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@entity_id", SqlDbType.Int, myEntity.SystemEntityId);
			Instance.AddInParameter("@table_name", SqlDbType.NVarChar, myEntity.TableName);
			Instance.AddInParameter("@entity_name", SqlDbType.NVarChar, myEntity.EntityName);
			Instance.AddInParameter("@entity_name_ar", SqlDbType.NVarChar, myEntity.EntityNameAr);
			Instance.AddInParameter("@module_name", SqlDbType.NVarChar, myEntity.ModuleName);
			Instance.AddInParameter("@name_space", SqlDbType.NVarChar, myEntity.NameSpace);
			Instance.AddInParameter("@imanager", SqlDbType.NVarChar, myEntity.Imanager);
			Instance.AddInParameter("@primary_key_field_name", SqlDbType.VarChar, myEntity.PrimaryKeyFieldName);
			Instance.AddInParameter("@view_label_filed_name", SqlDbType.VarChar, myEntity.ViewLabelFiledName);
			Instance.AddInParameter("@service_url", SqlDbType.VarChar, myEntity.ServiceUrl);
			Instance.AddInParameter("@service_findall_method", SqlDbType.VarChar, myEntity.ServiceFindallMethod);
			Instance.AddInParameter("@view_url", SqlDbType.VarChar, myEntity.ViewUrl);
            Instance.AddInParameter("@updated_by", SqlDbType.Int,myEntity.UpdatedBy);
            
            Instance.ExcuteNonQuery("dbo.SP_SystemEntity_Update", conn, false);
        }

        public SystemEntity FindById(int Id, CriteriaBase myCriteria)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@entity_id", SqlDbType.Int, Id);
            SqlDataReader reader;

            reader = Instance.ExcuteReader("dbo.SP_SystemEntity_FindById", conn);

            if (reader != null && reader.HasRows)
            {
                reader.Read();
                SystemEntity mySystemEntity = GetMapper(reader);
                reader.Close();
                return mySystemEntity;
            }
            else
                return null;
        }
        public List<SystemEntity> FindAll(CriteriaBase myCriteria)
        {
            List<SystemEntity> SystemEntityList = new List<SystemEntity>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;
            if (myCriteria != null)
            {
                SystemEntityCriteria criteria = (SystemEntityCriteria)myCriteria;
                if (!string.IsNullOrEmpty(criteria.keyword))
                    Instance.AddInParameter("@keyword", SqlDbType.NVarChar, criteria.keyword);
                if (criteria.mainEntity.HasValue)
                    Instance.AddInParameter("@main_entity", SqlDbType.NVarChar, criteria.mainEntity.Value);
                if (criteria.HasOptionalFields.HasValue)
                    Instance.AddInParameter("@has_optional_fields", SqlDbType.Bit, criteria.HasOptionalFields.Value);
            }
            reader = Instance.ExcuteReader("dbo.SP_SystemEntity_FindAll", conn);

            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                    SystemEntityList.Add(GetMapper(reader));
                reader.Close();
                return SystemEntityList;
            }
            else
                return null;

        }

        public List<SystemEntity> FindByParentId<TParentId>(TParentId ParentId, Type ParentType, CriteriaBase myCriteria)
        {
            List<SystemEntity> SystemEntityList = new List<SystemEntity>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);

	

            if (myCriteria != null)
            {
                SystemEntityCriteria criteria = (SystemEntityCriteria)myCriteria;
            }			

            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_SystemEntity_FindByParentId", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    SystemEntityList.Add(GetMapper(reader));
                }
                reader.Close();
                return SystemEntityList;
            }
            else
            {
                return null;
            }
        }

        public void Delete(ref SystemEntity myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@entity_id", SqlDbType.Int, myEntity.SystemEntityId);

            Instance.ExcuteNonQuery("dbo.SP_SystemEntity_Delete", conn, false);
        }

        public void DeleteLogical(ref SystemEntity myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@entity_id", SqlDbType.Int, myEntity.SystemEntityId);

            Instance.ExcuteNonQuery("dbo.SP_SystemEntity_DeleteLogical", conn, false);
        }

        public List<EntityRelations> CheckEntityRelations(int entityId, int entityValueId, CriteriaBase myCriteria)
        {
            SqlDataReader reader;
            List<EntityRelations> EntityRelations;
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);

            Instance.AddInParameter("@entity_id", SqlDbType.Int, entityId);
            Instance.AddInParameter("@entity_value_id", SqlDbType.Int, entityValueId);

            reader = Instance.ExcuteReader("SP_EntityRelations_Check", conn);

            if (reader != null && reader.HasRows)
            {
                EntityRelations = new List<EntityRelations>();
                while (reader.Read())
                    EntityRelations.Add(GetEntityRelationsLite(reader));
                reader.Close();
                return EntityRelations;
            }
            else
                return null;
        }

        public string CustomConnectionString
        {
            get;
            set;
        }

        public bool UseCustomConnectionString
        {
            get;
            set;
        }
        public Object BeginTransaction()
        {
            return new object();
        }


        public void CommitTransaction(Object object1)
        {
        }

        public void Rollback(Object object1)
        {

        }

        public void KillConnection()
        {

        }


        public bool UseSharedSession { get; set; }
		#endregion
    }
}