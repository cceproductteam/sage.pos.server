using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CentrixERP.Common.Business.Entity;
using SF.Framework.Data;
using System.Data.SqlClient;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class SystemEntityRepository
    {
        public SystemEntity GetMapper(SqlDataReader reader)
        {
            SystemEntity obj = new SystemEntity();
			if (reader["entity_id"] != DBNull.Value)
			{
				obj.SystemEntityId = (int)reader["entity_id"];
			}
			if (reader["table_name"] != DBNull.Value)
			{
				obj.TableName = (string)reader["table_name"];
			}
			if (reader["entity_name"] != DBNull.Value)
			{
				obj.EntityName = (string)reader["entity_name"];
			}
			if (reader["entity_name_ar"] != DBNull.Value)
			{
				obj.EntityNameAr = (string)reader["entity_name_ar"];
			}
			if (reader["module_name"] != DBNull.Value)
			{
				obj.ModuleName = (string)reader["module_name"];
			}
			if (reader["name_space"] != DBNull.Value)
			{
				obj.NameSpace = (string)reader["name_space"];
			}
			if (reader["imanager"] != DBNull.Value)
			{
				obj.Imanager = (string)reader["imanager"];
			}
			if (reader["created_by"] != DBNull.Value)
			{
				obj.CreatedBy = (int)reader["created_by"];
			}
			if (reader["created_date"] != DBNull.Value)
			{
				obj.CreatedDate = (DateTime)reader["created_date"];
			}
			if (reader["updated_by"] != DBNull.Value)
			{
				obj.UpdatedBy = (int)reader["updated_by"];
			}
			if (reader["updated_date"] != DBNull.Value)
			{
				obj.UpdatedDate = (DateTime)reader["updated_date"];
			}
			if (reader["primary_key_field_name"] != DBNull.Value)
			{
				obj.PrimaryKeyFieldName = (string)reader["primary_key_field_name"];
			}
			if (reader["view_label_filed_name"] != DBNull.Value)
			{
				obj.ViewLabelFiledName = (string)reader["view_label_filed_name"];
			}
			if (reader["service_url"] != DBNull.Value)
			{
				obj.ServiceUrl = (string)reader["service_url"];
			}
			if (reader["service_findall_method"] != DBNull.Value)
			{
				obj.ServiceFindallMethod = (string)reader["service_findall_method"];
			}
			if (reader["view_url"] != DBNull.Value)
			{
				obj.ViewUrl = (string)reader["view_url"];
			}
            if (reader["main_entity"] != DBNull.Value)
            {
                obj.MainEntity = (bool)reader["main_entity"];
            }

            return obj;
        }

        public EntityRelations GetEntityRelationsLite(SqlDataReader reader)
        {
            EntityRelations entityRelation = new EntityRelations();
            if (reader["related_entity_name_en"] != DBNull.Value)
                entityRelation.RelatedEntityNameEn = Convert.ToString(reader["related_entity_name_en"]);

            if (reader["related_entity_name_ar"] != DBNull.Value)
                entityRelation.RelatedEntityNameAr = Convert.ToString(reader["related_entity_name_ar"]);

            if (reader["related_view_url"] != DBNull.Value)
                entityRelation.RelatedEntityViewUrl = Convert.ToString(reader["related_view_url"]);

            if (reader["related_primary_key_id"] != DBNull.Value)
                entityRelation.RelatedPrimaryKeyId = Convert.ToInt32(reader["related_primary_key_id"]);
            return entityRelation;
        }
    }
}
