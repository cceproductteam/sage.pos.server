using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CentrixERP.Common.Business.Entity;
using SF.Framework.Data;
using System.Data.SqlClient;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class SystemConfigurationWebKeyRepository
    {
        public SystemConfigurationWebKey GetMapper(SqlDataReader reader)
        {
            SystemConfigurationWebKey obj = new SystemConfigurationWebKey();
			if (reader["system_configuration_web_key_id"] != DBNull.Value)
			{
				obj.SystemConfigurationWebKeyId = (int)reader["system_configuration_web_key_id"];
			}
			if (reader["key"] != DBNull.Value)
			{
				obj.Key = (string)reader["key"];
			}
            if (reader["key_value"] != DBNull.Value)
			{
                obj.Value = (string)reader["key_value"];
			}
			if (reader["type"] != DBNull.Value)
			{
				obj.Type = (int)reader["type"];
			}
			if (reader["updated_by"] != DBNull.Value)
			{
				obj.UpdatedBy = (int)reader["updated_by"];
			}
			if (reader["created_by"] != DBNull.Value)
			{
				obj.CreatedBy = (int)reader["created_by"];
			}
			if (reader["updated_date"] != DBNull.Value)
			{
				obj.UpdatedDate = (DateTime)reader["updated_date"];
			}
			if (reader["created_date"] != DBNull.Value)
			{
				obj.CreatedDate = (DateTime)reader["created_date"];
			}

            return obj;
        }
    }
}
