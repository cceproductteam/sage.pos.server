
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using CentrixERP.Common.Business.Entity;
using CentrixERP.Common.DataAccess.IRepository;
using System.Data.SqlClient;
using System.Data;
using CentrixERP.Configuration;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class EmailEntityRepository : RepositoryBaseClass<EmailEntity, EmailEntityLite, int>, IEmailEntityRepository
    {
        #region IRepository< EmailEntity, int > Members

        public EmailEntityRepository()
        {
            this.AddSPName = "SP_EmailEntity_Add";
            this.UpdateSPName = "SP_EmailEntity_Update";
            this.DeleteSPName = "SP_EmailEntity_Delete";
            this.DeleteLogicalSPName = "SP_EmailEntity_DeleteLogical";
            this.FindAllSPName = "SP_EmailEntity_FindAll";
            this.FindByIdSPName = "SP_EmailEntity_FindById";
            this.FindByParentSPName = "SP_EmailEntity_FindByParentId";
            this.FindAllLiteSPName = "SP_EmailEntity_FindAll_Lite";
            this.FindByIdLiteSPName = "SP_EmailEntity_FindById_Lite";
            this.AdvancedSearchSPName = "SP_EmailEntity_AdvancedSearch_Lite";
        }

        public override List<EmailEntity> FindByParentId<TParentId>(TParentId ParentId, Type ParentType, CriteriaBase myCriteria)
        {
            List<EmailEntity> EmailList = new List<EmailEntity>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;
            EmailEntityCriteria Criteria = new EmailEntityCriteria();
            Criteria.EntityValueId =Convert.ToInt32(ParentId);
            Criteria.EntityId = CentrixERP.Configuration.Configuration.GetEntityId(ParentType);

            if (Criteria != null)
            {

                Instance.AddInParameter("@entity_id", SqlDbType.Int, Criteria.EntityId);
                Instance.AddInParameter("@entity_value_id", SqlDbType.Int, Criteria.EntityValueId);//ParentId
            }
            reader = Instance.ExcuteReader("dbo.SP_EmailEntity_FindByParentId", conn);


            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                    EmailList.Add(GetEmailEntityData(reader));
                reader.Close();
                return EmailList;
            }
            else
                return null;
        }


        #endregion
    }
}