
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using CentrixERP.Common.Business.Entity;
using CentrixERP.Common.DataAccess.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class ModuleRepository : IModuleRepository
    {
        #region IRepository< Module, int > Members

        public void Add(ref Module myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);

			Instance.AddInParameter("@module_name_en", SqlDbType.NVarChar, myEntity.ModuleNameEn);
			Instance.AddInParameter("@module_name_ar", SqlDbType.NVarChar, myEntity.ModuleNameAr);
			Instance.AddInParameter("@has_docs", SqlDbType.Bit, myEntity.HasDocs);
            Instance.AddInParameter("@created_by", SqlDbType.Int, myEntity.CreatedBy);
            Instance.AddOutParameter("@module_id", SqlDbType.Int);

            Instance.ExcuteNonQuery("dbo.SP_Module_Add", conn, false);

            myEntity.ModuleId = (int)Instance.GetOutParamValue("@module_id");
        }

        public void Update(ref Module myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@module_id", SqlDbType.Int, myEntity.ModuleId);
			Instance.AddInParameter("@module_name_en", SqlDbType.NVarChar, myEntity.ModuleNameEn);
			Instance.AddInParameter("@module_name_ar", SqlDbType.NVarChar, myEntity.ModuleNameAr);
			Instance.AddInParameter("@has_docs", SqlDbType.Bit, myEntity.HasDocs);
            Instance.AddInParameter("@updated_by", SqlDbType.Int,myEntity.UpdatedBy);
            
            Instance.ExcuteNonQuery("dbo.SP_Module_Update", conn, false);
        }

        public Module FindById(int Id, CriteriaBase myCriteria)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@module_id", SqlDbType.Int, Id);
            SqlDataReader reader;

            reader = Instance.ExcuteReader("dbo.SP_Module_FindById", conn);

            if (reader != null && reader.HasRows)
            {
                reader.Read();
                Module myModule = GetMapper(reader);
                reader.Close();
                return myModule;
            }
            else
                return null;
        }
        public List<Module> FindAll(CriteriaBase myCriteria)
        {
            List<Module> ModuleList = new List<Module>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;
            if (myCriteria != null)
            {
                ModuleCriteria criteria = (ModuleCriteria)myCriteria;
                Instance.AddInParameter("@has_docs", SqlDbType.Bit, criteria.HasDocument);
            }
            reader = Instance.ExcuteReader("dbo.SP_Module_FindAll", conn);

            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                    ModuleList.Add(GetMapper(reader));
                reader.Close();
                return ModuleList;
            }
            else
                return null;

        }

        public List<Module> FindByParentId<TParentId>(TParentId ParentId, Type ParentType, CriteriaBase myCriteria)
        {
            List<Module> ModuleList = new List<Module>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);

	

            if (myCriteria != null)
            {
                ModuleCriteria criteria = (ModuleCriteria)myCriteria;
            }			

            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_Module_FindByParentId", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    ModuleList.Add(GetMapper(reader));
                }
                reader.Close();
                return ModuleList;
            }
            else
            {
                return null;
            }
        }

        public void Delete(ref Module myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@module_id", SqlDbType.Int, myEntity.ModuleId);

            Instance.ExcuteNonQuery("dbo.SP_Module_Delete", conn, false);
        }

        public void DeleteLogical(ref Module myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@module_id", SqlDbType.Int, myEntity.ModuleId);

            Instance.ExcuteNonQuery("dbo.SP_Module_DeleteLogical", conn, false);
        }

        public string CustomConnectionString
        {
            get;
            set;
        }

        public bool UseCustomConnectionString
        {
            get;
            set;
        }
        public Object BeginTransaction()
        {
            return new object();
        }


        public void CommitTransaction(Object object1)
        {
        }

        public void Rollback(Object object1)
        {

        }

        public void KillConnection()
        {

        }


        public bool UseSharedSession { get; set; }
		#endregion
    }
}