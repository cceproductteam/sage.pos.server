using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SF.Framework.Data;
using CentrixERP.Common.Business.Entity;
using CentrixERP.Common.DataAccess.IRepository;
using System.Data.SqlClient;


namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class CityRepository
    {
        public City GetMapper(SqlDataReader reader)
        {
            City myCity = new City();
            if (reader["city_id"] != DBNull.Value)
            {
                myCity.CityId = (int)reader["city_id"];
            }
            if (reader["country_id"] != DBNull.Value)
            {
                myCity.CountryId = (int)reader["country_id"];
            }
            if (reader["city_name_en"] != DBNull.Value)
            {
                myCity.CityNameEn = reader["city_name_en"].ToString();
            }
            if (reader["city_name_ar"] != DBNull.Value)
            {
                myCity.CityNameAr = reader["city_name_ar"].ToString();
            }

            return myCity;
        }
    }
}
