using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CentrixERP.Common.Business.Entity;
using SF.Framework.Data;
using System.Data.SqlClient;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class EntityControlConfigRepository
    {
        public EntityControlConfig GetMapper(SqlDataReader reader)
        {
            EntityControlConfig obj = new EntityControlConfig();
			if (reader["entity_control_config_id"] != DBNull.Value)
			{
				obj.EntityControlConfigId = (int)reader["entity_control_config_id"];
			}
			if (reader["entity_control_id"] != DBNull.Value)
			{
				obj.EntityControlId = (int)reader["entity_control_id"];
			}
			if (reader["user_id"] != DBNull.Value)
			{
				obj.UserId = (int)reader["user_id"];
			}
			if (reader["search_criteria_id"] != DBNull.Value)
			{
				obj.SearchCriteriaId = (int)reader["search_criteria_id"];
			}
			if (reader["searchable"] != DBNull.Value)
			{
				obj.Searchable = (bool)reader["searchable"];
			}
			if (reader["quick_search"] != DBNull.Value)
			{
				obj.QuickSearch = (bool)reader["quick_search"];
			}
			if (reader["created_by"] != DBNull.Value)
			{
				obj.CreatedBy = (int)reader["created_by"];
			}
			if (reader["updated_by"] != DBNull.Value)
			{
				obj.UpdatedBy = (int)reader["updated_by"];
			}
			if (reader["created_date"] != DBNull.Value)
			{
				obj.CreatedDate = (DateTime)reader["created_date"];
			}
			if (reader["updated_date"] != DBNull.Value)
			{
				obj.UpdatedDate = (DateTime)reader["updated_date"];
			}

            return obj;
        }

        public EntityControlConfig_Lite GetLiteMapper(SqlDataReader reader)
        {
            EntityControlConfig_Lite obj = new EntityControlConfig_Lite();
            if (reader["entity_control_config_id"] != DBNull.Value)
            {
                obj.EntityControlConfigId = (int)reader["entity_control_config_id"];
              
            }
            if (reader["entity_control_id"] != DBNull.Value)
            {
                obj.EntityControlId = (int)reader["entity_control_id"];
                obj.value = (int)reader["entity_control_id"];
            }
            if (reader["user_id"] != DBNull.Value)
            {
                obj.UserId = (int)reader["user_id"];
            }
            if (reader["search_criteria_id"] != DBNull.Value)
            {
                obj.SearchCriteriaId = (int)reader["search_criteria_id"];
            }
            if (reader["searchable"] != DBNull.Value)
            {
                obj.Searchable = (bool)reader["searchable"];
            }
            if (reader["quick_search"] != DBNull.Value)
            {
                obj.QuickSearch = (bool)reader["quick_search"];
            }
            if (reader["created_by"] != DBNull.Value)
            {
                obj.CreatedBy = (int)reader["created_by"];
            }
            if (reader["updated_by"] != DBNull.Value)
            {
                obj.UpdatedBy = (int)reader["updated_by"];
            }
            if (reader["created_date"] != DBNull.Value)
            {
                obj.CreatedDate = (DateTime)reader["created_date"];
            }
            if (reader["updated_date"] != DBNull.Value)
            {
                obj.UpdatedDate = (DateTime)reader["updated_date"];
            }


            if (reader["control_id"] != DBNull.Value)
            {
                obj.ControlId = (int)reader["control_id"];
            }
              if (reader["control_type_id"] != DBNull.Value)
            {
                obj.ControlTypeId = (int)reader["control_type_id"];
            }
              if (reader["control_name"] != DBNull.Value)
            {
                obj.ControlName = (string)reader["control_name"];
             
            }

            
              if (reader["entity_id"] != DBNull.Value)
            {
                obj.EntityId = (int)reader["entity_id"];
            }
              if (reader["data_type_content_id"] != DBNull.Value)
            {
                obj.DataTypeContentId = (int)reader["data_type_content_id"];
            }

              if (reader["ui_name_ar"] != DBNull.Value)
              {
                  obj.UINameAr = (string)reader["ui_name_ar"];
              }

             if (reader["ui_name_en"] != DBNull.Value)
              {
                  obj.UIName= (string)reader["ui_name_en"];
                  //obj.label = (string)reader["ui_name_en"];
              }
             if (reader["ui_value"] != DBNull.Value)
              {
                  obj.UIValue= (string)reader["ui_value"];
              }

            


             if (reader["entity_name"] != DBNull.Value)
              {
                  obj.EntityName = (string)reader["entity_name"];
              }
             if (reader["entity_name_ar"] != DBNull.Value)
              {
                  obj.EntityNameAr = (string)reader["entity_name_ar"];
              }

             if (reader["property_name_ar"] != DBNull.Value)
             {
                 obj.PropertyNameAr = (string)reader["property_name_ar"];
             }

             if (reader["property_name_en"] != DBNull.Value)
             {
                 obj.PropertyName = (string)reader["property_name_en"];
             }

             if (reader["property_parent_id"] != DBNull.Value)
             {
                 obj.ProprtyParentId = (int)reader["property_parent_id"];
             }
             if (reader["service_method"] != DBNull.Value)
             {
                 obj.ServiceMethod = (string)reader["service_method"];
             }
             if (reader["service_name"] != DBNull.Value)
             {
                 obj.ServiceName = (string)reader["service_name"];
             }

             if (reader["db_column_name_ar"] != DBNull.Value)
             {
                 obj.ColumnNameAr = (string)reader["db_column_name_ar"];
             }

             if (reader["db_column_name_en"] != DBNull.Value)
             {
                 obj.ColumnName= (string)reader["db_column_name_en"];
             }

             if (reader["show_in_search_result"] != DBNull.Value)
             {
                 obj.ShowInSearchResult = (bool)reader["show_in_search_result"];
             }



             if (reader["child_AC"] != DBNull.Value)
             {
                 obj.ChildAC = (string)reader["child_AC"];
             }

             if (reader["child_AC_args"] != DBNull.Value)
             {
                 obj.ChildACValue = (string)reader["child_AC_args"];
             }


             if (reader["method_args"] != DBNull.Value)
             {
                 obj.MethodArgs = (string)reader["method_args"];
             }

             if (reader["method_args_values"] != DBNull.Value)
             {
                 obj.MethodArgsValues = (string)reader["method_args_values"];
             }

             if (reader["entity_referance_url"] != DBNull.Value)
             {
                 obj.EntityReferanceUrl = (string)reader["entity_referance_url"];
             }

              if (reader["ui_value_ar"] != DBNull.Value)
             {
                 obj.UIValueAr = (string)reader["ui_value_ar"];
             }

            
            
            return obj;
        }
    }
}
