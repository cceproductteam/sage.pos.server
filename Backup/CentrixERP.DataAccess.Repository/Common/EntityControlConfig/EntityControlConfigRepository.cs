
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using CentrixERP.Common.Business.Entity;
using CentrixERP.Common.DataAccess.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class EntityControlConfigRepository : IEntityControlConfigRepository
    {
        #region IRepository< EntityControlConfig, int > Members

        public void Add(ref EntityControlConfig myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);

			Instance.AddInParameter("@entity_control_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.EntityControlId));
			Instance.AddInParameter("@user_id", SqlDbType.Int, myEntity.UserId);
			Instance.AddInParameter("@search_criteria_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.SearchCriteriaId));
			Instance.AddInParameter("@searchable", SqlDbType.Bit, myEntity.Searchable);
			Instance.AddInParameter("@quick_search", SqlDbType.Bit, myEntity.QuickSearch);
            Instance.AddInParameter("@created_by", SqlDbType.Int, myEntity.CreatedBy);
            Instance.AddOutParameter("@entity_control_config_id", SqlDbType.Int);

            Instance.ExcuteNonQuery("dbo.SP_EntityControlConfig_Add", conn, false);

            myEntity.EntityControlConfigId = (int)Instance.GetOutParamValue("@entity_control_config_id");
        }

        public void Update(ref EntityControlConfig myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@entity_control_config_id", SqlDbType.Int, myEntity.EntityControlConfigId);
			Instance.AddInParameter("@entity_control_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.EntityControlId));
			Instance.AddInParameter("@user_id", SqlDbType.Int, myEntity.UserId);
			Instance.AddInParameter("@search_criteria_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.SearchCriteriaId));
			Instance.AddInParameter("@searchable", SqlDbType.Bit, myEntity.Searchable);
			Instance.AddInParameter("@quick_search", SqlDbType.Bit, myEntity.QuickSearch);
            Instance.AddInParameter("@updated_by", SqlDbType.Int,myEntity.UpdatedBy);
            
            Instance.ExcuteNonQuery("dbo.SP_EntityControlConfig_Update", conn, false);
        }

        public EntityControlConfig FindById(int Id, CriteriaBase myCriteria)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@entity_control_config_id", SqlDbType.Int, Id);
            SqlDataReader reader;

            reader = Instance.ExcuteReader("dbo.SP_EntityControlConfig_FindById", conn);

            if (reader != null && reader.HasRows)
            {
                reader.Read();
                EntityControlConfig myEntityControlConfig = GetMapper(reader);
                reader.Close();
                return myEntityControlConfig;
            }
            else
                return null;
        }
        public List<EntityControlConfig> FindAll(CriteriaBase myCriteria)
        {
            List<EntityControlConfig> EntityControlConfigList = new List<EntityControlConfig>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;
            if (myCriteria != null)
            {
                EntityControlConfigCriteria criteria = (EntityControlConfigCriteria)myCriteria;
                if(criteria.EntityId > -1)
                    Instance.AddInParameter("@entity_id", SqlDbType.Int, criteria.EntityId);
                if (criteria.AdvancedSearchType)
                {
                    Instance.AddInParameter("@searchable", SqlDbType.Bit, true);
                   // Instance.AddInParameter("@quick_search", SqlDbType.Bit, false);
                }
                else
                {
                    
                    Instance.AddInParameter("@quick_search", SqlDbType.Bit, true);
                   // Instance.AddInParameter("@searchable", SqlDbType.Bit, false);
                }

            }
            reader = Instance.ExcuteReader("dbo.SP_EntityControlConfig_FindAll", conn);

            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                    EntityControlConfigList.Add(GetMapper(reader));
                reader.Close();
                return EntityControlConfigList;
            }
            else
                return null;

        }

        public List<EntityControlConfig> FindByParentId<TParentId>(TParentId ParentId, Type ParentType, CriteriaBase myCriteria)
        {
            List<EntityControlConfig> EntityControlConfigList = new List<EntityControlConfig>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);

            //if (ParentType == typeof(EntityControls))
            //    Instance.AddInParameter("@entity_control_id", SqlDbType.Int, ParentId);
		 if (ParentType == typeof(SearchCriteria))
				Instance.AddInParameter("@search_criteria_id", SqlDbType.Int, ParentId);
	

            if (myCriteria != null)
            {
                EntityControlConfigCriteria criteria = (EntityControlConfigCriteria)myCriteria;
            }			

            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_EntityControlConfig_FindByParentId", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    EntityControlConfigList.Add(GetMapper(reader));
                }
                reader.Close();
                return EntityControlConfigList;
            }
            else
            {
                return null;
            }
        }

        public void Delete(ref EntityControlConfig myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@entity_control_config_id", SqlDbType.Int, myEntity.EntityControlConfigId);

            Instance.ExcuteNonQuery("dbo.SP_EntityControlConfig_Delete", conn, false);
        }

        public void DeleteLogical(ref EntityControlConfig myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@entity_control_config_id", SqlDbType.Int, myEntity.EntityControlConfigId);

            Instance.ExcuteNonQuery("dbo.SP_EntityControlConfig_DeleteLogical", conn, false);
        }
		#endregion

        public List<EntityControlConfig_Lite> FindAllLite(CriteriaBase myCriteria)
        {
            List<EntityControlConfig_Lite> EntityControlConfigList = new List<EntityControlConfig_Lite>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;
            if (myCriteria != null)
            {
                EntityControlConfigCriteria criteria = (EntityControlConfigCriteria)myCriteria;
                if (criteria.EntityId > -1)
                    Instance.AddInParameter("@entity_id", SqlDbType.Int, criteria.EntityId);
                if (criteria.AdvancedSearchType)
                {
                    Instance.AddInParameter("@searchable", SqlDbType.Bit, true);
                    // Instance.AddInParameter("@quick_search", SqlDbType.Bit, false);
                }
                else
                {

                    Instance.AddInParameter("@quick_search", SqlDbType.Bit, true);
                    // Instance.AddInParameter("@searchable", SqlDbType.Bit, false);
                }

            }
            reader = Instance.ExcuteReader("dbo.SP_EntityControlConfig_FindAll_Lite", conn);

            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                    EntityControlConfigList.Add(GetLiteMapper(reader));
                reader.Close();
                return EntityControlConfigList;
            }
            else
                return null;

        }


        public string CustomConnectionString
        {
            get;
            set;
        }

        public bool UseCustomConnectionString
        {
            get;
            set;
        }
        public Object BeginTransaction()
        {
            return new object();
        }


        public void CommitTransaction(Object object1)
        {
        }

        public void Rollback(Object object1)
        {

        }

        public void KillConnection()
        {

        }


        public bool UseSharedSession { get; set; }

       
    }
}