
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using CentrixERP.Common.Business.Entity;
using CentrixERP.Common.DataAccess.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class PhoneEntityRepository : RepositoryBaseClass<PhoneEntity, PhoneEntityLite, int>, IPhoneEntityRepository
    {
        #region IRepository< PhoneEntity, int > Members

        public PhoneEntityRepository()
        {
            this.AddSPName = "SP_PhoneEntity_Add";
            this.UpdateSPName = "SP_PhoneEntity_Update";
            this.DeleteSPName = "SP_PhoneEntity_Delete";
            this.DeleteLogicalSPName = "SP_PhoneEntity_DeleteLogical";
            this.FindAllSPName = "SP_PhoneEntity_FindAll";
            this.FindByIdSPName = "SP_PhoneEntity_FindById";
            this.FindByParentSPName = "SP_PhoneEntity_FindByParentId";
            this.FindAllLiteSPName = "SP_PhoneEntity_FindAll_Lite";
            this.FindByIdLiteSPName = "SP_PhoneEntity_FindById_Lite";
            this.AdvancedSearchSPName = "SP_PhoneEntity_AdvancedSearch_Lite";
        }

        public override List<PhoneEntity> FindByParentId<TParentId>(TParentId ParentId, Type ParentType, CriteriaBase myCriteria)
        {
            List<PhoneEntity> PhoneList = new List<PhoneEntity>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;
            PhoneEntityCriteria Criteria = new PhoneEntityCriteria();
            Criteria.EntityValueId = Convert.ToInt32(ParentId);
            Criteria.EntityId = CentrixERP.Configuration.Configuration.GetEntityId(ParentType);

            if (Criteria != null)
            {

                Instance.AddInParameter("@entity_id", SqlDbType.Int, Criteria.EntityId);
                Instance.AddInParameter("@entity_value_id", SqlDbType.Int, Criteria.EntityValueId);//ParentId
            }
            reader = Instance.ExcuteReader("dbo.SP_PhoneEntity_FindByParentId", conn);

            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                    PhoneList.Add(GetPhoneEntityData(reader));
                reader.Close();
                return PhoneList;
            }
            else
                return null;
        }


		#endregion
    }
}