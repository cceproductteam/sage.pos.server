using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CentrixERP.Common.DataAccess.IRepository;
using SF.Framework;
using SF.FrameworkEntity;
using System.Data.SqlClient;
using SF.Framework.Data;
using CentrixERP.Common.Business.Entity;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class DocumentRepository : IDocumentRepository
    {
        #region IRepository<Document,int> Members

        public void Add(ref CentrixERP.Common.Business.Entity.Document myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@description", System.Data.SqlDbType.NVarChar, myEntity.Description);
            Instance.AddInParameter("@title", System.Data.SqlDbType.NVarChar, myEntity.Title);
            Instance.AddInParameter("@uploaded_by", System.Data.SqlDbType.Int, myEntity.CreatedBy);
            Instance.AddInParameter("@documentPath", System.Data.SqlDbType.NVarChar, myEntity.DocumentPath);
            Instance.AddInParameter("@parentFolder", System.Data.SqlDbType.NVarChar, myEntity.ParentFolder);
            Instance.AddInParameter("@fileExtension", System.Data.SqlDbType.NVarChar, myEntity.FileExtension);
            Instance.AddInParameter("@orginalFileName", System.Data.SqlDbType.NVarChar, myEntity.OrginalFileName);
            Instance.AddOutParameter("@documentID", System.Data.SqlDbType.Int);
            Instance.AddOutParameter("@documentFileName", System.Data.SqlDbType.NVarChar, 500);
            Instance.AddInParameter("@container_folder", System.Data.SqlDbType.Int, myEntity.FolderId);
            Instance.ExcuteNonQuery("dbo.sp_Document_Add", conn, false);

            var outPutParamValue = Instance.GetOutParamValue("@documentID");
            if (outPutParamValue != DBNull.Value)
            {
                myEntity.DocumentID = (int)outPutParamValue;
                myEntity.FileName = Convert.ToString(Instance.GetOutParamValue("@documentFileName"));
            }

        }

        public void Delete(ref CentrixERP.Common.Business.Entity.Document myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@DocumentID", System.Data.SqlDbType.Int, myEntity.DocumentID);
            Instance.ExcuteNonQuery("dbo.sp_Document_Delete", conn, false);
        }

        public void DeleteLogical(ref CentrixERP.Common.Business.Entity.Document myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@DocumentID", System.Data.SqlDbType.Int, myEntity.DocumentID);
            Instance.ExcuteNonQuery("dbo.sp_Document_DeleteLogical", conn, false);
        }

        public List<CentrixERP.Common.Business.Entity.Document> FindAll(SF.FrameworkEntity.CriteriaBase myCriteria)
        {
            List<Document> documentList = new List<Document>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            if (myCriteria != null)
            {
                DocumentCriteria documentCirteria = (DocumentCriteria)myCriteria;
                if (documentCirteria.Keyword != null)
                    Instance.AddInParameter("@Keyword", System.Data.SqlDbType.NVarChar, documentCirteria.Keyword);

            }
            SqlDataReader reader;
            reader = Instance.ExcuteReader("dbo.sp_Document_FindAll", conn);

            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                    documentList.Add(GetDocumentMapper(reader));
                reader.Close();
                return documentList;
            }
            else
                return null;
        }

        public CentrixERP.Common.Business.Entity.Document FindById(int Id, SF.FrameworkEntity.CriteriaBase myCriteria)
        {
            Document document = new Document();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@DocumentID", System.Data.SqlDbType.Int, Id);
            SqlDataReader reader;
            reader = Instance.ExcuteReader("dbo.sp_Document_FindByID", conn);

            if (reader != null && reader.HasRows)
            {
                reader.Read();
                Document doc = GetDocumentMapper(reader);
                reader.Close();
                return doc;
            }
            else
                return null;
        }

        public List<CentrixERP.Common.Business.Entity.Document> FindByParentId<TParentId>(TParentId ParentId, Type ParentType, SF.FrameworkEntity.CriteriaBase myCriteria)
        {
            throw new NotImplementedException();
        }

        public void Update(ref CentrixERP.Common.Business.Entity.Document myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@description", System.Data.SqlDbType.NVarChar, myEntity.Description);
            Instance.AddInParameter("@title", System.Data.SqlDbType.NVarChar, myEntity.Title);
            Instance.AddInParameter("@updated_by", System.Data.SqlDbType.Int, myEntity.UpdatedBy);
            Instance.AddInParameter("@fileName", System.Data.SqlDbType.NVarChar, myEntity.FileName);
            Instance.AddInParameter("@fileExtension", System.Data.SqlDbType.NVarChar, myEntity.FileExtension);
            Instance.AddInParameter("@orginalFileName", System.Data.SqlDbType.NVarChar, myEntity.OrginalFileName);
            Instance.AddInParameter("@documentID", System.Data.SqlDbType.Int, myEntity.DocumentID);
            Instance.AddInParameter("@documentPath", System.Data.SqlDbType.NVarChar, myEntity.DocumentPath);
            Instance.AddInParameter("@parentFolder", System.Data.SqlDbType.NVarChar, myEntity.ParentFolder);
            Instance.AddInParameter("@container_folder", System.Data.SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.FolderId));
            Instance.ExcuteNonQuery("dbo.sp_Document_Update", conn, false);

        }

        public void UpdateOrder(int DocumentID, int EntityId, int EntityValueId, int OrderID)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@order_id", System.Data.SqlDbType.Int, OrderID);
            Instance.AddInParameter("@document_id", System.Data.SqlDbType.Int, DocumentID);
            Instance.AddInParameter("@Entity_id", System.Data.SqlDbType.Int, EntityId);
            Instance.AddInParameter("@EntityValue_id", System.Data.SqlDbType.Int, EntityValueId);
            Instance.ExcuteNonQuery("dbo.sp_Document_order_Update", conn, false);

        }

        public string CustomConnectionString
        {
            get;
            set;
        }

        public bool UseCustomConnectionString
        {
            get;
            set;
        }
        public Object BeginTransaction()
        {
            return new object();
        }


        public void CommitTransaction(Object object1)
        {
        }

        public void Rollback(Object object1)
        {

        }

        public void KillConnection()
        {

        }


        public bool UseSharedSession { get; set; }
        #endregion
    }
}
