using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using CentrixERP.Common.Business.Entity;
using System.Data.SqlClient;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class DocumentRepository
    {
        public Document GetDocumentMapper(SqlDataReader reader)
        {
            Document document = new Document();
            if (reader["document_orginalFileName"] != DBNull.Value)
            {
                document.OrginalFileName = (string)reader["document_orginalFileName"];
            }
            if (reader["document_description"] != DBNull.Value)
            {
                document.Description = (string)reader["document_description"];
            }
            if (reader["document_id"] != DBNull.Value)
            {
                document.DocumentID = (int)reader["document_id"];
            }
            if (reader["document_fileExtension"] != DBNull.Value)
            {
                document.FileExtension = (string)reader["document_fileExtension"];
            }
            if (reader["document_fileName"] != DBNull.Value)
            {
                document.FileName = (string)reader["document_fileName"];
            }
            if (reader["document_title"] != DBNull.Value)
            {
                document.Title = (string)reader["document_title"];
            }
            if (reader["document_path"] != DBNull.Value)
            {
                document.DocumentPath = (string)reader["document_path"];
            }
            if (reader["ParentFolder"] != DBNull.Value)
            {
                document.ParentFolder = (string)reader["ParentFolder"];
            }
            if (reader["old_parent_folder"] != DBNull.Value)
            {
                document.OldParentFolder = (string)reader["old_parent_folder"];
            }

            if (reader["folder_id"] != DBNull.Value)
            {
                document.FolderId = (int)reader["folder_id"];
            }
            return document;
        }
    }
}
