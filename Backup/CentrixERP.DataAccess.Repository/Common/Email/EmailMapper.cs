using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.Framework;
using SF.FrameworkEntity;
using CentrixERP.Common.Business.Entity;
using System.Data.SqlClient;
using SF.Framework.Data;
using CentrixERP.Configuration;

namespace CentrixERP.Common.DataAccess.Repository
{

    public partial class EmailRepository
    {
        public Email GetEmailMapper(SqlDataReader reader)
        {

            Email email = new Email();
            if (reader["email_type_id"] != DBNull.Value)
            {
                email.Type = (Enums_S3.Email.EmailType)reader["email_type_id"];
            }

            if (reader["email_id"] != DBNull.Value)
            {
                email.Email_Id = (int)reader["email_id"];
            }

            if (reader["email_address"] != DBNull.Value)
            {
                email.EmailAddress = (string)reader["email_address"];
            }
            if (reader["created_by"] != DBNull.Value)
            {
                email.CreatedByName = (string)reader["created_by"];
            }
            if (reader["updated_by"] != DBNull.Value)
            {
                email.UpdatedByName = (string)reader["updated_by"];
            }

            if (reader["crearted_date"] != DBNull.Value)
            {
                email.Crearted_Date = (DateTime)reader["crearted_date"];
            }

            if (reader["updated_date"] != DBNull.Value)
            {
                email.Updated_Date = (DateTime)reader["updated_date"];
            }
            if (reader["description"] != DBNull.Value)
            {
                email.Description = (string)reader["description"];
            }
            return email;
        }


        public Email GetFindEmailMapper(SqlDataReader reader)
        {

            Email email = new Email();
            if (reader["email_address"] != DBNull.Value)
            {
                email.EmailAddress = (string)reader["email_address"];
            }


            return email;
        }


    }
}
