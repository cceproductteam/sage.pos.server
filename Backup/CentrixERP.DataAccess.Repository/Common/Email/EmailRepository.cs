using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.Framework;
using SF.FrameworkEntity;
using CentrixERP.Common.DataAccess.IRepository;
using System.Data.SqlClient;
using SF.Framework.Data;
using CentrixERP.Common.Business.Entity;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class EmailRepository : IEmailRepository
    {
        #region IRepository<Email,int> Members
        public void Add(ref Email myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@Email_address", System.Data.SqlDbType.NVarChar, myEntity.EmailAddress);
            Instance.AddInParameter("@description", System.Data.SqlDbType.NVarChar, myEntity.Description);
            Instance.AddInParameter("@CreatedBy", System.Data.SqlDbType.Int, myEntity.CreatedBy);
            Instance.AddInParameter("@type_id", System.Data.SqlDbType.Int, (int)myEntity.Type);
            Instance.AddOutParameter("@Email_Id", System.Data.SqlDbType.Int);
            try
            {
                Instance.ExcuteNonQuery("dbo.sp_Email_Add", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            var outPutParamValue = Instance.GetOutParamValue("@Email_Id");
            if (outPutParamValue != DBNull.Value)
                myEntity.Email_Id = (int)outPutParamValue;

        }
        public void Delete(ref Email myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@Email_Id", System.Data.SqlDbType.Int, myEntity.Email_Id);
            try
            {
                Instance.ExcuteNonQuery("dbo.sp_Email_Delete", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void DeleteLogical(ref Email myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@Email_Id", System.Data.SqlDbType.Int, myEntity.Email_Id);
            try
            {
                Instance.ExcuteNonQuery("dbo.sp_Email_DeleteLogical", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<Email> FindAll(CriteriaBase myCriteria)
        {
            List<Email> EmailList = new List<Email>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;
            reader = Instance.ExcuteReader("dbo.sp_Email_FindAll", conn);


            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                    EmailList.Add(GetEmailMapper(reader));
                reader.Close();
                return EmailList;
            }
            else
                return null;

        }
        public Email FindById(int Id, CriteriaBase myCriteria)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@Email_Id", System.Data.SqlDbType.Int, Id);
            SqlDataReader reader;

            try
            {
                reader = Instance.ExcuteReader("dbo.sp_Email_FindById", conn);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (reader != null && reader.HasRows)
            {
                reader.Read();
                Email email = GetEmailMapper(reader);
                reader.Close();
                return email;
            }
            else
                return null;
        }
        public List<Email> FindByParentId<TParentId>(TParentId ParentId, Type ParentType, CriteriaBase myCriteria)
        { throw new NotImplementedException(); }

        public void Update(ref Email myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@Email_address", System.Data.SqlDbType.NVarChar, myEntity.EmailAddress);
            Instance.AddInParameter("@UpdatedBy", System.Data.SqlDbType.Int, myEntity.UpdatedBy);
            Instance.AddInParameter("@Email_Id", System.Data.SqlDbType.Int, myEntity.Email_Id);
            Instance.AddInParameter("@description", System.Data.SqlDbType.NVarChar, myEntity.Description);
            Instance.AddInParameter("@type_id", System.Data.SqlDbType.Int, (int)myEntity.Type);

            try
            {
                Instance.ExcuteNonQuery("dbo.sp_Email_Update", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<Email> FindEmailList(string ids, bool forPerson)
        {
            List<Email> EmailList = new List<Email>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;
            Instance.AddInParameter("@ids", System.Data.SqlDbType.NVarChar, ids);
            Instance.AddInParameter("@for_person", System.Data.SqlDbType.Bit, forPerson);
            reader = Instance.ExcuteReader("dbo.SP_QuickEmail_FindAll", conn);


            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                    EmailList.Add(GetFindEmailMapper(reader));
                reader.Close();
                return EmailList;
            }
            else
                return null;
        }

        public string CustomConnectionString
        {
            get;
            set;
        }

        public bool UseCustomConnectionString
        {
            get;
            set;
        }
        public Object BeginTransaction()
        {
            return new object();
        }


        public void CommitTransaction(Object object1)
        {
        }

        public void Rollback(Object object1)
        {

        }

        public void KillConnection()
        {

        }


        public bool UseSharedSession { get; set; }

        #endregion
    }
}
