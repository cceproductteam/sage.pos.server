using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using CentrixERP.Common.Business.Entity;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class SearchCriteriaRepository
    {

        public SearchCriteria GetSearchCriteriaMapper(SqlDataReader reader)
        {
            SearchCriteria searchCriteria = new SearchCriteria();
            if (reader["user_id"] != DBNull.Value)
            {
                searchCriteria.UserId = (int)reader["user_id"];
            }

            if (reader["entity_id"] != DBNull.Value)
            {
                searchCriteria.EntityId = (int)reader["entity_id"];
            }
             if (reader["saved_search_id"] != DBNull.Value)
            {
                searchCriteria.SavedSearchId = (int)reader["saved_search_id"];
            }
             if (reader["search_criteria"] != DBNull.Value)
            {
                searchCriteria.SerachCriteria = (string)reader["search_criteria"];
            }
             if (reader["search_name"] != DBNull.Value)
            {
                searchCriteria.SearchName = (string)reader["search_name"];
            }
             if (reader["created_date"] != DBNull.Value)
             {
                 searchCriteria.CreatedDate = (DateTime)reader["created_date"];
             }
            return searchCriteria;
        }
    }
}
