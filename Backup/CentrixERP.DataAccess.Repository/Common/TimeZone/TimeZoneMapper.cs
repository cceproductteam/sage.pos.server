using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CentrixERP.Common.Business.Entity;
using SF.Framework.Data;
using System.Data.SqlClient;

namespace CentrixERP.Common.DataAccess.Repository
{
    public partial class TimeZoneRepository
    {
        public CentrixERP.Common.Business.Entity.TimeZone GetMapper(SqlDataReader reader)
        {
            CentrixERP.Common.Business.Entity.TimeZone obj = new CentrixERP.Common.Business.Entity.TimeZone();
			if (reader["time_zone_id"] != DBNull.Value)
			{
				obj.TimeZoneId = (int)reader["time_zone_id"];
			}
			if (reader["time_zone"] != DBNull.Value)
			{
				obj.TimeZoneDescription = (string)reader["time_zone"];
			}
			if (reader["created_by"] != DBNull.Value)
			{
				obj.CreatedBy = (int)reader["created_by"];
			}
			if (reader["updated_by"] != DBNull.Value)
			{
				obj.UpdatedBy = (int)reader["updated_by"];
			}
			if (reader["created_date"] != DBNull.Value)
			{
				obj.CreatedDate = (DateTime)reader["created_date"];
			}
			if (reader["updated_date"] != DBNull.Value)
			{
				obj.UpdatedDate = (DateTime)reader["updated_date"];
			}

            return obj;
        }
    }
}
