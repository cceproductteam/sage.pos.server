using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CentrixERP.Business.Entity;
using SF.Framework.Data;
using System.Data.SqlClient;
using CentrixERP.Common.Business.Entity;

namespace CentrixERP.DataAccess.Repository
{
    public partial class PhoneEntityRepository
    {
        public PhoneEntity GetPhoneEntityData(SqlDataReader reader)
        {
            PhoneEntity obj = new PhoneEntity();

            if (reader["phone_entity_id"] != DBNull.Value)
            {
                obj.PhoneEntityId = (int)reader["phone_entity_id"];
            }
            if (reader["phone_id"] != DBNull.Value)
            {
                obj.PhoneId = (int)reader["phone_id"];
            }
            if (reader["entity_id"] != DBNull.Value)
            {
                obj.EntityId = (int)reader["entity_id"];
            }
            if (reader["entity_value_id"] != DBNull.Value)
            {
                obj.EntityValueId = (int)reader["entity_value_id"];
            }
            return obj;
        }
    }
}
