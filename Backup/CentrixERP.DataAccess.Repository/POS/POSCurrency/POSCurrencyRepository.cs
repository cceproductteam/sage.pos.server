
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using CentrixERP.Business.Entity;
using CentrixERP.DataAccess.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace CentrixERP.DataAccess.Repository
{
    public partial class POSCurrencyRepository : RepositoryBaseClass<POSCurrency, POSCurrencyLite, int>, IPOSCurrencyRepository
    {
        #region IRepository< IcCurrency, int > Members

        public POSCurrencyRepository()
        {
            this.AddSPName = "SP_IcCurrency_Add";
            this.UpdateSPName = "SP_IcCurrency_Update";
            this.DeleteSPName = "SP_IcCurrency_Delete";
            this.DeleteLogicalSPName = "SP_IcCurrency_DeleteLogical";
            this.FindAllSPName = "SP_IcCurrency_FindAll";
            this.FindByIdSPName = "SP_IcCurrency_FindById";
            this.FindByParentSPName = "SP_IcCurrency_FindByParentId";
            this.FindAllLiteSPName = "SP_IcCurrency_FindAll_Lite";
            this.FindByIdLiteSPName = "SP_IcCurrency_FindById_Lite";
            this.AdvancedSearchSPName = "SP_IcCurrency_AdvancedSearch_Lite";
        }


        public string SaveIntegration(List<POSCurrency> currencyList)
        {
          
           
            try
            {
                foreach (POSCurrency curr in currencyList)
                {
                    SQLDAHelper Instance = new SQLDAHelper();
                    if (UseCustomConnectionString)
                        Instance.SetConnectionString(CustomConnectionString);
                    SqlConnection conn = Instance.GetConnection(false);
                    Instance.AddInParameter("@currency_id", SqlDbType.Int, curr.CurrencyId);
                    Instance.AddInParameter("@code", SqlDbType.NVarChar, curr.Code);
                    Instance.AddInParameter("@description", SqlDbType.NVarChar, curr.Description);
                    Instance.AddInParameter("@symbol", SqlDbType.NVarChar, curr.Symbol);
                    Instance.AddInParameter("@created_by", SqlDbType.Int, curr.CreatedBy);
                    Instance.AddInParameter("@flag_deleted", SqlDbType.Bit, curr.FlagDeleted);
                    Instance.AddOutParameter("@pos_currency_id", SqlDbType.Int);
                    Instance.ExcuteNonQuery("dbo.SP_IcCurrency_Add", conn, false);
                    curr.PosCurrencyId = (int)Instance.GetOutParamValue("@pos_currency_id");
                }
                return "success";

            }
            catch (Exception ex)
            {

                return ex.Message;
            }

        }

		#endregion
    }
}