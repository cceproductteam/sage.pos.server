
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using CentrixERP.Business.Entity;
using CentrixERP.DataAccess.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace CentrixERP.DataAccess.Repository
{
    public partial class POSPaymentCodeRepository : RepositoryBaseClass<POSPaymentCode, POSPaymentCodeLite, int>, IPOSPaymentCodeRepository
    {
        #region IRepository< PaymentCode, int > Members

        public POSPaymentCodeRepository()
        {
            this.AddSPName = "SP_PaymentCode_Add";
            this.UpdateSPName = "SP_PaymentCode_Update";
            this.DeleteSPName = "SP_PaymentCode_Delete";
            this.DeleteLogicalSPName = "SP_PaymentCode_DeleteLogical";
            this.FindAllSPName = "SP_PaymentCode_FindAll";
            this.FindByIdSPName = "SP_PaymentCode_FindById";
            this.FindByParentSPName = "SP_PaymentCode_FindByParentId";
            this.FindAllLiteSPName = "SP_PaymentCode_FindAll_Lite";
            this.FindByIdLiteSPName = "SP_PaymentCode_FindById_Lite";
            this.AdvancedSearchSPName = "SP_PaymentCode_AdvancedSearch_Lite";
        }

       
		#endregion
    }
}