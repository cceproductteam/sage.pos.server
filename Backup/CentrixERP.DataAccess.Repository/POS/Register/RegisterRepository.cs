
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using CentrixERP.Business.Entity;
using CentrixERP.DataAccess.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace CentrixERP.DataAccess.Repository
{
    public partial class RegisterRepository : RepositoryBaseClass<Register, RegisterLite, int>, IRegisterRepository
    {
        #region IRepository< Register, int > Members

        public RegisterRepository()
        {
            this.AddSPName = "SP_Register_Add";
            this.UpdateSPName = "SP_Register_Update";
            this.DeleteSPName = "SP_Register_Delete";
            this.DeleteLogicalSPName = "SP_Register_DeleteLogical";
            this.FindAllSPName = "SP_Register_FindAll";
            this.FindByIdSPName = "SP_Register_FindById";
            this.FindByParentSPName = "SP_Register_FindByParentId";
            this.FindAllLiteSPName = "SP_Register_FindAll_Lite";
            this.FindByIdLiteSPName = "SP_Register_FindById_Lite";
            this.AdvancedSearchSPName = "SP_Register_AdvancedSearch_Lite";
        }

		#endregion
    }
}