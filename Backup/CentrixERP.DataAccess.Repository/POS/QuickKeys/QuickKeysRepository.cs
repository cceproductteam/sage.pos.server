
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using CentrixERP.Business.Entity;
using CentrixERP.DataAccess.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace CentrixERP.DataAccess.Repository
{
    public partial class QuickKeysRepository : RepositoryBaseClass<QuickKeys, QuickKeysLite, int>, IQuickKeysRepository
    {
        #region IRepository< QuickKeys, int > Members

        public QuickKeysRepository()
        {
            this.AddSPName = "SP_QuickKeys_Add";
            this.UpdateSPName = "SP_QuickKeys_Update";
            this.DeleteSPName = "SP_QuickKeys_Delete";
            this.DeleteLogicalSPName = "SP_QuickKeys_DeleteLogical";
            this.FindAllSPName = "SP_QuickKeys_FindAll";
            this.FindByIdSPName = "SP_QuickKeys_FindById";
            this.FindByParentSPName = "SP_QuickKeys_FindByParentId";
            this.FindAllLiteSPName = "SP_QuickKeys_FindAll_Lite";
            this.FindByIdLiteSPName = "SP_QuickKeys_FindById_Lite";
            this.AdvancedSearchSPName = "SP_QuickKeys_AdvancedSearch_Lite";
        }

		#endregion
    }
}