
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using CentrixERP.Business.Entity;
using CentrixERP.DataAccess.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace CentrixERP.DataAccess.Repository
{
    public partial class GiftVoucherRepository : RepositoryBaseClass<GiftVoucher, GiftVoucherLite, int>, IGiftVoucherRepository
    {
        #region IRepository< GiftVoucher, int > Members

        public GiftVoucherRepository()
        {
            this.AddSPName = "SP_GiftVoucher_Add";
            this.UpdateSPName = "SP_GiftVoucher_Update";
            this.DeleteSPName = "SP_GiftVoucher_Delete";
            this.DeleteLogicalSPName = "SP_GiftVoucher_DeleteLogical";
            this.FindAllSPName = "SP_GiftVoucher_FindAll";
            this.FindByIdSPName = "SP_GiftVoucher_FindById";
            this.FindByParentSPName = "SP_GiftVoucher_FindByParentId";
            this.FindAllLiteSPName = "SP_GiftVoucher_FindAll_Lite";
            this.FindByIdLiteSPName = "SP_GiftVoucher_FindById_Lite";
            this.AdvancedSearchSPName = "SP_GiftVoucher_AdvancedSearch_Lite";
        }
        public GiftVoucher search(string giftCode)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);

            Instance.AddInParameter("@gift_code", System.Data.SqlDbType.NChar, giftCode);



            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_GiftVoucher_Search", conn);
            if (reader != null && reader.HasRows)
            {
                reader.Read();
                GiftVoucher myGift = GetMapper(reader);
                reader.Close();
                return myGift;
            }
            else
            {
                return null;
            }
            
        }

        public void EditVoucher(int id)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);

            Instance.AddInParameter("@id", System.Data.SqlDbType.NChar, id);



            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_GiftVoucher_Edit", conn);
            

        }

		#endregion
    }
}