
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using CentrixERP.Business.Entity;
using CentrixERP.DataAccess.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace CentrixERP.DataAccess.Repository
{
    public partial class POSItemCardRepository : RepositoryBaseClass<POSItemCard, POSItemCardLite, int>, IPOSItemCardRepository
    {
        #region IRepository< IcItemCard, int > Members

        public POSItemCardRepository()
        {
            this.AddSPName = "SP_IcItemCard_Add";
            this.UpdateSPName = "SP_IcItemCard_Update";
            this.DeleteSPName = "SP_IcItemCard_Delete";
            this.DeleteLogicalSPName = "SP_IcItemCard_DeleteLogical";
            this.FindAllSPName = "SP_IcItemCard_FindAll";
            this.FindByIdSPName = "SP_IcItemCard_FindById";
            this.FindByParentSPName = "SP_IcItemCard_FindByParentId";
            this.FindAllLiteSPName = "SP_IcItemCard_FindAll_Lite";
            this.FindByIdLiteSPName = "SP_IcItemCard_FindById_Lite";
            this.AdvancedSearchSPName = "SP_IcItemCard_AdvancedSearch_Lite";
        }




        public BOItemCardQl FindItemQl(CriteriaBase myCriteria)
        {

            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;
            if (myCriteria != null)
            {
                IcItemCardCriteria criteria = (IcItemCardCriteria)myCriteria;
                Instance.AddInParameter("@keyword", SqlDbType.NVarChar, criteria.BarCode);
                Instance.AddInParameter("@customer_id", SqlDbType.NVarChar, criteria.CustomerId);
                Instance.AddInParameter("@currency_code", SqlDbType.NVarChar, criteria.CurrencyCode);
                if (!string.IsNullOrEmpty(criteria.PriceListCode))
                    Instance.AddInParameter("@price_list_code", SqlDbType.NVarChar, criteria.PriceListCode);
            }
            reader = Instance.ExcuteReader("dbo.SP_BO_Item_FindQl", conn);

            if (reader != null && reader.HasRows)
            {
                reader.Read();
                BOItemCardQl BoItemCard = GetQlMapper(reader);
                reader.Close();
                return BoItemCard;
            }
            else
                return null;
        }

        public List<BOItemCardQl> SearchItemQl(CriteriaBase myCriteria)
        {
            List<BOItemCardQl> BoItemCardList = new List<BOItemCardQl>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;
            if (myCriteria != null)
            {
                IcItemCardCriteria criteria = (IcItemCardCriteria)myCriteria;
                if (!string.IsNullOrEmpty(criteria.keyword))
                    Instance.AddInParameter("@keyword", SqlDbType.NVarChar, criteria.keyword);
                if (!string.IsNullOrEmpty(criteria.SearchBarCode))
                    Instance.AddInParameter("@barcode", SqlDbType.NVarChar, criteria.SearchBarCode);
                if (!string.IsNullOrEmpty(criteria.Description))
                    Instance.AddInParameter("@itemDesc", SqlDbType.NVarChar, criteria.Description);
                if (!string.IsNullOrEmpty(criteria.ItemNumber))
                    Instance.AddInParameter("@itemNumber", SqlDbType.NVarChar, criteria.ItemNumber);

                Instance.AddInParameter("@include_zero_quantity", SqlDbType.Bit, criteria.IncludeZeroQuantity);



            }
            reader = Instance.ExcuteReader("dbo.SP_BO_Item_SearchQl", conn);

            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                    BoItemCardList.Add(GetQlMapper(reader));
                reader.Close();
                return BoItemCardList;
            }
            else
                return null;

        }

        public BOItemCardQl SearchItemSerial(CriteriaBase myCriteria,bool status)
        {

            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;
            if (myCriteria != null)
            {
                IcItemCardCriteria criteria = (IcItemCardCriteria)myCriteria;
                Instance.AddInParameter("@item_number", SqlDbType.NVarChar, criteria.ItemNumber);
                Instance.AddInParameter("@item_serial", SqlDbType.NVarChar, criteria.SerialNumber);
                Instance.AddInParameter("@status", SqlDbType.NVarChar, status);
                Instance.AddInParameter("@customer_id", SqlDbType.NVarChar, criteria.CustomerId);
                //Instance.AddInParameter("@serial_no", SqlDbType.NVarChar, criteria.SerialNumber);
                //Instance.AddInParameter("@currency_code", SqlDbType.NVarChar, criteria.CurrencyCode);
                if (!string.IsNullOrEmpty(criteria.PriceListCode))
                    Instance.AddInParameter("@price_list_code", SqlDbType.NVarChar, criteria.PriceListCode);
            }
            reader = Instance.ExcuteReader("dbo.[SP_Ic_CheckSerialNumber]", conn);

            if (reader != null && reader.HasRows)
            {
                reader.Read();
                BOItemCardQl BoItemCard = GetQlMapper(reader);
                reader.Close();
                return BoItemCard;
            }
            else
                return null;
        }



        public string SaveIntegration(List<POSItemCard> itemCardList)
        {
            
            try
            {
               
                foreach (POSItemCard item in itemCardList)
                {
                    SQLDAHelper Instance = new SQLDAHelper();
                    if (UseCustomConnectionString)
                        Instance.SetConnectionString(CustomConnectionString);
                    SqlConnection conn = Instance.GetConnection(false);
                    Instance.AddInParameter("@item_card_id", SqlDbType.Int, item.ItemCardId);
                    Instance.AddInParameter("@item_number", SqlDbType.NVarChar, item.ItemNumber);
                    Instance.AddInParameter("@item_description", SqlDbType.NVarChar, item.ItemDescription);
                    Instance.AddInParameter("@item_bar_code", SqlDbType.NVarChar, item.ItemBarCode);
                    Instance.AddInParameter("@default_price_list_id", SqlDbType.Int, item.DefaultPriceListId);
                    Instance.AddInParameter("@default_price_list", SqlDbType.NVarChar, item.DefaultPriceList);
                    Instance.AddInParameter("@default_price", SqlDbType.Money, item.DefaultPrice);
                    Instance.AddInParameter("@sales_tax_class_id", SqlDbType.Int, item.SalesTaxClassId);
                    Instance.AddInParameter("@sales_tax_class_number", SqlDbType.Int, item.SalesTaxClassNumber);
                    Instance.AddInParameter("@sales_tax_class_description", SqlDbType.NVarChar, item.SalesTaxClassDescription);
                    Instance.AddInParameter("@quantity_on_hand", SqlDbType.Float, item.QuantityOnHand);
                    Instance.AddInParameter("@serial_number", SqlDbType.Bit, item.SerialNumber);
                    Instance.AddInParameter("@unit_of_measure_id", SqlDbType.Int, item.UnitOfMeasureId);
                    Instance.AddInParameter("@created_by", SqlDbType.Int, item.CreatedBy);
                    Instance.AddInParameter("@flag_deleted", SqlDbType.Bit, item.FlagDeleted);
                    Instance.AddOutParameter("@pos_item_card_id", SqlDbType.Int);
                    Instance.ExcuteNonQuery("dbo.SP_IcItemCard_Add", conn, false);
                    item.PosItemCardId = (int)Instance.GetOutParamValue("@pos_item_card_id");
                }
                return "success";

            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }

        public Object BeginTransaction()
        {
            return new object();
        }

        public void CommitTransaction(Object object1)
        {
        }

        public void Rollback(Object object1)
        {

        }
		#endregion





        public List<POSItemCard> FindAll()
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;

            List<POSItemCard> lstItem = new List<POSItemCard>();

            reader = Instance.ExcuteReader("dbo.SP_IcItemCard_FindAll", conn);

            while(reader.Read()) {

                POSItemCard ItemCard = GetMapper(reader);
                
                lstItem.Add(ItemCard);
                
            }
            reader.Close();
            return lstItem;
            
        }
    }
}