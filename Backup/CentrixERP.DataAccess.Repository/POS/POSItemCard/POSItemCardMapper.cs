using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CentrixERP.Business.Entity;
using SF.Framework.Data;
using System.Data.SqlClient;

namespace CentrixERP.DataAccess.Repository
{
    public partial class POSItemCardRepository
    {
        //public IcItemCard GetMapper(SqlDataReader reader)
        //{
        //    IcItemCard obj = new IcItemCard();
        //    //Redaer//Values
        //    return obj;
        //}

        // public IcItemCardLite GetMapperAllLite(SqlDataReader reader)
        //{
        //    IcItemCardLite obj = new IcItemCardLite();
        //    //Redaer//Values
        //    obj.Id = obj.PosItemCardId;
        //    return obj;
        //}

        // public IcItemCardLite GetLiteMapper(SqlDataReader reader)
        //{
        //    IcItemCardLite obj = new IcItemCardLite();
        //    //Redaer//Values
        //         if (reader["created_by_name"] != DBNull.Value)
        //    {
        //        obj.CreatedByName = (string)reader["created_by_name"];
        //    }
        //    if (reader["updated_by_name"] != DBNull.Value)
        //    {
        //        obj.UpdatedByName = (string)reader["updated_by_name"];
        //    }
        //    return obj;
        //}




        public BOItemCardQl GetQlMapper(SqlDataReader reader)
        {
            BOItemCardQl obj = new BOItemCardQl();
            if (reader["item_number"] != DBNull.Value)
            {
                obj.ItemNumber = reader["item_number"].ToString();
            }
            if (reader["item_bar_code"] != DBNull.Value)
            {
                obj.ItemBarCode = reader["item_bar_code"].ToString();
            }
            if (reader["item_description"] != DBNull.Value)
            {
                obj.ItemDesc = reader["item_description"].ToString();
            }
            if (reader["prli_price"] != DBNull.Value)
            {
                obj.ItemPrice = (decimal)reader["prli_price"];
            }
            if (reader["prli_priceListCode"] != DBNull.Value)
            {
                obj.ItemPriceList = reader["prli_priceListCode"].ToString();
            }
            if (reader["unit_of_measure_id"] != DBNull.Value)
            {
                obj.ItemUnit = reader["unit_of_measure_id"].ToString();
            }
            if (reader["ittx_taxauth"] != DBNull.Value)
            {
                obj.ItemTaxAuth = reader["ittx_taxauth"].ToString();
            }
            if (reader["mtrx_rate"] != DBNull.Value)
            {
                obj.ItemTaxRate = (decimal)reader["mtrx_rate"];
            }
            if (reader["is_serial_item"] != DBNull.Value)
            {
                obj.ItemSerial = (bool)reader["is_serial_item"];
            }
            if (reader["sril_serialno"] != DBNull.Value)
            {
                obj.SerialNo = reader["sril_serialno"].ToString();
            }

            if (reader["quantity_on_hand"] != DBNull.Value)
            {
                obj.AvailableQuantity =Convert.ToInt32(reader["quantity_on_hand"]);
            }

            
            return obj;
        }
    }
}
