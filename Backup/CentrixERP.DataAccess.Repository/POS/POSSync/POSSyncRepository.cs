﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using CentrixERP.Business.Entity;
using CentrixERP.DataAccess.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace CentrixERP.DataAccess.Repository
{

    public partial class POSSyncRepository : RepositoryBaseClass<POSSync, POSSyncLite, int>, IPOSSyncRepository
    {
        #region IRepository<POSSync, int > Members

        public POSSyncRepository()
        {
            this.AdvancedSearchSPName = "SP_POSSync_AdvancedSearch_Lite";
        }

        public void POSSyncData(Object[] data)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;
           // Instance.AddInParameter("@", SqlDbType.Int, );
            reader = Instance.ExcuteReader("dbo.SP_POS_Sync", conn);

        }

        #endregion
    }
}
