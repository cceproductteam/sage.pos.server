
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using CentrixERP.Business.Entity;
using CentrixERP.DataAccess.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace CentrixERP.DataAccess.Repository
{
    public partial class PersonRepository : RepositoryBaseClass<Person, PersonLite, int>, IPersonRepository
    {
        #region IRepository< Person, int > Members

        public PersonRepository()
        {
            this.AddSPName = "SP_Person_Add";
            this.UpdateSPName = "SP_Person_Update";
            this.DeleteSPName = "SP_Person_Delete";
            this.DeleteLogicalSPName = "SP_Person_DeleteLogical";
            this.FindAllSPName = "SP_Person_FindAll";
            this.FindByIdSPName = "SP_Person_FindById";
            this.FindByParentSPName = "SP_Person_FindByParentId";
            this.FindAllLiteSPName = "SP_Person_FindAll_Lite";
            this.FindByIdLiteSPName = "SP_Person_FindById_Lite";
            this.AdvancedSearchSPName = "SP_Person_AdvancedSearch_Lite";
        }


        public List<PersonLite> GetAllPersonBirthOfDateFindAll(PersonCriteria myCriteria)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;
            List<PersonLite> myPersonLiteLite;
            if (myCriteria != null)
            {
                PersonCriteria criteria = (PersonCriteria)myCriteria;
                if (!myCriteria.NotificationOccasion.Equals(-1))
                    Instance.AddInParameter("@NotificationOccasion", SqlDbType.NVarChar, criteria.NotificationOccasion);
                if (!myCriteria.NotificationOccasion.Equals(-1))
                    Instance.AddInParameter("@EntityId", SqlDbType.NVarChar, criteria.EntityId);
            }
            reader = Instance.ExcuteReader("dbo.SP_Person_BirthOfDate_Lite", conn);
            if (reader != null && reader.HasRows)
            {
                myPersonLiteLite = new List<PersonLite>();
                while (reader.Read())
                    myPersonLiteLite.Add(GetPersonNotificationMapper(reader));
                reader.Close();
                return myPersonLiteLite;
            }
            else
                return null;
        }
        public List<PersonLite> GetAllPersonAnniversaryFindAll(PersonCriteria myCriteria)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;
            List<PersonLite> myPersonLiteLite;
            if (myCriteria != null)
            {
                PersonCriteria criteria = (PersonCriteria)myCriteria;
                if (!myCriteria.NotificationOccasion.Equals(-1))
                    Instance.AddInParameter("@NotificationOccasion", SqlDbType.NVarChar, criteria.NotificationOccasion);
                if (!myCriteria.NotificationOccasion.Equals(-1))
                    Instance.AddInParameter("@EntityId", SqlDbType.NVarChar, criteria.EntityId);
            }
            reader = Instance.ExcuteReader("dbo.SP_Person_Anniversary_Lite", conn);
            if (reader != null && reader.HasRows)
            {
                myPersonLiteLite = new List<PersonLite>();
                while (reader.Read())
                    myPersonLiteLite.Add(GetPersonNotificationMapper(reader));
                reader.Close();
                return myPersonLiteLite;
            }
            else
                return null;
        }
        public void UpdateSyncStatus(string trasactionNumber, int EntityId)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            if (UseCustomConnectionString)
                Instance.SetConnectionString(CustomConnectionString);
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@transaction_number", SqlDbType.NVarChar, trasactionNumber);
            Instance.AddInParameter("@entity_id", SqlDbType.Int, EntityId);

            Instance.ExcuteNonQuery("SP_UpdateSyncStatus", conn, false);
        }

		#endregion
    }
}