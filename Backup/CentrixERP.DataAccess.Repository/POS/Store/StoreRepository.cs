
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using CentrixERP.Business.Entity;
using CentrixERP.DataAccess.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace CentrixERP.DataAccess.Repository
{
    public partial class StoreRepository : RepositoryBaseClass<Store, StoreLite, int>, IStoreRepository
    {
        #region IRepository< Store, int > Members

        public StoreRepository()
        {
            this.AddSPName = "SP_Store_Add";
            this.UpdateSPName = "SP_Store_Update";
            this.DeleteSPName = "SP_Store_Delete";
            this.DeleteLogicalSPName = "SP_Store_DeleteLogical";
            this.FindAllSPName = "SP_Store_FindAll";
            this.FindByIdSPName = "SP_Store_FindById";
            this.FindByParentSPName = "SP_Store_FindByParentId";
            this.FindAllLiteSPName = "SP_Store_FindAll_Lite";
            this.FindByIdLiteSPName = "SP_Store_FindById_Lite";
            this.AdvancedSearchSPName = "SP_Store_AdvancedSearch_Lite";
        }

		#endregion

    }
}