
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using CentrixERP.Business.Entity;
using CentrixERP.DataAccess.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace CentrixERP.DataAccess.Repository
{
    public partial class SyncScheduleRepository : RepositoryBaseClass<SyncSchedule, SyncScheduleLite, int>, ISyncScheduleRepository
    {
        #region IRepository< SyncSchedule, int > Members

        public SyncScheduleRepository()
        {
            this.AddSPName = "SP_SyncSchedule_Add";
            this.UpdateSPName = "SP_SyncSchedule_Update";
            this.DeleteSPName = "SP_SyncSchedule_Delete";
            this.DeleteLogicalSPName = "SP_SyncSchedule_DeleteLogical";
            this.FindAllSPName = "SP_SyncSchedule_FindAll";
            this.FindByIdSPName = "SP_SyncSchedule_FindById";
            this.FindByParentSPName = "SP_SyncSchedule_FindByParentId";
            this.FindAllLiteSPName = "SP_SyncSchedule_FindAll_Lite";
            this.FindByIdLiteSPName = "SP_SyncSchedule_FindById_Lite";
            this.AdvancedSearchSPName = "SP_SyncSchedule_AdvancedSearch_Lite";
        }
        public List<SyncSchedule> FindAllToSync(int status)
        {
            List<SyncSchedule> syncEntity = new List<SyncSchedule>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;

            Instance.AddInParameter("@status", SqlDbType.Int, status);

            reader = Instance.ExcuteReader("dbo.SP_SyncSchedule_FindAll_to_sync", conn);

            if (reader != null && reader.HasRows)
            {

                while (reader.Read())
                    syncEntity.Add(GetMapper(reader));
                reader.Close();
                return syncEntity;
            }
            else
                return null;
        }

        //public string CustomConnectionString
        //{
        //    get;
        //    set;
        //}

        //public bool UseCustomConnectionString
        //{
        //    get;
        //    set;
        //}

        //public Object BeginTransaction()
        //{
        //    return new object();
        //}

        //public void CommitTransaction(Object object1)
        //{
        //}

        //public void Rollback(Object object1)
        //{

        //}

		#endregion
    }
}