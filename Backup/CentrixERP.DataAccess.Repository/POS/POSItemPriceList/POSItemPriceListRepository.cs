
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using CentrixERP.Business.Entity;
using CentrixERP.DataAccess.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace CentrixERP.DataAccess.Repository
{
    public partial class POSItemPriceListRepository : RepositoryBaseClass<POSItemPriceList, POSItemPriceListLite, int>, IPOSItemPriceListRepository
    {
        #region IRepository< IcItemPriceList, int > Members

        public POSItemPriceListRepository()
        {
            this.AddSPName = "SP_IcItemPriceList_Add";
            this.UpdateSPName = "SP_IcItemPriceList_Update";
            this.DeleteSPName = "SP_IcItemPriceList_Delete";
            this.DeleteLogicalSPName = "SP_IcItemPriceList_DeleteLogical";
            this.FindAllSPName = "SP_IcItemPriceList_FindAll";
            this.FindByIdSPName = "SP_IcItemPriceList_FindById";
            this.FindByParentSPName = "SP_IcItemPriceList_FindByParentId";
            this.FindAllLiteSPName = "SP_IcItemPriceList_FindAll_Lite";
            this.FindByIdLiteSPName = "SP_IcItemPriceList_FindById_Lite";
            this.AdvancedSearchSPName = "SP_IcItemPriceList_AdvancedSearch_Lite";
        }


        public string SaveIntegration(List<POSItemPriceList> ItemPriceList)
        {
           
            try
            {
                foreach (POSItemPriceList item in ItemPriceList)
                {
                    SQLDAHelper Instance = new SQLDAHelper();
                    if (UseCustomConnectionString)
                        Instance.SetConnectionString(CustomConnectionString);
                    SqlConnection conn = Instance.GetConnection(false);
                    Instance.AddInParameter("@item_price_list_id", SqlDbType.Int, item.ItemPriceListId);
                    Instance.AddInParameter("@item_card_id", SqlDbType.Int, item.ItemCardId);
                    Instance.AddInParameter("@item_bar_code", SqlDbType.NVarChar, item.ItemBarCode);
                    Instance.AddInParameter("@item_number", SqlDbType.NVarChar, item.ItemNumber);
                    Instance.AddInParameter("@price_list_group_id", SqlDbType.Int, item.PriceListGroupId);
                    Instance.AddInParameter("@price_list_id", SqlDbType.Int, item.PriceListId);
                    Instance.AddInParameter("@price_list_code", SqlDbType.NVarChar, item.PriceListCode);
                    Instance.AddInParameter("@price_list_description", SqlDbType.NVarChar, item.PriceListDescription);
                    Instance.AddInParameter("@base_price", SqlDbType.Money, item.BasePrice);
                    Instance.AddInParameter("@created_by", SqlDbType.Int, item.CreatedBy);
                    Instance.AddInParameter("@flag_deleted", SqlDbType.Bit, item.FlagDeleted);
                    Instance.AddOutParameter("@pos_item_price_list_id", SqlDbType.Int);
                    Instance.ExcuteNonQuery("dbo.SP_IcItemPriceList_Add", conn, false);
                    item.PosItemPriceListId = (int)Instance.GetOutParamValue("@pos_item_price_list_id");
                }
                return "success";

            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }

		#endregion
    }
}