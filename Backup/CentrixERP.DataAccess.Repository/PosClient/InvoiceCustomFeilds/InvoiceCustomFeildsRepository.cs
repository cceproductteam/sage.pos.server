
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
//using Centrix.POS.Standalone.Client.Business.Entity;
//using Centrix.POS.Standalone.Client.DataAccess.IRepository;

using CentrixERP.Business.Entity;
using CentrixERP.DataAccess.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace CentrixERP.DataAccess.Repository
{
    public partial class InvoiceCustomFeildsRepository : RepositoryBaseClass<InvoiceCustomFeilds, InvoiceCustomFeildsLite, int>, IInvoiceCustomFeildsRepository
    {
        #region IRepository< InvoiceCustomFeilds, int > Members

        public InvoiceCustomFeildsRepository()
        {
            this.AddSPName = "SP_InvoiceCustomFeilds_Add";
            this.UpdateSPName = "SP_InvoiceCustomFeilds_Update";
            this.DeleteSPName = "SP_InvoiceCustomFeilds_Delete";
            this.DeleteLogicalSPName = "SP_InvoiceCustomFeilds_DeleteLogical";
            this.FindAllSPName = "SP_InvoiceCustomFeilds_FindAll";
            this.FindByIdSPName = "SP_InvoiceCustomFeilds_FindById";
            this.FindByParentSPName = "SP_InvoiceCustomFeilds_FindByParentId";
            this.FindAllLiteSPName = "SP_InvoiceCustomFeilds_FindAll_Lite";
            this.FindByIdLiteSPName = "SP_InvoiceCustomFeilds_FindById_Lite";
            this.AdvancedSearchSPName = "SP_InvoiceCustomFeilds_AdvancedSearch_Lite";
        }

		#endregion
    }
}