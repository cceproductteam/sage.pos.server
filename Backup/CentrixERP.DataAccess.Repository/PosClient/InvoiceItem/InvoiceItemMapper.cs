using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using Centrix.POS.Standalone.Client.Business.Entity;
using CentrixERP.Business.Entity;
using SF.Framework.Data;
using System.Data.SqlClient;

namespace CentrixERP.DataAccess.Repository
{
    public partial class InvoiceItemRepository
    {



        public InvoiceItemIntegration GetIntegrationMapper(SqlDataReader reader)
        {
            InvoiceItemIntegration obj = new InvoiceItemIntegration();

            if (reader["item_card_id"] != DBNull.Value)
                obj.ItemId = (int)reader["item_card_id"];

            if (reader["unit_of_measure_id"] != DBNull.Value)
                obj.UnitOfMeasureId = (int)reader["unit_of_measure_id"];

            if (reader["quantity"] != DBNull.Value)
                obj.Quantity = (float)reader["quantity"];

            if (reader["unit_price"] != DBNull.Value)
                obj.UnitPrice = Convert.ToDouble(reader["unit_price"]);



            if (reader["percentage_discount"] != DBNull.Value)
                obj.LineDiscountPercent =  Convert.ToDouble(reader["percentage_discount"]);
            if (reader["amount_discount"] != DBNull.Value)
                obj.DiscountAmount =  Convert.ToDouble(reader["amount_discount"]);
            if (reader["line_total_after_discount"] != DBNull.Value)
                obj.LineTotalAfterDiscount = Convert.ToDouble(reader["line_total_after_discount"]);
            if (reader["total_price"] != DBNull.Value)
                obj.TotalLineAmount = Convert.ToDouble(reader["total_price"]);
            if (reader["tax"] != DBNull.Value)
                obj.TaxRate = Convert.ToDouble(reader["tax"]);
            if (reader["tax_amount"] != DBNull.Value)
                obj.TaxAmount = Convert.ToDouble(reader["tax_amount"]);
            if (reader["tax_class_id"] != DBNull.Value)
                obj.TaxClassId = (int)reader["tax_class_id"];
            if (reader["is_serial"] != DBNull.Value)
                obj.IsSerial = Convert.ToBoolean(reader["is_serial"]);
            if (reader["serial_number"] != DBNull.Value)
                obj.SerialNumber = Convert.ToString(reader["serial_number"]);
            if (reader["category_id"] != DBNull.Value)
                obj.CategoryId = Convert.ToInt32(reader["category_id"]);

            if (reader["cost"] != DBNull.Value)
                obj.UnitCost = Convert.ToDouble(reader["cost"]);
            if (reader["price_list_id"] != DBNull.Value)
                obj.PriceListId = Convert.ToInt32(reader["price_list_id"]);

            if (reader["cash_total"] != DBNull.Value)
                obj.CashTotal = Convert.ToDouble(reader["cash_total"]);

            return obj;
        }
        //public InvoiceItem GetMapper(SqlDataReader reader)
        //{
        //    InvoiceItem obj = new InvoiceItem();
        //    //Redaer//Values
        //    return obj;
        //}

        // public InvoiceItemLite GetMapperAllLite(SqlDataReader reader)
        //{
        //    InvoiceItemLite obj = new InvoiceItemLite();
        //    //Redaer//Values
        //    obj.Id = obj.InvoiceItemId;
        //    return obj;
        //}

        // public InvoiceItemLite GetLiteMapper(SqlDataReader reader)
        //{
        //    InvoiceItemLite obj = new InvoiceItemLite();
        //    //Redaer//Values
        //         if (reader["created_by_name"] != DBNull.Value)
        //    {
        //        obj.CreatedByName = (string)reader["created_by_name"];
        //    }
        //    if (reader["updated_by_name"] != DBNull.Value)
        //    {
        //        obj.UpdatedByName = (string)reader["updated_by_name"];
        //    }
        //    return obj;
        //}
    }
}
