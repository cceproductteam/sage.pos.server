
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
//using Centrix.POS.Standalone.Client.Business.Entity;
//using Centrix.POS.Standalone.Client.DataAccess.IRepository;
using CentrixERP.Business.Entity;
using CentrixERP.DataAccess.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace CentrixERP.DataAccess.Repository
{
    public partial class InvoiceItemRepository : RepositoryBaseClass<InvoiceItem, InvoiceItemLite, int>, IInvoiceItemRepository
    {
        #region IRepository< InvoiceItem, int > Members

        public InvoiceItemRepository()
        {
            this.AddSPName = "SP_InvoiceItem_Add";
            this.UpdateSPName = "SP_InvoiceItem_Update";
            this.DeleteSPName = "SP_InvoiceItem_Delete";
            this.DeleteLogicalSPName = "SP_InvoiceItem_DeleteLogical";
            this.FindAllSPName = "SP_InvoiceItem_FindAll";
            this.FindByIdSPName = "SP_InvoiceItem_FindById";
            this.FindByParentSPName = "SP_InvoiceItem_FindByParentId";
            this.FindAllLiteSPName = "SP_InvoiceItem_FindAll_Lite";
            this.FindByIdLiteSPName = "SP_InvoiceItem_FindById_Lite";
            this.AdvancedSearchSPName = "SP_InvoiceItem_AdvancedSearch_Lite";
        }

        public List<InvoiceItemIntegration> InvoiceItemIntegrationFindAll(CriteriaBase myCriteria)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            List<InvoiceItemIntegration> invList = new List<InvoiceItemIntegration>();
            SqlDataReader reader;
            if (myCriteria != null)
            {
                InvoiceItemCriteria criteria = (InvoiceItemCriteria)myCriteria;
                Instance.AddInParameter("@invoice_unique_number", SqlDbType.NVarChar, criteria.InvoiceUniqueNumber);
                Instance.AddInParameter("@location_id", SqlDbType.Int, criteria.LocationId);
                Instance.AddInParameter("@is_exchanged", SqlDbType.Int, criteria.IsExchanged);

            }
            reader = Instance.ExcuteReader("SP_InvoiceItemIntegration_FindByInvoiceIds", conn);

            if (reader.HasRows && reader != null)
            {
                while (reader.Read())
                    invList.Add(GetIntegrationMapper(reader));
                reader.Close();
                return invList;
            }
            else
                return null;
        }

		#endregion
    }
}