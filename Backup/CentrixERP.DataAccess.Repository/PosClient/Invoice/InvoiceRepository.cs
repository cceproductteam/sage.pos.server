
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;

using CentrixERP.Business.Entity;
using CentrixERP.DataAccess.IRepository;
using System.Data.SqlClient;
using System.Data;
using System.Globalization;

namespace CentrixERP.DataAccess.Repository
{
    public partial class InvoiceRepository : RepositoryBaseClass<Invoice, InvoiceLite, int>, IInvoiceRepository
    {
        #region IRepository< Invoice, int > Members

        public InvoiceRepository()
        {
            this.AddSPName = "SP_Invoice_Add";
            this.UpdateSPName = "SP_Invoice_Update";
            this.DeleteSPName = "SP_Invoice_Delete";
            this.DeleteLogicalSPName = "SP_Invoice_DeleteLogical";
            this.FindAllSPName = "SP_Invoice_FindAll";
            this.FindByIdSPName = "SP_Invoice_FindById";
            this.FindByParentSPName = "SP_Invoice_FindByParentId";
            this.FindAllLiteSPName = "SP_Invoice_FindAll_Lite";
            this.FindByIdLiteSPName = "SP_Invoice_FindById_Lite";
            this.AdvancedSearchSPName = "SP_Invoice_AdvancedSearch_Lite";
        }

        #endregion


        public Invoice FindLastInvoice(CriteriaBase myCriteria)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);

            SqlDataReader reader;
            if (myCriteria != null)
            {
                InvoiceCriteria criteria = (InvoiceCriteria)myCriteria;
                Instance.AddInParameter("@register_id", SqlDbType.Int, criteria.RegisterId);
            }

            reader = Instance.ExcuteReader("SP_Invoice_FindLastInvoice", conn);

            if (reader != null && reader.HasRows)
            {
                reader.Read();
                Invoice myInvoice = GetMapper(reader);
                reader.Close();
                return myInvoice;
            }
            else
                return null;
        }


        public Invoice FindInvoiceByNumber(CriteriaBase myCriteria)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;
            InvoiceCriteria criteria = (InvoiceCriteria)myCriteria;
            if (criteria.Status != 0)
                Instance.AddInParameter("@status", SqlDbType.Int, criteria.Status);
            if (criteria.RegisterId != 0)
                Instance.AddInParameter("@register_id", SqlDbType.Int, criteria.RegisterId);
            if (criteria.InvoiceNumber != "" && criteria.InvoiceNumber != null)
                Instance.AddInParameter("@invoice_number", SqlDbType.NVarChar, criteria.InvoiceNumber);
            if (criteria.StoreId != -1)
                Instance.AddInParameter("@store_id", SqlDbType.Int, criteria.StoreId);


            reader = Instance.ExcuteReader("dbo.SP_Invoice_FindByNumber", conn);


            while (reader.Read())
            {
                Invoice myInvoice = GetMapper(reader);
                reader.Close();
                return myInvoice;
            }
            return null;



            //if (reader != null && reader.HasRows)
            //{
            //    while (reader.Read())
            //        reader.Read();

            //    Invoice myInvoice = GetMapper(reader);
            //    reader.Close();
            //    return myInvoice;

            //}
            //else
            //    return null;
        }





        public List<Invoice> FindAllQl(CriteriaBase myCriteria, string phoneNumber)
        {
            List<Invoice> InvoiceList = new List<Invoice>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;
            if (myCriteria != null)
            {
                InvoiceCriteria criteria = (InvoiceCriteria)myCriteria;
                if (!string.IsNullOrEmpty(criteria.TransactionNumber))
                    Instance.AddInParameter("@transaction_number", SqlDbType.NVarChar, criteria.TransactionNumber);
                if (criteria.CustomerId > 0)
                    Instance.AddInParameter("@customer_id", SqlDbType.Int, criteria.CustomerId);
                if (criteria.DateFrom != null)
                {
                    if (!string.IsNullOrEmpty(criteria.DateFrom.Trim()) && !criteria.DateFrom.Contains("undefined"))
                        Instance.AddInParameter("@date_from", SqlDbType.DateTime, Convert.ToDateTime(criteria.DateFrom));
                }
                if (criteria.DateTo != null)
                {
                    if (!string.IsNullOrEmpty(criteria.DateTo.Trim())&&!criteria.DateFrom.Contains("undefined"))
                        Instance.AddInParameter("@date_to", SqlDbType.DateTime, Convert.ToDateTime(criteria.DateTo));
                }
                if (criteria.TypeId > 0)
                    Instance.AddInParameter("@type_id", SqlDbType.Int, criteria.TypeId);
                if (criteria.RegisterId > 0)
                    Instance.AddInParameter("@register_id", SqlDbType.Int, criteria.RegisterId);
                if (!string.IsNullOrEmpty(criteria.CustomerName))
                    Instance.AddInParameter("@cutomer_name", SqlDbType.NVarChar, criteria.CustomerName);
                if (!string.IsNullOrEmpty(phoneNumber))
                    Instance.AddInParameter("@phone_number", SqlDbType.NVarChar, phoneNumber);

            }
            reader = Instance.ExcuteReader("SP_Invoice_FindAllQl", conn);
            
                while (reader.Read())
                    InvoiceList.Add(GetMapper(reader));
                reader.Close();
                return InvoiceList;
            
           
                return null;
        }


        public List<Invoice> FindByCustomer(CriteriaBase myCriteria)
        {
            List<Invoice> InvoiceList = new List<Invoice>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;
            if (myCriteria != null)
            {
                InvoiceCriteria criteria = (InvoiceCriteria)myCriteria;
                Instance.AddInParameter("@customer_id", SqlDbType.Int, criteria.CustomerId);
                Instance.AddInParameter("@status", SqlDbType.Int, criteria.Status);
                //Instance.AddInParameter("@customer_type", SqlDbType.Int, criteria.CustomerType);
            }
            reader = Instance.ExcuteReader("SP_Invoice_FindByCustomerId", conn);
            if (reader.HasRows && reader != null)
            {
                while (reader.Read())
                    InvoiceList.Add(GetMapper(reader));
                reader.Close();
                return InvoiceList;
            }
            else
                return null;

        }

        public List<InvoiceIntegration> InvoiceIntegrationFindAll(CriteriaBase myCriteria)
        {
            List<InvoiceIntegration> InvoiceList = new List<InvoiceIntegration>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;
            if (myCriteria != null)
            {
                InvoiceCriteria criteria = (InvoiceCriteria)myCriteria;
                Instance.AddInParameter("@is_converted_to_shipment_return", SqlDbType.Bit, criteria.IsConvertedToshipmentReturn);
                Instance.AddInParameter("@invoice_type", SqlDbType.NVarChar, criteria.InvoiceType);
                Instance.AddInParameter("@store_id", SqlDbType.Int, criteria.StoreId);
                Instance.AddInParameter("@is_converted_to_shipment", SqlDbType.Int, criteria.IsConvertedToShipment);
            }

            reader = Instance.ExcuteReader("SP_invoice_integration_findAll", conn);

            if (reader.HasRows && reader != null)
            {
                while (reader.Read())
                    InvoiceList.Add(GetIntegrationMapper(reader));
                reader.Close();
                return InvoiceList;
            }
            else
                return null;
        }



        public void UpdateTransactionStatus(int transactionType, string transactionRef, string transactionNumber, string status, bool IsConvertedToShipment)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@transaction_type", SqlDbType.Int, transactionType);
            Instance.AddInParameter("@transaction_ref", SqlDbType.NVarChar, transactionRef);
            Instance.AddInParameter("@transaction_number", SqlDbType.NVarChar, transactionNumber);
            Instance.AddInParameter("@is_converted_to_shipment", SqlDbType.Bit, IsConvertedToShipment);
            Instance.AddInParameter("@status", SqlDbType.NVarChar, status);
            Instance.ExcuteNonQuery("SP_UpdateTransactionStatus", conn, false);
        }

        public void UpdateSyncStatus(string trasactionNumber, int EntityId)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            if (UseCustomConnectionString)
                Instance.SetConnectionString(CustomConnectionString);
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@transaction_number", SqlDbType.NVarChar, trasactionNumber);
            Instance.AddInParameter("@entity_id", SqlDbType.Int, EntityId);

            Instance.ExcuteNonQuery("SP_UpdateSyncStatus", conn, false);
        }

        public List<InvoiceLite> searchRefund(CriteriaBase myCriteria)
        {
            List<InvoiceLite> InvoiceList = new List<InvoiceLite>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;
            if (myCriteria != null)
            {
                InvoiceCriteria criteria = (InvoiceCriteria)myCriteria;
                Instance.AddInParameter("@customer_name", SqlDbType.NVarChar, criteria.CustomerName);
                Instance.AddInParameter("@invoice_number", SqlDbType.NVarChar, criteria.InvoiceNumber);
                if (!string.IsNullOrEmpty(criteria.DateFrom.Trim()))
                    Instance.AddInParameter("@date_from", SqlDbType.NVarChar, criteria.DateFrom);
                if (!string.IsNullOrEmpty(criteria.DateTo.Trim()))
                    Instance.AddInParameter("@date_to", SqlDbType.NVarChar, criteria.DateTo);
                
                    Instance.AddInParameter("@is_refund", SqlDbType.Bit, criteria.isRefund);
            }

            reader = Instance.ExcuteReader("SP_SearchRefund", conn);

            if (reader.HasRows && reader != null)
            {
                while (reader.Read())
                    InvoiceList.Add(GetLiteMapper(reader));
                reader.Close();
                return InvoiceList;
            }
            else
                return null;
        }


        public void UpdateInvoiceTransNumber(int invoiceId, int shipmentId, string shipmentNumber)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@invoice_id", SqlDbType.Int, invoiceId);
            Instance.AddInParameter("@shipment_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(shipmentId));
            Instance.AddInParameter("@shipment_number", SqlDbType.NVarChar, shipmentNumber);
            Instance.ExcuteNonQuery("SP_Invoice_UpdatePartial", conn, false);
        }


        public List<InvoiceInquiryLite> InvoiceInquiryResults(string StoreIds, DateTime? InvoiceDateFrom, DateTime? InvoiceDateTo)
        {
            List<InvoiceInquiryLite> resultsList = new List<InvoiceInquiryLite>();
            SQLDAHelper instance = new SQLDAHelper();
            SqlConnection conn = instance.GetConnection(false);
            instance.AddInParameter("@store_ids", SqlDbType.NVarChar, StoreIds);
            instance.AddInParameter("@date_from", SqlDbType.NVarChar, InvoiceDateFrom);
            instance.AddInParameter("@date_to", SqlDbType.NVarChar, InvoiceDateTo);
            SqlDataReader reader = instance.ExcuteReader("SP_ERP_Integration_Invoice_Inquiry", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    resultsList.Add(GetInvoiceInquiryMapper(reader));
                }
                return resultsList;
            }
            else
            {
                return null;
            }
        }


        public void UpdateAccpacInvoiceNumber(string invoiceUniuqeNumber, string AccpacInvoiceNum, string AccpacOrderentryNum, string AccpacShipmentNum, string AccpacPrePaymnentNumber, string accpacRefundNumber)
        {

            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@invoice_unique_number", SqlDbType.NVarChar, invoiceUniuqeNumber);
            Instance.AddInParameter("@accpc_invoice_number", SqlDbType.NVarChar, AccpacInvoiceNum);

            Instance.AddInParameter("@accpac_order_entry_number", SqlDbType.NVarChar, AccpacOrderentryNum);
            Instance.AddInParameter("@accpac_shipment_number", SqlDbType.NVarChar, AccpacShipmentNum);
            Instance.AddInParameter("@accpac_prepyament_no", SqlDbType.NVarChar, AccpacPrePaymnentNumber);
            Instance.AddInParameter("@accpac_refund_no", SqlDbType.NVarChar, accpacRefundNumber);
            Instance.ExcuteNonQuery("sp_update_accpac_invoice_number", conn, false);


        }



        public List<POSQuantityInQuiryLite> FindInquiryResults(int LocationId, int StoreId, int ItemId)
        {
            List<POSQuantityInQuiryLite> resultsList = new List<POSQuantityInQuiryLite>();
            SQLDAHelper instance = new SQLDAHelper();
            SqlConnection conn = instance.GetConnection(false);
            instance.AddInParameter("@location_id", SqlDbType.Int, LocationId);
            instance.AddInParameter("@store_id", SqlDbType.Int, StoreId);
            instance.AddInParameter("@item_id", SqlDbType.Int, ItemId);
            SqlDataReader reader = instance.ExcuteReader("SP_QuantityInquiry_FindAll", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    resultsList.Add(GetQuantityInquiryMapper(reader));
                }
                return resultsList;
            }
            else
                return null;
        }

      public  List<ICQuantityInQuiryLite> FindICInquiryResults(int LocationId, int StoreId, int ItemId)
        {
            List<ICQuantityInQuiryLite> resultsList = new List<ICQuantityInQuiryLite>();
            SQLDAHelper instance = new SQLDAHelper();
            SqlConnection conn = instance.GetConnection(false);
            instance.AddInParameter("@location_id", SqlDbType.Int, LocationId);
            instance.AddInParameter("@store_id", SqlDbType.Int, StoreId);
            instance.AddInParameter("@item_id", SqlDbType.Int, ItemId);
            SqlDataReader reader = instance.ExcuteReader("SP_InventoryQuantityInquiry_FindAll", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    resultsList.Add(GetICQuantityInquiryMapper(reader));
                }
                return resultsList;
            }
            else
                return null;
        }


        public bool CheckParentShipmentValidity()
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
          
            Instance.AddOutParameter("@flag_parent_inv_notPosted", SqlDbType.Bit);

            Instance.ExcuteNonQuery("dbo.SP_CheckRefundInvoicesBeforePostingRefunds", conn, false);

            return (bool)Instance.GetOutParamValue("@flag_parent_inv_notPosted");


        }




       
    }
}