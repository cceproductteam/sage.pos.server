using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using CentrixERP.Business.Entity;
using SF.Framework.Data;
using System.Data.SqlClient;

namespace CentrixERP.DataAccess.Repository
{
    public partial class InvoiceRepository
    {
        //public Invoice GetMapper(SqlDataReader reader)
        //{
        //    Invoice obj = new Invoice();
        //    //Redaer//Values
        //    return obj;s
        //}



        public InvoiceIntegration GetIntegrationMapper(SqlDataReader reader)
        {
            InvoiceIntegration obj = new InvoiceIntegration();
            if (reader["parent_invoice_id"] != DBNull.Value)
                obj.ParentInvoiceId = (int)reader["parent_invoice_id"];

            if (reader["invoice_id"] != DBNull.Value)
                obj.InvoiceId = (int)reader["invoice_id"];

            if (reader["invoice_date"] != DBNull.Value)
                obj.InvoiceDate = (DateTime)reader["invoice_date"];

            if (reader["invoice_unique_number"] != DBNull.Value)
                obj.InvoiceUniqueNumber = reader["invoice_unique_number"].ToString();

            if (reader["grand_total"] != DBNull.Value)
                obj.GrandTotal = Convert.ToDouble(reader["grand_total"]);

            if (reader["amount_discount"] != DBNull.Value)
                obj.AmountDiscount = Convert.ToDouble(reader["amount_discount"]);

            if (reader["discount_percent"] != DBNull.Value)
                obj.PercentageDiscount = Convert.ToDouble(reader["discount_percent"]);

            if (reader["invoice_number"] != DBNull.Value)
                obj.InvoiceNumber = reader["invoice_number"].ToString();

            if (reader["ar_cash_customer_id"] != DBNull.Value)
                obj.ArCashCustomerId = (int)reader["ar_cash_customer_id"];


            if (reader["ar_mixed_customer_id"] != DBNull.Value)
                obj.ArMixedCustomerId = (int)reader["ar_mixed_customer_id"];


            if (reader["ar_visa_customer_id"] != DBNull.Value)
                obj.ArVisaCustomerId = (int)reader["ar_visa_customer_id"];

            if (reader["location_id"] != DBNull.Value)
                obj.LocationId = (int)reader["location_id"];


            if (reader["person_name"] != DBNull.Value)
                obj.PersonName = reader["person_name"].ToString();

            if (reader["tax_class_id"] != DBNull.Value)
                obj.TaxClassId = Convert.ToInt32(reader["tax_class_id"]);
            if (reader["total_discount"] != DBNull.Value)
                obj.TotalDiscountAmount = Convert.ToDouble(reader["total_discount"]);
            if (reader["tax"] != DBNull.Value)
                obj.TotalTaxAmount = Convert.ToDouble(reader["tax"]);
            if (reader["default_currency"] != DBNull.Value)
                obj.CurrencyId = Convert.ToInt32(reader["default_currency"]);

            if (reader["grand_total"] != DBNull.Value)
                obj.TotalAmount = Convert.ToDouble(reader["grand_total"]);
            if (reader["customer_id"] != DBNull.Value)
                obj.CustomerId = Convert.ToInt32(reader["customer_id"]);
            if (reader["non_taxable_class_id"] != DBNull.Value)
                obj.NonTaxableClassId = Convert.ToInt32(reader["non_taxable_class_id"]);

            if (reader["credit_note_account_ids"] != DBNull.Value)
                obj.CreditNoteAccountIds = reader["credit_note_account_ids"].ToString();

            if (reader["change_amount"] != DBNull.Value)
                obj.ChangeAmount = Convert.ToDouble(reader["change_amount"]);

            if (reader["invoice_status"] != DBNull.Value)
                obj.InvoiceStatus = (int)reader["invoice_status"];
            return obj;
        }

        public InvoiceInquiryLite GetInvoiceInquiryMapper(SqlDataReader reader)
        {
            InvoiceInquiryLite obj = new InvoiceInquiryLite();

            if (reader["invoice_id"] != DBNull.Value)
            {
                obj.InvoiceId = (int)reader["invoice_id"];
            }

            

            if (reader["invoice_number"] != DBNull.Value)
            {
                obj.InvoiceNumber = Convert.ToString(reader["invoice_number"]);
            }

            if (reader["status"] != DBNull.Value)
            {
                obj.Type = Convert.ToString(reader["status"]);
            }

            if (reader["invoice_date"] != DBNull.Value)
            {
                obj.InvoiceDate = Convert.ToDateTime(reader["invoice_date"]);
            }

            if (reader["shipment_number"] != DBNull.Value)
            {
                obj.ShipmentNumber = Convert.ToString(reader["shipment_number"]);
            }

            if (reader["grand_total"] != DBNull.Value)
            {
                obj.Total =Convert.ToDouble(reader["grand_total"]);
            }

            if (reader["store_name"] != DBNull.Value)
            {
                obj.Store = Convert.ToString(reader["store_name"]);
            }


            if (reader["register_name"] != DBNull.Value)
            {
                obj.Register = Convert.ToString(reader["register_name"]);
            }

            if (reader["row_num"] != DBNull.Value)
            {
                obj.RowNum = (int)reader["row_num"];
            }


            if (reader["shipment_id"] != DBNull.Value)
            {
                obj.ShipmentId = (int)reader["shipment_id"];
            }
            if (reader["cash_amount"] != DBNull.Value)
            {
                obj.CashAmount = Convert.ToDouble(reader["cash_amount"]);
            }
            if (reader["credit_card_amount"] != DBNull.Value)
            {
                obj.CreditCardAmount = Convert.ToDouble(reader["credit_card_amount"]);
            }
            if (reader["cheque_amount"] != DBNull.Value)
            {
                obj.ChequeAmount = Convert.ToDouble(reader["cheque_amount"]);
            }
            if (reader["on_account_amount"] != DBNull.Value)
            {
                obj.OnAccountAmount =Convert.ToDouble(reader["on_account_amount"]);
            }
            if (reader["total_payment"] != DBNull.Value)
            {
                obj.TotalPayment = Convert.ToDouble(reader["total_payment"]);
            }
            if (reader["total_price"] != DBNull.Value)
            {
                obj.TotalPrice   = Convert.ToDouble(reader["total_price"]);
            }
            if (reader["tax"] != DBNull.Value)
            {
                obj.Tax = Convert.ToDouble(reader["tax"]);
            }
            if (reader["amount_discount"] != DBNull.Value)
            {
                obj.AmountDiscount = Convert.ToDouble(reader["amount_discount"]);
            }
            if (reader["items_discount"] != DBNull.Value)
            {
                obj.ItemsDiscount = Convert.ToDouble(reader["items_discount"]);
            }
            if (reader["Currency"] != DBNull.Value)
            {
                obj.Currency = reader["Currency"].ToString();
            }
            return obj;

        }


        public POSQuantityInQuiryLite GetQuantityInquiryMapper(SqlDataReader reader)
        {
            POSQuantityInQuiryLite obj = new POSQuantityInQuiryLite();

            if (reader["item_number"] != DBNull.Value)
            {
                obj.ItemNumber = reader["item_number"].ToString();
            }
            if (reader["location_name"] != DBNull.Value)
            {
                obj.LocationName = reader["location_name"].ToString();
            }
            if (reader["item_name"] != DBNull.Value)
            {
                obj.ItemName = reader["item_name"].ToString();
            }
            if (reader["erp_quantity"] != DBNull.Value)
            {
                obj.ERPQuantity = (int)reader["erp_quantity"];
            }
            if (reader["staging_quantity"] != DBNull.Value)
            {
                obj.StagingQuantity = (int)reader["staging_quantity"];
            }
            if (reader["pos_quantity"] != DBNull.Value)
            {
                obj.POSQuantity = (int)reader["pos_quantity"];
            }
            if (reader["row_num"] != DBNull.Value)
            {
                obj.RowNum = (int)reader["row_num"];
            }

            if (reader["StoreName"] != DBNull.Value)
            {
                obj.StoreName = reader["StoreName"].ToString();
            }
            if (reader["pendingQTY"] != DBNull.Value)
            {
                obj.PendingQTY = (int)reader["pendingQTY"];
            }
            if (reader["summationQTY"] != DBNull.Value)
            {
                obj.SummationQTY = (int)reader["summationQTY"];
            }
            if (reader["staging_out_quantity"] != DBNull.Value)
            {
                obj.StagingOutQuantity = (int)reader["staging_out_quantity"];
            }
            if (reader["pendingOutQTY"] != DBNull.Value)
            {
                obj.PendingOutQuantity = (int)reader["pendingOutQTY"];
            }
                
            return obj;

        }

        public ICQuantityInQuiryLite GetICQuantityInquiryMapper(SqlDataReader reader)
        {
            ICQuantityInQuiryLite obj = new ICQuantityInQuiryLite();

            if (reader["item_number"] != DBNull.Value)
            {
                obj.ItemNumber = reader["item_number"].ToString();
            }
            if (reader["location_name"] != DBNull.Value)
            {
                obj.LocationName = reader["location_name"].ToString();
            }
            if (reader["item_name"] != DBNull.Value)
            {
                obj.ItemName = reader["item_name"].ToString();
            }
            if (reader["erp_quantity"] != DBNull.Value)
            {
                obj.ERPQuantity = (int)reader["erp_quantity"];
            }
            if (reader["staging_quantity"] != DBNull.Value)
            {
                obj.StagingQuantity = (int)reader["staging_quantity"];
            }
            if (reader["pos_quantity"] != DBNull.Value)
            {
                obj.POSQuantity = (int)reader["pos_quantity"];
            }
            if (reader["row_num"] != DBNull.Value)
            {
                obj.RowNum = (int)reader["row_num"];
            }

            if (reader["StoreName"] != DBNull.Value)
            {
                obj.StoreName = reader["StoreName"].ToString();
            }
            if (reader["pendingQTY"] != DBNull.Value)
            {
                obj.PendingQTY = (int)reader["pendingQTY"];
            }
            if (reader["summationQTY"] != DBNull.Value)
            {
                obj.SummationQTY = (int)reader["summationQTY"];
            }
            if (reader["staging_out_quantity"] != DBNull.Value)
            {
                obj.StagingOutQuantity = (int)reader["staging_out_quantity"];
            }
            if (reader["pendingOutQTY"] != DBNull.Value)
            {
                obj.PendingOutQuantity = (int)reader["pendingOutQTY"];
            }
            if (reader["avg_cost"] != DBNull.Value)
            {
                obj.AverageCost = Convert.ToDouble(reader["avg_cost"]);
            }

            return obj;

        }

        // public InvoiceLite GetMapperAllLite(SqlDataReader reader)
        //{
        //    InvoiceLite obj = new InvoiceLite();
        //    //Redaer//Values
        //    obj.Id = obj.InvoiceId;
        //    return obj;
        //}

        // public InvoiceLite GetLiteMapper(SqlDataReader reader)
        //{
        //    InvoiceLite obj = new InvoiceLite();
        //    //Redaer//Values
        //         if (reader["created_by_name"] != DBNull.Value)
        //    {
        //        obj.CreatedByName = (string)reader["created_by_name"];
        //    }
        //    if (reader["updated_by_name"] != DBNull.Value)
        //    {
        //        obj.UpdatedByName = (string)reader["updated_by_name"];
        //    }
        //    return obj;
        //}
    }
}
