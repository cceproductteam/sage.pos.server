using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using Centrix.UM.Business.Entity;
using System.Data.SqlClient;
using CentrixERP.DataAccess.IRepository;

using System.Data;

namespace CentrixERP.DataAccess.Repository
{
    public partial class ClientLoginRepository : IClientLoginRepository
    {
        public User ClientLogin(string UserName, string Password, string AccessCode)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);

            Instance.AddInParameter("@UserName", System.Data.SqlDbType.NChar, UserName);
            Instance.AddInParameter("@Password", System.Data.SqlDbType.NChar, Password);
            if (!string.IsNullOrEmpty(AccessCode))
                Instance.AddInParameter("@AccessCode", System.Data.SqlDbType.NChar, AccessCode);

            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_User_Login", conn);
            if (reader != null && reader.HasRows)
            {
                reader.Read();
                User myuser = GetMapper(reader);
                reader.Close();
                return myuser;
            }
            else
            {
                return null;
            }
        }

        public void Add(ref User myEntity)
        {
            throw new NotImplementedException();
        }
        public void Delete(ref User myEntity)
        {
            throw new NotImplementedException();
        }
        public void DeleteLogical(ref User myEntity)
        {
            throw new NotImplementedException();
        }
        public List<User> FindAll(CriteriaBase myCriteria)
        {
            throw new NotImplementedException();
        }
        public User FindById(int Id, CriteriaBase myCriteria)
        {
            throw new NotImplementedException();
        }
        public List<User> FindByParentId<TParentId>(TParentId ParentId, Type ParentType, CriteriaBase myCriteria)
        {
            throw new NotImplementedException();
        }
        public void Update(ref User myEntity)
        {
            throw new NotImplementedException();
        }


        public Object BeginTransaction( )
        {
            return new object();
        }

        public void CommitTransaction( Object object1 )
        {
        }

        public string CustomConnectionString
        {
            get;
            set;
        }

        public void KillConnection()
        {
           
        }

        public void Rollback(Object object1)
        {
            
        }

        public bool UseCustomConnectionString
        {
            get;
            set;
        }

        public bool UseSharedSession
        {
            get;
            set;
        }



        User IClientLoginRepository.ClientLogin(string UserName, string Password, string AccessCode)
        {
            throw new NotImplementedException();
        }

        void IRepository<User, int>.Add(ref User myEntity)
        {
            throw new NotImplementedException();
        }

        object IRepository<User, int>.BeginTransaction()
        {
            throw new NotImplementedException();
        }

        void IRepository<User, int>.CommitTransaction(object transaction)
        {
            throw new NotImplementedException();
        }

        string IRepository<User, int>.CustomConnectionString
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        void IRepository<User, int>.Delete(ref User myEntity)
        {
            throw new NotImplementedException();
        }

        void IRepository<User, int>.DeleteLogical(ref User myEntity)
        {
            throw new NotImplementedException();
        }

        List<User> IRepository<User, int>.FindAll(CriteriaBase myCriteria)
        {
            throw new NotImplementedException();
        }

        User IRepository<User, int>.FindById(int Id, CriteriaBase myCriteria)
        {
            throw new NotImplementedException();
        }

        List<User> IRepository<User, int>.FindByParentId<TParentId>(TParentId ParentId, Type ParentType, CriteriaBase myCriteria)
        {
            throw new NotImplementedException();
        }

        void IRepository<User, int>.KillConnection()
        {
            throw new NotImplementedException();
        }

    void IRepository<User, int>.Rollback(object transaction)
        {
            throw new NotImplementedException();
        }

        void IRepository<User, int>.Update(ref User myEntity)
        {
            throw new NotImplementedException();
        }

        bool IRepository<User, int>.UseCustomConnectionString
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        bool IRepository<User, int>.UseSharedSession
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }
}
