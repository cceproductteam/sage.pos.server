
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
//using Centrix.POS.Standalone.Client.Business.Entity;
//using Centrix.POS.Standalone.Client.DataAccess.IRepository;
using CentrixERP.Business.Entity;
using CentrixERP.DataAccess.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace CentrixERP.DataAccess.Repository
{
    public partial class TransactionTypeRepository : RepositoryBaseClass<TransactionType, TransactionTypeLite, int>, ITransactionTypeRepository
    {
        #region IRepository< TransactionType, int > Members

        public TransactionTypeRepository()
        {
            this.AddSPName = "SP_TransactionType_Add";
            this.UpdateSPName = "SP_TransactionType_Update";
            this.DeleteSPName = "SP_TransactionType_Delete";
            this.DeleteLogicalSPName = "SP_TransactionType_DeleteLogical";
            this.FindAllSPName = "SP_TransactionType_FindAll";
            this.FindByIdSPName = "SP_TransactionType_FindById";
            this.FindByParentSPName = "SP_TransactionType_FindByParentId";
            this.FindAllLiteSPName = "SP_TransactionType_FindAll_Lite";
            this.FindByIdLiteSPName = "SP_TransactionType_FindById_Lite";
            this.AdvancedSearchSPName = "SP_TransactionType_AdvancedSearch_Lite";
        }

		#endregion
    }
}