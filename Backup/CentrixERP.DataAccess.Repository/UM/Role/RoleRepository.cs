using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using Centrix.UM.Business.Entity;
using Centrix.UM.DataAccess.IRepository;
using System.Data.SqlClient;
using System.Data;

namespace Centrix.UM.DataAccess.Repository
{
    public partial class RoleRepository : IRoleRepository
    {

        #region IRepository<Role,int> Members

        public void Add(ref Role myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            if (UseCustomConnectionString)
                Instance.SetConnectionString(CustomConnectionString);
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@RoleName", System.Data.SqlDbType.NVarChar, myEntity.NameEnglish);
            Instance.AddInParameter("@ArabicRoleName", System.Data.SqlDbType.NVarChar, myEntity.NameArabic);
            Instance.AddInParameter("@RoleDescription", System.Data.SqlDbType.NVarChar, myEntity.Description);
            Instance.AddInParameter("@PermissionLevel", System.Data.SqlDbType.Int, myEntity.PermissionLevel);
            Instance.AddInParameter("@CreatedBy", System.Data.SqlDbType.Int, myEntity.CreatedBy);
            Instance.AddOutParameter("@Id", System.Data.SqlDbType.Int,(object)myEntity.RoleId);

            try
            {
                Instance.ExcuteNonQuery("dbo.SP_Role_Add", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            myEntity.RoleId = (int)Instance.GetOutParamValue("@Id");
        }

        public void Delete(ref Role myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@Id", System.Data.SqlDbType.Int, myEntity.RoleId);
            try
            {
                Instance.ExcuteNonQuery("dbo.SP_Role_Delete", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteLogical(ref Role myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            if (UseCustomConnectionString)
                Instance.SetConnectionString(CustomConnectionString);
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@Id", System.Data.SqlDbType.Int, myEntity.RoleId);
            try
            {
                Instance.ExcuteNonQuery("dbo.SP_Role_DeleteLogical", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Role> FindAll(CriteriaBase myCriteria)
        {
            List<Role> Roles = new List<Role>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            if (myCriteria != null)
            {
                Instance.AddInParameter("@Name", System.Data.SqlDbType.NVarChar, ((RoleCriteria)myCriteria).Name);
            }

            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_Role_FindAll", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    Roles.Add(GetMapper(reader));
                }
                reader.Close();
                return Roles;
            }
            else
            {
                return null;
            }
        }

        public Role FindById(int Id, CriteriaBase myCriteria)
        {

            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            if (myCriteria != null)
            {
                //Instance.AddInParameter("@Name", System.Data.SqlDbType.Int, ((RoleCriteria)myCriteria).Name);
            }
            Instance.AddInParameter("@Id", System.Data.SqlDbType.Int, Id);
            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_Role_FindById", conn);
            if (reader != null && reader.HasRows)
            {
                reader.Read();
                Role myrole = GetMapper(reader);
                reader.Close();
                return myrole;
            }
            else
            {
                return null;
            }
        }

        public List<Role> FindByParentId<TParentId>(TParentId ParentId, Type ParentType, CriteriaBase myCriteria)
        {
            throw new NotImplementedException();
        }

        public void Update(ref Role myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@Id", System.Data.SqlDbType.Int, myEntity.RoleId);
            Instance.AddInParameter("@RoleName", System.Data.SqlDbType.NVarChar, myEntity.NameEnglish);
            Instance.AddInParameter("@ArabicRoleName", System.Data.SqlDbType.NVarChar, myEntity.NameArabic);
            Instance.AddInParameter("@RoleDescription", System.Data.SqlDbType.NVarChar, myEntity.Description);
            Instance.AddInParameter("@PermissionLevel", System.Data.SqlDbType.Int, myEntity.PermissionLevel);
            Instance.AddInParameter("@UpdatedBy", System.Data.SqlDbType.Int, myEntity.UpdatedBy);

            try
            {
                Instance.ExcuteNonQuery("dbo.SP_Role_Update", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion

        public List<Role> FindAll_RecentlyViewed(int userId, int entityId, RoleCriteria myCriteria)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;
            List<Role> List = new List<Role>();
            if (myCriteria != null)
                Instance.AddInParameter("@Keyword", System.Data.SqlDbType.NVarChar, myCriteria.keyword);
            Instance.AddInParameter("@entity_id", System.Data.SqlDbType.Int, entityId);
            Instance.AddInParameter("@user_id", System.Data.SqlDbType.Int, userId);

            reader = Instance.ExcuteReader("SP_Role_RecentlyViewed_FindAll", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                    List.Add(GetMapper(reader));

                reader.Close();
                return List;
            }
            else
                return null;

        }

        public List<Role_Lite> FindAllLite(RoleCriteria myCriteria)
        {
            List<Role_Lite> Roles = new List<Role_Lite>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);

            if (myCriteria != null)
            {
                RoleCriteria criteria = (RoleCriteria)myCriteria;
                if (!string.IsNullOrEmpty(criteria.keyword))
                    Instance.AddInParameter("@Keyword", SqlDbType.NVarChar, criteria.keyword);
                if (!criteria.pageNumber.Equals(-1))
                    Instance.AddInParameter("@page_number", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(criteria.pageNumber));
                if (!criteria.resultCount.Equals(-1))
                    Instance.AddInParameter("@result_count", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(criteria.resultCount));
            }

            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_Role_FindAll_Lite", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    Roles.Add(GetLiteMapper(reader));
                }
                reader.Close();
                return Roles;
            }
            else
            {
                return null;
            }
        }

        public Role_Lite FindByIdLite(int Id)
        {
            Role_Lite role = new Business.Entity.Role_Lite();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);

            Instance.AddInParameter("@Id", System.Data.SqlDbType.Int, Id);
            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_Role_FindById_Lite", conn);

            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                    role = GetLiteMapper(reader);
                reader.Close();
                return role;
            }
            else
                return null;
        }

        public string CustomConnectionString
        {
            get;
            set;
        }

        public bool UseCustomConnectionString
        {
            get;
            set;
        }
        public Object BeginTransaction()
        {
            return new object();
        }


        public void CommitTransaction(Object object1)
        {
        }

        public void Rollback(Object object1)
        {

        }

        public void KillConnection()
        {

        }


        public bool UseSharedSession { get; set; }
    }
}
