using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using Centrix.UM.Business.Entity;
using Centrix.UM.DataAccess.IRepository;
using System.Data.SqlClient;

namespace Centrix.UM.DataAccess.Repository
{
    public partial class PermissionRepository:IPermissionRepository
    {

        #region IRepository<Permission,int> Members

        public void Add(ref Permission myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();

            if (UseCustomConnectionString)
                Instance.SetConnectionString(CustomConnectionString);
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@permission_name", System.Data.SqlDbType.NVarChar, myEntity.NameEnglish);
            Instance.AddInParameter("@permission_name_ar", System.Data.SqlDbType.NVarChar, myEntity.NameArabic);
            Instance.AddInParameter("@permission_description", System.Data.SqlDbType.NVarChar, myEntity.Description);


            Instance.AddInParameter("@createdBy", System.Data.SqlDbType.Int, myEntity.CreatedBy);

            Instance.AddOutParameter("@Id", System.Data.SqlDbType.Int, (object)myEntity.PermissionId);

            try
            {
                Instance.ExcuteNonQuery("dbo.SP_Permission_Add", conn, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            myEntity.PermissionId = (int)Instance.GetOutParamValue("@Id");
        }

        public void Delete(ref Permission myEntity)
        {
            throw new NotImplementedException();
        }

        public void DeleteLogical(ref Permission myEntity)
        {
            throw new NotImplementedException();
        }

        public List<Permission> FindAll(CriteriaBase myCriteria)
        {
            List<Permission> Permissions = new List<Permission>();
             SQLDAHelper Instance = new SQLDAHelper(); 
             SqlConnection conn = Instance.GetConnection(false);
            if (myCriteria != null)
            {
                Instance.AddInParameter("@RoleIDToExclude", System.Data.SqlDbType.Int, ((PermissionCriteria)myCriteria).RoleToExclude);
                Instance.AddInParameter("@KeyWord", System.Data.SqlDbType.NVarChar, ((PermissionCriteria)myCriteria).KeyWord);
            }

            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_Permission_FindAll", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    Permissions.Add(GetMapper(reader));
                }
                reader.Close();
                return Permissions;
            }
            else
            {
                return null;
            }
        }

        public Permission FindById(int Id, CriteriaBase myCriteria)
        {
          
             SQLDAHelper Instance = new SQLDAHelper(); 
             SqlConnection conn = Instance.GetConnection(false);
            if (myCriteria != null)
            {
                //Instance.AddInParameter("@Name", System.Data.SqlDbType.Int, ((RoleCriteria)myCriteria).Name);
            }
            Instance.AddInParameter("@Id", System.Data.SqlDbType.Int, Id);
            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_Permission_FindById", conn);
            if (reader != null && reader.HasRows)
            {
                reader.Read();
                Permission per= GetMapper(reader);
                reader.Close();
                return per;

            }
            else
            {
                return null;
            }
        }

        public List<Permission> FindByParentId<TParentId>(TParentId ParentId, Type ParentType, CriteriaBase myCriteria)
        {
            throw new NotImplementedException();
        }

        public void Update(ref Permission myEntity)
        {
            throw new NotImplementedException();
        }

        public string CustomConnectionString
        {
            get;
            set;
        }

        public bool UseCustomConnectionString
        {
            get;
            set;
        }
        public Object BeginTransaction()
        {
            return new object();
        }


        public void CommitTransaction(Object object1)
        {
        }

        public void Rollback(Object object1)
        {

        }

        public void KillConnection()
        {

        }


        public bool UseSharedSession { get; set; }

        #endregion
    }
}
