using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SF.Framework.Data;
using Centrix.UM.Business.Entity;
using Centrix.UM.DataAccess.IRepository;
using System.Data.SqlClient;

namespace Centrix.UM.DataAccess.Repository
{
    public partial class PermissionRepository
    {
        public Permission GetMapper(SqlDataReader reader)
        {
            Permission myPermission = new Permission();
            if (reader["permission_id"] != DBNull.Value)
            {
                myPermission.PermissionId = (int)reader["permission_id"];
            }
            if (reader["permission_name"] != DBNull.Value)
            {
                myPermission.NameEnglish = reader["permission_name"].ToString();
            }
            if (reader["permission_name_ar"] != DBNull.Value)
            {
                myPermission.NameArabic  = reader["permission_name_ar"].ToString();
            }
            if (reader["permission_description"] != DBNull.Value)
            {
                myPermission.Description = reader["permission_description"].ToString();
            }
            if (reader["default_permission"] != DBNull.Value)
            {
                myPermission.DefaultPermission = (bool)reader["default_permission"];
            }

            return myPermission;
        }
    }
}
