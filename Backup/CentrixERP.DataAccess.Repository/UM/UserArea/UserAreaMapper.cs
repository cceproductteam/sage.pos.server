using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SF.Framework.Data;
using Centrix.UM.Business.Entity;
using Centrix.UM.DataAccess.IRepository;
using System.Data.SqlClient;

namespace Centrix.UM.DataAccess.Repository
{
    public partial class UserAreaRepository
    {
        public UserArea GetMapper(SqlDataReader reader)
        {
            UserArea myUserArea = new UserArea();
            if (reader["user_area_id"] != DBNull.Value)
            {
                myUserArea.UserAreaId = (int)reader["user_area_id"];
            }
            if (reader["user_id"] != DBNull.Value)
            {
                myUserArea.UserId = (int)reader["user_id"];
            }
            if (reader["area_id"] != DBNull.Value)
            {
                myUserArea.AreaId = (int)reader["area_id"];
            }
            if (reader["created_by"] != DBNull.Value)
            {
                myUserArea.CreatedBy = (int)reader["created_by"];
            }
            if (reader["updated_by"] != DBNull.Value)
            {
                myUserArea.UpdatedBy = (int)reader["updated_by"];
            }
            return myUserArea;
        }
    }
}
