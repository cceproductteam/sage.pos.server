using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SF.Framework.Data;
using Centrix.UM.Business.Entity;
using Centrix.UM.DataAccess.IRepository;
using Centrix.UM.Business.Entity;
using Centrix.UM.DataAccess.IRepository;
using System.Data.SqlClient;
using System.Data;


namespace Centrix.UM.DataAccess.Repository
{
    public partial class UserRepository : IUserRepository
    {

        #region IRepository<User,int> Members

        public void Add(ref User myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            if (UseCustomConnectionString)
                Instance.SetConnectionString(CustomConnectionString);
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@UserName", System.Data.SqlDbType.NVarChar, myEntity.UserName);
            Instance.AddInParameter("@Password", System.Data.SqlDbType.NVarChar, Cryption.Encrypt.String(myEntity.Password, Cryption.Algorithm.AES));//myEntity.Password
            Instance.AddInParameter("@AccessCode", System.Data.SqlDbType.NVarChar, Cryption.Encrypt.String(myEntity.AccessCode, Cryption.Algorithm.AES));
            Instance.AddInParameter("@FirstName", System.Data.SqlDbType.NVarChar, myEntity.FirstnameEnglish);
            Instance.AddInParameter("@MiddleName", System.Data.SqlDbType.NVarChar, myEntity.MiddleNameEnglish);
            Instance.AddInParameter("@LastName", System.Data.SqlDbType.NVarChar, myEntity.LastNameEnglish);
            Instance.AddInParameter("@ArabicFirstName", System.Data.SqlDbType.NVarChar, myEntity.FirstNameArabic);
            Instance.AddInParameter("@ArabicMiddleName", System.Data.SqlDbType.NVarChar, myEntity.MiddleNameArabic);
            Instance.AddInParameter("@ArabicLastName", System.Data.SqlDbType.NVarChar, myEntity.LastNameArabic);
            Instance.AddInParameter("@Dob", System.Data.SqlDbType.DateTime, SQLDAHelper.Convert_DateTiemTODB(myEntity.Dob));
            Instance.AddInParameter("@Title", System.Data.SqlDbType.NVarChar, myEntity.Title);
            Instance.AddInParameter("@Notes", System.Data.SqlDbType.NVarChar, myEntity.Notes);
            Instance.AddInParameter("@RoleID", System.Data.SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.RoleId));
            Instance.AddInParameter("@TeamID", System.Data.SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.TeamId));
            Instance.AddInParameter("@CreatedBy", System.Data.SqlDbType.Int, myEntity.CreatedBy);
            Instance.AddInParameter("@IsDefault", System.Data.SqlDbType.Bit, myEntity.IsDefault);
            Instance.AddInParameter("@is_pos_user", System.Data.SqlDbType.Bit, myEntity.IsPosUser);
            Instance.AddInParameter("@is_manager", System.Data.SqlDbType.Bit, myEntity.IsManager);
            Instance.AddInParameter("@manager_id", System.Data.SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.ManagerId));
            Instance.AddInParameter("@flag_deleted", System.Data.SqlDbType.Bit, myEntity.FlagDeleted);
            Instance.AddInParameter("@ReceiveNotifications", System.Data.SqlDbType.Bit, myEntity.ReceiveNoyifications);
            Instance.AddOutParameter("@Id", System.Data.SqlDbType.Int,(object)myEntity.UserId);
            Instance.ExcuteNonQuery("dbo.SP_User_Add", conn, false);

            myEntity.UserId = (int)Instance.GetOutParamValue("@Id");
        }

        public void Delete(ref User myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@Id", System.Data.SqlDbType.Int, myEntity.UserId);
            Instance.ExcuteNonQuery("dbo.SP_User_Delete", conn, false);

        }

        public void DeleteLogical(ref User myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@Id", System.Data.SqlDbType.Int, myEntity.UserId);
            Instance.ExcuteNonQuery("dbo.SP_User_DeleteLogical", conn, false);

        }

        public List<User> FindAll(CriteriaBase myCriteria)
        {
            List<User> Users = new List<User>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            UserCriteria criteria = (UserCriteria)myCriteria;
            if (criteria != null)
            {
                Instance.AddInParameter("@Keyword", System.Data.SqlDbType.NVarChar, (criteria.Keyword));
                if (criteria.Role != 0) Instance.AddInParameter("@role", System.Data.SqlDbType.Int, SQLDAHelper.Convert_IntTODB(criteria.Role));
                if (criteria.Team != 0) Instance.AddInParameter("@team", System.Data.SqlDbType.Int, SQLDAHelper.Convert_IntTODB(criteria.Team));
                if (criteria.Area != 0) Instance.AddInParameter("@area", System.Data.SqlDbType.Int, SQLDAHelper.Convert_IntTODB(criteria.Area));
                if (!string.IsNullOrEmpty(criteria.Title)) Instance.AddInParameter("@title", System.Data.SqlDbType.NVarChar, (criteria.Title));
                if (criteria.DOBFrom.HasValue) Instance.AddInParameter("@dob_from", System.Data.SqlDbType.DateTime, (criteria.DOBFrom.Value));
                if (criteria.DOBTo.HasValue) Instance.AddInParameter("@dob_to", System.Data.SqlDbType.DateTime, (criteria.DOBTo.Value));

                if (!string.IsNullOrEmpty(criteria.FirstName))
                    Instance.AddInParameter("@first_name", System.Data.SqlDbType.NVarChar, criteria.FirstName);
                if (!string.IsNullOrEmpty(criteria.FirstNameAR))
                    Instance.AddInParameter("@first_name_ar", System.Data.SqlDbType.NVarChar, criteria.FirstNameAR);
                if (!string.IsNullOrEmpty(criteria.MiddleName))
                    Instance.AddInParameter("@middle_name", System.Data.SqlDbType.NVarChar, criteria.MiddleName);
                if (!string.IsNullOrEmpty(criteria.MiddleNameAR))
                    Instance.AddInParameter("@middle_name_ar", System.Data.SqlDbType.NVarChar, criteria.MiddleNameAR);
                if (!string.IsNullOrEmpty(criteria.LastName))
                    Instance.AddInParameter("@last_name", System.Data.SqlDbType.NVarChar, criteria.LastName);
                if (!string.IsNullOrEmpty(criteria.LastNameAR))
                    Instance.AddInParameter("@last_name_ar", System.Data.SqlDbType.NVarChar, criteria.LastNameAR);
            }

            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_User_FindAll", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    Users.Add(GetMapper(reader));
                }
                reader.Close();
                return Users;
            }
            else
            {
                return null;
            }
        }

        public User FindById(int Id, CriteriaBase myCriteria)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            if (myCriteria != null)
            {
                //Instance.AddInParameter("@Name", System.Data.SqlDbType.Int, ((RoleCriteria)myCriteria).Name);
            }
            Instance.AddInParameter("@Id", System.Data.SqlDbType.Int, Id);
            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_User_FindById", conn);
            if (reader != null && reader.HasRows)
            {
                reader.Read();
                User myuser = GetMapper(reader);
                reader.Close();
                return myuser;
            }
            else
            {
                return null;
            }
        }

        public List<User> FindByParentId<TParentId>(TParentId ParentId, Type ParentType, CriteriaBase myCriteria)
        {
            throw new NotImplementedException();
        }

        public void Update(ref User myEntity)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@UserName", System.Data.SqlDbType.NVarChar, myEntity.UserName);
            Instance.AddInParameter("@Password", System.Data.SqlDbType.NVarChar, Cryption.Encrypt.String(myEntity.Password, Cryption.Algorithm.AES)); //  myEntity.Password
            Instance.AddInParameter("@FirstName", System.Data.SqlDbType.NVarChar, myEntity.FirstnameEnglish);
            Instance.AddInParameter("@MiddleName", System.Data.SqlDbType.NVarChar, myEntity.MiddleNameEnglish);
            Instance.AddInParameter("@LastName", System.Data.SqlDbType.NVarChar, myEntity.LastNameEnglish);
            //Instance.AddInParameter("@ArabicFirstName", System.Data.SqlDbType.NVarChar, myEntity.FirstNameArabic);
            //Instance.AddInParameter("@ArabicMiddleName", System.Data.SqlDbType.NVarChar, myEntity.MiddleNameArabic);
            //Instance.AddInParameter("@ArabicLastName", System.Data.SqlDbType.NVarChar, myEntity.LastNameArabic);
            Instance.AddInParameter("@Dob", System.Data.SqlDbType.DateTime, SQLDAHelper.Convert_DateTiemTODB(myEntity.Dob));
            Instance.AddInParameter("@Title", System.Data.SqlDbType.NVarChar, myEntity.Title);
            Instance.AddInParameter("@Notes", System.Data.SqlDbType.NVarChar, myEntity.Notes);
            Instance.AddInParameter("@RoleID", System.Data.SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.RoleId));
            Instance.AddInParameter("@TeamID", System.Data.SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.TeamId));
            Instance.AddInParameter("@UpdatedBy", System.Data.SqlDbType.Int, myEntity.UpdatedBy);
            Instance.AddInParameter("@IsDefault", System.Data.SqlDbType.Bit, myEntity.IsDefault);
            Instance.AddInParameter("@Id", System.Data.SqlDbType.Int, myEntity.UserId);
            Instance.AddInParameter("@ReceiveNotifications", System.Data.SqlDbType.Bit, myEntity.ReceiveNoyifications);
            Instance.AddInParameter("@is_manager", System.Data.SqlDbType.Bit, myEntity.IsManager);
            Instance.AddInParameter("@manager_id", System.Data.SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myEntity.ManagerId));
            Instance.AddInParameter("@is_pos_user", System.Data.SqlDbType.Bit, myEntity.IsPosUser);
            Instance.AddInParameter("@access_code", System.Data.SqlDbType.NVarChar, myEntity.AccessCode);
            Instance.ExcuteNonQuery("dbo.SP_User_Update", conn, false);

        }


        public List<User> FindAllTaskUsers(UserCriteria myCriteria)
        {
            List<User> TaskUsersList = new List<User>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            if (myCriteria != null)
            {
                if (myCriteria.schedualtaskid != -1)
                    Instance.AddInParameter("@schedualtaskid", System.Data.SqlDbType.Int, SQLDAHelper.Convert_IntTODB(myCriteria.schedualtaskid));

            }

            SqlDataReader reader;
            reader = Instance.ExcuteReader("SP_ScheduleTask_FindAllUsers", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                    TaskUsersList.Add(GetMapper(reader));
                reader.Close();
                return TaskUsersList;
            }
            else
                return null;

        }

        public string CustomConnectionString
        {
            get;
            set;
        }

        public bool UseCustomConnectionString
        {
            get;
            set;
        }
        public Object BeginTransaction()
        {
            return new object();
        }


        public void CommitTransaction(Object object1)
        {
        }

        public void Rollback(Object object1)
        {

        }

        public void KillConnection()
        {

        }


        public bool UseSharedSession { get; set; }


        #endregion

        #region IUserRepository Members

        public User Login(string UserName, string Password)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);

            Instance.AddInParameter("@UserName", System.Data.SqlDbType.NChar, UserName);
            Instance.AddInParameter("@Password", System.Data.SqlDbType.NChar, Password);

            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_User_Login", conn);
            if (reader != null && reader.HasRows)
            {
                reader.Read();
                User myuser = GetMapper(reader);
                reader.Close();
                return myuser;
            }
            else
            {
                return null;
            }
        }

        #endregion

        public List<User> FindAll_RecentlyViewed(int userId, int entityId, UserCriteria myCriteria)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;
            List<User> List = new List<User>();
            if (myCriteria != null)
            {
                Instance.AddInParameter("@Keyword", System.Data.SqlDbType.NVarChar, myCriteria.Keyword);
                if (myCriteria.Role != 0) Instance.AddInParameter("@role", System.Data.SqlDbType.Int, (myCriteria.Role));
                if (myCriteria.Team != 0) Instance.AddInParameter("@team", System.Data.SqlDbType.Int, (myCriteria.Team));
                if (myCriteria.Area != 0) Instance.AddInParameter("@area", System.Data.SqlDbType.Int, (myCriteria.Area));
                if (!string.IsNullOrEmpty(myCriteria.Title)) Instance.AddInParameter("@title", System.Data.SqlDbType.NVarChar, (myCriteria.Title));
                if (myCriteria.DOBFrom.HasValue) Instance.AddInParameter("@dob_from", System.Data.SqlDbType.DateTime, (myCriteria.DOBFrom.Value));
                if (myCriteria.DOBTo.HasValue) Instance.AddInParameter("@dob_to", System.Data.SqlDbType.DateTime, (myCriteria.DOBTo.Value));

                if (!string.IsNullOrEmpty(myCriteria.FirstName))
                    Instance.AddInParameter("@first_name", System.Data.SqlDbType.NVarChar, myCriteria.FirstName);
                if (!string.IsNullOrEmpty(myCriteria.FirstNameAR))
                    Instance.AddInParameter("@first_name_ar", System.Data.SqlDbType.NVarChar, myCriteria.FirstNameAR);
                if (!string.IsNullOrEmpty(myCriteria.MiddleName))
                    Instance.AddInParameter("@middle_name", System.Data.SqlDbType.NVarChar, myCriteria.MiddleName);
                if (!string.IsNullOrEmpty(myCriteria.MiddleNameAR))
                    Instance.AddInParameter("@middle_name_ar", System.Data.SqlDbType.NVarChar, myCriteria.MiddleNameAR);
                if (!string.IsNullOrEmpty(myCriteria.LastName))
                    Instance.AddInParameter("@last_name", System.Data.SqlDbType.NVarChar, myCriteria.LastName);
                if (!string.IsNullOrEmpty(myCriteria.LastNameAR))
                    Instance.AddInParameter("@last_name_ar", System.Data.SqlDbType.NVarChar, myCriteria.LastNameAR);
            }
            Instance.AddInParameter("@entity_id", System.Data.SqlDbType.Int, entityId);
            Instance.AddInParameter("@user_id", System.Data.SqlDbType.Int, userId);

            reader = Instance.ExcuteReader("SP_User_RecentlyViewed_FindAll", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                    List.Add(GetMapper(reader));

                reader.Close();
                return List;
            }
            else
                return null;

        }

        public List<UserLite> FindAllLite(CriteriaBase myCriteria)
        {
            List<UserLite> Users = new List<UserLite>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);

            if (myCriteria != null)
            {
                UserCriteria criteria = (UserCriteria)myCriteria;
                if (!string.IsNullOrEmpty(criteria.Keyword))
                    Instance.AddInParameter("@Keyword", SqlDbType.NVarChar, criteria.Keyword);
                if (criteria.pageNumber>0 && !criteria.pageNumber.Equals(-1))
                    Instance.AddInParameter("@page_number", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(criteria.pageNumber));
                if (criteria.resultCount>0 && !criteria.resultCount.Equals(-1))
                    Instance.AddInParameter("@result_count", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(criteria.resultCount));
                if (criteria.UserId.HasValue && !criteria.UserId.Equals(-1))
                    Instance.AddInParameter("@user_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(criteria.UserId.Value));

                if (criteria.isManager)
                    Instance.AddInParameter("@is_manager", SqlDbType.Bit, criteria.isManager);
                Instance.AddInParameter("@show_super_user", SqlDbType.Bit, criteria.showSuperUser);

                if (!string.IsNullOrEmpty(criteria.UserIds))
                    Instance.AddInParameter("@user_ids", SqlDbType.NVarChar, criteria.UserIds);

                Instance.AddInParameter("@except_user_id", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(criteria.ExceptUserId));
            }

            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_User_FindAll_Lite", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    Users.Add(GetLiteUserMapper(reader));
                }
                reader.Close();
                return Users;
            }
            else
            {
                return null;
            }
        }

        public UserLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            UserLite User = new Business.Entity.UserLite();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);

            Instance.AddInParameter("@user_id", System.Data.SqlDbType.Int, Id);
            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_User_FindById_Lite", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    User = GetLiteUserMapper(reader);
                }
                reader.Close();
                return User;
            }
            else
            {
                return null;
            }
        }


        public UserLite FindUserManager(int Id, CriteriaBase myCriteria)
        {
            UserLite User = new Business.Entity.UserLite();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);

            Instance.AddInParameter("@user_id", System.Data.SqlDbType.Int, Id);
            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_User_Find_User_Manager", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    User = GetLiteUserMapper(reader);
                }
                reader.Close();
                return User;
            }
            else
            {
                return null;
            }
        }

        public List<object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;

            List<object> myUserLiteList;

            if (myCriteria != null)
            {
                UserCriteria criteria = (UserCriteria)myCriteria;
                Instance.AddInParameter("@sort_type", SqlDbType.NVarChar, criteria.SortType);
                if (!string.IsNullOrEmpty(criteria.SearchCriteria))
                    Instance.AddInParameter("@search_criteria", SqlDbType.NVarChar, criteria.SearchCriteria);
                if (criteria.pageNumber>0 && !criteria.pageNumber.Equals(-1))
                    Instance.AddInParameter("@page_number", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(criteria.pageNumber));
                if (criteria.resultCount>0 && !criteria.resultCount.Equals(-1))
                    Instance.AddInParameter("@result_count", SqlDbType.Int, SQLDAHelper.Convert_IntTODB(criteria.resultCount));
                Instance.AddInParameter("@sort_fields", SqlDbType.NVarChar, criteria.SortFields);
            }

            reader = Instance.ExcuteReader("dbo.SP_User_AdvancedSearch_Lite", conn);

            if (reader != null && reader.HasRows)
            {
                myUserLiteList = new List<object>();
                while (reader.Read())
                    myUserLiteList.Add(GetLiteUserMapper(reader));


                reader.Close();
                return myUserLiteList;
            }
            else
                return null;
        }


        public List<RoleUsersLite> FindRoleUsersLite(int RoleId)
        {
            List<RoleUsersLite> User = new List<RoleUsersLite>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);

            Instance.AddInParameter("@role_id", System.Data.SqlDbType.Int, RoleId);
            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_Role_Users_FindAll_Lite", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    User.Add(GetLiteRoleUserMapper(reader));
                }
                reader.Close();
                return User;
            }
            else
            {
                return null;
            }
        }


        public void AddUserRoles(int roleId, string RoleUserIds)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@role_users_ids", System.Data.SqlDbType.NVarChar, RoleUserIds);
            Instance.AddInParameter("@role_id", System.Data.SqlDbType.Int, roleId);

            Instance.ExcuteNonQuery("dbo.SP_UserRoles_Update", conn, false);

            //myEntity.UserId = (int)Instance.GetOutParamValue("@Id");
        }
        public void AddUserTeams(int teamId, string TeamUserIds)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            Instance.AddInParameter("@team_users_ids", System.Data.SqlDbType.NVarChar, TeamUserIds);
            Instance.AddInParameter("@team_id", System.Data.SqlDbType.Int, teamId);

            Instance.ExcuteNonQuery("dbo.SP_UserTeam_Update", conn, false);

            //myEntity.UserId = (int)Instance.GetOutParamValue("@Id");
        }

        public List<TeamUsersLite> FindTeamUsersLite(int TeamId)
        {
            List<TeamUsersLite> User = new List<TeamUsersLite>();
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);

            Instance.AddInParameter("@team_id", System.Data.SqlDbType.Int, TeamId);
            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_Team_Users_FindAll_Lite", conn);
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    User.Add(GetLiteTeamUserMapper(reader));
                }
                reader.Close();
                return User;
            }
            else
            {
                return null;
            }
        }
        public bool CheckUserCount()
        {

            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);
            SqlDataReader reader;
            Instance.AddInParameter("@valid", System.Data.SqlDbType.Bit, null);
            reader = Instance.ExcuteReader("dbo.SP_User_ValidateCount", conn);


            if (reader != null && reader.HasRows)
            {
                reader.Read();
                return (bool)reader["is_valid"];

            }
            else
                return false;
        }


        public User ClientLogin(string UserName, string Password, string AccessCode)
        {
            SQLDAHelper Instance = new SQLDAHelper();
            SqlConnection conn = Instance.GetConnection(false);

            Instance.AddInParameter("@UserName", System.Data.SqlDbType.NChar, UserName);
            Instance.AddInParameter("@Password", System.Data.SqlDbType.NChar, Password);
            if (!string.IsNullOrEmpty(AccessCode))
                Instance.AddInParameter("@AccessCode", System.Data.SqlDbType.NChar, AccessCode);

            SqlDataReader reader = Instance.ExcuteReader("dbo.SP_User_Login", conn);
            if (reader != null && reader.HasRows)
            {
                reader.Read();
                User myuser = GetMapper(reader);
                reader.Close();
                return myuser;
            }
            else
            {
                return null;
            }
        }
    }
}
