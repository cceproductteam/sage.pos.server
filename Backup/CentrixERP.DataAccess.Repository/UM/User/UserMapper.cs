using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.Framework;
using SF.Framework.Data;
using Centrix.UM.Business.Entity;
using Centrix.UM.DataAccess.IRepository;
using System.Data.SqlClient;

namespace Centrix.UM.DataAccess.Repository
{
    public partial class UserRepository
    {
        public User GetMapper(SqlDataReader reader)
        {
            User myUser = new User();
            if (reader["users_id"] != DBNull.Value)
            {
                myUser.UserId = (int)reader["users_id"];
             
            }
            if (reader["flag_deleted"] != DBNull.Value)
            {
                myUser.FlagDeleted = (bool)reader["flag_deleted"];

            }
            if (reader["role_id"] != DBNull.Value)
            {
                myUser.RoleId = (int)reader["role_id"];
            }
            if (reader["team_id"] != DBNull.Value)
            {
                myUser.TeamId = (int)reader["team_id"];
            }
            if (reader["updated_by"] != DBNull.Value)
            {
                myUser.UpdatedBy = (int)reader["updated_by"];
            }
            if (reader["users_dob"] != DBNull.Value)
            {
                myUser.Dob = Convert.ToDateTime(reader["users_dob"]).GetLocalTime();
            }
            if (reader["users_firstname"] != DBNull.Value)
            {
                myUser.FirstnameEnglish = reader["users_firstname"].ToString();
            }
            if (reader["users_firstname_ar"] != DBNull.Value)
            {
                myUser.FirstNameArabic = reader["users_firstname_ar"].ToString();
            }
            if (reader["users_lastname_ar"] != DBNull.Value)
            {
                myUser.LastNameArabic = reader["users_lastname_ar"].ToString();
            }
            if (reader["users_lastname"] != DBNull.Value)
            {
                myUser.LastNameEnglish = reader["users_lastname"].ToString();
            }
            if (reader["users_middlename"] != DBNull.Value)
            {
                myUser.MiddleNameEnglish = reader["users_middlename"].ToString();
            }
            if (reader["users_middlename_ar"] != DBNull.Value)
            {
                myUser.MiddleNameArabic = reader["users_middlename_ar"].ToString();
            }
            if (reader["users_notes"] != DBNull.Value)
            {
                myUser.Notes = reader["users_notes"].ToString();
            }

            if (reader["access_code"] != DBNull.Value)
            {
                myUser.AccessCode = reader["access_code"].ToString();
            }
            if (reader["users_password"] != DBNull.Value)
            {
                // myUser.Password = reader["users_password"].ToString();
                try
                {
                     myUser.Password = Cryption.Decrypt.String(reader["users_password"].ToString(), Cryption.Algorithm.AES);
                }
                catch (Exception)
                {
                }

            }
            if (reader["users_title"] != DBNull.Value)
            {
                myUser.Title = reader["users_title"].ToString();
            }
            if (reader["users_username"] != DBNull.Value)
            {
                myUser.UserName = reader["users_username"].ToString();
            }
            if (reader["created_by"] != DBNull.Value)
            {
                myUser.CreatedBy = (int)reader["created_by"];
            }
            if (reader["is_default"] != DBNull.Value)
            {
                myUser.IsDefault = Convert.ToBoolean(reader["is_default"]);
            }
            if (reader["receive_notifications"] != DBNull.Value)
            {
                myUser.ReceiveNoyifications = Convert.ToBoolean(reader["receive_notifications"]);
            }
            if (reader["full_name"] != DBNull.Value)
            {
                myUser.Fullname = (string)reader["full_name"];
            }
            if (reader["is_manager"] != DBNull.Value)
            {
                myUser.IsManager = (bool)reader["is_manager"];
            }

            if (reader["is_pos_user"] != DBNull.Value)
            {
                myUser.IsPosUser = (bool)reader["is_pos_user"];
            }

            if (reader["manager_id"] != DBNull.Value)
            {
                myUser.ManagerId = SQLDAHelper.Convert_DBTOInt(reader["manager_id"]);
            }

            if (reader["is_super_user"] != DBNull.Value)
            {
                myUser.IsSupperUser =(bool)(reader["is_super_user"]);
            }
            return myUser;
        }

        public UserLite GetLiteUserMapper(SqlDataReader reader)
        {
            UserLite myUser = new UserLite();

            if (reader["users_id"] != DBNull.Value)
            {
                myUser.UserId = (int)reader["users_id"];
                myUser.Id = (int)reader["users_id"];
                myUser.value = (int)reader["users_id"];
            }
            if (reader["users_username"] != DBNull.Value)
            {
                myUser.UserName = reader["users_username"].ToString();
            }
            if (reader["users_password"] != DBNull.Value)
            {
                myUser.Password = Cryption.Decrypt.String(reader["users_password"].ToString(), Cryption.Algorithm.AES);
            }

            if (reader["is_pos_user"] != DBNull.Value)
            {
                myUser.IsPosUser = (bool)reader["is_pos_user"];
            }

            if (reader["access_code"] != DBNull.Value)
            {
                myUser.AccessCode = reader["access_code"].ToString();
            }

            if (reader["full_name"] != DBNull.Value)
            {
                myUser.FullName = reader["full_name"].ToString();
                myUser.label = reader["full_name"].ToString();
            }
            if (reader["users_firstname"] != DBNull.Value)
            {
                myUser.FirstnameEnglish = reader["users_firstname"].ToString();
            }
            if (reader["users_middlename"] != DBNull.Value)
            {
                myUser.MiddleNameEnglish = reader["users_middlename"].ToString();
            }
            if (reader["users_lastname"] != DBNull.Value)
            {
                myUser.LastNameEnglish = reader["users_lastname"].ToString();
            }
            if (reader["users_firstname_ar"] != DBNull.Value)
            {
                myUser.FirstNameArabic = reader["users_firstname_ar"].ToString();
            }
            if (reader["users_middlename_ar"] != DBNull.Value)
            {
                myUser.MiddleNameArabic = reader["users_middlename_ar"].ToString();
            }
            if (reader["users_lastname_ar"] != DBNull.Value)
            {
                myUser.LastNameArabic = reader["users_lastname_ar"].ToString();
            }
            if (reader["users_dob"] != DBNull.Value)
            {
                myUser.Dob = Convert.ToDateTime(reader["users_dob"]).GetLocalTime();
            }
            if (reader["users_notes"] != DBNull.Value)
            {
                myUser.Notes = reader["users_notes"].ToString();
            }
            if (reader["users_title"] != DBNull.Value)
            {
                myUser.Title = reader["users_title"].ToString();
            }

            if (reader["role_id"] != DBNull.Value)
            {
                myUser.RoleNameId = (int)reader["role_id"];
            }
            if (reader["role_name"] != DBNull.Value)
            {
                myUser.RoleName = reader["role_name"].ToString();
            }
            if (reader["team_id"] != DBNull.Value)
            {
                myUser.TeamNameId = (int)reader["team_id"];
            }
            if (reader["team_name"] != DBNull.Value)
            {
                myUser.TeamName = reader["team_name"].ToString();
            }

            if (reader["updated_by"] != DBNull.Value)
            {
                myUser.UpdatedById = (int)reader["updated_by"];
            }
            if (reader["created_by"] != DBNull.Value)
            {
                myUser.CreatedById = (int)reader["created_by"];
            }
            if (reader["created_date"] != DBNull.Value)
            {
                myUser.CreatedDate = Convert.ToDateTime(reader["created_date"]).GetLocalTime();
            }
            if (reader["updated_date"] != DBNull.Value)
            {
                myUser.UpdatedDate = Convert.ToDateTime(reader["updated_date"]).GetLocalTime();
            }
            if (reader["creator_full_name"] != DBNull.Value)
            {
                myUser.CreatedByName = reader["creator_full_name"].ToString();
            }
            if (reader["updater_full_name"] != DBNull.Value)
            {
                myUser.UpdatedByName = reader["updater_full_name"].ToString();
            }

            if (reader["user_status_value"] != DBNull.Value)
            {
                myUser.IsDefaultValue = Convert.ToBoolean(reader["user_status_value"]);
            }
            if (reader["user_status"] != DBNull.Value)
            {
                myUser.IsDefault = (string)reader["user_status"];
            }
            if (reader["receive_notifications_value"] != DBNull.Value)
            {
                myUser.ReceiveNoyificationsValue = Convert.ToBoolean(reader["receive_notifications_value"]);
            }
            if (reader["receive_notifications"] != DBNull.Value)
            {
                myUser.ReceiveNoyifications = (string)reader["receive_notifications"];
            }
            if (reader["email_user"] != DBNull.Value)
            {
                myUser.UserEmail = (string)reader["email_user"];
                myUser.UserEmailTruncated = ((string)reader["email_user"]).Truncate(10);
            }

            if (reader["last_rows"] != DBNull.Value)
            {
                myUser.LastRows = (bool)reader["last_rows"];
            }
            if (reader["total_count"] != DBNull.Value)
            {
                myUser.TotalRecords = (int)reader["total_count"];
            }

            if (reader["is_manager_value"] != DBNull.Value)
            {
                myUser.IsManagerValue = (bool)reader["is_manager_value"];
            }

            if (reader["is_manager"] != DBNull.Value)
            {
                myUser.IsManager = (string)reader["is_manager"];
            }


            if (reader["manager_id"] != DBNull.Value)
            {
                myUser.ManagerId = SQLDAHelper.Convert_DBTOInt(reader["manager_id"]);
            }
            if (reader["manager_full_name"] != DBNull.Value)
            {
                myUser.Manager= (string)reader["manager_full_name"];
            }
            if (reader["is_super_user"] != DBNull.Value)
            {
                myUser.IsSupperUser = (bool)(reader["is_super_user"]);
            }
            if (reader["is_management"] != DBNull.Value)
            {
                myUser.IsManagement = (bool)reader["is_management"];
            }

            return myUser;
        }

        public RoleUsersLite GetLiteRoleUserMapper(SqlDataReader reader)
        {
            RoleUsersLite myUser = new RoleUsersLite();

            if (reader["users_id"] != DBNull.Value)
            {
                myUser.UserId = (int)reader["users_id"];
                myUser.Id = (int)reader["users_id"];
                myUser.value = (int)reader["users_id"];
            }
            if (reader["users_username"] != DBNull.Value)
            {
                myUser.UserName = reader["users_username"].ToString();
            }
            if (reader["users_password"] != DBNull.Value)
            {
                myUser.Password = Cryption.Decrypt.String(reader["users_password"].ToString(), Cryption.Algorithm.AES);
            }

            if (reader["is_pos_user"] != DBNull.Value)
            {
                myUser.IsPosUser = (bool)reader["is_pos_user"];
            }

            if (reader["full_name"] != DBNull.Value)
            {
                myUser.FullName = reader["full_name"].ToString();
                myUser.label = reader["full_name"].ToString();
            }
            if (reader["users_firstname"] != DBNull.Value)
            {
                myUser.FirstnameEnglish = reader["users_firstname"].ToString();
            }
            if (reader["users_middlename"] != DBNull.Value)
            {
                myUser.MiddleNameEnglish = reader["users_middlename"].ToString();
            }
            if (reader["users_lastname"] != DBNull.Value)
            {
                myUser.LastNameEnglish = reader["users_lastname"].ToString();
            }
            if (reader["users_firstname_ar"] != DBNull.Value)
            {
                myUser.FirstNameArabic = reader["users_firstname_ar"].ToString();
            }
            if (reader["users_middlename_ar"] != DBNull.Value)
            {
                myUser.MiddleNameArabic = reader["users_middlename_ar"].ToString();
            }
            if (reader["users_lastname_ar"] != DBNull.Value)
            {
                myUser.LastNameArabic = reader["users_lastname_ar"].ToString();
            }
            if (reader["users_dob"] != DBNull.Value)
            {
                myUser.Dob = Convert.ToDateTime(reader["users_dob"]).GetLocalTime();
            }
            if (reader["users_notes"] != DBNull.Value)
            {
                myUser.Notes = reader["users_notes"].ToString();
            }
            if (reader["users_title"] != DBNull.Value)
            {
                myUser.Title = reader["users_title"].ToString();
            }

            if (reader["role_id"] != DBNull.Value)
            {
                myUser.RoleNameId = (int)reader["role_id"];
            }
            if (reader["role_name"] != DBNull.Value)
            {
                myUser.RoleName = reader["role_name"].ToString();
            }
            if (reader["team_id"] != DBNull.Value)
            {
                myUser.TeamNameId = (int)reader["team_id"];
            }
            if (reader["team_name"] != DBNull.Value)
            {
                myUser.TeamName = reader["team_name"].ToString();
            }

            if (reader["updated_by"] != DBNull.Value)
            {
                myUser.UpdatedById = (int)reader["updated_by"];
            }
            if (reader["created_by"] != DBNull.Value)
            {
                myUser.CreatedById = (int)reader["created_by"];
            }
            if (reader["created_date"] != DBNull.Value)
            {
                myUser.CreatedDate = Convert.ToDateTime(reader["created_date"]).GetLocalTime();
            }
            if (reader["updated_date"] != DBNull.Value)
            {
                myUser.UpdatedDate = Convert.ToDateTime(reader["updated_date"]).GetLocalTime();
            }
            if (reader["creator_full_name"] != DBNull.Value)
            {
                myUser.CreatedByName = reader["creator_full_name"].ToString();
            }
            if (reader["updater_full_name"] != DBNull.Value)
            {
                myUser.UpdatedByName = reader["updater_full_name"].ToString();
            }

            if (reader["user_status_value"] != DBNull.Value)
            {
                myUser.IsDefaultValue = Convert.ToBoolean(reader["user_status_value"]);
            }
            if (reader["user_status"] != DBNull.Value)
            {
                myUser.IsDefault = (string)reader["user_status"];
            }
            if (reader["receive_notifications_value"] != DBNull.Value)
            {
                myUser.ReceiveNoyificationsValue = Convert.ToBoolean(reader["receive_notifications_value"]);
            }
            if (reader["receive_notifications"] != DBNull.Value)
            {
                myUser.ReceiveNoyifications = (string)reader["receive_notifications"];
            }
            if (reader["email_user"] != DBNull.Value)
            {
                myUser.UserEmail = (string)reader["email_user"];
                myUser.UserEmailTruncated = ((string)reader["email_user"]).Truncate(10);
            }

            if (reader["last_rows"] != DBNull.Value)
            {
                myUser.LastRows = (bool)reader["last_rows"];
            }
            if (reader["total_count"] != DBNull.Value)
            {
                myUser.TotalRecords = (int)reader["total_count"];
            }

            if (reader["is_manager_value"] != DBNull.Value)
            {
                myUser.IsManagerValue = (bool)reader["is_manager_value"];
            }

            if (reader["is_manager"] != DBNull.Value)
            {
                myUser.IsManager = (string)reader["is_manager"];
            }


            if (reader["manager_id"] != DBNull.Value)
            {
                myUser.ManagerId = SQLDAHelper.Convert_DBTOInt(reader["manager_id"]);
            }
            if (reader["manager_full_name"] != DBNull.Value)
            {
                myUser.Manager = (string)reader["manager_full_name"];
            }
            if (reader["is_super_user"] != DBNull.Value)
            {
                myUser.IsSupperUser = (bool)(reader["is_super_user"]);
            }
            if (reader["is_management"] != DBNull.Value)
            {
                myUser.IsManagement = (bool)reader["is_management"];
            }

            if (reader["has_role"] != DBNull.Value)
            {
                myUser.HasRole = (bool)reader["has_role"];
            }

            return myUser;
        }

        public TeamUsersLite GetLiteTeamUserMapper(SqlDataReader reader)
        {
            TeamUsersLite myUser = new TeamUsersLite();

            if (reader["users_id"] != DBNull.Value)
            {
                myUser.UserId = (int)reader["users_id"];
                myUser.Id = (int)reader["users_id"];
                myUser.value = (int)reader["users_id"];
            }
            if (reader["users_username"] != DBNull.Value)
            {
                myUser.UserName = reader["users_username"].ToString();
            }
            if (reader["users_password"] != DBNull.Value)
            {
                myUser.Password = Cryption.Decrypt.String(reader["users_password"].ToString(), Cryption.Algorithm.AES);
            }

            if (reader["is_pos_user"] != DBNull.Value)
            {
                myUser.IsPosUser = (bool)reader["is_pos_user"];
            }
            if (reader["full_name"] != DBNull.Value)
            {
                myUser.FullName = reader["full_name"].ToString();
                myUser.label = reader["full_name"].ToString();
            }
            if (reader["users_firstname"] != DBNull.Value)
            {
                myUser.FirstnameEnglish = reader["users_firstname"].ToString();
            }
            if (reader["users_middlename"] != DBNull.Value)
            {
                myUser.MiddleNameEnglish = reader["users_middlename"].ToString();
            }
            if (reader["users_lastname"] != DBNull.Value)
            {
                myUser.LastNameEnglish = reader["users_lastname"].ToString();
            }
            if (reader["users_firstname_ar"] != DBNull.Value)
            {
                myUser.FirstNameArabic = reader["users_firstname_ar"].ToString();
            }
            if (reader["users_middlename_ar"] != DBNull.Value)
            {
                myUser.MiddleNameArabic = reader["users_middlename_ar"].ToString();
            }
            if (reader["users_lastname_ar"] != DBNull.Value)
            {
                myUser.LastNameArabic = reader["users_lastname_ar"].ToString();
            }
            if (reader["users_dob"] != DBNull.Value)
            {
                myUser.Dob = Convert.ToDateTime(reader["users_dob"]).GetLocalTime();
            }
            if (reader["users_notes"] != DBNull.Value)
            {
                myUser.Notes = reader["users_notes"].ToString();
            }
            if (reader["users_title"] != DBNull.Value)
            {
                myUser.Title = reader["users_title"].ToString();
            }

            if (reader["role_id"] != DBNull.Value)
            {
                myUser.RoleNameId = (int)reader["role_id"];
            }
            if (reader["role_name"] != DBNull.Value)
            {
                myUser.RoleName = reader["role_name"].ToString();
            }
            if (reader["team_id"] != DBNull.Value)
            {
                myUser.TeamNameId = (int)reader["team_id"];
            }
            if (reader["team_name"] != DBNull.Value)
            {
                myUser.TeamName = reader["team_name"].ToString();
            }

            if (reader["updated_by"] != DBNull.Value)
            {
                myUser.UpdatedById = (int)reader["updated_by"];
            }
            if (reader["created_by"] != DBNull.Value)
            {
                myUser.CreatedById = (int)reader["created_by"];
            }
            if (reader["created_date"] != DBNull.Value)
            {
                myUser.CreatedDate = Convert.ToDateTime(reader["created_date"]).GetLocalTime();
            }
            if (reader["updated_date"] != DBNull.Value)
            {
                myUser.UpdatedDate = Convert.ToDateTime(reader["updated_date"]).GetLocalTime();
            }
            if (reader["creator_full_name"] != DBNull.Value)
            {
                myUser.CreatedByName = reader["creator_full_name"].ToString();
            }
            if (reader["updater_full_name"] != DBNull.Value)
            {
                myUser.UpdatedByName = reader["updater_full_name"].ToString();
            }

            if (reader["user_status_value"] != DBNull.Value)
            {
                myUser.IsDefaultValue = Convert.ToBoolean(reader["user_status_value"]);
            }
            if (reader["user_status"] != DBNull.Value)
            {
                myUser.IsDefault = (string)reader["user_status"];
            }
            if (reader["receive_notifications_value"] != DBNull.Value)
            {
                myUser.ReceiveNoyificationsValue = Convert.ToBoolean(reader["receive_notifications_value"]);
            }
            if (reader["receive_notifications"] != DBNull.Value)
            {
                myUser.ReceiveNoyifications = (string)reader["receive_notifications"];
            }
            if (reader["email_user"] != DBNull.Value)
            {
                myUser.UserEmail = (string)reader["email_user"];
                myUser.UserEmailTruncated = ((string)reader["email_user"]).Truncate(10);
            }

            if (reader["last_rows"] != DBNull.Value)
            {
                myUser.LastRows = (bool)reader["last_rows"];
            }
            if (reader["total_count"] != DBNull.Value)
            {
                myUser.TotalRecords = (int)reader["total_count"];
            }

            if (reader["is_manager_value"] != DBNull.Value)
            {
                myUser.IsManagerValue = (bool)reader["is_manager_value"];
            }

            if (reader["is_manager"] != DBNull.Value)
            {
                myUser.IsManager = (string)reader["is_manager"];
            }


            if (reader["manager_id"] != DBNull.Value)
            {
                myUser.ManagerId = SQLDAHelper.Convert_DBTOInt(reader["manager_id"]);
            }
            if (reader["manager_full_name"] != DBNull.Value)
            {
                myUser.Manager = (string)reader["manager_full_name"];
            }
            if (reader["is_super_user"] != DBNull.Value)
            {
                myUser.IsSupperUser = (bool)(reader["is_super_user"]);
            }
            if (reader["is_management"] != DBNull.Value)
            {
                myUser.IsManagement = (bool)reader["is_management"];
            }

            if (reader["has_team"] != DBNull.Value)
            {
                myUser.HasTeam = (bool)reader["has_team"];
            }

            return myUser;
        }
    }
}