using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CentrixERP.Common.Business.Entity;

namespace CentrixERP.Common.Business.Factory
{
    public interface INote:IBaseClass
    {
        int NoteId { get; set; }
        Note NoteObj { get; set; }        
    }
}
