using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CentrixERP.Common.Business.Entity;

namespace CentrixERP.Common.Business.Factory
{
    public interface IAddress :IBaseClass
    {
        int AddressId { get; set; }
        Address AddressObj { get; set; }
        bool Isdefault { get; set; }
    }
}
