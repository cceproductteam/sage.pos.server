using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;

namespace CentrixERP.Common.Business.Factory
{
    public interface IBaseClass
    {
        void MarkDeleted();
        void MarkModified();

        /// <summary>
        /// Module Id like PersonId
        /// </summary>
        int ModuleId { get; set; }

        /// <summary>
        /// InterSection EntityId
        /// </summary>
        int Id { get; set; }

        int CreatedBy { get; set; }
        int UpdatedBy { get; set; }
    }
}
