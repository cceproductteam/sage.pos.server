using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CentrixERP.Common.Business.Entity; 

namespace CentrixERP.Common.Business.Factory
{
    public interface IPhone : IBaseClass
    {
        int PhoneId { get; set; }
        Phone PhoneObj { get; set; }
        bool Isdefault { get; set; }
    }
}
