using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CentrixERP.Common.Business.Factory
{
    public interface IDocumentCriteria
    {
        string Keyword { get; set; }
        string ParentFolder { get; set; }
        int ParentFolderId { get; set; }
    }
}
