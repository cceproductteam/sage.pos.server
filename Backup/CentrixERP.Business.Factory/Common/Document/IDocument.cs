using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CentrixERP.Common.Business.Entity;

namespace CentrixERP.Common.Business.Factory
{
    public interface IDocument:IBaseClass
    {
        int DocumentId { get; set; }
        Document DocumentObj { get; set; }
        string DocumentPath { get; }
        string DocumentUrl { get; }
        string ModuleName { get; }
    }
}
