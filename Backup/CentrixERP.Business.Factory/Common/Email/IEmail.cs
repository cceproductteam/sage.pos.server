using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CentrixERP.Common.Business.Entity; 

namespace CentrixERP.Common.Business.Factory
{
    public interface IEmail:IBaseClass
    {
        int EmailId { get; set; }
        Email EmailObj { get; set; }
        bool Isdefault { get; set; }
    }
}
