﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;


using SF.Framework;
using System.Web.Script.Services;
using CentrixERP.API.Common.BaseClasses;
using System.Reflection;
using CentrixERP.Common.API;
using CentrixERP.Configuration;
using CentrixERP.Common.Web;

namespace CentrixERP.API
{
    /// <summary>
    /// Summary description for CustomerWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    
    public class CustomerWebService : WebServiceBaseClass
    {
      
        public CustomerWebService()
        {
          
        }

    //    [WebMethod]
    //    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    //    public string Delete(int id)
    //    {
        
    //        try
    //        {
               
    //            return this.AttachStatusCode(null, 1, null);
    //        }
    //        catch (Exception ex)
    //        {
    //            return this.AttachStatusCode(null, 0, HandleException(ex));
    //        }
    //    }

    //    [WebMethod]
    //    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    //    public string FindAllLite(string keyword, int page, int resultCount, string argsCriteria)
    //    {
    //        E.CustomerCriteria criteria = null;
    //        if (!string.IsNullOrEmpty(argsCriteria))
    //        {
    //            criteria = this.javaScriptSerializer.Deserialize<E.CustomerCriteria>(argsCriteria);
    //        }
    //        if (criteria == null)
    //            criteria = new E.CustomerCriteria();


    //        criteria.KeyWord = keyword;
    //        criteria.pageNumber = page;
    //        criteria.resultCount = resultCount;


           
    //        return this.AttachStatusCode(list, 1, null);
    //    }

    //    [WebMethod]
    //    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    //    public string FindByIdLite(int id)
    //    {
           
    //        return this.AttachStatusCode(obj, 1, null);
    //    }

    //    [WebMethod]
    //    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    //    public string Add(int customerId, string customerInfo)
    //    {
    //        return Save(0, customerInfo);
    //    }

    //    [WebMethod]
    //    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    //    public string Edit(int customerId, string customerInfo)
    //    {
    //        return Save(customerId, customerInfo);
    //    }

    //    [WebMethod]
    //    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    //    public string GetOutStandingInvoices(int customerId)
    //    {
           
    //        criteria.CustomerId = customerId;
           
    //        return this.AttachStatusCode(list, 1, null);
    //    }

    //    private string Save(int customerId, string customerInfo)
    //    {
    //        customerInfo = Server.UrlDecode(customerInfo);
    //        Dictionary<string, Object> entityInfo = this.javaScriptSerializer.Deserialize<Dictionary<string, Object>>(customerInfo);
    //        E.Customer customer = null;


    //        if (customerId > 0)
    //        {
    //            //LockEntity(Configuration.Enums_S3.Entity.Customer, customerId);
    //            //customer = iCustomerManager.FindById(customerId, null);
    //            //customer.MarkOld();
    //            //customer.MarkModified();
    //            customer.UpdatedBy = LoggedInUser.UserId;
              
    //        }
    //        else
    //        {
    //            customer = new E.Customer();
    //            customer.CreatedBy = LoggedInUser.UserId;
    //        }

    //        //customer = this.javaScriptSerializer.Deserialize<E.Customer>(customerInfo);
    //        this.GetObjectFromDictionary<E.Customer>(customer, entityInfo);
    //        customer.CustomerId = customerId;

    //        List<Object> validations = ValidateCustomer(customer, customerId > 0);
    //        if (validations != null && validations.Count > 0)
    //            return this.AttachStatusCode(validations, 0, "validation");

    //        if (customer.Status && customerId>0)
    //        {
    //            CheckRelatedEntity((int)Enums_S3.Entity.Customer, customerId, false);
    //            //List<EntityRelations> EntityRelationsList = EntityRelationsCheck.CheckEntityRelations((int)Enums_S3.Entity.Customer, customerId, null);
    //            //if (EntityRelationsList != null && EntityRelationsList.Count() > 0)
    //            //    return this.AttachStatusCode(null, (int)Enums_S3.StatusCode.Codes.FailedToInactiveItem, null);
    //        }
    //        try
    //        {
    //            //iCustomerManager.Save(customer);
    //            if (customerId > 0)
    //            {
    //                //E.CustomerLite liteCustomer = this.iCustomerManager.FindByIdLite(customer.CustomerId, null);

    //                return this.AttachStatusCode(liteCustomer, 1, null);
    //            }

    //            else
    //            {
    //                return this.AttachStatusCode(null, 501, null);
                
    //            }
    //        }
    //        catch (SF.Framework.Exceptions.RecordExsitsException ex)
    //        {
    //            return this.AttachStatusCode(null, 501, null);
    //        }
    //        catch (Exception ex)
    //        {
    //            return this.AttachStatusCode(null, 0, HandleException(ex));
    //        }
    //    }

       

    //    //private List<Object> ValidateCustomer(E.Customer customer,bool edit)
    //    //{
    //    //    List<Object> errors = new List<object>();

    //    //    //if (!edit)
    //    //    //{
    //    //    //    if (string.IsNullOrEmpty(customer.CustomerNumber))
    //    //    //    {
    //    //    //        errors.Add(getErrorValidationObj(1, "Required", false, "CustomerNumber"));
    //    //    //    }
    //    //    //}
    //    //    if (string.IsNullOrEmpty(customer.CustomerName))
    //    //    {
    //    //        errors.Add(getErrorValidationObj(1, "Required", false, "CustomerName","required"));
    //    //    }

    //    //    if (customer.CustomerGroupId<=0)
    //    //    {
    //    //        errors.Add(getErrorValidationObj(1, "Required", false, "CustomerGroup", "required"));
    //    //    }
    //    //    else if (customer.CustomerGroupObj == null)
    //    //    {
    //    //        errors.Add(getErrorValidationObj(1, "Invalid group", false, "CustomerGroup", "Invalid"));
    //    //    }


    //    //    if (customer.AccountTypeId <= 0)
    //    //    {
    //    //        errors.Add(getErrorValidationObj(1, "Required", false, "AccountType", "required"));
    //    //    }
    //    //    else if (customer.AccountTypeObj == null)
    //    //    {
    //    //        errors.Add(getErrorValidationObj(1, "Inavlid account type", false, "AccountType", "Invalid"));
    //    //    }
    //    //    if (customer.AccountSetId <= 0)
    //    //    {
    //    //        errors.Add(getErrorValidationObj(1, "Required", false, "AccountSet", "required"));
    //    //    }
    //    //    else if (customer.AccountSetObj == null)
    //    //    {
    //    //        errors.Add(getErrorValidationObj(1, "Inavlid account set", false, "AccountSet", "Invalid"));
    //    //    }
    //    //    if (customer.PaymentTermCodeId <= 0)
    //    //    {
    //    //        errors.Add(getErrorValidationObj(1, "Required", false, "PaymentTerm", "required"));
    //    //    }
    //    //    else if (customer.PaymentTermObj == null)
    //    //    {
    //    //        errors.Add(getErrorValidationObj(1, "Inavlid payment term", false, "PaymentTerm", "Invalid"));
    //    //    }

    //    //    if (customer.DeliveryMethodId <= 0)
    //    //    {
    //    //        errors.Add(getErrorValidationObj(1, "Required", false, "DeliveryMethod", "required"));
    //    //    }
    //    //    else if (customer.DeliveryMethodObj == null)
    //    //    {
    //    //        errors.Add(getErrorValidationObj(1, "Inavlid delivery method", false, "DeliveryMethod", "Invalid"));
    //    //    }



    //    //    if (customer.CustomerTypeId <= 0)
    //    //    {
    //    //        errors.Add(getErrorValidationObj(1, "Required", false, "CustomerType", "required"));
    //    //    }
    //    //    else if (customer.CustomerTypeObj == null)
    //    //    {
    //    //        errors.Add(getErrorValidationObj(1, "Inavlid customer type", false, "CustomerType", "Invalid"));
    //    //    }

    //    //    if (customer.CheckDuplicatPosId <= 0)
    //    //    {
    //    //        errors.Add(getErrorValidationObj(1, "Required", false, "CheckDuplicatPos", "required"));
    //    //    }
    //    //    else if (customer.CheckDuplicatPosObj == null)
    //    //    {
    //    //        errors.Add(getErrorValidationObj(1, "Inavlid check duplicat POs", false, "CheckDuplicatPos", "Invalid"));
    //    //    }

    //    //    if (customer.TaxGroupId <= 0)
    //    //    {
    //    //        errors.Add(getErrorValidationObj(1, "Required", false, "TaxGroupCode", "required"));
    //    //    }
    //    //    else if (customer.TaxGroupHeaderObj == null)
    //    //    {
    //    //        errors.Add(getErrorValidationObj(1, "Inavlid tax group", false, "TaxGroupCode", "Invalid"));
    //    //    }


    //    //    if (customer.TaxAuthorityId <= 0)
    //    //    {
    //    //        errors.Add(getErrorValidationObj(1, "Required", false, "TaxAuthority", "required"));
    //    //    }
    //    //    else if (customer.TaxAuthorityObj == null)
    //    //    {
    //    //        errors.Add(getErrorValidationObj(1, "Inavlid tax authority", false, "TaxAuthority", "Invalid"));
    //    //    }

    //    //    if (customer.TaxClassId <= 0)
    //    //    {
    //    //        errors.Add(getErrorValidationObj(1, "Required", false, "TaxGroupHeader", "required"));
    //    //    }
    //    //    else if (customer.TaxGroupHeaderObj == null)
    //    //    {
    //    //        errors.Add(getErrorValidationObj(1, "Inavlid tax class", false, "TaxGroupHeader", "Invalid"));
    //    //    }

    //    //    return errors;
    //    //}

    }
}