using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;


using CentrixERP.Common.API;
using CentrixERP.Configuration;

using SF.FrameworkEntity;
using CentrixERP.Business.IManager;
using CentrixERP.Business.Entity;
using System.Reflection;
using CX.Framework;
using CentrixERP.API.Common.BaseClasses;
using E=CentrixERP.Business.Entity;

namespace CentrixERP.API
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]

    public class InvoiceWebService : WebServiceBaseClass
    {
        IInvoiceManager iInvoiceManager = null;
        IInvoiceCustomFeildsManager iCustomerfieldManager = null;
        public InvoiceWebService()
        {
            this.javaScriptSerializer.MaxJsonLength = int.MaxValue;
            iInvoiceManager = (IInvoiceManager)IoC.Instance.Resolve(typeof(IInvoiceManager));
            iCustomerfieldManager = (IInvoiceCustomFeildsManager)IoC.Instance.Resolve(typeof(IInvoiceCustomFeildsManager));
        }
        
        [WebMethod]
       
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string Delete(int id)
        {
            //this.LockEntityForDelete(Enums_S3.Entity.Invoice, id);
            E.Invoice myInvoice = iInvoiceManager.FindById(id, null);
            myInvoice.InvoiceId = id;
            myInvoice.MarkDeleted();
            try
            {
                iInvoiceManager.Save(myInvoice);
                return AttachStatusCode(true, 1, null);
            }
            catch (Exception ex)
            {
                return AttachStatusCode(null, 0, HandleException(ex));
            }

            

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindAllLite(string keyword, int page, int resultCount, string argsCriteria)
        {
            E.InvoiceCriteria criteria = null;
            if (!SF.Framework.String.IsEmpty(argsCriteria))
            {
                criteria = this.javaScriptSerializer.Deserialize<E.InvoiceCriteria>(argsCriteria);
            }
            else
                criteria = new E.InvoiceCriteria();

            criteria.Keyword = keyword;
            criteria.pageNumber = page;
            criteria.resultCount = resultCount;


            List<E.InvoiceLite> InvoiceList = iInvoiceManager.FindAllLite(criteria);
            return this.AttachStatusCode(InvoiceList, 1, null);

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindByIdLite(int id)
        {
            E.InvoiceLite myInvoice = iInvoiceManager.FindByIdLite(id, null);
            return this.AttachStatusCode(myInvoice, 1, null);

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string Add(int id, string InvoiceInfo)
        {
            return Save(true, id, InvoiceInfo);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.Edit)]
        public string Edit(int id, string InvoiceInfo)
        {
            if (id <= 0)
            {
                ValidationError error = new ValidationError()
                {
                    ControlName = "",
                    GlobalMessage = true,
                    Message = "Please select Invoice"
                };
                List<ValidationError> errors = new List<ValidationError>(){
                error
                };
                return this.AttachStatusCode(errors, 0, "validation");
            }
            else
            {
                return Save(false, id, InvoiceInfo);
            }
        }

        private string Save(bool add, int id, string InvoiceInfo)
        {
            E.Invoice myInvoice = this.javaScriptSerializer.Deserialize<E.Invoice>(this.unEscape((InvoiceInfo)));
            if (!add)
            {
                this.LockEntity(Enums_S3.Entity.Invoice, id);
                myInvoice.MarkOld();
                myInvoice.MarkModified();
                myInvoice.InvoiceId = id;
                myInvoice.UpdatedBy = LoggedInUser.UserId;
            }
            else
            {
                myInvoice.CreatedBy = LoggedInUser.UserId;
            }


            List<ValidationError> errors = iInvoiceManager.Validate(myInvoice);
            if (errors != null && errors.Count > 0)
            {
                return this.AttachStatusCode(errors, 0, "validation");
            }


            try
            {
                iInvoiceManager.Save(myInvoice);
                if (!add)
                    this.UnLockEntity(Enums_S3.Entity.Invoice, id);
                return this.AttachStatusCode(myInvoice.InvoiceId, 1, null);

            }
            catch (SF.Framework.Exceptions.RecordExsitsException ex)
            {
                return this.AttachStatusCode(null, 501, null);
            }
            catch (Exception ex)
            {

                return this.AttachStatusCode(null, 0, HandleException(ex));
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string AddEditInvoice(int InvoiceId, Dictionary<string, Object> InvoiceInfo, int UserId, Object[] InvoicePaymentInfo, int StoreId, int RegisterId, Object[] CustomFieldsInfo)
        {
            IInvoiceManager InvoiceManager = (IInvoiceManager)IoC.Instance.Resolve(typeof(IInvoiceManager));
            IInvoicePaymentsManager invoicePaymentManager = (IInvoicePaymentsManager)IoC.Instance.Resolve(typeof(IInvoicePaymentsManager));
            IInvoiceItemManager invoiceItemManager = (IInvoiceItemManager)IoC.Instance.Resolve(typeof(IInvoiceItemManager));
            E.InvoiceItemCriteria ItemCriteria = new E.InvoiceItemCriteria();
            E.InvoicePaymentsCriteria paymentCriteria = new E.InvoicePaymentsCriteria();
            ItemCriteria.InvoiceId = InvoiceId;
            paymentCriteria.InvoiceId = InvoiceId;

            E.Invoice myInvoice = null;

            if (InvoiceId <= 0)
            {
                myInvoice = new E.Invoice();
                myInvoice.CreatedBy = UserId;
                myInvoice.StoreId = StoreId;
                myInvoice.RegisterId = RegisterId;
                myInvoice.InvoiceDate = DateTime.Now;
            }
            else
            {
                myInvoice = InvoiceManager.FindById(InvoiceId, null);
                myInvoice.UpdatedBy = UserId;
                myInvoice.MarkModified();
                myInvoice.InvoiceDate = DateTime.Now;
            }



            foreach (KeyValuePair<string, Object> val in InvoiceInfo)
            {
                PropertyInfo pro = myInvoice.GetType().GetProperty(val.Key);
                if (pro == null || pro.Name == "InvoiceDate")
                    continue;

                object value = null;
                if (val.Value != null && !string.IsNullOrEmpty(val.Value.ToString()))
                    value = val.Value;

                pro.SetValue(myInvoice, GetPropertyValue(pro.PropertyType, value), null);
            }

            myInvoice.InvoicePaymentList = new List<E.InvoicePayments>();
            myInvoice.InvoiceItemList = new List<E.InvoiceItem>();
            if (InvoiceId <= 0)
                myInvoice.InvoiceDate = DateTime.Now;
            if (InvoiceId > 0)
            {
                List<E.InvoicePayments> paymentList = invoicePaymentManager.FindAll(paymentCriteria);
                if (paymentList != null && paymentList.Count() > 0)
                    myInvoice.InvoicePaymentList = paymentList;
                myInvoice.InvoiceItemList = invoiceItemManager.FindAll(ItemCriteria);
            }


            List<string> ids = (from Object obj in (Object[])InvoiceInfo["InvoiceItems"]
                                select ((Dictionary<string, object>)obj)["InvoiceItemId"].ToString()).ToList();

           
           
               

            List<string> paymentids = (from Object obj in (Object[])InvoicePaymentInfo
                                       select ((Dictionary<string, object>)obj)["InvoicePaymentsId"].ToString()).ToList();

                foreach (string id in paymentids)
                {
                    E.InvoicePayments paymentItem = getPaymentItem(InvoicePaymentInfo, id.ToNumber());
                    myInvoice.InvoicePaymentList.Add(paymentItem);
                }
            

            foreach (E.InvoiceItem item in myInvoice.InvoiceItemList)
            {
                var contain = from string Id in ids where item.InvoiceItemId == Convert.ToInt32(Id) select Id;
                if (contain == null || contain.Count() == 0)
                {
                    item.MarkDeleted();
                }
            }

            foreach (string id in ids)
            {
                var contain = from E.InvoiceItem item in myInvoice.InvoiceItemList
                              where item.InvoiceItemId == Convert.ToInt32(id) && Convert.ToInt32(id) != -1
                              select item;
                if (contain == null || contain.Count() == 0)
                {
                    E.InvoiceItem item = getInvoiceItem((Object[])InvoiceInfo["InvoiceItems"], id.ToNumber(), StoreId, (int)InvoiceInfo["Status"]);
                    if (item != null)
                        myInvoice.InvoiceItemList.Add(item);
                }
            }


            for (int i = 0; i < myInvoice.InvoiceItemList.Count; i++)
            {
                E.InvoiceItem item = myInvoice.InvoiceItemList[i];
                var contain = from string Id in ids
                              where item.InvoiceItemId == Convert.ToInt32(Id)
                                  && Convert.ToInt32(Id) != -1
                              select Id;
                if (contain != null && contain.Count() != 0)
                {
                    myInvoice.InvoiceItemList[i] = getInvoiceItem((Object[])InvoiceInfo["InvoiceItems"], item.InvoiceItemId, StoreId, (int)InvoiceInfo["Status"]);
                    myInvoice.InvoiceItemList[i].MarkOld();
                    myInvoice.InvoiceItemList[i].MarkModified();
                }
            }


            //myInvoice.GiftId = giftId;
            InvoiceManager.Save(myInvoice);
            //myInvoice = InvoiceManager.FindById(myInvoice.InvoiceId, null);

            E.InvoiceLite myInvoiceLite = InvoiceManager.FindByIdLite((int)myInvoice.InvoiceId, null);

           
            if (CustomFieldsInfo != null)
            {
                List<string> customactionsIDs = (from Object obj in (Object[])CustomFieldsInfo
                                                 select ((Dictionary<string, object>)obj)["CustomActionId"].ToString()).ToList();
                foreach (string id in customactionsIDs)
                {
                    E.InvoiceCustomFeilds invoiceCustomField = getCustomFieldItem(CustomFieldsInfo, id.ToNumber());
                    invoiceCustomField.InvoiceUniqueNumber = myInvoiceLite.InvoiceUniqueNumber;
                    invoiceCustomField.MarkNew();
                    try
                    {
                        iCustomerfieldManager.Save(invoiceCustomField);
                    }
                    catch (Exception ex )
                    {
                        
                        throw;
                    }
                    
                }

            }

            var invoiceObject = new
            {
                invoiceId = myInvoiceLite.InvoiceId,
                invoiceNumber = myInvoiceLite.InvoiceNumber,
                laybyNumber = myInvoiceLite.LaybyNumber,
                dateInvoice = myInvoice.InvoiceDate.Value.ToString("dd/MM/yyyy hh:mm")

            };
            
            return this.javaScriptSerializer.Serialize(invoiceObject);
        }

        private E.InvoiceItem getInvoiceItem(object[] items, int Id, int storeId, int status)
        {
           E. InvoiceItem item = null;
            foreach (Dictionary<string, Object> obj in items)
            {
                item = new E.InvoiceItem();
                if (obj["InvoiceItemId"].ToString().ToNumber() == Id)
                {
                    if (Id == -1)
                        obj["InvoiceItemId"] = "0";

                    foreach (KeyValuePair<string, Object> val in obj)
                    {
                        PropertyInfo pro = item.GetType().GetProperty(val.Key);
                        if (pro == null)
                            continue;

                        object value = null;
                        if (val.Value != null && !string.IsNullOrEmpty(val.Value.ToString()))
                            value = val.Value;

                        pro.SetValue(item, GetPropertyValue(pro.PropertyType, value), null);
                    }
                    item.InvoiceStatus = status;
                    item.StoreId = storeId;
                    break;
                }
            }
            return item;
        }

        private E.InvoicePayments getPaymentItem(object[] items, int Id)
        {
            E.InvoicePayments paymentItem = null;
            foreach (Dictionary<string, Object> obj in items)
            {
                paymentItem = new E.InvoicePayments();
                if (obj["InvoicePaymentsId"].ToString().ToNumber() == Id)
                {
                    if (Id == -1)
                        obj["InvoicePaymentsId"] = "0";

                    foreach (KeyValuePair<string, Object> val in obj)
                    {
                        PropertyInfo pro = paymentItem.GetType().GetProperty(val.Key);
                        if (pro == null)
                            continue;

                        object value = null;
                        if (val.Value != null && !string.IsNullOrEmpty(val.Value.ToString()))
                            value = val.Value;

                        pro.SetValue(paymentItem, GetPropertyValue(pro.PropertyType, value), null);

                    }
                    break;

                }
            }
            return paymentItem;
        }

        private E.InvoiceCustomFeilds getCustomFieldItem(object[] items, int Id)
        {
            E.InvoiceCustomFeilds paymentItem = null;
            foreach (Dictionary<string, Object> obj in items)
            {
                paymentItem = new E.InvoiceCustomFeilds();
                if (obj["CustomActionId"].ToString().ToNumber() == Id)
                {
                    if (Id == -1)
                        obj["CustomActionId"] = "0";

                    foreach (KeyValuePair<string, Object> val in obj)
                    {
                        PropertyInfo pro = paymentItem.GetType().GetProperty(val.Key);
                        if (pro == null)
                            continue;

                        object value = null;
                        if (val.Value != null && !string.IsNullOrEmpty(val.Value.ToString()))
                            value = val.Value;

                        pro.SetValue(paymentItem, GetPropertyValue(pro.PropertyType, value), null);

                    }
                    break;

                }
            }
            return paymentItem;
        }



        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string GetInvoiceInfo(int InvoiceId)
        {

            

            IInvoiceManager InvoiceManager = (IInvoiceManager)IoC.Instance.Resolve(typeof(IInvoiceManager));
            IInvoiceItemManager invoiceItemManager = (IInvoiceItemManager)IoC.Instance.Resolve(typeof(IInvoiceItemManager));
            E.InvoiceLite myInvoice = InvoiceManager.FindByIdLite(InvoiceId, null);
            E.InvoiceLite ParentInvoice = InvoiceManager.FindByIdLite(myInvoice.ParentInvoiceId, null);
            string ParentInvoiceNumber = "";
            if (ParentInvoice != null)
                ParentInvoiceNumber = ParentInvoice.InvoiceNumber;
            
            IPersonManager person = (IPersonManager)IoC.Instance.Resolve(typeof(IPersonManager));
            ICompanyManager company = (ICompanyManager)IoC.Instance.Resolve(typeof(ICompanyManager));

            Person personInfo = null;
            Company companyInfo = null;

            if (myInvoice.PersonId != -1)
            {
             personInfo = new Person();
                personInfo = person.FindById(myInvoice.PersonId, null);
            }
            if (myInvoice.CompanyId != -1)
            {
                 companyInfo = new Company();
                companyInfo = company.FindById(myInvoice.CompanyId, null);
            }

            string mobile = string.Empty;
            if (personInfo != null)
            {
                if (!string.IsNullOrEmpty(personInfo.MobileNumber))
                    mobile = personInfo.MobileNumber;
            }
            else if (companyInfo != null)
            {
                if (!string.IsNullOrEmpty(companyInfo.MobileNumber))
                    mobile = companyInfo.MobileNumber;
            }

            E.InvoiceItemCriteria itemCriteria = new E.InvoiceItemCriteria();
            itemCriteria.InvoiceId = InvoiceId;
            List<E.InvoiceItemLite> myInvoiceItemList = invoiceItemManager.FindAllLite(itemCriteria);
            E.InvoiceCriteria myCriteria = new E.InvoiceCriteria();

            if (myInvoice != null)
            {
                if (myInvoiceItemList != null)
                {
                    var InvoiceItemObj = from InvItem in myInvoiceItemList
                                         select new
                                         {
                                             InvoiceItemId = InvItem.InvoiceItemId,
                                             InvoiceId = InvItem.InvoiceId,
                                             ProductName = InvItem.ProductName,
                                             Quantity = InvItem.Quantity,
                                             OrginalPrice = InvItem.OrginalPrice,
                                             PercentageDiscount = InvItem.PercentageDiscount,
                                             AmountDiscount = InvItem.AmountDiscount,
                                             TotalPrice = InvItem.TotalPrice,
                                             UnitPrice = InvItem.UnitPrice,
                                             GrandTotal = InvItem.TotalPrice,
                                             MinPrice = InvItem.MinPrice,
                                             Tax = InvItem.Tax,
                                             BarCode = InvItem.BarCode,
                                             IsRefunded = InvItem.IsRefunded,
                                             ParentItemId = InvItem.ParentItemId,
                                             RefundedQuantity = InvItem.RefundedQuantity,
                                             UnitId = InvItem.UnitId,
                                             TaxAmount = InvItem.TaxAmount,
                                             ItemNumber = InvItem.ItemNo,
                                             TaxBeforeChange=InvItem.TaxBeforeChange,
                                             IsSerial = InvItem.IsSerial,
                                             SerialNumber = InvItem.SerialNumber,
                                             InvoiceDiscountPerItem = InvItem.InvoiceDiscountPerItem
                                         };

                    var InvoiceObj = new
                    {

                        InvoiceId = myInvoice.InvoiceId,
                        PersonId = myInvoice.PersonId,

                        PersonName = myInvoice.PersonName,
                        CompanyId = myInvoice.CompanyId,
                        CompanyName = myInvoice.CompanyName,
                        TotalPrice = myInvoice.TotalPrice,
                        GrandTotal = myInvoice.GrandTotal,
                        Tax = myInvoice.Tax,
                        Note = myInvoice.Note,
                        PercentageDiscount = myInvoice.PercentageDiscount,
                        AmountDiscount = myInvoice.AmountDiscount,
                        Status = myInvoice.InvoiceStatus,
                        PaymentType = myInvoice.PaymentType,
                        PaymentMethod = myInvoice.PaymentMethod,
                        ChangeAmount = myInvoice.ChangeAmount,
                        InvoiceNumber = myInvoice.InvoiceNumber,
                        SalesManId = myInvoice.SaleManId,
                        SalesManName = myInvoice.SalesManName,
                        DownPayment = myInvoice.DownPayment,
                        IsLayBy = myInvoice.IsLayby,
                        LaybyNumber = myInvoice.LaybyNumber,
                        ParentLayById = myInvoice.ParentLaybyId,
                        IsCopied = myInvoice.IsCopied,
                        ItemsDiscount = myInvoice.ItemsDiscount,
                        InvoiceDate = myInvoice.InvoiceDate,
                        InvoiceItems = InvoiceItemObj,
                        MobileNumber = mobile,
                        IsTaxableInvoice = myInvoice.IsTaxableInvoice,
                        OriginalInvoiceNumber = ParentInvoiceNumber
                    };

                    return this.javaScriptSerializer.Serialize(InvoiceObj);
                }
                else
                {
                    var InvoiceObj = new
                    {
                        InvoiceId = myInvoice.InvoiceId,
                        PersonId = myInvoice.PersonId,
                        PersonName = myInvoice.PersonName,
                        CompanyId = myInvoice.CompanyId,
                        CompanyName = myInvoice.CompanyName,
                        TotalPrice = myInvoice.TotalPrice,
                        GrandTotal = myInvoice.GrandTotal,
                        Tax = myInvoice.Tax,
                        Note = myInvoice.Note,
                        PercentageDiscount = myInvoice.PercentageDiscount,
                        AmountDiscount = myInvoice.AmountDiscount,
                        Status = myInvoice.InvoiceStatus,
                        PaymentType = myInvoice.PaymentType,
                        PaymentMethod = myInvoice.PaymentMethod,
                        ChangeAmount = myInvoice.ChangeAmount,
                        InvoiceNumber = myInvoice.InvoiceNumber,
                        SalesManId = myInvoice.SaleManId,
                        SalesManName = myInvoice.SalesManName,
                        DownPayment = myInvoice.DownPayment,
                        IsLayBy = myInvoice.IsLayby,
                        LaybyNumber = myInvoice.LaybyNumber,
                        ParentLayById = myInvoice.ParentLaybyId,
                        IsCopied = myInvoice.IsCopied,
                        ItemsDiscount = myInvoice.ItemsDiscount,
                        InvoiceDate = myInvoice.InvoiceDate,
                        InvoiceItems = "",
                        MobileNumber = mobile,
                        IsTaxableInvoice = myInvoice.IsTaxableInvoice,
                        OriginalInvoiceNumber = ParentInvoiceNumber

                    };
                    return this.javaScriptSerializer.Serialize(InvoiceObj);


                }

            }
            else
            {
                return null;
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string GetLastInvoice(int RegisterId)
        {
            IInvoiceManager InvoiceManager = (IInvoiceManager)IoC.Instance.Resolve(typeof(IInvoiceManager));
            IInvoiceItemManager invoiceItemManager = (IInvoiceItemManager)IoC.Instance.Resolve(typeof(IInvoiceItemManager));
            
            E.InvoiceCriteria invCriteria = new E.InvoiceCriteria();
            invCriteria.RegisterId = RegisterId;
            E.Invoice myInvoice = InvoiceManager.FindLastInvoice(invCriteria);
            E.InvoiceLite ParentInvoice = InvoiceManager.FindByIdLite(myInvoice.ParentInvoiceId, null);
            string ParentInvoiceNumber = "";
            if (ParentInvoice != null)
                ParentInvoiceNumber = ParentInvoice.InvoiceNumber;
            if (myInvoice != null)
            {
                E.InvoiceItemCriteria itemCriteria = new E.InvoiceItemCriteria();
                itemCriteria.InvoiceId = myInvoice.InvoiceId;

                IInvoicePaymentsManager invoicePaymentManager = (IInvoicePaymentsManager)IoC.Instance.Resolve(typeof(IInvoicePaymentsManager));
                E.InvoicePaymentsCriteria paymentCriteria = new E.InvoicePaymentsCriteria();
                paymentCriteria.InvoiceId = myInvoice.InvoiceId;
                List<E.InvoicePaymentsLite> myPaymentList = invoicePaymentManager.FindAllLite(paymentCriteria);

                List<E.InvoiceItemLite> myInvoiceItemList = invoiceItemManager.FindAllLite(itemCriteria);



                E.InvoiceCriteria myCriteria = new E.InvoiceCriteria();

                IPersonManager person = (IPersonManager)IoC.Instance.Resolve(typeof(IPersonManager));
                ICompanyManager company = (ICompanyManager)IoC.Instance.Resolve(typeof(ICompanyManager));

                Person personInfo = null;
                Company companyInfo =null;

                PersonCriteria personCriteria = new PersonCriteria();
                //personCriteria.PersonId = myInvoice.PersonId;

                //CompanyCriteria companyCriteria = new CompanyCriteria();
                //companyCriteria.CompanyId = myInvoice.CompanyId;
                if (myInvoice.PersonId != -1)
                {
              personInfo = new Person();
                
                    personInfo = person.FindById(myInvoice.PersonId, null);
                }
                if (myInvoice.CompanyId != -1)
                {
                     companyInfo = new Company();
                    companyInfo = company.FindById(myInvoice.CompanyId,null);
                }

                string mobile = string.Empty;
                if (personInfo != null)
                {
                    if (!string.IsNullOrEmpty(personInfo.MobileNumber))
                        mobile = personInfo.MobileNumber;
                }
                else if (companyInfo != null)
                {
                    if (!string.IsNullOrEmpty(companyInfo.MobileNumber))
                        mobile = companyInfo.MobileNumber;
                }
                if (myInvoiceItemList != null)
                {
                    var InvoiceItemObj = from InvItem in myInvoiceItemList
                                         select new
                                         {
                                             InvoiceItemId = InvItem.InvoiceItemId,
                                             InvoiceId = InvItem.InvoiceId,
                                             ProductName = InvItem.ProductName,
                                             Quantity = InvItem.Quantity,
                                             OrginalPrice = InvItem.OrginalPrice,
                                             PercentageDiscount = InvItem.PercentageDiscount,
                                             AmountDiscount = InvItem.AmountDiscount,
                                             TotalPrice = InvItem.TotalPrice,
                                             UnitPrice = InvItem.UnitPrice,
                                             GrandTotal = InvItem.TotalPrice,
                                             MinPrice = InvItem.MinPrice,
                                             Tax = InvItem.Tax,
                                             BarCode = InvItem.BarCode,
                                             IsRefunded = InvItem.IsRefunded,
                                             ParentItemId = InvItem.ParentItemId,
                                             RefundedQuantity = InvItem.RefundedQuantity,
                                             UnitId = InvItem.UnitId,
                                             TaxAmount = InvItem.TaxAmount,
                                             ItemNumber = InvItem.ItemNo,
                                             IsSerial = InvItem.IsSerial,
                                             SerialNumber = InvItem.SerialNumber,
                                             InvoiceDiscountPerItem = InvItem.InvoiceDiscountPerItem,

                                         };

                    var invoicePaymentObject = from payItem in myPaymentList
                                               select new
                                               {
                                                   PaymentTypeName = payItem.PaymenttypeName,
                                                   AmountDefaultCurrency = payItem.Amount
                                               };

                    var InvoiceObj = new
                    {
                        InvoiceId = myInvoice.InvoiceId,
                        PersonId = myInvoice.PersonId,
                        PersonName = myInvoice.PersonName,
                        CompanyId = myInvoice.CompanyId,
                        CompanyName = myInvoice.CompanyName,
                        TotalPrice = myInvoice.TotalPrice,
                        GrandTotal = myInvoice.GrandTotal,
                        Tax = myInvoice.Tax,
                        Note = myInvoice.Note,
                        PercentageDiscount = myInvoice.PercentageDiscount,
                        AmountDiscount = myInvoice.AmountDiscount,
                        Status = myInvoice.Status,
                        PaymentType = myInvoice.PaymentType,
                        PaymentMethod = myInvoice.PaymentMethod,
                        ChangeAmount = myInvoice.ChangeAmount,
                        InvoiceNumber = myInvoice.InvoiceNumber,
                        SalesManId = myInvoice.SalesManId,
                        SalesManName = myInvoice.SalesManName,
                        DownPayment = myInvoice.DownPayment,
                        IsLayBy = myInvoice.IsLayby,
                        LaybyNumber = myInvoice.LaybyNumber,
                        ParentLayById = myInvoice.ParentLaybyId,
                        IsCopied = myInvoice.IsCopied,
                        ItemsDiscount = myInvoice.ItemsDiscount,
                        InvoiceDate = myInvoice.InvoiceDate.Value.ToString("dd/MM/yyyy hh:mm"),
                        InvoiceItems = InvoiceItemObj,
                        InvoicePayment = invoicePaymentObject,
                        MobileNumber = mobile,
                        OriginalInvoiceNumber = ParentInvoiceNumber
                    };

                    return this.javaScriptSerializer.Serialize(InvoiceObj);
                }
                else
                {
                    var InvoiceObj = new
                    {
                        InvoiceId = myInvoice.InvoiceId,
                        PersonId = myInvoice.PersonId,
                        PersonName = myInvoice.PersonName,
                        CompanyId = myInvoice.CompanyId,
                        CompanyName = myInvoice.CompanyName,
                        TotalPrice = myInvoice.TotalPrice,
                        GrandTotal = myInvoice.GrandTotal,
                        Tax = myInvoice.Tax,
                        Note = myInvoice.Note,
                        PercentageDiscount = myInvoice.PercentageDiscount,
                        AmountDiscount = myInvoice.AmountDiscount,
                        Status = myInvoice.Status,
                        PaymentType = myInvoice.PaymentType,
                        PaymentMethod = myInvoice.PaymentMethod,
                        ChangeAmount = myInvoice.ChangeAmount,
                        InvoiceNumber = myInvoice.InvoiceNumber,
                        SalesManId = myInvoice.SalesManId,
                        SalesManName = myInvoice.SalesManName,
                        DownPayment = myInvoice.DownPayment,
                        IsLayBy = myInvoice.IsLayby,
                        LaybyNumber = myInvoice.LaybyNumber,
                        ParentLayById = myInvoice.ParentLaybyId,
                        IsCopied = myInvoice.IsCopied,
                        InvoiceItems = "",
                        InvoiceDate = myInvoice.InvoiceDate.Value.ToString("dd/MM/yyyy hh:mm"),
                        ItemsDiscount = myInvoice.ItemsDiscount,
                        MobileNumber = mobile,
                        OriginalInvoiceNumber = ParentInvoiceNumber

                    };
                    return this.javaScriptSerializer.Serialize(InvoiceObj);


                }

            }
            else
            {
                return null;
            }
        }


        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindInvoiceByNumber(string invoiceNumber, int StoreId)
        {

            IInvoiceManager InvoiceManager = (IInvoiceManager)IoC.Instance.Resolve(typeof(IInvoiceManager));
            IInvoiceItemManager invoiceItemManager = (IInvoiceItemManager)IoC.Instance.Resolve(typeof(IInvoiceItemManager));
            E.InvoiceCriteria invCriteria = new E.InvoiceCriteria();
            invCriteria.StoreId = StoreId;
            invCriteria.InvoiceNumber = invoiceNumber;
            E.Invoice myInvoice = InvoiceManager.FindInvoiceByNumber(invCriteria);
            E.InvoiceLite ParentInvoice = InvoiceManager.FindByIdLite(myInvoice.ParentInvoiceId, null);
            string ParentInvoiceNumber = "";
            if (ParentInvoice != null)
                ParentInvoiceNumber = ParentInvoice.InvoiceNumber;
            if (myInvoice != null)
            {
                myInvoice = InvoiceManager.FindById(myInvoice.InvoiceId, null);
                E.InvoiceItemCriteria itemCriteria = new E.InvoiceItemCriteria();
                itemCriteria.InvoiceId = myInvoice.InvoiceId;
                List<E.InvoiceItemLite> myInvoiceItemList = invoiceItemManager.FindAllLite(itemCriteria);
                E.InvoiceCriteria myCriteria = new E.InvoiceCriteria();

                IInvoicePaymentsManager invoicePaymentManager = (IInvoicePaymentsManager)IoC.Instance.Resolve(typeof(IInvoicePaymentsManager));
                E.InvoicePaymentsCriteria paymentCriteria = new E.InvoicePaymentsCriteria();
                paymentCriteria.InvoiceId = myInvoice.InvoiceId;
                List<E.InvoicePaymentsLite> myPaymentList = invoicePaymentManager.FindAllLite(paymentCriteria);

                IPersonManager person = (IPersonManager)IoC.Instance.Resolve(typeof(IPersonManager));
                ICompanyManager company = (ICompanyManager)IoC.Instance.Resolve(typeof(ICompanyManager));

                Person personInfo = null;
                Company companyInfo = null;

                if (myInvoice.PersonId != -1 && myInvoice.PersonId != 0)
                {
                personInfo = new Person();
                    personInfo = person.FindById(myInvoice.PersonId, null);
                }
                if (myInvoice.CompanyId != -1 && myInvoice.CompanyId != 0)
                {
                    companyInfo = new Company();
                    companyInfo = company.FindById(myInvoice.CompanyId, null);
                }

                string mobile = string.Empty;
                if (personInfo != null)
                {
                    if (!string.IsNullOrEmpty(personInfo.MobileNumber))
                        mobile = personInfo.MobileNumber;
                }
                else  if (companyInfo != null){
                    if (!string.IsNullOrEmpty(companyInfo.MobileNumber))
                    mobile = companyInfo.MobileNumber;
                }

                if (myInvoiceItemList != null)
                {
                    var invoicePaymentObject = from payItem in myPaymentList
                                               select new
                                               {
                                                   PaymentTypeName = payItem.PaymenttypeName,
                                                   AmountDefaultCurrency = payItem.Amount
                                               };

                    var InvoiceItemObj = from InvItem in myInvoiceItemList
                                         select new
                                         {
                                             InvoiceItemId = InvItem.InvoiceItemId,
                                             InvoiceId = InvItem.InvoiceId,
                                             ProductName = InvItem.ProductName,
                                             Quantity = InvItem.Quantity,
                                             OrginalPrice = InvItem.OrginalPrice,
                                             PercentageDiscount = InvItem.PercentageDiscount,
                                             AmountDiscount = InvItem.AmountDiscount,
                                             TotalPrice = InvItem.TotalPrice,
                                             UnitPrice = InvItem.UnitPrice,
                                             GrandTotal = InvItem.TotalPrice,
                                             MinPrice = InvItem.MinPrice,
                                             Tax = InvItem.Tax,
                                             BarCode = InvItem.BarCode,
                                             IsRefunded = InvItem.IsRefunded,
                                             ParentItemId = InvItem.ParentItemId,
                                             RefundedQuantity = InvItem.RefundedQuantity,
                                             UnitId = InvItem.UnitId,
                                             TaxAmount = InvItem.TaxAmount,
                                             ItemNumber = InvItem.ItemNo,
                                             IsSerial = InvItem.IsSerial,
                                             SerialNumber = InvItem.SerialNumber,
                                             IsExchanged = InvItem.IsExchanged,
                                             InvoiceDiscountPerItem = InvItem.InvoiceDiscountPerItem,


                                         };

                    var InvoiceObj = new
                    {

                        InvoiceId = myInvoice.InvoiceId,
                        PersonId = myInvoice.PersonId,
                        CompanyId = myInvoice.CompanyId,
                        CompanyName = myInvoice.CompanyName,
                        PersonName = myInvoice.PersonName,
                        TotalPrice = myInvoice.TotalPrice,
                        GrandTotal = myInvoice.GrandTotal,
                        Tax = myInvoice.Tax,
                        Note = myInvoice.Note,
                        PercentageDiscount = myInvoice.PercentageDiscount,
                        AmountDiscount = myInvoice.AmountDiscount,
                        Status = myInvoice.Status,
                        PaymentType = myInvoice.PaymentType,
                        PaymentMethod = myInvoice.PaymentMethod,
                        ChangeAmount = myInvoice.ChangeAmount,
                        InvoiceNumber = myInvoice.InvoiceNumber,
                        SalesManId = myInvoice.SalesManId,
                        SalesManName = myInvoice.SalesManName,
                        DownPayment = myInvoice.DownPayment,
                        IsLayBy = myInvoice.IsLayby,
                        LaybyNumber = myInvoice.LaybyNumber,
                        ParentLayById = myInvoice.ParentLaybyId,
                        IsCopied = myInvoice.IsCopied,
                        InvoiceItems = InvoiceItemObj,
                        InvoiceCreatedDate = (myInvoice.InvoiceDate.HasValue) ? myInvoice.InvoiceDate.Value.ToString("dd-MM-yyyy") : "",
                        CustomerId = myInvoice.CustomerId,
                        ERPCustomerID = myInvoice.CustomerId,
                        ItemsDiscount = myInvoice.ItemsDiscount,
                        InvoiceDate = myInvoice.InvoiceDate.Value.ToString("dd/MM/yyyy hh:mm"),
                        InvoicePayment = invoicePaymentObject,
                        MobileNumber = mobile,
                        OriginalInvoiceNumber = ParentInvoiceNumber,

                        IsTaxableInvoice=myInvoice.IsTaxableInvoice


                    };

                    return this.javaScriptSerializer.Serialize(InvoiceObj);
                }
                else
                {
                    var InvoiceObj = new
                    {
                        InvoiceId = myInvoice.InvoiceId,
                        PersonId = myInvoice.PersonId,
                        PersonName = myInvoice.PersonName,
                        CompanyId = myInvoice.CompanyId,
                        CompanyName = myInvoice.CompanyName,
                        TotalPrice = myInvoice.TotalPrice,
                        GrandTotal = myInvoice.GrandTotal,
                        Tax = myInvoice.Tax,
                        Note = myInvoice.Note,
                        PercentageDiscount = myInvoice.PercentageDiscount,
                        AmountDiscount = myInvoice.AmountDiscount,
                        Status = myInvoice.Status,
                        PaymentType = myInvoice.PaymentType,
                        PaymentMethod = myInvoice.PaymentMethod,
                        ChangeAmount = myInvoice.ChangeAmount,
                        InvoiceNumber = myInvoice.InvoiceNumber,
                        SalesManId = myInvoice.SalesManId,
                        SalesManName = myInvoice.SalesManName,
                        DownPayment = myInvoice.DownPayment,
                        IsLayBy = myInvoice.IsLayby,
                        LaybyNumber = myInvoice.LaybyNumber,
                        ParentLayById = myInvoice.ParentLaybyId,
                        IsCopied = myInvoice.IsCopied,
                        InvoiceItems = "",
                        CustomerId = myInvoice.CustomerId,

                        ERPCustomerID = myInvoice.CustomerId,
                        ItemsDiscount = myInvoice.ItemsDiscount,
                        InvoiceDate = myInvoice.InvoiceDate.Value.ToString("dd/MM/yyyy hh:mm"),
                        MobileNumber = mobile,
                        OriginalInvoiceNumber = ParentInvoiceNumber,
                        IsTaxableInvoice = myInvoice.IsTaxableInvoice

                    };
                    return this.javaScriptSerializer.Serialize(InvoiceObj);


                }

            }
            else
            {
                return null;
            }
        }


        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string InvoiceSearchCriteria(string InvoiceCriteria, int RegisterId,string phoneNumber)
        {

            IInvoiceManager myManager = (IInvoiceManager)IoC.Instance.Resolve(typeof(IInvoiceManager));
            E.InvoiceCriteria myCriteria = new E.InvoiceCriteria();
            if (!SF.Framework.String.IsEmpty(InvoiceCriteria))
            {
                myCriteria = this.javaScriptSerializer.Deserialize<E.InvoiceCriteria>(this.unEscape(InvoiceCriteria));
               
            }
            else
                myCriteria = new E.InvoiceCriteria();
            myCriteria.RegisterId = RegisterId;

            List<E.Invoice> myInvoiceList = myManager.FindAllQl(myCriteria,phoneNumber);


            IInvoicePaymentsManager invoicePaymentManager = (IInvoicePaymentsManager)IoC.Instance.Resolve(typeof(IInvoicePaymentsManager));
            E.InvoicePaymentsCriteria paymentCriteria = new E.InvoicePaymentsCriteria();
            if (myCriteria.InvoiceId !=null)
                paymentCriteria.InvoiceId = (int)myCriteria.InvoiceId;

            List<E.InvoicePaymentsLite> myPaymentList = invoicePaymentManager.FindAllLite(paymentCriteria);

            if (myInvoiceList != null)
            {
               
                if (myPaymentList != null)
                {
                    var invoicePaymentObject = from payItem in myPaymentList
                                               select new
                                               {
                                                   PaymentTypeName = payItem.PaymenttypeName,
                                                   AmountDefaultCurrency = payItem.Amount
                                               };
                }

                var InvoiceObj = from E.Invoice inv in myInvoiceList
                                 select new
                                 {
                                     InvoiceId = inv.InvoiceId,
                                     PersonId = inv.PersonId,
                                     PersonName = inv.PersonName,
                                     TotalPrice = inv.TotalPrice,
                                     GrandTotal = inv.GrandTotal,
                                     Tax = inv.Tax,
                                     Note = inv.Note,
                                     PercentageDiscount = inv.PercentageDiscount,
                                     AmountDiscount = inv.AmountDiscount,
                                     Status = inv.Status,
                                     PaymentType = inv.PaymentType,
                                     PaymentMethod = inv.PaymentMethod,
                                     ChangeAmount = inv.ChangeAmount,
                                     DownPayment = inv.DownPayment,
                                     IsLayBy = inv.IsLayby,
                                     LaybyNumber = inv.LaybyNumber,
                                     ParentLayById = inv.ParentLaybyId,
                                     IsCopied = inv.IsCopied,
                                     InvoiceCreatedDate = (inv.InvoiceDate.HasValue) ? inv.InvoiceDate.Value.ToString("dd-MM-yyyy hh:mm:ss tt") : "",
                                     ItemsDiscount = inv.ItemsDiscount,
                                     InvoiceTotalDiscount = inv.ItemsDiscount + inv.AmountDiscount,
                                     //TransactionTypeName = inv.TransactionTypeName,
                                     InvoiceNumber = inv.InvoiceNumber,
                                     CustomerName = (!string.IsNullOrEmpty(inv.PersonName)) ? inv.PersonName : inv.CompanyName,
                                     TransactionTypeName = inv.TransactionTypeName,
                                     CustomerId = inv.CustomerId,
                                     InvoiceDate = inv.InvoiceDate.Value.ToShortDateString(),
                                     InvoicePayment=""
                                 };
                return this.javaScriptSerializer.Serialize(InvoiceObj);
            }
            else
            {
                return null;
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string GetInvoicesByCustomer(int CustomerId, int Status, int CustomerType)
        {
            IInvoiceManager myManager = (IInvoiceManager)IoC.Instance.Resolve(typeof(IInvoiceManager));
            E.InvoiceCriteria InvoiceCriteria = new E.InvoiceCriteria();
            InvoiceCriteria.CustomerId = CustomerId;
            InvoiceCriteria.Status = Status;
            InvoiceCriteria.CustomerType = CustomerType;
            List<E.Invoice> myInvoiceList = myManager.FindByCustomer(InvoiceCriteria);
            if (myInvoiceList != null)
            {
                var InvoiceObj = from E.Invoice invObj in myInvoiceList
                                 select new
                                 {
                                     GrandTotal = invObj.GrandTotal,
                                     CreatedDate = invObj.CreatedDate.Value.ToString("dd/MM/yyyy"),
                                     downPayment = invObj.DownPayment,
                                     InvoiceId = invObj.InvoiceId,
                                     LayByNumber = invObj.LaybyNumber
                                 };


                return this.javaScriptSerializer.Serialize(InvoiceObj);
            }
            else
            {
                return null;
            }

        }


        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string GetLayByPayment(int invoiceId)
        {
            IInvoicePaymentsManager myManager = (IInvoicePaymentsManager)IoC.Instance.Resolve(typeof(IInvoicePaymentsManager));
            E.InvoicePaymentsCriteria criteria = new E.InvoicePaymentsCriteria();
            criteria.InvoiceId = invoiceId;
            List<E.InvoicePaymentsLite> invoicePaymentsList = myManager.FindAllLite(criteria);
            if (invoicePaymentsList != null)
            {
                var paymentObj = from E.InvoicePaymentsLite payObj in invoicePaymentsList
                                 select new
                                 {
                                     InvoicePaymentsId = payObj.InvoicePaymentsId,
                                     PaymentType = payObj.PaymentType,
                                     Amount = payObj.Amount,
                                     AmountDefaultCurrency = payObj.AmountDefaultCurrency,
                                     Currency = payObj.Currency,
                                     PaymentTypeName = payObj.PaymenttypeName,
                                     GiftNumber = payObj.GiftNumber,
                                     RegisterId = payObj.RegisterId
                                 };


                return this.javaScriptSerializer.Serialize(paymentObj);

            }
            return null;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string SearchRefund(string personName, string invoiceNumber, string dateFrom, string dateTo,bool isRefund)
        {
            E.InvoiceCriteria criteria = null;
            criteria = new E.InvoiceCriteria();
            criteria.CustomerName = personName;
            criteria.InvoiceNumber = invoiceNumber;
            criteria.DateTo = dateTo;
            criteria.DateFrom = dateFrom;
            criteria.isRefund = isRefund;
            List<E.InvoiceLite> invoiceList = iInvoiceManager.searchRefund(criteria);
            if (invoiceList != null)
            {
                var invoice = from E.Invoice invoiceObj in invoiceList
                              select new
                              {
                                  InvoiceNumber = invoiceObj.InvoiceNumber,
                                  PersonName = (invoiceObj.PersonName != null) ? invoiceObj.PersonName : invoiceObj.CompanyName,
                                  InvoiceDate = invoiceObj.InvoiceDate,
                                  GrandTotal = invoiceObj.GrandTotal,
                                  CompanyName = invoiceObj.CompanyName
                              };


                return this.javaScriptSerializer.Serialize(invoiceList);

            }
            return null;
            //return this.AttachStatusCode(invoiceList, 1, null);
        }


   [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public bool CheckIPConnectivity(int StoreId)
        {
            bool registerStatus = true;
           
            System.Net.NetworkInformation.Ping ping = new System.Net.NetworkInformation.Ping();
            try
            {
                if (ping.Send("192.168.2.229").Status != System.Net.NetworkInformation.IPStatus.Success)
                    registerStatus = false;
            }
            catch (Exception ex)
            {
                registerStatus = false;

            }

            return registerStatus;

        }



    }
}

