using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using SF.Framework;
using E = CentrixERP.Business.Entity;

using CentrixERP.Common.API;
using System.Reflection;
using CentrixERP.Configuration;
using SF.FrameworkEntity;
using CentrixERP.API.Common.BaseClasses;
using CentrixERP.Business.IManager;




namespace Centrix.POS.Standalone.Client.API
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    [EntityAttribute(Enums_S3.Entity.CloseRegister)]
    public class CloseRegisterWebService : WebServiceBaseClass
    {
        ICloseRegisterManager iCloseRegisterManager = null;
        public CloseRegisterWebService()
        {
            iCloseRegisterManager = (ICloseRegisterManager)IoC.Instance.Resolve(typeof(ICloseRegisterManager));
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string Delete(int id)
        {
            this.LockEntityForDelete(Enums_S3.Entity.CloseRegister, id);
            E.CloseRegister myCloseRegister = iCloseRegisterManager.FindById(id, null);
            myCloseRegister.CloseRegisterId = id;
            myCloseRegister.MarkDeleted();
            try
            {
                iCloseRegisterManager.Save(myCloseRegister);
                return AttachStatusCode(true, 1, null);
            }
            catch (Exception ex)
            {
                return AttachStatusCode(null, 0, HandleException(ex));
            }

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindAllLite(string keyword, int page, int resultCount, string argsCriteria)
        {
            E.CloseRegisterCriteria criteria = null;
            if (!SF.Framework.String.IsEmpty(argsCriteria))
            {
                criteria = this.javaScriptSerializer.Deserialize<E.CloseRegisterCriteria>(argsCriteria);
            }
            else
                criteria = new E.CloseRegisterCriteria();

            criteria.Keyword = keyword;
            criteria.pageNumber = page;
            criteria.resultCount = resultCount;


            List<E.CloseRegisterLite> CloseRegisterList = iCloseRegisterManager.FindAllLite(criteria);
            return this.AttachStatusCode(CloseRegisterList, 1, null);

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindByIdLite(int id)
        {
            E.CloseRegisterLite myCloseRegister = iCloseRegisterManager.FindByIdLite(id, null);
            return this.AttachStatusCode(myCloseRegister, 1, null);

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string Add(int id, string CloseRegisterInfo)
        {
            return Save(true, id, CloseRegisterInfo);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.Edit)]
        public string Edit(int id, string CloseRegisterInfo)
        {
            if (id <= 0)
            {
                ValidationError error = new ValidationError()
                {
                    ControlName = "",
                    GlobalMessage = true,
                    Message = "Please select CloseRegister"
                };
                List<ValidationError> errors = new List<ValidationError>(){
                error
                };
                return this.AttachStatusCode(errors, 0, "validation");
            }
            else
            {
                return Save(false, id, CloseRegisterInfo);
            }
        }

        private string Save(bool add, int id, string CloseRegisterInfo)
        {
            E.CloseRegister myCloseRegister = this.javaScriptSerializer.Deserialize<E.CloseRegister>(this.unEscape((CloseRegisterInfo)));
            if (!add)
            {
                this.LockEntity(Enums_S3.Entity.CloseRegister, id);
                myCloseRegister.MarkOld();
                myCloseRegister.MarkModified();
                myCloseRegister.CloseRegisterId = id;
                myCloseRegister.UpdatedBy = LoggedInUser.UserId;
            }
            else
            {
                myCloseRegister.CreatedBy = LoggedInUser.UserId;
            }


            List<ValidationError> errors = iCloseRegisterManager.Validate(myCloseRegister);
            if (errors != null && errors.Count > 0)
            {
                return this.AttachStatusCode(errors, 0, "validation");
            }


            try
            {
                iCloseRegisterManager.Save(myCloseRegister);
                if (!add)
                    this.UnLockEntity(Enums_S3.Entity.CloseRegister, id);
                return this.AttachStatusCode(myCloseRegister.CloseRegisterId, 1, null);

            }
            catch (SF.Framework.Exceptions.RecordExsitsException ex)
            {
                return this.AttachStatusCode(null, 501, null);
            }
            catch (Exception ex)
            {

                return this.AttachStatusCode(null, 0, HandleException(ex));
            }
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public int OpenRegister(int storeId, int registerId, int userId, string registerName, string storeName, double openCashLoan, string remarks)
        {
            E.CloseRegister myCloseRegister = new E.CloseRegister();
            myCloseRegister.RegisterId = registerId;
            myCloseRegister.StoreId = storeId;
            myCloseRegister.RegisterName = registerName;
            myCloseRegister.StoreName = storeName;
            myCloseRegister.OpenedCashLoan = openCashLoan;
            myCloseRegister.OpenedDate = DateTime.Now;
            myCloseRegister.CreatedBy = userId;
            myCloseRegister.Remarks = remarks;
            try
            {
                iCloseRegisterManager.Save(myCloseRegister);

            }
            catch (Exception Ex)
            {

                throw Ex;
            }
            return myCloseRegister.CloseRegisterId;

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string CheckRegisterStatus(int RegisterId, int UserId)
        {
            ICloseRegisterManager myManager = (ICloseRegisterManager)IoC.Instance.Resolve(typeof(ICloseRegisterManager));
            E.CloseRegisterCriteria myCriteria = new E.CloseRegisterCriteria();
            myCriteria.UserId = UserId;
            myCriteria.RegisterId = RegisterId;
            List<E.CloseRegister> myList = myManager.FindAll(myCriteria);


            if (myList != null && myList.Count > 0)
            {
                var closeregisterData = new
                 {
                     closeRegisterId = myList.First().CloseRegisterId,
                     RegisterId = myList.First().RegisterId,
                     OpenedCashLoan = myList.First().OpenedCashLoan,
                     OpenDate = myList.First().OpenedDate.Value.ToString("dd/MM/yyyy hh:mm:ss"),
                     CloseDate = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss"),
                     SessionNumber = myList.First().SessionNumber
                 };

                return this.javaScriptSerializer.Serialize(closeregisterData);
            }
            else
                return null;

        }


        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string SaveCloseRegister(int CloseRegisterId, int UserId, Object[] RegisterAmounts, double TotalAmount)
        {


            ICloseRegisterManager myManager = (ICloseRegisterManager)IoC.Instance.Resolve(typeof(ICloseRegisterManager));
            E.CloseRegister myCloseRegister = myManager.FindById(CloseRegisterId, null);
            E.CloseRegisterCriteria myCriteria = new E.CloseRegisterCriteria();
            if (myCloseRegister == null)
                myCloseRegister = new E.CloseRegister();

            myCloseRegister.ClosedDate = DateTime.Now;

            if (myCloseRegister.RegisterAmountsList == null)
                myCloseRegister.RegisterAmountsList = new List<E.RegisterAmounts>();


            foreach (object obj in RegisterAmounts)
            {
                E.RegisterAmounts registerAmount = getAmounts(obj, 0);
                myCloseRegister.RegisterAmountsList.Add(registerAmount);
            }

            try
            {

                if (myCloseRegister.RegisterAmountsList == null)
                    myCloseRegister.RegisterAmountsList = new List<E.RegisterAmounts>();

                double amounts = 0;
                myCloseRegister.TotalPrice = TotalAmount;
                foreach (E.RegisterAmounts reg in myCloseRegister.RegisterAmountsList)
                {
                    amounts += reg.AmountDefaultCurrency;
                }
                if (myCloseRegister.TotalPrice == (double)amounts)
                {
                    myCloseRegister.IsIdentical = true;
                    myCloseRegister.DiffAmount = 0;
                }
                else
                {
                    myCloseRegister.IsIdentical = false;
                    myCloseRegister.DiffAmount = 0;
                }

             

                myCloseRegister.MarkModified();
                myManager.Save(myCloseRegister);
                return this.javaScriptSerializer.Serialize(myCloseRegister.CloseRegisterId);
            }
            catch (Exception ex)
            {
                return this.javaScriptSerializer.Serialize("ex");
            }
        }

        private E.RegisterAmounts getAmounts(object item, int Id)
        {
            E.RegisterAmounts registerItem = null;
            Dictionary<string, Object> obj = (Dictionary<string, Object>)item;
            registerItem = new E.RegisterAmounts();
            if (obj["RegisterAmountId"].ToString().ToNumber() == Id)
            {
                if (Id == -1)
                    obj["RegisterAmountId"] = "0";

                foreach (KeyValuePair<string, Object> val in obj)
                {
                    PropertyInfo pro = registerItem.GetType().GetProperty(val.Key);
                    if (pro == null)
                        continue;

                    object value = null;
                    if (val.Value != null && !string.IsNullOrEmpty(val.Value.ToString()))
                        value = val.Value;

                    pro.SetValue(registerItem, GetPropertyValue(pro.PropertyType, value), null);

                }
            }

            return registerItem;
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string GetCloseRegisterData(int StoreId,int RegisterId, int UserId, int CloseRegisterId)
        {

            ICloseRegisterManager myManager = (ICloseRegisterManager)IoC.Instance.Resolve(typeof(ICloseRegisterManager));
            List<E.CloseRegiserDetails> myDetailsList = null;
            E.CloseRegister myCloseRegister = null;
            E.CloseRegisterCriteria myCriteria = new E.CloseRegisterCriteria();
            
            myCloseRegister = myManager.FindById(CloseRegisterId, myCriteria);
            myCriteria.RegisterId = RegisterId;
            myCriteria.StoreId = StoreId;
            myCriteria.UserId = UserId;
            myCriteria.CloseRegisterId = CloseRegisterId;
            myDetailsList = myManager.GetCloseRegisterDetailsPaymentType(myCriteria);
            if (myCloseRegister != null)
            {
                var CloseRegisterObj = new
                {
                    openDate = (myCloseRegister.OpenedDate != null) ? myCloseRegister.OpenedDate.Value.ToString() : "",
                    closeDate = (myCloseRegister.ClosedDate != null) ? myCloseRegister.ClosedDate.Value.ToString() : ""
                };

                var DetailsObj = from Details in myDetailsList
                                 select new
                                 {
                                     PaymentType = Details.PaymentType,
                                     TotalPrice = Details.TotalPrice,
                                     Name = Details.PaymentTypeName,
                                     Currency = Details.Currency,
                                     CurrencyDefaultPrice = Details.CurrencyDefaultPrice,
                                     cashLoan = Details.CashLoan,
                                     invoiceStatus = Details.InvoiceStatus,
                                     CreditCardType = Details.CreditCardType,
                                     InvoiceCount = Details.InvoiceCount
                                 };


                var CloseRegisterDetails = new
                {
                    CloseRegisterDetailsObject = DetailsObj,
                    CloseRegisterObject = CloseRegisterObj
                };
                return this.javaScriptSerializer.Serialize(CloseRegisterDetails);
            }
            else
            {
                return null;
            }
        }



        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string GetCloseRegisterTotal(int CloseRegisterId)
        {
            ICloseRegisterManager myManager = (ICloseRegisterManager)IoC.Instance.Resolve(typeof(ICloseRegisterManager));
            E.CloseRegisterTotals Totals = new E.CloseRegisterTotals();
            Totals = myManager.FindCloseRegisterSalesTotal(CloseRegisterId);
            if (Totals != null)
                return this.javaScriptSerializer.Serialize(Totals);
            else
                return null;
        }

    }
}

