using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using SF.Framework;
using E = Centrix.UM.Business.Entity;
using Centrix.UM.Business.IManager;
using System.Reflection;
using SF.FrameworkEntity;
using CentrixERP.API.Common.BaseClasses;
using CentrixERP.Common.API;
using CentrixERP.Configuration;



namespace Centrix.UM.API
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    [EntityAttribute(Enums_S3.Entity.UserStoreShifs)]
    public class UserStoreShifsWebService : WebServiceBaseClass
    {
        IUserStoreShifsManager iUserStoreShifsManager = null;
        public UserStoreShifsWebService()
        {
            iUserStoreShifsManager = (IUserStoreShifsManager)IoC.Instance.Resolve(typeof(IUserStoreShifsManager));
        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.Delete)]
        public string Delete(int id)
        {
            this.LockEntityForDelete(Enums_S3.Entity.UserStoreShifs, id);
            E.UserStoreShifs myUserStoreShifs = iUserStoreShifsManager.FindById(id, null);
            myUserStoreShifs.UserStoreShiftId = id;
            myUserStoreShifs.MarkDeleted();
            try
            {
                iUserStoreShifsManager.Save(myUserStoreShifs);
                return AttachStatusCode(true, 1, null);
            }
            catch (Exception ex)
            {
                return AttachStatusCode(null, 0, HandleException(ex));
            }

        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.List)]
        public string FindAllLite(string keyword, int page, int resultCount, string argsCriteria)
        {
            E.UserStoreShifsCriteria criteria = null;
            if (!SF.Framework.String.IsEmpty(argsCriteria))
            {
                criteria = this.javaScriptSerializer.Deserialize<E.UserStoreShifsCriteria>(argsCriteria);
            }
            else
                criteria = new E.UserStoreShifsCriteria();

            criteria.Keyword = keyword;
            criteria.pageNumber = page;
            criteria.resultCount = resultCount;


            List<E.UserStoreShifsLite> UserStoreShifsList = iUserStoreShifsManager.FindAllLite(criteria);
            return this.AttachStatusCode(UserStoreShifsList, 1, null);

        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.View)]
        public string FindByIdLite(int id)
        {
            E.UserStoreShifsLite myUserStoreShifs = iUserStoreShifsManager.FindByIdLite(id, null);
            return this.AttachStatusCode(myUserStoreShifs, 1, null);

        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.Add)]
        public string Add(int id, string UserStoreShifsInfo)
        {
            return Save(true, id, UserStoreShifsInfo);
        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.Edit)]
        public string Edit(int id, string UserStoreShifsInfo)
        {
            if (id <= 0)
            {
                ValidationError error = new ValidationError()
                {
                    ControlName = "",
                    GlobalMessage = true,
                    Message = "Please select UserStoreShifs"
                };
                List<ValidationError> errors = new List<ValidationError>(){
                error
                };
                return this.AttachStatusCode(errors, 0, "validation");
            }
            else
            {
                return Save(false, id, UserStoreShifsInfo);
            }
        }

        private string Save(bool add, int id, string UserStoreShifsInfo)
        {
            E.UserStoreShifs myUserStoreShifs = this.javaScriptSerializer.Deserialize<E.UserStoreShifs>(this.unEscape((UserStoreShifsInfo)));
            if (!add)
            {
                this.LockEntity(Enums_S3.Entity.UserStoreShifs, id);
                myUserStoreShifs.MarkOld();
                myUserStoreShifs.MarkModified();
                myUserStoreShifs.UserStoreShiftId = id;
                myUserStoreShifs.UpdatedBy = LoggedInUser.UserId;
            }
            else
            {
                myUserStoreShifs.CreatedBy = LoggedInUser.UserId;
            }


            List<ValidationError> errors = iUserStoreShifsManager.Validate(myUserStoreShifs);
            if (errors != null && errors.Count > 0)
            {
                return this.AttachStatusCode(errors, 0, "validation");
            }


            try
            {
                iUserStoreShifsManager.Save(myUserStoreShifs);
                if (!add)
                    this.UnLockEntity(Enums_S3.Entity.UserStoreShifs, id);
                return this.FindByIdLite(myUserStoreShifs.UserStoreShiftId);

            }
            catch (SF.Framework.Exceptions.RecordExsitsException ex)
            {
                return this.AttachStatusCode(null, 501, null);
            }
            catch (Exception ex)
            {

                return this.AttachStatusCode(null, 0, HandleException(ex));
            }
        }
    }
}

