using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using SF.Framework;
using E = Centrix.UM.Business.Entity;
using Centrix.UM.Business.IManager;
using CentrixERP.Common.API;
using CentrixERP.Configuration;
using System.Reflection;
using SF.FrameworkEntity;
using CentrixERP.API.Common.BaseClasses;



namespace Centrix.UM.API
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    [EntityAttribute(Enums_S3.Entity.Shifts)]
    public class ShiftsWebService : WebServiceBaseClass
    {
        IShiftsManager iShiftsManager = null;
        public ShiftsWebService()
        {
            iShiftsManager = (IShiftsManager)IoC.Instance.Resolve(typeof(IShiftsManager));
        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.Delete)]
        public string Delete(int id)
        {
            this.LockEntityForDelete(Enums_S3.Entity.Shifts, id);
            E.Shifts myShifts = iShiftsManager.FindById(id, null);
            myShifts.ShiftId = id;
            myShifts.MarkDeleted();
            try
            {
                iShiftsManager.Save(myShifts);
                return AttachStatusCode(true, 1, null);
            }
            catch (Exception ex)
            {
                return AttachStatusCode(null, 0, HandleException(ex));
            }

        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.List)]
        public string FindAllLite(string keyword, int page, int resultCount, string argsCriteria)
        {
            E.ShiftsCriteria criteria = null;
            if (!SF.Framework.String.IsEmpty(argsCriteria))
            {
                criteria = this.javaScriptSerializer.Deserialize<E.ShiftsCriteria>(argsCriteria);
            }
            else
                criteria = new E.ShiftsCriteria();

            criteria.Keyword = keyword;
            criteria.pageNumber = page;
            criteria.resultCount = resultCount;


            List<E.ShiftsLite> ShiftsList = iShiftsManager.FindAllLite(criteria);
            return this.AttachStatusCode(ShiftsList, 1, null);

        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.View)]
        public string FindByIdLite(int id)
        {
            E.ShiftsLite myShifts = iShiftsManager.FindByIdLite(id, null);
            return this.AttachStatusCode(myShifts, 1, null);

        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.Add)]
        public string Add(int id, string ShiftsInfo)
        {
            return Save(true, id, ShiftsInfo);
        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.Edit)]
        public string Edit(int id, string ShiftsInfo)
        {
            if (id <= 0)
            {
                ValidationError error = new ValidationError()
                {
                    ControlName = "",
                    GlobalMessage = true,
                    Message = "Please select Shifts"
                };
                List<ValidationError> errors = new List<ValidationError>(){
                error
                };
                return this.AttachStatusCode(errors, 0, "validation");
            }
            else
            {
                return Save(false, id, ShiftsInfo);
            }
        }

        private string Save(bool add, int id, string ShiftsInfo)
        {
            E.Shifts myShifts = this.javaScriptSerializer.Deserialize<E.Shifts>(this.unEscape((ShiftsInfo)));
            if (!add)
            {
                this.LockEntity(Enums_S3.Entity.Shifts, id);
                myShifts.MarkOld();
                myShifts.MarkModified();
                myShifts.ShiftId = id;
                myShifts.UpdatedBy = LoggedInUser.UserId;
            }
            else
            {
                myShifts.CreatedBy = LoggedInUser.UserId;
            }


            List<ValidationError> errors = iShiftsManager.Validate(myShifts);
            if (errors != null && errors.Count > 0)
            {
                return this.AttachStatusCode(errors, 0, "validation");
            }


            try
            {
                iShiftsManager.Save(myShifts);
                if (!add)
                    this.UnLockEntity(Enums_S3.Entity.Shifts, id);
                return this.FindByIdLite(myShifts.ShiftId);

            }
            catch (SF.Framework.Exceptions.RecordExsitsException ex)
            {
                return this.AttachStatusCode(null, 501, null);
            }
            catch (Exception ex)
            {

                return this.AttachStatusCode(null, 0, HandleException(ex));
            }
        }
    }
}

