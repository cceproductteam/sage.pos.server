using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using CentrixERP.Common.API;
using Centrix.UM.Business.IManager;
using Centrix.UM.Business.Entity;
using SF.Framework;
using CentrixERP.Common.Business.IManager;
using CentrixERP.Common.Business.Entity;
using System.Web.Script.Services;
using CentrixERP.API.Common.BaseClasses;

namespace Centrix.UM.API
{
    /// <summary>
    /// Summary description for EmailTabWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class EmailTabWebService : WebServiceBaseClass
    {

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string DeleteEmailUser(int id)
        {
            IEmailUserManager myEmailManager = (IEmailUserManager)IoC.Instance.Resolve(typeof(IEmailUserManager));
            EmailUser myEmail = new EmailUser();
            myEmail.EmailUserId = id;
            myEmail.MarkDeleted();
            myEmailManager.Save(myEmail);
            return "true";
        }


        //[WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        //public void AddEditEmail(int? EUserId, int? UserId, string EmailbusinessAddress, string EmailPersonalAddress, bool IsDefault)
        //{
        //    IEmailManager myEmailManager = (IEmailManager)IoC.Instance.Resolve(typeof(IEmailManager));
        //    IEmailUserManager myEmailUserManager = (IEmailUserManager)IoC.Instance.Resolve(typeof(IEmailUserManager));
        //    Email myEmail;


        //    if (EUserId != 0 && EUserId != -1)
        //    {
        //        if (UserId != 0)
        //        {
        //            int EmailUserId = EUserId.Value;
        //            EmailUser emailuser = myEmailUserManager.FindById(EmailUserId, null);
        //            emailuser.Isdefault = IsDefault;
        //            emailuser.EmailObj.EmailbusinessAddress = EmailbusinessAddress;
        //            emailuser.EmailObj.EmailPersonalAddress = EmailPersonalAddress;

        //            if (emailuser.EmailObj == null)
        //                emailuser.EmailObj = new CentrixERP.Common.Business.Entity.Email();

        //            try
        //            {
        //                emailuser.MarkModified();
        //                emailuser.EmailObj.MarkModified();
        //                myEmailUserManager.Save(emailuser);

        //            }
        //            catch (Exception e)
        //            { }
        //        }
        //    }

        //    else
        //    {
        //        myEmail = new Email();
        //        myEmail.EmailbusinessAddress = EmailbusinessAddress;
        //        myEmail.EmailPersonalAddress = EmailPersonalAddress;

        //        try
        //        {
        //            if (UserId != 0)
        //            {

        //                EmailUser myEmailUser = new EmailUser(myEmail);
        //                myEmailUser.Isdefault = IsDefault;
        //                myEmailUser.UserId = UserId.Value;
        //                myEmailUserManager.Save(myEmailUser);
        //            }

        //        }
        //        catch (Exception e)
        //        {
        //        }

        //    }







        //    //try
        //    //{
        //    //    if (UserId != 0)
        //    //    {

        //    //        EmailUser myEmailUser = new EmailUser(myEmail);
        //    //        myEmailUser.IsDefault = IsDefault;
        //    //        myEmailUser.UserId = UserId;
        //    //        myEmailUserManager.Save(myEmailUser);
        //    //    }
        //    // }

        //}
    }
}
