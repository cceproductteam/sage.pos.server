using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using CentrixERP.Common.API;
using System.Web.Script.Services;
using SF.CustomScriptControls;
using Centrix.UM.Business.IManager;
using SF.Framework;
using Centrix.UM.Business.Entity;
using System.Web.Script.Serialization;
using CentrixERP.API.Common.BaseClasses;

namespace Centrix.UM.API
{
    /// <summary>
    /// Summary description for SearchAreas
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class SearchAreas : WebServiceBaseClass
    {

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string SearchArea(string keyword, int from, int to, string args)
        {
            List<AutoCompleteItem> returnedList = new List<AutoCompleteItem>();
            IEnumerable<AutoCompleteItem> temp;

            IAreaManager myAreaManager = (IAreaManager)IoC.Instance.Resolve(typeof(IAreaManager));
            AreaCriteria areacriteria = new AreaCriteria();
            areacriteria.Name = keyword;
            List<Area> AreaList = myAreaManager.FindAll(areacriteria);
            //JavaScriptSerializer oSerializer = new JavaScriptSerializer();

            if (AreaList != null)
            {
                if (args == "en")
                    temp = from Area U in AreaList orderby U.NameEnglish select new AutoCompleteItem() { label = U.NameEnglish, value = U.AreaId, categoryId = 2, category = "" };
                else
                    temp = from Area U in AreaList orderby U.NameArabic select new AutoCompleteItem() { label = U.NameArabic, value = U.AreaId, categoryId = 2, category = "" };

                returnedList.AddRange(temp.ToList());
            }

            if (returnedList != null && returnedList.Count != 0)
            {
                //string sJSON = oSerializer.Serialize(returnedList);
                //return sJSON;
                return this.AttachStatusCode(returnedList, 0, null);
            }
            else
            {
                return this.AttachStatusCode(null, 0, null);
            }

        }
    }
}
