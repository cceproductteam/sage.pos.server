using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using CentrixERP.Common.API;
using Centrix.UM.Business.IManager;
using SF.Framework;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using SF.CustomScriptControls;
using Centrix.UM.Business.Entity;
using System.Reflection;
using CentrixERP.Configuration;
using CentrixERP.Common.Business.Entity;
using CentrixERP.API.Common.BaseClasses;

namespace Centrix.UM.API
{

    /// <summary>
    /// Summary description for RoleWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    [EntityAttribute(Enums_S3.Entity.Role)]
    public class RoleWebService : WebServiceBaseClass
    {
        IRoleManager roleManager = null;
        IRolePermissionManager rolePermissionManager = null;

        public RoleWebService()
        {
            roleManager = (IRoleManager)IoC.Instance.Resolve(typeof(IRoleManager));
            rolePermissionManager = (IRolePermissionManager)IoC.Instance.Resolve(typeof(IRolePermissionManager));
        }

        [WebMethod]
        [EntityMethod(Enums_S3.DefaultPermissions.Delete)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public bool Delete(int id)
        {

            Centrix.UM.Business.Entity.Role myRole = roleManager.FindById(id, null); //new Centrix.UM.Business.Entity.Role();
            myRole.RoleId = id;
            myRole.MarkDeleted();
            try
            {
                roleManager.Save(myRole);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethod(Enums_S3.DefaultPermissions.View)]
        public string FindAllLite(string keyword, int page, int resultCount, string argsCriteria)
        {

            RoleCriteria roleCriteria = null;
            if (!SF.Framework.String.IsEmpty(argsCriteria))
            {
                roleCriteria = this.javaScriptSerializer.Deserialize<RoleCriteria>(argsCriteria);
            }
            else
                roleCriteria = new RoleCriteria();


            roleCriteria.keyword = keyword;
            roleCriteria.pageNumber = page;
            roleCriteria.resultCount = resultCount;

            List<Role_Lite> rolesList = roleManager.FindAllLite(roleCriteria);
            if (rolesList != null && rolesList.Count > 0)
                return this.AttachStatusCode(rolesList, 1, null);
            else
                return this.AttachStatusCode(null, 0, null);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethod(Enums_S3.DefaultPermissions.View)]
        public string FindByIdLite(int id)
        {
            Role_Lite role = roleManager.FindByIdLite(id);
            if (role != null)
                return this.AttachStatusCode(role, 1, null);
            else
                return this.AttachStatusCode(null, 0, null);
        }


        [WebMethod]
        [EntityMethod(Enums_S3.ExtendedPermissions.None)]
        public string SaveRole(int UserId, string RolesList)
        {
          
            List<Role> roles = roleManager.FindAll(null);
            if (roles != null)
                this.LockEntity(Enums_S3.Entity.Role, null);
            else
                roles = new List<Role>();


            Object[] NewRolesList = this.javaScriptSerializer.Deserialize<Object[]>(unEscape((RolesList.ToString())));

            List<string> ids = (from Object obj in NewRolesList
                                select ((Dictionary<string, object>)obj)["RoleId"].ToString()).ToList();

            if (roles != null)
            {
                foreach (Role item in roles)
                {
                    var contain = from string Id in ids where item.RoleId == Convert.ToInt32(Id) select Id;
                    if (contain == null || contain.Count() == 0)
                    {
                        item.MarkDeleted();
                    }
                }

                for (int i = 0; i < roles.Count; i++)
                {
                    Role item = roles[i];
                    var contain = from string Id in ids
                                  where item.RoleId == Convert.ToInt32(Id)
                                      && Convert.ToInt32(Id) != -1 && Convert.ToInt32(Id) != 0
                                  select Id;
                    if (contain != null && contain.Count() != 0)
                    {
                        roles[i] = getItem(NewRolesList, item.RoleId);
                        roles[i].MarkOld();
                        roles[i].MarkModified();
                    }
                }

                foreach (string id in ids)
                {
                    var contain = from Role item in roles
                                  where item.RoleId == Convert.ToInt32(id)
                                   && item.RoleId != -1 && item.RoleId != 0
                                  select item;
                    if (contain == null || contain.Count() == 0)
                    {
                        Role item = getItem(NewRolesList, id.ToNumber());

                        if (item != null)
                            roles.Add(item);
                    }
                }
            }
            else {
                foreach (string id in ids)
                {
                    Role item = getItem(NewRolesList, id.ToNumber());

                    if (item != null)
                        roles.Add(item);
                }
            }
           


            try
            {
                foreach (Role roleObj in roles)
                    roleManager.Save(roleObj);

                this.UnLockEntity(Enums_S3.Entity.Role, null);
                return this.AttachStatusCode(null, 1, null);

            }
            catch (Exception ex)
            {
                string s = ex.Message + Environment.NewLine;
                if(ex.InnerException!=null)
                    s += ex.InnerException.Message + Environment.NewLine;
                return this.AttachStatusCode(null, 0, s);
            }

        }

        private Role getItem(object[] items, int Id)
        {
            Role item = null;
            foreach (Dictionary<string, Object> obj in items)
            {
                item = new Role();
                if (obj["RoleId"].ToString().ToNumber() == Id)
                {
                    if (Id == -1)
                        obj["RoleId"] = "0";

                    foreach (KeyValuePair<string, Object> val in obj)
                    {
                        PropertyInfo pro = item.GetType().GetProperty(val.Key);
                        if (pro == null)
                            continue;

                        object value = null;
                        if (val.Value != null && !string.IsNullOrEmpty(val.Value.ToString()))
                            value = val.Value;

                        pro.SetValue(item, GetPropertyValue(pro.PropertyType, value), null);
                    }
                    break;
                }
            }
            return item;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethod(Enums_S3.DefaultPermissions.View)]
        public string FindRolePermissionLite(int roleId)
        {
            RolePermissionCriteria criteria = new RolePermissionCriteria();
            criteria.getAllPermissions = true;
            List<RolePermissionLite> list = rolePermissionManager.FindAllRolePermission(roleId, criteria);
            if (list != null)
            {

                var entites = from RolePermissionLite litePermission in list
                              group litePermission by new { litePermission.EntityId, litePermission.EntityName, litePermission.EntityNameAr } into entity
                              select new
                              {
                                  EntityId = entity.Key.EntityId,
                                  EntityName = entity.Key.EntityName,
                                  EntityNameAr = entity.Key.EntityNameAr,
                                  Permissions = (from RolePermissionLite permission in list
                                                 where entity.Key.EntityId ==((permission.ParentId != 0) ? permission.ParentId : permission.EntityId)
                                                 //orderby permission.EntityId descending
                                                 select new
                                                 {
                                                     PermissionId = permission.PermissionId,
                                                     EntityPermissionId = permission.EntityPermissionId,
                                                     PermissionName = permission.PermissionName,
                                                     PermissionNameAr = permission.PermissionNameAr,
                                                     PermissionDescription = permission.PermissionDescription,
                                                     DefaultPermission = permission.DefaultPermission,
                                                     HasPermission = permission.HasPermission,
                                                     RolePermissionId = permission.RolePermissionId,
                                                     ModuleName=permission.ModuleName,
                                                     ModuleNameAR = permission.ModuleNameAR,
                                                     ModuleId=permission.ModuleId
                                                 }
                                                   )
                              };

                return this.AttachStatusCode(entites, 1, null);

            }
            else
                return this.AttachStatusCode(null, 1, null);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethod(Enums_S3.ExtendedPermissions.None)]
        public string AddEditRolePermission(int roleId, string permissionIds, bool AllPermissions)
        {
           

            try
            {
                DateTime updatedDate = DateTime.Now;
                var returnedData = new
                {
                    roleId = roleId,
                    EntityLastUpdatedDate = updatedDate
                };

                LockPermissionsRole(roleId);

                if (AllPermissions)
                {
                    this.rolePermissionManager.SetAllPermissions(true, roleId, UserId);
                    this.rolePermissionManager.UpdateRolePermissionUpdatedDate(new RolePermissionCriteria() { RoleId = roleId, EntityLastUpdatedDate = updatedDate });
                   return this.AttachStatusCode(returnedData, 1, null);
                }



                List<RolePermission> list = rolePermissionManager.FindByParentId(roleId, typeof(Role), null);
                if (list == null)
                    list = new List<RolePermission>();

                List<RolePermission> allPermissions = this.javaScriptSerializer.Deserialize<List<RolePermission>>(permissionIds);

                if (allPermissions.Count == 0)
                {
                    rolePermissionManager.DeleteByRole(roleId);

                    //delete all role permission
                }
                else
                {
                    IEnumerable<int> all = from RolePermission roleP in allPermissions
                                           select roleP.Permissionid;

                    IEnumerable<int> current = from RolePermission roleP in list
                                               select roleP.Permissionid;

                    string[] toDelete = (from RolePermission permi in list
                                         where !all.Contains(permi.Permissionid)
                                         select permi.EntityId.ToString() + "-" + permi.Permissionid.ToString() +  "-D").ToArray();
                    //"-" + LoggedInUser.UserId +

                    string[] toAdd = (from RolePermission permi in allPermissions
                                      where !current.Contains(permi.Permissionid)
                                      select permi.EntityId.ToString() + "-" + permi.Permissionid.ToString()  + "-A").ToArray();

                    //+ "-"+ LoggedInUser.UserId


                    string permissionStatuIds = string.Join(",", toDelete);
                    permissionStatuIds += string.Join(",", toAdd);

                    rolePermissionManager.Save(roleId, permissionStatuIds,LoggedInUser.UserId);
                }

                this.UnLockEntity(Enums_S3.Entity.RolePermission, roleId);
                this.rolePermissionManager.UpdateRolePermissionUpdatedDate(new RolePermissionCriteria() { RoleId = roleId, EntityLastUpdatedDate = updatedDate });
                return this.AttachStatusCode(returnedData, 1, null);

            }
            catch (SF.Framework.Exceptions.RecordNotAffected ex)
            {
                return this.AttachStatusCode(roleId, 1, null);
            }
            catch (Exception)
            {
                return this.AttachStatusCode(null, 0, null);
            }
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethod(Enums_S3.DefaultPermissions.None)]
        public string LockRolePermissions(int entityId,int roleId, string timeStamp)
        {
            DateTime? dt = null;
            if (!SF.Framework.String.IsEmpty(timeStamp))
            {
                try
                {
                    double d = Convert.ToDouble(timeStamp);
                    dt = new DateTime(1970, 1, 1);
                    dt = dt.Value.AddSeconds(d / 1000);
                }
                catch (Exception)
                { }
            }

           

            RolePermissionCriteria criteria = new RolePermissionCriteria()
            {
                EntityId = entityId,
                RoleId = roleId,
                UserId = LoggedInUser.UserId,
                EntityLastUpdatedDate = dt
            };

            int Status = -1;
            try { 
                Status = rolePermissionManager.LockRolePermissions(criteria);
                return this.AttachStatusCode(null, 1, null);
            }

            catch (SF.Framework.Exceptions.RecordExsitsException ex)
            {
                return this.AttachStatusCode(null, 1, ex.Message);
            }
            catch (SF.Framework.Exceptions.RecordNotAffected ex)
            {
                return this.AttachStatusCode(null, 1, ex.Message);
            }
            catch (SF.Framework.Exceptions.EntityLocked ex)
            {
                EntityLockCriteria entitycriteria = new EntityLockCriteria()
                {
                    EntityId = (int)Enums_S3.Entity.RolePermission,
                    EntityValueId = roleId,
                    LockedBy = LoggedInUser.UserId
                };
                List<EntityLock> lockedList = this.iEntityLockManager.FindAll(entitycriteria);
                var lockEntity = new
                {
                    Message = 402,
                    UserName = lockedList[0].UserName,
                    LockedDate = lockedList[0].CreatedDate
                };
                EndResponse(402, this.javaScriptSerializer.Serialize(lockEntity));
                return null;
            }
            catch (SF.Framework.Exceptions.EntityLockedChanged ex)
            {
                EndResponse(Enums_S3.StatusCode.Codes.EntityLockedChanged, ((int)Enums_S3.StatusCode.Codes.EntityLockedChanged).ToString());
                return null;
            }
            catch (Exception ex)
            {
                return this.AttachStatusCode(null, 0, ex.Message);

            }
           

        }


        private void LockPermissionsRole(int roleId) 
        {
            
            RolePermissionCriteria Criteria = new RolePermissionCriteria()
            {
                RoleId = roleId,
                EntityId = (int)Enums_S3.Entity.RolePermission,
                UserId = LoggedInUser.UserId,
                EntityLastUpdatedDate = this.EntityLastUpdatedDate 
            };


          
            try
            {
                int Status = rolePermissionManager.LockRolePermissions(Criteria);
            }
            catch (SF.Framework.Exceptions.RecordExsitsException ex)
            {
               
            }
            catch (SF.Framework.Exceptions.RecordNotAffected ex)
            {
                
            }
            catch (SF.Framework.Exceptions.EntityLocked ex)
            {
                EntityLockCriteria entitycriteria = new EntityLockCriteria()
                {
                    EntityId = (int)Enums_S3.Entity.RolePermission,
                    EntityValueId = roleId,
                    LockedBy = LoggedInUser.UserId
                };
                List<EntityLock> lockedList = this.iEntityLockManager.FindAll(entitycriteria);
                var lockEntity = new
                {
                    Message = 402,
                    UserName = lockedList[0].UserName,
                    LockedDate = lockedList[0].CreatedDate
                };
                EndResponse(402, this.javaScriptSerializer.Serialize(lockEntity));
            }
            catch (SF.Framework.Exceptions.EntityLockedChanged ex)
            {
                EndResponse(Enums_S3.StatusCode.Codes.EntityLockedChanged, ((int)Enums_S3.StatusCode.Codes.EntityLockedChanged).ToString());
            }
            catch (Exception ex)
            {
                throw ex;

            }



        }
    }
}