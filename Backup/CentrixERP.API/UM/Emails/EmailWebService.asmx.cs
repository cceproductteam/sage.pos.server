using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using CentrixERP.Common.API;
using System.Web.Script.Services;
using Centrix.UM.Business.IManager;
using Centrix.UM.Business.Entity;
using SF.Framework;
using CentrixERP.API.Common.BaseClasses;

namespace Centrix.UM.API
{
    /// <summary>
    /// Summary description for EmailWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class EmailWebService : WebServiceBaseClass
    {

        [WebMethod]
        [ScriptMethod(ResponseFormat=ResponseFormat.Json)]
        public string DeleteEmailUser(int id)
        {
            IEmailUserManager myEmailManager = (IEmailUserManager)IoC.Instance.Resolve(typeof(IEmailUserManager));
            EmailUser myEmail = new EmailUser();
            myEmail.EmailUserId = id;
            myEmail.MarkDeleted();
            myEmailManager.Save(myEmail);
            return "true";
        }
    }
}
