using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using CentrixERP.Common.API;
using Centrix.UM.Business.IManager;
using Centrix.UM.Business.Entity;
using SF.Framework;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using SF.CustomScriptControls;
using System.Reflection;
using CentrixERP.Configuration;
using CentrixERP.API.Common.BaseClasses;

namespace Centrix.UM.API
{
    /// <summary>
    /// Summary description for TeamWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    [EntityAttribute(Enums_S3.Entity.Team)]
    public class TeamWebService : WebServiceBaseClass
    {
        ITeamManager teamManager = null;
        IUserManager userManager = null;
        public TeamWebService()
        {
            teamManager = (ITeamManager)IoC.Instance.Resolve(typeof(ITeamManager));
             userManager = (IUserManager)IoC.Instance.Resolve(typeof(IUserManager));
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.Delete)]
        public bool Delete(int id)
        {
            Team team = teamManager.FindById(id, null); //new Team();
            team.TeamId = id;
            team.MarkDeleted();

            try
            {
                teamManager.Save(team);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.View)]
        public string FindTeam(int id)
        {
            Team team = teamManager.FindById(id, null);

            Object teamLite = null;
            if (team != null)
            {
                teamLite = new
                {
                    TeamName = team.Name,
                    Description = team.Description
                };
            }

            return this.AttachStatusCode(teamLite, 0, null);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.View)]
        public string FindALL(string keyword, int from, int to)
        {
            TeamCriteria criteria = new TeamCriteria() { Name = keyword };
            List<Team> teams = teamManager.FindAll(criteria);
            Object teamList = null;


            if (teams != null)
            {
                teamList = from Team team in teams
                           select new AutoCompleteItem()
                           {
                               value = team.TeamId,
                               label = team.Name
                           };

            }
            return this.AttachStatusCode(teamList, 0, null);
        }



        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.View)]
        public string FindAllLite(string keyword, int page, int resultCount, string argsCriteria)
        {
            TeamCriteria teamCriteria = null;
            if (!SF.Framework.String.IsEmpty(argsCriteria))
            {
                teamCriteria = this.javaScriptSerializer.Deserialize<TeamCriteria>(argsCriteria);
            }
            else
                teamCriteria = new TeamCriteria();

            teamCriteria.keyword = keyword;
            teamCriteria.pageNumber = page;
            teamCriteria.resultCount = resultCount;

            List<Team_Lite> teamsList = teamManager.FindAllLite(teamCriteria);
            if (teamsList != null && teamsList.Count > 0)
                return this.AttachStatusCode(teamsList, 1, null);
            else
                return this.AttachStatusCode(null, 0, null);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.View)]
        public string FindByIdLite(int id)
        {
            Team_Lite team = teamManager.FindByIdLite(id);
            if (team != null)
                return this.AttachStatusCode(team, 1, null);
            else
                return this.AttachStatusCode(null, 0, null);
        }


        [WebMethod]
        [EntityMethodAttribute(Enums_S3.ExtendedPermissions.None)]
        public string SaveTeam(int UserId, string TeamsList)
        {
            List<Team> teams = teamManager.FindAll(null);
            if (teams != null)
                this.LockEntity(Enums_S3.Entity.Team, null);
            else
                teams = new List<Team>();

            Object[] NewTeamsList = this.javaScriptSerializer.Deserialize<Object[]>(unEscape((TeamsList.ToString())));

            List<string> ids = (from Object obj in NewTeamsList
                                select ((Dictionary<string, object>)obj)["TeamId"].ToString()).ToList();

            if (teams != null)
            {
                foreach (Team item in teams)
                {
                    var contain = from string Id in ids where item.TeamId == Convert.ToInt32(Id) select Id;
                    if (contain == null || contain.Count() == 0)
                    {
                        item.MarkDeleted();
                    }
                }

                foreach (string id in ids)
                {
                    var contain = from Team item in teams
                                  where item.TeamId == Convert.ToInt32(id)
                                  select item;
                    if (contain == null || contain.Count() == 0)
                    {
                        Team item = getItem(NewTeamsList, id.ToNumber());
                        if (item != null)
                            teams.Add(item);
                    }
                }

                for (int i = 0; i < teams.Count; i++)
                {
                    Team item = teams[i];
                    var contain = from string Id in ids
                                  where item.TeamId == Convert.ToInt32(Id)
                                      && Convert.ToInt32(Id) != -1 && Convert.ToInt32(Id) != 0
                                  select Id;
                    if (contain != null && contain.Count() != 0)
                    {
                        teams[i] = getItem(NewTeamsList, item.TeamId);
                        teams[i].MarkOld();
                        teams[i].MarkModified();
                    }
                }
            }
            else {
                foreach (string id in ids)
                {
                    Team item = getItem(NewTeamsList, id.ToNumber());
                    if (item != null)
                        teams.Add(item);
                }
            }


            try
            {
                foreach (Team teamObj in teams)
                    teamManager.Save(teamObj);
                this.UnLockEntity(Enums_S3.Entity.Team, null);
                return this.AttachStatusCode(null, 1, null);

            }
            catch (Exception ex)
            {
                return this.AttachStatusCode(null, 0, null);
            }

        }


        private Team getItem(object[] items, int Id)
        {
            Team item = null;
            foreach (Dictionary<string, Object> obj in items)
            {
                item = new Team();
                if (obj["TeamId"].ToString().ToNumber() == Id)
                {
                    if (Id == -1)
                        obj["TeamId"] = "0";

                    foreach (KeyValuePair<string, Object> val in obj)
                    {
                        PropertyInfo pro = item.GetType().GetProperty(val.Key);
                        if (pro == null)
                            continue;

                        object value = null;
                        if (val.Value != null && !string.IsNullOrEmpty(val.Value.ToString()))
                            value = val.Value;

                        pro.SetValue(item, GetPropertyValue(pro.PropertyType, value), null);
                    }
                    break;
                }
            }
            return item;
        }



        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.View)]
        public string FindTeamUsersLite(int teamId)
        {

            List<TeamUsersLite> user = userManager.FindTeamUsersLite(teamId);

            return this.AttachStatusCode(user, 0, null);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.ExtendedPermissions.None)]
        public string SaveUsersTeam(int teamId, string teamUserIds)
        {
            try
            {
                userManager.AddUserTeams(teamId, teamUserIds);
                return this.AttachStatusCode(null, 0, null);
            }
            catch (Exception)
            {

                return this.AttachStatusCode(null, 0, null);
            }



        }
    }
}