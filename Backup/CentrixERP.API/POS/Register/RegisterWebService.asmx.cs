using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using SF.Framework;
using E = CentrixERP.Business.Entity;
using CentrixERP.Business.IManager;
using CentrixERP.Common.API;
using System.Reflection;
using CentrixERP.Configuration;
using SF.FrameworkEntity;
using CentrixERP.API.Common.BaseClasses;



namespace Centrix.POS.Server.API
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    [EntityAttribute(Enums_S3.Entity.Register)]
    public class RegisterWebService : WebServiceBaseClass
    {
        IRegisterManager iRegisterManager = null;
        public RegisterWebService()
        {
            iRegisterManager = (IRegisterManager)IoC.Instance.Resolve(typeof(IRegisterManager));
        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.Delete)]
        public string Delete(int id)
        {
            this.LockEntityForDelete(Enums_S3.Entity.Register, id);
            E.Register myRegister = iRegisterManager.FindById(id, null);
            myRegister.RegisterId = id;
            myRegister.MarkDeleted();
            try
            {
                iRegisterManager.Save(myRegister);
                UnLockEntity(Enums_S3.Entity.Register, id);
                return AttachStatusCode(true, 1, null);
            }
            catch (Exception ex)
            {
                return AttachStatusCode(null, 0, HandleException(ex));
            }

        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.List)]
        public string FindAllLite(string keyword, int page, int resultCount, string argsCriteria)
        {
            E.RegisterCriteria criteria = null;
            if (!SF.Framework.String.IsEmpty(argsCriteria))
            {
                criteria = this.javaScriptSerializer.Deserialize<E.RegisterCriteria>(argsCriteria);
            }
            else
                criteria = new E.RegisterCriteria();

            criteria.Keyword = keyword;
            criteria.pageNumber = page;
            criteria.resultCount = resultCount;


            List<E.RegisterLite> RegisterList = iRegisterManager.FindAllLite(criteria);
            return this.AttachStatusCode(RegisterList, 1, null);

        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.View)]
        public string FindByIdLite(int id)
        {
            E.RegisterLite myRegister = iRegisterManager.FindByIdLite(id, null);
            return this.AttachStatusCode(myRegister, 1, null);

        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.Add)]
        public string Add(int id, string RegisterInfo)
        {
            return Save(true, id, RegisterInfo);
        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.Edit)]
        public string Edit(int id, string RegisterInfo)
        {
            if (id <= 0)
            {
                ValidationError error = new ValidationError()
                {
                    ControlName = "",
                    GlobalMessage = true,
                    Message = "Please select Register"
                };
                List<ValidationError> errors = new List<ValidationError>(){
                error
                };
                return this.AttachStatusCode(errors, 0, "validation");
            }
            else
            {
                return Save(false, id, RegisterInfo);
            }
        }

        private string Save(bool add, int id, string RegisterInfo)
        {
            E.Register myRegister = this.javaScriptSerializer.Deserialize<E.Register>(this.unEscape((RegisterInfo)));
            if (!add)
            {
                this.LockEntity(Enums_S3.Entity.Register, id);
                myRegister.MarkOld();
                myRegister.MarkModified();
                myRegister.RegisterId = id;
                myRegister.UpdatedBy = LoggedInUser.UserId;
            }
            else
            {
                myRegister.CreatedBy = LoggedInUser.UserId;
            }


            List<ValidationError> errors = iRegisterManager.Validate(myRegister);
            if (errors != null && errors.Count > 0)
            {
                return this.AttachStatusCode(errors, 0, "validation");
            }


            try
            {
                iRegisterManager.Save(myRegister);
                if (!add)
                    this.UnLockEntity(Enums_S3.Entity.Register, id);
                return this.FindByIdLite(myRegister.RegisterId);

            }
            catch (SF.Framework.Exceptions.RecordExsitsException ex)
            {
                return this.AttachStatusCode(null, 501, null);
            }
            catch (SF.Framework.Exceptions.RecordNotAffected ex)
            {
                return this.AttachStatusCode(null, 501, null);
            }
            catch (Exception ex)
            {

                return this.AttachStatusCode(null, 0, HandleException(ex));
            }
        }
    }
}

