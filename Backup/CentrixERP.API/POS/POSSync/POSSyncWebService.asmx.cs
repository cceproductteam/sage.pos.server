using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using CentrixERP.Business.IManager;
using E = CentrixERP.Business.Entity;
using SF.Framework;
using System.Reflection;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services.Protocols;
using CentrixERP.Common.API;
using CentrixERP.Configuration;
using CentrixERP.API.Common.BaseClasses;

namespace Centrix.POS.API
{
    /// <summary>
    /// Summary description for POSSyncWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    [EntityAttribute(Enums_S3.Entity.POSSync)]
    public class POSSyncWebService : WebServiceBaseClass
    {
        IPOSSyncManager iPOSSyncManager = null;
       // ILocalConfigurationManager myConfigManager = null;
        IStoreManager iStoreManager = null;

        public POSSyncWebService()
        {
            iPOSSyncManager = (IPOSSyncManager)IoC.Instance.Resolve(typeof(IPOSSyncManager));
            // myConfigManager = (ILocalConfigurationManager)IoC.Instance.Resolve(typeof(ILocalConfigurationManager));
            iStoreManager = (IStoreManager)IoC.Instance.Resolve(typeof(IStoreManager));
        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string SyncPOSData(string SyncedDataObj, bool isServerSync)
        {

            E.POSSync entity = this.javaScriptSerializer.Deserialize<E.POSSync>(this.unEscape((SyncedDataObj)));
            JavaScriptSerializer ser = new JavaScriptSerializer();
            IPOSSyncManager myManager = (IPOSSyncManager)IoC.Instance.Resolve(typeof(IPOSSyncManager));

            //List<string> ids = (from Object obj in (Object[])SyncedDataObj
            //                    select ((Dictionary<string, object>)obj)["StoreId"].ToString()).ToList();
            //string[] results = new string[ids.Count()];
            int registerCount = 0;
            //foreach (string id in ids)
            //{
            //    int ErrorType = 0;
            //    E.POSSync mySync = getSyncItem(SyncedDataObj, id.ToNumber());
            //    E.LocalConfigurationCriteria configCriteria = new E.LocalConfigurationCriteria();
            //    mySync.ServerDataBaseName = BasePageLoggedIn.config.PosServerDataBaseName;
            //    mySync.ServerDataBaseIp = BasePageLoggedIn.config.POSServerIP;
            //    if (!isServerSync)
            //    {
            //        IRegisterManager myRegisterManager = (IRegisterManager)IoC.Instance.Resolve(typeof(IRegisterManager));
            //        E.Register CurrenctRegister = myRegisterManager.FindById(mySync.RegisterId, null);
            //        mySync.RegistersIP = CurrenctRegister.DataBaseInstanceName;
            //        mySync.Password = BasePageLoggedIn.config.PosServerDataBasePassword;
            //        mySync.UserName = BasePageLoggedIn.config.PosServerDataBaseUserame;
            //        mySync.DataBaseName = CurrenctRegister.DataBaseName;

            //    }
            //    configCriteria.IPAddress = mySync.RegistersIP;
            //    configCriteria.SyncMode = 3;
            //    configCriteria.DataBaseName = mySync.DataBaseName;

            //    if (isServerSync)
            //    {
            //        try
            //        {
            //            configCriteria.SyncMode = 2;
            //            //myConfigManager.SetSyncMode(configCriteria);

            //        }
            //        catch (Exception ex)
            //        {
            //            if (!System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            //            {
            //                ErrorType = 1;//Connection Error
            //            }
            //            if (ex.GetType() == typeof(System.Data.SqlClient.SqlException))
            //            {
            //                ErrorType = 2;
            //            }
            //            if (ex.GetType() == typeof(SF.Framework.Exceptions.RecordNotAffected))
            //            {

            //            }

            //        }
            //    }

            //    try
            //    {

            //        configCriteria.SyncMode = 3;
            //        myManager.SyncPOSData(mySync);
            //        results[registerCount] = mySync.RegisterName + "," + ErrorType;
            //        try
            //        {
            //           // myConfigManager.SetSyncMode(configCriteria);
            //        }
            //        catch (Exception ex)
            //        {
            //            if (!System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            //            {
            //                ErrorType = 1;//Connection Error
            //            }
            //            if (ex.GetType() == typeof(System.Data.SqlClient.SqlException))
            //            {
            //                ErrorType = 2;
            //            }
            //            if (ex.GetType() == typeof(SF.Framework.Exceptions.RecordNotAffected))
            //            {

            //            }

            //        }
            //    }

            //    catch (Exception ex)
            //    {
            //        results[registerCount] = mySync.RegisterName + "," + ErrorType;
            //        configCriteria.SyncMode = 3;

            //        try
            //        {
            //            //myConfigManager.SetSyncMode(configCriteria);

            //        }
            //        catch (Exception inEx)
            //        {
            //            if (inEx.GetType() == typeof(SF.Framework.Exceptions.RecordNotAffected))
            //            {

            //            }
            //        }
            //        if (!System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            //        {
            //            ErrorType = 1;//Connection Error
            //        }
            //        if (ex.GetType() == typeof(System.Data.SqlClient.SqlException))
            //        {
            //            ErrorType = 2;
            //        }
            //        if (ex.GetType() == typeof(SF.Framework.Exceptions.RecordNotAffected))
            //        {
            //            ErrorType = 3;
            //        }
            //        results[registerCount] = mySync.RegisterName + "," + ErrorType;

            //    }
            //    registerCount++;
            //}

           // return ser.Serialize(results);
            return "entity";
        }

        private E.POSSync getSyncItem(object items, int Id)
        {
            E.POSSync SyncItem = null;
            foreach (Dictionary<string, Object> obj in (Object[])items)
            {
                SyncItem = new E.POSSync();
                if (obj["StoreId"].ToString().ToNumber() == Id)
                {

                    foreach (KeyValuePair<string, Object> val in obj)
                    {
                        PropertyInfo pro = SyncItem.GetType().GetProperty(val.Key);
                        if (pro == null)
                            continue;

                        object value = null;
                        if (val.Value != null && !string.IsNullOrEmpty(val.Value.ToString()))
                            value = val.Value;

                        pro.SetValue(SyncItem, GetPropertyValue(pro.PropertyType, value), null);
                    }
                    break;
                }
            }


            return SyncItem;
        }

        private object GetPropertyValue(Type propertyType, object value)
        {
            if (propertyType == typeof(double))
            {
                value = Convert.ToDouble(value);
            }
            else if (propertyType == typeof(int))
            {
                value = Convert.ToInt32(value);
            }
            else if (propertyType == typeof(string))
            {
                value = Convert.ToString(value);
            }
            else if (propertyType == typeof(bool))
            {
                value = Convert.ToBoolean(value);
            }
            else if (propertyType == typeof(decimal))
            {
                value = Convert.ToDecimal(value);
            }
            else if (propertyType == typeof(DateTime))
            {
                value = Convert.ToDateTime(value);
            }

            return value;
        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public bool CheckIPConnectivity(string IpAddress)
        {
            bool registerStatus = true;
            System.Net.NetworkInformation.Ping ping = new System.Net.NetworkInformation.Ping();
            try
            {
                if (ping.Send(IpAddress).Status != System.Net.NetworkInformation.IPStatus.Success)
                    registerStatus = false;
            }
            catch (Exception ex)
            {
                registerStatus = false;

            }         
            return registerStatus;
        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.View)]
        public string FindAllLite(string keyword, int page, int resultCount, string argsCriteria)
        {
            E.StoreCriteria criteria = null;
            if (!SF.Framework.String.IsEmpty(argsCriteria))
            {
                criteria = this.javaScriptSerializer.Deserialize<E.StoreCriteria>(argsCriteria);
            }
            else
                criteria = new E.StoreCriteria();

            criteria.Keyword = keyword;
            criteria.pageNumber = page;
            criteria.resultCount = resultCount;


            List<E.StoreLite> StoreList = iStoreManager.FindAllLite(criteria);
            return this.AttachStatusCode(StoreList, 1, null);

        }




    }
}
