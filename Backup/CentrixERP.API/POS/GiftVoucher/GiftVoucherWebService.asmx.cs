using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using SF.Framework;
using E = CentrixERP.Business.Entity;
using CentrixERP.Business.IManager;
using CentrixERP.Common.API;
using System.Reflection;
using CentrixERP.Configuration;
using SF.FrameworkEntity;



namespace Centrix.POS.Server.API
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    [EntityAttribute(Enums_S3.Entity.GiftVoucher)]
    public class GiftVoucherWebService : CentrixERP.API.Common.BaseClasses.WebServiceBaseClass
    {
        IGiftVoucherManager iGiftVoucherManager = null;
        public GiftVoucherWebService()
        {
            iGiftVoucherManager = (IGiftVoucherManager)IoC.Instance.Resolve(typeof(IGiftVoucherManager));
        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.Delete)]
        public string Delete(int id)
        {
            this.LockEntityForDelete(Enums_S3.Entity.GiftVoucher, id);
            E.GiftVoucher myGiftVoucher = iGiftVoucherManager.FindById(id, null);
            myGiftVoucher.GiftVoucherId = id;
            myGiftVoucher.MarkDeleted();
            try
            {
                iGiftVoucherManager.Save(myGiftVoucher);
                return AttachStatusCode(true, 1, null);
            }
            catch (Exception ex)
            {
                return AttachStatusCode(null, 0, HandleException(ex));
            }

        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.List)]
        public string FindAllLite(string keyword, int page, int resultCount, string argsCriteria)
        {
            E.GiftVoucherCriteria criteria = null;
            if (!SF.Framework.String.IsEmpty(argsCriteria))
            {
                criteria = this.javaScriptSerializer.Deserialize<E.GiftVoucherCriteria>(argsCriteria);
            }
            else
                criteria = new E.GiftVoucherCriteria();

            criteria.Keyword = keyword;
            criteria.pageNumber = page;
            criteria.resultCount = resultCount;


            List<E.GiftVoucherLite> GiftVoucherList = iGiftVoucherManager.FindAllLite(criteria);
            return this.AttachStatusCode(GiftVoucherList, 1, null);

        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.View)]
        public string FindByIdLite(int id)
        {
            E.GiftVoucherLite myGiftVoucher = iGiftVoucherManager.FindByIdLite(id, null);
            return this.AttachStatusCode(myGiftVoucher, 1, null);

        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.Add)]
        public string Add(int id, string GiftVoucherInfo)
        {
            return Save(true, id, GiftVoucherInfo);
        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.Edit)]
        public string Edit(int id, string GiftVoucherInfo)
        {
            if (id <= 0)
            {
                ValidationError error = new ValidationError()
                {
                    ControlName = "",
                    GlobalMessage = true,
                    Message = "Please select GiftVoucher"
                };
                List<ValidationError> errors = new List<ValidationError>(){
                error
                };
                return this.AttachStatusCode(errors, 0, "validation");
            }
            else
            {
                return Save(false, id, GiftVoucherInfo);
            }
        }

        private string Save(bool add, int id, string GiftVoucherInfo)
        {
            E.GiftVoucher myGiftVoucher = this.javaScriptSerializer.Deserialize<E.GiftVoucher>(this.unEscape((GiftVoucherInfo)));
            if (!add)
            {
                this.LockEntity(Enums_S3.Entity.GiftVoucher, id);
                myGiftVoucher.MarkOld();
                myGiftVoucher.MarkModified();
                myGiftVoucher.GiftVoucherId = id;
                myGiftVoucher.UpdatedBy = LoggedInUser.UserId;
            }
            else
            {
                myGiftVoucher.CreatedBy = LoggedInUser.UserId;
            }


            List<ValidationError> errors = iGiftVoucherManager.Validate(myGiftVoucher);
            if (errors != null && errors.Count > 0)
            {
                return this.AttachStatusCode(errors, 0, "validation");
            }


            try
            {
                iGiftVoucherManager.Save(myGiftVoucher);
                if (!add)
                    this.UnLockEntity(Enums_S3.Entity.GiftVoucher, id);
                return this.FindByIdLite(myGiftVoucher.GiftVoucherId);

            }
            catch (SF.Framework.Exceptions.RecordExsitsException ex)
            {
                return this.AttachStatusCode(null, 501, null);
            }
            catch (SF.Framework.Exceptions.RecordNotAffected ex)
            {
                return this.AttachStatusCode(null, 501, null);
            }
            catch (Exception ex)
            {

                return this.AttachStatusCode(null, 0, HandleException(ex));
            }
        }



        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.View)]
        public string search(string giftCode)
        {
            E.GiftVoucher myGiftVoucher = iGiftVoucherManager.search(giftCode);
            return this.AttachStatusCode(myGiftVoucher, 1, null);

        }


        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.Edit)]
        public string EditVoucher(int id)
        {
            try
            {
                iGiftVoucherManager.EditVoucher(id);
                this.UnLockEntity(Enums_S3.Entity.GiftVoucher, id);
                return this.FindByIdLite(id);
            }
            catch (SF.Framework.Exceptions.RecordExsitsException ex)
            {
                return this.AttachStatusCode(null, 501, null);
            }
            catch (SF.Framework.Exceptions.RecordNotAffected ex)
            {
                return this.AttachStatusCode(null, 501, null);
            }
            catch (Exception ex)
            {

                return this.AttachStatusCode(null, 0, HandleException(ex));
            }

        }
    }
}

