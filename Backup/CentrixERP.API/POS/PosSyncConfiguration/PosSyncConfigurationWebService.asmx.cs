using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using SF.Framework;
using E = CentrixERP.Business.Entity;
using CentrixERP.Business.IManager;
using CentrixERP.Common.API;
using System.Reflection;
using CentrixERP.Configuration;
using SF.FrameworkEntity;
using CentrixERP.API.Common.BaseClasses;



namespace Centrix.POS.Server.API
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    [EntityAttribute(Enums_S3.Entity.PosSyncConfiguration)]
    public class PosSyncConfigurationWebService : WebServiceBaseClass
    {
        //CentrixERP.Business.IManager.IPosSyncConfigurationManage
        IPosSyncConfigurationManager iPosSyncConfigurationManager = null;
        public PosSyncConfigurationWebService()
        {
            iPosSyncConfigurationManager = (IPosSyncConfigurationManager)IoC.Instance.Resolve(typeof(IPosSyncConfigurationManager));
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.Delete)]
        public string Delete(int id)
        {
            this.LockEntityForDelete(Enums_S3.Entity.PosSyncConfiguration, id);
            E.PosSyncConfiguration myPosSyncConfiguration = iPosSyncConfigurationManager.FindById(id, null);
            myPosSyncConfiguration.PosSyncConfigurationId = id;
            myPosSyncConfiguration.MarkDeleted();
            try
            {
                iPosSyncConfigurationManager.Save(myPosSyncConfiguration);
                return AttachStatusCode(true, 1, null);
            }
            catch (Exception ex)
            {
                return AttachStatusCode(null, 0, HandleException(ex));
            }

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.List)]
        public string FindAllLite(string keyword, int page, int resultCount, string argsCriteria)
        {
            E.PosSyncConfigurationCriteria criteria = null;
            if (!SF.Framework.String.IsEmpty(argsCriteria))
            {
                criteria = this.javaScriptSerializer.Deserialize<E.PosSyncConfigurationCriteria>(argsCriteria);
            }
            else
                criteria = new E.PosSyncConfigurationCriteria();

            criteria.Keyword = keyword;
            criteria.pageNumber = page;
            criteria.resultCount = resultCount;


            List<E.PosSyncConfigurationLite> PosSyncConfigurationList = iPosSyncConfigurationManager.FindAllLite(criteria);
            return this.AttachStatusCode(PosSyncConfigurationList, 1, null);

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.View)]
        public string FindByIdLite(int id)
        {
            E.PosSyncConfigurationLite myPosSyncConfiguration = iPosSyncConfigurationManager.FindByIdLite(id, null);
            return this.AttachStatusCode(myPosSyncConfiguration, 1, null);

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.Add)]
        public string Add(int id, string PosSyncConfigurationInfo)
        {
            return Save(true, id, PosSyncConfigurationInfo);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.Edit)]
        public string Edit(int id, string PosSyncConfigurationInfo)
        {
            if (id <= 0)
            {
                ValidationError error = new ValidationError()
                {
                    ControlName = "",
                    GlobalMessage = true,
                    Message = "Please select PosSyncConfiguration"
                };
                List<ValidationError> errors = new List<ValidationError>(){
                error
                };
                return this.AttachStatusCode(errors, 0, "validation");
            }
            else
            {
                return Save(false, id, PosSyncConfigurationInfo);
            }
        }

        private string Save(bool add, int id, string PosSyncConfigurationInfo)
        {
            E.PosSyncConfiguration myPosSyncConfiguration = this.javaScriptSerializer.Deserialize<E.PosSyncConfiguration>(this.unEscape((PosSyncConfigurationInfo)));
            if (!add)
            {
                this.LockEntity(Enums_S3.Entity.PosSyncConfiguration, id);
                myPosSyncConfiguration.MarkOld();
                myPosSyncConfiguration.MarkModified();
                myPosSyncConfiguration.PosSyncConfigurationId = id;
                myPosSyncConfiguration.UpdatedBy = LoggedInUser.UserId;
            }
            else
            {
                myPosSyncConfiguration.CreatedBy = LoggedInUser.UserId;
            }


            List<ValidationError> errors = iPosSyncConfigurationManager.Validate(myPosSyncConfiguration);
            if (errors != null && errors.Count > 0)
            {
                return this.AttachStatusCode(errors, 0, "validation");
            }


            try
            {
                iPosSyncConfigurationManager.Save(myPosSyncConfiguration);
                if (!add)
                    this.UnLockEntity(Enums_S3.Entity.PosSyncConfiguration, id);
                return this.FindByIdLite(myPosSyncConfiguration.PosSyncConfigurationId);

            }
            catch (SF.Framework.Exceptions.RecordExsitsException ex)
            {
                return this.AttachStatusCode(null, 501, null);
            }
            catch (Exception ex)
            {

                return this.AttachStatusCode(null, 0, HandleException(ex));
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string getTime()
        {
            List<timeClass> TimeList = new List<timeClass>();
            timeClass newTime = null;
            for (int i = 1; i < 25; i++)
            {
                newTime = new timeClass();
                newTime.label = i < 9 ? "0" + i.ToString() + " : 00" : i.ToString() + " : 00";
                newTime.value = i;
                TimeList.Add(newTime);
            }
            return this.AttachStatusCode(TimeList, 1, null);
            // return Save(true, id, SyncScheduleConfigInfo);
        }


        private class timeClass
        {
            public string label { get; set; }
            public int value { get; set; }
        }
    }
}

