using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using CentrixERP.Business.IManager;
using E = CentrixERP.Business.Entity;
using SF.Framework;
using System.Reflection;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services.Protocols;
using CentrixERP.Common.API;
using CentrixERP.Configuration;
using CentrixERP.Business.Entity;
using CentrixERP.API.Common.BaseClasses;

namespace Centrix.POS.API
{
    /// <summary>
    /// Summary description for POSSyncWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    [EntityAttribute(Enums_S3.Entity.None)]
    public class PosSyncLogWebService : WebServiceBaseClass
    {
        IPosSyncLogManager iPOSSyncManager = null;
        // ILocalConfigurationManager myConfigManager = null;
        IStoreManager iStoreManager = null;

        public PosSyncLogWebService()
        {
            iPOSSyncManager = (IPosSyncLogManager)IoC.Instance.Resolve(typeof(IPosSyncLogManager));
            // myConfigManager = (ILocalConfigurationManager)IoC.Instance.Resolve(typeof(ILocalConfigurationManager));
            //  iStoreManager = (IStoreManager)IoC.Instance.Resolve(typeof(IStoreManager));
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindPosSyncLogAllLite(string StoreIds, string SyncEntityIDs, string DateFrom, string DateTo, int status ,string Message)
        {
            PosSyncLogCriteria posSyncLogCriteria = null;
            //if (!SF.Framework.String.IsEmpty(argsCriteria))
            //{
            //    posSyncLogCriteria = this.javaScriptSerializer.Deserialize<PosSyncLogCriteria>(argsCriteria);
            //}
            //else
            //{
            
            posSyncLogCriteria = new PosSyncLogCriteria();
            if (!string.IsNullOrEmpty(StoreIds))
                posSyncLogCriteria.StoreIds = StoreIds;
            if (!string.IsNullOrEmpty(SyncEntityIDs))
                posSyncLogCriteria.SyncEntityIDs = SyncEntityIDs;
            if (!string.IsNullOrEmpty(DateFrom))
                posSyncLogCriteria.DateFrom = DateFrom;
            if (!string.IsNullOrEmpty(DateTo))
                posSyncLogCriteria.DateTo = DateTo;
            if(status!=3)
                posSyncLogCriteria.Status =status==1?true:false;

            if (!string.IsNullOrEmpty(Message))
                posSyncLogCriteria.Message = Message;

            List<PosSyncLogLite> posSyncLogLite = iPOSSyncManager.FindAllLite(posSyncLogCriteria);

            if (posSyncLogLite != null && posSyncLogLite.Count() > 0)
                return this.AttachStatusCode(posSyncLogLite, 1, null);
            else
                return this.AttachStatusCode(null, 1, null);
        }

    }
}
