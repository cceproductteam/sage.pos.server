﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using SF.Framework;
using System.Web.Script.Serialization;
using CentrixERP.Common.Business.IManager;
using CE = CentrixERP.Common.Business.Entity;
using N=CentrixERP.Common.Business.Factory;
using CentrixERP.API.Common.BaseClasses;
using CentrixERP.Common.API;
using CentrixERP.Configuration;

namespace CentrixERP.API.Common
{
    /// <summary>
    /// Summary description for NoteWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    [EntityAttribute(Enums_S3.Entity.Note)]
    public class NoteWebService : WebServiceBaseClass
    {
        // N.NoteFactory NoteFactory = null;
        INoteEntityManager iNoteEntityManager = null;
        //NoteEntity noteEntity = null;
        public NoteWebService()
        {
            // NoteFactory = (NoteFactory)IoC.Instance.Resolve(typeof(NoteFactory));
            this.iNoteEntityManager = IoC.Instance.Resolve<INoteEntityManager>();

        }

        //[WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        //[EntityMethod(Enums_S3.DefaultPermissions.View)]
        //public string FindById(int id, string mKey)
        //{
        //    NoteFactory = new N.NoteFactory(mKey);
        //    N.INote inote = NoteFactory.FindById(id, null);
        //    return this.AttachStatusCode(inote, 1, null);

        //}

        //[WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        //[EntityMethod(Enums_S3.DefaultPermissions.Delete)]
        //public string DeleteNote(int id)
        //{
        //    noteEntity = iNoteEntityManager.FindById(id, null);
        //    //NoteFactory = new N.NoteFactory(mKey);
        //   // N.INote inote = NoteFactory.FindById(id, null);
        //    this.LockEntityForDelete(Enums_S3.Entity.Note, noteEntity.NoteId);
        //    noteEntity.MarkDeleted();
        //    iNoteEntityManager.Save(noteEntity);
        //    this.UnLockEntity(Enums_S3.Entity.Note, noteEntity.NoteId);
        //    return this.AttachStatusCode(null, 1, null);
        //}

        //[WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        //[EntityMethod(Enums_S3.DefaultPermissions.View)]
        //public string FindAll2(int ParentId, string mKey)
        //{
        //    NoteFactory = new N.NoteFactory(mKey);
        //    List<N.INote> inoteList = NoteFactory.FindByParentId(ParentId, null);
        //    if (inoteList != null && inoteList.Count() > 0)
        //    {
        //        var notes = from N.INote note in inoteList
        //                    where note.NoteObj != null
        //                    select new
        //                    {
        //                        Id = note.Id,
        //                        NoteID = note.NoteObj.NoteID,
        //                        NoteContent = note.NoteObj.NoteContent,
        //                        //UpdatedBy = note.NoteObj.UpdatedBy,
        //                        //CreatedBy = note.NoteObj.UpdatedBy,
        //                        CreatedBy = note.NoteObj.CreatedByName,
        //                        UpdatedBy = note.NoteObj.UpdatedByName,
        //                        Crearted_Date = note.NoteObj.Crearted_Date,
        //                        Updated_Date = note.NoteObj.Updated_Date,
        //                    };
        //        return this.AttachStatusCode(notes, 1, null);
        //    }
        //    else
        //        return this.AttachStatusCode(null, 1, null);

        //}


        //[WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        //[EntityMethodAttribute(true, "id", ValidDefaultPermissions1 = Enums_S3.DefaultPermissions.Edit, ValidDefaultPermissions2 = Enums_S3.DefaultPermissions.Add)]
        //public string AddEditNote(int id, int moduleId, string mKey, string content, int userId)
        //{

        //    NoteFactory = new N.NoteFactory(mKey);
        //    N.INote iNote = null;
        //    if (id <= 0)
        //    {
        //        iNote = NoteFactory.IEntity;
        //        iNote.CreatedBy = userId;
        //        iNote.NoteObj = new CE.Note();
        //        iNote.NoteObj.Createdby = userId;
        //    }
        //    else
        //    {
        //        iNote = NoteFactory.FindById(id, null);
        //        this.LockEntity(Enums_S3.Entity.Note, iNote.NoteId);
        //        iNote.MarkModified();
        //        iNote.NoteObj.MarkModified();
        //        iNote.NoteObj.UpdatedBy = userId;
        //        iNote.UpdatedBy = userId;
        //    }
        //    iNote.Id = id;
        //    iNote.ModuleId = moduleId;
        //    iNote.NoteObj.NoteContent = unEscape(content);

        //    try
        //    {
        //        NoteFactory.Save(iNote);

        //        if (id > 0)
        //            this.UnLockEntity(Enums_S3.Entity.Note, iNote.NoteId);

        //        return this.AttachStatusCode(iNote.NoteObj.NoteID, 1, null);
        //    }
        //    catch (Exception)
        //    {
        //        this.UnLockEntity(Enums_S3.Entity.Note, iNote.NoteId);
        //        return this.AttachStatusCode(null, 0, null);
        //    }

        //}

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethod(Enums_S3.DefaultPermissions.View)]
        public string FindAllLite(int entityId, int entityValueId)
        {
            CE.NoteEntityCriteria criteria = new CE.NoteEntityCriteria()
            {
                EntityId = entityId,
                EntityValueId = entityValueId
            };
            List<CE.NoteEntityLite> list = this.iNoteEntityManager.FindAllLite(criteria);

            if (list != null && list.Count > 0)
            {
                var notes = from CE.NoteEntityLite note in list
                            select new
                            {
                                Id = note.NoteEntityId,
                                NoteID = note.NoteId,
                                NoteContent = note.NoteContent,
                                CreatedBy = note.CreatedByName,
                                UpdatedBy = note.UpdatedByName,
                                Crearted_Date = note.CreatedDate,
                                Updated_Date = note.UpdatedDate,
                            };
                return this.AttachStatusCode(notes, 1, null);
            }
            else
            {
                return this.AttachStatusCode(null, 1, null);
            }
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(true, "id", ValidDefaultPermissions1 = Enums_S3.DefaultPermissions.Edit, ValidDefaultPermissions2 = Enums_S3.DefaultPermissions.Add)]
        public string Add(int id, int entityId, int entityValueId, string content)
        {
            return Save(true, id, entityId, entityValueId, content);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(true, "id", ValidDefaultPermissions1 = Enums_S3.DefaultPermissions.Edit, ValidDefaultPermissions2 = Enums_S3.DefaultPermissions.Add)]
        public string Edit(int id, int entityId, int entityValueId, string content)
        {
            return Save(false, id, entityId, entityValueId, content);
        }

        public string Save(bool add, int id, int entityId, int entityValueId, string content)
        {
            CE.NoteEntity note = null;
            if (add)
            {
                note = new CE.NoteEntity()
                {
                    CreatedBy = LoggedInUser.UserId,
                    EntityId = entityId,
                    EntityValueId = entityValueId,
                    NoteObj = new CE.Note()
                    {
                        Createdby = LoggedInUser.UserId,
                        NoteContent = unEscape(content)
                    }
                };
            }
            else
            {
                note = this.iNoteEntityManager.FindById(id, null);
                this.LockEntity(Enums_S3.Entity.Note, note.NoteId);
                note.MarkModified();
                note.UpdatedBy = LoggedInUser.UserId;
                note.NoteObj.UpdatedBy = LoggedInUser.UserId;
                note.NoteObj.NoteContent = content;
                note.NoteObj.MarkModified();
            }

            List<SF.FrameworkEntity.ValidationError> errors = this.iNoteEntityManager.Validate(note);
            if (errors != null && errors.Count > 0)
            {
                return this.AttachStatusCode(errors, 0, "validation");
            }

            try
            {
                this.iNoteEntityManager.Save(note);
                if (!add)
                    this.UnLockEntity(Enums_S3.Entity.Note, note.NoteId);
                return this.AttachStatusCode(note.NoteEntityId, 1, null);
            }
            catch (Exception ex)
            {
                this.UnLockEntity(Enums_S3.Entity.Note, note.NoteId);
                return this.AttachStatusCode(note.NoteEntityId, 1, HandleException(ex));
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethod(Enums_S3.DefaultPermissions.Delete)]
        public string Delete(int id)
        {
            CE.NoteEntity noteEntity = iNoteEntityManager.FindById(id, null);
            this.LockEntityForDelete(Enums_S3.Entity.Note, noteEntity.NoteId);
            noteEntity.MarkDeleted();
            iNoteEntityManager.Save(noteEntity);
            this.UnLockEntity(Enums_S3.Entity.Note, noteEntity.NoteId);
            return this.AttachStatusCode(null, 0, null);
        }




    }
}
