﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using SF.Framework;
using CentrixERP.Configuration;

namespace CentrixERP.Common.API
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class EntityMethodAttribute : Attribute
    {
        private int? PermissionId;

        public Enums_S3.Entity Entity { set; get; }
        public Enums_S3.DefaultPermissions DefaultPermission { set; get; }
        public Enums_S3.ExtendedPermissions ExtendedPermission { set; get; }
        public bool DependableAction = false;
        public string DependableKey = "";

        public Enums_S3.DefaultPermissions ValidDefaultPermissions1 = Enums_S3.DefaultPermissions.None;
        public Enums_S3.ExtendedPermissions ValidExtendedPermissions1 = Enums_S3.ExtendedPermissions.None;
        public Enums_S3.DefaultPermissions ValidDefaultPermissions2 = Enums_S3.DefaultPermissions.None;
        public Enums_S3.ExtendedPermissions ValidExtendedPermissions2 = Enums_S3.ExtendedPermissions.None;

        private Dictionary<string, object> RequestDataDictionary;
        public JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
        public string RequestData { get; set; }

        public EntityMethodAttribute( Enums_S3.DefaultPermissions permission)
        {
            this.DefaultPermission = permission;
        }

        public EntityMethodAttribute( Enums_S3.ExtendedPermissions permission)
        {
            this.ExtendedPermission = permission;
        }

        public EntityMethodAttribute(bool dependableAction, string key)
        {
            DependableAction = dependableAction;
            DependableKey = key;
        }

        public int getPermissionId()
        {
            if (PermissionId != null)
                return PermissionId.Value;

            if (DependableAction)
            {
                //RequestData = HttpContext.Current.Request.Headers["request-data"].ToString();
               // LoadRequestData(RequestData);
                if (this.RequestDataDictionary != null && this.RequestDataDictionary[DependableKey] != null && this.RequestDataDictionary[DependableKey].ToString().ToNumber() > 0)
                {
                    if (this.ValidDefaultPermissions1 != Enums_S3.DefaultPermissions.None)
                        PermissionId = (int)this.ValidDefaultPermissions1;
                    else
                        PermissionId = (int)this.ValidExtendedPermissions1;
                }
                else
                {
                    if (this.ValidDefaultPermissions2 != Enums_S3.DefaultPermissions.None)
                        PermissionId = (int)this.ValidDefaultPermissions2;
                    else
                        PermissionId = (int)this.ValidExtendedPermissions2;
                }
            }
            else
            {
                if (DefaultPermission != Enums_S3.DefaultPermissions.None)
                    PermissionId = (int)DefaultPermission;

                else
                    PermissionId = (int)ExtendedPermission;
            }
            return PermissionId.Value;
        }

        public void LoadRequestData(string RequestData)
        {
            RequestDataDictionary = this.javaScriptSerializer.Deserialize<Dictionary<string, object>>(RequestData);
        }


    }

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class EntityAttribute : Attribute
    {
        public Enums_S3.Entity Entity { set; get; }

        public EntityAttribute(Enums_S3.Entity entity)
        {
            this.Entity = entity;
        }
    }
}