﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using CentrixERP.API.Common.BaseClasses;
using CentrixERP.Common.API;
using CentrixERP.Configuration;
using CentrixERP.Common.Business.Entity;
using Centrix.UM.Business.Entity;

namespace CentrixERP.API.Common.Configuration
{
    /// <summary>
    /// Summary description for ConfigurationWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    [EntityAttribute(Enums_S3.Entity.None)]
    public class ConfigurationWebService : WebServiceBaseClass
    {



        public ConfigurationWebService()
        {

        }

        //[WebMethod]
        //[EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        //public string GetSystemConfiguration()
        //{
        //    return this.GetConfigurations();
        //}



        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string checkUnAvailablePermissions(int EntityId, int ParentId)
        {
            return GetUserUnAvailablePermissions(EntityId, ParentId);
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string ResponseStatusCode()
        {
            return AttachStatusCode(null, -1, NoPermissionValue);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string Lock(int entityId, int? entityValueId, string timeStamp)
        {
            //DateTime dateTime = timeStamp.HasValue ? UnixTimeStampToDateTime(timeStamp.Value) : null;
            EntityLockCriteria criteria = new EntityLockCriteria()
            {
                EntityId = entityId,
                EntityValueId = entityValueId,
                LockedBy = LoggedInUser.UserId
            };

            List<EntityLock> lockedList = this.iEntityLockManager.FindAll(criteria);
            if (lockedList != null && lockedList.Count > 0)
            {

                var lockEntity = new
                {
                    UserName = lockedList[0].UserName,
                    LockedDate = lockedList[0].CreatedDate
                };

                return this.AttachStatusCode(lockEntity, 0, null);
            }
            else
            {
                EntityLock Locked = new EntityLock()
                {
                    CreatedBy = LoggedInUser.UserId,
                    EntityId = entityId,
                    EntityValueId = entityValueId != null ? entityValueId.Value : 0
                };
                if (!string.IsNullOrEmpty(timeStamp))
                {
                    try
                    {
                        double d = Convert.ToDouble(timeStamp);
                        DateTime dt = new DateTime(1970, 1, 1);
                        Locked.EntityLastUpdatedDate = dt.AddSeconds(d / 1000);

                        Locked.EntityLastUpdatedDate = Locked.EntityLastUpdatedDate.Value.AddMilliseconds(-Locked.EntityLastUpdatedDate.Value.Millisecond);
                    }
                    catch (Exception)
                    {

                    }

                    //Locked.EntityLastUpdatedDate = timeStamp.ToDate(true);
                }

                try
                {
                    this.iEntityLockManager.Save(Locked);
                    return this.AttachStatusCode(null, 1, null);
                }
                catch (SF.Framework.Exceptions.RecordNotAffected ex)
                {
                    return this.AttachStatusCode(null, 1, ex.Message);
                }
                catch (SF.Framework.Exceptions.EntityLockedDeleted ex)
                {
                    //return this.AttachStatusCode(null, 666, ex.Message);
                    EndResponse(Enums_S3.StatusCode.Codes.EntityLockedDeleted, ((int)Enums_S3.StatusCode.Codes.EntityLockedDeleted).ToString());
                    return null;
                }
                catch (SF.Framework.Exceptions.EntityLockedChanged ex)
                {
                    EndResponse(Enums_S3.StatusCode.Codes.EntityLockedChanged, ((int)Enums_S3.StatusCode.Codes.EntityLockedChanged).ToString());
                    //return this.AttachStatusCode(null, 1, null);
                    return null;
                }
                catch (Exception ex)
                {
                    return this.AttachStatusCode(null, 0, ex.Message);
                }
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string LockForDelete(int entityId, int? entityValueId)
        {
            EntityLockCriteria criteria = new EntityLockCriteria()
            {
                EntityId = entityId,
                EntityValueId = entityValueId,
                LockedBy = LoggedInUser.UserId,

            };

            List<EntityLock> lockedList = this.iEntityLockManager.FindAll(criteria);
            if (lockedList != null && lockedList.Count > 0)
            {

                var lockEntity = new
                {
                    UserName = lockedList[0].UserName,
                    LockedDate = lockedList[0].CreatedDate
                };

                return this.AttachStatusCode(lockEntity, 0, null);
            }
            else
            {
                EntityLock Locked = new EntityLock()
                {
                    CreatedBy = LoggedInUser.UserId,
                    EntityId = entityId,
                    EntityValueId = entityValueId.Value,
                    LockForDelete = true
                };

                try
                {
                    this.iEntityLockManager.Save(Locked);
                    return this.AttachStatusCode(null, 1, null);
                }
                catch (SF.Framework.Exceptions.RecordNotAffected ex)
                {
                    return this.AttachStatusCode(null, 1, ex.Message);
                }
                catch (SF.Framework.Exceptions.EntityLockedDeleted ex)
                {
                    EndResponse(Enums_S3.StatusCode.Codes.EntityLockedDeleted, ((int)Enums_S3.StatusCode.Codes.EntityLockedDeleted).ToString());
                    return null;
                }
                catch (SF.Framework.Exceptions.EntityLockedChanged ex)
                {
                    return this.AttachStatusCode(null, 1, ex.Message);
                    //EndResponse(Enums_S3.StatusCode.Codes.EntityLockedChanged, ((int)Enums_S3.StatusCode.Codes.EntityLockedChanged).ToString());
                    //return this.AttachStatusCode(null, 1, null);
                    //return null;
                }
                catch (Exception ex)
                {
                    throw ex;
                    //return this.AttachStatusCode(null, 0, ex.Message);
                }
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string UnLock(int entityId, int? entityValueId)
        {
            try
            {
                this.iEntityLockManager.UnLock(entityId, entityValueId);
                return this.AttachStatusCode(true, 1, null);
            }
            catch (SF.Framework.Exceptions.RecordNotAffected)
            {
                return this.AttachStatusCode(null, 1, null);
            }
            catch (Exception ex)
            {
                return this.AttachStatusCode(null, 0, HandleException(ex));
            }
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string FindAllLite(string keyword, int page, int resultCount, string argsCriteria)
        {
            EntityLockCriteria criteria = new EntityLockCriteria();
            if (!SF.Framework.String.IsEmpty(argsCriteria))
                criteria = this.javaScriptSerializer.Deserialize<EntityLockCriteria>(argsCriteria);
            List<EntityLockLite> entityLockList = iEntityLockManager.FindAllLite(criteria);
            if (entityLockList != null)
            {
                var result = from EntityLockLite item in entityLockList
                             group item by new
                             {
                                 item.EntityId,
                                 item.EntityName,
                                 item.EntityViewURL
                             } into entityList
                             select new
                             {
                                 EntityId = entityList.Key.EntityId,
                                 EntityName = entityList.Key.EntityName,
                                 EntityViewURL = entityList.Key.EntityViewURL,
                                 EntityLock = from EntityLockLite itemLock in entityLockList
                                              where itemLock.EntityId == entityList.Key.EntityId
                                              select new
                                              {
                                                  UserName = itemLock.UserName,
                                                  EntityValueId = itemLock.EntityValueId,
                                                  Date = itemLock.CreatedDate
                                              }
                             };
                return this.AttachStatusCode(result, 1, "");
            }
            return this.AttachStatusCode(null, 1, "");
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EntityMethodAttribute(Enums_S3.DefaultPermissions.None)]
        public string UnLockAll(int userId)
        {
            try
            {
                this.iEntityLockManager.UnLock(userId);
                return this.AttachStatusCode(true, 1, null);
            }
            catch (SF.Framework.Exceptions.RecordNotAffected)
            {
                return this.AttachStatusCode(null, 1, null);
            }
            catch (Exception ex)
            {
                return this.AttachStatusCode(null, 0, HandleException(ex));
            }
        }

        public DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }
      
        [WebMethod]
        [EntityMethod(Enums_S3.DefaultPermissions.None)]
        public string CheckReportPermission(int reportPermissionId)
        {
            IEnumerable<bool> hasPermission = from RolePermissionLite rolePerm in RolePermissionsList where (rolePerm.EntityId == reportPermissionId && rolePerm.HasPermission == true) select true;
            if (hasPermission.Count() > 0 && hasPermission.First())
                return this.AttachStatusCode(true, 1, null);
            else return this.AttachStatusCode(false, 1, null); ;
        }

        [WebMethod]
        [EntityMethod(Enums_S3.DefaultPermissions.None)]
        public string CheckUserPassword(string password)
        {
            return this.AttachStatusCode(CheckUserLogin(password), 1, null);
        }

    }
}
