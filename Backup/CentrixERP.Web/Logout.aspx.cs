using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CentrixERP.Configuration;
using Centrix.UM.Business.IManager;
using SF.Framework;
using Centrix.UM.Business.Entity;
using CentrixERP.Common.Business.IManager;

namespace CentrixERP.Common.Web.NewCommon.Common
{
    public partial class Logout : BasePageLoggedIn
    {
        public Logout()
            : base(Enums_S3.Permissions.SystemModules.None) { }

        protected void Page_Load(object sender, EventArgs e)
        {
            IUserLogginManager userLogginManager = IoC.Instance.Resolve<IUserLogginManager>();

            //UserLogginCriteria criteria = new UserLogginCriteria()
            //{
            //    UserAgent = Request.UserAgent,
            //    PublicIP = Request.UserHostAddress,
            //    LocalIP = Request.UserHostName
            //};

            IEntityLockManager iEntityLockManager = IoC.Instance.Resolve<IEntityLockManager>();
            iEntityLockManager.UnLock(LoggedInUser.UserId);

            userLogginManager.KillUserSession(LoggedInUser.UserId);
            int sessionexpired = Request.QueryString["sexp"].ToNumber();
            //if (sessionexpired == 1)
            //    Response.Redirect("~/Login.aspx?sexp=1");
            //else
                Response.Redirect("~/Login.aspx");
            //Cookies.Abandon("uid");
            //Response.Redirect("~/Login.aspx");

            //Response.Write("<script>window.top.location.href = '" + URLs.GetURL(Enums_S3.Configuration.Module.Common, URLEnums.Action.Extended.Login, null) + "';</script>");
        }
    }
}