
var CustomFildServiceURL;
var CustomFildViewItemTemplate;
 
 var ItemListTemplate;
 var AddItemTemplate;
var addCustomFildMode = false;

var FildTypeAC=null ,ValidationAC=null,ActionAC =null;



function InnerPage_Load() {
    setSelectedMenuItem('.pos-icon');
    CustomFildServiceURL = systemConfig.ApplicationURL_Common + systemConfig.CustomFildServiceURL;
    currentEntity = systemEntities.CustomFildEntityId;

    //Advanced search Data
    //        mKey = 'CustomFild';//replace it with entity modual key
    //        InnerPage_Load2();
    //        $('.QuickSearch-holder').show();
    //        LoadSearchForm2(mKey, false);
    //Advanced search Data


    setPageTitle(getResource("CustomFild"));
    setAddLinkAction(getResource("AddCustomFild", 'add-new-item-large'));
    GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.AddCustomFildTemplate, null, 'AddItemTemplate');
    GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.CustomFildListItemTemplate, loadData, 'ItemListTemplate');
    
    $('.result-block').live("click", function () {
        CustomFildId = $(this).parent().attr('id');
    });

    deletedEntityCallBack = function () {
        removeDeletedEntity();
    };

    updatedEntityCallBack = function () {
        getEntityData(selectedEntityId);
    };
    
$('li:.Communication-tab').live('click', function () {
            checkPermissions(this, systemEntities.TaskId, -1, $('.Action-div'), function (template) {
                getCommunication(selectedEntityId, systemEntities.CustomFildEntityId, selectedEntityId, CustomFildName);
            });
        });$('li:.note-tabs').live('click', function () {
            NoteTemplate = $(NoteTemplate);
            checkPermissions(this, systemEntities.NoteEntityId, -1, NoteTemplate, function (template) {
                getNotes(systemEntities.CustomFildEntityId,selectedEntityId);
            });
        });$('li:.Attach-tabs').live('click', function () {

            checkPermissions(this, systemEntities.AttachmentsEntityId, -1, $('.Action-div'), function (template) {
                getAttachments(systemEntities.CustomFildEntityId,selectedEntityId, 'CustomFild', systemEntities.CustomFildEntityId);
            });
        });

        


}

function loadData(data) {

    GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.CustomFildListViewItemTemplate, function () {
    checkPermissions(this, systemEntities.CustomFildEntityId, -1, CustomFildViewItemTemplate, function(returnedData) {
            CustomFildViewItemTemplate = returnedData;
            loadEntityServiceURL = CustomFildServiceURL + "FindByIdLite";
            entityItemListTemplate = data;
            entityItemViewTemplate = returnedData;
            findAllEntityServiceURL = CustomFildServiceURL + "FindAllLite";
            deleteEntityServiceURL = CustomFildServiceURL + "Delete";
            selectEntityCallBack = getCustomFildInfo;
            loadDefaultEntityOptions();
        });
    }, 'CustomFildViewItemTemplate');

}

function initControls(template, isAdd) {
    if (!template) template = $('.TabbedPanels');

    if (!isAdd)
        $('.historyUpdated').append(HistoryUpdatedListViewTemplate.clone());

        

    LoadDatePicker(template.find('input:.date'), 'dd-mm-yy');
    template.find('.numeric').autoNumeric();
    preventInputChars(template.find('input:.integer'));
        FildTypeAC = fillDataTypeContentList($(template).find('select:.FildType'), systemConfig.dataTypes.FieldType, true, false);
        ValidationAC = fillDataTypeContentList($(template).find('select:.Validation'), systemConfig.dataTypes.Validation, true, false);
        ActionAC = fillDataTypeContentList($(template).find('select:.Action'),systemConfig.dataTypes.Action, true, false);
         FildTypeAC.onChange(function () {
       if(FildTypeAC.get_Item().value==systemConfig.AutoCompleteStatus)
        $('.AutoCompleteField').show();
        else
         $('.AutoCompleteField').hide();
    })    ;   
    
  }

function initAddControls(template) {
    addCustomFildMode = true;
    initControls(template, true);
}

function getCustomFildInfo(item) {
   
       if(item.IsRequired)
       {
       $('input.IsRequired').prop( "checked", true )   ;
       }

    if ($('.result-block').length > 0 && (getQSKey('fdb') > 0) && isFirstTimeAdd) {
        $('.add-entity-button').first().click();
       }

  }

function setListItemTemplateValue(entityTemplate, entityItem) {
entityTemplate.find('.result-data1 label').text(Truncate(entityItem.FildName, 15));
                 entityTemplate.find('.result-data2 label').text(Truncate(entityItem.FildType, 15));
                 entityTemplate.find('.result-data3 label').text(Truncate(entityItem.IsRequired, 15));
     return entityTemplate;
   }

 function callBackAfterList() {
     if ($('.result-block').length <= 0 && (getQSKey('fdb') > 0) && isFirstTimeAdd) {
         {
             $('.add-entity-button').first().click();
         }
     }
 }


 function setListItemServiceData(keyword, pageNumber, resultCount) {
     if (!QuickSearch) {
         return formatString('{ keyword: "{0}", page:{1}, resultCount:{2} ,argsCriteria:"{ CustomFildId:{3}}"}', keyword, pageNumber, resultCount, filterEntityId);
     }
     else {
         return formatString('{mKey:"{0}",SearchCriteriaObj:"{1}",SortType:{2},SortFields:"{3}",TotalRecords:{4},PageNumber:{5}}', mKey, SearchCriteriaObj, SortType, SortFields, resultCount, pageNumber);
     }
 }



 function SaveEntity(isAdd, template) {
     $('.validation').remove();
     var controlTemplate;
     var validForm = true;  

     if (isAdd)
         controlTemplate = $('.add-CustomFild-info');
     else
         controlTemplate = $('.CustomFild-info');
     

     if (!saveForm(controlTemplate))
         validForm = false;

     if (!validForm) {
         return false;
         hideLoading();
     }

     addCustomFildMode = isAdd;
     var CustomFildId = -1;
     var lastUpdatedDate = null;

     if (addCustomFildMode) {
         showLoading('.add-new-item');
     }
     else {
         CustomFildId = selectedEntityId;
         showLoading('.white-container-data');
         lastUpdatedDate = getEntityLockDateFormat(currentEntityLastUpdatedDate);
     }

   
     var CustomFildObj = {

FildName:controlTemplate.find('input.FildName').val(),
FildTypeId:FildTypeAC.get_ItemValue(),
IsRequired:controlTemplate.find('input:.IsRequired').is(':checked'),
ValidationId:ValidationAC.get_ItemValue(),
ActionId:ActionAC.get_ItemValue(),
AutocompleteValues: controlTemplate.find('textarea.AutoCompleteField').val(),
}      ;
     
     var data = formatString('{id:{0}, CustomFildInfo:"{1}"}', selectedEntityId, escape(JSON.stringify(CustomFildObj)));

     if (CustomFildId > 0)
         url = CustomFildServiceURL + "Edit";
     else
         url = CustomFildServiceURL + "Add";

     post(url, data, saveSuccess, saveError, getEntitylockAjaxHeaders(lastUpdatedDate));
 }

 function saveSuccess(returnedValue) {
 debugger;
     if (returnedValue.statusCode.Code == 501) {
         
         hideLoading();
         return false;
     }
     else if (returnedValue.statusCode.Code == 0) {
         hideLoading();
         if (returnedValue.statusCode.message == 'validation' && returnedValue.result) {
             filterError(returnedValue.result);
         }
         showStatusMsg(getResource("FaildToSave"));
         return false;
     }
     if (!addCustomFildMode) {
         showStatusMsg(getResource("savedsuccessfully"));
         hideLoading();
         loadEntityData(loadEntityServiceURL, entityItemViewTemplate, selectedEntityId, getCustomFildInfo);

     }
     else {
         hideLoading();
         addCustomFildMode = false;
         $('.add-new-item').hide();
         $('.add-new-item').empty();
         appendNewListRow(returnedValue.result);
         showStatusMsg(getResource("savedsuccessfully"));
     }
 }

 function saveError(ex) {
     hideLoading();
     handleLockEntityError(ex, deletedEntityCallBack, updatedEntityCallBack);
 }
 


 function filterError(errors) {
     var allMsg = '';
     $('.validation').remove();
     $.each(errors, function (index, error) {
         var template = $('.CustomFild-info');
         if (error.GlobalMessage) {
             allMsg += '<span>' + error.Message + '<br/></span>';
         }
        else if (error.ControlName == 'FildName') {
addPopupMessage(template.find('input.FildName'),getResource(error.ResourceKey));
}
else if (error.ControlName == 'FildType') {
addPopupMessage(template.find('input.FildType'),getResource(error.ResourceKey));
}
else if (error.ControlName == 'IsRequired') {
addPopupMessage(template.find('input.IsRequired'),getResource(error.ResourceKey));
}
else if (error.ControlName == 'Validation') {
addPopupMessage(template.find('input.Validation'),getResource(error.ResourceKey));
}
else if (error.ControlName == 'Action') {
addPopupMessage(template.find('input.Action'),getResource(error.ResourceKey));
}
else if (error.ControlName == 'CreartedDate') {
addPopupMessage(template.find('input.CreartedDate'),getResource(error.ResourceKey));
}
else if (error.ControlName == 'IsSysetm') {
addPopupMessage(template.find('input.IsSysetm'),getResource(error.ResourceKey));
}

     });

     hideLoading();
     if (allMsg.trim()) {
         showOkayMsg('Validation error', allMsg.trim());
     }
   }



 
