﻿

function InnerPage_Load() {

    // setPageTitle(getResource("ERPIntegration"));
    $('.Top-Mid-section').remove();
    setSelectedMenuItem($('.pos-icon'));

    $('.btn-inv-post').click(function () {
        
        showConfirmMsg("Post Invoice", "Are You Sure ?", function () {

            StartERPIntegration('1,7');
        }, null);

    });

    $('.btn-refund-post').click(function () {
        showConfirmMsg("Post Refund", "Are You Sure ?", function () {
            StartERPIntegration(POSInvoiceType.Refund);
        }, null);

    });

    $('.btn-onaccount-post').click(function () {
        showConfirmMsg("Post On Account", "Are You Sure ?", function () {
            StartERPIntegration(POSInvoiceType.OnAccount);
        }, null);

    });

    $('.btn-exchange-post').click(function () {
        showConfirmMsg("Post Exchange", "Are You Sure ?", function () {

            StartERPIntegration(POSInvoiceType.Exchange);
        }, null);

    });
}


function StartERPIntegration(invType) {
    showLoading($('.Main-Div'));
    post(systemConfig.ApplicationURL + systemConfig.POSPostInvoiceToShipmentServiceURL + "PostPOSInvoices", formatString("{invType:'{0}'}", invType), function (retuned) {
        hideLoading();
        if (retuned.statusCode.Code == 2) {
            showOkayMsg(getResource("SystemAlert"), getResource("NoInvoiceToPost"));
            //   showStatusMsg(getResource("NoInvoiceToPost"));
            return;
        }
        else if (retuned.statusCode.Code == 5) { //Case shipmentReturn with no shipment
            showOkayMsg(getResource("SystemAlert"), getResource("ParentInvoiceNotPostedYet"));
            return;
        }
        if (retuned.statusCode.Code == 1) {
            // showOkayMsg(getResource("SystemAlert"), getResource("postedsuccessfully"));

            // showStatusMsg(getResource("postedsuccessfully"));
            var result = retuned.result;
            if (result != null) {

                var resultText = getResource("POSShipmentPostResult");
                var shipNumbers = '';
                $.each(result, function (indx, item) {
                    shipNumbers += (item.TransReff + "<br>");
                });
                resultText = resultText + shipNumbers;
                showOkayMsg(getResource("Completed"), resultText);
            }
        }
        else {
            showOkayMsg(getResource("SystemAlert"), getResource("FaildToPost"));

            // showStatusMsg(getResource("FaildToPost")); 

        }

    }, null
    , null);
}


var POSInvoiceType = {
    Invoice: 1,
    Refund: 5,
    OnAccount: 7,
    Exchange: 10
};