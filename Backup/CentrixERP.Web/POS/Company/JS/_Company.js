var CompanyServiceURL;
var CompanyViewItemTemplate, CompanyPersonsTemplate, CompanyRelatedOpportunities, RelatedGiveawaysTemplate, RelatedCaseTemplate, RelatedOpportunityTemplate;
var CompanyTypeAC, DecisionMakerAC, SalesManAccountManagerAC, SegmentAC, SourceAC, YearlyRevenueAC, EmployeeNumberAC, StatusAC, ParentCompanyAC, PaymentTermsAC,
PaymentTypeAC, CountryAC, CustomerReferralCompanyAC, CustomerReferralPersonAC, EmployeeUserAC, CountryOfOrigionAC;
var ItemListTemplate;
var addCompanyMode = false;
var CompanyId = -1, CustomerReferralCompanyId = -1;
var CompanyName ='';
var InterestedInAC;

function InnerPage_Load() {
    setSelectedMenuItem('.Company-icon', '.TabLinks-Company');
   // entityKey = 'companyid';
    CompanyServiceURL = systemConfig.ApplicationURL_Common + systemConfig.CompanyServiceURL;
    currentEntity = systemEntities.CompanyEntityId;
    if (addCompanyMode) {
        setPageTitle(getResource("AddCompany"));
        LoadAddEntityDefaultOptions(systemConfig.ApplicationURL_Common + systemConfig.pageURLs.CompanyList);
        $('.save-tab-lnk').show();
        $('.cancel-tab-lnk').show();
        $('.search-box').hide();
    }
    else {
        // Advanced search Data
        mKey = 'comp';
        InnerPage_Load2();
        $('.QuickSearch-holder').show();
        // Advanced search Data


        setPageTitle(getResource("Company"));
        setAddLinkURL(systemConfig.ApplicationURL + systemConfig.pageURLs.AddCompany, getResource("AddCompany"));
        GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.CompanyListItemTemplate, loadData, 'ItemListTemplate');
        GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.CompanyPersonsListViewItemTemplate, null, 'CompanyPersonsTemplate')
        GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.RelatedGiveawaysListViewItemTemplate, null, 'RelatedGiveawaysTemplate')
        GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.RelatedCaseListViewItemTemplate, null, 'RelatedCaseTemplate')
        GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.RelatedOpportunityListViewItemTemplate, null, 'RelatedOpportunityTemplate')
        //        GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.RelatedOpportunities, null, 'CompanyRelatedOpportunities');

        $('.result-block').live("click", function () {
            CompanyId = $(this).parent().attr('id');
        });

        deletedEntityCallBack = function () {
            removeDeletedEntity();
        };

        updatedEntityCallBack = function () {
            getEntityData(selectedEntityId);
        };


        $('li:.email-tabs').live('click', function () {
            EmailTemplate = $(EmailTemplate);
            checkPermissions(this, systemEntities.EmailEntityId, -1, EmailTemplate, function (template) {
                getEmails(systemEntities.CompanyEntityId, selectedEntityId);
            });
        });
        $('li:.note-tabs').live('click', function () {
            NoteTemplate = $(NoteTemplate);
            checkPermissions(this, systemEntities.NoteEntityId, -1, NoteTemplate, function (template) {
                getNotes(systemEntities.CompanyEntityId, selectedEntityId);
            });
        });
        $('li:.address-tabs').live('click', function () {
            AddressTemplate = $(AddressTemplate);
            checkPermissions(this, systemEntities.AddressEntityId, -1, AddressTemplate, function (template) {
                getAddresses(systemEntities.CompanyEntityId, selectedEntityId);
            });
        });
        $('li:.phone-tabs').live('click', function () {
            PhoneTemplate = $(PhoneTemplate);
            checkPermissions(this, systemEntities.PhoneEntityId, -1, PhoneTemplate, function (template) {
                getPhones(systemEntities.CompanyEntityId, selectedEntityId);
            });
        });
        $('li:.Attach-tabs').live('click', function () {
            checkPermissions(this, systemEntities.AttachmentsEntityId, -1, $('.Action-div'), function (template) {
                getAttachments(systemEntities.CompanyEntityId, selectedEntityId, 'Company', systemEntities.CompanyEntityId);
            });
        });

        $('li:.Communication-tab').live('click', function () {
            checkPermissions(this, systemEntities.TaskId, -1, $('.Action-div'), function (template) {
                var companyItem = { label: CompanyName, value: selectedEntityId };
                getCommunication(selectedEntityId, systemEntities.CompanyEntityId, selectedEntityId, CompanyName, companyItem);
            });
        });


        $('li:.companyPersons-tabs').live('click', function () {
            checkPermissions(this, systemEntities.PersonEntityId, -1, CompanyPersonsTemplate, function (template) {
                CompanyPersonsTemplate = template;
                getCompanyPersons(selectedEntityId);
            });
        });
        $('li:.Giveaways-tabs').live('click', function () {
        
            checkPermissions(this, systemEntities.GiveawaysEntityId, -1, RelatedGiveawaysTemplate, function (template) {
                RelatedGiveawaysTemplate = template;
                getRelatedGiveAways(selectedEntityId);
            });
        });
        $('li:.Cases-tabs').live('click', function () {

            checkPermissions(this, systemEntities.ClientCareEntityId, -1, RelatedCaseTemplate, function (template) {
                RelatedCaseTemplate = template;
                getRelatedCase(selectedEntityId);
            });
        });

        $('li:.Opportunity-tabs').live('click', function () {

            checkPermissions(this, systemEntities.OpportunityEntityId, -1, RelatedOpportunityTemplate, function (template) {
                RelatedOpportunityTemplate = template;
                getRelatedOpportunities(selectedEntityId);
            });
        });

        $('li:.SocialMedia-tabs').live('click', function () {

            SocialMediaTemplate = $(SocialMediaTemplate);
            checkPermissions(this, systemEntities.SocialMediaEntityID, -1, SocialMediaTemplate, function (template) {
                getSocialMedia(systemEntities.CompanyEntityId, selectedEntityId);
            });
        });

       // $('li:.ProjectsOpporunitiesTabs').live('click', function () { });
        //$('li:.Cases-tabs').live('click', function () { });
       // $('li:.Appliances-tab').live('click', function () { });
    }
}

function loadData(data) {

    GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.CompanyListViewItemTemplate, function () {

        checkPermissions(this, systemEntities.SendMassMail, -1, CompanyViewItemTemplate, function (returnedData) {
           
        });
        checkPermissions(this, systemEntities.CompanyEntityId, -1, CompanyViewItemTemplate, function (returnedData) {
          
            CompanyViewItemTemplate = returnedData;
            loadEntityServiceURL = CompanyServiceURL + "FindByIdLite";
            entityItemListTemplate = data;
            entityItemViewTemplate = returnedData;
            findAllEntityServiceURL = CompanyServiceURL + "FindAllLite";
            deleteEntityServiceURL = CompanyServiceURL + "Delete";
            selectEntityCallBack = getCompanyInfo;
            loadDefaultEntityOptions();
        });
    }, 'CompanyViewItemTemplate');

}

function initControls() {
//    $('.country-num').mask('999999');
//    $('.city-num').mask('999999');
    //    $('.phone-num').mask('99999999');
    SetPhoneMask($('.phone-format'));
    if (!addCompanyMode)
        $('.historyUpdated').append(HistoryUpdatedListViewTemplate.clone());
    else
        CountryAC = $('select:.Country').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.CountryServiceURL + "FindAll", required: false, autoLoad: false});
    CountryOfOrigionAC = $('select:.CountryOfOrigion').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.CountryServiceURL + "FindAll", required: false, autoLoad: false });
    LoadDatePicker('input:.date', 'dd-mm-yy');
    preventInputChars('input:.integer');
    CompanyTypeAC = fillDataTypeContentList('select:.CompanyType', systemConfig.dataTypes.ContactType, false, false);
    DecisionMakerAC = $('select:.DecisionMaker').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.PersonsServiceURL + 'FindAllLite' });
    DecisionMakerAC.set_Args(formatString('{ CompanyId:{0}}', CompanyId));
    SalesManAccountManagerAC = $('select:.SalesManAccountManager').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.UsersWebService + 'FindUserAllLite' });
    SegmentAC = fillDataTypeContentList('select:.Segment', systemConfig.dataTypes.Segment, true, false);
    SourceAC = fillDataTypeContentList('select:.Source', systemConfig.dataTypes.Source, true, false);
    YearlyRevenueAC = fillDataTypeContentList('select:.YearlyRevenue', systemConfig.dataTypes.YearlyRevenue, false, false);
    EmployeeNumberAC = fillDataTypeContentList('select:.EmployeeNumber', systemConfig.dataTypes.EmployeeNumber, false, false);
    StatusAC = fillDataTypeContentList('select:.Status', systemConfig.dataTypes.CompanyStatus, false, false);
    InterestedInAC = fillDataTypeContentList('select:.InterestedIn', systemConfig.dataTypes.InterestedIn, false, true);

    if (addCompanyMode) {
        var Label = getResource('Customer');
        CompanyTypeAC.set_Item({ label: Label, value: 690 });
        //DecisionMakerAC.Disabled(true);
        DecisionMakerAC.set_Args(formatString('{ CompanyId:{0}}', 90000));
//        var StatusLabel = getResource('Active');
//        StatusAC.set_Item({ label: StatusLabel, value: 533 });

    }

}
function getPhoneFormat(obj) {


    var MCountryCode;
    if (obj.CountryMobileNumber != null && obj.CountryMobileNumber != "") {
        if (obj.CountryMobileNumber.substring(0, 2) == '00')
            MCountryCode = "(" + obj.CountryMobileNumber + ")";
        else
            MCountryCode = "+(" + obj.CountryMobileNumber + ")";
    }
    else
        MCountryCode = "";

    //var MCountryCode = obj.CountryMobileNumber != null && obj.CountryMobileNumber != "" ? "+(" + obj.CountryMobileNumber + ")" : "";
    var MAreaCode = obj.AreaMobileNumber != null && obj.AreaMobileNumber != "" ? obj.AreaMobileNumber + "-" : "";
    var MFullPhoneNumber = obj.MobileNumber != null ? obj.MobileNumber : "";
    var FullPhoneNumber = MCountryCode + MAreaCode + MFullPhoneNumber;
    $('.MobileNumber').text(FullPhoneNumber);


    var HCountryCode;
    if (obj.CountryHomeNumber != null && obj.CountryHomeNumber != "") {
        if (obj.CountryHomeNumber.substring(0, 2) == '00')
            HCountryCode = "(" + obj.CountryHomeNumber + ")";
        else
            HCountryCode = "+(" + obj.CountryHomeNumber + ")";
    }
    else
        HCountryCode = "";
    //var HCountryCode = obj.CountryHomeNumber != null && obj.CountryHomeNumber != "" ? "+(" + obj.CountryHomeNumber + ")" : "";
    var HAreaCode = obj.AreaHomeNumber != null && obj.AreaHomeNumber != "" ? obj.AreaHomeNumber + "-" : "";
    var HFullPhoneNumber = obj.HomeNumber != null ? obj.HomeNumber : "";
    var FullPhoneNumber = HCountryCode + HAreaCode + HFullPhoneNumber;
    $('.HomeNumber').text(FullPhoneNumber);

    var FCountryCode;
    if (obj.CountryFaxNumber != null && obj.CountryFaxNumber != "") {
        if (obj.CountryFaxNumber.substring(0, 2) == '00')
            FCountryCode = "(" + obj.CountryFaxNumber + ")";
        else
            FCountryCode = "+(" + obj.CountryFaxNumber + ")";
    }
    else
        FCountryCode = "";
   // var FCountryCode = obj.CountryFaxNumber != null && obj.CountryFaxNumber != "" ? "+(" + obj.CountryFaxNumber + ")" : "";
    var FAreaCode = obj.AreaFaxNumber != null && obj.AreaFaxNumber != "" ? obj.AreaFaxNumber + "-" : "";
    var FFullPhoneNumber =  obj.FaxNumber != null ? obj.FaxNumber : "";
    var FullPhoneNumber = FCountryCode + FAreaCode + FFullPhoneNumber;
    $('.FaxNumber').text(FullPhoneNumber);

}
function getCompanyInfo(obj) {
   
    if (obj != null) {

        $('.Business-email-to').attr('href', 'mailto:' + obj.BusinessEmail);
        $('.Personal-email-to').attr('href', 'mailto:' + obj.PersonalEmail);
        getPhoneFormat(obj);

        CompanyName = obj.CompanyNameEnglish;

        CompanyId = obj.CompanyId;
       
        CustomerReferralCompanyId = obj.CustomerReferralCompanyId;
        if (obj.IntresetedInList != null) {
            InterestedInAC.set_Items(obj.IntresetedInList);
            $.each(obj.IntresetedInList, function (index, item) {

                var label = (Lang == systemConfig.WebKeys.Ar) ? item.DataTypeContentAr : item.DataTypeContent;
                $('.InterestedIn').text($('.InterestedIn').html() + label + ',');
            });
            $('.InterestedIn').text($('.InterestedIn').html().slice(0, $('.InterestedIn').html().length - 1));
        }
        if (obj.CompanytypesList != null) {
     
            CompanyTypeAC.set_Item(obj.CompanytypesList[0]);
            $.each(obj.CompanytypesList, function (index, item) {
                var label = (Lang == systemConfig.WebKeys.Ar) ? item.DataTypeContentAr : item.DataTypeContent;
                $('.CompanyTypeList').text($('.CompanyTypeList').html() + label);

            });

        }
        
        $('a:.add-new-persons').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.AddPerson + '?cId=' + selectedEntityId);
        //$('.sendEmail-link').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.SendNotifications + '?cid=' + selectedEntityId);
        $('.sendEmail-link').click(function () {
            window.location = systemConfig.ApplicationURL + systemConfig.pageURLs.SendNotifications + '?cid=' + selectedEntityId;
        });
        
        if (obj.Website.substring(0, 7) == 'http://' || obj.Website.substring(0, 8) == 'https://')
            $('a:.Website-link').attr('href', obj.Website);
        else
            $('a:.Website-link').attr('href', 'http://' + obj.Website);
            //xxxxxxx

        $('.Website').text(obj.Website);
        $('a:.DecisionMaker-link').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.PersonList + "?key=" + obj.DecisionMakerId);
        $('a:.SalesManAccountManager-link').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.UserList + "?key=" + obj.SalesManAccountManagerId);
        
    }
}


function setListItemTemplateValue(entityTemplate, entityItem) {
    
    $('.HeaderBusinessEmail').text(Truncate(entityItem.BusinessEmail, 10));
   // $('.HeaderBusinessEmail').text('');
    var countryCode = entityItem.CountryMobileNumber != null && entityItem.CountryMobileNumber != "" ? "+(" + entityItem.CountryMobileNumber + ")" : "";
    var areaCode = entityItem.AreaMobileNumber != null && entityItem.AreaMobileNumber != "" ? entityItem.AreaMobileNumber + "-" : "";
    var PhoneNumber = entityItem.MobileNumber != null && entityItem.MobileNumber != "" ? entityItem.MobileNumber : "";
    var FullPhoneNumber = countryCode + areaCode + PhoneNumber;

    var HCountryCode;
    if (entityItem.CountryHomeNumber != null && entityItem.CountryHomeNumber != "") {
        if (entityItem.CountryHomeNumber.substring(0, 2) == '00')
            HCountryCode = "(" + entityItem.CountryHomeNumber + ")";
        else
            HCountryCode = "+(" + entityItem.CountryHomeNumber + ")";
    }
    else
        HCountryCode = "";
    //var HCountryCode = obj.CountryHomeNumber != null && obj.CountryHomeNumber != "" ? "+(" + obj.CountryHomeNumber + ")" : "";
    var HAreaCode = entityItem.AreaHomeNumber != null && entityItem.AreaHomeNumber != "" ? entityItem.AreaHomeNumber + "-" : "";
    var HFullPhoneNumber = entityItem.HomeNumber != null ? entityItem.HomeNumber : "";
    var FullPhoneNumber = HCountryCode + HAreaCode + HFullPhoneNumber;
    $('.HeaderLandLine').text(FullPhoneNumber);





  
    CompanyName = entityItem.CompanyNameEnglish;
       entityTemplate.find('.result-data1 .CompanyNameTrim').text(Truncate(CompanyName, 30));
      entityTemplate.find('.result-data2 .WebsiteTrim').text(Truncate(entityItem.Website, 30));

      entityTemplate.find('.result-data3 label').text((entityItem.BusinessEmail != null) ? ' ' + Truncate(entityItem.BusinessEmail, 30) : '');
      var BEmail=entityItem.BusinessEmail != null ? entityItem.BusinessEmail : '';
      entityTemplate.find('.result-data3 .HBusiness-email-to').attr('href', 'mailto:' + BEmail);
      entityTemplate.find('.result-data4 label').text(FullPhoneNumber);
     
      $('.HeaderCompanyName').text(Truncate(CompanyName, 30));
      //$('.HeaderWebsite').text(Truncate(entityItem.Website, 30));
      //$('.HeaderBusinessEmail').text(Truncate(entityItem.BusinessEmail, 30));
      //$('.HeaderLandLine').text(Truncate(entityItem.BusinessEmail, 30));
      if (entityItem.Website.substring(0, 7) == 'http://' || entityItem.Website.substring(0, 8) == 'https://')
          $('a:.Website-link').attr('href', entityItem.Website);
      else
          $('a:.Website-link').attr('href', 'http://' + entityItem.Website);


      return entityTemplate;
  }

  function setListItemServiceData(keyword, pageNumber, resultCount) {
     if (!QuickSearch) {

         return formatString('{ keyword: "{0}", page:{1}, resultCount:{2} ,argsCriteria:"{ CompanyId:{3}}"}', keyword, pageNumber, resultCount, filterEntityId);
     }
     else {
         return formatString('{mKey:"{0}",SearchCriteriaObj:"{1}",SortType:{2},SortFields:"{3}",TotalRecords:{4},PageNumber:{5}}', mKey, SearchCriteriaObj, SortType, SortFields, resultCount, pageNumber);
     }
 }



 function SaveEntity() {

     var validForm = true;

     if (!saveForm($('.Company-info')))
         validForm = false;

     if (CompanyTypeAC.get_Item() == null || CompanyTypeAC.get_Item() == '') {
         addPopupMessage($('select:.CompanyType').parent().find('.ui-combobox'), getResource("Required"));
         validForm = false;
     }

     if (addCompanyMode) {
        
     }

   

     if (!validForm)
         return false;
   
     var CompanyId = selectedEntityId;
     var lastUpdatedDate = null;

     if (addCompanyMode) {
         showLoading($('#demo1'));
     }
     else {
         showLoading($('.TabbedPanels'));
         lastUpdatedDate = getEntityLockDateFormat(currentEntityLastUpdatedDate);
     }
     
     var CompanyObj = {
         CompanyNameEnglish: $('input.CompanyNameEnglish').val().trim(),
         CompanyNameArabic: $('input.CompanyNameArabic').val(),
         Website: $('input.Website').val(),
         CompanyType: CompanyTypeAC.get_ItemValue(),
         DecisionMaker: DecisionMakerAC.get_ItemValue(),
         SalesManAccountManager: SalesManAccountManagerAC.get_ItemValue(),
         Abbreviation: $('input.Abbreviation').val(),
         RegistrationNumber: $('input.RegistrationNumber').val(),
         RegistrationDate: getDotNetDateFormat($('input.RegistrationDate').val()),
         Segment: SegmentAC.get_ItemValue(),
         Source: SourceAC.get_ItemValue(),
         YearlyRevenue: YearlyRevenueAC.get_ItemValue(),
         EmployeeNumber: EmployeeNumberAC.get_ItemValue(),
         Status: StatusAC.get_ItemValue(),
         CountryOfOrigion: CountryOfOrigionAC.get_ItemValue(),
         FacebookPage: $('input.FacebookPage').val(),
         LinkedInPage: $('input.LinkedInPage').val(),
         TwitterAccount: $('input.TwitterAccount').val(),
         InterestedIn: InterestedInAC.get_ItemsIds()
     };
     
     var data = formatString('{id:{0}, CompanyInfo:"{1}"}', selectedEntityId, escape(JSON.stringify(CompanyObj)));
  
     if (CompanyId > 0)
         url = CompanyServiceURL + "Edit";
     else
         url = CompanyServiceURL + "Add";

     post(url, data,  saveSuccess , saveError, getEntitylockAjaxHeaders(lastUpdatedDate));
 }

 function saveSuccess(returnedValue) {

     if (returnedValue.statusCode.Code == 501) {
         hideLoading();
         addPopupMessage($('input.CompanyNameEnglish'), getResource("DublicatatedName"));
         addPopupMessage($('input.CompanyNameArabic'), getResource("DublicatatedName"));
         return false;
     }
     else if (returnedValue.statusCode.Code == 0) {
         hideLoading();
         if (returnedValue.statusCode.message == 'validation' && returnedValue.result) {
             filterError(returnedValue.result);
         }
         showStatusMsg(getResource("FaildToSave"));
         return false;
     }
  
     CompanyId = returnedValue.result;
     if (!addCompanyMode) {

         showStatusMsg(getResource("savedsuccessfully"));
         hideLoading();
         loadEntityData(loadEntityServiceURL, entityItemViewTemplate, selectedEntityId, getCompanyInfo);
         //setListItemTemplateValue(selectedEntityItemList, obj);

     }
     else {
        saveCompanyContactInfo(CompanyId, CountryAC);
     }

 }

 function saveError(ex) {
     hideLoading();
     handleLockEntityError(ex, deletedEntityCallBack, updatedEntityCallBack);
 }



 function filterError(errors) {
 
     var allMsg = '';
     $('.validation').remove();
     $.each(errors, function (index, error) {
      
         var template = $('.Company-info');
         if (error.GlobalMessage) {
             allMsg += '<span>' + error.Message + '<br/></span>';
         }
         else if (error.ControlName == 'CompanyNameEnglish') {
             addPopupMessage(template.find('input.CompanyNameEnglish'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'CompanyNameArabic') {
             addPopupMessage(template.find('input.CompanyNameArabic'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'Website') {
             addPopupMessage(template.find('input.Website'), getResource(error.ResourceKey));
         }
          else if (error.ControlName == 'CompanyType') {
              addPopupMessage(template.find('select.CompanyType'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'DecisionMaker') {
             addPopupMessage(template.find('select.DecisionMaker'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'SalesManAccountManager') {
             addPopupMessage(template.find('select.SalesManAccountManager'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'Abbreviation') {
             addPopupMessage(template.find('input.Abbreviation'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'RegistrationNumber') {
             addPopupMessage(template.find('input.RegistrationNumber'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'RegistrationDate') {
             addPopupMessage(template.find('input.RegistrationDate'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'Segment') {
             addPopupMessage(template.find('select.Segment'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'Source') {
             addPopupMessage(template.find('select.Source'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'YearlyRevenue') {
             addPopupMessage(template.find('select.YearlyRevenue'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'EmployeeNumber') {
             addPopupMessage(template.find('select.EmployeeNumber'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'Status') {
             addPopupMessage(template.find('select.Status'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'IsBranch') {
             addPopupMessage(template.find('input.IsBranch'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'ParentCompany') {
             addPopupMessage(template.find('select.ParentCompany'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'PaymentTerms') {
             addPopupMessage(template.find('select.PaymentTerms'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'PaymentType') {
             addPopupMessage(template.find('select.PaymentType'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'CustomerReferralCompanyId') {
             addPopupMessage(template.find('select.CustomerReferralCompany'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'CustomerReferralPersonId') {
             addPopupMessage(template.find('select.CustomerReferralPerson'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'EmployeeUserId') {
             addPopupMessage(template.find('select.EmployeeUser'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'ScoutingComments') {
             addPopupMessage(template.find('textarea.ScoutingComments'), getResource(error.ResourceKey));
         }
     });

     hideLoading();
     if (allMsg.trim()) {
         showOkayMsg('Validation error', allMsg.trim());
     }
 }




 function saveCompanyContactInfo(CompanyId, CountryAC) {

     var AddressObj = {
         AddressID: -1,
         CountryId: CountryAC.get_ItemValue(),
         CityText: escape($('input:.City').val().trim()),
         BuildingNumber: ($('input:.BuildingNumber').val() != null && $('input:.BuildingNumber').val() != "") ? $('input:.BuildingNumber').val() : -1,
         StreetAddress: escape($('input:.StreetAddress').val().trim()),
         ZipCode: escape($('input:.ZipCode').val().trim()),
         POBox: escape($('input:.POBox').val().trim()),
         NearBy: escape($('input:.NearBy').val().trim()),
         Place: escape($('input:.Place').val().trim()),
         Area: escape($('input:.Area').val().trim()),
         Region: escape($('input:.Region').val().trim())
     };

     var HomePhone = {
         PhoneID: -1,
         PhoneNumber: $('input:.PphoneNumber').val().trim(),
         AreaCode: $('input:.PareaCode').val().trim(),
         CountryCode: $('input:.PcountryCode').val().trim(),
         Type: 1
     };

     var FaxPhone = {
         PhoneID: -1,
         PhoneNumber: $('input:.SphoneNumber').val().trim(),
         AreaCode: $('input:.SareaCode').val().trim(),
         CountryCode: $('input:.ScountryCode').val().trim(),
         Type: 2
     };

     var MobilePhone = {
         PhoneID: -1,
         PhoneNumber: $('input:.MphoneNumber').val().trim(),
         AreaCode: $('input:.MareaCode').val().trim(),
         CountryCode: $('input:.McountryCode').val().trim(),
         Type: 3
     };


     var BusinessEmail = {
         Email_Id: -1,
         Type: 1,
         EmailAddress: $('input:.BusinessEmail').val().trim()
     };

     var PersonalEmail = {
         Email_Id: -1,
         Type: 2,
         EmailAddress: $('input:.PersonalEmail').val().trim()
     };



     var url = CompanyServiceURL + "saveCompanyContactInfo";
     var data = formatString('{CompanyId:{0}, AddressObj:"{1}",HomePhoneObj:"{2}",FaxPhone:"{3}",MobilePhone:"{4}",BusinessEmail:"{5}",PersonalEmail:"{6}"}', CompanyId,
      escape(JSON.stringify(AddressObj)),
      escape(JSON.stringify(HomePhone)),
      escape(JSON.stringify(FaxPhone)),
      escape(JSON.stringify(MobilePhone)),
      escape(JSON.stringify(BusinessEmail)),
      escape(JSON.stringify(PersonalEmail))
     );


     post(url, data,
     function (returnedValue) {
    
         window.location = systemConfig.ApplicationURL + systemConfig.pageURLs.CompanyList + "?key=" + CompanyId + "&mood=add";
     }, saveError, '');

 }


 function getCompanyPersons(companyId) {

     showLoading($('.TabbedPanels'));
     $('.companyPersons-tab').children().remove();
     var data = formatString('{keyword:"", page:1, resultCount:50,argsCriteria:"{ PersonId:0, ExciptId:0,CompanyId:{0}}"}', companyId);
     var url = systemConfig.ApplicationURL_Common + systemConfig.PersonsServiceURL + "FindAllLite";

     post(url, data,
     function (data) {
         if (data.result != null) {
             var personObj = data.result;
             $('.companyPersons-tab').parent().find('.DragDropBackground').remove();
             $.each(personObj, function (index, item) {
                 var template = $(CompanyPersonsTemplate).clone();

                 template.css('cursor', 'default');
                 template = mapEntityInfoToControls(item, template);
                 template.find('.FullNameEn').text(Truncate(item.FullNameEn, 50));
                 template.find('.EmailBusiness').text(Truncate(item.BusinessEmail, 30));

                 var countryCode = item.CountryMobileNumber != null && item.CountryMobileNumber != "" ? "+(" + item.CountryMobileNumber + ")" : "";
                 var areaCode = item.AreaMobileNumber != null && item.AreaMobileNumber != "" ? item.AreaMobileNumber + "-" : "";
                 var phoneNumber = item.MobileNumber != null ? item.MobileNumber : "";
                 var FullPhoneNumber = countryCode + areaCode + phoneNumber;

                 template.find('.MobileNumber').text(FullPhoneNumber);
                 ViewPerson(item, template);
                 $('.companyPersons-tab').append(template);
             });
         }
         hideLoading();

     }, '', '');
 }


 function ViewPerson(Item, template) {
     var id = (Item != null) ? Item.Id : 0;
     template.find('.ViewPerson').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.PersonList + "?key=" + id);
     template.find('.Business-email-to').attr('href', 'mailto:' + Item.BusinessEmail);
 }




function checkPermissions(Sender, EntityId, ParentId, Template, Callback) {
    if ($(Sender).attr('pChecked') != 'checked') {
        //$(Sender).attr('pChecked', 'checked');
        var data = formatString('{EntityId:{0},ParentId:{1}}', EntityId, ParentId);
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: PemissionWebServiceURL + "checkUnAvailablePermissions",
            data: data,
            datatype: "json",
            success: function (data) {
                var returnedData = eval("(" + data.d + ")");
                if (returnedData.statusCode.Code == 1) {
                    if (returnedData.result != null) {
                        UnAvailablePermissionsList = returnedData.result;
                        Template = ApplyUserPermissions(Template);
                    }
                    else {
                        $('.add-entity-button').show();
                        $('.TabbedPanelsContent:visible').find('.Tab-action-header').show();
                    }
                }
                else {
                    //if 0: then there is no permissions at all
                    // alert('no access');
                }
                if (Callback) {
                    Callback(Template);
                }
            },
            error: function (e) { }
        });
    }
    else {
        if (Callback)
            Callback(Template);
    }
} 