
function getRelatedOpportunities(CompanyId) {

    $('.OpportunityTemp-tab').children().remove();
    showLoading($('.TabbedPanels'));
    var url = systemConfig.ApplicationURL_Common + systemConfig.OpportunityServiceURL + "FindAllLite";
    var data = formatString('{ keyword: "", page:1, resultCount:50,argsCriteria:"{CompanyId:{0}}"}', CompanyId);
    post(url, data, function (returnedData) {
        var RelatedObj = returnedData.result;
        AddRelatedSalesTab(null, CompanyId);
        if (RelatedObj != null) {
            $('.OpportunityTemp-tab').parent().find('.DragDropBackground').remove();
            $.each(RelatedObj, function (index, item) {
                AddRelatedSalesTab(item, CompanyId);
            });
        }
        hideLoading();
    });
}

function AddRelatedSalesTab(item, CompanyId) {

    var template = $(RelatedOpportunityTemplate).clone();
    ViewRelatedSales(item, template, CompanyId);
    if (item != null) {
        template.css('cursor', 'default');
        template = mapEntityInfoToControls(item, template);
        template.find('.Company').text(Truncate(item.Company, 25));
        template.find('.ProjectName').text(Truncate(item.ProjectName, 25));
        $('.OpportunityTemp-tab').append(template);
    }
}

function ViewRelatedSales(Item, template, CompanyId) {
    var id = (Item != null) ? Item.Id : 0;
    $('.Tab-action-header').show();
    $('a:.add-new-Opportunity').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.OpportunityList + "?cid=" + CompanyId);
    if (Item != null) {
        template.find('a:.company-link').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.CompanyList + "?key=" + Item.CompanyId);
        template.find('a:.sales-link').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.OpportunityList + "?key=" + Item.OpportunityId);
        template.find('a:.person-link').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.PersonList + "?key=" + Item.PersonId);

    }
}
