﻿
function getRelatedCase(CompanyId) {
    $('.CaseTemp-tab').children().remove();
    showLoading($('.TabbedPanels'));
    $.ajax({
        type: "POST",
        url: systemConfig.ApplicationURL_Common + systemConfig.ClientCareServiceURL + "FindAllLite",
        contentType: "application/json; charset=utf-8",
        data: formatString('{ keyword: "", page:1, resultCount:10,argsCriteria:"{CompanyId:{0}}"}', CompanyId),
        dataType: "json",
        success: function (data) {
            var RelatedObj = eval("(" + data.d + ")").result;
            AddRelatedCaseTab(null, CompanyId);
            if (RelatedObj != null) {
                $('.CaseTemp-tab').parent().find('.DragDropBackground').remove();
                $.each(RelatedObj, function (index, item) {
                    AddRelatedCaseTab(item, CompanyId);
                });
            }
            hideLoading();
        },
        error: function (e) {
        }
    });
}

function AddRelatedCaseTab(item, CompanyId) {
    var template = $(RelatedCaseTemplate).clone();
    template.find('.field-company').hide();
    if (item != null) {
        template.css('cursor', 'default');
        template = mapEntityInfoToControls(item, template);
        template.find('.Company').text(Truncate(item.Company, 20));
        template.find('.Status').text(Truncate(item.Status, 20));
        template.find('.CaseType').text(Truncate(item.CaseType, 20));
        $('.CaseTemp-tab').append(template);
    }

    ViewRelatedCase(item, template, CompanyId);
    
   
}

function ViewRelatedCase(Item, template, CompanyId) {
    var id = (Item != null) ? Item.Id : 0;
    $('a:.add-new-Case').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.ClientCareList + "?cid=" + CompanyId);
    if (Item != null) {
        template.find('a:.company-link').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.CompanyList + "?key=" + Item.CompanyId);
        template.find('a:.person-link').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.PersonList + "?key=" + Item.PersonId);
        template.find('a:.case-link').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.ClientCareList + "?key=" + Item.ClientCareId);

    }
}
