﻿var SelectedTabId = 0, TabIdentity = 1;
var ColorsArray = new Array();
var quickKeysArray = new Array(), TabProductArray = new Array(), QuickKeyProductArray = new Array(), AllItemsArray = null;
var IsEditTab = false, QuickKeyId = -1;
var lastUpdatedDate = null;
var identityIndex = 0;
var currentEntityLastUpdatedDate = null, lastUpdatedDate = null;
var EditMode = false;
///////////////////////////////
/////////////////////////////////
$(document).ready(function () {
    $('.TabsItemsContainer').click(function () {
        $('.colorsMenu').hide() 
     })
//    $('.Item-div').click(function () {
//        $('.colorsMenu').hide() 
//    })
});
function QuickKeysData() {
    this.TabId = TabIdentity;
    this.PageNo = '';
    this.ProductData = new Array();
    this.TabName = '';
    this.Identity = TabIdentity;
}
function QuickKeyProduct() {
    this.ItemIdentity = ++identityIndex;
    this.QuickKeyProductId = -1;
    this.QuickKeyId = QuickKeyId;
    this.TabId = '';
    this.ItemNumber = -1;
    this.ItemDescription = "";
    this.ItemBarCode = "";
    this.CssClass = '';
    this.TabName = '';
    this.Identity = TabIdentity;
    this.ProductId = -1;
}
function TabProduct() {
    this.TabId = -1;
    this.ProductsIds = '';
}

function InnerPage_Load() {
    
    currentEntity = systemEntities.QuickKeysEntityId;
    setSelectedMenuItem('.QuickKeys-icon', '.TabLinks-QuickKeys');
    QuickKeyId = (getQSKey('key')) ? getQSKey('key') : -1;
    $('.search-box').hide();
    setPageTitle(getResource("AddQuickKeys"));
    $('input:.add-item').attr('value', getResource("Add"));
    $('input:.confirm-button').attr('value', getResource("Save"));
    $('input:.button-cancel').attr('value', getResource("Cancel"))

    checkPermissions(this, systemEntities.QuickKeysEntityId, -1, $('.StoreSync-info-container'), function (returnedData) {
        if (QuickKeyId <= 0) {
            AddTab(true);
            showLoading($('.StoreSync-info-container'));
            $('.VieweditDisplayNone').removeClass('VieweditDisplayNone');
            $('Div.ItemsContainer').removeClass('fullwidth');
            $('.edit-pnl,.Back-pnl,.HName').hide();
            $('.save-cancel-pnl').show();
            $('.ActionCancel-link').live('click', function () {
                window.location = systemConfig.ApplicationURL_Common + systemConfig.pageURLs.QuickKeysList;
            });

        }

        else {
            $('.save-cancel-pnl').hide();
            getQuickKeyItem(QuickKeyId);
            GetQuickKeyInfo();

            $('.ActionCancel-link').live('click', function () {
                viewMode = true;
                EditMode = false;
                UnlockEntity(currentEntity, QuickKeyId, function () {
                    $('.QuickKey-selection,.TabArrow,.colorsArrow').addClass('VieweditDisplayNone');
                    $('Div.ItemsContainer').addClass('fullwidth');
                    $('.edit-pnl,.Back-pnl').show();
                    $('.save-cancel-pnl').hide();
                });
            });
        }


        CreateTabOptionsDiv(-1);
        GetAllItems();
        initControls();
        FillColorsArray();
    });
}

//function hideLists() {
//    $('.TabsMenu').hide();$('.colorsMenu').hide();
//}

function initControls() {
    $('.add-item').click(function () {
        addItem();
    });
    $('.add-all').click(function () {
        addAll();
    });
//    $('.TabArrow').click(function () {

//        $('.TabsMenu').show();
//    });
    $('.AddNewTab').click(function () {
        AddNewTab();
    });
    $('.item-search').keydown(function () {
        if ($(this).val().trim() != '')
            searchItems($(this).val());
        else
            LoadItems(AllItemsArray);

    });

    $('.cancel-qk').click(function () {
        $('.txtName').val('').removeClass('cx-control-red-border');
        $('.cmb-QK').hide();
        IsEditTab = false;
        $('.cmb-QK').find('.validation').remove();
        return false;
    });

    $('.confirm-button').click(function () {
        if (IsEditTab) {
            EditTab();
        }
        else {
            AddTab(false);
        }

        return false;
    });

    $('.txtName').keydown(function (event) {
        if (event.keyCode == 13 && event.which == 13)
            $('.confirm-button').click();
    });

    $('.ActionBack-link').live("click", function () {
        window.location = systemConfig.ApplicationURL_Common + systemConfig.pageURLs.QuickKeysList;
    });

    $('.ActionEdit-link').live("click", function () {
        EditMode = true;
        showLoading($('.StoreSync-info-container'));
        lockEntity(currentEntity, QuickKeyId, function () {
            $('.VieweditDisplayNone').removeClass('VieweditDisplayNone')
            $('Div.ItemsContainer').removeClass('fullwidth');
            $('.edit-pnl,.Back-pnl').hide();
            $('.save-cancel-pnl').show();

        }, currentEntityLastUpdatedDate, function () {
            window.location = systemConfig.ApplicationURL_Common + systemConfig.pageURLs.AddQuickKeys + "?key=" + QuickKeyId;
        }, function () {
            window.location = systemConfig.ApplicationURL_Common + systemConfig.pageURLs.QuickKeysList;
        });
    });

    $('.ActionSave-link').live('click', function () {
        SaveEntity();
        EditMode = false;
        return false;
    });
  //  $('.TabsItemsContainer').Live('click', function  (){ alert(); });

    showLoading($('.white-container-data'));
}


function AddNewTab() {
    SelectedItemTab = -1;
    $('.cmb-QK').show();

}
//////////////////////////////////
///////////////Add New Tab//////////////////////
////////////////////////////
function AddTab(IsDefault) {

    if (checkTabNameDuplication($('.txtName').val(), TabIdentity)) {
        addPopupMessage($('input.txtName'), getResource("AlreadyExist"));
        return false;
    }
    var TabNameValue = getResource('Default');
    if (!IsDefault)
        TabNameValue = $('.txtName').val();
    var TabUl = '.ItemsTabs-ul';
    var TabLi = $('<li>').attr({ class: 'tab' + TabIdentity, identity: TabIdentity }).click(function () {
        TabClick($(this).attr('identity'));
    });
    var TabDiv = $('<div>').attr({ class: 'ItemsTabs-div' }).click(function () {
        //$('.TabsMenu').hide();
        $('.colorsMenu').hide();
        $('.ItemsTabs-div').removeClass('selected');
        $(this).addClass('selected');
    });
    //debugger
    var cssClass = '';//  'VieweditDisplayNone';
    if (EditMode) cssClass = "";
    var TabOptions = $('<a>').attr({ class: 'TabArrow white ' + cssClass }).click(function () {
        //$('.TabsMenu').hide();
        $('.colorsMenu').hide();
        if (!$(TabDiv).find(TabOptionsDiv).hasClass('display'))
            $(TabDiv).find(TabOptionsDiv).addClass('display').show();
        else
            $(TabDiv).find(TabOptionsDiv).removeClass('display').hide();
    });
    var TabName = $('<span>').attr({ class: 'tab-name' }).html(TabNameValue + '&nbsp;');
    var TabOptionsDiv = $('<div>').attr({ class: 'TabsMenu' });

    var DeleteTabLink = $('<span>').attr({ class: 'DeleteTab' }).html(getResource("Delete")).click(function () {
        RemoveTabFromArray(SelectedTabId);
        DeleteTab($(TabLi).attr('identity'));

    });
    var RenameTabLink = $('<span>').attr({ class: 'RenameTab' }).html(getResource("Rename")).click(function () {
        IsEditTab = true;
        $(TabDiv).find(TabOptionsDiv).removeClass('display').hide();
        RenameTab();
    });
    var TabItemsContainer = '.TabsItemsContainer';
    var TabItemUL = $('<ul>').attr({ class: 'TabsItemsContainer-ul', id: 'container-' + TabIdentity });
    $(TabItemsContainer).append(TabItemUL);
    $(TabOptionsDiv).append(DeleteTabLink, RenameTabLink);
    $(TabDiv).append(TabName, TabOptions, TabOptionsDiv);
    $(TabLi).append(TabDiv);
    $(TabUl).append(TabLi);
    var TabUl = '.ItemsTabs-ul';
    $('.cancel-qk').click();
    IsEditTab = false;
    TabClick(TabIdentity);
    $(TabDiv).click();
    ///////////////////////////////////////////////////
    ////////////////////////////////////////////////
    AddArrayTabArray(TabIdentity, TabNameValue);
    TabIdentity = TabIdentity + 1;
}

///////////////////////////
////////////////Edit Tab
//////////////////////////////////////
function EditTab(TabId) {
    if (checkTabNameDuplication($('.txtName').val(), TabId)) {
        addPopupMessage($('input.txtName'), getResource("AlreadyExist"));
        return false;
    }
    var TabName = $('.txtName').val();
    if (TabName.trim() == '') {
        $('.txtName').attr({ style: 'border:1px solid red' });
        return false;
    }

    $.each(quickKeysArray, function (ix, item) {
        if (item.TabId == SelectedTabId) {
            item.TabName = TabName;
            return false;
        }
    });
    $.each(QuickKeyProductArray, function (ix, item) {
        if (item.TabId == SelectedTabId) {
            item.TabName = TabName;
        }
    });

    $('.ItemsTabs-ul').find('.selected').find('.tab-name').html(TabName);
    $('.cmb-QK').hide();
    $('.txtName').val('');
    IsEditTab = false;
}
//////Tab Click ////////////////
//////////////////////////////////
function TabClick(TabId) {
    $(".TabsItemsContainer-ul").hide();
    $("#container-" + TabId).fadeIn('slow').show();
    SelectedTabId = TabId;
    $('.item-color-open').removeClass('item-color-open');
    if ($('.divOptions').hasClass('display')) {
        $('.divOptions').removeClass('display').hide();
    }
    return false;
}
////////////////////////////////////
////////////Delete Tab//////////////
/////////////////////////////////////
function DeleteTab(id) {
    if ($('.tab' + id).prev().length > 0)
        $('.tab' + id).prev().find('.ItemsTabs-div').addClass('selected').click();
    else if ($('.tab' + id).next().length > 0)
        $('.tab' + id).next().find('.ItemsTabs-div').addClass('selected').click();

    $('.tab' + id).remove();
    $('#container-' + id).remove();

    quickKeysArray.pop(id);
    QuickKeyProductArray.pop(id);
}

//////////////////////
//////////Rename Tab
////////////////////////////
function RenameTab() {
    $('.cmb-QK').show();
    $('.txtName').val($('.ItemsTabs-ul').find('.selected').find('.tab-name').text()).focus();
}
/////////////////////////////////////
///////////Create Tab Option Div (Rename)//////
/////////////////////////////
function CreateTabOptionsDiv(TabId) {
    var divOptions = $('<div>').attr({ id: 'divOption', class: 'div-options', style: 'display:none;' });
    $('form').append(divOptions);
    var UpdateName = $('<a>').attr({ href: '', id: 'lnkUpdateTabName' }).html('✔ ' + getResource("Save"))
    .click(function () {
        if (IsEditTab)
            EditTab();
        else
            AddTab(false);

        return false;
    });
    var hideDive = $('<a>').attr({ href: '', id: 'lnkCloseTabOptions' }).html('✕ ' + getResource("Cancel")).click(function () {
        $('.txtName').val('');
        $('#divOption').hide();
        return false;
    });
    var txtName = $('<input>').attr({ type: 'Text', class: 'txtName' }).keyup(function (event) {
        if (event.keyCode == 13 || event.which == 13) {
            $(UpdateName).click();
        }
    });
    $('#divOption').append(txtName, UpdateName, hideDive);
}

//////////////////////////////
/////////Get All Items From Data Base ////////////////////
//////////////////////////////////////////////////////////
function GetAllItems() {
    var data = formatString('{keyword:"", page:1, resultCount:50,argsCriteria:"{}"}');
    var url = systemConfig.ApplicationURL_Common + systemConfig.ItemServiceURL + "FindAllLite";
    post(url, data,
     function (data) {
         if (data.result != null) {
             LoadItems(data.result)
         }
         else
             hideLoading();

     }, '', '');
}
////////////////////////////////////////
////////Load Items/////////////////////////////
function LoadItems(ItemsData) {
    AllItemsArray = ItemsData;
    $.each(ItemsData, function (ix, item) {
        var Option = ' <option value="' + item.ItemNumber + '"itemNumber="' + item.ItemNumber + '">' + item.ItemDescription + '</option>';
        $('.lstItems').append(Option);
    });
    hideLoading();
}
////////////////////////////
///////add item to tab///////////////////
//////////////////////////
function addItem() {
    $('.lstItems').find('option:selected').each(function () {
        if ($(this).html() != null) {
            listItem($(this));
        }

    });
}

function addAll() {
    $('.lstItems').find('option').each(function () {
        if ($(this).html() != null) {
            listItem($(this));
        }
    });
}
///////////////////////////////////////////////////////////////////
/////////////////// add item with item options to the selected tab/////////////
///////////////////////////////////////////////////////////////////
function listItem(Item) {
    var TabContainer = $('#container-' + SelectedTabId);
    if ($(TabContainer).find('#item-' + $(Item).attr('value')).length > 0) {
        return false;
    }
    var itemli = $('<li>');
    var itembtn = $('<button>').attr({ class: 'Item-div color10', id: 'item-' + $(Item).attr('value') }).click(function () {
        return false;
    });
    //debugger
   // var cssClass = 'VieweditDisplayNone';
    var cssClass = '';
    if (EditMode) cssClass = "";

    var itemSpan = $('<span>').html($(Item).html());
    var BtnOptions = $('<a>').attr({ class: 'colorsArrow ' + cssClass }).click(function () {

        //$('.TabsMenu').hide();
        $('.colorsMenu').hide();
        if ($(TabContainer).find('#colors-div-' + $(Item).attr('value')).hasClass('display'))
            $(TabContainer).find('#colors-div-' + $(Item).attr('value')).removeClass('display').hide();
        else
            $(TabContainer).find('#colors-div-' + $(Item).attr('value')).addClass('display').show();

    });
    var colorsDiv = $('<div>').attr({ class: 'colorsMenu', id: 'colors-div-' + $(Item).attr('value') });
    var DeleteSpan = $('<span>').attr({ class: 'DeleteItem' }).html(getResource("Delete")).click(function () {
        $(itembtn).remove();
        RemoveItemFromArray($(Item).attr('value'));
    });
    $(colorsDiv).append(DeleteSpan);
    var colorsUl = $('<ul>').attr({ class: 'colorsMenu-ul' });
    $.each(ColorsArray, function (ix, item) {
        var li = $('<li>');
        var colorA = $('<a>').attr({ class: 'itemColorsBlock' + ' ' + item }).click(function () {
            $(itembtn).removeClass();
            $(itembtn).addClass('Item-div' + ' ' + item);
            $(colorsDiv).removeClass('display').hide();
            $.each(QuickKeyProductArray, function (index, QuickkeyItem) {
                if (SelectedTabId == QuickkeyItem.TabId && QuickkeyItem.ItemNumber == $(Item).attr('value')) {
                    $.each(TabProductArray, function (index, tabProductItem) {
                        if (SelectedTabId == tabProductItem.TabId) {
                            QuickkeyItem.CssClass = item;
                            return false;
                        }
                    });
                    return;
                }
            });
        });
        $(li).append(colorA);
        $(colorsUl).append(li);
    });
    $(colorsDiv).append(colorsUl);
    $(itembtn).append(itemSpan, BtnOptions, colorsDiv);
    $(itemli).append(itembtn);
    $(TabContainer).append(itemli);
    AddEditItemArray($(Item).attr('value'));
}

///////////////////////////////
//////////Items Colors Array ////////////////////
/////////////////////////////////////////////////////
function FillColorsArray() {
    ColorsArray[0] = 'color1';
    ColorsArray[1] = 'color2';
    ColorsArray[2] = 'color3';
    ColorsArray[3] = 'color4';
    ColorsArray[4] = 'color5';
    ColorsArray[5] = 'color6';
    ColorsArray[6] = 'color7';
    ColorsArray[7] = 'color8';
    ColorsArray[8] = 'color9';
    ColorsArray[9] = 'color11';
    ColorsArray[10] = 'color12';
    ColorsArray[11] = 'color13';
    ColorsArray[12] = 'color10';
}


///////////////////////////////////////////////
///////////////Add Tab To Array //////////////////
function AddArrayTabArray(TabId, TabName) {
    var TabProductObj = new TabProduct();
    var QuickKeyObj = new QuickKeysData();
    TabProductObj.TabId = TabId;
    TabProductObj.ProductsIds = '';
    QuickKeyObj.TabId = TabId;
    QuickKeyObj.TabName = TabName;
    QuickKeyObj.Identity = TabId;
    quickKeysArray.push(QuickKeyObj);
    TabProductArray.push(TabProductObj);
}

////////////////////////////////////////////
//////////////Remove Tab From Array/////////////
////////////////////////////////////////////////////
function RemoveTabFromArray(TabId) {
    var index = 0;
    while (true) {
        if (QuickKeyProductArray[index]) {
            if (QuickKeyProductArray[index].TabId == TabId) {
                QuickKeyProductArray.splice(index, 1);
            }
            else {
                index++;
            }
        }
        else
            break;
    }
}
//////////////////////////////////////////////
///////AddEditItemArray////////////////////
function AddEditItemArray(ItemValue) {
    var Item = null;
    var ProductObject = new QuickKeyProduct();
    $.each(AllItemsArray, function (ix, item) {
        if (item.value == ItemValue || item.ItemNumber == ItemValue) {
            Item = item;
            return false;
        }
    });

    $.each(quickKeysArray, function (index, QuickkeyItem) {
        if (SelectedTabId == QuickkeyItem.TabId) {
            $.each(TabProductArray, function (index, tabProductItem) {
                if (SelectedTabId == tabProductItem.TabId) {
                    ProductObject.ItemNumber = Item.ItemNumber;
                    ProductObject.ItemBarCode = Item.ItemBarCode;
                    ProductObject.ItemDescription = Item.ItemDescription;
                    ProductObject.TabId = SelectedTabId;
                    ProductObject.CssClass = (Item.css) ? item.css : 'color10';
                    ProductObject.TabName = QuickkeyItem.TabName;
                    ProductObject.Identity = SelectedTabId;
                    ProductObject.QuickKeyId = QuickKeyId;
                    ProductObject.ProductId = Item.ItemCardId;
                    QuickKeyProductArray.push(ProductObject);
                    tabProductItem.ProductsIds += ',' + Item.ItemNumber;
                    $('#txtproduct_search').focus();
                    return false;
                }
            });
        }
    });
}

////////////////////////////////////////
//////////////Remove Item From Array //////////////
///////////////////////////////////////////////////
function RemoveItemFromArray(id) {
    $.each(QuickKeyProductArray, function (index, item) {
        if (item.Identity == SelectedTabId) {
            if (item.ItemNumber == id) {
                $.each(TabProductArray, function (index, TabProductitem) {
                    if (TabProductitem.TabId == SelectedTabId) {
                        TabProductitem.ProductsIds.replace((',' + id), "");
                    }
                });
                QuickKeyProductArray.splice(index, 1);
                $('#txtproduct_search').focus();
                return false;
            }
        }
    });
}

/////////////////////////////////////////////////////
//////////////////Search Items From Array ///////////
//////////////////////////////////////////////////////
function searchItems(keyword) {
    $('.lstItems').empty();
    $.each(AllItemsArray, function (ix, item) {
        if (item.ItemDescription.toUpperCase().indexOf(keyword.toUpperCase()) > -1) {
            var Option = ' <option value="' + item.value + '"index="' + ix + '">' + item.ItemDescription + '</option>';
            $('.lstItems').append(Option);
        }
    });
}
///////////////////////////////////////////////////////////////////////////////
///////////////////// Save Quick Key //////////////////////////////////////////////
function SaveEntity() {
    var QuickKeysServiceURL = systemConfig.ApplicationURL_Common + systemConfig.QuickKeysServiceURL;
    var validform = true;
    if (!saveForm($('.StoreSync-info-container')))
        validform = false;

    if (!validform)
        return false;
    if (QuickKeyProductArray.length <= 0) {
        showOkayMsg(getResource("Alert"), getResource("QuickKeyValidationMessage"));
        return false;

    }
    var data = formatString('{id:{0},QuickKeysInfo:"{1}",QuickKeyName:"{2}"}', QuickKeyId,  escape(JSON.stringify(QuickKeyProductArray)), $('.Name').val());
    if (QuickKeyId > 0)
        url = QuickKeysServiceURL + "Edit";
    else
        url = QuickKeysServiceURL + "Add";
    post(url, data, saveSuccess, saveError, getEntitylockAjaxHeaders(lastUpdatedDate));
}


function saveSuccess(returnedValue) {
    if (returnedValue.statusCode.Code == 501) {
        addPopupMessage($('input.Name'), getResource("AlreadyExist"));
    }
    else {
        showStatusMsg(getResource("savedsuccessfully"));
        window.location = systemConfig.ApplicationURL_Common + systemConfig.pageURLs.QuickKeysList;
    }

}

function saveError() {
}
//////////////////////////////////////////////////////////////////////////////////////
///////////////////////////// Load Quick Key ////////////////////////////////////////
function GetQuickKeyInfo() {
    var QuickKeyProductServiceURL = systemConfig.ApplicationURL_Common + systemConfig.QuickKeysServiceURL;
    var data = formatString('{keyword:"", page:1, resultCount:50,argsCriteria:"{QuickKeyId:{0}}"}', QuickKeyId);
    var url = QuickKeyProductServiceURL + "FindAllProductsLite";
    post(url, data,
     function (data) {
         if (data.result != null) {
             LoadQuickKey(data.result);
             return;
         }
         hideLoading();

     }, '', '');
}

////////////////////////////////////////
////////////////////////////////////////
function LoadQuickKey(QuickKeyData) {
    var LastTabId = 0;
    $('.Name').val(QuickKeyData[0].QuickKey);
    $('.QuickKey').text(QuickKeyData[0].QuickKey);

    $.each(QuickKeyData, function (ix, item) {
        if (LastTabId != item.TabId) {
            LastTabId = item.TabId;
            AddLoadTab(item);
        }
        LoadTabItems(item);
    });
}

//////////////////////////////
////////////////////////////////////////////////
//////////////////////////////////
function AddLoadTab(item) {

    var TabNameValue = item.TabName;
    var TabUl = '.ItemsTabs-ul';
    var TabLi = $('<li>').attr({ class: 'tab ' + item.TabId, identity: item.TabId }).click(function () {
        TabClick($(this).attr('identity'));
    });
    var TabDiv = $('<div>').attr({ class: 'ItemsTabs-div' }).click(function () {
        //$('.TabsMenu').hide();
        $('.colorsMenu').hide();
        $('.ItemsTabs-div').removeClass('selected');
        $(this).addClass('selected');
    });
    //debugger
    var cssClass = 'VieweditDisplayNone';
    if (EditMode) cssClass = "";

    var TabOptions = $('<a>').attr({ class: 'TabArrow white ' + cssClass }).click(function () {
        $('.TabsMenu').hide();
        $('.colorsMenu').hide();
        if (!$(TabDiv).find(TabOptionsDiv).hasClass('display'))
            $(TabDiv).find(TabOptionsDiv).addClass('display').show();
        else
            $(TabDiv).find(TabOptionsDiv).removeClass('display').hide();
    });
    var TabName = $('<span>').attr({ class: 'tab-name' }).html(TabNameValue + '&nbsp;');
    var TabOptionsDiv = $('<div>').attr({ class: 'TabsMenu' });

    var DeleteTabLink = $('<span>').attr({ class: 'DeleteTab' }).html(getResource("Delete")).click(function () {
        RemoveTabFromArray(SelectedTabId);
        DeleteTab(item.TabId);

    });
    var RenameTabLink = $('<span>').attr({ class: 'RenameTab' }).html(getResource("Rename")).click(function () {
        IsEditTab = true;
        $(TabDiv).find(TabOptionsDiv).removeClass('display').hide();
        RenameTab();
    });
    var TabItemsContainer = '.TabsItemsContainer';
    var TabItemUL = $('<ul>').attr({ class: 'TabsItemsContainer-ul', id: 'container-' + item.TabId });
    $(TabItemsContainer).append(TabItemUL);
    $(TabOptionsDiv).append(DeleteTabLink, RenameTabLink);
    $(TabDiv).append(TabName, TabOptions, TabOptionsDiv);
    $(TabLi).append(TabDiv);
    $(TabUl).append(TabLi);
    var TabUl = '.ItemsTabs-ul';
    $('#lnkCloseTabOptions').click();
    IsEditTab = false;
    $(TabDiv).click();
    TabIdentity = item.TabId + 1;


    ///////////////////////////////////////////////////
    ////////////////////////////////////////////////
    AddArrayTabArray(item.TabId, TabNameValue);

}

///////////////////////////////
//////////Load Tab Items
////////////////////////////
function LoadTabItems(Item) {
    var TabContainer = $('#container-' + SelectedTabId);
    var itemli = $('<li>');
    var itembtn = $('<button>').attr({ class: 'Item-div', id: 'item-' + Item.ItemNumber }).click(function () {
        return false;
    }).addClass(Item.CssClass);
    //debugger
    var cssClass = 'VieweditDisplayNone';
    if (EditMode) cssClass = "";

    var itemSpan = $('<span>').html(Item.ItemDescription);
    var BtnOptions = $('<a>').attr({ class: 'colorsArrow ' + cssClass }).click(function () {

       // $('.TabsMenu').hide();
        $('.colorsMenu').hide();
        if ($(TabContainer).find('#colors-div-' + Item.ItemNumber).hasClass('display'))
            $(TabContainer).find('#colors-div-' + Item.ItemNumber).removeClass('display').hide();
        else
            $(TabContainer).find('#colors-div-' + Item.ItemNumber).addClass('display').show();

    });
    var colorsDiv = $('<div>').attr({ class: 'colorsMenu', id: 'colors-div-' + Item.ItemNumber });
    var DeleteSpan = $('<span>').attr({ class: 'DeleteItem' }).html(getResource("Delete")).click(function () {
        $(itembtn).remove();
        RemoveItemFromArray(Item.ItemNumber);
    });
    $(colorsDiv).append(DeleteSpan);
    var colorsUl = $('<ul>').attr({ class: 'colorsMenu-ul' });
    $.each(ColorsArray, function (ix, item) {
        var li = $('<li>');
        var colorA = $('<a>').attr({ class: 'itemColorsBlock' + ' ' + item }).click(function () {
            $(itembtn).removeClass();
            $(itembtn).addClass('Item-div' + ' ' + item);
            $(colorsDiv).removeClass('display').hide();
            $.each(QuickKeyProductArray, function (index, QuickkeyItem) {
                if (SelectedTabId == QuickkeyItem.TabId && QuickkeyItem.ItemNumber == Item.ItemNumber) {
                    $.each(TabProductArray, function (index, tabProductItem) {
                        if (SelectedTabId == tabProductItem.TabId) {
                            QuickkeyItem.CssClass = item;
                            return false;
                        }
                    });
                    return;
                }
            });
        });
        $(li).append(colorA);
        $(colorsUl).append(li);
    });
    $(colorsDiv).append(colorsUl);
    $(itembtn).append(itemSpan, BtnOptions, colorsDiv);
    $(itemli).append(itembtn);
    $(TabContainer).append(itemli);
    LoadItemArray(Item);

}


//////////////////////////////////////////////
///////Load Item To Array ////////////////////
function LoadItemArray(Item) {
    var ProductObject = new QuickKeyProduct();
    $.each(quickKeysArray, function (index, QuickkeyItem) {
        if (SelectedTabId == QuickkeyItem.TabId) {
            $.each(TabProductArray, function (index, tabProductItem) {
                if (SelectedTabId == tabProductItem.TabId) {
                    ProductObject.ItemNumber = Item.ItemNumber;
                    ProductObject.ItemBarCode = Item.ItemBarCode;
                    ProductObject.ItemDescription = Item.ItemDescription;
                    ProductObject.TabId = SelectedTabId;
                    ProductObject.CssClass = (Item.CssClass) ? Item.CssClass : 'color10';
                    ProductObject.TabName = QuickkeyItem.TabName;
                    ProductObject.Identity = SelectedTabId;
                    ProductObject.QuickKeyId = QuickKeyId;
                    ProductObject.QuickKeyProductId = Item.QuickKeyProductId
                    QuickKeyProductArray.push(ProductObject);
                    tabProductItem.ProductsIds += ',' + Item.ItemNumber;
                    $('#txtproduct_search').focus();
                    return false;
                }
            });
        }
    });
}

////////////////////////////////////////
///////Check Tab Name Duplication/////////////
///////////////////////////////////////////////
function checkTabNameDuplication(Name, TabId) {
    var isExists = false;
    $.each(quickKeysArray, function (ix, item) {
        var TabName = item.TabName.trim();
        if (TabName.toUpperCase() == Name.toUpperCase().trim() && item.TabId != TabId) {
            isExists = true;
        };
    });

    return isExists;
}


function getQuickKeyItem(id) {
    var data = formatString('{id:{0}}', id);
    var url = systemConfig.ApplicationURL_Common + systemConfig.QuickKeysServiceURL + "FindByIdLite";
    post(url, data,
     function (data) {
         if (data.result != null) {
             currentEntityLastUpdatedDate = data.result.UpdatedDate || data.result.Updated_Date;
             lastUpdatedDate = getEntityLockDateFormat(currentEntityLastUpdatedDate);
         }

     }, '', '');
}