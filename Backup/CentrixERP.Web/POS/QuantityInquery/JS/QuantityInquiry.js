﻿var POSInvoiceServiceURL;

var QuantityInquiryHeaderTemplate;
var QuantityInquiryResultsGridDetailsTemplate;
var itemsGrid = null;
var TotalBeforediscountandTax = 0, TotalCash = 0, TotalItemDiscount = 0, TotalOnAccount = 0, TotalInvoicediscount = 0,
TotalCreditCard = 0, TotalSalesTax = 0, TotalCheque = 0;
var ItemAC;


function InnerPage_Load() {
    setPageTitle(getResource("QuantityInquiry"));
    setSelectedMenuItem('.pos-icon');
    POSInvoiceServiceURL = systemConfig.ApplicationURL + systemConfig.POSPostInvoiceToShipmentServiceURL;


    GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.POSQuantityViewItemTemplate, function () {
        $('.white-container-data').append(QuantityInquiryHeaderTemplate);
    }, 'QuantityInquiryHeaderTemplate');

    GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.POSQuantityResultGridTemplate, function () {
    }, 'QuantityInquiryResultsGridDetailsTemplate');
    initControls();
    $('.btnSearch').attr('value', getResource('Search'));
    $('.lnkReset').attr('value', getResource('Reset'));
    //$('.btnSearch').click();
}

function initControls() {

    LoadDatePicker('input:.date');

    StoreAC = $('select:.Store').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.StoreServiceURL + 'FindAllLite', required: false, multiSelect: false });
    ItemAC = $('select:.ItemNumber').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.ItemServiceURL + 'FindAllLite', required: false, multiSelect: false });

    var InvoiceDateFrom;
    var InvoiceDateTo;

    $('input:.btnSearch').click(function () {

        if (StoreAC.get_Item() == null && ItemAC.get_Item() == null) {
            showOkayMsg("Validation", "Please Select Search Criteria");
            return;
        }
        TotalBeforediscountandTax = 0, TotalCash = 0, TotalItemDiscount = 0, TotalOnAccount = 0, TotalInvoicediscount = 0,
        TotalCreditCard = 0, TotalSalesTax = 0, TotalCheque = 0;
        showLoading($('.Main-Div'));
        //InvoiceDateFrom = ($('input:.InvoiceDateFrom').val() != "") ? getDate('mm-dd-yy', getDotNetDateFormat($('input:.InvoiceDateFrom').val())) : getDate('mm-dd-yy', formatDate($('input:.InvoiceDateFrom').val()));
        //InvoiceDateTo = ($('input:.InvoiceDateTo').val() != "") ? getDate('mm-dd-yy', getDotNetDateFormat($('input:.InvoiceDateTo').val())) : getDate('mm-dd-yy', formatDate($('input:.InvoiceDateTo').val()));
        var storeId = (StoreAC.get_Item()) ? StoreAC.get_Item().value : -1;
        var ItemId = (ItemAC.get_Item()) ? ItemAC.get_Item().value : -1;
        var url = POSInvoiceServiceURL + "QuantityInquiryResults";
        var data = formatString('{locationId:{0},storeId:{1},ItemId:{2}}', -1, storeId, ItemId);

        post(url, data, FindResultsSuccess, function () { }, null);
    });

    $('input:.lnkReset').click(function () {
        StoreAC.clear();
        ItemAC.clear();
        
        $('.InvoiceDateFrom').val('');
        $('.InvoiceDateTo').val('');
    });
}

function loadItemsGrid(Template, datasource, rowTemplate, headerTemplate) {

    var control = [
  {
      controlId: 'InvoiceNumber',
      type: 'text'

  },
   {
       controlId: 'Type',
       type: 'text'

   },
    {
        controlId: 'InvoiceDate',
        type: 'text'

    },

    {
        controlId: 'TotalPrice',
        type: 'text',
        numeric: true

    },
     {
         controlId: 'ItemsDiscount',
         type: 'text',
         numeric: true

     },
      {
          controlId: 'Tax',
          type: 'text',
          numeric: true

      },
     {
         controlId: 'OnAccountAmount',
         type: 'text',
         numeric: true

     },
        {
            controlId: 'CashAmount',
            type: 'text',
            numeric: true

        },

      {
          controlId: 'Store',
          type: 'text'

      },
      {
          controlId: 'Register',
          type: 'text'

      },

      {
          controlId: 'Total',
          type: 'text'

      }


 ];
    //after create array of controls, we need init the grid

      itemsGrid = Template.find('#items-grid').Grid({
          headerTeamplte: Template.find('#h-template').html(),
          rowTemplate: QuantityInquiryResultsGridDetailsTemplate,
          rowControls: control,
          dataSource: datasource,
          selector: '.',
          appendBefore: '.footer-row',
          rowDeleted: function (grid) {

              var Count = 0;
              $.each(grid.table.find('tr.r-data'), function (index, item) {
                  Count = Count + 1;
              });
              if (grid.rows.length == 1) {
                  grid.defaults.preventDelete = true;
              } else grid.defaults.preventDelete = false;
          },
          rowAdded: function (row, controls, dataRow, grid) {

              TotalInvoicediscount += dataRow.AmountDiscount;
              TotalBeforediscountandTax += dataRow.TotalPrice;
              TotalCash += dataRow.CashAmount;
              TotalItemDiscount += dataRow.ItemsDiscount;
              TotalOnAccount += dataRow.OnAccountAmount;
              TotalCreditCard += dataRow.CreditCardAmount;
              TotalSalesTax += dataRow.Tax;
              TotalCheque += dataRow.ChequeAmount;
              $('.Subtotal').html(parseFloat(TotalBeforediscountandTax).toFixed(3));
              $('.totalCash').html(parseFloat(TotalCash).toFixed(3));
              $('.itemDiscount').html(parseFloat(TotalItemDiscount).toFixed(3));
              $('.onAccount').html(parseFloat(TotalOnAccount).toFixed(3));
              $('.invoicediscount').html(parseFloat(TotalInvoicediscount).toFixed(3));
              $('.totalcreditcard').html(parseFloat(TotalCreditCard).toFixed(3));
              $('.totaltax').html(parseFloat(TotalSalesTax).toFixed(3));
              $('.totalcheque').html(parseFloat(TotalCheque).toFixed(3));
              $('.totalPayment').html(parseFloat(TotalCheque + TotalCreditCard + TotalOnAccount + TotalCash).toFixed(3));
              $('.TotalSales').html(parseFloat(TotalBeforediscountandTax - TotalInvoicediscount - TotalItemDiscount + TotalSalesTax).toFixed(3));
              row.find('.AvailableQTY').css('color', 'green');
              row.find('.Total').css('font-weight', 'bold');
              row.find('.TotalPayment').css('font-weight', 'bold');

              row.find('.QTY').css('color', 'red');
              if (row.find('.StagingQuantity').html() == " ")
                  row.find('.StagingQuantity').html("0");
              if (row.find('.PendingQTY').html() == " ")
                  row.find('.PendingQTY').html("0");
              if (row.find('.POSQuantity').html() == " ")
                  row.find('.POSQuantity').html("0");
              if (row.find('.ERPQuantity').html() == " ")
                  row.find('.ERPQuantity').html("0");
              if (row.find('.PendingOutQuantity').html() == " ")
                  row.find('.PendingOutQuantity').html("0");

              if (row.find('.StagingOutQuantity').html() == " ")
                  row.find('.StagingOutQuantity').html("0");

              var SyncedQuantity = Number(row.find('.StagingQuantity').html()) + Number(row.find('.PendingQTY').html()) + Number(row.find('.POSQuantity').html()) + Number(row.find('.PendingOutQuantity').html()) + Number(row.find('.StagingOutQuantity').html());
              var NotSyncedQuantity = Number(row.find('.ERPQuantity').html()) - Number(SyncedQuantity);

              row.find('.SyncedQTY').html(SyncedQuantity);
              row.find('.NotSyncedQTY').html(NotSyncedQuantity);
              row.find(".TotalPOSQuantity").html(SyncedQuantity);
//              row.find('.StagingQuantity').html(dataRow.StagingQuantity * -1);
//              row.find('.StagingOutQuantity').html(dataRow.StagingOutQuantity * -1);
//              row.find('.PendingQTY').html(dataRow.PendingQTY * -1);
//              row.find('.PendingOutQuantity').html(dataRow.PendingOutQuantity * -1);
                
              var Count = 0;
              $.each(grid.table.find('tr.r-data'), function (index, item) {

                  row.find('.ShipmentNumber').unbind().click(function () {
                      window.open(systemConfig.ApplicationURL + systemConfig.pageURLs.IcShipmentList + "?key=" + item.ShipmentId);

                  });

                  Count = Count + 1;
              });


              if (grid.rows.length == 1) {
                  grid.defaults.preventDelete = true;
              } else grid.defaults.preventDelete = false;

          }, preventAdd: false
      });
}


function FindResultsSuccess(returnedValue) {   //Here Create the details grid , Fill it with corresponding data
    hideLoading();
    
    $('#items-grid').empty();
    var result = returnedValue.result;
    if (result && result.length > 0) {
        $('.NoDataFound').hide();
        loadItemsGrid($('.gridContainerDiv'), result);


        $($('table')[2]).wrap("<div class='grid-height'></div>");  //To Control the grid height


        $($('.ShipmentNumber')[0]).click(function () {
            window.open(systemConfig.ApplicationURL + systemConfig.pageURLs.IcShipmentList + "?key=" + $($('.ShipmentId')[0]).text());

        });

        $.each($('.InvoiceDate'), function (ix, itm) {
            $($('.InvoiceDate')[ix]).text(getDate("dd-mm-yy", formatDate($($('.InvoiceDate')[ix]).text())));
        });


    }
    else {
        $('.NoDataFound').show();
        $('.NoDataFound').attr('style', 'display: inline-block !important;');
    }





}



function getdate(date) {
    var arr = date.split('-');
    return arr[1] + '-' + arr[0] + '-' + arr[2];
}




