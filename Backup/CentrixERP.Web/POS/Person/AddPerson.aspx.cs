using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CentrixERP.Common.Web;
using CentrixERP.Common.Business.Entity;
using E = CentrixERP.Business.Entity;
using IM = CentrixERP.Business.IManager;
using SF.Framework;

using CentrixERP.Configuration;

namespace Centrix.StandardEdition.CRM.Web.CRM.Person
{
    public partial class AddPerson : BasePageLoggedIn
    {
        public AddPerson()
            : base(Enums_S3.Permissions.SystemModules.None) { }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}