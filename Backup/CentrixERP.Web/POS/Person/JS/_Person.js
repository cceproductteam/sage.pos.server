var PersonServiceURL;
var PersonViewItemTemplate, ItemListTemplate, RelatedOpportunities, RelatedGiveawaysTemplate, RelatedCaseTemplate, RelatedOpportunityTemplate;
var CompanyAC, SalesManAC, SalutationAC, GenderAC, SourceAC, ReligionAC, MaritalStatusAC, DepartmentAC, PositionAC, CustomerReferralCompanyAC, CustomerReferralPersonAC, EmployeeUserAC, PersonTypeAC,PrimaryContactEmployeeAC;
var HearAboutUsAC, BestTimeToContactAC, AccountManagerAC, VipClassAC;
var addPersonMode = false;
var PersonId = -1, CustomerReferralCompanyId = -1;
var PersonName = '', CompanyName = '', CompanyId = -1,ContactEmployeeName="",ContactEmployeeId=-1;
var ContactTypeAC, NationalityAC, InterestedInAC, IsBusinessEmail = false, IsPersonalEmail = false,IsBusinessEmailDuplication = false,IsPersonalEmailDupliaction = false;
var IsMobileDuplication = false;
function InnerPage_Load() {
    setSelectedMenuItem('.Person-icon', '.TabLinks-Person');
    PersonServiceURL = systemConfig.ApplicationURL_Common + systemConfig.PersonsServiceURL;
    currentEntity = systemEntities.PersonEntityId;
    if (addPersonMode) {
        setPageTitle(getResource("AddPerson"));
        LoadAddEntityDefaultOptions(systemConfig.ApplicationURL_Common + systemConfig.pageURLs.PersonList);
        $('.save-tab-lnk').show();
        $('.cancel-tab-lnk').show();
        $('input.NotifyForBirthday').attr('checked', false);
        $('input.NotifyForAnniversary').attr('checked', false);
        //$('input.NotifyForBirthday ').attr('checkeed', true);
        //$('input.NotifyForAnniversary').attr('checkeed', true);
        $('.search-box').hide();
    }
    else {
        //Advanced search Data
        mKey = 'pers';
        InnerPage_Load2();
        $('.QuickSearch-holder').show();
        //Advanced search Data


        setPageTitle(getResource("Person"));
        setAddLinkURL(systemConfig.ApplicationURL + systemConfig.pageURLs.AddPerson, getResource("AddPerson"));
        GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.PersonListItemTemplate, loadData, 'ItemListTemplate');
        //GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.RelatedOpportunities, null, 'RelatedOpportunities')
        GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.RelatedGiveawaysListViewItemTemplate, null, 'RelatedGiveawaysTemplate')
        GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.RelatedCaseListViewItemTemplate, null, 'RelatedCaseTemplate')
        GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.RelatedOpportunityListViewItemTemplate, null, 'RelatedOpportunityTemplate')
        $('.result-block').live("click", function () {
            PersonId = $(this).parent().attr('id');
        });

        deletedEntityCallBack = function () {
            removeDeletedEntity();
        };

        updatedEntityCallBack = function () {
            getEntityData(selectedEntityId);
        };


        $('li:.email-tabs').live('click', function () {
            EmailTemplate = $(EmailTemplate);
            checkPermissions(this, systemEntities.EmailEntityId, -1, EmailTemplate, function (template) {
                getEmails(systemEntities.PersonEntityId, selectedEntityId);
            });
        });
        $('li:.note-tabs').live('click', function () {
            NoteTemplate = $(NoteTemplate);
            checkPermissions(this, systemEntities.NoteEntityId, -1, NoteTemplate, function (template) {
                getNotes(systemEntities.PersonEntityId, selectedEntityId);
            });
        });
        $('li:.address-tabs').live('click', function () {
            AddressTemplate = $(AddressTemplate);
            checkPermissions(this, systemEntities.AddressEntityId, -1, AddressTemplate, function (template) {
                getAddresses(systemEntities.PersonEntityId, selectedEntityId);
            });
        });
        $('li:.phone-tabs').live('click', function () {
            PhoneTemplate = $(PhoneTemplate);
            checkPermissions(this, systemEntities.PhoneEntityId, -1, PhoneTemplate, function (template) {
                getPhones(systemEntities.PersonEntityId, selectedEntityId);
            });
        });
        $('li:.Attach-tabs').live('click', function () {

            checkPermissions(this, systemEntities.AttachmentsEntityId, -1, $('.Action-div'), function (template) {
                getAttachments(systemEntities.PersonEntityId, selectedEntityId, 'Person', systemEntities.PersonEntityId);
            });
        });


        $('li:.Communication-tab').live('click', function () {
            checkPermissions(this, systemEntities.TaskId, -1, $('.Action-div'), function (template) {
                var personItem = { label: PersonName, value: selectedEntityId };
                if (CompanyId > 0) {
                    personItem.Company = CompanyName;
                    personItem.CompanyId = CompanyId;
                }
                getCommunication(selectedEntityId, systemEntities.PersonEntityId, selectedEntityId, PersonName, personItem);
            });
        });
        $('li:.Giveaways-tabs').live('click', function () {

            checkPermissions(this, systemEntities.GiveawaysEntityId, -1, RelatedGiveawaysTemplate, function (template) {
                RelatedGiveawaysTemplate = template;
                getRelatedGiveAways(selectedEntityId);
            });
        });
        $('li:.Cases-tabs').live('click', function () {

            checkPermissions(this, systemEntities.ClientCareEntityId, -1, RelatedCaseTemplate, function (template) {
                RelatedCaseTemplate = template;
                getRelatedCase(selectedEntityId);
            });
        });

        $('li:.Opportunity-tabs').live('click', function () {

            checkPermissions(this, systemEntities.OpportunityEntityId, -1, RelatedOpportunityTemplate, function (template) {
                RelatedOpportunityTemplate = template;
                getRelatedOpportunities(selectedEntityId);
            });
        });

        $('li:.SocialMedia-tabs').live('click', function () {

            SocialMediaTemplate = $(SocialMediaTemplate);
            checkPermissions(this, systemEntities.SocialMediaEntityID, -1, SocialMediaTemplate, function (template) {
                getSocialMedia(systemEntities.PersonEntityId, selectedEntityId);
            });
        });

       // $('li:.ProjectsOpporunities-tabs').live('click', function () { getRelatedOpportunities(selectedEntityId); });
        //$('li:.Cases-tabs').live('click', function () { });
       // $('li:.Appliances-tabs').live('click', function () { });
    }
}

function notifyForBirthDateCheckd(isChecked, editMood) {
    if (isChecked) {
        $('.DateOfBirth').addClass('required');
        if (editMood)
            $('.BirthdayRequired').addClass('viewedit-display-none');
        else
            $('.BirthdayRequired').show();

    }
    else {
        $('.DateOfBirth').removeClass('required');
        if (editMood)
            $('.BirthdayRequired').removeClass('viewedit-display-none');
        else
            $('.BirthdayRequired').hide();

    }
}

function notifyForAnniversaryCheckd(isChecked, editMood) {
    if (isChecked) {
        $('.Anniversary').addClass('required');
        if (editMood)
            $('.AnniversaryRequired').addClass('viewedit-display-none');
        else
            $('.AnniversaryRequired').show();
    }
    else {
        $('.Anniversary').removeClass('required');
        if (editMood)
            $('.AnniversaryRequired').removeClass('viewedit-display-none');
        else
            $('.AnniversaryRequired').hide();
    }
}

function loadData(data) {

    GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.PersonListViewItemTemplate, function () {
        checkPermissions(this, systemEntities.SendMassMail, -1, PersonViewItemTemplate, function (returnedData) {

        });

        checkPermissions(this, systemEntities.PersonEntityId, -1, PersonViewItemTemplate, function (returnedData) {
            PersonViewItemTemplate = returnedData;
            loadEntityServiceURL = PersonServiceURL + "FindByIdLite";
            entityItemListTemplate = data;
            entityItemViewTemplate = returnedData;
            findAllEntityServiceURL = PersonServiceURL + "FindAllLite";
            deleteEntityServiceURL = PersonServiceURL + "Delete";
            selectEntityCallBack = getPersonInfo;
            loadDefaultEntityOptions();
        });
    }, 'PersonViewItemTemplate');
}

function initControls() {
    SetPhoneMask($('.phone-format'));
 
    if (!addPersonMode)
        $('.historyUpdated').append(HistoryUpdatedListViewTemplate.clone());
    else
        CountryAC = $('select:.Country').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.CountryServiceURL + "FindAll", required: false, autoLoad: false });

    preventInputChars('input:.integer');
    LoadDatePicker('input:.date', 'dd-mm-yy');
    CompanyAC = $('select:.Company').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.CompanyServiceURL + 'FindAllLite' });
    PrimaryContactEmployeeAC = $('select:.PrimaryContactEmployee').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.UsersWebService + 'FindUserAllLite' });
    VipClassAC = fillDataTypeContentList('select:.VipClass', systemConfig.dataTypes.VipClass, false, false);

    if (addPersonMode)
        VipClassAC.set_Item({ label: getResource('VIPNormal'), value: systemConfig.dataTypes.NormalVipClass });
    //VipClassAC.defaultItemLoaded = true;
    //systemConfig.dataTypes.VipClass
    AccountManagerAC = $('select:.AccountManager').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.UsersWebService + 'FindUserAllLite' });
    BestTimeToContactAC = fillDataTypeContentList('select:.BestTimeToContact', systemConfig.dataTypes.BestTimeToContact, false, false);
    HearAboutUsAC = fillDataTypeContentList('select:.HearAboutUs', systemConfig.dataTypes.Source, false, false);

    SourceAC = fillDataTypeContentList('select:.Source', systemConfig.dataTypes.Source, false, false);
    SalutationAC = fillDataTypeContentList('select:.Salutation', systemConfig.dataTypes.Salutation, false, false);
    MaritalStatusAC = fillDataTypeContentList('select:.MaritalStatus', systemConfig.dataTypes.MaritalStatus, false, false);
    DepartmentAC = fillDataTypeContentList('select:.Department', systemConfig.dataTypes.Department, false, false);
    PositionAC = fillDataTypeContentList('select:.Position', systemConfig.dataTypes.Position, false, false);
    GenderAC = fillDataTypeContentList('select:.Gender', systemConfig.dataTypes.Gender, false, false);
    ReligionAC = fillDataTypeContentList('select:.Religion', systemConfig.dataTypes.Religion, false, false);
    ContactTypeAC = fillDataTypeContentList('select:.ContactType', systemConfig.dataTypes.ContactType, false, false);
    InterestedInAC = fillDataTypeContentList('select:.InterestedIn', systemConfig.dataTypes.InterestedIn, false, true);
    NationalityAC = $('select:.Nationality').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.CountryServiceURL + "FindAll", required: false });
    if (getQSKey('cId')) {
        if (getQSKey('cId') > 0) {
            setCompany(getQSKey('cId'));
            $('a:.company-link').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.CompanyList + "?key=" + Number(getQSKey('cId')));
        }
    }

    $('input.NotifyForBirthday').bind('change', function () {
        if ($('input.NotifyForBirthday').attr('checked')) {
            $('input.NotifyForBirthday').attr('checkeed', true);
            notifyForBirthDateCheckd(true, false);
        }
        else {
            $('input.NotifyForBirthday').attr('checkeed', false);
            notifyForBirthDateCheckd(false, false);
        }
    });
    $('input.NotifyForAnniversary').bind('change', function () {
        if ($('input.NotifyForAnniversary').attr('checked')) {
            $('input.NotifyForAnniversary').attr('checkeed', true);
            notifyForAnniversaryCheckd(true, false);
        }
        else {
            $('input.NotifyForAnniversary').attr('checkeed', false);
            notifyForAnniversaryCheckd(false, false);
        }
    });

    if (addPersonMode) {
        var Label = getResource('Customer');
        if (Lang == systemConfig.WebKeys.Ar)
            Label = getResource('Customer');
        ContactTypeAC.set_Item({ label: Label, value: 690 });
    }
    $('input.BusinessEmail').bind('blur', function () {
        if ($(this).val() != '') {

            CheckEmailDuplication($(this).val(), 1);
            IsBusinessEmail = true;
            IsBusinessEmailDuplication = false;
        }
    });
    $('input.PersonalEmail ').bind('blur', function () {
        if ($(this).val() != '') {
            CheckEmailDuplication($(this).val(), 2);
            IsPersonalEmail = true;
            IsPersonalEmailDupliaction = false;
        }
    });
    $('input.MphoneNumber').bind('blur', function () {
        var countryCode = $('input.McountryCode').val();
        var areaCode = $('input.MareaCode').val();
        var phoneNumber = $('input.MphoneNumber').val();
        var mobileNumber = countryCode + areaCode + phoneNumber;
        if (mobileNumber != '') {
            IsMobileDuplication = false;
            CheckMobilelDuplication(mobileNumber.trim());
        }
    });

}

function getPhoneFormat(obj) {


    var MCountryCode;
    if (obj.CountryMobileNumber != null && obj.CountryMobileNumber != "") {
        if (obj.CountryMobileNumber.substring(0, 2) == '00')
            MCountryCode = "(" + obj.CountryMobileNumber + ")";
        else
            MCountryCode = "+(" + obj.CountryMobileNumber + ")";
    }
    else
        MCountryCode = "";

    //var MCountryCode = obj.CountryMobileNumber != null && obj.CountryMobileNumber != "" ? "+(" + obj.CountryMobileNumber + ")" : "";
    var MAreaCode = obj.AreaMobileNumber != null && obj.AreaMobileNumber != "" ? obj.AreaMobileNumber + "-" : "";
    var MFullPhoneNumber = obj.MobileNumber != null ? obj.MobileNumber : "";
    var FullPhoneNumber = MCountryCode + MAreaCode + MFullPhoneNumber;
    $('.MobileNumber').text(FullPhoneNumber);


    var HCountryCode;
    if (obj.CountryHomeNumber != null && obj.CountryHomeNumber != "") {
        if (obj.CountryHomeNumber.substring(0, 2) == '00')
            HCountryCode = "(" + obj.CountryHomeNumber + ")";
        else
            HCountryCode = "+(" + obj.CountryHomeNumber + ")";
    }
    else
        HCountryCode = "";
    //var HCountryCode = obj.CountryHomeNumber != null && obj.CountryHomeNumber != "" ? "+(" + obj.CountryHomeNumber + ")" : "";
    var HAreaCode = obj.AreaHomeNumber != null && obj.AreaHomeNumber != "" ? obj.AreaHomeNumber + "-" : "";
    var HFullPhoneNumber = obj.HomeNumber != null ? obj.HomeNumber : "";
    var FullPhoneNumber = HCountryCode + HAreaCode + HFullPhoneNumber;
    $('.HomeNumber').text(FullPhoneNumber);

    var FCountryCode;
    if (obj.CountryFaxNumber != null && obj.CountryFaxNumber != "") {
        if (obj.CountryFaxNumber.substring(0, 2) == '00')
            FCountryCode = "(" + obj.CountryFaxNumber + ")";
        else
            FCountryCode = "+(" + obj.CountryFaxNumber + ")";
    }
    else
        FCountryCode = "";
    // var FCountryCode = obj.CountryFaxNumber != null && obj.CountryFaxNumber != "" ? "+(" + obj.CountryFaxNumber + ")" : "";
    var FAreaCode = obj.AreaFaxNumber != null && obj.AreaFaxNumber != "" ? obj.AreaFaxNumber + "-" : "";
    var FFullPhoneNumber = obj.FaxNumber != null ? obj.FaxNumber : "";
    var FullPhoneNumber = FCountryCode + FAreaCode + FFullPhoneNumber;
    $('.FaxNumber').text(FullPhoneNumber);

}

function getPersonInfo(obj) {
    if (obj != null) {
        $('.Business-email-to').attr('href', 'mailto:' + obj.BusinessEmail);
        $('.Personal-email-to').attr('href', 'mailto:' + obj.PersonalEmail);
        getPhoneFormat(obj);
        CompanyName = obj.Company;
        CompanyId = obj.CompanyId;
        PersonId = obj.PersonId;
        ContactEmployeeName = obj.PrimaryContactEmployee;
        ContactEmployeeId = obj.PrimaryContactEmployeeValue;

        PersonName = obj.FullNameEn;


        if (obj.IntresetedInList != null) {
            InterestedInAC.set_Items(obj.IntresetedInList);
            $.each(obj.IntresetedInList, function (index, item) {
                var label = (Lang == systemConfig.WebKeys.Ar) ? item.DataTypeContentAr : item.DataTypeContent;
                $('.InterestedIn').text($('.InterestedIn').html() + label + ',');
            });
            $('.InterestedIn').text($('.InterestedIn').html().slice(0, $('.InterestedIn').html().length - 1));
        }
        // setListItemTemplateValue(selectedEntityItemList, obj);

        $('a:.Website-link').attr('href', 'http://' + obj.Website);
        $('.Website').text('http://' + obj.Website);
        $('a:.Company-link').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.CompanyList + "?key=" + obj.CompanyId);
        $('a:.PrimaryContactEmployee').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.UserList + "?key=" + ContactEmployeeId);
        //$('a:.sendEmail-link').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.SendNotifications + '?pid=' + selectedEntityId);
        $('.sendEmail-link').click(function () {
            window.location = systemConfig.ApplicationURL + systemConfig.pageURLs.SendNotifications + '?pid=' + selectedEntityId;

        });
        $('input.IsVip').attr('checkeed', obj.IsVip);
        $('input.NotifyForBirthday').attr('checked', obj.NotifyForBirthday);
        //$('input.NotifyForAnniversary').attr('checkeed', obj.NotifyForAnniversary);
        $('input.NotifyForAnniversary').attr('checked', obj.NotifyForAnniversary);
        $('span:.NotifyForBirthday').text(getResource(obj.NotifyForBirthday));
        $('span:.NotifyForAnniversary').text(getResource(obj.NotifyForAnniversary));
        $('a:.AccountManager-link').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.UserList + "?key=" + obj.AccountManagerId);

        if (obj.NotifyForAnniversary)
            notifyForAnniversaryCheckd(true, true);
        else
            notifyForAnniversaryCheckd(false, true);

        if (obj.NotifyForBirthday)
            notifyForBirthDateCheckd(true, true);
        else
            notifyForBirthDateCheckd(false, true);

    }
}

function setListItemTemplateValue(entityTemplate, entityItem) {
    var countryCode = entityItem.CountryMobileNumber != null && entityItem.CountryMobileNumber != "" ? "+(" + entityItem.CountryMobileNumber + ")" : "";
    var areaCode = entityItem.AreaMobileNumber != null && entityItem.AreaMobileNumber != "" ? entityItem.AreaMobileNumber + "-" : "";
    var PhoneNumber = entityItem.MobileNumber != null && entityItem.MobileNumber != "" ? entityItem.MobileNumber : "";
    var FullPhoneNumber = countryCode + areaCode + PhoneNumber;
    PersonName = entityItem.FullNameEn;
    entityTemplate.find('.result-data1 .PersonNameTrim').text(Truncate(PersonName, 20));
    entityTemplate.find('.result-data2 .PositionTitle').text(Truncate(entityItem.Position, 50));
    entityTemplate.find('.result-data3 label').text((entityItem.BusinessEmail != null) ? ' ' + Truncate(entityItem.BusinessEmail, 20) : '');
    var BEmail = entityItem.BusinessEmail != null ? entityItem.BusinessEmail : '';
    entityTemplate.find('.result-data3 .HBusiness-email-to').attr('href', 'mailto:' + BEmail);
    entityTemplate.find('.result-data4 label').text(FullPhoneNumber); // textFormat((entityItem.MobileNumber != null) ? entityItem.MobileNumber : (entityItem.PhoneNumber != null) ? entityItem.PhoneNumber : '');
    $('.HeaderPersonName').text(Truncate(PersonName, 20));
    $('.HeaderPersonEmail').text(Truncate((entityItem.BusinessEmail != null) ? ' ' + entityItem.BusinessEmail : '', 20));
    return entityTemplate;
}

function setListItemServiceData(keyword, pageNumber, resultCount) {
    if (!QuickSearch) {
        return formatString('{ keyword: "{0}", page:{1}, resultCount:{2} ,argsCriteria:"{ PersonId:{3}}"}', keyword, pageNumber, resultCount, filterEntityId);
    }
    else {
        return formatString('{mKey:"{0}",SearchCriteriaObj:"{1}",SortType:{2},SortFields:"{3}",TotalRecords:{4},PageNumber:{5}}', mKey, SearchCriteriaObj, SortType, SortFields, resultCount, pageNumber);
    }
}



function SaveEntity() {
    var validForm = true;
    if (!saveForm($('.Person-info')))
        validForm = false;

    if (addPersonMode) {
        if (IsBusinessEmailDuplication) {
            addPopupMessage($('input:.BusinessEmail'), getResource("AlreadyExist"));
            validForm = false;
        }
        if (IsPersonalEmailDupliaction) {
            addPopupMessage($('input:.PersonalEmail'), getResource("AlreadyExist"));
            validForm = false;
        }
        if (IsMobileDuplication) {
            addPopupMessage($('input.MphoneNumber'), getResource("AlreadyExist"));
            validForm = false;
        }

    }



    if (!validForm)
        return false;
    var PersonId = selectedEntityId;
    var lastUpdatedDate = null;

    if (addPersonMode) {
        showLoading($('#demo1'));
    }
    else {
        showLoading($('.TabbedPanels'));
        lastUpdatedDate = getEntityLockDateFormat(currentEntityLastUpdatedDate);
    }
    if ($('input.DateOfBirth').val() != "") {
        dateOfBirth = getDate("dd-mm-yy", getDotNetDateFormat($('input.DateOfBirth').val()));
        if (DateDiff(getDate("dd-mm-yy", new Date()), dateOfBirth) > 0) {
            addPopupMessage($('input.DateOfBirth'), getResource("DateOfBirthError"));
            hideLoading();
            return false;            
         }
    }
    var PersonObj = {

        CompanyId: CompanyAC.get_ItemValue(),
        FirstNameEn: $('input.FirstNameEn').val().trim(),
        MiddleNameEn: $('input.MiddleNameEn').val().trim(),
        LastNameEn: $('input.LastNameEn').val().trim(),
        FirstNameAr: $('input.FirstNameAr').val().trim(),
        MiddleNameAr: $('input.MiddleNameAr').val().trim(),
        LastNameAr: $('input.LastNameAr').val().trim(),
        FullNameEn: $('input.FullNameEn').val(),
        FullNameAr: $('input.FullNameAr').val(),
        NickName: $('input.NickName').val(),
        PrimaryContactEmployee: PrimaryContactEmployeeAC.get_ItemValue(),
        SalutationId: SalutationAC.get_ItemValue(),
        GenderId: GenderAC.get_ItemValue(),
        SourceId: SourceAC.get_ItemValue(),
        DateOfBirth: getDate("mm-dd-yy", getDotNetDateFormat($('input.DateOfBirth').val())),
        ReligionId: ReligionAC.get_ItemValue(),
        MaritalStatusId: MaritalStatusAC.get_ItemValue(),
        DepartmentId: DepartmentAC.get_ItemValue(),
        PositionId: PositionAC.get_ItemValue(),
        ContactImage: -1,
        InterstedIn: InterestedInAC.get_ItemsIds(),
        Anniversary: getDate("mm-dd-yy", getDotNetDateFormat($('input:.Anniversary').val())),
        NationalityId: NationalityAC.get_ItemValue(),
        ContactTypeId: ContactTypeAC.get_ItemValue(),
        NotifyForBirthday: $('input.NotifyForBirthday').is(':checked'),
        NotifyForAnniversary: $('input.NotifyForAnniversary').is(':checked'),
        //IsVip: $('input.IsVip').is(':checked'),
        VipClassId: VipClassAC.get_ItemValue(),
        HearAboutUsId: HearAboutUsAC.get_ItemValue(),
        BestTimeToContactId: BestTimeToContactAC.get_ItemValue(),
        AccountManagerId: AccountManagerAC.get_ItemValue(),
        FacebookPage: $('input.FacebookPage').val(),
        LinkedInPage: $('input.LinkedInPage').val(),
        TwitterAccount: $('input.TwitterAccount').val(),
        Website: $('input.Website').val()

    };
    
    var data = formatString('{id:{0}, PersonInfo:"{1}"}', selectedEntityId, escape(JSON.stringify(PersonObj)));

    if (PersonId > 0)
        url = PersonServiceURL + "Edit";
    else
        url = PersonServiceURL + "Add";

    post(url, data, saveSuccess, saveError, getEntitylockAjaxHeaders(lastUpdatedDate));

}

function DateDiff(date1, date2) {
    var Startdate = new Date(date1.replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
    var EndDate = new Date(date2.replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
    return EndDate.getTime() - Startdate.getTime();
}

function saveSuccess(returnedValue) {

    if (returnedValue.statusCode.Code == 501) {
        hideLoading();
        addPopupMessage($('input.FirstNameEn'), getResource("DublicatatedFullName"));
       // addPopupMessage($('input.FirstNameAr'), getResource("DublicatatedFullName"));
        return false;
    }
    else if (returnedValue.statusCode.Code == 0) {
        hideLoading();
        if (returnedValue.statusCode.message == 'validation' && returnedValue.result) {
            filterError(returnedValue.result);
        }
        showStatusMsg(getResource("FaildToSave"));
        return false;
    }
    PersonId = returnedValue.result;
    if (!addPersonMode) {
        showStatusMsg(getResource("savedsuccessfully"));
        hideLoading();
        loadEntityData(loadEntityServiceURL, entityItemViewTemplate, selectedEntityId, getPersonInfo);

    }
    else {
        savePersonContactInfo(PersonId, CountryAC);
    }

    IsBusinessEmailDuplication = false;
    IsPersonalEmailDupliaction = false;
}

function saveError(ex) {
    hideLoading();
    handleLockEntityError(ex, deletedEntityCallBack, updatedEntityCallBack);
}



function filterError(errors) {
    var allMsg = '';
    $('.validation').remove();
    $.each(errors, function (index, error) {
        var template = $('.Person-info');
        if (error.GlobalMessage) {
            allMsg += '<span>' + error.Message + '<br/></span>';
        }
        else if (error.ControlName == 'CompanyId') {
            addPopupMessage(template.find('select.CompanyId'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'FirstNameEn') {
            addPopupMessage(template.find('input.FirstNameEn'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'MiddleNameEn') {
            addPopupMessage(template.find('input.MiddleNameEn'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'LastNameEn') {
            addPopupMessage(template.find('input.LastNameEn'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'FirstNameAr') {
            addPopupMessage(template.find('input.FirstNameAr'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'MiddleNameAr') {
            addPopupMessage(template.find('input.MiddleNameAr'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'LastNameAr') {
            addPopupMessage(template.find('input.LastNameAr'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'FullNameEn') {
            addPopupMessage(template.find('input.FullNameEn'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'FullNameAr') {
            addPopupMessage(template.find('input.FullNameAr'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'NickName') {
            addPopupMessage(template.find('input.NickName'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'SalesManId') {
            addPopupMessage(template.find('select.SalesMan'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'PersonType') {
            addPopupMessage(template.find('select.PersonType'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'PrimaryContactEmployee') {
            addPopupMessage(template.find('input.PrimaryContactEmployee'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'SalutationId') {
            addPopupMessage(template.find('select.Salutation'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'GenderId') {
            addPopupMessage(template.find('select.Gender'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'SourceId') {
            addPopupMessage(template.find('select.Source'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'DateOfBirth') {
            addPopupMessage(template.find('input.DateOfBirth'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'ReligionId') {
            addPopupMessage(template.find('select.Religion'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'MaritalStatusId') {
            addPopupMessage(template.find('select.MaritalStatus'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'DepartmentId') {
            addPopupMessage(template.find('select.Department'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'PositionId') {
            addPopupMessage(template.find('select.Position'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'ContactImageId') {
            addPopupMessage(template.find('select.ContactImage'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'CustomerReferralCompanyId') {
            addPopupMessage(template.find('select.CustomerReferralCompany'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'CustomerReferralPersonId') {
            addPopupMessage(template.find('select.CustomerReferralPerson'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'EmployeeUserId') {
            addPopupMessage(template.find('select.EmployeeUser'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'ScoutingComments') {
            addPopupMessage(template.find('textarea.ScoutingComments'), getResource(error.ResourceKey));
        }

    });

    hideLoading();
    if (allMsg.trim()) {
        showOkayMsg('Validation error', allMsg.trim());
    }
}


function savePersonContactInfo(PersonId, CountryAC) {

    var AddressObj = {
        AddressID: -1,
        CountryId: CountryAC.get_ItemValue(),
        CityText: escape($('input:.City').val().trim()),
        BuildingNumber: ($('input:.BuildingNumber').val() != null && $('input:.BuildingNumber').val() != "") ? $('input:.BuildingNumber').val() : -1,
        StreetAddress: escape($('input:.StreetAddress').val().trim()),
        ZipCode: escape($('input:.ZipCode').val().trim()),
        POBox: escape($('input:.POBox').val().trim()),
        NearBy: escape($('input:.NearBy').val().trim()),
        Place: escape($('input:.Place').val().trim()),
        Area: escape($('input:.Area').val().trim()),
        Region: escape($('input:.Region').val().trim())
     
    };

    var HomePhone = {
        PhoneID: -1,
        PhoneNumber: $('input:.PphoneNumber').val().trim(),
        AreaCode: $('input:.PareaCode').val().trim(),
        CountryCode: $('input:.PcountryCode').val().trim(),
        Type: 1
    };

    var FaxPhone = {
        PhoneID: -1,
        PhoneNumber: $('input:.SphoneNumber').val().trim(),
        AreaCode: $('input:.SareaCode').val().trim(),
        CountryCode: $('input:.ScountryCode').val().trim(),
        Type: 2
    };

    var MobilePhone = {
        PhoneID: -1,
        PhoneNumber: $('input:.MphoneNumber').val().trim(),
        AreaCode: $('input:.MareaCode').val().trim(),
        CountryCode: $('input:.McountryCode').val().trim(),
        Type: 3
    };


    var BusinessEmail = {
        Email_Id: -1,
        Type: 1,
        EmailAddress: $('input:.BusinessEmail').val().trim()
    };

    var PersonalEmail = {
        Email_Id: -1,
        Type: 2,
        EmailAddress: $('input:.PersonalEmail').val().trim()
    };



    var url = PersonServiceURL + "savePersonContactInfo";
    var data = formatString('{PersonId:{0}, AddressObj:"{1}",HomePhoneObj:"{2}",FaxPhone:"{3}",MobilePhone:"{4}",BusinessEmail:"{5}",PersonalEmail:"{6}"}', PersonId,
      escape(JSON.stringify(AddressObj)),
      escape(JSON.stringify(HomePhone)),
      escape(JSON.stringify(FaxPhone)),
      escape(JSON.stringify(MobilePhone)),
      escape(JSON.stringify(BusinessEmail)),
      escape(JSON.stringify(PersonalEmail))
     );


    post(url, data,
     function (returnedValue) {
         window.location = systemConfig.ApplicationURL + systemConfig.pageURLs.PersonList + "?key=" + PersonId + "&mood=add";
     }, saveError, '');

 }


 function setCompany(companyId) {
     var url = systemConfig.ApplicationURL_Common + systemConfig.CompanyServiceURL + "FindByIdLite";
     var data = formatString('{ id: "{0}" }', Number(companyId));
     post(url, data,
     function (data) {
         companobj = data.result;
         CompanyAC.set_Item({ label: companobj.label, value: companobj.CompanyId });
         CompanyWorkFlow(companobj.CompanyNameEnglish);
     }, '', '');
   }






   function CheckEmailDuplication(Email,Type) {
       var data = formatString('{ email: "{0}",EmailType:"{1}" }', Email, Type);
       var url = systemConfig.ApplicationURL_Common + systemConfig.PersonsServiceURL + "CheckEmailDuplication"
       post(url, data, DuplicationSuccess, DuplicationError, null);

   }

   function DuplicationSuccess(returnedValue) {
       if (returnedValue != null) {
           if (returnedValue.statusCode.Code == 501) {
               hideLoading();
               if (IsBusinessEmail) {

                   IsBusinessEmailDuplication = true;
               }
               if (IsPersonalEmail) {

                   IsPersonalEmailDupliaction = true;
               }

           }
           IsBusinessEmail = false;
           IsPersonalEmail = false;
       }
   }

   function DuplicationError() {

   }


   function CheckMobilelDuplication(PhoneNumber) {
       var data = formatString('{ PhoneNumber: "{0}" }', PhoneNumber);
       var url = systemConfig.ApplicationURL_Common + systemConfig.PersonsServiceURL + "CheckMobileDuplication"
       post(url, data, DuplicationMobileSuccess, DuplicationMobileError, null);

   }


   function DuplicationMobileSuccess(returndValue) {
       if (returndValue != null) {
           if (returndValue.statusCode.Code == 501) {
               hideLoading();
               IsMobileDuplication = true;

           }
       }

   }


   function DuplicationMobileError() { }

   function CompanyWorkFlow(CompanyName) {

       $('.company-filed').show();
       $('.company-list').hide();
       $('.Company').text(CompanyName);
       $('.company-required').hide();
      // $('.person-required').hide();
   }


   function checkPermissions(Sender, EntityId, ParentId, Template, Callback) {
       if ($(Sender).attr('pChecked') != 'checked') {
           //$(Sender).attr('pChecked', 'checked');
           var data = formatString('{EntityId:{0},ParentId:{1}}', EntityId, ParentId);
           $.ajax({
               type: "POST",
               contentType: "application/json; charset=utf-8",
               url: PemissionWebServiceURL + "checkUnAvailablePermissions",
               data: data,
               datatype: "json",
               success: function (data) {
                   var returnedData = eval("(" + data.d + ")");
                   if (returnedData.statusCode.Code == 1) {
                       if (returnedData.result != null) {
                           UnAvailablePermissionsList = returnedData.result;
                           Template = ApplyUserPermissions(Template);
                       }
                       else {
                           $('.add-entity-button').show();
                           $('.TabbedPanelsContent:visible').find('.Tab-action-header').show();
                       }
                   }
                   else {
                       //if 0: then there is no permissions at all
                       // alert('no access');
                   }
                   if (Callback) {
                       Callback(Template);
                   }
               },
               error: function (e) { }
           });
       }
       else {
           if (Callback)
               Callback(Template);
       }
   }




   
