
function getRelatedOpportunities(PersonId) {
    $('.OpportunityTemp-tab').children().remove();
    showLoading($('.TabbedPanels'));
    var url = systemConfig.ApplicationURL_Common + systemConfig.OpportunityServiceURL + "FindAllLite";
    var data = formatString('{ keyword: "", page:1, resultCount:50,argsCriteria:"{PersonId:{0}}"}', PersonId);
    post(url, data, function (returnedData) {
        var RelatedObj = returnedData.result;
        AddRelatedSalesTab(null, PersonId);
        if (RelatedObj != null) {
            $('.OpportunityTemp-tab').parent().find('.DragDropBackground').remove();
            $.each(RelatedObj, function (index, item) {
                AddRelatedSalesTab(item, PersonId);
            });
        }
        hideLoading();
    });
}

function AddRelatedSalesTab(item, PersonId) {

    var template = $(RelatedOpportunityTemplate).clone();
    ViewRelatedSales(item, template, PersonId);
    if (item != null) {
        template.css('cursor', 'default');
        template = mapEntityInfoToControls(item, template);
        template.find('.Company').text(Truncate(item.Company, 25));
        template.find('.ProjectName').text(Truncate(item.ProjectName, 25));
        $('.OpportunityTemp-tab').append(template);
    }
}

function ViewRelatedSales(Item, template, PersonId) {
    var id = (Item != null) ? Item.Id : 0;
    $('.Tab-action-header').show();
    $('a:.add-new-Opportunity').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.OpportunityList + "?pid=" + PersonId);
    if (Item != null) {
        template.find('a:.company-link').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.CompanyList + "?key=" + Item.CompanyId);
        template.find('a:.sales-link').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.OpportunityList + "?key=" + Item.OpportunityId);
        template.find('a:.person-link').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.PersonList + "?key=" + Item.PersonId);

    }
}
