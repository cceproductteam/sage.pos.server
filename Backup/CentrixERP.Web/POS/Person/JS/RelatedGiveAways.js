﻿
function getRelatedGiveAways(PersonId) {
    $('.GiveAwaysTemp-tab').children().remove();
    showLoading($('.TabbedPanels'));
    $.ajax({
        type: "POST",
        url: systemConfig.ApplicationURL_Common + systemConfig.GiveawaysServiceURL + "FindAllLite",
        contentType: "application/json; charset=utf-8",
        data: formatString('{ keyword: "", page:1, resultCount:10,argsCriteria:"{PersonId:{0}}"}', PersonId),
        dataType: "json",
        success: function (data) {
            var RelatedObj = eval("(" + data.d + ")").result;
            AddRelatedGiveAwaysTab(null, PersonId);
            if (RelatedObj != null) {
                $('.GiveAwaysTemp-tab').parent().find('.DragDropBackground').remove();
                $.each(RelatedObj, function (index, item) {
                    AddRelatedGiveAwaysTab(item, PersonId);
                });
            }
            hideLoading();
        },
        error: function (e) {
        }
    });
}

function AddRelatedGiveAwaysTab(item, PersonId) {
    var template = $(RelatedGiveawaysTemplate).clone();
    template.find('.field-person').hide();
    ViewRelatedGiveAway(item, template, PersonId);
    if (item != null) {
        template.css('cursor', 'default');
        template = mapEntityInfoToControls(item, template);
        template.find('.Company').text(Truncate(item.Company, 20));
        template.find('.Branch').text(Truncate(item.Branch, 20));
        template.find('.SalesRepresentative').text(Truncate(item.SalesRepresentative, 20));
        $('.GiveAwaysTemp-tab').append(template);
    }
    
}

function ViewRelatedGiveAway(Item, template, PersonId) {
    var id = (Item != null) ? Item.Id : 0;
    $('a:.add-new-Giveaways').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.GiveawaysList + "?pid=" + PersonId);
    if (Item != null) {
        template.find('a:.company-link').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.CompanyList + "?key=" + Item.CompanyId);
        template.find('a:.person-link').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.PersonList + "?key=" + Item.PersonId);
        template.find('a:.Giveaway-link').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.GiveawaysList + "?key=" + Item.GiveawaysId);
        
    }
}
