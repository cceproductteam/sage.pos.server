<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddSyncSchedule.aspx.cs" MasterPageFile="~/Common/MasterPage/ERPSite.master"
    Inherits="Centrix.POS.Server.Web.Server.SyncSchedule.AddSyncSchedule" %>
    
<asp:Content runat="server" ContentPlaceHolderID="PageHeader" ID="PageHeader">
    <script type="text/javascript" src="JS/SyncSchedule.js?v=<%=Centrix_Version %>"></script>
    <script src="../../Common/JScript/CommonTabs.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <script type="text/javascript">
        addSyncScheduleMode = true;
           
    </script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="PageBody" ID="PageBody">
    <div id='content' class="SyncSchedule-info">
        <div class="Add-new-form">
            <div class="Add-title">
                <span resourcekey="SyncScheduleDetails"> </span>
                    <div class="Action-div">
                        <ul>
                            <li><span class="ActionSave-link save-tab-lnk"></span><a class="ActionSave-link save-tab-lnk">
                                <span resourcekey="Save"> </span> </a> </li>
                            <li><span class="ActionCancel-link cancel-tab-lnk"></span><a class="ActionCancel-link cancel-tab-lnk">
                                <span resourcekey="Cancel"> </span> </a> </li>
                        </ul>
                    </div>
            </div>
            <table border="0" cellpadding="0" cellspacing="0" class="Add-new-Table SyncSchedule-info">
            			<tr>
				<th>
					<span class="label-title" resourcekey="SyncEntities"></span>
				</th>
				<td>
					<textarea class="cx-control SyncEntities  txt " maxlength="500" minlength="3"></textarea>
				</td>
				<th>
					<span class="label-title" resourcekey="SyncEntitiesIds"></span>
				</th>
				<td>
					<textarea class="cx-control SyncEntitiesIds  txt " maxlength="500" minlength="3"></textarea>
				</td>
				<th>
					<span class="label-title" resourcekey="Status"></span>
				</th>
				<td>
					<input class="cx-control Status  txt   integer  " maxlength="" minlength="3"
                            type="text"  />
				</td>
			</tr>
            </table>
        </div>
        <div class="five-px">
        </div>
    </div>
</asp:Content>
