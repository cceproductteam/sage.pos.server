﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RegisterStatus.aspx.cs"
    MasterPageFile="~/Common/MasterPage/ERPSite.master" Inherits="Centrix.POS.Server.Web.POS.Register.RegisterStatus" %>

<asp:Content runat="server" ContentPlaceHolderID="PageHeader" ID="PageHeader">
    <script src="JS/Registerstatus.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <link href="../CSS/POSMain.css?v=<%=Centrix_Version %>" rel="stylesheet" type="text/css" />
    <link href="CSS/RegisterStatus.css?v=<%=Centrix_Version %>" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="PageBody" ID="PageBody">
    <div class="white-container" style="height: 560px;">
        <div class="white-container-data" style="height: 560px;">
            <div class="StoreSync-info-container RegisterStatus-Fieldset displayNone" style="height: 530px;">
                <div class="Stores-list-container" style="height: 510px;">
                    <div class="reg-regName-container" style="height: 500px;">
                    </div>
                </div>
               
            </div>
        </div>
        </div>
</asp:Content>