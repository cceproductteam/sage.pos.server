var RegisterServiceURL;
var RegisterViewItemTemplate;
var StoreAC;
var QuickKeysAC;

var ItemListTemplate;
var RegisterId = selectedEntityId;
var lastUpdatedDate = null;
var addRegisterMode = false;
var storeid = -1;
var addFromTab = false;

function InnerPage_Load() {
    setSelectedMenuItem($(".pos-icon"));
   
    RegisterServiceURL = systemConfig.ApplicationURL_Common + systemConfig.RegisterServiceURL;
    currentEntity = systemEntities.RegisterEntityId;


    if (getQSKey('sId') && getQSKey('sId') > 0) {
        storeid = Number(getQSKey('sId'));
        addFromTab = true;
    }
    //Advanced search Data
    mKey = 'Register'; //replace it with entity modual key
    InnerPage_Load2();
    $('.QuickSearch-holder').show();
    LoadSearchForm2(mKey, false);
    //Advanced search Data

    setPageTitle(getResource("Register"));
    setAddLinkAction(getResource("AddRegister"), 'add-new-item-large');

    GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.AddRegisterItemTemplate, function () {
        GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.RegisterListItemTemplate, loadData, 'ItemListTemplate');
    }, 'AddItemTemplate');

    $('.result-block').live("click", function () {
        RegisterId = $(this).attr('id');
    });

    deletedEntityCallBack = function () {
        removeDeletedEntity();
    };

    updatedEntityCallBack = function () {
        getEntityData(selectedEntityId);
    };



}

function loadData(data) {

    GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.RegisterListViewItemTemplate, function () {
        checkPermissions(this, systemEntities.RegisterEntityId, -1, RegisterViewItemTemplate, function (returnedData) {
            RegisterViewItemTemplate = returnedData;
            loadEntityServiceURL = RegisterServiceURL + "FindByIdLite";
            entityItemListTemplate = data;
            entityItemViewTemplate = returnedData;
            findAllEntityServiceURL = RegisterServiceURL + "FindAllLite";
            deleteEntityServiceURL = RegisterServiceURL + "Delete";
            selectEntityCallBack = getRegisterInfo;
            listEntityCallBack = callBackAfterList;
            loadDefaultEntityOptions();

        });
    }, 'RegisterViewItemTemplate');

}

function initControls(template, isAdd) {
    if (!template) template = $('.TabbedPanels');
    if (!isAdd)
        $('.historyUpdated').append(HistoryUpdatedListViewTemplate.clone());
    LoadDatePicker(template.find('input:.date'), 'dd-mm-yy');
    template.find('.numeric').autoNumeric();
    preventInputChars(template.find('input:.integer'));
    StoreAC = template.find('select:.Store').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.StoreServiceURL + 'FindAllLite', required: true, multiSelect: false });
    QuickKeysAC = template.find('select:.QuickKeys').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.QuickKeysServiceURL + 'FindAllLite', required: false, multiSelect: false });

}


function initAddControls(template) {
    initControls(template, true);

    if (getQSKey('sid') > 0 || getQSKey('fdb') > 0) {
        setStoreObj();
    }
}

function getRegisterInfo(item) {

    if (item != null) {
        $('a:.Store-link').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.StoreList + "?key=" + item.StoreId);
        $('a:.QuickKeys-link').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.QuickKeysList + "?key=" + item.QuickKeysId);
    }
    if ($('.result-block').length > 0 && getQSKey('fdb') > 0 && isFirstTimeAdd) {
        $('.add-entity-button').first().click();
    }
}

function setListItemTemplateValue(entityTemplate, entityItem) {
    entityTemplate.find('.result-data1 label').text(Truncate(entityItem.RegisterName, 20));
    entityTemplate.find('.result-data2 label').textFormat(Truncate(entityItem.RegisterNameAr, 20));
    entityTemplate.find('.result-data3 label').textFormat(Truncate(entityItem.RegiserCode, 20));
    return entityTemplate;
}

function setListItemServiceData(keyword, pageNumber, resultCount) {
    if (!QuickSearch) {
        return formatString('{ keyword: "{0}", page:{1}, resultCount:{2} ,argsCriteria:"{ RegisterId:{3}}"}', keyword, pageNumber, resultCount, filterEntityId);
    }
    else {
        return formatString('{mKey:"{0}",SearchCriteriaObj:"{1}",SortType:{2},SortFields:"{3}",TotalRecords:{4},PageNumber:{5}}', mKey, SearchCriteriaObj, SortType, SortFields, resultCount, pageNumber);
    }
}



function SaveEntity(isAdd, template) {
    var controlTemplate;
    var valid = true;

    if (isAdd) {
        controlTemplate = $('.add-Register-info');
        showLoading('.add-new-item');

    }
    else {
        controlTemplate = $('.Register-info');
        showLoading('.Result-info-container');
    }


    if (!saveForm(controlTemplate)) {
        hideLoading();
        valid = false;
    }

    addRegisterMode = isAdd;
    if (!valid) {
        hideLoading();
        return false;
    }

    RegisterId = -1;

    if (!isAdd) {
        RegisterId = selectedEntityId;
        lastUpdatedDate = getEntityLockDateFormat(currentEntityLastUpdatedDate);
    }

    var RegisterObj = {
        RegisterName: controlTemplate.find('input.RegisterName').val(),
        RegisterNameAr: controlTemplate.find('input.RegisterNameAr').val(),
        RegiserCode: controlTemplate.find('input.RegiserCode').val(),
        StoreId: StoreAC.get_ItemValue(),
        CashLoan: controlTemplate.find('input.CashLoan').autoNumericGet(),
        QuickKeysId: QuickKeysAC.get_ItemValue(),
        IpAddress: controlTemplate.find('input.IpAddress').val(),
        MacAddress: controlTemplate.find('input.MacAddress').val(),
        DataBaseInstanceName: controlTemplate.find('input.DataBaseInstanceName').val(),
        DataBaseName: controlTemplate.find('input.DataBaseName').val(),
        DataBaseUserName: controlTemplate.find('input.DataBaseUserName').val(),
        DataBasePassword: controlTemplate.find('input.DataBasePassword').val()
        //         BranchId: controlTemplate.find('input.BranchId').val(),
        //         InvoiceNumberFormat: controlTemplate.find('input.InvoiceNumberFormat').val(),
        //         LaybyNumberFormat: controlTemplate.find('input.LaybyNumberFormat').val(),
        //         PrintRecipt: controlTemplate.find('input:.PrintRecipt').is(':checked'),
        //         EmailRecipt: controlTemplate.find('input:.EmailRecipt').is(':checked'),
        //         IsUsed: controlTemplate.find('input:.IsUsed').is(':checked'),
        //         IsNew: controlTemplate.find('input:.IsNew').is(':checked')
    };
    var data = formatString('{id:{0}, RegisterInfo:"{1}"}', isAdd ? -1 : selectedEntityId, escape(JSON.stringify(RegisterObj)));

    if (RegisterId > 0)
        url = RegisterServiceURL + "Edit";
    else
        url = RegisterServiceURL + "Add";

    post(url, data, function (returnedValue) { saveSuccess(controlTemplate, returnedValue); }, saveError, getEntitylockAjaxHeaders(lastUpdatedDate));
}

function saveSuccess(template, returnedValue) {
    var RegisterObject = returnedValue.result;
    if (returnedValue.statusCode.Code == 501) {
        addPopupMessage(template.find('input.RegiserCode'), getResource('AlreadyExist'));
        //  showStatusMsg(getResource("error"));
        hideLoading();
        return false;
    }
    else if (returnedValue.statusCode.Code == 0) {
        hideLoading();
        if (returnedValue.statusCode.message == 'validation' && returnedValue.result) {
            filterError(returnedValue.result, template);
        }
        showStatusMsg(getResource("FaildToSave"));
        return false;
    }

    if (!addRegisterMode) {
        showStatusMsg(getResource("savedsuccessfully"));
        hideLoading();
        loadEntityData(loadEntityServiceURL, entityItemViewTemplate, selectedEntityId, getRegisterInfo);
    }
    else {
        hideLoading();
        addRegisterMode = false;
        $('.add-new-item').hide();
        $('.add-new-item').empty();
        appendNewListRow(returnedValue.result);
        showStatusMsg(getResource("savedsuccessfully"));
        addFromTab = false;
        loadEntityData(loadEntityServiceURL, entityItemViewTemplate, selectedEntityId, getRegisterInfo);

    }
}

function saveError(ex) {
    hideLoading();
    handleLockEntityError(ex, deletedEntityCallBack, updatedEntityCallBack);
}



function filterError(errors, template) {
    var allMsg = '';
    template.find('.validation').remove();
    $.each(errors, function (index, error) {
        if (error.GlobalMessage) {
            allMsg += '<span>' + error.Message + '<br/></span>';
        }
        else if (error.ControlName == 'RegisterName') {
            addPopupMessage(template.find('input.RegisterName'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'RegisterNameAr') {
            addPopupMessage(template.find('input.RegisterNameAr'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'RegiserCode') {
            addPopupMessage(template.find('input.RegiserCode'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'Store') {
            addPopupMessage(template.find('select.Store'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'CashLoan') {
            addPopupMessage(template.find('input.CashLoan'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'QuickKeys') {
            addPopupMessage(template.find('select.QuickKeys'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'IpAddress') {
            addPopupMessage(template.find('input.IpAddress'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'MacAddress') {
            addPopupMessage(template.find('input.MacAddress'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'DataBaseInstanceName') {
            addPopupMessage(template.find('input.DataBaseInstanceName'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'DataBaseName') {
            addPopupMessage(template.find('input.DataBaseName'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'DataBaseUserName') {
            addPopupMessage(template.find('input.DataBaseUserName'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'DataBasePassword') {
            addPopupMessage(template.find('input.DataBasePassword'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'Branch') {
            addPopupMessage(template.find('input.Branch'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'InvoiceNumberFormat') {
            addPopupMessage(template.find('input.InvoiceNumberFormat'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'LaybyNumberFormat') {
            addPopupMessage(template.find('input.LaybyNumberFormat'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'PrintRecipt') {
            addPopupMessage(template.find('input.PrintRecipt'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'EmailRecipt') {
            addPopupMessage(template.find('input.EmailRecipt'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'IsUsed') {
            addPopupMessage(template.find('input.IsUsed'), getResource(error.ResourceKey));
        }
        else if (error.ControlName == 'IsNew') {
            addPopupMessage(template.find('input.IsNew'), getResource(error.ResourceKey));
        }

    });

    hideLoading();
    if (allMsg.trim()) {
        showOkayMsg('Validation error', allMsg.trim());
    }
}

function callBackAfterList() {
    if ($('.result-block').length <= 0 && getQSKey('fdb') > 0 && isFirstTimeAdd) {
        $('.add-entity-button').first().click();
    }

    if (addFromTab) {
        $('.add-entity-button').first().click();
        getStoreInfo(storeid);
        hideLoading();
        return false;
    }
}



function getStoreInfo(StoreId) {
    $.ajax({
        type: "POST",
        url: systemConfig.ApplicationURL_Common + systemConfig.StoreServiceURL + "FindByIdLite",
        contentType: "application/json; charset=utf-8",
        data: formatString('{ id: "{0}" }', Number(StoreId)),
        dataType: "json",
        success: function (data) {

            var returnedValue = eval("(" + data.d + ")");
            storeobj = returnedValue.result;
            StoreAC.set_Item({ label: storeobj.StoreName, value: storeobj.StoreId });
        },
        error: {}
    });

}


function loadDefaultEntityOptions() {
    pendingAjaxRequest = false;
    resultCount = 30;
    $('.show-more-results-div').hide();
    $('.result-block').live("click", function () {
        $('.result-block').removeClass('SelectedTr');
        $('.add-new-item').hide();
        $('.add-new-item').html('');
        //Added by ruba 14-10-2014
        $(".black_overlay").hide();
        $(this).addClass('SelectedTr');
        var timmer = 0;
        try {
            var timmer = ($('.Filter-search').position().top == 0) ? 400 : 0;
            $('.Filter-search').animate({
                top: filterSearchTop
            }, 500);
        }
        catch (err) {

        }


        $('.TabbedPanelsTab').removeAttr('pChecked');
        selectedEntityId = $(this).attr('id');
        selectedEntityItemList = $(this);
        window.setTimeout(function () {
            if (!(getQSKey('sId') && getQSKey('sId') > 0))
                getEntityData(selectedEntityId);
        }, timmer);
    });

    $('.MoreSearchOptions').click(function () {
        if ($('.MoreSearchOptions').text() == getResource("HideOptions")) {
            QuickSearch = false;
            $('.Quick-Search-Div').hide();
            $(this).html("<a class='more-options'>" + getResource('MoreOptions') + "</a>");
            $('.GridTableDiv-body').height($('.GridTableDiv-body').height() + 154);
        }
        else {

            $('.Quick-Search-Div').show();
            $(this).html("<a class='more-options'>" + getResource('HideOptions') + "</a>");
            $('.GridTableDiv-body').height($('.GridTableDiv-body').height() - 154);
        }

    });

    $('#demo2').alternateScroll('remove');

    //  if (Lang != "ar")
    $("#demo2").resizable({
        handles: 'e',
        resize: function (event, ui) { }

    });

    $('.GridTableDiv-header table').find('th').each(function ($index) {
        $(".GridTableDiv-body table tr td:eq(" + $index + ")").width($(this).width());
    });



    $('#demo2').on("resize", function (event, ui) {
        var demo2width = $('#demo2').width();
        var demo2parentWidth = $('#demo2').offsetParent().width();
        var demo2percent = 100 * demo2width / demo2parentWidth;
        var widthinpercentage = 98 - demo2percent;
        $('.Result-info-container').css("width", widthinpercentage + "%");
        $('.GridTableDiv-header table').find('th').each(function ($index) {
            $(".GridTableDiv-body table tr td:eq(" + $index + ")").width($(this).width());
        });
        if ($('.lbl-invoice-number').length > 0) { $('.lbl-invoice-number').css('width', $('.th-entry-number').width()); }
        if ($('.lbl-invoice-number2').length > 0) { $('.lbl-invoice-number2').css('width', $('.th-invoice-number').width()); }
        if ($('.result-container').width() <= 530) { $('.SD-ul').children('li').addClass('FullWidth100'); }
        else { $('.SD-ul').children('li').removeClass('FullWidth100'); }

    });



    $('.ui-resizable-e').dblclick(function () {
        if (max_min) {
            $('#demo2').width(parseInt($('#demo2').css('max-width')) + '%');
            max_min = false;
        }
        else {
            $('#demo2').width(parseInt($('#demo2').css('min-width')) + '%');
            max_min = true;
        }
        window.setTimeout(function () {
            var demo2width = $('#demo2').width();
            var demo2parentWidth = $('#demo2').offsetParent().width();
            var demo2percent = 100 * demo2width / demo2parentWidth;
            var widthinpercentage = 98 - demo2percent;
            $('.Result-info-container').css("width", widthinpercentage + "%");
            $('.GridTableDiv-header table').find('th').each(function ($index) {
                $(".GridTableDiv-body table tr td:eq(" + $index + ")").width($(this).width());
            });
        }, 100);

    });

    loadKeyDownActions();
    $('.ActionEdit-link').live("click", function () {
        editingRecord = false;

        showLoading($('.TabbedPanels'));
        $('.add-new-item').hide();
        $('.add-new-item').html('');
        lockEntity(currentEntity, selectedEntityId, function () {
            editingRecord = true;
            $('.viewedit-display-block').hide();
            $('.viewedit-display-none').show();
            $('.TabbedPanelsContent').find('.cx-control:visible').first().focus();
            if (afterEditCallBack) afterEditCallBack();
        }, currentEntityLastUpdatedDate, function () {
            loadEntityData(loadEntityServiceURL, entityItemViewTemplate, selectedEntityId, function (dataObject) {
                currentEntityLastUpdatedDate = dataObject.UpdatedDate || dataObject.Updated_Date;
                if (selectEntityCallBack) selectEntityCallBack(dataObject); $('.TabbedPanelsTab:first').click();
            });
        }, deletedEntityCallBack);
    });


    $('.ActionDelete-link').live('click', function () {
        if (IsIntegratedEntity) {
            showStatusMsg(IntegratedDeleteMsg);
            return false;
        }
        else {
            showConfirmMsg(DeleteTitleMessage, DeleteMessage, function () {
                showLoading($('.TabbedPanels'));
                lockForDelete(currentEntity, selectedEntityId, function () {
                    deleteEntity(selectedEntityId);
                }, deletedEntityCallBack);
            });
            return false;
        }
    });

    $('.ActionSave-link').live('click', function () {
        // editingRecord = false;
        SaveEntity();
        $('.TabbedPanelsTab').removeAttr('pChecked');
        return false;
    });

    $('.Action-div').live('click', function (e) {

    });

    //Edited By Ruba Al-Sa'di to prevent executing this event on clicking on Cancel Button in Add Form
    //$('.ActionCancel-link').live('click', function () {
    $('.Result-info-container').find('.ActionCancel-link').live('click', function () {
        //Cancel button of default tab (Summary tab)

        //Edited by Ruba Al-Sa'di 4/11/2014
        var EditingOrAddingInProcess = CheckIfAddingOrEditingIsInProcess();

        if (EditingOrAddingInProcess && !AddMode) {
            showConfirmMsg(getResource("Warning"), getResource('AreYouSureYouWantToCancel'), function () {
                loadEntityData(loadEntityServiceURL, entityItemViewTemplate, selectedEntityId, selectEntityCallBack);
                UnlockEntity(currentEntity, selectedEntityId, function () {
                    editingRecord = false;
                    loadEntityData(loadEntityServiceURL, entityItemViewTemplate, selectedEntityId, selectEntityCallBack);
                });
                return false;
            }, function () {
                return false;
            });
        }
        else {
            if (!AddMode) {
                loadEntityData(loadEntityServiceURL, entityItemViewTemplate, selectedEntityId, selectEntityCallBack);
                UnlockEntity(currentEntity, selectedEntityId, function () {
                    loadEntityData(loadEntityServiceURL, entityItemViewTemplate, selectedEntityId, selectEntityCallBack);
                });
            }
            return false;
        }

        //        loadEntityData(loadEntityServiceURL, entityItemViewTemplate, selectedEntityId, selectEntityCallBack);
        //        UnlockEntity(currentEntity, selectedEntityId, function () {
        //            editingRecord = false;
        //            loadEntityData(loadEntityServiceURL, entityItemViewTemplate, selectedEntityId, selectEntityCallBack);
        //        });
    });

    $('.GridTableDiv-body').scroll(function () {
        loadMoreAction = true;
        if (($('.GridTableDiv-body').outerHeight() == Math.round(($('.GridTableDiv-body').get(0).scrollHeight - $('.GridTableDiv-body').scrollTop()) + .1, 2)) && !lastRowResultGrid && $('.myCustomLoader').length != 1) {
            pageNumber++;
            showMore = true;
            if (!QuickSearch)
                ListEntityItems($('.searchTextbox').val().trim());
            else
                ListEntityItems('');
        }
    });
    ListEntityItems('');
    SetSearchTextBoxEvent();
}
