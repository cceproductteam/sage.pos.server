using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CentrixERP.Common.Web;
using CentrixERP.Common.Business.Entity;
using E = CentrixERP.Business.Entity;
using IM = CentrixERP.Business.IManager;
using SF.Framework;
using SF.CustomScriptControls;
using CentrixERP.Configuration;

namespace Centrix.POS.Server.Web.POS.SystemConfigration
{
    public partial class SystemConfigration : BasePageLoggedIn
    {
        public SystemConfigration()
            : base(Enums_S3.Permissions.SystemModules.None) { }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}