var currentFolderId = 0;
var currentFolder, currentFolderName = null;
var ajaxDeletePageURL = "../WebServices/DocumentWebService.asmx/DeleteDocument";
var rowToDelete = '#divDocument';
var confirmMsgText = 'Are you Sure?';
var docFolderServiceURL;
var AttachmentsPermissionsList = '';
var hasViewPermission = false;
var hasAddPermission = false;
var hasDeletePermission = false;
var hasRenamePermission = false;
var webServiceURL = '../WebServices/DocumentFolderWebService.asmx/';
var fromCalendar = false;


function InnerPage_Load() {
    fromCalendar = getQSKey('fromCalendar') == 1;

    AttachmentsPermissionsList = eval("(" + PermissionList + ")");
    checkAttachmentPermissions();

    $('.folderscontainer').css('height', $('.folderscontainer').height($(window).height() - 12));
    $('#Filesorder').height($(window).height() - 48);
    $('#Filesorder').alternateScroll();

    //if he has view permission then continue
    if (hasViewPermission) {
        currentFolder = $('.tree-view');
        AddFolderNode(currentFolder, { folderId: 0, name: "" }, 'root-folder', true);
        currentFolder = $('.root-folder');

        getFoldersS(currentFolder, 0);

        $(".btnAdd").parent().click(function () {
            if ($(this).hasClass('clicked')) {
                $(".add-text-box").hide();
                $("#txtAddSubFolder").val('');
                //$('.btnsave').hide();
                $(this).removeClass('clicked');
            }
            else {
                $(".add-text-box").show();
                $('.btnsave').show();
                //$(this).removeClass('clicked');
                $(this).addClass('clicked');
                $('.new-folder-textbox').focus();
                removeFieldRequirdMsg();
            }
            return false;
        });

        $(".new-folder-textbox").live('keydown', function (e) {
            removeFieldRequirdMsg();
            if (e.keyCode == 13)
                SaveFolders();
        });

        $('#txtAddSubFolder').keydown(function (e) {
            if (e.which == 13 || e.keyCode == 13)
                e.preventDefault();
        });

        //        $('.deleteFolder').parent().click(function () {
        //            if (currentFolderId > 0)
        //                deleteFolders(currentFolderId);
        //            return false;
        //        });
        $('.deleteFolder').parent().click(function () {

            if (currentFolderId > 0)
                deleteFolders(currentFolderId);
            else
                showOkayMsg(getResource("DeleteFolder"), getResource("CantDeleteRoot"));
            return false;
        });

        $(".rename-folder").parent().click(function () {

            if (currentFolderId > 0) {
                $(currentFolder).children('span.folder-name').hide();
                $(currentFolder).children('.replacedInput').show().val($(currentFolder).children('span.folder-name').text()).focus();

                $('.replacedInput').blur(function () {

                    var newFolderName = $(currentFolder).children('.replacedInput').val().trim();

                    if (!newFolderName) {
                        newFolderName = currentFolderName;
                        $(this).val(newFolderName);
                    }
                    else {
                        RenameFolder(newFolderName);
                    }
                    $(this).text(newFolderName).hide();
                    $(this).siblings('span.folder-name').text(newFolderName).show();
                });

            }
            else
                showOkayMsg(getResource("RenameFolder"), getResource("CantRenameRoot"));

            return false;
        });

        $("div.whiteBG").live('mouseover',
    function () {
        $(this).children(".delete-file").css('display', 'block');
    });
        $("div.whiteBG").live('mouseout', function () {
            $(this).children(".delete-file").css('display', 'none');
        });
    }

}



function searchDocuments(parentId, moduleKey, folderId, ModuleName) {
    currentFolderId = folderId;
    //$('.existfiles').empty();
    showLoadingMsg()
    $.ajax({
        type: "POST",
        url: systemConfig.ApplicationURL_Common + "Common/Documents/DocumentHtml5Ajax.aspx",
        data: "id=" + parentId + "&key=" + moduleKey + "&folderId=" + folderId + "&modulename=" + ModuleName,
        success: function (data) {
            $('.existfiles').html(data);
            if (!hasDeletePermission) $('.delete-file').remove();
            hideLoadingMsg();
        },
        error: function () {
            hideLoadingMsg();
        }
    });
}

function getFolders(subFoldersJSON, parentFolder, show) {
    jQuery.each(subFoldersJSON, function (index, folder) {
        AddFolderNode(parentFolder, folder, '', !index);

    });
}

function AddFolderNode(parentFolder, folderObj, cssClass, setAsCurrentFolder) {

    var folderName = Truncate(folderObj.name, 300);

    var node = $('<li>').addClass(cssClass)
 .append(
    $('<span class="foldersArrow arrowClosedFolder">')

    .click(function () {
        showLoadingMsg();
        var parentLI = $(this).parents('li:first');
        //$('#fileList').empty().hide();

        if ($(this).hasClass("arrowOpenedFolder")) {
            $(this).removeClass('arrowOpenedFolder').addClass('arrowClosedFolder folder-loaded');
            parentLI.find('.folder-img').removeClass('open-folder').addClass('closed-folder');
            parentLI.find('.sub-folder:first').hide();
            //parentLI.removeClass('folder-open');
        }

        else {
            $(this).removeClass('arrowClosedFolder').addClass('arrowOpenedFolder').show();
            parentLI.find('.folder-img:first').removeClass('closed-folder').addClass('open-folder');


            currentFolder = parentLI;
            currentFolderId = folderObj.folderId;
            $('#pnlDocumentsList').empty();
            if (!$(this).hasClass('folder-loaded')) {
                parentLI.find('.sub-folder:first').children().remove();
                getFoldersS(parentLI, folderObj.folderId);
            }
            //else
            parentLI.find('.sub-folder:first').show();

            kw(slickUpload).set_Data("parentDocumentFolderId", folderObj.folderId);


        }
        //return false;

    })


    ,

    $('<span>').addClass('folder-img ' + ((cssClass == 'root-folder') ? 'open-folder' : 'closed-folder'))
        ,
    $('<input type=text>').addClass('replacedInput').click(function () {
        return false;
    }),
    $('<span>').addClass('folder-name').html(folderName).attr('title', folderObj.name)
    )

    .click(function () {

        $('#fileList').hide();
        $('#fileList').find('.su-filelistitem').hide();

        $('#fileList').empty();

        ////if ($('#fileList').find('.whiteBG:[folderId="' + folderObj.folderId + '"]').length) {
        //    $('.whiteBG:[folderId="' + folderObj.folderId + '"]').parents('.su-filelistitem').show();
        //    $('#fileList').show();
        //}

        //if($('.whiteBG:[folderId="' + folderId + '"]').parents('.su-filelistitem:first').remove();

        var parentLI = $(this)//.parents('li:first');

        //if (!parentLI.hasClass('folderSelected'))
        getFiles(folderObj.folderId);

        $('.folderSelected').removeClass('folderSelected');
        parentLI.addClass('folderSelected');
        parentLI.find('.sub-folder').css('background-color', '#FFF !important');
        currentFolder = parentLI;
        currentFolderId = folderObj.folderId;
        kw(slickUpload).set_Data("parentDocumentFolderId", folderObj.folderId);


        return false;
    }).dblclick(function () { return false; });


    var padding = '0px 0px';
    if (cssClass == 'root-folder') {
        node.find('.foldersArrow').addClass('arrowOpenedFolder').show();
        node.find('.folder-name').addClass('folder-lvl1');
        node.find('.deleteFolder').remove();
        node.addClass('folderSelected');
    }
    else {
        padding = '0px 15px';
        node.find('.folder-name').addClass('folder-lvl2');
        node.addClass('closed-folder');
    }


    node.append($('<ul>').addClass('sub-folder').hide());


    ///find('.folder-img').removeClass('closed-folder').addClass('open-folder')
    if ($(parentFolder).find('.folder-img').hasClass('open-folder') || $(parentFolder).hasClass('root-folder') || $(parentFolder).hasClass('tree-view'))
        $(parentFolder).find('.sub-folder:first').attr({ style: 'padding : ' + padding + '; background-color:white;' }).append(node).show();
    else
        $(parentFolder).find('.sub-folder:first').attr({ style: 'padding : ' + padding + '; background-color:white;' }).append(node).hide();

    if (setAsCurrentFolder) {
        $('.folderSelected').removeClass('folderSelected');
        if (cssClass == 'root-folder')
            $(parentFolder).find('li:first').addClass('folderSelected');
        else
            $(parentFolder).addClass('folderSelected');
    }

    return;
}

function getFoldersS(folder, folderId) {
    showLoadingMsg();
    $.ajax({
        type: "post",
        contentType: "application/json; charset=utf-8",
        url: webServiceURL + "getSubFolders",
        data: '{"folderId":' + folderId + ',"entityId":' + EntityID + ',parentEntityId:' + parentEntityId + '}',
        dataType: "json",
        success: function (e) {
            if (e.d != null) {
                getFolders(eval(e.d), folder, true);
            }

            getFiles(folderId);
        },
        error: function (e) {
            hideLoadingMsg();
            //OnFailed();
        }
    });

}

function getFiles(folderId) {
    searchDocuments(EntityID, moduleKey, folderId, moduleName);
}

function SaveFolders() {

    var orignalName = $("#txtAddSubFolder").val().trim();
    FolderName = escape($("#txtAddSubFolder").val().trim());
    if (!FolderName) {
        // addPopupMessage($("#txtAddSubFolder"), getResource("required"),false);
        return false;
    }
    showLoadingMsg();
    $.ajax({
        type: "post",
        contentType: "application/json; charset=utf-8",
        url: AjaxAddFolderLogically,
        data: '{ name: "' + FolderName + '",entityId:' + EntityID + ', parentEntityId:' + parentEntityId + ',createdBy:' + userId + ',moduleName:"' + moduleName + '",parentFolderId:' + currentFolderId + '}',
        dataType: "json",
        success: function (data) {
            if (data.d != null) {
                result = eval('(' + data.d + ')');
                //$(currentFolder).find('.sub-folder').empty();
                var folderObj = {
                    folderId: result[result.length - 1].folderId,
                    name: orignalName
                };

                AddFolderNode(currentFolder, folderObj, '', false);
                //getFoldersS(currentFolder, currentFolderId);
                //$('.btnsave').hide();
                $('.new-folder-textbox').val('');
                hideLoadingMsg();

                $(".add-text-box").hide();
                //$('.btnsave').hide();
                $(this).removeClass('clicked');
                $('.doc-AddFolder-icons').removeClass('clicked');
            }
        },
        error: function (e) {
            hideLoadingMsg();
            // alert(e.responseText)

        }
    });

}


function deleteFolders(folderId) {

    showConfirmMsg(getResource("DeleteFolder"), getResource("Areyousure"), function () {
        showLoadingMsg();
        $.ajax({
            type: "post",
            contentType: "application/json; charset=utf-8",
            url: webServiceURL + "DeleteFolder",
            data: '{ parentId : "' + parentId + '",moduleName :"' + moduleName + '", entityId: "' + EntityID + '",folderId: "' + folderId + '",createdBy:"' + userId + '"}',
            dataType: "json",
            success: function (data) {
                $('.existfiles').empty();
                $('.whiteBG:[folderId="' + folderId + '"]').parents('.su-filelistitem:first').remove();
                var cFolder = $(currentFolder);
                $(cFolder).parents('li:first').removeClass('open-folder').addClass('closed-folder').click();
                cFolder.remove();
                hideLoadingMsg();
            },
            error: function (e) {
                //alert(e.responseText)
                hideLoadingMsg();

            }
        });
    });

}



function DeleteDocument(fileEl, id) {
    showConfirmMsg(getResource("DeleteDocument"), getResource("Areyousure"), function () {
        showLoadingMsg();
        $.ajax({
            type: "post",
            contentType: "application/json; charset=utf-8",
            url: ajaxDeletePageURL,
            data: '{ id: "' + id + '",moduleKey: "' + moduleKey + '" }',
            dataType: "json",
            success: function (e) {
                $('.whiteBG:[docId="' + id + '"]').parents('.su-filelistitem:first').remove();
                $('.whiteBG:[docId="' + id + '"]').remove();
                //$(fileEl).parents('.inline-divs:first').remove();
                DocumentCount = Number(DocumentCount) - 1;
                if (typeof parent.removeAttachmentSign === "function" && fromCalendar) {
                    parent.removeAttachmentSign(Number(DocumentCount));
                }
                hideLoadingMsg();
            },
            error: function (e) {
                //OnFailed();
                //$(rowToDelete + id).html(OriginalData);
                hideLoadingMsg();

            }
        });
    });

}

function RenameFolder(newFolderName) {
    showLoadingMsg();
    $.ajax({
        type: 'POST',
        data: '{FolderId : ' + currentFolderId + ', FolderName : "' + newFolderName + '"}',
        url: webServiceURL + 'RenameFolder',
        dataType: 'json',
        contentType: 'application/json; charset:utf-8',
        success: function (data) {
            hideLoadingMsg();
        },
        error: function (ex) {
            hideLoadingMsg();
        }
    });
}


function Truncate(text, length) {
    if (text) {
        var truncatedText = '';
        length = !length ? 50 : length;
        truncatedText = text.length > length ? text.substring(0, length) + " ..." : text;
        return truncatedText;
    }
    else
        return "";
}

function showLoadingMsg() {
    if (fromCalendar)
        parent.showLoadingWithoutBG($('.Mid-section', parent.document));
    else
        parent.showLoading($('.TabbedPanels', parent.document));
}

function hideLoadingMsg() {
    parent.hideLoading();
}




function checkAttachmentPermissions() {

    if (AttachmentsPermissionsList != null) {
        $.each(AttachmentsPermissionsList, function (index, item) {

            if (item.PermissionId == permissions.ViewId && item.HasPermission) hasViewPermission = true;
            if (!item.HasPermission)
                $('.folderscontainer').find(item.CssClass).parent().remove();

            if (!hasViewPermission) {
                $(".btnAdd").parent().remove();
                $('.deleteFolder').parent().remove();
                $(".rename-folder").parent().remove();
                parent.showStatusMsg('You Dont Have Permissions for this page');

            }
            if (item.PermissionId == permissions.AddId && item.HasPermission) hasAddPermission = true;
            if (item.PermissionId == permissions.DeleteId && item.HasPermission) hasDeletePermission = true;
            if (item.PermissionId == permissions.RenameFolderId && item.HasPermission) hasRenamePermission = true;
        });

        $('.edit-pnl').show();
    }
    hideLoadingMsg();
}


function checkFunctionPermission(permission) {
    if (!permission) {
        showStatusMsg('You Dont Have Permissions for this page');
        return false;
    }
}

function removeFieldRequirdMsg() {
    $('.new-folder-textbox').addClass('cx-control-red-border');
    $('.validation').remove();
}

