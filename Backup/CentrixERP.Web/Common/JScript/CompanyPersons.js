function getCompanyPersons(companyId) {
    $('.companyPersons-tab').children().remove();

    var data = formatString('{keyword:{0}, from:"{1}", to:"{2}", lang:"{3}", UserId:"{4}", CompanyId:"{5}", NotIncludedPerson:"{6}"}', null, 0, 0, "en", UserId, companyId, 0);
    $.ajax({
        type: "POST",
        url: systemConfig.PersonsServiceURL + "FindAll",
        contentType: "application/json; charset=utf-8",
        data: data,
        dataType: "json",
        success: function (data) {
            var personObj = eval("(" + data.d + ")");
            if (personObj.result != null) {
                $('.companyPersons-tab').parent().find('.DragDropBackground').remove();
                $.each(personObj.result, function (index, item) {
                    var template = $(CompanyPersonsTemplate);
                    template = mapEntityInfoToControls(item, template);
                    $('.companyPersons-tab').append(template);

                });

                $('.viewedit-display-none').hide();

            }
        },
        error: function (e) {
        }
    });
}