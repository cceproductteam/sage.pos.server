function getRelationShips(ParentId) {
    $('.companyRelationShips-tab').children().remove();
    var data = formatString('{ParentId:{0}}', ParentId);
    $.ajax({
        type: "POST",
        url: systemConfig.CompanyRelationshipServiceURL + "FindAll",
        contentType: "application/json; charset=utf-8",
        data: data,
        dataType: "json",
        success: function (data) {
            var relationShipObj = eval("(" + data.d + ")");
            if (relationShipObj.result != null) {
                $('companyRelationShips-tab').parent().find('.DragDropBackground').remove();
                addRelationShipTab(null, ParentId);
                $.each(relationShipObj.result, function (index, item) {
                    addRelationShipTab(item, ParentId);
                });
                attachTabEvents();

            }
        },
        error: function (e) {
        }
    });
}


function addRelationShipTab(item, ParentId) {
    var id = (item != null) ? item.Id : 0;
    var template = $(CompanyRelationShipsTemplate);
    var RelatedCompanyAC = template.find('select:.RelatedCompany').AutoComplete({ serviceURL: systemConfig.CompanyServiceURL + "FindAll" });


    if (item != null) {
        template = mapEntityInfoToControls(item, template, [{ controlName: 'RelatedCompanyAC', control: RelatedCompanyAC}]);
    } else {
        template.hide();
        $('.add-new-companyRelationShip').click(function () {

            template.find('.itemheader').click();
            template.find('.viewedit-display-none').show();
            template.find('.viewedit-display-block').hide();
            template.find('input').val('');
            RelatedCompanyAC.clear();
            template.find('.edit-tab-lnk').remove();
            template.find('.save-tab-lnk').show();
            template.find('.cancel-tab-lnk').show();
            template.find('.delete-tab-lnk').hide();
            template.find('.validation').remove();
            if (template.is(':visible')) {
                template.hide();
            }
            else {
                template.show();
            }
            return false;
        });
    }


    template.find('.save-tab-lnk').on('click', function () {
        saveRelationShip(id, ParentId, template, RelatedCompanyAC);
        template.find('.edit-tab-lnk').show();
        return false;
    });


    template.find('.delete-tab-lnk').click(function () {
        deleteRelationShip(id, ParentId);
        return false;
    });

    template.find('.edit-tab-lnk').click(function () {
        template.find('.edit-tab-lnk').hide();
        template.find('.save-tab-lnk').show();
        template.find('.cancel-tab-lnk').show();
        template.find('.viewedit-display-none').show();
        template.find('.viewedit-display-block').hide();
        return false;
    });

    template.find('.cancel-tab-lnk').on('click', function () {
        template.find('.itemheader').click();
        getRelationShips(ParentId);
        template.find('.edit-tab-lnk').show();
        template.find('.save-tab-lnk').hide();
        return false;
    });



    $('.companyRelationShips-tab').append(template);

}


function saveRelationShip(id, parentId, template, relatedCompanyAC) {

    if (!saveForm(template, relatedCompanyAC)) {
        return;
    }

    var relationCompanyId = relatedCompanyAC.get_Item() != null ? relatedCompanyAC.get_Item().value : -1;
    var roles = template.find('input:.Role').val();
    var comments = template.find('input:.Comments').val();

    var data = '{CompanyId:{0}, RelationshipId:{1}, RelationCompanyId:"{2}",Role:"{3}",Comments:"{4}",userId:"{5}"}';
    data = formatString(data, parentId, id, relationCompanyId, roles, comments, UserId);

    $.ajax({
        type: "POST",
        url: systemConfig.CompanyRelationshipServiceURL + "AddEditCompanyRelationship",
        contentType: "application/json; charset=utf-8",
        data: data,
        dataType: "json",
        success: function (data) {
            getRelationShips(parentId);
        },
        error: function (e) {
        }
    });
}


function deleteRelationShip(id, parentId) {
    $.ajax({
        type: "POST",
        url: systemConfig.CompanyRelationshipServiceURL + "DeleteCompanyRelationship",
        contentType: "application/json; charset=utf-8",
        data: formatString('{id:{0}}', id),
        dataType: "json",
        success: function (data) {
            getRelationShips(parentId);
        },
        error: function (e) {
        }
    });
}
