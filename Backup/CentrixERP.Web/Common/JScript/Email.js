function getEmails(ParentId, mKey) {
    $('.email-tab').children().remove();
    var data = formatString('{ParentId:{0}, mKey:"{1}"}', ParentId, mKey);
    $.ajax({
        type: "POST",
        url: systemConfig.EmailServiceURL + "FindAll",
        contentType: "application/json; charset=utf-8",
        data: data,
        dataType: "json",
        success: function (data) {
            var emailObj = eval("(" + data.d + ")");
            addEmailTab(null, ParentId, mKey);
            if (emailObj.result != null) {
                $('.email-tab').parent().find('.DragDropBackground').remove();
                
                $.each(emailObj.result, function (index, item) {
                    addEmailTab(item, ParentId, mKey);
                });
               

            }
            else
                $('.TabbedPanelsContentGroup').css('min-height', $('.Result-info-container').height() - 31);
            attachTabEvents();
        },
        error: function (e) {
        }
    });
}


function addEmailTab(item, ParentId, mKey) {
    var id = (item != null) ? item.Id : 0;
    var isDefault = (item != null) ? item.Isdefault : false;

    var template = $(EmailTemplate);
    if (item != null) {
        template = mapEntityInfoToControls(item, template);
    } else {
        template.hide();
        $('.add-new-email').click(function () {

            template.find('.itemheader').click();
            template.find('.viewedit-display-none').show();
            template.find('.viewedit-display-block').hide();

            template.find('input').val('');

            template.find('.edit-tab-lnk').remove();
            template.find('.save-tab-lnk').show();
            template.find('.cancel-tab-lnk').show();
            template.find('.delete-tab-lnk').hide();
            template.find('.validation').remove();
            if (template.is(':visible')) {
                template.hide();
            }
            else {
                template.show();
            }
            return false;
        });
    }

    if (isDefault)
        template.find('.default-icon').addClass('default-iconVisited')

    template.find('.save-tab-lnk').on('click', function () {
        saveEmail(id, ParentId, mKey, template, isDefault);
        template.find('.edit-tab-lnk').show();
        return false;
    });


    template.find('.delete-tab-lnk').click(function () {
        deleteEmail(id, mKey, ParentId);
        return false;
    });


    template.find('.edit-tab-lnk').click(function () {
        template.find('.edit-tab-lnk').hide();
        template.find('.save-tab-lnk').show();
        template.find('.cancel-tab-lnk').show();
        template.find('.viewedit-display-none').show();
        template.find('.viewedit-display-block').hide();
        return false;
    })

    template.find('a.default-icon').click(function () {
        if (!$(this).hasClass('default-iconVisited')) {
            SetDefault(id, ParentId, mKey, EmailServiceURL + "SetEmailDefualt", true, UserId);
            $('.default-icon').removeClass('default-iconVisited');
            $(this).addClass('default-iconVisited');
        }
        return false;
    });

    template.find('.cancel-tab-lnk').on('click', function () {
        template.find('.itemheader').click();
        getEmails(ParentId, mKey);
        template.find('.edit-tab-lnk').show();
        template.find('.save-tab-lnk').hide();
        return false;
    });


    $('.email-tab').append(template);

}

function saveEmail(id, parentId, mKey, template, isDefault) {

    var emailAddress = template.find('input:.EmailAddress').val();

    var data = '{id:{0}, moduleId:{1}, emailAddress:"{2}",mKey:"{3}",isDefault:"{4}",userId:"{5}"}';
    data = formatString(data, id, parentId, emailAddress, mKey, isDefault, UserId);

    $.ajax({
        type: "POST",
        url: systemConfig.EmailServiceURL + "AddEditEmail",
        contentType: "application/json; charset=utf-8",
        data: data,
        dataType: "json",
        success: function (data) {
            getEmails(parentId, mKey);
        },
        error: function (e) {
        }
    });
}



function deleteEmail(id, mKey, parentId) {
    $.ajax({
        type: "POST",
        url: systemConfig.EmailServiceURL + "Delete",
        contentType: "application/json; charset=utf-8",
        data: formatString('{id:{0}, mKey:"{1}"}', id, mKey),
        dataType: "json",
        success: function (data) {
            getEmails(parentId, mKey);
        },
        error: function (e) {
        }
    });
}