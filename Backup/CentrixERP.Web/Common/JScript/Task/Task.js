var currentTask = null;
var DifferantDate = 0;

function Task(taskId) {

    this.TaskId = taskId;
    //this.ReferenceNo = null;
    this.StartDate = null;
    this.EndDate = null;
    this.Description = null;
    this.TypeId = null;
    this.PriorityId = null;
    this.StatusId = null;
    this.AllDay = false;
    //this.RecurrenceId = null;
    this.TaskColor = null;
    //this.CreatedBy = UserId;
    //this.UpdatedBy = UserId;
    this.TaskAssignmentList = new Array();
    this.TaskEntityList = new Array();
    this.TaskActivityList = new Array();

}

function TaskAssignment(taskId) {
    this.TaskId = taskId;
    this.TaskAssignmentId = -1;
    this.EntityId = null;
    this.EntityValueId = null;
    //this.StatusId = null;
    //this.CompletionPercentage = 0;
    //this.TaskColor = null;

    //this.CreatedBy = UserId;
    //this.UpdatedBy = UserId;
}

function TaskEntity(taskId) {
    this.TaskId = taskId;
    this.TaskEntityId = -1;
    this.EntityId = null;
    this.EntityValueId = null;
    // this.CreatedBy = UserId;
    //this.UpdatedBy = UserId;
}

function TaskActivity(taskId) {
    this.TaskId = taskId;
    this.TaskActivityId = -1;
    this.ActivityId = -1;
}



//////////////Task Object///////////////////////
///////////////////////////////////////////////


function getTaskObject(event, template, taskConfigValues) {

    var startTime = template.find('select:.lstStartDate').val()
    , endTime = template.find('select:.lstEndDate').val(),
    startDate = TaskListViewItemTemplate.find('input:.StartDate').val(),
    endDate = TaskListViewItemTemplate.find('input:.EndDate').val(),
    allDay = false;
    DifferantDate = DateDiff(startDate, endDate);
    if (DifferantDate < 0) {
        addPopupMessage(TaskListViewItemTemplate.find('input:.EndDate'), 'end date should be equal or after the start date');
    }
    else {
        var startTimeIndex = TaskListViewItemTemplate.find('select:.lstStartDate').find('option:selected').index();
        var endTimeIndex = TaskListViewItemTemplate.find('select:.lstEndDate').find('option:selected').index();
        if (endTimeIndex < startTimeIndex) {
            DifferantDate = -1;
            //addPopupMessage(TaskListViewItemTemplate.find('input:.EndDate'), 'end time should be equal or after the start time');
            addPopupMessageSmaller(TaskListViewItemTemplate.find('select:.lstEndDate'), 'end time should be equal or after the start time');
        }
    }
    allDay = (startTime == '--:--' && endTime == '--:--');


    startDate = getTaskDateFormat(startDate) + ' ' + (startTime == '--:--' ? '00:01' : startTime);
    //getDotNetDateFormat(startDate + ' ' + (startTime == '--:--' ? '' : startTime));
    endDate = getTaskDateFormat(endDate) + ' ' + (endTime == '--:--' ? '00:01' : endTime);
    //getDotNetDateFormat(endDate + ' ' + (endTime == '--:--' ? '11:30 pm' : endTime));



    currentTask = new Task(event.id);

    currentTask.StartDate = startDate;
    //getDotNetDateFormat(TaskListViewItemTemplate.find('input:.StartDate').val()  + ' ' + template.find('select:.lstStartDate').val());//  event.start;

    currentTask.EndDate = endDate;
    //getDotNetDateFormat(TaskListViewItemTemplate.find('input:.EndDate').val() + ' ' + template.find('select:.lstEndDate').val()); // event.end ? event.end : event.start;

    currentTask.Description = TaskListViewItemTemplate.find('textarea:.Description').val();
    currentTask.PriorityId = TaskPriorityAC.get_ItemValue();
    currentTask.StatusId = StatsuAC.get_ItemValue();
    currentTask.TypeId = TaskTypeAC.get_ItemValue();
    currentTask.AllDay = allDay;
    currentTask.TaskColor = StatsuAC.get_Item().statusColor;
    currentTask.EnabledTask = StatsuAC.get_Item().EnabledTask;

    if (ActivityLookupAC.get_ItemsIds()) {
        var Ids = ActivityLookupAC.get_ItemsIds();
        if (Ids) {
            var idsArray = Ids.split(',');
            $.each(idsArray, function (ix, id) {
                var activity = new TaskActivity(event.id);
                activity.ActivityId = id;
                currentTask.TaskActivityList.push(activity);
            });
        }
    }

    //currentTask.TaskAssignmentList = new Array();
    $.each(taskConfigValues.taskAssignment, function (index, taskAssignment) {

        var Ids = taskAssignment.control.get_ItemsIds();
        if (Ids) {
            var idsArray = Ids.split(',');
            $.each(idsArray, function (ix, id) {

                var assign = new TaskAssignment(event.id);
                assign.EntityId = taskAssignment.entityId;
                assign.EntityValueId = id;
                //assign.TaskAssignmentId = item.TaskAssignmentId;
                assign.TaskAssignmentId = -1;

                currentTask.TaskAssignmentList.push(assign);
            });
        }
    });

    $.each(taskConfigValues.taskEntity, function (index, taskEntity) {

        var Id = taskEntity.control.get_ItemValue();
        if (Id > 0) {
            //var idsArray = Ids.split(',');
            //$.each(idsArray, function (ix, id) {

            var entity = new TaskEntity(event.id);
            entity.EntityId = taskEntity.entityId;
            entity.EntityValueId = Id;
            entity.TaskEntityId = -1;

            currentTask.TaskEntityList.push(entity);
            //});
        }
    });

    return currentTask;
}



function saveMainTask(event, template, taskConfigControl) {

    var taskObj = getTaskObject(event, template, taskConfigControl);
    var json = encodeURIComponent(JSON.stringify(taskObj));

    if (DifferantDate < 0) {
        hideLoading();
        return;
    }

    var args = formatString('{taskId:{0}, taskInfo:"{1}", UserId:{2}}', event.id, json, UserId);

    createPostRequest(taskServiceURL + "AddEditTask", args,
     function (result) {

         if (result.result) {

             if (typeof getEntityCommunication == 'function') {
                 getEntityCommunication();
             }
             else {
                 fullCalendarObj.fullCalendar('removeEvents', event.id);
                 //debugger;
                 if (CurrentStatusId == taskObj.StatusId || CurrentStatusId == -1) {
                     result.result.color = taskObj.TaskColor;
                     result.result.allDay = taskObj.AllDay;
                     result.result.editable = taskObj.EnabledTask;    

                     fullCalendarObj.fullCalendar('renderEvent', result.result);
                 }
             }
             ShowHideEventInfo(false);
             hideLoading();
         }
         else {
             hideLoading();
             showStatusMsg('error');
         }


     },
     function (ex) {
     }
     )

}

function updateTaskDate(event, allDay) {
    //debugger;
    var args = '{TaskId:{0}, StartDate:"{1}", EndDate:"{2}", UserId:{3},allday:{4}}';
    var start = event.start.getMonth() + 1 + "-" + event.start.getDate() + "-" + event.start.getFullYear() + ' ' + event.start.getHours() + ':' + event.start.getMinutes();
    var end = event.end ? event.end.getMonth() + 1 + "-" + event.end.getDate() + "-" + event.end.getFullYear() + ' ' + event.end.getHours() + ':' + event.end.getMinutes() : start;
    args = formatString(args, event.id, start, end, UserId, allDay);
    createPostRequest(taskServiceURL + "UpdateTaskDate", args,
     function (result) {
         //debugger;
         hideLoading();
     },
     function (ex) {
         hideLoading();
         //debugger;
     }
     );
}



function updateSubTaskStatus(taskAssignmentId, eventId, statusId) {

    var args = formatString('{taskAssignmentId:{0}, statusId:{1}, UserId:{2}}', taskAssignmentId, statusId, UserId);

    createPostRequest(taskServiceURL + "UpdateAssignmentTaskStatus", args,
     function (result) {
         if (result.result) {

             if (CurrentStatusId != statusId && CurrentStatusId != -1) {
                 fullCalendarObj.fullCalendar('removeEvents', eventId);
             }

             //if (CurrentStatusId == statusId || CurrentStatusId == -1) {
             //    result.result.color = taskObj.TaskColor;
             //    fullCalendarObj.fullCalendar('renderEvent', result.result);
             //}

             //fullCalendarObj.fullCalendar('renderEvent', result.result);
             ShowHideEventInfo(false);
         }
         else {

             showStatusMsg('error');
         }
         hideLoading();

     },
     function (ex) {
         hideLoading();
     }
     )

}

function deleteTask(event) {
    var url = taskServiceURL + "DeleteTask";
    var args = formatString("{taskId:{0}}", event.id);

    createPostRequest(url, args,
    function (result) {
        if (result.statusCode.Code) {
            DisplayStatusMsg(deleteTaskSuccess, 'success');

            if (typeof removeCommunication == 'function') {
                removeCommunication();
            }
            else {
                fullCalendarObj.fullCalendar('removeEvents', event.id);
            }
            ShowHideEventInfo(false);
        }
        else {
            DisplayStatusMsg(deleteTaskError, 'error');
        }
        hideLoading();
    },
    function (ex) {
        hideLoading();
    });

}


function addTaskDicussion(template, taskId, details) {
    showLoadingWithoutBG($('#demo1'));
    var url = taskServiceURL + "AddDiscussion";
    var args = formatString('{taskId:{0}, details:"{1}", UserId:{2}}', taskId, escape(details), UserId);

    createPostRequest(url, args,
    function (result) {
        if (result.result) {
            var discussion = {
                Details: details,
                TaskDiscussionId: result.result,
                CreatedBy: LoggedInUser.FullName
            }
            appendNewDiscussion(template, discussion, true, new Date());
        }
        else {

        }
        hideLoading();
    },
    function (ex) {
        hideLoading();
    });

}

function deleteDiscussion(taskDiscussionId) {
    var url = taskServiceURL + "DeleteTaskDiscussion";
    var args = formatString('{taskDiscussionId:{0}}', taskDiscussionId);

    createPostRequest(url, args,
    function (result) {
        if (result.result) {

            //appendNewDiscussion(template, details, true);
        }
        else {

        }
        hideLoading();
    },
    function (ex) {
        hideLoading();
    });
}


function addPopupMessageSmaller(element, message) {
    var validation = $('<div>').attr({ 'class': 'TimeValidation' })
    .append($('<span>').html(message), $('<span>').attr('class', 'valid-arrow')).insertAfter(element);
    if (element.is('select'))
        validation.css('margin-top', '-50px');
    element.addClass('cx-control-red-border')
}






//////////////Task Object///////////////////////
///////////////////////////////////////////////