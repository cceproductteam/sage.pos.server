var uploadedDocument = 0;
var oldItems = 0;
function onSessionStarted(data) {
    try {
        kw(slickUpload).set_Data("modulekey", moduleKey);
        kw(slickUpload).set_Data("modulename", moduleName);
        kw(slickUpload).set_Data("userId", userId);
        kw(slickUpload).set_Data("id", parentId);
        kw(slickUpload).set_Data("folder", parentId);

        kw(slickUpload).set_Data("selectedFolder", selectedFolder);
        kw(slickUpload).set_Data("entityId", EntityID);
        kw(slickUpload).set_Data("parentDocumentFolderId", currentFolderId);
        window.setTimeout(function () {
            $('img[alt="Powered By SlickUpload Community Edition"]').remove();
            $('.removePluginIcon').remove();
        }, 700);

        onbeforeunload = function (e) {
            if (uploadedDocument != 0 || (kw(fileList).get_Element().children.length - 1 - oldItems) > 0)
                return "";
            else onbeforeunload = null;
        };
    }
    catch (e) {
        //  logToLocalStorge("on session start " + e);
    }
}

function onFileStart(file) {

    try {


        if (uploadedDocument <= 0) {
            $('.Main-Div', window.parent.document).parent().parent().prepend($('<div>').attr('class', 'loading-div').html($('<span>').text('Please wait while the document(s) being uploaded...')));

        }
        //$('.fileselecter').find('img[alt~="Powered"]').remove();
        $("#" + fileList).css("display", "block");
        var fileEl = kw(fileList).getItemElementById(file.get_Id());
        var listEl = $("#" + fileList);
        listEl.scrollTop = fileEl.offsetTop - fileEl.offsetHeight;

        if (!checkFileSizeIfAllowed(file.get_Size())) {
            $(fileEl).remove();
            parent.showOkayMsg('File Size Error', 'Exceeds the maximum document size');
            return false;
        }

        var fileName = $(fileEl).find('.File-img-title .su-filename').html()
        var ext = fileName.lastIndexOf('.');
        ext = fileName.substring(ext + 1, fileName.length);
        $(fileEl).find('.dd-filename').removeClass('dd-filename').addClass('su-filename su-ext-unk su-ext-' + ext);
        fileName = Truncate(fileName, 10);

        $(fileEl).find('.File-img-title .su-filename').html(fileName);
        $(fileEl).find('.File-img-title .su-filename').removeClass('su-filename');
        $(fileEl).children('.su-filename1').children('a').children('.su-filename').css({ 'white-space': 'pre-wrap', 'width': '78%' });
    } catch (e) {
        //  logToLocalStorge("on file end " + e);
    }

}

function onFileEnd(file) {
    DocumentCount = Number(DocumentCount) + 1;
    //$("#" + fileList).css("display", "none")
    //getFoldersS(currentFolder, currentFolderId);
    //
    //$('.fileselecter').find('img[alt~="Powered"]').remove();
    try {

        if (uploadedDocument <= 0) {
            uploadedDocument = kw(fileList).get_Element().children.length - 1 - oldItems;
            oldItems += uploadedDocument;
        }
        uploadedDocument -= 1;

        var x = $(file);
        var fileEl = kw(fileList).getItemElementById(file.get_Id());
        $(fileEl).find('.whiteBG').attr({ 'docId': file.get_Status().DocumentID, folderId: currentFolderId });
        $(fileEl).children('div:last').empty();
        //xxxxxxxxxxxxx

        // var url = '../../SavedDocument/' + moduleName + "/" + parentId + "/" + file.get_Status().serverFileName;
        var url2 = DocumentPath + moduleName + "/" + parentId + "/" + file.get_Status().serverFileName;
        var url = '../../Common/Documents/OpenDocument.aspx?path=' + url2 + "&&downloadName=" + encodeURIComponent(file.get_Status().name);

        $(fileEl).find('.File-img-title,.su-filename').attr({ href: url, target: '_blank' });

        $(fileEl).children('.deleteexisitingfile').attr('docid', file.get_Status().DocumentID);

        $(fileEl).find('.File-img-title').find('span').attr('title', $(fileEl).find('.File-img-title').find('span').text());
        $(fileEl).find('.File-img-title').find('span').text(Truncate($(fileEl).find('.File-img-title').find('span').text(), 13));


        $(fileEl).find('.delete-file').unbind().click(function () {
            DeleteDocument(this, file.get_Status().DocumentID)
        });

        ///// by amer 7-4-2014
        //    $(fileEl).find('.delete-file').unbind().click(function () {
        //        checkeFile(this, file.get_Status().DocumentID);
        //    });

        window.setTimeout(function () {
            //$(fileEl).children('.su-filename1').children('a').children('.su-filename').truncate(60);
            $(fileEl).attr('title', file.get_Name());
            $(fileEl).children('.su-filename1').children('a').children('.su-filename').removeClass('su-filename'); // css('width', '100%');
            $(fileEl).children('.su-filename1').addClass('su-filename').removeClass('su-filename1');
            $(fileEl).find('.File-img-title').find('span').text(Truncate($(fileEl).find('.File-img-title').find('span').text(), 13));
        }, 1000);

        if (typeof parent.SendAttachmentNotifications === "function") {
            parent.SendAttachmentNotifications(file.get_Status().DocumentID);
        }

        UpdateDocumentEntity(file.get_Status().DocumentEntityID);
        // UpdateDocumentEntity(file.get_Status().DocumentID);
        //        if (file.get_Status().DocumentID > 0) {
        //            //$('.attachment-tab').append("<span class='has-attachment'> </span>");
        //            $('.has-attachment').show();
        //        }



        if (uploadedDocument <= 0) {
            $('.loading-div', window.parent.document).remove();
        }
    } catch (e) {
        //   logToLocalStorge("on file end " + e);
        // $('.loading-div', window.parent.document).remove();
        // alert(e);
    }

    window.setTimeout(function () {
        document.location = document.location;
    }, 1000);
}

kw(function () {
    if (kw.support.dragDrop)
        $("#dropInstructions").css("display", "block");
    kw(slickUpload).start();
});



function checkFileSizeIfAllowed(fileSize) {
    var allowedFileSizeByte = maxDocumentSizeKB * 1024;
    return fileSize <= allowedFileSizeByte;
}

function UpdateDocumentEntity(documentEntityId) {
    //  
    $.ajax({
        type: "post",
        contentType: "application/json; charset=utf-8",
        url: 'DocumentWebService.asmx/UpdateDocument',
        data: '{ id: "' + documentEntityId + '"}',
        dataType: "json",
        success: function (e) {
        },
        error: function (e) {

        }
    });
}