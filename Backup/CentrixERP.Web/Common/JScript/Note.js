function getNotes(ParentId, mKey) {
    $('.note-tab').children().remove();
    var data = formatString('{ParentId:{0}, mKey:"{1}"}', ParentId, mKey);
    $.ajax({
        type: "POST",
        url: systemConfig.NoteServiceURL + "FindAll",
        contentType: "application/json; charset=utf-8",
        data: data,
        dataType: "json",
        success: function (data) {
            var noteObj = eval("(" + data.d + ")");

            addNoteTab(null, ParentId, mKey);
            if (noteObj.result != null) {
                $('.note-tab').parent().find('.DragDropBackground').remove();
                $.each(noteObj.result, function (index, item) {
                    addNoteTab(item, ParentId, mKey);
                });
                

            }
            else
                $('.TabbedPanelsContentGroup').css('min-height', $('.Result-info-container').height() - 31);
            attachTabEvents();
        },
        error: function (e) {
        }
    });
}

function addNoteTab(item, ParentId, mKey) {
    var id = (item != null) ? item.Id : 0;

    var template = $(NoteTemplate);
    if (item != null) {
        template = mapEntityInfoToControls(item, template);
    } else {

        template.hide();
        $('.add-new-note').click(function () {

            template.find('.itemheader').click();
            template.find('.viewedit-display-none').show();
            template.find('.viewedit-display-block').hide();
            template.find('textArea').val('');
            template.find('.edit-tab-lnk').remove();
            template.find('.save-tab-lnk').show();
            template.find('.cancel-tab-lnk').show();
            template.find('.delete-tab-lnk').hide();
            template.find('.validation').remove();
            if (template.is(':visible')) {
                template.hide();
            }
            else {
                template.show();
            }
            return false;
        });
    }



    template.find('.save-tab-lnk').on('click', function () {
        saveNote(id, ParentId, mKey, template);
        template.find('.edit-tab-lnk').show();
        return false;
    });


    template.find('.delete-tab-lnk').click(function () {
        deleteNote(id, mKey, ParentId);
        return false;
    });

    template.find('.edit-tab-lnk').click(function () {
        template.find('.edit-tab-lnk').hide();
        template.find('.save-tab-lnk').show();
        template.find('.cancel-tab-lnk').show();
        template.find('.viewedit-display-none').show();
        template.find('.viewedit-display-block').hide();
        return false;
    });

    template.find('.cancel-tab-lnk').on('click', function () {
        template.find('.itemheader').click();
        getNotes(ParentId, mKey);
        template.find('.edit-tab-lnk').show();
        template.find('.save-tab-lnk').hide();
        return false;
    });

    $('.note-tab').append(template);

}




function saveNote(id, parentId, mKey, template) {

    var noteContent = template.find('textArea:.NoteContent').val();
    var data = '{id:{0}, moduleId:{1}, mKey:"{2}",content:"{3}",userId:"{4}"}';
    data = formatString(data, id, parentId, mKey, noteContent, UserId);

    $.ajax({
        type: "POST",
        url: systemConfig.NoteServiceURL + "AddEditNote",
        contentType: "application/json; charset=utf-8",
        data: data,
        dataType: "json",
        success: function (data) {
            getNotes(parentId, mKey);
        },
        error: function (e) {
        }
    });
}



function deleteNote(id, mKey, parentId) {
    $.ajax({
        type: "POST",
        url: systemConfig.NoteServiceURL + "DeleteNote",
        contentType: "application/json; charset=utf-8",
        data: formatString('{id:{0}, mKey:"{1}"}', id, mKey),
        dataType: "json",
        success: function (data) {
            getNotes(parentId, mKey);
        },
        error: function (e) {
        }
    });
}

