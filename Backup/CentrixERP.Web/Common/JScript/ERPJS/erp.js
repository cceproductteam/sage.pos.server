var rateObject;
function LoadAddEntityDefaultOptions2(listPage) {
    $('.ActionSave-link').live('click', function () {
        SaveEntity();
    });
    $('.ActionCancel-link').live('click', function () {
        loadEntityData(loadEntityServiceURL, entityItemViewTemplate, selectedEntityId, selectEntityCallBack);
    });

    $('.ActionList-link').live('click', function () {
        document.location = listPage;
    });

    $('.ActionEdit-link').live("click", function () {
        $('.viewedit-display-block').hide();
        $('.viewedit-display-none').show();
    });
    $('.ActionDelete-link').live('click', function () {
        showConfirmMsg(DeleteTitleMessage, DeleteMessage, function () {
            deleteEntity(selectedEntityId);
        });
        return false;
    });


    if (selectedEntityId) {
        loadEntityData(loadEntityServiceURL, entityItemViewTemplate, selectedEntityId, selectEntityCallBack);
    }
    else {
        $('.ActionEdit-link').hide();
        $('.ActionDelete-link').hide();
        $('.ActionCancel-link').hide();
        initControls();
    }
    initControlsStatus = true;
}

function CheckReturnedResult(resultCode) {
    if (resultCode == systemConfig.BatchPostingErrors.EmptyBatch) {
        showOkayMsg(getResource('Error'), getResource("vldPostEmptyBatch"));
        return;
    }
    if (resultCode == systemConfig.BatchPostingErrors.PostedBatch) {
        showOkayMsg(getResource('Error'), getResource("vldPostPostedBatch")); //getResource("")
        return;
    }

    if (resultCode == systemConfig.BatchPostingErrors.DeletedBatch) {
        showOkayMsg(getResource('Error'), getResource("vldPostDeletedBatch"));
        return;
    }
    if (resultCode == systemConfig.BatchPostingErrors.ReadyToPostBatch) {
        showOkayMsg(getResource('Error'), getResource("vldEditReadyToPostBatch"));
        return;
    }
    if (resultCode == systemConfig.BatchPostingErrors.EntryInLockedPeriod) {
        showOkayMsg(getResource('Error'), getResource("vldPostLockedEntries"));
        return;
    }
    if (resultCode == systemConfig.BatchPostingErrors.EntryBeforOldesetYear) {
        showOkayMsg(getResource('Error'), getResource("vldPostBeforOldesetEntries"));
        return;
    }
    if (resultCode == systemConfig.BatchPostingErrors.EntryAccountInActive) {
        showOkayMsg(getResource('Error'), getResource("vldPostEntriesWithInactiveAcc"));
        return;
    }
    if (resultCode == systemConfig.BatchPostingErrors.AllowPostingToPreviousYear) {
        showOkayMsg(getResource('Error'), getResource("vldPostBeforCurrentYear"));
        return;
    }
    if (resultCode == systemConfig.BatchPostingErrors.NotBalanceBatch) {
        showOkayMsg(getResource('Error'), getResource("vldPostUnbalancedBatch"));
        return;
    }
    if (resultCode == systemConfig.BatchPostingErrors.AdjustmentPeriodLocked) {
        showOkayMsg(getResource('Error'), getResource("vldAdjustmentPeriodLocked"));
        return;
    }
    if (resultCode == 0) {
        showOkayMsg(getResource('Error'), getResource("FaildToSave"));
        return;
    }
}


function GetCurrencyRate(fromCurrencyId, toCurrencyId) {
    //if to currency is the funcional currency

    var data = formatString("{fromCurrencyId:{0},toCurrencyId:{1}}", fromCurrencyId, toCurrencyId);

    $.ajax({
        type: "POST",
        url: systemConfig.ApplicationURL_Common + systemConfig.CurrencyRateDetailsURL + "GetCurrencyRate",
        contentType: "application/json; charset=utf-8",
        data: data,
        dataType: "json",
        success: function (data) {
            var result = eval('(' + data.d + ')').result;
            rateObject= result;
        },
        error: function (ex) {
        }
    });
}


function getFunctionalAmount(rate, dataMatchedId, amount) {
    switch (dataMatchedId) {
        case systemConfig.WebKeys.MultiblyTypeId:
            return parseFloat(rate) * amount;
            break;
        case systemConfig.WebKeys.DevideTypeId:
            return amount / parseFloat(rate);
            break;
        default:
            return parseFloat(rate) * amount;
            break;
    }
}