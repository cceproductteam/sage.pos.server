<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CentrixGrid.aspx.cs" Inherits="CentrixERP.Common.Web.centrixgrid.CentrixGrid"
    MasterPageFile="~/Common/MasterPage/Site.master" %>

<asp:Content runat="server" ContentPlaceHolderID="PageHeader">
    <script src="CentrixGrid.js" type="text/javascript"></script>
    <script type="text" id="r-template1">
    <tr class="invoice-details-row">
        <td class="td-r1-data-table center-content">
            <span class="line-number"></span>
        </td>
        <td class="td-r1-data-table left-content">
            <div class="viewedit-display-none">
                <select class="lst ModelNumber">
                </select>
            </div>
            <span class="label-data ModelNumber viewedit-display-block"></span>
        </td>   
        <td class="td-r1-data-table left-content">
            <div class="viewedit-display-none">
                <select class="lst Items">
                </select>
            </div>
            <span class="label-data Items viewedit-display-block"></span>
        </td>               
        <td class="td-r1-data-table right-content">
            <input type="text" class="cx-control Price txt numeric required viewedit-display-none" />
            <span class="label-data Price money viewedit-display-block"></span>
        </td>
        <td class="td-r1-data-table right-content">
            <input type="text" class="cx-control Date date required viewedit-display-none" />
            <span class="label-data Date viewedit-display-block"></span>
        </td>
        <td class="td-r1-data-table right-content">
            <span class="label-data Price2"></span>
        </td>
        <td class="td-r1-data-table right-content">
            <input type="text" class="cx-control Comment txt required viewedit-display-none" />
            <span class="label-data Comment viewedit-display-block"></span>
        </td>
        <td class="td-r1-data-table right-content">
            <input type="checkbox" class="cx-control Active chk required viewedit-display-none" />
            <span class="label-data Active viewedit-display-block"></span>
        </td>
        
    </tr>
    </script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="PageBody">
    <div id="grid">
    </div>
    <div id="h-template">
        <table width="100%" border="0" cellpadding="0" cellspacing="1" class="details-grid data-table invoice-details">
            <tr>
                <th  class="th-data-table tbl_batchlist-th td-batch-no">
                    #
                </th>
                <th class="th-data-table tbl_batchlist-th td-batch-no">
                    Model Number
                </th>
                <th class="th-data-table tbl_batchlist-th td-batch-no">
                    Items
                </th>
                <th class="th-data-table tbl_batchlist-th td-batch-no">
                    Amount
                </th>
                <th class="th-data-table tbl_batchlist-th td-batch-no">
                    Date
                </th>
                <th class="th-data-table tbl_batchlist-th td-batch-no">
                    Price2
                </th>
                <th class="th-data-table tbl_batchlist-th td-batch-no">
                    Comment
                </th>
                <th class="th-data-table tbl_batchlist-th td-batch-no">
                    Status
                </th>
            </tr>
            <tr>
            <td colspan></td>
            </tr>
        </table>
    </div>
</asp:Content>
