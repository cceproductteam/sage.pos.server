﻿var OptionalFieldServiceURL;
var OptionalFieldGrid;
var OptionalFieldAC;
var OptionalFieldAC2;
var OptionalFieldDetailsAC;
var OptionalFieldDetailsAC2;

function getOptionalField(entityId, entityValueId, accountId, preventEdit) {
    $('.pnl-optional-fields').empty();
    $('.pnl-optional-fields').show();
    $('.pnl-optional-fields').append($(optionalFieldListViewTemplate).clone());
    $('.pnl-optional-fields').find('.viewedit-display-none').hide(); //$('.viewedit-display-none').hide();
    $('.pnl-optional-fields').find('.viewedit-display-block').show(); //$('.viewedit-display-block').show();
    $('#optional-field-items-grid').empty();
    OptionalFieldServiceURL = systemConfig.ApplicationURL_Common + systemConfig.OptionalFieldWebServiceURL;
    $('.td-data').remove();

    if (preventEdit) {
        $('.pnl-optional-fields').find('.edit-tab-lnk').remove();  //$('.edit-tab-lnk').remove();
    }

    $('.edit-tab-lnk').click(function () {
        $('.pnl-optional-fields').find('.viewedit-display-none').show(); //$('.viewedit-display-none').show();
        $('.pnl-optional-fields').find('.viewedit-display-block').hide(); //$('.viewedit-display-block').hide();
        OptionalFieldGrid.edit();
        return false;
    });

    $('.cancel-tab-lnk').click(function () {
        getOptionalField(entityId, entityValueId, -1);
        return false;
    });


    $('.ActionSave-link').unbind().click(function () {
        SaveOptionalFields(entityId, entityValueId, null);
        return false;
    });

    OptionalFieldFindAll($('.optional-field-tab'), entityId, entityValueId, function (returned) { LoadOptionalField(returned, accountId, $('.pnl-optional-fields')); });

}

function OptionalFieldFindAll(template, entityId, entityValueId, callback) {
    OptionalFieldServiceURL = systemConfig.ApplicationURL_Common + systemConfig.OptionalFieldWebServiceURL;

    showLoading(template);
    var data = formatString("{keyword:'',page:{0},resultCount:{1},argsCriteria:'{EntityId:{2}, EntityValueId:{3}}'}", 1, 100, entityId, entityValueId);
    $.ajax({
        type: "POST",
        url: OptionalFieldServiceURL + "FindAllLite",
        contentType: "application/json; charset=utf-8",
        data: data,
        dataType: "json",
        success: function (data) {
            $('.td-data').remove();
            var resultList = eval("(" + data.d + ")").result;
            if (callback)
                callback(resultList);
            //   LoadOptionalField(resultList, accountId, $('.pnl-optional-fields'));
            hideLoading();
        },
        error: function (e) {
        }
    });
}

function LoadOptionalField(datasource, accountId, template) {
    var control = [
    {
        controlId: 'OptionalField',
        type: 'ac',
        required: true,
        service: systemConfig.ApplicationURL + systemConfig.ClassServiceURL + "FindAllLite",
        serviceArgs: formatString('{AccountId:{0},EntityId:{1}}', ((accountId) ? accountId : -1), currentEntity),
        onChange: function (item, template, controls) {
            if (item) {
                template.find('.OptionalFieldDesc').text(item.Description);
                controls[1].control.set_Args(formatString('{ClassId:{0}}', item.value));
                controls[1].control.clear();
            }
        }

    },
   {
       controlId: 'OptionalFieldDetails',
       type: 'ac',
       required: true,
       service: systemConfig.ApplicationURL + systemConfig.ClassCodeServiceURL + "FindAllLite",
       serviceArgs: formatString('{ClassId:{0}}', -1),
       onChange: function (item, template, controls) {
           if (item) template.find('.OptionalFieldDetailsDesc').text(item.ClassCodeDescription);
       }

   }

];
    //after create array of controls, we need init the grid
    OptionalFieldGrid = template.find('#optional-field-items-grid').Grid({
        headerTeamplte: template.find('#h-optional-field-template').html(), //header template must has the table tag
        rowTemplate: $(optionalFieldRowTemplate),
        rowControls: control,
        dataSource: datasource, //this the data source that comes from API,i will set it for null just fro testing
        selector: '.',
        appendBefore: 'tr:last',
        rowDeleted: function (grid) {
        },
        rowAdded: function (row, controls, dataRow, grid) {
            var lineNumber = template.find('#optional-field-items-grid').find('.r-data').length + 1;
            row.find('.row-number').text(lineNumber);
            row.attr('RowLine', lineNumber);
            if (dataRow)
                controls[1].control.set_Args(formatString('{ClassId:{0}}', dataRow.OptionalFieldId));
        },
        preventAdd: false,
        preventDelete: false
    });

}


function SaveOptionalFields(entityId, entityValueId, updatedDate) {

    var OptionalFieldsArray = new Array();

    if (OptionalFieldGrid.validate()) {

        $.each(OptionalFieldGrid.get(), function (index, item) {
            var ItemToAdd = new Object();
            ItemToAdd.OptionalFieldEntityId = (item.dataRow) ? item.dataRow.OptionalFieldEntityId : -1;
            ItemToAdd.OptionalFieldId = item.OptionalFieldId;
            ItemToAdd.OptionalFieldDetailsId = item.OptionalFieldDetailsId;
            ItemToAdd.EntityId = entityId;
            ItemToAdd.EntityValueId = entityValueId;
            ItemToAdd.LineNumber = item.template.find('.row-number').text();
            OptionalFieldsArray.push(ItemToAdd);
        });
    }
    else { return false; }

    if (OptionalFieldsArray.length <= 0) {
        showOkayMsg(getResource('Error'), 'No Optional Fields to save');
        return false;
    }
    showLoading($('.optional-field-tab'));
    var lastUpdatedDate;

    var data = formatString('{entityId:{0},entityValueId:{1}, OptionalFieldEntityInfo:"{2}"}', entityId, entityValueId, escape(JSON.stringify(OptionalFieldsArray)));

    $.ajax({
        type: "POST",
        url: systemConfig.ApplicationURL + systemConfig.OptionalFieldWebServiceURL + "Add",
        contentType: "application/json; charset=utf-8",
        data: data,
        dataType: "json",
        headers: getEntitylockAjaxHeaders(getEntityLockDateFormat(updatedDate)),
        success: function (data) {
            hideLoading();
            var returned = eval('(' + data.d + ')'); //RowLine
            var result = returned.result;
            var code = returned.statusCode.Code;
            if (code == 1) {
                showStatusMsg(getResource('savedsuccessfully'));
                getOptionalField(entityId, entityValueId);
            }
            else {
                FilterOptionalFieldsErrors(result);
                showStatusMsg(getResource('FaildToSave'));
            }
        }, error: function (ex) {
            hideLoading();
            handleLockEntityError(ex, function () { }, function () { getOptionalField(entityId, entityValueId); });
        }
    });

}

function FilterOptionalFieldsErrors(errors) {
    var allMsg = '';
    $('.validation').remove();
    $.each(errors, function (indx, item) {
        var row = $(".r-data[rowline='" + item.Line + "']");
        var errorMessage = (item.Resourcekey) ? getResource(item.Resourcekey) : item.Msg;
        if (item.ForAll) {
            allMsg += '<span>' + errorMessage + '<br/></span>';
        }
        else if (item.Control == 'OptionalField')
            addPopupMessage(row.find('select:.OptionalField'), errorMessage);

    });

    hideLoading();
    if (allMsg.trim()) {
        showOkayMsg(getResource('Error'), allMsg.trim());
    }
}



/////////////////////////////////////   Load and Get Optional Field In Add Popup Mode ///////////////////////////////////////////////////////////////////////////////////////
function LoadOptionalFieldInAdd(template) {
    template.find('.add-optional-field').empty();
    template.find('.add-optional-field').html('');

    template.find('.add-optional-field').append($(addOptionalFieldTemplate).clone());
    OptionalFieldAC = template.find('select:.OptionalField').AutoComplete({ serviceURL: systemConfig.ApplicationURL + systemConfig.ClassServiceURL + "FindAllLite", required: false });
    OptionalFieldAC.set_Args(formatString("{EntityId:{0}}", currentEntity));
    OptionalFieldAC.onChange(function (item) {
        if (item) {
            OptionalFieldDetailsAC.clear();
            OptionalFieldDetailsAC.set_Required(true);
            OptionalFieldDetailsAC.set_Args(formatString("{ClassId:{0}}", item.value));
        }
        else { OptionalFieldDetailsAC.set_Required(false); }
    });
    OptionalFieldAC2 = template.find('select:.OptionalField2').AutoComplete({ serviceURL: systemConfig.ApplicationURL + systemConfig.ClassServiceURL + "FindAllLite", required: false });
    OptionalFieldAC2.set_Args(formatString("{EntityId:{0}}", currentEntity));
    OptionalFieldAC2.onChange(function (item) {
        if (item) {
            OptionalFieldDetailsAC2.clear();
            OptionalFieldDetailsAC2.set_Required(true);
            OptionalFieldDetailsAC2.set_Args(formatString("{ClassId:{0}}", item.value));
        }
        else { OptionalFieldDetailsAC2.set_Required(false); }
    });
    OptionalFieldDetailsAC = template.find('select:.OptionalFieldDetails').AutoComplete({ serviceURL: systemConfig.ApplicationURL + systemConfig.ClassCodeServiceURL + "FindAllLite", required: false });
    OptionalFieldDetailsAC2 = template.find('select:.OptionalFieldDetails2').AutoComplete({ serviceURL: systemConfig.ApplicationURL + systemConfig.ClassCodeServiceURL + "FindAllLite", required: false });
    //  template.find('.add-optional-field').append($(addOptionalFieldTemplate));
    //   LoadOptionalField(null, -1, template.find('.optional-field-tab'));
    //   $('.viewedit-display-none').show();
    //   $('.viewedit-display-block').hide();
    //   OptionalFieldGrid.edit();
    //addOptionalFieldTemplate

    //Added By Ruba Al-Sa'di 8-2-2015 to hide/show Optional Field region in add form.
    //if (icModule) 
    {
        template.find('.ClickToShowHide').removeClass('hidden');
        $('.add-new-item').find('div.gridContainerDiv').height($('.add-new-item').find('div.gridContainerDiv').height() + 95);
        template.find('.add-optional-field .Add-title').click(function () {
            if ($('div.optional-field-table').is(':visible')) {
                $('.ClickToShowHide').html('&#9660;');               
                $('div.optional-field-table').slideUp();
                $('.add-new-item').find('div.gridContainerDiv').height($('.add-new-item').find('div.gridContainerDiv').height() + 80);
            }
            else {
                $('.ClickToShowHide').html('&#9650;');
                $('div.optional-field-table').slideDown();
                $('.add-new-item').find('div.gridContainerDiv').height($('.add-new-item').find('div.gridContainerDiv').height() - 80);
            }
        });
    }
}

function getAddOptionalFields(entityId) {
    var optionalList = new Array();
    if (OptionalFieldAC.get_Item())
        optionalList.push({ OptionalFieldId: OptionalFieldAC.get_ItemValue(), OptionalFieldDetailsId: OptionalFieldDetailsAC.get_ItemValue(), EntityId: entityId, LineNumber: 1 });
    if (OptionalFieldAC2.get_Item())
        optionalList.push({ OptionalFieldId: OptionalFieldAC2.get_ItemValue(), OptionalFieldDetailsId: OptionalFieldDetailsAC2.get_ItemValue(), EntityId: entityId, LineNumber: 2 });
    return optionalList;
}


function filterAddOptionalFieldsError(errors, template) {
    var allMsg = '';
    template.find('.validation').remove();
    $.each(errors, function (indx, item) {
        // var row = $(".r-data[rowline='" + item.Line + "']");
        var errorMessage = (item.Resourcekey) ? getResource(item.Resourcekey) : item.Msg;
        if (item.ForAll) {
            allMsg += '<span>' + errorMessage + '<br/></span>';
        }
        else if (item.Control == 'OptionalField')
            addPopupMessage(template.find('select:.OptionalField' + ((item.Line == 2) ? "2" : "")), errorMessage);

    });
    hideLoading();
    if (allMsg.trim()) {
        showOkayMsg(getResource('Error'), allMsg.trim());
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



function LoadOptionalFieldsOnDetailsLine(control, dataSource, edit, saveCallback, options) {
    if (!options)
        options = {};

    control.click(function () {
        //  entityValueId = template.find('.optional-field-details-edit').attr('id');
        $('.optional-field-pop-up').remove();

        var template = $(OptionalFieldPopUpTemplate).clone();
        $('.pnl-optional-fields-details').append(template);
        showBlackOverlay(null, 'black_overlay_on_add_popup');
        $('.black_overlay').css('z-index', $('.optional-field-pop-up').css('z-index') - 1); //Added By Ruba Al-Sa'di 6-1-2015
        template.find('.cancel-optional-field,.close-optional-field').click(function () {
            hideBlackOverlay('black_overlay_on_add_popup', $('.add-new-item').is(':visible'));
            $('.black_overlay').css('z-index', ''); //Added By Ruba Al-Sa'di 6-1-2015
            $('.optional-field-pop-up').remove();
            return false;
        });

        template.find('.save-optional-field').click(function () {

            var OptionalFieldsArray = new Array();

            if (OptionalFieldGrid.validate()) {
                $.each(OptionalFieldGrid.get(), function (index, item) {
                    var ItemToAdd = new Object();
                    ItemToAdd.OptionalFieldEntityId = (item.dataRow) ? item.dataRow.OptionalFieldEntityId : -1;
                    ItemToAdd.OptionalFieldId = item.OptionalFieldId;
                    ItemToAdd.OptionalField = item.OptionalFieldItem.label;
                    ItemToAdd.OptionalFieldDetailsId = item.OptionalFieldDetailsId;
                    ItemToAdd.OptionalFieldDetails = item.OptionalFieldDetailsItem.label;
                    //ItemToAdd.DetailsType = (options.DetailsType) ? options.DetailsType : -1;
                    //ItemToAdd.EntityId = entityId;
                    //ItemToAdd.EntityValueId = entityValueId;
                    ItemToAdd.LineNumber = item.template.find('.row-number').text();
                    OptionalFieldsArray.push(ItemToAdd);
                });

            }
            else { return false; }

            if (OptionalFieldsArray.length <= 0) {
                showOkayMsg(getResource('Error'), 'No Optional Fields to save');
                return false;
            }
            var duplicationArray = checkOptionDuplication(OptionalFieldsArray);

            if (duplicationArray.length > 0) {
                var row;
                $.each(duplicationArray, function (indx, item) {
                    row = $('.pnl-optional-fields-details').find(".r-data[rowline='" + item.LineNumber + "']");
                    addPopupMessage(row.find('select:.OptionalField'), getResource("AlreadyExist"));
                });
                return false;
            }

            dataSource = OptionalFieldsArray;
            saveCallback(OptionalFieldsArray);
            //   detailsArray[detailsIndex].OptionalFieldEntityList = OptionalFieldsArray;
            //    detailsArray[detailsIndex].OptionalFieldChanged = true;
            hideBlackOverlay('black_overlay_on_add_popup', $('.add-new-item').is(':visible'));
            $('.black_overlay').css('z-index', ''); //Added By Ruba Al-Sa'di 6-1-2015
            $('.optional-field-pop-up').hide();
            return false;
        });
        //  if (entityValueId <= 0 || detailsArray[detailsIndex].OptionalFieldChanged) {
        LoadOptionalField(dataSource, -1, template);
        if (editingRecord || addingRecord) {
            $('.optional-field-pop-up').find('.viewedit-display-none').show();
            $('.optional-field-pop-up').find('.viewedit-display-block').hide();
            OptionalFieldGrid.edit();
        }
        else {
            $('.optional-field-pop-up').find('.viewedit-display-none').hide();
            $('.optional-field-pop-up').find('.viewedit-display-block').show();
            OptionalFieldGrid.view();
        }
        //  }
        //  else {
        //     OptionalFieldFindAll(template, entityId, entityValueId, function (returned) {
        //         detailsArray[detailsIndex].OptionalFieldEntityList = returned;

        //         LoadOptionalField(returned, -1, template);
        //         $('.optional-field-pop-up').find('.viewedit-display-none').show();
        //         $('.optional-field-pop-up').find('.viewedit-display-block').hide();
        //         OptionalFieldGrid.edit();
        //    });
        //   }
    });
}


function checkDetailsOptionalFieldErrors(errors, code) {
}


function getOptionalFieldListObject(OptionalFieldEntityList) {
    var optionalList = [];
    if (!OptionalFieldEntityList)
        return optionalList;
    $.each(OptionalFieldEntityList, function (indx, item) {
        optionalList.push({
            OptionalFieldEntityId: (item.OptionalFieldEntityId) ? item.OptionalFieldEntityId : -1,
            OptionalFieldId: item.OptionalFieldId,
            OptionalFieldDetailsId: item.OptionalFieldDetailsId,
            EntityId: (item.EntityId) ? item.EntityId : -1,
            EntityValueId: (item.EntityValueId) ? item.EntityValueId : -1,
            LineNumber: item.Line//,
            //DetailsType: (item.DetailsType) ? item.DetailsType : -1
        });
    });
    return optionalList;
}

function checkOptionDuplication(optionalFieldList) {
    var validArr = new Array();
    var duplicationArr = new Array();

    $.each(optionalFieldList, function (indx, item) {
        if ($.inArray(item.OptionalFieldId, validArr) < 0)
            validArr.push(item.OptionalFieldId);
        else duplicationArr.push(item);
    });
    return duplicationArr;
}

function setOptionalFields(optionalFieldArr, detailsObj) {
    detailsObj.OptionalFieldEntityList = optionalFieldArr;
    detailsObj.OptionalFieldChanged = true;
}