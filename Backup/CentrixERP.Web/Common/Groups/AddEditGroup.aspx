<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddEditGroup.aspx.cs" Inherits="CentrixERP.Common.Web.NewCommon.Common.Groups.AddEditGroup"
    MasterPageFile="../MasterPage/Site.Master" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="PageHeader">
    <link href="CSS/Groups.css" rel="stylesheet" type="text/css" />
    <script src="JS/AddGroups.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="PageBody">
    <div id='content'>
        <div class="Add-new-form">
            <div class="Add-title">
                <span>Group Details<span>
                    <div class="Action-div">
                        <ul>
                            <li class="full-view-pnl"><a class="ActionFullView-link view-result"><span class="ActionFullView-link">
                            </span>Show Result</a></li>
                            <li class="edit-pnl viewedit-display-block"><span class="ActionEdit-link"></span><a
                                class="ActionEdit-link">Edit</a></li>
                            <li class="save-cancel-pnl viewedit-display-none"><span class="ActionSave-link"></span>
                                <a class="ActionSave-link">Save</a></li>
                            <li class="save-cancel-pnl viewedit-display-none"><span class="ActionCancel-link"></span>
                                <a class="ActionCancel-link">Cancel</a></li>
                        </ul>
                    </div>
            </div>
            <table border="0" cellpadding="0" cellspacing="0" class="Add-new-Table group-info">
                <tr>
                    <th>
                        <span class="label-title">Group Name<span class="smblreqyuired viewedit-display-none">
                            *</span></span>
                    </th>
                    <td>
                        <span class="label-data nowrap GroupName viewedit-display-block"></span>
                        <input class="cx-control GroupName txt required viewedit-display-none" maxlength="100"
                            type="text" />
                    </td>
                    <th>
                        <span class="label-title">Entity<span class="smblreqyuired viewedit-display-none"> *</span></span>
                    </th>
                    <td>
                        <span class="label-data nowrap Entity viewedit-display-block"></span>
                        <div class="viewedit-display-none">
                            <select class="Entity lst">
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>
                        <span class="label-title">Description</span>
                    </th>
                    <td>
                        <textarea class="cx-control Description txt viewedit-display-none" maxlength="200"></textarea>
                        <span class="label-data nowrap Description viewedit-display-block"></span>
                    </td>
                </tr>
            </table>
        </div>
        <div class="five-px">
        </div>
        <div class="Add-new-form control-panel">
            <div class="Add-title">
                <asp:Label ID="Label1" runat="server">Search Criteria</asp:Label>
            </div>
            <table border="0" cellpadding="0" cellspacing="0" class="Add-new-Table group-search-criteria">
                <tr>
                    <th>
                        <span class="label-title">Field<span class="smblreqyuired"> *</span></span>
                    </th>
                    <td>
                        <div>
                            <select class="EntityField lst">
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>
                        <span class="label-title">Value 1<span class="smblreqyuired"> *</span></span>
                    </th>
                    <td class="control-ac display-none control">
                        <div>
                            <select class="EntityValues lst">
                            </select>
                        </div>
                    </td>
                    <td class="control-txt control">
                        <input class="cx-control txt-Value txt" maxlength="100" type="text" />
                    </td>
                    <td class="control-date display-none control">
                        <input class="cx-control date-Value1 txt" type="text" />
                    </td>
                    <td class="control-checkbox display-none control">
                        <input class="cx-control chk-Value1-y txt" type="radio" value="Yes" name="gr-check" /><span
                            class="span-Value1-y">Yes</span>
                        <input class="cx-control chk-Value1-n txt" type="radio" value="No" name="gr-check" /><span
                            class="span-Value1-n">No</span>
                        <input class="cx-control chk-Value1-a txt" type="radio" value="All" name="gr-check"
                            checked="checked" /><span class="span-Value1-a">All</span>
                    </td>
                    <th>
                        <span class="label-title">Filter<span class="smblreqyuired">*</span></span>
                    </th>
                    <td>
                        <div>
                            <select class="Filter lst">
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="display-none control-val2 control">
                        <span class="label-title">Value 2<span class="smblreqyuired"> *</span></span>
                    </th>
                    <td class="display-none control-val2 control">
                        <input class="cx-control date-Value2 txt" type="text" />
                    </td>
                    <th>
                    </th>
                    <td>
                    </td>
                </tr>
                <tr>
                    <th>
                    </th>
                    <td colspan="2">
                    </td>
                    <td>
                        <input type="button" value="Add" class="button2 confirm-button save-field">
                        <input type="button" value="Clear" class="button-cancel clear-field">
                    </td>
                </tr>
            </table>
        </div>
        <div class="Add-new-form">
            <div class="Add-title">
                <span>Fields</span>
            </div>
            <table width="100%" border="0" cellpadding="0" cellspacing="1" class="search-criteria-info">
                <tbody>
                    <tr>
                        <th class="th-data-table th-item-num">
                            #
                        </th>
                        <th class="th-data-table">
                            Field
                        </th>
                        <th class="th-data-table">
                            Value 1
                        </th>
                        <th class="th-data-table ">
                            Filter
                        </th>
                        <th class="th-data-table">
                            Value 2
                        </th>
                        <th class="th-data-table th-operation">
                            Operation
                        </th>
                        <th class="th-data-table th-bracket">
                            Group
                        </th>
                        <th class="th-data-table th-options">
                            Options
                        </th>
                    </tr>
                    <tr>
                        <td class="td-data-border" colspan="8">
                        </td>
                    </tr>
                    <tr class="ContentRow">
                        <td class="td-r1-data-table center-content">
                            <span class="RowNo">#</span>
                        </td>
                        <td class="td-r1-data-table center-content">
                            <span class="start-group"></span><span class="FieldName"></span><span class="end-group">
                            </span>
                        </td>
                        <td class="td-r1-data-table center-content">
                            <span class="Value1"></span>
                        </td>
                        <td class="td-r1-data-table center-content">
                            <span class="Filter"></span>
                        </td>
                        <td class="td-r1-data-table center-content">
                            <span class="Value2"></span>
                        </td>
                        <td class="td-r1-data-table center-content">
                            <span class="Operation viewedit-display-block"></span>
                            <select class="lst-operation viewedit-display-none">
                                <option value="AND">AND</option>
                                <option value="OR">OR</option>
                            </select>
                        </td>
                        <td class="td-r1-data-table center-content td-group">
                            <a class="link link-group gr-start" href="javascript:void(0);">Start</a> <a class="link link-group gr-end"
                                href="javascript:void(0);">End</a> <a class="link link-group gr-cancel" href="javascript:void(0);">
                                    Cancel</a>
                        </td>
                        <td class="td-r1-data-table center-content">
                            <input type="button" value="Remove" class="button-cancel remove-btn viewedit-display-none">
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</asp:Content>
