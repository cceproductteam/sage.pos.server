var ItemListTemplate;
var GroupViewItemTemplate;
var GroupId = -1;
var EntityNameAC;

function InnerPage_Load() {
    setPageTitle('Contacts Groups');
    setSelectedMenuItem('.setup');
    currentEntity = systemEntities.ContactsGroup;
    setAddLinkURL(systemConfig.ApplicationURL + systemConfig.pageURLs.AddGroup, 'Add Contacts Group');


    GetTemaplte(systemConfig.ApplicationURL_Common + systemConfig.HTMLTemplateURLs.ContactsGroupListItem, loadData, 'ItemListTemplate');

    $('.result-block').live("click", function () {
        GroupId = $(this).parent().attr('id');
    });

    deletedEntityCallBack = function () {
        removeDeletedEntity();
    };

    updatedEntityCallBack = function () {
        getEntityData(selectedEntityId);
    };

    $('.GridTableDiv-body').height($('.Result-info-container').height() - 61);
}

function loadData(data) {
    GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.GroupListViewItem, function (dataview) {
        checkPermissions(this, systemEntities.ContactsGroup, -1, GroupViewItemTemplate, function (template) {
            GroupViewItemTemplate = template;
            loadEntityServiceURL = systemConfig.ApplicationURL_Common + systemConfig.GroupWebService + "FindByIdLite";
            entityItemListTemplate = data;
            entityItemViewTemplate = GroupViewItemTemplate;
            findAllEntityServiceURL = systemConfig.ApplicationURL_Common + systemConfig.GroupWebService + "FindAllLite";
            deleteEntityServiceURL = systemConfig.ApplicationURL_Common + systemConfig.GroupWebService + "Delete";
            selectEntityCallBack = getGroupInfo;
            loadDefaultEntityOptions();
        });
    }, 'GroupViewItemTemplate');
}

function initControls() {
    EntityNameAC = $('select:.EntityName').AutoComplete({
        serviceURL: systemConfig.ApplicationURL_Common + systemConfig.EntityWebService + "FindAll",
        autoLoad: false, required: true
    });
    $('.historyUpdated').append(HistoryUpdatedListViewTemplate);
}

function getGroupInfo(group) {
    $('a:.ActionFullView-link').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.AddGroup + '?gid=' + group.Id);
    $('a:.view-result').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.GroupContacts + '?gid=' + group.Id);
}


function setListItemTemplateValue(entityTemplate, entityItem) {
    entityTemplate.find('.result-data1 label').text(entityItem.GroupNameEn);
    entityTemplate.find('.result-data2 label').text(entityItem.EntityName);
    return entityTemplate;
}

function setListItemServiceData(keyword, pageNumber, resultCount) {
    return formatString('{ keyword: "{0}", page:{1}, resultCount:{2},argsCriteria:"{GroupId:{3}}"}', keyword, pageNumber, resultCount, filterEntityId);
}



function getGroupContact(groupId) {
    $.ajax({
        type: 'post',
        url: systemConfig.ApplicationURL_Common + systemConfig.GroupWebService + "FindGroupContact",
        data: '{groupId:' + groupId + '}',
        dataType: 'json',
        contentType: 'application/json; charset=utf8',
        success: function (data) {
            hideLoading();
        },
        error: function (ex) {
            hideLoading();
        }
    });
}