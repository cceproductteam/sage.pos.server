var EntityAC, EntityFieldAC, EntityListValuesAC;
var RowTemplate = null;
var searchCriteriaList, defaultSearchCriteriaList, currentSearchCriteriaIndex = -1;
var groupId = -1, disabledMode = true;
var currentEntity, currentEntityLastUpdatedDate;
var lastGroupClicked = 0;

function InnerPage_Load() {
    showLoading('#demo1');
    setPageTitle('Add Group');
    setSelectedMenuItem('.setup');
    currentEntity = systemEntities.ContactsGroup;
    groupId = getQSKey('gid') || -1;


    checkPermissions(this, systemEntities.ContactsGroup, -1, $('#content'), function (template) {
        getTemplate();
        initControls();
    });



}



   
function initControls() {
    searchCriteriaList = new Array();

    EntityAC = $('select:.Entity').AutoComplete({
        serviceURL: systemConfig.ApplicationURL_Common + systemConfig.EntityWebService + "FindAll",
        autoLoad: false, required: true
    });
    EntityAC.set_Args('{mainEntity:true}');
    EntityAC.onChange(entityChanged);

    EntityFieldAC = $('select:.EntityField').AutoComplete({
        serviceURL: systemConfig.ApplicationURL_Common + systemConfig.EntityWebService + "FindEntityControls",
        autoLoad: false, required: true, staticParams: false
    });


    EntityFieldAC.onChange(entityFieldChanged);
    //EntityFieldAC.set_Args('{entityId:6}');
    EntityFieldAC.Disabled(true);

    $('.save-field').click(saveField);
    $('.clear-field').click(function () {
        clearFieldCriteriaInfo(true);
        currentSearchCriteriaIndex = -1;
    });

    EntityListValuesAC = $('select:.EntityValues').AutoComplete({
        //serviceURL: systemConfig.ApplicationURL_Common + systemConfig.EntityWebService + "FindAll",
        autoLoad: true, multiSelect: true, customParams: true
    });
    EntityListValuesAC.set_Args('{mainEntity:true}');

    LoadDatePicker($('input:.date-Value1,input:.date-Value2'));

    if (groupId > 0) {
        $('.viewedit-display-block').show();
        $('.viewedit-display-none').hide();
        disableGroupInfo(true);
        disableControls(true);
        getGroup(groupId);
        $('a:.ActionFullView-link').attr('href', systemConfig.ApplicationURL + systemConfig.pageURLs.GroupContacts + '?gid=' + groupId);
    }
    else {
        disabledMode = false;
        $('.viewedit-display-block').hide();
        $('.viewedit-display-none').show();
        hideLoading();
        $('.ActionFullView-link').remove();
    }


    $('.ActionEdit-link').click(function () {
        showLoading($('#demo1'));
        lockEntity(currentEntity, groupId, function () {
            disabledMode = false;
            $('.viewedit-display-block').hide();
            $('.viewedit-display-none').show();
            $('.ContentRow:last').find('.lst-operation,.Operation').hide();
            disableGroupInfo(false);
            disableControls(false);
        }, currentEntityLastUpdatedDate, function () {
            getGroup(groupId);
        }, function () {
            window.location = systemConfig.ApplicationURL_Common + systemConfig.pageURLs.ListGroup;
        });
    });



    $('.ActionCancel-link').click(function () {
        showLoading($('#demo1'));
        disabledMode = true;
        UnlockEntity(currentEntity, groupId, function () {
            if (groupId > 0) {
                $('.viewedit-display-block').show();
                $('.viewedit-display-none').hide();
                disableGroupInfo(false);
                disableControls(true);
                clearFieldCriteriaInfo(false);
                getGroup(groupId);
            }
            else {
                window.location = systemConfig.ApplicationURL_Common + systemConfig.pageURLs.ListGroup;
            }
        });
    });


    $('.ActionSave-link').click(function () {
        saveGroup();
    });
}

function disableControls(disable) {
    EntityFieldAC.Disabled(disable);
    EntityListValuesAC.Disabled(disable);
    if (disable) {
        $('.txt-Value,.date-Value1,.date-Value2').attr('readonly', 'readonly');
        $('radio,select:.Filter,input:.save-field,input:.clear-field,select:.lst-operation').attr('disabled', 'disabled');
    }
    else {
        $('.txt-Value,.date-Value1,.date-Value2,radio').removeAttr('readonly');
        $('radio,select:.Filter,input:.save-field,input:.clear-field,select:.lst-operation').removeAttr('disabled');
    }
}

function disableGroupInfo(disable) {
    EntityAC.Disabled(disable);
    if (disable) {
        $('input:.GroupName,textarea:.Description').attr('readonly', 'readonly');
        //$('button:.continue-field').attr('disabled', 'disabled');
    }
    else {
        $('input:.GroupName,textarea:.Description').removeAttr('readonly');
        //$('button:.continue-field').removeAttr('disabled');
    }
}

function getTemplate() {
    RowTemplate = $('.ContentRow').clone();
    $('.ContentRow').remove();
}


function entityChanged(item) {
    //showOkayMsg
    searchCriteriaList = new Array();
    $('.ContentRow').remove();
    currentSearchCriteriaIndex = -1;

    if (item) {
        EntityFieldAC.clear();
        EntityFieldAC.Disabled(false);
        EntityFieldAC.set_Args('{entityId:' + item.value + '}');
    }
    else {
        EntityFieldAC.clear();
        EntityFieldAC.Disabled(true);
    }
}

function entityFieldChanged(item) {
    if (item) {
        if (item.IsDataType) {
            EntityListValuesAC.set_ServiceRL(systemConfig.ApplicationURL + systemConfig.dataTypeContentServiceURL + "FindAll");
            EntityListValuesAC.set_CustomArgs('{parentId:' + item.DataTypeId + '}');
        }
        else {
            EntityListValuesAC.set_ServiceRL(systemConfig.ApplicationURL + item.ServiceURL + item.ServiceMethod);
            EntityListValuesAC.set_DefaultArgs();
            //EntityListValuesAC.set_CustomArgs('{parentId:' + item.DataTypeId + '}');
        }


        clearFieldCriteriaInfo(false);
        fillFilters(item.ControlId);
        setRadioLabels(item);
        var fieldInfo = getFieldInfo(item.value);
        if (fieldInfo)
            showFieldControls(fieldInfo.controlId, fieldInfo.filter);
        else
            showFieldControls(item.ControlId, 1, null);
    }
    else
        clearFieldCriteriaInfo(true);
}

function setRadioLabels(item) {
    if (item.ControlId == 5 || item.controlId == 5) {
        var labels = item.UiValue.split(',');
        if (labels && labels.length > 0) {
            $('span:.span-Value1-y').text(labels[0]);
            if (labels[1]) {
                $('span:.span-Value1-n').text(labels[1]);
            }
        }
    }
}

function getValue1(controlId) {
    var val = { value: '', label: '', items: null };
    if (controlId == 1) {
        if (EntityListValuesAC.get_Items()) {
            val.items = new Array();
            $.each(EntityListValuesAC.get_Items(), function (index, item) {
                val.label += item.label + ', ';
                val.value += item.value + ',';
                val.items.push(item);
            });
        }
    }
    else if (controlId == 2) {
        val.value = $('.txt-Value').val().trim();
        val.label = val.value;
    }
    else if (controlId == 5) {
        if ($('.chk-Value1-a').attr('checked')) {
            val.label = 'All';
            val.value = 1;
        }
        else if ($('.chk-Value1-y').attr('checked')) {
            val.label = $('span:.span-Value1-y').html().trim();
            val.value = 2;
        }
        else {
            val.label = $('span:.span-Value1-n').html().trim();
            val.value = 3;
        }
    }
    else if (controlId == 6) {
        val.value = $('.date-Value1').val().trim();
        val.label = val.value;
    }

    return val;
}

function getValue2(controlId) {
    var val = { value: '', label: '', items: null };

    if (controlId == 6) {
        val.value = $('.date-Value2').val().trim();
        val.label = val.value;
    }

    return val;
}

function setValue1(controlId, value) {
    if (controlId == 1) {
        EntityListValuesAC.clear();
        if (value.items) {
            $.each(value.items, function (index, item) {
                EntityListValuesAC.append_Item(item);
            });
        }
    }
    else if (controlId == 2) {
        $('.txt-Value').val(value.label)
    }
    else if (controlId == 5) {
        //$('control-checkbox checkbox').removeAttr('checked');
        if (value.value == 2) {
            $('.chk-Value1-y').attr('checked', 'checked');
        }
        else if (value.value == 3) {
            $('.chk-Value1-n').attr('checked', 'checked');
        }
        else {
            $('.chk-Value1-a').attr('checked', 'checked');
        }
    }
    else if (controlId == 6) {
        $('.date-Value1').val(value.label)
    }
}

function setValue2(controlId, value) {
    if (controlId == 6) {
        $('.date-Value2').val(value.label);
    }
}

function validateField() {
    $('.validation').remove();
    var filter = $('select:.Filter').val();
    var field = EntityFieldAC.get_Item();
    if (!saveForm($('.group-search-criteria')))
        return false;

    if (field.ControlId == 1 && (!EntityListValuesAC.get_Items())) {
        addPopupMessage($('select:.EntityValues'), 'required');
        return false;
    }
    else if (field.ControlId == 2 && (!$('input:.txt-Value').val())) {
        addPopupMessage($('input:.txt-Value'), 'required');
        return false;
    }
    //    else if (field.ControlId == 5) {
    //        //addPopupMessage($('input:.txt-Value'), 'required');
    //        //return false;
    //    }
    else if (field.ControlId == 6) {
        if (!$('input:.date-Value1').val().trim()) {
            addPopupMessage($('input:.date-Value1'), 'required');
            return false;
        }
        if (filter == 5) {
            if (!$('input:.date-Value2').val().trim()) {
                addPopupMessage($('input:.date-Value2'), 'required');
                return false;
            }
        }
    }
    return true;
}

function saveField() {

    if (!validateField())
        return;

    var fieldItem = EntityFieldAC.get_Item();
    var value1 = getValue1(fieldItem.ControlId);
    var value2 = getValue2(fieldItem.ControlId);
    var filter = $('select:.Filter option:selected').val();

    var criteria = {
        fieldItem: fieldItem,
        fieldId: fieldItem.value,
        fieldLabel: fieldItem.label,
        value1: value1,
        value2: value2,
        filter: filter,
        filterLabel: $('select:.Filter option:selected').html(),
        controlId: fieldItem.ControlId,
        Operation: 'AND',
        StartBracket: '',
        EndBracket: ''
    };

    if (currentSearchCriteriaIndex >= 0) {
        searchCriteriaList[currentSearchCriteriaIndex] = criteria;
        appendNewRow(criteria, currentSearchCriteriaIndex + 1, true, false);
    }
    else {
        searchCriteriaList.push(criteria);
        appendNewRow(criteria, searchCriteriaList.length, false, false);
    }

    currentSearchCriteriaIndex = -1;
    clearFieldCriteriaInfo(true);
}

function appendNewRow(criteria, index, replace, loadData) {
    var template = RowTemplate.clone();
    template.find('span:.RowNo').text(index);
    template.find('span:.FieldName').text(criteria.fieldLabel);
    template.find('span:.Value1').text(criteria.value1.label);
    template.find('span:.Filter').text(criteria.filterLabel);
    template.find('span:.Value2').text(criteria.value2.label);
    template.find('select:.lst-operation').val(criteria.Operation);
    template.find('span:.Operation').text(criteria.Operation);
    template.find('.start-group').text(criteria.StartBracket);
    template.find('.end-group').text(criteria.EndBracket);

    template.find('.remove-btn').click(function () {
        if (disabledMode)
            return false;

        $.each(searchCriteriaList, function (ix, item) {
            if (item.fieldId == criteria.fieldId) {
                searchCriteriaList.splice(ix, 1);
                template.remove();
                setRowsIndex();

                if (ix == searchCriteriaList.length && searchCriteriaList.length > 1) {
                    $('.ContentRow:last').find('select:.lst-operation').hide();
                    searchCriteriaList[ix - 1].Operation = '';
                }
                else if (searchCriteriaList.length == 1) {
                    $('.ContentRow:first').find('select:.lst-operation').hide();
                }

                return false;
            }
        });

        if (EntityFieldAC.get_Item() && EntityFieldAC.get_Item().value == criteria.fieldId) {
            clearFieldCriteriaInfo(true);
            currentSearchCriteriaIndex = -1;
        }
        return false;
    });

    template.click(function () {

        $('.selected').removeClass('selected');
        $(this).addClass('selected');

        if (disabledMode)
            return false;
        clearFieldCriteriaInfo(true);
        fillFilters(criteria.controlId);
        var fieldInfo = getFieldInfo(criteria.fieldId);
        setRadioLabels(fieldInfo.fieldItem);
        showFieldControls(fieldInfo.controlId, fieldInfo.filter);
        return false;
    });

    template.find('select:.lst-operation').change(function () {
        $.each(searchCriteriaList, function (ix, item) {
            if (item.fieldId == criteria.fieldId) {
                searchCriteriaList[ix].Operation = template.find('select:.lst-operation').val();
                return false;
            }
        });
    });

    template.find('.td-group').mouseover(function () {
        if (disabledMode)
            return false;

        if (lastGroupClicked == 1) {
            template.find('.gr-end').show();
        }
        else if (lastGroupClicked == 2 || lastGroupClicked == 0) {
            template.find('.gr-start').show();
        }
    });

    template.find('.td-group').mouseout(function () {
        if (disabledMode)
            return false;

        if (lastGroupClicked == 0) {
            template.find('.gr-end').hide();
            template.find('.gr-start').hide();
        }
        //        if (lastGroupClicked == 1) {
        //            template.find('.gr-end').hide();
        //        }
        //        else if (lastGroupClicked == 2) {
        //            template.find('.gr-start').hide();
        //        }
    });

    template.find('.gr-start').click(function () {
        if (disabledMode)
            return false;

        $('.gr-start').hide();
        if (lastGroupClicked == 2) {
            $('.link-group').hide();
            lastGroupClicked = 0;
            //return false;
        }
        else {
            lastGroupClicked = 1;
            template.find('.gr-cancel').show();
            $('.gr-end').show();
        }

        var text = template.find('.start-group').text().trim() + '(';
        template.find('.start-group').text(text);

        $.each(searchCriteriaList, function (ix, item) {
            if (item.fieldId == criteria.fieldId) {
                searchCriteriaList[ix].StartBracket = text;
                return false;
            }
        });
    });

    template.find('.gr-end').click(function () {
        if (disabledMode)
            return false;

        $('.gr-end').hide();

        if (lastGroupClicked == 1) {
            $('.link-group').hide();
            lastGroupClicked = 0;
        }
        else {
            lastGroupClicked = 2;
            template.find('.gr-cancel').show();
            $('.gr-start').show();
        }

        var text = template.find('.end-group').text().trim() + ')'
        template.find('.end-group').text(text);

        $.each(searchCriteriaList, function (ix, item) {
            if (item.fieldId == criteria.fieldId) {
                searchCriteriaList[ix].EndBracket = text;
                return false;
            }
        });
    });

    template.find('.gr-cancel').click(function () {
        if (disabledMode)
            return false;

        lastGroupClicked = 0;
        $('.link-group').hide();
        template.find('.end-group,.start-group').text('');
        $.each(searchCriteriaList, function (ix, item) {
            if (item.fieldId == criteria.fieldId) {
                searchCriteriaList[ix].EndBracket = '';
                searchCriteriaList[ix].StartBracket = '';
                return false;
            }
        });
    });




    if (!replace) {
        $('.search-criteria-info').children().append(template);
        if (index == 1) {
            template.find('select:.lst-operation,span:.Operation').hide();
        }
        else if (index > 1) {
            $('.ContentRow').find('select:.lst-operation').show();
            //$('.ContentRow:last').before().find('select:.lst-operation').show();
            $('.ContentRow:last').find('select:.lst-operation,span:.Operation').hide();
        }
    }
    else {
        $('.search-criteria-info tr:eq(' + (index + 1) + ')').replaceWith(template);
        //replaceWith
    }

    if (loadData) {
        $('.ContentRow').find('select:.lst-operation,.remove-btn').hide();
        $('.ContentRow').find('span:.Operation').show();
    }
    else if (index > 1) {
        $('.ContentRow').find('select:.lst-operation:not(:last-child),.remove-btn:not(:last-child)').show();
        $('.ContentRow').find('span:.Operation').hide();
    }
}



function setRowsIndex() {
    $('.search-criteria-info .ContentRow').each(function (index, item) {
        $(this).find('.RowNo').text(index + 1);
    });
}


function clearFieldCriteriaInfo(clearEntityField) {
    if (clearEntityField) {
        EntityFieldAC.clear();
        EntityFieldAC.Disabled(false);
    }
    fillFilters(2);
    $('input:.txt-Value,.date-Value1,.date-Value2').val('');
    EntityListValuesAC.clear();
    $('.chk-Value1-y,.chk-Value1-n').removeAttr('checked');
    $('.chk-Value1-a').attr('checked', 'checked');
    $('.control').hide();
    $('.control-txt').show();
}

function getFieldInfo(fieldId) {
    var fieldInfo = null;
    if (searchCriteriaList.length == 0)
        return null;

    $.each(searchCriteriaList, function (index, item) {
        if (item.fieldId == fieldId) {
            EntityFieldAC.set_Item(item.fieldItem);
            //entityFieldChanged(item.fieldItem);
            setValue1(item.controlId, item.value1);
            setValue2(item.controlId, item.value2);

            if (item.fieldItem.IsDataType) {
                EntityListValuesAC.set_ServiceRL(systemConfig.ApplicationURL + systemConfig.dataTypeContentServiceURL + "FindAll");
                EntityListValuesAC.set_CustomArgs('{parentId:' + item.fieldItem.DataTypeId + '}');
            }
            else {
                EntityListValuesAC.set_ServiceRL(systemConfig.ApplicationURL + item.fieldItem.ServiceURL + item.fieldItem.ServiceMethod);
                EntityListValuesAC.set_DefaultArgs();
                //EntityListValuesAC.set_CustomArgs('{parentId:' + item.DataTypeId + '}');
            }


            $('select:.Filter').val(item.filter);

            if (item.controlId == 6 && item.filter == 2)
                $('.Value2').show();

            currentSearchCriteriaIndex = index;
            EntityFieldAC.Disabled(true);

            fieldInfo = item;
            return false;
        }
    });
    return fieldInfo;
}


function showFieldControls(controlId, filter) {
    $('.control').hide();
    switch (controlId) {
        case 1:
            {
                $('.control-ac').show();
                break;
            }
        case 2:
            {
                $('.control-txt').show();
                break;
            }
        case 5:
            {
                $('.control-checkbox').show();
                break;
            }
        case 6:
            {
                $('.control-date').show();
                if (filter == 5) {
                    $('.control-val2').show();
                }
                break;
            }
    }
}

function showFieldBetweenControls(filter, show) {
    if (Number(filter) == 5) {
        if (show) {
            $('.control-val2').show();
        }
        else {
            $('.control-val2').hide();
        }
    }
    else {
        $('.control-val2').hide();
    }
}



function fillFilters(controlId) {
    $('select:.Filter option').remove();
    $('select:.Filter').unbind('change');
    $('select:.Filter').removeAttr('disabled');
    //$('.Value2').hide();
    switch (controlId) {
        case 1:
            {
                $('select:.Filter').append($('<option value="1">Equal</option>'));
                $('select:.Filter').attr('disabled', 'disabled');
                break;
            }
        case 2:
            {
                $('select:.Filter').append($('<option value="1">Equal</option>'));
                $('select:.Filter').append($('<option value="2">Contains</option>'));
                $('select:.Filter').append($('<option value="3">Start With</option>'));
                $('select:.Filter').append($('<option value="4">End With</option>'));
                break;
            }
        case 5:
            {
                $('select:.Filter').append($('<option value="1">Equal</option>'));
                $('select:.Filter').attr('disabled', 'disabled');
                //                $('select:.Operation').append($('<option value="1">All</option>'));
                //                $('select:.Operation').append($('<option value="2">True</option>'));
                //                $('select:.Operation').append($('<option value="3">False</option>'));
                break;
            }
        case 6:
            {
                $('select:.Filter').append($('<option value="1">Equal</option>'));
                $('select:.Filter').append($('<option value="5">Between</option>'));
                $('select:.Filter').append($('<option value="3">After</option>'));
                $('select:.Filter').append($('<option value="4">Before</option>'));

                $('select:.Filter').change(filterChanged);
                break;
            }
    }

    $('select:.Filter').val(1);
}
//showFieldBetweenControls
function filterChanged() {
    var value = $('select:.Filter option:selected').val();
    showFieldBetweenControls(value, value == 5);
}

function validateGroupInfo() {
    $('.validation').remove();
    if (!saveForm($('.group-info'))) {
        return false;
    }

    if (searchCriteriaList.length <= 0) {
        showOkayMsg('Required', 'please select search criteria');
        return false;
    }


    if (!validateBrackets())
        return false;

    return true;
}

function validateBrackets() {
    var bracketsArr = new Array();
    var opendBracketCount = 0, closedBracketCount = 0;
    //debugger;

    $.each(searchCriteriaList, function (index, item) {
        var startBrackets = item.StartBracket ? (item.StartBracket.length == 1 ? '(' : item.StartBracket.substring(0, item.StartBracket.length)) : '';
        var endtBrackets = item.EndBracket ? (item.EndBracket.length == 1 ? ')' : item.EndBracket.substring(0, item.EndBracket.length)) : '';
        //debugger;
        $.each(startBrackets, function () {
            //debugger;
            var obj = {
                RowIndex: index,
                IsOpenBracket: true,
                IsClosed: false,
                IsOpened: false
            };
            bracketsArr.push(obj);
            opendBracketCount++;
        });

        $.each(endtBrackets, function () {
            //debugger;
            var obj = {
                RowIndex: index,
                IsOpenBracket: false,
                IsClosed: false,
                IsOpened: false
            };
            bracketsArr.push(obj);
            closedBracketCount++;
        });


    });

    if (closedBracketCount != opendBracketCount) {
        showOkayMsg('Brackets Count Error', 'No. of opened bracket doesn\'t match no. of closed brackets');
        return false;
    }


    if (bracketsArr.length == 0) {
        return true;
    }

    if (!bracketsArr[0].IsOpenBracket) {
        addPopupMessage($('.ContentRow:first').find('.start-group'), 'error');
        return false;
    }

    var currentBracketIndex = 0, lastIndexPosition = 0, stopLoop = false;
    //debugger;
    var currentBracket = bracketsArr[0];
    var valid = true;
    while (!stopLoop) {
        //currentBracketIndex = 0;
        stopLoop = false;
        $.each(bracketsArr, function (ix, item) {

            if (ix == 0 || (item.IsClosed && item.IsOpened))
                return;

            if (item.IsOpenBracket) {
                stopLoop = true;

                if (ix == bracketsArr.length - 1) {
                    //alert('error');
                    stopLoop = true;
                    valid = false;
                    return false;
                }

            }
            else {
                item.IsClosed = true;
                item.IsOpened = true;
                var parentOpendBracketFound = false;
                for (var i = ix - 1; i >= 0; i--) {
                    var obj = bracketsArr[i];
                    if (!obj.IsClosed && !obj.IsOpened) {
                        bracketsArr[i].IsClosed = true;
                        bracketsArr[i].IsOpened = true;
                        parentOpendBracketFound = true;
                        break;
                    }
                }

                if (!parentOpendBracketFound) {
                    valid = false;
                    stopLoop = true;
                }
                else
                    stopLoop = false;

                stopLoop = ix == bracketsArr.length - 1
                return false;
            }
        });
    }

    if (!valid)
        showOkayMsg('Brackets Error', 'Brackets combination error');

    return valid;

}

function saveGroup() {
    if (!validateGroupInfo()) {
        return false;
    }

    showLoading($('#demo1'));

    var info = getGroupInfo();
    var post = formatString('{groupInfo:"{0}"}', escape(info));
    $.ajax({
        type: 'post',
        data: post,
        url: systemConfig.ApplicationURL_Common + systemConfig.GroupWebService + (groupId > 0 ? 'Edit' : 'Add'),
        dataType: 'json',
        contentType: 'application/json; charset=utf8',
        headers: getEntitylockAjaxHeaders(getEntityLockDateFormat(currentEntityLastUpdatedDate)),
        success: function (data) {

            var result = eval('(' + data.d + ')');
            if (result.statusCode.Code) {

                if (result.statusCode.Code == 501) {
                    addPopupMessage($('input:.GroupName'), 'Already exists');
                }
                else {
                    showStatusMsg('saved successfully');
                    if (groupId > 0) {
                        disabledMode = true;
                        showLoading($('#demo1'));
                        clearFieldCriteriaInfo(false);
                        $('.viewedit-display-block').show();
                        $('.viewedit-display-none').hide();
                        searchCriteriaList = new Array();
                        disableGroupInfo(false);
                        disableControls(true);
                        $('.ContentRow').remove();
                        currentSearchCriteriaIndex = -1;
                        getGroup(groupId);
                    }
                    else {
                        window.location = systemConfig.ApplicationURL_Common + systemConfig.pageURLs.ListGroup + '?key=' + result.result;
                    }
                }
            }
            else {
                showStatusMsg('system error');
            }

            hideLoading();

        },
        error: function (ex) {
            hideLoading();
            showStatusMsg('system error');
        }
    });
}

function getGroupInfo() {
    var group = {
        GroupId: groupId,
        GroupNameEn: $('input:.GroupName').val().trim(),
        Description: $('input:.GroupName').val().trim(),
        ModuleId: EntityAC.get_Item().value,
        GroupSearchCriteriaList: new Array()
    };

    $.each(searchCriteriaList, function (index, item) {
        var obj = {
            GroupId: groupId,
            FieldId: item.fieldId,
            FilterId: item.filter,
            ControlId: item.controlId,
            Value1: '',
            Value2: '',
            Operation: item.Operation ? item.Operation : 'AND',
            StartBracket: item.StartBracket,
            EndBracket: item.EndBracket
        };

        if (item.controlId == 1) {
            var acItems = new Array();
            $.each(item.value1.items, function () {
                var acItem = {
                    label: this.label,
                    value: this.value
                };
                acItems.push(acItem);
            });

            obj.Value1 = JSON.stringify(acItems);
        }
        else if (item.controlId == 5) {
            obj.Value1 = item.value1.value; //== true ? '1' : (item.value1.value == false) ? '0' : '';
        }
        else {
            obj.Value1 = item.value1.label
            obj.Value2 = item.value2.label
        }
        group.GroupSearchCriteriaList.push(obj);

    });

    group.GroupSearchCriteriaList[group.GroupSearchCriteriaList.length - 1].Operation = '';

    //    if (group.GroupSearchCriteriaList.length == 1) {
    //        group.GroupSearchCriteriaList[0].Operation = '';
    //    }
    //    else if (group.GroupSearchCriteriaList.length > 1) {
    //        group.GroupSearchCriteriaList[group.GroupSearchCriteriaList.length - 1].Operation = '';
    //    }

    return JSON.stringify(group);
}



function getGroup(groupId) {
    searchCriteriaList = new Array();
    $('.ContentRow').remove();
    currentSearchCriteriaIndex = -1;

    showLoading($('#demo1'));
    var args = '{id:' + groupId + '}';
    $.ajax({
        type: 'post',
        url: systemConfig.ApplicationURL_Common + systemConfig.GroupWebService + "FindByIdLite",
        data: args,
        dataType: 'json',
        contentType: 'application/json; charset=utf8',
        success: function (data) {

            var result = eval('(' + data.d + ')');
            if (result.result) {
                loadGroupInfo(result.result);
            }
            else {
                //error
            }
            hideLoading();
        },
        error: function (ex) {
            hideLoading();
        }
    });
}

function loadGroupInfo(group) {

    $('input:.GroupName').val(group.GroupNameEn);
    $('textarea:.Description').text(group.Description);
    $('span:.GroupName').text(group.GroupNameEn);
    $('span:.Description').text(group.Description);

    var entityItem = { label: group.EntityName, value: group.EntityNameId };
    EntityAC.set_Item(entityItem);
    EntityFieldAC.set_Args('{entityId:' + group.EntityNameId + '}');
    $('span:.Entity').text(entityItem.label);
    currentEntityLastUpdatedDate = group.UpdatedDate;

    if (group.GroupSearchCriteriaList) {
        $.each(group.GroupSearchCriteriaList, function (index, item) {

            var criteria = {
                fieldItem: {
                    label: item.UiNameEn,
                    value: item.EntityControlId,
                    ServiceMethod: item.ServiceURL,
                    IsDataType: item.IsDataType,
                    DataTypeId: item.DataTypeId,
                    ServiceURL: item.ServiceURL,
                    ServiceMethod: item.ServiceMethod
                },
                fieldId: item.EntityControlId,
                fieldLabel: item.UiNameEn,
                value1: getItemValue1(item), //    { label: item.Value1 },
                value2: getItemValue2(item), // { label: item.Value2 },
                filter: item.FilterId,
                filterLabel: getFilterLabel(item.ControlId, item.FilterId),
                controlId: item.ControlId,
                Operation: item.Operation ? item.Operation : '',
                StartBracket: item.StartBracket,
                EndBracket: item.EndBracket,
                IsDataType: item.IsDataType,
                DataTypeId: item.DataTypeId
            };

            criteria.fieldItem.controlId = item.ControlId;
            criteria.fieldItem.ControlId = item.ControlId;
            criteria.fieldItem.UiValue = item.UiValue;

            searchCriteriaList.push(criteria);
            appendNewRow(criteria, searchCriteriaList.length, false, true);


        });
    }

    hideLoading();
}


function getFilterLabel(controlId, filterId) {
    if (controlId == 1) {
        return "Equal";
    }
    else if (controlId == 2) {
        if (filterId == 1) {
            return "Equal";
        }
        else if (filterId == 2) {
            return "Contains";
        }
        else if (filterId == 3) {
            return "Start With";
        }
        else if (filterId == 4) {
            return "End With";
        }
    }
    else if (controlId == 5) {
        if (filterId == 1) {
            return "All";
        }
        else if (filterId == 2) {
            return "True";
        }
        else if (filterId == 3) {
            return "False";
        }
    }
    else if (controlId == 6) {
        if (filterId == 1) {
            return "Equal";
        }
        else if (filterId == 2) {
            return "Between";
        }
        else if (filterId == 3) {
            return "After";
        }
        else if (filterId == 4) {
            return "Before";
        }
        else if (filterId == 5) {
            return "Between";
        }
    }
}


function getItemValue1(item) {
    var val = { value: '', label: '', items: null };

    if (!item.Value1)
        return val;

    if (item.ControlId == 1) {
        var items = eval('(' + item.Value1 + ')');
        val.items = new Array();
        $.each(items, function (index, obj) {
            val.label += obj.label + ', ';
            val.value += obj.value + ',';
            val.items.push(obj);
        });
    }
    else if (item.ControlId == 2) {
        val.value = item.Value1
        val.label = val.value;
    }
    else if (item.ControlId == 5) {
        var labels = item.UiValue.split(',');
        if (item.Value1 == 2) {
            val.label = labels[0];
            val.value = 2;
        }
        else if (item.Value1 == 3) {
            val.label = labels[1];
            val.value = 3;
        }
        else {
            val.label = "All";
            val.value = 1;
        }
    }
    else if (item.ControlId == 6) {
        val.value = item.Value1;
        val.label = val.value;
    }

    return val;
}

function getItemValue2(item) {
    var val = { value: '', label: '', items: null };

    if (item.ControlId == 6) {
        val.value = item.Value2;
        val.label = val.value;
    }

    return val;
}