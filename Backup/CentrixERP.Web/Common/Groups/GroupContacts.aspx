<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GroupContacts.aspx.cs"
    Inherits="CentrixERP.Common.Web.NewCommon.Common.Groups.GroupContacts"
    MasterPageFile="../MasterPage/Site.Master" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="PageHeader">
    <script src="JS/GroupContacts.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="PageBody">
    <div id='content'>
        <div class="Add-new-form">
            <div class="Add-title">
                <span>Group Contacts<span>
                    <div class="Action-div">
                        <ul>
                            <li class="save-cancel-pnl viewedit-display-none"><span class="ActionCancel-link"></span>
                                <a class="ActionCancel-link">Close</a></li>
                        </ul>
                    </div>
            </div>
            <table border="0" cellpadding="0" cellspacing="0" class="Add-new-Table group-info">
                <tr>
                    <th>
                        <span class="label-title">Group Name</span>
                    </th>
                    <td>
                        <span class="label-data nowrap GroupName"></span>
                    </td>
                    <th>
                        <span class="label-title">Entity</span>
                    </th>
                    <td>
                        <span class="label-data nowrap Entity"></span>
                    </td>
                </tr>
                <tr>
                    <th>
                        <span class="label-title">Description</span>
                    </th>
                    <td>
                        <span class="label-data nowrap Description"></span>
                    </td>
                </tr>
            </table>
        </div>
        <div class="five-px">
        </div>
        <div class="Add-new-form">
            <div class="Add-title">
                <span>Contacts</span>
            </div>
            <table width="100%" border="0" cellpadding="0" cellspacing="1" class="search-criteria-info">
                <tbody>
                    <tr>
                        <th class="th-data-table th-item-num">
                            #
                        </th>
                        <th class="th-data-table">
                            Contact Name
                        </th>
                        <th class="th-data-table">
                            Email
                        </th>
                        <th class="th-data-table ">
                            Mobile
                        </th>
                    </tr>
                    <tr>
                        <td class="td-data-border" colspan="8">
                        </td>
                    </tr>
                    <tr class="ContentRow">
                        <td class="td-r1-data-table center-content">
                            <span class="RowNo">#</span>
                        </td>
                        <td class="td-r1-data-table center-content">
                            <a class="view-url lnkLabel"><span class="ContactName"></span></a>
                        </td>
                        <td class="td-r1-data-table center-content">
                            <span class="Email"></span>
                        </td>
                        <td class="td-r1-data-table center-content">
                            <span class="Mobile"></span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</asp:Content>
