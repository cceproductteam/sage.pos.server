using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CentrixERP.Configuration;

namespace CentrixERP.Common.Web.NewCommon.Common.Groups
{
    public partial class AddEditGroup : BasePageLoggedIn
    {
        public AddEditGroup()
            : base(Enums_S3.Permissions.SystemModules.None) { }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}