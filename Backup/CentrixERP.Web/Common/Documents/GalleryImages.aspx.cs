﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Krystalware.SlickUpload;
using Krystalware.SlickUpload.Web.Controls;
using Krystalware.SlickUpload.Configuration;
using SF.Framework;
using System.Web.Script.Serialization;
using System.IO;
using System.Configuration;
using CentrixERP.Common.Business.IManager;
using CentrixERP.Configuration;
using E = CentrixERP.Common.Business.Entity;
using System.Linq;
using Centrix.UM.Business.IManager;
using Centrix.UM.Business.Entity;
using CentrixERP.Common.Business.Entity;

namespace CentrixERP.Common.Web.Documents
{
    public partial class GalleryImages : BasePageLoggedIn
    {
        public string path = null;
        public int parentId { get; set; }
        public string moduleKey { get; set; }
        public int FolderId { get; set; }
        public string moduleName { get; set; }
        public string FolderArr { get; set; }
        public string SaveString = string.Empty;
        public string AddFolder = string.Empty;
        public string ParentFolderID = null;
        public int entityId;
        public int ParentEntityId;
        public int DisableAttachment = 0;
        //public string subFolder = null;
        //public int parentFolder;
        public int DocumentParentFolderId;
        public List<RolePermissionLite> RolePermissionsList = null;
        IRolePermissionManager iRolePermissionManager = null;
        public JavaScriptSerializer javaScriptSerializer;
        public string PermissionsList = null;
        public string DocumentPath { get; set; }
        public int DocumentCount { get; set; }
        public int EntityValueId { get; set; }

        public GalleryImages()
            : base(Enums_S3.Permissions.SystemModules.ViewDocument, false)
        {
            
        }

        IDocumentFolderManager docFolderManager = (IDocumentFolderManager)IoC.Instance.Resolve(typeof(IDocumentFolderManager));

        protected void Page_Load(object sender, EventArgs e)
        {
            CheckAttachmentsPermission();

            path = CentrixERP.Configuration.Configuration.GetConfigKeyPath("DocumentsFolderPath");
            SaveString = GetLocalResourceObject("Save.Text").ToString();
            AddFolder = GetLocalResourceObject("AddFolder.Text").ToString();
            parentId = Request["id"].ToNumber();
            moduleKey = Request["key"];
            FolderId = Request["folderId"].ToNumber();
            moduleName = Request["modulename"];
            entityId = Request["eid"].ToNumber();
            ParentEntityId = Request["parententityid"].ToNumber();
            DocumentParentFolderId = hdnSubFolder.Value.ToNumber();
            ParentFolderID = parentId.ToString();
            DisableAttachment = Request["disabled"].ToNumber();
            DocumentPath = CentrixERP.Configuration.Configuration.GetConfigKeyValue("NewDocumentsURL");
            bool hasAddPermission = false;
            bool hasViewPermission = false;
            DocumentCount = CalcDocumentCount();
            EntityValueId = Request["entityValueId"].ToNumber();

            if (RolePermissionsList != null && RolePermissionsList.Count() > 0)
            {
                var existsPermission = from RolePermissionLite rolePer in RolePermissionsList
                                       where rolePer.PermissionId == (int)Enums_S3.DefaultPermissions.Add && rolePer.HasPermission == true
                                       select rolePer;
                hasAddPermission = existsPermission.Count() > 0;
            }

            if (RolePermissionsList != null && RolePermissionsList.Count() > 0)
            {
                var existsPermission = from RolePermissionLite rolePer in RolePermissionsList
                                       where rolePer.PermissionId == (int)Enums_S3.DefaultPermissions.View && rolePer.HasPermission == true
                                       select rolePer;
                hasViewPermission = existsPermission.Count() > 0;
            }



            if (DisableAttachment == 1 || !hasAddPermission || !hasViewPermission)
                fileSelector.Visible = false;

        }

        private void CheckAttachmentsPermission()
        {
            iRolePermissionManager = (IRolePermissionManager)IoC.Instance.Resolve(typeof(IRolePermissionManager));
            javaScriptSerializer = new JavaScriptSerializer();

            RolePermissionCriteria criteria = new RolePermissionCriteria() { EntityId = (int)Enums_S3.Entity.Attachments };
            RolePermissionsList = iRolePermissionManager.FindAllRolePermission(LoggedInUser.RoleId, criteria);
            PermissionsList = this.javaScriptSerializer.Serialize(RolePermissionsList);

        }

        private int CalcDocumentCount()
        {

            DocumentEntity documentEntity = new DocumentEntity();
            IDocumentEntityManager IDocumentEntityManager = (IDocumentEntityManager)IoC.Instance.Resolve(typeof(IDocumentEntityManager));
            DocumentEntityCriteria criteria = new DocumentEntityCriteria
            {
                ParentFolderId = FolderId,
                EntityId = moduleKey.ToNumber(),
                EntityValueId = parentId,
            };

            List<DocumentEntity> list = IDocumentEntityManager.FindByEntityId(criteria);
            if (list == null)
                return 0;
            else
                return list.Count();
        }
    }
}