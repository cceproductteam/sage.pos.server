using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Krystalware.SlickUpload;
using Krystalware.SlickUpload.Web.Controls;
using Krystalware.SlickUpload.Configuration;
using SF.Framework;
using System.Web.Script.Serialization;
using System.IO;
using System.Configuration;
using CentrixERP.Common.Business.IManager;
using E = CentrixERP.Common.Business.Entity;
using System.Linq;
using Centrix.UM.Business.IManager;
using Centrix.UM.Business.Entity;
using CentrixERP.Common.Business.Entity;
using CentrixERP.Common.Web;
using CentrixERP.Configuration;

namespace CentrixERP.Common.Web.Documents
{

    public partial class DocumentHTML5 : BasePageLoggedIn
    {
        public string path = null;
        public int parentId { get; set; }
        public string moduleKey { get; set; }
        public int FolderId { get; set; }
        public string moduleName { get; set; }
        public string FolderArr { get; set; }
        public string SaveString = string.Empty;
        public string AddFolder = string.Empty;
        public string ParentFolderID = null;
        public int entityId;
        public int ParentEntityId;
        public int DisableAttachment = 0;
        //public string subFolder = null;
        //public int parentFolder;
        public int DocumentParentFolderId;
        public List<RolePermissionLite> RolePermissionsList = null;
        IRolePermissionManager iRolePermissionManager = null;
        public JavaScriptSerializer javaScriptSerializer;
        public string PermissionsList = null;
        public string DocumentPath { get; set; }
        public int DocumentCount { get; set; }

        public DocumentHTML5()
            : base(Enums_S3.Permissions.SystemModules.ViewDocument, false)
        {

        }

        IDocumentFolderManager docFolderManager = (IDocumentFolderManager)IoC.Instance.Resolve(typeof(IDocumentFolderManager));

        protected void Page_Load(object sender, EventArgs e)
        {
            CheckAttachmentsPermission();

            path = CentrixERP.Configuration.Configuration.GetConfigKeyPath("DocumentsFolderPath");
            SaveString = GetLocalResourceObject("Save.Text").ToString();
            AddFolder = GetLocalResourceObject("AddFolder.Text").ToString();
            parentId = Request["id"].ToNumber();
            moduleKey = Request["key"];
            FolderId = Request["folderId"].ToNumber();
            moduleName = Request["modulename"];
            entityId = Request["eid"].ToNumber();
            ParentEntityId = Request["parententityid"].ToNumber();
            DocumentParentFolderId = hdnSubFolder.Value.ToNumber();
            ParentFolderID = parentId.ToString();
            DisableAttachment = Request["disabled"].ToNumber();
            DocumentPath = CentrixERP.Configuration.Configuration.GetConfigKeyValue("NewDocumentsURL");
            bool hasAddPermission = false;
            bool hasViewPermission = false;
            DocumentCount = CalcDocumentCount();

            if (RolePermissionsList != null && RolePermissionsList.Count() > 0)
            {
                var existsPermission = from RolePermissionLite rolePer in RolePermissionsList
                                       where rolePer.PermissionId == (int)Enums_S3.DefaultPermissions.Add && rolePer.HasPermission == true
                                       select rolePer;
                hasAddPermission = existsPermission.Count() > 0;
            }

            if (RolePermissionsList != null && RolePermissionsList.Count() > 0)
            {
                var existsPermission = from RolePermissionLite rolePer in RolePermissionsList
                                       where rolePer.PermissionId == (int)Enums_S3.DefaultPermissions.View && rolePer.HasPermission == true
                                       select rolePer;
                hasViewPermission = existsPermission.Count() > 0;
            }



            if (DisableAttachment == 1 || !hasAddPermission || !hasViewPermission)
                fileSelector.Visible = false;


        }

        private void CheckAttachmentsPermission()
        {
            iRolePermissionManager = (IRolePermissionManager)IoC.Instance.Resolve(typeof(IRolePermissionManager));
            javaScriptSerializer = new JavaScriptSerializer();

            RolePermissionCriteria criteria = new RolePermissionCriteria() { EntityId = (int)Enums_S3.Entity.Attachments };
            RolePermissionsList = iRolePermissionManager.FindAllRolePermission(LoggedInUser.RoleId, criteria);
            PermissionsList = this.javaScriptSerializer.Serialize(RolePermissionsList);

        }

        private int CalcDocumentCount()
        {

            DocumentEntity documentEntity = new DocumentEntity();
            IDocumentEntityManager IDocumentEntityManager = (IDocumentEntityManager)IoC.Instance.Resolve(typeof(IDocumentEntityManager));
            DocumentEntityCriteria criteria = new DocumentEntityCriteria
            {
                ParentFolderId = FolderId,
                EntityId = moduleKey.ToNumber(),
                EntityValueId = parentId,
            };

            List<DocumentEntity> list = IDocumentEntityManager.FindByEntityId(criteria);
            if (list == null)
                return 0;
            else
                return list.Count();
        }


        //protected void getFolders()
        //{
        //    //var root = path + moduleName + @"\" + parentId;
        //    List<E.DocumentFolder> documentFolderList = new List<E.DocumentFolder>();

        //    E.DocumentFolderCriteria criteria = new E.DocumentFolderCriteria();
        //    criteria.EntityID = entityId;
        //    criteria.parentFolder = FolderId;
        //    documentFolderList = docFolderManager.FindAll(criteria);

        //    //DirectoryInfo dirInfo = new DirectoryInfo(root);

        //    if (documentFolderList != null)
        //    {
        //        var allFolders = from E.DocumentFolder folder in documentFolderList
        //                         select new Folders()
        //                         {
        //                             name = folder.FolderName,
        //                             folderId = folder.FolderId
        //                         };

        //        JavaScriptSerializer OSerializer = new JavaScriptSerializer();
        //        FolderArr = OSerializer.Serialize(allFolders);

        //        //listFolders.Add(new Folders() { name = "Root", folders = getDirectories(documentFolderList, path), folderId = 1 });
        //    }

        //    //else
        //    //{
        //    //    Directory.CreateDirectory(root);
        //    //    listFolders.Add(new Folders() { name = "Root", path = ParentFolderID, folders = getDirectories(documentFolderList, path), folderId = 1 });
        //    //}  



        //}

        //private List<Folders> getDirectories(List<CentrixERP.Common.Business.Entity.DocumentFolder> documentFolderList, string parentFolder)
        //{
        //    List<Folders> listFolders = new List<Folders>();
        //    List<Folders> temp = new List<Folders>();
        //    temp.Add(new Folders() { name = "ff", path = "C:\\" });

        //    if (documentFolderList != null)
        //    {
        //        foreach (CentrixERP.Common.Business.Entity.DocumentFolder item in documentFolderList)
        //        {
        //            Folders myfs = new Folders()
        //            {
        //                name = item.FolderName,
        //                path = path + moduleName + @"\" + parentId,
        //                folderId = item.FolderId,
        //                folders = temp
        //            };
        //            listFolders.Add(myfs);
        //        }
        //    }
        //    //           folders = getSubDirectories(item.FolderId),
        //    //if (dirInfo.GetDirectories() != null)
        //    //{
        //    //    foreach (DirectoryInfo dir in dirInfo.GetDirectories())
        //    //    {

        //    //        Folders myfs = new Folders()
        //    //        {
        //    //            name = dir.Name,
        //    //            path = parentFolder + "/" + dir.Name,
        //    //            folders = getDirectories(dir, parentFolder + "/" + dir.Name)
        //    //        };
        //    //        listFolders.Add(myfs);
        //    //    }

        //    //}
        //    return listFolders;
        //}

        //private List<Folders> getSubDirectories(int parentFolder)
        //{
        //     DocumentFolderCriteria criteria = new DocumentFolderCriteria();
        //    criteria.EntityID = entityId;
        //    criteria.parentFolder = parentFolder;
        //    List<CentrixERP.Common.Business.Entity.DocumentFolder> documentFolderList = docFolderManager.FindAll(criteria);
        //   List<Folders> listFolders = new List<Folders>();

        //   if (documentFolderList != null)
        //   {
        //       foreach (CentrixERP.Common.Business.Entity.DocumentFolder child in documentFolderList)
        //       {

        //           Folders myfs = new Folders()
        //           {
        //               name = child.FolderName,
        //               path = parentFolder + "/" + child.FolderName,
        //               folders = getSubDirectories(child.FolderId),
        //               folderId = child.FolderId

        //           };
        //           listFolders.Add(myfs);
        //       }
        //   }
        //    return listFolders;

        //}

        //private List<Folders> getDirectories(DirectoryInfo dirInfo, string parentFolder)
        //{
        //    List<Folders> listFolders = new List<Folders>();


        //    if (dirInfo.GetDirectories() != null)
        //    {
        //        foreach (DirectoryInfo dir in dirInfo.GetDirectories())
        //        {

        //            Folders myfs = new Folders()
        //            {
        //                name = dir.Name,
        //                path = parentFolder + "/" + dir.Name,
        //                folders = getDirectories(dir, parentFolder + "/" + dir.Name)
        //            };
        //            listFolders.Add(myfs);
        //        }

        //    }
        //    return listFolders;
        //}
        //protected void GetSubFolders()
        //{
        //    var root = path + moduleName + @"\" + Folder;
        //    //var root = path;

        //    DirectoryInfo dirInfo = new DirectoryInfo(root);
        //    List<Folders> listFolders = new List<Folders>();
        //    if (dirInfo.Exists)
        //        listFolders.Add(new Folders() { name = "Root", path = ParentFolderID, folders = getDirectories(dirInfo, dirInfo.Name) });
        //    else
        //    {
        //        Directory.CreateDirectory(root);
        //        listFolders.Add(new Folders() { name = "Root", path = ParentFolderID, folders = getDirectories(dirInfo, dirInfo.Name) });
        //    }
        //    JavaScriptSerializer OSerializer = new JavaScriptSerializer();
        //    FolderArr = OSerializer.Serialize(listFolders);
        //}
    }

}