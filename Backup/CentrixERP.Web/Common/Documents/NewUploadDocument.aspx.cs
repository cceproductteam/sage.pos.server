﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CentrixERP.Common.Business.Factory;
using CentrixERP.Common.Business.Entity;
using CentrixERP.Common.Business.IManager;
using SF.Framework;
using SF.FrameworkEntity;
using System.IO;
using System.Configuration;
using System.Web.Script.Serialization;
using CentrixERP.Common.Web;

namespace CentrixERP.Common.Web.NewCommon.Common.Documents
{
    public partial class NewUploadDocument : BasePageLoggedIn
    {

        public NewUploadDocument()
            : base(CentrixERP.Configuration.Enums_S3.Permissions.SystemModules.None) { }

        public int EntityId = 0;
        public int EntityValueId = 0;
        //public string ModuleKey = "";
        //public int UserId = -1;
        public int DocumentId = -1;
        public string URLName = "";
        public string Filename = "";
        public int DocId = -1;
        public string VarDocId = "";
        public string UploadTypes = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext postedContext = HttpContext.Current;
            HttpPostedFile file = postedContext.Request.Files[0];
            string name = file.FileName;
            byte[] binaryWriteArray = new
            byte[file.InputStream.Length];
            file.InputStream.Read(binaryWriteArray, 0,
            (int)file.InputStream.Length);
            //FileStream objfilestream = new FileStream(Server.MapPath("uploads//" + name), FileMode.Create, FileAccess.ReadWrite);
            //objfilestream.Write(binaryWriteArray, 0,
            //binaryWriteArray.Length);
            //objfilestream.Close();
            //string[][] JaggedArray = new string[1][];
            //JaggedArray[0] = new string[] { "File was uploaded successfully" };
            //JavaScriptSerializer js = new JavaScriptSerializer();
            //string strJSON = js.Serialize(JaggedArray);
            //Response.Write(strJSON);
            //Response.End();



            EntityId = Request["entityId"].ToNumber();
            URLName = Request["URLName"];
            DocId = Request["DocId"].ToNumber();
            VarDocId = Request["VarDocId"];
            EntityValueId = Request["entityValueId"].ToNumber();
            UploadTypes = Request["UploadTypes"];

            /////////////////////////////
            IDocumentEntityManager iDocumentEntityManager = (IDocumentEntityManager)IoC.Instance.Resolve(typeof(IDocumentEntityManager));
            DocumentEntity documentEntity = new DocumentEntity();
            documentEntity.CreatedBy = LoggedInUser.UserId;
            documentEntity.EntityId = EntityId;
            documentEntity.EntityValueId = EntityValueId;
            documentEntity.DocumentObj = new Business.Entity.Document();
            documentEntity.DocumentObj.CreatedBy = LoggedInUser.UserId;
            documentEntity.DocumentObj.OrginalFileName = Path.GetFileNameWithoutExtension(file.FileName);
            documentEntity.DocumentObj.FileExtension = Path.GetExtension(file.FileName);
            documentEntity.DocumentObj.documentBytes = binaryWriteArray;
            documentEntity.DocumentObj.PreventPhysicalSave = false;
            //documentEntity.DocumentObj.DocumentPath = documentEntity.DocumentPath;
            documentEntity.DocumentObj.DocumentPath = CentrixERP.Configuration.Configuration.GetConfigKeyPath("DocumentsPath") + "CX-Entity-" + EntityId + "\\";
            documentEntity.DocumentObj.FolderId = 0;
            documentEntity.DocumentObj.ParentFolder = "1";
            documentEntity.DocumentObj.OldParentFolder = "1";
            documentEntity.DocumentObj.ParentEntityId = EntityValueId;
            iDocumentEntityManager.Save(documentEntity);
            DocumentId = documentEntity.DocumentId;
            //pnlDoc.Visible = true;
            /////////////////////////////////

            //byte[] binaryWriteArray = new
            //byte[file.InputStream.Length];
            //file.InputStream.Read(binaryWriteArray, 0,
            //(int)file.InputStream.Length);
            //FileStream objfilestream = new FileStream(CentrixERP.Configuration.Configuration.GetConfigKeyPath("DocumentsPath") + "CX-Entity-" + EntityId + "\\"+name, FileMode.Create, FileAccess.ReadWrite);
            //objfilestream.Write(binaryWriteArray, 0,
            //binaryWriteArray.Length);
            //objfilestream.Close();
            //string[][] JaggedArray = new string[1][];
            //JaggedArray[0] = new string[] { "File was uploaded successfully" };
            //JavaScriptSerializer js = new JavaScriptSerializer();
            //string strJSON = js.Serialize(JaggedArray);
            //Response.Write(strJSON);
            //Response.End();


            





        }
    }
}