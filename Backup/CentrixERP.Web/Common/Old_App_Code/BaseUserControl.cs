using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Threading;
using System.Globalization;
using CentrixERP.Common.Business.IManager;
using CentrixERP.Common.Business.Entity;
using CentrixERP.Configuration;
using SF.Framework;


namespace CentrixERP.Common.Web
{
    public class BaseUserControl : System.Web.UI.UserControl
    {
        public Enums_S3.Configuration.Language Lang { get; set; }
        public Enums_S3.Configuration.Language OppositeLang { get; set; }
        //public SSS.Business.Configuration config { get; set; }
        public Configration config { get; set; }
        public string CompanyDelimeter { get { return GetGlobalResourceObject("Globalvariables", "CompanyText").ToString().ToDelimiter(); } set { CompanyDelimeter = value; } }
        public string PersonDelimeter { get { return GetGlobalResourceObject("Globalvariables", "PersonText").ToString().ToDelimiter(); } set { PersonDelimeter = value; } }

        public int LangNumber { get; set; }
        public int OppositeLangNumber { get; set; }
        public static string ApplicationURL
        {
            get { return Configuration.Domain + Configuration.RootDirectory; }
            set { ApplicationURL = value; }
        }

        protected override void OnInit(EventArgs e)
        {
            //config = new SSS.Business.Configuration();
            //config.Get();
            IConfigrationManager myConfigMgr = (IConfigrationManager)IoC.Instance.Resolve(typeof(IConfigrationManager));
            config = myConfigMgr.FindById(-1, null);
            if (string.IsNullOrEmpty(Cookies.Get("lang")))
            {
                switch (config.DefaultLanguage)
                {
                    case Enums_S3.Configuration.Language.en:
                        setEnglishLanguage();
                        break;
                    case Enums_S3.Configuration.Language.ar:
                        setArabicLanguage();
                        break;
                    default:
                        setArabicLanguage();
                        break;
                }
            }
            else if (Cookies.Get("lang") == Enums_S3.Configuration.Language.en.ToString())
            {
                setEnglishLanguage();
            }
            else
            {
                setArabicLanguage();
            }
            base.OnInit(e);
        }

        private void setEnglishLanguage()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");
            Lang = Enums_S3.Configuration.Language.en;
            LangNumber = (int)Enums_S3.Configuration.Language.en;
            OppositeLangNumber = (int)Enums_S3.Configuration.Language.ar;
            OppositeLang = Enums_S3.Configuration.Language.ar;
        }

        private void setArabicLanguage()
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("ar-SA");
            Lang = Enums_S3.Configuration.Language.ar;
            LangNumber = (int)Enums_S3.Configuration.Language.ar;
            OppositeLangNumber = (int)Enums_S3.Configuration.Language.en;
            OppositeLang = Enums_S3.Configuration.Language.en;
        }
    }
}