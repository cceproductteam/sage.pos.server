using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using CentrixERP.Common.Business.Entity;
using System.Collections.Generic;
using System.Text;
using SF.Framework;
using CentrixERP.Common.Business.IManager;
using SFLib.CustomControls;
using SF.CustomScriptControls;
using CentrixERP.Configuration;

namespace CentrixERP.Common.Web
{
    public static class Configuration
    {

        public static string Domain
        {
            get { return ConfigurationManager.AppSettings["Domain"].ToString(); }
            set { Domain = value; }
        }

        public static string RootDirectory
        {
            get { return ConfigurationManager.AppSettings["RootDirectory"].ToString(); }
            set { RootDirectory = value; }
        }


    }
}