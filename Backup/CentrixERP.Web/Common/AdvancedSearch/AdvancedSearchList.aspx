<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdvancedSearchList.aspx.cs"
    MasterPageFile="~/Common/MasterPage/Site.master" Inherits="CentrixERP.Common.Web.NewCommon.Common.AdvancedSearch.AdvancedSearchList" %>

<asp:Content runat="server" ContentPlaceHolderID="PageHeader" ID="PageHeader">
    <script src="JS/AdvancedSearch.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <script type="text/javascript">
        function InnerPage_Load() {
            AdvancedSearch = true;
            mKey = '<%# mKey %>';
            InnerPage_Load2();
        } 

    </script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="PageBody" ID="PageBody">
    <div id='content' class="View-info">
        <div class="TabbedPanelsContent">
            <div class="QuickSearchDiv">   </div>
            <div class="TabbedPanelsContent-info">
                <div class="Add-new-form" style="min-height: 450px;">
                    <div class="Add-title">
                        <span>Results List</span>
                    </div>
                    <div class="QuickSearchResultDiv">
                    </div>
                    <div class="DragDropBackground NoDataFound">
                        <span class="background-text">No Data Found</span>
                    </div>
                </div>
                <div class="five-px">
                </div>
            </div>
        </div>
    </div>
</asp:Content>
