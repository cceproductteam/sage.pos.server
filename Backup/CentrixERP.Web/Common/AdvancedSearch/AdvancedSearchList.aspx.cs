using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CentrixERP.Configuration;
using SF.Framework;

namespace CentrixERP.Common.Web.NewCommon.Common.AdvancedSearch
{
    public partial class AdvancedSearchList  : BasePageLoggedIn
    {
        public AdvancedSearchList()
            : base(Enums_S3.Permissions.SystemModules.None) { }
        public string mKey;
        protected void Page_Load(object sender, EventArgs e)
        {
            mKey = Request["mKey"].ToString();
            this.Page.DataBind();
        }
    }
}