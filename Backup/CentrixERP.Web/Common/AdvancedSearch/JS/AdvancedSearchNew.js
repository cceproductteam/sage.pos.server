var AdvancedSearchTemplate;
var ControlsTemplate;
var ControlDiv = '';
var acArray = new Array(), ControlsList;
var ControlValue1 = '', ControlValue2 = '';
var EntityId = -1
var AdvancedSearch = false;
var QuickSearch = false;
var AdvancedSearchListTemplate;
var SortType = true;
var SortFields = '', SearchTemplate = '', SearchCriteriaObj = '';
var PageURL = '', WebServiceURL = '', mKey = '';
var SortingFieldAC;
var tr, td, th;
var LastId = 0;
pageNumber = 1;
resultCount = 10;
var ControlsArray = new Object();
var firstTimeLoading = true;
var filterSearchHeight = 0;
var filterSearchTop = 0;
var newSearch = true;
var EntityObj;


function InnerPage_Load2() {
    WebServiceURL = systemConfig.ApplicationURL_Common + systemConfig.AdvancedSearchWebServiceURL + "AdvancedSearchNew";
    PageURL = systemConfig.ApplicationURL_Common + systemConfig.pageURLs.AdvancedSearchList;
    GetTemaplte(systemConfig.ApplicationURL_Common + systemConfig.HTMLTemplateURLs.QuickSearchTemplate, function () { $('.Quick-Search-Div').html(QuickSearchTemplate); $('.Quick-Search-Div').find('.search-option').hide(); $('.Filter-search').height(filterSearchHeight); $('.Filter-search').css('top', filterSearchTop); }, 'QuickSearchTemplate');
    GetTemaplte(systemConfig.ApplicationURL_Common + systemConfig.HTMLTemplateURLs.AdvancedSearchControlsTemplate, function () {
        if (AdvancedSearch) {
            setPageTitle('Advanced Search');
            LoadSearchForm(mKey, AdvancedSearch);
            FindEntityInfo(mKey, true);
        }
    }, 'ControlsTemplate');

    $('a:.more-options').click(function () {
        if (!QuickSearch) // 19/3 mieassar
            searcholdWebserviceURL = findAllEntityServiceURL;
        // LoadSearchForm(mKey, false);
    });

    $('.LnkReset').live('click', function () {
        $('form').each(function () {
            this.reset();
            //Added By Ruba Al-Sa'di 26/10/2014 to reset AutoComplete controls
            if (ControlsList) {
                $.each(ControlsList, function (index, item) {
                    if (item.Control.ControlName == "AutoComplete") {
                        $.each(acArray, function (i, acItem) {
                            acItem.AC.clear();
                        });
                    }
                });
            }
        });
    });
}

$(document).keydown(function (event) {
    if (event.keyCode == 70 && event.ctrlKey) {
        event.preventDefault();
        if (!QuickSearch) // 19/3 mieassar
            searcholdWebserviceURL = findAllEntityServiceURL;
        LoadSearchForm(mKey, false);
    }
    else if (event.keyCode == 27) {
        $('.Filter-search').animate({
            top: filterSearchTop
        }, 500);
    }
});


function LoadSearchForm(ModuleKey, advancedSearchType) {
 //   debugger;
    $('.QuickSearchDiv').show();
    $('.Filter-search').animate({
        top: 0
    }, 500, function () {
        if (firstTimeLoading) {
            showLoading($('#demo2'));
            firstTimeLoading = false;
            var data = formatString('{mKey:"{0}",AdvancedSearchType:{1}}', ModuleKey, advancedSearchType);
            window.setTimeout(function () {
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: systemConfig.ApplicationURL_Common + systemConfig.AdvancedSearchWebServiceURL + "LoadSearchForm",
                    data: data,
                    datatype: "json",
                    success: function (data) {
                        ControlsList = eval("(" + data.d + ")").result;
                        var template = $(ControlsTemplate);
                        MapToControls(ControlsList, template);
                        hideLoading();
                    },
                    error: function (e) { }
                });
            }, 0);
        }
    });
}



function MapToControls(ControlsList, Template) {

    CreateAdvancedSearchList();
    var QuickSearchDivTemplate = $($('.QuickSearchDiv').html());
    if (AdvancedSearch) {
        QuickSearchDivTemplate.removeClass('Filter-search').addClass('Advanced-Filter-search');
        QuickSearchDivTemplate.find('.Action-div').hide();
    }

    SortingFieldAC = $(QuickSearchDivTemplate).find('select:.sortingField').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.AdvancedSearchWebServiceURL + "FindAllControls", required: false, staticParams: false });
    SortingFieldAC.set_Args(formatString('{mKey:"{0}",AdvancedSearchType:{1}}', mKey, AdvancedSearch));

    if (ControlsList) {
        $.each(ControlsList, function (index, item) {
            if (AdvancedSearch) {
                if (index % 3 == 0) tr = $('<tr></tr>');
            }
            else {
                if (index % 2 == 0) tr = $('<tr></tr>');
            }

            ControlDiv = $($(Template).find('.' + item.Control.ControlName).html());
            $(ControlDiv).find('span:.label-title').text(item.Control.UIName);
            $(ControlDiv).find('.cx-control').addClass('search_' + item.Control.PropertyName);
            $(ControlDiv).find('span:.value').text(item.Control.UIValue);

            if (item.Control.ControlName == "AutoComplete") {

                var ac;
                if (item.Control.DataTypeContentId > -1)
                    ac = fillDataTypeContentList($(ControlDiv).find('select:.search_' + item.Control.PropertyName), item.Control.DataTypeContentId, false);
                else
                    ac = $(ControlDiv).find('select:.search_' + item.Control.PropertyName).AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + item.Control.ServiceName + item.Control.ServiceMethod });


                if (item.Control.MethodArgs != '' && item.Control.MethodArgs != null)
                    ac.set_Args(formatString('{ ' + item.Control.MethodArgs + ':{0}}', item.Control.MethodArgsValues));

                acArray.push({ proName: item.Control.PropertyName, AC: ac });
                eval("ControlsArray.search_" + item.Control.PropertyName + " = ac");
                $.each(item.ControlEvents, function (index, event) {
                    if (event.ControlPropertyName == item.Control.PropertyName)
                        ac.onChange(new Function(event.JsFunctionBody));
                });
            }

            if (item.Control.ControlName == "TextBox") {
                eval("ControlsArray.search_" + item.Control.PropertyName + " = $(ControlDiv).find('.cx-control')");
                $.each(item.ControlEvents, function (index, event) {
                    if (event.ControlPropertyName == item.Control.PropertyName) {
                        ControlsArray["search_" + item.Control.PropertyName].bind(event.EventName, new Function(event.JsFunctionBody));
                    }
                });
            }



            if (item.Control.ControlName == "CheckBox") {
                getMoreCheckBoxs(item);
            }

            if (item.Control.ControlName == "DatePicker") {
                $(ControlDiv).find('span:.from').text(getResource("from"));
                $(ControlDiv).find('span:.to').text(getResource("to"));
                $(ControlDiv).find('.cx-control').addClass('date');


                $.each($(ControlDiv).find('.cx-control'), function (index, Item) {
                    LoadDatePicker(Item, 'dd-mm-yy');
                });
            }


            th = $('<th>').append($(ControlDiv.get(0)).children());
            td = $('<td>').append($(ControlDiv.get(2)).children());
            tr.append(th);
            tr.append(td);
            QuickSearchDivTemplate.find('.Quick-Search-Controls li:.search-option-header').before(tr);

        });


        QuickSearchDivTemplate.find('a:.lnkSearch').click(function () {
            newSearch = true;
            Search();
        });


        QuickSearchDivTemplate.find('.cx-control').keyup(function (e) {
            if (e.keyCode == 13)
                Search();
        });


        QuickSearchDivTemplate.find('input:.lnkReset').click(function () {
            SearchCriteriaObj = '';
            acArray = new Array();
        });

        QuickSearchDivTemplate.find('a:.existlnk').click(function () {
            $('.Filter-search').animate({
                opacity: 1,
                top: filterSearchTop
            }, 500);
        });
        QuickSearchDivTemplate.find('a:.AdvancedSearchlnk').click(function () {
            AdvancedSearch = true;
            QuickSearch = false;
            document.location.href = PageURL + "?mKey=" + mKey;
        });
    }


    $('.QuickSearchDiv').html(QuickSearchDivTemplate);
    $('.QuickSearchDiv').find('.search-option').show();
    $('.QuickSearchDiv').find('.cx-control').first().focus();

}

function getMoreCheckBoxs(Obj) {
    if (Obj.Control.UIValue != '' && Obj.Control.UIValue != null) {
        var array = Obj.Control.UIValue.split(',');
        if (array.length > 0) {
            $.each($(ControlDiv).find('.checkbox'), function (index, item) {
                if (array.length - 1 >= index) {
                    $(item).addClass(Obj.Control.PropertyName + '-' + array[index]);
                    $(item).attr('text', array[index]);
                    $(item).css('display', 'inline');

                    eval("ControlsArray.search_" + Obj.Control.PropertyName + "_" + array[index] + " = $(item)");
                    $.each(Obj.ControlEvents, function (i, event) {
                        if (event.ControlPropertyName == Obj.Control.PropertyName) {
                            ControlsArray["search_" + Obj.Control.PropertyName + "_" + array[index]].bind(event.EventName, new Function(event.JsFunctionBody));
                        }
                    });

                }
            });

            $.each($(ControlDiv).find('span:.CheckBoxvalue'), function (index, item) {
                if (array.length - 1 >= index) {
                    $(item).text(array[index]);
                    $(item).css('display', 'inline');
                }
            });


            $.each($(ControlDiv).find('input:.checkbox'), function (index, item) {
                $(item).click(function () {
                    if ($(this).hasClass('None')) {
                        $.each($('.Quick-Search-Div').find('input:.checkbox'), function (checkboxindex, checkboxItem) { $(checkboxItem).attr('checked', false) });
                        $('.Quick-Search-Div').find('input:.None').attr('checked', true);
                    }
                    else if ($(this).hasClass('All')) {
                        $.each($('.Quick-Search-Div').find('input:.checkbox'), function (checkboxindex, checkboxItem) { $(checkboxItem).attr('checked', true) });
                        $('.Quick-Search-Div').find('input:.None').attr('checked', false);
                    }

                    else {
                        $('.Quick-Search-Div').find('input:.None').attr('checked', false);
                    }
                });
            });
        }
    }
}


function Search() {
    QuickSearch = true;
    SearchCriteriaObj = '';
    ControlValue1 = '';
    ControlValue2 = '';

    if (SortingFieldAC.get_Item() != null)
        SortFields = SortingFieldAC.get_Item().ColumnName;
    SortType = $('input[type="checkbox"]:.sortType').is(':checked')

    if (ControlsList) {
        $.each(ControlsList, function (index, item) {
            if (item.Control.ControlName == "CheckBox") {

                ControlValue1 = '';
                ControlValue2 = '';
                var cssClass = '.search_' + item.Control.PropertyName;
                var count = 0;
                var chkUIValues = item.Control.UIValue.split(',');
                $.each($('.Quick-Search-Div').find(cssClass), function (checkboxindex, checkboxvalueitem) {
                    if ($(checkboxvalueitem).is(':checked')) {
                        count = count + 1;
                        if ($(checkboxvalueitem).hasClass(item.Control.PropertyName + "-" + chkUIValues[0]))
                            ControlValue2 = "^1";
                        else
                            ControlValue2 = "^0";
                    }
                });

                if (count > 1)
                    ControlValue2 = '';

                if (ControlValue2 != null && ControlValue2 != '') {
                    ControlValue1 = item.Control.EntityControlConfigId + ControlValue2;
                    SearchCriteriaObj = SearchCriteriaObj + ControlValue1 + "|";
                }

                ControlValue1 = '';
                ControlValue2 = '';

                //$.each($('.QuickSearchDiv').find(cssClass), function (checkboxindex, checkboxvalueitem) {
                //if ($(checkboxvalueitem).is(':checked')) {
                //ControlValue2 = ControlValue2 + "^" + $(checkboxvalueitem).attr('text');
                //}
                //});

                //ControlValue2 = ControlValue2 + "^" + $(checkboxvalueitem).attr('text');
                //if (ControlValue2 != null && ControlValue2 != '')
                //ControlValue1 = item.Control.EntityControlConfigId + ControlValue2;
                //if (ControlValue1 != "" && ControlValue1 != null)
                //SearchCriteriaObj = SearchCriteriaObj + ControlValue1 + "|";
                //ControlValue1 = '';
                //ControlValue2 = '';

                // if ($('input[type="checkbox"]:.All').is(':checked')) {
                // ControlValue1 = item.Control.EntityControlConfigId;
                // $.each($('.QuickSearchDiv').find('span:.CheckBoxvalue'), function (checkboxindex, checkboxvalueitem) {
                // ControlValue1 = ControlValue1 + "^" + $(checkboxvalueitem).text();
                // });

                // }
                // else if ($('input[type="checkbox"]:.None').is(':checked')) {
                // ControlValue1 = '';
                // }
                //else {
                //}



                //                ControlValue1 = '';
                //                ControlValue2 = '';
                //                var cssClass = '.search_' + item.Control.PropertyName;

                //                $.each($('.Quick-Search-Div').find(cssClass), function (checkboxindex, checkboxvalueitem) {
                //                    if ($(checkboxvalueitem).is(':checked')) {
                //                        ControlValue2 = ControlValue2 + "^" + $(checkboxvalueitem).attr('text');
                //                    }
                //                });

                //                if (ControlValue2 != null && ControlValue2 != '') {
                //                    ControlValue1 = item.Control.EntityControlConfigId + ControlValue2;
                //                    SearchCriteriaObj = SearchCriteriaObj + ControlValue1 + "|";
                //                }

                //                ControlValue1 = '';
                //                ControlValue2 = '';
            }
            else if (item.Control.ControlName == "AutoComplete") {
                ControlValue1 = '';
                ControlValue2 = '';
                $.each(acArray, function (i, acItem) {
                    if (acItem.proName == item.Control.PropertyName) {
                        if (acItem.AC.get_ItemValue() > -1) {
                            ControlValue1 = item.Control.EntityControlConfigId + "^" + acItem.AC.get_ItemValue();
                            SearchCriteriaObj = SearchCriteriaObj + ControlValue1 + "|";
                            return false;
                        }
                        else
                            return false;
                    }
                });

            }
            else if (item.Control.ControlName == "DatePicker") {

                ControlValue1 = '';
                ControlValue2 = '';
                var cssClass = '.search_' + item.Control.PropertyName;

                ControlValue1 = escapeString($('.Quick-Search-Div').find('input:.from' + cssClass));
                ControlValue2 = escapeString($('.Quick-Search-Div').find('input:.to' + cssClass));

                ControlValue1 = (ControlValue1 != "" && ControlValue1 != undefined) ? getDate2(ControlValue1) : "";
                ControlValue2 = (ControlValue2 != "" && ControlValue2 != undefined) ? getDate2(ControlValue2) : "";

                if ((ControlValue1 != null && ControlValue1 != '' && ControlValue2 != null && ControlValue2 != ''))
                    SearchCriteriaObj = SearchCriteriaObj + item.Control.EntityControlConfigId + "^" + ControlValue1 + "^" + ControlValue2 + "|";

                else if (ControlValue1 != null && ControlValue1 != '')
                    SearchCriteriaObj = SearchCriteriaObj + item.Control.EntityControlConfigId + "^" + ControlValue1 + "^-|";

                else if (ControlValue2 != null && ControlValue2 != '')
                    SearchCriteriaObj = SearchCriteriaObj + item.Control.EntityControlConfigId + "^-^" + ControlValue2 + "|";

                SearchCriteriaObj = SearchCriteriaObj
            }

            else {

                ControlValue1 = '';
                ControlValue2 = '';

                var value = escapeString($('.Quick-Search-Div').find('.search_' + item.Control.PropertyName));
                if (value != null && value != '') {
                    ControlValue1 = item.Control.EntityControlConfigId + "^" + value;
                    SearchCriteriaObj = SearchCriteriaObj + ControlValue1 + "|";
                }
            }

        });
        if (!AdvancedSearch) {
            QuickSearch = true;
            pageNumber = 1; //19/3 mieassar
            findAllEntityServiceURL = WebServiceURL;
            ListEntityItems('');

        }
        else {
            var data = formatString('{mKey:"{0}",SearchCriteriaObj:"{1}",SortType:{2},SortFields:"{3}",TotalRecords:{4},PageNumber:{5}}', mKey, SearchCriteriaObj, SortType, SortFields, resultCount, pageNumber);
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: WebServiceURL,
                data: data,
                datatype: "json",
                success: function (data) {
                    var returnedValue = eval("(" + data.d + ")");
                    ListResults(returnedValue.result);
                },
                error: function (e) { }
            });
        }
    }
}


function ListResults(List) {
    CreateAdvancedSearchList();
    $('.result-body').remove();
    if (List) {
        $.each(List, function (index, item) {
            var template = $(AdvancedSearchListTemplate).clone();
            template = mapEntityInfoToControls(item, template);
            var links = template.find('.link-data');
            $.each(links, function () {
                var value = $(this).attr('link');
                if (value == '') {
                    $(this).attr('href', systemConfig.ApplicationURL + EntityObj.ViewUrl + item.Id);
                }
                else {
                    var url = $(this).attr('href');
                    var id = item[value];
                    $(this).attr('href', url + id);

                }


            });
            $('.QuickSearchResultDiv').find('tbody').append(template);

        });
    }
    else
        return null;
}

function CreateAdvancedSearchList() {
    $('.QuickSearchResultDiv').html('');
    var table = $('<table width="100%" border="0" cellpadding="0" cellspacing="1" class="info"></table>');
    var trHeader = $('<tr id="-2" class="result-header"></tr>');
    var RecordTemplateTr = $('<tr></tr>').attr('class', 'result-body');

    if (ControlsList) {
        $.each(ControlsList, function (index, item) {
            if (item.Control.ShowInSearchResult) {
                //create table header dynamically
                var th = $('<th class="th-data-table"></th>');
                var span = $('<a class="label-data lnkLabel link-data" href="" ></a>');
                span.text(item.Control.UIName);
                span.addClass(item.Control.UIName);
                th.append(span);
                trHeader.append(th);

                //creating template dynamically
                var RecordTemplateTd = $('<th class="td-r1-data-table"></th>');
                span = $('<a class="label-data lnkLabel link-data" href="" ></a>');
                span.addClass(item.Control.PropertyName);

                if (item.Control.EntityReferanceUrl) {
                    span.attr("href", systemConfig.ApplicationURL + item.Control.EntityReferanceUrl);
                    span.attr("link", item.Control.PropertyName + 'Id');
                }
                else {
                    span.attr("href", '');
                    span.attr("link", '');
                }

                if (item.Control.ControlName == "DatePicker")
                    span.addClass('date');
                RecordTemplateTd.append(span);
                RecordTemplateTr.append(RecordTemplateTd);
            }
        });
        table.append(trHeader);
        var ListTemplateTable = table.html();

        //for dynamic template
        AdvancedSearchListTemplate = RecordTemplateTr;

        $('.QuickSearchResultDiv').append(ListTemplateTable);
        $('.NoDataFound').hide();
    }
    else
        $('.NoDataFound').show();
}

function FindEntityInfo(ModuleKey, advancedSearchType) {


    var data = formatString('{mKey:"{0}",AdvancedSearchType:{1}}', ModuleKey, advancedSearchType);

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: systemConfig.ApplicationURL_Common + systemConfig.AdvancedSearchWebServiceURL + "FindEntityInfo",
        data: data,
        datatype: "json",
        success: function (data) {
            EntityObj = eval("(" + data.d + ")").result;
            // var template = $(ControlsTemplate);

        },
        error: function (e) { }
    });
}


function getDate2(date) {
    var parts = date.split('-');
    return parts[2] + '-' + parts[1] + '-' + parts[0];
}

function LoadSearchForm2(ModuleKey, EntityId, advancedSearchType) {
    showLoading($('.Mid-section'));
    firstTimeLoading = false;
    var data = formatString('{mKey:"{0}",entityId:{1},AdvancedSearchType:{2}}', ModuleKey, EntityId, advancedSearchType);
    window.setTimeout(function () {

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: systemConfig.ApplicationURL_Common + systemConfig.AdvancedSearchWebServiceURL + "LoadSearchForm2",
            data: data,
            datatype: "json",
            success: function (data) {
                ControlsList = eval("(" + data.d + ")").result;
                var template = $(ControlsTemplate);
                MapToControls2(ControlsList, template);
                hideLoading();
            },
            error: function (e) { }
        });
    }, 0);
}

function MapToControls2(ControlsList, Template) {
    CreateAdvancedSearchList();
    var QuickSearchDivTemplate = $($('.Quick-Search-Div').html());
    if (AdvancedSearch) {
        QuickSearchDivTemplate.removeClass('Filter-search').addClass('Advanced-Filter-search');
        QuickSearchDivTemplate.find('.Action-div').hide();
    }

    SortingFieldAC = $(QuickSearchDivTemplate).find('select:.sortingField').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.AdvancedSearchWebServiceURL + "FindAllControls", required: false, staticParams: false });
    SortingFieldAC.set_Args(formatString('{mKey:"{0}",entityId:{1},AdvancedSearchType:{2}}', mKey, EntityId, AdvancedSearch));    

    if (ControlsList) {
        $.each(ControlsList, function (index, item) {
            li = $('<li></li>');
            //                if (AdvancedSearch) {
            //                    if (index % 3 == 0) li = $('<li></li>');
            //                }
            //                else {
            //                    if (index % 2 == 0) li = $('<li></li>');
            //                }

            ControlDiv = $($(Template).find('.' + item.Control.ControlName).html());
            $(ControlDiv).find('span:.label-title').text(Lang == 'en' ? item.Control.UIName : item.Control.UINameAr);
            $(ControlDiv).find('.cx-control').addClass('search_' + item.Control.PropertyName);
            $(ControlDiv).find('span:.value').text(item.Control.UIValue);

            if (item.Control.ControlName == "AutoComplete") {

                var ac;
                if (item.Control.DataTypeContentId > -1)
                    ac = fillDataTypeContentList($(ControlDiv).find('select:.search_' + item.Control.PropertyName), item.Control.DataTypeContentId, false);
                else
                    ac = $(ControlDiv).find('select:.search_' + item.Control.PropertyName).AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + item.Control.ServiceName + item.Control.ServiceMethod });

                if (item.Control.MethodArgs != '' && item.Control.MethodArgs != null)
                    ac.set_Args(formatString('{ ' + item.Control.MethodArgs + ':{0}}', item.Control.MethodArgsValues));


                acArray.push({ proName: item.Control.PropertyName, AC: ac });
                eval("ControlsArray.search_" + item.Control.PropertyName + " = ac");
                $.each(item.ControlEvents, function (index, event) {
                    if (event.ControlPropertyName == item.Control.PropertyName)
                        ac.onChange(new Function(event.JsFunctionBody));
                });
            }

            if (item.Control.ControlName == "TextBox") {
                eval("ControlsArray.search_" + item.Control.PropertyName + " = $(ControlDiv).find('.cx-control')");
                $.each(item.ControlEvents, function (index, event) {
                    if (event.ControlPropertyName == item.Control.PropertyName) {
                        ControlsArray["search_" + item.Control.PropertyName].bind(event.EventName, new Function(event.JsFunctionBody));
                    }
                });
            }



            if (item.Control.ControlName == "CheckBox") {
                getMoreCheckBoxs(item);
            }

            if (item.Control.ControlName == "DatePicker") {
                $(ControlDiv).find('span:.from').text(getResource("from"));
                $(ControlDiv).find('span:.to').text(getResource("to"));
                $(ControlDiv).find('.cx-control').addClass('date');


                $.each($(ControlDiv).find('.cx-control'), function (index, Item) {
                    LoadDatePicker(Item, 'dd-mm-yy');
                });
            }


            li.append($(ControlDiv.get(0)).children());
            li.append($(ControlDiv.get(2)).children());

            QuickSearchDivTemplate.find('.Quick-Search-Controls li:.search-option-header').before(li);

        });


        QuickSearchDivTemplate.find('a:.lnkSearch').click(function () {
            newSearch = true;
            showMore = false;
            Search();
        });

        QuickSearchDivTemplate.find('.cx-control').keyup(function (e) {
            if (e.keyCode == 13) {
                newSearch = true;
                showMore = false;
                Search();
            }
        });


        QuickSearchDivTemplate.find('input:.lnkReset').click(function () {
            SearchCriteriaObj = '';
            acArray = new Array();
        });

        QuickSearchDivTemplate.find('a:.existlnk').click(function () {
            $('.Filter-search').animate({
                opacity: 1,
                top: filterSearchTop
            }, 500);
        });
        QuickSearchDivTemplate.find('a:.AdvancedSearchlnk').click(function () {
            AdvancedSearch = true;
            QuickSearch = false;
            document.location.href = PageURL + "?mKey=" + mKey;
        });
    }


    $('.Quick-Search-Div').html(QuickSearchDivTemplate);
    $('.Quick-Search-Div').find('.Filter-search').removeClass('Filter-search');
    $('.Quick-Search-Div').find('.search-option').show();
    $('.Quick-Search-Div').find('.cx-control').first().focus();

}