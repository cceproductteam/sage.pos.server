using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SF.Framework;
using CentrixERP.Common.Web;
using CentrixERP.Configuration;
using Centrix.UM.Business.Entity;

namespace CentrixERP.Common.Web.Settings
{
    public partial class OldERPSetup : BasePageLoggedIn
    {
        public OldERPSetup()
            : base(Enums_S3.Permissions.SystemModules.None) { }

        List<RolePermission> Permissionslist = new List<RolePermission>();
        public bool GLSetups = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            GLSetups = Convert.ToBoolean(Request["glsetup"]);
            //int? Id = -1;
            //int roleID = LoggedInUser.RoleId;
            //IRolePermissionManager mgr = (IRolePermissionManager)IoC.Instance.Resolve(typeof(IRolePermissionManager));
            //Permissionslist = mgr.FindByParentId(roleID, typeof(Role), null);

            //lnkUsers.NavigateUrl = CentrixERP.Configuration.URLs.GetURL(CentrixERP.Configuration.URLEnums.Entity.User, CentrixERP.Configuration.URLEnums.Action.Default.List, ref Id);
            //lnkUsers.Enabled = checkPermission(Id.Value, lnkUsers);


            //lnkRoles.NavigateUrl = CentrixERP.Configuration.URLs.GetURL(CentrixERP.Configuration.URLEnums.Entity.Role, CentrixERP.Configuration.URLEnums.Action.Default.List, ref Id);
            //lnkRoles.Enabled = checkPermission(Id.Value, lnkRoles);

            //lnkTeams.NavigateUrl = CentrixERP.Configuration.URLs.GetURL(CentrixERP.Configuration.URLEnums.Entity.Team, CentrixERP.Configuration.URLEnums.Action.Default.List, ref Id);
            //lnkTeams.Enabled = checkPermission(Id.Value, lnkTeams);

            //lnkLovs.NavigateUrl = CentrixERP.Configuration.URLs.GetURL(CentrixERP.Configuration.URLEnums.Entity.DataTypeContent, CentrixERP.Configuration.URLEnums.Action.Default.List, ref Id);
            //lnkLovs.Enabled = checkPermission(Id.Value, lnkLovs);

            //lnkActivityLookups.NavigateUrl = CentrixERP.Configuration.URLs.GetURL(CentrixERP.Configuration.URLEnums.Entity.ActivityLookup, CentrixERP.Configuration.URLEnums.Action.Default.List, ref Id);
            //lnkActivityLookups.Enabled = checkPermission(Id.Value, lnkActivityLookups);

            //lnkConfigrations.NavigateUrl = CentrixERP.Configuration.URLs.GetURL(CentrixERP.Configuration.URLEnums.Entity.Common, CentrixERP.Configuration.URLEnums.Action.Extended.Configration, ref Id);
            //lnkConfigrations.Enabled = checkPermission(Id.Value, lnkConfigrations);

            //lnkTemplates.NavigateUrl = CentrixERP.Configuration.URLs.GetURL(CentrixERP.Configuration.URLEnums.Entity.Template, CentrixERP.Configuration.URLEnums.Action.Default.List, ref Id);
            //lnkTemplates.Enabled = checkPermission(Id.Value, lnkTemplates);

            //lnkTemplatesTypes.NavigateUrl = CentrixERP.Configuration.URLs.GetURL(CentrixERP.Configuration.URLEnums.Entity.TemplateType, CentrixERP.Configuration.URLEnums.Action.Default.List, ref Id);
            //lnkTemplatesTypes.Enabled = checkPermission(Id.Value, lnkTemplatesTypes);

            //lnkHistory.NavigateUrl = CentrixERP.Configuration.URLs.GetURL(CentrixERP.Configuration.URLEnums.Entity.SentNotifications, CentrixERP.Configuration.URLEnums.Action.Default.List, ref Id);
            //lnkHistory.Enabled = checkPermission(Id.Value, lnkHistory);

            //lnkSendNotification.NavigateUrl = CentrixERP.Configuration.URLs.GetURL(CentrixERP.Configuration.URLEnums.Entity.SendNotifications, CentrixERP.Configuration.URLEnums.Action.Extended.SendNotifications, ref Id);
            //lnkSendNotification.Enabled = checkPermission(Id.Value, lnkSendNotification);

            //lnkEmailConfigration.NavigateUrl = CentrixERP.Configuration.URLs.GetURL(CentrixERP.Configuration.URLEnums.Entity.EmailConfigration, CentrixERP.Configuration.URLEnums.Action.Default.Edit, ref Id);
            //lnkEmailConfigration.Enabled = checkPermission(Id.Value, lnkEmailConfigration);

            //lnkDuplicationRule.NavigateUrl = CentrixERP.Configuration.URLs.GetURL(CentrixERP.Configuration.URLEnums.Entity.DuplicationRule, CentrixERP.Configuration.URLEnums.Action.Default.Edit, ref Id);
            //lnkDuplicationRule.Enabled = checkPermission(Id.Value, lnkDuplicationRule);

            //lnkAccountInformations.NavigateUrl = CentrixERP.Configuration.URLs.GetURL(CentrixERP.Configuration.URLEnums.Entity.UserAccount, CentrixERP.Configuration.URLEnums.Action.Default.Edit, ref Id);
            //lnkAccountInformations.Enabled = checkPermission(Id.Value, lnkAccountInformations);

            //lnkCredit.NavigateUrl = CentrixERP.Configuration.URLs.GetURL(CentrixERP.Configuration.URLEnums.Entity.Credits, CentrixERP.Configuration.URLEnums.Action.Default.List, ref Id);
            //lnkCredit.Enabled = checkPermission(Id.Value, lnkCredit);

            //lnkCreditHistory.NavigateUrl = CentrixERP.Configuration.URLs.GetURL(CentrixERP.Configuration.URLEnums.Entity.CreditsHistory, CentrixERP.Configuration.URLEnums.Action.Default.List, ref Id);
            //lnkCreditHistory.Enabled = checkPermission(Id.Value, lnkCreditHistory);

            //lnkPriceList.NavigateUrl = CentrixERP.Configuration.URLs.GetURL(CentrixERP.Configuration.URLEnums.Entity.PriceList, CentrixERP.Configuration.URLEnums.Action.Default.List, ref Id);
            //lnkPriceList.Enabled = checkPermission(Id.Value, lnkPriceList);
            //lnkCurrency.NavigateUrl = CentrixERP.Configuration.URLs.GetURL(CentrixERP.Configuration.URLEnums.Entity.Currency, CentrixERP.Configuration.URLEnums.Action.Default.List, ref Id);
            //lnkCurrency.Enabled = checkPermission(Id.Value, lnkCurrency);

            //lnkSecurityGroups.NavigateUrl = CentrixERP.Configuration.URLs.GetURL(CentrixERP.Configuration.URLEnums.Entity.SelfServiceSecurityGroup, CentrixERP.Configuration.URLEnums.Action.Default.List, ref Id);
            //lnkSecurityGroups.Enabled = checkPermission(Id.Value, lnkSecurityGroups);
        }


        //protected bool checkPermission(int Id, HyperLink lnk)
        //{
        //    bool hasPermission = LoggedInUser.IsAdmin ? true : false;
        //    if (!hasPermission && Permissionslist != null)
        //    {
        //        IEnumerable<RolePermission> permission = from RolePermission per in Permissionslist
        //                                                 where per.Permissionid == Id
        //                                                 select per;
        //        hasPermission = permission != null && permission.Count() != 0;
        //    }

        //    lnk.CssClass = (!hasPermission) ? "settingslnkdisabled" : "settingslnkenabled";
        //    return (hasPermission);
        //}
    }
}