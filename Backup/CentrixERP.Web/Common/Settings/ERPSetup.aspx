<%@ Page Title="" Language="C#" MasterPageFile="../MasterPage/ERPSite.Master" AutoEventWireup="true"
    CodeBehind="ERPSetup.aspx.cs" Inherits="CentrixERP.Common.Web.Settings.ERPSetup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHeader" runat="server">
   <script src="JS/ERPSetup.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
  <%--   <link href="CSS/erpsetup.css?v=<%=Centrix_Version %>" rel="stylesheet" type="text/css" />--%>
     <link rel="icon" type="image/png" href="images/favicon.png">
 
    <link href="css/new_style.css" rel="stylesheet" type="text/css">
    <link href="css/featherlight.css" rel="stylesheet" type="text/css">
    <script type="text/javascript">
        function InnerPage_Load() {
           // setSelectedMenuItem('.general-ledger');
            //            setPageTitle(getResource('PurchaseOrder'));
            
        }

        !function (d, s, id) { var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https'; if (!d.getElementById(id)) { js = d.createElement(s); js.id = id; js.src = p + '://platform.twitter.com/widgets.js'; fjs.parentNode.insertBefore(js, fjs); } } (document, 'script', 'twitter-wjs');

        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
			m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//stats.g.doubleclick.net/dc.js', 'ga');

        ga('create', 'UA-5342062-6', 'noelboss.github.io');
        ga('send', 'pageview');
    </script>
    <script src="js/featherlight.js" type="text/javascript" charset="utf-8"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageBody" runat="server">
     <div id="pageContainerInner">
        <div class="general-ledger-div display-none">
        <div class="ModulesBlock">
            <div class="iconBlock">
                <img src="images/TransactionProcess.png" alt="" width="84" height="84"></div>
            <h2>
                <span resourcekey="Transactions"></span></h2>
            <div class="BtnsBox">
                <div class="BtnContainer">
                    <a href="../../NewGeneralLedger/BatchList/BatchListList.aspx" class="mod"  resourcekey="BatchListing"></a> <a href="#" class="help btn btn-default"
                       ></a>
                </div>
                <div class="BtnContainer">
                    <a href="../../NewGeneralLedger/PostBatchRange/PostBatchRange.aspx" class="mod"  resourcekey="PostRangesofBatches"></a> <a href="#" class="help btn btn-default"
                        ></a>
                </div>
                <div class="BtnContainer">
                    <a href="../../NewGeneralLedger/YearEnd/ProcessYearEnd.aspx" class="mod"  resourcekey="CreateNewYear"></a> <a href="#" class="help btn btn-default"
                        ></a>
                </div>
              <%--  <div class="BtnContainer">
                    <a href="../../NewGeneralLedger/AutoAllocation/AddAutoAllocation.aspx" class="mod"  resourcekey="AutoAllocation"></a> <a href="#" class="help btn btn-default"
                        ></a>
                </div>--%>
                <br /> <br /> <br />
                
                
            </div>
        </div>
        <div class="ModulesBlock">
            <div class="iconBlock">
                <img src="images/reports.png" alt="" width="84" height="84"></div>
            <h2>
                <span resourcekey="ReportsAndInquiries"></span></h2>
            <div class="BtnsBox">
                <div class="BtnContainer">
                    <a href="../../NewGeneralLedger/Account/AccountReport.aspx" class="mod"  resourcekey="TrialBalance"></a> <a href="#" class="help btn btn-default"
                        ></a>
                </div>
                <div class="BtnContainer">
                    <a href="../../NewReports/GLAccount/GLIncomeStatements.aspx" class="mod"  resourcekey="IncomeAccountReport"></a> <a href="#" class="help btn btn-default"
                        ></a>
                </div>
                 <div class="BtnContainer">
                    <a href="../../NewGeneralLedger/Account/AccountReport.aspx?atid=42" class="mod"  resourcekey="BalanceSheetAccountReport"></a> <a href="#" class="help btn btn-default"
                        ></a>
                </div>
                <div class="BtnContainer">
                    <a href="../../NewGeneralLedger/JournalEntryHeader/TransactionReport.aspx" class="mod"  resourcekey="TransactionListing"></a> <a href="#" class="help btn btn-default"
                        ></a>
                </div>
                 <div class="BtnContainer">
                    <a href="../../NewReports/GLAccount/GLAccountTransactions.aspx" class="mod"  resourcekey="AccountTransactions"></a> <a href="#" class="help btn btn-default"
                        ></a>
                </div>
              <%--  <div class="BtnContainer">
                    <a href="../../NewReports/GLAccount/CashFlowStatements.aspx" class="mod"  resourcekey="CashFlowStatements"></a> <a href="#" class="help btn btn-default"
                        ></a>
                </div>--%>
                
                
            </div>
        </div>
        <div class="ModulesBlock">
            <div class="iconBlock">
                <img src="images/transactionalSetup.png" alt="" width="84" height="84"></div>
            <h2>
                <span resourcekey="ModuleSetup"></span></h2>
            <div class="BtnsBox">
                <div class="BtnContainer">
                  <%--  <a href="../../NewGeneralLedger/Option/GLOptions.aspx" class="mod"  resourcekey="Options"></a> <a href="#" class="help btn btn-default"
                       data-featherlight="#fl3" ></a>--%>
                         <a href="../../NewGeneralLedger/Option/GLOptions.aspx" class="mod"  resourcekey="Options"></a> <a href="#" class="help btn btn-default"
                        ></a>
                </div>
                <div class="BtnContainer">
                    <a href="../../NewGeneralLedger/Segment/SegmentList.aspx" class="mod"  resourcekey="Segment"></a> <a href="#" class="help btn btn-default"
                     ></a>
                </div>
                 <div class="BtnContainer">
                    <a href="../../NewGeneralLedger/SegmentCode/AddSegmentCode.aspx" class="mod"  resourcekey="SegmentsCodes"></a> <a href="#" class="help btn btn-default"
                        ></a>
                </div>
                <div class="BtnContainer">
                    <a href="../../NewGeneralLedger/AccountStructure/AccountStructureList.aspx" class="mod"  resourcekey="AccountStructure"></a> <a href="#" class="help btn btn-default"
                        ></a>
                </div>
                 <div class="BtnContainer">
                    <a href="../../NewGeneralLedger/GroupCode/GroupCodeList.aspx" class="mod"  resourcekey="AccountGroups"></a> <a href="#" class="help btn btn-default"
                        ></a>
                </div>
                <br />
                <br />
                <br />
              <%--  <div class="BtnContainer">
                    <a href="../../NewGeneralLedger/SourceCode/SourceCodeList.aspx" class="mod"  resourcekey="SourceCode"></a> <a href="#" class="help btn btn-default"
                      ></a>
                </div>--%>
                
                
            </div>
        </div>
       
       <div class="ModulesBlock">
            <div class="iconBlock">
                <img src="images/dataDefinition.png" alt="" width="84" height="84"></div>
            <h2>
                <span resourcekey="MasterDataSetup"></span></h2>
            <div class="BtnsBox">
                <div class="BtnContainer">
                    <a href="../../NewGeneralLedger/Account/AccountList.aspx" class="mod"  resourcekey="Accounts"></a> <a href="#" class="help btn btn-default"
                        ></a>
                </div> 
                <div class="BtnContainer">
                    <a href="../../CashManagment/Bank/ListBank.aspx" class="mod"  resourcekey="Bank"></a> <a href="#" class="help btn btn-default"
                       ></a>
                </div>
                 <br /> <br /> <br />
                
            </div>
        </div>
        
      
        
        </div>

        <div class="administrative-settings-div display-none">
        <div class="ModulesBlock">
            <div class="iconBlock">
                <img src="images/tax.png" alt="" width="84" height="84"></div>
            <h2>
                <span resourcekey="TAXSERVICES"></span></h2>
            <div class="BtnsBox">
                <div class="BtnContainer">
                   <%-- <a href="../../newAdministrativeSettings/TaxAuthority/TaxAuthorityList.aspx" class="mod"  resourcekey="TaxAuthority"></a> <a href="#" class="help btn btn-default"
                       data-featherlight="#fl3" ></a>--%>
                        <a href="../../newAdministrativeSettings/TaxAuthority/TaxAuthorityList.aspx" class="mod"  resourcekey="TaxAuthority"></a> <a href="#" class="help btn btn-default"
                        ></a>
                </div>
                <div class="BtnContainer">
                    <a href="../../newAdministrativeSettings/TaxClass/TaxClassHeaderList.aspx" class="mod"  resourcekey="TaxClasses"></a> <a href="#" class="help btn btn-default"
                     ></a>
                </div>
                <div class="BtnContainer">
                    <a href="../../newAdministrativeSettings/TaxRate/TaxRaeList.aspx" class="mod"  resourcekey="TaxRates"></a> <a href="#" class="help btn btn-default"
                       ></a>
                </div>
                <div class="BtnContainer">
                    <a href="../../newAdministrativeSettings/TaxGroup/TaxGroupHeaderList.aspx" class="mod"  resourcekey="TaxGroups"></a> <a href="#" class="help btn btn-default"
                     ></a>
                </div>
                
                
            </div>
        </div>
        <div class="ModulesBlock">
            <div class="iconBlock">
                <img src="images/fiscal_calendar.png" alt="" width="84" height="84"></div>
            <h2>
                <span resourcekey="FISCALCALENDAR"></span></h2>
            <div class="BtnsBox">
                <div class="BtnContainer">
                    <a href="../../newAdministrativeSettings/FiscalCalendar/FiscalCalendarList.aspx" class="mod"  resourcekey="FiscalCalendar"></a> <a href="#" class="help btn btn-default"
                        ></a>
                </div>
               
                
            </div>
        </div>
        <div class="ModulesBlock">
            <div class="iconBlock">
                <img src="images/currencies.png" alt="" width="84" height="84"></div>
            <h2>
                <span resourcekey="CURRENCIES"></span></h2>
            <div class="BtnsBox">
                <div class="BtnContainer">
                    <a href="../../NewAdministrativeSettings/Currency/CurrencyList.aspx" class="mod"  resourcekey="Currency"></a> <a href="#" class="help btn btn-default"
                        ></a>
                </div>
                <div class="BtnContainer">
                    <a href="../../NewAdministrativeSettings/CurrencyRate/CurrencyRateHeaderList.aspx" class="mod"  resourcekey="CurrencyRate"></a> <a href="#" class="help btn btn-default"
                        ></a>
                </div>
               
                
            </div>
        </div>
        <div class="ModulesBlock">
            <div class="iconBlock">
                <img src="images/company_profile.png" alt="" width="84" height="84"></div>
            <h2>
                <span resourcekey="COMPANYPROFILE"></span></h2>
            <div class="BtnsBox">
                <div class="BtnContainer">
                    <a href="../../NewAdministrativeSettings/CompanyProfile/CompanyProfile.aspx" class="mod"  resourcekey="CompanyProfile"></a> <a href="#" class="help btn btn-default"
                        ></a>
                </div>
              
                
            </div>
        </div>
        </div>
        
    </div>
  
   
   <iframe class="lightbox" src="index.html" width="600" height="400" id="fl3" style="border: none;"
        webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
</asp:Content>
