<%@ Page Title="" Language="C#" MasterPageFile="../MasterPage/ERPSite.Master" AutoEventWireup="true"
    CodeBehind="OldERPSetup.aspx.cs" Inherits="CentrixERP.Common.Web.Settings.ERPSetup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHeader" runat="server">
    <script src="JS/ERPSetup.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <link href="CSS/erpsetup.css?v=<%=Centrix_Version %>" rel="stylesheet" type="text/css" />
    <script type="text/javascript">

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageBody" runat="server">
    <div class="Adminstration-main-div">
        <div class="administrative-settings-div display-none">
            <ul>
                <li class="left-col">
                    <div>
                        <div class="icon tax-services">
                        </div>
                        <div class="link-title">
                            <span resourcekey="TAXSERVICES"></span>
                        </div>
                        <div class="settings-links">
                            <ul>
                                <li><a href="../../newAdministrativeSettings/TaxAuthority/TaxAuthorityList.aspx"><span
                                    resourcekey="TaxAuthority"></span></a></li>
                                <li>|</li>
                                <li><a href="../../newAdministrativeSettings/TaxClass/TaxClassHeaderList.aspx"><span
                                    resourcekey="TaxClasses"></span></a></li>
                                <li>|</li>
                                <li><a href="../../newAdministrativeSettings/TaxRate/TaxRaeList.aspx"><span resourcekey="TaxRates">
                                </span></a></li>
                                <li>|</li>
                                <li><a href="../../newAdministrativeSettings/TaxGroup/TaxGroupHeaderList.aspx"><span
                                    resourcekey="TaxGroups"></span></a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="right-col">
                    <div>
                        <div class="icon fiscal-calender">
                        </div>
                        <div class="link-title">
                            <span resourcekey="FISCALCALENDAR"></span>
                        </div>
                        <div class="settings-links">
                            <ul>
                                <li><a href="../../newAdministrativeSettings/FiscalCalendar/FiscalCalendarList.aspx">
                                    <span resourcekey="FiscalCalendar"></span></a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="left-col">
                    <div>
                        <div class="icon currencies">
                        </div>
                        <div class="link-title">
                            <span resourcekey="CURRENCIES"></span>
                        </div>
                        <div class="settings-links">
                            <ul>
                                <li><a href="../../NewAdministrativeSettings/Currency/CurrencyList.aspx"><span resourcekey="Currency">
                                </span></a></li>
                                <li>|</li>
                                <li><a href="../../NewAdministrativeSettings/CurrencyRate/CurrencyRateHeaderList.aspx">
                                    <span resourcekey="CurrencyRate"></span></a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="right-col">
                    <div>
                        <div class="icon company-profile">
                        </div>
                        <div class="link-title">
                            <span resourcekey="COMPANYPROFILE"></span>
                        </div>
                        <div class="settings-links">
                            <ul>
                                <li><a href="../../NewAdministrativeSettings/CompanyProfile/CompanyProfile.aspx"><span
                                    resourcekey="CompanyProfile"></span></a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <%--<li class="left-col">
                    <div>
                        <div class="icon reports">
                        </div>
                        <div class="link-title">
                            <span resourcekey="REPORTS"></span>
                        </div>
                        <div class="settings-links">
                            <ul>
                                <li><a href="../../NewReports/Currency/CurrencyExchangeRates.aspx"><span resourcekey="CurrencyExchangeRates">
                                </span></a></li>
                            </ul>
                        </div>
                    </div>
                </li>--%>
            </ul>
        </div>
        <div class="general-ledger-div display-none">
            <ul>
                <li class="right-col">
                    <div>
                        <div class="icon transactional-setup">
                        </div>
                        <div class="link-title">
                            <span resourcekey="TRANSACTIONALSETUP"></span>
                        </div>
                        <div class="settings-links">
                            <ul>
                                <li><a href="../../NewGeneralLedger/Option/GLOptions.aspx" resourcekey="Options"></a>
                                </li>
                                <li>|</li>
                                <li><a href="../../NewGeneralLedger/Segment/SegmentList.aspx" resourcekey="Segment">
                                </a></li>
                              <%--  <li>|</li>
                                <li><a href="../../NewGeneralLedger/Class/ClassList.aspx" resourcekey="Class"></a>
                                </li>--%>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="left-col">
                    <div>
                        <div class="icon master-data-setup">
                        </div>
                        <div class="link-title">
                            <span resourcekey="MASTERDATASETUP"></span>
                        </div>
                        <div class="settings-links">
                            <ul>
                                <li><a href="../../NewGeneralLedger/SegmentCode/AddSegmentCode.aspx" resourcekey="SegmentsCodes">
                                </a></li>
                             <%--   <li>|</li>
                                <li><a href="../../NewGeneralLedger/ClassCode/addClassCode.aspx" resourcekey="ClassCodes">
                                </a></li>--%>
                                <li>|</li>
                                <li><a href="../../NewGeneralLedger/AccountStructure/AccountStructureList.aspx" resourcekey="AccountStructure">
                                </a></li>
                            </ul>
                            <ul>
                                <li><a href="../../NewGeneralLedger/GroupCode/GroupCodeList.aspx" resourcekey="AccountGroups">
                                </a></li>
                                <li>|</li>
                                <li><a href="../../NewGeneralLedger/SourceCode/SourceCodeList.aspx" resourcekey="SourceCode">
                                </a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="right-col">
                    <div>
                        <div class="icon master-data-definition">
                        </div>
                        <div class="link-title">
                            <span resourcekey="MASTERDATADEFINITION"></span>
                        </div>
                        <div class="settings-links">
                            <ul>
                                <li><a href="../../NewGeneralLedger/Account/AccountList.aspx" resourcekey="Accounts">
                                </a></li> <li>|</li>
                                 <li><a href="../../CashManagment/Bank/ListBank.aspx" resourcekey="Bank"></a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="right-col">
                    <div>
                        <div class="icon transactions-processing">
                        </div>
                        <div class="link-title">
                            <span resourcekey="TRANSACTIONSPROCESSING"></span>
                        </div>
                        <div class="settings-links">
                            <ul>
                                <li><a href="../../NewGeneralLedger/BatchList/BatchListList.aspx" resourcekey="BatchListing">
                                </a></li>
                                <li>|</li>
                                <li><a href="../../NewGeneralLedger/PostBatchRange/PostBatchRange.aspx" resourcekey="PostRangesofBatches">
                                </a></li>
                               <%-- <li>|</li>
                                <li><a href="../../CashManagment/ReverseTransaction/ReverseTransactions.aspx" resourcekey="ReverseTransactions">
                                </a></li>--%>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="right-col">
                    <div>
                        <div class="icon periodic-processing">
                        </div>
                        <div class="link-title">
                            <span resourcekey="PERIODICPROCESSING"></span>
                        </div>
                        <div class="settings-links">
                            <ul>
                                <li><a href="../../NewGeneralLedger/YearEnd/ProcessYearEnd.aspx" resourcekey="CreateNewYear">
                                </a></li>
                                <li>|</li>
                                <li><a href="../../NewGeneralLedger/AutoAllocation/AddAutoAllocation.aspx" resourcekey="AutoAllocation">
                                </a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="right-col">
                    <div>
                        <div class="icon reports">
                        </div>
                        <div class="link-title">
                            <span resourcekey="REPORTS"></span>
                        </div>
                        <div class="settings-links">
                            <ul>
                                <li class="TrialBalance"><a href="../../NewGeneralLedger/Account/AccountReport.aspx" resourcekey="TrialBalance">
                                </a></li>
                                <li>|</li>
                                <li class="IncomeAccountReport"><a href="../../NewReports/GLAccount/GLIncomeStatements.aspx" resourcekey="IncomeAccountReport">
                                </a></li>
                                <br />
                                <li class="BalanceSheetAccountReport"><a href="../../NewGeneralLedger/Account/AccountReport.aspx?atid=42" resourcekey="BalanceSheetAccountReport">
                                </a></li>
                                <li>|</li>
                                <li class="TransactionListing"><a href="../../NewGeneralLedger/JournalEntryHeader/TransactionReport.aspx" resourcekey="TransactionListing">
                                </a></li>
                                <br />
                                <li class="AccountTransactions"><a href="../../NewReports/GLAccount/GLAccountTransactions.aspx" resourcekey="AccountTransactions">
                                </a></li>
                                <li>|</li>
                                <li class="CashFlowStatements"><a href="../../NewReports/GLAccount/CashFlowStatements.aspx" resourcekey="CashFlowStatements">
                                </a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <%--<div class="cash-managment display-none">
            <ul>
                <li class="right-col">
                    <div>
                        <div class="icon transactional-setup">
                        </div>
                        <div class="link-title">
                            <span resourcekey="TRANSACTIONALSETUP"></span>
                        </div>
                        <div class="settings-links">
                            <ul>
                                <li><a href="../../CashManagment/Option/CMOption.aspx" resourcekey="Options"></a>
                                </li>
                                <li>|</li>
                                <li><a href="../../CashManagment/CreditCardType/CreditCardTypeList.aspx" resourcekey="CreditCardType">
                                </a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="left-col">
                    <div>
                        <div class="icon master-data-definition">
                        </div>
                        <div class="link-title">
                            <span resourcekey="MASTERDATADEFINITION"></span>
                        </div>
                        <div class="settings-links">
                            <ul>
                                <li><a href="../../CashManagment/Bank/ListBank.aspx" resourcekey="Bank"></a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="right-col">
                    <div>
                        <div class="icon transactions-processing">
                        </div>
                        <div class="link-title">
                            <span resourcekey="TRANSACTIONSPROCESSING"></span>
                        </div>
                        <div class="settings-links">
                            <ul>
                                <li><a href="../../CashManagment/BankEntryBatchList/ListBankEntryBatchList.aspx"
                                    resourcekey="BankEntryBatch"></a>|</li>
                                <li><a href="../../CashManagment/TransferBatchList/ListTransferBatchList.aspx" resourcekey="TransferBatch">
                                </a>|</li>
                                <li><a href="../../CashManagment/ReverseTransaction/ReverseTransactions.aspx" resourcekey="ReverseTransactions">
                                </a>|</li>
                                <li><a href="../../CashManagment/BankReconciliation/ListBankReconciliation.aspx"
                                    resourcekey="Reconciliation"></a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="left-col">
                    <div>
                        <div class="icon reports">
                        </div>
                        <div class="link-title">
                            <span resourcekey="REPORTS"></span>
                        </div>
                        <div class="settings-links">
                            <ul>
                                <li><a href="../../NewReports/Reports/Report.aspx?cm=1" resourcekey="Reports"></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul>
        </div>--%>
        <div class="admin-bottom-img">
        </div>
    </div>
</asp:Content>
