
var entityLockListTemplate;
var entityLockRowTemplate;
var UserAC;

function InnerPage_Load() {
    setSelectedMenuItem('.setup');
    setPageTitle('Entity lock');
    $('.crm-lock-link').attr('href', systemConfig.Jabbakam_ApplicationURL + systemConfig.pageURLs.EntityLockList);
    GetTemaplte(systemConfig.ApplicationURL_Common + systemConfig.HTMLTemplateURLs.EntityLockListViewItemTemplate, function () {
        GetTemaplte(systemConfig.ApplicationURL_Common + systemConfig.HTMLTemplateURLs.EntityLockRowItem, function () {

            checkPermissions(this, systemEntities.LockEntity, -1, entityLockListTemplate, function (Listtemplate) {
                entityLockListTemplate = Listtemplate;
                checkPermissions(null, systemEntities.LockEntity, -1, entityLockRowTemplate, function (Rowtemplate) {
                    entityLockRowTemplate = Rowtemplate;
                    getEntityLockList();

                });
            });

        }, 'entityLockRowTemplate');
    }, 'entityLockListTemplate');
    initControls();

}


function initControls() {
    UserAC = $('select:.lstUser').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.UsersServiceURL + "FindUserAllLite" });
    UserAC.set_Args(formatString("{showSuperUser:{0}}", true));
    UserAC.onChange(function () {
        getEntityLockList();
    });
    $('.unlock-all-user').click(UnlockAll);
}

function getEntityLockList() {
    showLoading($('.Mid-section'));
    $('.entity-lock-div').children().remove();
    var userId = -1;
    if (UserAC.get_Item() != null)
        userId = UserAC.get_Item().value;
    var data = formatString("{keyword:'',page:-1,resultCount:-1,argsCriteria:'{LockedBy:{0}}'}", userId);
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: systemConfig.ApplicationURL_Common + systemConfig.LockEntityWebServiceURL + "FindAllLite",
        data: data,
        datatype: "json",
        success: function (data) {
            var result = eval('(' + data.d + ')').result;
            $('.LockPage-action-div').show();
            if (result != null) {
                $('.unlock-all-user').show();
                $.each(result, function (index, item) {
                    var entityId = item.EntityId;
                    var entityName = item.EntityName;
                    var uiName = item.UIName;
                    var entityViewURL = item.EntityViewURL;
                    var template = $(entityLockListTemplate).clone();
                    template.find('.entity-name').text(uiName);
                    if (item.EntityLock != null) {
                        $.each(item.EntityLock, function (entityIndex, entityItem) {
                            var rowTemplate = $(entityLockRowTemplate).clone();
                            rowTemplate.find('.UserName').text(entityItem.UserName);
                            rowTemplate.find('.CreatedDate').text(getDate('dd-mm-yy hh:mm tt', formatDate(entityItem.Date)));
                            rowTemplate.find('.entity-ref-number').text("Centrix Ref #" + ((entityItem.EntityValueId > 0) ? entityItem.EntityValueId : ' All'));
                            if (entityViewURL != null && entityItem.EntityValueId > 0)
                                rowTemplate.find('.entity-ref-number').attr('href', systemConfig.ApplicationURL_Common + entityViewURL + entityItem.EntityValueId);
                            else rowTemplate.find('.entity-ref-number').removeClass('link');

                            rowTemplate.find('.unlock-lnk').click(function () {
                                if (entityId != 61)//SystemEntities.MembershipPermission
                                    UnlockEntity(entityId, entityItem.EntityValueId, rowTemplate, entityName);
                                else UnlockAll_Membership(entityItem.EntityValueId, rowTemplate);
                            });

                            template.find('.entity-table').append(rowTemplate);
                        });

                        template.find('.unlock-all').click(function () {
                            if (entityId != 61)// SystemEntities.MembershipPermission
                                UnlockEntity(entityId, -1, template, entityName);
                            else UnlockAll_Membership(-1, template);
                        });
                        template.find('.Details-div').addClass('Details-' + entityName); //.removeClass('Details');
                        $('.entity-lock-div').append(template);
                    }

                });

            }
            else {
                $('.unlock-all').remove();
                $('.unlock-all-user').hide();
                $('.DragDropBackground').show();
            }

            hideLoading();

        },
        error: function (e) {

        }
    });
}

function UnlockAll() {
    showConfirmMsg('unlock all', 'Are you sure?', function () {
        var userID = -1;
        if (UserAC.get_Item() != null)
            userID = UserAC.get_Item().value;
        var data = formatString("{userId:{0}}", userID);
        showLoading($('.Mid-section'));
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: systemConfig.ApplicationURL_Common + systemConfig.LockEntityWebServiceURL + "UnLockAllEntities",
            data: data,
            datatype: "json",
            success: function (data) {
                hideLoading();
                var result = eval('(' + data.d + ')').statusCode.Code;
                if (result == 1) {
                    showStatusMsg('Unlocked successfully');
                    $('.unlock-all-user').hide();
                    $('.entity-lock-div').children().remove();
                }
                else showStatusMsg('Failed to unlock');
            },
            error: function (e) {

            }
        });
    });
}

function UnlockEntity(entityId, entityValueId, template, entityName) {
    showLoading($('.Mid-section'));
    var data = formatString("{entityId:{0},entityValueId:{1}}", entityId, entityValueId);
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: systemConfig.ApplicationURL_Common + systemConfig.LockEntityWebServiceURL + "UnLockUserEntity",
        data: data,
        datatype: "json",
        success: function (data) {
            var result = eval('(' + data.d + ')').statusCode.Code;
            if (result == 1) {
                showStatusMsg('Unlocked successfully');


                //unlock-lnk click

                var lockedEntityCount = $('.unlock-same-entity-all').length;
                var sameLockedEntityCount = $(template).parents('table').find('a:.ActionUnlock-link').length;
                if (lockedEntityCount == 2) {
                    $('.entity-lock-div').empty();
                    $('.unlock-all-user').hide();
                }
                else if (sameLockedEntityCount == 1) {
                    $(template).parents('table').remove();
                    $('.Details-' + entityName).remove();
                }



                template.remove();
                if ($('.ActionUnlock-link').length <= 1)
                    $('.unlock-all-user').hide();
                hideLoading();

                // if ($(template).siblings().length == 1) {
                // $(template).parent().parent('.entity-table').parent().remove();
                // $('.Details-' + entityName).remove();
                // }
                //  $(template).remove();

            }
            else showStatusMsg('Failed to unlock');
        },
        error: function (e) {

        }
    });
}




function UnlockAll_Membership(personId, template) {
    showLoading($('.Mid-section'));
    var args = formatString('{personId:{0}}', personId);
    $.ajax({
        type: 'POST',
        url: systemConfig.ApplicationURL_Common + systemConfig.ConfigurationWebServiceURL + 'MembershipUnLockAll',
        data: args,
        contentType: 'application/json; charset=utf8;',
        dataType: 'json',
        success: function (data) {
            var result = eval('(' + data.d + ')');
            if (result.statusCode.Code == 0) {
                showStatusMsg('Failed to unlock');
            }
            else {
                showStatusMsg('Unlocked successfully');
            }
            template.remove();
            if ($('.ActionUnlock-link').length <= 1)
                $('.unlock-all-user').hide();
            hideLoading();
        },
        error: function (ex) {

            hideLoading();
        }
    });
}
