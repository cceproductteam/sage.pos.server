﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OldSetup.aspx.cs" Inherits="CentrixERP.Common.Web.NewCommon.Common.Settings.Setup"  MasterPageFile="../MasterPage/ERPSite.Master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="PageHeader" runat="server">
    <script src="JS/Setup.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <link href="CSS/setup.css?v=<%=Centrix_Version %>" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageBody" runat="server">
    <div class="Adminstration-main-div">
        <ul>
            <li class="left-col">
                <div>
                    <div class="icon user-mangmnt">
                    </div>
                    <div class="link-title">
                       <span resourcekey="SPUserManagement"></span> </div>
                    <div class="settings-links">
                        <ul>
                            <li><a href="../../NewUserManagement/Users/UsersList.aspx"><span resourcekey="SPUsers"></span></a></li>
                            <li>|</li>
                            <li><a href="../../NewUserManagement/Roles/RolesList.aspx"><span resourcekey="SPRols"></span></a></li>
                            <li>|</li>
                            <li><a href="../../NewUserManagement/Teams/TeamsList.aspx"><span resourcekey="SPTeams"></span></a></li>
                            <li>|</li>
                            <li><a href="../../newUserManagement/Roles/RolePermission.aspx"><span resourcekey="SPPermissions"></span></a></li>
                           <li>|</li>
                             <li><a href="../../newUserManagement/Shifts/ShiftsList.aspx"><span resourcekey="Shifts"></span></a></li>

                        </ul>
                    </div>
                </div>
            </li>
            <li class="right-col">
                <div>
                    <div class="icon data-admin">
                    </div>
                    <div class="link-title">
                        <span resourcekey="SPDataAdminstration"></span></div>
                    <%--<div class="link-disc">Lorem ipsum dolor sit amet, consectetur adipiscing elit, Nullam molestie interdum interdum.</div>--%>
                    <div class="settings-links">
                        <ul>
                          <li><a href="ERPEntityLockList.aspx"><span resourcekey="SPEntitylock"></span></a></li>                           
                        </ul>
                    </div>
                </div>
            </li>
        </ul>
        <div class="admin-bottom-img">
        </div>
    </div>
</asp:Content>