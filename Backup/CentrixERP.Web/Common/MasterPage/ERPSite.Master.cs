using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CentrixERP.Common.Business.IManager;
using CentrixERP.Common.Business.Entity;
using SF.Framework;
using C = CentrixERP.Configuration;
using Centrix.UM.Business.Entity;
using CentrixERP.Configuration;
using System.Configuration;

namespace CentrixERP.Common.Web.NewCommon.Common.MasterPage
{
    public partial class ERPSite : System.Web.UI.MasterPage
    {
        public string RedirectURL = "";
        public User LoggedInUser { get; set; }
        public Enums_S3.Configuration.Language Lang = Enums_S3.Configuration.Language.en;
       
        public ERPSite()
        {
            CentrixERP.Configuration.Configuration.setLanguage(false);
            Lang = CentrixERP.Configuration.Configuration.Lang;
        }

        public static string UploadItemsErrorLogURL
        {
            get
            {
                return ConfigurationManager.AppSettings["UploadItemsErrorLogURL"].ToString();
            }
        }
     
        public static int NumberOfListedNavigationMenu
        {
            get
            {
                return ConfigurationManager.AppSettings["NumberOfListedNavigationMenu"].ToNumber();
            }
        }
        public static string ApplicationURL
        {
            get { return BasePageLoggedIn.ApplicationURL; }
            set { BasePageLoggedIn.ApplicationURL = value; }
        }

        public static string ApplicationURL_Common
        {
            get { return BasePageLoggedIn.ApplicationURL_Common; }
            set { BasePageLoggedIn.ApplicationURL_Common = value; }
        }

        public static string Centrix_Version
        {
            get
            {
                return ConfigurationManager.AppSettings["CentrixVersion"].ToString();
            }
        }
       
        protected void Page_Load(object sender, EventArgs e)
        {
            //CentrixERP.Configuration.Configuration.setLanguage(false);
            if (!Page.IsPostBack)
            {
                MainStyle.Href = CentrixERP.Configuration.URLs.GetStyleURL(C.URLEnums.Action.Extended.MainStyle, Lang.ToString()) + "?v=" + Centrix_Version;
                JQueryUIAllCSS.Href = CentrixERP.Configuration.URLs.GetStyleURL(C.URLEnums.Action.Extended.JQueryUIAllCSS, C.Enums_S3.Configuration.Language.en.ToString()) + "?v=" + Centrix_Version;
                JQueryUIDemo.Href = CentrixERP.Configuration.URLs.GetStyleURL(C.URLEnums.Action.Extended.JQueryUIDemo, C.Enums_S3.Configuration.Language.en.ToString()) + "?v=" + Centrix_Version;
                SpryTabbedPanels.Href = CentrixERP.Configuration.URLs.GetStyleURL(C.URLEnums.Action.Extended.SpryTabbedPanels, C.Enums_S3.Configuration.Language.en.ToString()) + "?v=" + Centrix_Version;
                ERPStyle.Href = CentrixERP.Configuration.URLs.GetStyleURL(C.URLEnums.Action.Extended.ERPStyle, C.Enums_S3.Configuration.Language.en.ToString()) + "?v=" + Centrix_Version;
                Autocomplete.Href = CentrixERP.Configuration.URLs.GetStyleURL(C.URLEnums.Action.Extended.Autocomplete, Lang.ToString()) + "?v=" + Centrix_Version;

                LoggedInUser = BasePageLoggedIn.getLoggedInUser();
                if (LoggedInUser == null)
                    Response.Redirect(URLs.GetURL(Enums_S3.Configuration.Module.Common, URLEnums.Action.Extended.Logout, null));
                lblUserName.Text = LoggedInUser.UserName;
                lblLogout.NavigateUrl = URLs.GetURL(Enums_S3.Configuration.Module.Common, URLEnums.Action.Extended.Logout, null);

                LoadNavigationMenu();
            }
        }

        private string GetUrl()
        {
            string Port = ConfigurationManager.AppSettings["Port"].ToString();
            string Domin = ConfigurationManager.AppSettings["Domain"].ToString();
            Uri uri = HttpContext.Current.Request.Url;
            string url = string.Format("{0}{1}{2}", Domin, uri.AbsolutePath, uri.Query);
            return Server.UrlEncode(url);
        }

        void LoadNavigationMenu()
        {
            INavigationMenueManager myNavManager = (INavigationMenueManager)IoC.Instance.Resolve(typeof(INavigationMenueManager));
            NavigationMenueCriteria myCrt = new NavigationMenueCriteria();
            myCrt.Category = 1;
            myCrt.RoleId = LoggedInUser.RoleId;
            //MaxNumberOfNavigation

           
            List<NavigationMenue> NavigationList = myNavManager.FindAll(myCrt);
            List<NavigationMenue> ListedNavigation=new List<NavigationMenue>();
            List<NavigationMenue> HiddenNavigation = new List<NavigationMenue>();
            if (NavigationList != null)
            {
                ListedNavigation = (from NavigationMenue item in NavigationList select item).Take(NumberOfListedNavigationMenu).ToList();
                HiddenNavigation = (from NavigationMenue item in NavigationList select item).Except(ListedNavigation).ToList();
            }


            rptNavMenu.DataSource = ListedNavigation;
            rptHiddenNavMenu.DataSource = HiddenNavigation;          
            rptNavMenu.DataBind();
            rptHiddenNavMenu.DataBind();
        }

        //protected void RestructureNavigationGrid(object sender, RepeaterItemEventArgs e)
        //{
        //    if ((e.Item.ItemIndex + 1) != MaxNumberOfNavigation)
        //    { }
        //}
          

    }
}