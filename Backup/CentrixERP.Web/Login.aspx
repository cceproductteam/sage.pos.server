﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="CentrixERP.Common.Web.NewCommon.Common.Login" %>

<%@ Register TagPrefix="SF" Namespace="SFLib.CustomControls" Assembly="SFCustomControls" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
   <title>Centrix <%=ConfigurationManager.AppSettings["CentrixRelease"].ToString() %></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" runat="server" id="LoginStyle" type="text/css" />
    <script src="Common/JScript/jquery-1.8.0.js" type="text/javascript"></script>
    <script type="text/javascript">
        checkLogout();
        function checkLogout() {
            if ((window.top != window.self) && window.top.logOut) {
                window.top.logOut("");
            }
        }


        $(document).ready(function () {
            $('.continuebtn').focus();

            $('.X-btn').click(function () {
                $('.LoginNotificationPannel').remove();
            });

            if (!$.browser.chrome)
                $('.LoginNotificationPannel').show();
        });
    
    </script>
</head>
<body class="Login-page">
    <form id="form1" runat="server" autocomplete="off">
    <div class="Main-Div mLogin">
            <div class="LoginNotificationPannel">
            <a class="X-btn">x</a> <span class="noti-title">For better performance, we recommend using Google Chrome browser</span> <a class="InstallGoogle" target="_blank" href="https://www.google.com/intl/en/chrome/browser/">Install Google
                    Chrome</a>
        </div>
        <div class="Login-Box-shadow">
        </div>
        <div class="Login-Box-shadow-2">
        </div>
        <!--Login-->
        <div class="Login-Box">
            <!--Login Logo-->
            <div class="login-logo">
            </div>
            <div class="Login-logotitle">
            </div>
            <!--End Login Logo-->
            <div class="username-area">
                <asp:Panel runat="server" Visible="true" ID="pnlLogin">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <asp:Label ID="Label1" runat="server" CssClass="username-area-name" meta:resourcekey="lblUserName"></asp:Label>
                            </td>
                            <td>
                                <SF:CustomTextBox runat="server" ID="txtUserName" Required="false" CssClass="login-text"
                                    meta:resourcekey="txtUserName" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label2" runat="server" CssClass="username-area-password" meta:resourcekey="lblPassword"></asp:Label>
                            </td>
                            <td>
                                <SF:CustomTextBox runat="server" ID="txtPassword" TextMode="Password" Required="false"
                                    ForeColor="Black" CssClass="login-text" meta:resourcekey="txtPassword" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Label runat="server" CssClass="errorLoginMSG" ID="lblErrorLogin" meta:resourcekey="lblErrorLogin"
                                    Visible="false"></asp:Label>
                                <asp:Button runat="server" CssClass="Login-btn" ID="btnLogin" OnClick="btnLogin_Click"
                                    meta:resourcekey="btnLogin" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Label runat="server" CssClass="errorLoginMSG" ID="lblSessionExpired" meta:resourcekey="lblSessionExpired"
                                    ForeColor="Black" Visible="false"></asp:Label>
                                <asp:Label runat="server" CssClass="errorLoginMSG" ID="lblLoggedFromAnotherPlace"
                                    meta:resourcekey="lblLoggedFromAnotherPlace" ForeColor="Black" Visible="false"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel runat="server" Visible="false" ID="pnlLoginConfirmation">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <asp:Label ID="lblLoginConfirmation" runat="server" meta:resourcekey="lblLoginConfirmation" CssClass="errorLoginMSG" ForeColor="Black"></asp:Label>
                            </td>
                            <td>
                                <asp:Button runat="server" ID="btnContinue" OnClick="btnContinue_Click" meta:resourcekey="btnContinue" CssClass="Login-btn continuebtn" />
                                <asp:Button runat="server" ID="btnCancel" OnClick="btnCancel_Click" meta:resourcekey="btnCancel" CssClass="Login-btn" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="pnlError" Visible="false" runat="server">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <asp:Label ID="lblError" runat="server" meta:resourcekey="lblError"  CssClass="errorLoginMSG" ></asp:Label>
                            </td>
                            <td>
                                <asp:Button runat="server" ID="btnTryAgain" OnClick="btnCancel_Click" meta:resourcekey="btnTryAgain" CssClass="Login-btn"/>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>
        </div>
        <!--End Login-->
        <!--Copyrights-->
        <div class="copyrights">
            Powered By CCE © 2010-2016 All rights reserved.</div>
        <!--End Copyrights-->
    </div>
    </form>
</body>
</html>
