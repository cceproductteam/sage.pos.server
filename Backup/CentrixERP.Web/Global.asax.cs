using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using SF.Framework.Logging;
using System.Text;

namespace CentrixERP.Common.Web.NewCommon
{
    public class Global : System.Web.HttpApplication
    {
        private StringBuilder errorMessage;
        protected void Application_Start(object sender, EventArgs e)
        {
            //TODO: Uncomment
            //CX.Framework.Log.Logger.Instance.Info("test");

        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            //if (Logger.Instance.EnableLogging)
            //{
            //    string url = Request.Url.ToString();
            //    Exception ex = Server.GetLastError();


            //    if (ex.Message == "LockedEntity")
            //    {
            //        throw ex;
            //    }


            //    if (ex == null)
            //        return;

            //    // this.errorMessage = new StringBuilder();


            //    //StringBuilder EmailBody = new StringBuilder(string.Format("<b>Date</b> : {0}<br /><br />", DateTime.Now.ToString()));

            //    //string error = HandleException(ex).ToString();

            //    //EmailBody.AppendLine("<b>EXCEPTION START</b>");
            //    //EmailBody.AppendLine(string.Format("<b>URL</b> : {0}<br /><br />", url));
            //    //EmailBody.AppendLine(string.Format("<b>Message</b> : {0}<br /><br />", error));
            //    //EmailBody.AppendLine("<b>EXCEPTION END</b>");

            //    Logger.Instance.LogExecption(ex, url);
            //}
        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }


        private StringBuilder HandleException(Exception ex)
        {

            this.errorMessage.AppendLine(string.Format("<b>Message</b> : {0}<br /><br />", ex.Message));
            this.errorMessage.AppendLine(string.Format("<b>InnerException</b> : {0}<br /><br />", ex.StackTrace));

            if (ex.InnerException != null)
                return HandleException(ex.InnerException);

            return this.errorMessage;
        }
    }
}