using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using E = Centrix.UM.Business.Entity;
using Centrix.UM.Business.IManager;
using SF.Framework;
using CentrixERP.Configuration;
using System.Configuration;
using CentrixERP.Common.Business.IManager;

namespace CentrixERP.Common.Web.NewCommon.Common
{
    public partial class Login : BasePage
    {
        IUserLogginManager userLogginManager = null;
        Enums_S3.UserLoggin.UserLogginStatus userLogginStatus = Enums_S3.UserLoggin.UserLogginStatus.NotLoggedIn;
        IUserManager myUserManager = null;
        IEntityLockManager iEntityLockManager = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            txtUserName.Focus();
            LoginStyle.Href = URLs.GetStyleURL(URLEnums.Action.Extended.MainStyle, Lang.ToString());
            userLogginManager = IoC.Instance.Resolve<IUserLogginManager>();

            myUserManager = (IUserManager)IoC.Instance.Resolve(typeof(IUserManager));
            iEntityLockManager = IoC.Instance.Resolve<IEntityLockManager>();

            if (!Page.IsPostBack)
            {

                E.UserLogginCriteria criteria = new E.UserLogginCriteria()
                {
                    UserAgent = HttpContext.Current.Request.UserAgent,
                    PublicIP = HttpContext.Current.Request.UserHostName,
                    LocalIP = HttpContext.Current.Request.UserHostAddress
                };
                if (userLogginManager.ValidateLogginSession(criteria) == CentrixERP.Configuration.Enums_S3.UserLoggin.UserLogginStatus.LoggedIn)
                {
                    Response.Redirect(System.Configuration.ConfigurationManager.AppSettings["defaultPage"].ToString());
                }


                CentrixERP.Configuration.Enums_S3.UserLoggin.UserLogginStatus UserLogginStatus = userLogginManager.CheckCurrentSession();
                if (UserLogginStatus == CentrixERP.Configuration.Enums_S3.UserLoggin.UserLogginStatus.LoggedFromAnotherPlace)
                {
                    lblLoggedFromAnotherPlace.Visible = true;
                    //pnlLogin.Visible = false;
                    //pnlLoginConfirmation.Visible = true;
                }
                else if (UserLogginStatus == CentrixERP.Configuration.Enums_S3.UserLoggin.UserLogginStatus.SessionExpired)
                {
                    lblSessionExpired.Visible = true;
                }
            }
            else
            {
                lblLoggedFromAnotherPlace.Visible = false;
                lblSessionExpired.Visible = false;
            }



            SF.Framework.Cookies.Abandon("cxuser");

            //lblSessionExpired.Visible = Request["sexp"].ToNumber() == 1;

            //if (SF.Framework.Cookies.Get("cxuser-status") != null)
            //{
            //    int status = SF.Framework.Cookies.Get("cxuser-status").ToNumber();
            //    if (status == (int)Enums_S3.UserLoggin.UserLogginStatus.LoggedFromAnotherPlace)
            //        lblLoggedFromAnotherPlace.Visible = true;
            //    else if (status == (int)Enums_S3.UserLoggin.UserLogginStatus.SessionExpired)
            //        lblSessionExpired.Visible = true;

            //    SF.Framework.Cookies.Abandon("cxuser-status");
            //}

            //lnkLanguage.NavigateUrl = URLs.GetURL(Enums_S3.Configuration.Module.Common, URLEnums.Action.Extended.ChangeLang, "to=" + OppositeLang + "&url=" + HttpContext.Current.Request.Url.AbsoluteUri);
            //loginStatusMsg.Language = (Enums.Language)LangNumber;
            //SSS.Business.Users.User user = new SSS.Business.Users.User();        
            //IUserManager myUserManager = (IUserManager)IoC.Instance.Resolve(typeof(IUserManager));

            //if (myUserManager.IsLoggedIn()) Response.Redirect(URLs.GetURL(Enums_S3.Configuration.Module.Common, URLEnums.Action.Extended.Configration, null));
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            


            E.User LoggedInUser = myUserManager.Login(txtUserName.Text, txtPassword.Text);
            if (LoggedInUser != null)
            {
                iEntityLockManager.UnLock(LoggedInUser.UserId);
                E.UserLogginCriteria criteria = new E.UserLogginCriteria()
                {
                    UserId = LoggedInUser.UserId
                };
                List<E.UserLoggin> currentUserSessions = this.userLogginManager.FindByParentId(LoggedInUser.UserId, typeof(E.User), null);

                if (currentUserSessions != null && currentUserSessions.Count > 0)
                {
                    int defaultSessionTimeOut = ConfigurationManager.AppSettings["AllowedSeesionTimeOut"].ToNumber();
                    if (defaultSessionTimeOut <= 0)
                        defaultSessionTimeOut = 20;

                    bool validSessionExists = false;
                    foreach (var session in currentUserSessions)
                    {
                        double minutes = ((TimeSpan)(DateTime.Now - session.LastRequest)).TotalMinutes;
                        bool validSessionTime = minutes < defaultSessionTimeOut;
                        validSessionExists = false;
                        if (session.UserAgent != Request.UserAgent || session.LocalIp != Request.UserHostAddress || session.PublicIp != Request.UserHostName)
                        {
                            if (validSessionTime)
                            {
                                validSessionExists = true;
                                break;
                            }
                        }
                    }

                    if (validSessionExists)
                    {
                        ViewState["loggedInUser"] = SF.Framework.Cryption.Encrypt.String(LoggedInUser.UserId.ToString(), Cryption.Algorithm.AES);
                        pnlLoginConfirmation.Visible = true;
                        pnlLogin.Visible = false;
                    }
                    else {
                        createNewUserSession(LoggedInUser.UserId);
                    }
                    //Enums_S3.UserLoggin.UserLogginStatus UserLogginStatus = userLogginManager.ValidateLogginSession(criteria);



                    //userLogginManager.KillUserSession(LoggedInUser.UserId);

                    //criteria.PublicIP = Request.UserHostName;
                    //criteria.LocalIP = Request.UserHostAddress;

                    //userLogginManager.CreateUserSession(LoggedInUser.UserId, criteria, true);
                    ////LoggedInUser = myUserManager.FindLoggedInUser(Cookies.Get("uid").ToNumber());
                }
                else
                {
                    createNewUserSession(LoggedInUser.UserId);
                }
            }
            else
            {
                lblErrorLogin.Visible = true;
            }
        }


        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl);
        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["loggedInUser"] != null)
                {
                    int userId = SF.Framework.Cryption.Decrypt.String(ViewState["loggedInUser"].ToString(), Cryption.Algorithm.AES).ToNumber();
                    if (userId > 0)
                    {
                        E.User loggedInUser = myUserManager.FindById(userId, null);
                        if (loggedInUser != null)
                        {
                            createNewUserSession(loggedInUser.UserId);
                        }
                    }
                    else
                    {
                        ViewState["loggedInUser"] = null;
                        pnlError.Visible = true;
                        pnlLogin.Visible = false;
                        pnlLoginConfirmation.Visible = false;
                    }
                }
                else
                    Response.Redirect(Request.RawUrl);

            }
            catch (Exception ex)
            {
                ViewState["loggedInUser"] = null;
                pnlError.Visible = true;
                pnlLogin.Visible = false;
                pnlLoginConfirmation.Visible = false;
            }
        }

        private void createNewUserSession(int userId)
        {
            E.UserLogginCriteria criteria = new E.UserLogginCriteria()
            {
                UserId = userId,
                PublicIP = Request.UserHostName,
                LocalIP = Request.UserHostAddress,
                UserAgent = Request.UserAgent
            };

            userLogginManager.KillUserSession(userId);
            userLogginManager.CreateUserSession(userId, criteria, true);
            Response.Redirect(System.Configuration.ConfigurationManager.AppSettings["defaultPage"].ToString());
        }
    }
}