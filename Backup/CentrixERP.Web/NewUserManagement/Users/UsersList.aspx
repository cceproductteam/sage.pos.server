<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UsersList.aspx.cs" MasterPageFile="~/Common/MasterPage/ERPSite.Master"
    Inherits="Centrix.UM.Web.NewUserManagement.Users.UsersList" %>

<asp:Content runat="server" ContentPlaceHolderID="PageHeader" ID="PageHeader">
    <script src="../../Common/JScript/CommonTabs.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <script src="JS/Users.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <script src="../../Common/AdvancedSearch/JS/AdvancedSearch.js?v=<%=Centrix_Version %>"
        type="text/javascript"></script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="PageBody" ID="PageBody">
    <div id="demo2" class="result-container">
        <div class="SearchDivContainer">
            <div class="SD-SearchDiv">
                <input type="text" class="SD-ipnutText-MainSearch search-text-box searchTextbox" /></div>
            <div class="Quick-Search-Div">
            </div>
            <div class="MoreSearchOptions">
                <a class="more-options"><span resourcekey="MoreOptions"></span></a>
            </div>
        </div>
        <div class="GridTableDiv-header">
            <table class="GridTableFloating" cellpadding="0" cellspacing="0" border="0">
                <thead class="THeadGridTable">
                    <tr>
                        <th class="THead-Th th-number">
                            <span>#</span>
                        </th>
                        <th class="THead-Th">
                            <span resourcekey="UUserName"></span>
                        </th>
                        <th class="THead-Th">
                            <span resourcekey="FullName"></span>
                        </th>
                          <th class="THead-Th">
                            <span resourcekey="UTeam"></span>
                        </th>
                          <th class="THead-Th">
                            <span resourcekey="URole"></span>
                        </th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="GridTableDiv-body ResultGridTableDiv">
            <table class="Result-list" cellpadding="0" cellspacing="0" border="0">
                <thead class="THeadGridTable">
                   <tr>
                        <th class="THead-Th th-number">
                            <span>#</span>
                        </th>
                        <th class="THead-Th">
                            <span resourcekey=""></span>
                        </th>
                        <th class="THead-Th">
                            <span resourcekey=""></span>
                        </th>
                          <th class="THead-Th">
                            <span resourcekey="UTeam"></span>
                        </th>
                          <th class="THead-Th">
                            <span resourcekey="URole"></span>
                        </th>
                    </tr>
                </thead>
                <tbody class="TBodyGridTable result-list">
                </tbody>
            </table>
        </div>
        <div class="total-result-div TotalDiv">
            <span></span>
        </div>
    </div>
    <div id='content'>
        <div class="Result-info-container">
            <div class="result-name">
                <ul>
                    <li class="nameLI"><span resourcekey="UHeaderUserName"></span><span class="HaderUserName">
                    </span></li>
                    <li class="emailLI"><span resourcekey="UHeaderName"></span><span class="FullNameTruncated">
                    </span></li>
                    <li class="phoneLI"><span resourcekey="UHeaderEMail"></span><a class="email-to" href=""
                        target="_top"><span class="UserEmailTruncated"></span></a></li>
                </ul>
            </div>
            <div style="float: right;" id='page_navigation'>
            </div>
            <div class="ClearDiv">
            </div>
            <div class="test">
                <div class="QuickSearchDiv">
                </div>
                <span class="SlideUp" title="Slide Up"></span>
                <div id="TabbedPanels1" class="TabbedPanels">
                    <ul class="TabbedPanelsTabGroup">
                        <li class="TabbedPanelsTab" tabindex="0">
                            <div class="TabbedPanelsTabSelected-arrow" style="display: block;">
                            </div>
                            <span class="TabbedPanelsTab-img Summery-tab"></span><span resourcekey="Summary">
                            </span></li>
                        <li class="TabbedPanelsTab email-tabs" tabindex="0">
                            <div class="TabbedPanelsTabSelected-arrow">
                            </div>
                            <span class="TabbedPanelsTab-img Mail-tab "></span><span resourcekey="Emails"></span>
                        </li>
                        <li class="TabbedPanelsTab note-tabs" tabindex="0">
                            <div class="TabbedPanelsTabSelected-arrow">
                            </div>
                            <span class="TabbedPanelsTab-img  Note-tab "></span><span resourcekey="note"></span>
                        </li>
                        <li class="TabbedPanelsTab Attach-tabs" tabindex="4">
                            <div class="TabbedPanelsTabSelected-arrow">
                            </div>
                            <span class="TabbedPanelsTab-img Attach-tab"></span><span resourcekey="Attach"></span>
                        </li>

                         <%--<li class="TabbedPanelsTab StoresShiftsTab" tabindex="5">
                            <div class="TabbedPanelsTabSelected-arrow">
                            </div>
                            <span class="TabbedPanelsTab-img StoresShifts-tab"></span><span resourcekey="UserStoresShifts"></span>
                     </li>  --%>
                        <%-- <li class="TabbedPanelsTab Communication-tab" tabindex="5">
                            <div class="TabbedPanelsTabSelected-arrow">
                            </div>
                            <span class="TabbedPanelsTab-img Communication-tab"></span><span resourcekey="Communications"></span>
                     </li>  --%>
                    </ul>
                    <div class="TabbedPanelsContentGroup">
                    </div>
                </div>
                <span class="SlideDown" style="" title="Slide Down"></span>
            </div>
        </div>
    </div>
    <div class="add-new-div">
        <div class="add-new-item display-none">
        </div>
    </div>
</asp:Content>
