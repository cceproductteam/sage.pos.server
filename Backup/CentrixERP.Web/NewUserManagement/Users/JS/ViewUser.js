var userId = -1;
function InnerPage_Load() {
    setAddLinkURL(systemConfig.ApplicationURL + systemConfig.pageURLs.AddUsers, 'Add User');
    $('.ActionEdit-link').live("click", function () {
        window.document.location = systemConfig.ApplicationURL_Common + systemConfig.pageURLs.AddUser + "?uid=" + userId;
    });

//    $('.ActionDelete-link').live('click', function () {
//        showConfirmMsg(DeleteTitleMessage, DeleteMessage, function () {
//            deleteEntity(selectedEntityId);
//        });
//        return false;
//    });
    $('.ActionCancel-link').live('click', function () {
        window.document.location = systemConfig.ApplicationURL_Common + systemConfig.pageURLs.UserList ;
    });

    setPageTitle("User");
    var userServiceURL = systemConfig.ApplicationURL_Common + systemConfig.UsersServiceURL;
    //loadViewEntityData(userServiceURL + "FindUserByIdLite", $('.user-info'), userId);

    $.ajax({
        type: "POST",
        url: userServiceURL + "FindUserByIdLite",
        contentType: "application/json; charset=utf-8",
        data: '{ id: ' + userId + ' }',
        dataType: "json",
        success: function (data) {

            if (data.d != null) {
                dataObject = eval("(" + data.d + ")").result;
                //$('.TabbedPanelsContentGroup').html($('.user-info'));

                if (dataObject != null) {
                    for (var prop in dataObject) {
                        var className = '.' + prop;
                        var lblValue = dataObject[prop];
                        if ($(className).hasClass('date')) {
                            if (lblValue)
                                lblValue = getDate('dd-mm-yy', formatDate(lblValue));
                            $('.' + prop).val(lblValue);

                        }

                        $('.' + prop).html(lblValue);
                    }
                }
            }
            $('a:.UserEmail').attr('href', 'mailto:' + dataObject.UserEmail);
        },
        error: function (e) {
        }
    });
}