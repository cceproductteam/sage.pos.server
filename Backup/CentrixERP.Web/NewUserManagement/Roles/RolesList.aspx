<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RolesList.aspx.cs" MasterPageFile="~/Common/MasterPage/ERPSite.master"
    Inherits="Centrix.UM.Web.NewUserManagement.Roles.RolesList" %>

<asp:Content runat="server" ContentPlaceHolderID="PageHeader" ID="PageHeader">
    <script src="JS/Roles.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
     <link href="CSS/Role_<%=Lang %>.css?v=<%=Centrix_Version %>" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="PageBody" ID="PageBody">
    <div id='content' class="View-info">
        <div class="TabbedPanelsContent">
            <div class="TabbedPanelsContent-info">
                <div class="Add-title">
                    <div class="Details-div">
                        <span class="details-div-title" resourcekey="Roles"></span>
                        <div class="Action-div display-none">
                             <ul>
                                <li><span class="ActionSave-link save-tab-lnk viewedit-display-none"></span><a class="ActionSave-link save-tab-lnk viewedit-display-none">
                                    <span resourcekey="Save"></span></a></li>
                                <li><span class="ActionCancel-link cancel-tab-lnk viewedit-display-none"></span><a
                                    class="ActionCancel-link cancel-tab-lnk viewedit-display-none"><span resourcekey="Cancel">
                                    </span></a></li>
                                <li class="role-add"><span class="add-new-img add-new-link viewedit-display-none"></span>
                                    <a class="add-new-link viewedit-display-none"><span resourcekey="RAddnewRole"></span>
                                    </a></li>
                                <li class="role-delete"><span class="ActionDelete-link delete-lnk display-none viewedit-display-none">
                                </span><a class="delete-lnk display-none viewedit-display-none"><span resourcekey="Delete">
                                </span></a></li>
                                <li class="role-AddNew"><span class="add-new-img AddNew-tab-lnk viewedit-display-block ">
                                </span><a class=" AddNew-tab-lnk viewedit-display-block"><span class="addSpan" resourcekey="Add">
                                </span></a></li>
                                 <li class="role-edit"><span class="ActionEdit-link edit-tab-lnk viewedit-display-block ">
                                </span><a class="edit-tab-lnk ActionEdit-link viewedit-display-block"><span class="addSpan" resourcekey="Edit">
                                </span></a></li>

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="Add-new-form-team">
                    <table width="100%" border="0" cellpadding="0" cellspacing="1" class="Roles-info">
                        <tr id="-2">
                            <th class="th-data-table th-item-num">
                                #
                            </th>
                            <th class="th-data-table">
                                <span resourcekey="RName"></span>
                            </th>
                            <th class="th-data-table display-none">
                                <span resourcekey="RArabicName"></span>
                            </th>
                            <th class="th-data-table ">
                                <span resourcekey="RDescription"></span>
                            </th>
                             <th class="th-data-table ">
                                <span resourcekey="Users"></span>
                            </th>
                            <th class="th-data-table">
                                <span resourcekey="CreatedBy"></span>
                            </th>
                            <th class="th-data-table">
                                <span resourcekey="CreatedDate"></span>
                            </th>
                        </tr>
                        <tr id="-2">
                            <td class="td-data-border" colspan="6">
                            </td>
                        </tr>
                    </table>
                    
                    <table class="RoleUsers-div display-none popup-div">
                    <tr>
                     <td>
                       <span class="label-title" resourcekey="AllUsers"></span>  
                     </td>
                     <td>
                     
                     </td><td>
                     
                     </td>
                     <td>
                       <span class="label-title" resourcekey="RoleUsers"></span>  
                     </td>
                     </tr>
                        <tr>
                            <td>
                                <select id="lstAllUsers" class="lstAllUsers popup-txt-area" runat="server" disabled="disabled" multiple="true" size="20" style="min-width: 170px;">
                                </select>
                            </td>
                            <td colspan="2" class="edit-mode-controle">
                                <input type="button" id="btn_move" class="btn_move popup-btn" value=">" runat="server" style="cursor: pointer;" />
                                <br />
                                <br />
                                <input type="button" id="btn_moveAll" class="btn_moveAll popup-btn" value=">>" runat="server" style="cursor: pointer;" />
                                <br />
                                <br />
                                <input type="button" id="btn_return" class="btn_return popup-btn" value="<" runat="server" style="cursor: pointer;" />
                                <br />
                                <br />
                                <input type="button" id="btn_return_all" class="btn_return_all popup-btn" value="<<" runat="server" style="cursor: pointer;" />
                            </td>
                            <td colspan="2" class="view-mode-controle">
                            </td>
                            <td>
                                <select id="lstRoleUsers" class="lstRoleUsers popup-txt-area" runat="server" disabled="disabled" multiple="true" size="20" style="min-width: 170px;">
                                </select>
                            </td>
                            <td>
                            <div class="Action-div display-none">
                            <ul>
                                <li class="edit-mode-controle"><span class="ActionSaveRole-link save_RoleUsers viewedit-display-block"></span><a class="save_RoleUsers viewedit-display-block">
                                    <span resourcekey="Save"></span></a></li>
                                   
                                   <li class="view-mode-controle"><span class="ActionEditRole-link edit-tab-lnk viewedit-display-block ">
                                        </span><a class="edit_RoleUsers viewedit-display-block"><span class="edit-tab-lnk"
                                            resourcekey="Edit"></span></a></li>
                                   
                                     <li><span class="ActionCancelRole-link cancle_TeamUsers viewedit-display-block"></span><a class="cancle_TeamUsers viewedit-display-block">
                                    <span resourcekey="Cancel"></span></a></li>
                            </ul>
                            </div>
                              <%--  <input type="button" id="save_RoleUsers" class="save_RoleUsers popup-btn" value="Save" runat="server" style="cursor: pointer;" />--%>
                                 <%--<input type="button" id="cancle_TeamUsers" class="cancle_TeamUsers popup-btn" value="Cancle" runat="server"
                                    style="cursor: pointer;" />--%>
                                </td>
                        </tr>
                    </table>
                </div>
                
            </div>
            <div class="five-px">
            </div>
        </div>
    </div>
</asp:Content>
