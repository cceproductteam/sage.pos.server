var RoleItemTemplate;
var RoleId = -1;
var ItemIdentity = 1;
var RolesList = new Array();
var validated = true;
var edit_mood = false;
var LastUpdatedDate = null;
var ExistRoles = false;

//////by Amer 19-2-2014
var lstAllUsers, lstRoleUsers;
var btn_return, btn_move, btn_moveAll, btn_return_all;
var RoleUsersIds = "";
var RestRoleUsersIds = "";
var AllUsersIds = "";
var UserIdsToSave = "";
//var permissionIds = "", hdnPermissionIds;
var UserRoleId = -1;
//var PermissionLists = "";
/////


function RoleItem() {
    this.itemIdentity = ++ItemIdentity;
    this.RoleId = -1;
    this.NameEnglish = '';
    this.NameArabic = '';
    this.Description = '';
    this.PermissionLevel = -1
    this.CreatedBy = '';
    this.CreatedDate = '';
    this.UpdatedBy = ''
}


function InnerPage_Load() {

    

    $('.search-box').hide();
    setSelectedMenuItem('.setup');
    currentEntity = systemEntities.RoleEntityId;
    $('.ActionEdit-link').Show();
    $('.TabbedPanelsContent').height($('.white-container').height() - 8);
    $('.Add-new-form-team').height($('.TabbedPanelsContent').height() - 57);
    checkPermissions(this, systemEntities.RoleEntityId, -1, $('.Action-div'), function (template) {
        LoadViewDefualtOption();
    });
    setPageTitle(getResource("RolesList"));



    GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.RoleItemTemplate, function () {
        ListRoles();
    }, 'RoleItemTemplate');


    $('.add-new-link').click(function () { InsertNewRow(); });
    $('.delete-lnk').click(function () { DeleteRole(); });
    $('.ActionEdit-link').live('click', function () {
        cancleUsersDiv();
        if (ExistRoles) {
            showLoading($('.TabbedPanelsContent'));
            lockEntity(systemEntities.RoleEntityId, -1, function () {
                LoadDefultOptions(); edit_mood = true;
            }, LastUpdatedDate, function () { ListRoles(); });
        } else {
            LoadDefultOptions();
        }
    });


    $('.AddNew-tab-lnk').live('click', function () {
        cancleUsersDiv();
        if (ExistRoles) {
            showLoading($('.TabbedPanelsContent'));
            lockEntity(systemEntities.RoleEntityId, -1, function () {
                LoadDefultOptions(); edit_mood = true;
            }, LastUpdatedDate, function () { ListRoles(); });
        } else {
            LoadDefultOptions();
        }
        InsertNewRow();
    });


    $('.ActionSave-link').live('click', function () { SaveEntity($('#content')); });
    $('.ActionCancel-link').live('click', function () {
        showLoading($('.TabbedPanelsContent'));
        UnlockEntity(systemEntities.RoleEntityId, -1, function () {
            LoadViewDefualtOption();
            ListRoles();
            edit_mood = false;
        });
    });


    $('.Roles-info').find('.cx-control').live('keyup', function (e) {
        if (e.keyCode == 45 || e.which == 45) { InsertNewRow(); }
        if (e.keyCode == 46 || e.which == 46) { DeleteRole(); }
    });

    $('.ContentRow').live('click', function () {
        var TR = this;
        $('.ContentRow').removeClass('selected');
        $(TR).addClass('selected');
        
        if ($(TR).attr('id') != -2 && $(TR).attr('id') != 1) {
            if (edit_mood) $('.delete-lnk').show();
        }
        else $('.delete-lnk').hide();
    });


    //    $('a.ViewRolePermissions').click(function () {
    //        window.open(this.href);
    //        return false;
    //    });

     getRoleUsersInit();

}


function LoadDefultOptions() {
    $('.viewedit-display-none').show();
    $('.viewedit-display-block').hide();
    $('.delete-lnk').hide();
}
function LoadViewDefualtOption() {
    $('.Action-div').show();
    $('.viewedit-display-none').hide();
    $('.viewedit-display-block').show();
}

function RenumberRows() {
    ItemIdentity = 1;
    $.each($('.Roles-info tr'), function (index, item) {
        if ($(this).hasClass('ContentRow')) {
            $(this).find('.ItemIdentity').text(ItemIdentity);
            $(this).removeClass();
            $(this).addClass('ContentRow');
            $(this).addClass('Item' + ItemIdentity);
            ItemIdentity = ItemIdentity + 1;
        }
    });

    ItemIdentity = ItemIdentity - 1;
    $('.Roles-info tr:last').addClass('last-row');
}


function initControls() {

}

function ListRoles() {
    $('.Roles-info').find("tr:.ContentRow").remove();
    ItemIdentity = 1;
    // showLoading($('.TabbedPanelsContent'));
    showLoading($('#content'));
    var data = formatString('{keyword:"{0}",page:{1},resultCount:{2},argsCriteria:"{3}"}', '', 1, 100, '');

    $.ajax({
        type: "POST",
        url: systemConfig.ApplicationURL_Common + systemConfig.RoleWebServiceURL + "FindAllLite",
        contentType: "application/json; charset=utf-8",
        data: data,
        dataType: "json",
        success: function (data) {
            var RolesList = eval("(" + data.d + ")");
            var template = $(RoleItemTemplate);
            if (RolesList.result != null) {
                LastUpdatedDate = RolesList.result[0].UpdatedDate;
                ExistRoles = true;
                $.each(RolesList.result, function (index, item) {
                    FillRoleData(item);
                    ItemIdentity = ItemIdentity + 1;
                });
                ItemIdentity = ItemIdentity - 1;
            }
            else { ExistRoles = false; }

            $('.Roles-info tr:last').addClass('last-row');
            edit_mood = false;
            hideLoading();
        },
        error: function (e) {
        }
    });
}

function FillRoleData(Item) {
    if (Item != null) {
        var template = $(RoleItemTemplate).clone();
        template = mapEntityInfoToControls(Item, template);
        template.attr('id', Item.Id);
        template.attr('UsersCount', Item.UsersCount);
        template.addClass('Item' + ItemIdentity);
        template.attr('validate', true);
        template.find('input:.NameEnglish').addClass('Item' + ItemIdentity);
        template.find('input:.NameArabic').addClass('Item' + ItemIdentity);
        template.find('textArea:.Description').addClass('Item' + ItemIdentity);
        template.find('.ItemIdentity').text(ItemIdentity);
        template.find('.cx-control').blur(function () { ValidateItem(template, Item.Id); });
        template.find('.CreatedDate').text(getDate('dd-mm-yy', formatDate(Item.CreatedDate)));

        template.find('.ViewRolePermissions').attr("href", systemConfig.ApplicationURL_Common + systemConfig.pageURLs.RolePermissions + "?rid=" + Item.Id);
        template.find('.ShowAllUser-link').click(function () {

            UserRoleId = Item.Id;
            showRoleUser(Item.Id);

        });


        //template.find('.ViewRolePermissions').attr("target","_blank");
        template.find('.viewedit-display-none').hide();
        template.find('.viewedit-display-block').show();
        $('.Roles-info').append(template);

    }

}


function InsertNewRow() {
    
    if ($('.last-row').attr('id') != -2 || ExistRoles) {
        if (!$('.Roles-info').find('.last-row').find('input:.NameEnglish').val() == "") { //|| ExistRoles

            //&& $('.Roles-info').find('.last-row').find('.NameArabic').val() == "")
            ItemIdentity = ItemIdentity + 1;
            var template = $(RoleItemTemplate).clone();
            $('.Roles-info').find('tr').removeClass('last-row');
            template.addClass('last-row');
            template.attr('id', -1);
            template.attr('validate', false);
            template.find('.cx-control').keyup(function () { ValidateItem(template, -1); });
            template.find('.ItemIdentity').text(ItemIdentity);
            $('.Roles-info').append(template);
            template.find('input:.NameEnglish').select();
            template.find('.ShowAllUser-link').hide();
            $('.ContentRow').removeClass('selected');
            $(template).addClass('selected');
            $('.delete-lnk').show();

        }
        else {
            $('.Roles-info').find('.last-row').addClass('selected');
            $('.Roles-info').find('.last-row').find('input:.NameEnglish').select();
        }
    }
    else {
        if (ItemIdentity == 0)
            ItemIdentity = ItemIdentity + 1;
        var template = $(RoleItemTemplate);
        $('.Roles-info').find('tr').removeClass('last-row');
        template.addClass('last-row');
        template.attr('id', -1);
        template.attr('validate', false);
        template.find('.cx-control').keyup(function () { ValidateItem(template, -1); });
        template.find('.ItemIdentity').text(ItemIdentity);
        $('.Roles-info').append(template);
        template.find('input:.NameEnglish').select();
        // $('.ContentRow').find('td,th').removeClass('selected-row');
        // $(template).find('td,th').addClass('selected-row');

    }
}

function DeleteRole() {

    if ($('.selected').attr('UsersCount') > 0) {
        var msg = getResource("prefxDeleteRoleMsg") + $('.selected').attr('UsersCount') + getResource("suffixDeleteRoleMsg");
        showOkayMsg(getResource("Delete"), msg);
    }
    else {
        showConfirmMsg(getResource("Delete"),getResource("AreyousureRole"), function () {
            $('.selected').remove();
            RenumberRows();
            $('.Roles-info tr:last').addClass('last-row selected');
        });
    }

    //    showConfirmMsg('Delete', 'Are you Sure you want to delete this Role?', function () {
    //        if ($(tdRowToDelete).hasClass('last-row')) {
    //            $(tdRowToDelete).remove();
    //            $('.Roles-info tr:last').addClass('last-row');
    //        }
    //        else {
    //            $(tdRowToDelete).remove();
    //            RenumberRows();
    //        }
    //        ItemIdentity = ItemIdentity - 1;
    //        $('.delete-lnk').hide();
    //        $('.ActionDelete-link').hide();
    //        $('.ContentRow').find('td,th').removeClass('selected-row');
    //    });
}


function ValidateItem(template, Id) {
//comented by Amer 2-4-2014
    //template.find('.validation').remove();
    if (template.find('input:.NameEnglish').val() == "") {
        //addPopupMessage(template.find('input:.NameEnglish'), getResource("required"));
    }
    else {
        template.attr('validate', true);
       // template.find('.validation').remove();
        //template.find('input:.NameEnglish').removeClass('cx-control-red-border');
    }

}


function getItems() {

    validated = true;
    RolesList = new Array();
    if ($('.Roles-info').find('.last-row').find('input:.NameEnglish').val() == "" && $('.Roles-info').find('.last-row').find('textArea:.Description').val() == "") {
        //&& $('.Roles-info').find('.last-row').find('input:.NameArabic').val() == ""
        $('.last-row').remove();
    }

    $.each($('.Roles-info tr'), function (index, item) {
        if ($(this).attr('id') != "-2") {
            if (eval($(this).attr('validate'))) {
                var newItem = new RoleItem();
                newItem.RoleId = $(this).attr('Id');
                newItem.NameEnglish = $(this).find('input:.NameEnglish').val();
                newItem.NameArabic = $(this).find('input:.NameArabic').val();
                newItem.Description = $(this).find('textArea:.Description').val();
                if ($(this).attr('id') == -1)
                    newItem.CreatedBy = UserId;
                else
                    newItem.UpdatedBy = UserId;
//                RolesList.push(newItem);
                              validated = true;
                
                if (RolesList.length == 0)
                { RolesList.push(newItem); }
                //  validated = true;
                else {
                    $.each(RolesList, function (index, item) {
                        if (item.NameEnglish.trim() == newItem.NameEnglish.trim()) {
                            validated = false;
                            return false;
                        }
                    });
                    if (validated)
                        RolesList.push(newItem);
                    else {
                        addPopupMessage($(this).find('input:.NameEnglish'), 'already exists');
                        return false;
                    }
                }
            }
            else { validated = false; }
        }
    });


    if (!validated)
        return false;
    else validated = true;
}


function SaveEntity() {

    //saveForm($('table'));
    //2-4-2014
   // debugger
    if (!saveForm($('.Roles-info'))) {
        return false;
    }
    getItems();
    if (!validated)
        return false;

    if (!saveForm($('.Roles-info'))) {
        return false;
    }

    var lastUpdatedDate = getEntityLockDateFormat(LastUpdatedDate);
    var data = '{UserId:{0}, RolesList:"{1}"}';
    data = formatString(data, UserId, escape(JSON.stringify(RolesList)));
    showLoading($('.TabbedPanelsContent'));
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: systemConfig.ApplicationURL + systemConfig.RoleWebServiceURL + "SaveRole",
        headers: getEntitylockAjaxHeaders(lastUpdatedDate),
        data: data,
        datatype: "json",
        success: function (data) {
            hideLoading();
            showStatusMsg(getResource("savedsuccessfully"));
            ListRoles();
            $('.delete-lnk').hide();
            $('.viewedit-display-none').hide();
            $('.viewedit-display-block').show();

        },
        error: function (ex) {
            handleLockEntityError(ex, null, function () {
                ListRoles();
                $('.ActionSave-link').hide();
                $('.add-new-link').hide();
                $('.ActionCancel-link').hide();
                $('.ActionDelete-link').hide();
                $('.ActionEdit-link').Show();
            });
        }
    });

}

/////////////////by Amer

function FillAllUsers(AllUsers) {
    $('.lstAllUsers').find('option').remove().end();
    $('.lstRoleUsers').find('option').remove().end();
    RoleUsersIds = "";
    RestRoleUsersIds = "";
    $.each(AllUsers, function (index, item) {
        if (item.HasRole) {
            var Option = ' <option value="' + item.value + '">' + item.label + '</option>';
            $('.lstRoleUsers').append(Option);
            RoleUsersIds = RoleUsersIds + ',' + item.UserId;
        }
        else {
            var Option = ' <option value="' + item.value + '">' + item.label + '</option>';
            $('.lstAllUsers').append(Option);
            RestRoleUsersIds = RestRoleUsersIds + ',' + item.UserId;
        }
    });
    UserIdsToSave = RoleUsersIds;
    AllUsersIds = RoleUsersIds + RestRoleUsersIds;
}

function showRoleUser(RoleId) {
    $('.RoleUsers-div').show();
    RoleUserViewMood();
    getRoleUsers(RoleId);
}

function getRoleUsers(roleId) {
    var lastUpdatedDate = getEntityLockDateFormat(LastUpdatedDate);
    var data = '{roleId:{0}}';
    data = formatString(data, roleId);
    showLoading($('.TabbedPanelsContent'));
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: systemConfig.ApplicationURL + systemConfig.UsersServiceURL + "FindRoleUsersLite",
        headers: getEntitylockAjaxHeaders(lastUpdatedDate),
        data: data,
        datatype: "json",
        success: function (data) {
            var RolesList = eval("(" + data.d + ")");
            if (RolesList.result != null)
                FillAllUsers(RolesList.result);
            hideLoading();
        },
        error: function (ex) {
            handleLockEntityError(ex, null, function () {
            });
        }
    });
}

function MovePermission(IsAdd) {

    if (IsAdd) {
        var selectedOption = $('.lstAllUsers').find('option:selected');
        if (selectedOption.html() != null) {
            var Option = '<option value="' + selectedOption.attr('value') + '" index="' + selectedOption.index() + '">' + selectedOption.html() + '</option>';
            $('.lstAllUsers').find('option:selected').next().attr('selected', 'selected');
            $('.lstRoleUsers').append(Option);
            selectedOption.remove();
            UserIdsToSave += ","+selectedOption.attr('value') ;
        }
    }
    else {
        var selectedOption = $('.lstRoleUsers').find('option:selected');
        if (selectedOption.html() != null) {
            var Option = '<option value="' + selectedOption.attr('value') + '">' + selectedOption.html() + '</option>';
            $('.lstAllUsers').append(Option);
            $('.lstRoleUsers').find('option:selected').next().attr('selected', 'selected');
            selectedOption.remove();
            UserIdsToSave = UserIdsToSave.replace(selectedOption.attr('value'), "").replace(",,", ",");
        }
    }
}

function MoveAllPermissions(IsAdd) {
    if (IsAdd) {
        UserIdsToSave = "";
        $('.lstAllUsers').find('option').each(function () {
            var Option = '<option value="' + $(this).attr('value') + '">' + $(this).html() + '</option>';
            $('.lstRoleUsers').append(Option);
            $(this).remove();
            UserIdsToSave = AllUsersIds;
        });
    }
    else {
        $('.lstRoleUsers').find('option').each(function () {
            var Option = '<option value="' + $(this).attr('value') + '">' + $(this).html() + '</option>';
            $('.lstAllUsers').append(Option);
            $(this).remove();
        });
        UserIdsToSave = "";
    }
}


function saveRoleUsers(roleId,UserIds) {

    var lastUpdatedDate = getEntityLockDateFormat(LastUpdatedDate);
    var data = '{roleId:{0},RoleUserIds:"{1}"}';
    data = formatString(data, roleId, UserIds);
    showLoading($('.TabbedPanelsContent'));
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: systemConfig.ApplicationURL + systemConfig.UsersServiceURL + "SaveUsersRole",
        headers: getEntitylockAjaxHeaders(lastUpdatedDate),
        data: data,
        datatype: "json",
        success: function (data) {
            //            var RolesList = eval("(" + data.d + ")");
            //            if (RolesList.result != null)
            //                FillAllUsers(RolesList.result);
            hideLoading();
            $('.RoleUsers-div').hide();
            RoleUserViewMood();
            //xxxxx
            ListRoles();
        },
        error: function (ex) {
            RoleUserViewMood();
            handleLockEntityError(ex, null, function () {
            });
        }
    });
}

function getRoleUsersInit() {

    document.onkeydown = function (e) {
        if (e.keyCode == 27 || e.which == 46) {
            RoleUserViewMood();
            $('.RoleUsers-div').hide();
        }
    };
    lstAllUsers = $(lstAllUsers);
    lstRoleUsers = $(lstRoleUsers);
    btn_move = $('.btn_move');
    btn_return = $('.btn_return');
    btn_moveAll = $('.btn_moveAll');
    btn_return_all = $('.btn_return_all');
    btn_save = $('.save_RoleUsers');
    btn_cancle = $('.cancle_TeamUsers');

    btn_edit = $('.edit-tab-lnk');

    $('.edit-mode-controle').hide();
    btn_edit.click(function () {
        editRoleUsers();
    });

    btn_move.click(function () {
        MovePermission(true);
    });
    btn_return.click(function () {

        MovePermission(false);
    });
    btn_moveAll.click(function () {
        MoveAllPermissions(true);
    });
    btn_return_all.click(function () {
        MoveAllPermissions(false);
    });
    btn_save.click(function () {

        saveRoleUsers(UserRoleId, UserIdsToSave);
    });
    btn_cancle.click(function () {
        cancleUsersDiv();
    });
}

function editRoleUsers() {

    $('.lstAllUsers').prop('disabled', false);
    $('.lstRoleUsers').prop('disabled', false);
    $('.view-mode-controle').hide();
    $('.edit-mode-controle').show();

}
function RoleUserViewMood() {
    $('.lstAllUsers').prop('disabled', true);
    $('.lstRoleUsers').prop('disabled', true);
    $('.view-mode-controle').show();
    $('.edit-mode-controle').hide();
}
function cancleUsersDiv() {
    RoleUserViewMood();
    $('.RoleUsers-div').hide();
}

////////////////////////