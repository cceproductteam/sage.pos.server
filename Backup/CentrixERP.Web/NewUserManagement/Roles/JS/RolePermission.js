var roleId = -1, roleServiceURL, RolePermissionTemplate; //, permIds = ',';
var permissionArray = new Array();
var RoleAC;
var CategoryId = -1;
var CategoryTemplate;
var checkBoxChange = false;
var lastUpdatedDate = null;
var wait = false;

function InnerPage_Load() {
    $('.search-box').hide();
    setSelectedMenuItem('.setup');
    CategoryTemplate = $('.Catetogry-perm').clone();
    $('.Catetogry-perm').remove();
    roleServiceURL = systemConfig.ApplicationURL_Common + systemConfig.RoleWebServiceURL;
    $('.view-group-permi').height($('.white-container').height() - 120);
    roleId = getQSKey('rid') || -1;
    GetTemaplte(systemConfig.ApplicationURL_Common + systemConfig.HTMLTemplateURLs.RolePermissionTemplate, function () {
        RoleAC = $('.RoleAC').AutoComplete({
            serviceURL: roleServiceURL + 'FindAllLite', required: true
        });

        RoleAC.onChange(function () {

            UnCollapsAll();
            UnSelectAll();
            if (wait)
                return;
            //wait = true;
            roleId = RoleAC.get_ItemValue();
            lastUpdatedDate = (RoleAC.get_Item() != null) ? RoleAC.get_Item().PermissionsUpdatedDate : null;
            permissionArray = new Array();
            $('.permissions-container').empty();
            editMode();
            if (roleId > 0) {
                $('.default-permi').hide();
                getRolePermission();
                wait = true;
            }
            else {
                $('.default-permi').show();
            }
        });

        if (roleId > 0)
            getRole();

        $('.btn-save-permission').click(function () {
            lockRolePermissionEntity(roleId, function () {
                if (saveForm($('.action-control'))) {
                    saveEntity();
                }
            }, lastUpdatedDate);
        });

        $('.btn-edit-permissions').click(function () {
            if (RoleAC.get_Item() != null) {

                lockRolePermissionEntity(roleId, function () {
                    $('input[type=checkbox]').removeAttr('disabled');
                    $('.btn-save-permission').show();
                    $('.btn-edit-permissions').hide();
                }, lastUpdatedDate);
            }
        });


    }, 'RolePermissionTemplate');


    ///////////////group category

    $('.category-perm-group-title').live('click', function () {
        var collapsParent = $(this).parents('.category-perm-group');
        if ($(this).children('.collaps-arrow').hasClass('collaps-open')) {
            $(collapsParent).find('.Permissions').slideUp();
            $(collapsParent).css('padding-bottom', '0px');
            $(this).children('.collaps-arrow').toggleClass('collaps-open');
            $(this).find('.category-perm-group-title').removeClass('perm-group-title-closed');
        }
        else {
            $(collapsParent).find('.Permissions').slideDown();
            $(collapsParent).css('padding-bottom', '3px');
            $(this).children('.collaps-arrow').toggleClass('collaps-open');
            $(this).find('.category-perm-group-title').addClass('perm-group-title-closed');
        }
    });
    ////////////////
}


function getRolePermission() {

    showLoading($('.white-container'))
    permissionArray = new Array();
    var para = formatString('{roleId:{0}}', roleId);
    $('.permission-items').empty();
    editMode();
    $.ajax({
        type: 'POST',
        data: para,
        url: roleServiceURL + 'FindRolePermissionLite',
        contentType: 'application/json; charset:utf-8',
        dataType: 'json',
        success: function (data) {
            var result = eval('(' + data.d + ')');
            if (result.result)
                loadPermissions(result.result);
            else
                showStatusMsg(getResource("SMNoDataFound"));
            wait = false;
            $('input[type=checkbox]').not('.collapse-all-roles').attr('disabled', 'disabled');
            hideLoading();
        },
        error: function () {
            wait = false;
            hideLoading();
        }
    });
}

function loadPermissions(permissions) {
    
    var counter = 0;
    var tempCategoryTemplate = null;
    $.each(permissions, function (index, group) {

        if (counter % 3 == 0)
            counter = 0;
        counter++;
        var permissionObj = new Object();
        permissionObj.permissions = new Array();
        permissionObj.EntityId = group.EntityId;
        permissionObj.RoleId = roleId;

        var template = $(RolePermissionTemplate).clone();

        if (CategoryId != group.Permissions[0].ModuleId) {
            counter = 1;
            if (index != 0) {
                $('.permissions-container').append(tempCategoryTemplate);
            }

            tempCategoryTemplate = CategoryTemplate.clone();
            CategoryId = group.Permissions[0].ModuleId;
            
            tempCategoryTemplate.find('.group-name').text(Lang == 'en' ? group.Permissions[0].ModuleName : group.Permissions[0].ModuleNameAR);
        }
        //4/5/2013
        template.find('.group-name').text(Lang == 'en' ? group.EntityName : group.EntityNameAr);
        var checkAllGroup = true;

        template.find('.select-all').change(function () {
            checkBoxChange = false;
            //if ($('.check-all-checkbox-perm').is(':checked'))
            //$('.check-all-checkbox-perm').attr('checked', false);
            selectAllCheckBoxClick(this, group.EntityId, group.Permissions);
        });



        $.each(group.Permissions, function (index, item) {
            
            var li = $('<li>').addClass('perm-item');
            var chk = $('<input>').attr({ type: 'checkbox', class: 'perm-checkbox', checked: item.HasPermission });
            var span = $('<span>').addClass('perm-group-items-span').html(Lang == 'en' ? item.PermissionName : item.PermissionNameAr);
            li.append(chk, span);
            template.find('.permission-list').append(li);

            if (item.HasPermission) {
                permissionObj.permissions.push(item.EntityPermissionId);
            }

            chk.change(function () {
                permissionCheckBoxClick(item.EntityPermissionId, this, group.EntityId);
            });

            checkAllGroup = item.HasPermission && checkAllGroup;
        });

        template.find('.select-all').attr('checked', checkAllGroup);

        tempCategoryTemplate.find('.perm-div-' + counter + 'Col').append(template);
        permissionArray.push(permissionObj);
    });
    $('.permissions-container').append(tempCategoryTemplate);
   
    initEvents();
    hideLoading();
}

function UnCollapsAll() {
    $('.collapse-all-roles').attr('checked', false);
    $('.perm-group-items').slideUp();
    $('.Permissions').slideUp();
    $('.PermDetails-div').removeClass('PermDetails-div-open');
    $('.perm-group-title-span').removeClass('PermDetails-div-open-title');
    $('.perm-group').css('padding-bottom', '0px');
    $('.collaps-arrow').toggleClass('collaps-open');
    $('.collapse-all-roles').siblings('.lbl-title').html(getResource("UnCollapseAll")).css('color', '#4f4f4f');
}

function UnSelectAll() {
    $('.check-all-checkbox-perm').attr('checked', false);
    //$('.Permissions').find('input[type=checkbox]').attr('checked', false);
    $('.check-all-checkbox-perm').siblings('.lbl-title').html(getResource("SelectAll")).css('color', '#4f4f4f');


}

function initEvents() {

    $('.select-all').click(function () {
        checkBoxChange = true ;
    });
    $('.perm-group-title').not('.select-all').click(function () {
        if (!checkBoxChange) {
            var collapsParent = $(this).parents('.perm-group');
            if ($(this).children('.collaps-arrow').hasClass('collaps-open')) {
                $(collapsParent).find('.perm-group-items').slideUp();
                $(collapsParent).find('.PermDetails-div').removeClass('PermDetails-div-open');
                $(collapsParent).find('.perm-group-title-span').removeClass('PermDetails-div-open-title');
                $(collapsParent).css('padding-bottom', '0px');
                $(this).children('.collaps-arrow').toggleClass('collaps-open');
                $(this).find('.perm-group-title').removeClass('perm-group-title-closed');
            }
            else {

                $(collapsParent).find('.perm-group-items').slideDown();
                $(collapsParent).find('.PermDetails-div').addClass('PermDetails-div-open');
                $(collapsParent).find('.perm-group-title-span').addClass('PermDetails-div-open-title');
                $(collapsParent).css('padding-bottom', '15px');
                $(this).children('.collaps-arrow').toggleClass('collaps-open');
                $(this).find('.perm-group-title').addClass('perm-group-title-closed');
            }
        }


    });



    $('.check-all-checkbox-perm').click(function () {
        if ($(this).is(':checked')) {
            $('.Permissions').find('input[type=checkbox]').attr('checked', true);
            $(this).siblings('.lbl-title').html(getResource("UnSelectAll")).css('color', '#A4C677');

            $('.select-all').trigger("change");

        }
        else {
            $('.Permissions').find('input[type=checkbox]').attr('checked', false);
            $(this).siblings('.lbl-title').html(getResource("SelectAll")).css('color', '#4f4f4f');

            $.each(permissionArray, function (index, entityObj) {
                entityObj.permissions = new Array();
            });

        }
    });

    $('.collapse-all-roles').click(function () {
        if ($(this).is(':checked')) {
            //  $('.category-perm-group-title').trigger("click");
            $('.perm-group-items').slideDown();
            $('.Permissions').slideDown();
            $('.PermDetails-div').addClass('PermDetails-div-open');
            $('.perm-group-title-span').addClass('PermDetails-div-open-title');
            $('.perm-group').css('padding-bottom', '15px');
            $('.collaps-arrow').toggleClass('collaps-open');
            $(this).siblings('.lbl-title').html(getResource("CollapseAll")).css('color', '#A4C677');

        }
        else {
            //  $('.category-perm-group-title').trigger("click");
            $('.perm-group-items').slideUp();
            $('.Permissions').slideUp();
            $('.PermDetails-div').removeClass('PermDetails-div-open');
            $('.perm-group-title-span').removeClass('PermDetails-div-open-title');
            $('.perm-group').css('padding-bottom', '0px');
            $('.collaps-arrow').toggleClass('collaps-open');
            $(this).siblings('.lbl-title').html(getResource("UnCollapseAll")).css('color', '#4f4f4f');

        }
    });
}

function saveEntity() {
    var allPermissions = $('.check-all-checkbox-perm').is(':checked');
    var permissions = '{}';
    if (!allPermissions && ($('.perm-checkbox:[checked="checked"]').length != 0)) {        
        permissions = JSON.stringify(getPermissionArrayToSave())
    }

   showLoadingPnl();
   $.ajax({
       type: 'POST',
       data: formatString("{roleId:{0}, permissionIds:'{1}',AllPermissions:{2}}", roleId, permissions, allPermissions),
       url: roleServiceURL + 'AddEditRolePermission',
       dataType: 'json',
       contentType: 'application/json; charset=utf-8',
       headers: getEntitylockAjaxHeaders(getEntityLockDateFormat(lastUpdatedDate)),
       success: function (data) {
          // UnCollapsAll();
           UnSelectAll();
           var returnedData = eval('(' + data.d + ')');
           if (returnedData.result) {
               showStatusMsg(getResource("savedsuccessfully"));
               lastUpdatedDate = returnedData.result.EntityLastUpdatedDate;
           }
           else
               showStatusMsg(getResource("FaildToSave"));

           $('.check-all-checkbox-perm').removeAttr('checked');
           $('input[type=checkbox]').not('.collapse-all-roles').attr('disabled', 'disabled');
           editMode();
           hideLoading();


       },
       error: function (ex) {
           hideLoading();
       }


   });


}


function permissionCheckBoxClick(permissionId, checkBoxPermission, entityId) {
    if ($(checkBoxPermission).is(':checked')) {
        var checkedCount = $(checkBoxPermission).parents('.permission-list').find('input:[type="checkbox"]').not('input:[checked="checked"]').length;
        $(checkBoxPermission).parents('#General-Perm').find('.select-all').attr('checked', checkedCount == 0);

        if (permissionArray.length == 0) {
            var permissionObj = new Object();
            permissionObj.permissions = new Array();
            permissionObj.EntityId = entityId;
            permissionObj.RoleId = roleId;
            permissionArray.push(permissionObj);
        }

        $.each(permissionArray, function (index, entityObj) {
            if (entityObj.EntityId == entityId) {
                entityObj.permissions.push(permissionId);
                return false;
            }
        });
    }
    else {
        var exitLoop = false;
        $(checkBoxPermission).parents('#General-Perm').find('.select-all').removeAttr('checked');
        $.each(permissionArray, function (entityIndex, entityObj) {

            if (entityObj.EntityId == entityId) {
                $.each(entityObj.permissions, function (permissionIndex, permissionObj) {
                    if (permissionObj == permissionId) {
                        //splice from the array
                        entityObj.permissions.splice(permissionIndex, 1);
                        exitLoop = true;
                        return false;
                    }
                });
            }
            if (exitLoop) {
                return false;
            }

        });
        $('.check-all-checkbox-perm').attr('checked', false);

    }
}

function selectAllCheckBoxClick(selectallchk, entityId, entityPermissions) {
    if ($(selectallchk).is(':checked')) {
        $(selectallchk).parents('#General-Perm').find('input[type="checkbox"]').attr('checked', 'checked');
      
        $.each(permissionArray, function (entityIndex, entity) {
            if (entity.EntityId == entityId) {
                permissionArray.splice(entityIndex, 1);
                var permissionObj = new Object();
                permissionObj.permissions = new Array();
                permissionObj.EntityId = entityId;
                permissionObj.RoleId = roleId;
                $.each(entityPermissions, function (index, item) {
                    permissionObj.permissions.push(item.EntityPermissionId);
                });

                permissionArray.push(permissionObj);
                return false;
            }
        });

    }
    else {
       
         $(selectallchk).parents('#General-Perm').find('input[type="checkbox"]').removeAttr('checked');
        $.each(permissionArray, function (index, item) {
            if (entityId == item.EntityId) {
                permissionArray[index].permissions = new Array();
                //permissionArray.splice(index, 1);
                return false;
            }
        });
        $('.check-all-checkbox-perm').attr('checked', false);
    }
}


function getPermissionArrayToSave() {
    var allPermission = new Array();
    $.each(permissionArray, function (index, entityObj) {
        $.each(entityObj.permissions, function (index, permissoin) {
            var obj = {
                EntityId: entityObj.EntityId,
                RoleId: roleId,
                Permissionid: permissoin
            };
            allPermission.push(obj);
        });
    });
    return allPermission;
}

function showLoadingPnl() {
    showLoading('.white-container');
    $('.Permissions').css('opcity', '0.4');
}


function getRole() {
    $.ajax({
        type: 'POST',
        url: roleServiceURL + 'FindByIdLite',
        data: '{id:' + roleId + '}',
        dataType: 'json',
        contentType: 'application/json; charset:utf-8;',
        success: function (data) {
            var result = eval('(' + data.d + ')').result;
            if (result) {
                var roleObj = {
                    value: result.Id,
                    label: result.NameEnglish,
                    PermissionsUpdatedDate: result.PermissionsUpdatedDate
                };

                $('.permission-items').empty();
                $('.default-permi').hide();

                RoleAC.set_Item(roleObj);
                lastUpdatedDate = roleObj.PermissionsUpdatedDate;
                getRolePermission();
            }
            else {
                showStatusMsg(getResource("RoleNotFound"));
            }
        },
        error: function () { }
    });
}



function lockRolePermissionEntity(RoleId, callback, UpdatedDate) {
   // alert(formatDate(UpdatedDate));
    var DateTime = getEntityLockDateFormat(UpdatedDate);
    var url = systemConfig.ApplicationURL_Common + systemConfig.RoleServiceURL + 'LockRolePermissions';
    var args = formatString('{entityId:{0},roleId:{1},timeStamp:"{2}"}', systemEntities.RolePermissions, RoleId, DateTime);

    $.ajax({
        type: 'POST',
        url: url,
        data: args,
        contentType: 'application/json; charset=utf8;',
        dataType: 'json',
        success: function (data) {
            var result = eval('(' + data.d + ')');
            if (result.statusCode.Code == 0) {
               
            }
            else {
                if (typeof callback == 'function')
                    callback();
            }
           // if (hideLoadingAfterLock)
                hideLoading();
        },
        error: function (ex) {
            handleLockEntityError(ex, null, function () { $('.permissions-container').empty(); getRole(); });
          
            hideLoading();
        }
    });
}

function editMode() {
    $('.btn-save-permission').hide();
    $('.btn-edit-permissions').show();
}