var ShiftsServiceURL;
var ShiftsViewItemTemplate;

var ItemListTemplate;
var ShiftsId = selectedEntityId;
var lastUpdatedDate = null;
var addShiftsMode = false;

function InnerPage_Load() {
    setSelectedMenuItem('.setup');
    ShiftsServiceURL = systemConfig.ApplicationURL_Common + systemConfig.ShiftsServiceURL;
    currentEntity = systemEntities.ShiftsEntityId;
    //Advanced search Data
    mKey = 'Shifts'; //replace it with entity modual key
    InnerPage_Load2();
    $('.QuickSearch-holder').show();
    LoadSearchForm2(mKey, false);
    // Advanced search Data

    setPageTitle(getResource("Shifts"));
    setAddLinkAction(getResource("AddShifts"), 'add-new-item');
    $('.add-entity-button').show();
    GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.AddShiftsItemTemplate, function () {
        GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.ShiftsListItemTemplate, loadData, 'ItemListTemplate');

    }, 'AddItemTemplate');

    $('.result-block').live("click", function () {
        ShiftsId = $(this).attr('id');
    });

    deletedEntityCallBack = function () {
        removeDeletedEntity();
    };

    updatedEntityCallBack = function () {
        getEntityData(selectedEntityId);
    };

}

function loadData(data) {

    GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.ShiftsListViewItemTemplate, function () {
    checkPermissions(this, systemEntities.ShiftsEntityId, -1, ShiftsViewItemTemplate, function(returnedData) {
            ShiftsViewItemTemplate = returnedData;
            loadEntityServiceURL = ShiftsServiceURL + "FindByIdLite";
            entityItemListTemplate = data;
            entityItemViewTemplate = returnedData;
            findAllEntityServiceURL = ShiftsServiceURL + "FindAllLite";
            deleteEntityServiceURL = ShiftsServiceURL + "Delete";
            selectEntityCallBack = getShiftsInfo;
            listEntityCallBack = callBackAfterList;
            loadDefaultEntityOptions();
        });
    }, 'ShiftsViewItemTemplate');

}

function initControls(template, isAdd) {
    if (!template) template = $('.TabbedPanels');
    if (!isAdd)
        $('.historyUpdated').append(HistoryUpdatedListViewTemplate.clone());
    LoadDatePicker(template.find('input:.date'), 'dd-mm-yy');
    template.find('.numeric').autoNumeric();
    preventInputChars(template.find('input:.integer'));
    fillDateList(template.find('select:.lstStartTime,.lstEndTime'));
}


function initAddControls(template) {
    initControls(template, true);
}

function getShiftsInfo(item) {

    if ($('.result-block').length > 0 && getQSKey('fdb') > 0 && isFirstTimeAdd) {
        $('.add-entity-button').first().click();
    }

    if (item != null) {
        if (item.StartTime != '' && item.StartTime != null) {
            $('select:.lstStartTime').val(item.StartTime);
            $('span:.StartTime').text(item.StartTime);
        }

        if (item.EndTime != '' && item.EndTime != null) {
            $('select:.lstEndTime').val(item.EndTime);
            $('span:.EndTime').text(item.EndTime);
        }


    }

}

 function setListItemTemplateValue(entityTemplate, entityItem) {
     entityTemplate.find('.result-data1 label').text(Truncate(entityItem.ShiftName, 20));
     entityTemplate.find('.result-data2 label').text(Truncate(entityItem.ShiftDescription, 20));
     return entityTemplate;
 }

 function setListItemServiceData(keyword, pageNumber, resultCount) {
     if (!QuickSearch) {
         return formatString('{ keyword: "{0}", page:{1}, resultCount:{2} ,argsCriteria:"{ ShiftsId:{3}}"}', keyword, pageNumber, resultCount, filterEntityId);
     }
     else {
         return formatString('{mKey:"{0}",SearchCriteriaObj:"{1}",SortType:{2},SortFields:"{3}",TotalRecords:{4},PageNumber:{5}}', mKey, SearchCriteriaObj, SortType, SortFields, resultCount, pageNumber);
     }
 }



 function SaveEntity(isAdd, template) {
     var controlTemplate;
     var valid = true;

     if (isAdd) {
         controlTemplate = $('.add-Shifts-info');
         showLoading('.add-new-item');
     }
     else {
         controlTemplate = $('.Shifts-info');
         showLoading('.Result-info-container');
     }

     var StartTimeValue = controlTemplate.find('select:.lstStartTime').val();
     var EndTimeValue = controlTemplate.find('select:.lstEndTime').val();

     if (!saveForm(controlTemplate)) {
         hideLoading();
         valid = false;
     }


     if (StartTimeValue == '--:--') {
         addPopupMessage(controlTemplate.find('select:.lstStartTime'), getResource("required"));
         valid = false;
     }

     if (EndTimeValue == '--:--') {
         addPopupMessage(controlTemplate.find('select:.lstEndTime'), getResource("required"));
         valid = false;
     }

     addShiftsMode = isAdd;
     if (!valid) {
         hideLoading();
         return false;
     }

     ShiftsId = -1;
     if (!isAdd) {
         ShiftsId = selectedEntityId;
         lastUpdatedDate = getEntityLockDateFormat(currentEntityLastUpdatedDate);
     }

     var ShiftsObj = {
         ShiftName: controlTemplate.find('input.ShiftName').val(),
         ShiftDescription: controlTemplate.find('input.ShiftDescription').val(),
         StartTime: (StartTimeValue == '--:--' ? '00:01' : StartTimeValue),
         EndTime: (EndTimeValue == '--:--' ? '00:01' : EndTimeValue)
     };
     var data = formatString('{id:{0}, ShiftsInfo:"{1}"}', isAdd ? -1 : selectedEntityId, escape(JSON.stringify(ShiftsObj)));

     if (ShiftsId > 0)
         url = ShiftsServiceURL + "Edit";
     else
         url = ShiftsServiceURL + "Add";

     post(url, data, function (returned) { saveSuccess(returned, isAdd, template); }, saveError, getEntitylockAjaxHeaders(lastUpdatedDate));
 }

 function saveSuccess(returnedValue,isAdd,template) {
     if (returnedValue.statusCode.Code == 501) {
         addPopupMessage(template.find('input.ShiftName'), getResource("DublicatedShiftName"));
         hideLoading();
         return false;
     }
     else if (returnedValue.statusCode.Code == 0) {
         hideLoading();
         if (returnedValue.statusCode.message == 'validation' && returnedValue.result) {
             filterError(returnedValue.result,template);
         }
         showStatusMsg(getResource("FaildToSave"));
         return false;
     }
     if (!addShiftsMode) {
         showStatusMsg(getResource("savedsuccessfully"));
         hideLoading();
         loadEntityData(loadEntityServiceURL, entityItemViewTemplate, selectedEntityId, getShiftsInfo);
     }
     else {
         hideLoading();
         addShiftsMode = false;
         $('.add-new-item').hide();
         $('.add-new-item').empty();
         appendNewListRow(returnedValue.result);
         showStatusMsg(getResource("savedsuccessfully"));
     }
 }

 function saveError(ex) {
     hideLoading();
     handleLockEntityError(ex, deletedEntityCallBack, updatedEntityCallBack);
 }



 function filterError(errors, template) {
     var allMsg = '';
     template.find('.validation').remove();
     $.each(errors, function (index, error) {
         // var template = $('.Shifts-info');
         if (error.GlobalMessage) {
             allMsg += '<span>' + error.Message + '<br/></span>';
         }
         else if (error.ControlName == 'ShiftName') {
             addPopupMessage(template.find('input.ShiftName'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'ShiftDescription') {
             addPopupMessage(template.find('input.ShiftDescription'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'StartTime') {
             addPopupMessage(template.find('select.StartTime'), getResource(error.ResourceKey));
         }
         else if (error.ControlName == 'EndTime') {
             addPopupMessage(template.find('select.EndTime'), getResource(error.ResourceKey));
         }

     });

     hideLoading();
     if (allMsg.trim()) {
         showOkayMsg('Validation error', allMsg.trim());
     }
 }

 function callBackAfterList() {
     if ($('.result-block').length <= 0 && getQSKey('fdb') > 0 && isFirstTimeAdd) {
         $('.add-entity-button').first().click();
     }
 }
 function fillDateList(list) {
     var liAllDay = $('<option>').html('--:--');
     list.append(liAllDay);

     for (var hour = 1; hour < 12; hour++) {
         var label = (hour >= 10) ? hour : '0' + hour;

         var liHour = $('<option>').html(label + ':00 ' + getResource("am")).attr('value', label + ':00 ' + "am");
         var liHalf = $('<option>').html(label + ':30 ' + getResource("am")).attr('value', label + ':30 ' + "am");
         list.append(liHour, liHalf);
     }

     var liHour = $('<option>').html('12:00 ' + getResource("pm")).attr('value', '12:00 ' + "pm");
     var liHalf = $('<option>').html('12:30 ' + getResource("pm")).attr('value', '12:30 ' + "pm");
     list.append(liHour, liHalf);

     for (var hour = 1; hour < 12; hour++) {
         var label = (hour >= 10) ? hour : '0' + hour;
         var liHour = $('<option>').html(label + ':00 ' + getResource("pm")).attr('value', label + ':00 ' + "pm");
         var liHalf = $('<option>').html(label + ':30 ' + getResource("pm")).attr('value', label + ':30 ' + "pm");
         list.append(liHour, liHalf);
     }

     list.val('--:--').attr('selected');
 }

 function formatAMPM(date) {
     var hours = date.getHours();
     var minutes = date.getMinutes();
     var ampm = hours >= 12 ? 'pm' : 'am';
     hours = hours % 12;
     hours = hours ? ((hours < 10) ? '0' + hours : hours) : 12;  // the hour '0' should be '12'
     minutes = minutes < 10 ? '0' + minutes : minutes;
     var strTime = hours + ':' + minutes + ' ' + ampm;
     return strTime;
 }