﻿var TeamItemTemplate;
var TeamId = -1;
var ItemIdentity = 1;
var TeamsList = new Array();
var validated = true;
var edit_mood = false;
var IsExists = false;
var LastUpdatedDate = null;
var IsNotEmptyTeam = false;

//////by Amer 19-2-2014
var lstAllUsers, lstTeamUsers;
var btn_return, btn_move, btn_moveAll, btn_return_all;
var TeamUsersIds = "";
var RestTeamUsersIds = "";
var AllUsersIds = "";
var UserIdsToSave = "";
//var permissionIds = "", hdnPermissionIds;
var UserTeamId = -1;
//var PermissionLists = "";
/////

function TeamItem() {
    this.itemIdentity = ++ItemIdentity;
    this.TeamId = -1;
    this.Name = '';
    this.NameAr = '';
    this.Description = '';
    this.ManagerId = -1;
    this.CreatedBy = '';
    this.CreatedDate = '';
    this.UpdatedBy = ''
}


function InnerPage_Load() {
    setSelectedMenuItem('.setup');
    $('.search-box').hide();
    //$('.Add-new-form-team').height(500);
    currentEntity = systemEntities.TeamEntityId;

    $('.TabbedPanelsContent').height($('.white-container').height()-8);
    $('.Add-new-form-team').height($('.TabbedPanelsContent').height() - 57);
    checkPermissions(this, systemEntities.TeamEntityId, -1, $('.Action-div'), function (template) {
        LoadViewDefualtOption();
    });
    setPageTitle(getResource("TeamsList"));


    GetTemaplte(systemConfig.ApplicationURL + systemConfig.HTMLTemplateURLs.TeamItemTemplate, function () {
        ListTeams();
    }, 'TeamItemTemplate');


    $('.add-new-link').click(function () { InsertNewRow(); });
    $('.delete-lnk').click(function () { DeleteTeam(); });
    $('.ActionEdit-link').live('click', function () {
        cancleUsersDiv();
        if (IsNotEmptyTeam) {
            showLoading($('.TabbedPanelsContent'));
            lockEntity(systemEntities.TeamEntityId, -1, function () {
                LoadDefultOptions(); edit_mood = true;
            }, LastUpdatedDate, function () {
                ListTeams();
            });
        }
        else { LoadDefultOptions(); }
    });


    $('.AddNew-tab-lnk').live('click', function () {
        cancleUsersDiv();
        if (IsNotEmptyTeam) {
            showLoading($('.TabbedPanelsContent'));
            lockEntity(systemEntities.TeamEntityId, -1, function () {
                LoadDefultOptions(); edit_mood = true;
            }, LastUpdatedDate, function () {
                ListTeams();
            });
        }
        else { LoadDefultOptions(); }

        InsertNewRow();
    });


    $('.ActionSave-link').live('click', function () { SaveEntity($('#content')); });
    $('.ActionCancel-link').live('click', function () {
        showLoading($('.TabbedPanelsContent'));
        UnlockEntity(systemEntities.TeamEntityId, -1, function () {
            LoadViewDefualtOption();
            ListTeams();
            edit_mood = false;
        });
    });

    $('.Teams-info').find('.Name').live('blur', function (e) {
        var Name = $(this).val();
        //$(this).text() = $(this).val().trim();
        // $(this).val(OwnerName);
        //            var index = $(this).parents('tr').index() - 2;
        var index = $(this).parents('tr').index();
        checkTeamNameExistence(Name.trim(), index, $(this));
    });
    $('.Teams-info').find('.cx-control').live('keyup', function (e) {
        if (e.keyCode == 45 || e.which == 45) { InsertNewRow(); }
        if (e.keyCode == 46 || e.which == 46) { DeleteTeam(); }
    });

    $('.ContentRow').live('click', function () {
        var TR = this;
        $('.ContentRow').removeClass('selected');
        $(TR).addClass('selected');

        if ($(TR).attr('id') != -2) {
            if (edit_mood) $('.delete-lnk').show();
        }
        else $('.delete-lnk').hide();
    });

    //    deletedEntityCallBack = function () {
    //        removeDeletedEntity();
    //    };

    //    updatedEntityCallBack = function () {
    //        getEntityData(selectedEntityId);
    //    };

    getTeamUsersInit();

}

//       if (e.keyCode == 46 || e.which == 46) {
//            showConfirmMsg('Delete', 'Are you Sure you want to delete this Team ?', function () {
//                var TRtoDelete = TR;
//                var id = eval($(TRtoDelete).parent().parent().attr('id'));
//                if ($(TRtoDelete).parent().parent().hasClass('last-row')) {
//                    $(TRtoDelete).parent().parent().remove();
//                    $('.Teams-info tr:last').addClass('last-row');
//                }
//                else {
//                    $(TRtoDelete).parent().parent().remove();
//                    RenumberRows();
//                }
//                ItemIdentity = ItemIdentity - 1;
//                $('.delete-lnk').hide();
//            });
//       }



function LoadDefultOptions() {
    $('.viewedit-display-none').show();
    $('.viewedit-display-block').hide();
    $('.delete-lnk').hide();
}
function LoadViewDefualtOption() {
    $('.Action-div').show();
    $('.viewedit-display-none').hide();
    $('.viewedit-display-block').show();

}

function RenumberRows() {
    ItemIdentity = 1;
    $.each($('.Teams-info tr'), function (index, item) {
        if ($(this).hasClass('ContentRow')) {
            $(this).find('.ItemIdentity').text(ItemIdentity);
            $(this).removeClass();
            $(this).addClass('ContentRow');
            $(this).addClass('Item' + ItemIdentity);
            ItemIdentity = ItemIdentity + 1;
        }
    });

    ItemIdentity = ItemIdentity - 1;
    $('.Teams-info tr:last').addClass('last-row');
}


function ListTeams() {
    $('.Teams-info').find("tr:.ContentRow").remove();
    ItemIdentity = 1;
    // showLoading($('.TabbedPanelsContent'));
    showLoading($('#content'));
    var data = formatString('{keyword:"{0}",page:{1},resultCount:{2},argsCriteria:"{3}"}', '', 1, 100, '');

    $.ajax({
        type: "POST",
        url: systemConfig.ApplicationURL_Common + systemConfig.TeamWebServiceURL + "FindAllLite",
        contentType: "application/json; charset=utf-8",
        data: data,
        dataType: "json",
        success: function (data) {
            var TeamsList = eval("(" + data.d + ")");
            var template = $(TeamItemTemplate).clone();
            if (TeamsList.result != null) {
                LastUpdatedDate = TeamsList.result[0].UpdatedDate;
                IsNotEmptyTeam = true;
                $.each(TeamsList.result, function (index, item) {
                    FillTeamData(item);
                    ItemIdentity = ItemIdentity + 1;
                });
                ItemIdentity = ItemIdentity - 1;
            }
            else { IsNotEmptyTeam = false; }
            $('.Teams-info tr:last').addClass('last-row');
            edit_mood = false;
            hideLoading();
        },
        error: function (e) {
        }
    });
}

function FillTeamData(Item) {
    if (Item != null) {
        var template = $(TeamItemTemplate).clone();
        var ManagerAC = template.find('select:.Manager').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.UsersServiceURL + "FindUserAllLite", required: false });
        var acs = [{ controlName: 'ManagerAC', control: ManagerAC}];
        template = mapEntityInfoToControls(Item, template, acs);
        template.attr('id', Item.Id);
        template.attr('UsersCount', Item.UsersCount);
        template.addClass('Item' + ItemIdentity);
        template.attr('validate', true);
        template.find('input:.Name').addClass('Item' + ItemIdentity);
        template.find('input:.NameAr').addClass('Item' + ItemIdentity);
        template.find('textArea:.Discription').addClass('Item' + ItemIdentity);
        template.find('.ItemIdentity').text(ItemIdentity);
        template.find('.cx-control').keyup(function () { ValidateItem(template, Item.Id); });
        template.find('.CreatedDate').text(getDate('dd-mm-yy', formatDate(Item.CreatedDate)));
        template.find('.viewedit-display-none').hide();
        template.find('.viewedit-display-block').show();

        template.find('.ShowAllUser-link').click(function () {
            UserTeamId = Item.Id;
            showTeamUser(Item.Id);
        });
        $('.Teams-info').append(template);

    }

}



function ValidateItem(template, Id) {
    template.find('.validation').remove();
    if (template.find('input:.Name').val() == "") {
       // addPopupMessage(template.find('input:.Name'), getResource("required"));
        //&& template.find('input:.NameAr').val() == ""
        // addPopupMessage(template.find('input:.NameAr'),'required');
    }
    else {

        template.attr('validate', true);
       // template.find('.validation').remove();
       // template.find('input:.Name').removeClass('cx-control-red-border');
    }

}


function InsertNewRow() {
    if ($('.last-row').attr('id') != -2 || IsNotEmptyTeam) {
        if (!$('.Teams-info').find('.last-row').find('input:.Name').val() == "") { //|| IsNotEmptyTeam
            //&& $('.Teams-info').find('.last-row').find('.NameAr').val() == "")
            ItemIdentity = ItemIdentity + 1;
            var template = $(TeamItemTemplate).clone();
            $('.Teams-info').find('tr').removeClass('last-row');
            template.addClass('last-row');
            template.attr('id', -1);
            template.attr('validate', false);
            template.find('.cx-control').keyup(function () { ValidateItem(template, -1); });
            template.find('.ItemIdentity').text(ItemIdentity);
            $('.Teams-info').append(template);
            template.find('input:.Name').select();
            template.find('.ShowAllUser-link').hide();
            template.find('select:.Manager').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.UsersServiceURL + "FindUserAllLite", required: false });
            $('.ContentRow').removeClass('selected');
            $(template).addClass('selected');
            $('.delete-lnk').show();



        }
        else {
            $('.Teams-info').find('.last-row').addClass('selected');
            $('.Teams-info').find('.last-row').find('input:.Name').select();

        }
    }
    else {
        if (ItemIdentity == 0)
            ItemIdentity = ItemIdentity + 1;
        var template = $(TeamItemTemplate);
        $('.Teams-info').find('tr').removeClass('last-row');
        template.addClass('last-row');
        template.attr('id', -1);
        template.attr('validate', false);
        template.find('.cx-control').blur(function () { ValidateItem(template, -1); });
        template.find('.ItemIdentity').text(ItemIdentity);
        template.find('select:.Manager').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.UsersServiceURL + "FindUserAllLite", required: false });
        $('.Teams-info').append(template);
        template.find('input:.Name').select();
    }
}



function DeleteTeam() {


    if ($('.selected').attr('UsersCount') > 0) {
        var msg = getResource("prefxDeleteTeamMsg") + $('.selected').attr('UsersCount') + getResource("suffixDeleteTeamMsg");
        showOkayMsg(getResource("Delete"), msg);
    }
    else {
        showConfirmMsg(getResource("Delete"), getResource("AreyousureTeam"), function () {
            $('.selected').remove();
            RenumberRows();
            $('.Teams-info tr:last').addClass('last-row selected');
        });
    }

    //        showConfirmMsg('Delete', 'Are you Sure you want to delete this Team ?', function () {
    //            if ($(TRtoDelete).hasClass('last-row')) {
    //                $(TRtoDelete).remove();
    //                $('.Teams-info tr:last').addClass('last-row');
    //            }
    //            else {
    //                $(TRtoDelete).remove();
    //                RenumberRows();
    //            }
    //            ItemIdentity = ItemIdentity - 1;
    //            $('.delete-lnk').hide();
    //        });


}

function getItems() {
    validated = true;
    TeamsList = new Array();
    if ($('.Teams-info').find('.last-row').find('input:.Name').val() == "" && $('.Teams-info').find('.last-row').find('textArea:.Description').val() == "") {
        //&& $('.Teams-info').find('.last-row').find('input:.NameAr').val() == ""
        $('.last-row').remove();
    }

    $.each($('.Teams-info tr'), function (index, item) {
        if ($(this).attr('id') != "-2") {
            if (eval($(this).attr('validate'))) {
                var newItem = new TeamItem();
                newItem.TeamId = $(this).attr('Id');
                newItem.Name = $(this).find('input:.Name').val();
                newItem.NameAr = $(this).find('input:.NameAr').val();
                newItem.Description = $(this).find('textArea:.Description').val();
                newItem.ManagerId = $(this).find('.hdnManager').val();
                if ($(this).attr('id') == -1)
                    newItem.CreatedBy = UserId;
                else
                    newItem.UpdatedBy = UserId;
                if (TeamsList.length == 0)
                { TeamsList.push(newItem); }
                //  validated = true;
                else {
                    $.each(TeamsList, function (index, item) {
                        if (item.Name.trim() == newItem.Name.trim()) {
                            validated = false;
                            return false;
                        }
                    });
                    if (validated)
                        TeamsList.push(newItem);
                    else {
                        addPopupMessage($(this).find('input:.Name'), 'already exists');
                        return false;
                    }
                }

            }
            else { validated = false; }
        }
    });


    if (!validated)
        return false;
    else validated = true;
}


function SaveEntity() {
    if (!saveForm($('.Teams-info'))) {
        return false;
    }
    getItems();
    if (!validated)
        return false;
    //return false;
    if (!saveForm($('.Teams-info'))) {
        return false;
    }
    //----------------
    var doublicate = false;
    var lastUpdatedDate = null;

    $.each(TeamsList, function (index, item) {
        $.each($('.Teams-info tr'), function (Index, Item) {
            if (!doublicate) {
                if ($(this).find('input:.Name').val()) {
                    if (item.Name.toLowerCase() == $(this).find('input:.Name').val().trim().toLowerCase() && item.TeamId != $(this).attr('id')) {
                        // addPopupMessage($(this).parent().find('input:.Name'), 'already exists');

                        addPopupMessage($(this).find('input:.Name'), 'already exists');

                        doublicate = true;
                        return;
                    }
                }
            }
        });

    });

    ///-------------------
    if (doublicate)
        return;
    //debugger;
    var lastUpdatedDate = getEntityLockDateFormat(LastUpdatedDate);
    showLoading($('.TabbedPanelsContent'));
    var data = '{UserId:{0}, TeamsList:"{1}"}';
    data = formatString(data, UserId, escape(JSON.stringify(TeamsList)));

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: systemConfig.ApplicationURL + systemConfig.TeamWebServiceURL + "SaveTeam",
        headers: getEntitylockAjaxHeaders(lastUpdatedDate),
        data: data,
        datatype: "json",
        success: function (data) {
            hideLoading();
            showStatusMsg(getResource("savedsuccessfully"));
            ListTeams();
            $('.delete-lnk').hide();
            $('.viewedit-display-none').hide();
            $('.viewedit-display-block').show();
        },
        error: function (ex) {
            handleLockEntityError(ex, null, function () {
                ListTeams();
                $('.ActionSave-link').hide();
                $('.add-new-link').hide();
                $('.ActionCancel-link').hide();
                $('.ActionDelete-link').hide();
                $('.ActionEdit-link').show();
            });
        }
    });

}



//    function checkOwnerNameExistence(teamName, index, input) {

//        //var IsExists = false;
//        $.each(TeamsList, function (teamIndex, team) {
//            if (ownerIndex != index) {
//                if (owner.teamName == teamName) {
//                    IsExists = true;
//                    isNotDouplicate = false;
//                   // TeamsList[teamIndex].IsExists = true;
//                    addPopupMessage(input, 'already exist');
//                    oldName = teamName;
//                    return false;
//                }
//            }
//        });

//    }


function checkTeamNameExistence(TeamName, index, input) {
    //        getItems();
    //      
    //        $.each(TeamsList, function (teamIndex, team) {
    //            if (teamIndex != index) {
    //                if (team.Name == TeamName) {
    //                    IsExists = true;
    //                    addPopupMessage(input, 'already exist');
    //                    return false;
    //                }
    //            }
    //        });
}



///////////////////////by Amer  19-2-2014

function FillAllUsers(AllUsers) {
    $('.lstAllUsers').find('option').remove().end();
    $('.lstTeamUsers').find('option').remove().end();
    TeamUsersIds = "";
    RestTeamUsersIds = "";
    $.each(AllUsers, function (index, item) {
        if (item.HasTeam) {
            var Option = ' <option value="' + item.value + '">' + item.label + '</option>';
            $('.lstTeamUsers').append(Option);
            TeamUsersIds = TeamUsersIds + ',' + item.UserId;
        }
        else {
            var Option = ' <option value="' + item.value + '">' + item.label + '</option>';
            $('.lstAllUsers').append(Option);
            RestTeamUsersIds = RestTeamUsersIds + ',' + item.UserId;
        }
    });
    UserIdsToSave = TeamUsersIds;
    AllUsersIds = TeamUsersIds + RestTeamUsersIds;
}

function showTeamUser(TeamId) {
//    $('.TabbedPanelsContent').css('opacity', '0.4');
//    $('.TeamUsers-div').css('opacity', '1');
    $('.TeamUsers-div').show();
    teamUserViewMood();
    getRoleUsers(TeamId);
}

function getRoleUsers(teamId) {
    var lastUpdatedDate = getEntityLockDateFormat(LastUpdatedDate);
    var data = '{teamId:{0}}';
    data = formatString(data, teamId);
    showLoading($('.TabbedPanelsContent'));
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: systemConfig.ApplicationURL + systemConfig.TeamWebServiceURL + "FindTeamUsersLite",
        headers: getEntitylockAjaxHeaders(lastUpdatedDate),
        data: data,
        datatype: "json",
        success: function (data) {
            var TeamsList = eval("(" + data.d + ")");
            if (TeamsList.result != null)
                FillAllUsers(TeamsList.result);
            hideLoading();
        },
        error: function (ex) {
            handleLockEntityError(ex, null, function () {
            });
        }
    });
}

function MovePermission(IsAdd) {

    if (IsAdd) {
        var selectedOption = $('.lstAllUsers').find('option:selected');
        if (selectedOption.html() != null) {
            var Option = '<option value="' + selectedOption.attr('value') + '" index="' + selectedOption.index() + '">' + selectedOption.html() + '</option>';
            $('.lstAllUsers').find('option:selected').next().attr('selected', 'selected');
            $('.lstTeamUsers').append(Option);
            selectedOption.remove();
            UserIdsToSave += "," + selectedOption.attr('value');
        }
    }
    else {
        var selectedOption = $('.lstTeamUsers').find('option:selected');
        if (selectedOption.html() != null) {
            var Option = '<option value="' + selectedOption.attr('value') + '">' + selectedOption.html() + '</option>';
            $('.lstAllUsers').append(Option);
            $('.lstTeamUsers').find('option:selected').next().attr('selected', 'selected');
            selectedOption.remove();
            UserIdsToSave = UserIdsToSave.replace(selectedOption.attr('value'), "").replace(",,", ",");
        }
    }
}

function MoveAllPermissions(IsAdd) {
    if (IsAdd) {
        UserIdsToSave = "";
        $('.lstAllUsers').find('option').each(function () {
            var Option = '<option value="' + $(this).attr('value') + '">' + $(this).html() + '</option>';
            $('.lstTeamUsers').append(Option);
            $(this).remove();
            UserIdsToSave = AllUsersIds;
        });
    }
    else {
        $('.lstTeamUsers').find('option').each(function () {
            var Option = '<option value="' + $(this).attr('value') + '">' + $(this).html() + '</option>';
            $('.lstAllUsers').append(Option);
            $(this).remove();
        });
        UserIdsToSave = "";
    }
}


function saveTeamUsers(teamId, UserIds) {

    var lastUpdatedDate = getEntityLockDateFormat(LastUpdatedDate);
    var data = '{teamId:{0},teamUserIds:"{1}"}';
    data = formatString(data, teamId, UserIds);
    showLoading($('.TabbedPanelsContent'));
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: systemConfig.ApplicationURL + systemConfig.TeamWebServiceURL + "SaveUsersTeam",
        headers: getEntitylockAjaxHeaders(lastUpdatedDate),
        data: data,
        datatype: "json",
        success: function (data) {
            //            var RolesList = eval("(" + data.d + ")");
            //            if (RolesList.result != null)
            //                FillAllUsers(RolesList.result);
            hideLoading();
            teamUserViewMood();
            $('.TeamUsers-div').hide();
            //xxxxxx
            ListTeams();

        },
        error: function (ex) {
            teamUserViewMood();
            handleLockEntityError(ex, null, function () {
            });
        }
    });
}

function getTeamUsersInit() {
    document.onkeydown = function (e) {
        if (e.keyCode == 27 || e.which == 46) {
            teamUserViewMood();
            $('.TeamUsers-div').hide();
        }
    };
    
    lstAllUsers = $(lstAllUsers);
    lstTeamUsers = $(lstTeamUsers);
    btn_move = $('.btn_move');
    btn_return = $('.btn_return');
    btn_moveAll = $('.btn_moveAll');
    btn_return_all = $('.btn_return_all');
    btn_save = $('.save_TeamUsers');
    btn_cancle = $('.cancle_TeamUsers');
    btn_edit = $('.edit-tab-lnk');

//    $(".TeamUsers-div").click(function (event) {
//           $(".TeamUsers-div").animate({right: '+=50', bottom: '+=50'}, 1000);​​​​​​​​​​​​​​​ 
//    });

   // $('.TeamUsers-div').droppable({ option: 22 });

    $('.edit-mode-controle').hide();
    btn_edit.click(function () {
        editTeamUsers();
    });

    btn_move.click(function () {
        MovePermission(true);
    });
    btn_return.click(function () {

        MovePermission(false);
    });
    btn_moveAll.click(function () {
        MoveAllPermissions(true);
    });
    btn_return_all.click(function () {
        MoveAllPermissions(false);
    });
    btn_save.click(function () {
        saveTeamUsers(UserTeamId, UserIdsToSave);
    });
    btn_cancle.click(function () {
        cancleUsersDiv();
    });
}


function editTeamUsers() {

    $('.lstAllUsers').prop('disabled', false);
    $('.lstTeamUsers').prop('disabled', false);
    $('.view-mode-controle').hide();
    $('.edit-mode-controle').show();

}
function teamUserViewMood() {
    $('.lstAllUsers').prop('disabled', true);
    $('.lstTeamUsers').prop('disabled', true);
    $('.view-mode-controle').show();
    $('.edit-mode-controle').hide();
}
function cancleUsersDiv() {
    $('.TeamUsers-div').hide();
    teamUserViewMood();
}

/////////////////////////////////////////