using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CentrixERP.Common.Web;
using CentrixERP.Configuration;

namespace Centrix.UM.Web.NewUserManagement.Teams
{
    public partial class TeamsList :  BasePageLoggedIn
    {
        public TeamsList()
            : base(Enums_S3.Permissions.SystemModules.ViewTeam) { }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}