﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LandingPage.aspx.cs" Inherits="CentrixERP.Web.LandingPage"
    MasterPageFile="Common/MasterPage/ERPSite.Master" %>

<asp:Content ContentPlaceHolderID="PageHeader" ID="PageHeader" runat="server">
    <link href="Common/CSS/LandingPage.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ContentPlaceHolderID="PageBody" ID="PageBody" runat="server">
    <div class="LandingPageBody">
        <div id="pageContainer">
            <fieldset id="Modules_fieldset">
                <legend>Centrix Modules</legend>
                <div class="ModulesBox">
                    <div class="greenCircle tooltip">
                        <span>
                            <img class="callout" src="Common/Images/LandingPage/tooltip_tung.png" />
                            <strong>Administration Services</strong><br />
                            Centrix Administration Services will help you manage ,organize & control your users.</span>
                             <a href="Common/Settings/Setup.aspx" >
                        <img src="Common/Images/LandingPage/Administration.png" width="128" height="128"
                            alt="" class="greenCircleimg"/></a> 
                            </div>
                    <a href="Common/Settings/Setup.aspx" class="titles">Administration Services</a>
                </div>
                <div class="ModulesBox">
                    <div class="greenCircle tooltip">
                        <span>
                            <img class="callout" src="Common/Images/LandingPage/tooltip_tung.png" />
                            <strong>General Ledger</strong><br />
                            Centrix General Ledger will help you manage ,organize & control your [accounts &
                            accounting entires. </span>
                        <a href="Common/Settings/ERPSetup.aspx?glsetup=true"><img src="Common/Images/LandingPage/General_Ledger.png" width="128" height="128"
                            alt="" class="greenCircleimg" /></a></div>
                    <a href="Common/Settings/ERPSetup.aspx?glsetup=true" class="titles">General Ledger</a>
                </div>
                <div class="ModulesBox">
                    <div class="greenCircle tooltip">
                        <span>
                            <img class="callout" src="Common/Images/LandingPage/tooltip_tung.png" />
                            <strong>Common Services </strong>
                            <br />
                            Centrix Common Services will help you manage ,organize & control your Tax Year &
                            currencies </span>
                         <a href="Common/Settings/ERPSetup.aspx"><img src="Common/Images/LandingPage/Common_Services.png" width="128" height="128"
                            alt="" class="greenCircleimg" /></a></div>
                    <a href="Common/Settings/ERPSetup.aspx" class="titles">Common Services </a>
                </div>
                <div class="ModulesBox">
                    <div class="greenCircle tooltip">
                        <span>
                            <img class="callout" src="Common/Images/LandingPage/tooltip_tung.png" />
                            <strong>Accounts Receivable</strong><br />
                            Centrix Accounts Receivable will help you manage ,organize & control your customers
                            & customer transactions. </span>
                       <a href="AccountsReceivable/Setup/ARSetup.aspx" ><img src="Common/Images/LandingPage/Receivable.png" width="128" height="128" alt=""
                            class="greenCircleimg" /></a></div>
                    <a href="AccountsReceivable/Setup/ARSetup.aspx" class="titles">Accounts Receivable
                    </a>
                </div>
                <div class="ModulesBox">
                    <div class="greenCircle tooltip">
                        <span>
                            <img class="callout" src="Common/Images/LandingPage/tooltip_tung.png" />
                            <strong>Accounts Payable </strong>
                            <br />
                            Centrix Accounts Payable will help you manage ,organize & control your vendors &
                            vendor transactions. </span>
                       <a href="AccountsPayable/Setup/APSetup.aspx"><img src="Common/Images/LandingPage/Payable.png" width="128" height="128" alt=""
                            class="greenCircleimg" /></a></div>
                    <a href="AccountsPayable/Setup/APSetup.aspx" class="titles">Accounts Payable </a>
                </div>
                <div class="ModulesBox">
                    <div class="greenCircle tooltip">
                        <span>
                            <img class="callout" src="Common/Images/LandingPage/tooltip_tung.png" />
                            <strong>Point of Sale</strong><br />
                            Centrix Point Of Sales will help you manage ,organize & control your invoices &
                            items via barocode. </span>
                       <a href="POS/POSSetup/POSSetup.aspx" > <img src="Common/Images/LandingPage/pos.png" width="128" height="128" alt="" class="greenCircleimg" /></a></div>
                    <a href="POS/POSSetup/POSSetup.aspx" class="titles">Point of Sale </a>
                </div>
                <div class="ModulesBox">
                    <div class="greenCircle tooltip">
                        <span>
                            <img class="callout" src="Common/Images/LandingPage/tooltip_tung.png" />
                            <strong>Inventory Control</strong><br />
                            Centrix Inventory Control will help you manage ,organize & control your items &
                            item transactions. </span>
                       <a href="Inventory/InventorySetup/InventorySetup.aspx"><img src="Common/Images/LandingPage/Inventory.png" width="128" height="128" alt=""
                            class="greenCircleimg" /></a></div>
                    <a href="Inventory/InventorySetup/InventorySetup.aspx" class="titles">Inventory Control</a>
                </div>
                <div class="ModulesBox">
                    <div class="greenCircle tooltip">
                        <span>
                            <img class="callout" src="Common/Images/LandingPage/tooltip_tung.png" />
                            <strong>Purchase Order</strong><br />
                            Centirx Purchase Order will help you manage,oganize & control your PO's , Receipts & Invoice Transactions
                        </span>
                         <a href="PO/POSetup/POSetup.aspx"><img src="Common/Images/LandingPage/Purchase.png" width="128" height="128" alt="" class="greenCircleimg" /></a></div>
                    <a href="PO/POSetup/POSetup.aspx" class="titles">Purchase Order</a>
                </div>
                 <div class="ModulesBox">
                    <div class="greenCircle tooltip">
                        <span>
                            <img class="callout" src="Common/Images/LandingPage/tooltip_tung.png" />
                            <strong>Order Entry</strong><br />
                            Centirx Order Entry will help you manage,oganize & control your OE's , Shipments & Invoices Transactions
                        </span>
                         <a href="OE/OESetup/OESetup.aspx"><img src="Common/Images/LandingPage/Purchase.png" width="128" height="128" alt="" class="greenCircleimg" /></a></div>
                    <a href="OE/OESetup/OESetup.aspx" class="titles">Order Entry</a>
                </div>
                <%--  <div class="ModulesBox">
                    <div class="greenCircle tooltip">
                        <span>
                            <img class="callout" src="Common/Images/LandingPage/tooltip_tung.png" />
                            <strong>Order Entry</strong><br />
                        </span>
                        <a href="#"> <img src="Common/Images/LandingPage/OrderEntry.png" width="128" height="128" alt="" class="greenCircleimg" /></a></div>
                    <a href="#" class="titles">Order Entry</a>
                </div>
               <div class="ModulesBox">
                    <div class="greenCircle tooltip">
                        <span>
                            <img class="callout" src="Common/Images/LandingPage/tooltip_tung.png" />
                            <strong>Mobile Salesman</strong><br />
                            Centrix Mobile Salesman allows companies to manage their sales personnel on the
                            go. It assists employees by invoicing, tracking, inventory and cross-selling.</span>
                          <a href="#">  <img src="Common/Images/LandingPage/salesman.png" width="128" height="128" alt="" class="greenCircleimg" /></a></div>
                    <a href="#" class="titles">Mobile Salesman</a>
                </div>--%>
            </fieldset>
        </div>
    </div>
</asp:Content>
<%--<asp:Content ContentPlaceHolderID="PageBody" ID="PageBody" runat="server">
  <body style="">
    <div id="mainContainer">
        <div class="label-1">
        <a href="Common/Settings/Setup.aspx">
            <img src="Common/Images/Administration.png" alt="" width="80" height="60" class="label_img" />
          
            <div class="label_txt">
                Administration
            </div>
              </a>
        </div>
        <div class="label-4">
        <a href="AccountsPayable/Setup/APSetup.aspx">
            <img src="Common/Images//Accounts Payable.png" alt="" width="80" height="60" class="label_img" />
           
            <div class="label_txt">
                Accounts Payable
            </div>
             </a>
        </div>
        
        <div class="label-2">
         <a href="Common/Settings/ERPSetup.aspx?glsetup=true">
            <img src="Common/Images//General Ledger.png" alt="" width="80" height="60" class="label_img" />
            <div class="label_txt">
                General Ledger
            </div>
            </a>
        </div>
        <div class="label-5">
         <a href="AccountsReceivable/Setup/ARSetup.aspx">
            <img src="Common/Images//Accounts Receivable.png" alt="" width="80" height="60" class="label_img" />
            <div class="label_txt">
                Accounts Receivable
            </div>
            </a>
        </div>
        <div class="label-6">
        <a href="Common/Settings/ERPSetup.aspx">
            <img src="Common/Images//Common Services.png" alt="" width="80" height="60" class="label_img" />
            <div class="label_txt">
                Common Services
            </div>
            </a>
        </div>
        <div class="label-7">
        <a href="Inventory/InventorySetup/InventorySetup.aspx">
            <img src="Common/Images//Inventory Control.png" alt="" width="80" height="60" class="label_img" />
            <div class="label_txt">
                Inventory Control
            </div>
            </a>
        </div>
        <div class="label-8">
        <a href="POS/POSSetup/POSSetup.aspx">
            <img src="Common/Images//46-128.png" alt="" width="80" height="60" class="label_img" />
            <div class="label_txt">
               Point Of Sale
            </div>
            </a>
        </div>
        
        <fieldset class="box01">
            <legend>Administration  Services  </legend>
            <ul>
                <li> Centrix Administration Services will help you  manage ,organize & control your users .</li>
            </ul>
            
        </fieldset>
        <fieldset class="box02">
            <legend>Common Services </legend>
            <ul>
            <li> Centrix Common Services will help you  manage ,organize & control your Tax ,fiscal Year & currencies  .</li>
             
            </ul>
        </fieldset>
       
        <fieldset class="box05">
            <legend>General Ledger </legend>
         
            <ul>
               <li>
               Centrix General Ledger will help you  manage ,organize & control your accounts & accounting entires .
               </li>
            </ul>
        </fieldset>
        <fieldset class="box04">
            <legend>Accounts Payable </legend>
            <ul>
                <li> Centrix Accounts Payable will help you  manage ,organize & control your vendors & vendor transactions .  </li>
              
            </ul>
        </fieldset>
        <fieldset class="box06">
            <legend>Inventory Control  </legend>
            <ul>
                  <li> Centrix Inventory Control will help you  manage ,organize & control your items & item transactions .  </li>
            </ul>
        </fieldset>
        <fieldset class="box07">
            <legend>Accounts Receivable  </legend>
            <ul>
                <li> Centrix Accounts Receivable will help you  manage ,organize & control your customers & customer transactions .  </li>
            </ul>
        </fieldset>

         <fieldset class="box08">
            <legend>Point Of Sale </legend>
            <ul>
                <li> Centrix Point Of Sales will help you  manage ,organize & control your invoices & items via barocode .  </li>
            </ul>
        </fieldset>
    </div>
</asp:Content>--%>