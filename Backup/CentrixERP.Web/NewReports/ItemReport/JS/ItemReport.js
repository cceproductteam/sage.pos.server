﻿var InvoiceReportSchema;
var ExportPageURL;
var FromLocationAC;
var ToLocationAC;
var FromCustomerAC;
var ToCustomerAC;
var RegisterAC, CustomerAC, PersonAC;

function InnerPage_Load() {
    initControls();
    setPageTitle(getResource("Dailysales"));
    InvoiceReportSchema = systemConfig.ReportSchema.ItemReport;
    setSelectedMenuItem('.pos-icon');
}

function initControls() {
   
    $('.lnkReset').click(resetForm);
    $('.btnSearch').click(BuildSearchCriteria);
    $('.Quick-Search-Controls').live('keydown', function (e) {
        if (e.keyCode == 13 || e.which == 13) {
            $('.btnSearch').click();
        }
    });

    $('.btnSearch').attr('value', getResource("Search"));
    $('.lnkReset').attr('value', getResource("Reset"));
}

function BuildSearchCriteria() {


    if (!saveForm($('.Quick-Search-Controls'))) {
        return false;
    }


    $('.search-criteria').val('');
    var argsCriteria = new Object();
    //argsCriteria.ExportPageURL = ExportPageURL;




    if ($('.ItemNumber').val() != null)
        argsCriteria.item_number = $('.ItemNumber').val();

    if ($('.ItemDescription').val() != null)
        argsCriteria.item_description = $('.ItemDescription').val();

        
    var SearchCriteria = '{';
    for (var key in argsCriteria) {
        if (argsCriteria.hasOwnProperty(key))
            if (argsCriteria[key] != "") {
                SearchCriteria = SearchCriteria + ('"' + key + '":' + '"' + argsCriteria[key] + '",');
            }
    }


    if (SearchCriteria != '{') {
        SearchCriteria = SearchCriteria.slice(0, -1);
    }
    SearchCriteria = SearchCriteria + '}';
    var exportPageURL = ExportPageURL + formatString("schemaID={0}&SearchCriteria={1}", InvoiceReportSchema, SearchCriteria);
    debugger;
    window.open(exportPageURL, '_blank');
}

function resetForm() {

    $('.ItemNumber').val('');
    $('.ItemDescription').val('');
   
}


function getdate(date) {
    var arr = date.split('-');
    return arr[1] + '-' + arr[0] + '-' + arr[2];
}