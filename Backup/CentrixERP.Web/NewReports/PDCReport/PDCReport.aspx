﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PDCReport.aspx.cs" Inherits="CentrixERP.Reports.Web.NewReports.PDCReport.PDCReport"
    MasterPageFile="~/Common/MasterPage/ERPSite.master" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="PageHeader">
    <script src="JS/PDCReport.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <link href="../CSS/ReportCSS.css?v=<%=Centrix_Version %>" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        ExportPageURL = '<%=ExportPageURL %>';
    </script>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="PageBody">
    <input runat="server" class="search-criteria" id="hdnSearchCriteria" type="hidden"
        value="" />
    <div class="">
        <div class="">
            <div class="Details-div">
                <span class="details-div-title" resourcekey="AdvFilterYourResults"></span>
                <div class="Action-div" style="display: none;">
                    <ul>
                    </ul>
                </div>
            </div>
            <table cellspacing="0" cellpadding="0" border="0" class="Data-Table Quick-Search-Controls">
                <tbody>
                    <tr>
                        <th>
                            <span class="label-title" resourcekey="ChequeDateFrom"></span>
                        </th>
                        <td>
                            <input type="text" class="cx-control txt date ChequeDateFrom" />
                        </td>
                        <th>
                            <span class="label-title" resourcekey="ChequeDateTo"></span>
                        </th>
                        <td>
                            <input type="text" class="cx-control txt date ChequeDateTo" />
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <span class="label-title" resourcekey="TransactionDateFrom"></span>
                        </th>
                        <td>
                            <input type="text" class="cx-control txt date TransactionDateFrom" />
                        </td>
                        <th>
                            <span class="label-title" resourcekey="TransactionDateTo"></span>
                        </th>
                        <td>
                            <input type="text" class="cx-control txt date TransactionDateTo" />
                        </td>
                    </tr>
                    <tr>
                        <td class="search-buttons" colspan="6">
                            <div class="filter-reset">
                                <input type="button" class="btnSearch button2" id="btnSearch" value="Search" />
                                <input value="Reset" class="lnkReset button-cancel button2" type="button" />
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</asp:Content>
