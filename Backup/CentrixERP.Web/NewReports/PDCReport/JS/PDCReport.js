﻿var PDCReportSchema;
var ExportPageURL;

function InnerPage_Load() {
    setSelectedMenuItem('.account-receivable');
    initControls();
    setPageTitle(getResource('PDCReport'));
    PDCReportSchema = systemConfig.ReportSchema.PDCReportSchema;
}

function initControls() {

    LoadDatePicker('input:.date');
    $('.lnkReset').click(resetForm);
    $('.btnSearch').click(BuildSearchCriteria);
    $('.Quick-Search-Controls').live('keydown', function (e) {
        if (e.keyCode == 13 || e.which == 13) {
            $('.btnSearch').click();
        }
    });
    $('.btnSearch').attr('value', getResource("Search"));
    $('.lnkReset').attr('value', getResource("Reset"));
}

function BuildSearchCriteria() {


    if (!saveForm($('.Quick-Search-Controls'))) {
        return false;
    }


    $('.search-criteria').val('');
    var argsCriteria = new Object();
    if ($('input.ChequeDateFrom').val() != "")
        argsCriteria.check_date_from = getdate($('input.ChequeDateFrom').val());
    if ($('input.ChequeDateTo').val() != "")
        argsCriteria.check_date_to = getdate($('input.ChequeDateTo').val());
    if ($('input.TransactionDateFrom').val() != "")
        argsCriteria.transaction_date_from = getdate($('input.TransactionDateFrom').val());
    if ($('input.TransactionDateTo').val() != "")
        argsCriteria.transaction_date_to = getdate($('input.TransactionDateTo').val());
  

    var SearchCriteria = '{';
    for (var key in argsCriteria) {
        if (argsCriteria.hasOwnProperty(key))
            if (argsCriteria[key] !== "") {
                SearchCriteria = SearchCriteria + ('"' + key + '":' + '"' + argsCriteria[key] + '",');
            }
    }


    if (SearchCriteria != '{') {
        SearchCriteria = SearchCriteria.slice(0, -1);
    }
    SearchCriteria = SearchCriteria + '}';
    var exportPageURL = ExportPageURL + formatString("schemaID={0}&SearchCriteria={1}", PDCReportSchema, SearchCriteria);
    window.open(exportPageURL, '_blank');
}

function resetForm() {
    $('input.date').val('');
}


function getdate(date) {
    var arr = date.split('-');
    return arr[1] + '-' + arr[0] + '-' + arr[2];
}