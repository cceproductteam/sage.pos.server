
var ExportPageURL;
var VendorAc;

function InnerPage_Load() {
    setSelectedMenuItem('.account-payable');
    initControls();
    setPageTitle(getResource('VendorAgingReport'));
}

function initControls() {

    VendorAc = $('select:.Vendor').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.VendorServiceURL + 'FindAllLite', required: false, multiSelect: true });
    LoadDatePicker('input:.date');
    $('.lnkReset').click(resetForm);
    $('.btnSearch').click(BuildSearchCriteria);
    $('.Quick-Search-Controls').live('keydown', function (e) {
        if (e.keyCode == 13 || e.which == 13) {
            $('.btnSearch').click();
        }
    });

    $('input.integer').autoNumeric({ aPad: false, vMax: '9999' });

    $('input.period3').blur(function () {
        var val = $(this).autoNumericGet();
        $('input.period4').autoNumericSet(val);
    });

    $('.btnSearch').attr('value', getResource("Search"));
    $('.lnkReset').attr('value', getResource("Reset"));
}

function BuildSearchCriteria() {


    if (!saveForm($('.Quick-Search-Controls'))) {
        return false;
    }

    $('.search-criteria').val('');
    var argsCriteria = new Object();
  
    argsCriteria.vendor_ids = VendorAc.get_ItemsIds();
    argsCriteria.age_date = getdate($('input.Asofage').val());
    argsCriteria.cutoff_date = getdate($('input.CutoffDate').val());

    argsCriteria.period1 = Number($('input.period1').autoNumericGet());
    argsCriteria.period2 = Number($('input.period2').autoNumericGet());
    argsCriteria.period3 = Number($('input.period3').autoNumericGet());
    argsCriteria.current = Number($('input.Current').autoNumericGet());

    argsCriteria.include_all_invoices = $('input.allInvoices').prop('checked');

    argsCriteria.show_in_functional_currency = $('.rdb-print-functional-currency').is(':checked');
    if (argsCriteria.period2 <= argsCriteria.period1) {
        addPopupMessage($('input.period2'), formatString(getResource('vldGreaterPeriod'), "1"));
        return false;
    }

    if (argsCriteria.period3 <= argsCriteria.period2) {
        addPopupMessage($('input.period3'), formatString(getResource('vldGreaterPeriod'), "2"));
        return false;
    }


    var SearchCriteria = '{';
    for (var key in argsCriteria) {
        if (argsCriteria.hasOwnProperty(key))
            if (argsCriteria[key] !== "") {
                SearchCriteria = SearchCriteria + ('"' + key + '":' + '"' + argsCriteria[key] + '",');
            }
    }


    if (SearchCriteria != '{') {
        SearchCriteria = SearchCriteria.slice(0, -1);
    }
    SearchCriteria = SearchCriteria + '}';
    var exportPageURL = ExportPageURL + formatString("schemaID={0}&SearchCriteria={1}", systemConfig.ReportSchema.VendorAgingReport, SearchCriteria);
    window.open(exportPageURL, '_blank');
}

function resetForm() {
    $('input.date').val('');
    VendorAc.clear();
}


function getdate(date) {
    var arr = date.split('-');
    return arr[1] + '-' + arr[0] + '-' + arr[2];
}