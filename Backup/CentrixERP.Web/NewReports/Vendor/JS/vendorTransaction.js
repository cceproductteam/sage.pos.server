var ExportPageURL;
var VendorAc;

function InnerPage_Load() {
    setSelectedMenuItem('.account-payable');
    initControls();
    setPageTitle(getResource('VendorTransactionsReport'));
}

function initControls() {

    VendorAc = $('select:.Vendor').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.VendorServiceURL + 'FindAllLite', required: false, multiSelect: true });
    LoadDatePicker('input:.date');
    $('.lnkReset').click(resetForm);
    $('.btnSearch').click(BuildSearchCriteria);
    $('.Quick-Search-Controls').live('keydown', function (e) {
        if (e.keyCode == 13 || e.which == 13) {
            $('.btnSearch').click();
        }
    });

    $('.btnSearch').attr('value', getResource("Search"));
    $('.lnkReset').attr('value', getResource("Reset"));
}

function BuildSearchCriteria() {

    if (!saveForm($('.Quick-Search-Controls'))) {
        return false;
    }

    $('.search-criteria').val('');
    var argsCriteria = new Object();
    if (VendorAc.get_ItemsIds())
        argsCriteria.vendor_ids = VendorAc.get_ItemsIds();

    if ($('input.From').val())
        argsCriteria.from_date = getdate($('input.From').val());
    if ($('input.To').val())
        argsCriteria.to_date = getdate($('input.To').val());

   var transactionType = "";
    $.each($("input[type=checkbox]:checked"), function (index, item) {
        transactionType += $(item).val() + ",";
    });

    if (transactionType != "")
        argsCriteria.transactions_type_ids = transactionType;

    argsCriteria.show_in_functional_currency = $('.rdb-print-functional-currency').is(':checked');

    var SearchCriteria = '{';
    for (var key in argsCriteria) {
        if (argsCriteria.hasOwnProperty(key))
            if (argsCriteria[key] != "") {
                SearchCriteria = SearchCriteria + ('"' + key + '":' + '"' + argsCriteria[key] + '",');
            }
    }
    if (SearchCriteria != '{') {
        SearchCriteria = SearchCriteria.slice(0, -1);
    }
    SearchCriteria = SearchCriteria + '}';
    var exportPageURL = ExportPageURL + formatString("schemaID={0}&SearchCriteria={1}", systemConfig.ReportSchema.VendorTransactionReport, SearchCriteria);
    window.open(exportPageURL, '_blank');
}

function resetForm() {
    $('input.date').val('');
    VendorAc.clear();
}


function getdate(date) {
    var arr = date.split('-');
    return arr[1] + '-' + arr[0] + '-' + arr[2];
}


var TransactionType = new Object();
TransactionType.Invoice = 1;
TransactionType.DebitNote = 2;
TransactionType.CreditNote = 3;
TransactionType.Prepayment = 5;
TransactionType.Payment = 4;
TransactionType.Refund = 6;
TransactionType.Adjustment = 7;
