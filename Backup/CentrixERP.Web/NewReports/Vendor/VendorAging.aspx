<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VendorAging.aspx.cs" Inherits="CentrixERP.Reports.Web.NewReports.Vendor.VendorAging"
    MasterPageFile="~/Common/MasterPage/ERPSite.master" %>

<asp:Content runat="server" ContentPlaceHolderID="PageHeader">
    <script src="JS/VendorAging.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <link href="../CSS/ReportCSS.css?v=<%=Centrix_Version %>" rel="stylesheet" type="text/css" />
    <%--  <link href="CSS/customer-ageing.css?v=<%=Centrix_Version %>" rel="stylesheet" type="text/css" />--%>
    <script type="text/javascript">
        ExportPageURL = '<%=ExportPageURL %>';
    </script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="PageBody">
    <input runat="server" class="search-criteria" id="hdnSearchCriteria" type="hidden"
        value="" />
    <div class="">
        <div class="">
            <div class="Details-div">
                <span class="details-div-title" resourcekey=""></span>
                <div class="Action-div" style="display: none;">
                    <ul>
                    </ul>
                </div>
            </div>
            <table cellspacing="0" cellpadding="0" border="0" class="Data-Table Quick-Search-Controls">
                <tbody>
                    <tr>
                        <th>
                            <span class="label-title top-label" resourcekey="AgeingColumns"></span>
                        </th>
                        <th>
                            <span class="label-title top-label" resourcekey="Current"> </span>
                        </th>
                        <th>
                            <span class="label-title top-label" resourcekey="Period+1"> </span>
                        </th>
                        <th>
                            <span class="label-title top-label" resourcekey="Period+2"> </span>
                        </th>
                        <th>
                            <span class="label-title top-label" resourcekey="Period+3"> </span>
                        </th>
                        <th>
                            <span class="label-title top-label" resourcekey="Over"> </span>
                        </th>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <input type="text" class="cx-control Current required integer txt txt-width" value="0"
                                maxlength="4" /><span class="label-title" resourcekey="days"> </span><br />
                        </td>
                        <td>
                            <input type="text" class="cx-control period1 required integer txt txt-width" value="30"
                                maxlength="4" /><span class="label-title" resourcekey="days"> </span><br />
                        </td>
                        <td>
                            <input type="text" class="cx-control period2 required integer txt txt-width" value="60"
                                maxlength="4" /><span class="label-title" resourcekey="days"> </span><br />
                        </td>
                        <td>
                            <input type="text" class="cx-control period3 required integer txt txt-width" value="90"
                                maxlength="4" /><span class="label-title" resourcekey="days"> </span><br />
                        </td>
                        <td>
                            <input type="text" class="cx-control period4 required integer txt txt-width" value="90"
                                readonly="readonly" maxlength="4" /><span class="label-title" resourcekey="days"> </span><br />
                        </td>
                    </tr>
                    <tr>
                    </tr>
                    <tr>
                        <th>
                            <span class="label-title" resourcekey="Vendors"></span>
                        </th>
                        <td>
                            <div>
                                <select class="lst Vendor">
                                </select>
                            </div>
                        </td>
                        <th>
                            <span class="label-title" resourcekey="Asofage"></span>
                        </th>
                        <td>
                            <input type="text" class="cx-control Asofage required date txt" /><br />
                        </td>
                        <th>
                            <span class="label-title" resourcekey="CutoffDate"></span>
                        </th>
                        <td>
                            <input type="text" class="cx-control CutoffDate required date txt" /><br />
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <span class="label-title" resourcekey="Includeallinvoices"></span>
                        </th>
                        <td>
                            <input type="checkbox" class="cx-control allInvoices" />
                        </td>
                          <th>
                            <span class="label-title" resourcekey="PrintAmountsIn"></span>
                        </th>
                        <td colspan="2">
                            <input type="radio" class="rdb-print-vendor-currency" name="printByCurrency" checked="checked" /><span
                                class="label-data" resourcekey="VendorCurrency"> </span><br />
                            <input type="radio" class="rdb-print-functional-currency" name="printByCurrency" /><span
                                class="label-data" resourcekey="FunctionalCurrency"> </span>
                        </td>
                        </tr><tr>
                        <td class="search-buttons" colspan="4">
                            <div class="filter-reset">
                                <input type="button" class="btnSearch button2" id="btnSearch" value="Search" />
                                <input value="Reset" class="lnkReset button-cancel button2" type="button" />
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</asp:Content>
