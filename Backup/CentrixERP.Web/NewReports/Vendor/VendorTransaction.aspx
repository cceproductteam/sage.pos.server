<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VendorTransaction.aspx.cs"
    Inherits="CentrixERP.Reports.Web.NewReports.Vendor.VendorTransaction" MasterPageFile="~/Common/MasterPage/ERPSite.master" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="PageHeader">
    <script src="JS/vendorTransaction.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <link href="../CSS/ReportCSS.css?v=<%=Centrix_Version %>" rel="stylesheet" type="text/css" />
    <link href="CSS/vendor.css?v=<%=Centrix_Version %>" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        ExportPageURL = '<%=ExportPageURL %>';
    </script>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="PageBody">
    <input runat="server" class="search-criteria" id="hdnSearchCriteria" type="hidden"
        value="" />
    <div class="">
        <div class="">
            <div class="Details-div">
                <span class="details-div-title" resourcekey="AdvFilterYourResults"></span>
                <div class="Action-div" style="display: none;">
                    <ul>
                    </ul>
                </div>
            </div>
            <table cellspacing="0" cellpadding="0" border="0" class="Data-Table Quick-Search-Controls">
                <tbody>
                    <tr>
                        <th>
                            <span class="label-title" resourcekey="VendorNumber"></span>
                        </th>
                        <td>
                            <div>
                                <select class="lst Vendor">
                                </select></div>
                        </td>
                        <th colspan="2">
                            <span class="label-title" resourcekey="FromDocumentDate"></span>
                        </th>
                        <td>
                            <input type="text" class="cx-control From date txt" />
                             <span class="label-title" resourcekey="To"></span>
                               <input type="text" class="cx-control To date txt" /><br>
                        </td>
                   
                    </tr>
                    <tr>
                        <th>
                            <span class="label-title" resourcekey="TransactionType"></span>
                        </th>
                        <td colspan="2">
                            <input type="checkbox" class="invoice-type chk-trans-type" value="1" checked="checked" /><span class="label-data" resourcekey="Invoice"> </span>
                            <input type="checkbox" class="debit-note-type margin-left40 chk-trans-type" value="2" checked="checked" /><span
                                class="label-data" resourcekey="DebitNote"> </span>
                            <br />
                            <input type="checkbox" class="receipt-type chk-trans-type" value="4" checked="checked" />
                            <span class="label-data" resourcekey="Payment"> </span>
                            <input type="checkbox" class="credit-note-type chk-trans-type margin-left40" value="3" checked="checked" /><span
                                class="label-data" resourcekey="CreditNote"> </span>
                            <br />
                            <input type="checkbox" class="chk-trans-type" value="5" checked="checked" />
                            <span class="label-data" resourcekey="Prepayment"> </span>
                        </td>
                        <th>
                            <span class="label-title" resourcekey="PrintAmountsIn"></span>
                        </th>
                        <td colspan="2">
                            <input type="radio" class="rdb-print-vendor-currency" name="printByCurrency" checked="checked" /><span
                                class="label-data" resourcekey="VendorCurrency"> </span><br />
                            <input type="radio" class="rdb-print-functional-currency" name="printByCurrency" /><span
                                class="label-data" resourcekey="FunctionalCurrency"> </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="search-buttons" colspan="6">
                            <div class="filter-reset">
                                <input type="button" class="btnSearch button2" id="btnSearch" value="Search" />
                                <input value="Reset" class="lnkReset button-cancel button2" type="button" />
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</asp:Content>
