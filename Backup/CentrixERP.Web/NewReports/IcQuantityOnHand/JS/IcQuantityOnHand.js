﻿var QuantityOnHandReportSchema;
var ExportPageURL;
var FromLocationAC;
var ToLocationAC;
var FromCustomerAC;
var ToCustomerAC;

function InnerPage_Load() {
    initControls();
    setPageTitle(getResource("IcQuantityOnHandReport"));
    QuantityOnHandReportSchema = systemConfig.ReportSchema.IcQuantityOnHandReport;
    setSelectedMenuItem('.inventory-icon');
}

function initControls() {

    FromLocationAC = $('select:.FromLocation').AutoComplete({ serviceURL: systemConfig.ApplicationURL + systemConfig.IcLocationServiceURL + 'FindAllLite', required: true });
    ToLocationAC = $('select:.ToLocation').AutoComplete({ serviceURL: systemConfig.ApplicationURL + systemConfig.IcLocationServiceURL + 'FindAllLite', required: true });
    FromCustomerAC = $('select:.FromItem').AutoComplete({ serviceURL: systemConfig.ApplicationURL + systemConfig.IcItemCardServiceURL + 'FindAllLite', required: true });
    ToCustomerAC = $('select:.ToItem').AutoComplete({ serviceURL: systemConfig.ApplicationURL + systemConfig.IcItemCardServiceURL + 'FindAllLite', required: true });
    
    $('.lnkReset').click(resetForm);
    $('.btnSearch').click(BuildSearchCriteria);
    $('.Quick-Search-Controls').live('keydown', function (e) {
        if (e.keyCode == 13 || e.which == 13) {
            $('.btnSearch').click();
        }
    });

    $('.btnSearch').attr('value', getResource("Search"));
    $('.lnkReset').attr('value', getResource("Reset"));
}

function BuildSearchCriteria() {


    if (!saveForm($('.Quick-Search-Controls'))) {
        return false;
    }


    $('.search-criteria').val('');
    var argsCriteria = new Object();
    //argsCriteria.ExportPageURL = ExportPageURL;

    if (FromLocationAC.get_Item() != null) {
        argsCriteria.from_location_id = FromLocationAC.get_ItemValue();
        argsCriteria.from_location_name = FromLocationAC.get_Item().label;
    }

    if (ToLocationAC.get_Item() != null) {
        argsCriteria.to_location_id = ToLocationAC.get_ItemValue();
        argsCriteria.to_location_name = ToLocationAC.get_Item().label;
    }
    if (FromCustomerAC.get_Item() != null) {
        argsCriteria.from_item_id = FromCustomerAC.get_ItemValue();
        argsCriteria.from_item_number = FromCustomerAC.get_Item().label;
    }

    if (ToCustomerAC.get_Item() != null) {
        argsCriteria.to_item_id = ToCustomerAC.get_ItemValue();
        argsCriteria.to_item_number = ToCustomerAC.get_Item().label;
    }
        
    var SearchCriteria = '{';
    for (var key in argsCriteria) {
        if (argsCriteria.hasOwnProperty(key))
            if (argsCriteria[key] != "") {
                SearchCriteria = SearchCriteria + ('"' + key + '":' + '"' + argsCriteria[key] + '",');
            }
    }


    if (SearchCriteria != '{') {
        SearchCriteria = SearchCriteria.slice(0, -1);
    }
    SearchCriteria = SearchCriteria + '}';
    var exportPageURL = ExportPageURL + formatString("schemaID={0}&SearchCriteria={1}", QuantityOnHandReportSchema, SearchCriteria);
    debugger;
    window.open(exportPageURL, '_blank');
}

function resetForm() {
    FromLocationAC.clear();
    ToLocationAC.clear();
    FromCustomerAC.clear();
    ToCustomerAC.clear();
}


function getdate(date) {
    var arr = date.split('-');
    return arr[1] + '-' + arr[0] + '-' + arr[2];
}