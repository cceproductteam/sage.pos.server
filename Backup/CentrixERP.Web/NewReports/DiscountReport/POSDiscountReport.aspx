﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="POSDiscountReport.aspx.cs"
    Inherits="CentrixERP.Reports.Web.NewReports.IcQuantityOnHand.IcQuantityOnHand"
    MasterPageFile="~/Common/MasterPage/ERPSite.master" %>

<asp:Content runat="server" ID="Content1" ContentPlaceHolderID="PageHeader">
    <script src="../../Common/DataDictionary/DataDictionary-ar.js" type="text/javascript"></script>
    <script src="../../Common/DataDictionary/DataDictionary-en.js" type="text/javascript"></script>
    <link href="../CSS/ReportCSS.css?v=<%=Centrix_Version %>" rel="stylesheet" type="text/css" />
    <script src="JS/POSDiscountReport.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <script type="text/javascript">
        ExportPageURL = '<%=ExportPageURL %>';
    </script>
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="PageBody">
    <div class="">
        <div class="">
            <div class="Details-div">
                <span class="details-div-title" resourcekey="AdvFilterYourResults"></span>
                <div class="Action-div">
                    <ul>
                    </ul>
                </div>
            </div>
            <table cellpadding="0" cellspacing="0" id="id-form" class="Data-Table">
                <tr>
                    <th>
                        <span class="label-title" id="span1" resourcekey="InvoiceNumber"></span>
                    </th>
                    <td>
                        <input class="cx-control InvoiceNumber  string" type="text">
                    </td>
                   <th>
                        <span class="label-title" id="span4" resourcekey="Person" font-bold="true"></span>
                    </th>
                    <td>
                        <select class="lst Person">
                        </select>
                    </td>
                </tr>
                <tr>
                        <th>
                            <span class="label-title" resourcekey="Store"></span>
                        </th>
                        <td>
                            <select class="lst Store">
                            </select>
                        </td>
                        <th>
                            <span class="label-title" resourcekey="Register"></span>
                        </th>
                        <td>
                            <select class="lst Register">
                            </select>
                        </td>
                    </tr>
                <tr style="display:none">
                
                 <th>
                        <span class="label-title" id="span2" resourcekey="Customer" ></span>
                    </th>
                    <td>
                        <select class="lst Customer">
                        </select>
                    </td>
                    
                </tr>
                <tr style="display:none">
                    <th>
                        <span class="label-title" id="span6" resourcekey="LessThanDiscount" ></span>
                    </th>
                    <td>
                       <input class="cx-control txtLessThanDiscount  money" type="text" />
                    </td>
                    <th>
                        <span class="label-title" id="span7" resourcekey="GreaterThanDiscount" font-bold="true"></span>
                    </th>
                    <td>
                        <input class="cx-control txtGreaterThanDiscount  money" type="text" />
                    </td>
                    
                </tr>
                
                <tr>
                    <th>
                        <span class="label-title" id="span8" resourcekey="DateFrom" font-bold="true"></span>
                    </th>
                    <td>
                        <input class="cx-control DateFrom  date" type="text" />
                    </td>
                    <th>
                        <span class="label-title" id="span9" resourcekey="DateTo" font-bold="true"></span>
                    </th>
                    <td>
                        <input class="cx-control DateTo  date" type="text" />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td class="search-buttons" colspan="6">
                        <div class="filter-reset">
                            <input type="button" class="btnSearch button2" id="btnSearch" value="Search" />
                            <input value="Reset" class="lnkReset lnkReset-InvoiceInquiry button-cancel button2"
                                type="button" />
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
