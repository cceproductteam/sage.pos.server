﻿var InvoiceReportSchema;
var ExportPageURL;
var FromLocationAC;
var ToLocationAC;
var FromCustomerAC;
var ToCustomerAC;
var CustomerAC;
var PersonAC;

function InnerPage_Load() {
    initControls();
    setPageTitle(getResource("DiscountReport"));
    InvoiceReportSchema = systemConfig.ReportSchema.DiscountReport;
    setSelectedMenuItem('.pos-icon');
}

function initControls() {

    LoadDatePicker('input:.date', 'mm-dd-yy');
    CustomerAC = $('select:.Customer').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.PersonServiceURL + 'FindAllLite', required: false, multiSelect: true });
    PersonAC = $('select:.Person').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.PersonServiceURL + 'FindAllLite', required: false, multiSelect: true });
    StoreAC = $('select:.Store').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.StoreServiceURL + 'FindAllLite', required: false, multiSelect: true });
    RegisterAC = $('select:.Register').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.RegisterServiceURL + 'FindAllLite', required: false, multiSelect: true });
    
    $('.lnkReset').click(resetForm);
    $('.btnSearch').click(BuildSearchCriteria);
    $('.Quick-Search-Controls').live('keydown', function (e) {
        if (e.keyCode == 13 || e.which == 13) {
            $('.btnSearch').click();
        }
    });

    $('.btnSearch').attr('value', getResource("Search"));
    $('.lnkReset').attr('value', getResource("Reset"));
}

function BuildSearchCriteria() {


    if (!saveForm($('.Quick-Search-Controls'))) {
        return false;
    }


    $('.search-criteria').val('');
    var argsCriteria = new Object();
    //argsCriteria.ExportPageURL = ExportPageURL;



    if ($('.InvoiceNumber').val() != null)
        argsCriteria.invoice_number = $('.InvoiceNumber').val();

    if ($('.txtGreaterThanDiscount').val() != null)
        argsCriteria.greater_than_discount = $('.txtGreaterThanDiscount').val();

    if ($('.txtLessThanDiscount').val() != null)
        argsCriteria.less_than_discount = $('.txtLessThanDiscount').val();


    if ($('.DateFrom').val() != null)
        argsCriteria.date_from = $('.DateFrom').val();
    if ($('.DateTo').val() != null)
        argsCriteria.date_to = $('.DateTo').val();

    if (CustomerAC.get_Item() != null)
        argsCriteria.customer_id = CustomerAC.get_ItemsIds();
    if (PersonAC.get_Item() != null)
        argsCriteria.person_ids = PersonAC.get_ItemsIds();
    if (StoreAC.get_Item() != null)
        argsCriteria.store_id = StoreAC.get_ItemsIds();
    if (RegisterAC.get_Item() != null)
        argsCriteria.register_id = RegisterAC.get_ItemsIds();

    var SearchCriteria = '{';
    for (var key in argsCriteria) {
        if (argsCriteria.hasOwnProperty(key))
            if (argsCriteria[key] != "") {
                SearchCriteria = SearchCriteria + ('"' + key + '":' + '"' + argsCriteria[key] + '",');
            }
    }


    if (SearchCriteria != '{') {
        SearchCriteria = SearchCriteria.slice(0, -1);
    }
    SearchCriteria = SearchCriteria + '}';
    var exportPageURL = ExportPageURL + formatString("schemaID={0}&SearchCriteria={1}", InvoiceReportSchema, SearchCriteria);
    debugger;
    window.open(exportPageURL, '_blank');
}

function resetForm() {
    $('.InvoiceNumber').val('');
    $('.txtGreaterThanDiscount').val('');
    $('.txtLessThanDiscount').val('');
    $('.DateFrom').val('');
    $('.DateTo').val('');
    CustomerAC.clear();
    PersonAC.clear();

}


function getdate(date) {
    var arr = date.split('-');
    return arr[1] + '-' + arr[0] + '-' + arr[2];
}