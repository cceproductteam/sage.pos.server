﻿var ItemDeliveryReportSchema;
var ExportPageURL;
var FromItemAC;
var ToItemAC;
var FromVendorAC;
var ToVendorAC;

function InnerPage_Load() {
    initControls();
    setPageTitle(getResource("IcItemDeliveryReport"));
    ItemDeliveryReportSchema = systemConfig.ReportSchema.IcItemDeliveryReport;
    setSelectedMenuItem('.inventory-icon');
}

function initControls() {

    FromVendorAC = $('select:.FromVendor').AutoComplete({ serviceURL: systemConfig.ApplicationURL + systemConfig.VendorServiceURL + 'FindAllLite', required: true });
    ToVendorAC = $('select:.ToVendor').AutoComplete({ serviceURL: systemConfig.ApplicationURL + systemConfig.VendorServiceURL + 'FindAllLite', required: true });
    FromItemAC = $('select:.FromItem').AutoComplete({ serviceURL: systemConfig.ApplicationURL + systemConfig.IcItemCardServiceURL + 'FindAllLite', required: true });
    ToItemAC = $('select:.ToItem').AutoComplete({ serviceURL: systemConfig.ApplicationURL + systemConfig.IcItemCardServiceURL + 'FindAllLite', required: true });

    LoadDatePicker('input:.date');

    $('.fractional-numeric').autoNumeric();
    $('.numeric').autoNumeric({ aSep: ',', aNeg: '', mDec: 0, mRound: 'S', mNum: 10 });

    $('.lnkReset').click(resetForm);
    $('.btnSearch').click(BuildSearchCriteria);
    $('.Quick-Search-Controls').live('keydown', function (e) {
        if (e.keyCode == 13 || e.which == 13) {
            $('.btnSearch').click();
        }
    });

    $('.btnSearch').attr('value', getResource("Search"));
    $('.lnkReset').attr('value', getResource("Reset"));
}

function BuildSearchCriteria() {

    if (!saveForm($('.Quick-Search-Controls'))) {
        return false;
    }

    $('.search-criteria').val('');
    var argsCriteria = new Object();
    //argsCriteria.ExportPageURL = ExportPageURL;

    if (FromVendorAC.get_Item() != null)
        argsCriteria.from_vendor_id = FromVendorAC.get_ItemValue();

    if (ToVendorAC.get_Item() != null)
        argsCriteria.to_vendor_id = ToVendorAC.get_ItemValue();

    if (FromItemAC.get_Item() != null)
        argsCriteria.from_item_id = FromItemAC.get_ItemValue();

    if (ToItemAC.get_Item() != null)
        argsCriteria.to_item_id = ToItemAC.get_ItemValue();

    if ($('input:.FromDate').val() != "")
        argsCriteria.from_date = getdate($('input:.FromDate').val());

    if ($('input:.ToDate').val() != "")
        argsCriteria.to_date = getdate($('input:.ToDate').val());  
        
    var SearchCriteria = '{';
    for (var key in argsCriteria) {
        if (argsCriteria.hasOwnProperty(key))
            if (argsCriteria[key] != "") {
                SearchCriteria = SearchCriteria + ('"' + key + '":' + '"' + argsCriteria[key] + '",');
            }
    }

    if (SearchCriteria != '{') {
        SearchCriteria = SearchCriteria.slice(0, -1);
    }
    SearchCriteria = SearchCriteria + '}';
    var exportPageURL = ExportPageURL + formatString("schemaID={0}&SearchCriteria={1}", ItemDeliveryReportSchema, SearchCriteria);
    debugger;
    window.open(exportPageURL, '_blank');
}

function resetForm() {
    FromVendorAC.clear();
    ToVendorAC.clear();
    FromItemAC.clear();
    ToItemAC.clear();
    $('input:.FromDate').val('');
    $('input:.ToDate').val('');
}


function getdate(date) {
    var arr = date.split('-');
    return arr[1] + '-' + arr[0] + '-' + arr[2];
}