﻿var RollupAccountsReportSchema;
var ExportPageURL;
var FromAccountAC, ToAccountAC;


function InnerPage_Load() {
    initControls();
    setPageTitle(getResource("RollupAccountsReportt"));
    RollupAccountsReportSchema = systemConfig.ReportSchema.RollupAccountsReport;
    setSelectedMenuItem('.general-ledger');
}


function initControls() {

    FromAccountAC = $('select:.FromAccount').AutoComplete({ serviceURL: systemConfig.ApplicationURL + systemConfig.accountServiceURL + 'FindAllLite', required: false });
    FromAccountAC.set_Args("{IsRollUp:true}");

    ToAccountAC = $('select:.ToAccount').AutoComplete({ serviceURL: systemConfig.ApplicationURL + systemConfig.accountServiceURL + 'FindAllLite', required: false });
    ToAccountAC.set_Args("{IsRollUp:true}");

    $('.lnkReset').click(resetForm);
    $('.btnSearch').click(BuildSearchCriteria);
    $('.Quick-Search-Controls').live('keydown', function (e) {
        if (e.keyCode == 13 || e.which == 13) {
            $('.btnSearch').click();
        }
    });

    $('.btnSearch').attr('value', getResource("Search"));
    $('.lnkReset').attr('value', getResource("Reset"));
}

function BuildSearchCriteria() {

    if (!saveForm($('.Quick-Search-Controls'))) {
        return false;
    }

    $('.search-criteria').val('');
    var argsCriteria = new Object();
    //argsCriteria.ExportPageURL = ExportPageURL;

    if (FromAccountAC.get_Item() != null)
        argsCriteria.account_id_from = FromAccountAC.get_ItemValue();

    if (ToAccountAC.get_Item() != null)
        argsCriteria.account_id_to = ToAccountAC.get_ItemValue();


    var SearchCriteria = '{';
    for (var key in argsCriteria) {
        if (argsCriteria.hasOwnProperty(key))
            if (argsCriteria[key] != "") {
                SearchCriteria = SearchCriteria + ('"' + key + '":' + '"' + argsCriteria[key] + '",');
            }
    }

    if (SearchCriteria != '{') {
        SearchCriteria = SearchCriteria.slice(0, -1);
    }
    SearchCriteria = SearchCriteria + '}';
    var exportPageURL = ExportPageURL + formatString("schemaID={0}&SearchCriteria={1}", RollupAccountsReportSchema, SearchCriteria);
    window.open(exportPageURL, '_blank');
}

function resetForm() {
    FromAccountAC.clear();
    ToAccountAC.clear();
}

function getdate(date) {
    var arr = date.split('-');
    return arr[1] + '-' + arr[0] + '-' + arr[2];
}
