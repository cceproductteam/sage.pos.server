﻿var VendorBalanceReportSchema;
var ExportPageURL;
var VendorAC;


function InnerPage_Load() {
    initControls();
    setPageTitle(getResource("VendorBalanceReportt"));
    VendorBalanceReportSchema = systemConfig.ReportSchema.VendorBalanceReport;
    setSelectedMenuItem('.account-payable');
}


function initControls() {

    VendorAC = $('select:.Vendor').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.VendorServiceURL + 'FindAllLite', required: false, multiSelect: true });

    LoadDatePicker('input:.date');

    $('.lnkReset').click(resetForm);
    $('.btnSearch').click(BuildSearchCriteria);
    $('.Quick-Search-Controls').live('keydown', function (e) {
        if (e.keyCode == 13 || e.which == 13) {
            $('.btnSearch').click();
        }
    });

    $('.btnSearch').attr('value', getResource("Search"));
    $('.lnkReset').attr('value', getResource("Reset"));
}

function BuildSearchCriteria() {

    if (!saveForm($('.Quick-Search-Controls'))) {
        return false;
    }

    $('.search-criteria').val('');
    var argsCriteria = new Object();

    if (VendorAC.get_ItemsIds() != null && VendorAC.get_ItemsIds() != "")
        argsCriteria.vendor_ids = VendorAC.get_ItemsIds();
    if ($('input.From').val())
        argsCriteria.from_date = getdate($('input.From').val());
    if ($('input.To').val())
        argsCriteria.to_date = getdate($('input.To').val());



    var SearchCriteria = '{';
    for (var key in argsCriteria) {
        if (argsCriteria.hasOwnProperty(key))
            if (argsCriteria[key] != "") {
                SearchCriteria = SearchCriteria + ('"' + key + '":' + '"' + argsCriteria[key] + '",');
            }
    }

    if (SearchCriteria != '{') {
        SearchCriteria = SearchCriteria.slice(0, -1);
    }
    SearchCriteria = SearchCriteria + '}';
    var exportPageURL = ExportPageURL + formatString("schemaID={0}&SearchCriteria={1}", VendorBalanceReportSchema, SearchCriteria);
    window.open(exportPageURL, '_blank');
}

function resetForm() {
    VendorAC.clear();
    $('input.From').val('');
    $('input.To').val('');

}

function getdate(date) {
    var arr = date.split('-');
    return arr[1] + '-' + arr[0] + '-' + arr[2];
}
