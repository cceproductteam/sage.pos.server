﻿var ItemTransactionsListReportSchema;
var ExportPageURL;
var FromItemAC, ToItemAC;
var TransactionTypeAC, ItemsTypeAC, FromLocationAC, ToLocationAC;


function InnerPage_Load() {
    initControls();
    setPageTitle(getResource("ItemTransactionsReport"));
    ItemTransactionsListReportSchema = systemConfig.ReportSchema.ItemTransactionsReport;
    setSelectedMenuItem('.inventory-icon');
}


function initControls() {

    FromItemAC = $('select:.FromItem').AutoComplete({ serviceURL: systemConfig.ApplicationURL + systemConfig.IcItemCardServiceURL + 'FindAllLite', required: false });
    ToItemAC = $('select:.ToItem').AutoComplete({ serviceURL: systemConfig.ApplicationURL + systemConfig.IcItemCardServiceURL + 'FindAllLite', required: false });

    TransactionTypeAC = fillDataTypeContentList($('select:.TransactionType'), systemConfig.dataTypes.ReportTransactionType, false, false);
    ItemsTypeAC = fillDataTypeContentList($('select:.ItemsType'), systemConfig.dataTypes.ItemsType, false, false);

    FromLocationAC = $('select:.FromLocation').AutoComplete({ serviceURL: systemConfig.ApplicationURL + systemConfig.IcLocationServiceURL + 'FindAllLite', required: false });
    ToLocationAC = $('select:.ToLocation').AutoComplete({ serviceURL: systemConfig.ApplicationURL + systemConfig.IcLocationServiceURL + 'FindAllLite', required: false });

    LoadDatePicker('input:.date');

    //$('.numeric').autoNumeric({ aSep: ',', aNeg: '', mDec: 0, mRound: 'S', mNum: 10 });

    $('.lnkReset').click(resetForm);
    $('.btnSearch').click(BuildSearchCriteria);
    $('.Quick-Search-Controls').live('keydown', function (e) {
        if (e.keyCode == 13 || e.which == 13) {
            $('.btnSearch').click();
        }
    });

    $('.btnSearch').attr('value', getResource("Search"));
    $('.lnkReset').attr('value', getResource("Reset"));
}

function BuildSearchCriteria() {

    if (!saveForm($('.Quick-Search-Controls'))) {
        return false;
    }

    $('.search-criteria').val('');
    var argsCriteria = new Object();
    //argsCriteria.ExportPageURL = ExportPageURL;

    if (FromItemAC.get_Item() != null)
        argsCriteria.from_item_id = FromItemAC.get_ItemValue();

    if (ToItemAC.get_Item() != null)
        argsCriteria.to_item_id = ToItemAC.get_ItemValue();

    if (FromLocationAC.get_Item() != null)
        argsCriteria.from_location_id = FromLocationAC.get_ItemValue();

    if (ToLocationAC.get_Item() != null)
        argsCriteria.to_location_id = ToLocationAC.get_ItemValue();

    if (TransactionTypeAC.get_Item() != null)
        argsCriteria.transaction_type_id = TransactionTypeAC.get_ItemValue();

    if (ItemsTypeAC.get_Item() != null)
        argsCriteria.items_type_id = ItemsTypeAC.get_ItemValue();

    if ($('input:.FromDate').val() != "")
        argsCriteria.from_date = getdate($('input:.FromDate').val());

    if ($('input:.ToDate').val() != "")
        argsCriteria.to_date = getdate($('input:.ToDate').val());  

    var SearchCriteria = '{';
    for (var key in argsCriteria) {
        if (argsCriteria.hasOwnProperty(key))
            if (argsCriteria[key] != "") {
                SearchCriteria = SearchCriteria + ('"' + key + '":' + '"' + argsCriteria[key] + '",');
            }
    }

    if (SearchCriteria != '{') {
        SearchCriteria = SearchCriteria.slice(0, -1);
    }
    SearchCriteria = SearchCriteria + '}';
    var exportPageURL = ExportPageURL + formatString("schemaID={0}&SearchCriteria={1}", ItemTransactionsListReportSchema, SearchCriteria);
    window.open(exportPageURL, '_blank');
}

function resetForm() {
    FromItemAC.clear();
    ToItemAC.clear();
    FromLocationAC.clear();
    ToLocationAC.clear();
    TransactionTypeAC.clear();
    ItemsTypeAC.clear();
    $('input:.FromDate').val('');
    $('input:.ToDate').val('');
}

function getdate(date) {
    var arr = date.split('-');
    return arr[1] + '-' + arr[0] + '-' + arr[2];
}
