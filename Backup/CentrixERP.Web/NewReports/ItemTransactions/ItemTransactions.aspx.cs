﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using CentrixERP.Configuration;
using CentrixERP.Common.Web;

namespace CentrixERP.Web.NewReports.ItemTransactions
{
    public partial class ItemTransactions : BasePageLoggedIn
    {
        public ItemTransactions()
            : base(CentrixERP.Configuration.Enums_S3.Permissions.SystemModules.None) { }

        public string ExportPageURL;
        protected void Page_Load(object sender, EventArgs e)
        {
            ExportPageURL = ConfigurationManager.AppSettings["ExportPageURL"].ToString();
        }
    }
}