﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using CentrixERP.Common.Web;

namespace CentrixERP.Reports.Web.NewReports.IcSalesReport
{
    public partial class IcSalesReport : BasePageLoggedIn
    {
        public IcSalesReport()
            : base(CentrixERP.Configuration.Enums_S3.Permissions.SystemModules.None) { }
        public string ExportPageURL;
        protected void Page_Load(object sender, EventArgs e)
        {
            ExportPageURL = ConfigurationManager.AppSettings["ExportPageURL"].ToString();
        }
    }
}