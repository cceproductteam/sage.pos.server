﻿var SalesReportSchema;
var ExportPageURL;
var FromCustomerAC;
var ToCustomerAC;
var FromLocationAC;
var ToLocationAC;
var FromItemAC;
var ToItemAC;

function InnerPage_Load() {
    initControls();
    setPageTitle(getResource("IcSalesReport"));
    SalesReportSchema = systemConfig.ReportSchema.IcSalesReport;
    setSelectedMenuItem('.inventory-icon');
}

function initControls() {

    FromLocationAC = $('select:.FromLocation').AutoComplete({ serviceURL: systemConfig.ApplicationURL + systemConfig.IcLocationServiceURL + 'FindAllLite', required: true });
    ToLocationAC = $('select:.ToLocation').AutoComplete({ serviceURL: systemConfig.ApplicationURL + systemConfig.IcLocationServiceURL + 'FindAllLite', required: true });
    FromCustomerAC = $('select:.FromCustomer').AutoComplete({ serviceURL: systemConfig.ApplicationURL + systemConfig.CustomerWebService + 'FindAllLite', required: true });
    ToCustomerAC = $('select:.ToCustomer').AutoComplete({ serviceURL: systemConfig.ApplicationURL + systemConfig.CustomerWebService + 'FindAllLite', required: true });
    FromItemAC = $('select:.FromItem').AutoComplete({ serviceURL: systemConfig.ApplicationURL + systemConfig.IcItemCardServiceURL + 'FindAllLite', required: true });
    ToItemAC = $('select:.ToItem').AutoComplete({ serviceURL: systemConfig.ApplicationURL + systemConfig.IcItemCardServiceURL + 'FindAllLite', required: true });

    LoadDatePicker('input:.date');

    $('.fractional-numeric').autoNumeric();
    $('.numeric').autoNumeric({ aSep: ',', aNeg: '', mDec: 0, mRound: 'S', mNum: 10 });

    $('.lnkReset').click(resetForm);
    $('.btnSearch').click(BuildSearchCriteria);
    $('.Quick-Search-Controls').live('keydown', function (e) {
        if (e.keyCode == 13 || e.which == 13) {
            $('.btnSearch').click();
        }
    });

    $('.btnSearch').attr('value', getResource("Search"));
    $('.lnkReset').attr('value', getResource("Reset"));
}

function BuildSearchCriteria() {

    if (!saveForm($('.Quick-Search-Controls'))) {
        return false;
    }

    $('.search-criteria').val('');
    var argsCriteria = new Object();
    //argsCriteria.ExportPageURL = ExportPageURL;

    if (FromLocationAC.get_Item() != null)
        argsCriteria.from_location_id = FromLocationAC.get_ItemValue();

    if (ToLocationAC.get_Item() != null)
        argsCriteria.to_location_id = ToLocationAC.get_ItemValue();

    if (FromCustomerAC.get_Item() != null)
        argsCriteria.from_customer_id = FromCustomerAC.get_ItemValue();

    if (ToCustomerAC.get_Item() != null)
        argsCriteria.to_customer_id = ToCustomerAC.get_ItemValue();

    if (FromItemAC.get_Item() != null)
        argsCriteria.from_item_id = FromItemAC.get_ItemValue();

    if (ToItemAC.get_Item() != null)
        argsCriteria.to_item_id = ToItemAC.get_ItemValue();

    if ($('input:.FromQuantity').val() != "")
        argsCriteria.from_quantity= $('input:.FromQuantity').autoNumericGet();

    if ($('input:.ToQuantity').val() != "")
        argsCriteria.to_quantity = $('input:.ToQuantity').autoNumericGet();

    if ($('input:.FromValue').val() != "")
        argsCriteria.from_value= $('input:.FromValue').autoNumericGet();

    if ($('input:.ToValue').val() != "")
        argsCriteria.to_value = $('input:.ToValue').autoNumericGet();

    if ($('input:.FromDate').val() != "")
        argsCriteria.from_date = getdate($('input:.FromDate').val());

    if ($('input:.ToDate').val() != "")
        argsCriteria.to_date = getdate($('input:.ToDate').val());  

    var SearchCriteria = '{';
    for (var key in argsCriteria) {
        if (argsCriteria.hasOwnProperty(key))
            if (argsCriteria[key] != "") {
                SearchCriteria = SearchCriteria + ('"' + key + '":' + '"' + argsCriteria[key] + '",');
            }
    }

    if (SearchCriteria != '{') {
        SearchCriteria = SearchCriteria.slice(0, -1);
    }
    SearchCriteria = SearchCriteria + '}';
    var exportPageURL = ExportPageURL + formatString("schemaID={0}&SearchCriteria={1}", SalesReportSchema, SearchCriteria);
    window.open(exportPageURL, '_blank');
}

function resetForm() {
    FromLocationAC.clear();
    ToLocationAC.clear();
    FromCustomerAC.clear();
    ToCustomerAC.clear();
    FromItemAC.clear();
    ToItemAC.clear();
    $('input:.FromQuantity').val('');
    $('input:.ToQuantity').val('');
    $('input:.FromValue').val('');
    $('input:.ToValue').val('');
    $('input:.FromDate').val('');
    $('input:.ToDate').val('');
}


function getdate(date) {
    var arr = date.split('-');
    return arr[1] + '-' + arr[0] + '-' + arr[2];
}