﻿var SalesReportSchema;
var ExportPageURL;
var FromItemAC, ToItemAC;
var FromBrandAC, ToBrandAC, FromCategoryAC, ToCategoryAC, FromColorAC, ToColorAC, FromSizeAC, ToSizeAC;


function InnerPage_Load() {
    initControls();
    setPageTitle(getResource("IcSalesReport"));
    SalesReportSchema = systemConfig.ReportSchema.SalesReport;
    setSelectedMenuItem('.inventory-icon');
}


function initControls() {

    FromItemAC = $('select:.FromItem').AutoComplete({ serviceURL: systemConfig.ApplicationURL + systemConfig.IcItemCardServiceURL + 'FindAllLite', required: false });
    ToItemAC = $('select:.ToItem').AutoComplete({ serviceURL: systemConfig.ApplicationURL + systemConfig.IcItemCardServiceURL + 'FindAllLite', required: false });

    FromBrandAC = $('select:.FromBrand').AutoComplete({ serviceURL: systemConfig.ApplicationURL + systemConfig.IcSegmentCodeServiceURL + 'FindAllLite', required: false, args: '{ SegmentId:2}' });
    ToBrandAC = $('select:.ToBrand').AutoComplete({ serviceURL: systemConfig.ApplicationURL + systemConfig.IcSegmentCodeServiceURL + 'FindAllLite', required: false, args: '{ SegmentId:2}' });

    FromCategoryAC = $('select:.FromCategory').AutoComplete({ serviceURL: systemConfig.ApplicationURL + systemConfig.IcSegmentCodeServiceURL + 'FindAllLite', required: false, args: '{ SegmentId:3}' });
    ToCategoryAC = $('select:.ToCategory').AutoComplete({ serviceURL: systemConfig.ApplicationURL + systemConfig.IcSegmentCodeServiceURL + 'FindAllLite', required: false, args: '{ SegmentId:3}' });

    FromColorAC = $('select:.FromColor').AutoComplete({ serviceURL: systemConfig.ApplicationURL + systemConfig.IcSegmentCodeServiceURL + 'FindAllLite', required: false, args: '{ SegmentId:4}' });
    ToColorAC = $('select:.ToColor').AutoComplete({ serviceURL: systemConfig.ApplicationURL + systemConfig.IcSegmentCodeServiceURL + 'FindAllLite', required: false, args: '{ SegmentId:4}' });

    FromSizeAC = $('select:.FromSize').AutoComplete({ serviceURL: systemConfig.ApplicationURL + systemConfig.IcSegmentCodeServiceURL + 'FindAllLite', required: false, args: '{ SegmentId:7}' });
    ToSizeAC = $('select:.ToSize').AutoComplete({ serviceURL: systemConfig.ApplicationURL + systemConfig.IcSegmentCodeServiceURL + 'FindAllLite', required: false, args: '{ SegmentId:7}' });

    LoadDatePicker('input:.date');


    $('.lnkReset').click(resetForm);
    $('.btnSearch').click(BuildSearchCriteria);
    $('.Quick-Search-Controls').live('keydown', function (e) {
        if (e.keyCode == 13 || e.which == 13) {
            $('.btnSearch').click();
        }
    });

    $('.btnSearch').attr('value', getResource("Search"));
    $('.lnkReset').attr('value', getResource("Reset"));
}

function BuildSearchCriteria() {

    if (!saveForm($('.Quick-Search-Controls'))) {
        return false;
    }

    $('.search-criteria').val('');
    var argsCriteria = new Object();
   

    if (FromItemAC.get_Item() != null)
        argsCriteria.from_item_id = FromItemAC.get_ItemValue();

    if (ToItemAC.get_Item() != null)
        argsCriteria.to_item_id = ToItemAC.get_ItemValue();

    if (FromBrandAC.get_Item() != null)
        argsCriteria.from_brand = FromBrandAC.get_ItemValue();

    if (ToBrandAC.get_Item() != null)
        argsCriteria.to_brand = ToBrandAC.get_ItemValue();

    if (FromCategoryAC.get_Item() != null)
        argsCriteria.from_category = FromCategoryAC.get_ItemValue();

    if (ToCategoryAC.get_Item() != null)
        argsCriteria.to_category = ToCategoryAC.get_ItemValue();

    if (FromColorAC.get_Item() != null)
        argsCriteria.from_color = FromColorAC.get_ItemValue();

    if (ToColorAC.get_Item() != null)
        argsCriteria.to_color = ToColorAC.get_ItemValue();

    if (FromSizeAC.get_Item() != null)
        argsCriteria.from_size = FromSizeAC.get_ItemValue();

    if (ToSizeAC.get_Item() != null)
        argsCriteria.to_size = ToSizeAC.get_ItemValue();

    if ($('input:.FromDate').val() != "")
        argsCriteria.from_date = getdate($('input:.FromDate').val());

    if ($('input:.ToDate').val() != "")
        argsCriteria.to_date = getdate($('input:.ToDate').val());

    var SearchCriteria = '{';
    for (var key in argsCriteria) {
        if (argsCriteria.hasOwnProperty(key))
            if (argsCriteria[key] != "") {
                SearchCriteria = SearchCriteria + ('"' + key + '":' + '"' + argsCriteria[key] + '",');
            }
    }

    if (SearchCriteria != '{') {
        SearchCriteria = SearchCriteria.slice(0, -1);
    }
    SearchCriteria = SearchCriteria + '}';
    var exportPageURL = ExportPageURL + formatString("schemaID={0}&SearchCriteria={1}", SalesReportSchema, SearchCriteria);
    window.open(exportPageURL, '_blank');
}

function resetForm() {
    FromItemAC.clear();
    ToItemAC.clear();
    FromBrandAC.clear();
    ToBrandAC.clear();
    FromCategoryAC.clear();
    ToCategoryAC.clear();
    FromColorAC.clear();
    ToColorAC.clear();
    FromSizeAC.clear();
    ToSizeAC.clear();
    $('input:.FromDate').val('');
    $('input:.ToDate').val('');
}

function getdate(date) {
    var arr = date.split('-');
    return arr[1] + '-' + arr[0] + '-' + arr[2];
}
