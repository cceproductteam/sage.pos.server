﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SalesReport.aspx.cs" 
Inherits="CentrixERP.Web.NewReports.IcSalesReport.SalesReport" MasterPageFile="~/Common/MasterPage/ERPSite.master" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="PageHeader">
    <script src="JS/SalesReport.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <link href="../CSS/ReportCSS.css?v=<%=Centrix_Version %>" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        ExportPageURL = '<%=ExportPageURL %>';
    </script>
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="PageBody">
    <input runat="server" class="search-criteria" id="hdnSearchCriteria" type="hidden"
        value="" />
    <div class="">
        <div class="">
            <div class="Details-div">
                <span class="details-div-title" resourcekey="AdvFilterYourResults"></span>
                <div class="Action-div" style="display: none;">
                    <ul>
                    </ul>
                </div>
            </div>
            <table cellspacing="0" cellpadding="0" border="0" class="Data-Table Quick-Search-Controls">
                <tbody>


                 <tr>
                     <th>
                            <span class="label-title" resourcekey="FromDate"></span>
                        </th>
                        <td>
                            <input type="text" class="cx-control FromDate date txt" /><br />
                        </td>
                        <th>
                            <span class="label-title" resourcekey="ToDate"></span>
                        </th>
                        <td>
                            <input type="text" class="cx-control ToDate date txt" /><br />
                        </td>
                       
                    </tr>

                    <tr>
                        <th>
                            <span class="label-title" resourcekey="FromItem"></span>
                        </th>
                        <td>
                            <div>
                                <select class="lst FromItem">
                                </select></div>
                        </td>
                         <th>
                            <span class="label-title" resourcekey="ToItem"></span>
                        </th>
                        <td>
                            <div>
                                <select class="lst ToItem">
                                </select></div>
                        </td>
                   
                    </tr>
                   

                    <tr>
                    
                      <th>
                            <span class="label-title" resourcekey="FromBrand"></span>
                        </th>
                        <td>
                            <div>
                                <select class="lst FromBrand">
                                </select></div>
                        </td>
                         <th>
                            <span class="label-title" resourcekey="ToBrand"></span>
                        </th>
                        <td>
                            <div>
                                <select class="lst ToBrand">
                                </select></div>
                        </td>
                    </tr>

                     <tr>
                    
                      <th>
                            <span class="label-title" resourcekey="FromCategory"></span>
                        </th>
                        <td>
                            <div>
                                <select class="lst FromCategory">
                                </select></div>
                        </td>
                         <th>
                            <span class="label-title" resourcekey="ToCategory"></span>
                        </th>
                        <td>
                            <div>
                                <select class="lst ToCategory">
                                </select></div>
                        </td>
                    </tr>
                      <tr>
                    
                      <th>
                            <span class="label-title" resourcekey="FromColor"></span>
                        </th>
                        <td>
                            <div>
                                <select class="lst FromColor">
                                </select></div>
                        </td>
                         <th>
                            <span class="label-title" resourcekey="ToColor"></span>
                        </th>
                        <td>
                            <div>
                                <select class="lst ToColor">
                                </select></div>
                        </td>
                    </tr>
                   
   <tr>
                    
                      <th>
                            <span class="label-title" resourcekey="FromSize"></span>
                        </th>
                        <td>
                            <div>
                                <select class="lst FromSize">
                                </select></div>
                        </td>
                         <th>
                            <span class="label-title" resourcekey="ToSize"></span>
                        </th>
                        <td>
                            <div>
                                <select class="lst ToSize">
                                </select></div>
                        </td>
                    </tr>
                   

                    <tr>
                        <td class="search-buttons" colspan="6">
                            <div class="filter-reset">
                                <input type="button" class="btnSearch button2" id="btnSearch" value="Search" />
                                <input value="Reset" class="lnkReset button-cancel button2" type="button" />
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</asp:Content>
