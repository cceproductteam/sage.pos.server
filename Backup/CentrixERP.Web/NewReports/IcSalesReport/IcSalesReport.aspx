﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="IcSalesReport.aspx.cs" Inherits="CentrixERP.Reports.Web.NewReports.IcSalesReport.IcSalesReport" MasterPageFile="~/Common/MasterPage/ERPSite.master" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="PageHeader">
    <script src="../../Common/DataDictionary/DataDictionary-ar.js" type="text/javascript"></script>
    <script src="../../Common/DataDictionary/DataDictionary-en.js" type="text/javascript"></script>
    <link href="../CSS/ReportCSS.css?v=<%=Centrix_Version %>" rel="stylesheet" type="text/css" />
    <script src="JS/IcSalesReport.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <script type="text/javascript">
        ExportPageURL = '<%=ExportPageURL %>';
    </script>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="PageBody">
<div class="">
        <div class="">
            <div class="Details-div">
                <span class="details-div-title" resourcekey="AdvFilterYourResults"></span>
                <div class="Action-div">
                    <ul>
                    </ul>
                </div>
            </div>
            <table cellspacing="0" cellpadding="0" border="0" class="Data-Table">
                <tbody>
                    <tr>
                        <th>
                            <span class="label-title" resourcekey="FromCustomer"></span>
                        </th>
                        <td>
                            <select class="lst FromCustomer">
                            </select>
                        </td>
                         
                     <th>
                            <span class="label-title" resourcekey="ToCustomer"></span>
                        </th>
                        <td>
                            <select class="lst ToCustomer">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <span class="label-title" resourcekey="FromLocation"></span>
                        </th>
                        <td>
                            <select class="lst FromLocation">
                            </select>
                        </td>
                         
                     <th>
                            <span class="label-title" resourcekey="ToLocation"></span>
                        </th>
                        <td>
                            <select class="lst ToLocation">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <span class="label-title" resourcekey="FromItem"></span>
                        </th>
                        <td>
                            <select class="lst FromItem">
                            </select>
                        </td>
                         
                        <th>
                            <span class="label-title" resourcekey="ToItem"></span>
                        </th>
                        <td>
                            <select class="lst ToItem">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <span class="label-title" resourcekey="FromQuantity"></span>
                        </th>
                        <td>
                           <input type="text" class="FromQuantity cx-control numeric" />
                        </td>
                         
                        <th>
                            <span class="label-title" resourcekey="ToQuantity"></span>
                        </th>
                        <td>
                           <input type="text" class="ToQuantity cx-control numeric" />
                        </td>
                    </tr>
                    <tr class='hidden'>
                        <th>
                            <span class="label-title" resourcekey="FromValue"></span>
                        </th>
                        <td>
                           <input type="text" class="FromValue cx-control fractional-numeric" />
                        </td>                         
                        <th>
                            <span class="label-title" resourcekey="ToValue"></span>
                        </th>
                        <td>
                           <input type="text" class="ToValue cx-control fractional-numeric" />
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <span class="label-title" resourcekey="FromDate"></span>
                        </th>
                        <td>
                           <input type="text" class="FromDate cx-control date" />
                        </td>
                        <th>
                            <span class="label-title" resourcekey="ToDate"></span>
                        </th>
                        <td>
                           <input type="text" class="ToDate cx-control date" />
                        </td>                         
                    </tr>
                    <tr>
                        <td class="search-buttons" colspan="6">
                            <div class="filter-reset">
                                <input type="button" class="btnSearch button2" id="btnSearch" value="Search" />
                                <input value="Reset" class="lnkReset button-cancel button2" type="button" />
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</asp:Content>