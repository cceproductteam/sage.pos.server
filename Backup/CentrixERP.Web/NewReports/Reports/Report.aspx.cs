using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CentrixERP.Common.Web;

namespace CentrixERP.Reports.Web.NewReports.Reports
{
    public partial class Report:BasePageLoggedIn
    {
        public Report()
            : base(CentrixERP.Configuration.Enums_S3.Permissions.SystemModules.None) { }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}