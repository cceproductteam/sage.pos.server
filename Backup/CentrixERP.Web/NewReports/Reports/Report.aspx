<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Report.aspx.cs" Inherits="CentrixERP.Reports.Web.NewReports.Reports.Report"
    MasterPageFile="~/Common/MasterPage/ERPSite.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="PageHeader" ID="PageHeader">
    <script src="JS/ReportJS.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <link href="CSS/ReportCSS.css?v=<%=Centrix_Version %>" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="PageBody" ID="PageBody">
    <div class="">
        <div class="" >
            <div class="cm-report display-none">
                <div class="Add-title">
                    <span resourcekey="Cashmanagmentreports"></span>
                </div>
                <table cellspacing="0" cellpadding="0" border="0" class="Data-Table">
                    <tbody>
                        <tr>
                            <th>
                                <a class="trans-listing" href="../CMTransaction/TransactionListing.aspx"><span class="transaction-icon icon">
                                </span><span class="label-title" resourcekey="Transactionlisting"></span></a>
                            </th>
                            <th>
                                <a class="bank-recon" href="../BankReconciliation/BankReconciliation.aspx"><span
                                    class="bank-recon-icon icon"></span><span class="label-title" resourcekey="BankReconciliation"></span></a>
                            </th>
                        </tr>
                    </tbody>
                </table>
            </div>
            <br />
            <div class="ar-report display-none">
                <div class="Add-title">
                    <span resourcekey="Accountreceivablereports"></span>
                </div>
                <table cellspacing="0" cellpadding="0" border="0" class="Data-Table">
                    <tbody>
                        <tr>
                            <th>
                                <a class="customer-aging-report" href="../Customer/CustomerAging.aspx"><span class="customer-aging-icon icon">
                                </span><span class="label-title" resourcekey="Customeraging"></span></a>
                            </th>
                            <th>
                                <a class="report" href="../Customer/CustomerStatement.aspx"><span class="statment-icon icon">
                                </span><span class="label-title" resourcekey="CustomerStatement"></span></a>
                            </th>
                        </tr>
                        <tr>
                            <th>
                                <a class="report" href="../Customer/CustomerTransactions.aspx"><span class="customer-trans-icon icon">
                                </span><span class="label-title" resourcekey="CustomerTransactions"></span></a>
                            </th>                           
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
