var ReportServiceURL;
var entityId;

function InnerPage_Load() {
    //setSelectedMenuItem('.Report-icon');
    setPageTitle(getResource('Reports'));
    if (getQSKey('cm') == "1")
    { entityId = systemEntities.CmReport; setSelectedMenuItem('.cash-managment'); }
    else { entityId = systemEntities.ArReport; setSelectedMenuItem('.account-receivable'); }
    ConfigurationServiceURL = systemConfig.ApplicationURL + systemConfig.ConfigurationWebServiceURL;
    checkPermission();
}


function checkPermission() {
    var data = '{reportPermissionId:{0}}';
    data = formatString(data, entityId);

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: ConfigurationServiceURL + "CheckReportPermission",
        data: data,
        datatype: "json",
        success: function (data) {
            var returnedValue = eval("(" + data.d + ")").result;
            if (!returnedValue) {

                $('.Data-Table').remove();
                showStatusMsg(getResource('vldDontHavePermission'));
            }
            else {
                $('.Data-Table').show();
                if (entityId == systemEntities.CmReport) {
                    $('.cm-report').show();
                    $('.ar-report').hide();
                 }
                else {
                    $('.cm-report').hide();
                    $('.ar-report').show();
                }
            }
        },
        error: function (ex) {
            hideLoading();

        }
    });
}