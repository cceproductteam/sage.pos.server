<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExportReports.aspx.cs"
    Inherits="CentrixERP.Reports.Web.NewReports.ExportReports" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Report</title>
    <script src="../Common/JScript/jquery-1.8.0.js" type="text/javascript"></script>
    <script src="JS/export.js" type="text/javascript"></script>
    <link href="CSS/export.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        var iFrameURL = "<%=iFrameURL%>";
    </script>

</head>
<body onload="showPrintButton()">
    <form id="frmReportViewer" runat="server">
    <asp:ScriptManager ID="ScriptManager1" EnablePageMethods="true" EnablePartialRendering="true"
        runat="server">
    </asp:ScriptManager>
    <div id="divMain">
        <rsweb:ReportViewer ID="rvREXReport" class="reportViewer" runat="server" height="500px" Width="1330px" 
         Font-Names="Verdana" Font-Size="8pt" ProcessingMode="Remote" AsyncRendering="true"
            ShowPrintButton="true" ShowBackButton="false" OnReportRefresh="rrvREXReport_ReportRefresh"
            OnReportError="OnReportError" Visible="false">
            <ServerReport />
        </rsweb:ReportViewer>

        <%-- <rsweb:ReportViewer ID="rvREXReport" class="reportViewer" runat="server" height="500px" Width="1330px"   Font-Names="Verdana" Font-Size="8pt" ProcessingMode="Remote"  AsyncRendering="true" ShowPrintButton="true" ShowBackButton="false" OnReportRefresh="OnRefreshReport" OnReportError="OnReportError"
           >
            <ServerReport  />
        </rsweb:ReportViewer>--%>

        <iframe id="frmPrint" name="frmPrint" runat="server" style="display: none"></iframe>
    </div>
    <div id="spinner" class="spinner" style="display: none;">
        <table align="center" valign="middle" style="height: 100%; width: 100%">
            <tr>
                <td>
                    <img id="img-spinner" src="../Common/Images/loading.gif" alt="Printing" />
                </td>
                <td>
                    <span style="font-family: Verdana; font-weight: bold; font-size: 10pt; width: 86px;">
                        Printing...</span>
                </td>
            </tr>
        </table>
    </div>
    <div>
        <asp:Label runat="server" ID="lblPermissionError" Text="Access denied you don't have permission"
            Visible="false"></asp:Label>
        <asp:Label runat="server" ID="lblReportError" Text="Error" Visible="false"></asp:Label>
    </div>
    </form>
</body>
</html>
