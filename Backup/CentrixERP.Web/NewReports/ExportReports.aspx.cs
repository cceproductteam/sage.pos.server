using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;
using System.Configuration;
using SF.Framework;
using CentrixERP.Configuration;
using System.Net;
using System.Web.Script.Serialization;
using CentrixERP.Common.Web;
using Centrix.UM.Business.IManager;
using Centrix.UM.Business.Entity;
using System.IO;

namespace CentrixERP.Reports.Web.NewReports
{
    public partial class ExportReports : BasePageLoggedIn, ICallbackEventHandler
    {
        public ExportReports()
            : base(CentrixERP.Configuration.Enums_S3.Permissions.SystemModules.None) { }
        private List<ReportParameter> list = new List<ReportParameter>();
        public bool listingReport = false;
        public bool cmReport = false;
        public string _reportName;
        private string _sessionPDFFileName;
        protected string iFrameURL;
        IRolePermissionManager iRolePermissionManager = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            iRolePermissionManager = (IRolePermissionManager)IoC.Instance.Resolve(typeof(IRolePermissionManager));
            _sessionPDFFileName = Session.SessionID.ToString() + ".pdf";
            lblReportError.Visible = false;

            RegisterClientsCallBackReference();
            RegisterClientCallBackAfterPrint();

            int schemaId = Request.QueryString[0].ToNumber();
            if (schemaId == (int)CentrixERP.Configuration.Enums_S3.Reports.schema.CMTransactionListing || schemaId == (int)CentrixERP.Configuration.Enums_S3.Reports.schema.BankReconciliation)
                cmReport = true;
            if (checkPermission())
            {

                rvREXReport.Visible = true;
                lblPermissionError.Visible = false;
                rvREXReport.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportViwerURL"].ToString());
                string reportFolderPath = "/" + ConfigurationManager.AppSettings["ServerReportFolder"];
                switch (schemaId)
                {

                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.CustomerStatement:
                        _reportName = ConfigurationManager.AppSettings["CustomerStatementReportViewerPath"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["CustomerStatementReportViewerPath"].ToString();
                        listingReport = true;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.CustomerTransaction:
                        _reportName = ConfigurationManager.AppSettings["CustomerTransactionReportViewerPath"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["CustomerTransactionReportViewerPath"].ToString();
                        listingReport = true;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.CustomerAging:
                        _reportName = ConfigurationManager.AppSettings["CustomerAgingReportViewerPath"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["CustomerAgingReportViewerPath"].ToString();
                        listingReport = true;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.CMTransactionListing:
                        _reportName = ConfigurationManager.AppSettings["CMTransactionListing"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["CMTransactionListing"].ToString();
                        listingReport = true;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.BankReconciliation:
                        _reportName = ConfigurationManager.AppSettings["BankReconciliation"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["BankReconciliation"].ToString();
                        listingReport = true;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.InvoiceBatchList:
                        _reportName = ConfigurationManager.AppSettings["ArInvoiceBatchList"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["ArInvoiceBatchList"].ToString();
                        listingReport = true;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.ReceiptbatchList:
                        _reportName = ConfigurationManager.AppSettings["ReceiptbatchListReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["ReceiptbatchListReport"].ToString();
                        listingReport = true;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.AdjustmentBatchList:
                        _reportName = ConfigurationManager.AppSettings["AdjustmentBatchListReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["AdjustmentBatchListReport"].ToString();
                        listingReport = true;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.RefundBatchList:
                        _reportName = ConfigurationManager.AppSettings["RefundBatchListReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["RefundBatchListReport"].ToString();
                        listingReport = true;
                        break;

                    ////////
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.InvoiceDetailsReport:
                        _reportName = ConfigurationManager.AppSettings["InvoiceDetailsReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["InvoiceDetailsReport"].ToString();
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.ReceiptDetailsReport:
                        _reportName = ConfigurationManager.AppSettings["ReceiptDetailsReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["ReceiptDetailsReport"].ToString();
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.ReceiptMiscReport:
                        _reportName = ConfigurationManager.AppSettings["ReceiptMiscReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["ReceiptMiscReport"].ToString();
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.ReceiptApplyDocReport:
                        _reportName = ConfigurationManager.AppSettings["ReceiptApplyDocReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["ReceiptApplyDocReport"].ToString();
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.ReceiptPrePaymentReport:
                        _reportName = ConfigurationManager.AppSettings["ReceiptPrePaymentReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["ReceiptPrePaymentReport"].ToString();
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.RefundDetailsReport:
                        _reportName = ConfigurationManager.AppSettings["RefundDetailsReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["RefundDetailsReport"].ToString();
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.AdjustmentDetailsReport:
                        _reportName = ConfigurationManager.AppSettings["AdjustmentDetailsReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["AdjustmentDetailsReport"].ToString();
                        break;

                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.InvoiceAppliedDocumentDetails:
                        _reportName = ConfigurationManager.AppSettings["InvoiceAppliedDocumentDetailsReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["InvoiceAppliedDocumentDetailsReport"].ToString();
                        break;

                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.ReceiptAppliedDocumentDetails:
                        _reportName = ConfigurationManager.AppSettings["ReceiptAppliedDocumentDetailsReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["ReceiptAppliedDocumentDetailsReport"].ToString();
                        break;

                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.RevenueRecognitionReport:
                        _reportName = ConfigurationManager.AppSettings["RevenueRecognitionReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["RevenueRecognitionReport"].ToString();
                        listingReport = true;
                        break;

                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.CurrencyExchangeRateReport:
                        _reportName = ConfigurationManager.AppSettings["CurrencyExchangeRateReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["CurrencyExchangeRateReport"].ToString();
                        listingReport = true;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.ApInvoiceBatchListReport:
                        _reportName = ConfigurationManager.AppSettings["ApInvoiceBatchReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["ApInvoiceBatchReport"].ToString();
                        listingReport = true;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.ApInvoiceDetailsReport:
                        _reportName = ConfigurationManager.AppSettings["ApInvoiceDetailsReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["ApInvoiceDetailsReport"].ToString();
                        listingReport = false;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.VendorTransaction:
                        _reportName = ConfigurationManager.AppSettings["VendorTransactionReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["VendorTransactionReport"].ToString();
                        listingReport = true;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.VendorStatment:
                        _reportName = ConfigurationManager.AppSettings["VendorStatmentReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["VendorStatmentReport"].ToString();
                        listingReport = true;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.ApPaymentBatchListReport:
                        _reportName = ConfigurationManager.AppSettings["ApPaymentBatchReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["ApPaymentBatchReport"].ToString();
                        listingReport = true;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.ApPaymentDetailsReport:
                        _reportName = ConfigurationManager.AppSettings["ApPaymentDetailsReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["ApPaymentDetailsReport"].ToString();
                        listingReport = false;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.VendorAging:
                        _reportName = ConfigurationManager.AppSettings["VendorAgingReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["VendorAgingReport"].ToString();
                        listingReport = true;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.TrialBalanceReport:
                        _reportName = ConfigurationManager.AppSettings["TrialBalanceReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["TrialBalanceReport"].ToString();
                        listingReport = true;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.IcQuantityOnHandReport:
                        _reportName = ConfigurationManager.AppSettings["IcQuantityonHandReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["IcQuantityonHandReport"].ToString();
                        listingReport = true;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.IcSalesReport:
                        _reportName = ConfigurationManager.AppSettings["ICSalesReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["ICSalesReport"].ToString();
                        listingReport = true;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.IcItemDeliveryReport:
                        _reportName = ConfigurationManager.AppSettings["IcItemDeliveryReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["IcItemDeliveryReport"].ToString();
                        listingReport = true;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.IcSellingPriceListReport:
                        _reportName = ConfigurationManager.AppSettings["IcSellingPriceListReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["IcSellingPriceListReport"].ToString();
                        listingReport = true;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.GLIncomeStatementsReport:
                        _reportName = ConfigurationManager.AppSettings["GLIncomeStatementsReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["GLIncomeStatementsReport"].ToString();
                        listingReport = true;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.GLAccountTransactionsReport:
                        _reportName = ConfigurationManager.AppSettings["GLAccountTransactionsReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["GLAccountTransactionsReport"].ToString();
                        listingReport = true;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.GLCashFlowStatementReport:
                        _reportName = ConfigurationManager.AppSettings["GLCashFlowStatementReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["GLCashFlowStatementReport"].ToString();
                        listingReport = true;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.IcItemSellingPriceReport:
                        _reportName = ConfigurationManager.AppSettings["IcItemSellingPriceReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["IcItemSellingPriceReport"].ToString();
                        listingReport = true;
                        break;

                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.ICShipmentReport:
                        _reportName = ConfigurationManager.AppSettings["ICShipmentReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["ICShipmentReport"].ToString();
                        listingReport = false;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.ICReceiptReport:
                        _reportName = ConfigurationManager.AppSettings["ICReceiptReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["ICReceiptReport"].ToString();
                        listingReport = false;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.GLTransactionsListReport:
                        _reportName = ConfigurationManager.AppSettings["GLTransactionsListReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["GLTransactionsListReport"].ToString();
                        listingReport = true;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.PDCCheckReport:
                        _reportName = ConfigurationManager.AppSettings["PDCCheckReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["PDCCheckReport"].ToString();
                        listingReport = true;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.IcAdjustmentReport:
                        _reportName = ConfigurationManager.AppSettings["IcAdjustmentReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["IcAdjustmentReport"].ToString();
                        listingReport = false;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.IcTransferReport:
                        _reportName = ConfigurationManager.AppSettings["IcTransferReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["IcTransferReport"].ToString();
                        listingReport = false;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.POSInvoiceReport:
                        _reportName = ConfigurationManager.AppSettings["InvoiceReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["InvoiceReport"].ToString();
                        listingReport = true;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.POSTotalDailySales:
                        _reportName = ConfigurationManager.AppSettings["TotalDailySales"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["TotalDailySales"].ToString();
                        listingReport = true;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.POSDiscountReport:
                        _reportName = ConfigurationManager.AppSettings["DiscountReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["DiscountReport"].ToString();
                        listingReport = true;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.POSDailySalesPerItem:
                        _reportName = ConfigurationManager.AppSettings["DailySalesPerItem"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["DailySalesPerItem"].ToString();
                        listingReport = true;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.POSDailysales:
                        _reportName = ConfigurationManager.AppSettings["Dailysales"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["Dailysales"].ToString();
                        listingReport = true;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.POSChequeDetails:
                        _reportName = ConfigurationManager.AppSettings["ChequeDetails"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["ChequeDetails"].ToString();
                        listingReport = true;
                        break;

                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.GLBatchListReport:
                        _reportName = ConfigurationManager.AppSettings["GLBatchListReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["GLBatchListReport"].ToString();
                        listingReport = true;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.POSGenerateBarcode:
                        _reportName = ConfigurationManager.AppSettings["GenerateBarcode"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["GenerateBarcode"].ToString();
                        listingReport = true;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.POSCloseRegisterReport:
                        _reportName = ConfigurationManager.AppSettings["CloseRegisterReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["CloseRegisterReport"].ToString();
                        listingReport = true;
                        break;

                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.ItemTransactionsReport:
                        _reportName = ConfigurationManager.AppSettings["ItemTransactionsReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["ItemTransactionsReport"].ToString();
                        listingReport = true;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.APPaymentVoucher:
                        _reportName = ConfigurationManager.AppSettings["APPaymentVoucher"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["APPaymentVoucher"].ToString();
                        listingReport = false;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.ARReceiptVoucherReport:
                        _reportName = ConfigurationManager.AppSettings["ARReceiptVoucherReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["ARReceiptVoucherReport"].ToString();
                        listingReport = false;
                        break;

                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.ChartOfAccounts:
                        _reportName = ConfigurationManager.AppSettings["ChartOfAccounts"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["ChartOfAccounts"].ToString();
                        listingReport = true;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.CustomerReport:
                        _reportName = ConfigurationManager.AppSettings["CustomerReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["CustomerReport"].ToString();
                        listingReport = true;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.VendorReport:
                        _reportName = ConfigurationManager.AppSettings["VendorReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["VendorReport"].ToString();
                        listingReport = true;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.ItemReport:
                        _reportName = ConfigurationManager.AppSettings["ItemReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["ItemReport"].ToString();
                        listingReport = true;
                        break;

                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.SummarySalesPerItem:
                        _reportName = ConfigurationManager.AppSettings["SummarySalesPerItem"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["SummarySalesPerItem"].ToString();
                        listingReport = true;
                        break;

                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.SalesReport:
                        _reportName = ConfigurationManager.AppSettings["SalesReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["SalesReport"].ToString();
                        listingReport = true;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.RollupAccountsReport:
                        _reportName = ConfigurationManager.AppSettings["RollupAccountsReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["RollupAccountsReport"].ToString();
                        listingReport = true;
                        break;

                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.CustomerBalanceReport:
                        _reportName = ConfigurationManager.AppSettings["CustomerBalanceReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["CustomerBalanceReport"].ToString();
                        listingReport = true;
                        break;

                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.VendorBalanceReport:
                        _reportName = ConfigurationManager.AppSettings["VendorBalanceReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["VendorBalanceReport"].ToString();
                        listingReport = true;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.IcAdjustmenteReport:
                        _reportName = ConfigurationManager.AppSettings["IcAdjustmenteReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["IcAdjustmenteReport"].ToString();
                        listingReport = true;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.POSCustomField:
                        _reportName = ConfigurationManager.AppSettings["POSCustomField"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["POSCustomField"].ToString();
                        listingReport = true;
                        break;

                    ////////po report
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.POReport:
                        _reportName = ConfigurationManager.AppSettings["POReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["POReport"].ToString();
                        listingReport = false;
                        break;

                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.POReceiptReport:
                        _reportName = ConfigurationManager.AppSettings["POReceiptReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["POReceiptReport"].ToString();
                        listingReport = false;
                        break;

                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.PoInvoiceReport:
                        _reportName = ConfigurationManager.AppSettings["PoInvoiceReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["PoInvoiceReport"].ToString();
                        listingReport = false;
                        break;

                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.POReceiptReturnReport:
                        _reportName = ConfigurationManager.AppSettings["POReceiptReturnReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["POReceiptReturnReport"].ToString();
                        listingReport = false;
                        break;

                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.OeOrderEntryReport:
                        _reportName = ConfigurationManager.AppSettings["OeOrderEntryReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["OeOrderEntryReport"].ToString();
                        listingReport = false;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.OeShipmentReport:
                        _reportName = ConfigurationManager.AppSettings["OeShipmentReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["OeShipmentReport"].ToString();
                        listingReport = false;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.OeInvoiceReport:
                        _reportName = ConfigurationManager.AppSettings["OeInvoiceReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["OeInvoiceReport"].ToString();
                        listingReport = false;
                        break;
                    case (int)CentrixERP.Configuration.Enums_S3.Reports.schema.OeShipmentReturnReport:
                        _reportName = ConfigurationManager.AppSettings["OeShipmentReturnReport"].ToString();
                        rvREXReport.ServerReport.ReportPath = reportFolderPath + ConfigurationManager.AppSettings["OeShipmentReturnReport"].ToString();
                        listingReport = false;
                        break;


                }
                string url = ConfigurationManager.AppSettings["ExportToPDFURL"].ToString() + _reportName.Substring(1) + "/" + _sessionPDFFileName; //HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + HttpContext.Current.Request.ApplicationPath + "/" + _sessionPDFFileName;
                iFrameURL = url;
                this.Page.Title = _reportName;
                rvREXReport.ShowPrintButton = true;
                rvREXReport.ShowRefreshButton = true;
                rvREXReport.ShowZoomControl = true;
                rvREXReport.ShowToolBar = true;

                //solution for asp.net expired issue
                //  this.rvREXReport. = false;
                //this.rvREXReport.AsyncRendering = false;

                if (ConfigurationManager.AppSettings["UseServerCredential"].ToString() != "0")
                    rvREXReport.ServerReport.ReportServerCredentials = new Credentials();
                GetParameters();
                //rvREXReport.ProcessingMode = ProcessingMode.Remote;
                List<ReportParameter> Prameterlist = list;
                rvREXReport.ServerReport.SetParameters(Prameterlist);
            }
            else
            {
                rvREXReport.Visible = false;
                lblPermissionError.Visible = true;
                // Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "script", "showStatusMsg('You don\'t have permission');", true);               
            }
            this.Page.DataBind();
        }
        void GetParameters()
        {
            JavaScriptSerializer serilizer = new JavaScriptSerializer();

            if (Request.QueryString.Count != 0)
            {
                object criteria = null;
                if (listingReport)
                {
                    if (!SF.Framework.String.IsEmpty(Request.QueryString[1]))
                    {
                        criteria = serilizer.Deserialize<object>(Request.QueryString[1]);

                        if (criteria != null)
                        {
                            Dictionary<string, Object> searchCriteria = (Dictionary<string, Object>)criteria;
                            foreach (KeyValuePair<string, Object> val in searchCriteria)
                            {
                                list.Add(new ReportParameter(val.Key, val.Value.ToString()));
                            }
                        }
                    }
                }
                else { list.Add(new ReportParameter(Request.QueryString.Keys[1], Request.QueryString[1])); }

            }
        }

        bool checkPermission()
        {
            int entityId = (int)Enums_S3.Entity.ARReport;
            if (cmReport)
                entityId = (int)Enums_S3.Entity.CMReport;
            RolePermissionCriteria criteria = new RolePermissionCriteria() { EntityId = entityId };
            List<RolePermissionLite> RolePermissionsList = iRolePermissionManager.FindAllRolePermission(LoggedInUser.RoleId, criteria);
            IEnumerable<bool> hasPermission = from RolePermissionLite rolePerm in RolePermissionsList where (rolePerm.EntityId == (int)entityId && rolePerm.HasPermission == true) select true;
            if (hasPermission.Count() > 0 && hasPermission.First())
                return true;
            else return false;

            return true;

        }
        public void OnRefreshReport(object sender, EventArgs e)
        {
            if (checkPermission())
                rvREXReport.ServerReport.Refresh();
            else
            {
                rvREXReport.Visible = false;
                lblPermissionError.Visible = true;
            }

        }
        public void OnReportError(object sender, EventArgs e) { lblReportError.Visible = true; rvREXReport.Visible = false; }

        protected void ShowPrintButton()
        {

            string script = "<SCRIPT LANGUAGE='JavaScript'> ";
            script += "showPrintButton()";
            script += "</SCRIPT>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowStatus", "javascript:showPrintButton();showDatePicker();", true);

        }

        protected void SavePDF()
        {


            string _reportPath = ConfigurationManager.AppSettings["ServerReportFolder"];
            string exportingPath = ConfigurationManager.AppSettings["ExportToPDFPath"];


            //delete all files in report folder
            if (Directory.Exists(exportingPath + _reportName))
            {
                string[] files = System.IO.Directory.GetFiles(exportingPath + _reportName);
                foreach (var file in files)
                {
                    try
                    {
                        System.IO.File.Delete(file);

                    }
                    catch (Exception)
                    {
                    }

                }

            }

            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string extension;
          rvREXReport.LocalReport.ReportPath = _reportPath + _reportName;
           
            //rvREXReport.LocalReport.ReportPath = "D:/SF-Projects/Development/CentrixERP/CentrixERPV3.3.27.Web/CentrixERP.Reports.BI/ICSalesReport";
            //byte[] bytes = rvREXReport.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamids, out warnings);

          byte[] bytes = rvREXReport.ServerReport.Render("PDF", "", out mimeType, out encoding, out extension, out streamids, out warnings);
            //save the pdf byte to the folder
            string fileName = exportingPath +  _reportName.Substring(1) + "\\" + _sessionPDFFileName;

            if (!Directory.Exists(exportingPath + _reportName.Substring(1)))
                Directory.CreateDirectory(exportingPath + _reportName.Substring(1));
            if (!File.Exists(fileName))
            {

                using (StreamWriter sw = new StreamWriter(File.Create(fileName)))
                {
                    sw.Write("");
                }
            }
            FileStream fs = new FileStream(fileName, FileMode.Open);

            byte[] data = new byte[fs.Length];
            fs.Write(bytes, 0, bytes.Length);
            fs.Close();
        }

        protected void rrvREXReport_ReportRefresh(object sender, System.ComponentModel.CancelEventArgs e)
        {
            ShowPrintButton();
        }

        public string AjaxCall(string name)
        {
            //var ajaxcall = Request.Form["ajaxcall"];
            if (name != null)
            {
                if (File.Exists(Server.MapPath(_sessionPDFFileName)))
                {
                    frmPrint.Attributes["src"] = "";
                    File.Delete(Server.MapPath(_sessionPDFFileName));
                }
                else
                {
                    SavePDF();
                }
            }
            return name;
        }

        string returnValue;

        string ICallbackEventHandler.GetCallbackResult()
        {
            return returnValue;
        }

        void ICallbackEventHandler.RaiseCallbackEvent(string eventArgument)
        {
            returnValue = AjaxCall(eventArgument);
        }

        private void RegisterClientsCallBackReference()
        {

            System.String myClientsCallBack = Page.ClientScript.GetCallbackEventReference(this, "arg", "ServerCallSucceeded", "context", "ServerCallFailed", true);
            //Could also call this wtihout the callback succeeded or failed methods:
            System.String myCompleteClientFunction = @"function CallServerMethodBeforePrint(arg, context)
                                      { " +
                                            myClientsCallBack + @"; 
                                      }";
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "TheScriptToCallServer", myCompleteClientFunction, true);

        }

        private void RegisterClientCallBackAfterPrint()
        {
            System.String myClientAfterPrintCallBack = Page.ClientScript.GetCallbackEventReference(this, "arg", "ServerCallSucceededAfterPrint", "context", "ServerCallFailed", true);

            System.String myAfterPrintCompleteClientFunction = @"function CallServerAfterPrint(arg, context)
                                      { " +
                                            myClientAfterPrintCallBack + @"; 
                                      }";
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "TheScriptToCallServerAfterPrint", myAfterPrintCompleteClientFunction, true);
        }

    }


    public class Credentials : IReportServerCredentials
    {
        #region IReportServerCredentials Members
        private string serverUserName = ConfigurationManager.AppSettings["ReportServerUserName"].ToString();
        private string serverPassword = ConfigurationManager.AppSettings["ReportServerPassword"].ToString();
        private string serverDomain = ConfigurationManager.AppSettings["ReportServerDomain"].ToString();
        public bool GetFormsCredentials(out System.Net.Cookie authCookie, out string userName, out string password, out string authority)
        {
            authority = null;
            authCookie = null;
            userName = null;
            password = null; ;
            return false;
        }

        public System.Security.Principal.WindowsIdentity ImpersonationUser
        {
            get { return null; }
        }

        public System.Net.ICredentials NetworkCredentials
        {
            get { return new NetworkCredential(serverUserName, serverPassword, serverDomain); }
        }

        #endregion
    }
}
