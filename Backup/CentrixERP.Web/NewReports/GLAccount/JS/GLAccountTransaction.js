﻿var reportSchema;
var ExportPageURL;
var AccountAC;

function InnerPage_Load() {
    initControls();
    setPageTitle(getResource("AccountTransactions"));
    reportSchema = systemConfig.ReportSchema.GLAccountTransactionsReport;
    setSelectedMenuItem('.general-ledger');
}

function initControls() {
    LoadDatePicker('input:.date');
    AccountAC = $('select:.lstAccount').AutoComplete({ serviceURL: systemConfig.ApplicationURL + systemConfig.accountServiceURL + "FindAllLite", multiSelect: "true" });
    $('.lnkReset').click(resetForm);
    $('.btnSearch').click(BuildSearchCriteria);
    $('.Quick-Search-Controls').live('keydown', function (e) {
        if (e.keyCode == 13 || e.which == 13) {
            $('.btnSearch').click();
        }
    });

    $('.btnSearch').attr('value', getResource("Search"));
    $('.lnkReset').attr('value', getResource("Reset"));
}

function BuildSearchCriteria() {
    if (!saveForm($('.Quick-Search-Controls'))) {
        return false;
    }

    $('.search-criteria').val('');
    var argsCriteria = new Object();

    if ($('input:.FromDate').val() != "")
        argsCriteria.from_date = getdate($('input:.FromDate').val());
    if ($('input:.ToDate').val() != "")
        argsCriteria.to_date = getdate($('input:.ToDate').val());

    if (AccountAC.get_ItemsIds() != "")
        argsCriteria.account_ids = AccountAC.get_ItemsIds();

    var SearchCriteria = '{';
    for (var key in argsCriteria) {
        if (argsCriteria.hasOwnProperty(key))
            if (argsCriteria[key] != "") {
                SearchCriteria = SearchCriteria + ('"' + key + '":' + '"' + argsCriteria[key] + '",');
            }
    }

    if (SearchCriteria != '{') {
        SearchCriteria = SearchCriteria.slice(0, -1);
    }
    SearchCriteria = SearchCriteria + '}';
    var exportPageURL = ExportPageURL + formatString("schemaID={0}&SearchCriteria={1}", reportSchema, SearchCriteria);
    window.open(exportPageURL, '_blank');
}

function resetForm() {
    $('input.date').val('');
    AccountAC.clear();
}

function getdate(date) {
    var arr = date.split('-');
    return arr[1] + '-' + arr[0] + '-' + arr[2];
}