﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CashFlowStatements.aspx.cs"
    Inherits="CentrixERP.Reports.Web.NewReports.GLAccount.CashFlowStatements" MasterPageFile="~/Common/MasterPage/ERPSite.master" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="PageHeader">
    <link href="../CSS/ReportCSS.css?v=<%=Centrix_Version %>" rel="stylesheet" type="text/css" />
    <script src="JS/CashFlowStatements.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <script type="text/javascript">
        ExportPageURL = '<%=ExportPageURL %>';
    </script>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="PageBody">
    <input runat="server" class="search-criteria" id="hdnSearchCriteria" type="hidden"
        value="" />
    <div class="">
        <div class="">
            <div class="Details-div">
                <span class="details-div-title" resourcekey=""></span>
                <div class="Action-div" style="display: none;">
                    <ul>
                    </ul>
                </div>
            </div>
            <table cellspacing="0" cellpadding="0" border="0" class="Data-Table Quick-Search-Controls">
                <tbody>
                    <tr>
                        <th>
                            <span class="label-title top-label" resourcekey="FromPeriod"></span>
                        </th>
                        <td>
                            <div>
                                <select class="FromPeriod lst">
                                </select></div>
                        </td>
                        <th>
                            <span class="label-title top-label" resourcekey="ToPeriod"></span>
                        </th>
                        <td>
                            <div>
                                <select class="ToPeriod lst">
                                </select></div>
                        </td>
                    </tr>
                    <tr>
                        <td class="search-buttons" colspan="4">
                            <div class="filter-reset">
                                <input type="button" class="btnSearch button2" id="btnSearch" value="Search" />
                                <input value="Reset" class="lnkReset button-cancel button2" type="button" />
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</asp:Content>
