﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="POSDailySalesPerItem.aspx.cs"
    Inherits="CentrixERP.Reports.Web.NewReports.IcQuantityOnHand.IcQuantityOnHand"
    MasterPageFile="~/Common/MasterPage/ERPSite.master" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="PageHeader">
    <script src="../../Common/DataDictionary/DataDictionary-ar.js" type="text/javascript"></script>
    <script src="../../Common/DataDictionary/DataDictionary-en.js" type="text/javascript"></script>
    <link href="../CSS/ReportCSS.css?v=<%=Centrix_Version %>" rel="stylesheet" type="text/css" />
    <script src="JS/POSDailySalesPerItem.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <script type="text/javascript">
        ExportPageURL = '<%=ExportPageURL %>';
    </script>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="PageBody">
    <div class="">
        <div class="">
            <div class="Details-div">
                <span class="details-div-title" resourcekey="AdvFilterYourResults"></span>
                <div class="Action-div">
                    <ul>
                    </ul>
                </div>
            </div>
            <table cellspacing="0" cellpadding="0" border="0" class="Data-Table">
                <tbody>
                    <tr>
                        <th>
                            <span class="label-title" resourcekey="Store"></span>
                        </th>
                        <td>
                            <select class="lst Store">
                            </select>
                        </td>
                          <th>
                            <span class="label-title" resourcekey="ItemNumber"></span>
                        </th>
                        <td>
                            <select class="lst ItemNumber">
                            </select>
                        </td>
                      
                    </tr>
                    <tr>
                      <th>
                            <span class="label-title" resourcekey="InvoiceNumberFrom"></span>
                        </th>
                        <td>
                            <input class="cx-control InvoiceNumber  string" type="text">
                        </td>
                        <th>
                            <span class="label-title" resourcekey="InvoiceNumberTo"></span>
                        </th>
                        <td>
                            <input class="cx-control InvoiceNumberTo  string" type="text">
                        </td>
                        
                    </tr>
                    <tr style="display: none;">
                        <th>
                            <span class="label-title" resourcekey="ItemStructure"></span>
                        </th>
                        <td>
                            <select class="lst ItemStructure">
                            </select>
                        </td>
                        <th>
                            <span class="label-title" resourcekey="Segment"></span>
                        </th>
                        <td>
                            <select class="lst Segment">
                            </select>
                        </td>
                    </tr>
                    <tr style="display: none;">
                        <th>
                            <span class="label-title" resourcekey="SegmentCodeFrom"></span>
                        </th>
                        <td>
                            <select class="lst SegmentCodeFrom">
                            </select>
                        </td>
                        <th>
                            <span class="label-title" resourcekey="SegmentCodeTo"></span>
                        </th>
                        <td>
                            <select class="lst SegmentCodeTo">
                            </select>
                        </td>
                    </tr>
                    <tr style="display: none">
                        <th>
                            <span id="span6" resourcekey="LessThanDiscount"></span>
                        </th>
                        <td>
                            <input class="cx-control txtLessThanDiscount  money" type="text" />
                        </td>
                        <th>
                            <span id="span7" resourcekey="GreaterThanDiscount" font-bold="true"></span>
                        </th>
                        <td>
                            <input class="cx-control txtGreaterThanDiscount  money" type="text" />
                        </td>
                    </tr>
                    <tr style="display: none;">
                        <th>
                            <span class="label-title" resourcekey="AllItem"></span>
                        </th>
                        <td>
                            <input type="checkbox" class="cx-control AllItem chk " />
                        </td>
                    </tr>
                    <tr>
                    <th>
                            <span class="label-title" resourcekey="InvoiceDateFrom"></span>
                        </th>
                        <td>
                            <input class="cx-control InvoiceDateFrom  date " type="text">
                        </td>
                        <th>
                            <span class="label-title" resourcekey="InvoiceDateTo"></span>
                        </th>
                        <td>
                            <input class="cx-control InvoiceDateTo  date  " type="text">
                        </td>
                        
                    </tr>
                  
                    <tr>
                        <td class="search-buttons" colspan="6">
                            <div class="filter-reset">
                                <input type="button" class="btnSearch button2" id="btnSearch" value="Search" />
                                <input value="Reset" class="lnkReset lnkReset-InvoiceInquiry button-cancel button2"
                                    type="button" />
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</asp:Content>
