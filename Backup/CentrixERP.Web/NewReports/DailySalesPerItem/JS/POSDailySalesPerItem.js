﻿var InvoiceReportSchema;
var ExportPageURL;
var FromLocationAC;
var ToLocationAC;
var FromCustomerAC;
var ToCustomerAC;
var ItemAC;


function InnerPage_Load() {
    initControls();
    setPageTitle(getResource("DailySalesPerItem"));
    InvoiceReportSchema = systemConfig.ReportSchema.DailySalesPerItem;
    setSelectedMenuItem('.pos-icon');
}

function initControls() {
    LoadDatePicker('input:.date', 'mm-dd-yy');
    ItemAC = $('select:.ItemNumber').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.ItemServiceURL + 'FindAllLite', required: false, multiSelect: true });

    ItemStructureAC = $('select:.ItemStructure').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.ItemStructureServiceURL + 'FindAllLite', required: false });

    SegmentAC = $('select:.Segment').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.IcSegmentServiceURL + 'FindAllLite', required: false });
    SegmentCodeFromAC = $('select:.SegmentCodeFrom').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.IcSegmentCodeServiceURL + 'FindAllLite', required: false });
    SegmentCodeToAC = $('select:.SegmentCodeTo').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.IcSegmentCodeServiceURL + 'FindAllLite', required: false});
    StoreAC = $('select:.Store').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.StoreServiceURL + 'FindAllLite', required: false, multiSelect: false });
    


    $('.lnkReset').click(resetForm);
    $('.btnSearch').click(BuildSearchCriteria);
    $('.Quick-Search-Controls').live('keydown', function (e) {
        if (e.keyCode == 13 || e.which == 13) {
            $('.btnSearch').click();
        }
    });

    $('.btnSearch').attr('value', getResource("Search"));
    $('.lnkReset').attr('value', getResource("Reset"));
}

function BuildSearchCriteria() {


    if (!saveForm($('.Quick-Search-Controls'))) {
        return false;
    }


    $('.search-criteria').val('');
    var argsCriteria = new Object();
    //argsCriteria.ExportPageURL = ExportPageURL;

    if (ItemAC.get_Item() != null) {
        var itemNumbers = '';
        $.each(ItemAC.get_Items(), function (index, itemm) {
            itemNumbers += itemm.ItemNumber + ',';

        });
        argsCriteria.item_numbers = itemNumbers;
    
    }
    if (ItemStructureAC.get_Item() != null)
        argsCriteria.item_structure = ItemStructureAC.get_Item().ItemStuctureId;
    if (SegmentAC.get_Item() != null)
        argsCriteria.segments = SegmentAC.get_Item().SegmentId;
    if (SegmentCodeFromAC.get_Item() != null)
        argsCriteria.segment_code_from = SegmentCodeFromAC.get_Item().SegmentCodeId;
    if (SegmentCodeToAC.get_Item() != null)
        argsCriteria.segment_code_to = SegmentCodeToAC.get_Item().SegmentCodeId;

    if (StoreAC.get_Item() != null) {
        argsCriteria.store_id = StoreAC.get_Item().value;
        argsCriteria.store_name = StoreAC.get_Item().label;
    }

    if ($('.InvoiceNumber').val() != null)
        argsCriteria.invoice_number = $('.InvoiceNumber').val();

    if ($('.InvoiceNumberTo').val() != null)
        argsCriteria.to_invoice_number = $('.InvoiceNumberTo').val();

      

    if ($('.txtGreaterThanDiscount').val() != null)
        argsCriteria.greater_than_discount = $('.txtGreaterThanDiscount').val();

    if ($('.txtLessThanDiscount').val() != null)
        argsCriteria.less_than_discount = $('.txtLessThanDiscount').val();


    if ($('.InvoiceDateFrom').val() != null)
        argsCriteria.from_date = $('.InvoiceDateFrom').val();
    if ($('.InvoiceDateTo').val() != null)
        argsCriteria.to_date = $('.InvoiceDateTo').val();
    argsCriteria.all_items = $('input.AllItem').prop("checked") ? true : false
        
    var SearchCriteria = '{';
    for (var key in argsCriteria) {
        if (argsCriteria.hasOwnProperty(key))
            if (argsCriteria[key] != "") {
                SearchCriteria = SearchCriteria + ('"' + key + '":' + '"' + argsCriteria[key] + '",');
            }
    }


    if (SearchCriteria != '{') {
        SearchCriteria = SearchCriteria.slice(0, -1);
    }
    SearchCriteria = SearchCriteria + '}';
    var exportPageURL = ExportPageURL + formatString("schemaID={0}&SearchCriteria={1}", InvoiceReportSchema, SearchCriteria);
    window.open(exportPageURL, '_blank');
}

function resetForm() {
    $('.InvoiceNumber,.InvoiceNumberTo').val('');
    $('.txtGreaterThanDiscount').val('');
    $('.txtGreaterThanDiscount').val('');
    $('.InvoiceDateFrom').val('');
    $('.InvoiceDateTo').val('')
    $('input.AllItem').attr("checked", false);
    ItemStructureAC.clear()
    SegmentAC.clear()
    SegmentCodeFromAC.clear()
    SegmentCodeToAC.clear();
    StoreAC.clear();
    ItemAC.clear();



  
}
    


function getdate(date) {
    var arr = date.split('-');
    return arr[1] + '-' + arr[0] + '-' + arr[2];
}