﻿var QuantityOnHandReportSchema;
var ExportPageURL;
var FromLocationAC;
var ToLocationAC;
var FromCustomerAC;
var ToCustomerAC;

function InnerPage_Load() {
    initControls();
    setPageTitle(getResource("IcQuantityOnHandReport"));
    //QuantityOnHandReportSchema = systemConfig.ReportSchema.IcAdjustmentReport;
    QuantityOnHandReportSchema = 79;
    
    setSelectedMenuItem('.inventory-icon');
}

function initControls() {

    LoadDatePicker('input:.date', 'mm-dd-yy');
    AdjustmentTypeAC = fillDataTypeContentList('select:.AdjustmentType', systemConfig.dataTypes.AdjustmentType, true);

    $('.lnkReset').click(resetForm);
    $('.btnSearch').click(BuildSearchCriteria);
    $('.Quick-Search-Controls').live('keydown', function (e) {
        if (e.keyCode == 13 || e.which == 13) {
            $('.btnSearch').click();
        }
    });

    $('.btnSearch').attr('value', getResource("Search"));
    $('.lnkReset').attr('value', getResource("Reset"));
}

function BuildSearchCriteria() {


    if (!saveForm($('.Quick-Search-Controls'))) {
        return false;
    }


    $('.search-criteria').val('');
    var argsCriteria = new Object();
    //argsCriteria.ExportPageURL = ExportPageURL;

    if (AdjustmentTypeAC.get_Item() != null)
        argsCriteria.adjustment_type = AdjustmentTypeAC.get_ItemValue();

    if ($('.AdjustmentNumber').val() != null)
        argsCriteria.adjustment_number = $('.AdjustmentNumber').val();
    if ($('.AdjustmentDateFrom').val() != null)
        argsCriteria.adjustment_date_from = $('.AdjustmentDateFrom').val();
    if ($('.AdjustmentDateTo').val() != null)
        argsCriteria.adjustment_date_to = $('.AdjustmentDateTo').val();
    if ($('.YearPeriod').val() != null)
        argsCriteria.period = $('.YearPeriod').val();

    var SearchCriteria = '{';
    for (var key in argsCriteria) {
        if (argsCriteria.hasOwnProperty(key))
            if (argsCriteria[key] != "") {
                SearchCriteria = SearchCriteria + ('"' + key + '":' + '"' + argsCriteria[key] + '",');
            }
    }


    if (SearchCriteria != '{') {
        SearchCriteria = SearchCriteria.slice(0, -1);
    }
    SearchCriteria = SearchCriteria + '}';
    var exportPageURL = ExportPageURL + formatString("schemaID={0}&SearchCriteria={1}", QuantityOnHandReportSchema, SearchCriteria);
    debugger;
    window.open(exportPageURL, '_blank');
}

function resetForm() {
    AdjustmentTypeAC.clear();
    $('.AdjustmentNumber').val('');
    $('.AdjustmentDateFrom').val('');
    $('.AdjustmentDateTo').val('');
    $('.YearPeriod').val('');

}


function getdate(date) {
    var arr = date.split('-');
    return arr[1] + '-' + arr[0] + '-' + arr[2];
}