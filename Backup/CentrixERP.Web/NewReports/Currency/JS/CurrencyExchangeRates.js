var CurrencyExchageReportSchema;
var ExportPageURL;
var FromCurrencyAC;
var ToCurrencyAC;

function InnerPage_Load() {
    initControls();
    setPageTitle(getResource("CurrencyExchangeRates"));
    CurrencyExchageReportSchema = systemConfig.ReportSchema.CurrencyExchangeRateReport;
    setSelectedMenuItem('.administrative-settings');
}

function initControls() {

    FromCurrencyAC = $('select:.fromCurrency').AutoComplete({ serviceURL: systemConfig.ApplicationURL + systemConfig.CurrencyWebService + 'FindAllLite', required: true });
    ToCurrencyAC = $('select:.toCurrency').AutoComplete({ serviceURL: systemConfig.ApplicationURL + systemConfig.CurrencyWebService + 'FindAllLite', required: true });
    LoadDatePicker('input:.date');
    $('.lnkReset').click(resetForm);
    $('.btnSearch').click(BuildSearchCriteria);
    $('.Quick-Search-Controls').live('keydown', function (e) {
        if (e.keyCode == 13 || e.which == 13) {
            $('.btnSearch').click();
        }
    });

    $('.btnSearch').attr('value',getResource("Search"));
    $('.lnkReset').attr('value', getResource("Reset"));
}

function BuildSearchCriteria() {


    if (!saveForm($('.Quick-Search-Controls'))) {
        return false;
    }


    $('.search-criteria').val('');
    var argsCriteria = new Object();
    //argsCriteria.ExportPageURL = ExportPageURL;

    if (FromCurrencyAC.get_Item() != null)
        argsCriteria.from_currency_id = FromCurrencyAC.get_ItemValue();

    if (ToCurrencyAC.get_Item() != null)
        argsCriteria.to_currency_id = ToCurrencyAC.get_ItemValue();

    if ($('input:.FromRateDate').val() != "")
        argsCriteria.rate_date_from = getdate($('input:.FromRateDate').val());
    if ($('input:.ToRateDate').val() != "")
        argsCriteria.rate_date_to = getdate($('input:.ToRateDate').val());  

    var SearchCriteria = '{';
    for (var key in argsCriteria) {
        if (argsCriteria.hasOwnProperty(key))
            if (argsCriteria[key] != "") {
                SearchCriteria = SearchCriteria + ('"' + key + '":' + '"' + argsCriteria[key] + '",');
            }
    }


    if (SearchCriteria != '{') {
        SearchCriteria = SearchCriteria.slice(0, -1);
    }
    SearchCriteria = SearchCriteria + '}';
    var exportPageURL = ExportPageURL + formatString("schemaID={0}&SearchCriteria={1}", CurrencyExchageReportSchema, SearchCriteria);
    window.open(exportPageURL, '_blank');
}

function resetForm() {
    $('input.date').val('');
    FromCurrencyAC.clear();
    ToCurrencyAC.clear();
}


function getdate(date) {
    var arr = date.split('-');
    return arr[1] + '-' + arr[0] + '-' + arr[2];
}