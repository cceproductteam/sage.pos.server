<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CurrencyExchangeRates.aspx.cs" Inherits="CentrixERP.Reports.Web.NewReports.Currency.CurrencyExchangeRates" MasterPageFile="~/Common/MasterPage/ERPSite.master" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="PageHeader">
    <link href="../CSS/ReportCSS.css?v=<%=Centrix_Version %>" rel="stylesheet" type="text/css" />
    <script src="JS/CurrencyExchangeRates.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <script type="text/javascript">
        ExportPageURL = '<%=ExportPageURL %>';
    </script>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="PageBody">
<div class="">
        <div class="">
            <div class="Details-div">
                <span class="details-div-title" resourcekey="AdvFilterYourResults"></span>
                <div class="Action-div">
                    <ul>
                    </ul>
                </div>
            </div>
            <table cellspacing="0" cellpadding="0" border="0" class="Data-Table">
                <tbody>
                    <tr>
                        <th>
                            <span class="label-title" resourcekey="FromCurrency"></span>
                        </th>
                        <td>
                            <select class="lst fromCurrency">
                            </select>
                        </td>
                         
                     <th>
                            <span class="label-title" resourcekey="ToCurrency"></span>
                        </th>
                        <td>
                            <select class="lst toCurrency">
                            </select>
                        </td>
                    </tr>
                    <tr>
                      <th>
                            <span class="label-title" resourcekey="RateDateFrom"></span>
                        </th>
                        <td>
                           <input type="text" class="FromRateDate cx-control date" />
                        </td>
                     <th>
                            <span class="label-title" resourcekey="RateDateTo"></span>
                        </th>
                        <td>
                           <input type="text" class="ToRateDate cx-control date" />
                        </td>
                         
                    </tr>
                    <tr>
                        <td class="search-buttons" colspan="6">
                            <div class="filter-reset">
                                <input type="button" class="btnSearch button2" id="btnSearch" value="Search" />
                                <input value="Reset" class="lnkReset button-cancel button2" type="button" />
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</asp:Content>