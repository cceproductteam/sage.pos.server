using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CentrixERP.Common.Web;
using System.Configuration;

namespace CentrixERP.Reports.Web.NewReports.Currency
{
    public partial class CurrencyExchangeRates : BasePageLoggedIn
    {
        public CurrencyExchangeRates()
            : base(CentrixERP.Configuration.Enums_S3.Permissions.SystemModules.None) { }
        public string ExportPageURL;
        protected void Page_Load(object sender, EventArgs e)
        {
            ExportPageURL = ConfigurationManager.AppSettings["ExportPageURL"].ToString();
        }
    }
}