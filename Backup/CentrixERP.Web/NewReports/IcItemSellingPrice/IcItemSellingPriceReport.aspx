﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="IcItemSellingPriceReport.aspx.cs" Inherits="CentrixERP.Reports.Web.NewReports.IcItemSellingPrice.IcItemSellingPriceReport" MasterPageFile="~/Common/MasterPage/ERPSite.master" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="PageHeader">
    <script src="../../Common/DataDictionary/DataDictionary-ar.js" type="text/javascript"></script>
    <script src="../../Common/DataDictionary/DataDictionary-en.js" type="text/javascript"></script>
    <link href="../CSS/ReportCSS.css?v=<%=Centrix_Version %>" rel="stylesheet" type="text/css" />
    <script src="JS/IcItemSellingPrice.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <script type="text/javascript">
        ExportPageURL = '<%=ExportPageURL %>';
    </script>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="PageBody">
<div class="">
        <div class="">
            <div class="Details-div">
                <span class="details-div-title" resourcekey="AdvFilterYourResults"></span>
                <div class="Action-div">
                    <ul>
                    </ul>
                </div>
            </div>
            <table cellspacing="0" cellpadding="0" border="0" class="Data-Table">
                <tbody>
                    <tr>
                        <th>
                            <span class="label-title" resourcekey="FromPriceList"></span>
                        </th>
                        <td>
                            <select class="lst FromPriceList">
                            </select>
                        </td>
                         
                     <th>
                            <span class="label-title" resourcekey="ToPriceList"></span>
                        </th>
                        <td>
                            <select class="lst ToPriceList">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <span class="label-title" resourcekey="FromCurrency"></span>
                        </th>
                        <td>
                            <select class="lst FromCurrency">
                            </select>
                        </td>
                         
                     <th>
                            <span class="label-title" resourcekey="ToCurrency"></span>
                        </th>
                        <td>
                            <select class="lst ToCurrency">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <span class="label-title" resourcekey="FromItem"></span>
                        </th>
                        <td>
                            <select class="lst FromItem">
                            </select>
                        </td>
                         
                     <th>
                            <span class="label-title" resourcekey="ToItem"></span>
                        </th>
                        <td>
                            <select class="lst ToItem">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <span class="label-title" resourcekey="FromLocation"></span>
                        </th>
                        <td>
                            <select class="lst FromLocation">
                            </select>
                        </td>
                         
                     <th>
                            <span class="label-title" resourcekey="ToLocation"></span>
                        </th>
                        <td>
                            <select class="lst ToLocation">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="search-buttons" colspan="6">
                            <div class="filter-reset">
                                <input type="button" class="btnSearch button2" id="btnSearch" value="Search" />
                                <input value="Reset" class="lnkReset button-cancel button2" type="button" />
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</asp:Content>
