﻿var IcItemSellingPriceReportSchema;
var ExportPageURL;
var FromCurrencyAC;
var ToCurrencyAC;
var FromPriceListAC;
var ToPriceListAC;
var FromItemAC, ToItemAC, FromLocationAC, ToLocationAC;

function InnerPage_Load() {
    initControls();
    setPageTitle(getResource("IcItemSellingPriceReport"));
    IcItemSellingPriceReportSchema = systemConfig.ReportSchema.IcItemSellingPriceReport;
    setSelectedMenuItem('.inventory-icon');
}

function initControls() {

    FromPriceListAC = $('select:.FromPriceList').AutoComplete({ serviceURL: systemConfig.ApplicationURL + systemConfig.IcPriceListServiceURL + 'FindAllLite', required: true });
    ToPriceListAC = $('select:.ToPriceList').AutoComplete({ serviceURL: systemConfig.ApplicationURL + systemConfig.IcPriceListServiceURL + 'FindAllLite', required: true });
    FromCurrencyAC = $('select:.FromCurrency').AutoComplete({ serviceURL: systemConfig.ApplicationURL + systemConfig.CurrencyWebService + 'FindAllLite', required: true });
    ToCurrencyAC = $('select:.ToCurrency').AutoComplete({ serviceURL: systemConfig.ApplicationURL + systemConfig.CurrencyWebService + 'FindAllLite', required: true });

    FromItemAC = $('select:.FromItem').AutoComplete({ serviceURL: systemConfig.ApplicationURL + systemConfig.IcItemCardServiceURL + 'FindAllLite', required: true });
    ToItemAC = $('select:.ToItem').AutoComplete({ serviceURL: systemConfig.ApplicationURL + systemConfig.IcItemCardServiceURL + 'FindAllLite', required: true });
    FromLocationAC = $('select:.FromLocation').AutoComplete({ serviceURL: systemConfig.ApplicationURL + systemConfig.IcLocationServiceURL + 'FindAllLite', required: true });
    ToLocationAC = $('select:.ToLocation').AutoComplete({ serviceURL: systemConfig.ApplicationURL + systemConfig.IcLocationServiceURL + 'FindAllLite', required: true });

    LoadDatePicker('input:.date');

    $('.fractional-numeric').autoNumeric();
    $('.numeric').autoNumeric({ aSep: ',', aNeg: '', mDec: 0, mRound: 'S', mNum: 10 });

    $('.lnkReset').click(resetForm);
    $('.btnSearch').click(BuildSearchCriteria);
    $('.Quick-Search-Controls').live('keydown', function (e) {
        if (e.keyCode == 13 || e.which == 13) {
            $('.btnSearch').click();
        }
    });

    $('.btnSearch').attr('value', getResource("Search"));
    $('.lnkReset').attr('value', getResource("Reset"));
}

function BuildSearchCriteria() {

    if (!saveForm($('.Quick-Search-Controls'))) {
        return false;
    }

    $('.search-criteria').val('');
    var argsCriteria = new Object();
    //argsCriteria.ExportPageURL = ExportPageURL;

    if (FromPriceListAC.get_Item() != null)
        argsCriteria.from_price_list_id = FromPriceListAC.get_ItemValue();
    if (ToPriceListAC.get_Item() != null)
        argsCriteria.to_price_list_id = ToPriceListAC.get_ItemValue();

    if (FromCurrencyAC.get_Item() != null)
        argsCriteria.from_currency_id = FromCurrencyAC.get_ItemValue();
    if (ToCurrencyAC.get_Item() != null)
        argsCriteria.to_currency_id = ToCurrencyAC.get_ItemValue();

    if (FromItemAC.get_Item() != null)
        argsCriteria.from_item_id = FromItemAC.get_ItemValue();
    if (ToItemAC.get_Item() != null)
        argsCriteria.to_item_id = ToItemAC.get_ItemValue();

    if (FromLocationAC.get_Item() != null)
        argsCriteria.from_location_id = FromLocationAC.get_ItemValue();
    if (ToLocationAC.get_Item() != null)
        argsCriteria.to_location_id = ToLocationAC.get_ItemValue();

    var SearchCriteria = '{';
    for (var key in argsCriteria) {
        if (argsCriteria.hasOwnProperty(key))
            if (argsCriteria[key] != "") {
                SearchCriteria = SearchCriteria + ('"' + key + '":' + '"' + argsCriteria[key] + '",');
            }
    }

    if (SearchCriteria != '{') {
        SearchCriteria = SearchCriteria.slice(0, -1);
    }
    SearchCriteria = SearchCriteria + '}';
    var exportPageURL = ExportPageURL + formatString("schemaID={0}&SearchCriteria={1}", IcItemSellingPriceReportSchema, SearchCriteria);
    debugger;
    window.open(exportPageURL, '_blank');
}

function resetForm() {
    FromPriceListAC.clear();
    ToPriceListAC.clear();
    FromCurrencyAC.clear();
    ToCurrencyAC.clear();

    FromItemAC.clear();
    ToItemAC.clear();
    FromLocationAC.clear();
    ToLocationAC.clear();
}

function getdate(date) {
    var arr = date.split('-');
    return arr[1] + '-' + arr[0] + '-' + arr[2];
}