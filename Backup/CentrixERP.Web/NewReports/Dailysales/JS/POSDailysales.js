﻿var InvoiceReportSchema;
var ExportPageURL;
var FromLocationAC;
var ToLocationAC;
var FromCustomerAC;
var ToCustomerAC;
var RegisterAC, CustomerAC, PersonAC;

function InnerPage_Load() {
    initControls();
    setPageTitle(getResource("Dailysales"));
    InvoiceReportSchema = systemConfig.ReportSchema.Dailysales;
    setSelectedMenuItem('.pos-icon');
}

function initControls() {
    LoadDatePicker('input:.date', 'mm-dd-yy');
    ItemAC = $('select:.ItemNumber').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.ItemServiceURL + 'FindAllLite', required: false, multiSelect: true });
    StoreAC = $('select:.Store').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.StoreServiceURL + 'FindAllLite', required: false, multiSelect: false });
    RegisterAC = $('select:.Register').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.RegisterServiceURL + 'FindAllLite', required: false, multiSelect: true });
    CustomerAC = $('select:.Customer').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.PersonServiceURL + 'FindAllLite', required: false, multiSelect: true });
    PersonAC = $('select:.Person').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.UsersServiceURL + 'FindUserAllLite', required: false, multiSelect: true });
    $('.lnkReset').click(resetForm);
    $('.btnSearch').click(BuildSearchCriteria);
    $('.Quick-Search-Controls').live('keydown', function (e) {
        if (e.keyCode == 13 || e.which == 13) {
            $('.btnSearch').click();
        }
    });

    $('.btnSearch').attr('value', getResource("Search"));
    $('.lnkReset').attr('value', getResource("Reset"));
}

function BuildSearchCriteria() {


    if (!saveForm($('.Quick-Search-Controls'))) {
        return false;
    }


    $('.search-criteria').val('');
    var argsCriteria = new Object();
    //argsCriteria.ExportPageURL = ExportPageURL;

    if (ItemAC.get_Item() != null) {
        var itemNumbers = '';
        $.each(ItemAC.get_Items(), function (index, itemm) {
            itemNumbers += itemm.ItemNumber + ',';

        });
        argsCriteria.item_number = itemNumbers;

    }
   

    if ($('.InvoiceDateTo').val() != null)
        argsCriteria.to_date = $('.InvoiceDateTo').val();

    if ($('.InvoiceDateFrom').val() != null)
        argsCriteria.from_date = $('.InvoiceDateFrom').val();

    if (StoreAC.get_Item() != null) {
        argsCriteria.store_id = StoreAC.get_Item().value;
        argsCriteria.store_name = StoreAC.get_Item().label;
    }
    if (RegisterAC.get_Item() != null)
        argsCriteria.register_id = RegisterAC.get_ItemsIds();
    if (CustomerAC.get_Item() != null)
        argsCriteria.customer_id = CustomerAC.get_ItemsIds();
    if (PersonAC.get_Item() != null)
        argsCriteria.sales_man_ids = PersonAC.get_ItemsIds();

    if ($('.InvoiceNumber').val() != null) {
        argsCriteria.invoice_number = $('.InvoiceNumber').val();
    }

    if ($('.InvoiceNumberTo').val() != null) {
        argsCriteria.invoice_number_to = $('.InvoiceNumberTo').val();
    }
        
    var SearchCriteria = '{';
    for (var key in argsCriteria) {
        if (argsCriteria.hasOwnProperty(key))
            if (argsCriteria[key] != "") {
                SearchCriteria = SearchCriteria + ('"' + key + '":' + '"' + argsCriteria[key] + '",');
            }
    }


    if (SearchCriteria != '{') {
        SearchCriteria = SearchCriteria.slice(0, -1);
    }
    SearchCriteria = SearchCriteria + '}';
    var exportPageURL = ExportPageURL + formatString("schemaID={0}&SearchCriteria={1}", InvoiceReportSchema, SearchCriteria);
    window.open(exportPageURL, '_blank');
}

function resetForm() {
    StoreAC.clear();
    RegisterAC.clear();
    CustomerAC.clear();
    ItemAC.clear();
    PersonAC.clear();
    $('.ItemNumber').val('');
    $('.InvoiceDateTo').val('');
    $('.InvoiceDateFrom').val('');
    $('.InvoiceNumberTo,.InvoiceNumber').val('');
}


function getdate(date) {
    var arr = date.split('-');
    return arr[1] + '-' + arr[0] + '-' + arr[2];
}