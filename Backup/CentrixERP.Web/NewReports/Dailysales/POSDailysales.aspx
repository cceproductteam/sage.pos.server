﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="POSDailysales.aspx.cs"
    Inherits="CentrixERP.Reports.Web.NewReports.IcQuantityOnHand.IcQuantityOnHand"
    MasterPageFile="~/Common/MasterPage/ERPSite.master" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="PageHeader">
    <script src="../../Common/DataDictionary/DataDictionary-ar.js" type="text/javascript"></script>
    <script src="../../Common/DataDictionary/DataDictionary-en.js" type="text/javascript"></script>
    <link href="../CSS/ReportCSS.css?v=<%=Centrix_Version %>" rel="stylesheet" type="text/css" />
    <script src="JS/POSDailysales.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <script type="text/javascript">
        ExportPageURL = '<%=ExportPageURL %>';
    </script>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="PageBody">
    <div class="">
        <div class="">
            <div class="Details-div">
                <span class="details-div-title" resourcekey="AdvFilterYourResults"></span>
                <div class="Action-div">
                    <ul>
                    </ul>
                </div>
            </div>
            <table cellspacing="0" cellpadding="0" border="0" class="Data-Table">
                <tbody>
                    <tr style="display: none;">
                        <th>
                            <span class="label-title" id="span4" resourcekey="Person" font-bold="true"></span>
                        </th>
                        <td>
                            <select class="lst Person">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <span class="label-title" resourcekey="Store"></span>
                        </th>
                        <td>
                            <select class="lst Store">
                            </select>
                        </td>
                        <th>
                            <span class="label-title" resourcekey="ItemNumber"></span>
                        </th>
                        <td>
                            <select class="lst ItemNumber">
                            </select>
                        </td>
                        <th style="display: none;">
                            <span class="label-title" resourcekey="Register"></span>
                        </th>
                        <td style="display: none;">
                            <select class="lst Register">
                            </select>
                        </td>
                    </tr>
                    <tr style="display: none">
                        <th>
                            <span class="label-title" id="span2" resourcekey="Customer"></span>
                        </th>
                        <td>
                            <select class="lst Customer">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <span class="label-title" resourcekey="InvoiceNumberFrom"></span>
                        </th>
                        <td>
                            <input class="cx-control InvoiceNumber  string" type="text">
                        </td>
                        <th>
                            <span class="label-title" resourcekey="InvoiceNumberTo"></span>
                        </th>
                        <td>
                            <input class="cx-control InvoiceNumberTo  string" type="text">
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <span class="label-title" resourcekey="InvoiceDateFrom"></span>
                        </th>
                        <td>
                            <input class="cx-control InvoiceDateFrom  date " type="text">
                        </td>
                        <th>
                            <span class="label-title" resourcekey="InvoiceDateTo"></span>
                        </th>
                        <td>
                            <input class="cx-control InvoiceDateTo  date  " type="text">
                        </td>
                    </tr>
                    <tr>
                        <td class="search-buttons" colspan="6">
                            <div class="filter-reset">
                                <input type="button" class="btnSearch button2" id="btnSearch" value="Search" />
                                <input value="Reset" class="lnkReset lnkReset-InvoiceInquiry button-cancel button2"
                                    type="button" />
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</asp:Content>
