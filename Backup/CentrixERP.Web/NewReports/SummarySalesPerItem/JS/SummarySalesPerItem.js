﻿var InvoiceReportSchema;
var ExportPageURL;
var FromLocationAC;
var ToLocationAC;
var FromCustomerAC;
var ToCustomerAC;
var ItemAC;

function InnerPage_Load() {
    initControls();
    setPageTitle(getResource("SummarySalesPerItem"));
    InvoiceReportSchema = systemConfig.ReportSchema.SummarySalesPerItem;
    setSelectedMenuItem('.pos-icon');
}

function initControls() {
    LoadDatePicker('input:.date', 'mm-dd-yy');
    ItemFromAC = $('select:.ItemFrom').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.ItemServiceURL + 'FindAllLite', required: false });
    ItemToAC = $('select:.ItemTo').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.ItemServiceURL + 'FindAllLite', required: false });

    StoreFromAC = $('select:.StoreFrom').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.StoreServiceURL + 'FindAllLite', required: false });
    StoreToAC = $('select:.StoreTo').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.StoreServiceURL + 'FindAllLite', required: false });

    RegisterFromAC = $('select:.RegisterFrom').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.RegisterServiceURL + 'FindAllLite', required: false });
    RegisterToAC = $('select:.RegisterTo').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.RegisterServiceURL + 'FindAllLite', required: false });








    $('.lnkReset').click(resetForm);
    $('.btnSearch').click(BuildSearchCriteria);
    $('.Quick-Search-Controls').live('keydown', function (e) {
        if (e.keyCode == 13 || e.which == 13) {
            $('.btnSearch').click();
        }
    });

    $('.btnSearch').attr('value', getResource("Search"));
    $('.lnkReset').attr('value', getResource("Reset"));
}

function BuildSearchCriteria() {


    if (!saveForm($('.Quick-Search-Controls'))) {
        return false;
    }


    $('.search-criteria').val('');
    var argsCriteria = new Object();
    //argsCriteria.ExportPageURL = ExportPageURL;


    if (ItemFromAC.get_Item() != null) {

        argsCriteria.item_from = ItemFromAC.get_Item().ItemCardId;

    }
    if (ItemToAC.get_Item() != null) {

        argsCriteria.item_to = ItemToAC.get_Item().ItemCardId;

    }

    if (StoreFromAC.get_Item() != null) {

        argsCriteria.store_from = StoreFromAC.get_Item().StoreId;

    }
    if (StoreToAC.get_Item() != null) {

        argsCriteria.store_to = StoreToAC.get_Item().StoreId;

    }

    if (RegisterFromAC.get_Item() != null) {

        argsCriteria.register_from = RegisterFromAC.get_Item().RegisterId;

    }

    if (RegisterToAC.get_Item() != null) {

        argsCriteria.register_to = RegisterToAC.get_Item().RegisterId;

    }




    if ($('.InvoiceDateFrom').val() != null)
        argsCriteria.from_date = $('.InvoiceDateFrom').val();
    if ($('.InvoiceDateTo').val() != null)
        argsCriteria.to_date = $('.InvoiceDateTo').val();

    var SearchCriteria = '{';
    for (var key in argsCriteria) {
        if (argsCriteria.hasOwnProperty(key))
            if (argsCriteria[key] != "") {
                SearchCriteria = SearchCriteria + ('"' + key + '":' + '"' + argsCriteria[key] + '",');
            }
    }


    if (SearchCriteria != '{') {
        SearchCriteria = SearchCriteria.slice(0, -1);
    }
    SearchCriteria = SearchCriteria + '}';
    var exportPageURL = ExportPageURL + formatString("schemaID={0}&SearchCriteria={1}", InvoiceReportSchema, SearchCriteria);
    debugger;
    window.open(exportPageURL, '_blank');
}

function resetForm() {

    $('.DateFrom').val('');
    $('.DateTo').val('')

    ItemFromAC.clear()
    ItemToAC.clear()
    StoreFromAC.clear()
    StoreToAC.clear()

    RegisterFromAC.clear()
    RegisterToAC.clear()

}



function getdate(date) {
    var arr = date.split('-');
    return arr[1] + '-' + arr[0] + '-' + arr[2];
}