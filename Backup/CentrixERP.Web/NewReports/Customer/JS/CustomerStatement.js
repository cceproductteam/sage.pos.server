var CustomerStatementReportSchema;
var ExportPageURL;
var CustomerAc;

function InnerPage_Load() {
    setSelectedMenuItem('.account-receivable');
    initControls();
    setPageTitle(getResource('CustomerStatementReport'));
    CustomerStatementReportSchema = systemConfig.ReportSchema.CustomerStatementReport;
}

function initControls() {

    CustomerAc = $('select:.Customer').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.CustomerWebService + 'FindAllLite', required: true });
    LoadDatePicker('input:.date');
    $('.lnkReset').click(resetForm);
    $('.btnSearch').click(BuildSearchCriteria);
    $('.Quick-Search-Controls').live('keydown', function (e) {
        if (e.keyCode == 13 || e.which == 13) {
            $('.btnSearch').click();
        }
    });
    $('.btnSearch').attr('value', getResource("Search"));
    $('.lnkReset').attr('value', getResource("Reset"));
}

function BuildSearchCriteria() {


    if (!saveForm($('.Quick-Search-Controls'))) {
        return false;
    }


    $('.search-criteria').val('');
    var argsCriteria = new Object();
    //argsCriteria.ExportPageURL = ExportPageURL;

    argsCriteria.customer_id = CustomerAc.get_ItemValue();
    if ($('input.From').val())
        argsCriteria.from_date = getdate($('input.From').val());
    if ($('input.To').val())
        argsCriteria.to_date = getdate($('input.To').val());

    argsCriteria.exportpage = ExportPageURL;
    var SearchCriteria = '{';
    for (var key in argsCriteria) {
        if (argsCriteria.hasOwnProperty(key))
            if (argsCriteria[key] != "") {
                SearchCriteria = SearchCriteria + ('"' + key + '":' + '"' + argsCriteria[key] + '",');
            }
    }

    

    if (SearchCriteria != '{') {
        SearchCriteria = SearchCriteria.slice(0, -1);
    }
    SearchCriteria = SearchCriteria + '}';
    var exportPageURL = ExportPageURL + formatString("schemaID={0}&SearchCriteria={1}", CustomerStatementReportSchema, SearchCriteria);
    window.open(exportPageURL, '_blank');
}

function resetForm() {
    $('input.date').val('');
    CustomerAc.clear();
}


function getdate(date) {
    var arr = date.split('-');
    return arr[1] + '-' + arr[0] + '-' + arr[2];
}