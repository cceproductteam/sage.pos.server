var CustomerStatementReportSchema;
var ExportPageURL;
var CustomerAc;

function InnerPage_Load() {
    initControls();
    setPageTitle('Customer Statement report');
    CustomerStatementReportSchema = systemConfig.ReportSchema.CustomerStatementReport;
    setSelectedMenuItem('.Report-icon');
}

function initControls() {

    CustomerAc = $('select:.Customer').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.CustomerWebService + 'FindAllLite', required: true });
    LoadDatePicker('input:.date');
    $('.lnkReset').click(resetForm);
    $('.btnSearch').click(BuildSearchCriteria);
    $('.Quick-Search-Controls').live('keydown', function (e) {
        if (e.keyCode == 13 || e.which == 13) {
            $('.btnSearch').click();
        }
    });
}

function BuildSearchCriteria() {


    if (!saveForm($('.Quick-Search-Controls'))) {
        return false;
    }


    $('.search-criteria').val('');
    var argsCriteria = new Object();
    argsCriteria.DetailsSchemaID = CustomerStatementReportSchema;
    argsCriteria.ExportPageURL = ExportPageURL;

    argsCriteria.CustomerId = CustomerAc.get_ItemValue();
    argsCriteria.From = $("input.From").val();
    argsCriteria.To = $("input.To").val();


    var SearchCriteria = '{';
    for (var key in argsCriteria) {
        if (argsCriteria.hasOwnProperty(key))
            if (argsCriteria[key] != "") {
                SearchCriteria = SearchCriteria + ('"' + key + '":' + '"' + argsCriteria[key] + '",');
            }
    }


    if (SearchCriteria != '{') {
        SearchCriteria = SearchCriteria.slice(0, -1);
    }
    SearchCriteria = SearchCriteria + '}';
    var exportPageURL = ExportPageURL + formatString("schemaID={0}&SearchCriteria={1}", CustomerStatementReportSchema, SearchCriteria);
    window.open(exportPageURL, '_blank');
}

function resetForm() {
    $('input.date').val('');
    CustomerAc.clear();
}
