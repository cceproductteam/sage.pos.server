<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomerTransactions.aspx.cs"
    Inherits="CentrixERP.Reports.Web.NewReports.Customer.CustomerTransactions" MasterPageFile="~/Common/MasterPage/ERPSite.master" %>

<asp:Content runat="server" ContentPlaceHolderID="PageHeader">
    <script src="JS/CustomerTransactions.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <link href="../CSS/ReportCSS.css?v=<%=Centrix_Version %>" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        ExportPageURL = '<%=ExportPageURL %>';
    </script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="PageBody">
    <input runat="server" class="search-criteria" id="hdnSearchCriteria" type="hidden"
        value="" />
    <div class="">
        <div class="" >
            <div class="Details-div">
                <span class="details-div-title" resourcekey="AdvFilterYourResults"></span>
                <div class="Action-div" style="display: none;">
                    <ul>
                    </ul>
                </div>
            </div>
            <table cellspacing="0" cellpadding="0" border="0" class="Data-Table Quick-Search-Controls">
                <tbody>
                    <tr>
                        <th>
                            <span class="label-title" resourcekey="CustomerNumber"></span>
                        </th>
                        <td>
                        <div>
                            <select class="lst Customer">
                            </select></div>
                        </td>
                        <th>
                            <span class="label-title" resourcekey="From"></span>
                        </th>
                        <td>
                            <input type="text" class="cx-control From date txt" /><br>
                        </td>
                        <th>
                            <span class="label-title" resourcekey="To"></span>
                        </th>
                        <td>
                            <input type="text" class="cx-control To date txt" /><br>
                        </td>
                    </tr>
                    <tr>
                    <th>
                        <span class="label-title" resourcekey="Transactiontype"></span>
                    </th>
                    <td>
                    <input type="checkbox" class="invoice-type chk-trans-type" value="1" /><span class="label-data" resourcekey="Invoice"> </span>
                        <input type="checkbox" class="debit-note-type margin-left40 chk-trans-type" value="2" /><span class="label-data" resourcekey="DebitNote">
                            </span>
                        <br />
                        <input type="checkbox" class="receipt-type chk-trans-type" value="5" />
                        <span class="label-data" resourcekey="Receipt"></span>
                        <input type="checkbox" class="credit-note-type margin-left40 chk-trans-type" value="6" />
                        <span class="label-data" resourcekey="Refund"></span>
                        <br />
                        <input type="checkbox" class="credit-note-type chk-trans-type" value="3" /><span class="label-data" resourcekey="CreditNote"> </span>
                        <input type="checkbox" class="prepayment-type margin-left40 chk-trans-type" value="4" />
                        <span class="label-data" resourcekey="Prepayment"></span>
                        <br />
                        <input type="checkbox" class="adjustment-note-type chk-trans-type" value="7" />
                        <span class="label-data" resourcekey="Adjustment"></span>
                    </td>
                    <td>
                    
                    </td>
                    </tr>
                    <tr>
                        <td class="search-buttons" colspan="6">
                            <div class="filter-reset">
                                <input type="button" class="btnSearch button2" id="btnSearch" value="Search" />
                                <input value="Reset" class="lnkReset button-cancel button2" type="button" />
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</asp:Content>
