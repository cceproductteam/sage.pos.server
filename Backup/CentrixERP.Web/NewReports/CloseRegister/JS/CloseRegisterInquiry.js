﻿
var InvoiceReportSchema;
var ExportPageURL;
var FromLocationAC;
var ToLocationAC;
var FromCustomerAC;
var ToCustomerAC;
var RegisterAC, CustomerAC, PersonAC;

function InnerPage_Load() {
    initControls();
    setPageTitle(getResource("CloseRegisterInquiry"));
    InvoiceReportSchema = systemConfig.ReportSchema.CloseRegister;
    setSelectedMenuItem('.pos-icon');
}

function initControls() {
    LoadDatePicker('input:.date', 'mm-dd-yy');
   

    StoreAC = $('select:.Store').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.StoreServiceURL + 'FindAllLite', required: false });
    RegisterAC = $('select:.Register').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.RegisterServiceURL + 'FindAllLite', required: true});


    $('.lnkReset').click(resetForm);
    $('.btnSearch').click(BuildSearchCriteria);
    $('.Quick-Search-Controls').live('keydown', function (e) {
        if (e.keyCode == 13 || e.which == 13) {
            $('.btnSearch').click();
        }
    });

    StoreAC.onChange(function () {
        if (StoreAC.get_Item() != null)
            RegisterAC.set_Args(formatString('{StoreId:{0}}', StoreAC.get_Item().value));
        else {
            RegisterAC.clear();
            RegisterAC.set_Args("");
        }
    })

    $('.btnSearch').attr('value', getResource("Search"));
    $('.lnkReset').attr('value', getResource("Reset"));
}

function BuildSearchCriteria() {

    debugger;
    if (!saveForm($('.Data-Table'))) {
        return false;
    }


    $('.search-criteria').val('');
    var argsCriteria = new Object();
    //argsCriteria.ExportPageURL = ExportPageURL;

    


    if ($('.ClosedDate').val() != null)
        argsCriteria.close_date = $('.ClosedDate').val();

    if ($('.OpenedDate').val() != null)
        argsCriteria.open_date = $('.OpenedDate').val();

    if (StoreAC.get_Item() != null) {
        argsCriteria.store_id = StoreAC.get_Item().StoreId;
        argsCriteria.store_name = StoreAC.get_Item().label;
    }
    if (RegisterAC.get_Item() != null) {
        argsCriteria.register_id = RegisterAC.get_Item().RegisterId;
        argsCriteria.register_name = RegisterAC.get_Item().label;
    }
    

    var SearchCriteria = '{';
    for (var key in argsCriteria) {
        if (argsCriteria.hasOwnProperty(key))
            if (argsCriteria[key] != "") {
                SearchCriteria = SearchCriteria + ('"' + key + '":' + '"' + argsCriteria[key] + '",');
            }
    }


    if (SearchCriteria != '{') {
        SearchCriteria = SearchCriteria.slice(0, -1);
    }
    SearchCriteria = SearchCriteria + '}';
    var exportPageURL = ExportPageURL + formatString("schemaID={0}&SearchCriteria={1}", InvoiceReportSchema, SearchCriteria);
    window.open(exportPageURL, '_blank');
}

function resetForm() {
    StoreAC.clear();
    RegisterAC.clear();
    RegisterAC.set_Args("");
    $('.OpenedDate').val('');
    $('.ClosedDate').val('');
}


function getdate(date) {
    var arr = date.split('-');
    return arr[1] + '-' + arr[0] + '-' + arr[2];
}