﻿



<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CloseRegisterInquiry.aspx.cs"
    Inherits="CentrixERP.Reports.Web.NewReports.IcQuantityOnHand.IcQuantityOnHand"
    MasterPageFile="~/Common/MasterPage/ERPSite.master" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="PageHeader">
    <script src="../../Common/DataDictionary/DataDictionary-ar.js" type="text/javascript"></script>
    <script src="../../Common/DataDictionary/DataDictionary-en.js" type="text/javascript"></script>
    <link href="../CSS/ReportCSS.css?v=<%=Centrix_Version %>" rel="stylesheet" type="text/css" />
    <script src="JS/CloseRegisterInquiry.js?v=<%=Centrix_Version %>" type="text/javascript"></script>
    <script type="text/javascript">
        ExportPageURL = '<%=ExportPageURL %>';
    </script>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="PageBody">
    <div class="">
        <div class="Quick-Search-Controls">
            <div class="Details-div">
                <span class="details-div-title" resourcekey="AdvFilterYourResults"></span>
                <div class="Action-div">
                    <ul>
                    </ul>
                </div>
            </div>
            <table cellspacing="0" cellpadding="0" border="0" class="Data-Table">
               <tbody>
                <tr>
                    <th>
                        <span class="label-title" resourcekey="Store"></span>
                    </th>
                    <td>
                        <select class="lst Store">
                        </select>
                    </td>
                    <th>
                        <span class="label-title" resourcekey="Register"></span>
                        <span class="smblreqyuired "> *</span>
                    </th>
                    <td>
                        <select class="lst Register" class="required">
                        </select>
                    </td>
                </tr>
                <tr>
                    <th>
                        <span class="label-title" resourcekey="OpenedDate"></span>
                    </th>
                    <td>
                        <input class="cx-control OpenedDate  date" type="text" />
                    </td>
                    <th>
                        <span class="label-title" resourcekey="ClosedDate"></span>
                    </th>
                    <td>
                        <input class="cx-control ClosedDate  date" type="text" />
                    </td>
                </tr>
                
                <tr>
                    <td class="search-buttons" colspan="6">
                        <div class="filter-reset">
                           <input type="button" class="btnSearch button2" id="Button1" value="Search" />
                            <input value="Reset" class="lnkReset lnkReset-InvoiceInquiry button-cancel button2"
                                type="button" />
                        </div>
                    </td>
                </tr>
            </tbody>
            </table>
        </div>
    </div>
</asp:Content>
