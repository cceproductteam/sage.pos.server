﻿var InvoiceReportSchema;
var ExportPageURL;
var CustomFieldAC;
var CustomFieldToAC;
var CustomerAC;
var CustomerToAC;


function InnerPage_Load() {
    initControls();
    setPageTitle(getResource("CustomField"));
    InvoiceReportSchema = 80;
    setSelectedMenuItem('.pos-icon');
}

function initControls() {
    LoadDatePicker('input:.date', 'mm-dd-yy');

    LoadDatePicker('input:.InvoiceDateFrom', 'mm-dd-yy');
    LoadDatePicker('input:.InvoiceDateTo', 'mm-dd-yy');

    CustomFieldAC = $('select:.customField').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.CustomFildServiceURL + 'FindAllLite', required: false, multiSelect: false });
    CustomFieldToAC = $('select:.customFieldTo').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.CustomFildServiceURL + 'FindAllLite', required: false, multiSelect: false });
    CustomerAC = $('select:.CustomerFrom').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.CustomerWebService + 'FindAllLite', required: true });
    CustomerToAC = $('select:.CustomerTo').AutoComplete({ serviceURL: systemConfig.ApplicationURL_Common + systemConfig.CustomerWebService + 'FindAllLite', required: true });


    $('.lnkReset').click(resetForm);
    $('.btnSearch').click(BuildSearchCriteria);
    $('.Quick-Search-Controls').live('keydown', function (e) {
        if (e.keyCode == 13 || e.which == 13) {
            $('.btnSearch').click();
        }
    });

    $('.btnSearch').attr('value', getResource("Search"));
    $('.lnkReset').attr('value', getResource("Reset"));
}

function BuildSearchCriteria() {


    if (!saveForm($('.Quick-Search-Controls'))) {
        return false;
    }


    $('.search-criteria').val('');
    var argsCriteria = new Object();
    //argsCriteria.ExportPageURL = ExportPageURL;

     

    if (CustomFieldAC.get_Item() != null)
        argsCriteria.custom_field_from = CustomFieldAC.get_Item().label;

    if (CustomFieldToAC.get_Item() != null)
        argsCriteria.custom_field_to = CustomFieldToAC.get_Item().label;

    if (CustomerAC.get_Item() != null)
        argsCriteria.customer_id_from = CustomerAC.get_ItemValue();

    if (CustomerToAC.get_Item() != null)
        argsCriteria.customer_id_to = CustomerToAC.get_ItemValue();

    if ($('.InvoiceDateFrom').val() != null)
        argsCriteria.invoice_date_from = $('.InvoiceDateFrom').val();

    if ($('.InvoiceDateTo').val() != null)
        argsCriteria.invoice_date_to = $('.InvoiceDateTo').val();


    var SearchCriteria = '{';
    for (var key in argsCriteria) {
        if (argsCriteria.hasOwnProperty(key))
            if (argsCriteria[key] != "") {
                SearchCriteria = SearchCriteria + ('"' + key + '":' + '"' + argsCriteria[key] + '",');
            }
    }


    if (SearchCriteria != '{') {
        SearchCriteria = SearchCriteria.slice(0, -1);
    }
    SearchCriteria = SearchCriteria + '}';
    var exportPageURL = ExportPageURL + formatString("schemaID={0}&SearchCriteria={1}", InvoiceReportSchema, SearchCriteria);
    window.open(exportPageURL, '_blank');
}

function resetForm() {

    CustomFieldAC.clear();
    CustomFieldToAC.clear();
    CustomerAC.clear();
    CustomerToAC.clear();
    $('.InvoiceDateFrom').val('');
    $('.InvoiceDateTo').val('');
}


function getdate(date) {
    var arr = date.split('-');
    return arr[1] + '-' + arr[0] + '-' + arr[2];
}