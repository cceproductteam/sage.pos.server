using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Common.Business.Entity;
using CentrixERP.Common.Business.IManager;
using CentrixERP.Common.DataAccess.IRepository;
using SF.Framework;

namespace CentrixERP.Common.Business.Manager
{
    public class RecentViewManager : BusinessManagerBase<RecentView, int>, IRecentViewManager
    {
         public RecentViewManager()
            : base((IRepository<RecentView, int>)IoC.Instance.Resolve(typeof(IRecentViewRepository))) { }
    }
}
