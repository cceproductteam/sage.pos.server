using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Common.Business.Entity;
using SF.Framework;
using CentrixERP.Common.Business.IManager;
using CentrixERP.Common.DataAccess.IRepository;

namespace CentrixERP.Common.Business.Manager
{
    public class EntityLockManager : BusinessManagerBase<EntityLock, int>, IEntityLockManager
    {
        public EntityLockManager()
            : base((IRepository<EntityLock, int>)IoC.Instance.Resolve(typeof(IEntityLockRepository)))
        { }

        public void UnLock(int entityId, int? entityValueId)
        {
            ((IEntityLockRepository)this.Repository).UnLock(entityId, entityValueId);
        }

        public void UnLock(int? userId)
        {
            try
            {
                ((IEntityLockRepository)this.Repository).UnLock(userId);
            }
            catch (SF.Framework.Exceptions.RecordNotAffected) { }
            catch (Exception)
            {
                throw;
            }
        }

        public List<EntityLockLite> FindAllLite(CriteriaBase myCriteria) { return ((IEntityLockRepository)this.Repository).FindAllLite(myCriteria); }

        public void Save(EntityLock myEntity)
        {

            try
            {
                base.Save(myEntity);
                if (myEntity.Status==2)
                {
                    throw new SF.Framework.Exceptions.EntityLockedDeleted(null);
                }
                else if (myEntity.Status == 3)
                {
                    throw new SF.Framework.Exceptions.EntityLockedChanged(null);
                }
                else if (myEntity.Status == 4)
                {
                    throw new SF.Framework.Exceptions.EntityLocked(null);
                }
            }
            catch (SF.Framework.Exceptions.RecordExsitsException) {
            
            }
            catch (SF.Framework.Exceptions.RecordNotAffected)
            {

            }
            catch (Exception)
            {
                throw;
            }
        }



        public void LockWithOutCheck(EntityLock myEntity)
        {
            ((IEntityLockRepository)this.Repository).LockWithOutCheck(myEntity);
        }
    }
}