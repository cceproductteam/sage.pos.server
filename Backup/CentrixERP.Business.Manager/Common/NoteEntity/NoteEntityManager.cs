using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Common.Business.Entity;
using SF.Framework;
using CentrixERP.Common.Business.IManager;
using CentrixERP.Common.DataAccess.IRepository;

namespace CentrixERP.Common.Business.Manager
{
    public class NoteEntityManager : BusinessManagerBase<NoteEntity,int>,INoteEntityManager
    {
        public NoteEntityManager()
            : base((IRepository<NoteEntity, int>)IoC.Instance.Resolve(typeof(INoteEntityRepository)))
        {
        
        
        }

        public NoteEntityLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((INoteEntityRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<NoteEntityLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((INoteEntityRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((INoteEntityRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }


        public override List<ValidationError> Validate(NoteEntity myEntity)
        {
            List<ValidationError> errors = new List<ValidationError>();
            if (SF.Framework.String.IsEmpty(myEntity.NoteObj.NoteContent))
            {
                errors.Add(new ValidationError()
                {
                    ControlName = "Note",
                    GlobalMessage = false,
                    Line = 1,
                    Message = "required"
                });
            }
            return errors;
        }
    }
}
