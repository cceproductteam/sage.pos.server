using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CentrixERP.Common.Business.Entity;
using SF.Framework;
using SF.FrameworkEntity;
using CentrixERP.Common.Business.IManager;
using CentrixERP.Common.DataAccess.IRepository;

namespace CentrixERP.Common.Business.Manager
{
    class NoteManager : BusinessManagerBase<Note, int>, INoteManager
    {
        public NoteManager()
            : base((IRepository<Note, int>)IoC.Instance.Resolve(typeof(INoteRepository))) { }
    }
}
