using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Common.Business.Entity;
using SF.Framework;
using CentrixERP.Common.Business.IManager;
using CentrixERP.Common.DataAccess.IRepository;

namespace CentrixERP.Common.Business.Manager
{
    public class ModuleManager : BusinessManagerBase<Module,int>,IModuleManager
    {
        public ModuleManager()
            : base((IRepository<Module, int>)IoC.Instance.Resolve(typeof(IModuleRepository)))
        { }
    }
}
