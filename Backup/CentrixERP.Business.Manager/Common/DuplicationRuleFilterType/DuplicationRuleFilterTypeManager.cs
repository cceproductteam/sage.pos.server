using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Common.Business.Entity;
using CentrixERP.Common.Business.IManager;
using SF.Framework;
using CentrixERP.Common.DataAccess.IRepository;

namespace CentrixERP.Common.Business.Manager
{
    public class DuplicationRuleFilterTypeManager : BusinessManagerBase<DuplicationRuleFilterType, int>, IDuplicationRuleFilterTypeManager
    {
        public DuplicationRuleFilterTypeManager()
            : base((IRepository<DuplicationRuleFilterType, int>)IoC.Instance.Resolve(typeof(IDuplicationRuleFilterTypeRepository))) { }

        public List<DuplicationRuleFilterType> DuplicationFilter_FindAll(CriteriaBase myCriteria)
        { return ((IDuplicationRuleFilterTypeRepository)this.Repository).DuplicationFilter_FindAll(myCriteria); }
    }
}
