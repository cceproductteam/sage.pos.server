using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.Framework;
using SF.FrameworkEntity;
using CentrixERP.Common.Business.Entity;
using CentrixERP.Common.Business.IManager;
using CentrixERP.Common.DataAccess.IRepository;

namespace CentrixERP.Common.Business.Manager
{
    
    public class ModuleFieldManager: BusinessManagerBase< ModuleField, int>, IModuleFieldManager
    {
        #region "Constructors"
      
        public ModuleFieldManager()
        : base((IRepository< ModuleField, int>)IoC.Instance.Resolve(typeof(IModuleFieldRepository))) { }
        
        #endregion


        #region IModuleFieldManager Members

        public List<SearchFieldCriteria> FindAllSearchCriteria()
        {
            return ((IModuleFieldRepository)this.Repository).FindAllSearchCriteria();
        }

        public SearchFieldCriteria FindSearchFieldCriteriaById(int SearchFieldCriteriaId)
        {
            return ((IModuleFieldRepository)this.Repository).FindSearchFieldCriteriaById(SearchFieldCriteriaId);
        }


        #endregion
    }
}


