using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Common.Business.Entity;
using SF.Framework;
using CentrixERP.Common.Business.IManager;
using CentrixERP.Common.DataAccess.IRepository;

namespace CentrixERP.Common.Business.Manager
{
    public class TimeZoneManager : BusinessManagerBase<CentrixERP.Common.Business.Entity.TimeZone, int>, ITimeZoneManager
    {
        public TimeZoneManager()
            : base((IRepository<CentrixERP.Common.Business.Entity.TimeZone, int>)IoC.Instance.Resolve(typeof(ITimeZoneRepository)))
        { }
    }
}
