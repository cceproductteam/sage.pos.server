using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Common.Business.Entity;
using SF.Framework;
using CentrixERP.Common.Business.IManager;
using CentrixERP.Common.DataAccess.IRepository;

namespace CentrixERP.Common.Business.Manager
{
    public class DocumentEntityManager : BusinessManagerBase<DocumentEntity,int>,IDocumentEntityManager
    {
        public DocumentEntityManager()
            : base((IRepository<DocumentEntity, int>)IoC.Instance.Resolve(typeof(IDocumentEntityRepository)))
        {
        
        
        }

        public DocumentEntityLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((IDocumentEntityRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<DocumentEntityLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((IDocumentEntityRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((IDocumentEntityRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }

        public List<DocumentEntity> FindByEntityId(DocumentEntityCriteria myCriteria)
        {
            return ((IDocumentEntityRepository)this.Repository).FindByEntityId(myCriteria);
        }



    }
}
