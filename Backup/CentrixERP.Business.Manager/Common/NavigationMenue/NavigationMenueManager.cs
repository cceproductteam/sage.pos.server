using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Common.Business.Entity;
using CentrixERP.Common.Business.IManager;
using CentrixERP.Common.DataAccess.IRepository;
using SF.Framework;

namespace CentrixERP.Common.Business.Manager
{
    public class NavigationMenueManager : BusinessManagerBase<NavigationMenue, int>, INavigationMenueManager
    {
        public NavigationMenueManager()
            : base((IRepository<NavigationMenue, int>)IoC.Instance.Resolve(typeof(INavigationMenueRepository))) { }

        #region INavigationMenueManager Members

        public void reOrder(string Ids)
        {
            ((INavigationMenueRepository)this.Repository).reOrder(Ids);
        }

        #endregion
    }
}
