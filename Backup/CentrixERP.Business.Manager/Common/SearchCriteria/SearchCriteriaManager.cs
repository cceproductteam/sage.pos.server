using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CentrixERP.Common.Business.Entity;
using SF.FrameworkEntity;
using CentrixERP.Common.Business.IManager;
using SF.Framework;
using CentrixERP.Common.DataAccess.IRepository;

namespace CentrixERP.Common.Business.Manager
{
    public class SearchCriteriaManager : BusinessManagerBase<SearchCriteria, int>, ISearchCriteriaManager
    {
        public SearchCriteriaManager()
            : base((IRepository<SearchCriteria, int>)IoC.Instance.Resolve(typeof(ISearchCriteriaRepository))) { }

        
    }
}
