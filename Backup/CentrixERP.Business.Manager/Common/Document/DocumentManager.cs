using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.Framework;
using SF.FrameworkEntity;
using CentrixERP.Common.Business.Entity;
using CentrixERP.Common.Business.IManager;
using CentrixERP.Common.DataAccess.IRepository;
using System.Xml.Linq;
using System.Configuration;
using System.IO;
using E = CentrixERP.Common.Business.Entity;


namespace CentrixERP.Common.Business.Manager
{
    public class DocumentManager : BusinessManagerBase<E.Document, int>, IDocumentManager
    {
        public DocumentManager()
            : base((IRepository<E.Document, int>)IoC.Instance.Resolve(typeof(IDocumentRepository))) { }


        #region IBusinessManager<Document,int> Members

        void IBusinessManager<E.Document, int>.Save(E.Document myEntity)
        {

            bool add_falg = false;

            if (myEntity.IsNew && !myEntity.IsDeleted)
                add_falg = true;

            base.Save(myEntity);

            //if (CentrixERP.Common.Web.BasePageLoggedIn.PreventPhysicalSave)
            //    return;
            if (myEntity.PreventPhysicalSave)
                return;

            if (add_falg)
            {
                string path = myEntity.DocumentPath +"\\" + myEntity.ParentEntityId;
                Directory.CreateDirectory(path);
                System.IO.FileStream fs = new System.IO.FileStream(path + "\\" + myEntity.FileName + myEntity.FileExtension, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite);
                System.IO.BinaryWriter sw = new System.IO.BinaryWriter(fs);
                try
                {

                    sw.Write(myEntity.documentBytes);
                    sw.Flush();
                }
                catch (Exception ex)
                {
                    myEntity.MarkDeleted();
                    base.Save(myEntity);
                }
                finally
                {
                    sw.Close();
                }
            }

        }



        public DocumentIcons GetIcon(string extension)
        {
            var DocumentsIconURL = ConfigurationManager.AppSettings["DocumentsIconURL"];
            DocumentIcons docIcon = new DocumentIcons();
            XElement xml = XElement.Load(ConfigurationManager.AppSettings["DocumentIconsURLsXMLPath"]);

            IEnumerable<DocumentIcons> xmlElements = from XElement c in xml.Elements("Page")
                                                     where c.Element("Name").Value == extension
                                                     select new DocumentIcons() { Icon = DocumentsIconURL + c.Element("URL").Value, CssClass = c.Element("CssClass").Value };


            //foreach (var xmlElement in xmlElements)
            //{
            //    if (xmlElement.Element("Name").Value == extension)
            //    {
            //        docIcon.Icon = DocumentsIconURL + xmlElement.Element("URL").Value;
            //        docIcon.CssClass = xmlElement.Element("CssClass").Value;

            //        break;
            //    }

            //}
            return (xmlElements.Count().Equals(0)) ? new DocumentIcons() { Icon = DocumentsIconURL + "default-icon.png", CssClass = "su-ext-default" } : xmlElements.Single();
        }

        public void UpdateOrder(int DocumentID, int EntityId, int EntityValueId, int OrderID)
        {
            ((IDocumentRepository)this.Repository).UpdateOrder(DocumentID, EntityId, EntityValueId, OrderID);
        }

        #endregion
    }
}
