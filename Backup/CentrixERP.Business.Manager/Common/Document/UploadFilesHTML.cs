using System;
using System;
using Krystalware.SlickUpload;
using Krystalware.SlickUpload.Configuration;
using Krystalware.SlickUpload.Storage;
using System.Collections;
using System.Collections.Generic;
using SF.Framework;
using System.IO;
using System.Configuration;
using CentrixERP.Common.Business.IManager;
using CentrixERP.Common.Business.Entity;

namespace CentrixERP.Common.Business.Manager
{
    public class UploadFilesHTML : FileUploadStreamProvider
    {
        public UploadFilesHTML(UploadStreamProviderElement settings)
            : base(settings)
        { }

        public override string GetServerFileName(UploadedFile file)
        {
            Object obj = Settings;

            int moduleKey = Convert.ToInt32(file.UploadRequest.Data["modulekey"]);
            string moduleName = file.UploadRequest.Data["modulename"];
            int parentId = Convert.ToInt32(file.UploadRequest.Data["id"]);
            string folder = file.UploadRequest.Data["folder"];
            int userId = file.UploadRequest.Data["userId"].ToNumber();
            var fileName = file.ClientName;

            int FolderId = 1;
            FolderId = file.UploadRequest.Data["parentDocumentFolderId"].ToNumber();
            FileInfo info = new FileInfo(fileName);

            IDocumentEntityManager iDocumentEntityManager = (IDocumentEntityManager)IoC.Instance.Resolve(typeof(IDocumentEntityManager));
            DocumentEntity documentEntity = new DocumentEntity();
            documentEntity.CreatedBy = userId;

            documentEntity.DocumentObj = new Document();
            documentEntity.DocumentObj.OrginalFileName = Path.GetFileNameWithoutExtension(info.FullName);
            documentEntity.DocumentObj.CreatedBy = userId;
            documentEntity.DocumentObj.FileExtension = info.Extension;
            documentEntity.DocumentObj.ParentFolder = parentId.ToString();
            documentEntity.DocumentObj.PreventPhysicalSave = true;

            //string path = documentEntity.DocumentPath;
            string path = ConfigurationManager.AppSettings["DocumentsURL"] + moduleName + "/";// +folder;
            documentEntity.DocumentObj.FolderId = FolderId;
            documentEntity.DocumentObj.DocumentPath = path; //+ parentId.ToString();//+ folder;
            documentEntity.EntityValueId = parentId;
            documentEntity.EntityId = moduleKey;

            //DocumentFactory factory = new DocumentFactory(moduleKey);
            //IDocument doc = factory.IEntity;
            //doc.CreatedBy = userId;

            //doc.DocumentObj = new Centrix.Common.Business.Entity.Document();
            ////doc.DocumentObj.ParentFolder = folder;
            //doc.DocumentObj.OrginalFileName = Path.GetFileNameWithoutExtension(info.FullName);
            //doc.DocumentObj.CreatedBy = userId;
            //doc.DocumentObj.FileExtension = info.Extension;
            ////doc.DocumentObj.ParentFolder = folder;
            //doc.DocumentObj.ParentFolder = parentId.ToString();
            //doc.DocumentObj.PreventPhysicalSave = true;

            //string path = doc.DocumentPath;

            //doc.DocumentObj.FolderId=FolderId;
            //doc.DocumentObj.DocumentPath = path + parentId.ToString();//+ folder;
            //doc.ModuleId = parentId;
            try
            {
                iDocumentEntityManager.Save(documentEntity);
            }
            catch (Exception)
            {
            }

            fileName = documentEntity.DocumentId + documentEntity.DocumentObj.FileExtension;//+ "_" + fileName;
            file.Data.Add("serverFileName", fileName);
            file.Data.Add("DocumentID", documentEntity.DocumentId.ToString());
            file.Data.Add("DocumentEntityID", documentEntity.DocumentEntityId.ToString());
            return moduleName + @"\" + folder + @"\" + fileName;

        }
    }
}

