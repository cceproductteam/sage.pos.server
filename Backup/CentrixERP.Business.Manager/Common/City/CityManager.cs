using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.Framework;
using SF.FrameworkEntity;
using CentrixERP.Common.Business.Entity;
using CentrixERP.Common.Business.IManager;
using CentrixERP.Common.DataAccess.IRepository;

namespace CentrixERP.Common.Business.Manager
{
    public class CityManager : BusinessManagerBase<City, int>, ICityManager
    {

        public CityManager()
            : base((IRepository<City, int>)IoC.Instance.Resolve(typeof(ICityRepository))) { }
    }
}
