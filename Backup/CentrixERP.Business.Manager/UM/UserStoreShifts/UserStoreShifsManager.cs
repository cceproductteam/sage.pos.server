using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using Centrix.UM.Business.Entity;
using SF.Framework;
using Centrix.UM.Business.IManager;
using Centrix.UM.DataAccess.IRepository;

namespace Centrix.UM.Business.Manager
{
    public class UserStoreShifsManager : BusinessManagerBase<UserStoreShifs,int>,IUserStoreShifsManager
    {
        public UserStoreShifsManager()
            : base((IRepository<UserStoreShifs, int>)IoC.Instance.Resolve(typeof(IUserStoreShifsRepository)))
        {
        
        
        }

        public UserStoreShifsLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((IUserStoreShifsRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<UserStoreShifsLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((IUserStoreShifsRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((IUserStoreShifsRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }
    }
}
