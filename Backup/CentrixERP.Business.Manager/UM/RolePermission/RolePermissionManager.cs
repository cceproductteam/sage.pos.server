using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.Framework;
using SF.FrameworkEntity;
using Centrix.UM.Business.Entity;
using Centrix.UM.Business.IManager;
using Centrix.UM.DataAccess.IRepository;

namespace Centrix.UM.Business.Manager
{
    public class RolePermissionManager : BusinessManagerBase<RolePermission, int>, IRolePermissionManager
    {
        public RolePermissionManager()
            : base((IRepository<RolePermission, int>)IoC.Instance.Resolve(typeof(IRolePermissionRepository))) { }

        public List<RolePermissionLite> FindAllRolePermission(int RoleId, CriteriaBase myCriteria)
        {
            return ((IRolePermissionRepository)this.Repository).FindAllRolePermission(RoleId, myCriteria);
        }

        public List<RolePermissionLite> FindRolePermission(int RoleId, CriteriaBase myCriteria)
        {
            return ((IRolePermissionRepository)this.Repository).FindRolePermission(RoleId, myCriteria);
        }



        public void Save(int roleId, string permissionStatusIds, int createdBy)
        {
            ((IRolePermissionRepository)this.Repository).Save(roleId, permissionStatusIds, createdBy);
        }

        public void DeleteByRole(int roleId)
        {
            ((IRolePermissionRepository)this.Repository).DeleteByRole(roleId);
        }


        public void SetAllPermissions(bool all, int roleId, int userId)
        {
            ((IRolePermissionRepository)this.Repository).SetAllPermissions(all, roleId, userId);
        }



        public int LockRolePermissions(CriteriaBase myCriteria)
        {

            int Status = 0;
            try
            {
                Status = ((IRolePermissionRepository)this.Repository).LockRolePermissions(myCriteria);
                if (Status == 2)
                {
                    throw new SF.Framework.Exceptions.EntityLocked(null);
                }
                else if (Status == 3)
                {
                    throw new SF.Framework.Exceptions.EntityLockedChanged(null);
                }
                else
                    return Status;
            }
            catch (SF.Framework.Exceptions.RecordExsitsException)
            {
                throw new SF.Framework.Exceptions.RecordExsitsException(null);
            }
            catch (SF.Framework.Exceptions.RecordNotAffected)
            {
                throw new SF.Framework.Exceptions.RecordNotAffected(null);
            }
            catch (Exception)
            {
                throw;
            }
            return Status;

            //if (Status == 2)
            //{
            //    throw new SF.Framework.Exceptions.EntityLocked(null);
            //}
            //else if (Status == 3)
            //{
            //    throw new SF.Framework.Exceptions.EntityLockedChanged(null);
            //}
            //else
            //    return Status;

                
        }

        public void UpdateRolePermissionUpdatedDate(CriteriaBase myCriteria)
        {
            ((IRolePermissionRepository)this.Repository).UpdateRolePermissionUpdatedDate(myCriteria);
        }
    }
}