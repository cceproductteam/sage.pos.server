using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.DataAccess.IRepository;
using CentrixERP.Common.Business.Factory;
using CentrixERP.Business.IManager;

using CentrixERP.Business.Entity;
using SF.Framework;
using Centrix.UM.Business.IManager;
using Centrix.UM.Business.Entity;
namespace CentrixERP.Business.Manager
{
    public class ClientLoginManager : BusinessManagerBase<User, int>, IClientLoginManager
    {

        public ClientLoginManager()
            : base((IRepository<User, int>)IoC.Instance.Resolve(typeof(IClientLoginManager)))
        {


        }
        public User ClientLogin(string UserName, string Password, string AccessCode)
        {
            AccessCode = (!string.IsNullOrEmpty(AccessCode)) ? Cryption.Encrypt.String(AccessCode, Cryption.Algorithm.AES) : null;
            Password = Cryption.Encrypt.String(Password, Cryption.Algorithm.AES);
            User myUser =  ((IUserManager)this.Repository).ClientLogin(UserName, Password, AccessCode);
            if (myUser != null)
            {
                myUser.MarkOld();
                myUser.Manager = this;
                //Cookies.Add("uid", myUser.UserId.ToString());


            }
            return myUser;
        }
        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            throw new NotImplementedException();
        }
    }
}
