using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;
using SF.Framework;
using CentrixERP.Business.IManager;
using CentrixERP.DataAccess.IRepository;

namespace CentrixERP.Business.Manager
{
    public class LocalConfigurationManager : BusinessManagerBase<LocalConfiguration,int>,ILocalConfigurationManager
    {
        public LocalConfigurationManager()
            : base((IRepository<LocalConfiguration, int>)IoC.Instance.Resolve(typeof(ILocalConfigurationRepository)))
        {
        
        
        }

        public LocalConfigurationLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((ILocalConfigurationRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<LocalConfigurationLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((ILocalConfigurationRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((ILocalConfigurationRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }
      
    }
}
