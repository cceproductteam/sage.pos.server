using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;
using SF.Framework;
using CentrixERP.Business.IManager;
using CentrixERP.DataAccess.IRepository;
namespace CentrixERP.Business.Manager
{
    public class RegisterAmountsManager : BusinessManagerBase<RegisterAmounts,int>,IRegisterAmountsManager
    {
        public RegisterAmountsManager()
            : base((IRepository<RegisterAmounts, int>)IoC.Instance.Resolve(typeof(IRegisterAmountsRepository)))
        {
        
        
        }

        public RegisterAmountsLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((IRegisterAmountsRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<RegisterAmountsLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((IRegisterAmountsRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((IRegisterAmountsRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }
    }
}
