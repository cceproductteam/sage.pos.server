using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;
using SF.Framework;
using CentrixERP.Business.IManager;
//using Centrix.POS.Standalone.Client.DataAccess.IRepository;
using CentrixERP.DataAccess.IRepository;

namespace CentrixERP.Business.Manager
{
    public class InvoiceCustomFeildsManager : BusinessManagerBase<InvoiceCustomFeilds,int>,IInvoiceCustomFeildsManager
    {
        public InvoiceCustomFeildsManager()
            : base((IRepository<InvoiceCustomFeilds, int>)IoC.Instance.Resolve(typeof(IInvoiceCustomFeildsRepository)))
        {
        
        
        }

        public InvoiceCustomFeildsLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((IInvoiceCustomFeildsRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<InvoiceCustomFeildsLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((IInvoiceCustomFeildsRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((IInvoiceCustomFeildsRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }
    }
}
