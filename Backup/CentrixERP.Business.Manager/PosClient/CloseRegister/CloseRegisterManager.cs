using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;
using SF.Framework;
using CentrixERP.Business.IManager;
//using Centrix.POS.Standalone.Client.DataAccess.IRepository;
using CentrixERP.DataAccess.IRepository;
namespace CentrixERP.Business.Manager
{
    public class CloseRegisterManager : BusinessManagerBase<CloseRegister, int>, ICloseRegisterManager
    {
        public CloseRegisterManager()
            : base((IRepository<CloseRegister, int>)IoC.Instance.Resolve(typeof(ICloseRegisterRepository)))
        {


        }

        public CloseRegisterLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((ICloseRegisterRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<CloseRegisterLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((ICloseRegisterRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((ICloseRegisterRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }

        public List<CloseRegiserDetails> GetCloseRegisterDetailsPaymentType(CriteriaBase myCriteria)
        {
            return ((ICloseRegisterRepository)this.Repository).GetCloseRegisterDetailsPaymentType(myCriteria);
        }
        public void UpdateSyncStatus(string trasactionNumber, int EntityId)
        {
            ((ICloseRegisterRepository)this.Repository).UpdateSyncStatus(trasactionNumber, EntityId);
        }

        public List<CloseRegisterInquiryLite> CloseRegisterInquiryResults(string StoreIds, string RegisterIds, DateTime? OpenedDateFrom, DateTime? OpenedDateTo, DateTime? ClosedDateFrom, DateTime? ClosedDateTo)
        {
            return ((ICloseRegisterRepository)this.Repository).CloseRegisterInquiryResults(StoreIds, RegisterIds, OpenedDateFrom, OpenedDateTo, ClosedDateFrom, ClosedDateTo);
        }

        public CloseRegisterTotals FindCloseRegisterSalesTotal(int CloseRegisterId)
        {
            return ((ICloseRegisterRepository)this.Repository).FindCloseRegisterSalesTotal(CloseRegisterId);
        }
    }
}

