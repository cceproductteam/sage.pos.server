using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;
using SF.Framework;
using CentrixERP.Business.IManager;
using CentrixERP.DataAccess.IRepository;

namespace CentrixERP.Business.Manager
{
    public class POSCurrencyManager : BusinessManagerBase<POSCurrency,int>,IPOSCurrencyManager
    {
        public POSCurrencyManager()
            : base((IRepository<POSCurrency, int>)IoC.Instance.Resolve(typeof(IPOSCurrencyRepository)))
        {
        
        
        }

        public POSCurrencyLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((IPOSCurrencyRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<POSCurrencyLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((IPOSCurrencyRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((IPOSCurrencyRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }

        public string SaveIntegration(List<POSCurrency> currencyList)
        {
            return ((IPOSCurrencyRepository)this.Repository).SaveIntegration(currencyList);
        }
    }
}
