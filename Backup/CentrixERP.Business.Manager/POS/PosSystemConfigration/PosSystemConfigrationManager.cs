using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;
using SF.Framework;
using CentrixERP.Business.IManager;
using CentrixERP.DataAccess.IRepository;
using CentrixERP.Business.Entity;
using CentrixERP.Business.IManager;

namespace CentrixERP.Business.Manager
{
    public class PosSystemConfigrationManager : BusinessManagerBase<PosSystemConfigration,int>,IPosSystemConfigrationManager
    {
        public PosSystemConfigrationManager()
            : base((IRepository<PosSystemConfigration, int>)IoC.Instance.Resolve(typeof(IPosSystemConfigrationRepository)))
        {
        
        
        }
        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((IPOSSyncRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }
     
        public List<PosSystemConfigrationLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((IPosSystemConfigrationRepository)this.Repository).FindAllLite(myCriteria);
        }

     
    }
}
