using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;
using SF.Framework;
using CentrixERP.Business.IManager;
using CentrixERP.DataAccess.IRepository;

namespace CentrixERP.Business.Manager
{
    public class GiftVoucherManager : BusinessManagerBase<GiftVoucher,int>,IGiftVoucherManager
    {
        public GiftVoucherManager()
            : base((IRepository<GiftVoucher, int>)IoC.Instance.Resolve(typeof(IGiftVoucherRepository)))
        {
        
        
        }

        public GiftVoucherLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((IGiftVoucherRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<GiftVoucherLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((IGiftVoucherRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((IGiftVoucherRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }
        public GiftVoucher search(string giftCode)
        {
            return ((IGiftVoucherRepository)this.Repository).search(giftCode);
        }
        public void EditVoucher(int id)
        {
            ((IGiftVoucherRepository)this.Repository).EditVoucher(id);
        }
    }
}
