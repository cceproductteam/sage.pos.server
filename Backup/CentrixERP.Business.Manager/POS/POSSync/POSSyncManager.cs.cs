﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;
using SF.Framework;
using CentrixERP.Business.IManager;
using CentrixERP.DataAccess.IRepository;

namespace CentrixERP.Business.Manager
{
   public class POSSyncManager : BusinessManagerBase<POSSync,int>,IPOSSyncManager
    {
       public POSSyncManager()
           : base((IRepository<POSSync, int>)IoC.Instance.Resolve(typeof(IPOSSyncRepository)))
        {
        
        
        }
       public List<Object> AdvancedSearchLite(CriteriaBase myCriteria) {
           return ((IPOSSyncRepository)this.Repository).AdvancedSearchLite(myCriteria);
       }

       public void POSSyncData(Object[] data)
        {
            ((IPOSSyncRepository)this.Repository).POSSyncData(data);
        }
    }
}
