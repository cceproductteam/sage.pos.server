using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;
using SF.Framework;
using CentrixERP.Business.IManager;
using CentrixERP.DataAccess.IRepository;

namespace CentrixERP.Business.Manager
{
    public class POSStockManager : BusinessManagerBase<POSStock,int>,IPOSStockManager
    {
        public POSStockManager()
            : base((IRepository<POSStock, int>)IoC.Instance.Resolve(typeof(IPOSStockRepository)))
        {
        
        
        }

        public POSStockLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((IPOSStockRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<POSStockLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((IPOSStockRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((IPOSStockRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }
        public string SaveIntegration(List<POSStock> stockList)
        {
            return ((IPOSStockRepository)this.Repository).SaveIntegration(stockList);
        }
    }
}
