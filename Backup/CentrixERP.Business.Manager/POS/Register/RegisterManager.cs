using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;
using SF.Framework;
using CentrixERP.Business.IManager;
using CentrixERP.DataAccess.IRepository;

namespace CentrixERP.Business.Manager
{
    public class RegisterManager : BusinessManagerBase<Register,int>,IRegisterManager
    {
        public RegisterManager()
            : base((IRepository<Register, int>)IoC.Instance.Resolve(typeof(IRegisterRepository)))
        {
        
        
        }

        public RegisterLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((IRegisterRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<RegisterLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((IRegisterRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((IRegisterRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }
    }
}
