using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;
using SF.Framework;
using CentrixERP.Business.IManager;
using CentrixERP.DataAccess.IRepository;

namespace CentrixERP.Business.Manager
{
    public class SyncScheduleManager : BusinessManagerBase<SyncSchedule,int>,ISyncScheduleManager
    {
        public SyncScheduleManager()
            : base((IRepository<SyncSchedule, int>)IoC.Instance.Resolve(typeof(ISyncScheduleRepository)))
        {
        
        
        }

        public SyncScheduleLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((ISyncScheduleRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<SyncScheduleLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((ISyncScheduleRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((ISyncScheduleRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }
        public List<SyncSchedule> FindAllToSync(int status)
        {
            return ((ISyncScheduleRepository)this.Repository).FindAllToSync(status);
        }
    }
}
