using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;
using SF.Framework;
using CentrixERP.Business.IManager;
using CentrixERP.DataAccess.IRepository;

namespace CentrixERP.Business.Manager
{
    public class POSCurrencyRateDetailsManager : BusinessManagerBase<POSCurrencyRateDetails,int>,IPOSCurrencyRateDetailsManager
    {
        public POSCurrencyRateDetailsManager()
            : base((IRepository<POSCurrencyRateDetails, int>)IoC.Instance.Resolve(typeof(IPOSCurrencyRateDetailsRepository)))
        {
        
        
        }

        public POSCurrencyRateDetailsLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((IPOSCurrencyRateDetailsRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<POSCurrencyRateDetailsLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((IPOSCurrencyRateDetailsRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((IPOSCurrencyRateDetailsRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }

        public string SaveIntegration(List<POSCurrencyRateDetails> currenyRateList)
        {
            return ((IPOSCurrencyRateDetailsRepository)this.Repository).SaveIntegration(currenyRateList);
        }
    }
}
