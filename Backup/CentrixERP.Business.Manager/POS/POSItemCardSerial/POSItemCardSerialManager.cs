using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;

using SF.Framework;
using CentrixERP.Business.Entity;
using CentrixERP.Business.IManager;
using CentrixERP.DataAccess.IRepository;



namespace CentrixERP.Business.Manager
{
    public class POSItemCardSerialManager : BusinessManagerBase<POSItemCardSerial,int>,IPOSItemCardSerialManager
    {
        public POSItemCardSerialManager()
            : base((IRepository<POSItemCardSerial, int>)IoC.Instance.Resolve(typeof(IPOSItemCardSerialRepository)))
        {
        
        
        }

        public POSItemCardSerialLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((IPOSItemCardSerialRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<POSItemCardSerialLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((IPOSItemCardSerialRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((IPOSItemCardSerialRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }

        public string SaveIntegration(List<POSItemCardSerial> SerialList)
        {
            return ((IPOSItemCardSerialRepository)this.Repository).SaveIntegration(SerialList);
        }
    }
}
