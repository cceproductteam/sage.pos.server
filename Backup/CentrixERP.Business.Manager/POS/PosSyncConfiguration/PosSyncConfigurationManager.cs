using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;
using SF.Framework;
using CentrixERP.Business.IManager;
using CentrixERP.DataAccess.IRepository;
using CentrixERP.Business.IManager;

namespace CentrixERP.Business.Manager
{
    public class PosSyncConfigurationManager : BusinessManagerBase<PosSyncConfiguration,int>,IPosSyncConfigurationManager
    {
        public PosSyncConfigurationManager()
            : base((IRepository<PosSyncConfiguration, int>)IoC.Instance.Resolve(typeof(IPosSyncConfigurationRepository)))
        {
        
        
        }

        public PosSyncConfigurationLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((IPosSyncConfigurationRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<PosSyncConfigurationLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((IPosSyncConfigurationRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((IPosSyncConfigurationRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }
    }
}
