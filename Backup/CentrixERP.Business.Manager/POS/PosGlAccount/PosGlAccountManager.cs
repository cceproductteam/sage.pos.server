using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;
using SF.Framework;
using CentrixERP.Business.IManager;
using CentrixERP.DataAccess.IRepository;

namespace CentrixERP.Business.Manager
{
    public class PosGlAccountManager : BusinessManagerBase<PosGlAccount,int>,IPosGlAccountManager
    {
        public PosGlAccountManager()
            : base((IRepository<PosGlAccount, int>)IoC.Instance.Resolve(typeof(IPosGlAccountRepository)))
        {
        
        
        }

        public PosGlAccountLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((IPosGlAccountRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<PosGlAccountLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((IPosGlAccountRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((IPosGlAccountRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }
    }
}
