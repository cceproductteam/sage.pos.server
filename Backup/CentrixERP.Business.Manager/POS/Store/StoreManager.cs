using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;
using SF.Framework;
using CentrixERP.Business.IManager;
using CentrixERP.DataAccess.IRepository;

namespace CentrixERP.Business.Manager
{
    public class StoreManager : BusinessManagerBase<Store,int>,IStoreManager
    {
        public StoreManager()
            : base((IRepository<Store, int>)IoC.Instance.Resolve(typeof(IStoreRepository)))
        {
        
        
        }

        public StoreLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((IStoreRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<StoreLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((IStoreRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((IStoreRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }
    }
}
