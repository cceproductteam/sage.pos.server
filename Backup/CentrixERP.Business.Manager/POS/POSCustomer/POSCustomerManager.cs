using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;
using SF.Framework;
using CentrixERP.Business.IManager;
using CentrixERP.DataAccess.IRepository;

namespace CentrixERP.Business.Manager
{
    public class POSCustomerManager : BusinessManagerBase<POSCustomer,int>,IPOSCustomerManager
    {
        public POSCustomerManager()
            : base((IRepository<POSCustomer, int>)IoC.Instance.Resolve(typeof(IPOSCustomerRepository)))
        {
        
        
        }

        public POSCustomerLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((IPOSCustomerRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<POSCustomerLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((IPOSCustomerRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((IPOSCustomerRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }

        public string SaveIntegration(List<POSCustomer> CustomerList)
        {
            return ((IPOSCustomerRepository)this.Repository).SaveIntegration(CustomerList);
        }
    }
}
