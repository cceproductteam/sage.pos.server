using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;
using SF.Framework;
using CentrixERP.Business.IManager;
using CentrixERP.DataAccess.IRepository;

namespace CentrixERP.Business.Manager
{
    public class CompanyManager : BusinessManagerBase<Company,int>,ICompanyManager
    {
        public CompanyManager()
            : base((IRepository<Company, int>)IoC.Instance.Resolve(typeof(ICompanyRepository)))
        {
        
        
        }

        public CompanyLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((ICompanyRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<CompanyLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((ICompanyRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((ICompanyRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }
        public void UpdateSyncStatus(string trasactionNumber, int EntityId)
        {
            ((ICompanyRepository)this.Repository).UpdateSyncStatus(trasactionNumber, EntityId);
        }
    }
}
