using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;
using SF.Framework;
using CentrixERP.Business.IManager;
using CentrixERP.DataAccess.IRepository;

namespace CentrixERP.Business.Manager
{
    public class POSItemTaxManager : BusinessManagerBase<POSItemTax,int>,IPOSItemTaxManager
    {
        public POSItemTaxManager()
            : base((IRepository<POSItemTax, int>)IoC.Instance.Resolve(typeof(IPOSItemTaxRepository)))
        {
        
        
        }

        public POSItemTaxLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((IPOSItemTaxRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<POSItemTaxLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((IPOSItemTaxRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((IPOSItemTaxRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }

        public string SaveIntegration(List<POSItemTax> itemTaxList)
        {
            return ((IPOSItemTaxRepository)this.Repository).SaveIntegration(itemTaxList);
        }
    }
}
