using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;
using SF.Framework;
using CentrixERP.Business.IManager;
using CentrixERP.DataAccess.IRepository;

namespace CentrixERP.Business.Manager
{
    public class PersonManager : BusinessManagerBase<Person,int>,IPersonManager
    {
        public PersonManager()
            : base((IRepository<Person, int>)IoC.Instance.Resolve(typeof(IPersonRepository)))
        {
        
        
        }

        public PersonLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((IPersonRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<PersonLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((IPersonRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((IPersonRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }
        public List<PersonLite> GetAllPersonBirthOfDateFindAll(PersonCriteria myCriteria)
        {
            return ((IPersonRepository)this.Repository).GetAllPersonBirthOfDateFindAll(myCriteria);
        }
        public List<PersonLite> GetAllPersonAnniversaryFindAll(PersonCriteria myCriteria)
        {
            return ((IPersonRepository)this.Repository).GetAllPersonAnniversaryFindAll(myCriteria);
        }

        public void UpdateSyncStatus(string trasactionNumber, int EntityId)
        {
            ((IPersonRepository)this.Repository).UpdateSyncStatus(trasactionNumber, EntityId);
        }

        public override List<ValidationError> Validate(Person myEntity)
        {
            List<ValidationError> errors = base.Validate(myEntity);

          


            return errors;
        }

        

    }
}
