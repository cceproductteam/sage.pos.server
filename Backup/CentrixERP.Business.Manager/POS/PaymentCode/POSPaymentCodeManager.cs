using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;

using SF.Framework;
using CentrixERP.Business.IManager;
using CentrixERP.DataAccess.IRepository;
using CentrixERP.Business.Entity;

namespace CentrixERP.Business.Manager
{
    public class POSPaymentCodeManager : BusinessManagerBase<POSPaymentCode,int>,IPOSPaymentCodeManager
    {
        public POSPaymentCodeManager()
            : base((IRepository<POSPaymentCode, int>)IoC.Instance.Resolve(typeof(IPOSPaymentCodeRepository)))
        {
        
        
        }

        public POSPaymentCodeLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((IPOSPaymentCodeRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<POSPaymentCodeLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((IPOSPaymentCodeRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((IPOSPaymentCodeRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }
        
    }
}
