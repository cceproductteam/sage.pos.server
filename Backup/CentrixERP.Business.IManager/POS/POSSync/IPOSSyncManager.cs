﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;
using CentrixERP.Common.Business.Factory;

namespace CentrixERP.Business.IManager
{
    public interface IPOSSyncManager : IBusinessManager<POSSync, int>, IAdvancedSearch
    {
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
        void POSSyncData(Object[] data);
    }
}