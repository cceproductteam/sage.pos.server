using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;
using CentrixERP.Common.Business.Factory;

namespace CentrixERP.Business.IManager
{
    public interface IPOSCurrencyRateDetailsManager : IBusinessManager<POSCurrencyRateDetails, int>, IAdvancedSearch
    {
        POSCurrencyRateDetailsLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<POSCurrencyRateDetailsLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
        string SaveIntegration(List<POSCurrencyRateDetails> currenyRateList);
    }
}
