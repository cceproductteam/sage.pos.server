using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;
using CentrixERP.Common.Business.Factory;

namespace CentrixERP.Business.IManager
{
    public interface IPosSyncLogManager : IBusinessManager<PosSyncLog, int>, IAdvancedSearch
    {
        PosSyncLogLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<PosSyncLogLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
    }
}
