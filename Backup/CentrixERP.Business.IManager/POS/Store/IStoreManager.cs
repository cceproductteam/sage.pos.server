using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;
using CentrixERP.Common.Business.Factory;
using CentrixERP.Business.Entity;

namespace CentrixERP.Business.IManager
{
    public interface IStoreManager : IBusinessManager<Store, int>, IAdvancedSearch
    {
        StoreLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<StoreLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
    }
}
