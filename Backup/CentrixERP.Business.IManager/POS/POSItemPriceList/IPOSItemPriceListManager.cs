using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;
using CentrixERP.Common.Business.Factory;

namespace CentrixERP.Business.IManager
{
    public interface IPOSItemPriceListManager : IBusinessManager<POSItemPriceList, int>, IAdvancedSearch
    {
        POSItemPriceListLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<POSItemPriceListLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
        string SaveIntegration(List<POSItemPriceList> ItemPriceList);
    }
}
