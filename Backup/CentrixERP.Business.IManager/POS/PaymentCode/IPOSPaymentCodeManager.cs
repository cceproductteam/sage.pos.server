using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;
using CentrixERP.Common.Business.Factory;

namespace CentrixERP.Business.IManager
{
    public interface IPOSPaymentCodeManager : IBusinessManager<POSPaymentCode, int>, IAdvancedSearch
    {
        POSPaymentCodeLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<POSPaymentCodeLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
        
    }
}
