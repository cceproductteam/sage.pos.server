using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;
using CentrixERP.Common.Business.Factory;

namespace CentrixERP.Business.IManager
{
    public interface IPosSyncConfigurationManager : IBusinessManager<PosSyncConfiguration, int>, IAdvancedSearch
    {
        PosSyncConfigurationLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<PosSyncConfigurationLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
    }
}
