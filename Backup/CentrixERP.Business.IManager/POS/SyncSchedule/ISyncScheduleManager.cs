using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;
using CentrixERP.Common.Business.Factory;

namespace CentrixERP.Business.IManager
{
    public interface ISyncScheduleManager : IBusinessManager<SyncSchedule, int>, IAdvancedSearch
    {
        SyncScheduleLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<SyncScheduleLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
        List<SyncSchedule> FindAllToSync(int status);
    }
}
