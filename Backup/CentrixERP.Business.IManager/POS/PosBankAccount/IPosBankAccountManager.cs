using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;
using CentrixERP.Common.Business.Factory;

namespace CentrixERP.Business.IManager
{
    public interface IPosBankAccountManager : IBusinessManager<PosBankAccount, int>, IAdvancedSearch
    {
        PosBankAccountLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<PosBankAccountLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
    }
}
