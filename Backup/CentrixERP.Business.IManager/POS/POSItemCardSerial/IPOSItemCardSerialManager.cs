using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;

using CentrixERP.Common.Business.Factory;
using CentrixERP.Business.Entity;

namespace CentrixERP.Business.IManager
{
    public interface IPOSItemCardSerialManager : IBusinessManager<POSItemCardSerial, int>, IAdvancedSearch
    {
        POSItemCardSerialLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<POSItemCardSerialLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
        string SaveIntegration(List<POSItemCardSerial> SerialList);
    }
}
