using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;
using CentrixERP.Common.Business.Factory;

namespace CentrixERP.Business.IManager
{
    public interface ICompanyManager : IBusinessManager<Company, int>, IAdvancedSearch
    {
        CompanyLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<CompanyLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
        void UpdateSyncStatus(string trasactionNumber, int EntityId);
    }
}
