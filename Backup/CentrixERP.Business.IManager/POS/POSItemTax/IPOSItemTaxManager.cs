using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;
using CentrixERP.Common.Business.Factory;

namespace CentrixERP.Business.IManager
{
    public interface IPOSItemTaxManager : IBusinessManager<POSItemTax, int>, IAdvancedSearch
    {
        POSItemTaxLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<POSItemTaxLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
        string SaveIntegration(List<POSItemTax> itemTaxList);
    }
}
