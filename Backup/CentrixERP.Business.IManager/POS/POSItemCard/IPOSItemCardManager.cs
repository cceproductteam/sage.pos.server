using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Common.Business.Entity;
using CentrixERP.Common.Business.Factory;
using CentrixERP.Business.Entity;

namespace CentrixERP.Business.IManager
{
    public interface IPOSItemCardManager : IBusinessManager<POSItemCard, int>, IAdvancedSearch
    {
        POSItemCardLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<POSItemCardLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
        BOItemCardQl FindItemQl(CriteriaBase myCriteria);
        List<BOItemCardQl> SearchItemQl(CriteriaBase myCriteria);
        BOItemCardQl SearchItemSerial(CriteriaBase myCriteria, bool status);
        string SaveIntegration(List<POSItemCard> itemCardList);
        List<POSItemCard> FindAll();
    }
}
