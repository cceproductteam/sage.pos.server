using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;
using CentrixERP.Common.Business.Factory;

namespace CentrixERP.Business.IManager
{
    public interface IPOSStockManager : IBusinessManager<POSStock, int>, IAdvancedSearch
    {
        POSStockLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<POSStockLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
        string SaveIntegration(List<POSStock> stockList);
    }
}
