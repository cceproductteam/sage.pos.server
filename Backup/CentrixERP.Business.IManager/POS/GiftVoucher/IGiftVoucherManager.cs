using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;
using CentrixERP.Common.Business.Factory;

namespace CentrixERP.Business.IManager
{
    public interface IGiftVoucherManager : IBusinessManager<GiftVoucher, int>, IAdvancedSearch
    {
        GiftVoucherLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<GiftVoucherLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
        GiftVoucher search(string giftCode);
        void EditVoucher(int id);
    }
}
