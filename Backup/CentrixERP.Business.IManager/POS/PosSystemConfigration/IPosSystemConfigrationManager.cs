using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;
using CentrixERP.Common.Business.Factory;

namespace CentrixERP.Business.IManager
{
    public interface IPosSystemConfigrationManager : IBusinessManager<PosSystemConfigration, int>, IAdvancedSearch
    {
       List<PosSystemConfigrationLite> FindAllLite(CriteriaBase myCriteria);
    }
}
