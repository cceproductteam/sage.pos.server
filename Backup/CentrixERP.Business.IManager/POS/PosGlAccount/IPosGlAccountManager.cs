using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;
using CentrixERP.Common.Business.Factory;

namespace CentrixERP.Business.IManager
{
    public interface IPosGlAccountManager : IBusinessManager<PosGlAccount, int>, IAdvancedSearch
    {
        PosGlAccountLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<PosGlAccountLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
    }
}
