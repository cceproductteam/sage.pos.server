using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;
using CentrixERP.Common.Business.Factory;

namespace CentrixERP.Business.IManager
{
    public interface IRegisterManager : IBusinessManager<Register, int>, IAdvancedSearch
    {
        RegisterLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<RegisterLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
    }
}
