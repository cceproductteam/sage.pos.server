using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Common.Business.Entity;

namespace CentrixERP.Common.Business.IManager
{
    public interface IDocumentEntityManager : IBusinessManager<DocumentEntity, int>//, IAdvancedSearch
    {
        DocumentEntityLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<DocumentEntityLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
        List<DocumentEntity> FindByEntityId(DocumentEntityCriteria myCriteria);
    }
}
