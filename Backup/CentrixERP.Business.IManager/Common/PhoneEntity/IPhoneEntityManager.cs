using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;
using CentrixERP.Common.Business.Entity;


namespace CentrixERP.Common.Business.IManager
{
    public interface IPhoneEntityManager : IBusinessManager<PhoneEntity, int>//, IAdvancedSearch
    {
        PhoneEntityLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<PhoneEntityLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
    }
}
