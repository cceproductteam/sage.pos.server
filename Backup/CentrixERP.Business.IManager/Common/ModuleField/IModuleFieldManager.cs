using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Common.Business.Entity;

namespace CentrixERP.Common.Business.IManager
{    
    public interface IModuleFieldManager: IBusinessManager< ModuleField, int>
    {
        List<SearchFieldCriteria> FindAllSearchCriteria();
        SearchFieldCriteria FindSearchFieldCriteriaById(int SearchFieldCriteriaId);
    }
}


