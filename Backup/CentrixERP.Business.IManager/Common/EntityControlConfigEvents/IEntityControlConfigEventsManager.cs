using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Common.Business.Entity;

namespace CentrixERP.Common.Business.IManager
{
    public interface IEntityControlConfigEventsManager : IBusinessManager<EntityControlConfigEvents,int>
    {
        List<EntityControlConfigEvents_Lite> FindAllLite(CriteriaBase myCriteria);
    }
}
