using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CentrixERP.Common.Business.Entity;
using SF.Framework;
using SF.FrameworkEntity;

namespace CentrixERP.Common.Business.IManager
{
    public interface INoteManager : IBusinessManager<Note, int>
    {
    }
}
