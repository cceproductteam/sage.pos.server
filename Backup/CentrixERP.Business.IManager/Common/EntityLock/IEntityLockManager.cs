using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Common.Business.Entity;

namespace CentrixERP.Common.Business.IManager
{
    public interface IEntityLockManager : IBusinessManager<EntityLock, int>
    {
        //void Lock(int entityId, int? entityValueId);
        void UnLock(int entityId, int? entityValueId);
        void UnLock(int? userId);
        List<EntityLockLite> FindAllLite(CriteriaBase myCriteria);
        void LockWithOutCheck(EntityLock myEntity);
    }
}
