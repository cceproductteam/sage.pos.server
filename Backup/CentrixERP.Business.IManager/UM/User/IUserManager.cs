using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using Centrix.UM.Business.Entity;
using CentrixERP.Common.Business.Entity;
using CentrixERP.Common.Business.Factory;

namespace Centrix.UM.Business.IManager
{
    public interface IUserManager : IBusinessManager<User, int>, IAdvancedSearch
    {
        User Login(string UserName, string Password);
        User FindLoggedInUser(int UserId);
        void Logout();
        bool IsLoggedIn();
        bool HasPermission(CentrixERP.Configuration.Enums_S3.Permissions.SystemModules permission, User myUser);
        bool HasPermission(int permission, User myUser);
        List<User> FindAll_RecentlyViewed(int userId, int entityId, UserCriteria myCriteria);
        List<User> FindAllTaskUsers(UserCriteria myCriteria);

        UserLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<UserLite> FindAllLite(CriteriaBase myCriteria);
        UserLite FindUserManager(int Id, CriteriaBase myCriteria);

        Dictionary<string, string> ValidateUser(User user);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
        bool CheckUserPassword(int userId, string password);
        List<RoleUsersLite> FindRoleUsersLite(int RoleId);
        void AddUserRoles(int roleId, string RoleUserIds);
        void AddUserTeams(int teamId, string TeamUserIds);
        List<TeamUsersLite> FindTeamUsersLite(int TeamId);
        bool CheckUserCount();
        User ClientLogin(string UserName, string Password, string AccessCode);
    }


}
