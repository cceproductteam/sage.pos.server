using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using Centrix.UM.Business.Entity;

namespace Centrix.UM.Business.IManager
{
    public interface IRoleManager : IBusinessManager<Role, int>
    {
        List<Role> FindAll_RecentlyViewed(int userId, int entityId, RoleCriteria myCriteria);
        List<Role_Lite> FindAllLite(RoleCriteria myCriteria);
        Role_Lite FindByIdLite(int Id);
    }
}
