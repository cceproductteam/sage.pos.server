using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;
using CentrixERP.Common.Business.Factory;

namespace CentrixERP.Business.IManager
{
    public interface IInvoiceItemManager : IBusinessManager<InvoiceItem, int>, IAdvancedSearch
    {
        InvoiceItemLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<InvoiceItemLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
        List<InvoiceItemIntegration> InvoiceItemIntegrationFindAll(CriteriaBase myCriteria);
    }
}
