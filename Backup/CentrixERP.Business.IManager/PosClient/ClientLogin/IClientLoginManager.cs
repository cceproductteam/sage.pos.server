using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using Centrix.UM.Business.Entity;
using CentrixERP.Common.Business.Factory;

namespace CentrixERP.Business.IManager
{
    public interface IClientLoginManager : IBusinessManager<User, int>, IAdvancedSearch
    {
        User ClientLogin(string UserName, string Password, string AccessCode);
    }
}
