using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;
using CentrixERP.Common.Business.Factory;

namespace CentrixERP.Business.IManager
{
    public interface IInvoicePaymentsManager : IBusinessManager<InvoicePayments, int>, IAdvancedSearch
    {
        InvoicePaymentsLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<InvoicePaymentsLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);

        int GetParentInvoiceId(int refundInvoiceId);
    }
}
