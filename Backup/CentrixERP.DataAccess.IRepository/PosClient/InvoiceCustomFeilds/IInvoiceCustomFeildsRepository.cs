using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;

using CentrixERP.Business.Entity;


namespace CentrixERP.DataAccess.IRepository
{
    public interface IInvoiceCustomFeildsRepository : IRepository<InvoiceCustomFeilds, int>
    {
        InvoiceCustomFeildsLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<InvoiceCustomFeildsLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
    }
}
