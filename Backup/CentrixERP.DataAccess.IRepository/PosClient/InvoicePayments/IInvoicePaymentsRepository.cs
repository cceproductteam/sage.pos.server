using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
//using Centrix.POS.Standalone.Client.Business.Entity;
using CentrixERP.Business.Entity;

namespace CentrixERP.DataAccess.IRepository
{
    public interface IInvoicePaymentsRepository : IRepository<InvoicePayments, int>
    {
        InvoicePaymentsLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<InvoicePaymentsLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
        int GetParentInvoiceId(int refundInvoiceId);
    }
}
