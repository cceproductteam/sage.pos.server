using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
//using Centrix.POS.Standalone.Client.Business.Entity;
using CentrixERP.Business.Entity;

namespace CentrixERP.DataAccess.IRepository
{
    public interface IInvoiceItemRepository : IRepository<InvoiceItem, int>
    {
        InvoiceItemLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<InvoiceItemLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
        List<InvoiceItemIntegration> InvoiceItemIntegrationFindAll(CriteriaBase myCriteria);
    }
}
