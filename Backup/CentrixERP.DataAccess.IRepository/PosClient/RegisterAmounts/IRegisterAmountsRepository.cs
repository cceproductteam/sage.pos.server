using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
//using Centrix.POS.Standalone.Client.Business.Entity;
using CentrixERP.Business.Entity;
namespace CentrixERP.DataAccess.IRepository
{
    public interface IRegisterAmountsRepository : IRepository<RegisterAmounts, int>
    {
        RegisterAmountsLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<RegisterAmountsLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
    }
}
