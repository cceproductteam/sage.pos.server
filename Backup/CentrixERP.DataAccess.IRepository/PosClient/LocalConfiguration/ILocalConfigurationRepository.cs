using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
//using Centrix.POS.Standalone.Client.Business.Entity;
using CentrixERP.Business.Entity;

namespace CentrixERP.DataAccess.IRepository
{
    public interface ILocalConfigurationRepository : IRepository<LocalConfiguration, int>
    {
        LocalConfigurationLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<LocalConfigurationLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
       
    }
}
