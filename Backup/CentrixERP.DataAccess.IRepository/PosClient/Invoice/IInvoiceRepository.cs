using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;

using CentrixERP.Business.Entity;

namespace CentrixERP.DataAccess.IRepository
{
    public interface IInvoiceRepository : IRepository<Invoice, int>
    {
        InvoiceLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<InvoiceLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
        Invoice FindLastInvoice(CriteriaBase myCriteria);
        Invoice FindInvoiceByNumber(CriteriaBase myCriteria);
        List<Invoice> FindAllQl(CriteriaBase myCriteria, string phoneNumber);
        List<Invoice> FindByCustomer(CriteriaBase myCriteria);
        List<InvoiceIntegration> InvoiceIntegrationFindAll(CriteriaBase myCriteria);
        void UpdateTransactionStatus(int transactionType, string transactionRef, string transactionNumber, string status, bool IsConvertedToShipment);
        void UpdateSyncStatus(string trasactionNumber, int EntityId);

        List<InvoiceLite> searchRefund(CriteriaBase myCriteria);
        void UpdateInvoiceTransNumber(int invoiceId, int shipmentId, string shipmentNumber);

        List<InvoiceInquiryLite> InvoiceInquiryResults(string StoreIds, DateTime? InvoiceDateFrom, DateTime? InvoiceDateTo);
        List<POSQuantityInQuiryLite> FindInquiryResults(int LocationId, int StoreId, int ItemId);
        List<ICQuantityInQuiryLite> FindICInquiryResults(int LocationId, int StoreId, int ItemId);
        bool CheckParentShipmentValidity();
        void UpdateAccpacInvoiceNumber(string invoiceUniqueNumber, string AccpacInvoiceNum, string AccpacOrderentryNum, string AccpacShipmentNum, string AccpacPrePaymnentNumber, string accpacRefundNumber);
    }
}
