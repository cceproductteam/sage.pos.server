using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Common.Business.Entity;

namespace CentrixERP.Common.DataAccess.IRepository
{
    public interface IDuplicationRuleFilterTypeRepository : IRepository<DuplicationRuleFilterType, int>
    {
        List<DuplicationRuleFilterType> DuplicationFilter_FindAll(CriteriaBase myCriteria);
    }
}
