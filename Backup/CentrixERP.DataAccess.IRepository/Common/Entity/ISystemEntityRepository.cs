using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Common.Business.Entity;

namespace CentrixERP.Common.DataAccess.IRepository
{
    public interface ISystemEntityRepository : IRepository<SystemEntity, int>
    {
        List<EntityRelations> CheckEntityRelations(int entityId, int entityValueId, CriteriaBase myCriteria);
    }
}
