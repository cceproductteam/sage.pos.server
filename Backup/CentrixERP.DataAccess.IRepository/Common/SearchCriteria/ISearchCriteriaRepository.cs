using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CentrixERP.Common.Business.Entity;
using SF.FrameworkEntity;

namespace CentrixERP.Common.DataAccess.IRepository
{
    public interface ISearchCriteriaRepository : IRepository<SearchCriteria, int>
    {
        
    }
}
