using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using CentrixERP.Common.Business.Entity;

namespace CentrixERP.Common.DataAccess.IRepository
{
    public interface IDataTypeContentRepository : IRepository<DataTypeContent, int>
    {
        void ReorderDataTypeContent(string dataTypeContentIDs);
        List<CentrixERP.Common.Business.Entity.DataTypeContentLite> FindAllByIds(string Ids);
    }
}
