using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using CentrixERP.Common.Business.Entity;

namespace CentrixERP.Common.DataAccess.IRepository
{
    public interface IDocumentRepository : IRepository<Document, int>
    {
        void UpdateOrder(int DocumentID, int EntityId, int EntityValueId, int OrderID);
    }
}
