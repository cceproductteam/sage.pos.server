using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Common.Business.Entity;

namespace CentrixERP.Common.DataAccess.IRepository
{
    public interface IPhoneEntityRepository : IRepository<PhoneEntity, int>
    {
        PhoneEntityLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<PhoneEntityLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
    }
}
