using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Common.Business.Entity;

namespace CentrixERP.Common.DataAccess.IRepository
{
    public interface IDocumentFolderRepository : IRepository<DocumentFolder, int>
    {
        void DeleteFolder(DocumentFolder myEntity, int newFolderId);
    }
}
