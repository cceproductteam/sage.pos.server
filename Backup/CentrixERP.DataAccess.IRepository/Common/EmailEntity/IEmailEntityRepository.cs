using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Common.Business.Entity;

namespace CentrixERP.Common.DataAccess.IRepository
{
    public interface IEmailEntityRepository : IRepository<EmailEntity, int>
    {
        EmailEntityLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<EmailEntityLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
    }

}
