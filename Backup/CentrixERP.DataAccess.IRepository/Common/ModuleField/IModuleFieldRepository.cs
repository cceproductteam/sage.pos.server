
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.Framework;
using SF.FrameworkEntity;
using CentrixERP.Common.Business.Entity;


namespace CentrixERP.Common.DataAccess.IRepository
{
    public interface IModuleFieldRepository : IRepository< ModuleField,int>
    {
        List<SearchFieldCriteria> FindAllSearchCriteria();
        SearchFieldCriteria FindSearchFieldCriteriaById(int SearchFieldCriteriaId);
    }
}


