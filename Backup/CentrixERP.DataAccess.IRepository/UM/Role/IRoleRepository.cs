using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using Centrix.UM.Business.Entity;

namespace Centrix.UM.DataAccess.IRepository
{
    public interface IRoleRepository : IRepository<Role, int>
    {
         List<Role> FindAll_RecentlyViewed(int userId, int entityId, RoleCriteria myCriteria);
         List<Role_Lite> FindAllLite(RoleCriteria myCriteria);
         Role_Lite FindByIdLite(int Id);
    }
}
