using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using Centrix.UM.Business.Entity;

namespace Centrix.UM.DataAccess.IRepository
{
    public interface IUserStoreShifsRepository : IRepository<UserStoreShifs, int>
    {
        UserStoreShifsLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<UserStoreShifsLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
    }
}
