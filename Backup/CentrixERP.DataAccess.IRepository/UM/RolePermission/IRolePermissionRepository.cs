using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using Centrix.UM.Business.Entity;

namespace Centrix.UM.DataAccess.IRepository
{
    public interface IRolePermissionRepository : IRepository<RolePermission, int>
    {
        List<RolePermissionLite> FindAllRolePermission(int RoleId, CriteriaBase myCriteria);
        List<RolePermissionLite> FindRolePermission(int RoleId, CriteriaBase myCriteria);
        void Save(int roleId, string permissionStatusIds, int createdBy);
        void DeleteByRole(int roleId);
        void SetAllPermissions(bool all, int roleId, int userId);
       
        int LockRolePermissions(CriteriaBase myCriteria);
        void UpdateRolePermissionUpdatedDate(CriteriaBase myCriteria);
        
    }
}
