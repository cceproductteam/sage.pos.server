using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;

namespace CentrixERP.DataAccess.IRepository
{
    public interface IPOSStockRepository : IRepository<POSStock, int>
    {
        POSStockLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<POSStockLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
        string SaveIntegration(List<POSStock> stockList);
    }
}
