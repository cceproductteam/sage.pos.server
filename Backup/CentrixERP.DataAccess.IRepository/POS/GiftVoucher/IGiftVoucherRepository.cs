using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;

namespace CentrixERP.DataAccess.IRepository
{
    public interface IGiftVoucherRepository : IRepository<GiftVoucher, int>
    {
        GiftVoucherLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<GiftVoucherLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
        GiftVoucher search(string giftCode);
        void EditVoucher(int id);
    }
}
