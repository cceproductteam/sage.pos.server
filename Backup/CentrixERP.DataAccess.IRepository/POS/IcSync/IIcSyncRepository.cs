using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;

namespace CentrixERP.DataAccess.IRepository
{
    public interface IIcSyncRepository : IRepository<IcSync, int>
    {
        IcSyncLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<IcSyncLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
    }
}
