using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;

namespace CentrixERP.DataAccess.IRepository
{
    public interface ICustomFildRepository : IRepository<CustomFild, int>
    {
        CustomFildLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<CustomFildLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
        void UpdateSyncStatus(string trasactionNumber, int EntityId);
    }
}
