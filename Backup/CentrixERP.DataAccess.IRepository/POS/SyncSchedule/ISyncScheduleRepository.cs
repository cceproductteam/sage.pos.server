using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;

namespace CentrixERP.DataAccess.IRepository
{
    public interface ISyncScheduleRepository : IRepository<SyncSchedule, int>
    {
        SyncScheduleLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<SyncScheduleLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
        List<SyncSchedule> FindAllToSync(int status);
    }
}
