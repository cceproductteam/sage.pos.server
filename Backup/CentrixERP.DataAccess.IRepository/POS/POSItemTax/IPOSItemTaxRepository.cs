using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;

namespace CentrixERP.DataAccess.IRepository
{
    public interface IPOSItemTaxRepository : IRepository<POSItemTax, int>
    {
        POSItemTaxLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<POSItemTaxLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
        string SaveIntegration(List<POSItemTax> itemTaxList);
    }
}
