﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;

namespace CentrixERP.DataAccess.IRepository 
{

    public interface IPOSSyncRepository : IRepository<POSSync, int>
    {
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
        void POSSyncData(Object[] data);
    }

}
