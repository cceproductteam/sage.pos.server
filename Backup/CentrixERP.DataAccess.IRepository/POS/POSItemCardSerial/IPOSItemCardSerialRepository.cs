using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;


namespace CentrixERP.DataAccess.IRepository
{
    public interface IPOSItemCardSerialRepository : IRepository<POSItemCardSerial, int>
    {
        POSItemCardSerialLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<POSItemCardSerialLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
        string SaveIntegration(List<POSItemCardSerial> SerialList);
    }
}
