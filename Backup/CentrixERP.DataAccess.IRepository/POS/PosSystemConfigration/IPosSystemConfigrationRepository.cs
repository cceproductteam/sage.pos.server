using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;

namespace CentrixERP.DataAccess.IRepository
{
    public interface IPosSystemConfigrationRepository : IRepository<PosSystemConfigration, int>
    {
        List<PosSystemConfigrationLite> FindAllLite(CriteriaBase myCriteria);
    }
}
