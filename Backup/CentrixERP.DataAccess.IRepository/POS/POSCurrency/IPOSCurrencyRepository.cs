using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;

namespace CentrixERP.DataAccess.IRepository
{
    public interface IPOSCurrencyRepository : IRepository<POSCurrency, int>
    {
        POSCurrencyLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<POSCurrencyLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);

        string SaveIntegration(List<POSCurrency> currencyList);
    }
}
