using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;

namespace CentrixERP.DataAccess.IRepository
{
    public interface IStoreRepository : IRepository<Store, int>
    {
        StoreLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<StoreLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
    }
}
