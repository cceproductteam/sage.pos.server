using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;

namespace CentrixERP.DataAccess.IRepository
{
    public interface IPOSCustomerRepository : IRepository<POSCustomer, int>
    {
        POSCustomerLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<POSCustomerLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
        string SaveIntegration(List<POSCustomer> CustomerList);
    }
}
