using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;

namespace CentrixERP.DataAccess.IRepository
{
    public interface IPOSItemPriceListRepository : IRepository<POSItemPriceList, int>
    {
        POSItemPriceListLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<POSItemPriceListLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);

        string SaveIntegration(List<POSItemPriceList> ItemPriceList); 
    }
}
