using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;

namespace CentrixERP.DataAccess.IRepository
{
    public interface IPOSCurrencyRateDetailsRepository : IRepository<POSCurrencyRateDetails, int>
    {
        POSCurrencyRateDetailsLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<POSCurrencyRateDetailsLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
        string SaveIntegration(List<POSCurrencyRateDetails> currenyRateList);
    }
}
