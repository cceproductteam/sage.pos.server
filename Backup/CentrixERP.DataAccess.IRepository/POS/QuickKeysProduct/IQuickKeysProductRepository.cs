using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;

namespace CentrixERP.DataAccess.IRepository
{
    public interface IQuickKeysProductRepository : IRepository<QuickKeysProduct, int>
    {
        QuickKeysProductLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<QuickKeysProductLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
    }
}
