using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;

namespace CentrixERP.DataAccess.IRepository
{
    public interface IPosSyncLogRepository : IRepository<PosSyncLog, int>
    {
        PosSyncLogLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<PosSyncLogLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
    }
}
