using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;

namespace CentrixERP.DataAccess.IRepository
{
    public interface IPosGlAccountRepository : IRepository<PosGlAccount, int>
    {
        PosGlAccountLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<PosGlAccountLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
    }
}
