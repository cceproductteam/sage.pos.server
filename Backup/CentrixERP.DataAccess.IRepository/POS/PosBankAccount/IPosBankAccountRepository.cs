using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using CentrixERP.Business.Entity;

namespace CentrixERP.DataAccess.IRepository
{
    public interface IPosBankAccountRepository : IRepository<PosBankAccount, int>
    {
        PosBankAccountLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<PosBankAccountLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
    }
}
