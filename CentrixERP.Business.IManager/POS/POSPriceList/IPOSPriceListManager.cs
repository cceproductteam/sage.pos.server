using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;
using SagePOS.Server.Business.Factory;

namespace SagePOS.Server.Business.IManager
{
    public interface IPOSPriceListManager : IBusinessManager<POSPriceList, int>, IAdvancedSearch
    {
        POSPriceListLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<POSPriceListLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
        string SaveIntegration(List<POSPriceList> IcPriceList);
    }
}
