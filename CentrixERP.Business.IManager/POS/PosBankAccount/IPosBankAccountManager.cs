using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;
using SagePOS.Server.Business.Factory;

namespace SagePOS.Server.Business.IManager
{
    public interface IPosBankAccountManager : IBusinessManager<PosBankAccount, int>, IAdvancedSearch
    {
        PosBankAccountLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<PosBankAccountLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
    }
}
