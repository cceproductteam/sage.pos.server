using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;
using SagePOS.Server.Business.Factory;
using SagePOS.Server.Business.Entity;

namespace SagePOS.Server.Business.IManager
{
    public interface IStoreManager : IBusinessManager<Store, int>, IAdvancedSearch
    {
        StoreLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<StoreLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
    }
}
