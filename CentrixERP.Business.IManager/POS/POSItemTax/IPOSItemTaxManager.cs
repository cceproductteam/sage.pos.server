using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;
using SagePOS.Server.Business.Factory;

namespace SagePOS.Server.Business.IManager
{
    public interface IPOSItemTaxManager : IBusinessManager<POSItemTax, int>, IAdvancedSearch
    {
        POSItemTaxLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<POSItemTaxLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
        string SaveIntegration(List<POSItemTax> itemTaxList);
    }
}
