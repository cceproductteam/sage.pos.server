using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;
using SagePOS.Server.Business.Factory;

namespace SagePOS.Server.Business.IManager
{
    public interface IPosSystemConfigrationManager : IBusinessManager<PosSystemConfigration, int>, IAdvancedSearch
    {
       List<PosSystemConfigrationLite> FindAllLite(CriteriaBase myCriteria);
    }
}
