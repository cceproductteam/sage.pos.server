using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;

using SagePOS.Server.Business.Factory;
using SagePOS.Server.Business.Entity;

namespace SagePOS.Server.Business.IManager
{
    public interface IBoCurrencyManager : IBusinessManager<BoCurrency, int>, IAdvancedSearch
    {
        BoCurrencyLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<BoCurrencyLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
    }
}
