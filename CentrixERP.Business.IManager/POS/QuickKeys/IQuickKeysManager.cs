using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;
using SagePOS.Server.Business.Factory;

namespace SagePOS.Server.Business.IManager
{
    public interface IQuickKeysManager : IBusinessManager<QuickKeys, int>, IAdvancedSearch
    {
        QuickKeysLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<QuickKeysLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
    }
}
