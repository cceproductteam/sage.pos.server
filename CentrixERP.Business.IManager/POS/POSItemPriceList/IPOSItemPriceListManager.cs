using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;
using SagePOS.Server.Business.Factory;

namespace SagePOS.Server.Business.IManager
{
    public interface IPOSItemPriceListManager : IBusinessManager<POSItemPriceList, int>, IAdvancedSearch
    {
        POSItemPriceListLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<POSItemPriceListLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
        string SaveIntegration(List<POSItemPriceList> ItemPriceList);
    }
}
