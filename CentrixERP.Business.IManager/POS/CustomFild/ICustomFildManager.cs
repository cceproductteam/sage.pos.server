using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;
using SagePOS.Server.Business.Factory;

namespace SagePOS.Server.Business.IManager
{
    public interface ICustomFildManager : IBusinessManager<CustomFild, int>, IAdvancedSearch
    {
        CustomFildLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<CustomFildLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
        void UpdateSyncStatus(string trasactionNumber, int EntityId);
    }
}
