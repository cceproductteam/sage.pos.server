using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;


using SagePOS.Server.Business.Factory;
using SagePOS.Server.Business.Entity;

namespace SagePOS.Server.Business.IManager
{
    public interface IAccpacSyncScheduleManager : IBusinessManager<AccpacSyncSchedule, int>, IAdvancedSearch
    {
        AccpacSyncScheduleLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<AccpacSyncScheduleLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
        List<AccpacSyncLogLite> FindAllLiteLog(string datefrom, string dateto, string AccpacSyncUniqueNumber);

    }
}
