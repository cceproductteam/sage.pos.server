using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;
using SagePOS.Server.Business.Factory;

namespace SagePOS.Server.Business.IManager
{
    public interface IPosGlAccountManager : IBusinessManager<PosGlAccount, int>, IAdvancedSearch
    {
        PosGlAccountLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<PosGlAccountLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
    }
}
