using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;
using SagePOS.Server.Business.Factory;

namespace SagePOS.Server.Business.IManager
{
    public interface IPOSCustomerManager : IBusinessManager<POSCustomer, int>, IAdvancedSearch
    {
        POSCustomerLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<POSCustomerLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
        string SaveIntegration(List<POSCustomer> CustomerList);
    }
}
