using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Factory;
using SagePOS.Server.Business.Entity;


namespace SagePOS.Server.Business.IManager
{
    public interface IBoBankManager : IBusinessManager<BoBank, int>, IAdvancedSearch
    {
        BoBankLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<BoBankLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
    }
}
