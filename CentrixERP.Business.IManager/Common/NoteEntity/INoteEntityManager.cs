using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Manger.Common.Business.Entity;

namespace CentrixERP.Common.Business.IManager
{
    public interface INoteEntityManager : IBusinessManager<NoteEntity, int>//, IAdvancedSearch
    {
        NoteEntityLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<NoteEntityLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
    }
}
