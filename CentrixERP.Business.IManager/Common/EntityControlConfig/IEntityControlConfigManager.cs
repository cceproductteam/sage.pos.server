using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Manger.Common.Business.Entity;

namespace CentrixERP.Common.Business.IManager
{
    public interface IEntityControlConfigManager : IBusinessManager<EntityControlConfig,int>
    {
        List<EntityControlConfig_Lite> FindAllLite(CriteriaBase myCriteria);
    }
}
