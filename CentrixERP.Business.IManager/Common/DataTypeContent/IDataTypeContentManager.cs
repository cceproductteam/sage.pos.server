using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.Framework;
using SF.FrameworkEntity;
using SagePOS.Manger.Common.Business.Entity;

namespace CentrixERP.Common.Business.IManager
{
    public interface IDataTypeContentManager : IBusinessManager<DataTypeContent, int>
    {
        void ReorderDataTypeContent(string dataTypeContentIDs);
        List<SagePOS.Manger.Common.Business.Entity.DataTypeContentLite> FindAllByIds(string Ids);

        
    }
}
