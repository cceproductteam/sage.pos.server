using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.Framework;
using SF.FrameworkEntity;
using SagePOS.Manger.Common.Business.Entity;
using SagePOS.Server.Configuration;

namespace CentrixERP.Common.Business.IManager
{
    public interface IAddressManager : IBusinessManager<Address, int>
    {

        string AddressName(Address address, string addressFormat, Enums_S3.Configuration.Language lang);
        string AddressName(int addressID, string addressFormat, Enums_S3.Configuration.Language lang);
    }
}
