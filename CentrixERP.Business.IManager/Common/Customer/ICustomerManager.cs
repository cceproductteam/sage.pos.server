using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;
using SagePOS.Server.Business.Factory;

namespace SagePOS.Server.Business.IManager
{
    public interface ICustomerManager : IBusinessManager<Customer, int>, IAdvancedSearch
    {
        List<CustomerLite> FindAllLite(CriteriaBase myCriteria);
        CustomerLite FindByIdLite(int Id, CriteriaBase myCriteria);
        double FindPendingAmount(int Id, CriteriaBase myCriteria);
        List<POSIntegrationCustomer> POSIntegrationCustomerFindAll(DateTime? LastSyncDate);
    }
}
