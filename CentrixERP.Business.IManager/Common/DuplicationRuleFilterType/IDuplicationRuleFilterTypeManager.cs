using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Manger.Common.Business.Entity;

namespace CentrixERP.Common.Business.IManager
{
    public interface IDuplicationRuleFilterTypeManager : IBusinessManager<DuplicationRuleFilterType, int>
    {
        List<DuplicationRuleFilterType> DuplicationFilter_FindAll(CriteriaBase myCriteria);
    }
}
