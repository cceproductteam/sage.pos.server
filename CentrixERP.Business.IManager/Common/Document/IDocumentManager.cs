using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.Framework;
using SF.FrameworkEntity;
using SagePOS.Manger.Common.Business.Entity;

namespace CentrixERP.Common.Business.IManager
{
    public interface IDocumentManager : IBusinessManager<Document, int>
    {
        DocumentIcons GetIcon(string extension);
        void UpdateOrder(int DocumentID, int EntityId, int EntityValueId, int OrderID);
    }
}
