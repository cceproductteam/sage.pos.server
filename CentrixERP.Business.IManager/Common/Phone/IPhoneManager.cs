using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.Framework;
using SF.FrameworkEntity;
using SagePOS.Manger.Common.Business.Entity;

namespace CentrixERP.Common.Business.IManager
{
    public interface IPhoneManager : IBusinessManager<Phone, int>
    {
        List<Phone> FindMobileList(string ids, bool forPerson);
    }
}
