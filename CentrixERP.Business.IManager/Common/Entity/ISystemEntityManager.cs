using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Manger.Common.Business.Entity;

namespace CentrixERP.Common.Business.IManager
{
    public interface ISystemEntityManager : IBusinessManager<SystemEntity,int>
    {
        List<EntityRelations> CheckEntityRelations(int entityId, int entityValueId, CriteriaBase myCriteria);
    }
}
