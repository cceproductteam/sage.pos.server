using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Manger.Common.Business.Entity;
//using SagePOS.Server.Business.Factory;

namespace CentrixERP.Common.Business.IManager
{
    public interface IEmailEntityManager : IBusinessManager<EmailEntity, int>//, IAdvancedSearch
    {
        EmailEntityLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<EmailEntityLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
    }
}
