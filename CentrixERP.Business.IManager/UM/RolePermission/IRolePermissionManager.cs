using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using Centrix.UM.Business.Entity;

namespace Centrix.UM.Business.IManager
{
    public interface IRolePermissionManager : IBusinessManager<RolePermission, int>
    {
        List<RolePermissionLite> FindRolePermission(int RoleId, CriteriaBase myCriteria);
        List<RolePermissionLite> FindAllRolePermission(int RoleId, CriteriaBase myCriteria);
        void Save(int roleId, string permissionStatusIds, int createdBy);
        void DeleteByRole(int roleId);
        void SetAllPermissions(bool all, int roleId, int userId);
        int LockRolePermissions(CriteriaBase myCriteria);
        void UpdateRolePermissionUpdatedDate(CriteriaBase myCriteria);

    }
}
