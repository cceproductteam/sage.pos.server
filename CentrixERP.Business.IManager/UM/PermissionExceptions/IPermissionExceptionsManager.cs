using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using Centrix.UM.Business.Entity;


namespace Centrix.UM.Business.IManager
{
    public interface IPermissionExceptionsManager : IBusinessManager<PermissionExceptions, int>
    {
    }
}
