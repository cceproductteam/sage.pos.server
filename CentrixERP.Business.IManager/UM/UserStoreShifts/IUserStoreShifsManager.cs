using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using Centrix.UM.Business.Entity;

namespace Centrix.UM.Business.IManager
{
    public interface IUserStoreShifsManager : IBusinessManager<UserStoreShifs, int>
    {
        UserStoreShifsLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<UserStoreShifsLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
    }
}
