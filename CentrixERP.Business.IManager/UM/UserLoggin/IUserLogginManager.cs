using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using Centrix.UM.Business.Entity;
using SagePOS.Server.Configuration;

namespace Centrix.UM.Business.IManager
{
    public interface IUserLogginManager : IBusinessManager<UserLoggin,int>
    {
        Enums_S3.UserLoggin.UserLogginStatus CheckCurrentSession();
        Enums_S3.UserLoggin.UserLogginStatus ValidateLogginSession(UserLogginCriteria userLogginCriteria);
        void KillUserSession(UserLogginCriteria userLogginCriteria);
        void CreateUserSession(int userId, UserLogginCriteria userLogginCriteria, bool createNewSession);
        User CurrentLoggedUser { get; }
        bool ForceCreateNewSession { get; set; }
        void KillUserSession(int userId);
        //void KillUserLogging(UserLogginCriteria userLogginCriteria);
    }
}
