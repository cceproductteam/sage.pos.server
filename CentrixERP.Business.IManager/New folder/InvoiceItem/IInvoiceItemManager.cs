using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using Centrix.POS.Standalone.Client.Business.Entity;
using CentrixERP.Common.Business.Factory;

namespace Centrix.POS.Standalone.Client.Business.IManager
{
    public interface IInvoiceItemManager : IBusinessManager<InvoiceItem, int>, IAdvancedSearch
    {
        InvoiceItemLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<InvoiceItemLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
        List<InvoiceItemIntegration> InvoiceItemIntegrationFindAll(CriteriaBase myCriteria);
    }
}
