using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using Centrix.POS.Standalone.Client.Business.Entity;
using CentrixERP.Common.Business.Factory;

namespace Centrix.POS.Standalone.Client.Business.IManager
{
    public interface ICloseRegisterManager : IBusinessManager<CloseRegister, int>, IAdvancedSearch
    {
        CloseRegisterLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<CloseRegisterLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
        List<CloseRegiserDetails> GetCloseRegisterDetailsPaymentType(CriteriaBase myCriteria);
        void UpdateSyncStatus(string trasactionNumber, int EntityId);
        List<CloseRegisterInquiryLite> CloseRegisterInquiryResults(string StoreIds, string RegisterIds, DateTime? OpenedDateFrom, DateTime? OpenedDateTo, DateTime? ClosedDateFrom, DateTime? ClosedDateTo);
        CloseRegisterTotals FindCloseRegisterSalesTotal(int CloseRegisterId);
    }
}
