using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using Centrix.POS.Standalone.Client.Business.Entity;
using CentrixERP.Common.Business.Factory;

namespace Centrix.POS.Standalone.Client.Business.IManager
{
    public interface ITransactionTypeManager : IBusinessManager<TransactionType, int>, IAdvancedSearch
    {
        TransactionTypeLite FindByIdLite(int Id, CriteriaBase myCriteria);
        List<TransactionTypeLite> FindAllLite(CriteriaBase myCriteria);
        List<object> AdvancedSearchLite(CriteriaBase myCriteria);
    }
}
