using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using Centrix.UM.Business.Entity;
using SagePOS.Server.Business.Factory;

namespace SagePOS.Server.Business.IManager
{
    public interface IClientLoginManager : IBusinessManager<User, int>, IAdvancedSearch
    {
        User ClientLogin(string UserName, string Password, string AccessCode);
    }
}
