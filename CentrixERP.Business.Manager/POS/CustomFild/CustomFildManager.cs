using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;
using SF.Framework;
using SagePOS.Server.Business.IManager;
using SagePOS.Server.DataAcces.IRepository;

namespace SagePOS.Server.Business.Manager
{
    public class CustomFildManager : BusinessManagerBase<CustomFild, int>, ICustomFildManager
    {
        public CustomFildManager()
            : base((IRepository<CustomFild, int>)IoC.Instance.Resolve(typeof(ICustomFildRepository)))
        {
        
        
        }

        public CustomFildLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((ICustomFildRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<CustomFildLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((ICustomFildRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((ICustomFildRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }
        public void UpdateSyncStatus(string trasactionNumber, int EntityId)
        {
            ((ICustomFildRepository)this.Repository).UpdateSyncStatus(trasactionNumber, EntityId);
        }


        
    }
}
