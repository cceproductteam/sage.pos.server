using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;

using SF.Framework;

using SagePOS.Server.Business.Entity;
using SagePOS.Server.Business.IManager;
using SagePOS.Server.DataAcces.IRepository;

namespace SagePOS.Server.Business.Manager
{
    public class BoCurrencyManager : BusinessManagerBase<BoCurrency,int>,IBoCurrencyManager
    {
        public BoCurrencyManager()
            : base((IRepository<BoCurrency, int>)IoC.Instance.Resolve(typeof(IBoCurrencyRepository)))
        {
        
        
        }

        public BoCurrencyLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((IBoCurrencyRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<BoCurrencyLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((IBoCurrencyRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((IBoCurrencyRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }
    }
}
