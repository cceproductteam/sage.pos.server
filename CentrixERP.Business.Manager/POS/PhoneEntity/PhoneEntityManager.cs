using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;
using SF.Framework;
using SagePOS.Server.Business.IManager;
using SagePOS.Server.DataAcces.IRepository;
using SagePOS.Manger.Common.Business.Entity;
using CentrixERP.Common.Business.IManager;

namespace SagePOS.Server.Business.Manager
{
    public class PhoneEntityManager : BusinessManagerBase<PhoneEntity,int>,IPhoneEntityManager
    {
        public PhoneEntityManager()
            : base((IRepository<PhoneEntity, int>)IoC.Instance.Resolve(typeof(IPhoneEntityRepository)))
        {
        
        
        }

        public PhoneEntityLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((IPhoneEntityRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<PhoneEntityLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((IPhoneEntityRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((IPhoneEntityRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }
        public override List<ValidationError> Validate(PhoneEntity myEntity)
        {
            List<ValidationError> errors = new List<ValidationError>();
            if (SF.Framework.String.IsEmpty(myEntity.PhoneObj.PhoneNumber))
            {
                errors.Add(new ValidationError()
                {
                    ControlName = "EmailAddress",
                    GlobalMessage = false,
                    Line = 1,
                    Message = "required"
                });
            }
            return errors;
        }

    }
}
