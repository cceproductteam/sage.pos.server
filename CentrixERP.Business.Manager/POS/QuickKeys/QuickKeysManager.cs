using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;
using SF.Framework;
using SagePOS.Server.Business.IManager;
using SagePOS.Server.DataAcces.IRepository;

namespace SagePOS.Server.Business.Manager
{
    public class QuickKeysManager : BusinessManagerBase<QuickKeys,int>,IQuickKeysManager
    {
        public QuickKeysManager()
            : base((IRepository<QuickKeys, int>)IoC.Instance.Resolve(typeof(IQuickKeysRepository)))
        {
        
        
        }

        public QuickKeysLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((IQuickKeysRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<QuickKeysLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((IQuickKeysRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((IQuickKeysRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }
    }
}
