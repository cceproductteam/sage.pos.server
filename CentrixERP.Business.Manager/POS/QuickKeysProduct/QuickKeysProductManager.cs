using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;
using SF.Framework;
using SagePOS.Server.Business.IManager;
using SagePOS.Server.DataAcces.IRepository;

namespace SagePOS.Server.Business.Manager
{
    public class QuickKeysProductManager : BusinessManagerBase<QuickKeysProduct,int>,IQuickKeysProductManager
    {
        public QuickKeysProductManager()
            : base((IRepository<QuickKeysProduct, int>)IoC.Instance.Resolve(typeof(IQuickKeysProductRepository)))
        {
        
        
        }

        public QuickKeysProductLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((IQuickKeysProductRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<QuickKeysProductLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((IQuickKeysProductRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((IQuickKeysProductRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }
    }
}
