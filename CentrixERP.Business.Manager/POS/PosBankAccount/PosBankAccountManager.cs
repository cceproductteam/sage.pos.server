using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;
using SF.Framework;
using SagePOS.Server.Business.IManager;
using SagePOS.Server.DataAcces.IRepository;

namespace SagePOS.Server.Business.Manager
{
    public class PosBankAccountManager : BusinessManagerBase<PosBankAccount,int>,IPosBankAccountManager
    {
        public PosBankAccountManager()
            : base((IRepository<PosBankAccount, int>)IoC.Instance.Resolve(typeof(IPosBankAccountRepository)))
        {
        
        
        }

        public PosBankAccountLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((IPosBankAccountRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<PosBankAccountLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((IPosBankAccountRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((IPosBankAccountRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }
    }
}
