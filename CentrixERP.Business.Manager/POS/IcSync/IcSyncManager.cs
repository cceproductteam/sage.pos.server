using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;
using SF.Framework;
using SagePOS.Server.Business.IManager;
using SagePOS.Server.DataAcces.IRepository;

namespace SagePOS.Server.Business.Manager
{
    public class IcSyncManager : BusinessManagerBase<IcSync,int>,IIcSyncManager
    {
        public IcSyncManager()
            : base((IRepository<IcSync, int>)IoC.Instance.Resolve(typeof(IIcSyncRepository)))
        {
        
        
        }

        public IcSyncLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((IIcSyncRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<IcSyncLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((IIcSyncRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((IIcSyncRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }
    }
}
