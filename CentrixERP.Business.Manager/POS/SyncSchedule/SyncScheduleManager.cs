using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;
using SF.Framework;
using SagePOS.Server.Business.IManager;
using SagePOS.Server.DataAcces.IRepository;

namespace SagePOS.Server.Business.Manager
{
    public class SyncScheduleManager : BusinessManagerBase<SyncSchedule,int>,ISyncScheduleManager
    {
        public SyncScheduleManager()
            : base((IRepository<SyncSchedule, int>)IoC.Instance.Resolve(typeof(ISyncScheduleRepository)))
        {
        
        
        }

        public SyncScheduleLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((ISyncScheduleRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<SyncScheduleLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((ISyncScheduleRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((ISyncScheduleRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }
        public List<SyncSchedule> FindAllToSync(int status)
        {
            return ((ISyncScheduleRepository)this.Repository).FindAllToSync(status);
        }

        public void addNewSchedule(int type, int userId)
        {
             ((ISyncScheduleRepository)this.Repository).addNewSchedule(type, userId);
        }

        public NewSyncSchedule checkSynchronizationStatus()
        {
            return ((ISyncScheduleRepository)this.Repository).checkSynchronizationStatus();
        }

        public SyncScheduleNew FindsyncSchedule(int status)
        {
              return ((ISyncScheduleRepository)this.Repository).FindsyncSchedule(status);
        }
        public void UpdateSyncScheduleStatus(int Id)
        {
            ((ISyncScheduleRepository)this.Repository).UpdateSyncScheduleStatus(Id);
        }

        public List<SyncScheduleDetails> getSyncScheduleDetails(string SyncLogNumber)
        {
            return ((ISyncScheduleRepository)this.Repository).getSyncScheduleDetails(SyncLogNumber);
        }

        public List<SyncLogEntityDetails> getSyncScheduleentityDetails(string entity, int action, string logNumber)
        {
            return ((ISyncScheduleRepository)this.Repository).getSyncScheduleentityDetails(entity, action, logNumber);
        }

        public string getLastSyncNumber(int action)
        {
            return ((ISyncScheduleRepository)this.Repository).getLastSyncNumber(action);
        }
    }
}
