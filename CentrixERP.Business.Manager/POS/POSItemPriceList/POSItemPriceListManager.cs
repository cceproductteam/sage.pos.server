using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;
using SF.Framework;
using SagePOS.Server.Business.IManager;
using SagePOS.Server.DataAcces.IRepository;

namespace SagePOS.Server.Business.Manager
{
    public class POSItemPriceListManager : BusinessManagerBase<POSItemPriceList,int>,IPOSItemPriceListManager
    {
        public POSItemPriceListManager()
            : base((IRepository<POSItemPriceList, int>)IoC.Instance.Resolve(typeof(IPOSItemPriceListRepository)))
        {
        
        
        }

        public POSItemPriceListLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((IPOSItemPriceListRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<POSItemPriceListLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((IPOSItemPriceListRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((IPOSItemPriceListRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }

        public string SaveIntegration(List<POSItemPriceList> ItemPriceList)
        {
            return ((IPOSItemPriceListRepository)this.Repository).SaveIntegration(ItemPriceList);
        }
    }
}
