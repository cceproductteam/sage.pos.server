using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;
using SF.Framework;
using SagePOS.Server.Business.IManager;
using SagePOS.Server.DataAcces.IRepository;
using SagePOS.Server.Business.Entity;
using SagePOS.Server.Business.IManager;

namespace SagePOS.Server.Business.Manager
{
    public class PosSystemConfigrationManager : BusinessManagerBase<PosSystemConfigration,int>,IPosSystemConfigrationManager
    {
        public PosSystemConfigrationManager()
            : base((IRepository<PosSystemConfigration, int>)IoC.Instance.Resolve(typeof(IPosSystemConfigrationRepository)))
        {
        
        
        }
        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((IPOSSyncRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }
     
        public List<PosSystemConfigrationLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((IPosSystemConfigrationRepository)this.Repository).FindAllLite(myCriteria);
        }

     
    }
}
