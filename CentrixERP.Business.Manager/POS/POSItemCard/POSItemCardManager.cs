using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;
using SF.Framework;
using SagePOS.Server.Business.IManager;
using SagePOS.Server.DataAcces.IRepository;


namespace SagePOS.Server.Business.Manager
{
    public class POSItemCardManager : BusinessManagerBase<POSItemCard,int>,IPOSItemCardManager
    {
        public POSItemCardManager()
            : base((IRepository<POSItemCard, int>)IoC.Instance.Resolve(typeof(IPOSItemCardRepository)))
        {
        
        
        }

        public POSItemCardLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((IPOSItemCardRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<POSItemCardLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((IPOSItemCardRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((IPOSItemCardRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }

        public BOItemCardQl FindItemQl(CriteriaBase myCriteria)
        {
            return ((IPOSItemCardRepository)this.Repository).FindItemQl(myCriteria);
        }
        public List<BOItemCardQl> SearchItemQl(CriteriaBase myCriteria)
        {
            return ((IPOSItemCardRepository)this.Repository).SearchItemQl(myCriteria);
        }

        public BOItemCardQl SearchItemSerial(CriteriaBase myCriteria,bool status)
        {
            return ((IPOSItemCardRepository)this.Repository).SearchItemSerial(myCriteria, status);
        }

        public string SaveIntegration(List<POSItemCard> itemCardList)
        {
            return ((IPOSItemCardRepository)this.Repository).SaveIntegration(itemCardList);
        }

        public List<POSItemCard> FindAll()
        {
            return ((IPOSItemCardRepository)this.Repository).FindAll();
        }

    }
}
