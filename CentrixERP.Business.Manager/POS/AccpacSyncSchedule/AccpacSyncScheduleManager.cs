using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SF.Framework;


using SagePOS.Server.Business.Entity;
using SagePOS.Server.Business.Factory;
using SagePOS.Server.DataAcces.IRepository;
using SF.FrameworkEntity;
using SagePOS.Server.Business.IManager;

namespace SagePOS.Server.Business.Manager
{
    public class AccpacSyncScheduleManager : BusinessManagerBase<AccpacSyncSchedule,int>,IAccpacSyncScheduleManager
    {
        public AccpacSyncScheduleManager()
            : base((IRepository<AccpacSyncSchedule, int>)IoC.Instance.Resolve(typeof(IAccpacSyncScheduleRepository)))
        {
        
        
        }

        public AccpacSyncScheduleLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((IAccpacSyncScheduleRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<AccpacSyncScheduleLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((IAccpacSyncScheduleRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((IAccpacSyncScheduleRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }

        public List<AccpacSyncLogLite> FindAllLiteLog(string datefrom, string dateto, string AccpacSyncUniqueNumber)
        
        {
            return ((IAccpacSyncScheduleRepository)this.Repository).FindAllLiteLog(datefrom, dateto, AccpacSyncUniqueNumber);
        }

    }
}
