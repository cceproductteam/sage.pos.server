using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SagePOS.Server.Business.Entity;
using SagePOS.Server.Business.IManager;
using SagePOS.Server.DataAcces.IRepository;


namespace SagePOS.Server.Business.Manager
{
    public class BoLocationManager : BusinessManagerBase<BoLocation,int>,IBoLocationManager
    {
        public BoLocationManager()
            : base((IRepository<BoLocation, int>)IoC.Instance.Resolve(typeof(IBoLocationRepository)))
        {
        
        
        }

        public BoLocationLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((IBoLocationRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<BoLocationLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((IBoLocationRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((IBoLocationRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }
    }
}
