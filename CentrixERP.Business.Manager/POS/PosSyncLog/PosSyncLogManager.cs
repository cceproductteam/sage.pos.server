using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;
using SF.Framework;
using SagePOS.Server.Business.IManager;
using SagePOS.Server.DataAcces.IRepository;

namespace SagePOS.Server.Business.Manager
{
    public class PosSyncLogManager : BusinessManagerBase<PosSyncLog,int>,IPosSyncLogManager
    {
        public PosSyncLogManager()
            : base((IRepository<PosSyncLog, int>)IoC.Instance.Resolve(typeof(IPosSyncLogRepository)))
        {
        
        
        }

        public PosSyncLogLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((IPosSyncLogRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<PosSyncLogLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((IPosSyncLogRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((IPosSyncLogRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }
    }
}
