using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;
using SF.Framework;
using SagePOS.Server.Business.IManager;
using SagePOS.Server.DataAcces.IRepository;

namespace SagePOS.Server.Business.Manager
{
    public class POSPriceListManager : BusinessManagerBase<POSPriceList,int>,IPOSPriceListManager
    {
        public POSPriceListManager()
            : base((IRepository<POSPriceList, int>)IoC.Instance.Resolve(typeof(IPOSPriceListRepository)))
        {
        
        
        }

        public POSPriceListLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((IPOSPriceListRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<POSPriceListLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((IPOSPriceListRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((IPOSPriceListRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }

        public string SaveIntegration(List<POSPriceList> IcPriceList)
        {
            return ((IPOSPriceListRepository)this.Repository).SaveIntegration(IcPriceList);
        }
    }
}
