using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SagePOS.Server.Business.Entity;
using SagePOS.Server.Business.IManager;
using SagePOS.Server.DataAcces.IRepository;

namespace SagePOS.Server.Business.Manager
{
    public class BoBankManager : BusinessManagerBase<BoBank, int>, IBoBankManager
    {
        public BoBankManager()
            : base((IRepository<BoBank, int>)IoC.Instance.Resolve(typeof(IBoBankRepository)))
        {
        
        
        }

        public BoBankLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((IBoBankRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<BoBankLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((IBoBankRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((IBoBankRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }
    }
}
