using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;
using SF.Framework;
using SagePOS.Server.Business.IManager;
using SagePOS.Server.DataAcces.IRepository;

namespace SagePOS.Server.Business.Manager
{
    public class StoreManager : BusinessManagerBase<Store,int>,IStoreManager
    {
        public StoreManager()
            : base((IRepository<Store, int>)IoC.Instance.Resolve(typeof(IStoreRepository)))
        {
        
        
        }

        public StoreLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((IStoreRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<StoreLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((IStoreRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((IStoreRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }
    }
}
