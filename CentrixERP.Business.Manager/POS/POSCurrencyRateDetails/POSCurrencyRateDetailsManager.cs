using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;
using SF.Framework;
using SagePOS.Server.Business.IManager;
using SagePOS.Server.DataAcces.IRepository;

namespace SagePOS.Server.Business.Manager
{
    public class POSCurrencyRateDetailsManager : BusinessManagerBase<POSCurrencyRateDetails,int>,IPOSCurrencyRateDetailsManager
    {
        public POSCurrencyRateDetailsManager()
            : base((IRepository<POSCurrencyRateDetails, int>)IoC.Instance.Resolve(typeof(IPOSCurrencyRateDetailsRepository)))
        {
        
        
        }

        public POSCurrencyRateDetailsLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((IPOSCurrencyRateDetailsRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<POSCurrencyRateDetailsLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((IPOSCurrencyRateDetailsRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((IPOSCurrencyRateDetailsRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }

        public string SaveIntegration(List<POSCurrencyRateDetails> currenyRateList)
        {
            return ((IPOSCurrencyRateDetailsRepository)this.Repository).SaveIntegration(currenyRateList);
        }
    }
}
