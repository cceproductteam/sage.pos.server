using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;
using SF.Framework;
using SagePOS.Server.Business.IManager;
//using Centrix.POS.Standalone.Client.DataAccess.IRepository;
using SagePOS.Server.DataAcces.IRepository;

namespace SagePOS.Server.Business.Manager
{
    public class InvoiceItemManager : BusinessManagerBase<InvoiceItem,int>,IInvoiceItemManager
    {
        public InvoiceItemManager()
            : base((IRepository<InvoiceItem, int>)IoC.Instance.Resolve(typeof(IInvoiceItemRepository)))
        {
        
        
        }

        public InvoiceItemLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((IInvoiceItemRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<InvoiceItemLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((IInvoiceItemRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((IInvoiceItemRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }


        public List<InvoiceItemIntegration> InvoiceItemIntegrationFindAll(CriteriaBase myCriteria)
        {
            return ((IInvoiceItemRepository)this.Repository).InvoiceItemIntegrationFindAll(myCriteria);
        }
    }
}
