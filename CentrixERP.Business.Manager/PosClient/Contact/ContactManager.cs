using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;
using SF.Framework;
using SagePOS.Server.Business.IManager;
using SagePOS.Server.DataAcces.IRepository;


namespace SagePOS.Server.Business.Manager
{
    public class ContactManager : BusinessManagerBase<Contact,int>,IContactManager
    {
        public ContactManager()
            : base((IRepository<Contact, int>)IoC.Instance.Resolve(typeof(IContactRepository)))
        {
        
        
        }

        public ContactLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((IContactRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<ContactLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((IContactRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((IContactRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }
    }
}
