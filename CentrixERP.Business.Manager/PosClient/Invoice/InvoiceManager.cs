using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;
using SF.Framework;
using SagePOS.Server.Business.IManager;
//using Centrix.POS.Standalone.Client.DataAccess.IRepository;
using SagePOS.Server.DataAcces.IRepository;
namespace SagePOS.Server.Business.Manager
{
    public class InvoiceManager : BusinessManagerBase<Invoice, int>, IInvoiceManager
    {

        public InvoiceManager()
            : base((IRepository<Invoice, int>)IoC.Instance.Resolve(typeof(IInvoiceRepository)))
        {


        }


        public InvoiceLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((IInvoiceRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<InvoiceLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((IInvoiceRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((IInvoiceRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }


        public Invoice FindLastInvoice(CriteriaBase myCriteria)
        {
            Invoice myInvoice = ((IInvoiceRepository)this.Repository).FindLastInvoice(myCriteria);

            return myInvoice;
        }


        public Invoice FindInvoiceByNumber(CriteriaBase myCriteria)
        {
            Invoice myInvoice = ((IInvoiceRepository)this.Repository).FindInvoiceByNumber(myCriteria);

            return myInvoice;
        }


        public List<Invoice> FindAllQl(CriteriaBase myCriteria, string phoneNumber)
        {
            return ((IInvoiceRepository)this.Repository).FindAllQl(myCriteria, phoneNumber);
        }
        public List<Invoice> FindByCustomer(CriteriaBase myCriteria)
        {
            return ((IInvoiceRepository)this.Repository).FindByCustomer(myCriteria);
        }
        public List<InvoiceIntegration> InvoiceIntegrationFindAll(CriteriaBase myCriteria)
        {
            return ((IInvoiceRepository)this.Repository).InvoiceIntegrationFindAll(myCriteria);
        }

        public void UpdateTransactionStatus(int transactionType, string transactionRef, string transactionNumber, string status, bool IsConvertedToShipment)
        {
            ((IInvoiceRepository)this.Repository).UpdateTransactionStatus(transactionType, transactionRef, transactionNumber, status, IsConvertedToShipment);
        }

        public void UpdateAccpacInvoiceNumber(string InvoiceUniqueNumber, string AccpacInvoiceNum, string AccpacOrderentryNum, string AccpacShipmentNum, string AccpacPrePaymnentNumber, string accpacRefundNumber)
        {
            ((IInvoiceRepository)this.Repository).UpdateAccpacInvoiceNumber(InvoiceUniqueNumber, AccpacInvoiceNum, AccpacOrderentryNum, AccpacShipmentNum, AccpacPrePaymnentNumber, accpacRefundNumber);
        }
        public void UpdateSyncStatus(string trasactionNumber, int EntityId)
        {
            ((IInvoiceRepository)this.Repository).UpdateSyncStatus(trasactionNumber, EntityId);
        }

        public List<InvoiceLite> searchRefund(CriteriaBase myCriteria)
        {
            return ((IInvoiceRepository)this.Repository).searchRefund(myCriteria);
        }
        public void UpdateInvoiceTransNumber(int invoiceId, int shipmentId, string shipmentNumber)
        {
            ((IInvoiceRepository)this.Repository).UpdateInvoiceTransNumber(invoiceId, shipmentId, shipmentNumber);
        }

        public List<InvoiceInquiryLite> InvoiceInquiryResults(string StoreIds, DateTime? InvoiceDateFrom, DateTime? InvoiceDateTo)
        {
            return ((IInvoiceRepository)this.Repository).InvoiceInquiryResults(StoreIds, InvoiceDateFrom, InvoiceDateTo);
        }

        public List<POSQuantityInQuiryLite> FindInquiryResults(int LocationId, int StoreId, int ItemId)
        {
            return ((IInvoiceRepository)this.Repository).FindInquiryResults(LocationId, StoreId, ItemId);
        }

        public bool CheckParentShipmentValidity()
        {
            return ((IInvoiceRepository)this.Repository).CheckParentShipmentValidity();
        }

        public List<ICQuantityInQuiryLite> FindICInquiryResults(int LocationId, int StoreId, int ItemId)
        {
            return ((IInvoiceRepository)this.Repository).FindICInquiryResults(LocationId, StoreId, ItemId);
        }


        public List<POSDailyRecieptsReport> FindPOSDailySalesReport(string fromdate, string todate, int storeId)
        {
            return ((IInvoiceRepository)this.Repository).FindPOSDailySalesReport(fromdate, todate, storeId);
        }

        public List<POSDailySalesReport> FindPOSDailySalesPerItemReport(int StoreId, string itemNumber, string InvoiceNumberFrom, string InvoiceNumberTo, string datefrom, string dateTo)
        {
            return ((IInvoiceRepository)this.Repository).FindPOSDailySalesPerItemReport(StoreId, itemNumber, InvoiceNumberFrom, InvoiceNumberTo, datefrom, dateTo);
        }

        public List<POSDailySalesSummary> FindPOSDailySalesSummaryReport(int StoreId, string itemNumber, string InvoiceNumberFrom, string InvoiceNumberTo, string datefrom, string dateTo)
        {
            return ((IInvoiceRepository)this.Repository).FindPOSDailySalesSummaryReport(StoreId, itemNumber, InvoiceNumberFrom, InvoiceNumberTo, datefrom, dateTo);
        }

        public List<POSDailySalesReport> InvoiceInquirSalesReport(string fromdate, string todate, string AccpacSyncUniqueNumber, int Posted, string InvoiceNumber)
        {
            return ((IInvoiceRepository)this.Repository).InvoiceInquirSalesReport(fromdate, todate, AccpacSyncUniqueNumber, Posted, InvoiceNumber);
        }


        public List<POSDailyRecieptsReport> FindPOSDailySalesReportSummation(string fromdate, string todate, int storeId)
        {
            return ((IInvoiceRepository)this.Repository).FindPOSDailySalesReportSummation(fromdate, todate, storeId);
        }


        public List<POSDaileSalesSummation> FindPOSDaileSalessummation(string fromdate, string todate, int storeId)
        {
            return ((IInvoiceRepository)this.Repository).FindPOSDaileSalessummation(fromdate, todate, storeId);
        }

        public DashBoarBoxes FindDashBoardBoxed()
        {
            return ((IInvoiceRepository)this.Repository).FindDashBoardBoxed();
        }

        public List<RegistersSales> FindRegistersSales(int storeId)
        {
            return ((IInvoiceRepository)this.Repository).FindRegistersSales(storeId);
        }
        public List<StoreSales> FindStoresSales()
        {
            return ((IInvoiceRepository)this.Repository).FindStoresSales();
        }


        public List<POSPettyCash> FindPOSPettyCash(string fromdate, string todate, int storeId ,int registerId)
        {
            return ((IInvoiceRepository)this.Repository).FindPOSPettyCash(fromdate, todate, storeId , registerId);
        
        }
    }
}
