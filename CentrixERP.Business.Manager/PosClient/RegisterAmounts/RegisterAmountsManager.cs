using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;
using SF.Framework;
using SagePOS.Server.Business.IManager;
using SagePOS.Server.DataAcces.IRepository;
namespace SagePOS.Server.Business.Manager
{
    public class RegisterAmountsManager : BusinessManagerBase<RegisterAmounts,int>,IRegisterAmountsManager
    {
        public RegisterAmountsManager()
            : base((IRepository<RegisterAmounts, int>)IoC.Instance.Resolve(typeof(IRegisterAmountsRepository)))
        {
        
        
        }

        public RegisterAmountsLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((IRegisterAmountsRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<RegisterAmountsLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((IRegisterAmountsRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((IRegisterAmountsRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }
    }
}
