using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;

using SF.Framework;

using SagePOS.Server.DataAcces.IRepository;
using SagePOS.Server.Business.Entity;
using SagePOS.Server.Business.IManager;
namespace SagePOS.Server.Business.Manager
{
    public class TransactionTypeManager : BusinessManagerBase<TransactionType,int>,ITransactionTypeManager
    {
        public TransactionTypeManager()
            : base((IRepository<TransactionType, int>)IoC.Instance.Resolve(typeof(ITransactionTypeRepository)))
        {
        
        
        }

        public TransactionTypeLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((ITransactionTypeRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<TransactionTypeLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((ITransactionTypeRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((ITransactionTypeRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }
    }
}
