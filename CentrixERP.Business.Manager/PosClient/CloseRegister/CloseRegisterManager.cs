using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;
using SF.Framework;
using SagePOS.Server.Business.IManager;
//using Centrix.POS.Standalone.Client.DataAccess.IRepository;
using SagePOS.Server.DataAcces.IRepository;
namespace SagePOS.Server.Business.Manager
{
    public class CloseRegisterManager : BusinessManagerBase<CloseRegister, int>, ICloseRegisterManager
    {
        public CloseRegisterManager()
            : base((IRepository<CloseRegister, int>)IoC.Instance.Resolve(typeof(ICloseRegisterRepository)))
        {


        }

        public CloseRegisterLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((ICloseRegisterRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<CloseRegisterLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((ICloseRegisterRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((ICloseRegisterRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }

        public List<CloseRegiserDetails> GetCloseRegisterDetailsPaymentType(CriteriaBase myCriteria)
        {
            return ((ICloseRegisterRepository)this.Repository).GetCloseRegisterDetailsPaymentType(myCriteria);
        }
        public void UpdateSyncStatus(string trasactionNumber, int EntityId)
        {
            ((ICloseRegisterRepository)this.Repository).UpdateSyncStatus(trasactionNumber, EntityId);
        }

        public List<CloseRegisterInquiryLite> CloseRegisterInquiryResults(string StoreIds, string RegisterIds, DateTime? OpenedDateFrom, DateTime? OpenedDateTo, DateTime? ClosedDateFrom, DateTime? ClosedDateTo)
        {
            return ((ICloseRegisterRepository)this.Repository).CloseRegisterInquiryResults(StoreIds, RegisterIds, OpenedDateFrom, OpenedDateTo, ClosedDateFrom, ClosedDateTo);
        }

        public CloseRegisterTotals FindCloseRegisterSalesTotal(int CloseRegisterId)
        {
            return ((ICloseRegisterRepository)this.Repository).FindCloseRegisterSalesTotal(CloseRegisterId);
        }


        public List<CloseRegisterInquiryLiteNew> CloseRegisterInquiryResultsNew(string StoreId, string RegisterId, string fromdate, string todate)
        {
            return ((ICloseRegisterRepository)this.Repository).CloseRegisterInquiryResultsNew(StoreId, RegisterId, fromdate, todate);
        }
    
    }
}

