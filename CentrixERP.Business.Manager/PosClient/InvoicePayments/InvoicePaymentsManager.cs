using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;
using SF.Framework;
using SagePOS.Server.Business.IManager;

using SagePOS.Server.DataAcces.IRepository;
namespace SagePOS.Server.Business.Manager
{
    public class InvoicePaymentsManager : BusinessManagerBase<InvoicePayments,int>,IInvoicePaymentsManager
    {
        public InvoicePaymentsManager()
            : base((IRepository<InvoicePayments, int>)IoC.Instance.Resolve(typeof(IInvoicePaymentsRepository)))
        {
        
        
        }

        public InvoicePaymentsLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((IInvoicePaymentsRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<InvoicePaymentsLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((IInvoicePaymentsRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((IInvoicePaymentsRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }

        public int GetParentInvoiceId(int refundInvoiceId)
        {
            return ((IInvoicePaymentsRepository)this.Repository).GetParentInvoiceId(refundInvoiceId);
        }

        public List<InvoicePaymentIntegration> InvoicePaymentIntgration(string InvoiceUnqiueNumber)
        {
            return ((IInvoicePaymentsRepository)this.Repository).InvoicePaymentIntgration(InvoiceUnqiueNumber);
        }
    }
}
