using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.Framework;
using SF.FrameworkEntity;
using Centrix.UM.Business.Entity;
using Centrix.UM.Business.IManager;
using Centrix.UM.DataAccess.IRepository;

namespace Centrix.UM.Business.Manager
{
    public class AreaManager :BusinessManagerBase<Area,int>, IAreaManager
    {
        public AreaManager()
            : base((IRepository<Area, int>)IoC.Instance.Resolve(typeof(IAreaRepository))) { }
    }
}
