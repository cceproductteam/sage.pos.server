using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.Framework;
using SF.FrameworkEntity;
using Centrix.UM.Business.Entity;
using Centrix.UM.Business.IManager;
using Centrix.UM.DataAccess.IRepository;

namespace Centrix.UM.Business.Manager
{
    
    public class EmailUserManager: BusinessManagerBase< EmailUser, int>, IEmailUserManager
    {
        #region "Constructors"
      
        public EmailUserManager()
        : base((IRepository< EmailUser, int>)IoC.Instance.Resolve(typeof(IEmailUserRepository))) { }
        
        #endregion
      
    }
}


