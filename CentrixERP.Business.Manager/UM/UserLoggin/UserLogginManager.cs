using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using Centrix.UM.Business.Entity;
using SF.Framework;
using Centrix.UM.Business.IManager;
using Centrix.UM.DataAccess.IRepository;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using System.Configuration;
using SagePOS.Server.Configuration;
using C = SagePOS.Server.Configuration;

namespace Centrix.UM.Business.Manager
{
    public class UserLogginManager : BusinessManagerBase<UserLoggin, int>, IUserLogginManager
    {
        IUserManager iUserManager = null;
        UserLoggin userLoggin = null;
        JavaScriptSerializer javaScriptSerializer = null;
        int AllowedSeesionTimeOut = 20;
        public int IsClient = ConfigurationManager.AppSettings["isClient"].ToNumber();

        User currentLoggedUser = null;
        public User CurrentLoggedUser
        {
            get
            {
                return this.currentLoggedUser;
            }
        }

        public bool ForceCreateNewSession { get; set; }

        public UserLogginManager()
            : base((IRepository<UserLoggin, int>)IoC.Instance.Resolve(typeof(IUserLogginRepository)))
        {
            this.iUserManager = IoC.Instance.Resolve<IUserManager>();
            ForceCreateNewSession = true;
            this.javaScriptSerializer = new JavaScriptSerializer();
            AllowedSeesionTimeOut = C.Configuration.GetConfigKeyValue("AllowedSeesionTimeOut").ToNumber();
            if (AllowedSeesionTimeOut == 0)
                AllowedSeesionTimeOut = 20;
        }

        public Enums_S3.UserLoggin.UserLogginStatus ValidateLogginSession(UserLogginCriteria userLogginCriteria)
        {
            string session = GetUserSession();
            if (string.IsNullOrEmpty(session))
                return Enums_S3.UserLoggin.UserLogginStatus.NotLoggedIn;

            try
            {
                string plainText = SF.Framework.Cryption.Decrypt.String(session, Cryption.Algorithm.AES);

                return compareUserSession(plainText, userLogginCriteria);
            }
            catch (Exception ex)
            {
                //throw;
                //SF.Framework.Cookies.Abandon("cxuser");
                //this.KillUserSession(userLogginCriteria);
                return Enums_S3.UserLoggin.UserLogginStatus.NotLoggedIn;
            }
        }

        private string GetUserSession()
        {
            return SF.Framework.Cookies.Get("cxuser");
        }

        private Enums_S3.UserLoggin.UserLogginStatus compareUserSession(string plainText, UserLogginCriteria userLogginCriteria)
        {
            UserLogginCriteria currentCriteria = null;
            try
            {
                currentCriteria = this.javaScriptSerializer.Deserialize<UserLogginCriteria>(plainText);
            }
            catch (Exception ex)
            {
                //throw new Exception("decryption failed");
                return Enums_S3.UserLoggin.UserLogginStatus.NotLoggedIn;
                //throw ex;
            }

            if (currentCriteria == null)
                return Enums_S3.UserLoggin.UserLogginStatus.NotLoggedIn;

            List<UserLoggin> logginList = this.FindAll(currentCriteria);
            if (logginList == null)
                return Enums_S3.UserLoggin.UserLogginStatus.NotLoggedIn;

            this.userLoggin = logginList.First();

            double minutes = ((TimeSpan)(DateTime.Now - userLoggin.LastRequest)).TotalMinutes;

            if (minutes > AllowedSeesionTimeOut && IsClient!=1)
            {
                //userLoggin.MarkDeleted();
                //try
                //{
                //    this.Save(userLoggin);
                //}
                //catch (Exception)
                //{
                //}
                return Enums_S3.UserLoggin.UserLogginStatus.SessionExpired;
            }




            //if (this.userLoggin.PublicIp != userLogginCriteria.PublicIP)
            //{
            //    return Enums_S3.UserLoggin.UserLogginStatus.LoggedFromAnotherPlace;
            //}
            //if (this.userLoggin.LocalIp != userLogginCriteria.LocalIP)
            //{
            //    return Enums_S3.UserLoggin.UserLogginStatus.LoggedFromAnotherPlace;
            //}
            //if (this.userLoggin.UserAgent != userLogginCriteria.UserAgent)
            //    return Enums_S3.UserLoggin.UserLogginStatus.LoggedFromAnotherPlace;

            //if (this.userLoggin.RandomString != currentCriteria.RandomString)
            //    return Enums_S3.UserLoggin.UserLogginStatus.LoggedFromAnotherPlace;

            if (this.userLoggin.UserId <= 0)
                return Enums_S3.UserLoggin.UserLogginStatus.NotLoggedIn;

            this.currentLoggedUser = this.iUserManager.FindById(this.userLoggin.UserId, null);
            if (this.currentLoggedUser == null)
                return Enums_S3.UserLoggin.UserLogginStatus.NotLoggedIn;


            userLogginCriteria.RandomString = currentCriteria.RandomString;
            //if (ForceCreateNewSession)
            CreateUserSession(this.currentLoggedUser.UserId, userLogginCriteria, ForceCreateNewSession);

            return Enums_S3.UserLoggin.UserLogginStatus.LoggedIn;
        }


        public void KillUserSession(UserLogginCriteria userLogginCriteria)
        {
            SF.Framework.Cookies.Abandon("cxuser");
            try
            {
                ((IUserLogginRepository)this.Repository).KillUserLogging(userLogginCriteria);
            }
            catch (SF.Framework.Exceptions.RecordNotAffected ex)
            {
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CreateUserSession(int userId, UserLogginCriteria userLogginCriteria, bool createNewSession)
        {
            string randomString = GenerateRandomString();
            //userLoggin.MarkNew();

            UserLoggin newLoggin = new UserLoggin()
            {
                UserLogginId = this.userLoggin != null ? this.userLoggin.UserLogginId : -1,
                UserId = userId,
                RandomString = createNewSession ? randomString : userLoggin.RandomString,
                LocalIp = userLogginCriteria.LocalIP,
                PublicIp = userLogginCriteria.PublicIP,
                UserAgent = userLogginCriteria.UserAgent,
                LastRequest = DateTime.Now
            };

            if (this.userLoggin != null)
            {
                newLoggin.MarkOld();
                newLoggin.MarkModified();
            }

            try
            {
                this.Save(newLoggin);

                if (createNewSession)
                {
                    var userSeesion = new UserLogginLite
                    {
                        UserId = userId,
                        RandomString = randomString,
                        LocalIp = userLogginCriteria.LocalIP,
                        PublicIp = userLogginCriteria.PublicIP,
                        UserAgent = userLogginCriteria.UserAgent
                    };

                    string session = this.javaScriptSerializer.Serialize(userSeesion);
                    //userId.ToString() + "/" + randomString;
                    string encryptdSession = SF.Framework.Cryption.Encrypt.String(session, Cryption.Algorithm.AES);
                    SF.Framework.Cookies.Abandon("cxuser");
                    HttpCookie cookie = new HttpCookie("cxuser", encryptdSession);
                    HttpContext.Current.Request.Cookies.Remove("cxuser");
                    HttpContext.Current.Request.Cookies.Add(cookie);
                    SF.Framework.Cookies.Add("cxuser", encryptdSession);
                }

            }
            catch (Exception)
            {

                //throw;
            }
        }

        private string GenerateRandomString()
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            var result = new string(
                Enumerable.Repeat(chars, 8)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());

            return result;
        }

        public void KillUserSession(int userId)
        {
            try
            {
                ((IUserLogginRepository)this.Repository).KillUserLogging(userId);
            }
            catch (Exception ex)
            {

            }
        }

        public Enums_S3.UserLoggin.UserLogginStatus CheckCurrentSession()
        {
            string session = GetUserSession();
            if (string.IsNullOrEmpty(session))
                return Enums_S3.UserLoggin.UserLogginStatus.NotLoggedIn;

            try
            {
                string plainText = SF.Framework.Cryption.Decrypt.String(session, Cryption.Algorithm.AES);
                UserLogginLite criteria = this.javaScriptSerializer.Deserialize<UserLogginLite>(plainText);


                List<UserLoggin> currentUserSessions = this.FindByParentId(criteria.UserId, typeof(User), null);

                if (currentUserSessions != null && currentUserSessions.Count > 0)
                {
                    foreach (var oldSession in currentUserSessions)
                    {
                        double Sminutes = ((TimeSpan)(DateTime.Now - oldSession.LastRequest)).TotalMinutes;
                        bool validSessionTime = Sminutes < AllowedSeesionTimeOut;

                        if (oldSession.UserAgent != criteria.UserAgent || oldSession.LocalIp != criteria.LocalIp || oldSession.PublicIp != criteria.PublicIp || oldSession.RandomString != criteria.RandomString)
                        {
                            if (!validSessionTime)
                            {
                                oldSession.MarkDeleted();
                                this.Save(oldSession);
                                return Enums_S3.UserLoggin.UserLogginStatus.NotLoggedIn;
                            }
                            else
                            {
                                //oldSession.MarkDeleted();
                                //this.Save(oldSession);
                                return Enums_S3.UserLoggin.UserLogginStatus.LoggedFromAnotherPlace;
                            }
                        }
                        else if (!validSessionTime)
                        {
                            oldSession.MarkDeleted();
                            this.Save(oldSession);
                            return Enums_S3.UserLoggin.UserLogginStatus.SessionExpired;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return Enums_S3.UserLoggin.UserLogginStatus.NotLoggedIn;
            }
            return Enums_S3.UserLoggin.UserLogginStatus.NotLoggedIn;
        }
    }
}