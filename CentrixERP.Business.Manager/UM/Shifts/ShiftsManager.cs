using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using Centrix.UM.Business.Entity;
using SF.Framework;
using Centrix.UM.Business.IManager;
using Centrix.UM.DataAccess.IRepository;

namespace Centrix.UM.Business.Manager
{
    public class ShiftsManager : BusinessManagerBase<Shifts,int>,IShiftsManager
    {
        public ShiftsManager()
            : base((IRepository<Shifts, int>)IoC.Instance.Resolve(typeof(IShiftsRepository)))
        {
        
        
        }

        public ShiftsLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((IShiftsRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<ShiftsLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((IShiftsRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((IShiftsRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }
    }
}
