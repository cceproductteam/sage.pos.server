using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.Framework;
using SF.FrameworkEntity;
using Centrix.UM.Business.Entity;
using Centrix.UM.Business.IManager;
using Centrix.UM.DataAccess.IRepository;

namespace Centrix.UM.Business.Manager
{
    public class TeamManager :BusinessManagerBase<Team,int>, ITeamManager
    {
        public TeamManager()
            : base((IRepository<Team, int>)IoC.Instance.Resolve(typeof(ITeamRepository))) { }

        public List<Team> FindAll_RecentlyViewed(int userId, int entityId, TeamCriteria myCriteria)
        { return ((ITeamRepository)this.Repository).FindAll_RecentlyViewed(userId, entityId, myCriteria); }

        public List<Team> FindAllTaskTeams(TeamCriteria myCriteria)
        {
            return ((ITeamRepository)this.Repository).FindAllTaskTeams(myCriteria);
        }
      
        public List<Team_Lite> FindAllLite(TeamCriteria myCriteria)
        {
            return ((ITeamRepository)this.Repository).FindAllLite(myCriteria);
        }

        public Team_Lite FindByIdLite(int Id)
        {
            return ((ITeamRepository)this.Repository).FindByIdLite(Id);
        }


        public SagePOS.Manger.Common.Business.Entity.Email FindTeamEmail(int id)
        {
            return ((ITeamRepository)this.Repository).FindTeamEmail(id);
        }
    }
}
