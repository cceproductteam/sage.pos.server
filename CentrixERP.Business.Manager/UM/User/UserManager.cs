using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.Framework;
using SF.FrameworkEntity;
using Centrix.UM.Business.Entity;
using Centrix.UM.Business.IManager;
using Centrix.UM.DataAccess.IRepository;
using SagePOS.Manger.Common.Business.Entity;
using System.Text.RegularExpressions;
using System.Configuration;
using C = SagePOS.Server.Configuration;



namespace Centrix.UM.Business.Manager
{
    public class UserManager : BusinessManagerBase<User, int>, IUserManager
    {
        public UserManager()
            : base((IRepository<User, int>)IoC.Instance.Resolve(typeof(IUserRepository))) { }

        #region IUserManager Members

        public User Login(string UserName, string Password)
        {
            Password = Cryption.Encrypt.String(Password, Cryption.Algorithm.AES);
            User myUser = ((IUserRepository)this.Repository).Login(UserName, Password);

            if (myUser != null)
            {
                myUser.MarkOld();
                myUser.Manager = this;
                //Cookies.Add("uid", myUser.UserId.ToString());
                if (myUser.MyRole != null)
                {
                    Role r = myUser.MyRole;
                    if (myUser.MyRole.MyRolePermission != null)
                    {
                        List<RolePermission> rolePer = myUser.MyRole.MyRolePermission;
                    }
                }
                System.Web.HttpContext.Current.Session.Add("LoggedInUser", myUser);
            }
            return myUser;
        }

        public void Logout()
        {
            //Cookies.Abandon("uid");
            if (System.Web.HttpContext.Current.Session != null && System.Web.HttpContext.Current.Session["LoggedInUser"] != null)
                System.Web.HttpContext.Current.Session.Remove("LoggedInUser");
        }

        public bool IsLoggedIn()
        {
            int uid = Cookies.Get("uid").ToNumber();
            return Convert.ToBoolean(uid);
        }

        public User FindLoggedInUser(int UserId)
        {
            User myUser = null;
            if (System.Web.HttpContext.Current.Session == null || System.Web.HttpContext.Current.Session["LoggedInUser"] == null)
            {
                myUser = this.FindById(UserId, null);
                if (myUser != null)
                {
                    Cookies.Add("uid", myUser.UserId.ToString());
                    if (myUser.MyRole != null)
                    {
                        Role r = myUser.MyRole;
                        if (myUser.MyRole.MyRolePermission != null)
                        { List<RolePermission> rolePer = myUser.MyRole.MyRolePermission; }
                    }
                    System.Web.HttpContext.Current.Session.Add("LoggedInUser", myUser);
                }
            }
            else
            {
                myUser = (User)System.Web.HttpContext.Current.Session["LoggedInUser"];
            }
            return myUser;
        }

        public bool HasPermission(SagePOS.Server.Configuration.Enums_S3.Permissions.SystemModules permission, User myUser)
        {
            bool permitted = true;
            if (!permission.Equals(SagePOS.Server.Configuration.Enums_S3.Permissions.SystemModules.None) && !myUser.IsAdmin)
            {
                permitted = false;
                if (myUser.MyRole != null && myUser.MyRole.MyRolePermission != null)
                {
                    IEnumerable<RolePermission> per = from RolePermission rolePer in myUser.MyRole.MyRolePermission where rolePer.Permissionid == (int)permission select rolePer;
                    if (per != null && per.Count() != 0)
                        permitted = true;
                }

            }
            return permitted;
        }

        #endregion

        public List<User> FindAll_RecentlyViewed(int userId, int entityId, UserCriteria myCriteria)
        { return ((IUserRepository)this.Repository).FindAll_RecentlyViewed(userId, entityId, myCriteria); }
        public List<User> FindAllTaskUsers(UserCriteria myCriteria)
        {
            return ((IUserRepository)this.Repository).FindAllTaskUsers(myCriteria);
        }

        
        public UserLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((IUserRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<UserLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((IUserRepository)this.Repository).FindAllLite(myCriteria);
        }
        public UserLite FindUserManager(int Id, CriteriaBase myCriteria)
        {
            return ((IUserRepository)this.Repository).FindUserManager(Id, myCriteria);
        }


        public Dictionary<string, string> ValidateUser(User user)
        {
            Dictionary<string, string> validationUser = new Dictionary<string, string>();

            if (user != null)
            {
                if (vldUserNamePassword(user.UserName))
                    validationUser.Add("UserName", "user name can\'t contain spaces or special charachter");
                //if (vldUserNamePassword(user.Password))
                //    validationUser.Add("UserPassword", "password can\'t contain spaces or special charachter");
            }
            return validationUser;
        }

        private bool vldUserNamePassword(string str)
        {
            //ConfigurationManager.AppSettings["SpecialChatachterRegExpression"]  "[^a-z0-9]"
            Match match = Regex.Match(str,C.Configuration.GetConfigKeyValue("SpecialChatachterRegExpression"), RegexOptions.IgnoreCase);
            return match.Success;
        }

        public List<object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((IUserRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }

        public bool CheckUserPassword(int userId, string password)
        {
            User user = this.FindById(userId, null);
            return password.Equals(user.Password);
        }

        public List<RoleUsersLite> FindRoleUsersLite(int RoleId)
        {
            return ((IUserRepository)this.Repository).FindRoleUsersLite(RoleId);
        }
        public void AddUserRoles(int roleId, string RoleUserIds)
        {
            ((IUserRepository)this.Repository).AddUserRoles(roleId, RoleUserIds);
        }
        public void AddUserTeams(int teamId, string TeamUserIds)
        {
            ((IUserRepository)this.Repository).AddUserTeams(teamId,TeamUserIds);
        }
        public List<TeamUsersLite> FindTeamUsersLite(int TeamId)
        {
            return ((IUserRepository)this.Repository).FindTeamUsersLite(TeamId);
        }

        public bool CheckUserCount()
        {
            return ((IUserRepository)this.Repository).CheckUserCount();
        }

        public User ClientLogin(string UserName, string Password, string AccessCode)
        {
            AccessCode = (!string.IsNullOrEmpty(AccessCode)) ? Cryption.Encrypt.String(AccessCode, Cryption.Algorithm.AES) : null;
            Password = Cryption.Encrypt.String(Password, Cryption.Algorithm.AES);
            User myUser = ((IUserRepository)this.Repository).ClientLogin(UserName, Password, AccessCode);
            if (myUser != null)
            {
                myUser.MarkOld();
                myUser.Manager = this;
                //Cookies.Add("uid", myUser.UserId.ToString());


            }
            return myUser;
        }

        public bool HasPermission(int permission, User myUser)
        {
            bool permitted = true;
            if (!permission.Equals(SagePOS.Server.Configuration.Enums_S3.Permissions.SystemModules.None) && !myUser.IsAdmin)
            {
                permitted = false;
                if (myUser.MyRole != null && myUser.MyRole.MyRolePermission != null)
                {
                    IEnumerable<RolePermission> per = from RolePermission rolePer in myUser.MyRole.MyRolePermission where rolePer.Permissionid == permission select rolePer;
                    if (per != null && per.Count() != 0)
                        permitted = true;
                }

            }
            return permitted;
        }
    }
}