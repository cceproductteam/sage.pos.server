using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.Framework;
using SF.FrameworkEntity;
using Centrix.UM.Business.Entity;
using Centrix.UM.Business.IManager;
using Centrix.UM.DataAccess.IRepository;




namespace Centrix.UM.Business.Manager
{
    public class RoleManager :BusinessManagerBase<Role,int>, IRoleManager
    {
        public RoleManager()
            : base((IRepository<Role, int>)IoC.Instance.Resolve(typeof(IRoleRepository))) { }
        public List<Role> FindAll_RecentlyViewed(int userId, int entityId, RoleCriteria myCriteria)
        { return ((IRoleRepository)this.Repository).FindAll_RecentlyViewed(userId, entityId, myCriteria); }

        public List<Role_Lite> FindAllLite(RoleCriteria myCriteria)
        {
            return ((IRoleRepository)this.Repository).FindAllLite(myCriteria);
        }

        public Role_Lite FindByIdLite(int Id)
        {
            return ((IRoleRepository)this.Repository).FindByIdLite(Id);
        }
    }
}
