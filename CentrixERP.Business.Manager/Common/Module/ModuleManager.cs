using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Manger.Common.Business.Entity;
using SF.Framework;
using CentrixERP.Common.Business.IManager;
using SagePOS.Server.DataAcces.IRepository;
using SagePOS.Common.DataAccess.IRepository;

namespace CentrixERP.Common.Business.Manager
{
    public class ModuleManager : BusinessManagerBase<Module,int>,IModuleManager
    {
        public ModuleManager()
            : base((IRepository<Module, int>)IoC.Instance.Resolve(typeof(IModuleRepository)))
        { }
    }
}
