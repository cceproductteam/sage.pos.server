using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.Framework;
using SF.FrameworkEntity;
using SagePOS.Server.DataAcces.IRepository;
using SagePOS.Server.DataAcces.IRepository;
using SagePOS.Manger.Common.Business.Entity;
using CentrixERP.Common.Business.IManager;
using System.Xml.Linq;
using System.Configuration;
using System.IO;
using SagePOS.Common.DataAccess.IRepository;

namespace CentrixERP.Common.Business.Manager
{
    public class PhoneManager : BusinessManagerBase<Phone, int>, IPhoneManager
    {
        public PhoneManager()
            : base((IRepository<Phone, int>)IoC.Instance.Resolve(typeof(IPhoneRepository))) { }

        public List<Phone> FindMobileList(string ids, bool forPerson)
        {
            return ((IPhoneRepository)this.Repository).FindMobileList(ids, forPerson);
        }    
    }
}
