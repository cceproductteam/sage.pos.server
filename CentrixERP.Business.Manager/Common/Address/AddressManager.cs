using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.Framework;
using SF.FrameworkEntity;
using SagePOS.Manger.Common.Business.Entity;
using CentrixERP.Common.Business.IManager;
using SagePOS.Server.DataAcces.IRepository;
using SagePOS.Server.Configuration;
using SagePOS.Common.DataAccess.IRepository;


namespace CentrixERP.Common.Business.Manager
{
    public class AddressManager : BusinessManagerBase<Address, int>, IAddressManager
    {
        public AddressManager()
            : base((IRepository<Address, int>)IoC.Instance.Resolve(typeof(IAddressRepository))) { }

        #region IAddressManager Members

        public string AddressName(Address address, string addressFormat, Enums_S3.Configuration.Language lang)
        {
            string addressName = addressFormat;
            if (lang == Enums_S3.Configuration.Language.ar)
            {
                if (addressFormat.IndexOf("[" + Enums_S3.AddressFormatKeyword.Area + "]") > -1)
                {
                    if (address.Area != null)
                        addressName = addressName.Replace("[" + Enums_S3.AddressFormatKeyword.Area + "]", address.Area.NameArabic);
                    else
                        addressName = addressName.Replace("[" + Enums_S3.AddressFormatKeyword.Area + "]", string.Empty);
                }
                if (addressFormat.IndexOf("[" + Enums_S3.AddressFormatKeyword.City + "]") > -1)
                {
                    if (address.City != null)
                        addressName = addressName.Replace("[" + Enums_S3.AddressFormatKeyword.City + "]", address.City.CityNameAr);
                    else
                        addressName = addressName.Replace("[" + Enums_S3.AddressFormatKeyword.City + "]", string.Empty);
                }
                if (addressFormat.IndexOf("[" + Enums_S3.AddressFormatKeyword.Country + "]") > -1)
                {
                    if (address.Country != null)
                        addressName = addressName.Replace("[" + Enums_S3.AddressFormatKeyword.Country + "]", address.Country.CountryNameAr);
                    else
                        addressName = addressName.Replace("[" + Enums_S3.AddressFormatKeyword.Country + "]", string.Empty);
                }
            }
            else
            {
                
                if (addressFormat.IndexOf("[" + Enums_S3.AddressFormatKeyword.Area + "]") > -1)
                {
                    if (address.Area != null)
                        addressName = addressName.Replace("[" + Enums_S3.AddressFormatKeyword.Area + "]", address.Area.NameEnglish);
                    else
                        addressName = addressName.Replace("[" + Enums_S3.AddressFormatKeyword.Area + "]", string.Empty);
                }
                if (addressFormat.IndexOf("[" + Enums_S3.AddressFormatKeyword.City + "]") > -1)
                {
                    if (address.City != null)
                        addressName = addressName.Replace("[" + Enums_S3.AddressFormatKeyword.City + "]", address.City.CityNameEn);
                    else
                        addressName = addressName.Replace("[" + Enums_S3.AddressFormatKeyword.City + "]", string.Empty);
                }
                if (addressFormat.IndexOf("[" + Enums_S3.AddressFormatKeyword.Country + "]") > -1)
                {
                    if (address.Country != null)
                        addressName = addressName.Replace("[" + Enums_S3.AddressFormatKeyword.Country + "]", address.Country.CountryNameEn);
                    else
                        addressName = addressName.Replace("[" + Enums_S3.AddressFormatKeyword.Country + "]", string.Empty);
                }
            }
            if (addressFormat.IndexOf("[" + Enums_S3.AddressFormatKeyword.ZipCode + "]") > -1)
            {
                addressName = addressName.Replace("[" + Enums_S3.AddressFormatKeyword.ZipCode + "]", address.ZipCode);
            }
            if (addressFormat.IndexOf("[" + Enums_S3.AddressFormatKeyword.StreetAddress + "]") > -1)
            {
                addressName = addressName.Replace("[" + Enums_S3.AddressFormatKeyword.StreetAddress + "]", address.StreetAddress);
            }
            if (addressFormat.IndexOf("[" + Enums_S3.AddressFormatKeyword.Region + "]") > -1)
            {
                addressName = addressName.Replace("[" + Enums_S3.AddressFormatKeyword.Region + "]", address.Region);
            }
            if (addressFormat.IndexOf("[" + Enums_S3.AddressFormatKeyword.POBox + "]") > -1)
            {
                addressName = addressName.Replace("[" + Enums_S3.AddressFormatKeyword.POBox + "]", address.POBox);
            }
            if (addressFormat.IndexOf("[" + Enums_S3.AddressFormatKeyword.Place + "]") > -1)
            {
                addressName = addressName.Replace("[" + Enums_S3.AddressFormatKeyword.Place + "]", address.Place);
            }
            if (addressFormat.IndexOf("[" + Enums_S3.AddressFormatKeyword.NearBy + "]") > -1)
            {
                addressName = addressName.Replace("[" + Enums_S3.AddressFormatKeyword.NearBy + "]", address.NearBy);
            }
            if (addressFormat.IndexOf("[" + Enums_S3.AddressFormatKeyword.BuildingNumber + "]") > -1)
            {
                addressName = addressName.Replace("[" + Enums_S3.AddressFormatKeyword.BuildingNumber + "]", address.BuildingNumber);
            }
            return addressName;
        }

        public string AddressName(int addressID, string addressFormat, Enums_S3.Configuration.Language lang)
        {
            string addressName = addressFormat;
            Address address = new Address();
            address = FindById(addressID, null);
            if (lang == Enums_S3.Configuration.Language.ar)
            {
                if (addressFormat.IndexOf("[" + Enums_S3.AddressFormatKeyword.Area + "]") > -1)
                {
                    if (address.Area != null)
                        addressName = addressName.Replace("[" + Enums_S3.AddressFormatKeyword.Area + "]", address.Area.NameArabic);
                    else
                        addressName = addressName.Replace("[" + Enums_S3.AddressFormatKeyword.Area + "]", string.Empty);
                }
                if (addressFormat.IndexOf("[" + Enums_S3.AddressFormatKeyword.City + "]") > -1)
                {
                    if (address.City != null)
                        addressName = addressName.Replace("[" + Enums_S3.AddressFormatKeyword.City + "]", address.City.CityNameAr);
                    else
                        addressName = addressName.Replace("[" + Enums_S3.AddressFormatKeyword.City + "]", string.Empty);
                }
                if (addressFormat.IndexOf("[" + Enums_S3.AddressFormatKeyword.Country + "]") > -1)
                {
                    if (address.Country != null)
                        addressName = addressName.Replace("[" + Enums_S3.AddressFormatKeyword.Country + "]", address.Country.CountryNameAr);
                    else
                        addressName = addressName.Replace("[" + Enums_S3.AddressFormatKeyword.Country + "]", string.Empty);
                }
            }
            else
            {

                if (addressFormat.IndexOf("[" + Enums_S3.AddressFormatKeyword.Area + "]") > -1)
                {
                    if (address.Area != null)
                        addressName = addressName.Replace("[" + Enums_S3.AddressFormatKeyword.Area + "]", address.Area.NameEnglish);
                    else
                        addressName = addressName.Replace("[" + Enums_S3.AddressFormatKeyword.Area + "]", string.Empty);
                }
                if (addressFormat.IndexOf("[" + Enums_S3.AddressFormatKeyword.City + "]") > -1)
                {
                    if (address.City != null)
                        addressName = addressName.Replace("[" + Enums_S3.AddressFormatKeyword.City + "]", address.City.CityNameEn);
                    else
                        addressName = addressName.Replace("[" + Enums_S3.AddressFormatKeyword.City + "]", string.Empty);
                }
                if (addressFormat.IndexOf("[" + Enums_S3.AddressFormatKeyword.Country + "]") > -1)
                {
                    if (address.Country != null)
                        addressName = addressName.Replace("[" + Enums_S3.AddressFormatKeyword.Country + "]", address.Country.CountryNameEn);
                    else
                        addressName = addressName.Replace("[" + Enums_S3.AddressFormatKeyword.Country + "]", string.Empty);
                }
            }
            if (addressFormat.IndexOf("[" + Enums_S3.AddressFormatKeyword.ZipCode + "]") > -1)
            {
                addressName = addressName.Replace("[" + Enums_S3.AddressFormatKeyword.ZipCode + "]", address.ZipCode);
            }
            if (addressFormat.IndexOf("[" + Enums_S3.AddressFormatKeyword.StreetAddress + "]") > -1)
            {
                addressName = addressName.Replace("[" + Enums_S3.AddressFormatKeyword.StreetAddress + "]", address.StreetAddress);
            }
            if (addressFormat.IndexOf("[" + Enums_S3.AddressFormatKeyword.Region + "]") > -1)
            {
                addressName = addressName.Replace("[" + Enums_S3.AddressFormatKeyword.Region + "]", address.Region);
            }
            if (addressFormat.IndexOf("[" + Enums_S3.AddressFormatKeyword.POBox + "]") > -1)
            {
                addressName = addressName.Replace("[" + Enums_S3.AddressFormatKeyword.POBox + "]", address.POBox);
            }
            if (addressFormat.IndexOf("[" + Enums_S3.AddressFormatKeyword.Place + "]") > -1)
            {
                addressName = addressName.Replace("[" + Enums_S3.AddressFormatKeyword.Place + "]", address.Place);
            }
            if (addressFormat.IndexOf("[" + Enums_S3.AddressFormatKeyword.NearBy + "]") > -1)
            {
                addressName = addressName.Replace("[" + Enums_S3.AddressFormatKeyword.NearBy + "]", address.NearBy);
            }
            if (addressFormat.IndexOf("[" + Enums_S3.AddressFormatKeyword.BuildingNumber + "]") > -1)
            {
                addressName = addressName.Replace("[" + Enums_S3.AddressFormatKeyword.BuildingNumber + "]", address.BuildingNumber);
            }
            return addressName;
        }

        #endregion

        #region IAddressManager Members

      
      

        #endregion
    }
}
