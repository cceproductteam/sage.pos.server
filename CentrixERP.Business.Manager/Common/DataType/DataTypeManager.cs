using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SF.Framework;
using SagePOS.Manger.Common.Business.Entity;
using CentrixERP.Common.Business.IManager;
using SagePOS.Server.DataAcces.IRepository;
using SagePOS.Common.DataAccess.IRepository;

namespace CentrixERP.Common.Business.Manager
{
    public class DataTypeManager : BusinessManagerBase<DataType, int>, IDataTypeManager
    {
        public DataTypeManager()
            : base((IRepository<DataType, int>)IoC.Instance.Resolve(typeof(IDataTypeRepository))) { }
    }
}
