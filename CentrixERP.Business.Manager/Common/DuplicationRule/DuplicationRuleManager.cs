using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.Framework;
using SF.FrameworkEntity;
using SagePOS.Manger.Common.Business.Entity;
using CentrixERP.Common.Business.IManager;
using SagePOS.Server.DataAcces.IRepository;
using SagePOS.Common.DataAccess.IRepository;

namespace CentrixERP.Common.Business.Manager
{
    
    public class DuplicationRuleManager: BusinessManagerBase< DuplicationRule, int>, IDuplicationRuleManager
    {
        #region "Constructors"
      
        public DuplicationRuleManager()
        : base((IRepository< DuplicationRule, int>)IoC.Instance.Resolve(typeof(IDuplicationRuleRepository))) { }
        
        #endregion

      
    }
}


