using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Manger.Common.Business.Entity;
using SF.Framework;
using CentrixERP.Common.Business.IManager;
using SagePOS.Server.DataAcces.IRepository;
using SagePOS.Common.DataAccess.IRepository;

namespace CentrixERP.Common.Business.Manager
{
    public class DocumentFolderManager : BusinessManagerBase<DocumentFolder,int>,IDocumentFolderManager
    {
        public DocumentFolderManager()
            : base((IRepository<DocumentFolder, int>)IoC.Instance.Resolve(typeof(IDocumentFolderRepository)))
        { }

        public void DeleteFolder(DocumentFolder myEntity, int newFolderId) { ((IDocumentFolderRepository)this.Repository).DeleteFolder(myEntity, newFolderId); }
    }
}
