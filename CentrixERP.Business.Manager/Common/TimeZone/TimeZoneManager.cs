using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Manger.Common.Business.Entity;
using SF.Framework;
using CentrixERP.Common.Business.IManager;
using SagePOS.Server.DataAcces.IRepository;
using SagePOS.Common.DataAccess.IRepository;

namespace CentrixERP.Common.Business.Manager
{
    public class TimeZoneManager : BusinessManagerBase<SagePOS.Manger.Common.Business.Entity.TimeZone, int>, ITimeZoneManager
    {
        public TimeZoneManager()
            : base((IRepository<SagePOS.Manger.Common.Business.Entity.TimeZone, int>)IoC.Instance.Resolve(typeof(ITimeZoneRepository)))
        { }
    }
}
