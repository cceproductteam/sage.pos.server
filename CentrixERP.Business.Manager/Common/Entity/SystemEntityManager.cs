using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Manger.Common.Business.Entity;
using SF.Framework;
using CentrixERP.Common.Business.IManager;
using SagePOS.Server.DataAcces.IRepository;
using SagePOS.Common.DataAccess.IRepository;

namespace CentrixERP.Common.Business.Manager
{
    public class SystemEntityManager : BusinessManagerBase<SystemEntity,int>,ISystemEntityManager
    {
        public SystemEntityManager()
            : base((IRepository<SystemEntity, int>)IoC.Instance.Resolve(typeof(ISystemEntityRepository)))
        { }


        public List<EntityRelations> CheckEntityRelations(int entityId, int entityValueId, CriteriaBase myCriteria) {
            return ((ISystemEntityRepository)this.Repository).CheckEntityRelations(entityId, entityValueId, myCriteria);
        }
    }
}
