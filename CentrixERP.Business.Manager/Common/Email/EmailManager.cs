using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.Framework;
using SF.FrameworkEntity;
using SagePOS.Manger.Common.Business.Entity;
using CentrixERP.Common.Business.IManager;

using SagePOS.Common.DataAccess.IRepository;

namespace CentrixERP.Common.Business.Manager
{

    public class EmailManager : BusinessManagerBase<Email, int>, IEmailManager
    {
        #region "Constructors"

        public EmailManager()
            : base((IRepository<Email, int>)IoC.Instance.Resolve(typeof(IEmailRepository))) { }

        #endregion

        public List<Email> FindEmailList(string ids, bool forPerson)
        {
            return ((IEmailRepository)this.Repository).FindEmailList(ids, forPerson);
        }

    }
}


