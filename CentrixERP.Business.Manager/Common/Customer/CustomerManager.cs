using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;
using SF.Framework;
using SagePOS.Server.Business.IManager;
using SagePOS.Server.DataAcces.IRepository;

namespace SagePOS.Server.Business.Manager
{
    public class CustomerManager : BusinessManagerBase<Customer, int>, ICustomerManager
    {
        public CustomerManager()
            : base((IRepository<Customer, int>)IoC.Instance.Resolve(typeof(ICustomerRepository)))
        { }

        public List<CustomerLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((ICustomerRepository)this.Repository).FindAllLite(myCriteria);
        }

        public CustomerLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((ICustomerRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }


        public List<object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((ICustomerRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }


        public double FindPendingAmount(int Id, CriteriaBase myCriteria)
        {
            return ((ICustomerRepository)this.Repository).FindPendingAmount(Id, myCriteria);
        }

        public List<POSIntegrationCustomer> POSIntegrationCustomerFindAll(DateTime? LastSyncDate)
        {

            return ((ICustomerRepository)this.Repository).POSIntegrationCustomerFindAll(LastSyncDate);
        }
    }
}