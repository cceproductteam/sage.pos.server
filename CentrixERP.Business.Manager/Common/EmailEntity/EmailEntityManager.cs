using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Manger.Common.Business.Entity;
using SF.Framework;
using CentrixERP.Common.Business.IManager;
using SagePOS.Server.DataAcces.IRepository;
using SagePOS.Common.DataAccess.IRepository;

namespace CentrixERP.Common.Business.Manager
{
    public class EmailEntityManager : BusinessManagerBase<EmailEntity,int>,IEmailEntityManager
    {
        public EmailEntityManager()
            : base((IRepository<EmailEntity, int>)IoC.Instance.Resolve(typeof(IEmailEntityRepository)))
        {
        
        
        }

        public EmailEntityLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((IEmailEntityRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<EmailEntityLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((IEmailEntityRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((IEmailEntityRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }

        public override List<ValidationError> Validate(EmailEntity myEntity)
        {
            List<ValidationError> errors = new List<ValidationError>();
            if (SF.Framework.String.IsEmpty(myEntity.EmailObj.EmailAddress))
            {
                errors.Add(new ValidationError()
                {
                    ControlName = "EmailAddress",
                    GlobalMessage = false,
                    Line = 1,
                    Message = "required"
                });
            }
            return errors;
        }

      
    }
}
