using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Manger.Common.Business.Entity;
using CentrixERP.Common.Business.IManager;
using SagePOS.Server.DataAcces.IRepository;
using SF.Framework;
using SagePOS.Common.DataAccess.IRepository;

namespace CentrixERP.Common.Business.Manager
{
    public class RecentViewManager : BusinessManagerBase<RecentView, int>, IRecentViewManager
    {
         public RecentViewManager()
            : base((IRepository<RecentView, int>)IoC.Instance.Resolve(typeof(IRecentViewRepository))) { }
    }
}
