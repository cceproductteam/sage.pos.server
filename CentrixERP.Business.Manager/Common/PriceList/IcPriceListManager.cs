using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.Entity;
using SF.Framework;
using SagePOS.Server.Business.IManager;
using SagePOS.Server.DataAcces.IRepository;

namespace SagePOS.Server.Business.Manager
{
    public class IcPriceListManager : BusinessManagerBase<IcPriceList,int>,IIcPriceListManager
    {
        public IcPriceListManager()
            : base((IRepository<IcPriceList, int>)IoC.Instance.Resolve(typeof(IIcPriceListRepository)))
        {
        
        
        }

        public IcPriceListLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((IIcPriceListRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<IcPriceListLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((IIcPriceListRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((IIcPriceListRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }
    }
}
