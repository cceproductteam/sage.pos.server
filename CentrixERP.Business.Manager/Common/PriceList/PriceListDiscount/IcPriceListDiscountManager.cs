﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Server.Business.IManager;

using SagePOS.Server.Business.Entity;
using SF.Framework;
using SagePOS.Server.DataAcces.IRepository;

namespace SagePOS.Server.Business.Manager
{
    public class IcPriceListDiscountManager : BusinessManagerBase<PriceListDiscount, int>, IIcPriceListDiscountManager
    {
        public IcPriceListDiscountManager()
            : base((IRepository<PriceListDiscount, int>)IoC.Instance.Resolve(typeof(IIcPriceListDiscountRepository)))
        {


        }

        public PriceListDiscountLite FindByIdLite(int Id, CriteriaBase myCriteria)
        {
            return ((IIcPriceListDiscountRepository)this.Repository).FindByIdLite(Id, myCriteria);
        }
        public List<PriceListDiscountLite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((IIcPriceListDiscountRepository)this.Repository).FindAllLite(myCriteria);
        }

        public List<Object> AdvancedSearchLite(CriteriaBase myCriteria)
        {
            return ((IIcPriceListDiscountRepository)this.Repository).AdvancedSearchLite(myCriteria);
        }
    }
}