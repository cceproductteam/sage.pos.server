using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Manger.Common.Business.Entity;
using SF.Framework;
using CentrixERP.Common.Business.IManager;

using SagePOS.Common.DataAccess.IRepository;

namespace CentrixERP.Common.Business.Manager
{
    public class EntityControlConfigEventsManager : BusinessManagerBase<EntityControlConfigEvents,int>,IEntityControlConfigEventsManager
    {
        public EntityControlConfigEventsManager()
            : base((IRepository<EntityControlConfigEvents, int>)IoC.Instance.Resolve(typeof(IEntityControlConfigEventsRepository)))
        { }

        public List<EntityControlConfigEvents_Lite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((IEntityControlConfigEventsRepository)this.Repository).FindAllLite(myCriteria);
        }
    
    }
}
