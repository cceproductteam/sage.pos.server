using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Manger.Common.Business.Entity;
using SF.Framework;
using CentrixERP.Common.Business.IManager;
using SagePOS.Server.DataAcces.IRepository;
using SagePOS.Common.DataAccess.IRepository;

namespace CentrixERP.Common.Business.Manager
{
    public class EntityControlConfigManager : BusinessManagerBase<EntityControlConfig,int>,IEntityControlConfigManager
    {
        public EntityControlConfigManager()
            : base((IRepository<EntityControlConfig, int>)IoC.Instance.Resolve(typeof(IEntityControlConfigRepository)))
        { }

        public List<EntityControlConfig_Lite> FindAllLite(CriteriaBase myCriteria)
        {
            return ((IEntityControlConfigRepository)this.Repository).FindAllLite(myCriteria);
        }
    }
}
