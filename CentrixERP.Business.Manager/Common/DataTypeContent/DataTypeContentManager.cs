using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.Framework;
using SF.FrameworkEntity;
using SagePOS.Manger.Common.Business.Entity;
using CentrixERP.Common.Business.IManager;
using SagePOS.Server.DataAcces.IRepository;
using SagePOS.Common.DataAccess.IRepository;

namespace CentrixERP.Common.Business.Manager
{
    public class DataTypeContentManager : BusinessManagerBase<DataTypeContent, int>, IDataTypeContentManager
    {
        public DataTypeContentManager()
            : base((IRepository<DataTypeContent, int>)IoC.Instance.Resolve(typeof(IDataTypeContentRepository))) { }

        #region IDataTypeContentManager Members

        void IDataTypeContentManager.ReorderDataTypeContent(string dataTypeContentIDs)
        {
            ((IDataTypeContentRepository)Repository).ReorderDataTypeContent(dataTypeContentIDs);
            //IDataTypeContentRepository repository =(IDataTypeContentRepository) IoC.Instance.Resolve(typeof(IDataTypeContentRepository));
            //Repository.ReorderDataTypeContent(dataTypeContentIDs);

        }

        #endregion
        List<DataTypeContentLite> IDataTypeContentManager.FindAllByIds(string Ids)
        {
            return ((IDataTypeContentRepository)Repository).FindAllByIds(Ids);
        }

    }
}
