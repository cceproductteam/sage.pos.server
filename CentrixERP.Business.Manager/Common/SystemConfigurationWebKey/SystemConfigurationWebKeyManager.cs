using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SF.FrameworkEntity;
using SagePOS.Manger.Common.Business.Entity;
using SF.Framework;
using CentrixERP.Common.Business.IManager;
using SagePOS.Server.DataAcces.IRepository;
using SagePOS.Common.DataAccess.IRepository;

namespace CentrixERP.Common.Business.Manager
{
    public class SystemConfigurationWebKeyManager : BusinessManagerBase<SystemConfigurationWebKey,int>,ISystemConfigurationWebKeyManager
    {
        public SystemConfigurationWebKeyManager()
            : base((IRepository<SystemConfigurationWebKey, int>)IoC.Instance.Resolve(typeof(ISystemConfigurationWebKeyRepository)))
        { }
    }
}
