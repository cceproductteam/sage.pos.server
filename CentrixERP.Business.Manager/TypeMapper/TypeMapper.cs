using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.IO;
using System.Xml.Linq;
using SF.Framework;
//using Centrix.StandardEdition.CRM.Business.IManager;

namespace SagePOS.Manger.Business.Factory
{
    public class TypeMapper<T>
    {
        private string xmlPath;
        private T entityObj;
        private Object iManager;
        private Object entityCriteria;
        private Type entityParent;

        public T EntityObject
        {
            get { return this.entityObj; }
        }
        public Object IManager
        {
            get { return this.iManager; }
        }
        public Object EntityCriteria
        {
            get { return this.entityCriteria; }
        }
        public Type EntityParent
        {
            get { return this.entityParent; }
        }

        public TypeMapper(string moduleKey)
        {
            xmlPath = ConfigurationManager.AppSettings["TypeMapperPath"].ToString();
            CheckFileExists();
            GetType(moduleKey);
        }

        private void CheckFileExists()
        {
            if (!File.Exists(this.xmlPath))
                throw new Exception("The TypeMapper.xml file is not exists!!!!!");
        }

        private void GetType(string moduleKey)
        {
            string mapParent = null;
            try
            {
                XDocument doc = XDocument.Load(this.xmlPath);
                var tmp = from XElement element in doc.Elements("TypeMapper").Elements("Type")
                          where element.Attribute("Key").Value == moduleKey
                          select element;
                if (tmp == null && tmp.Count() == 0)
                    return;

                XElement type = (XElement)tmp.First();
                this.entityObj = (T)Activator.CreateInstance(Type.GetType(type.Element("Entity").Value));
                this.iManager = IoC.Instance.Resolve(Type.GetType(type.Element("IManager").Value));
                if (type.Element("Criteria").Value != null && type.Element("Criteria").Value != "")
                    this.entityCriteria = Activator.CreateInstance(Type.GetType(type.Element("Criteria").Value));
                mapParent = type.Attribute("mapToParent").Value;

                //get entity parent
                var parent = from XElement el in doc.Elements("TypeMapper").Elements("Parents").Elements()
                             where el.Attribute("Name").Value == mapParent
                             select el;

                if (parent != null && parent.Count() != 0)
                {
                    XElement eParent = (XElement)parent.First();
                    this.entityParent = Type.GetType(eParent.Value);
                }
            }
            catch (Exception ex) 
            {
                throw ex;
            }
        }
    }
}
